module assert
function assert
// echoblock(arr, indent="  ", v="| ", t="-", b="-", lt=".", lb="'")
module echoblock(arr, indent="  ", v="| ", t='-', b="-", lt=".", lb="'")
{
	lines = arrblock(arr, indent=indent, v=v,t=t,b=b, lt=lt, lb=lb);

	_Div( join(lines,"<br/>")
		, "font-family:Droid Sans Mono"
		);  
}

    echoblock=[ "echoblock", "arr", "", "Doc, Console" , 
    " Given an array of strings, echo them as a block. Arguments:
    ;;
    ;;   indent: prefix each line
    ;;   v : vertical left edge, default '| '
    ;;   h : horizontal line on top and bottom, default '-'
    ;;   lt: left-top symbol, default '.'
    ;;   lb: left-bottom symbol, default \"'\"
    ;;
    ;; See arrblock()
    "];

    function echoblock_test( mode=MODE, opt=[] )=
    (
        doctest( echoblock, mode=mode, opt=opt )
    );
 
