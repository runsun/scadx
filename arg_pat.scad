include <scadx_core_dev.scad>
/*
  Developing an argument pattern for group subunits
  
  group --> members 
  
  arms --> arm.l, arm.r
  axes --> axis.x, axis.y
  labels --> label.p, label.q, label.r
  

  [A] Set default at design time. At DT, defaults of display and other options
      (in both group and members) are set seperately. Display is set at the 
      obt option, df_ops, while individual ops are set seperately:
  
    A1. Set the display (show or hide) of each group and each member
        at the obj option, df_ops:
       
       df_ops= [               // Set group display               
               , "axes", true
               , "labels", false 
               , "arms", true
                               // Set member display, if diff from above
               , "armL", false    
               ]
               
    A2. Default of each group option is set seperately. Allowed: hash, []
    
        df_arms= ["len",3]
        df_labels = []
        df_axes = []
        
    A3. Default of each member is set seperately. Allowed: hash, []
        
        df_armL=[]
        df_armR=[]
        df_lblx=[]
        df_lbly=[]
        df_ax_x=[]
        df_ax_y=[]
    
  [B] User input options at run-time. Note that, different from the default
      options that the display and non-display settings, user input along 
      takes care of both display and non-display with one setting, like
     
       arms = false  --- hide
       arms = true   --- show using defaults
       arms = hash   --- show using hash 
  
     B1. Set group, allowed: true,false,hash
     
        Obj( [ "arms",true  // this one is redundunt, 'cos its df already true
             , "labels",true
             , "axes,["color","red"]
             ] 
           )
           
     B1. Set member, allowed: true,false,hash
     
        Obj( [ "arms",false           // hide all arms, unless ...
             , "armL", ["len", 3]     // Set armL len=3, which also turns on armL
             
             ,"labels", true          // Show all labels
             ,"labelx",["transp",0.3] // labelx has transp= 0.3
             
             , "axes,["color","red"]  // Set color of all axes to red 
             , "ax_x", false          // but turn ax_x off
             ] 
           )
*/        
//        
//function subops( 
//      u_ops  
//    , df_ops   /* Default obj ops set at the design time, containing the
//                  settings for show/hide of group and members 
//      
//                  [ "r",1          // general option
//                  , "arms", true   // Show all arms
//                  , "armL", false  // But hide armL
//                  ]  
//                  
//                  Note that only true|false are allowed. Or you can skip it
//                  to let the other settings decide.
//               */
//      , df_subs  /* A hash of two pairs showing both group and member defaults. 
//                   It's arranged as group first, and member the 2nd:
//                    
//                    [ group_name, df_group, memb_name, df_memb]
//                   
//                    Example: [ "arms", df_arms, "armL", df_armL ] 
//                 */
//    , com_keys  /* Names of sub ops, which you want them to be also be set 
//                   from the obj level. Ex,["r", "color"]
//                   Setting these options at the obj level, like 
//                   
//                   Obj( ... ops= ["r",1] )  
//                   
//                   set group and member r to 1. Note: 
//                   
//                   -- This doesn't affect the display setting
//                   -- Different groups can have different com_keys
//                */
//    )=
//(  let( keys  = keys( df_subs )    // name of subunit   ["arms", "armL"]
//      , gn = keys[0]            // group name, "arms"
//      , mn = keys[1]            // member name, "armL"
//      , subs  = vals( df_subs )    // [df_arms, df_armL]
//      , df_com= com_keys?
//         [ for(i=range(len(com_keys)*2) ) // part of df ops that have 
//                                          // keys showing up in com_keys
//              let( isk= iseven(i)
//                 , k= com_keys[ 2*floor(i/2) ]
//                 , v= hash( df_ops,k)
//                 ) 
//              if( v!=undef ) isk? k:v
//          ]:[]  
//      , df =[ df_com, df_subs[0], df_subs[1] ]
//              
//      // Chk show/hide
//      , df_show_grp = hash( df_ops, str("show_",gn) )
//      , df_show_mem = hash( df_ops, str("show_",mn) )
//      , df_show = !(  df_show_mem== false
//                   || ( df_show_mem== undef
//                      && df_show_grp==false
//                      )
//                   )
//              
//      // Take care of user input below
//      , u_grp = hash( u_ops, gn) 
//      , u_mem = hash( u_ops, mn) 
//      , u_show = isbool(u_mem)? u_mem
//                 : [u_grp, u_mem]==[undef,undef]?  df_show
//                 : [u_grp, u_mem]==[false,undef]?  false
//                 : true     // not found(=Everything else):true
//                    
//      , u_com= com_keys?
//         [ for(i=range(len(com_keys)*2) ) // part of user ops that have 
//                                          // keys showing up in com_keys
//              let( isk= iseven(i)
//                 , k= com_keys[ 2*floor(i/2) ]
//                 , v= hash( u_ops,k) 
//                 )
//              if( v!=undef ) isk? k:v
//          ]:[]   
//      , u = [u_com, u_grp, u_mem ]  
//      
//      ,subops= update( app( df, u ) )
//   )           
//
//   u_show? subops 
//        : false
//);        
//     
//===============================================
subops=["subops", "u_ops,df_ops,df_subs,com_keys,debug=false","false|hash", "Core"
," Get the ops settings of a specific member of a group subunits for 
;; displaying that member. Say, an obj has left arms and right arms.
;; denoted as: Obj:arms {armL,armR}
;; subops, called inside Obj, allows we to:  
;; (1) Set defaults of show/hide and format both arms, together or 
;;     separately;
;; (2) Set/disable both arms at once 
;; (3) Set/disable/enable each arms seperately (by revoking (1))
;;
;; Brief usage guide (see subops_demo() for details):
;; 
;; @DT:
;; 
;;    module Obj( ops=[])
;;    {
;;    //==========  define ===============
;;      df_ops=  
;;       [  \"r\",1       // obj-level-properties
;;       , \"show_arms\", false
;;       , \"show_armL\", true  // if different 
;;       ];
;;      com_keys = [\"color\"]; // names of common properties
;;      // Default ops for each group and member
;;      df_arms=[\"len\",3];       
;;      df_armL=[\"transp\",0.6];
;;      df_armR=[];
;;        
;;      opsL= subops( 
;;                      u_ops= ops  
;;                    , df_ops = df_ops
;;                    , df_subs= [\"arms\", df_arms, \"armL\", df_armL ]
;;                    , com_keys= com_keys
;;                    //, debug=true
;;                    );
;;    function opsL(k) = hash( ops_armL,k);
;;    //==========  use ===============
;;    if(opsL){ 
;;        .... color(opsL(\"color\"), opsL(\"transp\"))
;;            cube( size=[0.5,ops_armR(\"len\"),0.5] );
;;     ...}
;;
;; @RT:
;;
;;    Obj( [\"arms\", true] )
;;    Obj( [\"arms\", [\"r\",2], \"armL\", false] )
;;    Obj( [\"arms\", [\"r\",2], \"armL\", [\"color\",\"red\"]] )
"];
    
function subops( 
      u_ops  
    , df_ops   /* Default obj ops set at the design time, containing the
                  settings for show/hide of group and members 
      
                  [ "r",1          // general option
                  , "arms", true   // Show all arms
                  , "armL", false  // But hide armL
                  ]  
                  
                  Note that only true|false are allowed. Or you can skip it
                  to let the other settings decide.
               */
      , df_subs  /* A hash of two pairs showing both group and member defaults. 
                   It's arranged as group first, and member the 2nd:
                    
                    [ group_name, df_group, memb_name, df_memb]
                   
                    Example: [ "arms", df_arms, "armL", df_armL ] 
                 */
    , com_keys  /* Names of sub ops, which you want them to be also be set 
                   from the obj level. Ex,["r", "color"]
                   Setting these options at the obj level, like 
                   
                   Obj( ... ops= ["r",1] )  
                   
                   set group and member r to 1. Note: 
                   
                   -- This doesn't affect the display setting
                   -- Different groups can have different com_keys
                */
    , debug=false            
    )=
(  let( keys  = keys( df_subs )    // name of subunit   ["arms", "armL"]
      , gn = keys[0]            // group name, "arms"
      , mn = keys[1]            // member name, "armL"
      , df_com= com_keys?
            [ for(i=range(df_ops)) // part of user ops that have 
                                           // keys showing up in com_keys
                      let( key= df_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) df_ops[i]
                  ]:[] // ["r",1]                       
      , df = joinarr(updates( [ df_com, df_subs[1], df_subs[3] ] ))
              
      // Chk show/hide
      , df_show_grp = hash( df_ops, str("show_",gn) )
      , df_show_mem = hash( df_ops, str("show_",mn) )
      , df_show = !(  df_show_mem== false
                   || ( df_show_mem== undef
                      && df_show_grp==false
                      )
                   )          
      // Take care of user input below
      , u_grp = hash( u_ops, gn,[] )
      , u_mem = hash( u_ops, mn,[] )
      , u_show = isbool(u_mem)? u_mem
                 : [u_grp, u_mem]==[[],[]]?  df_show
                 : [u_grp, u_mem]==[false,[]]?  false
                 : true     // not found(=Everything else):true
      , u_com= com_keys?
                [ for(i=range(u_ops)) // part of user ops that have keys 
                                      // showing up in com_keys
                      let( key= u_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) u_ops[i]
                  ] // ["r",1]
                  :[] 
      , user = joinarr( updates([u_com, u_grp, u_mem ]) )
      
      ,rtn= u_show? joinarr(update( df, user  )):false
   )           
   debug?
   _fmth( 
   [ "###", _red(" OUTPUT OF subops_debug() ")
   , "///", "subops(): get the ops of a group member. This function,"
   , "///", "subops_debug(), is the same, but outputs all vars that are "
   , "///", "either defined (for checking if defined wrong) or calculated"
   , "///", "(for verifying calc)"           
   , "###", "The Args You Give When Calling subops()"
   , "///", "These are args of subops() called inside Obj() to set ops"
   , "///", " of a group member:"
   , "///", " ops_armL = subops( u_ops, df_ops, df_subs, com_keys )" 
              
   , "u_ops", u_ops  
       , "///", "u_ops is taken from Obj args:"
       , "///", "  Obj(u_ops){ ... ops_armL= subops(u_ops) ... }"           
   , "///", "The following 3 are defined in Obj(). See below."       
   , "df_ops", df_ops
   , "df_subs", df_subs
   , "com_keys", com_keys
   , "///",""    
   , "###", "Define Default Options"
   , "///", "The following need to be defined in Obj() for subops()"

   , "df_ops", df_ops
       , "///", "df_ops: obj level default, defining everything obj needs."
   , "df_subs", df_subs
       , "///", "df_subs: define defaults of group and member as"
       , "///", "   df_arms=xxx; df_armL=ooo:"
       , "///", "  and sent to subops as:"
       , "///", "   df_subs= [ \"arms\",df_arms, \"armL\",df_armL ]"           
   , "com_keys", com_keys
       , "///", "com_keys: member properties that can be set at obj level at RT"
              
   , "###", "Set Default Display"
   , "///", "Display default is defined in df_ops as:"
   , "///", "  df_ops= [...\"show_arms\", true, \"show_armL\", false...]"
   , "///", "They are extracted as df_show_grp, df_show_mem"
   , "df_show_grp", df_show_grp
   , "df_show_mem", df_show_mem
   , "df_com", df_com
   , "///", "df_com is value of com_keys in df_ops. Like: "
   , "///", "[\"color\", \"red\", \"transp\", 1]"
   , "df", df
   , "///", "df_is the final result of default settings."
   , "df_show", df_show
   , "///", "df_show: show or hide decided in default."
   
   , "###", "User input @ RT"
   , "u_ops", u_ops
   , "///", "u_ops: entered by user"
   , "u_com", u_com
   , "///", "Hash of the com_keys defined in u_ops as:"
   , "///", "  u_ops=[ ... \"color\",xxx ...] "
   , "///", "If it's [], means user doesn't set com_keys"
   , "u_grp", u_grp
   , "///", "u_grp: user-given group ops, enter in u_ops as:"
   , "///", "  u_ops=[ ... \"arms\",xxx ...] where xxx could be"
   , "///", "true|false|hash, or it could be [] if not given"
   , "u_mem", u_mem
   , "///", "u_mem: user-given member ops, enter in u_ops as:"
   , "///", "  u_ops=[ ... \"armL\",xxx ...] where xxx could be"
   , "///", "true|false|hash, or it could be [] if not given"
   , "user", user
   , "###", "Result"
   , "u_show",u_show
   , "rtn", rtn 
  ], pairbreak="<br/>,")
  :rtn
);      
              

module subops_demo()
{ 
    /* 
    This module serves as an example of using subops().
    It uses an Obj() containing a "group" called arms, and 2 members
    called "armL" and "armR", denoted as (for the sake of describing it)
    
      Obj:arms{armL,armR}
    
    The quickest way of making good use of it is to copy the entire
    Obj() code, and modify from there. 
    */
    
    echom("subops_demo()");
    
    module Obj( ops=[], transl=[0,0,0] )
    {
        df_ops=  // df_ops is an overall, obj-level ops. Has 2 parts.
        [ 
           // [1] Default obj level properties
        
             "color",undef
           , "transp", 1
           , "r", 0.5
        
           // [2] Default display boolean for group (arms) and members.
           // You can play around by changing the booleans below to see
           // what they cause.
           // 
           // The setting for a member is only needed when it's
           // different from that of the group:
        
           , "show_arms", false
           //, "show_armL", false  // can skip if same as show_arms
           , "show_armR", true  // can skip if same as show_arms
           ];
        com_keys = ["transp","color"]; // Set properties in com_keys
                                       // in Obj ops also set those 
                                       // of sub units

        // Default ops for each group and member
        df_arms=["len",3];       
        df_armL=["transp",0.6];
        df_armR=[];
        
        u_ops = ops;    // Rename for clarity
        _ops= update(df_ops, u_ops); // Needed only if func ops()
                                     // in the next line is used
                                     // You can always use hash(u_ops,k,df) 
        function ops(k,df)= hash( _ops,k,df);
        
        // Inside one Obj, all sub unit members share at least 2
        // arguments, u_ops and df_ops. The com_keys can be different,
        // and ndf_subs will difinitely be. 
        //  
        // Lets say, Obj contains 2 groups:
        //   Labels {labelx, labely}
        //   Axes {ax_x, ax_y}
        // 
        // We can have:
        //   com_keys_labels = ["color"]
        //   com_keys_axes = ["r"]
        // 
        // Thus, when set color like: Obj( ...["color","red"] ...), it
        // also sets color of all labels, but not those of axes. 
        
        // IMPORTANT: To debug:
        //  
        //  1. uncomment the line, 
        //      //, debug=true 
        //  2. uncomment the line
        //      //echo( ops_armL );
        //
        // This will echo details of variables
        function sub_ops(df_subs, com_keys=com_keys)=
               subops( 
                      u_ops= u_ops  
                    , df_ops = df_ops
                    , df_subs= df_subs
                    , com_keys= com_keys
                    //, debug=true
                    );
        ops_armL = sub_ops(["arms", df_arms, "armL", df_armL ]);
        ops_armR = sub_ops(["arms", df_arms, "armR", df_armR ] );
        //echo( ops_armL );
        //echo( ops_armR );
        
        function ops_armL(k,df) = hash(ops_armL,k,df);
        function ops_armR(k,df) = hash(ops_armR,k,df);
       
        // Member ops are all set. Start using them:
        
        translate(transl) color(ops("color"), ops("transp"))
        cube( size=[0.5,1,2] );
        if ( ops_armR )
        {   
            translate( transl+[0,1, 1]) 
            color(ops_armR("color"), ops_armR("transp"))
            cube( size=[0.5,ops_armR("len"),0.5] );
        }    
        if ( ops_armL )
        {    translate( transl+[0,-ops_armL("len"), 1]) 
            color(ops_armL("color"), ops_armL("transp"))
            cube( size=[0.5,ops_armL("len"),0.5] );
            
        }   
    }  
    
    all_ops= [
         []
        , ["arms",false]  // 1
        ,["armL",false]  // 2
        ,["arms",["len",2]] // 3
        ,["arms",["len",2], "armL", false] // 4
        ,["arms",["len",2, "armL", false]] // 5 <== this doesn't work; keep armL at the obj level
        ,["arms",["len",2], "armL", ["color","red", "len",1]] // 7
        ,["color","red", "transp", 0.3] // 8
        ,["transp", 0.5, "armL", ["transp", 1] ] // 9  
        ,["armR",["color", "blue"]]
        ,["armR",["len",1]]
    ];
    for(i=range(all_ops)){
        //echo(i= str("<br/>===================== #",i+1, "========"));
        echo(str(i, ": ", arg= all_ops[i]));
        Obj( all_ops[i], [1.5*(i+1), 0, 0] );
        LabelPt( [(i+1)*1.5+0.3, 4.3, 1.5], str(i+1, " ops=", all_ops[i]) 
               , ["color", "blue"] );
    }
}                
subops_demo();






