include <scadx.scad>

// Note [2018.10.23]
// This file 'seems' to be written when scadx tests were written as 
// module. They were later converted to functions so this file should
// be considered obsolete.  

//
// The funcs here are usually called by scadx_doctesting_dev.scad.
// To test them directly in this file, un-mark the following line:
//include <../doctest/doctest_dev.scad>
//

/* 
=================================================================
                    scadx_core_test.scad
=================================================================
*/



//module angle_test( mode=112 ) { //======================
//	p = [1,0,0];
//	q = [0,sqrt(3),0];
//	r = ORIGIN;
//
//    pqr1=[ p,q,r];
//	pqr2=[ p,r,q];
//	pqr3= [ r,p,q];
//	pqr4= randPts(3);
//	pqr5= randPts(3);
//	pqr6= randPts(3);
//	pqr7= [ORIGIN, [1,0,0], [3,2,0]];
//    
//    line1 = [[0,0,0],[0,0,1]];
//    line2 = [[1,0,0],[2,0,0]]; 
//    twolines = [randPts(2),randPts(2)];
//
////    function lines2()= [randPts(2), randPts(2)]; 
////    function angarg(lineHasToMeet)= str( "[randPts(2),randPts(2)], lineHasToMeet="
////                                       , lineHasToMeet );
//	doctest( angle,
//	[
//		[angle(pqr1),30, "pqr1", 30 ]
//		,[angle(pqr2),90, "pqr2", 90 ]
//		,[angle(pqr3),60, "pqr3", 60 ]
//		,[angle(pqr7),135, "pqr7", 135 ]
//		,"// The following 3 are random demos. No test."
//        ,[ angle( randPts(3) ),"","randPts(3)",["notest",true] ]
//        ,[ angle( randPts(3) ),"","randPts(3)",["notest",true] ]
//        ,[ angle( randPts(3) ),"","randPts(3)",["notest",true] ]
//        ,"// angle of two lines:"
//        //, str("Are line1,line2 on plane? ",isOnPlane( app(line1, line2[0]), line2[1]))
//        //, str("lineCrossPt = ", lineCrossPt( line1,line2 ) )
//        ,[ angle( [ line1,line2 ], lineHasToMeet=true), 90
//         , "[line1,line2], lineHasToMeet=true" ] 
//        ,[ angle( [ line1,line2 ], lineHasToMeet=false), 90
//         , "[line1,line2], lineHasToMeet=false" ]
//
//        ,"// twolines: randomly generated 2x2 array: [ [p1,q1], [p2,q2] ] "
////        ,[ angle( twolines, lineHasToMeet=true )
////          , "twolines, lineHasToMeet=true",""
////          ,["notest",true]]
//        ,[ angle( twolines, lineHasToMeet=false ), ""
//          , "twolines, lineHasToMeet=false"
//          ,["notest",true]]
//
//	
//	], mode=mode, scope=["pqr1",pqr1,"pqr2",pqr2, "pqr3",pqr3
//                      //  ,"pqr4",pqr4, "pqr5",pqr5, "pqr6",pqr6
//                        ,"pqr7", pqr7
//                        ,"line1",line1, "line2",line2
//                        ,"lines", twolines
//                        ]
//	);
//}
//

module angle_series_test( mode=112 ){  // What is this ????? 2018.10.23
   _pqr= randPts(3);
   echo( _pqr=_pqr );
   P=P(_pqr); Q=Q(_pqr);
    
   //angs = [10,30,45,60,90,120,150, 170];
   _angs= [for(i=range(10)) rand( 1,180 )];
   angs = quicksort(_angs);
   
   Rs = [ for(a=angs) anglePt(_pqr, a=a) ]; 
   precs= [5,10,20,40,60,"-"]; 
   recs = [ for( i=range(Rs),p=precs )
            let( a=angs[i], R=Rs[i], pqr=[P,Q,R] 
               , ang= p!="-"?angle(pqr):0
               , angcor = p!="-"?angle_cordic(pqr, prec=p):0
               ) 
            p=="-"? "//-----------------------"
            :_s("a={_},p={_}, [angle,angle_cordic, diff] = [{_}, {_}, {_}]"
              , [a,p,ang,angcor, ang-angcor]
              )
          ];   
   doctest(angle_series,
    recs, mode=mode );
}

module angleBisectPt_test( mode=112 ){ //====================== 
	scope=[ "pqr1", randPts(3)
		 , "pqr2", randPts(3)
		 ];
	pqr1= hash(scope, "pqr1");
	pqr2= hash(scope, "pqr2");

	angle1 = angle( pqr1 );
	angle2 = angle( pqr2 );

	doctest( angleBisectPt, 
	[
		""
		, "We get 2 random 3-pointers, pqr1 and pqr2, to see if the angle is divided evenly"
		,""
        ,str( "angle1= ", angle1 )
    
		,[angle([pqr1[0],pqr1[1], angleBisectPt( pqr1 )])*2
                , angle1, ""
                //, ["myfunc","angle([p,q,angleBisectPt(pqr1)])*2"]
                , ["myfunc",["angle{_}*2","[p,q,angleBisectPt(pqr1)]"]]
        ]
		,""
        ,str( "angle2= ", angle2 )

		,[angle([pqr2[0],pqr2[1], angleBisectPt( pqr2 )])*2 
                , angle2, ""
                //, ["myfunc","angle([p,q,angleBisectPt(pqr2)])*2"]
                , ["myfunc",["angle{_}*2","[p,q,angleBisectPt(pqr2)]"]]
         ]       
	]
	, mode=mode, scope=scope
	);
}

module anglePt_test( mode=112 ){  //======================
	doctest( anglePt, [], mode=mode );
}

module anyAnglePt_test( mode=112 ){  //====================== 
	doctest( anyAnglePt, 
	[], mode=mode
	);
}


module app_test( mode=112 ){  //======================
   doctest( app
    ,[ 
        [ app( [1,2,3],4 ),  [1,2,3,4], "[1,2,3], 4" ]
    ], mode=mode );
}


module appi_test( mode=112 ){  // =========================
    doctest( appi
    , [
        [ appi([1,2,[3,33],4], 2,9), [1,2,[3,33,9],4],"[1,2,[3,33],4], 2,9" ]
      , [ appi([1,2,[3,33],4],-2,9), [1,2,[3,33,9],4], "[1,2,[3,33],4],-2,9" ]
      ,""
      ,"// If item-i not an array:"
      ,""
      //,str( "> appi([1,2,[3,33],4],1,9) = ",  appi([1,2,[3,33],4],1,9)  )
      ,[appi([1,2,[3,33],4],1,9), "", "[1,2,[3,33],4],1,9)",  
              ["notest",true] ]
      ,""
      ,"// Set conv=true to turn it into an array:"
      ,""
      , [ appi([1,2,[3,33],4],1,9,true), [1,[2,9],[3,33],4], "[1,2,[3,33],4],1,9,true"
     ]
    
    ], mode=mode 
    );
}


module arcPts_test( mode=112 ){  // =========================
	doctest( arcPts, mode=mode );
}


module arrblock_test( mode=112 ){ //=================
    br="<br/>";
	doctest
	(
		arrblock
		,[
            [ join( arrblock(["abc","def"]), br)
            , join( [ "  .---------------------------------------------"
			  		, "  | abc"
			  		, "  | def"
			  		, "  '---------------------------------------------"
					], br)
            , ["abc","def"]
            , ["nl",true, "myfunc", "join( arrblock(['abc','def']), br) "] 
           ]  
    
		,	[ join(arrblock(["abc","def","ghi"], lnum=1),br)
            , join([ "  .---------------------------------------------"
			  		, "  | 1. abc"
			  		, "  | 2. def"
			  		, "  | 3. ghi"
			  		, "  '---------------------------------------------"
					], br ) 
			, "['abc','def', 'ghi'], lnum=1"
			, ["nl",true
              , "myfunc"
                 , ["join", "arrblock( ['abc','def','ghi'], lnum=1), br"]
              ] 
			]
		 ]
		,mode=mode, scope=["br","&lt;br/>"]
	);
}
module begwith_test( mode=112 ){ //================

	begwith_tests=
	[ 
  	  [ begwith("abcdef","def"), false, "'abcdef', 'def'" ]
	, [ begwith("abcdef","abc"), true, "'abcdef', 'abc'" ]
	, [ begwith("abcdef", ""), undef, "'abcdef', '" ]
	, [ begwith(["ab","cd","ef"], "ab"), true, "['ab','cd','ef'], 'ab'"]
	, [ begwith([], "ab"), false, "[], 'ab'"]
	, [ begwith("", "ab"), false, "', 'ab'"]
	, "// New 2014.8.11"
  	, [ begwith("abcdef",["abc","ghi"]),"abc", "'abcdef',['abc','ghi']"] 
  	, [ begwith("ghidef",["abc","ghi"]),"ghi", "'ghidef',['abc','ghi']"] 
  	, [ begwith("aaadef",["abc","ghi"]),undef, "'aaadef',['abc','ghi']"] 
 	, [ begwith(["ab","cd","ef"], ["gh","ab"])
         , "ab", "['ab','cd','ef'], ['gh','ab']"]
	];
	
	doctest( begwith, begwith_tests, mode=mode );
}
module begword_test( mode=112 ) { //================   
    doctest( begword
    , [
        [ begword("abc+123"), "abc","'abc+123'" ]
       ,[ begword("123"), "", "'123'"]
       ,[ begword("ab_c+123"), "ab_c", "'ab_c+123'" ]
       ,[ begword("x01+123"), "x01", "'x01+123'"]
       //,[ begword("abc+123"), "'abc+123'", "abc" ]
       ,[ begword("abc123+5"), "abc123", "'abc123+5'" ]
       ,[ begword("_x123+5"), "_x123", "'_x123+5'" ]
       ,[ begword("12.3+abc",isnum=true), "12.3", "'12.3+abc',isnum=true"]
       ,[ begword(".123+abc",isnum=true), ".123", "'.123+abc',isnum=true"]
       ,[ begword("0.5+abc",isnum=true), "0.5","'0.5+abc',isnum=true"]
       ,[ begword("12.3",isnum=true), "12.3","'12.3',isnum=true"]
       ,[ begword("3+a",isnum=true), "3","'3+a',isnum=true"]
       ,[ begword("(3+a)",isnum=true), "","'(3+a)',isnum=true"]
       ,[ begword("(3+a)"), "","'(3+a)'"]
     
//       ,"", "// It's too expensive to solve the problem below, so we leave it:"
//       ,""
//       ,str("> begword(\".12.3+abc\",isnum=true)= ", "\"\""
//            , str("\"",_red( begword(".12.3+abc",isnum=true) ) )
//           )
//       , [ begword(".12.3+abc",isnum=true), ".12.3", "'.12.3+abc',isnum=true"] 
       
    ], mode=mode );
}



module between_test( mode=112 ){ // ====================================
    //echo(" between_test ");
	a=5;
   doctest( between,
	[
	   [ between(3,a,8),true, "3,a,8" ]
	  ,[ between(3,a,5),false, "3,a,5" ]
	  ,[ between(3,a,5,[0,1]),true, "3,a,5,[0,1]", ["//","Include right end"] ]
	  ,[ between(3,a,5,1),true, "3,a,5,1",  ["//","Include both ends"] ]
	  ,""	
	  ,[ between(5,a,8),false, "5,a,8" ]
	  ,[ between(5,a,8,[0,1]),false, "5,a,8,[0,1]",  ["//","Include right end"] ]
	  ,[ between(5,a,8),false,"5,a,8" ]
	  ,[ between(5,a,8,[1,0]),true, "5,a,8,[1,0]", ["//","Include left end"] ]
	  ,[ between(5,a,8,1),true, "5,a,8,1", ["//","Include both ends"] ]
	], mode=mode, scope=["a",a] );    
}


module blockidx_test( mode=112 ){ //==========================
    doctest( blockidx
    ,[
//        ["'(x+1)*2'", blockidx("(x+1)*2"), [[0, "(", 4, ")"]] ]
//      , ["'x*(y+1)'", blockidx("x*(y+1)"), [[2, "(", 6, ")"]] ] 
//      , ["'(y+(z+2))*x'", blockidx("(y+(z+2))*x"), [[0, "(", 8, ")"], [3, "(", 7, ")"]] ]
//      , ["'((x+1)+y)*(z+2)'", blockidx("((x+1)+y)*(z+2)"),[[0, "(", 8, ")"], [1, "(", 5, ")"], [10, "(", 14, ")"]] ]
      
     [blockidx("((x+1)+y)*(z+2)", rootonly=true), "'((x+1)+y)*(z+2), rootonly=true'"
           ,[[0, "(", 8, ")"], [10, "(", 14, ")"]] 
        ]

//      , ["'(x+1)*(y+2)'",blockidx("(x+1)*(y+2)"),[[0, "(", 4, ")"], [6, "(", 10, ")"]] ]
//
//     ,  ["'[x+1]*[y+2]', ['[',']']",blockidx("[x+1]*[y+2]", blk=["[","]"])
//            ,[[0, "[", 4, "]"], [6, "[", 10, "]"]] ]
//      , ["'(x+1)*sin[y+2]', ['(',')','sin[',']']",blockidx("(x+1)*sin[y+2]", blk=["(",")","sin[","]"])
//            ,[[0, "(", 4, ")"], [6, "sin[", 13, "]"]] ]
//      , ["'(x+1)*[y+2]', ['(',')','[',']']",blockidx("(x+1)*[y+2]", blk=["(",")","[","]"])
//            ,[[0, "(", 4, ")"], [6, "[", 10, "]"]] ]
//      , ["'a(x+1)b*c[y+2]d', ['a(',')b','c[',']d']",blockidx("a(x+1)b*c[y+2]d", blk=["a(",")b","c[","]d"])
//            ,[[0, "a(", 5, ")b"], [8, "c[", 13, "]d"]]  ]
      
    ], mode=mode
    );
}  

module boxPts_test( mode=112 ){ //==========================
	scope=["pq",[[1,2,-1],[2,1,1]]];
	pq = hash(scope,"pq");

	doctest
	(
		boxPts
		,[
		  [	boxPts(pq), [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
							, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0],       ], "pq", ["nl",true]
		  ]
		 ,[ boxPts(pq,true)
			, [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
			, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0]] 
            , "pq,true", ["nl",true]
		  ]
		]
		,mode=mode, scope=scope
	);
}



module calc_test( mode=112 ){ //==========================
    isdebug=false;
    
    doctest( calc
    ,[ 
       [calc("11"),11,"'11'",  11 ]
     , [calc("3/2+1"),2.5, "'3/2+1'", 2.5 ]
     , [calc("x+1", ["x",3]),4, "'x+1',['x',3]", 4 ]
     , [calc("(y+2)*x", ["x",3,"y",4]),18, "'(y+2)*x',['x',3,'y',4]", 18 ]
     , [calc("x*(y+2)", ["x",3,"y",1]),9, "'x*(y+2)',['x',3,'y',1]", 9 ]
     , [calc("x*(y+2)^2", ["x",3,"y",1]),81, "'x*(y+2)^2',['x',3,'y',1]", 81 ]
     , [calc("x*((y+2)^2)", ["x",3,"y",1]),27, "'x*((y+2)^2)',['x',3,'y',1]", 27 ]
     , [calc("sin(x)", ["x",30]),0.5,"'sin(x)',['x',30]",  0.5 ]
     , [calc("sin(x)*2", ["x",30], isdebug=isdebug),1,"'sin(x)*2',['x',30]"
         ,  1 ]
     , [calc("sin(x)^2", ["x",30]),0.25, "'sin(x)^2',['x',30]",  0.25 ]
     , [calc("(x+2)+sin(x)", ["x",30]), 32.5, "'(x+2)+sin(x)',['x',30 ]", 32.5 ]
     , [calc("(x+2)+(sin(x)^2)", ["x",30]), 32.25,"'(x+2)+(sin(x)^2)',['x',30 ]", 32.25 ]
    
    
     , [calc("sin(x)^2+(cos(x)^2)", ["x",30]), 1,"'sin(x)^2+(cos(x)^2)',['x',30]", 1 ]
 
       ,""
       ,"// 3x is interpretated as 3 * x"
       ,""
     , [calc("2x", ["x",3]), 6,"'2x',['x',3]", 6 ]
     , [calc("2x+1", ["x",3]), 7,"'2x+1',['x',3]", 7 ]
     , [calc("2x+1.5y", ["x",3,"y",4]),12,"'2x+1.5y',['x',3,'y',4]",  12 ]
     
     , ""
     , "// Net yet ready for 2(x+1) "
     , ""
     , [ calc("2(x+1)", ["x",3]), 8, "'2(x+1)', ['x',3]", ["notest",true] ] 
//     , str(" calc(\"2(x+1)\", [\"x\",3]) wants: 8, but: ", calc("2(x+1)", ["x",3]) )
     
     ], mode=mode );

    module debug(fml)
    {
        uni = getuni(fml, blk=CALC_BLOCKS);
        puni= packuni(uni);
        echo( "<br/>", _fmth(["fml",fml, "uni",uni,"puni",puni]) ); //uni= _fmt(uni), puni= _fmt(puni) );
    }
//      debug("2(x+1)");
//    debug("(y+2)*x");
//    debug("sin(x)*2");   
//    debug("(x+1)*sin(y+2)");
//    debug("(x+1)*sin(y+2)*3");
//    debug("sin(x)^2+cos(x)^2");
//    debug("(x+2)+(sin(x)^2)");
//    debug("(sin(x))^2+((cos(x))^2)");
}    

module calc_basic_test( mode=112 ){ //==========================
    doctest( calc_basic
    ,[ 
       [ calc_basic([5]),5, [5]]
      ,[ calc_basic(["x"], scope=["x",3]), 3, "['x'],['x',3]"]
      ,[ calc_basic(["x","+",1],["x",3]), 4, "['x','+',1], ['x',3]" ]
      ,[ calc_basic(["x","+","3"], scope=["x",3]), 6, "['x','+','3'],['x',3]"]
      ,[ calc_basic(["x","*","2"], scope=["x",3]),6,"['x','*','2'],['x',3]"]
      ,[ calc_basic([3,"+","x","+",3], scope=["x",3]),9,"[3,'+','x','+',3],['x',3]"]
      ,[ calc_basic([2,"+","x","*",2], scope=["x",3]),10,"[2,'+','x','*',2],['x',3]"]

      ,"// Multiple vars"
      ,[ calc_basic([2,"+","x0","*","y"], scope=["x0",3,"y",4]),20,  
          "[2,'+','x0','*','y'],['x0',3,'y',4]"]
       
       ,""
       ,"// power "
       ,""
       ,[ calc_basic(["x","^","3"], scope=["x",3]), 27,"['x','^','3'],['x',3]"]
       
       ,[ calc_basic(["x","^","2","/","4"], scope=["x",3]),2.25, "['x','^','2','/','4'],['x',3]"]
       ,[ calc_basic(["x","^","0.5"], scope=["x",4]),2,"['x','^','0.5'],['x',3]"]
       ,[ calc_basic(["x","^","-2"], scope=["x",2]),0.25,"['x','^','-2'],['x',3]"]

       ,""
       ,"// negative "
       ,""
       ,[ calc_basic(["x","*","-2"], scope=["x",3]), -6,"['x','*','-2'],['x',3]"]
       ,[ calc_basic(["-2","*","x"], scope=["x",3]),-6,"['-2','*','x'],['x',3]"]
       ,[ calc_basic(["x","-","-21"], scope=["x",3]),24,"['x','-','-21'],['x',3]"]        
       ,""
       ,"// functions. Func name must be the first item."
       ,""
       ,[calc_basic(["sin",30]),0.5,["sin",30]]
       ,[ calc_basic(["ceil",2.7]),3,["ceil",2.7]]
                  
        ,""
        ,"// 2015.2.23: trying to make 2x+3 work:"
        ,""
        ,[calc_basic([2, "*", "x", "+", 1], ["x",3]),7
                ,"[2,'*','x','+',1], ['x',3]"
                ]
        ,""
       ,"// It takes only simple array, i.e., the array should NOT have array item."
       ,""
       ,str("> calc('x*2')= ", _red(calc_basic("x*2") ))
       ,""
       ,str("> calc((['2', '+', 'ceil', ['2.7']])= "
            , _red( calc_basic(["2", "+", "ceil", ["2.7"]]) )
            )
//       ,""
//       ,"// debugging "
//       ,""
//       ,[["sin",30], calc_basic(["sin",30]), 0.5]
       
       
       
    
    ], mode=mode );
}    

module centroidPt_test( mode=112 ){ //==========================
    doctest( centroidPt,
    [
    ], mode=mode );
}    

module cirfrags_test( mode=112 ){ //==========================
	doctest( cirfrags, 
	[ 
		"// Without any frag args, the default frag is 30 (no matter r is):"
		,[cirfrags(6),30, "r=6",    30]
		,[cirfrags(3),30, "r=3",   30]
		,[cirfrags(0.1),30, "r=0.1", 30]

		,""
		,"// fn (num of frags) - set NoF directly. Independent of r, too:"
		,""
		
		,[cirfrags(3,fn=10),10,"r=3,fn=10"]
		,[cirfrags(3,fn=3),3 , "r=3,fn=3"]
		,[cirfrags(0.05, fn=12),12, "r=0.05, fn=12"]

		,""
		,"// fs (frag size)"
		,""
        ,[cirfrags(1,fs=1), 2*PI*1/1, "r=1,fs=1"]
        ,[cirfrags(2,fs=1), 2*PI*2/1, "r=2,fs=1"]
        ,[cirfrags(3,fs=1), 2*PI*3/1, "r=3,fs=1"]
        ,[cirfrags(6,fs=1), 2*PI*6/1, "r=6,fs=1"]
//		
//		,[cirfrags(1,fs=1),6.28319-4.69282*1e-6, "r=1,fs=1"]
//		,[cirfrags(2,fs=1),6.28319-4.69282*1e-6, "r=2,fs=1"]
//		,[cirfrags(1,fs=1),6.28319,"r=1,fs=1", ["asstr",true, "prec",6,"//","prec=6"]]
//		,[cirfrags(1,fs=1),6.28319,"r=1,fs=1", ["notest",true, "prec",6,"//","prec=6"]]
//		,[ cirfrags(1,fs=1),6.28319,"r=1,fs=1", ["prec",5,"//","prec=5"]]
//		,[cirfrags(1,fs=1),6.28319,"r=1,fs=1",  ["prec",4,"//","prec=4"]]
//		,[cirfrags(3,fs=1),18.8496,"r=3,fs=1",  ["asstr",true]]
//		,[cirfrags(6,fs=1),20,"r=6,fs=1"]

//		,[cirfrags(6,fs=2),18.8496,"r=6,fs=2", ["asstr",true]]
//		,[cirfrags(12,fs=4),18.8496,"r=12,fs=4", ["asstr",true]]
//		,[cirfrags(3,fs=10),5,"r=3,fs=10"]
//		,[cirfrags(6,fs=100),5,"r=6,fs=100", ["//","min=5 using fa/fs"] ]

		,""
		,"// fa (frag angle)"
		,""
		,[cirfrags(3,fa=10),36,"r=3,fa=10",  36]
		,[cirfrags(6,fa=100),5,"r=6,fa=100",  5,["rem","min=5 using fa/fs"] ]
		,""

	]
	, mode=mode
	);
}



module cornerPt_test( mode=112 ){ //==========================
    doctest( cornerPt, 
	[ 
		[ cornerPt([ [2,0,4],ORIGIN,[1,0,2]  ]),  [3,0,6]
            ,"[ [2,0,4],ORIGIN,[1,0,2]]"
          ]
	], mode=mode
 );
}

module countArr_test( mode=112 ){ //==========================
	doctest
	( 
	  countArr
	, [
	   [ countArr(["x",2,[1,2],5.1]),1, ["x",2,[1,2],5.1]]
	  ]
	, mode=mode
	);

} 
module countInt_test( mode=112 ){ //======================== 
	doctest
	( 
	  countInt
	, [
		[ countInt(["x",2,"3",5.3,4]),2, "['x',2,'3',5.3,4]"]
	  ]
	, mode=mode
	);

} 
module countNum_test( mode=112 ){ //========================
	doctest
	( 
	  countNum
	, [
		[ countNum(["x",2,"3",5.3,4]),3, "['x',2,'3',5.3,4]" ]
	  ]
	, mode=mode
	);
} 
module countStr_test( mode=112 ){ //========================
	doctest
	( 
	  countStr
	, [
		[ countStr(["x",2,"3",5]),2, "['x',2,'3',5]" ]
	  ,	[ countStr(["x",2,5]),1, "['x',2, 5]" ]
	  ]
	, mode=mode
	);

} // countStr_test();
module countType_test( mode=112 ){ //====================

	countType_tests=  
	[
		[ countType(["x",2,[1,2],5.1],"str"),1
            , "['x',2,[1,2],5.1], 'str'" ]
	  ,	[ countType(["x",2,[1,2],5.1,3], "int"),2
            , "['x',2,[1,2],5.1,3], 'int'" ]
	  ,	[ countType(["x",2,[1,2],5.1,3] ,"float"),1
            , "['x',2,[1,2],5.1,3], 'float'" ]
	];

	doctest(countType, countType_tests,mode=mode );

} 
module cubePts_test( mode=112 ){ //=======================
    doctest(cubePts,mode=mode );}

module cubePts_demo_pqr_is_number()
{
   echo(_b("cubePts_demo_pqr_is_number()"));
  
   color("green", 0.5)
   cube(3,center=true);
   ptsc = cubePts(3, center=true);
   echo(ptsc=ptsc);
   MarkPts(ptsc,"l");

   color("gold", 0.5) cube(3);
   pts = cubePts(3);
   echo(pts=pts);
   MarkPts(pts,"l");
}
//cubePts_demo_pqr_is_number();

module cubePts_demo_pqr_is_number_more_center()
{
   echo(_b("cubePts_demo_pqr_is_number_more_center()"));
   echo( "Showing: center=[1,0,0] and [0,1,1]" );
   
   color("green", 0.5)
   translate([-1.5,0,0]) cube(3);
   ptsc = cubePts(3, center=[1,0,0]);
   echo(ptsc=ptsc);
   MarkPts(ptsc,"l");

   color("gold", 0.5)
   translate([0,-1.5,-1.5]) cube(3);
   ptsc2 = cubePts(3, center=[0,1,1]);
   echo(ptsc2=ptsc2);
   MarkPts(ptsc2,"l");
}
//cubePts_demo_pqr_is_number_more_center();

module cubePts_demo_pqr_is_pt()
{
   echo(_b("cubePts_demo_pqr_is_pt()"));
   pqr = [5,3,1];
   
   
   translate( pqr/-2 )
   color("green", 0.5) cube(pqr);
   pts1= cubePts( pqr, center=1);
   MarkPts( pts1, "l");
   
   color("gold", 0.5) cube(pqr);
   pts2= cubePts( pqr);
   MarkPts( pts2, "l");
}
//cubePts_demo_pqr_is_pt();


module cubePts_demo_pqr_is_pt_more_center()
{
   echo(_b("cubePts_demo_pqr_is_pt_more_center()"));
   pqr = [5,3,1];   
   
   color("green", 0.5) translate([-2.5,0,0])cube(pqr);
   pts1= cubePts( pqr, center=[1,0,0]);
   MarkPts( pts1, "l");
   
   color("gold", 0.5) translate([0, -1.5,-0.5])cube(pqr);
   pts2= cubePts( pqr, [0,1,1]);
   MarkPts( pts2, "l");
}
//cubePts_demo_pqr_is_pt_more_center();

module cubePts_demo_pqr_is_pqr()
{
   echo(_b("cubePts_demo_pqr_is_pqr()"));
   pqr= pqr();
   MarkPts( pqr);
   
   pts = cubePts(pqr);
   MarkPts( pts, ["l","ball", false]);
   color(undef, 0.5) polyhedron( pts, rodfaces(4));


}
//cubePts_demo_pqr_is_pqr();

module del_test( mode=112 ){ //=======================
	doctest
	(
		del
		,[ 
			 [ del( [2,4,4,8], 4), [2,8], "[2,4,4,8], 4" ]
			,[ del( [2,4,6,8], 6), [2,4,8], "[2,4,6,8], 6" ]
			,[ del( [2,4,6,8], 2), [4,6,8], "[2,4,6,8], 2" ]
			,[ del( [ 3,undef, 4, undef], undef), [3,4] , "[3,undef, 4, undef], undef" ]
			,"// byindex = true"
			,[ del( [2,4,6,8], 0, true), [4,6,8], "[2,4,6,8], 0, true" ]
			,[ del( [2,4,6,8], 3, true), [2,4,6], "[2,4,6,8], 3, true" ]
			,[ del( [2,4,6,8], 2, true), [2,4,8], "[2,4,6,8], 2, true" ]
			,"//"
			,"// New 2014.8.7: arg *count* when byindex=true"
			,"//"
			,[ del( [2,4,6,8], 1, true), [2,6,8], "[2,4,6,8], 1, true" ]
			,[ del( [2,4,6,8], 1, true,2), [2,8], "[2,4,6,8], 1, true,2"
                       ,["//","del 2 tiems"]]
			,[ del( [2,4,6,8], 2, true,2), [2,4], "[2,4,6,8], 2, true,2"
                , ["//","del only 1 'cos it's in the end."] ]
			,[ del( [], 1, true), [], "[], 1, true" ]
		 ]
		, mode=mode
	);
}
module delkey_test( mode=112 ){ //==========================
	scope=["h1", ["a",1,"b",2]
		  ,"h2", [1,10,2,20,3,30] ];
	h1 = hash( scope, "h1" );
	h2 = hash( scope, "h2");

	doctest
	(
	 delkey
	,[
	   [delkey([], "b"),[], "[], 'b'" ]
	 , [delkey(h1, "b"), ["a",1], "h1, 'b'" ]
	 , [delkey(h1, "c"), ["a",1,"b",2], "h1, 'c'" ]
	 , [delkey(h2, 1), [2,20,3,30], "h2, 1"]
	 , [delkey(h2, 2), [1,10,3,30], "h2, 2"]
	 , [delkey(h2, 3), [1,10,2,20], "h2, 3"]
	 ], mode=112,  scope=scope
	);
}
module dels_test( mode=112 ){ //======================
	doctest
	(
		dels
		,[ 
			 [ dels( [2,4,4,8], 4), [2,8], "[2,4,4,8], 4" ]
			,[ dels( [2,4,6,8], [4,6]), [2,8], "[2,4,6,8], [4,6]" ]
		 ]
		,mode=mode
	);
}


module det_test(mode=112){  //=======================
	doctest( det, 
	[
	]
	,mode=mode );
}

module deeprep_test( mode=112 ){  //=======================
   //_mdoc("deeprep_test");
    
   doctest( deeprep
   , [
        [deeprep([0,1,2,3],indices=[2], new=99 )
                , [0,1,99,3]
                ,"[0,1,2,3],[2],99"
        ]
     ,  [deeprep([0,1,2,3],indices=[-1], new=99 )
                , [0,1,2, 99]
                ,"[0,1,2,3],[-1],99"
        ]
     ,  [deeprep([0,1,[20,21,22],3],indices=[-2,1], new=99 )
                , [0,1,[20,99,22],3]
                ,"[0,1,[20,21,22],3],[-2,1],99"
        ]
     ,  [deeprep([0,1,[20,[40,41,42],22],3]
                                ,indices=[2,1,-1], new=99 
                         )
                , [0,1,[20,[40,41,99],22],3 ]
                ,"[0,1,[20,[40,41,42],22],3], [2,1,-1],99"
        ]
     ,  [deeprep([0,1,[20,[40,41,42],22],3]
                                ,indices=[2,-1], new=99 
                         )
                , [0,1,[20,[40,41,42],99],3]
                ,"[0,1,[20,[40,41,42],22],3], [2,-1],99"
        ]
     ,[deeprep([0,1,2,3]
                                ,indices=[], new=99 
                         )
                , [0,1,2,3]
                ,"[0,1,2,3],[],99"
        ]        
     ]
   , mode=mode );
}    


module dist_test( mode=112 ){ //========================
	pqrs = randPts(4);
    P = pqrs[0];
    Q = pqrs[1];
    R = pqrs[2];
    S = pqrs[3];
    L1= [ [2,0,0], [2,2,0] ];
    L2= [ [0,0,0], [0,2,0] ];
    L3= [ [0,0,0], [0,2,2] ];
    dPQ = sqrt( pow(P.x-Q.x,2)+pow(P.y-Q.y,2)+pow(P.z-Q.z,2) );
    
   // dP_QR= abs( 
	doctest( dist, 
        [
            [ dist(P,Q),  dPQ, "P,Q" ]
           ,[ dist([P,Q]),dPQ, "[P,Q]" ]
           ,[ dist([L1,L2]), 2, "[L1,L2]" ]
           ,[ dist([L1,L3]), 2, "[L1,L3]" ]
           ,[ dist([P,L1]), 2, "[P,L1]" ]
           ,[ dist([L1,P]), 2, "[L1,P]" ]
        ]	
        ,mode=mode
        ,scope=[ "P",P, "Q",Q,"R",R,"S",S
               , "L1",L1,"L2",L2, "L3",L3
               ]
	);
}

module distPtPl_test( mode=112 ){ //=====================
	pt= [1,2,0];
	pqr = [ [-1,0,2], [1,0,-3], [2,0,4]];
	//echo( "planecoefs({_}, false) =", planecoefs(pqr));
    //echo( pt * planecoefs(pqr, has_k=false) );
	
	doctest
	(
		distPtPl
		,[
			 [distPtPl(pt,pqr), 2, "pt,pqr" ]
		], mode=mode, scope=[ "pt", pt
		  , "pqr", pqr ]	
	);
}


module distx_test( mode=112 ){ //======================
	pq = [[0,3,1],[4,5,-1]];
	pq2= reverse(pq);
	
	scope=["pq", pq, "pq2",pq2];

	doctest( distx
			,[ 
				[ distx(pq), 4,"pq" ]			   		
				,[ distx(pq2), -4,"pq2" ]			   		
			 ],mode=mode, scope=scope
		   );
}

module disty_test( mode=112 ){  //=========================
	pq = [[0,3,1],[4,5,-1]];
	pq2= reverse(pq);
	
	scope=["pq", pq, "pq2",pq2];

	doctest( disty
			,[ 
				[ disty(pq), 2,"pq" ]			   		
				,[ disty(pq2), -2,"pq2" ]			   		
		     ],mode=mode, scope=scope
		   );
}

module distz_test( mode=112 ){ //=========================
	pq = [[0,3,1],[4,5,-1]];
	pq2= reverse(pq);
	
	scope=["pq", pq, "pq2",pq2];

	doctest( distz
			,[ 
				[ distz(pq), -2,"pq" ]			   		
			   ,[ distz(pq2), 2,"pq2" ]			   		
		   ],mode=mode, scope=scope
		   );
}


module dot_test(mode=112){ //=========================
    P = [2,3,4];
    Q = [-1,0,2];
    
    doctest( dot,
    [
        str( "P*Q = ", P*Q )
      , ""
      , [ dot(P,Q), 6,"P,Q"]
      , [ dot([P,Q]), 6,"[P,Q]" ]
    ], mode=mode, scope=["P",P,"Q",Q] );
}
module echoblock_test( mode=112 ){ //================
    doctest( echoblock, mode=mode ); }

module echo__test( mode=112 ){ //===================
    doctest( echo_, mode=mode ); }

module echoh_test( mode=112 ){ //===========================
    doctest( echoh, mode=mode );}
module endwith_test( mode=112 ){ //========================
	doctest
	(
	 endwith
	,[ 
	    [ endwith("abcdef", "def"), true, "'abcdef', 'def'" ]
	  , [ endwith("abcdef", "abc"), false, "'abcdef', 'def'" ]
	  , [ endwith(["ab","cd","ef"], "ef"),true, "['ab','cd','ef'], 'ef'" ]
	  , [ endwith(["ab","cd","ef"], "ab"),false, "['ab','cd','ef'], 'ab'" ]
	  , [ endwith(33, "abc"), false, "33, 'def'" ]
	  , "// New 2014.8.11"
  	, [ endwith("abcdef", ["def","ghi"]), "def", "'abcdef', ['def','ghi']" ] 
  	, [ endwith("abcghi", ["def","ghi"]),  "ghi", "'abcghi', ['def','ghi']" ] 
  	, [ endwith("aaaddd", ["def","ghi"]), undef, "'abcddd', ['def','ghi']" ] 
 	, [ endwith(["ab","cd","ef"], ["gh","ef"]),"ef", "['ab','cd','ef'], ['gh','ef']"]	
	  ], mode=mode
	);
}

module expandPts_test( mode=112 ){ //========================
    doctest( expandPts, mode=mode ); }

module fac_test( mode=112 ){ //====================
	doctest
	( fac
	,  [ 
		[ fac(5), 120, 5 ]
	  , [ fac(6), 720, 6 ]
	  ], mode=mode
    );
}


module faces_test( mode=112 ){ //==================
    doctest( faces,
    []
	,mode=mode
	);
}


module fidx_test( mode=112 ){ //==================

	doctest
	( 
	 fidx, [
		"// string "
	  , [ fidx("789",2),2, "'789',2" ]
	  ,	[ fidx("789",3), undef, "'789',3",  ["//","out of range"] ]
	  ,	[ fidx("789",-2),1,"'789',-2" ]
	  ,	[ fidx("789",-3),0, "'789',-3" ]
	  ,	[ fidx("789",-4),undef, "'789',-4", ["//","out of range"] ]
	  ,	[ fidx("''",0),undef, "',0" ]
	  ,	[ fidx("789","a"), undef, "'789','a'" ]
	  ,	[ fidx("789",true), undef, "'789',true" ]
	  ,	[ fidx("789",[3]), undef, "'789',[3]" ]
	  ,	[ fidx("789",undef), undef, "'789',undef" ]
	  , "// array"
	  ,	[ fidx([7,8,9],2), 2, "[7,8,9],2", ]
	  ,	[ fidx([7,8,9],3), undef, "[7,8,9],3", ["//","out of range"] ]
	  ,	[ fidx([7,8,9],-2), 1, "[7,8,9],-2", 1 ]
	  ,	[ fidx([7,8,9],-3), 0, "[7,8,9],-3", 0 ]
	  ,	[ fidx([7,8,9],-4), undef, "[7,8,9],-4", ["//","out of range"] ]
	  ,	[ fidx([],0), undef,  "[],0" ]
	  ,	[ fidx([7,8,9],"a"), undef, "[7,8,9],'a'" ]
	  ,	[ fidx([7,8,9],true), undef,"[7,8,9],true" ]
	  ,	[ fidx([7,8,9],[3]), undef,"[7,8,9],[3]" ]
	  ,	[ fidx([7,8,9],undef), undef,"[7,8,9],undef" ]

	  , "// If out of range, return undef, or:"
	  , "//  -- Set cycle to go around the other end."
	  , "//     (it never goes out of bound) "
	  , "//  -- Set fitlen to return the last index."
	  , "//"
	  ,	[ fidx([7,8,9],3), undef,"[7,8,9],3", ["//","out of range"] ]
	  ,	[ fidx([7,8,9],3,fitlen=true), 2, "[7,8,9],3,fitlen=true"
                               , ["//","fitlen"] ]
	  , [ fidx([7,8,9],3,true), 0, "[7,8,9],3, cycle=true" ]
	  ,	[ fidx([7,8,9],4,true), 1, "[7,8,9],4, cycle=true" ]
	  ,	[ fidx([7,8,9],7,true), 1, "[7,8,9],7, cycle=true" ]
	  ,	[ fidx([7,8,9],-4), undef,"[7,8,9],-4",  ["//","out of range"] ]
	  ,	[ fidx([7,8,9],-4,true), 2, "[7,8,9],-4, cycle=true"  ]
	  ,	[ fidx([7,8,9],-16,true), 2, "[7,8,9],-16, cycle=true" ]
	  ,	[ fidx([7,8,9],-5,true), 1, "[7,8,9],-5, cycle=true" ]
	  
	  ,"//"
	  ,"// New 2014.8.17: o can be an integer for range 0~o"
	  ,"//"
	  ,	[ fidx(3,5), undef,"3,5", ["//","out of range"] ]
	  ,	[ fidx(3,5,fitlen=true), 2, "3,5,fitlen=true"
            , ["//","get the last idx"] ]
	  ,	[ fidx(3,4,cycle=true), 1, "3,4,cycle=true"
            , ["//","cycle from begin"] ]
	  , [ fidx(3,-2), 1, "3,-2" ]
	  ,	[ fidx(3,-3), 0, "3,-3" ]
	  ,	[ fidx(3,-4), undef,"3,-4", ["//","out of range"] ]
	  ,	[ fidx(3,-4,cycle=true), 2, "3,-4,cycle=true",  ["//","cycle=true"] ]
	  , "//"	

	  ], mode=mode
	);
}




module flatten_test( mode=112 ) { //===============
    
    a=[ [[1,2],[2,3]], [[[1,2],[2,3]]] ];
    b=[ ["c",[2,3]], [["cc",[2,3]]] ];
    c=[ ["abc", ["def","g"]],"h"]; 
    
    doctest( flatten,
    [
       [ flatten(a), [1, 2, 2, 3, 1, 2, 2, 3], "a" ]
      ,[ flatten(a,0), [ [[1,2],[2,3]], [[[1,2],[2,3]]] ] , "a,d=0"]
      ,[ flatten(a,1), [[1, 2], [2, 3], [[1, 2], [2, 3]]] , "a,d=1"]
      ,[ flatten(a,2), [1, 2, 2, 3, [1, 2], [2, 3]] , "a,d=2"]
      ,[ flatten(b,1), ["c", [2, 3], [ "cc", [2, 3]]] , "b,d=1"]
      ,[ flatten(b,2), ["c", 2, 3, "cc", [2, 3]] , "b,d=2"]
      ,[ flatten(c), [ "abc", "def","g","h"] , "c"]
      ,"// Note that flatten( [a,b,c],d=1 ) = concat( a,b,c )"
      ,"// when all a,b,c are arrays."
    ]
    ,mode=mode, scope=["a",a,"b",b,"c",c]
    );
      
  }

  
module _fmth_test( mode=112 ){ //====================
	scope=[ "h", ["num",23,"str","test", "arr", [2,"a"] ]
		  , "h2",["pqr", [2,3,4]]
		  , "h3",["pqr", [[0,1,2],undef] ]
		  ];
	h= hash(scope, "h");
	h2= hash(scope, "h2");
	h3= hash(scope, "h3");

	r1 = _fmth(h);
	//r2 = _fmth(h2);
	//r3 = _fmth(h3);

	doctest(
	 _fmth
	,[
		//["h", _fmth(h), ""]
	  //, 
	//	_fmth(h)
	 //"No test is done."
	//, "2nd line string"
	""
    ,str("_fmth(h): ", _fmth(h))
	,str("_fmth(h2): ", _fmth(h2))
	,str("_fmth(h3): ", _fmth(h3))
    ,str("_fmth(h3,iskeyfmt=true): ", _fmth(h3, iskeyfmt=true))
    ,str("_fmth(h,pairbreak='&lt;br/>'): ", str("<br/>",_fmth(h,pairbreak="<br/>")))
//	,[ "", _fmth(h,pairbreak="<br/>")
//     ,"h,pairbreak='&lt;br/>'", ["notest", true, "nl",true]
//          ]
    
//	  ["['a',23]", _fmth(["a",23]), "[\"a\",23]" ]
//	, ["h", _fmth(h ),""]
//	, ["h2", _fmth( h2),""]
//	, ["h3", _fmth( h3),""]

	],  mode=mode, ,scope=scope 
	);
}

module _fmt_test( mode=112 ){ //====================

	doctest(
	 _fmt
	,[
	   str("_fmt(3.45): ", _fmt(3.45))
     , str("_fmt(3.45, n='{,}'): ", _fmt(3.45, n="{,}"))
     ,  str("_fmt(['a',[3,'b'], 4]): ", _fmt(["a",[3,"b"], 4] ))
     ,  str("_fmt(['a',[3,'b'], 4], n='{,}'): ", _fmt(["a",[3,"b"], 4],n="{,}" ))
     ,  str("_fmt(['a',[3,'b'], 4], a='&lt;u>,&lt;/u>'): ", _fmt(["a",[3,"b"], 4],a="<u>,</u>" ))
     ,  str("_fmt('a string with     5 spaces'): ", _fmt("a string with     5 spaces"))			, str("_fmt(false): ", _fmt(false))

	],  mode=mode
	);
}


module ftin2in_test( mode=112 ){ //====================
	doctest( ftin2in,
	[
	  [ ftin2in(3.5), 42, "3.5",  ]
	 ,[ ftin2in([3,6]), 42, "[3,6]",  ]
	, [ ftin2in("3'6\""), 42, "3`6'" ]
	, [ ftin2in("8'"), 96, "8'" ]
	, [ ftin2in("6\""), 6, "6'" ]
	], mode=mode
	);
}

module get_test( mode=112 ){ //=================
	arr = [20,30,40,50];

	doctest
	( get
		,[ 
		  "## array ##"
		 , [ get( arr, 1 ), 30, "arr, 1" ]
         , [ get(arr, -1), 50, "arr, -1", ]
    	 , [ get(arr, -2), 40, "arr, -2" ]
    	 , [ get(arr, true), undef, "arr, true" ]
    	 , [ get([], 1), undef, "[], 1" ]
		 , [ get([[2,3],[4,5],[6,7]],-2), [4,5], "[[2,3],[4,5],[6,7]], -2" ]
    	 , "## string ##"
    	 , [ get("abcdef", -2), "e", "'abcdef', -2" ]
    	 , [ get("abcdef", false), undef, "'aabcdef', false" ]
    	 , [ get("", 1), undef, "'', 1" ]
		 , "//New 2014.6.27: cycle"
		 , [ get(arr,4), undef, "arr, 4" ]
		 , [ get(arr,4,cycle=true), 20, "arr, 4, cycle=true" ]
		 , [ get(arr,10), undef, "arr, 10" ]
		 , [ get(arr,10,cycle=true), 40, "arr, 10, cycle=true" ]
		 , [ get(arr,-5), undef, "arr, -5" ]
		 , [ get(arr,-5,cycle=true), 50, "arr, -5, cycle=true" ]

    	],mode=mode,  scope=["arr", arr]
	);
}
module getcol_test( mode=112 ){ //=================
	mm= [[1,2,3],[3,4,5]];
	doctest
	(
		getcol
	,	[
			[ getcol(mm), [1,3],"mm" ]
		,	[ getcol(mm,2), [3,5], "mm,2" ]
		,	[ getcol(mm,-2), [2,4], "mm,-2" ]
		], mode=mode, scope=["mm", [[1,2,3],[3,4,5]] ]
	);
}
module getSideFaces_test( mode=112 ){ //================= 
    d1=[ [0,1,2,3],[4,5,6,7]];
	d2=[ [6,7,8],[9,10,11]];
	doctest(getSideFaces,
	[ [getSideFaces(d1[0],d1[1]),
        [[0,3,7,4], [1,0,4,5], [2,1,5,6], [3,2,6,7]]
        ,"d1[0], d1[1]", ["nl",true]
      ]
     ,[getSideFaces(d1[0],d1[1],false),
        [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
        , "d1[0], d1[1], clockwise=false", ["nl",true]
      ]
     ,[getSideFaces(d2[0],d2[1]),
        [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
        , "d2[0], d2[1]", ["nl",true]
      ]
     ,[ getSideFaces(d2[0],d2[1],false),
        [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
       , "d2[0], d2[1], clockwise=false", ["nl",true]
      ]
	]
	,mode=mode, scope=["d1", d1, "d2",d2]);
  
}


module getsubops_test( mode=112 ){ //===================
    
    opt=[];
    df_arms=["len",3,"r",1];
    df_armL=["fn",5];
    df_armR=[];
    df_ops= [ "len",2
            , "arms", true
            , "armL", true
            , "armR", true
            ];
    com_keys= ["fn"];
    
    /*
        setops: To simulate obj( ops=[...] )
        args  : ops  
                // In real use, following args are set inside obj()
                df_ops
                df_subops // like ["arms",[...], "armL",[...]] 
                com_keys 
                objname   // like "obj","armL","armR"          
                propname  // like "len"
    */
    function setops(   
            ops=opt
            , df_ops=df_ops
            , df_subops=[ "arms",df_arms, "armL",df_armL,"armR",df_armR]
            , com_keys = com_keys
            , objname ="obj"
            , propname=undef
            )=
        let(
             _ops= update( df_ops, ops )
           , df_arms= hash(df_subops, "arms")  
           , df_armL= hash(df_subops, "armL")  
           , df_armR= hash(df_subops, "armR")  
           , ops_armL= getsubops( _ops, df_ops = df_ops
                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]                    
                    , com_keys= com_keys
                    )
            , ops_armR= getsubops( _ops, df_ops = df_ops
                        , sub_dfs= ["arms", df_arms, "armR", df_armR ]
                        , com_keys= com_keys
                        )
            , ops= objname=="obj"?_ops
                    :objname=="df_ops"?df_ops
                    :objname=="armL"?ops_armL
                    :ops_armR
            )
        // ["ops",_ops,"df_arms", df_arms, "ops_armL",ops_armL, "ops_armR",ops_armR];
        propname==undef? ops:hash(ops, propname);
      
     /**
        Debugging Arrow2(), in which a getsubops call results in the warning:
        DEPRECATED: Using ranges of the form [begin:end] with begin value
        greater than the end value is deprecated.
            
     */       
     function debug_Arrow2(ops=["r", 0.1, "heads", false, "color","red"])=
        let (
        
           df_ops=[ "r",0.02
               , "fn", $fn
               , "heads",true
               , "headP",true
               , "headQ",true
               , "debug",false
               , "twist", 0
               ]
          ,_ops= update(df_ops,ops) 
          ,df_heads= ["r", 1, "len", 2, "fn",$fn]    
          , ops_headP= getsubops( _ops, df_ops=df_ops
                , com_keys= [ "color","transp","fn"]
                , sub_dfs= ["heads", df_heads, "headP", [] ]
                ) 
        )
        ops_headP;
        
     doctest("getsubops"
        , [ ""
            ,"In above settings, ops is the user input, the rest are"
            ,"set at design time. With these:"
            ,""
            ,[setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]
                 ,"df_ops"
                 ]
            ,[setops()
                 , ["len",2,"arms",true,"armL", true, "armR", true]
                 , "ops"
                 ]
            ,[ setops(objname="armL")
                 , ["len", 3, "r", 1, "fn", 5]
                 , "ops_armL"
                 ]
            ,[ setops(objname="armR")
                 , [ "len", 3, "r", 1]
                 , "ops_armR"
                 ]
                            
            ,""
            ,"Set r=4 at the obj level. Since r doesn't show up in com_keys, "
            ,"it only applies to obj, but not either arm. "
            ,""
            
            ,[ setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]
                 , "df_ops"
                 ]
            ,[ setops(ops=["r",4])
                 , ["len",2,"arms",true,"armL", true, "armR", true,"r",4]
                 , "ops"
                 ]
            ,[ setops(ops=["r",4],objname="armL")
                 , ["len", 3, "r", 1,"fn", 5]
                 , "ops_armL"
                 ]
            ,[ setops(ops=["r",4],objname="armR")
                 , [ "len", 3, "r", 1]
                 , "ops_armR"
                 ]
                            
            ,""
            ,"Set fn=10 at the obj level. Since fn IS in com_keys, "
            ,"it applies to obj and both arms. "
            ,""
            
            ,[ setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]
                 , "df_ops"
                 ]
            ,[ setops(ops=["fn",10])
                 , ["len",2,"arms",true,"armL", true, "armR", true, "fn",10]
                 , "ops"
                 ]
            ,[ setops(ops=["fn",10],objname="armL")
                 , ["len", 3, "r", 1, "fn", 10]
                 , "ops_armL"
                 ]
            ,[ setops(ops=["fn",10],objname="armR")
                 , ["len", 3, "r", 1, "fn", 10]
                 , "ops_armR"
                 ]
        
            ,""
            ,"Disable armL:"
            ,""
            
            ,[ setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]
                 , "df_ops"
                 ]
            ,[ setops(ops=["armL",false])
                 , ["len",2,"arms",true,"armL", false, "armR", true]
                 , "ops"
                 ]
            ,[ setops(ops=["armL",false],objname="armL")
                 , false
                 , "ops_armL"
                 ]
            ,[ setops(ops=["armL",false],objname="armR")
                 , ["len", 3, "r", 1]
                 , "ops_armR"
                 ]


            ,""
            ,"Disable both arms:"
            ,""
            
            ,[ setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]
                 , "df_ops"
                 ]
            ,[ setops(ops=["arms",false])
                 , ["len",2,"arms",false,"armL", true, "armR", true]
                 , "ops"
                 ]
            ,[ setops(ops=["arms",false],objname="armL")
                 , false
                 , "ops_armL"
                 ]
            ,[ setops(ops=["arms",false],objname="armR")
                 , false
                 , "ops_armR"
                 ]
        
        
//            ,""
//            ,"Debugging Arrow2(), in which a getsubops call results in the warning:"
//            ,"DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated."
//            ,""
//            
//            ,["debug_Arrow2(ops=['r', 0.1, 'heads', false, 'color','red']"
//             , debug_Arrow2(ops=["r", 0.1, "heads", false, "color","red"]),[]
//             ]
                            
          ]
          
        , mode=mode, scope= [ "df_ops",df_ops, 
               , "df_arms", df_arms
               , "df_armL", df_armL
               , "df_armR", df_armR
               , "com_keys", com_keys
               , "ops",opt
               ]
    );
}


module getTubeSideFaces_test( mode=112 ){ //=================== 
    d1=[ [6,7,8,9],[10,11,12,13],[14,15,16,17] ];
    
	doctest(getTubeSideFaces,
	[ [ getTubeSideFaces(d1),
        [[[ 6, 9,13,10], [ 7, 6,10,11]
         ,[ 8, 7,11,12], [ 9, 8,12,13]]
        ,[[10,13,17,14], [11,10,14,15]
         ,[12,11,15,16], [13,12,16,17]]
        ], "d1", ["ml",2]
      ]
//     ,["d1[0], d1[1], clockwise=false"
//        , getSideFaces(d1[0],d1[1],false),
//        [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
//      ]
//     ,["d2[0], d2[1]", getSideFaces(d2[0],d2[1]),
//        [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
//      ]
//     ,["d2[0], d2[1], clockwise=false"
//        , getSideFaces(d2[0],d2[1],false),
//        [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
//      ]
	]
	,mode=mode, scope=["d1", d1]);
  
}





module getuni_test( mode=112 ){ //====================
    doctest( getuni
    , [

    [getuni("(x+1)*(y+2)")
                , [["#/1", "*", "#/2"], ["x", "+", 1], ["y", "+", 2]]
                , "'(x+1)*(y+2)'"
                , ["nl",true]
    ]
    
    ,[getuni("((y+2)*3)*(x+1)")
                , [ ["#/1", "*", "#/3"], ["#/2","*",3]
                  , ["y", "+", 2], , ["x", "+", 1] 
                  ] 
                ,"'((y+2)*3)*(x+1)'"
                , ["nl",true]
                ]
     ,[getuni("(x+1)*((y+2)*3)")
                , [ ["#/1", "*", "#/2"], ["x", "+", 1]
                  , ["#/3","*",3],["y", "+", 2]
                  ] 
                  ,"'(x+1)*((y+2)*3)'"
                  , ["nl",true]
                    ]
    
    , [getuni("(ab+1)*20-1.5")
                ,[["#/1", "*", 20, "-", 1.5], ["ab", "+", 1]]
                ,"'(ab+1)*20-1.5'"
                  , ["nl",true]
    ]
//    
    , [getuni("(left_arm+1)^2-1.5")
                ,[["#/1", "^", 2, "-", 1.5], ["left_arm", "+", 1]]
                ,"'(left_arm+1)^2-1.5'"
                  , ["nl",true]
    ]
    
     ,"","// Spaces are removed:",""
     ,[getuni("(b,(c,d)),e")
                , [["#/1", "e"], ["b", "#/2"], ["c", "d"]] 
                ,"'(b,(c,d)),e'"
                ]   
     ,[getuni("(b ,( c,d )), e")
                , [["#/1", "e"], ["b", "#/2"], ["c", "d"]] 
                ,"'(b ,( c,d )), e'", 
                ] 
  
      ,"","// Multiple block symboles:", ""  
    
     ,[getuni("{b,[c,d]},e", blk=["{","}","[","]"])
                , [["#/1", "e"], ["b", "#/2"], ["c", "d"]] 
                ,"'{b,[c,d]},e',['{','}','[',']']"
                    , ["nl",true]
    ]     
  
      ,"","// With function names: ", ""  
  
     ,[getuni("(x+1)*sin(y+2)")
        ,  [["#/1", "*", "sin", "#/2"], ["x", "+", 1], ["y", "+", 2]]
        ,"'(x+1)*sin(y+2)'" 
                  , ["nl",true]
    ]
    
     ,[getuni("(x+1)*sin(y+2)", blk=["(",")","sin(",")"] )
        , [["#/1", "*", "#/2"], ["x", "+", 1], ["sin", "y", "+", 2]]
        , "'(x+1)*sin(y+2)', blk=['(',')','sin(',')']"
                  , ["nl",true]
    ]
      
     ,[getuni("(x+1)*sin(y+2)", blk=["(",")","sin(",")"], , keep=false )
        , [["#/1", "*", "#/2"], ["x", "+", 1], ["y", "+", 2]]
        , "'(x+1)*sin(y+2)', blk=['(',')','sin(',')'], keep=false"
                  , ["nl",true]
    ]  
      
    ,[getuni( "5" )
        , [[5]], "'5'"
      ]  

    ,[getuni( "x" )
        , [["x"]], "'x'"
      ]  

    ,[getuni("(x+2)+(sin(x)^2)", blk=CALC_BLOCKS)
        ,  [["#/1", "+", "#/2"], ["x", "+", 2], ["#/3", "^", 2], ["sin", "x"]]
        , "'(x+2)+(sin(x)^2)', blk=CALC_BLOCKS"
        , ["nl",true]
      ]
        
    ,""
    ,"//=========================================="
    ,""
    
      , [packuni(getuni("a,(b,(c,d))"))
                , ["a", ["b", ["c", "d"]]]
                , "'a,(b,(c,d))'"
                , ["myfunc", "packuni( getuni{_} )" ]
       ]         
      , [packuni(getuni("a,x, (b,(c,d))"))
                , ["a", "x", ["b", ["c", "d"]]]
                , "'a,x, (b,(c,d))'"
                , ["myfunc", "packuni( getuni{_} )" ]
       ]         
     , [packuni( getuni("(a,x,(b,(c,d)))") )
                , [["a","x",["b",["c","d"]]]]
                , "'(a,x,(b, (c,d)))'"
                , ["myfunc", "packuni( getuni{_})" ]
       ] 
    , 
    , [packuni( getuni("(x+1)*(y+2)") )
                , [ ["x","+",1], "*", ["y","+",2] ] 
                , "'(x+1)*(y+2)'"
                , ["myfunc", "packuni(getuni{_})" ]
       ]
    
    , [packuni( getuni("(ab+1)*20-1.5") )
                , [ ["ab","+",1], "*", 20, "-", 1.5 ]
                , "'(ab+1)*20-1.5'"
                , ["myfunc", "packuni(getuni{_})" ]
      ]
    
    , [packuni( getuni("(left_arm+1)^2-1.5") )
                , [ ["left_arm","+",1], "^", 2, "-", 1.5 ]
                , "'(left_arm+1)^2-1.5'"
                , ["myfunc", "packuni(getuni{_})", "nl",true ]
      ]
      
    , [packuni( getuni("5") )
                , [ 5 ]
                , "'5'"
                , ["myfunc", "packuni(getuni{_})" ]
      ]

    , [packuni( getuni("x") )
                , [ "x" ]
                , "'x'"
                , ["myfunc", "packuni(getuni{_})" ]
      ]

      ,"","// With function names: ", ""  
  
     ,[packuni( getuni("(x+1)*sin(y+2)",blk=["(",")","sin(",")"]) )
                , [ ["x", "+", 1], "*", ["sin", "y", "+", 2] ] 
                ,"'(x+1)*sin(y+2)',blk=['(',')','sin(',')']"
                , ["myfunc", "packuni(getuni{_})", "nl",true ]
                
      ]

      ,[packuni( getuni("(x+1)*sin(y+2)*3",blk=["(",")","sin(",")"]) )
                , [ ["x", "+", 1], "*", ["sin", "y", "+", 2], "*",3 ] 
                , "'(x+1)*sin(y+2)*3',blk=['(',')','sin(',')']"
                , ["myfunc", "packuni(getuni{_})", "nl",true ]
      ]

    ], mode=mode );
}  


module _h_test( mode=112 ){ //====================
	data=["m",3,"d",6];
	tmpl= "2014.{m}.{d}";
	doctest
	( 
		_h, [
		 	[
				_h("2014.{m}.{d}",  data)
				, "2014.3.6"
                ,str( tmpl, ", ", data )
			]
			,"// showkey=true :"
	 	 	,[
				_h("2014.[m].[d]",  data, true, "[,]")
				, "2014.m=3.d=6"
		  		, "'2014.[m].[d]', data, showkey=true, wrap='[,]'"
			]
			,"// hash has less key:val " 
			,[
				_h("2014.{m}.{d}", ["m",3])
				, "2014.3.{d}"
		 		, "tmpl, ['m',3]"
			]
			,"// hash has more key:val " 
			,[
				_h("2014.{m}.{d}", ["a",1,"m",3,"d",6] )
				, "2014.3.6"
		 		, "tmpl, ['a',1,'m',3,'d',6]"
			]	
	  	  ], mode=mode , scope=["tmpl", tmpl, "data", data]
	);
}
module has_test( mode=112 ){ //===================== 
	doctest(has,
	[ [has("abcdef","d"),true, "'abcdef','d'"]
	, [has("abcdef","m"),false, "'abcdef','m'"]
	, [has([2,3,4,5],3),true, "[2,3,4,5],3"]	
	, [has([2,3,4,5],6),false, "[2,3,4,5],6"]	
	, [has([2,3,4,5],undef),false, "[2,3,4,5],undef"]
    , ""
    , "// New 2015.2.16: x could be an array, and return true if has any"
    , ""
    , [has("abcdef",["d","y"]),true, "'abcdef',['d','y']"]
	, [has("abcdef",["m","y"]),false,"'abcdef',['m','y']"]
	, [has([2,3,4,5],[3,9]),true,"[2,3,4,5],[3,9]"]	
	, [has([2,3,4,5],[6,9]),false,"[2,3,4,5],[6,9]"]	
    ]
	,mode=mode );
}	
module hash_test( mode=112 ){ //====================
	h= 	[ -50, 20 
			, "xy", [0,1] 
			, 2.3, 5 
			,[0,1],10
			, true, 1 
			, 1, false
			,-5
			];

	doctest( hash,  
    [ str("> h= ", _fmth(h, eq=", ", iskeyfmt=true))
    , ""
    , [ hash(h, -50), 20, "h, -50" ]
    , [ hash(h, "xy"), [0,1], "h,'xy'"  ]
    , [ hash(h, 2.3), 5, "h,2.3" ]
    , [ hash(h, [0,1]), 10, "h,[0,1]" ]
    , [ hash(h, true), 1, "h,true" ]
    , [ hash(h, 1), false, "h,1" ]
    , [ hash(h, "xx"), undef, "h,'xx'" ]

    , "## key found but it's in the last item without value:"
    , [ hash(h, -5), undef, "h, -5" ]
    , "## notfound is given: "
    , [ hash(h, -5,"df"), "df", "h,-5,'df'", ["//", "value not found"] ]
    , [ hash(h, -50,"df"), 20, "h,-50,'df'" ]
    , [ hash(h, -100,"df"), "df", "h,-100,'df'" ]
    , "## Other cases:"
    , [ hash([], -100), undef, "[],-100" ]
    , [ hash(true, -100), undef, "true,-100" ]
    , [ hash([ [undef,10]], undef), undef, "[[undef,10]],undef" ]
    , " "
    , "## Note below: you can ask for a default upon a non-hash !!"
    , [ hash(true, -100, "df"), "df", "true,-100,'df'"]
    , [ hash(true, -100), undef, "true,-100 "]
    , [ hash(3, -100, "df"), "df", "3,-100,'df'"]
    , [ hash("test", -100, "df"), "df", "'test',-100,'df'"]
    , [ hash(false, 5, "df"), "df", "false,5,'df'"]
    , [ hash(0, 5, "df"), "df", "0,5,'df'" ]
    , [ hash(undef, 5, "df"), "df", "undef,5,'df'" ]
    , "  "
//    , "## New 20140528: if_v, then_v, else_v "
//    , ""
//    , str("> h= ", _fmth(h, eq=", ", iskeyfmt=true))
//    , ""
//    , [ hash(h, 2.3, if_v=5, then_v="got_5"), "got_5", "h,2.3,if_v=5, then_v='got_5'" ]
//    , [ hash(h, 2.3, if_v=6, then_v="got_5"), 5, "h,2.3,if_v=6, then_v='got_5'" ]
//    , [ hash(h, 2.3, if_v=6, then_v="got_5", else_v="not_5"), "not_5", "h, 2.3, if_v=6, then_v='got_5', else_v='not_5'"
//             ]
    ,"  "
    ,"## New 20140730: You can use string for single-letter hash:"
    ,[hash("a1b2c3","b"), "2", "'a1b2c3','b'" ]
    ,[hash("aAbBcC","b"), "B", "'aAbBcC','b'" ]
    //           ," "
    //           ,"## 20150108: test concat "
    //           ,["['a',3,'b',4,'a',5]", hash(["a",3,"b",4,"a",5],"a"), 3] 
    ], mode=mode //,["h",h]
    );
}
module hashex_test( mode=112 ){ //=======================
    //_mdoc("hashex_test","");
    h= 	[ -50, 20 
			, "xy", [0,1] 
			, 2.3, 5 
			];
    doctest( hashex,
    
       [ str("> h= ", _fmth(h, eq=", ", iskeyfmt=true))
         , ""
         , [ hashex(h, ["xy",2.3]), ["xy", [0,1], 2.3, 5] 
		   , "h, ['xy',2.3]"]
       ], mode=mode );
    }

module hashkvs_test( mode=112 ){ //=====================
	h = ["a",1,  "b",2, 3,33];

	doctest( hashkvs,
	[ [ hashkvs(h), [["a",1],["b",2],[3,33]], h ]
	], mode=mode
	);	
}
module haskey_test( mode=112 ){ //========================
	table = [ -50, 20 
			, "80", 25 
			, 2.3, "yes" 
			,[0,1],5
			, true, "YES" 
			];
	doctest(haskey, 
		 [ str( "table= ", _fmth(table))
         , [ haskey(table, -50), true, "table, -50" ]
    	 , [ haskey(table, "80"), true, "table, '80'" ]
		 , [ haskey(table, "xy"), false, "table, 'xy'" ]
		 , [ haskey(table, 2.3), true, "table, 2.3" ]
    	 , [ haskey(table, [0,1]), true, "table, [0,1]" ]
		 , [ haskey(table, true), true, "table, true" ]
    	 , [ haskey(table, -100), false, "table, -100" ]
    	 , [ haskey([], -100), false, "[], -100" ]
    	 , [ haskey( [undef,1], undef), true, "[[undef,1], undef", 
		                 , ["//","# NOTE THIS!"] ]
    	 ],mode=mode//, scope=["table", table]
	);

}
module incenterPt_test(mode=112) { //======================
        doctest (incenterPt, mode=mode ); }
module index_test( mode=112 ){ //======================
     arr = ["xy","xz","yz"];
	 s= "I_am_the_findw";

	doctest(index,  
	[ 
      "## Array indexing ##"
    , [ index(["xy","xz","yz"],"xz"),  1, "arr, 'xz'" ]
    , [ index(["xy","xz","yz"],"yz"),  2, "arr, 'yz'" ]
    , [ index(["xy","xz","yz"],"xx"),  -1, "arr, 'xx'" ]
    , [ index([],"xx"), -1, "[], 'xx'" ]
    , "## String indexing ##"
    , [ index( "I_am_the_findw","a"),  2, "s, 'a'" ]
    , [ index( "I_am_the_findw","am"),  2, "s, 'am'",  ]
    , [ index(  "abcdef","f"),  5, "'abcdef','f'" ]
    , [ index( "findw","n"),  2, "'findw','n'" ]
    , [ index( "I_am_the_findw","find"),  9, "s, 'find'" ]
    , [ index( "I_am_the_findw","the"),  5,"s, 'the'",  ]
    , [ index("I_am_the_ffindw","findw"),  10,"s, 'findw'" ]
    , [ index("findw", "findw"),  0, "'findw', 'findw'" ]
    , [ index( "findw","dwi"),  -1, "'findw', 'dwi'" ]
    , [ index("I_am_the_findw","The"),  -1,"s, 'The'" ]
    , [ index( "replace__text","tex"),  9, "s, 'tex'" ]
    , [ index("replace__ttext","tex"), 10, "'replace__ttext', 'tex'" ]
    , [ index( "replace_t_text","text"),  10, "'replace_t_text','tex'"]
    , [ index("abc", "abcdef"),  -1, "'abc', 'abcdef'" ]
    , [ index( "","abcdef"),  -1,"'', 'abcdef'" ]
    , [ index( "abc",""),  -1, "'abc',''" ]
//    , "## String indexing with wrap ##"
//    , [ index("{a}bc","a",wrap="{,}"), 0, "'{a}bc','a', '{,}'"] 
//    , [ index("a}bc","a",wrap=",}"), 0, "'a}bc','a', ',}'"] 
//    , [ index("abc","a",wrap=",}"), -1, "'abc','a', ',}'", 
//            ["//","Asking for a}"] ] 
//    , [ index("a{b}c","b",wrap="{,}"), 1, "'a{b}c','b', '{,}'"] 
//    , [ index("a//bc","b",wrap="//,"), 1, "'a//bc','b', '//,'"] 
//    , [ index("abc:d","c",wrap=",:"), 2, "'abc:d','c', ',:'"] 
//    , [ index("a{_b_}{_c_}","c",wrap="{_,_}"), 6, "'a{_b_}{_c_}','c', '{_,_}'", ] 
    ,"## Others ##"
    , [ index([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
    , [ index([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
    
    ],mode=mode, scope=["arr",arr, "s",s]
         
	);
}
module inrange_test( mode=112 ){ //======================

	doctest( inrange,
	[
	  "## string ##"
    , [ inrange("789",2), true , "'789',2"]
    , [ inrange("789",3), false , "'789',3"]
    , [ inrange("789",-2), true , "'789',-2"]
    , [ inrange("789",-3), true , "'789',-3"]
    , [ inrange("789",-4), false , "'789',-4"]
    , [ inrange("",0), false , "',0"]
    , [ inrange("789","a"), false , "'789','a'"]
    , [ inrange("789",true), false , "'789',true"]
    , [ inrange("789",[3]), false , "'789',[3]"]
    , [ inrange("789",undef), false , "'789',undef"]
	, "## array ##"
    , [ inrange([7,8,9],2), true , "[7,8,9],2"]
    , [ inrange([7,8,9],3), false , "[7,8,9],3"]
    , [ inrange([7,8,9],-2), true , "[7,8,9],-2"]
    , [ inrange([7,8,9],-3), true , "[7,8,9],-3"]
    , [ inrange([7,8,9],-4), false , "[7,8,9],-4"]
    , [ inrange([],0), false , "[],0"]
    , [ inrange([7,8,9],"a"), false , "[7,8,9],'a'"]
    , [ inrange([7,8,9],true), false , "[7,8,9],true"]
    , [ inrange([7,8,9],[3]), false , "[7,8,9],[3]"]
    , [ inrange([7,8,9],undef), false , "[7,8,9],undef"]
	], mode=mode
	);
}
module int_test( mode=112 ){ //=========================
	doctest( int, 
    [ [ int(3),3, "3"]
    , [ int(3.3),3, "3.3"]
    , [ int(-3.3),-3, "-3.3"]
    , [ int(3.8),4, "3.8"]
    , [ int(9.8),10, "9.8"]
    , [ int(-3.8),-4, "-3.8"]
    , [ int("-3.3"),-3, "'-3.3'"]
    , [ int("-3.8"),-3, "'-3.8'", ["//"," !!Note this!!"]]
    , [ int("3"),3, "'3'"]
    , [ int("3.0"),3, "'3.0'"]
    , [ int("0.3"),0, "'0.3'"]
    , [ int("34"),34, "'34'"]
    , [ int("-34"),-34, "'-34'"]
    , [ int("-34.5"),-34, "'-34.5'"]
    , [ int("345"),345, "'345'"]
    , [ int("3456"),3456, "'3456'"]
    , [ int("789987"),789987, "'789987'"]
    , [ int("789.987"),789, "'789.987'", ["//"," !!Note this!!"]]
    , [ int([3,4]),undef, "[3,4]"]
    , [ int("x"),undef, "'x'"]
    , [ int("34-5"),undef, "'34-5'"]
    , [ int("34x56"),undef, "'34x56'"]
	],mode=mode	
	);
}
module intersectPt_test( mode=112 ){ //=================
	doctest( intersectPt, mode=mode );
}


module is0_test( mode=112 ){ //=====================
	doctest( is0,
    [
      "// A value, 1e-15, is NOT zero:"
    , str("//   1e-15==0: ", 1e-15==0 )
    , "// but it's so small that it is equivalent to zero:"
    , [ is0(1e-15), true, "1e-15" ]
	],mode=mode
	);
}
module is90_test( mode=112 ){ //=================
	pqr = randPts(3);           // 3 random points
	sqPts = 	squarePts(pqr);   //  This gives 4 points of a square, must be perpendicular
							  // The first 2 pts = pq[0], pq[1]. 	

	pq1 = [ pqr[0], pqr[1]   ];  // Points 0,1 of the square
	_pq2= [ pqr[1], sqPts[2] ];  // Points 1,2 of the square

	// Make a random translation to pq2x:
	t = randPt();
	pq2 = [ _pq2[0]+t, _pq2[1]+t ];

	doctest( is90
	,[
	  "// A 3-pointer, pqr, is randomly generated. It is then made to generate"
	 ,"// a square with sqarePts(pqr), in which side PQ is our pq1, and a side"
	 ,"// perpendicular to pq is translated to somewhere randomly and taken as pq2."
	,  [ is90(pq1,pq2), true, "pq1,pq2"  ]
	],mode=mode, scope=["pq1",pq1,"pq2",pq2]  );
}


module isarr_test( mode=112 ){ //=========================
	doctest( isarr, 
    [ [ isarr([1,2,3]), true, [1,2,3]]
    , [ isarr("a"), false, "'a'"]
    , [ isarr(true), false, "true"]
    , [ isarr(false), false, "false"]
    , [ isarr(undef), false, "undef"]
	],mode=mode
	);
}
module isbool_test( mode=112 ){ //====================
	doctest( isbool, 
    [ [ isbool([1,2,3]), false, [1,2,3]]
    , [ isbool("a"), false, "'a'"]
    , [ isbool(true), true, "true"]
    , [ isbool(false), true, "false"]
    , [ isbool(undef), false, "undef"]
    ],mode=mode
	);
}
module isequal_test( mode=112 ){ //====================
	doctest( isequal, 
	[ str("// ZERO = ",ZERO)
    , [ isequal(1e-15,0),true, "1e-15,0"]
    , [ isequal(1e-12,0),false, "1e-12,0"]
    , [ isequal(1e-9,1e-10),false, "1e-9,1e-10"]
    , [ isequal(1e-12,1e-13),false, "1e-12,1e-13"]
    , [ isequal(1e-14,1e-13),true, "1e-14,1e-13"]
    , [ isequal("1e-15",0),false, "'1e-15',0"]
    , [ isequal("1e-15","0"),false, "'1e-15','0'"]
    , [ isequal("abc","abc"),true, "'abc','abc'"]
    , [ isequal(undef,undef),true, "undef,undef"]
    , [ isequal(undef,false),false, "undef,false"]
	],mode=mode
	);
}


module isfloat_test( mode=112 ){ //====================
	doctest(isfloat, 
	[ [ isfloat(3), false, "3"]
    , [ isfloat(3.2), true, "3.2"]
    , [ isfloat("a"), false, "'a'"]
	],mode=mode
	);
}
module ishash_test( mode=112 ){ //====================
	doctest(ishash, 
	[ [ ishash([1,2,3]), false, "[1,2,3]"]
    , [ ishash([1,2,3,4]), true, "[1,2,3,4]"]
    , [ ishash(3), false, "3"]
    , [ ishash(3.2), false, "3.2"]
    , [ ishash("a"), false, "'a'"]
	],mode=mode
	);
}
module isint_test( mode=112 ){ //====================
	doctest(isint, 
	[ [ isint(3), true, "3"]
    , [ isint(3.2), false, "3.2"]
    , [ isint("a"), false, "'a'"]
	],mode=mode
	);
}
module isln_test( mode=112 ){ //=================
    P=[2,3,4];
    Q=[5,6,7];
    R=[3,4,5];
    doctest( isln,
    [ [isln( P ), false, "P" ]
    , [isln( [P,Q] ), true, "[P,Q]" ]
    , [isln( [P,["a",2,3]] ), false, "[P,['a',2,3]]" ]
    , [isln( [P,P] ), false,  "[P,P]" ]
    , [isln( [P,Q,R] ), false, "[P,Q,R]" ]
    , [isln( [[2,3],[5,6]] ), false, [[2,3],[5,6]]]
    , [isln( [[2,3],[5,6]],2 ), true, "[[2,3],[5,6]],dim=2"]
    ], mode=mode , scope=["P",P,"Q",Q,"R",R]
    );
} 


module isnum_test( mode=112 ){ //=========================
	doctest(isnum, 
	[ [ isnum(3), true, "3"]
    , [ isnum(3.2), true, "3.2"]
    , [ isnum("a"), false, "'a'"]
	],mode=mode
	);
}
module isOnPlane_test( mode=112, ops=[] ){ //=========================
	pqr    = randPts(3);
	pcoefs = planecoefs( pqr );
	othopt = othoPt( pqr );
	abipt  = angleBisectPt( pqr );
	midpt  = midPt( p01(pqr) );

	doctest( isOnPlane,
	[ "## Randomly create a 3-pointer, pqr, and get its planecoefs, pcoefs."
    , "## From pqr also create othoPt, angleBisectPt and midPt (of p,q)."
    , "## They should all be on the same plane."
    , [ isOnPlane( pqr,P(pqr) ), true, "pqr,P(pqr)" ]
    , [ isOnPlane( pqr, othopt ), true,  "pqr, othopt" ]
    , [ isOnPlane( newy(pqr,othopt),  othopt  ), true, "newy(pqr,othopt), othopt" ]
    , [ isOnPlane( pcoefs,othopt),  true, "pcoefs, othopt" ]
    , "// angleBisectPt: "
    , [ isOnPlane( newy( pqr,abipt), abipt ),true, "newy(pqr,abipt), abipt" ]
    , [ isOnPlane( pcoefs, abipt ),  true, "pcoefs, abipt" ]
    , "// Mixed:"
    , [ isOnPlane( [othopt, abipt, midpt] , pqr[2] ), true, 
       "[othopt, abipt, midpt], pqr[2]" ]
    , "// normalPt:"
    , [ isOnPlane( pqr, N(pqr)), true, "pqr,N(pqr)"]
    
	], mode=mode, ops=ops, scope= ["pqr", pqr
		    , "pcoefs", pcoefs
			, "othopt", othopt
			, "abipt", abipt
			, "midpt", midpt]
	);
}

module ispl_test( mode=112 ){ //=================
    P=[2,3,4];
    Q=[5,6,7];
    R=[3,4,5];
    doctest( ispl,
    [ [ ispl( P ), false, "P" ]
    , [ ispl( [P,Q] ), false, "[P,Q]" ]
    , [ ispl( [P,Q,R] ), true, "[P,Q,R]"]
    , [ ispl( [P,P,R] ), false, "[P,P,R]"]
    , [ ispl( [P,Q,P] ), false, "[P,Q,P]"]
    , [ ispl( [P,Q,Q] ), false, "[P,Q,Q]"]
    , [ ispl( [[2,3],[5,6],[7,8]] ), false, [[2,3],[5,6],[7,8]]]
    , [ ispl( [[2,3],[5,6],[7,8]],2 ), true, "[[2,3],[5,6],[7,8]],dim=2"]
    ], mode=mode , scope=["P",P,"Q",Q,"R",R]
    );
}


module ispt_test( mode=112){ //===================
    doctest( ispt,
    [ [ ispt([2,3,4]), true, [2,3,4] ]
    , [ ispt([2,3,4,5]), false, [2,3,4,5] ]
    , [ ispt([2,3,"4"]), false, [2,3,"4"] ]
    , [ ispt([2,3]), false, [2,3] ]
    , [ ispt([2,3],2), true, "[2,3],dim=2" ]
    , [ ispt( 234), false, 234]
    , [ ispt( "234"), false, "'234'"]
    ],mode=mode
    );
}


module isstr_test( mode=112 ){ //=====================
	doctest( isstr, 
	[ [ isstr(3), false, "3"]
    , [ isstr(3.2), false, "3.2"]
	, [ isstr("a"), true, "'a'"]
	],mode=mode
	);
}


module istype_test( mode=112 ){ //=====================
	doctest( istype,
	[ [ istype("3","str"), true, "'3','str'"]
    , [ istype("3",["str","arr"]), true, "'3',['str','arr']"]
    , [ istype(3,["str","arr"]), false, "3,['str','arr']"]
    , [ istype(3,["str","arr","int"]), true, "3,['str','arr','int']"]
    , [ istype(3,"str,arr,int"), true, "3,'str,arr,int'"]
    , [ istype(3,"str arr int"), true, "3,'str arr int'"]
    , [ istype(3,"str arr bool"), false, "3,'str arr bool'"]
	], mode=mode
	);
}


module join_test( mode=112 ){ //=====================
	doctest( join, 
	[ [ join([2,3,4]),      "234" , "[2,3,4]"]
    , [ join([2,3,4], "|"), "2|3|4" , "[2,3,4], '|'"]
    , [ join([2,[5,1],"a"], "/"), "2/[5, 1]/a" , "[2,[5,1],'a'], '/'"]
    , [ join([], "|"), "" , "[], '|'"]
	],mode=mode
	);
}

module joinarr_test( mode=112 ){ //=====================
	doctest( joinarr, 
	[ [ joinarr([[2,3],[4,5]]), [2,3,4,5], "[[2,3],[4,5]]" ]
    , ""
    , "// 2015.2: you can also use the new run(): "
    , ""
    , [ run("concat",[[2,3],[4,5]]), [2,3,4,5],[[2,3],[4,5]],
            ["myfunc", "run('concat',[[2,3],[4,5]])" ] ]
    , ""
    , "// 2015.4: or the new flatten(a,d=1): "
    , ""
    , [ flatten([[2,3],[4,5]]), [2,3,4,5], [[2,3],[4,5]],
            ["myfunc", "flatten{_}" ] ]
    
	],mode=mode
	);
}
module keys_test( mode=112 ){ //====================
    
	doctest( keys, 
	[ [ keys(["a",1,"b",2]), ["a","b"] , "['a',1,'b',2]"]
    , [ keys([1,2,3,4]), [1,3] , "[1,2,3,4]"]
    , [ keys([[1,2],0,3,4]), [[1,2],3] , "[1,2],0,3,4"]
    , [ keys([]), [] , "[]"]
	],mode=mode
	);
}


module lcase_test( mode=112 ){ //=======================
	doctest( lcase,
	[ [lcase("aBC,Xyz"), "abc,xyz", "'aBCC,Xyz'"]
	], mode=mode
	);
}
module last_test( mode=112 ){ //==================
	doctest( last, 
	 [ [ last([20,30,40,50]), 50, [20,30,40,50] ]
     , [ last([]), undef,[] ]
     , [ last("abcdef"), "f", "'abcdef'" ]
     , [ last(""), undef,"''" ]
     ],mode=mode
	);
}

module lineCrossPt_test(mode=112){ //=====================
    doctest( lineCrossPt, mode=mode ); }
module linePts_test( mode=112 ){ //=====================
	doctest( linePts,mode=mode );
}

module longside_test( mode=112 ){ //=====================
    doctest( longside, 
    [ [ longside(3,4), 5, "3,4" ]
	], mode=mode
	);
}

module lpCrossPt_test( mode=112 ){ //========================
    }



module parsedoc_test( mode=112 ){ //====================
	doctest( parsedoc, mode=mode );
}

module _mdoc_test( mode=112 ){ //====================
	doctest( _mdoc, mode=mode );
}


module midPt_test( mode=112 ){ //====================
    pq= [ [2,0,4],[1,0,2] ];
    doctest( midPt, 
	[ 
	  [ midPt( pq ), [1.5,0,3], pq ]
	], mode=mode
 );
}

module minPt_test( mode=112 ){ //======================
	pts1= [[2,1,3],[0,2,1] ];
	pts2= [[2,1,3],[0,3,1],[0,2,0], [0,2,1]];
	
	doctest( minPt,
	[
      [ minPts( pts1 ), [0,2,1], "pts1" ]
    , [ minPts( pts2 ), [0,2,0], "pts2" ]
    ], mode=mode, scope=["pts1", pts1, "pts2", pts2]
	);
}

module minxPts_test( mode=112 ){ //======================
	pts1= [[2,1,3],[0,2,1] ];
	pts2= [[2,1,3],[0,3,1],[0,2,0]];
	
	doctest( minxPts,
	[
      [ minxPts( pts1 ), [[0,2,1]], "pts1" ]
    , [ minxPts( pts2 ), [[0,3,1],[0,2,0]], "pts2" ]
    ],mode=mode, scope= ["pts1", pts1, "pts2", pts2]
	);
}
module minyPts_test( mode=112 ){ //==================
	pts1= [[2,1,3],[0,2,1],[3,3,0]];
	pts2= [[2,1,3],[0,3,1],[3,1,0]];
	
	doctest
	( minyPts
		,[
			 [ minyPts( pts1 ), [[2,1,3]], "pts1" ]
			,[ minyPts( pts2 ), [[2,1,3],[3,1,0]], "pts2" ]
		 ],mode=mode, scope=["pts1", pts1, "pts2", pts2]
	);
}
module minzPts_test( mode=112 ){ //==================
	pts1= [[2,1,3],[0,2,1]];
	pts2= [[2,1,3],[0,3,1],[0,2,1]];
	
	doctest
	( minzPts
		,[
			 [ minzPts( pts1 ), [[0,2,1]], "pts1"  ]
			,[ minzPts( pts2 ), [[0,3,1],[0,2,1]], "pts2" ]
		 ], mode=mode, scope=["pts1", pts1, "pts2", pts2]
	);
}
module mod_test( mode=112 ){ //==================
	doctest( mod,
	[
      [ mod(5,3), 2, "5,3"]
    , [ mod(-5,3), -2 , "-5,3"]
    , [ mod(5,-3), 2, "5,-3"]
    , [ mod(7,3),1, "7,3"]
    , [ mod(-7,3), -1 , "-7,3"]
    , [ mod(3,3), 0 , "3,3"]
    , [ mod(3.8,1), 0.8 , "3.8,1"]
    , [ mod(38,1), 0 , "38,1"]
	], mode=mode

	);
}
module normal_test( mode=112 ){ //==================
	pqr = [[-1, -2, 2], [1, -4, -1], [2, 3, 3]];
	doctest
	(
		normal, 
		[
			[ normal(pqr), [-13, 11, -16], pqr ]

		], mode=mode
	);
}


module normalPt_test( mode=112 ){ //==================
	doctest( normalPt,
	[]
	, mode=mode
	);
}
		

//module normalPt_demo()
//{
//	echom("normalPt");
//	pqr = randPts(3);
//	P = pqr[0];  Q=pqr[1]; R=pqr[2];
//	Chain( pqr, ["closed", false] );
//	N = normalPt( pqr );
//	echo("N",N);
//	MarkPts( concat( pqr,[N]) , ["labels","PQRN"] );
//	//echo( [Q, N] );
//	Mark90( [N,Q,P] );
//	Mark90( [N,Q,R] );
//	Dim( [N, Q, P] );
//	Line0( [Q, N], ["r",0.02,"color","red"] );
//	Nr = normalPt( pqr, len=1, reversed=true);
//	echo("Nr",Nr);
//	Mark90( [Nr,Q,P] );
//	Mark90( [Nr,Q,R] );
//	Dim( [Nr, Q, P] );
//	Line0( [Q, Nr], ["r",0.02,"color","green"] );
//
//}
//normalPt_demo();
//Dim_test( );

//========================================================
module normalPts_test( mode=112 ){}



//========================================================
module num_test( mode=112 )
{
	doctest( num, 
    [
      [ num(-3.8),-3.8, "-3.8"]
    , [ num("3"),3, "'3'"]
    , [ num("340"),340, "'340'"]
    , [ num("3040"),3040, "'3040'"]
    , [ num("3.4"),3.4, "'3.4'"]
    , [ num("30.04"),30.04, "'30.04'"]
    , [ num("340"),340, "'340'"]
    , [ num("3450"),3450, "'3450'"]
    , [ num(".3"),0.3, "'.3'"]
    , [ num("-.3"),-0.3, "'-.3'"]
    , [ num("0.3"),0.3, "'0.3'"]
    , [ num("-0.3"),-0.3, "'-'0.3'"]
    , [ num("-15"),-15, "'-15'"]
    , [ num("-3.35"),-3.35, "'-3.35'"]
    , [ num("789.987"),789.987, "'789.987'"]
	, ""	
	, "// Convert to inches:"
    , [ num("2'6\""), 30, "2`6'"]
    //, [ num("3""), 3, "'3'"]
    , [ num("3'"), 36, "3\""]
	, ""	
    , [ num([3,4]),undef, "[3,4]"]
    , [ num("x"),undef, "'x'"]
    , [ num("34-5"),undef, "'34-5'"]
    , [ num("34x56"),undef, "'34x56'"]
    , [ num(""),undef, "''"]
	],mode=mode

	);
}

function _getdeci(n)= index(str(n),".")<0?0:len(split(str(n),".")[1]);

//========================================================
module numstr_test( mode=112)
{
	doctest( numstr,
	[
	  "// Integer: "
    , [ numstr(0), "0", "0"]
    , [ numstr(5), "5", "5"]
    , [ numstr(5,d=2), "5.00", "5,d=2"]
    , [ numstr(5,dmax=2), "5", "5,dmax=2"]
    , [ numstr(38, d=0), "38", "38,d=0"]
    , [ numstr(38, d=2), "38.00", "38,d=2"]
	, "// Float. Note the difference between d and dmax: "
    , [ numstr(3.80), "3.8", "3.80"]
    , [ numstr(38.2, dmax=3), "38.2", "38.2,dmax=3"]
    , [ numstr(38.2, d=3, dmax=2), "38.20", "38.2,d=3, dmax=2"]
    , [ numstr(38.2, d=3, dmax=4), "38.200", "38.2,d=3, dmax=4"]
    , [ numstr(38.2, d=5, dmax=4), "38.2000", "38.2,d=5, dmax=4"]
    , [ numstr(3.839,d=0), "4", "3.839, d=0"]
    , [ numstr(3.839,d=1), "3.8", "3.839, d=1"]
    , [ numstr(3.839,d=2), "3.84", "3.839, d=2"]
    , [ numstr(3.839,d=3), "3.839", "3.839, d=3"]
    , [ numstr(3.839,d=4), "3.8390", "3.839, d=4"]
    , [ numstr(3.839,d=1, dmax=2), "3.8", "3.839, d=1, dmax=2"]
    , [ numstr(3.839,d=2, dmax=2), "3.84", "3.839, d=2, dmax=2"]
    , [ numstr(3.839,d=3, dmax=2), "3.84", "3.839, d=3, dmax=2"]
	 ,[ numstr(3.839,d=4, dmax=2), "3.84", "3.839, d=4, dmax=2"]	
    , [ numstr(-3.839,d=1), "-3.8", "-3.839, d=1"]
    , [ numstr(-3.839,d=2), "-3.84", "-3.839, d=2"]
    , [ numstr(-3.839,d=4), "-3.8390", "-3.839, d=4"]

	, "// conversion: "
	 ,"// conv = 'ft:ftin'"
    , [ numstr(0.5,conv="ft:ftin"), "6\"", "0.5,conv='ft:ftin'"]
    , [ numstr(3,conv="ft:ftin"), "3'", "3,conv='ft:ftin'"]
    , [ numstr(3.5,conv="ft:ftin"), "3'6\"", "3.5,conv='ft:ftin'"]
    , [ numstr(3.5,d=2,conv="ft:ftin"), "3'6.00\"", "3.5,d=2,conv='ft:ftin'"]
    , [ numstr(3.343,conv="ft:ftin"), "3'4.116\"", "3.343,conv='ft:ftin'"]
    , [ numstr(3.343,d=2,conv="ft:ftin"), "3'4.12\"", "3.343,d=2, conv='ft:ftin'"]
	, "// conv = 'in:ftin'"
    , [ numstr(96,conv="in:ftin"), "8'", "96,conv='in:ftin'"]
    , [ numstr(32,conv="in:ftin"), "2'8\"", "32,conv='in:ftin'"]
	, "// d is ignored if negative:"
    , [ numstr(38, d=-1), "38", "38,d=-1"]
		
	], mode=mode );

}
//numstr_test();


//========================================================
module onlinePt_test( mode=112 )
{
	pq= randPts(2);
	d1pt= onlinePt( pq, len=1 );
	r3pt= onlinePt( pq, ratio=0.3);
	distpq = dist(pq);
 
	doctest( onlinePt, 
	[
     [ onlinePt(pq,0), pq[0] , "pq,0"]
    , [ onlinePt(pq,ratio=1), pq[1] , "pq,ratio=1"]

	, [ norm(pq[0]- onlinePt(pq,len=1))
		, 1, "pq,len=1"
        , ["myfunc", "norm( pq[0]- onlinePt{_} )"]]
    
	, [ norm(pq[1]- onlinePt(pq,len=-2))
		, distpq+2, "pq,len=-2"
        , ["myfunc", "norm( onlinePt{_}-pq[1] )"]]

	, [ norm(pq[0]- onlinePt(pq,ratio=0.3))
		, distpq*0.3, "pq,ratio=0.3"
        , ["myfunc", "norm( pq[0]- onlinePt{_} )"]]
        //, ["funcwrap", "norm({_}-pq[0])"]]
        
	, [ norm(pq[1]- onlinePt(pq,ratio=-0.3))
		, distpq*1.3, "pq,ratio=-0.3"
        , ["myfunc", "norm( pq[1]- onlinePt{_} )"]]
        //, ["funcwrap", "norm({_}-pq[1])"]]	 
	]

	, mode=mode, scope=["pq", pq, "distpq", distpq, 
                        "d1pt", d1pt, "r3pt", r3pt]
    );
}

//doc(onlinePt);



//========================================================
module onlinePts_test( mode=112 )
{
	pq= [[0,1,0],[10,1,0]]; //randPts(2);
	lens= [-1,2];
	ratios= [0.2, 0.4, 0.6];
	distpq = dist(pq);
 
	doctest( onlinePts, 
	[
	   [ onlinePts(pq,lens), [[-1, 1, 0], [2, 1, 0]], "pq,[-1,2]" ]
	,  [ onlinePts(pq,ratios=ratios)
                 , [[2, 1, 0], [4, 1, 0], [6, 1, 0]], "pq,ratios=ratios"]
//    ,"// Case below produces exact same result, but why test fails ? " 
//    ,  [ "pq,ratios=range(0.2,0.2,0.7)", onlinePts(pq,ratios=range(0.2,0.2,0.7))
//                                        , [[2, 1, 0], [4, 1, 0], [6, 1, 0]]]
     ]
	, mode=mode, scope=["pq", pq
           ,"lens", lens
           ,"ratios",ratios
           ,"distpq",distpq] 
            );
}


//========================================================
module or_test( mode=112 )
{
	doctest( or, 
	[ [ or(false,3), 3, "false, 3"]
    , [ or(undef,3), 3, "undef, 3"]
    , [ or(0    ,3), 3, "0,     3"]
    , [ or(50   ,3), 50, "50,    3"]
	], mode=mode
	);
}
//trueor_test( ["mode",22] );	

//========================================================
module othoPt_test( mode=112 )
{
	pqr= randPts(3);
	opt= othoPt( pqr );
	doctest( othoPt,
	[ 
      [ "pqr=",pqr,"", ["notest",true, "ml",1,"myfunc","pqr"] ]
    , [ is90([ pqr[0], opt],[opt, pqr[1]]), true,  "pqr",
        [ "myfunc"
        , "is90([ pqr[0], othoPt(pqr)],[othoPt(pqr), pqr[1]])"
        ]
      ]
    , [ is90([ pqr[0], opt, pqr[1]]), true,  "pqr",
        [ "myfunc"
        , "is90([ pqr[0], othoPt(pqr), pqr[1]])"
        ]
      ]
//    ,[ is90([ pqr[0], opt, pqr[1]]), "pqr", true,
//        ["funcwrap", "is90([ pqr[0], {_},  pqr[1]])"]
//     ]
    , [ angle([ pqr[0], opt, pqr[1]]), 90,  "pqr",
        [ "myfunc"
        , "angle([ pqr[0], othoPt(pqr), pqr[1]])"
        ]
      ]
//      ,[ angle([ pqr[0], opt, pqr[1]]), "pqr", 90,
//        ["funcwrap", "angle([ pqr[0], {_}, pqr[1]])"]
//      ]
	], mode=mode //, scope=["pqr",pqr]
	);
}
//========================================================
module packuni_test( mode=112 )
{
   doctest( packuni
   ,[ 
    
     [ packuni([["a","#/1"], ["b","#/2"], ["c","d"]])
      , ["a",["b",["c","d"]]]
      , [ ["a","#/1"], ["b","#/2"], ["c","d"]]
     ]

   , [ packuni( [["#/1"], ["a","x","#/2"], ["b","#/3"], ["c","d"]])
     , [["a", "x", ["b", ["c", "d"]]]]
     , [ ["#/1"], ["a","x","#/2"], ["b","#/3"], ["c","d"]]
     ]

   , [ packuni( [ ["#/1","*","#/2"], ["x","+","1"], ["y","+","2"]] )
     , [["x", "+", "1"], "*", ["y", "+", "2"]]
     , [["#/1","*","#/2"], ["x","+","1"], ["y","+","2"]]
     ]

    ,[ packuni( [["#/1","*","#/2"], ["x","+","1"], ["sin","y","+","2"]] )
     , [["x", "+", "1"], "*", ["sin", "y", "+", "2"]]
     , [["#/1","*","#/2"], ["x","+","1"], ["sin","y","+","2"]]
     ]
         
    ,[ packuni( [["#/1", "*", "#/2"], ["x", "+", 1], ["y", "+", 2]] )
     , [["x", "+", 1], "*", ["y", "+", 2]]
     , [["#/1", "*", "#/2"], ["x", "+", 1], ["y", "+", 2]]
     ]
     
//     ,[  [["#/1", "^"], ["left_arm", "+", 1]]
//     , packuni( [["#/1", "^"], ["left_arm", "+", 1]] )
//     , []
//     ]
//     
//     ,[  [["#/1", "+", 2], ["left_arm", "+", 1]]
//     , packuni( [["#/1", "+", 2], ["left_arm", "+", 1]] )
//     , []
//     ]
     
//     ,[  [["#/1", "^", 2], ["left_arm", "+", 1]]
//     , packuni( [["#/1", "^", 2], ["left_arm", "+", 1]] )
//     , []
//     ]
     
 
    ], mode=mode );
}



//========================================================
module permute_test( mode=112 )
{
	doctest( permute
	,[
	  [permute([2,4,6])
		, [[2,4,6],[2,6,4],[4,2,6],[4,6,2],[6,2,4],[6,4,2]]
        ,[2,4,6], ["ml",1]
	  ]
	,  [ permute(range(3)),[[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]], "range(3)", ["ml",1]]
  
    , [permute([1,2,3,4]),
		[ [1, 2, 3, 4], [1, 2, 4, 3], [1, 3, 2, 4]
		, [1, 3, 4, 2], [1, 4, 2, 3], [1, 4, 3, 2]
		, [2, 1, 3, 4], [2, 1, 4, 3], [2, 3, 1, 4]
		, [2, 3, 4, 1], [2, 4, 1, 3], [2, 4, 3, 1]
		, [3, 1, 2, 4], [3, 1, 4, 2], [3, 2, 1, 4]
		, [3, 2, 4, 1], [3, 4, 1, 2], [3, 4, 2, 1]
		, [4, 1, 2, 3], [4, 1, 3, 2], [4, 2, 1, 3]
		, [4, 2, 3, 1], [4, 3, 1, 2], [4, 3, 2, 1]
	    ]
        ,[1,2,3,4] 
        , ["ml",1]
	   ]

	], mode=mode
	);
}
//=======================================================
module pick_test( mode=112 )
{
	arr = range(1,5);
	arr2= [ [-1,-2], [2,3],[5,7], [9,10]] ;
	
	doctest( pick
	,[
		"//Array:"
		,[ pick(arr, 2), [ 3, [1,2,4] ], "arr,2" ]
		,[ pick(arr,-3), [ 2, [1,3,4] ], "arr,-3" ]
		,[ pick(arr2,2), [ [5,7], [ [-1,-2], [2,3], [9,10]] ], "arr2,2" ]
		,"//String:"
		,[ pick("abcdef",3), ["d", ["abc", "ef"]], "'abcdef',3" ]
		,[ pick("this is",2), [ "i", ["th", "s is"] ], "'this is',2" ]
		,"//Note:"
		,str("split('this is','i') = ", split("this is","i") )
	],mode=mode, scope=["arr", arr, "arr2", arr2]
	);
}
//========================================================
module planecoefs_test( mode=112 )
{
	pqr = randPts(3);
	//pcs = planecoefs(pqr); // =[ a,b,c,k ]
	//a= pcs[0];
	//b= pcs[1];
	//c= pcs[2];
	//k= pcs[3];
	othopt = othoPt( pqr );
	//abpt= angleBisectPt( pqr );
	//mcpt= cornerPt( pqr );
	
	doctest( planecoefs, 
	[
         [ pqr, pqr,"",["myfunc", "pqr", "ml",1, "notest",true]]
        //,str("pqr = ", dt_arrbr( pqr ) )
		, [ othopt, othopt,"pqr",["myfunc", "othoPt", "notest",true]]
        , [ isOnPlane( planecoefs(pqr),othopt ), true ,"pqr", 
		, ["myfunc","isOnPlane( planecoefs{_}, othopt )"]
		//, ["funcwrap","isOnPlane( othopt, planecoefs= {_} )"]
		]
		,"// Refer to isOnPlane_test() for more tests."

	], mode=mode//, scope=["pqr", pqr, "othopt", othopt] 
    );



}
module ppEach_test( mode=112 )
{ 
	arr=[[1,2],[3,4],[5,6]];
	doctest( ppEach,
	[
		[ppEach(arr,9), [[9,1,2],[9,3,4],[9,5,6]], "arr,9" ]
		,[ppEach([[1,2],3],9), [[9,1,2],[9,3]], "[[1,2],3],9" ]
	],mode=mode, scope=["arr",arr]
	);
} 
//========================================================
module pqr90_test( mode=112 ){} // ???

module prod_test( mode=112)
{
   doctest( prod,
  [
    [ prod([1,2,3]), 6, [1,2,3] ]
   ,[ prod(range(1,0.5,3)),7.5, "range(1,0.5,3)" ]
  ], mode=mode );
}    
   

//projPt_demo();
module projPt_test( mode=112 ){ 
    doctest( projPt,
    []
    ,mode=mode );
    }
//========================================================
module quadrant_test( mode=112 )
{
	doctest( quadrant,
	mode=mode
	);
}


//========================================================
module rand_test( mode=112 )
{
	arr=["a","b","c"];
    //echo("rand_test, arr=", arr);
	doctest( rand,
	[
		"// rand(a)"
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,"//rand(a,b)"
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,"//rand(arr)"
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,"// rand(len(arr))=> float"
		,"// rand(range(arr))=> integer = floor(rand(len(arr)))"
		,str(">> rand(range(arr)):", rand(range(arr)))
		,str(">> rand(range(arr)):", rand(range(arr)))
		,str(">> rand(range(arr)):", rand(range(arr)))
        ,"// Add arg seed 2015.2.26, making it repeatable"
        ,[ rand(5,seed=3),2.75399,"5,seed=3", ["asstr",true]]
        ,[ rand(5,8, seed=3),6.65239,"5,8, seed=3", ["asstr",true]]
        ,[ rand(arr, seed=3),"b","arr, seed=3"]
    
	], mode=mode, scope=["arr",arr]
	);
}
//module 


//========================================================
module randc_test( mode=112 ){ doctest(randc, [], mode=mode );}
//module randc_demo()
//{
//	light = [0.8,1];
//	dark  = [0,0.4];
//	translate(randPt()) color(randc(r=dark,g=dark,b=dark)) sphere(r=0.5);
//	translate(randPt()) color(randc()) sphere(r=1);
//	translate(randPt()) color(randc(r=light, g=light,b=light)) sphere(r=1.5);
//}


//========================================================
module randPt_test( mode=112 ){ doctest( randPt, mode=mode ); }


//doc(randPt);

//========================================================
module randPts_test( mode=112 ){ doctest( randPts, mode=mode ); }


//randPts_test();
//doc(randPts);

//========================================================
//randCirclingPts=[[]];
//function randCirclingPts(count=4, r=undef)
//(
//3
//
//);

//========================================================

//randInPlanePt_demo();
module randInPlanePt_test( mode=112 ){}


//========================================================
module randInPlanePts_test( mode=112 ){}


//========================================================
module randOnPlanePt_test( mode=112 ){}


//========================================================
module randOnPlanePts_test( mode=112 ){}




//========================================================
module range_test( mode=112 )
{
	obj = [10,11,12,13];
	doctest( range,
	[
    
	  "// 1 arg: starting from 0"
    , [ range(5), [0,1,2,3,4], "5"]
    , [ range(-5), [0, -1, -2, -3, -4] , "-5"]
    , [ range(0), [] , "0"]
    , [ range([]), [] , "[]"]

	, ""
	, "// 2 args: "
    , [ range(2,5), [2,3,4], "2,5"]
    , [ range(5,2), [5,4,3], "5,2"]
    , [ range(-2,1), [-2,-1,0], "-2,1"]
    , [ range(-5,-1), [-5,,-4,-3,-2], "-5,-1"]

	,""
    , "// Note these are the same:"
    , [ range(3), [0,1,2], "3"]
    , [ range(0,3), [0,1,2], "0,3"]
    , [ range(-3), [0,-1,-2], "-3"]
    , [ range(0,-3), [0,-1,-2], "0,-3"]
    , [ range(1),[0] , "1"]
    , [ range(-1),[0] , "-1"]
		
    ,""
	,"// 3 args, the middle one is interval. Its sign has no effect"
    , [ range(2,0.5, 5), [2,2.5,3,3.5, 4, 4.5], "2,0.5,5"]
    , [ range(2, -0.5, 5), [2,2.5,3,3.5, 4, 4.5], "2,-0.5,5"]
    , [ range(5,-1,0), [5,4,3,2,1], "5,-1,0"]
    , [ range(5,1,2), [5,4,3], "5,1,2"]
    , [ range(8,2,0),[8,6,4,2], "8,2,0"]
	, [ range(0,2,8),[0, 2, 4, 6], "0,2,8"]			
    , [ range(0,3,8),[0, 3, 6], "0,3,8"]
		
	, ""
	, "// Extreme cases: "
		
    , [ range(), [], "", ["//", "Give nothing, get nothing"]]
    , [ range(0),[], "0", ["//", "Count from 0 to 0 gets you nothing"] ]
    , [ range(2,2),[], "2,2", ["//", "2 to 2 gets you nothing, either"] ]
    , [ range(2,0,4),[], "2,0,4", ["//", "No intv gets you nothing, too"] ]
    , [ range(0,1),[0] , "0,1"]
    , [ range(0,1,1),[0] , "0,1,1"]

	, "// When interval > range, count only the first: "
    , [ range(2,5,4),[2] , "2,5,4"]
    , [ range(2,5,-1),[2] , "2,5,-1"]
    , [ range(-2,5,-4),[-2] , "-2,5,-4"]
    , [ range(-2,5,1),[-2] , "-2,5,1"]

    , ""
    , " range( <b>obj</b> )"
    , ""
    , "// range( arr ) or range( str ) to return a range of an array"
    , "// so you don't have to do : range( len(arr) )"
    , ""
    , str("> obj = ",obj)
    , ""
    , [ range(obj),[0,1,2,3], "obj"]
    , [ range([3,4,5]), [0,1,2] , "[3,4,5]"]
    , [ range("345"), [0,1,2] , "'345'"]
    , [ range([]), [] , "[]"]
    , [ range(""), [] , "'"]

    , ""
    , " range( ... <b>cycle= i,-i,true</b>...)"
    , ""
    , "// New 2014.9.7: "
    , "// cycle= +i: extend the index on the right by +i count "
    , "// cycle= -i: extend the index on the right by -i count"
    , "// cycle= true: same as cycle=1 "
    , "" 
    , [ range(4,cycle=1), [0,1,2,3,0], "4,cycle=1", ["//","Add first to the right"] ]
    , [ range(4,cycle=-1), [3,0,1,2,3], "4,cycle=-1", ["//","Add last to the left"] ]

	, [ range(4,cycle=true), [0,1,2,3,0], "4,cycle=true", ["//","true=1"] ]
		,""

    , [ range(2,5,cycle=1), [2,3,4,2], "2,5,cycle=1", ["//","Add first to the right"] ]
    , [ range(2,5,cycle=2), [2,3,4,2,3], "2,5,cycle=2", ["//","Add 1st,2nd to the right"] ]
	, [ range(2,5,cycle=-1), [4,2,3,4], "2,5,cycle=-1", ["//","Add last to the left"] ]
    , [ range(2,5,cycle=-2), [3,4,2,3,4], "2,5,cycle=-2"]
    , [ range(2,5,cycle=true), [2,3,4,2], "2,5,cycle=true", ["//","true=1"] ]
		
	, ""
    , [ range(2,1.5,8,cycle=2), [2, 3.5, 5, 6.5, 2, 3.5] , "2,1.5,8,cycle=2"]
    , [ range(2,1.5,8,cycle=-2), [5, 6.5, 2, 3.5, 5, 6.5] , "2,1.5,8,cycle=-2"]
    , [ range(2,1.5,8,cycle=true), [2, 3.5, 5, 6.5, 2] , "2,1.5,8,cycle=true"]
		
    , ""
    , " range(<b>obj</b>,<b>cycle=i,-1,true</b>...)"
    , ""
    , str("> obj = ",obj)
    ,[ range(obj,cycle=1)	,[0,1,2,3,0], "obj,cycle=1"] 
    ,[ range(obj,cycle=2)	,[0,1,2,3,0,1], "obj,cycle=2"] 
    ,[ range(obj,cycle=-1)	,[3,0,1,2,3], "obj,cycle=-1"] 
    ,[ range(obj,cycle=-2)	,[2,3,0,1,2,3], "obj,cycle=-2"] 

	]

	, mode=mode
	);
}


//========================================================
module ranges_test( mode=112 )
{
	obj = [10,11,12,13];
	doctest( ranges
	,[
	  "// 1 arg: starting from 0"
    , [ ranges(3), [[0, 1], [1, 2], [2, 0]] , "3"]
    , [ ranges(-3), [[0,-1], [-1,-2], [-2,0]] , "-3"]
	, ""
	, "// 2 args: "
    , [ ranges(2,5), [[2,3], [3,4], [4,2]] , "2,5"]
    , [ ranges(5,2), [[5,4], [4,3], [3,5]] , "5,2"]
    , [ ranges(-2,1), [[-2,-1], [-1,0], [0,-2]], "-2,1"]
    , [ ranges(-5,-1), [[-5,-4], [-4,-3], [-3,-2], [-2,-5]], "-5,-1"]

	, ""
    , "// Note these are the same:"
    , [ ranges(3), [[0, 1], [1, 2], [2, 0]] , "3"]
    , [ ranges(0,3), [[0, 1], [1, 2], [2, 0]], "0,3"]
    , [ ranges(-3), [[0, -1], [-1, -2], [-2, 0]], "-3"]
    , [ ranges(0,-3), [[0, -1], [-1, -2], [-2, 0]], "0,-3"]
    , [ ranges(1),[[0]] , "1"]
    , [ ranges(-1),[[0]] , "-1"]
    
    , ""
    , "// 3 args, the middle one is interval. Its sign has no effect"
    , [ranges(2,0.5,4) ,  [[2, 2.5], [2.5, 3], [3, 3.5], [3.5, 2]], "2,0.5,4"]
    , [ranges(2,-0.5,4) ,  [[2, 2.5], [2.5, 3], [3, 3.5], [3.5, 2]], "2,-0.5,4"]
    , [ranges(2,0.5,4,cover=2) ,  [[2,2.5,3], [2.5,3,3.5], [3,3.5,2], [3.5,2,2.5]],"2,0.5,4, cover=2"]
    , [ranges(2,0.5,4,cover=-1) 
        ,  [[3.5,2], [2,2.5], [2.5, 3], [3, 3.5]], "2,0.5,4, cover=-1"]
    , [ranges(2,0.5,4,cover=-2) ,  [[3, 3.5,2], [3.5, 2,2.5], [2, 2.5, 3], [2.5, 3, 3.5]], "2,0.5,4, cover=-2"]

    , ""
    , [ ranges(4,1,1),[[4, 3], [3, 2], [2, 4]], "4,1,1"]
    , [ ranges(8,2,0),[[8, 6], [6, 4], [4, 2], [2, 8]] , "8,2,0"]
    , [ ranges(0,3,8),[[0, 3], [3, 6], [6, 0]] , "0,3,8"]
	
    , ""
    , "// Extreme cases: "		
    , [ ranges(), [], "", ["//", "Give nothing, get nothing"]]
    , [ ranges(0),[], "0", ["//", "Count from 0 to 0 gets you nothing"] ]
    , [ ranges(2,2),[], "2,2", ["//", "2 to 2 gets you nothing, either"] ]
    , [ ranges(2,0,4),[], "2,0,4", ["//", "No intv gets you nothing, too"] ]
    , [ ranges(0,1),[[0]] , "0,1"]
    , [ ranges(0,1,1),[[0]] , "0,1,1"]

	, "// When interval > range, count only the first: "
    , [ ranges(2,5,4),[[2]] , "2,5,4"]
    , [ ranges(2,5,-1),[[2]] , "2,5,-1"]
    , [ ranges(-2,5,-4),[[-2]] , "-2,5,-4"]
    , [ ranges(-2,5,1),[[-2]] , "-2,5,1"]

    , ""
    , " ranges( <b>obj</b> )"
    , ""
    , "// ranges( arr ) or ranges( str ) to return a range of an array"
    , "// so you don't have to do : ranges( len(arr) )"
    , ""
    , str("> obj = ",obj)
    , ""
    , [ ranges(obj),[[0, 1], [1, 2], [2, 3], [3, 0]], "obj"]
    , [ ranges([3,4,5]), [[0, 1], [1, 2], [2, 0]], "[3,4,5]"]
    , [ ranges("345"), [[0, 1], [1, 2], [2, 0]]  , "'345'"]
    , [ ranges([]), [] , "[]"]
    , [ ranges(""), [] , "'"]

    , ""
    , " ranges( ... <b>cycle= i,-i,true</b>...)"
    , ""
    , "// cycle= true: extend the index on the right by +i count "
    , "" 
    , [ ranges(4,cycle=false), [[0, 1], [1, 2], [2, 3], [3]], "4,cycle=false"
                , ["//","Add first to the right"] ]
    , [ ranges(2,5,cycle=false), [[2, 3], [3, 4], [4]] , "2,5,cycle=false"]
    , [ ranges(2,1.5,8,cycle=false),
                [[2, 3.5], [3.5, 5], [5, 6.5], [6.5]], "2,1.5,8,cycle=false" ]
    
    , ""
    , " ranges(<b>obj</b>,<b>cycle=true</b>...)"
    , ""
    , str("> obj = ",obj)
    , [ ranges(obj,cycle=true),[[0, 1], [1, 2], [2, 3], [3, 0]], "obj,cycle=true"]
	
    , ""
    , " ranges(... <b>cover</b>=[i,j], n, -n )"
    , ""
    , "// cover=[1,2] means, instead of returning i, return"
    , "//[i-1,i,i+1,i+2]. Note that when cover is set, return array of arrays."
    , "// Also note that w/cover, cycle=+i/-i is treated as cycle=true."

    , [ ranges(3), [[0, 1], [1, 2], [2, 0]] , "3"]
    , [ ranges(3,cover=[0,1],cycle=false)
        , [[0, 1], [1, 2], [2]], "3,cover=[0,1]"]
    , [ ranges(3,cover=[0,1],cycle=true)
        , [[0, 1], [1, 2], [2,0]], "3,cover=[0,1],cycle=true"]
		
		
	, ""
    , [ ranges(3,cover=[0,0]), [[0], [1], [2]], "3,cover=[0,0]"]
    , [ ranges(5,8), [[5, 6], [6, 7], [7, 5]], "5,8"]
    , [ ranges(5,8,cover=[0,0]), [[5], [6], [7]], "5,8,cover=[0,0]"]
//		,["5,8,cover=[1,0]", ranges(5,8,cover=[1,0])
//						, [[5], [5, 6], [6, 7]]]
//		,["5,8,cover=[0,1]", ranges(5,8,cover=[0,1])
//						, [[5, 6], [6, 7], [7]]]
//		,["5,8,cover=[1,0],cycle=true", ranges(5,8,cover=[1,0],cycle=true)
//						, [[7,5], [5, 6], [6, 7]]]
//		,["5,8,cover=[0,2],cycle=true", ranges(5,8,cover=[0,2],cycle=true)
//						, [[5, 6,7], [6, 7, 5], [7,5,6]]]

    , ""
    , [ ranges(0,2,8,cover=[0,1])
                        ,[[0, 2], [2, 4], [4, 6], [6, 0]]
                        , "0,2,8,cover=[0,1]"]
                        
    , [ ranges(0,3,8,cover=[0,1])
                        ,[[0, 3], [3, 6], [6, 0]]
                        , "0,3,8,cover=[0,1]"
                        ]
    , [ ranges(0,2,6,cover=[0,2])
            ,[[0, 2, 4], [2, 4, 0], [4, 0, 2]]
            , "0,2,6,cover=[0,2]", ] 
            

		,""
    , [ ranges(obj),[[0, 1], [1, 2], [2, 3], [3, 0]], "obj"]
	 ,[ ranges(obj,cover=[1,1])
			,[[3, 0, 1], [0, 1, 2], [1, 2, 3], [2, 3, 0]]
            , "obj,cover=[1,1]" ] 

	]

	, mode=mode
	);
}




//========================================================
module repeat_test( mode=112 )
{
	doctest( repeat, 
    [ 
      [ repeat("d"), "dd" , "'d'"]
    , [ repeat("d",5), "ddddd" , "'d',5'"]
    , [ repeat("d",0), "" , "'d',0'"]
    , [ repeat([2,3]), [2,3,2,3] , "[2,3]"]
    , [ repeat([2,3],3), [2,3,2,3,2,3] , "[2,3],3"]
    , [ repeat([[2,3]],3), [[2,3],[2,3],[2,3]] , "[[2,3]],3"]
    , [ repeat([2,3],0), [] , "[2,3],0"]
    , [ repeat(3), "33" , "3"]
    , [ repeat(3,5), "33333", "3,5", ["//","return a string"] ]
	], mode=mode

	);
} 
//multi_test( ["mode",22] ); 
//doc(multi);


//========================================================
module replace_test( mode=112 )
{
	doctest( replace, 
	[
	  [	replace("a_text", "e"), "a_txt", "'a_text', 'e'"]
	, [	replace("a_text", ""), "a_text", "'a_text', '"]
    , [ replace("a_text", "a_t", "A_T"), 	"A_Text", "'a_text', 'a_t', 'A_T'"]
	, [	replace("a_text", "ext", "EXT"), "a_tEXT", "'a_text', 'ext', 'EXT'"]
	, [	replace("a_text", "abc", "EXT"), "a_text", "'a_text', 'abc', 'EXT'"]
	, [	replace("a_text", "t", 	"{t}"), "a_{t}ex{t}", "'a_text', 't',   '{t}'"]
    , [ replace("a_text", "t", 	4), 		"a_4ex4", "'a_text', 't', 4"]
    , [ replace("a_text", "a_text","EXT"), "EXT", "'a_text', 'a_text', 'EXT'"]
    , [	replace("", "e"), "",  "', 'e'"]
	, "# Array # "
    , [ replace([2,3,5], 0, 4), [4,3,5] , "[2,3,5], 0, 4"]
    , [ replace([2,3,5], 1, 4), [2,4,5] , "[2,3,5], 1, 4"]
    , [ replace([2,3,5], 2, 4), [2,3,4] , "[2,3,5], 2, 4"]
    , [ replace([2,3,5], 3, 4), [2,3,5] , "[2,3,5], 3, 4"]
    , [ replace([2,3,5], -1, 4), [2,3,4] , "[2,3,5], -1, 4"]

	 ], mode=mode	
	);
} 		



//replace_test( ["mode",22] );
//doc(replace);


//========================================================
module reverse_test( mode=112 ){

   doctest( reverse,
   [ [ reverse([2,3,4]), [4,3,2], [2,3,4] ]
   , [ reverse([2]), [2], [2] ]
   , [ reverse( "234" ), "432", "'234'"  ]
   , [ reverse( "2" ), "2", "'2'" ]
   , [ reverse( "" ), "", "'"  ]
   ], mode=mode
   );
}
//reverse_test( ["mode",22] );
//doc(reverse);


//========================================================
module ribbonPts_test( mode=112 ){}


//========================================================
module RM_test( mode=112 ){ doctest(RM,[],mode=mode ); }

//RM_test( [["mode",22]] );
//doc(RM);
//module RM_demo()
//{
//	a3 = randPt(r=1)*360;
//	echo("in RM, a3= ",a3);
//	rm = RM(a3);
//	echo(" rm = ", rm);
//}
////RM_demo();


//========================================================
module RMx_test( mode=112 )
{
	doctest( RMx
	,[
	
	],mode=mode );
}
//RMx_test( ["mode",22] );
//doc(RMx);


////========================================================
module RMy_test( mode=112 )
{
	doctest( RMy
	,[
	
	],mode=mode );
}
//RMy_test( ["mode",22] );

//========================================================
module RMz_test( mode=112 )
{
	doctest( RMz
	,[
	
	],mode=mode );
}
//RMz_test( ["mode",22] );
//doc(RMz);

//========================================================


module RMs_test( mode=112 ){}

//========================================================

//doc(RM2z);

//========================================================
module rodfaces_test( mode=112 ){ doctest( rodfaces,[],mode=mode );}                     

//========================================================

//========================================================
module rodPts_test( mode=112 ){ doctest( rodPts,[],mode=mode );} 

//========================================================
module rodsidefaces_test( mode=112 )
{
	doctest( rodsidefaces,
	[
	 [ rodsidefaces(4), 
			[ [0,1,5,4]
			, [1,2,6,5]
			, [2,3,7,6]
			, [3,0,4,7]
			], "4"
	]
	,[ rodsidefaces(5), 
			[ [0, 1, 6, 5]
			, [1, 2, 7, 6]
			, [2, 3, 8, 7]
			, [3, 4, 9, 8]
			, [4, 0, 5, 9]
			], "5"
	 ]
	,[ rodsidefaces(4,twist=1), 
			[ [0,1,4,7]
			, [1,2,5,4]
			, [2,3,6,5]
			, [3,0,7,6]
			], "4,twist=1"
	]
	,[ rodsidefaces(4,twist=-1), 
			[ [0,1,6,5]
			, [1,2,7,6]
			, [2,3,4,7]
			, [3,0,5,4]
			], "4,twist=-1"
	]
	,[ rodsidefaces(4,twist=2), 
			[ [0,1,7,6]
			, [1,2,4,7]
			, [2,3,5,4]
			, [3,0,6,5]
			], "4,twist=2"
	 ]

	], mode=mode
	);
}



//========================================================
module roll_test( mode=112)
{
	doctest( roll,
	[
      [ roll([0,1,2,3]), [3,0,1,2] , "[0,1,2,3]"]
    , [ roll([0,1,2,3],-1), [1,2,3,0] , "[0,1,2,3],-1"]
    , [ roll([0,1,2,3], 2), [2,3,0,1] , "[0,1,2,3], 2"]
    , [ roll([0,1,2,3,4,5], 2), [4,5,0,1,2,3] , "[0,1,2,3,4,5], 2"]
    , [ roll([0,1,2,3],0), [0,1,2,3] , "[0,1,2,3],0"]
    ], mode=mode
	);
}

//========================================================

module rotangles_p2z_test( mode=112 )
{ doctest( rotangles_p2z, mode=mode ); }
//rotangles_p2z_test( ["mode",22] );

//rotangles_p2z_demo();
//doc(rotangles_p2z);



//========================================================

module rotangles_z2p_test( mode=112 )
{ doctest( rotangles_z2p, mode=mode ); }

//rotangles_z2p_test( ["mode",22] );
//rotangles_z2p_demo();



//========================================================
module run_test( mode=112 )
{
   doctest( run,
    [
      [ run("abs",-3), 3 , "'abs', -3"]
    , [ run("abs",[-3]), 3 , "'abs', [-3]"]
    , [ run("sign",-3), -1 , "'sign', -3"]
    , [ run("sin",30), 0.5 , "'sin', 30"]
    , [ run("max",[3,1,4,2]), 4 , "'max', [3,1,4,2]"]
    , [ run("pow",3), 9 , "'pow', 3"]
    , [ run("pow",[3,3]), 27 , "'pow', [3,3]"]
    , [ run("concat",[[3,4],[5,6],[7,8]])   
            , [3,4,5,6,7,8], "'concat', [[3,4],[5,6],[7,8]]"
            ,  ]
    ,""
    ,"// New: you can do: run(['sin, 30])"
    ,"//      instead of run(sin, 30)"
    ,""
    , [ run(["sin",30]), 0.5 , ["sin", 30]]
        
    ], mode=mode


    );
}    
//========================================================
module _s_test( mode=112 )
{
	doctest( _s,
    [ 
      [ _s("2014.{_}.{_}", [3,6]), "2014.3.6", "'2014.{_}.{_}', [3,6]"]
    , [ _s("", [3,6]), "", "', [3,6]"]
    , [ _s("2014.{_}.{_}", []), "2014.{_}.{_}", "'2014.{_}.{_}', []"]
    , [ _s("2014.{_}.{_}", [3]), "2014.3.{_}", "'2014.{_}.{_}', [3]"]
    , [ _s("2014.3.6", [4,5]), "2014.3.6", "'2014.3.6', [4,5]"]
	, [ _s("{_} car {_} seats?", ["red","blue"]), "red car blue seats?"
        , "'{_} car {_} seats?', ['red','blue']"
		]	 
	], mode=mode

	);
}
//s__test( ["mode",22] );


//========================================================
module shift_test( mode=112 )
{
	doctest( shift ,
    [
      [ shift([2,3,4]), [2,3] , "[2,3,4]"]
    , [ shift([2]), [] , "[2]" ]
    , [ shift([]), [] , "[]" ]
    , [ shift( "234" ), "23"  , "'234'"]
    , [ shift( "2" ), ""  , "'2'"]
    , [ shift( "" ), ""  , "'"]
	],mode=mode

	);
}
//shift_test( ["mode",22] );

//========================================================
module shortside_test( mode=112 )
{ doctest( shortside, 
    [
	  [ shortside(5,4), 3, "5,4" ]
	, [ shortside(5,3), 4, "5,3" ]
	], mode=mode
	);
}



//========================================================
module shrink_test( mode=112 )
{
	doctest( shrink, 
	[
      [ shrink([2,3,4]), [3,4], [2,3,4] ]
    , [ shrink([2]), [], [2] ]
    , [ shrink( "234" ), "34",  "'234'" ]
    , [ shrink( "2" ), "", "'2'" ]
    , [ shrink( "" ), "", "'" ]
    ], mode=mode
	);
}
//shrink_test( ["mode",22] );

//========================================================
module shuffle_test( mode=112 )
{
	arr = range(6);
	arr2= [ [-1,-2], [2,3],[5,7], [9,10]] ;
	doctest( shuffle,
	[
		str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr2)= ", shuffle(arr2) )
		,str( "shuffle(arr2)= ", shuffle(arr2) )
		,str( "shuffle(arr2)= ", shuffle(arr2) )
	], mode=mode, scope=["arr", arr, "arr2", arr2]
	);
}
//shuffle_test();

//========================================================

module sinpqr_test( mode=112 ){ doctest( sinpqr,[],mode=mode );}

//========================================================
module slice_test( mode=112 )
{
	s="012345";
	a=[10,11,12,13,14,15];
	doctest( slice, 
	[
	  "## slice a string ##"
	  
	, [ slice(s,2),"2345", "'012345',2"]		 
    , [ slice(s,2,4),"23", "'012345',2,4"]
	, [ slice(s,0,2),"01", "'012345',0,2"]		 
	, [ slice(s,-2),"45","'012345',-2"]		 
	, [ slice(s,1,-1),"1234","'012345',1,-1"]		 
	, [ slice(s,-3,-1),"34","'012345',-3,-1"]		 
	, [ slice(s,3,3),"", "'012345',3,3"]		 
	, [ slice("",3),"","',3"]		 
	  
	, "## slice an array ##" 	
    , [ slice(a, 2), [12,13,14,15], "[10,11,12,13,14,15],2"]
    , [ slice(a, 2,5), [12,13,14], "[10,11,12,13,14,15],2,5"]
    , [ slice(a, 2,20), [12,13,14,15], "[10,11,12,13,14,15],2,20"
            , ["//", "When out of range"]]
    , [ slice(a, -3), [13,14,15], "[10,11,12,13,14,15],-3"]
    , [ slice(a, -20, -3), [10,11,12], "[10,11,12,13,14,15],-20,-3"
            , ["//", "When out of range"]]
    , [ slice(a, -8, -3), [10,11,12], "[10,11,12,13,14,15],-8,-3"
            , , ["//", "When out of range"]]
    , [ slice([[1,2],[3,4],[5,6]], 2), [[5,6]],"[[1,2],[3,4],[5,6]],2"]
    , [ slice([[1,2],[3,4],[5,6]], 1,1), [], "[[1,2],[3,4],[5,6]], 1,1"]
    , [ slice([], 1), [], "[], 1"]
    , [ slice([9], 1), [], "[9], 1"]

    ,""
	,"// Reversed slicing:"
	,""
    , [ slice(a, 2,5), [12,13,14], "[10,11,12,13,14,15], 2,5"]
    , [ slice(a, 5,2), [15,14,13], "[10,11,12,13,14,15], 5,2"]
    , [ slice(s, 5,2), "543", "'012345', 5,2"]
    , [ slice(s, 3,1), "32", "'012345', 3,1"]
	  
    , [ slice(s, -3,-5), "32", "'012345', -3,-5"]
	] , mode=mode
	);
}
//slice_test( ["mode",22] );

//========================================================
module slope_test( mode=112 )
{	
    doctest( slope, 
     [ 
        [ slope([0,0],[4,2]), 0.5,[[0,0],[4,2]] ]
     ,  [ slope( [[0,0],[4,2]] ), 0.5,[ [[0,0],[4,2]] ] ]
     ,  [ slope( [[0,0],[4,2]] ), 0.5,[ [[1,1],[4,2]] ] ]
     ],mode=mode
    // , [["echo_level", 1]]	
    );
}

//slope_test( ["mode",22] );

//========================================================
module split_test( mode=112 )
{
	doctest( split,
    [ 
      [ split("this is it"), ["this","is","it"], "'this is it'"]
    , [ split("this is it","i"), ["th","s ","s ","t"], "'this is it','i'"]
    , [ split("this is it","t"), ["his is i"], "'this is it','t'"]
    , [ split("this is it","is"), ["th"," "," it"], "'this is it','is'"]
    , [ split("this is it",","), ["this is it"], "'this is it',','"]
    , [ split("Scadx",","), ["Scadx"], "'Scadx',','"]
    , [ split("Scadx","Scadx"), [], "'Scadx','Scadx'"]
	,  "// 2015.2.11: New arg keep"
    , [ split("abcabc","b"), ["a","ca","c"], "'abcabc','b'"]
    , [ split("abcabc","b",true), ["a","b","ca","b","c"], "'abcabc','b',keep=true"]
    , [ split("abcabc","a",true), ["a","bc","a","bc"], "'abcabc','a',keep=true"]
    , [ split("abcabc","c",true), ["ab","c","ab","c"], "'abcabc','c',keep=true"]
    ,  "// 2015.3.2: New arg keepempty=false "
    , [ split("ttt","t"), [] , "'ttt'"]
    , [ split("ttt","t", keepempty=true), ["",""] , "'ttt', keepempty=true"]
    , [ split(",t",","), ["t"] , "',t',','"]
    , [ split(",t",",", keepempty=true), ["","t"] , "',t',',', keepempty=true"]
    , [ split("",","), [], "',','"]
    , [ split("",",", keepempty=true), [""], "',',', keepempty=true"]
	], mode=mode
	);
}



//========================================================
module splits_test( mode=112 )
{
	//s="this is it";
	s="abc_def_ghi";
	doctest( splits,
	[ 
      [ splits(s,["b"]), ["a", "c_def_ghi"], "s,['b']"]
	, [ splits(s,["b","_"]), ["a", "c","def", "ghi"],"s1,['b','_']"]	
	, [ splits(s,["_","b"]), ["a", "c","def", "ghi"], "s1,['_','b']"]	
    , [ splits(s,["b","_","e"]), ["a", "c","d","f", "ghi"], "s1,['b','_','e']"]
    , "// New 2015.2.11: argument keep"
    , [ splits("abcabc",["b","c"]), ["a","a"], "'abcabc',['b','c']"]
    , [ splits("abcabc",["c","b"],true)
            , ["a","b","c","a","b","c"], "'abcabc',['c','b'],keep=true"]
    , [ splits("abcabc",["b","c"],true)
            , ["a","b","c","a","b","c"], "'abcabc',['b','c'],keep=true"]
    , "// New 2015.3.2: argument keepempty"
    , [ splits("abcabc",["b","c"],keepempty=true), ["a","","a"], 
            "'abcabc',['b','c'],keepempty=true"
       ]
	], mode=mode, scope=["s",s]

	);
}


//========================================================


module squarePts_test( mode=112 )
{
	pqr = randPts(3);
	sqs = squarePts( pqr );
	
    doctest( squarePts, 
	[
	  [ is90( [for(i=[0,1,2]) sqs[i] ] ) , true, "pqr"
		, ["myfunc", "is90( [ for(i=[0,1,2]) squarePts{_}[i] ] ) "]
	  ]

	  ,[ is90( [for(i=[1,2,3]) sqs[i] ] ), true, "pqr", 
		, ["myfunc", "is90( [ for(i=[1,2,3]) squarePts{_}[i] ] )"]
	  ]

	,[ is90( [for(i=[2,3,0]) sqs[i] ] ), true, "pqr", 
		, ["myfunc", "is90( [ for(i=2,3,0]) squarePts{_}[i] ] )"]
	  ]
	], mode=mode
     );
}

module subops_test( ops=[] )
{
    /*
       Let an obj has 2 sub unit groups: arms and legs, each has L and R
       Obj: arms{armL,armR}
          : legs{legL,legR}
       
       com_keys for arms: ["color","r"]
       com_keys for legs: ["color","len"]
       
       
    
    
    */
    //=========================================
    //=========================================
    
    module tester( 
    // The following are to set defaults on the fly
    //   for checking how different default settings work.
    //   I.e., in real use, don't do it this way.
    // Usage: as a test rec: 
    //   [
              u_ops
              , df_color = undef
              , df_r     = undef
              , df_len   = undef
              , df_show_arms = undef 
              , df_show_armL = undef 
              , df_show_armR = undef
              , df_show_legs = undef
              , df_show_legL = undef
              , df_show_legR = undef 
              
              , df_arms = undef 
              , df_armL = undef 
              , df_armR = undef
              , df_legs = undef
              , df_legL = undef
              , df_legR = undef 
              
              , com_keys_arms= ["color","r"]
              , com_keys_legs= ["color","len"]
    ){
        df_ops= updates(
            [
              df_color==undef? []:["color", df_color]
            , df_r    ==undef? []:["r",     df_r]
            , df_len  ==undef? []:["len",   df_len]
        
            , df_show_arms ==undef?[]:["df_arms", df_show_arms]
            , df_show_armL ==undef?[]:["df_armL", df_show_armL]
            , df_show_armR ==undef?[]:["df_armR", df_show_armR]
            , df_show_legs ==undef?[]:["df_legs", df_show_legs]
            , df_show_legL ==undef?[]:["df_legL", df_show_legL]
            , df_show_legR ==undef?[]:["df_legR", df_show_legR]
            ]
        );
        
        function get_ops( df_subs, com_keys )=
            subops( u_ops
                  , df_ops = df_ops
                  , df_subs= df_subs
                  , com_keys=com_keys
                  );
        function get_arm_ops( armname, df)=
            get_ops( df_subs= [ "arms", df_arms, armname,df]
                   , com_keys= com_keys_arms
                   );
        function get_leg_ops( legname, df)=
            get_ops( df_subs= [ "legs", df_legs, legname,df]
                   , com_keys= com_keys_legs
                   );
                   
        // We need to set 4 ops:
        
        ops_armL = get_arm_ops( "armL", df_armL );
        ops_armR = get_arm_ops( "armR", df_armR );
        ops_legL = get_leg_ops( "legL", df_legL );
        ops_legR = get_leg_ops( "legR", df_legR );
            
        
    }
//    
//    opt=[];
//    df_arms=["len",3,"r",1];
//    df_armL=["fn",5];
//    df_armR=[];
//    df_ops= [ "len",2
//            , "arms", true
//            , "armL", true
//            , "armR", true
//            ];
//    com_keys= ["fn"];
//    
//    /*
//        setops: To simulate obj( ops=[...] )
//        args  : ops  
//                // In real use, following args are set inside obj()
//                df_ops
//                df_subops // like ["arms",[...], "armL",[...]] 
//                com_keys 
//                objname   // like "obj","armL","armR"          
//                propname  // like "len"
//    */
//    function setops(   
//            ops=opt
//            , df_ops=df_ops
//            , df_subops=[ "arms",df_arms, "armL",df_armL,"armR",df_armR]
//            , com_keys = com_keys
//            , objname ="obj"
//            , propname=undef
//            )=
//        let(
//             _ops= update( df_ops, ops )
//           , df_arms= hash(df_subops, "arms")  
//           , df_armL= hash(df_subops, "armL")  
//           , df_armR= hash(df_subops, "armR")  
//           , ops_armL= subops( _ops, df_ops = df_ops
//                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]                    
//                    , com_keys= com_keys
//                    )
//            , ops_armR= subops( _ops, df_ops = df_ops
//                        , sub_dfs= ["arms", df_arms, "armR", df_armR ]
//                        , com_keys= com_keys
//                        )
//            , ops= objname=="obj"?_ops
//                    :objname=="df_ops"?df_ops
//                    :objname=="armL"?ops_armL
//                    :ops_armR
//            )
//        // ["ops",_ops,"df_arms", df_arms, "ops_armL",ops_armL, "ops_armR",ops_armR];
//        propname==undef? ops:hash(ops, propname);
//      
//     /**
//        Debugging Arrow2(), in which a getsubops call results in the warning:
//        DEPRECATED: Using ranges of the form [begin:end] with begin value
//        greater than the end value is deprecated.
//            
//     */       
//     function debug_Arrow2(ops=["r", 0.1, "heads", false, "color","red"])=
//        let (
//        
//           df_ops=[ "r",0.02
//               , "fn", $fn
//               , "heads",true
//               , "headP",true
//               , "headQ",true
//               , "debug",false
//               , "twist", 0
//               ]
//          ,_ops= update(df_ops,ops) 
//          ,df_heads= ["r", 1, "len", 2, "fn",$fn]    
//          , ops_headP= subops( _ops, df_ops=df_ops
//                , com_keys= [ "color","transp","fn"]
//                , sub_dfs= ["heads", df_heads, "headP", [] ]
//                ) 
//        )
//        ops_headP;
//        
//     doctest(subops
//        , [ ""
//            ,"In above settings, ops is the user input, the rest are"
//            ,"set at design time. With these:"
//            ,""
//            ,["df_ops"
//                 , setops(objname="df_ops")
//                 , ["len",2,"arms",true,"armL", true, "armR", true]]
//            ,["ops"
//                 , setops()
//                 , ["len",2,"arms",true,"armL", true, "armR", true]]
//            ,["ops_armL"
//                 , setops(objname="armL")
//                 , ["len", 3, "r", 1, "fn", 5]]
//            ,["ops_armR"
//                 , setops(objname="armR")
//                 , [ "len", 3, "r", 1]]
//                            
//            ,""
//            ,"Set r=4 at the obj level. Since r doesn't show up in com_keys, "
//            ,"it only applies to obj, but not either arm. "
//            ,""
//            
//            ,["df_ops"
//                 , setops(objname="df_ops")
//                 , ["len",2,"arms",true,"armL", true, "armR", true],]
//            ,["ops"
//                 , setops(ops=["r",4])
//                 , ["len",2,"arms",true,"armL", true, "armR", true,"r",4],]
//            ,["ops_armL"
//                 , setops(ops=["r",4],objname="armL")
//                 , ["len", 3, "r", 1,"fn", 5]]
//            ,["ops_armR"
//                 , setops(ops=["r",4],objname="armR")
//                 , [ "len", 3, "r", 1]]
//                            
//            ,""
//            ,"Set fn=10 at the obj level. Since fn IS in com_keys, "
//            ,"it applies to obj and both arms. "
//            ,""
//            
//            ,["df_ops"
//                 , setops(objname="df_ops")
//                 , ["len",2,"arms",true,"armL", true, "armR", true],]
//            ,["ops"
//                 , setops(ops=["fn",10])
//                 , ["len",2,"arms",true,"armL", true, "armR", true, "fn",10],]
//            ,["ops_armL"
//                 , setops(ops=["fn",10],objname="armL")
//                 , ["len", 3, "r", 1, "fn", 10]]
//            ,["ops_armR"
//                 , setops(ops=["fn",10],objname="armR")
//                 , ["len", 3, "r", 1, "fn", 10]]  
//                 
//        
//            ,""
//            ,"Disable armL:"
//            ,""
//            
//            ,["df_ops"
//                 , setops(objname="df_ops")
//                 , ["len",2,"arms",true,"armL", true, "armR", true]]
//            ,["ops"
//                 , setops(ops=["armL",false])
//                 , ["len",2,"arms",true,"armL", false, "armR", true]]
//            ,["ops_armL"
//                 , setops(ops=["armL",false],objname="armL")
//                 , false]
//            ,["ops_armR"
//                 , setops(ops=["armL",false],objname="armR")
//                 , ["len", 3, "r", 1]]
//
//
//            ,""
//            ,"Disable both arms:"
//            ,""
//            
//            ,["df_ops"
//                 , setops(objname="df_ops")
//                 , ["len",2,"arms",true,"armL", true, "armR", true]]
//            ,["ops"
//                 , setops(ops=["arms",false])
//                 , ["len",2,"arms",false,"armL", true, "armR", true]]
//            ,["ops_armL"
//                 , setops(ops=["arms",false],objname="armL")
//                 , false]
//            ,["ops_armR"
//                 , setops(ops=["arms",false],objname="armR")
//                 , false]
//        
//        
////            ,""
////            ,"Debugging Arrow2(), in which a getsubops call results in the warning:"
////            ,"DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated."
////            ,""
////            
////            ,["debug_Arrow2(ops=['r', 0.1, 'heads', false, 'color','red']"
////             , debug_Arrow2(ops=["r", 0.1, "heads", false, "color","red"]),[]
////             ]
//                            
//          ]
//          
//        , ops, [ "df_ops",df_ops, 
//               , "df_arms", df_arms
//               , "df_armL", df_armL
//               , "df_armR", df_armR
//               , "com_keys", com_keys
//               , "ops",opt
//               ]
//    );
}
//========================================================
module sumto_test( mode=112 )
{
	doctest( sumto, 
	[ [ sumto(5), 15, "5" ]
    , [ sumto(6), 21, "6" ]
	],mode=mode
	);
}
//sumto_test( ["mode", 22 ] );

//========================================================
module sum_test( mode=112 )
{
    doctest( sum, 
    [
      [ sum([2,3,4,5]),14, [2,3,4,5]]
    ], mode=mode );
}
//sum_test( ["mode",22] );

//========================================================
module switch_test( mode=112 )
{
	arr=[2,3,4,5,6];
	doctest( switch,
	[		
      [ switch(arr,1,2), [2,4,3,5,6] , "arr,1,2"]
    , [ switch(arr,0,2), [4,3,2,5,6] , "arr,0,2"]
    , [ switch(arr,0,-1), [6,3,4,5,2] , "arr,0,-1"]
    , [ switch(arr,-1,-5), [6,3,4,5,2] , "arr,-1,-5"]
    , [ switch(arr,-1,4), [2,3,4,5,6], "arr,-1,4", ["//","No switch"] ]

	],mode=mode, ["arr", arr]

	);
}


////========================================================
//module subarr_test(mode=112)
//{
//	arr=[10,11,12,13];
//	doctest( subarr,
//	[
//		 ["arr", subarr( arr ), [[13,10,11],[10, 11,12],[11,12,13],[12,13,10]] ]
//		,["arr,cycle=false", subarr( arr,cycle=false ), [[10,11],[10, 11,12],[11,12,13],[12,13]] ]
//
//		,["arr,cover=[0,1]", subarr( arr,cover=[0,1] ), [[10,11],[11,12],[12,13],[13,10]] ]
//		,["arr,cover=[2,0]", subarr( arr,cover=[2,0] ), [[12,13,10],[13,10,11],[10,11,12],[11,12,13]] ]
//	],mode=mode, ["arr",arr]
//	);
//}


//========================================================
module symmedianPt_test( mode=112 ){doctest( symmedianPt,[],mode=mode );}

//========================================================
module transpose_test( mode=112 )
{
	mm = [[1,2,3],[4,5,6]];
	mm2= [[1,4],[2,5],[3,6]];
	doctest
	( 
		transpose
	,	[
			[  transpose(mm), mm2, mm ]
		,	[  transpose(mm2), mm, mm2 ]
		], mode=mode
	);
}

//transpose_test( ["mode",22] );

//========================================================
module trim_test( mode=112)
{
    doctest( trim,
    [
     [ trim("abcxyz",l="abc"), "xyz", "'abcxyz'"]
    , [ trim("bcaxyz",l="abc"), "xyz", "'bcaxyz'"]
    , [ trim("cabxyz",l="abc"), "xyz", "'cabxyz'"]
    , [ trim("  abc "), "abc", "'  abc '"]
    , [ trim(",abc!", l=",", r="!" ), "abc", "',abc!',l=',',r='!'"]
    , [ trim(", abc! . ", l=" ,", r="!. " ), "abc", "', abc! . ', l=' ,', r='!. '"]
    , [ trim([" a, ", ", b"], l=" ,.", r=" ,."), ["a","b"], "['a,',',b'], l=',.', r=' ,.'" ] 
    , [ trim(["# a, ", 3, undef, true], l=" #", r=", ."), ["a",3,undef,true]
        , "['# a, ', 3, undef, true], l=' #', r=', .'"] 
    ],mode=mode );
}    

//========================================================
module twistangle_test(mode=112)
{
	doctest( twistangle,
	mode=mode
	);
}
//module twistangle_demo()
//{	
//	pqrs = randPts(4);
//	P=P(pqrs); Q=Q(pqrs); R=R(pqrs);
//	S=pqrs[3];
//	
//	MarkPts([P,Q,R,S],["labels","PQRS"]);
//	Chain( [P,Q,R,S ] );
//	//Line0( [Q,S] );
//
//	echo( "quad:", quadrant( [Q,R,P],S), "twistangle:", twistangle( [P,Q,R, S] ));
//
//}
//twistangle_demo();



//========================================================
module type_test( mode=112 )
{
	doctest( type,
    [  
     [ type([2,3]), "arr" , [2,3]]
    , [ type([]), "arr" , "[]"]
    , [ type(-4), "int", "-4"]
    , [ type( 0 ), "int", "0"]
    , [ type( 0.3 ), "float", "0.3"]
    , [ type( -0.3 ), "float", "-0.3"]
    , [ type("a"), "str", "'a'"]
    , [ type("10"), "str", "'10'"]
    , [ type(true), "bool", "true"]
    , [ type(false), "bool", "false"]
    , [ type(undef), undef, "undef"]
    	 ],mode=mode

	);
}
//type_test( ["mode",22] );


module typeplp_test( mode=112 ){ //=================
    P=[2,3,4];
    Q=[5,6,7];
    R=[3,5,7];
    
    doctest( typeplp,
    [
      [typeplp( P ), "pt", "P"]
    , [typeplp( [P,Q] ), "ln", "[P,Q]"]
    , [typeplp( [P,Q,R] ), "pl", "[P,Q,R]"]
    , [typeplp( "a" ), false, "'a'"]
    , [typeplp( [P,Q,R,P] ), false, "[P,Q,R,P]"]
    ], mode=mode, scope=["P",P,"Q",Q,"R",R]
    );
}

//========================================================
module ucase_test( mode=112 )
{
	doctest( ucase,
	[
		[ucase("abc,Xyz"), "ABC,XYZ", "'abc,Xyz'", ]
	], mode=mode
	);
}

//========================================================
module uconv_test( mode=112)
{
	doctest( uconv,
	[
     [ uconv(3,"cm,in"), 3/2.54 , "3,'cm,in'"]
    , [ uconv(100,"cm,ft"), 100/2.54/12, "100, 'cm,ft'"]
	  ,"// Use own units:"
    , [ uconv(100,"cm,aaa"), undef, "100, 'cm,aaa'"]
    , [ uconv(100,"cm,aaa", ["cm,aaa",2]), 200, ["//","Use your factor"], "100, 'cm,aaa', ['cm,aaa',2]"]
	  ,"// Simply set units= the conv factor: "
    , [ uconv(100,utable=3), 300, "100, utable=3", ["//","Enter a conv factor"]]
	   
     ,"// When n is a string like 2'8', arg *from* is ignored:"
    , [ uconv("1'3\"", ",in"), 15, "'1'3', 'in'"]
    , [ uconv("1'3\"", ",cm"), 15*2.54, "'1'3', 'cm'"]
    , [ uconv("1'3\"", ",ft"), 15/12, "'1'3', 'ft'"]
    , [ uconv("1'3\"", ",aaa"), undef, "'1'3', 'aaa'"]
 
	],mode=mode );
} 

//========================================================

module update_test( mode=112 )
{
    a1= ["a",1,"b",2,"c",3]; 
	doctest( update,
	[
      [ update( [1,11],[2,22] ), [1,11,2,22]  , "[1,11],[2,22]"]
    , [ update( [1,11,2,22], [2,33] ), [1,11,2,33]  , "[1,11,2,22],[2,33]"]
    , [ update([1,11,2,22],[1,3,3,33]), [1,3,2,22,3,33], "[1,11,2,22],[1,3,3,33]"]
    , [ update([1,11,2,22],[]), [1,11,2,22], "[1,11,2,22],[]"]
	 , ""
	 , " Clone a hash: "
	 , ""
    , [ update( [],[3,33] ), [3,33]  , "[],[3,33]"]
	 , ""
	 , " Show that the order of h2 doesn't matter"
	 , ""
     , str("a1 = ", a1)
	 , [ update( a1,["b",22,"c",33] )
		, ["a",1,"b",22,"c",33], "a1, ['b',22,'c',33]"
		 ]
	 , [ update( a1, ["c",33,"b",22] )
		,["a",1,"b",22,"c",33], "a1, ['c',33,'b',22]"
		]
	 ], mode=mode 
	);
}

//update_test( ["mode",2]);

//========================================================
module updates_test( mode=112 )
{ 
    a1= [1,11,2,22];
    a2= [1,3,3,33];
    a3= ["a",1,"b",2,"c",3];
    
	doctest( updates	  
	,[
       str("a1=", a1)
	  ,str("a2=", a2)
	  ,[ updates( a1,[[2,20],a2] )
		, [1, 3, 2, 20, 3, 33] 
        , "a1,[[2,20],a2]"
	   ]	 
	 , [ updates( a1,[[1,3,3,33],[2,20]] )
		, [1, 3, 2, 20, 3, 33] 
        , "a1,[a2,[2,20]]"
		
 	   ]	 
	 , [ updates( a1,[[1,3,3,33],[],[2,20]] )
		, [1, 3, 2, 20, 3, 33]
        , "a1, [a2,[],[2,20]]"
	   ]	 
	 , [ updates( a1,[[1,3,3,33,2,20]] )
		, [1, 3, 2, 20, 3, 33] 
        , "a1, [[1,3,3,33,2,20]]"
	   ]	 
	 , [ updates( a1,[[1,3,3,33],[2,20],[2,12,4,40]] )
		, [1,3, 2,12, 3,33, 4,40] 
        ,"a1,[a2,[2,20],[2,12,4,40]]"
	   ]
    , [ updates( [],[[1,3,3,33]] )
		, [1,3,  3,33] 
        , "[],[a2]"
	   ]
    , [ updates( [],[[1,3,3,33],[2,20]] )
		, [1,3, 3,33, 2,20] 
        , "[],[a2,[2,20]]"
	   ]		 
    , [ updates( [],[[1,3,3,33],[2,20],[2,12,4,40]] )
		, [1,3, 3,33, 2,12, 4,40] 
        , "[],[a2,[2,20],[2,12,4,40]]"
	   ]		
     ,""
     ,"New 2015.2.1: enter (h,hs) as a one-layer array: updates(hs). That is, "
     ," updates the hs[0] with the rest in hs"
     ,""
     , str("a3=", a3)
	 , [updates( [a3,["a",11,"d",4],["c",33,"e",5] ])
        ,["a",11,"b",2,"c",33,"d",4,"e",5]
        ,"[ a3, ['a',11,'d',4],['c',33,'e',5] ]", ["nl",true]
       ] 
     , [updates( [a3,[],["c",33,"e",5] ])
        ,["a",1,"b",2,"c",33,"e",5]
        //,[a3,[],["c",33,"e",5] ]
        ,"[ a3,[],['c',33,'e',5] ]", ["nl",true]
       ] 
     , [updates( [[],["a",11,"d",4],["c",33,"e",5] ])
        ,["a",11,"d",4,"c",33,"e",5]
        ,[[],["a",11,"d",4],["c",33,"e",5] ], ["nl",true]
        //, "[[], ['a',11,'d',4],['c',33,'e',5] ]"
       ]     
	 ], mode=mode
	);
}
//updates_test( ["mode",22]);

//========================================================
module vals_test( mode=112 )
{
	doctest( 
	vals, [
		[ vals(["a",1,"b",2]), [1,2], "['a',1,'b',2]" ]
	  ,	[ vals([1,2,3,4]), [2,4], [1,2,3,4] ]
	  ,	[ vals([[1,2],3, 0,[3,4]]), [3,[3,4]],[[1,2],3, 0,[3,4]] ]
	  ,	[ vals([]), [],[] ]
	  ], mode=mode
	//, [["echo_level",1]]
	);
}
//vals_test( ["mode",22] );


//======================================
module vlinePt_test( mode=112 )
{
	doctest( vlinePt, mode=mode );
}


//======================================
lineRotation=[ "lineRotation", "pq,p2", "?", "Angle"
, " The rotation needed for a line
;; on X-axis to align with line-p1-p2
"];

// The rotation needed for a line
// on X-axis to align with line-p1-p2
// 
function lineRotation(p1,p2)=
(
 [ 0,-sign(p2[2]-p1[2])*atan( abs((p2[2]-p1[2])
		                    /slopelen((p2[0]-p1[0]),(p2[1]-p1[1]))))
    , p2[0]==p1[0]?0
	  :sign(p2[1]-p1[1])*atan( abs((p2[1]-p1[1])/(p2[0]-p1[0])))]
);
  

//===========================================
//
//           Version / History
//
//===========================================

            
scadx_core_test_ver=[
["20150207-1", "Separated from original scadx_core.scad"]
,["20150420-1", "Revised all to fit new doctest.scad. Time to run all tests at mode 112: 3 min"]
];

SCADX_CORE_TEST_VERSION = last(scadx_core_test_ver)[0];

//echo_( "<code>||.........  <b>scadx core test version: {_}</b>  .........||</code>",[SCADX_CORE_TEST_VERSION]);
//echo_( "<code>||Last update: {_} ||</code>",[join(shrink(scadx_core_test_ver[0])," | ")]);    


    
    