include <scadx.scad>
include <doctest.scad>

//------------------------------------------------------------------
// 203h: We pick up scax_cutlist.scadx coded in 1917 and re-visit
//
// 1917 is good for single stock, but for multiple stock, it didn't
// fill up the smaller one(s) first, which should be investigated. 
//------------------------------------------------------------------
/* 
   203h: revisit terminology:
    
   stock: [ [x,y],[w,h] ]
   cut  : [ [x,y],[w,h] ]
   
   where: [x,y] = cutpoint
          [w,h] = size
        
   stocksize: [[w,h,n],...]   n = count of this size
   parts:     [[w,h,n],...]   n = count of this size
         
   // 19142: srcWeight:
   //        if stocksize is [x,y,n] but not [x,y], n is the weighting 
   //        factor coming from source stock (the very original one(s))
   //        applied to it's fitness. This allows specific piece of source
   //        stock be filled first.  


*/


ISLOG=1;

function cl2d_makecut_203h( stocks // [ [0,2], [4,1] 
                                   // , [1,0], [3,3] 
                                   // ] 
                     , cut  // [ [1,0], [2,1] ]  = [cutpt, cutsize]
                     , _i
                     )=
( 
  // Use new cl2d_restock/cl2d_restocks(). Great success !!
  // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]
  
  //echo( cl2d_makecut_203h( [ [0,2], [4,1], [1,0], [3,3]], [[1,0],[2,1]] ));
  //
  //     +-------------------+     
  //     |    :         :    |  1  
  //     +----+ - - - - : - -+     
  //   [0,2]  |         :    |     
  //          +---------+    |  
  //        [1,1]       |    | 
  //                    +----+  
  //                 [3,0]
  //
  // Return: [[0, 2], [4, 1], [1, 1], [3, 2, 1], [3, 0], [1, 3, 1]]
 
    
  let(ISLOG=ISLOG)
  ISLOG? 
    echo(log_b( str( "cl2d_markcut_203h"
                   , _ss("(stocks={_:b;bc=lightgreen}, cut={_:c=red}: {_:b;bc=lightgreen})"
                   , [ str(stocks),_i==undef?"":_i, str(cut) ]
        )), 2) ) 
        
    let( _stocks   = cl2d_sort_by_coordX( stocks )
       , newstocks = cl2d_restocks( stocks, cut ) 
       )
       echo(log_e( ["newstocks", newstocks], 2))         
       newstocks
       
  : let( _stocks   = cl2d_sort_by_coordX( stocks )
       , newstocks = cl2d_restocks( stocks, cut ) 
       )
       newstocks
);    


function cl2d_fitness_203h( stocksize // [x,y] 
                     , cutsize   // [x,y]  
                     , LF // 203h: Landscape Factor, default = len/width     
                     )=
(/* Check how well a cut of cutsize fits to the stocksize 
  Return an array [a,b]
  
  where a,b are fitness for :
    (a) cut piece is as it is (=f0) or 
    (b) is rotated by 90 degree (=fr)
    
  The larger a or b is, the better fit.   
                        P     Q
           sy +---------+-----+
        a     |               |
           cy +---------+     + R
              |        O|     |  
        b     |         |     |
              |         |     |
              +---------+-----+
                        cx    sx
                   c       d
                                   
   The init fitness f0 is defined as:
          
      f0= 1-((sx-cx)/sx) * ((sy-cy)/sy)
        = 1- a*d/(a+b)/(c+d)
        ( = 1- area OPQR ) 
        
   The larger the f0, the better fit of cut 
       
   if oversized ( cx>sx or cy>sy ): undef
  
 */
 //let(ISLOG=ISLOG)
 //ISLOG?
   echo(log_b( str( "cl2d_fitness_203h"
                  , _ss(" ( cutsize={_:b}, LF={_:b}, stocksiz={_:b;c=blue} )"
                       ,[cutsize, LF, stocksize])
                  ), 4) )
   //:echo()
 let( sx = stocksize.x
    , sy = stocksize.y
    , cx = cutsize.x 
    , cy = cutsize.y 
    
    // Exclude oversized stocks --- there shouldn't have any fitness
    // if the cut is larger than stock.
    //  
    // To avoid rounding error, need to use isequal() but not just:
    //    , rtn =  [ sx<cx || sy<cy ? undef: f0*L  
    //             , sx<cy || sy<cx ? undef: fr*L  
    //             ] 
    , size_fits=[ (isequal(sx,cx)||sx>cx) && (isequal(sy,cy)||sy>cy)
                , (isequal(sx,cy)||sx>cy) && (isequal(sy,cx)||sy>cx)
                ]    
    
    // The following criteria puts case A ahead of B, which is not what we want:
    //
    // , f0= (1-(sx-cx)/sx) + (1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx) + (1-(sy-cx)/sy)
    //
    // or
    //
    // , f0= (1-(sx-cx)/sx)*(1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx)*(1-(sy-cx)/sy)
    //
    // or
    //
    // , _f0= (cx/sx) * cy/sy 
    // , _fr= (cy/sx) * cx/sy 
    //
    //        A             B   
    //   +-----+    
    //   +--+  |    +--+-------------+    
    //   |  |  |    |  |             |    
    //   |  |  |    |  |             |    
    //   +--+--+    +--+-------------+   
    
    , _f0 = 1-((sx-cx)/sx) * ((sy-cy)/sy)  
    , _fr = 1-((sx-cy)/sx) * ((sy-cx)/sy) 
    
    // 1914: To make a long piece be filled first, 
    //       introduce a "landscape ratio" for the stock:
    //
    //    +-----------------+
    //  w |                 |
    //    +-----------------+
    //             l
    //  L = l/w
    //
    // THIS IS VERY CRITICAL.
    //
    , L = stocksize.x/stocksize.y 
        
    // 203h: srcStockWeight:
    //        if stocksize is [x,y,n] but not [x,y], n is the weighting 
    //        factor coming from source stock (the very original one(s))
    //        applied to it's fitness. This allows specific piece of source
    //        stock be filled first.  
    , srcWeight= und(stocksize[2],1) 
    
    //, f0=_f0*LF * srcWeight
    //, fr=_fr*LF * srcWeight 
    
    , f0= (_f0 * srcWeight + L*und(LF,1))// : undef
    , fr= (_fr * srcWeight + L*und(LF,1))// : undef
    
    //    , rtn =  [ (isequal(sx,cx)||sx>cx) && (isequal(sy,cy)||sy>cy)? f0:undef  
    //             , (isequal(sx,cy)||sx>cy) && (isequal(sy,cx)||sy>cx)? fr:undef  
    //             , LF
    //             ]

    // 203h: we replace the original rtn, which is [f0,f1] as the fitness for
    //       "asis" and "rotated", respectively, to [f,r,LF] where f is the 
    //       max between f0 and fr and r is 0 or 1 for "asis" or "rotated".
    //           
    //, size_fits=[ (isequal(sx,cx)||sx>cx) && (isequal(sy,cy)||sy>cy)
    //            , (isequal(sx,cy)||sx>cy) && (isequal(sy,cx)||sy>cx)
    //            ] 
    , rtn =  size_fits==[true,false]? [f0,stocksize,cutsize,LF]
            :size_fits==[false,true]? [fr,stocksize,,[cy,cx],LF]
            :size_fits==[false,false]? []
            :f0>fr? [f0,stocksize,cutsize,LF]:[fr,stocksize,[cy,cx],LF]
             
    )
 //ISLOG?
   echo( log_( [ "srcWeight", srcWeight, "L",L, "LF", LF, "size_fits", size_fits], 4))
   echo( log_( [ _s("_f0 (cut={_})", str(cutsize)), _f0               
               , _s("f0 (cut={_}){_}",[str(cutsize), size_fits[0]?"":_orange(" oversized! ")]), f0
               ], 4))
   echo( log_( [ _s("_fr (cut={_})", str([cy,cx])), _fr
               , _s("fr (cut={_}){_}",[str([cy,cx]), size_fits[1]?"":_orange(" oversized! ")]), fr
               ], 4))
   //echo( log_( [ _s("f0 (cut={_})", str(cutsize)), f0, _s("fr (cut={_})",str([cy,cx])), fr], 4))
   echo( log_e(["stocksize", stocksize 
               , "fitness [f, stocksize, cutsize,LF]", _b(_blue(rtn))
               ],4))
   rtn
 //: rtn  
);
 


function cl2d_best_fit_203h( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       , stockWeights // 19142: optional, h{stock_i:weight}
                                       //         weight: number, default=1
                       , LF // 203h: Landscape Factor (optional)
                       )=
( /*
    Decide which stock is the best fit for the cutsize.
    Return a sorted array [ ["fitness",f, "stocksize", stocksize, "stock",i, "rot",j ]
                          , ["fitness",f, "stocksize", stocksize, "stock",i, "rot",j ]
                          , ...]
   (sorted based on f, from small to large)  
   Like:
   
   	[ ["fitness", 0.25, "stocksize", [2, 8, 1], "stock_i", 1, "rot", 1]
   	, ["fitness", 0.72, "stocksize", [4, 5, 1], "stock_i", 0, "rot", 1]
   	, ["fitness", 0.8, "stocksize", [4, 5, 1], "stock_i", 0, "rot", 0]
   	]                      
   Which means: stock [4,5] w/o cut rotation is the best fit. 

   For special cases where f=1 like case A and B below:

    A             B   
   +--.-----+    +--.-------------+
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   +--.-----+    +--.-------------+
    
   f= 1 for both cases. But obviously we want A to have the priority
   (fill and used up the smaller stock first)
   
   So f=1 needs special treatment. 
    
    
 */
  let( ISLOG = ISLOG  
     , _fits= [ for(i = range(stocksizes) )
                 let(f = echo(log_(_s("Calc fitness of cut {_} to stock {_}, LF={_}", [cutsize,stocksizes[i], LF]),3))
                          cl2d_fitness_203h( stocksize = stocksizes[i]
                                           , cutsize = cutsize
                                           , LF = LF ))
                          // fs => a[[f,is_rot,LF]]
                  if(f)
                  [ f[0]  // fitness
                  , i     // stock_i
                  , f[1]  // stocksize
                  , f[2]  // cutsize
                  , f[3]  // LF
                  ]        
                 //each [ for(fi=range(fs)) // 
                 //        if( fs[fi]!=undef ) [fs[fi],[stocksizes[i], i,fi]]
                 //     ]    
              ]
       
      // fits = arr[fit, [stock_i, is_rot]]
      //      =[[2.56055, [1, 0]], [0.5, [2, 1]], ...]
      
//     , fits= [ for( fir= _fits )
//                let( stock_i= fir[1][0] 
//                   , weight= h(stockWeights, stock_i,1)
//                   )
//                 [fir[0]*weight, fir[1] ]  
//             ] 
     , sfits = len(_fits)>1?sortArrs(_fits,0):_fits
     , f = last(sfits)
     , rtn = f?[ "fitness", f[0], "stock_i", f[1]
             , "stocksize", f[2], "cutsize", f[3] 
             , "LF", f[4]
             ] :[]         
     )
   //ISLOG?  
     echo(log_b( str( "cl2d_best_fit_203h"
                  , _s(" ( stocksizs={_},cutsize={_}, LF={_} )",[stocksizes, cutsize,LF])
                  ), 3) ) 
     echo( log_(["_fits (a[fitness,stock_i, stocksize, cutsize, LF]])", _fits ],3))
     echo( log_(["sfits (a[fitness,stock_i, stocksize, cutsize, LF]])", sfits ],3))
     //echo( log_(["sorted fitness", rtn],3))
     echo( log_e(["best fit", rtn],3))
     rtn
   //: rtn  
);


function cl2d_203h( stocks // h{coord:size} = [[x,y],[a,b],[x,y],[a,b]...] 
                  , parts // [ [x,y], [x,y] .... ]
                  //, stocks // if given, stocksize is ignored 
                  , LF  // Landscape Factor (203h)
                       
                  , stop_i // Set to an int to stop the iteration midway for debug
                  , _stocks // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _cuts   // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _unfits // 203h: parts that are not fitted or run-outta stocks 
                            // [ [sx,sy],[sx,sy]...]                    
                  , _i=-1
                  
                  )=
(
   //let(ISLOG=ISLOG)
   //ISLOG?
    echo(log_b(
       _i==-1?
         _ss("<b>cl2d_203h</b>(stocks={_:b},parts={_:b}, LF={_:b}): init(_i=<b>-1</b>)", [stocks,parts, LF])  
        :_ss("<b>cl2d_203h</b>(_stocks={_:c=red}, already _cuts={_:c=red}, _i={_:b;c=red} )", [_stocks,_cuts, _span(_i,"font-size:14px")])
       , 1))
    //:echo()
         
   _i==-1? // init 
   let(             
       // Make sure stocks is in landscape mode:
      , _stocks = [ for( i = [0:len(stocks)/2-1] )
                    let( coord = stocks[i*2]
                       , size = stocks[i*2+1]
                       )
                    each [ coord, size.x<size.y?[size.y,size.x]:size ]
                  ]              

      , _parts = reverse(
                   quicksort([  // Convert parts to sorted 
                              //   [ [[x,y],3], [[x,y],1], [[x,y],2] .... ]
                              // where 3,1,2 are the original index   
                    for( i=range(parts)) 
                        [ parts[i].x<parts[i].y  // flip x,y if y > x, for sort
                            ? [parts[i].y,parts[i].x]
                            : parts[i]
                        , i
                        ]  
                  ])
                )  
      )
//    ISLOG?  
//      (
      echo(log_(["_stocks", _bcolor(_b(_stocks),"lightgreen")],1))
      //echo(log_(["_parts(sorted parts)", _bcolor(_b(_parts),"yellow")],1))
      echo(log_(["_parts(parts sorted by long side => a[part_size, part_i])", _bcolor(_b(_parts),"lightgreen")],1))
      echo(log_e("Ready to cut parts one by one",1)) 
      cl2d_203h( stocks=stocks
                , parts=_parts
                , LF = LF  // Landscape Factor (203h)
                , stop_i= stop_i
                , _stocks= _stocks // [ [0,0], [x,y] ]
                , _cuts= []
                , _unfits=[] 
                , _i=0 )
//      )          
//    : cl2d_203h( stocks=stocks
//                , parts=_parts
//                , LF = LF  // Landscape Factor (203h)
//                , stop_i= stop_i
//                , _stocks= _stocks // [ [0,0], [x,y] ]
//                , _cuts= []
//                , _unfits=[] 
//                , _i=0 )
                
          
 :_i<len(parts) && (stop_i==undef?true: (_i<stop_i))?
   
    //ISLOG?
    echo(log_(["part", parts[_i], ", LF", LF, "=> cut", _bcolor(_b(parts[_i][0]),"lightgreen")],1))     
   //:echo()
   
   let( cut= parts[_i]  // [part_size([x,y]), part_index]
      , stocksizes = vals( _stocks )
      , cutsize = cut[0]
      , bestfit = echo(log_( str("Calling cl2d_best_fit_203h(...LF=",LF,")"),1))
                    cl2d_best_fit_203h( stocksizes=stocksizes // [ [x,y], [x,y]...]
                       , cutsize=cutsize    // [x,y]
                       , LF=LF  // Landscape Factor (203h)
                       )
                    // =>    
                    // bestfit: arr of ["fitness", 0.833333, "stock", 1, "rot", 1]
      )
       bestfit==[]||_stocks==[]? 
          let( __unfits= concat( _unfits, [parts[_i][0]] ) )
          //(
          // ISLOG?   
            echo(log_(["bestfit", bestfit],1 ))
            echo(log_(str("===> bestfit=[] => "
                         , _red("can't find any stock for cut ")
                         , _b(_red( cut[0]))
                         ) ,1 ))
            echo(log_(["__unfits", __unfits],1 ))
            echo(log_e(str("End of _i=",_i),1)) 
            cl2d_203h( stocks=stocks
                      , parts=parts
                      , LF = LF  // Landscape Factor (203h)
                      , stop_i= stop_i
                      , _stocks= _stocks // [ [0,0], [x,y] ]
                      , _cuts= _cuts 
                      , _unfits = __unfits
                      , _i=_i+1 )
//          : cl2d_203h( stocks=stocks
//                      , parts=parts
//                      , LF = LF  // Landscape Factor (203h)
//                      , stop_i= stop_i
//                      , _stocks= _stocks // [ [0,0], [x,y] ]
//                      , _cuts= _cuts 
//                      , _unfits = __unfits
//                      , _i=_i+1 )
//          )
                    
      :let(
           stock_i = h(bestfit, "stock_i")
          , stocksize= h(bestfit, "stocksize")
          , cutpoint = keys( _stocks )[stock_i]
          , _cutsize= h(bestfit, "cutsize") //h(bestfit, "rot")? reverse(cutsize): cutsize               
                       
          , __stocks = cl2d_makecut_203h( _stocks // [ [0,2], [4,1] 
                                  // , [1,0], [3,3] 
                                  // ] 
                         , [cutpoint, _cutsize]   // [[1,0],[2,1]]  
                         , _i
                         )                 
          , __cuts = concat(_cuts, [cutpoint, _cutsize ] )                     
          )
         //( 
         //ISLOG?
          echo(log_(["bestfit", bestfit, "_unfits", _unfits],1 ))
          echo(log_e(str("End of _i=",_i),1)) 
          cl2d_203h( stocks=stocks
                    , parts=parts
                    , LF = LF  // Landscape Factor (203h)
                    , stop_i= stop_i
                    , _stocks= __stocks // [ [0,0], [x,y] ]
                    , _cuts= __cuts
                    , _unfits = _unfits 
                    , _i=_i+1 
                    )
         //: cl2d_203h( stocks=stocks
                    //, parts=parts
                    //, LF = LF  // Landscape Factor (203h)
                    //, stop_i= stop_i
                    //, _stocks= __stocks // [ [0,0], [x,y] ]
                    //, _cuts= __cuts
                    //, _unfits = _unfits 
                    //, _i=_i+1 )
                    //)
         //)            
   
  : 
    echo( log_(["_stocks", _stocks, "_cuts", _cuts],1))
    echo(log_e("Leaving cl2d_203h()",1))
    [_stocks,_cuts, _unfits]
);  

 //: cl2d_203h_test
stocksize= [8,4]; 
//parts= [ [2,3], [5,4], [1,2] ];
//parts= [ [2,3], [2,3], [1,2] ];
//parts= [ [2,3], [5,1], [1,2] ]; 
//parts= [ [2,4], [1,3], [1,3], [2,1],[2,1] ]; 
//parts= [ [1,3], [1,3], [2,1],[2,1] ]; 
//parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ]; 
//parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1],[3,1] ]; 
//parts= [ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ];
//parts= [ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ]; 
////
//// Multiple copies
//parts=[ [1,0.5,4], [2,3,3], [0.2,2,5] ];
//parts=[ [1,0.5,4], [2,3,1],  [1,2,3], [0.2,2,5] ];
//parts=[ [1,0.5,4], [2,3], [8,0.1,2], [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1], [1,2,3], [0.2,2,5] ];
//parts=[ [1,0.5,4], [2,3,1], [8,0.1,3], [1,2,3], [0.2,2,5] ];
//

// The following stocks are to demo the srcWeight in multi-stock conditions:
stocksize=[ [8,2], [2,4] ];
//stocksize=[ [8,2], [2,4,10] ];
stocksize=[ [5,3], [4,2], [2,3] ];
//stocksize=[ [5,3], [4,2], [2,3,10] ];
//stocksize=[ [5,3], [4,2,10], [2,3,20] ];
//stocksize=[ [5,3], [4,2,20], [2,3,10] ];
//stocksize=[ [5,3], [4,2,10], [2,3] ];
//stocksize=[ [5,3,10], [4,2], [2,3] ];
//stocksize=[ [5,5], [4,3,10], [2,4] ];
//stocksize=[ [5,5], [4,3], [2,4,10] ];
//stocksize=[ [5,5,10], [4,3], [2,4] ];
//stocksize=[ [5,5,10], [4,3,10], [2,4] ];
//stocksize=[ [5,5,10], [4,3,20], [2,4] ];
////stocksize=[ [5,5,10], [4,3], [2,4,10] ];
//stocksize=[ [5,5,10], [4,3], [2,4,20] ];
//stocksize=[ [5,5], [4,3], [2,4] ];
//stocksize=[ [5,5,20], [4,3], [2,4] ];
//
////stocks=undef;
//
//stocksize=[ [10,10] ];
//parts=[ [4,8],[7,3],[3,3,2]];
//parts=[ [7,6], [4,3,2], [4,2] ];
//parts=[ [7,6], [4,3,2], [4,2,2] ]; // This example shows that "fully-left-pack" is not always good
   // 1918: test this by changing LF   
   //
   // fitness for cutsize [4,3] at i=1 ([4,3],[3,4]):
   //
   //    LF    stock[10,3], stock[4,10]   best
   //  -----------------------------------------------
   //  undef   (4.3,os)  (1.4,1.25)    [10,3]/[4,3]
   //    0     (1,os)    (1,0.85)         "
   //   0.5    (2.67,os) (1.2,1.05)       " 
   //    1     (4.33,os) (1.4,1.25)       "
   //    2    (7.66,os)  (1.8, 1.65)      "  
   //   -1    (-2.33,os) (0.6,0.45)   [4,10]/[4,3] 
   //   -2    (-5.66,os) (0.2,0.05)       "
   //
   // The lower the LF, the less effect of "Lanscape Factor" ( = L = 10/3 )
   // LF < 0 ===> negative effect of Landscape Factor, making [4,10] more likely.
   
//cl2d_203h_test( stocksize=stocksize
//              , parts=parts
//              //, LF = -2//0.8 // 0.1 
//              );

module cl2d_203h_test( stocksize, parts
                     //, stocks // if given, ignore stocksize 
                     , show_stocks=false
                     , LF  // Landscape Factor (203h)
                     , stop_i
                     //, stop_i=5 // <=== for debug
                     )
{

   echom("cl2d_203h_test()");
   
   //stocksize = [8,4];
   //parts = [ [2,3], [5,4], [1,2] ]; // nice
   
   
   //=========================================
   //parts = [ [2,3], [2,3], [1,2] ]; // last cutpoint error
   /*
      parts = [ [2,3], [2,3], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [2, 3], [2, 0], [2, 3], [2, 3], [2, 1]]
      _stocks=[[4, 0], [4, 4], [4, 3], [4, 1], [0, 3], [8, 1]]
      
         Error _stocks [[4, 3], [4, 1]] causing the [2,1] cut 
         up in the wrong place
      
      .---.---+=======+---.---.---.---.
      |   :   |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |      -|---.---.---.---.
      |       |       |   :   :   :   :
      |       |       |---.---.---.---.
      |       |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      
   */
   
   //=========================================
   //parts = [ [2,3], [5,1], [1,2] ];  
   /* parts = [ [2,3], [5,1], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
          f0= _f0==1? (1+ ( cx==sx?cy/sy:cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:cy/sx )):_f1
          
          
      cuts = [[0, 0], [5, 1], [5, 0], [3, 2], [5, 2], [1, 2]]
      stocks = [[6, 2], [2, 2], [0, 1], [8, 3]]  <=== wrong !!!
               should have been:
               [[6, 2], [2, 2], [0, 1], [5, 3]]
               
      .---.---.---.---.---+---+---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---|   |---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---+===+=======+
      |   :   :   :   :   |           |
      +===================+           |
      |                   |           |
      +===================+===========+
                                      
      Consider fix: when placing [3,2] on either [8,3] or [3,5]:
   
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      +===================+---.---.---.
      |                   |   :   :   :
      +===================+---.---.---.
     
      [8,3] is filled first. 
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [5, 1], [0, 1], [2, 3], [5, 0], [2, 1]]    
      _stocks=[[7, 0], [1, 4], [5, 1], [3, 3], [2, 1], [6, 3]]
      
      _stocks should have been:
                [[7, 0], [1, 4], [2, 1], [6, 3]]
          
      +=======+---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      +=======+===========+=======+---.
      |                   |       |   :
      +===================+=======+---.

   
   
   */
   
   //stocksize = [12,4];
   //parts = [ [2,3], [5,1], [1,2] ]; 
   
   //parts=[ [2,4], [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ]; // Error: cutpoint overlap
   //parts=[ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ];
   /*
      +=======+---.---.---+=======+===+
      |       |   :   :   |       |   |
      |       |---.---.---|       |   |
      |       |   :   :   |       |   |
      |       |---.---.---+=======+===+
      |       |   :   :   |       |   |
      +=======+===========+       |   |
      |                   |       |   |
      +===================+=======+===+
   
   Note: this doesn't look a good arrangement. HOWEVER, 
      it is better than what is thought to be a good one:
      
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |=======+=======+---.---.
      |       |       |       |   :   :
      |       |       |      -|---.---.
      |       |       |       |   :   :
      +=======+=======+===+===+===+---.
      |                   |       |   :
      +===================+=======+---.
   
   because it leaves out a square block. 
   
   */
   
   //parts= repeat( [ [1,3] ], 8 ); // ERROR cutpoints overlap !!! 
   //parts= concat( repeat( [ [1,2] ], 5 )
   //             , repeat( [ [1,3] ], 4 ) ); // ERROR cutpoints overlap !!! 
   //echo( log_b("cl2d_203h_test()") );
   echo(log_b(str("<b>cl2d_203h_test</b>("
                 , _ss( "stocksize={_:b},parts={_:b}, LF={_:b})"
                      , [stocksize,parts,LF])
                 ))) ; 
  
   __stocksize = isnum(stocksize[0])?[stocksize]:stocksize;
   
   _stocksize = [ for(xy=__stocksize)
                    xy.x>xy.y? [xy[0],xy[1],und(xy[2],1) ] 
                             : [xy[1],xy[0],und(xy[2],1) ] 
   
                ];
                
   __stocks = [ for( i=range( _stocksize) )  
                      let( sizexy = _stocksize[i] 
                         , base_y= i==0?0
                                   :sum( [for(j=[0:i-1]) _stocksize[j].y ])+ 0.2*i
                         )
                         each [ [0,base_y], sizexy ]
                    ]; 
   echo(log_(["stocksize", stocksize]));
   echo(log_(["_stocksize", _stocksize]));
   echo(log_(["__stocks", __stocks]));
                          
   // Make sure stocks is in landscape mode:
//   _stocks = [ for(i = [0:len(__stocks)/2-1] )
//                    let( coord = __stocks[i*2]
//                       , size = __stocks[i*2+1]
//                       )
//                    each [ coord, size.x<size.y?[size.y,size.x, size[2]]:size ]
//                  ];              
   
   _stocks = __stocks;
   
   echo(log_(["_stocks (assure landscape stocks)", _stocks]));  
   
   _parts = [ each for(p=parts) 
               len(p)==3? repeat( [[p[0],p[1]]], p[2] ) 
                          : [[p[0],p[1]]] 
            ];
   echo(log_(["parts", parts]));
   echo(log_(["_parts (repeats expanded)", _parts]));
   
   //--------------------------------------------------
   echo(log_(str("Calling cl2d= cl2d_203h( "
                , _ss("stocks={_:b}, parts={_:b}, LF={_:b})"
                     , [_stocks, _parts, LF])
                ) ,1));
   cl2d= cl2d_203h( stocks= _stocks //stocksize // [x,y] 
                  , parts=_parts 
                  , LF = LF  // Landscape Factor (203h)
                , stop_i= stop_i
                  );
   echo(log_(["cl2d (=[stocks,cuts]) ", cl2d],1));
   echo(log_(["=> cl2d[0](remaining stocks)",cl2d[0]],1));
   echo(log_(["=> cl2d[1](cuts)",cl2d[1]],1));
   if(cl2d[2])
   echo(log_(_span(str("=> cl2d[2](unfits) = ",_b(cl2d[2])),"color:red;background:yellow;font-size:12px"),1));
     
   //--------------------------------------------------               
   cuts= cl2d[1];
   stocks_left = cl2d[0];
   //echo(stocks_left= stocks_left);
   if(show_stocks)
   for(i = range( len(stocks_left)/2 ))
   {
      pts= cl2d_get_stock_pts( stocks_left[2*i], stocks_left[2*i+1]);
      //echo("stocks_left: ", i=i, stocks_left[2*i], pts=pts);
      Colors(i+1) Line( addz(pts,-0.5)
                      , closed=1);
      Colors(i+1,0.1) Plane( addz(pts,-0.5) );                 
   }
   
   
   //---------------------------------
   // Draw multiple original _stocks:                 
   for(sto=hashkvs(_stocks))
   {
     //echo(sto = sto);
     x=sto[0].x;
     y=sto[0].y;
     a=sto[1].x; //max([sto[1][0], sto[1][1]]);
     b=sto[1].y; //min([sto[1][0], sto[1][1]]);
     for(r= [y:b+y]) //x:x+a] ) 
     {  p2=[r*Y+x*X, (x+a)*X+r*Y];
        //echo(r=r, p2=p2);
        Line( p2 );
     }
     for(c= [x:a+x]) //x:x+a] ) 
     {  p2=[y*Y+c*X, (y+b)*Y+c*X];
        //echo(c=c, p2=p2);
        Line( p2 );
     }
   }
   //---------------------------------
   
      
     //x=stocksize[0];
     //y=stocksize[1];
     //for(r= range(y+1) ) Line( [O+r*Y, x*X+r*Y]  );
     //for(c= range(x+1) ) Line( [O+c*X, c*X+y*Y]  );
      
                    
                    
   
   for( i = range(keys(cuts))) //x0= keys(cuts) )
   {
      x0= cuts[2*i];
      _x0 = addz(x0,0);
      size = h(cuts, x0); 
      x=size.x;
      y=size.y;
      pts= addz( [ _x0, _x0+y*Y, _x0+x*X+y*Y, _x0+x*X], i*0.2) ;
      //echo(x0=x0, size=size, pts=pts);
      Colors(i+1,0.3)Plane(pts);
      Black() 
         MarkCenter( addz(pts,0.2)
                   , Label=["text",str(i,":[",x,",",y,"]"),"scale",.75]);
   }
   
   echo(log_e("End of cl2d_203h_test()"));
}



//. tools


//cl2d_makecut_tester( [ [0,0],[5,5] ], [[0,0],[5,5]], [[0,5],[5,5],[5,0],O] ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
//cl2d_makecut_tester( [ [0,0],[5,5] ], [[0,0],[5,3]], [[0,5],[5,5],[5,0],O] ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
//cl2d_makecut_tester( [ [0,0],[5,5] ], [[0,0],[3,5]], [[0,5],[5,5],[5,0],O] ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
//cl2d_makecut_tester( [ [0,0],[5,5] ], [[0,0],[1,3]], [[0,5],[5,5],[5,0],O] ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
//cl2d_makecut_tester( [[3, 0], [2, 5], [1,1],[4,4], [0,4],[5,1]], [[1,1],[1,1]], [[0,5],[5,5],[5,0],[3,0],[3,1],[1,1],[1,4],[0,4]] ); // [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]

//----------------------------------
// cl2d_makecut_18cp-specific test (overlapping blocking):

//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[1,2]] ); // 
//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 1], [4, 4], [0, 2], [5, 3]], [[1,1],[1,2]], [[0,5],[5,5],[5,0],[2,0],[2,1],[1,1],[1,2],[0,2]] ); // 
//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 1], [4, 4], [0, 2], [5, 3]], [[1,1],[1,3]], [[0,5],[5,5],[5,0],[2,0],[2,1],[1,1],[1,2],[0,2]] ); // 
//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[2,1]] ); // 
//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[3,1]] ); // 

//----------------------------------
// cl2d_makecut_18cu-specific test (overlapping blocking on both x and y):

//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[1,1]] ); // 
//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[3,2]] ); // 
//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[4,2]] ); // 
//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[3,3]] ); // 
//cl2d_makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[4,3]] ); // 

//----------------------------------
// cl2d_makecut_18cv-specific test (overlapping blocking):

//cl2d_makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[1,2]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//cl2d_makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[1,3]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//cl2d_makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[4,3]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//cl2d_makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[1,4]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//cl2d_makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[1,5]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//cl2d_makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[4,5]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 

//cl2d_makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[1,1]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//cl2d_makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[2,1]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//cl2d_makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[3,1]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//cl2d_makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[2,2]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//cl2d_makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[5,1]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//cl2d_makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[5,2]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 

//----------------------------------
// cl2d_makecut_18cv debugging:

//cl2d_makecut_tester( [[5, 0], [3, 3], [6, 0], [2, 4]], [[5, 0], [1, 3]], [[5,3],[6,3],[6,4],[8,4],[8,0],[5,0] ] ); 
//cl2d_makecut_tester( [[5, 0], [3, 3], [6, 0], [2, 4]], [[5, 0], [1, 2]], [[5,3],[6,3],[6,4],[8,4],[8,0],[5,0] ] ); 
//cl2d_makecut_tester( [[1, 0], [7, 3], [3, 0], [5, 4]], [[1, 0], [1, 2]], [[1,0],[1,3],[3,3],[3,4],[8,4],[8,0] ] ); 
//cl2d_makecut_tester( [[1, 0], [7, 3], [3, 0], [5, 4]], [[1, 0], [2, 2]], [[1,0],[1,3],[3,3],[3,4],[8,4],[8,0] ] ); 
//cl2d_makecut_tester( [[1, 0], [7, 3], [3, 0], [5, 4]], [[1, 0], [3, 2]], [[1,0],[1,3],[3,3],[3,4],[8,4],[8,0] ] ); 


module cl2d_makecut_tester( stocks
                     , cut
                     , framePts // optional
                     )
{
  /* visual demo for new makecut routines */
  
  /*
       +------------------------+    [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ] 
       |    :         :         |    
       |    :         :         |    
       |    :         :         |    
       +----+ - - - - - - - - - |     
     [0,3]  |         :         |     
            |         :         |     
            |         :         |     
            +---------+ - - - - |  
          [1,1]       |         | 
                      +---------+                      
                    [3,0]

   */
   echom("cl2d_makecut_tester()");
   
   st = addz(getStocksTop(stocks));
   echo(stockstop = st);
   Line(st, r=0.1);

   stocks2 = cl2d_makecut_203h( stocks, cut ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
                  
   echo(stocks = stocks);
   echo(stocks2 = stocks2);
   
   if(framePts) Line( addz(framePts,0), r=0.025, closed=1, color="black");
   else 
   cl2d_Draw_frames([5,5], [[0,3],[1,2],[2,0]] );
   
   cl2d_Draw_cuts(stocks2, raise=0.1);
   
   Purple(0.5) Line( cl2d_get_stock_pts( cut[0], cut[1] ), r=0.075, closed=1 );
   Black(0.1)cl2d_Draw_stock(cut);
   
}

 
  

function cl2d_restock( stock   // [coord, size] = [ [x,y],[a,b]]
                , cut     // [coord, size] = [ [x,y],[a,b]]
                )=
(
  /* A stock = a square given as [coord, size]
  
    stock [[x,y],[a,b]] 
        +-------------+
      b |             |
        |             |
        +-------------+
      [x,y]    a 

    is subjected to a "cl2d_restock" when a part of it is cut away by
    a *cut* (also given as [coord,size]):
              a
        C------D------E
        |             |
      b B      +======F==+
        |      |         |
        A------G         |
      [x,y]    +=========+
      
    cl2d_restock() example above returns a hash h{coord:size} containing 2 stocks:   
      
        C-------------E
        |             |
        B-------------F
      
        C------D
        |      |
        |      |
        |      |
        A------G
       
            
  
  */
   let( st_cp= stock[0] 
      , st_size= stock[1]
      , st_tr= sum( stock )  // top_right corner
      , st_weight = stock[2]==undef?1:stock[2] 
      
      , cut_cp= cut[0]
      , cut_size= cut[1]
      , cut_tr= sum(cut)
      
      , sw = und(stock[1][2],1) // 19142: Source Stock Weight --- 
          // 19142: srcWeight:
          //        if stocksize is [x,y,n] but not [x,y], n is the weighting 
          //        factor coming from source stock (the very original one(s))
          //        applied to it's fitness. This allows specific piece of source
          //        stock be filled first.  
    
   , rtn=   
   cut_tr.x<=st_cp.x || 
   cut_tr.y<=st_cp.y ||
   cut_cp.x>=st_tr.x ||
   cut_cp.y>=st_tr.y ? [stock]
   :[ 
    //    B--G---C
    //    |      |  
    // +==A==+   D
    // |     F---E
    // +=====+
     st_cp.y <= cut_tr.y && cut_tr.y< st_tr.y ?
        // ABCD
        [ [ st_cp.x, cut_tr.y], [st_size.x, st_tr.y-cut_tr.y, sw] ] : []
   , st_cp.x <= cut_tr.x && cut_tr.x < st_tr.x ?
        // FGCE
        [ [ cut_tr.x, st_cp.y], [st_tr.x-cut_tr.x, st_size.y, sw] ] : []     

    //      +==+==+   
    //  B---C     |
    //  E   +==F==+
    //  |      |
    //  A---D--G
   , st_cp.x < cut_cp.x && cut_cp.x <= st_tr.x ?
        // ABCD
        [ st_cp, [cut_cp.x-st_cp.x, st_size.y, sw] ]:[]
   , st_cp.y < cut_cp.y && cut_cp.y<= st_tr.y ?   
        // AEFG
        [ st_cp, [st_size.x, cut_cp.y-st_cp.y, sw] ]:[]   
   ]   
   )
   [ for(x=rtn) each x ]   
//   :  3 
);

//echo(test__ = sum( [[2,3], [2,3,4]] ));
cl2d_restock_tests();
module cl2d_restock_tests()
{
   echom("cl2d_restock_tests()");
   stock = [ [2,1], [5,3] ];
   
   cut = [ [1,0], [1,1] ];
   cut = [ [1,0], [3,3] ];
   cut = [ [1,0], [3,4] ];
   cut = [ [1,0], [6,3] ];
   cut = [ [1,0], [6,4] ];
   //---------------------
   cut = [ [1,1], [1,1] ];
   cut = [ [1,1], [3,2] ];
   cut = [ [1,1], [3,3] ];
   cut = [ [1,1], [6,2] ];
   cut = [ [1,1], [6,3] ];
   //---------------------
   cut = [ [2,1], [1,1] ];
   cut = [ [2,1], [3,2] ];
   cut = [ [2,1], [3,3] ];
   cut = [ [2,1], [6,2] ];
   cut = [ [2,1], [5,3] ];
   //---------------------
   cut = [ [3,1], [1,1] ];
   cut = [ [3,1], [3,2] ];
   cut = [ [3,1], [3,3] ];
   cut = [ [3,1], [6,2] ];
   cut = [ [3,1], [5,3] ];
   //---------------------
   cut = [ [2,2], [1,1] ];
   cut = [ [2,2], [3,2] ];
   cut = [ [2,2], [3,3] ];
   cut = [ [2,2], [6,2] ];
   cut = [ [2,2], [5,3] ];
   //---------------------
   cut = [ [3,2], [1,1] ];
//   cut = [ [3,2], [3,2] ];
//   cut = [ [3,2], [3,3] ];
//   cut = [ [3,2], [6,2] ];
//   cut = [ [3,2], [5,3] ];
//   //---------------------
//   cut = [ [7,1], [1,1] ];
//   cut = [ [7,1], [3,2] ];
//   cut = [ [7,1], [3,3] ];
//   cut = [ [7,1], [6,2] ];
//   cut = [ [7,1], [5,3] ];
//   //---------------------
//   cut = [ [7,2], [1,1] ];
//   cut = [ [7,2], [3,2] ];
//   cut = [ [7,2], [3,3] ];
//   cut = [ [7,2], [6,2] ];
//   cut = [ [7,2], [5,3] ];
   
   
   newstocks = cl2d_restock( stock, cut );
   
   ss = [ each cut, each newstocks ];
   echo(newstocks = newstocks);
   cl2d_Draw_cuts( ss, raise=0.25 );
   Line( cl2d_get_stock_pts( stock[0], stock[1]), r=0.05, closed=1 );
}


function cl2d_restocks( stocks, cut )=
(
  echo( log_("<b>cl2d_restocks</b>(stocks,cut): Cut and re-arranging stocks",3) ) 
  let( _rtn=[ for( stock= hashkvs(stocks) )
            //echo( stock = stock )
            let( new = cl2d_restock( stock, cut ) )
            //echo( new= new )
            each new
  	    ]
     )
  cl2d_deInnerStocks( _rtn)   	    
);    
  


//cl2d_restocks_tests();
module cl2d_restocks_tests()
{
   echom("cl2d_restocks_tests()");
   stocks =  [[2, 2], [5, 2], [4, 1], [3, 3]];
   
   cut = [ [1,0], [1,1] ];
   cut = [ [1,0], [3,3] ];
   cut = [ [1,0], [3,4] ];
   cut = [ [1,0], [6,3] ];
   cut = [ [1,0], [6,4] ];
   //---------------------
   cut = [ [1,1], [1,1] ];
   cut = [ [3,0], [3,3] ];
//   cut = [ [1,1], [3,3] ];
//   cut = [ [1,1], [6,2] ];
//   cut = [ [1,1], [6,3] ];
//   //---------------------
//   cut = [ [2,1], [1,1] ];
//   cut = [ [2,1], [3,2] ];
//   cut = [ [2,1], [3,3] ];
//   cut = [ [2,1], [6,2] ];
//   cut = [ [2,1], [5,3] ];
//   //---------------------
//   cut = [ [3,1], [1,1] ];
//   cut = [ [3,1], [3,2] ];
//   cut = [ [3,1], [3,3] ];
//   cut = [ [3,1], [6,2] ];
//   cut = [ [3,1], [5,3] ];
//   //---------------------
//   cut = [ [2,2], [1,1] ];
//   cut = [ [2,2], [3,2] ];
//   cut = [ [2,2], [3,3] ];
//   cut = [ [2,2], [6,2] ];
//   cut = [ [2,2], [5,3] ];
//   //---------------------
//   cut = [ [3,2], [1,1] ];
//   cut = [ [3,2], [3,2] ];
//   cut = [ [3,2], [3,3] ];
//   cut = [ [3,2], [6,2] ];
//   cut = [ [3,2], [5,3] ];
//   //---------------------
//   cut = [ [7,1], [1,1] ];
//   cut = [ [7,1], [3,2] ];
//   cut = [ [7,1], [3,3] ];
//   cut = [ [7,1], [6,2] ];
//   cut = [ [7,1], [5,3] ];
//   //---------------------
//   cut = [ [7,2], [1,1] ];
//   cut = [ [7,2], [3,2] ];
//   cut = [ [7,2], [3,3] ];
//   cut = [ [7,2], [6,2] ];
//   cut = [ [7,2], [5,3] ];
   
   
   _newstocks = cl2d_restocks( stocks, cut );
   newstocks = cl2d_deInnerStocks( _newstocks ); //cl2d_restocks( _newstocks, cut ) );
   
   ss = [ each cut, each newstocks ];
   echo( stocks = stocks , cut=cut);
   echo( _newstocks = _newstocks);
   echo( newstocks = newstocks);
   //echo( newstocks2 = newstocks2);
   
   cl2d_Draw_cuts( stocks, raise=0 );
   cl2d_Draw_cuts( newstocks, raise=0.5 );
   //Line( cl2d_get_stock_pts( stock[0], stock[1]), r=0.05, closed=1 );
   cl2d_Draw_frames( [7,4] );
   Black() cl2d_Draw_cuts(cut, raise=-0.15);
   
}


function cl2d_get_stock_pts( coord, size, z=0 )= 
( 
  // cl2d_get_stock_pts( [coord,size] ) or cl2d_get_stock_pts( coord,size ) 
  // => 4 pts 
  let( cosiz = size==undef? coord:[coord,size]
     , _coord= cosiz[0]
     , _size = cosiz[1]
     )
  addz( [ _coord
        , [ _coord.x        , _coord.y+_size.y ]
        , [ _coord.x+_size.x, _coord.y+_size.y ] 
        , [ _coord.x+_size.x, _coord.y         ]
        ], z)
);


module cl2d_Draw_stock( stock,z=0 )  // stock = [ coord, size ]
{
   Plane( cl2d_get_stock_pts( stock, z=z ) );
}

function cl2d_isInnerStock( small,big )=  // big, small: [coord, size]
(
   // Check if small is an inner stock of big. ==> 1 or 0
   let( b_tr= sum(big)
      , s_tr= sum(small)
      , chk = [ b_tr.x>=s_tr.x ?1:0
              , b_tr.y>=s_tr.y ?1:0
              , big[0].x <= small[0].x?1:0
              , big[0].y <= small[0].y ?1:0
              ]
      , isIn= sum(chk)==4
   )
   //echo( _b(big), big_cp= big[0], b_tr=b_tr )
   //echo( "cl2d_isInnerStock() ", big=big, small=_color(small, isIn?"red":"green")
   //    , s_tr= _color(s_tr, isIn?"red":"green"), chk=chk, isIn = isIn
   //    , isIn? _b(str(small, " is in ", big)):"")
   isIn?1:0
);

function cl2d_deInnerStocks(stocks)=  // stocks: h{coord:size}
(                                     // Remove all inner stocks => stocks:h{coord:size}
   let( _stocks = quicksort( hashkvs( stocks )) 
      , rtn = [ for( small = _stocks )
                let(chk= sum( [ for( big = _stocks )
                                cl2d_isInnerStock( small, big )
                              ]
                            )
                    )
                 if(chk<=1)
                  //echo(small=_bcolor(_b(small),"yellow"), chk=chk)   
                  small 
              ] 
      )
      //echo( "In cl2d_deInnerStocks, ", stocks=stocks)
      //echo( "In cl2d_deInnerStocks, ", _stocks=_stocks)
      [ for(ss=rtn) each ss ]
);


//cl2d_Draw_frames(size=[5,5]);
//cl2d_Draw_frames(size=[5,5], cutpts=[[1,3],[2,1]]);
//cl2d_Draw_frames(size=[5,5], cutpts=[[0,3],[1,1], [2,0]]);
module cl2d_Draw_frames( size=[5,3]  // optional total size [x,y] 
                       , coords 
                       )  
{
   /*
       +-----------------------+     
       |    :        :         |    
       |    :        :         |    
       |    :        :         |    
       +----+ - -  - - - - - - +     
     [0,3]  |        :         |     
            |        :         |     
            |        :         |     
            +--------+ - - - - |  
          [1,1]      |         | 
                     +-- ------+  
                   [3,0]
                   
    coords = [ [0,3],[1,1],[3,0]]              
   */                
   //echom("cl2d_Draw_frames()");
   w=size.x; h=size.y;
   
   cpts = coords?
          [ for(i=range(coords))
            each [ coords[i]
                 , [ i==len(coords)-1? size.x:coords[i+1].x
                   , coords[i].y] ]
          , size, [ coords[0].x, size.y]
          ]
          : [ [size.x,0], [0,0], [0,size.y], size ] ;  
   
   //echo( cpts=addz(cpts,0) );
   Black() Line( addz(cpts,0), closed=true, r=0.025 );
   
   for(r= range(h+1) ) Line( [O+r*Y, w*X+r*Y]  );
   for(c= range(w+1) ) Line( [O+c*X, c*X+h*Y]  );   
}

//cl2d_Draw_frames(size=[5,3], coords=[[0,2],[1,0]]);
//cl2d_Draw_cuts([ [0,2], [4,1], [1,0], [2,2]]);
//cl2d_Draw_frames([5,4]);
module cl2d_Draw_cuts( blocks  // h{coord:size}
                  , raise=0.01 // raise up block to check overlap
                  , markCenterOpt=[]
                  )  
{
   //echom("cl2d_Draw_cuts()");
   for( i = range(keys(blocks))) 
   {
      blockpts= cl2d_get_stock_pts( coord=blocks[2*i], size=blocks[2*i+1] , z=(i+1)*raise);
      //echo("In cl2d_Draw_cuts: ", blockpts=blockpts);
      color(COLORS[i+1],0.4)Plane(blockpts);
      
      Black() MarkCenter( addz(blockpts,raise+0.1)
                        , Label=[ "text", str(i,":[",join(blocks[2*i+1],","),"]")
                                , "scale",1.5]
                        , opt=markCenterOpt);
                        
   }
   
}


//function getStockSize( stocks )= // stocks: h{coord:size} rtn: [sx,sy]
//(
//   let( xys = [ for(kv=hashkvs(stocks)) sum(kv) ] // [ top_right_of_blocks_[sx,sy] ]
//      , x = max( [for(xy=xys) xy[0] ] )
//      , y = max( [for(xy=xys) xy[1] ] )
//      )
//   [x,y]   
//);
//


function getStocksTop(stocks)= // stocks: h{coord:size}
(
   /* Stocks top is needed to fix errors caused by stocks w/ collapsed roof:
   
        D----E    stocks = [ [0,0], [2,1]   // ABFG
        |    |             , [1,0], [1,2] ] // HDEG
   B----C    F    blockpts= [ ABFG, HDEG ]
   |         |    blocktops=  [ BF, DE ]
   A----H----G    _blocktops= [ BC, DE ]
                  stockstop = BCDE
   */           
   let( blockpts = quicksort( [ for( i=[0:len(stocks)/2-1] )
                     cl2d_get_stock_pts( stocks[2*i], stocks[2*i+1] )
                   ])
      , blocktops= [ for(pts=blockpts) 
                      let(y=maxy(pts))
                      [ [minx(pts),y], [maxx(pts),y] ]
                   ] // = [ [[0,1],[2,1]], [[1,2],[2,2]] ]
      , _blocktops= len(blocktops)==1? blocktops
                    : [ for(i= [0:len(blocktops)-2] ) // blocktops[i]= [[1,2],[2,2]] 
                       each [ blocktops[i][0] 
                             , //echo(len(blocktops)-1, i=i,blocktops[i][0])
                               [ blocktops[i+1][0].x, blocktops[i][1].y ]
                             ]
                    , each last(blocktops)   
                    ]       
      )
      //echo("In getStocksTop, blockpts=", blockpts)
      //echo("In getStocksTop, blocktops=", blocktops)
      //echo("In getStocksTop, _blocktops=", _blocktops)
      addz( uniq(_blocktops) )
);



function cl2d_sort_by_coordX( stocks )=
(  
   echo(log_("Sort stocks by coord.x: <b>cl2d_sort_by_coordX</b>(stocks)", 3) )
   let( _stocks= [ for( i= range(keys(stocks)) )
                    [ stocks[2*i].x, [ stocks[2*i], stocks[2*i+1]] ]  
                 ] 
      , rtn = [ for( x= quicksort(_stocks) ) each x[1] ]
      )
   rtn   
);
   
   
   


//. ref
/*

* Branch and Bound algorithm
  https://en.wikipedia.org/wiki/Branch_and_bound


* PATTERN GENERATION FOR TWODIMENSIONAL CUTTING STOCK PROBLEM WITH LOCATION (2012)
  W. N. P Rodrigo1, W. B Daundasekera2 and A. A. I Perera3
  https://pdfs.semanticscholar.org/e670/93eb987900009d2e64a24fdc684706787eb4.pdf  
  http://www.ijmttjournal.org/Volume-3/issue-2/IJMTT-V3I2P504.pdf  
 
* Invited Review
  Cutting stock problems and solution procedures (1991)
  https://pdfs.semanticscholar.org/baa1/7869fc41e62bbf36776fc650f0179e8f43fc.pdf  
  Robert W. Haessler and Paul E. Sweeney

* Modeling Two-Dimensional Guillotine Cutting Problems via Integer Programming
  Fabio Furini, Enrico Malaguti, Dimitri Thomopulos, 2016
  http://www.optimization-online.org/DB_FILE/2014/11/4663.pdf
  https://pubsonline.informs.org/doi/10.1287/ijoc.2016.0710
  
* An Exact Method for the 2D Guillotine Strip Packing Problem (2009)
  Abdelghani Bekrar1 and Imed Kacem2
  http://www.kurims.kyoto-u.ac.jp/EMIS/journals/HOA/AOR/Volume2009/732010.pdf
  
  
* Algorithms for Two-Dimensional Bin Packing and Assignment Problems (2000 )
  Andrea Lodi Il Coordinatore I Tutori Prof. Giovanni Marro Prof. Silvano Martello
  https://pdfs.semanticscholar.org/c8fc/4b62b6fc4d9b5184377b0cc7a91a9cb72052.pdf  
  ## Looks like my algorithm
  

*/ 
/* Glossary

Column Generation
Branch and Bound
Strip packing problem
guillotine
dichotomic method vs brand-and-bound method 
    
*/  