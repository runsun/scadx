/*
    scadx_ref.scad
    
    This file stores info that's not needed for running scadx:
 
    * naming convension
    * doc
    * idea
    * theories
    * external references
    * history (version)
    * ascii line drawing symbons (for documentation)
    * to-do
        
        
    
*/

//. Naming Convension

/*
Naming convention: 

    arr : array
    str : string
    int : integer

    pq  : [P,Q]
    pqr : [P,Q,R]

    v,pt: a vector, a point 
    ln  : a line
    pl  : a plane
    pts : many points
    pis : face 
    pi  : pt index
    fi  : face index
    PF  : [pt, faces]
    PFS : [pf, faces, site]

    2-pointer: pq, st
    3-pointer: pqr, stu
    4-pointer: pqrs

    Single capital letters: points/vector (P=[2,5,0]);

    N: normal pt
    B: binormal pt
    O: [0,0,0]
    X: [1,0,0]
    Y: [0,1,0]
    Z: [0,0,1]
    
    aPQR: angle of P-Q-R
   
    RAS: Rich Argument System (see scadx_args.scad)
    
    function: starts with lower case (angle(), hash(...)) 
    module for object: starts with capitals (Line, MarkPts)
    for console: starts with underscore (_)

*/    

//. Function Template

/*

function funcname(n, _rtn, _i=-1)=  
( 
  
  let(lv=1)
  _i==-1?
      echo( log_b( "<func>()",lv) )
      echo( log_b( "init"), lv+1 )
      let( C = C?C:sum(pts)/len(pts) 
         , _rtn=[]  
         )
      echo( log_( ["C",C], lv+1 ))
      echo( log_e( "", lv+1 ))
      funcname(n, _rtn=_rtn, _i=0)   
  
  : _i<n?
    echo( log_b( _blue(str("_i=",_i)), lv+1 ))
    let( _rtn= _rtn
       )
    echo( log_(["_rtn",_rtn], lv+1 ) )
    echo( log_e( "", lv+1 ))
    funcname( n, _rtn=_rtn, _i=_i+1)   

  :  echo( log_b( "<func>() -- existing"), lv+1 )
     echo( log_(["_rtn",_rtn], lv+1 ) )
     echo( log_e( "", lv+1 ))
     echo( log_e( "", lv ))
     
  echo( log_e( "", 1 ))
  _rtn
  


);

*/

//. Doc 


/* 
=================================================================
                    scadx -- an OpenScad lib.
=================================================================

-- scadx_core.scad
-- Function doc and Test Tools 
III. Shape
=================================================================
*/ 

//===================================
// Builtin doc
//===================================

/*
cube = [["cube","size,center","n/a","3D, Shape"],
"
 Creates a cube at the origin of the coordinate system. When center\\
 is true, the cube will be centered on the origin, otherwise it is \\
 created in the first octant. The argument names are optional if the\\
 arguments are given in the same order as specified in the parameters.\\
 \\
 cube(size = 1, center = false);\\
 cube(size = [1,2,3], center = true);\\
"];

norm = [["norm","v","number","Math, Geometry"],
"
 Givena vector (v), returns the euclidean norm of v. Note this\\
 returns is the actual numeric length while len returns the number\\
 of elements in the vector or array.\\
"];

//doc(norm);
//doc(cube);
//doc(_fmt);
*/


//. Idea

//. Theories


//===========================================
//
//           Theories
//
//===========================================

/*

[ Woodworking and Openscad ]

    Lasercut by bmsleight 
    http://forum.openscad.org/Lasercut-tp14126.html
    
    OpenSCAD carving plugin by Pascal Eberhard 
    http://forum.openscad.org/OpenSCAD-carving-plugin-tp14136.html
    
[ Projection ]

    Gnomonic projection
    https://en.wikipedia.org/wiki/Gnomonic_projection

    Maps on a Cuboctahedron
    http://www.progonos.com/furuti/MapProj/Normal/ProjPoly/projPoly3.html
    
    The Perspective and Orthographic Projection Matrix http://www.scratchapixel.com/lessons/3d-basic-rendering/perspective-and-orthographic-projection-matrix
    Source code:    http://www.scratchapixel.com/code.php?id=4&origin=/lessons/3d-basic-rendering/perspective-and-orthographic-projection-matrix
    
    Perspective Projection (matrix)
    http://web.iitd.ac.in/~hegde/cad/lecture/L9_persproj.pdf
    
    Taxonomy of projection http://www.cs.princeton.edu/courses/archive/fall99/cs426/lectures/view/sld014.htm

    Perspective Projection
    http://ogldev.org/www/tutorial12/tutorial12.html
    
[ transformation matrix ]

    Translation Transformation
    http://ogldev.org/www/tutorial06/tutorial06.html
    
[ Ellipse ]

--- David's blog : Ellipse by Cylindrical Section
    http://binnerd.blogspot.com/2012/11/ellipse-by-cylindrical-section.html

--- Cylindrical Segment / Cylindrical wedge 
    http://mathworld.wolfram.com/CylindricalSegment.html
    
--- http://math.stackexchange.com/questions/493104/evaluating-int-ab-frac12-r2-mathrm-d-theta-to-find-the-area-of-an-ellips/687384#687384

--- Since4All
    Practical explanation of ellipse with daily experiences 
    http://www.science4all.org/le-nguyen-hoang/ellipses-parabolas-hyperbolas/
    
--- Drawing Hour Lines with Elliptical Coordinates 
    http://condor.depaul.edu/slueckin/elliptical.html

--- The ellipse can also be defined as the locus of points whose distance from the focus is proportional to the horizontal distance from a vertical line known as the conic section directrix, where the ratio is <1. Letting r be the ratio and d the distance from the center at which the directrix lies, then in order for this to be true, it must hold at the extremes of the major and minor axes, ... 
    http://mathworld.wolfram.com/images/eps-gif/EllipseDirectrix_1001.gif
 
--- Scale the entire figure along the y direction by a factor of a/b. The ellipse becomes a circle of radius a, and the two angles become tan−1(abtanθ1) and tan−1(abtanθ2).    http://math.stackexchange.com/questions/114371/deriving-the-area-of-a-sector-of-an-ellipse
    
--- Elliptic coordinate system
    https://en.wikipedia.org/wiki/Elliptic_coordinate_system


[ Font ]

--- MichaelAtOz's textManipulation.scad http://forum.openscad.org/textCylinder-dodgy-version-for-text-around-a-cylinder-td9262.html

--- brodykendrick rework of version 3 of write.scad to use the new OpenSCAD internal text() primitive https://github.com/brodykenrick/text_on_OpenSCAD/blob/master/text_on.scad

[ sweep ]

    Bending text
    http://f
    
    
    https://en.wikipedia.org/wiki/Neighborhood_operation.html
    
    Real Time Cloth Simulation with B-spline Surfaces    http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/real-time-cloth-simulation-with-b-spline-surfaces-r3814

    
    Sweep along polylines with mitered join
    http://www.cs.berkeley.edu/~sequin/CS284/LECT09/L7.html
    http://www.cs.berkeley.edu/~sequin/CS184/IMGS/Sweep_PolyLine.jpg
    
[ Geometry operations ]
   
    This section contains links related to geometry operations 
    like sweep, taper .. etc
    
    http://doc.cgal.org/latest/Manual/packages.html#PkgNef3Summary
    
    
    Blender: SimpleDeform Modifier http://wiki.blender.org/index.php/Doc:2.4/Manual/Modifiers/Deform/Simple_Deform
    
[ portal / links ]

    http://fablabamersfoort.nl/book/openscad
    https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Print_version#Libraries
    
    Libre Graphics World - 3D, Design, Photography
    http://libregraphicsworld.org/

[ polygon drawing tool ]

    PolygonRabbit
    http://www.protorabbit.nl/flash/polygonrabbit/PolygonRabbit.html

[ Polygon triangulation ]

    Polygon Triangulation
    http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/polygon-triangulation-r3334

    Adaptive Polyhedral Resampling for Vertex Flow Animation / Craig Reynolds 
    http://www.red3d.com/cwr/papers/1992/DuctileFlow.html

[ Sphere ]

    Polyhedral Maps
    http://www.progonos.com/furuti/MapProj/Normal/ProjPoly/projPoly.html

    ### Creating an icosphere mesh in code
    http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html

    ### Turning a cylinder into a sphere without pinching at the poles
    http://stackoverflow.com/questions/29126226/turning-a-cylinder-into-a-sphere-without-pinching-at-the-poles/29139125#29139125

    ### Math for a geodesic sphere
    http://stackoverflow.com/questions/3031875/math-for-a-geodesic-sphere

    ### Supercomputers Take a Cue from Microwave Ovens
    http://cs.lbl.gov/news-media/news/2011/supercomputers-take-a-cue-from-microwave-ovens/

[ Bezier, B-spline ]

    B-spline, Bezier, Bezier surface
    https://www.youtube.com/watch?v=qhQrRCJ-mVg
    Cubic Bezier pt:
    P(t) = P0(1-t)^3 + 3tP1(1-t)^2 + (3t^2)P2(1-t) + t^3P3

    Real Time Cloth Simulation with B-spline Surfaces
    http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/real-time-cloth-simulation-with-b-spline-surfaces-r3814

[ OpenScad animation gif ]

    OpenSCAD Animation view
    http://kitwallace.tumblr.com/post/85663417144/openscad-animation-view
    
[ Theoy | algorithm ]

http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/

[ Optimal fn ]

// calc the minimum fn fal to turn a curve for a defined feature size.
http://gist.github.com/hovissimo/2fdec1736880e77d49da

[ Other openscad libs ]

obiscad
http://github.com/Obijuan/obiscad


-- Intersection of line and plan:
http://paulbourke.net/geometry/pointlineplane/

The equation of a plane (points P are on the plane with normal N and point P3 on the plane) can be written as
N dot (P - P3) = 0
The equation of the line (points P on the line passing through points P1 and P2) can be written as
P = P1 + u (P2 - P1)
The intersection of these two occurs when
N dot (P1 + u (P2 - P1)) = N dot P3
Solving for u gives

u = (N dot (P3-P1) ) / (N dot (P2-P1))

P = P1 +   (N dot (P3-P1) ) / (N dot (P2-P1)) * (P2 - P1)


[P1,P2] => pq
[P3, P4, P5] = target
N = normal(rst)

P = pq[0] + dot(normal(rst), target[0]-pq[0]) 
		 / dot(normal(rst), pq[1]    -pq[0])
		 * (pq[1]-pq[0])

         
*/         

/*
	P1 = Q + t * (P-Q)
	P2 = N + s * (M-N)

	when P1=P2

	Q.x + t(P-Q).x = N.x + t(M-N).x


	t = 

	let:	A = a point on both pq and mn
		c = PA / PQ
 		
	=> A = [ c * dx(pq), c * dy(pq), c * dz(pq) ] 
		= c * [ dx(pq), dy(pq), dz(pq) ] 
		= c* (Q-P)
		= PA (Q-P) / PQ

	let d = MA/MN
	=> A = MA (N-M ) / MN

	PA(Q-P)/PQ = MA(N-M)/MN

	 PA							   MA
	---- [ dx(pq), dy(pq), dz(pq) ] = ---- [ dx(mn), dy(mn), dz(mn) ] 
	 PQ							   MN	

	let a = PA/PQ, b = MA/MN

	a [ dx(pq), dy(pq), dz(pq) ] = b [ dx(mn), dy(mn), dz(mn) ] 
	
	a * dx(pq) = b * dx(mn)
	a * dy(pq) = b * dy(mn)
	a * dz(pq) = b * dz(mn)

	a =  b * dx(mn) / dx(pq)
	    (a * dy(pq) / dy(mn)) * dx(mn) / dx(pq)

			(N-M) PQ
	PA = MA ---------
			(Q-P) MN

	//-----------------------------------------------

      P3 
	   &'-_
	   |&  '-._     
	   | &     '-._
	   |  &        '-_ 
	   |   &         _'P2
	   |  b &      -' /
	   |     &  _-'  /	
	   |      X'    /
	   |  a -' &   /
	   |  -'    & /
	   |-'_______&
     P1            P4


http://paulbourke.net/geometry/pointlineplane/

Line a = P:Q
Line b = M:N

This note describes the technique and algorithm for determining the intersection point of two lines (or line segments) in 2 dimensions.

The equations of the lines are

Pa = P + ua ( Q - P )
Pb = M + ub ( N - M )

Solving for the point where Pa = Pb gives the following two equations in two unknowns (ua and ub)

P.x + ua (Q.x - P.x) = M.x + ub (N.x - M.x)

and 

P.y + ua (Q.y - P.y) = M.y + ub (N.y - M.y)

Solving gives the following expressions for ua and ub

k = [ (N.y-M.y)(Q.x-P.x) - (N.x-M.x)(Q.y-P.y) ]

ua = [ (N.x-M.x)(P.y-M.y) - (N.y-M.y)(P.x-M.x) ] / k

ub = [ (Q.x-P.x)(P.y-M.y) - (Q.y-P.y)(P.x-M.x) ] / k




Line a = P1:P2
Line b = P3:P4

This note describes the technique and algorithm for determining the intersection point of two lines (or line segments) in 2 dimensions.

The equations of the lines are

Pa = P1 + ua ( P2 - P1 )
Pb = P3 + ub ( P4 - P3 )

Solving for the point where Pa = Pb gives the following two equations in two unknowns (ua and ub)

x1 + ua (x2 - x1) = x3 + ub (x4 - x3)

and 

y1 + ua (y2 - y1) = y3 + ub (y4 - y3)

Solving gives the following expressions for ua and ub

k = [ (y4-y3)(x2-x1) - (x4-x3)(y2-y1) ]

ua = [ (x4-x3)(y1-y3) - (y4-y3)(x1-x3) ] / k

ub = [ (x2-x1)(y1-y3) - (y2-y1)(x1-x3) ] / k

*/

//);

/*
// OpenSCAD transformation matrix function library. 
// Henry Baker, Santa Barbara, CA, 7/2013. 
http://forum.openscad.org/Transformation-matrix-function-library-td5154.html

function scale(v)=[[v[0],0,0,0], 
                   [0,v[1],0,0], 
                   [0,0,v[2],0], 
                   [0,0,0,1]]; 

function rotatex(a)=[[1,0,0,0], 
                     [0,cos(a),-sin(a),0], 
                     [0,sin(a),cos(a),0], 
                     [0,0,0,1]]; 

function rotatey(a)=[[cos(a),0,sin(a),0], 
                     [0,1,0,0], 
                     [-sin(a),0,cos(a),0], 
                     [0,0,0,1]]; 

function rotatez(a)=[[cos(a),-sin(a),0,0], 
                     [sin(a),cos(a),0,0], 
                     [0,0,1,0], 
                     [0,0,0,1]]; 

// From Wikipedia. 
function rotatea(c,s,l,m,n)=[[l*l*(1-c)+c,m*l*(1-c)-n*s,n*l*(1-c)+m*s,0], 
                             [l*m*(1-c)+n*s,m*m*(1-c)+c,n*m*(1-c)-l*s,0], 
                             [l*n*(1-c)-m*s,m*n*(1-c)+l*s,n*n*(1-c)+c,0], 
                             [0,0,0,1]]; 

function rotateanv(a,nv)=rotatea(cos(a),sin(a),nv[0],nv[1],nv[2]); 

function rotate(a,v)=(v==undef)?rotatez(a[2])*rotatey(a[1])*rotatex(a[0]): 
                     rotateanv(a,v/sqrt(v*v)); 

function translate(v)=[[1,0,0,v[0]], 
                       [0,1,0,v[1]], 
                       [0,0,1,v[2]], 
                       [0,0,0,1]]; 

// From Wikipedia. 
function mirrorabc(a,b,c)=[[1-2*a*a,-2*a*b,-2*a*c,0], 
                           [-2*a*b,1-2*b*b,-2*b*c,0], 
                           [-2*a*c,-2*b*c,1-2*c*c,0], 
                           [0,0,0,1]]; 

function mirrornv(nv)=mirrorabc(nv[0],nv[1],nv[2]); 

function mirror(v)=mirrornv(v/sqrt(v*v)); 

// Example of the use of these functions. 
// The following two commands both do the same thing. 
// Note that the order of transformations in both versions is the same! 

multmatrix(m=translate([20,0,0])*rotate([0,10,0])*translate([10,0,0])) cube(size=[1,2,3],center=false); 

translate([20,0,0]) rotate([0,10,0]) translate([10,0,0]) cube(size=[1,2,3],center=false); 

*/

            

/* Funcs that take a varying len of args:

LabelPt labels with formatted pt easily; Deprecating LabelPts, use MarkPts() which now has several subobjs: (sphere) marking, label, grid, chain

dist(pq), dist(P,Q)

*/

//. Ref

/* 3D Transformations:

http://help.autodesk.com/view/ACD/2016/ENU/?guid=GUID-44020C98-A94C-42C9-976B-D032FEF014E6


transformation:

Understanding the Transformation Matrix in Flash 8
http://www.senocular.com/flash/tutorials/transformmatrix/

Transformation matrix function library
http://forum.openscad.org/Transformation-matrix-function-library-td5154.html

Jar-O-Math! 
http://forum.openscad.org/Jar-O-Math-td8539.html


*/
  //: Lofting
/*
Lofting: https://visualizingarchitecture.com/lofting-basics-process/
*/
/*
Kit Wallace 3D polyhedron index
http://kitwallace.co.uk/3d/solid-index.xq?3D=yes

http://kitwallace.tumblr.com/tagged/polyhedra

https://github.com/openscad/openscad/wiki/OEP1%3A-Generalized-extrusion-module#discretized-space-curves

*/


/* 
---------------------------------------------

multmatrix( m = 
[
	 [ X, tzx ,tyx, x ]
	,[ tzy, Y, txy, y ]
	,[ txz, tyz, Z, z ]
	,[0, 0, 0, 1 ]
])

tzx: tilt with surface on xz plane unchanged and with the 
	 z-axis edge attached (unchanged ) and tilts along x direction.

---------------------------------------------
	A=[a,b,c]
	B=[x,y,z]

dot prod: 	A * B = ax+by+cz

cross prod:

	|A x B| = |A||B|cos(theta)

	A x B  = [bz-cy, cx-az, ay-bx ] 

	which is the determinant of : 

		 [[i,j,k]
		  [a,b,c]
		  [x,y,z]]

---------------------------------------------
3 vectors, A,B,C, forms a 3-D object.

The vol is : | A * ( B x C ) |

If it is 0, means A,B,C on the same plane 
	
	= abs( A * cross(B,C) ) == 0

*/ 
 
/*
Rotation Matrix
http://www.cs.brandeis.edu/~cs155/Lecture_07_6.pdf
*/

/*  Line formula in 3d space

  vector form of the equation of a line:

    P= Q +t*[a,b,c] 

    where [a,b,c] = Q-P

  parametric form of the equation of a line:

    P.x = Q.x + at
    P.y = Q.y + bt
    P.z =	 Q.z + ct

  symmetric equations of the line:

	( Q.x-P.x )/a = ( Q.y-P.y )/b = ( Q.z-P.z )/c 


    P.z =	 Q.z + ct

       
http://mathforum.org/library/drmath/view/65721.html

Good lesson:
http://tutorial.math.lamar.edu/Classes/CalcIII/EqnsOfLines.aspx

*/

/*
Formula in 3D space:
http://math.stackexchange.com/questions/73237/parametric-equation-of-a-circle-in-3d-space

A circle of radius r on the pqr plane, center at q:

x(a)= Q.x+ r*cos(a)*(P/norm(P)).x+rsin(a)*(R/norm(R)).x
y(a)= Q.y+ r*cos(a)*(P/norm(P)).y+rsin(a)*(R/norm(R)).y
z(a)= Q.z+ r*cos(a)*(P/norm(P)).z+rsin(a)*(R/norm(R)).z

*/



//. Ascii Lib


//===========================================
//
//       ASCII lib 
//
//===========================================



 //: Ascii Lines
 
/*_____________________________________________________________
  
\  :   &  `.  `-_  `-._  `'-.._
 \  :   &   `.   `-_   `-._    `'-.._
  \  :   &    `.    `-_    `-._      `'-.._
   \  :   &     `.     `-_     `-._        `'-.._
    \  :   &      `.      `-_      `-._          `'-.._ 
    65  65  65     50       40       30               23  
        
     65       48         38                25
     /       .'        _-`             _.-'`
    /      .'       _-`           _.-'`
   /     .'      _-`         _.-'`                __..--``12
  /    .'     _-`       _.-'`           __..--''``
 /   .'    _-`     _.-'`      __..--''``
/  .'   _-`   _.-'` __..--''``

_____________________________________________________________*/

 //: Ascii Axis
 
/*_____________________________________________________________
  

            |
     '-_    |
        '-_ | 
   --------'+----------
            | '-_
            |    '-_
            |       '   


           |
      ^.   | 
        ^. |      
    -------+--------
           |^.     
           |  ^.      
           |    ^.
           
  ;;       N (z)
;;       |
;;       |________
;;      /|'-_     |
;;     / |   '-_  |
;;    |  |      : |
;;    |  Q------|---- M (y)
;;    | / '-._/ |
;;    |/_____/'.|
;;    /          '-R
;;   /
;;  P (x)


;;       N (z)
;;       |
;;       |________
;;      /|'-_     |
;;     / |   '-_  |
;;    |  |      : |
;;    |  Q------|---- M (y)
;;    | / '-._/ |
;;    |/_____/'.|
;;    /          '-R
;;   /
;;  P (x)

	//	       N (z)
	//	       |
	//	       |
	//	      /|
	//	     / |         R2  
	//	    |  |       .' 
	//	    |  Q----_-`----- M (y)
	//	    | / `_-`
	//	    |/_-` '-_
	//	  T /--------'R1
	//	   /
	//	  P (x)
	//   /
	//  /
	// P2

	//	       N (z)
	//	       |
	//	       |
	//	      /|
	//	     / |       R2  
	//	    |  |     .' 
	//	    |  Q---_------- M (y)
	//	    | / '-:_
	//	    |/_''   '._
	//	  T /----------'-R1
	//	   /
	//	  P (x)
	//   /
	//  /
	// P2

                      N
                      |     _ Rx
                      |  _-`.`
              - - - - Q`- .:- - - - M 
                     / `.:    
                    / .:   `-_ 
                   /.:        `-_
                  J- - - - - - -R    
                 /
                P 
                
                A
               / '.
              /    \
             /      : 
       N    /       | X
       ^   /      _-```''-._   
       |  / b  _-`          `-_P
       | /  _-`  a  __..--''``
       |/_-`..--''``   
       Q-------------------> M
       

                           R
;;                        /|  
;;                       / |
;;                      /  |
;;                     /   |
;;                    /    |
;;        S.---------/--T  |
;;        |:        /   |  |   _.'
;;        | :      /    |  :.-'
;;        U.-:----/-----+-'
;;          '.:  /  _.-'
;;            ':/.-'
;;   N---------Q---------
;;       _.-' 
;;    P-'
;;  

;;                     D   S
;;                     :  /|
;;                     : / |
;;                     :/  |   
;;                     /   |  .'
;;                    /:  .|'
;;       B        :  / :.' |
;;  P    |        : / .C---:
;;  |'.  |   :    :/.'_.-''
;;  |  '.| --:----R--'------
;;  |    |.  :  .'
;;  |    | '.:.'
;; -|----|-_.Q -------    
;;  |.'_.|'.'
;;  '----A'
;;     .'

 
 ;;         |
;;         |     |      mode 1
;;     ----+-----|-.---
;;        /  '-_ |
;;       :      ':
;;         |
;;         |     |       mode 2
;;     ----+-----|-.---
;;        /      |/
;;       :-------/
;;      / 
;;         |_______    mode 3
;;        /|      /|
;;       /-------+ |
;;    ---|-|-----|-+---
;;       |/      |/
;;       :-----'-/
;;      / 

              
                .----------+----------+
               /|         /|         /|
              / |        / |        / |
             /----------+----------+  |
            /|  +------/|--+------/|--+  
           / | /|     / | /|     / | /|
          '----------+----------+  |/ |
          |  +-------|--+-------|--+  |
          | /|  +----|-/|--+----|-/|--+
          |/ | /     |/ | /     |/ | /
          +----------+----------+  |/
          |  +-------|--+-------|--+
          | /        | /        | /
          |/         |/         |/
          '----------'----------+
          
          
                             |
                             |
                    +--------|---+------------+
                   /         |  /            /
                  /          | /            /
                 /           |/            /
                +------------+------------+
               /            /|           /
              /            / |          /
             /            /  |         /
            +------------+------------+
                             |
                             |
                             
                             |   .
                             |  /|
                             | / |
                             |/  | /
                             +   |/
                    .-------/|---+------------.
                   /       / |  /|           /
                  /       /  | / |          /
                 /       .   |/  |         /
           -----+--------|---+------------+-----
               /         |  /|   '       /
              /          | / |  /       /
             /           |/  | /       /
            '------------+------------'
                        /|   +
                       / |  /|
                         | / | 
                         |/
                         '            
           
                              |   .
                             |  /|
                             | / |
                             |/  | /
                .------------+------------.
                |   .-------/|---+--------|---.
                |  /       / |  /|        |  /
                | /       /  | / |        | /
                |/       .   |/  |        |/
           -----+--------|---+------------+-----
               /|        |  /|   '       /|
              / |        | / |  /       / |
             /  |        |/  | /       /  |
            '------------+------------'   |
                '-------/|---+------------'
                       / |  /|
                         | / | 
                         |/        
_____________________________________________________________*/
 
 
  //: Ascii blocks
  
 /*_____________________________________________________________
         
      P3 
	   &'-_
	   |&  '-._     
	   | &     '-._
	   |  &        '-_ 
	   |   &         _'P2
	   |  b &      -' /
	   |     &  _-'  /	
	   |      X'    /
	   |  a -' &   /
	   |  -'    & /
	   |-'_______&
     P1            P4


                      N 
                ______|_________
               /:     |        /|  
              / :     +       / |
             /  :     |      /  |
            /_______________/   |
           | x  :_____|____|____'
           | :  /     Q----|-+-----  B
           | : /     /     |   /
           | :/     /      |  /
           | /J    +       | / 
           |/_____/________|/
           A     /
                P 
  
                            /  
                           / Z
                       /  / /    /
                      / _Q_/    /
                     _-`  /`-_ /
                    R    O----P
                     `-_   _-`
                        `S`
                        

              8
	//       /:`   
	//      / |: -
	//     /  | : `_ 
	//    /   |  :  '_
	//   /_.-'6-._:   `_
	//  5-._  |    :'-._`_  
	//  |   `'-._   :   `7
	//  |     |  `'-.4-'`|
	//  | _.-'2-._   |   |
	//  1-._       `'|._ |  
	//      `'-._    |   3
	//           `'-.:-'`
	//               0   
    

;;       _6-_
;;    _-' |  '-_
;; 5 '_   |     '-7
;;   | '-_|   _-'| 
;;   |    '-4'   |
;;   |    | |    |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0


;; In the graph below, P,Q,R and 0,4 are co-planar. 
;;
;;       _7-_
;;    _-' |  '-_
;; 6 :_   |Q.   '-4
;;   | '-_|  '_-'| 
;;   |    '-5' '-|
;;   |    | |    '-_
;;   |   _-_|    |  'R
;;   |_-' 3 |'-_ | 
;;  2-_    P|   '-0
;;     '-_  | _-'
;;        '-.'
;;          1


               
  
	//	      5-------4
	//	     /|      /|
	//	    / |  Q'-/----------C
	//	   /  |  /\/'-|       /
	//	  /   6-/-/---7'-.   /
	//       /   / / /  \/ '. R2/
	//	1-------0   /\   './
	//	|  / /  |  /  R1  /'.
	//	| / P.--|-/------D   A    
	//	|/    '.|/          /
	//      2-------3          /
	//               '.       /
	//                 '.    /
	//                   '. /
        //                     B
          
  //                                 vleg W    
  //                               |---------|
  //                            13 _________ 12         
  //                              /:       /|           
  //           ___             5 /_______4/ |                     
  //            |                | :     |  |                    
  //            |     15_________|_:14   |  |_11__________ 10    
  //       vleg len   / |        |/      |  /            /|      
  //           _|_  7____________|6      3/____________2/ |      
  //            |    |  :..............................|..| 9     ...
  //  w, hleg w |    | / 8                             | /        /  h
  //           _|_  0 /________________________________/ 1     _/_    -----> x-axis
  //                  
  //                  |------|--------------|
  //            /       L1 W    L1 len
  //          y-axis

  //                      L1 W    
  //                    |---------|
  //                  8 _________ 9         
  //                   /:       /|           
  //           ___  5 /_______4/ |                     
  //            |     | :     |  |                    
  //            |     | :     |  |_10__________ 11    
  //         L1 len   | :     |  /            /|      
  //           _|_    | :     3/____________2/ |      
  //            |     | :...................|..| 6    ....
  //    w, L2 w |     |/ 7                  | /        /  h
  //           _|_  0 /______________________/ 1     _/_    -----> x-axis
  //                  
  //                  |------|--------------|
  //            /       L1 W    L1 len
  //          y-axis  
                               11____________ 10
       15 _________ 14          /:          /|
         /:       /|           3___________2 |
      7 /_______6/ |           | :         | |
        | :     |  |           | :         | |
        | :     |  |_13________|_: 12      | |
        | :     |  /           | /         | |
        | :     5/_____________4/          | |
        | :................................|.. 9
        |/ 8                               |/
      0 /__________________________________/ 1
  

            leg1 W       gap1        leg2 W  
          |---------|------------|-----------|
                               19____________ 18   ___
       23 _________ 22          /:          /|     /    h 
         /:       /|          7/___________6 |   _/_   
      11/______10/ |           | :         | |    | 
        | :     |  |           | :         | |    |
        | :     |  |_21________|_: 20      | |    |  len, leg2L
        | :     |  /           | /         | |    |    
        | :     |/_____________|/          | |   _|_ 
        | :     9               8          | |    |
        | :8                               | |    | 
      0 | :      14 ..............15       | |    |  w (LW)   
        | :      .:             .:         | |    |
        | :    2_______________: :         | |   _|_
        | :     |  |          3| :         | |
        | :.....|..|13         | :16_______|.| 17      
        |/ 12   | /            |/          | /
       0/_______1/            4|___________|/5     -----> x-axis   

          leg3 W       gap2        leg4 W  
        |---------|------------|-----------|   
        
       
	// tube
	//             _6-_
	//          _-' |  '-_
	//       _-'   14-_   '-_
	//    _-'    _-'| _-15   '-_
	//   5_  13-'  _-'  |      .7
	//   | '-_ |'12_2-_ |   _-' | 
	//   |    '-_'|    '|_-'    |
	//   |   _-| '-_10_-|'-_    |
	//   |_-'  |_-| 4' _-11 '-_ | 
	//   1_   9-  | |-'       _-3
	//     '-_  '-8'|      _-'    
	//        '-_   |   _-'
	//           '-_|_-'
    //              0
    
   
	// chain                
	//       _6-_-------10------14
	//    _-' |  '-_    | '-_   | '-_
	//  2'_   |     '-5-----'9-------13
	//   | '-_|   _-'|  |    |  |    |
	//   |    |-1'   |  |    |  |    |
	//   |   _-_|----|-11_---|--15   |
	//   |_-' 7 |'-_ |    '-_|    '-_| 
	//  3-_     |   '-4------8-------12
	//     '-_  | _-'
	//        '-.'
	//          0
  
               13-------------9
             _-`|`-_______10-` `-_
          _-'   _-`|       |`-_   `-_ 
      17.'____.' -_|_____11|   `-6____.5
        |`-_  |`-_-`        `-_-`| _-`| 
        |   `-_ _-`-22____2_-`  _-`   | 
      16|_____|`-_/_|______|%_-` |7__ |4
         `-_   21|  |      | |1 `  _-`
            `-_  |  |23___3| |  _-`
               `-|-/________%|-`
                 20          0
 

       
	//                          _-5
	//                       _-'.' '.
	//                    _-' .'     '.
	//                 _-'  .'         '.
	//              _-'   _6    _-Q----_-4--------------A 
	//            1'   _-'  '_-'   '_-'.'            _-'
	//          .' '.-'   _-'  '._-' .'           _-'
	//        .' _-' '. -'    _-'. .' '.       _-'
	//      .'_-'   _-''.  _-'   _-7    R1  _-'
	//     2-'    P-----0-'----------------B     
	//      '.         .'  _-'
	//        '.     .' _-'
	//          '. .'_-'
	//            3-'
	//        

	//	      5-------4
	//	     /|      /|
	//	    / |  Q'-/----------A
	//	   /  |  /\/  |       /
	//	  /   6-/-/---7      /
	//       /   /  \/   /      /
	//	1-------0   /\  	  /
	//	|  / /  |  /  R1  /
	//	| / P.--|-/------B
	//	|/    '.|/          
	//      2-------3          
	//           
    

_____________________________________________________________*/


 //: Ascii arc/angle
 
/*_____________________________________________________________
  
   
       B-----------_R-----A 
       |         _:  :    |
       |  T._---:-----:---+ S 
       |  |  '-:_      :  | 
       |  |   :  '-._   : |
       |  |  :     a '-._:| 
       p--+--+-----------'Q
          U  D    


                     _R 
                   _:  :
                  :     : 
                 -       :
                :         : 
               -           : 
       p ------S-----------'Q




                     _R 
                   _:  :
                D :     : 
                 -'-_    :
                :    '-_  : 
               -     a  '-_: 
       p ------S-----------'Q                   
                    
 
                       _R 
                  S _-`  
                 _-`:      
              _-`    :       
           _-`        :          
        _-`           :            
      Q---------------: P
             rad (radius of curve= dPQ if not given)

                       _R 
                  S _-`  
                 _:`: :    
              _-`  : : :     
           _-`      : : :        
        _-`         : : :           
      Q-------------:-P-:
      |..... rad .....|-|  
                       r  (base radius of the curve line PS)
    
                     _._       C: center of xsec 
                   -` | `-     rss: radius around C 
     Q------------|---C---|            
                  '_  |  _`  
                    `'-'`


            Q
           _|_ 
      _-'`` | ``'-_
    -(------P------)----- 
      `'-.._|_..-'`
            |      | 
            |<---->|
                r    
              
    //     0
    //     | 
    //  _4-|-3_
    // 5_  +  _2
    //   -6-1-
   
          	. R  						 	       _
             _-' -      
          _-'     :
       _-'         :     
    _-'            : 
  Q+---------------+ P
  
                R  |N (N is on pl_pqr, and NP perp to PQ )
             _-' : | 
          _-'     || 
       _-'        _|_ 
    _-'      _--'' | ''--_
  Q+--------{------P------}----- 
             ''-.__|__.-''
                   |      | 
   |<------------->|<---->|
           r           r2  
           
               N
          .:'`|`':.
        :`    |    `:
       :_____Q|______:____M
       :     /|`.    :
        '_  / |  `._'
          '/._|_.-'`. 
          /          R
         P
            _.--+--._
         .'`    |    `'. 
        /`      |       \
       :        |        :
       +--------|--------+
       :        |        :
        \       |       /
         `:_    |    _:`
            `'--+--'`
     
           .:'`|`':.
        :`    |    `:
       :______|______:
       :      |      :
        '_    |    _'
          '-._|_.-' 

      
             ___
         .:'' | ''-
        /     |  / 
       :      | / 
       +------+'-----+
       :      |      :
        :     |     /
         ':.._|_..:'
          
                   

       B----_R-----A--._ 
       |      :    |    ''-_
       |       : S +--------T 
       |        :  |      -'|
       |         : |   _-'  |
       |          :|_-'     |
       p----------'Q--------U

            a = a_PQT
     _R    B-----------A--._ 
       `-_ |      :    |    ''-_
          `|_      : S +--------T 
           | `-_    :  |      -'|
           |    `-_  : |   _-'  |
           |       `-_:|_-'     |
           p----------'Q--------U
           

;;      N-_
;;         `-_
;;    ---B    M
;;        `.   :
;;    C-----A---L---P0   
;;        pts1  pts2
;;       
;;       N B2
;;         `-_
;;    ---B    M
;;        `M1  :
;;    C-----A---L'---P0   
;;        pts1  pts2


_____________________________________________________________*/


 //: Ascii Misc
 
/*_____________________________________________________________
 
              _.Q
            _- /|
          _'  / |
       _-'   /  |
      P-----D---R
    
 

    
         S         0.       P
          `.    _-`  '.   .`
            `..'   4   `.` 
            -``._-` `-_` `. 
          -`   9`.  .` 5   `.  
        3`     |  `O   |     `1
         `.    8 .` `. 6   _-`
           `.  .`-_ _-`. _'
             `.    7   _'.
            .` `.    .'   `.                -`
          .`     `2`        Q
          R
              
              
            G--...__
    dH1=   :        ``H
     dQR  :   _.---._  : dH2 = dQR (default)
         :_-``       `-_:
        Q`---------------R       
       : \`'-_      _-'`/ :
      :   \   A1  A2   /   :
           \          /     : 
          X=P        S
 
        
           Q           ___
           ^.           :
          /: ^.         :   
       a / :   ^.  b    : h
        /  :     ^.     :
       /   :-+     ^.   :    
     P-----D-'-------R ---

	 |------ c ------|  
     
  
  
;;       6
;;  +----N-----+
;;  : 2  |  1  :
;;  :    |     :
;; 7-----P-----R- 5
;;  :    :     :
;;  : 3  :  4  :
;;  +----:-----+ 
;;       8         

     
//         size
//   p---------------n
//   |'-           -'|
//   |  '-       -'  |
//   |    '-   -'    |
//   |      'x'      |
//   |        '-     |
//   |          '-   |
//   |            '- |
//   m---------------q

/*
;; Given [p,q,r], return [P,Q,N,M]
;;   P-------Q
;;   |      /|
;;   |     / |
;;   |    /  |
;;   M---R---N
;;
;;   P------Q
;;   |'-_   |:
;;   |   '-_| :
;;   |      '-_:
;;   |      |  ':
;;   M------N    R
;;
;; New 2014.7.15: new OPTIONAL arg: p, r to set size. 
;;
;;          p  
;;   P---|-----Q
;;   |   |    /| 
;;   |   |   / | r
;;   |   +--/---
;;   |     /   |
;;   '----R----'

                                   Q
                      J         _-`|
                    --+-------M`---K---R----S
                      |   _-` 
                      |_-` m 
                    _-P
                    
                  .------.
                .'        `.
              .`     .      `.
               `.          .'
                 `.______-`
                    
 
                    
        +..__
     rP |    ``''--..
        |           | rR
        +-----------+
        P           R                    
                    
//        _-'|          |'-_ 
//      P----A----------B----Q
//        '-_|          |_-'  
//           '          '                  
                    
//;;    corner       mid1          mid2         center
//;;    q-----+      +-----+      +-----+      +-----+
//;;   / :     :    p :     :    / q     :    / :     :
//;;  p   +-----+  +   +-----+  +   +-----+  + p +-----+
//;;   : /     /    : q     /    p /     /    : /     /
//;;    +-----+      +-----+      +-----+      +-----+
//;;
//;;    corner2
//;;    +-----+
//;;   / :     :
//;;  +   q-----+
//;;   : /     /
//;;    p-----+

 




     dPR = dQR = dPQ   and OR = OP = OQ
        
                  R _     r     
            el  .'|\ ''--._     
             .'   | \      ''--. O (ORIGIN)
       Q  .'      |  \ el  _.-'/ 
        ':._      |   \.-'`   /  
            '-._..J'`  \     / r
              M '-._    \   /
           el       '-._ \ / 
                        '-'P                  

    
;;                         R
;;                        :
;; |       .--------.    :  |
;; |     .'          '. :   |  
;; |   P'--------------Q.   | 
;; | .' pa            qa '. |
;; |------------------------|



             R      
            / '-_
           /     '-_
          /         '-_
         /             '-_
       P -----------------'Q

               


         |    arrowP                   arrowQ  |
         |  _='                           '=_  |  
    hP=iQ|-'------------iP      iP-----------'-| hQ=iQ
         | '-_          |<- gap->|         _-' |
         |    '                           '    |
          <-- spacer                            <-- spacer      
         P-------------------------------------Q 

            /  
           /'-._
          /     '-._  /
        P'-._       '/
         |    '-._  /        
         |        'Q
         |        /| 
         |       / Ro
                R
            /                    
           /'-._
          /     '-._  /
        Q'-._       '/
        /|    '-._  /        
       / |        '| P
      R  |         | 
         Ro                                          

    //-------------------------------------------- guide 
    
            gP2         gQ2  --- 
             |           |    |
          hP +-----------+ hQ | g_len  
             |           |    |
         gP0=gP1         gQ1 ---
    spacer-->            |    |
             Po          |    | gQx_len  
             | '-_       |    |  
             X    '-_   gQ0  ---       
             |       '-_    <--- spacer    
             |          'Qo 
             |           |
             |           Ro     
             
            followslope= true:  R decides the plane and side  
                /               where the dim line lies.   
               /'-._
              /     '-._  /
             P'-._      '/
             |    '-._  /        
             |        'Q
             |         | 
             |         R 

            false:

             |_________|   R further decides direction of guides
             |         |   and dim line. 
             P'-._     |
             |    '-._ |        
             |        'Q
             |         | 
             |         R     
             
;;                     D   S
;;                     :  /|
;;                     : / |
;;                     :/  |   
;;                     /   |  .'
;;                    /:  .|'
;;       B        :  / :.' |
;;  P    |        : / .C---:
;;  |'.  |   :    :/.'_.-''
;;  |  '.| --:----R--'------
;;  |    |.  :  .'
;;  |    | '.:.'
;; -|----|-_.Q -------    
;;  |.'_.|'.'
;;  '----A'
;;     .'
             
                      
_____________________________________________________________*/


//. Old History (before git)

//===========================================
//
//       Old History 
//
//===========================================

old_scadex_ver=[
["20150202_1", "getsubops: fixed warning (beg shouldn't be larger 
than end) by inserting let(h=isarr(h)?h:[],g=isarr(g)?g:[]) to update()"
] 
,["20150201_1", "updates() now can take one argument (instead of 2); Still debugging getsubops about beg shouldn't be larger than end warning."] 
,["20150131_1", "getsubops revised; tested"] 
,["20150130_1", "kidx(); Updated update();"] 
,["20150125_5", "Cone() new arg: twist."] 
,["20150125_4", "arcPts() new arg: twist."] 
,["20150125_3", "Arc() done."] 
,["20150125_2", "angleBisecPt: new arg len, ratio; hash:let notfound be shown when value is set to undef; Chainf(): fix bug."] 
,["20150125_1", "Chainf(): formatted Chain; Arc in progress"] 
,["20150124_1", "Change Arrow args from arrow to head. Text() in progress"] 
,["20150123_1", "Dim2's guides and arrows seem to be done. "] 
,["20150122_1", "hashex done. getsubops seems tested ok, checking if can use in Dim2 "] 
,["20150121_2", "getsubops in progress. hashex in progress "] 
,["20150121_1", "Dim2 in progress. "] 
,["20150120_1", "Arrow doc and demos. "] 
,["20150119_2", "Arrow (the line and arrow faces not yet aligned. "] 
,["20150119_1", "Cone(), w/new feature: set markpts on the fly"] 
,["20150117_2", "CurvedCone_demo_1() cleanup"]
,["20150117_1", "getSideFaces(), tested"
              , "getTubeSideFaces(), tested"
              , "mergeA(), tested"
              , "CurvedCone, CurvedCone_demo_1()"
               ]
,[ "20150116-1", "CurvedCone first step done. Check CurvedCone_demo_1()", 
    "Change some argument name r to len" 
]
,[ "20140921-1","Change args of isOnPlane()" 
]
,[ "20140918-1","New:roll(arr,count)" 
]
,[ "20140916-1","join(): change default sp from , to empty str."
			,"numstr(): change arg maxd back to dmax." 
]
,[ "20140914-1","New ranges(): similar to range, but return array of arrays."
			, "Both range() and ranges() now return only indices. No more return obj content. Both need docs." 
]
,[ "20140912-1","range(): Revert to entire 0909-1. Re-design range to have simpler args: range(i,j,k, cycle)." 
]
,[ "20140910-2_segmentation_fault","range(): revert this func back to 0909-1, but it causes segmentation fault. Next version: copy entire 0909-1" 
]
,[ "20140910-2","range(): revert it back to 0909-1." 
]
,[ "20140910-1","Re-design range() for partial obj ranging: not yet succeed but already very very slow (too much condition checks in each call). Will revert it back to 0909-1 in next ver." 
]
,[ "20140909-1","Re-design range(): remove args: maxidx, obj; allow cycle= i,-i" 
]
,[ "20140901-1","New:vlinePt()." 
]
,[ "20140831-2","onlinePt: change arg name dist to len." 
]
,[ "20140831-1","Misc improvements. (mainly Rod())" 
]
,[ "20140825-1","New: twistangle()" 
]
,[ "20140823-1","New arg s for angle().","New quadrant()" 
]
,[ "20140822-3","faces() demo of shape ''tube''. Pretty cool." 
]
,[ "20140822-2","New: rodPts(). Makes Rod() uses it."
			, "New: rodfaces(), faces()" 
]
,[ "20140822-1","Rod(): Fix another rotation bug. Doc done, too."
			, "LabelPts: default prefix changed from P to '', begnum from 1 to 0" 
]
,[ "20140821-3","Rod(): Fix rotation bug found earlier today"
]
,[ "20140821-2","Rod(): New args markrange, markOps, labelrange, labelOps; remove showsideindex",
	"Rename mdoc() to parsedoc()"
]
,[ "20140821-1","Bug in Rod : sometime rotate to wrong side. See Rod_demo_8_rotate()"
]
, [ "20140820-1","New mdoc(), _mdoc()"
			  , "Fix major Rod bugs (guess so). More demo with _mdoc"
]
,[ "20140819-1","New Rod arg: rotate; more Rod demos (including gears)"
]
,[ "20140818-3","New: subarr()"
]
,[ "20140818-2","Revised range(): add new args: obj and isitem"
]
,[ "20140818-1", "Revised fidx(o,i): o can be an integer"
			 ,"Revised range(): remove arg obj, add new arg cover and maxidx"
]
,[ "20140817-2", "New: lpCrossPt()"]
,[ "20140817-1", "Rename: rodfaces() to rodsidefaces()"
			 , "New: Rod()"]
,[ "20140816-3", "New: rodfaces()"]
,[ "20140816-2", "New arcPts, good implementation. "]
,[ "20140816-1", "New for LabelPts: args: prefix, suffix, begnum, usexyz; Use new builtin text()"]
,["20140815-1","Done with the migration to OpenSCAD 2014.05.31 snapshot in terms
			of doctest behaviors. Need to fix shape making. Somehow it seems that
			the rotation of points goes wrong."]
,["20140814-1","New:dels()"
	, "Improved: isequal(), now can take var of non-number. tests done"
	, "is0: doc and tests done."]
,["20140812-1","Fix bug: endwith(), istype(). Doc and tests done."]
,["20140811-2","New: begwith and endwith can check if beg/end with any of [a,b,...] ", "Working on: new istype(), improving uconv()"]
,["20140811-1","New: splits()", "Rename: trueor()=> or()"]
,["20140810-1","Disable neighbors(). The new range() can cover it."
			, "consider to disable rearr(). The new list comp can handle it.==> maybe not"
			, "Make range() accept str as the 1st arg as well, so range(''abc'')=[0,1,2]. This is used in reversed()"
			, "New arg for fidx: fitlen"
			, "Simplify slice code, allowing reversed slicing" 
			, "New: joinarr()"]
,["20140809-1","Rename all *iscycle* to *cycle*"
			, "Del range() and copy range2() code to range()"
			, "range2(): new arg: obj,cycle. Doc and test done."
]
,["20140808-3","Remove fitrange(). It can be replaced by fidx()"
			,"New: range2(), much elegantly written than range()."
			,"Re-evaluating the need of neighbors ... it might be able to use range2() instead"
]
,["20140808-2","New function: fidx(). This should be very powerful. "
]
,["20140808-1","Disabled for future removal: mina, maxa, inc, hashi, hashkv"
			,"Renamed: multi()=>repeat(), midCornerPt()=>cornerPt()"
]
,["20140807_1", "START CODING WITH 2014.05.31/Linux 64bit"
			, "Remove addv (too easy to code with list comp so no need a func)"
			, "New arg for del(): *count*."
			, "New func: fitrange() (consider to replace inrange in the future )" 
			, "Re-chk code for use of OpenScad_DocTest for doc testing --- down to hash()"
		    
]
,["20140730_1", "New: prependEach(), permute()"
]
,["20140729_1", "New:switch()", "New: ucase(), lcase()"
			,"New: Error message reporting in switch(). Consider apply it to other functions in the future"
]
,["20140715_5", "New:cubePts(), Cube() done (Cube() needs doc)"]
,["20140715_4", "squarePts(): fix runtime error. "]
,["20140715_3", "Dim(): Fixed bug (S= squarePt(pqr,[''mode'',4]): remove mode. "]
,["20140715_2", "New: addv()"]
,["20140715_1", "New optional args p,q for squarePts() to set sizes." 
             , "_TODO_: to change boxPts from anti-clockwise to clockwise for pqr polyhedron geometry making. --- may not need it after squarePts() has p,r "]
,["20140714_1", "New: p012(pts) ...p320(pts) 24 funclets"
			, "New: p2(), p3(), p4() 3 funclets" ]
,["20140713_2", "numstr(): change arg dmax to maxd; Dim(): new arg maxdecimal." ]
,["20140713_1", "New args for Dim(): conv, decimal." ]
,["20140712_1", "numstr() done." ]
,["20140711_4", "New: uconv() --- need more tests." ]
,["20140711_3", "num() now can convert f'i'' to feet." ]
,["20140711_2", "New: ftin2in()" ]
,["20140711_1", "New: num(numstr)=>num" ]
,["20140710_1", "Working on: new: numstr() for Dim() to use" ]
,["20140709_2", "rand() can take an array or string, return one item randomly." ]
,["20140709_1", "Introducing groupdoc feature and apply it to addx/addy/addz" ]
,["20140708_3", "addx, addy, addz improved, plus their docs and tests. Remove add_z_to_pts" ]
,["20140708_2", "rename doctest to doctest_old, doctest2 to doctest; MOVE this new doctest to a new file scadex_doctest.scad" ]
,["20140708_1", "doctest2: simplify doc from fname=[[fname,arg,retn,tags],doclines] to [fname,arg,retn,tags,doclines]; also using ; instead of \\ for linebreak in doclines." ]
,["20140707_2", "More demos for anyAnglePt." 
]
,["20140707_1", "anglePt bug fixed: failed when a=90 (detected by anyAnglePt_demo_4();" 
]
,["20140703_1", "neighbors(), neighborsAll(): change default of cycle to true"
		   , "normalPts(): fix bug (arg *len* was written as dist in the code)"
		   , "normalPt: change default:len=undef, new: reversed=false" 
]
,["20140701_1", "angle(): use simpler formula.","anyangle()", "P(),Q(),R(),N(),M()"
			 ]
,["20140630_1", "randOnPlanePt: new arg a (for angle); and change the center of pts from incenterPt to Q" ]
,["20140629_1", "anglePt(): change angle to aPQR (originally aQPR) and reaname arg from (pqr, ang, len) to (pqr,a,r).", "Fix randOnPlanePts (it uses anglePt) accordingly" ]
,["20140628_2", "normalPt(), normalPts()"]
,["20140628_1", "expandPts(). cycle=false needs fix."]
,["20140627_4", "neighbors(), neighborsAll()"]
,["20140627_3", "mod()", "remove increase(). Has an inc() already."]
,["20140627_2", "get() got a new arg cycle", "In work: intersectPt(), expand()"]
,["20140627_1", "Output scadex version info when loaded."]
,["20140626_5", "LabelPts_demo_2_scale()"]
,["20140626_4", "increase(pts,n=1)", "LabelPts and MarkPts_demo() can set labels=range(pts) instead of range(len(pts))"]
,["20140626_3", "LabelPts Bug fixed.(demo with LabelPts_demo() and MarkPts_demo())"]
,["20140626_2", "MarkPts: Now can set labels", "LabelPts Bug found: fail to output label when pts contains only one item."]
,["20140626_1", "ishash()"
			, "Improved LabelPts arguments (shift)"
			, "range can do range(arr)"]
,["20140625_5", "Pie() 2D"]
,["20140625_4", "projPt() code, doc and test done."]
,["20140625_3", "Normal() new argument:reverse; Also show arrow but not just line."]
,["20140625_2", "Revising Arc()=> failed. But made a good 2D pie."]
,["20140625_1", "between() code, doc and test done."]
,["20140624_7", "randRightAnglePts improved (allows for dist= a point or an integer."]
,["20140624_6", "Bugfix for pick() and shuffle()."]
,["20140624_5", "shuffle() code, doc and test done."]
,["20140624_4", "pick() code, doc and test done."]
,["20140624_3", "range() code, doc and test done. "]
,["20140624_2", "randRightAnglePts() improved to allow fixed sizes right tria. "]
,["20140624_1", "randRightAnglePts() done. "]
,["20140623_4", "det(), RMs()", "Working on randRightAnglePts()"]
,["20140623_3", "Fix randOnPlanePt(), randOnPlanePts()"]
,["20140623_2", "Bug fix for anglePt()"]
,["20140623_1", "p021, p120, p102, p201, p210", "rearr()", "incenterPt()", "Bug in doctest when mode= 2"]
,["20140622_3", "randInPlanePts(), randOnPlanePt, randOnPlanePts():this one needs fixes"
]
,["20140622_2", "rand()", "p01() ... p03() ... p30()", "randInPlanePt()"
]
,["20140622_1", "lineCrossPt(), crossPt()", "dot()", "is0()", "prodPt()", "addx, addy, addz, dx, dy, dz"
]
,["20140620_1", "Default *closed* param in Chain set to true"
 , "Mark90 seems to have false-positive error"
 , "making anglePt() ==> doesn't work very well. Need more work"
 , "include PGreenland's TextGenerator."
 , "COLORS for default colors. Set LabelPts default colors to COLORS"
]
,["20140619_3", "Plane improved: len(pqr) can be >3. But all faces connect to pqr[0]"]
,["20140619-2", "LabelPt(), LabelPts(). Both need doc. "
  ,"randPt, randPts x,y,z range doesn't seem to work", "LabelPts()" ]
,["20140619-1", "ColorAxes()"]
,["20140618-1", "Dim(), needs fixes on the angle when vp=false"]
,["20140613-5", "Rename: dx=>distx, dy=>disty, dz=>distz."]
,["20140613-4", "Done rewriting doc and test to fit new doctest."]
,["20140613-3", "new func: select()"
		,"Rewriting doc and test to fit new doctest ... down to split()"]
,["20140613-2", "newx(), newy(), newz()", "tests for isOnlinePt, planecoefs"
		,"Rewriting doc and test to fit new doctest ... down to planecoefs()"]
,["20140613-1", "New: isOnPlane()"
		,"Rewriting doc and test to fit new doctest ... down to Plane()"]
,["20140612-2", "onlinePt takes only 3d pts.", "Random tests for onlinePt()"
		,"Rewriting doc and test to fit new doctest ... down to onlinePts()"]
,["20140612-1", "Done migration from index(x,o) to index(o,x)."]
,["20140611-3", "About to make a big change to change index(x,o) to index(o,x) to be 
consistent with other functions."]
,["20140611-2", "doctest: change arg from (doc,recs=[], ops=[], ops2=[]) 
to (doc,recs=[], ops=[], scope=[]). So we can do:
 doctest(doc, recs, ops, [''i'',3])
but not 
 doctest(doc, recs, ops, [''scope'',[''i'',3]] )
"]
,["20140611-1", "doctest: for functions difficult to test (like, _fmt), testcount is 0, 
so do not show 'Tests:', show 'Usage:' instead."
]
,["20140610-5", "scadex_doctest: some improvements."
			, "Rewriting doc and test to fit new doctest ... down to echoblock()" ]
,["20140610-4", "scadex_doctest: some improvement on its header" ]
,["20140610-3", "doctest(): header adds some highlighting; need re-factoring but we'll leave it for now. " ]
,["20140610-2", "doctest() done. " ]
,["20140610-1", "Rename scadex_test to scadex_doctest and improve header, footer" ]
,["20140609-3", "Fine-tuning doctest()"
		, "On the way to change doctest mode 0 to list all func names" ]
,["20140609-2", "DT_MODES; New doctest mostly done. Needs only minor adjustment. " ]
,["20140609-1", "Combining doc() and test() into doctest() about complete. " ]
,["20140608-1", "Combine doc() and test() into doctest(). They were previously
together but separated around 5/29. Put them back to doctest(), and aiming to
have doc() and test() call doctest() with specific parameters." ]
,["20140607-1", "Using concat (instead of update) to setup ops in test(). Will gradually adapt this pattern for all modules." ]
,["20140606-3", "New module Normal(pqr)", "Fix bug in angle() for angle >90"
			, "MarkPts: Fix err that [''grid'',[ops]] doesn't have effect" ]
,["20140606-2", "New ops *funcwrap* for test. Chk angleBisectPt_test() for example"]
,["20140606-1", "New func angle(), _fmth() ", "test() uses isequal() for number comparison" ]
,["20140605-4","Rename h_() to _h(), s_() to _s()"]
,["20140605-3","test() refined."]
,["20140605-2","New func subops(); test() now can show doc (when [''showdoc'',true])."]
,["20140605-1","New function _getDocLines(), so doc can be called to either doc() or test()."]
,["20140604-1","doc for test() finished."]
,["20140603-1","Rename midAnglePt to angleBisectPt"
	,"Rename pts_2d_2_3d to add_z_to_pts"
	,"Done revising doc of each funcmod for new doc() from 
      top down"]
,
["20140601-2","New: isbool, isequal", "Remove ishash, LineNew"
	,"Disable midAngleCrossLinePts()"
	,"Revising doc of each funcmod for new doc() from 
      top down ... to midAnglePt() "]
, 
["20140601-1","Revising doc of each funcmod for new doc() from 
              \\ top down ... to isarr() "]
,["20140531-1","Remove fontPts(), hasht()", "Rename hashitems to hashhkvs"
		, "When showing doc header, replace _ with ＿ (_ -> ＿) to make it higher" 
		, "Revising doc of each funcmod for new doc() from top down ... to hashkvs "]
,["20140530-7","Remove Box()"
		 , "Revising doc of each funcmod for new doc() from top down ... to echoblock."]
,["20140530-6","Revising doc of each funcmod for new doc() from top down ... to Block."]
,["20140530-5","doc(): better algorithm to speed up. Also allows for correct \\
	display of mono-spacing text. We finally have a system that displays code and \\
	arranged ascii graph correctly in both the code editor and console. "]
,["20140530-4","doc(): new doc format that uses double back-slash \\
	to indicate line-breaking", "_tab(): convert tab to html spaces"]
,["20140530-3","test() done, including scoping for test records "]
,["20140530-2","test() mostly done. "]
,["20140530-1","test() basic done. Still buggy."]
,["20140529-3","Start to seperate doctest into doc() and test(). doc() done."]
,["20140529-2","Basic feature of doctests works fine."]
,["20140529-1","Rename fmt to _fmt. Failed to make deep fmt into array."]
,["20140528-4","console html tools", "Working on doctest2", "fmt()"]
,["20140528-3","onlinePts()"]
,["20140528-2","PtGrids for multiple points. Note that the 'scale' feature in PtGrid is disabled now. "]
,["20140528-1","Disgard hashif; add new arg to hash: if_v, then_v, else_v"]
,["20140527-3","hash: fix default argument recursion error", "working on hashif()"]
,["20140527-2","Mark90 auto adjusts the size of L to fit small pt xyz"]
,["20140527-1","Remove PtFrame; Remove the DashLine style op in PtGrid"]
,["20140526-1","Rename PtGrids to PtGrid and significantly simplies it to reduce process time, including change the use of DashLine to Line"]
,["20140525-1","accum()", "onlinePts()"]
,["20140524-1","randPts() allows setting x,y,z = 0 to put pt on plane or axis."]
,["20140522-6","Rename Line0 to Line_by_x_align and LineNew to Line0"]
,["20140522-5", "Dashline: Fix bug of over-draw when dash > line length  " ]
,["20140522-4", "LineNew()." ]
,["20140522-3", "RodCircle: done. Seems rodrotate in all conditions are good." ]
,["20140522-2", "RodCircle:Fix problems with the rodrotate --- almost" ]
,["20140522-1", "RodCircle, almost done", "inc()", "pts_2d_2_3d()", "Fixed arcPts: at a=360, should remove the last one, but not the first" ]
,["20140521-3", "randc() for random color", "OPS as a common ops for all shapes" ]
,["20140521-2", "New func: updates(h,hs) for multiple updates."
			, "Remove the feature of multiple hashes in update." ]
,["20140521-1", "Improved update: can update up to 3 hashes at once" ]
,["20140520-4", "Working on : More Rings; improving update" ]
,["20140520-3", "One more demo for Ring" ]
,["20140520_2-2", "Done docs for new hash design. Ready to merge back to scadex" ]
,["20140520_2-1", "Found that all files before this have wrong year tag (2013 should have been 20140. Embarrassing.", "Finish the doc for rotangles_p2z and rotangles_z2p" ]
,["20140518-3", "New: hash2 using [k,v,k,v ...] but not [[k,v],[k,v]...] as data. " ]
,["20140518-2", "Ring: add feature to mark rod colors with ''markrod''", "Ring_demo() --- weird circle line around the line through. " ]
,["20140518-1", "Line: new arg extension0/extension1 for line extension", "arcPts: add ''a==360&&_i_==0?[]:'' to skip _i_=0 when a=360"
			,"shortside(),longside()" ]
,["20140517-4", "New module Ring()"]
,["20140517-3", "New function dist(pq) (same as norm(p-q))", "onlinePt got new argument *dist*"]
,["20140517-2", "Rename all rm (rotation matrix) to RM."]
,["20140517-1", "Let rm2z uses rotangles_p2z"]
,["20140516-3", "Revising doc down to reverse()", "replace(): add replacing array item"]
,["20140516-2", "Mark90()"]
,["20140516-1", "is90()"]
,["20140515-1", "Found Line's bug source in rotangles_z2p( Not in rotangles_p2z). Fixed"]
,["20140514-5", "Fix bug (unable to rotate [x,y,0] to z) in rotangles_p2z. The bug in Line still there"]
,["20140514-4", "The bug in Line could be due to the new-found bug in rotangles_p2z -- unable to rotate [x,y,0] to z-axis. "]
,["20140514-3", "Revising function docs to use new doctest"
  , "Found bug in Line (but not in Line0): unable to draw lines on xy-plane (like [[1,1,0],[-1,2,0]]. "]
,["20140514-2", "Revising function docs to use new doctest"
  , "Add *markpts* argument to Line0 and Line"]
,["20140514-1", "Revising function docs to use new doctest", "Rename getd() to trueor()", "Make hash return [v1,v2...] when item = [k,v1,v2...]", "Rename the argument *strick* in ishash to *relax"]
,["20140513-6", "Revising function docs to use new doctest", "Remove h_ feature from echo_ and create a new func echoh"]
,["20140513-5", "Revising function docs to use new doctest", "Func del allows byindex" ]
,["20140513-4", "Revising function docs to use new doctest", "Rename countType??? to count???" ]
,["20140513-3", "doctest: fine tuning; Revising function docs to use new doctest" ]
,["20140513-2", "doctest: fine tuning; Revising function docs to use new doctest" ]
,["20140513-1", "doctest: fine tuning, looks good" ]
,["20140512-4", "doctest: fine tuning, new arg: mode" ]
,["20140512-3", "doctest done, remove DT_old and doctest3" ]
,["20140512-2", "doctest3 clean up", "rename: doctest=>DT_old, doctest3=>doctest" ]
,["20140512-1", "doctest3, mainly based on doctest, works fine." ]
,["20140509-2", "sum(arr)","Trying doctests, good idea, almost done, but extremely slow, maybe more than 100x slower than doctest, has to give up" ]
,["20140509-1", "arrblock()" ]
,["20140507-3", "Remove Arrow()", "Adding thickness for plane(working)" ]
,["20140507-2", "Plane: allows for changing frame settings " ]
,["20140507-1", "randPt() (only one pt)" ]
,["20140506-1", "Block " ]
,["20140505-4", "Plane: done mode 1 ~ 4 " ]
,["20140505-3", "squarePts()" ]
,["20140505-2", "Plane: mode 1 done" ]
,["20140505-1", "Chain: change argument pattern to ops" ]
,["20140504-2", "Plane: allows 4-side plane", "midPt()", "midCornerPt()" ]
,["20140504-1", "Line: failed to make touch work","Rename Surface to Plane" ]
,["20140503-4", "Line: found mechanism to find the p3rd -- the pt on surface. Still under construction." ]
,["20140503-3", "Line: let shift apply not only to block but to line, too", 
				"Remove Line2. In Line, rename blockAnchor to shift" ]
,["20140503-1", "Line style block: allows any shift" ]
,["20140502-4", "echom(modulename).", "Detailed Line_demo_???"
			, "Line:Fix rotate in block style" ]
,["20140502-3", "Del Line, rename Line3 to Line, style=block to be fixed."]
,["20140502-2", "module Line0, to replace Line as the simple line module."]
,["20140502-1", "Fix Line3 (same problems also in Line2) head/tail at wrong side when switched"]
,["20140501-2", "Working on Line3 head/tail cone"]
,["20140501-1", "rotangles_z2p() made with RMz2p_demo(). Leave RMz2p_demo for future references", "Rename rot2z_angles to rotangles_p2z"]
,["20140428-1", "rmx(), rmy(), rmz(), rm2z(), rot2z_angles()"]
,["20140425-3", "Complete PtGrid new scale feature and doc"]
,["20140425-2", "hasAny()", "arrItcp()"]
,["20140425-1", "has()", "Add 'scale' to PtGrid()"]
,["20140424-1", "Remove dot(pq) ==> simply p*q; PtGrid takes new arg 'scale'"]
,["20140423-4", "Surface()","distPtPl()", "dot(pq)"]
,["20140423-3", "Line2: Improved. New shift:corner2."]
,["20140423-2", "Line2: Improve doc."]
,["20140423-1", "Line2: Rename style panel to block."]
,["20140422-6", "Add new style panel to Line2"
		     , "randPts() take new argument r."]
,["20140422-5", "Fix Line2 head/tail color and transp issues."]
,["20140422-4", "Re-organize Line2 examples."]
,["20140422-3", "Bugfix Line2."]
,["20140422-2", "Add fn arg to DashLine, Line"
			,"Fix arrow position problem in Line2" ]
,["20140422-1", "Rename markPts to MarkPts. Improve MarkPts()"]
,["20140421-1", "markPts(), randPts()"]
,["20140420-3", "Line2(): new head/teail style: cylinder"]
,["20140420-2", "Improved Line2()"]
,["20140420-1", "del()", "Raname: arrmax=>maxa, arrmin=>mina", "Line2()"]
,["20140419-3", "minPt()"]
,["20140419-2", "minxPts(), minyPts(), minzPts()"]
,["20140419-1", "mina(),maxa()"]
,["20140418-5", "Improved Box()"]
,["20140418-4", "Add a strick flag to ishash()"]
,["20140418-3", "Improve update: item order of h2 doesn't matter."]
,["20140418-2", "hashi()", "hashkv()"]
,["20140418-1", "Rename hashd to hasht", "New hashd()"]
,["20140417-3","Box()."]
,["20140417-2","DashBox()."]
, ["20140417-1","boxPts() improved."]
,["20140416-3","boxPts()"]
,["20140416-2","getd()", "hashd()"]
,["20140416-1","Remove DottedLine()", "Modify PtGrid to take hash argument", "PtGrid_test()"]
,["20140415-4","Modify DashLine to take hash argument"]
,["20140415-3", "echo_()"]
,["20140415-2","Rename rep() to s_()", "h_()"]
,["20140415-1", "normal_demo()","Allow wrap in index()"]
,["20140413-1", "PtGrid()"]
,["20140412-1","Rename Lines to Chain", "scadex_demo()", "DashLine()","DottedLine()"]
,["20140409-2","normal()", "planecoefs()"]
,["20140409-1","getcol()", "transpose()","Rename semiAnglePt to midAnglePt"
			, "Rename crossLinePts to othoCrossLinePts"
			, "Rename crossPts to begCrossLinePts" ]
,["20140406-5", "othoCrossLinePts()", "semiAnglePt()"]
,["20140406-4","Line_test() and _demo()"
			, "Use convert_scadex.py to create scadex.scad and scadex_test.scad" ]
,["20140406-3", "Fix othoPt", "Line() module"]
,["20140406-2","Restructure the code layout a bit to make them consistent"]
,["20140406-1","disable dist()", "Define doc of each function with name of it"]
,["20140405-1", "Change [_] in rep() to {_}", "add echo_level arg to doctest"
	, "dx(),dy(),dz()","linecoef", "slope()", "othoPt()", "begCrossLinePts()"
	, "Rename arcPoints to arcPts"]
,["20140402-1", "Rename curvePoints() to arcPoints"]
,["20140401-2", "reverse();"]
,["20140401-1", "Hey it's April Fool!; curvePoints();In shift(), change arg i to _i_;"]
,["20140326-2", "Remove test_run() (use doctest instead); Update doc for doctest(); scad_ver" ]
,["20140326-1", "doctest(); Rename items() to vals(); Remove stepdown()" ]
,["20140324-1", "multi_test(); echoblock()" ]
,["20140321-2", "shrink()" ] 
,["20140321-1", "stepdown()" ]
,["20140320-3", "Trying to add doc feature in test_run(), under construction." ]
,["20140320-2", "Allows update to add single [k,v] pair more easily
			get(o,i) now get(o,i,[j]) that can retrieve multiarray data." ]
,["20140320-1", "more examples for update()." ]
,["20140319-6", "update(). Surprisingly easy." ] 
,["20140319-5", "ishash();delkey()" ]
,["20140319-4", "bugfix: test_run(): make sure no * is shown when the test-specific 
			asstr is set to false in cases where function-wise asstr is true;
			Setup norm_test() to demo the use of test_run() (asstr, rem, etc);
			Add indent to output when test fails in test_run()" ]

,["20140319-3", "countType(); Rename count???Items to count???Type, and use countType( except countNum )" ]
,["20140319-2", "Only int allowed in i in get(o,i) now;
			 slice(s,i,j)=undef when i,j order wrong" ]
,["20140319-1", "inrange_test()" ]
,["20140318-1", "keys()" ]
,["20140315-1", "Start using 2013.03.11 with concat feature turned on; 
           bugfix: previous ver doesn't show * for test-specific asstr;
           slice works on array; 
           split() " ]
,["20140313-1", "int(),countArr(),countInt(), countNum(); Add "*" for asstr" ] 
,["20140312-2", "Re-doc for test_run(); minor bug fixes." ]
,["20140312-1", "1.Restructure again for easier test cases writing for user." ]
,["20140311-1", "1.Restructure test arguments to allow for 'asstr' and 'rem' options
		    2. One test_echo() for all (including those with optional args)" ]
,["20140309-3", "rename from tools to scadex" ]
,["20140309-2", "More doc for the functions. " ]
,["20140309-1", "Remove findw (use index instead)" ] 
,["20140308-3", "more thorough doc." ]
,["20140308-2", "doc for test tools" ]
,["20140308-1", "test features done; code structured" ]
,["20140307-4", "test_echo1(), test_echo2()" ]
,["20140307-3", "rep()" ]
,["20140307-2", "make index() take string. deprecricating findw()." ]
,["20140307-1", "fix bugs in findw." ]
];
     
//===========================================
//
//           Version / History
//
//===========================================

            
scadx_history=[
        
 ["20161230-1", "match: New pattern, '..', for match_at(); Restructure code for match_ps_at"]
        
,[]
,["20161209 ... ", "new:growPts(), transPts(), getHShapePts(), Cylinder(), Line(), hashs(); upgrade: addx,addy,addz have new arg 'keep'"]
        
, ["20150204-1", "Migrated from scadex."]
,["20150206-1", "Remove:subops; Add: centroidPt, symediamPt; Add back: prependEach; update: lineCrossPt accepts different arguments "]
,["20150210-1", "dig(), digrep(), repeat(x,n) now return [] or '' if n not valid"]
,["20150211-1", "preparing for fml_parse."
    ,"split(),splits(): new arg, keep, and new behavior: no more empty str \"\" in return"
    , "New: trim(), blockidx(), splitb()"]
,["20150215-1", "Improved blockidx: now 1. takes multiple sets of block beg/end; 2. each beg/end could be more than 1 letter; 3. ret [ [bi,b,ei,e],[...],...]"]
,["20150218-1", "unify(); app(), appi(), Improved begwith/endwith(o,x): return val (instead of T/F) when x is an array"]
,["20150219-1", "Rename unify to getuni(); packuni()"]
,["20150219-2", "begword(); Imp getuni: cut by word, num converted"]
,["20150220-1", "Serious bugfix and imp for begword() and getuni()"]
,["20150220-2", "getuni & packuni: customizable prefix; bugfix: previous can't handle (ab+1)*20-1.5"]
,["20150221-1", "New: run(); calc_basic(); Fix _fmth a bit. "]
,["20150222-1", "getuni() bug found: (x+1)*(y+2) is correct, (x+1)*((y+2)*3) should be (str quotes removed for clearity): [[#/1,*,#/2],[x,+,1],[#/3,*,3],[y,+,2]] but got: [[#/1,*,#/2],[x,+,1,#/3],[],[y,+,2,*,3]] "]
,["20150223-1", "fix getuni() bug"]
,["20150223-2", "calc() bug-free now"]
,["20150223-3", "Imp: packuni() to split '2x' to 2,*,x. Not yet able for 2(x+1)."]
,["20150224-1", " Fix uconv bug; Comment out to be removed: blockidx(), dig();"]
,["20150225-1", "Move all demos to scadx_demo.scad; Rename: prepandEach=> ppEach; Disable mergeA()"]
,["20150226-1", "rand(): new arg seed"]
,["20150302-1", "Fixed some bugs in ranges() "]
,["20150302-2", "split(), splits(): new arg keepempty; Comment subarr for removal. Done code cleaning for scadx_core."]
,["20150305-1", "New:isSameSide(), demo tested. Imp: angle: new arg refPt"
 ]
,["20150306-1", "New: angle_series(), prod(), sel()"
 ]
,["20150306-2", "New: pw(), pqr(), pqr90, coord(); Recode: randPt, randPts"]
,["20150306-3", "randPt/randPts: Rename arg len to d (=distance to ORIGIN)"
 ]
,["20150306-4", "rotM, rotPt,rotPts (untested)"]
,["20150307-1", "Cool demo of rotPts_demo(); Imp: cubePts(pqr): pqr will be given if not; bug found: Mark90 makrs when a< 0"]
,["20150307-2", "Fix bug in is90() that causes Mark90 to goes wrong in some cases."]
,["20150308-1", "rotObj() to rotate obj (not polyh). Tested: rotObj_demo()."]
,["20150308-2", "New: uv(); Imp: angle: now allow([pq1,pq2]); New: normalLn; Imp: modify error msg text; new: Chainc: closed chain; Get transf_dev() in rot_test.scad to work"]
,["20150309-1", "Attempt trfPts_demo_polyh_cube() but seems not perfect"]
,["20150310-1", "coordPts, iscoord(), Coord(); Coord_demo() works great; N2(): change direction. This might have impact on other funcs."]
,["20150311-1", "Imp has(): non-arr and non-str gets false"]
,["20150312-1", "Tidy up getsubops and give more detailed doc. New mod echofh()"]
,["20150317-1", "bugfix: update(h,g) error when g=[]; Imp: rewrite _fmth and add new args (sectionhead/comment); Working on new subops() in arg_pat.scad"]
,["20150317-2", "New subops() and subops_demo() in arg_pat.scad. Seems to ready for transfering to scadx_core_dev.scad. Need a test."]
,["20150328-1", "Imp lineCrossPt(): report err when pts not on plane. New arg in angle(): lineHasToMeet."]
,["20150412-1", "Recode:sum(); Rename:digrep=>deeprep; New: all(), ispt(), isln(),ispl(),typeplp(), lineCrossPts()(needs tests), flatten()"]
,["20150417-?", "flatten() has new arg:d"]
,["20150420-1", "Recode hash usning list comp. Also simplify hash args to h,k,notfound; Recode index using list comp. Also remove wrap from its args."
] 
,["20150425-1", "New:combine()." ]
,["20150507-1", "New arg for anglePt: a2,ratio. To replace anyAnglePt later" ]
,["20150507-2", "calc00() and calc0() for simple calc, hope to get it quicker than calc(). " ]
,["20150508-1", "New: begmatch(). Tested" ]
,["20150508-2", "New: calc_shrink(). Tested" ]
,["20150508-3", "New: calc_base(), handles operator priority and 2a style calc. Tested. Excellent. Rename: calc00=>calc_axb() " ]
,["20150508-4", "Rename: calc_base => calc_flat; Fixed bug: calc_flat fails to handle [[3,'/'], [4,'*']]" ]
,["20150508-5", "Recode calc_flat, calc_shrink to take care of 2b^2, which is done, but fail to treat * and / (or + & -) equally (a/b*3 wrongly treated as a/(b*3)" ]
,["20150508-6", "New calc_flat and calc_shrink, together take care of anything flat." ]
,["20150509-1", "calc_flat(s) bug fixed: unable to calc when s= 5 or '5'; begword new arg: we " ]
,["20150509-2", "working on new func: match() " ]

,["20150510-1", "match() done. Works pretty well. " ]
,["20150510-2", "New idx(), same as index(), better code" ]
,["20150510-3", "match2() done. Another excellent code. " ]
,["20150511-1", "New: match3(), isat()" ]
,["20150511-2", "New: match_at();Improve: isat(): chk multiple x's. String only. " ]
,["20150511-3", "New: match4(),Excellent regex-like matcher" ]
,["20150511-4", "Improve: match4() new arg 'want' to return selected matches" ]
,["20150512-1", "New: matches_at(). Tested. Bugfix: match_at() and some changes" ]
,["20150512-2", "New: match5(). We reached the goal of a matching func. " ]
,["20150512-3", "Remove old match?() and rename match5 to match. Imp: matches_at: want can be an int." ]
,["20150512-4", "New: match_nps_at(); Rename matches_at() to match_ps_at()" ]
,["20150513-1", "New: calc_blk()" ]
,["20150513-1", "New: marchr_at(): reversed match from tail of s" ]
,["20150513-2", "New: calc_func(): much better features and much faster than calc @20150223-2." ]
,["20150513-3", "Recode calc_flat to make use of match(). Much easier to debug." ]

,["20150514-1", "calc_axb,_flat,_func: new ops: %><=&|, and new conditional a?b:c construct. " ]
,["20150514-2", "calc_func: allow array val in scope: ['x',range(4)] " ]
,["20150516-1", "To be Removed: minPts, minxPt, minyPt, minzPt, getuni, packuni, vlinePt( anglePt can do that)" ]
,["20150517-1", "Rename: accumulate=> accum; Fix ln-ln dist in dist()" ]
,["20150518-1", "Imp dist() and dist() to include pt,ln and pl. New: isparal()" ]
,["20150518-2", "Rm: d01pow2,d12pow2,d02pow2 (use pw(d01(pqr)) instead); boxPts retired, use cubePts instead; " ]
,["20150519-", "Introuce new var mm=0.1 (for millimeter);" ]
,["20150521-", "subopt1();" ]
,["20150522-1", "subopt1() new arg: mainprop; Introduce mti (module tree indent), used in echom()" ]
,["20150522-2", "Match, Match_ps_at, Match_nps_at: change return from [i,j.ma] to [i,j,ms,ma]" ]
,["20150523-1", "Excellent formatting function _f()" ]
,["20150523-2", "Excellent _s2() making good use of _f()" ]
,["20150524-1", "Make _s2() more customizable" ]
,["20150524-2", "Fix bug in split() due to change of default keepempty; _f() can format items of array (using a and aj)" ]
,["20150525-1", "_s2(): new data selection {*}; {i} i can be neg." ]
,["20150526-1", "range(...cycle) cycle can take arr like [-1,1]; sel can take arr of 'index arr' as the range" ]
,["20150526-1", "sel can take arr of i array. range and ranges: new arg returnitems. New: tanglnPt()" ]
,["20150528-1", "New: uvN(), trslN(); New shape 'ring' in faces; convPolyPts() (might not be very useful);" ]
,["20150528-2", "new: rotpqr()" ]
,["20150529-1", "new: intsec()" ]
,["20150529-2", "Rename: lpCrossPt=> intsec_lnpl; Retiring: lineCrossPt(), lineCrossPts(), intersectPt()" ]
,["20150529-3", "Reworking rodPts() and Rod()" ]
,["20150530-1", "New:eval() for basic eval'ion. No nested array yet. New: sopt()" ]
,["20150530-", "New:trslN2, trslP" ]
,["20150601-1", "New:match_rp(); scomp(); quicksort uses scomp" ]
,["20150601-2", "Resurrect getuni, packuni. The mechnism is valuable. evalarr(), still some bugs" ]
,["20150601-3", "evalarr() bug fixes. Cool. Most complicated recursive funcs ever made" ]
,["20150601-4", "_f(): fix % error" ]
,["20150601-5", "sum() can sum up pts: sum([[1,2,3],[4,5,6]])=[5,7,9]; Fix bug in rotpqr()" ]
,["20150602-1", "Simplify sopt() (remove dual)" ]
,["20150602-2", "Further simplify sopt(). New uopt()" ]
,["20150602-3", "uopt() can use internal sopt" ]
,["20150603-1", "und(); Major upgrade to hash(), now can handle sopt. But this feature might put too much overhead on hash()" ]
,["20150603-2", "BIG CHANGE IN PROGRESS. New: popt(), getv(); Improve update(), updates() to take sopt and mopt (other than the normal popt)" ]
,["20150604-1", "sel() can take neg. indices" ]
,["20150604-2", "chainPts(). Cool." ]
,["20150605-1", "randchain()." ]
,["20150606-1", "New: getBezier()" ]
,["20150607-1", "New: tangLn(); Rename: tanglnPt=> tanglnPt_P" ]
,["20150607-2", "New: angleAt; Rewrite getBezierPt to make it customizable" ]
,["20150608-1", "Change getBezierPt to getBezierPtsAt to make it usable at any i of pts"]
,["20150609-1", "Rewrite getBezierPtsAt to make it debug-friendly; Imp: angleAt(pts,i): now i can be arr [i,j,k]"]
,["20150609-2", "Update smoothPts for the new getBezierPtsAt"]
,["20150610-1", "New: iscoln(); Imp: chainPts new arg: Rf, rot"]
,["20150610-2", "Imp: projPt(): new arg *along*. New: projPts() => used in chainPts"]
,["20150610-3", "Imp: chainPts can do hcut (head cut)"]
,["20150611-1", "Imp: chainPts: hcut easier to use; output includes h/t cut tilt axes"]
,["20150611-2", "Imp: chainPts: can do tcut(tail cut)"]
,["20150611-3", "Imp: chainPts: r2. Cool"]
,["20150611-4", "Imp: randchain: smooth; fix:getBezierPtsAt minor bug"]
,["20150612-1", "Imp: randchain: lens; chainPts: rs"]
,["20150612-2", "Imp: chainPts:closed"]
,["20150612-3", "Imp: chainPts:twist"]
,["20150613-1", "Imp: chainPts:xpts basic"]
,["20150614-1", "Imp: chainPts:can use opt(and sopt) to pack arguments."]
,["20150614-", "Imp: arrprn new arg:nl; subopts to set multiple subopts"]
,["20150615-1", "Imp: faces: new arg tailroll (see Chain)"]
,["20150616-1", "Imp: isequal can compare pts; twistangle bug fix and recode"]
,["20150616-2", "New: plat() (used in twistangle)"]
,["20150617-1", "New: isnan(); bug fix: twistangle (escape nan value) "]
,["20150617-", "New: firstbend => rename ibend"]
,["20150620-1", "New: planePts"]
,["20150620-2", "New: get3(); Imp: get()... i can be array, to replace sel()"]
,["20150620-3", "Imp: get( ...add): can add new pt(s)"]
,["20150621-", "Imp: get(): new arg pp; New: randofflnPt() untested"]
,["20150622-1", "Imp: N() & N2(): return undef when 180deg; Imp:angleBisectPt: takes care of iscoln by extra optional Rf."]
,["20150622-2", "Working on: planeAt() when isbeg, seems to work."]
,["20150623-1", "Working on: planeAt() when isbeg, seems to work."]
,["20150623-2", "Bugfix: get3(...i) failed when last i; Update: squarePts now uses anglePt( not anyAnglePt); bugfound: anglePt fail when a=90 and pqr=coln (see anglePt_demo_coln()"]
,["20150624-1", "new: dist_2(); Recode: iscoln, is90; Change ZERO to from 1e-10 to 1e-6; new: isSamePt(); Recode angle() to use atan2 (instead of acos)(see anglePt_demo_coln() in scadx_demo.scad for details), rename refPt to Rf"]
,["20150624-2", "Recode: Reverse angle() back to acos (from atan2). angle() and planepts() both seem to work fine."]
,["20150624-3", "Both planePts() and planeAt() seem to work fine."]
,["20150624-4", "Start recoding chainPts using new planeAt()"]
,["20150625-1", "Fix bug in planeAt when isend"]
,["20150625-2", "Clone get() to sel() for future retirement of get() Note:sel is much easier to type; chainPt: tcut working"]
,["20150625-3", "chainPts_demo_r2 working"]
,["20150627-1", "chainPts_demo_rs working"]
,["20150627-2", "chainPts: rs items can be array. This is cool."]
,["20150628-1", "chainPts: closed working."]
,["20150628-2", "chainPts: twist working."]
,["20150628-3", "chainPts: xpts working."]
,["20150629-1", "faces: New shape: tubeEnd. See faces_demo_6_tubeEnd()"]
,["20150629-2", "faces: shape tube can handle tubes with different inner and outer nsides. Excellent breakthrough"]
,["20150630-1", "faces: new shape cone; chainPts() able to handle cone."]
,["20150630-2", "faces: new shape conedown; chainPts() able to handle conedown."]
,["20150630-3", "Recode getBezierPtsAt such that it: 1. uses opt 2. outputs hash (but not array)"]
,["20150701-1", "Recode smoothPts to make it uses opt as well. Also see smoothPts_demo_circle for different dH settings to make circles out of regular polygon w/ different sides."]
,["20150701-2", "New: dor(); Recode getBezierPtsAt and smoothPTs to make individual arg assignment( like a=30) over write opt (like ['a',30] "]
,["20150703-", "New: angle_bylen()"]
,["20150704-1", "New: New chainPts0 and Chain0 for simple line/chain"]
,["20150708-1", "New: toHtmlColor, colorPts"]
,["20150709-1", "New: ispts, notall, isball, iscir, gtype to replace typeplp"]
,["20150710-1", "Improve errmsg()"]
,["20150710-2", "New: assert()"]
,["20150711-1", "Apply assert() to planePts and planeAt. Still not perfect but we leave further improvement to the future."]
,["20150711-2", "Bugfix: faces shape=spindle. Checkout spindle example in chainPts_demo_coln_arrow()"]
,["20150714-1", "Rename colorPts to _colorPts"]
,["20150715-1", "MAJOR RESTRUCTURING --- split scadx_core_dev to smaller files: init,funclet, array, range, hash, inspect, geometry, string, eval, match, error, args"]

,["20150715-1", "MAJOR RESTRUCTURING --- split scadx_core_dev to smaller files: init,funclet, array, range, hash, inspect, geometry, string, eval, match, error, args"]

,["20150718-1", "Rename planePts() to planeDataAtQ()"]
,["20150718-2", "Rename planeAt() to planeDataAt()"]
,["20150720-1", "Imp planeDataAtQ: del isabi, add rot2. Seems to be fine. Improve funclet pij (like p01(),p12()) to enable them to concat pts"]
,["20150720-2", "Imp planeDataAtQ: Fixed and tested"]
,["20150720-3", "Imp planeDataAtQ: made sure works when pqr iscoln"]
,["20150720-4", "Imp planeDataAt: tested except when coln"]
,["20150721-1", "planeDataAt: tested coln (using planeDataAt_demo_i_coln)"]
,["20150722-1", "Rename: randchain=> chainbone, chainPts=> chaindata. Also renames some args. Tested."]
,["20150723-1", "Branch loft: new: sort(arr,i), pairPts_sorted_afips(), pairPts()"]
,["20150724-1", "Branch loft: new: circlePts(), centerPt(), getPairingData_ang_seg()"]
,["20150724-2", "Branch loft: minor fix angle() (when P==R); Imp: sort(arr,i)=> sort(arr,i,j); New: uniq(arr); Fix bug that the sorted afips returned by getPairingData_sorted_afips sometimes is not sorted (due to precision issue)."]
,["20150725-1", "Branch loft: get getPairingData to work nicely (see getPairingData_demo2. We are now ready to go lofting"]
,["20150729-1", "scadx_geometry: big bug fix: use p_on_this_angle in getPairingData"]
,["20150729-2", "inpect: num() now ca handle scentific ntation ('3e-2', etc)"]
,["20150730-1", "geometry: another bugfix in getPairingData (see getPairingData_demo1)"]
,["20150730-2", "geometry: Finally got loft done. see chaindata_demo_loft()"]
,["20150730-3", "geometry: lofting bugfix. See chaindata_demo_loft_rlast()"]
,["20150730-4", "geometry: chaindata new arg: loftfactor. See  chaindata_demo_loftfactor_hires()"]
,["20150731-", "Rename scadx_obj_basic_dev.scad to scadx_obj.scad; geometry: chaindata: change arg loftfactor to loftpower; "]
,["20150801-1", "Make chain_demo_? based on chaindata_demo_? (currently finished chain_demo_conedown); "]
,["20150802-1", "Fixed bug: eval was unable to eval undef; obj: show Chain can make cup (see chain_demo_cup2)."]
,["20150802-2", "Rename scadx_core_tests.scad to scadx_tests.scad; Imp: now can do dist(pts,ij=[i,j]); start working on Arrow() "]
,["20150802-3", "New: insert(); working on Arrow()"]
,["20150803-1", "New: any(); Add back: all(); bugfix: getsubops: recode show= ...; bugfix: chaindata failed to show correct r at corner"]
,["20150804-1", "New: geometry: distRatios"]
,["20150804-2", "Bugfix & imp: chaindata: remove rs, r can take array as rs; rename cut to tilt, and csecs to cuts. Add one more projPt for each step along pts --> fix bug but slow down big"]
,["20150805-1", "New: getbonedata(), trsfbonePts(); Imp: Mark90: can draw chain, and skip if arg pqr is not pl; Rename: trfPts => trsfPts"]
,["20150805-2", "New: getTurnDataAtQ (untested), isonln( tested), isinln(untested)"]
,["20150806-1", "[error]: imp:assert: when *check*=type or gtype, *want* can be array, in such cases, *val* will be checked against any item in *want*; [inspect] bugfix: isonln, isinln"
 ]
,["20150806-1", "[geometry]:tria_props; getAnglePtData() and tested (see getAnglePtData_demo() for how to transform a triangle)"
 ]
,["20150806-2", "[geometry]:recode trsfPts, only works in simple cases(compare trsfPts_demo1 and trsfPts_demo2"
 ]
,["20150806-3", "[geometry]:trsfPts_dev() works pretty well"]
,["20150806-4", "[geometry]:trsfPts() works nicely"
 ]
,["20150808-1", "projPl(see projPl_demo(); Recode expandPts"]
,["20150809-", "Imp:insert: (1) Change api from i,x to x,i (2) when i large, append.Tested; More funclets: PQ(pqr,S)=>PQS, PR(pqr,S,1)=>PSR, etc "]
,["20150811-1", "Rename planeDataAtQ's args, and recode. Also make the first attempt of testing an obj with their pts location. See planeDataAtQ_demo1 and planeDataAtQ_demo_tiltR_a."]
,["20150811-2", "Another rename to make it more competible with downstream functions and easier to use: planeDataAtQ's args: tilt_a=>tilt, tiltR_a=>tiltforward, pretilt_rot=>rot"]

,["20150811-3", "Cool polyhedron testing in planeDataAt_demo_i_simple."]
,["20150812-1", "Good testing model in planeDataAt_demo_at_0; New: planeDataAtQ_testing_get_gots(),planeDataAtQ_testing_Draw(),planeDataAtQ_testing() "]
,["20150812-2", "Excellent polyhedron testing approach. See planeDataAtQ_demo1(). New: angleAfter2Rots(); Minor fix Mark90 (if...) "]
,["20150813-1", "Finish complete tests in planeDataAtQ_demo1()"]
,["20150814-1", "Recode behavior at tilt=0 in planeDataAtQ(): tilt=0 is now what it is: tilt at 0; Move older codes to /old"]
,["20150815-1", "All tests for planeDataAtQ (see planeDataAtQ_demo1 and planeDataAtQ_demo_coln) completed"]
,["20150815-2", "Pass all tests in planeDataAt_demo_at_1()" ]
,["20150816-1", "Give up using planeDataAtQ_testing for planeDataAt(). Write it's own testing set, tested the first 3 settings of planeDataAt_demo_at_0()" ]
,["20150817-1", "planeDataAt_demo_at_0() done" ]
,["20150818-1", "Done:planeDataAtQ_tests, planeDataAt_tests, planeData_tests()" ]
,["20150820-1", "chaindata_testing() -- instead of using several mod/funcs in testing planeData, use this as the single module; Tested chaindata_testing_2pointer, _rot, _tilt" ]
,["20150820-2", "Fixed minor bug on chaindata( tilt ...). chaindata_testing_tilt() done" ]
,["20150821-1", "chaindata_testing_tiltlast(),chaindata_testing_closed(), chaindata_demo_arrow_0() demo done" ]
,["20150822-1", "new: chainlen(); New: chainData, which is a stripped-off version of chaindata; New file: scadx_test_chainData.scad. Pretty cool arrow demo in chainData_demo_curved_arrow." ]
,["20150822-2", "new: chainlen(); Fix minor bug in chainData_demo_curved_arrow(); chainData_demo_r()" ]
,["20150822-3", "new: func2d(). Applied in chainData_demo_r()" ]
,["20150823-1", "Rename chainData to simpleChainData.  simpleChainData_demo_basecut() done"]
,["20150823-2", "SimpleChain(); Apply SimpleChain() to simpleChainData_demo_basecut(), MarkPts()"]
,["20150823-3", "Apply SimpleChain() to PtGrid() (Some demos need fixes) and Mark90 (also fix Mark90 bug that can't set ch style"]
,["20150826-1", "Revert from BEFORE commit clonePts_n_add_prec; Add ASCII lib to scadx_ref that's added in the clonePts_n_add_prec version; Remove old files from /scadx to new /old_backup, files are accidentally added back by git add --all."]
,["20150826-2", "Copy scadx/old/scadx_obj_adv_dev.scad to scadx/scadx_obj2.scad in which Dim() seems to work nicely."]
,["20150826-3", "ArcArrow, seems to work."]
,["20150827-1", "ArcArrow bug fix. Still needs fine tune but looks good to use for now to use. See ArcArrow_demo()"]
,["20150827-2", "scadx_hash:sopt(): add ['ch','chain','pl','plane'] to df; scadx_obj:ArcArrow: add chain and chainext args, and be able to handle popt" ]
,["20150827-3", "Trying moving pts from any pqr to any stu. See move_dev???() especially move_dev2_1() [scadx_obj]"] 
,["20150827-4", "move_dev2_1() & move_dev5() bug fix to ensure correct rota2 [scadx_obj]"]  
,["20150827-5", "Excellent getMoveData() [scadx_geometry], can be used on pts (see getMoveData_test_poly) and obj (see getMoveData_test_obj_text) [scadx_obj]"]  
,["20150827-6", "New Move(), Write(), both working nicely [scadx_obj]"]  
,["20150828-1", "New movePts() [scadx_geometry]"]  
,["20150828-2", "Rename args in getMoveData to *from*,*to*. Fix bug in getMoveData() when from and to are parallel. [scadx_geometry]"]  
,["20150828-3", "Make Dim works after bug fix of getMoveData. [scadx_obj2]"]  
,["20150828-4", "extPt(); guessTextWidth() [geometry]. Use guessTextWidth to auto set gap in Dim(); Dim now can show label on the same plane of pqr"]  
,["20150829-1", "Fun stuff in Write_on_curve_dev() [obj2]"]  
,["20150829-2", "Quick simple draft of Ruler to be used in FontWidthCheck [obj2]"]  
,["20150829-3", "Improve FontWindthCheck() for checking font width (that is needed for guessTextWidth... default font width checked:A~Z) [obj2]"]  
,["20150830-1", "FontWindthCheck(): checked font width a-z [obj2]"]  
,["20150830-2", "verifyFontWidth(): verified font guess, A-Za-z, pretty close to perfection [obj2]"]  
,["20150830-3", "explore_multmatrix_150830() under construct [dev.scad]"]  
,["20150831-1", "Transform() using multmatrix [geometry]"]  
,["20150831-2", "text on curve surface, failed text_on_curve2_150831(). Should be working after further fine-tune [dev.scad]"]
,["20150831-3", "Done with measuring default font width using fontWidthCheck();  Excellent font box border in verifyFontWidth [obj2.scad]; Rename: guessTextWidth=> textWidth 'cos the width measure is near perfect [geometry];"]
,["20150831-4", "Improve Write: can take children; can rot. [geometry];"]
,["20150901-1", "mvx(),mvy(), etc [funclet]. Piggie_Corner_Shelf_20150901 [dev.scad]"]
,["20150902-1", "New Piggie_Corner_Shelf_20150902, with color and better dim. In progress [dev.scad]"]
,["20150903-1", "Cool Dims() for dimensioning pts [obj2.scad]"]
,["20150903-2", "Piggie_Corner_Shelf_20150902() done. [dev.scad]"]
,["20150904-1", "Success: text on curve with Bend_dev_20150904_2. [dev.scad]"]
,["20150904-2", "Fix bug angle_bylen(); Rename to anglebylen()"]
,["20150907-3", "Fix another bug anglebylen() when a=90; Organizing scadx_tests, starting from scadx_geometry; doc and tests for anglebylen, angleAfter2Rots [geometry]"]
,["20150909-1", "Bug fix for angle() when applied to 2 lines, and remove the arg lineHasToMeet; Recode twistangle and demo [geometry]"]
,["20150909-2", "Improve angleAt and done test; Delete angle_atan() [geometry]; Minor fix Write to allow children own color/transp; Rename Write's arg pqr to at [obj]"]
,["20150910-1", "New feature for Write: Write(childcare=...) [obj]"]
,["20150910-2", "The i in get() can take float [range]"]
,["20150910-3", "New type([1:2:10])='range' [inspect]"]
,["20150912-1", "Rename doctest2 to doctest"]
,["20150912-2", "Delete anyAnglePts_demo_??? [demo]; Mark out chainPts0 [geometry] and Chain0 [obj]. Use simpleChain instead."]
,["20150913-1", "Incorporate obj tests( like simpleChainData_tests() in [ scadx_test_chainData.scad ] into scadx_tests.scad."]
,["20150915-1", "Rename chainbone() to chainBoneData()"]
,["20150916-1", "New file scadx_woodwork.scad"]
,["20150918-1", "Beautiful lofting with getPairingData_demo_loft2() [demo]"]
,["20150918-2", "In the process of recoding getPairingData to getPairingData2 using recursion [geometry]"]
,["20150919-1", "Replace sel() with get() in all files."]
,["20150920-1", "New func get2() [range]; Clone the getPairingData2_dev (which works pretty well but need fix) and rename to loftData(). Keep getPairingData2_dev for future ref [geometry]"]
,["20150920-2", "Change name loftData to getLoftData(). Recoding it to take large nsides into consideration (under construct) [geometry]"]
,["20150921-1", "getLoftData() under construct, works ok so far, able to do n=6, n1=5, n2=4 upto _i=3 [geometry]"]
,["20150921-2", "Save a copy of func getLoftData to getLoftData_dev_20150920() [geometry]. Also save a copy of module getLoftData_dev_20150920() [demo]"]
,["20150921-3", "getLoftData() works pretty well. [geometry]. Check out getLoftData_dev_20150921() [demo]"]
,["20150922_1", "Excellent getBoxData (to replace borderPts). Check getBoxData_demo() [demo]"]
,["20151001-1", "Revert back from 20150929(Del animate_gif...). Fix major bug of getLoftData (when n=10,n1=6,n2=14 found in new getLoftData_demo_animate(). See getLoftData_demo_debug() [demo] "]
,["20151002-1", "Fix another bug of getLoftData (when n=6,n1=10,n2=10)[demo]; Add new arg 'isdebug' to getLoftData.[geometry]"]
,["20151002-2", "Add step-specific debugging to getLoftData, and change 'isdebug' to 'debug.[geometry]"]
,["20151002-3", "getLoftData_demo_animate() used to generate loft_demo.scad [demo]"]
,["20151115-1", "sci()[string]; insert() now can handle string; new fullnumstr()"]
,["20151116-1", "New file scadx_num.scad and move num-related functions to it"]
,["20151117-1", "fullnumstr() done. Bugfix on type(12345678901234500)"]
,["20151118-1", "bugfix on fullnumstr() and repeat('x',3.000003) (which gave xxxx)."]
,["20151120-1", "Bugfix on fullnumstr(): apply 'order-rasing' (i*f-j*f)/f to fix bugs on cases like 300.00000002."]

,["20170527", "Recode transPts(); Add new Primitive( in obj ), Transforms( in geometry), _mono( in string), countAll ( in inspect); Add new file scadx_animate.scad with get_animate_visibility(); Add new file scadx_make.scad"
 ]

          //mvPts
          
// ChainByHull() for debug use
];
   
/*
To fix:

--- [chaindata] 

    When rs is given as [ r,r,r, ...] (i.e., all the same), 
    Chain( pts, rs=rs ) fails to show the turn with consistent r. 
    
    See chaindata_demo_rs_debug()

--- [Chain] See scadx_demo::chain_demo_rlast():
    
    [2015.8.1]: rlast in the following 3 should have been working:
    
      Chain( bone2, ["rlast",r/20]);
      Chain( bone2, ["cl=red","rlast",r/20]);
      Chain( bone2, str("cl=red;rlast=",r/20));

    But only this works (as expected) :
        Chain( bone2, rlast=r/20, "cl=red");
        Chain( bone2, "cl=red", rlast=r/20);

--- Why this doesn't work?
    // Why this doesn't work ?
    //Chain( bone, n=len(bone), r=0.8, nside=4, nsidelast=6 );

    but this works:
    
    chdata= chaindata( bone, r=0.8
                     , nside= 4
                     , nsidelast=24
                     );
    Chain( chdata );
    
    see: chain_demo_loft()
        
*/

SCADX_VERSION = scadx_history[ len(scadx_history)-1][0];

/*
////////////////////////////////////////////////////////////

                      TOPICS

////////////////////////////////////////////////////////////

# Recursive function (recursive) tips
  
  1) tail recursion
  2) init parameter w/ opt
  3) arg validation 
  4) debugging (ex: getLoftData )
  5) step-specific debugging (ex: getLoftData )
  
# Argument pattern
  
  1) opt
  
# Precision handling 
   
   num(str(n)) --- Look at example in getLoftData
   
# How to test a polyhedron

  See example in 
    planeDataAtQ_demo1, 
    planeDataAtQ_demo_tiltforward
    planeDataAt_demo_i_simple <== cool group testing
  
# Arg design pattern: variable len of arguments: check range()

*/  

/*
* 2015-09-29 e6c008e17b4981b64b5ffec2e818c3b6c8e0bbdd (origin/Pt_of_return_to_getBoxData) Hard to debug. Commit before reverting back to 2015-09-22 (e68aba4c7adc260affdc3acf008be24cb0d108da) Runsun Pan
| * 2015-09-26 19faedf892bdd2034c76a1f9d2c3aa23a23f0ced animateRot3d() Runsun Pan
| * 2015-09-24 f8bedf438cee058c9eaa2044dc9db74215bb80b2 Comment out trsfbonePts, trsfPts, getPairingData, getPairingData_ang_seg, getPairingData_sorted_afips. Improve expandPts. [Geometry]; Del pp2,pp3,pp4 [funclet] Runsun Pan
| * 2015-09-24 aa58aea50ef09db6663e753faa0594da457f3510 Comment out convPolyPts(); Rename: distRatios=>lenRatios [Geometry] Runsun Pan
| * 2015-09-23 13dc1d38cd7378483f003263fd745f30f4df16c9 Twick a little on centerPt, need further attention. New dist_RMSD [Geometry] Runsun Pan
*/
