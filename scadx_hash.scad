

 function all_in_hashs(hashs, key, _rtn=1, _i=0)=  //2017.6.19
 (  // h1 = ["visible", 1]
    // h2 = ["visible", 1]
    // h3 = ["visible", 0]
    // all_in_hashs( [h1,h2], "visible" ) => 1
    // all_in_hashs( [h1,h3], "visible" ) => 0
    // all_in_hashs( [h1,h2,h3], "visible" ) => 0
    
    _i<len(hashs)
    ? let( rtn = hash( hashs[_i], key, 1) )
       all_in_hashs(hashs, key, _rtn= _rtn&&(rtn?1:0) , _i=_i+1)
    : _rtn==true?1:(_rtn==false?0:_rtn)
 );
    
 //............................................
    
 function any_in_hashs(hashs, key, _rtn=0, _i=0)=  //2017.6.19
 (  // h1 = ["visible", 0]
    // h2 = ["visible", 0]
    // h3 = ["visible", 1]
    // any_in_hashs( [h1,h2], "visible" ) => 0
    // any_in_hashs( [h1,h3], "visible" ) => 1
    // any_in_hashs( [h1,h2,h3], "visible" ) => 1
    
    _i<len(hashs)
    ? let( rtn = hash( hashs[_i], key, 0) )
       any_in_hashs(hashs, key, _rtn= _rtn||(rtn?1:0) , _i=_i+1)
    : _rtn==true?1:(_rtn==false?0:_rtn)
 );   
     
     
     
//========================================================

     
//function delkey(h,key, _i_=0)=
//(
//     _i_>=len(h)? []
//	 : concat( h[_i_]==key? []:[h[_i_], h[_i_+1]]
//	         , delkey( h, key, _i_+2) 
//			 )
//);

function delkey(h,k)=    // recode 2017.6.19
(
   h?[ for(i=[0:len(h)-1])  
     if((i%2==0 && h[i]!=k)||(i%2!=0 && h[i-1]!=k)) h[i] 
   ]:[]
);

//h= ["a",1, "b",2, "c",3]; 
//echo( delkey( h, "b" ) );   
     
function delkeys(h,ks)=  // new 2017.6.19
( 
   h?[ for(i=[0:len(h)-1])  
     if(i%2==0 && !has(ks,h[i])||(i%2!=0 && !has(ks,h[i-1]))) h[i] 
   ]:[] 
); 


    delkey=[ "delkey", "h,k", "hash",  "Hash",
    " Given a hash (h) and a key (k), delete the [key,val] pair from h. 
    ;;
    ;;   delkey( ['a',1,'b',2], 'b' )= ['a', 1]      
    "];

    function delkey_test( mode=MODE, opt=[] )=
    (
        let( h1= ["a",1,"b",2]
           , h2= [1,10,2,20,3,30] 
           )

        doctest( delkey,
        [
          "var h1"
        , [delkey([], "b"),[], "[], 'b'" ]
        , [delkey(h1, "b"), ["a",1], "h1, 'b'" ]
        , [delkey(h1, "c"), ["a",1,"b",2], "h1, 'c'" ]
        , ""
        , "var h2"
        , [delkey(h2, 1), [2,20,3,30], "h2, 1"]
        , [delkey(h2, 2), [1,10,3,30], "h2, 2"]
        , [delkey(h2, 3), [1,10,2,20], "h2, 3"]
        ]
        , mode=mode, opt=opt, scope= ["h1",h1,"h2",h2]
        )
    );

//========================================================
function vals_of_key( hashs, key, allow_undef=false )=  //2017.6.19
( 
  // Return an array of values by matching key to each hash in hashs
  // 
  [ for( h=hashs ) 
    let( v = hash(h,key))
     if( (allow_undef && haskey(h,key))
       || (!allow_undef && v!=undef) 
       ) v ]
); 


//========================================================
//function hash(h,k, notfound, df_hash)= 
//(
//  // 2017.10.18: add df_hash --- when given, would try to retrieve
//  //             val of k from df_hash if k is not found in h.
//  //             This allows for better reading of code. 
//  //             See Frame for example.
//  // 2016.12.16: changed to "search()-based" approach 
//  // based on the profiling in hash_profiling.scad
//
//  und( h[ search([k], [for(i=[0:2:len(h)-2])h[i]])[0]*2+1 ]
//     , df_hash? hash( df_hash, k, notfound) : notfound
//     )
//  
//);

function h(h,k,notfound)= hash(h,k,notfound); // 2018.7.20

function hash(h,k, notfound)= 
(
  // 2016.12.16: changed to "search()-based" approach 
  // based on the profiling in hash_profiling.scad

  und( h[ search([k], [for(i=[0:2:len(h)-2])h[i]])[0]*2+1 ], notfound )
  
);
  
    hash=[ "hash", "h,k, notfound=undef", "Any","Hash",
    " Given an array (h, arranged like h=[k,v,k2,v2 ...] to serve
    ;; as a hash) and a key (k), return a v from h: hash(h,k)=v.
    ;; If key not found, or value not found (means k is the last item
    ;; in h), return notfound.
    ;;
    ;; h could be a str for single-digit key and value like :
    ;;  
    ;;    hash('w3h4', 'h') => '4'
    ;;
    ;; New 2016.12.16: use search, much faster, based on profiling in
    ;;                 hash_profiling.scad
    ;; New 2015.4.20: use list comprehension (instead of recursion)
    "];

//   The 'dot-key' approach on 16.12.10 is not practical, 'cos in cases
//   where a lookup for inner k-v are needed, more often they require
//   a calculation of the seedked value, like the idea below:
//
//      ['a', 3
//      ,'b', '.a'+1 
//      ]
//   But it's impossible to do the +1 part
//
//    ;; New 2016.12.10: a 'dot-key' to get val from self:
//    ;;                 if val is a str in the form of \".<key>\" (i.e., 
//    ;;                 starts with \".\"), get the new val from h itself. 
//    ;;                 For example, 
//    ;;
//    ;;                 hash( [\"a\",3, \"b\", \".a\"], \"b\" ) => 3
//    "];

    function hash_test( mode=MODE, opt=[] )=
    (
    //	let(h= 	[ -50, 20 
    //			, "xy", [0,1] 
    //			,[0,1],10
    //			, true, 1 
    //            , "c",true
    //			]
    //       ,h2= "c;xy=[0,1];true=1"
    //       ,h3= ["c;xy=[0,1]", -50,20
    //       )

        let( h1= ["c",true, "a",30, "xy",[0,1], "r",3, "u", undef ] 
           , h2= ["a",false, "b",0, "c", undef] //;a=30;xy=[0,1]","r",3]
           , h3= "c;a=30;xy=[0,1];r=3"
           , h4= "c|a=30|xy=[0,1]|r=3"
           , h5= "a=3;b=B;c=3"
           )
        doctest( hash,  
        [ 
          "", "var h1"
        , ""
        , [ hash(h1, "c"), true, "h1,'c'" ]
        , [ hash(h1, "xy"), [0,1], "h1,'xy'"  ]
        , [ hash(h1, "u"), undef, "h1,'u'"  ]
        , [ hash(h1, "u", 3), 3, "h1,'u', 3"
            , ["//", "Returning undef is treated the same as though the key is not found"]  ]
        ,""
        , [ hash(h1, "b"), undef, "h1,'b'"  ]
        , [ hash(h1, "b",notfound=3), 3, "h1,'b',notfound=3"  ]
        ,""
        , "Same as: "
        ,""
        , [ und(hash(h1,"b"),3), 3, "", ["fname", "und(hash(h1,\"b\"),3)"] ]
        , ""
        ,"but note:"
        , ""
        //, [ h2, h2, "", ["fname", "h2"]]
        , [ hash( [], "c", notfound=3 ), 3, "[],'c', notfound=3"]
        //, [ hash( ["c", undef], "c", notfound=3 ), 3, "['c', undef],'c', notfound=3", ["//", "value undef is treated as the key is not there"]]
        , [ und(hash(h2,"c"),3), 3, "", ["fname","und(hash(h2,\"c\"),3)"]]
        , [ hash( ["a", false], "a", notfound=3 ), false, "['a', false],'a', notfound=3"]
        , [ und(hash( ["a", false], "a"), 3 ), false, "", ["fname","und(hash([\"a\", false],\"a\"),3)"]]
        , [ or(hash( ["a", false], "a"), 3 ), 3, "", ["fname","or(hash([\"a\", false],\"a\"),3)"]]
        , ""
//        , "// New 2016.12.10: a 'dot-key' to get val from self:"
//        , ""
//        , [ hash( ["a",3, "b", "...a"], "b" ), 3, "[\"a\",3, \"b\", \"...a\"], \"b\""]
      
        , "h could be a string, for single digit keys and values:"
        , ""
        , [hash("abcdef","c"), "d", "'abcdef','c'"]
        , [hash("w1h2d3","d"), "3", "'w1h2d3','d'"]
        , ""
        , "Also ref the <b>sopt</b> function:"
        , ""
        , [ sopt("c;e=3;f=name"), ["c", true, "e", 3, "f", "name"], "", ["fname", "sopt(\"c;e=3;f=name\")"]]
        
        //, str( "//Same as: und(hash(h1,'b'),3)= ", und(hash(h1,"b"),3) )
       /* ,"","//Return h1 when key not given:"
        , [ hash(h1), h1, "h1"  ]
        
        
        , "", "// The 1st item is a packed hash called <b>sopt</b>:"
        , "var h2 "
        , ""  
        , [ hash(h2, "c"), true, "h2,'c'" ]
        , [ hash(h2, "xy"), [0,1], "h2,'xy'"  ]
        , [ hash(h2, "b"), undef, "h2,'b'"  ]
        , [ hash(h2, "b",notfound=3), 3, "h2,'b',notfound=3"  ]
        , [ hash(h2), h1, "h2"  ]
        
        , "", "// Entire hash is a <b>sopt</b>:"
        , "var h3 "
        , ""  
        , [ hash(h3, "c"), true, "h3,'c'" ]
        , [ hash(h3, "xy"), [0,1], "h3,'xy'"  ]
        , [ hash(h3, "b"), undef, "h3,'b'"  ]
        , [ hash(h3, "b",notfound=3), 3, "h3,'b',notfound=3"  ]
        , [ hash(h3), h1, "h3"  ]
        
        , "", "// Change the sp to '|':"
        , "var h4 "
        , ""  
        , [ hash(h4, "c", sp="|"), true, "h4,'c',sp='|'" ]
        , [ hash(h4, "xy", sp="|"), [0,1], "h4,'xy',sp='|'"  ]
        , [ hash(h4, "b", sp="|"), undef, "h4,'b',sp='|'"  ]
        , [ hash(h4, "b",notfound=3, sp="|"), 3, "h4,'b',notfound=3,sp='|'"  ]
        , [ hash(h4, sp="|"), h1, "h4,sp='|'"  ]
        */
    //    ,""
    //    ,"var h5"
    //    ,""
    //    , [ hash(h5,"a"), 3, "h5,'a'"]
    //    , [ hash(h5,"b"), "B", "h5,'b'"]
        
        
        
    //    , [ hash(h, 2.3), 5, "h,2.3" ]
    //    , [ hash(h, [0,1]), 10, "h,[0,1]" ]
    //    , [ hash(h, true), 1, "h,true" ]
    //    , [ hash(h, 1), false, "h,1" ]
    //    , [ hash(h, "xx"), undef, "h,'xx'" ]
    //
    //    , "## key found but it's in the last item without value:"
    //    , [ hash(h, -5), undef, "h, -5" ]
    //    , "## notfound is given: "
    //    , [ hash(h, -5,"df"), "df", "h,-5,'df'", ["//", "value not found"] ]
    //    , [ hash(h, -50,"df"), 20, "h,-50,'df'" ]
    //    , [ hash(h, -100,"df"), "df", "h,-100,'df'" ]
    //    , "## Other cases:"
    //    , [ hash([], -100), undef, "[],-100" ]
    //    , [ hash(true, -100), undef, "true,-100" ]
    //    , [ hash([ [undef,10]], undef), undef, "[[undef,10]],undef" ]
    //    , " "
    //    , "## Note below: you can ask for a default upon a non-hash !!"
    //    , [ hash(true, -100, "df"), "df", "true,-100,'df'"]
    //    , [ hash(true, -100), undef, "true,-100 "]
    //    , [ hash(3, -100, "df"), "df", "3,-100,'df'"]
    //    , [ hash("test", -100, "df"), "df", "'test',-100,'df'"]
    //    , [ hash(false, 5, "df"), "df", "false,5,'df'"]
    //    , [ hash(0, 5, "df"), "df", "0,5,'df'" ]
    //    , [ hash(undef, 5, "df"), "df", "undef,5,'df'" ]
    //    , "  "
    //    , "## New 20140528: if_v, then_v, else_v "
    //    , ""
    //    , str("> h= ", _fmth(h, eq=", ", iskeyfmt=true))
    //    , ""
    //    , [ hash(h, 2.3, if_v=5, then_v="got_5"), "got_5", "h,2.3,if_v=5, then_v='got_5'" ]
    //    , [ hash(h, 2.3, if_v=6, then_v="got_5"), 5, "h,2.3,if_v=6, then_v='got_5'" ]
    //    , [ hash(h, 2.3, if_v=6, then_v="got_5", else_v="not_5"), "not_5", "h, 2.3, if_v=6, then_v='got_5', else_v='not_5'"
    //             ]
    //    ,"  "
    //    ,"## New 20140730: You can use string for single-letter hash:"
    //    ,[hash("a1b2c3","b"), "2", "'a1b2c3','b'" ]
    //    ,[hash("aAbBcC","b"), "B", "'aAbBcC','b'" ]
        //           ," "
        //           ,"## 20150108: test concat "
        //           ,["['a',3,'b',4,'a',5]", hash(["a",3,"b",4,"a",5],"a"), 3] 
        ], mode=mode, opt=opt
            , scope=["h1",h1] //,"h2",h2,"h3",h3,"h4",h4,"h5",h5]
        )
    );

    //echo( hash_test( mode=12 )[1] );
     
//========================================================
function hashs(h,ks, notfound, _i=0)= 
(
  h==undef || _i>len(ks)-1? h
  : let ( h = hash(h, ks[_i]) )
    h==undef? notfound
       : hashs( h, ks, notfound, _i+1 )
);
    
hashs=[ "hashs", "h,ks, notfound=undef", "Any","Hash",
    "Given an array (h, arranged like a multiple layers of hash, 
    ;; h=[ h1,[k,v],h2,[k2,v2] ...] and a list of keys (ks), return 
    ;; a v.
    ;; 
    ;;  hashs(h, [\"a\",\"b\"] )
    ;;  = hash( hash( h, \"a\"), \"b\")
    "];

    function hashs_test( mode=MODE, opt=[] )=
    (
    //	let(h= 	[ -50, 20 
    //			, "xy", [0,1] 
    //			,[0,1],10
    //			, true, 1 
    //            , "c",true
    //			]
    //       ,h2= "c;xy=[0,1];true=1"
    //       ,h3= ["c;xy=[0,1]", -50,20
    //       )

        let( h1= ["arm",["len",3], "leg",["len",5] ] 
           , h2= ["a",false, "b",0, "c", undef] //;a=30;xy=[0,1]","r",3]
           , h3= "c;a=30;xy=[0,1];r=3"
           , h4= "c|a=30|xy=[0,1]|r=3"
           , h5= "a=3;b=B;c=3"
           )
        doctest( hashs,  
        [ 
          ""
        , ""
        , [ hashs(h1, ["arm","len"]), 3
             , "[\"arm\",[\"len\",3],\"leg\",[\"len\",5]], [\"arm\",\"len\"]" ]
        , [ hashs(h1, ["leg","len"]), 5
             , "[\"arm\",[\"len\",3],\"leg\",[\"len\",5]], [\"leg\",\"len\"]" ]
        ,""
        ], mode=mode, opt=opt
            , scope=["h1",h1] //,"h2",h2,"h3",h3,"h4",h4,"h5",h5]
        )
    );    
//========================================================
function getv( h,k,nf)=
( 
  let( h = isstr(h) || (len(h)%2==1)? popt(h):h )
  und( [ for(i=[0:2:len(h)-2])
          if( h[i]==k ) h[i+1] 
       ][0], nf )
);          

    getv=["getv", "h,k,nf", "opt", "opt,hash",
    "Similar to hash(h,k,notfound) but also take care of string option (sopt)
    ;;
    ;; h could be one of :
    ;;  sopt 'a;b=3;e'       // string opt
    ;;  mopt ['a;b=3','r',1] // mixed opt
    ;;  popt ['a',true,'b',3,'r',1] // pure opt
    ;; 
    ;; Same as: hash( popt(h), k)
    "];

    function getv_test( mode=MODE, opt=[] )=
    (
        doctest( getv,
        [
          [getv("a;b=3;e","a")
             ,true, "'a;b=3;e','a'" 
          ]
        , [getv("a;b=3;e","b"), 3, "'a;b=3;e','b'" 
          ]
        , [ getv(["a;b=3","e",false], "b"), 3, "['a;b=3','e',false],'b'" 
          ]   
        , [ getv(["a",true,"b",3,"e",false], "e"), false
              ," [\"a\",true,\"b\",3,\"e\",false], 'e'"
          ] 
        ]
        ,mode=mode, opt=opt )
    );      
          
//echo( hash( ["c;a=30;xy=[0,1]", "r", 3], "c") );
          
//========================================================
function popt(h, df=[], sp=";")= 
(
  // If h or h[0] is a sopt, convert h to pure hash;
  // Else, return h
  //
  //  h = ["a",3,"d",4,"c",5]  // pure hash
  //  h = ["a=3;d=4", "c",5] // hybrid between h and sopt
  //  h = "a=3;d=4;c=5"  // a sopt          
//  isstr(h)? 
//    [for(s=ss)
//       let( kv=split(s,"=")
//          , k= getv(df,kv[0],kv[0]) )
//       len(kv)==1? [ k,true ]:[k,eval(kv[1]) ]
//    ]      
//  : len(h)%2==1?
//        update( h[0], slice(h,1) )
//  : h
//);    
    
//          sopt( h, df, sp)
//  : len(h)%2==1? update( sopt(h[0],df,sp), slice(h,1) ) // 
//  : h        
//          
          
     isstr(h)? sopt(h,df=df,sp=sp)
     :len(h)%2==1
       ? update( sopt(h[0],df=df,sp=sp), slice(h,1) )
       : h     
);   

    popt=["popt", "h,df=[],sp=';'", "opt", "opt,core",
    "make sure h is a pure opt.
    ;;
    ;; h could be one of :
    ;;  sopt 'a;b=3;e'       // string opt
    ;;  mopt ['a;b=3','r',1] // mixed opt
    ;;  popt ['a',true,'b',3,'r',1] // pure opt
    ;; 
    ;; popt() makes sure the output is a popt.
    "];

    function popt_test( mode=MODE, opt=[] )=
    (
        doctest( popt,
        [
          [popt("a;b=3;e")
             ,["a",true,"b",3,"e",true], "'a;b=3;e'" 
          ]
        , [ popt(["a;b=3","e",false])
              ,["a",true,"b",3,"e",false], ["a;b=3","e",false] 
          ]   
        , [ popt(["a",true,"b",3,"e",false])
              ,["a",true,"b",3,"e",false]
              , ["a",true,"b",3,"e",false] 
              , ["nl",true]
          ] 
        ,""
        ,"Use <b>df</b> to define a shortcut for key names."
        ,"The name 'e' is converted to 'esc'."
        ,""
        , [popt("a;b=3;e",df=["e","esc"])
             ,["a",true,"b",3,"esc",true]
             , "'a;b=3;e',df=['e','esc']" 
              , ["nl",true]
          ]
        ,""
        ,"This <b>df</b> only works on the sopt. See that the"
        ,"'e' below is not converted:"
        ,""
        , [popt(["a;b=3","e",false],df=["e","esc"])
             ,["a",true,"b",3,"e",false]
             , "['a;b=3','e',false],df=['e','esc']" 
              , ["nl",true]
          ]
        
        ]
        ,mode=mode, opt=opt )
    );

    //echo( popt_test( mode=112) [1] );
     
//========================================================

function hashex(h, keys)=
    (joinarr( [for(k=keys) 
                let( v=hash(h,k) )
                if(v) [k,v] ] )
    );
////  let( hh = [for(k=keys) [k, hash(h,k)] ] )
////  [ for(kv=hh, x=kv) x ]   ;
      
     hashex= ["hashex", "h,keys", "hash", "Hash",
    "Extract a sub hash from a hash based on given keys"
    ];
    function hashex_test( mode=MODE, opt=[] )=
    (
        //_mdoc("hashex_test","");
        let( h=	[ -50, 20 
                , "xy", [0,1] 
                , 2.3, 5 
                ] 
           )
        doctest( hashex,
        [ 
          str("> h= ", _fmth(h, eq=", ", iskeyfmt=true))
        , ""
        , [ hashex(h, ["xy",2.3]), ["xy", [0,1], 2.3, 5] 
           , "h, ['xy',2.3]"
          ]
        ]
        , mode=mode, opt=opt )
    ); 
//========================================================

function hashkvs(h)=
(
    let( h = popt(h) )            
	[for(i=[0:2:len(h)-2]) [h[i],h[i+1]]]
);

    hashkvs=["hashkvs", "h", "array of arrays", "Hash",
    "
     Given a hash (h), return [[k1,v1], [k2,v2] ...].
    "];

    function hashkvs_test( mode=MODE, opt=[] )=
    (
        let( h = ["a",1,  "b",2, 3,33] )

        doctest( hashkvs,
        [ 
          str("h= ", _fmth(h))
        , [ hashkvs(h), [["a",1],["b",2],[3,33]], h ]
        ]
        , mode=mode, opt=opt
        )
    );


//========================================================

function haskey(h,k)=
(
	len([for( i=[0:2:len(h)-2]) if(h[i]==k)k ])>0
    //    index(keys(h),k)>=0
);

    haskey=[ "haskey", "h,k", "T|F", "Hash, Inspect",
    "
     Check if the hash (h) has a key (k)
    "];
     
    function haskey_test( mode=MODE, opt=[] )=
    (
        let( table = [ -50, 20 
                , "80", 25 
                , 2.3, "yes" 
                ,[0,1],5
                , true, "YES" 
                ] )
                
        doctest( haskey, 
        [ 
          str( "table= ", _fmth(table))
        , [ haskey(table, -50), true, "table, -50" ]
        , [ haskey(table, "80"), true, "table, '80'" ]
        , [ haskey(table, "xy"), false, "table, 'xy'" ]
        , [ haskey(table, 2.3), true, "table, 2.3" ]
        , [ haskey(table, [0,1]), true, "table, [0,1]" ]
        , [ haskey(table, true), true, "table, true" ]
        , [ haskey(table, -100), false, "table, -100" ]
        , [ haskey([], -100), false, "[], -100" ]
        , [ haskey( [undef,1], undef), true, "[[undef,1], undef", 
                     , ["//","# NOTE THIS!"] ]
        ],mode=mode, opt=opt//, scope=["table", table]
        )
    );
    
//========================================================

function keys(h)=
(
	[for(i=[0:2:len(h)-2]) h[i]]
);

    keys=["keys", "h", "array", "Hash",
    "
     Given a hash (h), return keys of h as an array.
    "];

    function keys_test( mode=MODE, opt=[] )=(//====================
        
        doctest( keys, 
        [ 
          [ keys(["a",1,"b",2]), ["a","b"] , "['a',1,'b',2]"]
        , [ keys([1,2,3,4]), [1,3] , "[1,2,3,4]"]
        , [ keys([[1,2],0,3,4]), [[1,2],3] , "[1,2],0,3,4"]
        , [ keys([]), [] , "[]"]
        ]
        , mode=mode, opt=opt
        )
    );
    
//========================================================
   
function kidx(h,k)=
    [for(i=[0:2:len(h)-2]) if(h[i]==k)i/2][0];

    kidx=["kidx", "h,k", "hash", "Array, Hash"
    , "Return index of key k in hash h. It's the order of k
    ;; in keys(h).
    ;; 2015.1.30"
    ];

    function kidx_test( mode=MODE, opt=[] )=(//====================
        
        doctest( kidx, 
        [ 
        ]
        , mode=mode, opt=opt
        )
    );
   
//========================================================
function sopt(s, df=[], sp=";")=  
(  // sopt: string opt to set opt
   // s : "b;c;name=kkk,age=30"
   // df: ["b","border","c","chain", "d","diameter" ... ]
   let(
     ss = split(s,sp)
     //,df= update( ["cl","color","pl","plane"], df) // default name: clr,trp
     ,ss2= [for(s=ss)
              if(s)
              let( kv=split(s,"=")
                 , k= hash(df,kv[0],kv[0]) )
              len(kv)==1? [ k,true ]
                        : [k,eval(kv[1]) ]
           ]
     )
     joinarr( ss2 )
);

    sopt=["sopt","s,df=[],sp=';'","opt","String,Hash,Core,Inspect",
      "string shortcut to set an opt
    ;;
    ;;  s : list of k=v or to-be-true flags
    ;;     'b;c;grid;name=kkk;age=30'
    ;;
    ;;  flag could be a property name, like grid, or a shortcut
    ;;  of that, like c,b, which are defined in df:
    ;;
    ;; df: ['b','border','c','chain', 'd','diameter' ... ]
    "
    ,"uopt,subopt1"];

    function sopt_test( mode=MODE, opt=[] )=
    (
        doctest( sopt, 
        [
          [sopt("c;d"), ["c",true,"d",true], "'c;d'"]
        , [sopt("b"), ["b",true], "'b'"]
        , [sopt("b;"), ["b",true], "'b;'"]
        , [sopt(";;b;"), ["b",true], "';;b;'"]
        , [sopt("b=-"), ["b","-"], "'b=-'"]
        , [sopt("b=-;c"), ["b","-","c",true], "'b=-;c'"]
        , [sopt("c;e=3;f=name;x=[0,1]")
                , ["c",true,"e",3,"f","name","x",[0,1]]
                , "'c;e=3;f=name'"
                , ["nl",true]]
        ,""
        ,"Use <b>df</b> to define a shortcut for key names."
        ,"The name 'e','c' are converted to 'esc','chain',"
        ,"respectively."
        ,"Also note that f='name' is the same as f=name." 
        ,""
        
        , [sopt("c;e=3;f=\"name\"", df=["c","chain","e","esc"])
                , ["chain",true,"esc",3,"f","name"]
                , "'c;e=3;f=\"name\"', df=['c','chain','e','esc']"
                , ["nl",true]]
                
        ]
        , mode=mode, opt=opt )
    );
    //echo( sopt_test( mode=112)[1] );
    //echo(hash("abcd","c") );
    //echo(hash("","c","x") );


//========================================================
//function sortByKey(hashs,key, isreverse=0)=  // hashs is a hash of hashes
//(
//   let( a = [ for(kvs=hashkvs(a_hashs)) // each val is a hash
//              [ hash(kvs[1],key), ]
//                
//
//);

//========================================================

function update(h,g,df=[])=
(
  let( h= popt(h,df) //isarr(h)?h:[]
     , g= popt(g,df) //isarr(g)?g:[] 
     )
//  let( h= isarr(h)?h:[]
//     , g= isarr(g)?g:[] 
//     )
  !g? h //<=============== this line added 2015/3/17
  :!h? g //<=============== this line added 2015/6/10
  :[ for(i2=[0:len(h)+len(g)-1])      // Run thru entire range of h + g
      let(is_key= round(i2/2)==i2/2  // i2 is even= this position is a key
         ,is_h = i2<len(h)           // Is i2 currently in the range of h ?
         ,cur_h = is_h?h:g           // Is current hash the h or g?
         ,cur_i = is_h?i2:(i2-len(h)) // Convert i2 to h or g index
         ,k= cur_h[ is_key?cur_i:cur_i-1 ]  // Current key
         ,overlap = kidx((is_h?g:h),k)>=0   // Is cur k-v overlap btw h & g?
      )
      if( !(overlap&&!is_h)) // Skip if overlap and current hash is g
         is_key? k
               : overlap?hash(g,k):cur_h[cur_i]    
   ]
);

    update= ["update", "h,h1,df=[],sp=';'", "hash", "Hash" ,
    "
     Given two hashes (h,h1) update h with items in h1.
    "
    ];
    function update_test( mode=MODE, opt=[] )=
    (
        let( a1= ["a",1,"b",2,"c",3] )
        
        doctest( update,
        [
          ""
        ,[ update( [1,11],[2,22] ), [1,11,2,22]  , "[1,11],[2,22]"]
        , [ update( [1,11,2,22], [2,33] ), [1,11,2,33]  , "[1,11,2,22],[2,33]"]
        , [ update([1,11,2,22],[1,3,3,33]), [1,3,2,22,3,33], "[1,11,2,22],[1,3,3,33]"]
        , [ update([1,11,2,22],[]), [1,11,2,22], "[1,11,2,22],[]"]
         , ""
         , " Clone a hash: "
         , ""
        , [ update( [],[3,33] ), [3,33]  , "[],[3,33]"]
         , ""
         , " Show that the order of h2 doesn't matter"
         , ""
         , str("a1 = ", a1)
         , [ update( a1,["b",22,"c",33] )
            , ["a",1,"b",22,"c",33], "a1, ['b',22,'c',33]"
             ]
         , [ update( a1, ["c",33,"b",22] )
            ,["a",1,"b",22,"c",33], "a1, ['c',33,'b',22]"
            ]
         , ""
         , "2015.6.3: can take <b>sopt</b> or <b>mopt</b>"
         , ""
         , "// update a sopt w/ a popt:"
         , [ update("a;b",["c",1])
              , ["a",true, "b", true, "c",1]
              , "'a;b',['c',1]"
           ]   
         
         , "// update a popt w/ a sopt"  
         , [ update(["c",1],"a=2;x=[0,1]")
              , ["c",1,"a",2, "x",[0,1]]
              , "['c',1],'a=2;x=[0,1]'"
           ]  
      
         , "// update a popt w/ a mopt " 
         , [ update(["c",1],["a;b","x",[0,1]])
              , ["c",1,"a",true,"b",true, "x",[0,1]]
              , "['c',1],['a;b','x',[0,1]]"
              , ["nl",true]
           ]    

        ,""
        ,"Use <b>df</b> to define a shortcut for key names."
        ,"  The following <b>df</b> intends to define 'c','a'"
        ,"  and 'x' as 'cycle', 'angle' and 'Y', respectively."
        ,"  Only 'a'=>'angle' works, `cos <b>df</b> only works for"
        ,"  sopt."
        ,""
        
         , [ update(["c",1],["a;b","x",[0,1]]
                 , df=["c","cycle","a","angle","x","Y"])
           , ["c",1,"angle",true,"b",true, "x",[0,1]]
           , "['c',1],['a;b','x',[0,1]] <br/>| 
              ,df=['c','cycle','a','angle','x','Y']"
           , ["nl",true]
           ]    
           
         //, ""
         //, "Customize <b>sp</b> for sopt:"
         //, ""
         //, [ update( "a/b=1","x=[0,1]/d", sp="/")
           //, ["a", true, "b",1,"x",[0,1],"d",true]
           //, "'a/b=1','x=[0,1]/d', sp='/'"
           //, ["nl",true]
           //]
           
        ], mode=mode, opt=opt 
        )
    );

    //echo( update_test( mode=12 )[1]);

//========================================================
 

function update2(h,g,_hks, _gks, _rtn,_i=-1,_j=-1)=
(
  // update2: 
  //
  //   For some special args, instead of just replace old values,
  //   apply special-arg handlers to either merge special new and old
  //   values, or apply extra process to handle them
  //        
  //   size (3, [3,3,3] ==> ensure 3-num array)
  //   color ("red", ["red",0.5] ==> ensure 2-item array)
  //   actions (concat them)
  //   scale (process them: 3, [3,3,3] ==> ensure 3-num arr
  //         and then merge them: old: [2,2,2] new: [3,3,1] => [6,6,2] 
  //         )
  //
  //  2018.2.4 
   
  let( h= popt(h)
     , g= popt(g)  
     )   
  !g? h //<=============== this line added 2015/3/17
  :!h? g //<=============== this line added 2015/6/10
  :[ for(i2=[0:len(h)+len(g)-1])      // Run thru entire range of h + g
      //let(is_key= round(i2/2)==i2/2  // i2 is even= this position is a key
      let(is_key= i2%2==0             // i2 is even= this position is a key
         ,is_h = i2<len(h)           // Is i2 currently in the range of h ?
         ,cur_h = is_h?h:g           // Is current hash the h or g?
         ,cur_i = is_h?i2:(i2-len(h)) // Convert i2 to h or g index
         ,k= cur_h[ is_key?cur_i:cur_i-1 ]  // Current key
         ,overlap = kidx((is_h?g:h),k)>=0   // Is cur k-v overlap btw h & g?
      )
      if( !(overlap&&!is_h)) // Skip if overlap and current hash is g
         is_key? k
         : overlap?
           ( k=="color"? let( hv=assureColor(hash(h,k))
                            , gv=assureColor( hash(g,k)
                                            , df=[undef,1] )) 
                                            // Set df=[undef,1] so we can do
                                            // update(["color","red"],["color",0.3]) 
                                            // ==> ["color",["red",0.3]]
                                            // Otherwise, gv will be df to ["gold",0.3]
                                            // and it will give ["color",["gold",0.3] ]
                                            // --- 2018.10.5 
                         //[hash(h,k),hash(g,k),hv,gv]
                         [ gv[0]==undef?(hv[0]==undef?"gold":hv[0])
                                       : gv[0]==undef?"gold":gv[0]
                         , gv[1] ]   
           : k=="scale"? let( hv=assureTriplet(hash(h,k))
                            , gv=assureTriplet(hash(g,k)))
                         //echo( str("update2-scale, h=",h,", g=", g,", hv=",hv,", gv=",gv) )   
                         [hv[0]*gv[0], hv[1]*gv[1], hv[2]*gv[2]]
           : k=="actions" ? concat( hash(h,k), hash(g,k) )
           : k=="size"? assureTriplet(hash(g,k)) 
           : hash(g,k)
           )
         : ( k=="color"? assureColor(cur_h[cur_i], "gold")
           : k=="size"||k=="scale"? assureTriplet(cur_h[cur_i]) 
           : cur_h[cur_i]
           )    
   ]
);

update2= ["update2", "h,g", "hash", "Hash" ,
    "
     Advanced version of update() with special treatments for 
    ;; 'size','scale','actions','color'
    ;;
    ;;  size (3, [3,3,3] ==> ensure 3-num array)
    ;;  color ('red', ['red', 0.5] ==> ensure 2-item array)
    ;;  actions (concat them)
    ;;  scale (process them: 3, [3,3,3] ==> ensure 3-num arr
    ;;        and then merge them: old: [2,2,2] new: [3,3,1] => [6,6,2] 
    ;;        )
    "
    ];
    function update2_test( mode=MODE, opt=[] )=
    (
        let( a1= ["a",1,"b",2,"c",3] )
        
        doctest( update2,
        [
          ""
        ,[ update2( [1,11],[2,22] ), [1,11,2,22]  , "[1,11],[2,22]"]
        , [ update2( [1,11,2,22], [2,33] ), [1,11,2,33]  , "[1,11,2,22],[2,33]"]
        , [ update2([1,11,2,22],[1,3,3,33]), [1,3,2,22,3,33], "[1,11,2,22],[1,3,3,33]"]
        , [ update2([1,11,2,22],[]), [1,11,2,22], "[1,11,2,22],[]"]
         , ""
         , " Clone a hash: "
         , ""
        , [ update2( [],[3,33] ), [3,33]  , "[],[3,33]"]
         , ""
         , " Show that the order of h2 doesn't matter"
         , ""
         , str("a1 = ", a1)
         , [ update2( a1,["b",22,"c",33] )
            , ["a",1,"b",22,"c",33], "a1, ['b',22,'c',33]"
             ]
         , [ update2( a1, ["c",33,"b",22] )
            ,["a",1,"b",22,"c",33], "a1, ['c',33,'b',22]"
            ]
         , ""
         , "2015.6.3: can take <b>sopt</b> or <b>mopt</b>"
         , ""
         , "// update2 a sopt w/ a popt:"
         , [ update2("a;b",["c",1])
              , ["a",true, "b", true, "c",1]
              , "'a;b',['c',1]"
           ]   
         
         , "// update2 a popt w/ a sopt"  
         , [ update2(["c",1],"a=2;x=[0,1]")
              , ["c",1,"a",2, "x",[0,1]]
              , "['c',1],'a=2;x=[0,1]'"
           ]  
      
         , "// update2 a popt w/ a mopt " 
         , [ update2(["c",1],["a;b","x",[0,1]])
              , ["c",1,"a",true,"b",true, "x",[0,1]]
              , "['c',1],['a;b','x',[0,1]]"
              , ["nl",true]
           ]    
	, ""
	, "//..............................................."
	, "// The above are functions of the update(). Below update2(): 2018.2.4 Superbowl Eagles vs Patriots"
	, "// Note that a single value of scale, color and size are expanded into an array"
	, "// , and actions are appended"
	, "// NOTE: if one of them is empty, they will not be processed:"  
	, ""
	, "// color"
	, [ update2( ["a",1],["color",2]), ["a",1,"color",["gold",1]]
	    , "['a',1],['color',2]"
	  ]  
	, [ update2( ["color",0.5], ["a",1]), ["color",["gold",0.5], "a",1]
	    , "['color',0.5],['a',1]"
	  ]  
	, [ update2( ["color","red", "a",1],["color",0.6]), ["color",["red",0.6], "a",1]
	    , "['color','red', 'a',1],['color',0.6]"
	  ] 
	  
	, [ update2( ["color","red"],["color",0.4]), ["color",["red",0.4]]
	    , "['color','red'],['color',0.4]"
	  ] 
	, [ update2( ["color","red"],["color",0.2]), ["color",["red",0.2]]
	    , "['color','red'],['color',0.2]"
	  ] 
	  
	, "", "// size"
	
	, [ update2( ["a",1],["size",2]), ["a",1,"size",[2,2,2]]
	    , "['a',1],['size',2]"
	  ]  
	, [ update2( ["size",2], ["a",1]), ["size",[2,2,2], "a",1]
	    , "['size',2],['a',1]"
	  ]  
	, [ update2( ["size",3, "a",1],["size",2]), ["size",[2,2,2], "a",1]
	    , "['size',3, 'a',1],['size',2]"
	  ] 
	, [ update2( [],["size",2]), ["size",2]
	    , "[],['size',2]"
	  ]  
	, [ update2( ["size",2],[]), ["size",2]
	    , "['size',2],[]"
	  ]
	     
	, "", "// scale"
	  
	, [ update2( ["a",1],["scale",2]), ["a",1,"scale",[2,2,2]]
	    , "['a',1],['scale',2]"
	  ]  
	, [ update2( ["scale",2], ["a",1]), ["scale",[2,2,2], "a",1]
	    , "['scale',2],['a',1]"
	  ]  
	, [ update2( ["scale",3, "a",1],["scale",2]), ["scale",[6,6,6], "a",1]
	    , "['scale',3, 'a',1],['scale',2]"
	  ]  
	, [ update2( ["scale",[1,2,3], "a",1],["scale",2]), ["scale",[2,4,6], "a",1]
	    , "['scale',[1,2,3], 'a',1],['scale',2]"
	  ]  
	, [ update2( [],["scale",2]), ["scale",2]
	    , "[],['scale',2]"
	  ]  
	, [ update2( ["scale",2],[]), ["scale",2]
	    , "['scale',2],[]"
	  ]
	  
	,"" ,"// actions"
	
	, [ update2( ["a",1], ["actions",["y",1]])
	    , ["a",1, "actions",["y",1] ] 
	    , "['a',1], ['actions',['y',1]]"
	  ]  
	, [ update2( ["actions",["x",2]],["a",1])
	    , ["actions",["x",2],"a",1 ] 
	    , "['actions',['x',2]],['a',1]"
	  ]  
	, [ update2( ["actions",["x",2],"a",1], ["actions",["x",1]])
	    , ["actions",["x",2,"x",1],"a",1 ] 
	    , "['actions',['x',2],'a',1], ['actions',['x',1]]"
	  ]  
	  
	, [ update2( ["actions",["x",2],"a",1], ["actions",["y",1]])
	    , ["actions",["x",2,"y",1],"a",1 ] 
	    , "['actions',['x',2],'a',1], ['actions',['y',1]]"
	  ]  
		
		
        //,""
        //,"Use <b>df</b> to define a shortcut for key names."
        //,"  The following <b>df</b> intends to define 'c','a'"
        //,"  and 'x' as 'cycle', 'angle' and 'Y', respectively."
        //,"  Only 'a'=>'angle' works, `cos <b>df</b> only works for"
        //,"  sopt."
        //,""
        //
         //, [ update2(["c",1],["a;b","x",[0,1]]
                 //, df=["c","cycle","a","angle","x","Y"])
           //, ["c",1,"angle",true,"b",true, "x",[0,1]]
           //, "['c',1],['a;b','x',[0,1]] <br/>| 
              //,df=['c','cycle','a','angle','x','Y']"
           //, ["nl",true]
           //]    
           
           
        ], mode=mode, opt=opt 
        )
    );



function updatekey(h,k,v,newfirst=false)=  // h=["a",["b",v]] => updatekey(h,"a",["b",v2])=> ["a",["b",v2]]
(
  let(v0= hash(h,k,[]))
  newfirst?update(h, [k, update(v,v0)])
  : update(h, [k, update(v0,v)])
);

    updatekey= ["updatekey", "h,k,v,newfirst=false", "hash", "Hash" ,
    "Update a hash inside h.
    ;; 
    ;; h=['a',['b',v]] 
    ;; updatekey(h,'a',['b',v2])=> ['a',['b',v2]]
    ;; updatekey(h,'a',['c',v2])=> ['a',['b',v,'c',v2]]
    ;; updatekey(h,'a',['c',v2],newfirst=true)=> ['a',['c',v2,'b',v]]
    ;;
    ;; usage: LabelPt 
    ;; (2018.1.29)
    "
    ];
    function updatekey_test( mode=MODE, opt=[] )=
    (
        let( a1= ["a",1,"b",2,"c",3] )
        
        doctest( updatekey,
        [
          ""
        , [updatekey(["a",["b",3]], "a", ["b",4]), ["a",["b",4]],"['a',['b',3]], 'a',['b',4]" ]
        , [updatekey(["a",["b",3],"c",3], "a", ["b",4]), ["a",["b",4],"c",3],"['a',['b',3],'c',3], 'a',['b',4]" ]
        , [updatekey(["a",["c",3]], "a", ["b",4]), ["a",["c",3, "b",4]],"['a',['c',3]], 'a',['b',4]" ]
        , [updatekey(["a",["c",3]], "a", ["b",4],true)
               , ["a",["b",4,"c",3]],"['a',['c',3]], 'a',['b',4], newfirst=true" ]
        , [updatekey([], "a", ["b",4]), ["a",["b",4]],"[], 'a',['b',4]" ]
           
        ], mode=mode, opt=opt 
        )
    );

function update2s(h,hs,_i=0)=
(
  _i>=len(hs)? h
  : update2s( update2(h, hs[_i]), hs, _i=_i+1)
);

//========================================================
function updates(h,hs,df=[], sp=";")=
    let ( _h= hs==undef? h[0]:h
        , hs= hs==undef? slice(h,1): hs 
        )
(	len(hs)==0? _h
	: updates( update(_h,hs[0]), slice(hs,1), df=df, sp=sp )
);

    updates= ["updates", "h,hs", "hash", "Hash" ,
    "
     Given a hash (h) and an array of multiple hashes (hs),
    ;; update h with hashes one by one.
    ;; 
    ;; New 2015.2.1: no hs, enter h as a one-layer array: updates(hs).
    ;; That is, updates the hs[0] with the rest in hs
    "
    ];

    function updates_test( mode=MODE, opt=[] )=
    ( 
        let( a1= [1,11,2,22]
           , a2= [1,3,3,33]
           , a3= ["a",1,"b",2,"c",3]
           )
        doctest( updates	  
        ,[
          ""
          ,str("a1=", a1)
          ,str("a2=", a2)
          ,[ updates( a1,[[2,20],a2] )
            , [1, 3, 2, 20, 3, 33] 
            , "a1,[[2,20],a2]"
           ]	 
         , [ updates( a1,[[1,3,3,33],[2,20]] )
            , [1, 3, 2, 20, 3, 33] 
            , "a1,[a2,[2,20]]"
            
           ]	 
         , [ updates( a1,[[1,3,3,33],[],[2,20]] )
            , [1, 3, 2, 20, 3, 33]
            , "a1, [a2,[],[2,20]]"
           ]	 
         , [ updates( a1,[[1,3,3,33,2,20]] )
            , [1, 3, 2, 20, 3, 33] 
            , "a1, [[1,3,3,33,2,20]]"
           ]	 
         , [ updates( a1,[[1,3,3,33],[2,20],[2,12,4,40]] )
            , [1,3, 2,12, 3,33, 4,40] 
            ,"a1,[a2,[2,20],[2,12,4,40]]"
           ]
        , [ updates( [],[[1,3,3,33]] )
            , [1,3,  3,33] 
            , "[],[a2]"
           ]
        , [ updates( [],[[1,3,3,33],[2,20]] )
            , [1,3, 3,33, 2,20] 
            , "[],[a2,[2,20]]"
           ]		 
        , [ updates( [],[[1,3,3,33],[2,20],[2,12,4,40]] )
            , [1,3, 3,33, 2,12, 4,40] 
            , "[],[a2,[2,20],[2,12,4,40]]"
           ]		
         ,""
         ,"New 2015.2.1: enter (h,hs) as a one-layer array: updates(hs). That is, "
         ," updates the hs[0] with the rest in hs"
         ,""
         , str("a3=", a3)
         , [updates( [a3,["a",11,"d",4],["c",33,"e",5] ])
            ,["a",11,"b",2,"c",33,"d",4,"e",5]
            ,"[ a3, ['a',11,'d',4],['c',33,'e',5] ]", ["nl",true]
           ] 
         , [updates( [a3,[],["c",33,"e",5] ])
            ,["a",1,"b",2,"c",33,"e",5]
            //,[a3,[],["c",33,"e",5] ]
            ,"[ a3,[],['c',33,'e',5] ]", ["nl",true]
           ] 
         , [updates( [[],["a",11,"d",4],["c",33,"e",5] ])
            ,["a",11,"d",4,"c",33,"e",5]
            ,[[],["a",11,"d",4],["c",33,"e",5] ], ["nl",true]
            //, "[[], ['a',11,'d',4],['c',33,'e',5] ]"
           ]     
         ,""
         ,"New 2015.6.3: can take sopt and mopt (other than popt)"
         ,""
       
         , [ updates( ["a;b",["c",1],["x=[0,1];d", "c",5]] )
           , ["a",true,"b",true,"c",5,"x",[0,1],"d",true]
           , ["a;b",["c",1],["x=[0,1];d", "c",5]]
           , ["nl",true]
           ]
            
         ], mode=mode, opt=opt
        )
    );
    //echo( updates_test( mode=12 )[1]);

//========================================================
function vals(h)=
(
	//[ for(i=[0:2:len(h)-2]) h[i+1] ]
  [for(i=[1:2:len(h)]) h[i]]  // changed 20170607
);

    vals=[  "vals", "h", "arr",  "Hash",
    "
     Return the vals of a hash as an array.
    "
    ];

    function vals_test( mode=MODE, opt=[] )=
    (
        doctest( 
        vals, [
            [ vals(["a",1,"b",2]), [1,2], "['a',1,'b',2]" ]
          ,	[ vals([1,2,3,4]), [2,4], [1,2,3,4] ]
          ,	[ vals([[1,2],3, 0,[3,4]]), [3,[3,4]],[[1,2],3, 0,[3,4]] ]
          ,	[ vals([]), [],[] ]
          ], mode=mode, opt=opt
        //, [["echo_level",1]]
        )
    );
    //vals_test( ["mode",22] );

/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_hash_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_hash.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_hash_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_hash", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      //all_in_hashs_test( mode, opt ) // to-be-coded
    //, any_in_hashs_test( mode, opt ) // to-be-coded
     delkey_test( mode, opt )
    //, delkeys_test( mode, opt ) // to-be-coded
    , getv_test( mode, opt )
    //, h_test( mode, opt ) // to-be-coded
    , hash_test( mode, opt )
    , hashex_test( mode, opt )
    , hashkvs_test( mode, opt )
    , hashs_test( mode, opt )
    , haskey_test( mode, opt )
    , keys_test( mode, opt )
    , kidx_test( mode, opt )
    , popt_test( mode, opt )
    , sopt_test( mode, opt )
    , update_test( mode, opt )
    , update2_test( mode, opt )
    //, update2s_test( mode, opt ) // to-be-coded
    , updatekey_test( mode, opt )
    , updates_test( mode, opt )
    , vals_test( mode, opt )
    //, vals_of_key_test( mode, opt ) // to-be-coded
    ]
);
