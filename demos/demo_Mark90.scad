include <../scadx.scad>

module Mark90_demo()
{
	_pqr = randPts(3);
    p90 = anglePt(_pqr, a=90,a2=0);
    pqr = replace(_pqr,-1,p90);
    opt = "pl=[transp,0.1];ball=0;l=PQR";
    //MarkPts( pqr,opt);
    Line(pqr);
    Mark90(pqr);
    
    pqr2= trslN(pqr,1);
    Line(pqr2, color="red");
    Mark90( pqr2, "cl=red");

    pqr3= trslN(pqr,2);
    Line(pqr3, color="green");
    Mark90( pqr3, "len=1;r=0.05;trp=1");

    pqr4= trslN(pqr,3);
    Line(pqr4, color="blue");
    Mark90( pqr4, 0.1);
    
}

Mark90_demo();