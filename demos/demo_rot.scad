include <../scadx.scad>
//
//   Demo rotM and its usage on rotObj and rotPt, rotPts
//


 
//. rotObj

module demo_RotObj()
{
  echom("demo_RotObj()");
  module C()
  {translate([1,2,0]) cube([3,2,1]);}

  RotObj( axis=[[1,0,0],[1,1,0]], a=135) color("red",0.9) C();
  echo(_red( "axis=[[1,0,0],[1,1,0]], a=135") );

  RotObj( axis=[[4,2,0],[1,4,1]], a=45) color("green",1) C();
  echo(_green( "axis=[[4,2,0],[1,4,1]], a=45") );
  
  RotObj( axis=[[5,0,0],[5,1,0]], a=-90) color("blue",1) C();
  echo(_blue( "axis=[[4,2,0],[1,4,1]], a=45") );
  
  color("gold",0.9) C();

}

//demo_RotObj();


//. RotFromTo


//. RotObjFromTo

module demo_RotObjFromTo()
{
   echom("demo_RotObjFromTo");
   pqr = pqr(4);
   R = randPt();
   
   module Obj()
   {
     Cube([3,2,1], site=[pqr[0],pqr[1],R] );
   }

   MarkPts(pqr,"PQR");
   Obj();
   Green(0.3) RotObjFromTo(pqr) Obj();
   Plane( expandPts(pqr,3) );

}
demo_RotObjFromTo();

//. rotPts

module demo_rotPts()
{
  echom("demo_rotPts()");
  
  //pts = randPts(4);
  pts= [[0.96, -1.18, -2.49], [0.26, -0.89, -2.05]
       , [1.74, -0.04, -0.3], [-0.25, -1.14, 2.9]];
  echopts("pts= ", pts);
  
  color("red") MarkPts(pts);
  
  pts2 = rotPts( pts, get(pts,[1,-1]), 30);
  color("green") MarkPts(pts2);

  pts3 = rotPts( pts, get(pts,[1,-1]), 60);
  color("blue") MarkPts(pts3);
  
}
//demo_rotPts();

module demo_rotPts_2()
{
  echom("demo_rotPts_2()");
  
  pq = randPts(2); 
  echopts("pq= ", pq);
  MarkPts(pq, "PQ");
  
  pts= [[0.96, -1.18, -2.49], [0.26, -0.89, -2.05]
       , [1.74, -0.04, -0.3], [-0.25, -1.14, 2.9]];
  echopts("pts= ", pts);
  
  color("red") MarkPts(pts);
  
  pts2 = rotPts( pts, pq, 30);
  color("green") MarkPts(pts2);

  //pts3 = rotPts( pts, pq, 60);
  //color("blue") MarkPts(pts3);
  
}
//demo_rotPts_2();
