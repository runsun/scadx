/*
  Demo the transPts() function and Transform() module
  
  Their actions must be consistent

*/

include <../scadx.scad>

module tt_demo_translate()
{
   echom("tt_demo_translate");
   x=8; y=4; z=2;
   
   cube([x,y,z]);

   pts = cubePts([x,y,z]);
   
   polyhedron( pts, faces= rodfaces(4) );

   MarkPts( pts, "l");

}

tt_demo_translate();

/*
  module mirrorPt_demo()
  {
    echo(_red("mp = mirrorPt( pt, pts )"));

    pts = randPts(3);
    pt = randPt();

    mp = mirrorPt( pt, pts );
    pp = projPt( pt, pts );
    echo( mp = mp );

    MarkPts(pts, ["plane", ["color",["blue", .5]], "label","PQR"]);
    MarkPts( [pt,  pp], ["l", "label",["text",["pt","pp"]]] );
    MarkPt( mp , ["g", "label",["text","mp"]]);

    for(p=pts) Mark90( [ p, pp, pt] ); 
  
    Line( [pt, pp] );
    Line( [pp, mp] );  
  }

  module mirrorPt_demo_normalLine()
  {
    echo(_red("mp = mirrorPt( pt, pq )"));

    pq = randPts(2);
    pt = randPt();

    pl = expandPts( getPlaneByNormalLine(pq), 1);
    echo(pl=pl);
    
    mp = mirrorPt( pt, pq );
    pp = projPt( pt, pl );
    echo( mp = mp );

    //color("red") Line(pq);
  
    MarkPts( pq, ["ch","label",["text","pq"]]);
    MarkPts( [pt,mp], "ch");
    
    MarkPts(pl, ["plane", ["color",["blue", .5]]]);
    MarkPts( [pt,  pp], ["l", "label",["text",["pt","pp"]]] );
    MarkPt( mp , ["g", "label",["text","mp"]]);

    for(p=pl) Mark90( [ p, pp, pt] ); 
 
  }

  module mirrorPt_demo_pt()
  {
    echo(_red("mp = mirrorPt( pt, pt2 )"));

    pt = randPt();
    pt2 = randPt();
    pq = [ORIGIN,pt2];
    pl = expandPts( getPlaneByNormalLine( [ORIGIN, pt2]), 1);
    echo(pl=pl);
    
    mp = mirrorPt( pt, pt2 );
    pp = projPt( pt, pl );
    echo( mp = mp );

    //color("red") Line(pq);
  
    MarkPts( pq, ["ch","label",["text","pq"]]);
    MarkPts( [pt,mp], "ch");
    
    MarkPts(pl, ["plane", ["color",["blue", .5]]]);
    MarkPts( [pt,  pp], ["l", "label",["text",["pt","pp"]]] );
    MarkPt( mp , ["g", "label",["text","mp"]]);

    for(p=pl) Mark90( [ p, pp, pt] ); 
 
  }
*/