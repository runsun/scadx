include <../scadx.scad>

LabelPt_demo();

module LabelPt_demo()
{
    echom("LabelPt_demo");
    P=randPt(); 
    d= [-0.5,0.5,0];
    Q=P+d; 
    R=Q+d;
    S=R+d;
    T=S+d;
    U=T+d;
    V=U+d;
    
    LabelPt(P); 
    MarkPt(P, "Marked"); //translate(P)sphere(0.2, $fn=12);
    
    
    //LabelPt(Q, "Q"); 
    MarkPt(Q, "Marked", color="red"); //translate(Q)color("red") sphere(0.2, $fn=12);
    /*
    //LabelPt(R, "text"); 
    MarkPt(R, color="green"); //translate(R, 123)color("green") sphere(0.2, $fn=12);

    //LabelPt(S, "followView=false", followView=false); 
    MarkPt(S, color="blue"); //translate(R, 123)color("green") sphere(0.2, $fn=12);
    
    //LabelPt(T, opt=["text", "opt.text"]); 
    MarkPt(T, color="purple"); //translate(R, 123)color("green") sphere(0.2, $fn=12);
    
    //LabelPt(U, true); 
    MarkPt(U, color="purple"); //translate(R, 123)color("green") sphere(0.2, $fn=12);
    
    //LabelPt(V, opt=["text", true]); 
    MarkPt(V, color="purple"); //translate(R, 123)color("green") sphere(0.2, $fn=12);
    */
}
//LabelPt_demo();


module LabelPt_demo_fmt()
{
    echom("LabelPt_demo_fmt");
    P = randPt();
    d= [-0.5,0.5,0];
    Q=P+d; 
    R=Q+d;
    S=R+d;
    T=S+d;
    U=T+d;
    
    MarkPt(P);
    MarkPt(Q);
    MarkPt(R);
    MarkPt(S);
    MarkPt(T);
    MarkPt(U);
    
    //translate(Q)sphere(0.2, $fn=12);
    //translate(R)sphere(0.2, $fn=12);
    //translate(S)sphere(0.2, $fn=12);
    
    LabelPt( P, color="red" );
    LabelPt( Q, fmt="Q:{*}", color="green" );
    LabelPt( R, fmt="R:{*`a|d1|wr=/,/}", color="blue" );
    LabelPt( S, fmt="S:==>{*`a%|d2}", color="purple" );
    LabelPt( T, true, fmt="T:==>{*`a%|d2}", color="teal" );
    LabelPt( U, fmt="U:==>{*`a%|d2}", color="olive", opt=["text",true] );

}
//LabelPt_demo_fmt();

module LabelPt_demo_sopt()
{
    echom("LabelPt_demo_sopt");
        
    P = randPt();
    d= [-0.5,0.5,0];
    Q=P+d; 
    R=Q+d;
    S=R+d;
    MarkPt(P);
    MarkPt(Q);
    MarkPt(R);
    MarkPt(S);
        
    LabelPt( P, opt=["color","red"] );
    LabelPt( Q, opt="color=green"); 
    LabelPt( R, opt="color=blue;text=testing the sopt feature" );
    LabelPt( S, color="purple", opt="indent=1" );
}
//LabelPt_demo_sopt();


module LabelPt_demo_scale()
{
    echom("LabelPt_demo_scale");
        
    P = ORIGIN; //randPt();
    d= [0,0.5,0];
    Q=P+d; 
    R=Q+d;
    S=R+d;
    T=S+d+d;
    U=T+2*d;
    MarkPt(P);
    MarkPt(Q);
    MarkPt(R);
    MarkPt(S);
    MarkPt(T);
    MarkPt(U);
        
    LabelPt( P, "|scadx| default" );
    LabelPt( Q, "|scadx| scale=0.5",scale=0.5, opt="color=red"); 
    LabelPt( R, "|scadx| scale=1", scale=1, opt="color=green;text=testing the sopt feature" );
    LabelPt( S, "|scadx| scale=2", scale=2, color="blue" );
    LabelPt( T, "|scadx| opt.scale=2", color="purple", opt=["scale",2] );
    LabelPt( U, "|scadx| scale=2, opt.scale=2", color="teal", scale=2, opt=["scale",2] );
}
//LabelPt_demo_scale();

module LabelPt_demo_actions()
{
    echom("LabelPt_demo_actions");
        
    P = ORIGIN; //randPt();
    d= [0,0.5,0];
    Q=P+d; 
    R=Q+d;
    S=R+d;
    T=S+d;
    U=T+d;
    V=U+3*d;
    W=V+2*d;
    A=W+2*d;
    
//    MarkPt(P);
//    MarkPt(Q);
//    MarkPt(R);
//    MarkPt(S);
//    MarkPt(T);
//    MarkPt(U);
//    MarkPt(V);
//    MarkPt(W);
//    MarkPt(A);
    
        
    LabelPt( P, "default" );
    LabelPt( Q, "actions=[\"rotx\", 45]",actions=["rotx", 45] ); 
    LabelPt( R, color="red", "followView=false",followView=false ); 
    LabelPt( S, color="green", "actions=[\"rotz\",45],followView=false",actions=["rotz",45], followView=false ); 
    LabelPt( T, color="blue", "actions=[\"rotz\",45, \"x\",0.5],followView=false",actions=["rotz", 45,"x",0.5], followView=false ); 
    LabelPt( U, color="purple", "actions=[\"x\",0.5, \"rotz\",45],followView=false",actions=["x",0.5, "rotz", 45], followView=false ); 
    LabelPt( V, color="teal", "actions=[\"rotz\",45, \"rotx\",90,\"y\",0.2], halign=\"center\",followView=false"
                ,halign="center"
                ,actions=["rotz", 45, "rotx",90, "y",0.2], followView=false ); 
    
    LabelPt( W, color="black", "opt=[\"actions\",[\"rotz\",45]], halign=\"center\",followView=false"
                ,halign="center"
                ,opt=["actions",["rotz", 45]], followView=false ); 
                
    LabelPt( A, color="black", //"opt=[\"actions\",[\"rotz\",45]], halign=\"center\",followView=false"
                ,halign="center"
                ,text=["actions",["rotz", 45]], followView=false ); 
    
}
//LabelPt_demo_actions();
