include <../scadx.scad>

module demo_holeCube()
{
  echom("demo_holeCube()");
  
  echo(_red("") );
  pts = cubePts(3);
  faces = rodfaces(4);
  edges = get_edges(faces);
  template = [[1,1,0],[-1,1,0],[-1,-1,0],[1,-1,0]];
  template = [[1,-0.5], [0,1],[-1,-1]];
  
  MarkPts(pts, Line=false);
  for(eg=edges) Line(get(pts,eg));
  MarkPts( addz(template,-0.2) ); 
  Plane( addz(template,-0.2), mode=1);
  
  for(fi=range(faces))
  {
     fpts = get(pts,faces[fi]);
     MarkPt( sum(fpts)/len(fpts), Ball=false
           , Label=["text",fi, "indent",0, "color","red"]  
           );
  }
  
  ptfis= holeCube(pts,faces, template=template);
  newpts = ptfis[0];
  newfaces = ptfis[1];
  
  echo(newpts=newpts); 
  echo(newfaces = newfaces );
  MarkPts(newpts);
  
  color("gold")
  polyhedron( newpts, newfaces );
  
  
  
  
  
  
  
  
  
  
//  echo(_red("Find the index of Pt cloest to ref") );
//  pts= randPts(5);
//  ref = randPt();
//  
//  MarkPts(pts); 
//  MarkPt(ref, Label="R", Ball=["color",["red", 0.25], "r", 0.25]);
//  
//  cpi = closestPi(pts, ref);
//  MarkPt( pts[cpi], Ball=["color",["blue", 0.25], "r", 0.25] );
}
demo_holeCube();