include <../scadx.scad>

module demo_cavePF_temp_is_num()
{
  echom("demo_cavePF_temp_is_num()");
  echo(_red("template=1 => a square of size 1 in the center"));

  template = 1;
  
  _red("offset=undef");
  pts=  [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
        , [0, 0, 1], [0, 3, 1], [3, 3, 1]];
  faces = rodfaces(4);
  ptfis= cavePF( pts,faces, template=template );
  Red() Poly( ptfis );
  echo(ptfis);
  
  _green("offset=undef");
  pts2= addx( [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
            , [0, 0, 1], [0, 3, 2], [3, 3, 2]]
            , 3.5) ;  
  ptfis2= cavePF( pts2,faces, template=template );
  Green() Poly( ptfis2 );
  
  
  _blue("offset=undef");
  pts3= addx( [[3, 0, .5], [0, 0, 0], [0, 3, 0], [3, 3, 0.5], [3, 0, 1]
            , [0, 0, 1], [0, 3, 2], [3, 3, 2]]
            , 7) ;  
  ptfis3= cavePF( pts3,faces, template=template );
  Blue() Poly( ptfis3 );
  
}
//demo_cavePF_temp_is_num();



module demo_cavePF_temp_is_num_anysite()
{
  echom("demo_cavePF_temp_is_num_anysite()");
  echo(_red("template=1 => a square of size 1 in the center"));

  template = 1;
  faces = rodfaces(4);
  
  _red("offset=undef");
  //pts=  [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
  //      , [0, 0, 1], [0, 3, 1], [3, 3, 1]];
  pqr=pqr();      
  //echo(pqr=pqr);
  MarkPts(pqr, "PQR");
  pts= cubePts(size=3,site=pqr );
  
  //echo(pts=pts);  
  DrawEdges( [pts, faces] );
  
  ptfis= cavePF( pts,faces, template=template );
  Red(0.5) Poly( ptfis );
 
}
//demo_cavePF_temp_is_num_anysite();



module demo_cavePF_temp_is_xy()
{
  echom("demo_cavePF_temp_is_xy)");
  echo(_red("template=[1,2] => a retangular of size 1x2"));

  template = [1,2];
  faces = rodfaces(4);
  
  _red("offset=undef");
  pts=  [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
        , [0, 0, 1], [0, 3, 1], [3, 3, 1]];
  ptfis= cavePF( pts,faces, template=template );
  Red() Poly(ptfis);
  
  //-----------------------------------
  // offset = 0.5
  _green("offset=0.5");
  pts2= addx(pts, 3.5);
  ptfis2= cavePF( pts2,faces, template=template, offset=0.5 );
  Green() Poly(ptfis2);
  
 
  _blue("offset=0.5");
  pts3= addx( [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
            , [0, 0, 1], [0, 3, 2], [3, 3, 2]]
            , 7) ;
  ptfis3= cavePF( pts3,faces, template=template, offset=0.5 );
  Blue(.8) Poly(ptfis3);
}
//demo_cavePF_temp_is_xy();




module demo_cavePF_temp_is_2D_pts()
{
  echom("demo_cavePF_temp_is_2D_pts()");
  echo(_red("template=[[2,1], [1,2],[.5,.5]]"));

  template = [[2,1], [1,2],[.5,.5]];
  faces = rodfaces(4);
  
  //echo(_red("") );
  pts=  [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
        , [0, 0, 1], [0, 3, 1], [3, 3, 1]];
  /* Show center */
  MarkCenter( get(pts, [4,5,6,7]) );
  MarkCenter( addz(template,-0.2) );
  Plane( addz(template,-0.2), mode=1);  
  
  _red("offset=undef, template is centered");
  ptfis= cavePF( pts,faces, template=template );
  Red() Poly(ptfis);
  
  //-----------------------------------
  // offset = 0.5
  _green("offset=[0,0] -- template as given");
  pts2= [for(p=pts)p+[3.5,0,0] ];
  ptfis2= cavePF( pts2,faces, template=template, offset=ORIGIN );
  Green() Poly(ptfis2);
  
  //-----------------------------------
  // offset = 0.5
  _blue("offset= [0.8,0] -- add offset on top of what it's defined");
  pts3= addx( [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
            , [0, 0, 1], [0, 3, 5], [3, 3, 5]]
            , 7) ;
  ptfis3= cavePF( pts3,faces, template=template, offset=[0.8,0] );
  Blue(0.8) Poly(ptfis3);
  
}
//demo_cavePF_temp_is_2D_pts();



//. Partial offset: offset=[a,undef] or [undef,b]

module demo_cavePF_partial_offset() //  offset=[a,undef] or [undef,b]
{
  echom("demo_cavePF_partial_offset()");
  echo(_red("Partial offset: offset=[x,undef] or [undef,y]"));

  template = 1;
  faces = rodfaces(4);  
  
  _red("offset=[0.2,undef] ===> centered y");
  pts=  [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
        , [0, 0, 1], [0, 3, 1], [3, 3, 1]];
  ptfis= cavePF( pts,faces, template=template, offset=[0.2,undef] );
  Red() Poly(ptfis);
  
  //-----------------------------------
  _green("offset=[undef,0.2] ===> centered x");
  pts2= addx(pts,3.5);  
  ptfis2= cavePF( pts2,faces, template=template, offset=[undef,0.2] );
  Green() Poly(ptfis2);
  
  //-----------------------------------
  _blue("offset=[undef,0.2] ===> centered x");
  pts3= addx( [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
            , [0, 0, 1], [0, 3, 5], [3, 3, 5]]
            , 7) ; 
  ptfis3= cavePF( pts3,faces, template=template, offset=[undef,0.2] );
  Green() Poly(ptfis3); 
}
//demo_cavePF_partial_offset();



//. On other faces (dest) 

module demo_cavePF_other_dest_temp_is_num()
{
  echom("demo_cavePF_other_dest_temp_is_num()");
  
  template = 1;
  
  faces = rodfaces(4);  
  
  pts = cubePts(3);
  _red("dest=[0,1,5]");
  
  for(eg=get_edges(faces)) Line(get(pts,eg));
  
  
  ptfis= cavePF( pts,faces, dest=[0,1,5], template=template, offset=0.5 );
  newpts = ptfis[0];
  newfaces = ptfis[1];
  Red() MarkPts(ptfis[0], Line=false);
  Pink() Poly(ptfis);
  //-----------------------------------

  _teal("dest=[4,5,6]", "teal");
  pts2= [for(p=pts)p+[3.5,0,0] ];
  ptfis2= cavePF( pts2,faces, dest=[4,5,6], template=template, offset=0.5);
  Teal() Poly(ptfis2);
  //-----------------------------------

  _blue("dest=[0,4,7]");
  pts3= [for(p=pts)p+[8,0,0] ];
  ptfis3= cavePF( pts3,faces, dest=[0,4,7], template=template, offset=0.5 );
  Blue() Poly(ptfis3);
  
}
//demo_cavePF_other_dest_temp_is_num();



module demo_cavePF_other_dest_is_fi()
{
  echom("demo_cavePF_other_dest_is_fi()");
  echo(_red("Using fi (face index) but not [i,j,k] for dest"));
  
  template = 1;
  faces = rodfaces(4);  
  Blue() MarkFaces(pts,faces);
  
  _red("dest= 1");
  pts = cubePts(3);
  ptfis= cavePF( pts,faces, dest=1, template=template, offset=0.5 );
  //newpts = ptfis[0];
  //newfaces = ptfis[1];
  Pink(0.6) Poly(ptfis) ;
  Red() MarkPts(ptfis[0], Line=false, Ball=false, opt=["Label",["scale",0.6]]);
  //-----------------------------------

  _teal("dest= 2");
  ptfis2= cavePF( addx(pts,4) ,faces, dest=2, template=template, offset=0.5);
  Teal() Poly(ptfis2);
  //-----------------------------------

  _blue("dest= 5");
  ptfis3= cavePF( addx(pts,8),faces, dest=5, template=template, offset=0.5 );
  Blue() Poly(ptfis3);
  
}
//demo_cavePF_other_dest_is_fi();



//. Allow depth (2018.5.8)

module demo_cavePF_depth_is_num()
{
  echom("demo_cavePF_depth_is_num()");
  
  template = 2;
  faces = rodfaces(4);  
  
  pts = cubePts(3);
  _red("depth = 0.5 ");
  ptfis= cavePF( pts,faces, dest=[4,5,6], template=template, depth=0.5);
  DrawEdges(ptfis);
  Pink(0.9) Poly(ptfis); 
  
  //-----------------------------------

  _teal("depth= -0.5 --- pop-up");
  pts2= addx(pts,4);
  ptfis2= cavePF( pts2,faces, dest=[0,1,5], template=template, depth=-0.5);
  DrawEdges(ptfis2);
  Teal() Poly(ptfis2);
  
  //-----------------------------------

  _blue("depth=5 --- out of size");
  pts3= addx(pts,8); 
  
  ptfis3= cavePF( pts3,faces, dest=[0,1,5], template=template, depth=5 );
  DrawEdges(ptfis3);
  Blue(0.8) Poly(ptfis3);
  
}
//demo_cavePF_depth_is_num();



module demo_cavePF_depth_is_ratio()
{
  echom("demo_cavePF_depth_is_ratio()");
  echo(_red("When depth is [d], treat d as a ratio"));
  
  template = 2;
  faces = rodfaces(4);  
  
  pts = cubePts(3);
  _red("depth = [0.5] ");
  ptfis= cavePF( pts,faces, dest=[4,5,6], template=template, depth=[0.5]);
  DrawEdges(ptfis);
  Pink(0.9) Poly(ptfis); 
  
  //-----------------------------------

  _teal("depth= [-0.5] --- pop-up");
  pts2= addx(pts,4);
  ptfis2= cavePF( pts2,faces, dest=[0,1,5], template=template, depth=[-0.5]);
  DrawEdges(ptfis2);
  Teal() Poly(ptfis2);
  
  //-----------------------------------

  _blue("depth=[5] --- out of size");
  pts3= addx(pts,8); 
  
  ptfis3= cavePF( pts3,faces, dest=[0,1,5], template=template, depth=[5] );
  DrawEdges(ptfis3);
  Blue(0.8) Poly(ptfis3);
  
}
//demo_cavePF_depth_is_ratio();



module demo_cavePF_depth_tenon()
{
  echom("demo_cavePF_depth_tenon()");
  echo(_red("tenon examples"));
  
  template = [0.5,2];
  faces = rodfaces(4);  
  
  pts = cubePts(2);
  _red("depth = [-0.5], offset undef ");
  ptfis= cavePF( pts,faces, dest=[4,5,6], template=template, depth=[-0.5]);
  Pink() Poly(ptfis);
  
  //-----------------------------------

  _teal("depth= [-0.5], offset=[0,0]");
  pts2= addx(pts,3);
  ptfis2= cavePF( pts2,faces, dest=[4,5,6], template=template, offset=[0,0], depth=[-0.5]);
  Teal() Poly( ptfis2 );
  
  //-----------------------------------

  echo(str(_blue("depth= [0.5], offset=[0,0]"), _red(" ===== Can't handle this !!!!")));
  pts3= addx(pts,6);
  ptfis3= cavePF( pts3,faces, dest=[4,5,6], template=template, offset=[0,0],depth=[0.5] );
  DrawEdges(ptfis3);
  Lightblue() Poly(ptfis3 );

  //-----------------------------------

  echo(str(_olive("depth= [0.5], offset=[0,0]"), _red(" ===== Can't handle this !!!!")));
  pts4= addx(pts,9);
  ptfis4= cavePF( pts4,faces, dest=[4,5,6], template=template, offset=[0.5,0],depth=[0.5] );
  DrawEdges(ptfis4);
  //for(eg=get_edges(ptfis4[1])) Line(get(ptfis4[0],eg),r=0.01,color="black");
  Olive(0.8) Poly(ptfis4);
  
  //-----------------------------------
  
  template2= [1.5, 2]; 
  
  echo(str(_purple("depth=[-0.5], offset=[0.5,0]"), _purple(" ===== Need Use cube w/ smaller z. Have extra and duplicate pts ")));
  pts5= addy(addx( [for(p=pts) [p[0],p[1],p[2]/2]] ,6),4);
  
  ptfis5= cavePF( pts5,faces, dest=[4,5,6], template=template2, offset=[0.5,0],depth=[-0.5] );
  DrawEdges(ptfis5);
  Purple() Poly(ptfis5 );
  MarkPts(ptfis5[0]);
  
  //-----------------------------------

  template3= [1, 2]; 

  echo(str(_blue("depth= [-0.5], offset=[1,0],[0,0]"), _blue(" ===== Need two cuts !!")));
  pts6= addy(addx( [for(p=pts) [p[0],p[1],p[2]/2]] ,9),4);
  ptfis6= cavePF( pts6,faces, dest=[4,5,6], template=template3, offset=[1,0],depth=[-0.5] );
  ptfis7= cavePF( ptfis6[0],ptfis6[1], dest=[4,5,6], template=template, offset=[0,0],depth=[-0.5] );
  
  DrawEdges(ptfis7);
  //for(eg=get_edges(ptfis4[1])) Line(get(ptfis4[0],eg),r=0.01,color="black");
  Blue() Poly(ptfis7);
  
}
//demo_cavePF_depth_tenon();



//. Multiple

module demo_cavePF_multiple_failed()
{
  echom("demo_cavePF_multiple_failed()");
  echo(_red("template=1 => a square of size 1 in the center"));

  
  _red("offset=undef");
  pts= cubePts([3,3,1]);
  template = 1;
  faces = rodfaces(4);
  ptfis= cavePF( pts,faces, template=template );
  Red() Poly( ptfis );
  
  _green("offset=undef");
  pts2= addx( pts, 3.5) ;  
  pf2= cavePF( pts2,faces, template=template, offset=1.8 );
  pf22= cavePF( pf2[0],pf2[1], dest=[4,5,6], template=template, offset=.5 );
  Green() Poly( pf22 );
  MarkPts( pf2[0] );
  
  
//  _blue("offset=undef");
//  pts3= addx( [[3, 0, .5], [0, 0, 0], [0, 3, 0], [3, 3, 0.5], [3, 0, 1]
//            , [0, 0, 1], [0, 3, 2], [3, 3, 2]]
//            , 7) ;  
//  ptfis3= cavePF( pts3,faces, template=template );
//  Blue() Poly( ptfis3 );
  
}
demo_cavePF_multiple_failed();


//. dev

module dev_cavePF_oversize()
{
  echom("dev_cavePF_oversize)");
  echo(_red("template=[1,2]"));

  template = [1,2];
  
  //echo(_red("") );
  echo(_green("offset=undef"));
  pts=  [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
        , [0, 0, 1], [0, 3, 1], [3, 3, 1]];
  //echo(pts=pts);
  faces = rodfaces(4);
  
  
  //MarkPts(pts, Line=false);
  //for(eg=get_edges(faces)) Line(get(pts,eg));
  //MarkPts( addz(template,-0.2) ); 
  //Plane( addz(template,-0.2), mode=1);
  
  //for(fi=range(faces))
  //{
     //fpts = get(pts,faces[fi]);
     //MarkPt( sum(fpts)/len(fpts), Ball=false
           //, Label=["text",fi, "indent",0, "color","red"]  
           //);
  //}
  
  ptfis= cavePF2( pts,faces, template=template, depth=0.5 );
  newpts = ptfis[0];
  newfaces = ptfis[1];
  MarkPts(newpts, Line=false);
  //echo(newpts=newpts); 
  //echo(newfaces = newfaces );
  for(eg=get_edges(newfaces)) Line(get(newpts,eg),r=0.01,color="black");
  MarkFaces( pts, faces, color="red" );
  color("lightgreen") polyhedron( newpts, newfaces );
  //Poly( ptfis, color="green");
  //MarkPts(newpts);
  
  
  //-----------------------------------
  
  
  // offset = 0.5
  echo(_blue("offset=0.5"));
  pts2= [for(p=pts)p+[3.5,0,0] ];
  
  ptfis2 = cavePF2( pts2, faces
                  //, template=[1,3], offset=[undef,0]  // cut thru, shaped like "1 1"
                  //, template=[1,2], offset=[undef,0]  // cut down, shaped like "n"
                  , template=[1,2], offset=[undef,1]    // cut up, shaped like "U"
                  , depth=0.5 
                  );
  echo( ptfis2_0 = ptfis2[0]);
  MarkPts(ptfis2[0], Line=false);
  for(eg=get_edges(ptfis2[1])) Line(get(ptfis2[0],eg),r=0.01,color="black");
  color("lightblue", 0.8) polyhedron( ptfis2[0], ptfis2[1] );
  
}
//dev_cavePF_oversize();

