include <../scadx_make3.scad>


//. == demo (duplication) ===============  

//================================================================
module Make_demo_1shape(loglevel=0)
{
  
  data=  
  [ 
    "title", "Make_demo_1shape__cube()"
  , "note", "<b>obj1</b> has 1 shape made of cube()."
  , "objs" 
  , [  
      "obj1"
      , [ 
          "units", [ "shape", "cube"
                   , "opt", ["size",[10,1,4]]
                   , "islogging", 0
                   ]
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
  data2=  
  [ 
    "title", "Make_demo_1shape__Cube()"
  , "note", "<b>obj2</b> has 1 shape made of Cube()."
  , "objs" 
  , [  
      "obj2"
      , [ 
          "units", [ "shape", "Cube"
                   , "opt", ["size",[10,1,4]]
                   , "islogging", 0
                   ]
        , "actions", ["y", 4] 
        , "color", "teal"         
        ] 
    ]   
  ]; 

  Make( data = data2,  loglevel=loglevel );  
  
}  
//Make_demo_1shape(loglevel=0);


//================================================================
//================================================================
module Make_demo_2shapes_by_shape_array(loglevel=10)
{
  
  data=  
  [ 
    "title", "Make_demo_2shapes_by_shape_array"
  , "note", "<b>obj1</b>'s <b>shapes</b> has 2 shapes(gold and green cubes)"
  , "objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "Cube"
                   , "opt", ["size",[10,1,4]]
                   //, "copies", [[]]
                   ]
                 , ["shape", "Cube"
                   , "opt", ["size",[10,1,4]]
                   , "actions", ["y", 4] 
                   , "color", "green"
                   ]
                 ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_2shapes_by_shape_array(loglevel=0);

//================================================================
module Make_demo_2shapes_by_shape_copies(loglevel=10)
{
  
  data=  
  [ 
    "title", "Make_demo_2shapes_by_shape_copies"
  , "note", "<b>obj1</b> has only one shape, but this shape has 2 copies(gold and brown)"
   
  , "objs" 
  , [  
      "obj1"
    , [ 
        "units", [ "shape", "cube"
                 , "opt", ["size",[10,1,4]]
                 , "copies", [ [] 
                             , [ "actions", ["y", 5] 
                               , "color", "brown"
                               ]
                             ]
                 ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_2shapes_by_shape_copies(loglevel=0);

//================================================================
module Make_demo_2shapes_by_obj_copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_2shapes_by_obj_copies"
  , "note", "<b>obj1</b> has only one shape, but this obj1 itself has 2 copies(gold and blue)"
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                   , "opt", ["size",[10,1,4]]
                   ]
                 ] 
      , "copies", [ [] 
                  , [ "actions", ["y", 5] 
                    , "color", "blue"]
                  ]           
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_2shapes_by_obj_copies(loglevel=0);

//================================================================
module Make_demo_8shapes(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_8shapes"
  , "note", ["Combine *shape array*,*shape copies* and *obj copies* approaches demoed above"
            ,"to generate 8 shapes"]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units",[ [ "shape", "cube"
                  , "opt", ["size",[5,1,2]]
                  , "copies", [ ["color", "red"]
                              , ["color", "orange"
                                ,"actions",["rotz",20, "y",1]
                                ] 
                              ]
                  ]
                , [ "shape", "sphere"
                  , "opt", ["r",1, "$fn", 36]
                  , "actions", ["x", 5,"y",1]
                  , "copies", [ ["color", "blue"]
                              , ["color", ["purple",0.7]
                                ,"actions",["y",2]
                                ] 
                              ]
                  ] 
                ] 
      , "copies", [ [] 
                  , [ "color", "teal"
                    , "actions", ["roty", -45, "y", 5] 
                    ]    
                  ]       
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_8shapes(loglevel=0);


//. == demo (cutoffs) ===============  


//================================================================
module Make_demo_shape_cutoff(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_cutoff"
  , "note", ["<b>obj1</b> has one shape and one cutoff. "
            ,"The property '<b>cutoffs</b>' could be <b>[\"shapes\"...]</b> or <b>[[\"shapes\"...]]</b>"
            ,"(just like the structure of obj.units)"
            ,"Note that the <b>shape's 'actions'</b>, [\"roty\",-15], are applied to the cutoff, too"
            ," So a cutoff follows both shape's and its own actions combined."
            ,"Also note that you can adjust '<b>visible</b>' properties of shape and cutoffs"
            ," as follows to decide what's visible:"
            ,_mono(" shape.visible | cutoffs.visible | shape | cutoffs" )
            ,_mono(replace("     1            1/0/undef        yes      no  "," ","&nbsp;") )
            ,_mono(replace("     0             0/undef         no       no  "," ","&nbsp;") )
            ,_mono(replace("     0               1             no       yes  "," ","&nbsp;") )
            ,"This is critical for debugging."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units", [ "shape", "cube"
                   //, "visible", 0
                   , "opt", ["size",[10,1,4]]
                   , "actions", ["roty", -15]
                   , "cutoffs", [ "units", [ "shape", "cube"
                  		           , "opt", ["size",[2,4,2]]
                  		           , "actions", ["t",[2,-0.1,1]]
                  		           ]
                  		//, "visible",1           
                                ]   
                   ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_cutoff(loglevel=0);

//================================================================
module Make_demo_shape_specific_cutoffs_with_shape_copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_specific_cutoffs_with_shape_copies"
  , "note", ["<b>obj1</b> has one shape and one cutoff, but the shape has multiple copies."
            ,"There can be 2 types of cutoffs: <b>shape-specific</b> and <b>cross-shapes</b>"
            ,"A <b>shape-specific cutoff</b> follows both shape's and its own actions."
            ,"So it follows its parent shape whereever it goes, and be duplicated "
            , "whenever its parent shape is duplicated."
            ,"A <b>cross-shapes cutoff</b> doesn't have a specific parent shape and is applied"
            ,"across a set of shapes."
            ,"This demo shows the <b>shape-specific cutoff</b> with shape duplication."
            , "('copies' is on the shape level)"
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ [ "shape", "cube"
                   , "opt", ["size",[5,1,4]]
                   , "actions", ["roty", -15]
                   , "visible",1
                   , "cutoffs", [ "units", [ "shape", "cube"
		        		   , "opt", ["size",[2,4,2]]
					   , "actions", ["t",[2,-0.1,1]]
					   ]
		                //,"visible",1
				] 
                    
                   , "copies", [ [ ] 
                               , [ "actions", ["rotx", -90], "color", "green" ]
                               , [ "actions", ["mirror", [[0,0,1],[0,0,0]]]
                                 , "color", "blue" 
                                 ]
                               ]  
                   ]
                 ]
          
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_specific_cutoffs_with_shape_copies(loglevel=0);

//================================================================
module Make_demo_shape_specific_cutoffs_with_obj_copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_specific_cutoffs_with_obj_copies"
  , "note", ["This demo shows the <b>shape-specific cutoff</b> with obj duplication."
            , "('copies' is on the obj level)"
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                    , "opt", ["size",[5,1,4]]
                    , "actions", ["roty", -15]
                    , "visible",1
                    , "cutoffs", [ "units", [ "shape", "cube"
					    , "opt", ["size",[2,4,2]]
					    , "actions", ["t",[2,-0.1,1]]
					    ]
		                 ,"visible",1
				 ] 
                    ]
                 ]
                    
      , "copies", [ [ ] 
                  , [ "actions", ["rotx", -90], "color", "green" ]
                  , [ "actions", ["mirror", [[0,0,1],[0,0,0]]], "color", "blue" ]
                  ]            
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_specific_cutoffs_with_obj_copies(loglevel=0);

//================================================================
module Make_demo_cross_shape_cutoffs(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_cross_shape_cutoffs"
  , "note", ["A <b>cross-shapes cutoff</b>:"
            , " -- Its parent is a set of shapes;"
            , " -- doesn't follow the 'actions' of a specific shape; "
            , " -- will cut through all copies of the shapes."
            , ""
            , "Set shape 'visible' to 0 to see the cutoff"
            , "Note the '<b>copies</b>' is done on the <i>shape</i> level."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                    , "opt", ["size",[5,1,4]]
                    , "actions", ["roty", -15]
                    , "visible",1
                    , "copies", [ [ ] 
                                , [ "actions", ["rotx", -90], "color", "green" ]
                                , [ "actions", ["mirror", [0,0,1]], "color", "blue" ]
                                ] 
                    ]
                 ]
                    
      , "cutoffs", [ "units", [ "shape", "cube"
			      , "opt", ["size",[2,10,2]]
			      , "actions", ["t", [1,-2,-0.8]]
			      ]
		    , "visible",1	      
		   ]            
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_cross_shape_cutoffs(loglevel=0);

//================================================================
module Make_demo_cross_subshapes_cutoffs(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_cross_subshapes_cutoffs"
  , "note", ["A <b>cross-shapes cutoff</b> across multiple different shapes"
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                    , "opt", ["size",[5,1,4]]
                    , "actions", ["roty", -15]
                    , "visible",1
                    ]
                  , ["shape", "sphere"
                    , "opt", ["r", 2, "$fn", 36]
                    , "actions", ["x",1,"y",1, "z", -1]
                    , "color", "blue"                    
                    ]  
                 ]
      , "visible", 1              
      , "cutoffs", [ "units", [ "shape", "cube"
			      , "opt", ["size",[2,10,2]]
			      , "actions", ["t", [1,-2,-0.8]]
			      ]
		    , "visible",1	      
		   ]            
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_cross_subshapes_cutoffs(loglevel=0);

//================================================================
module Make_demo_cross_shape_cutoffs2(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_cross_shape_cutoffs2"
  , "note", ["Another case of <b>cross-shapes cutoff</b>:"
            , "Both <b>copies</b> and <b>cutoffs</b> are done on the <i>obj</i> level."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                    , "opt", ["size",[5,1,4]]
                    , "actions", ["roty", -15]
                    , "visible",1
                    
                    ]
                 ]
      
      , "copies", [ [ ] 
                  , [ "actions", ["rotx", -90], "color", "green" ]
                  , [ "actions", ["mirror", [0,0,1]], "color", "blue" ]
                  ]        
                         
      , "cutoffs", [ "units", [ "shape", "cube"
			      , "opt", ["size",[2,10,2]]
			      , "actions", ["t", [1,-2,-0.8]]
			      ]
		    , "visible",1	      
		   ]            
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_cross_shape_cutoffs2(loglevel=0);



//================================================================
module Make_demo_shape_2cutoffs(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_2cutoffs"
  , "note", ["<b>obj1</b> has one shape and 2 cutoffs. "
            ,"Note that the shape's actions are applied to the cutoff."]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ "shape", "cube"
                  , "opt", ["size",[7,1,4]]
                  , "actions", ["roty", -45]
                  , "visible", 1
                  , "cutoffs", [ ["units", [ "shape", "cube"
					    , "opt", ["size",[2,4,2]]
					    , "actions", ["t",[1,-0.1,1]]
                                            ]
                                 ]
                               , ["units", [ "shape", "sphere"
					    , "opt", ["r",1.2, "$fn", 48]
					    , "actions", ["t",[5,0,2]]
                                            ]
                                 ]
                               ]
                 ]
      , "copies", [ []
                  , ["actions",["mirror", [-1,0,1] ]
                    , "color", "blue"]
                  ]              
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_2cutoffs(loglevel=0);

//================================================================
module Make_demo_shape_2cutoffs_2copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_2cutoffs_2copies"
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units",["shape", "cube"
                    , "opt", ["size",[10,1,2]]
                    , "copies", [ ["visible",1 ] 
                                , [ "actions", ["y", 2] 
                                  , "color", "brown"
                                  , "visible",1
                                  ]
                                ]
                    , "cutoffs", [[ "units", [ "shape", "cube"
                  		              , "opt", ["size",[2,2.5,2]]
                  		              , "actions", ["t",[2,-0.1,1]]
                                              ]
                                  ]
                                 ,[ "units", [ "shape", "sphere"
                  		              , "opt", ["r",1.5, "$fn", 48]
                  		              , "actions", ["t",[6,0.8,2]]
                  		              , "islogging", 0
                                              ]
                                  ]
                                 ]
                   ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_2cutoffs_2copies(loglevel=0);


//================================================================
module Make_demo_2shapes_2cutoffs_2copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_2shapes_2cutoffs_2copies"
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units", [ [ "shape", "cube"
                      , "opt", ["size",[5,1,2]]
                      , "copies", [ ["visible",1] 
                                  , [ "actions", ["y", 1.5] 
                                    , "color", "brown"
                                    , "visible",1
                                    ]
                                  ]
                      , "cutoffs", [[ "units", [ "shape", "cube"
                    		              , "opt", ["size",[2,2.5,2]]
                    		              , "actions", ["t",[2,-0.1,1]]
                                                ]
                                    //, "visible",1            
                                    ]
                                   ]
                     ] 
                     , ["shape", "cube"
                        , "opt", ["size",[5,1,2]]
                        , "actions", ["x", 6]
                        , "copies", [ ["visible",1 
                                      , "color", "darkkhaki"
                                      ] 
                                    , [ "actions", ["y", 1.5] 
                                      , "color", "blue"
                                      , "visible",1
                                      ]
                                    ]
                        , "cutoffs", [[ "units", [ "shape", "sphere"
                      		              , "opt", ["r",1, "$fn", 48]
                      		              , "actions", ["t",[2,0,2]]
                                                  ]
                                      , "visible",1            
                                      ]
                                     ]
                       ]
                 ]   
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_2shapes_2cutoffs_2copies(loglevel=0);

//. == demo (parts) ===============  

//================================================================
module Make_demo_part(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_part"
  , "note", "<b>obj1</b> has 1 part."
  , "partlib", [ 
                 "p1", ["units",[ "shape", "cube"
                                 , "opt", ["size",[10,1,4]]
                                 //, "islogging", 1
                                 ]
                      , "actions", ["y", 4] 
                      , "color", "green"         
                      ] 
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units",[ ["part","p1"] ]         
        ] 
    ]    
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_part(loglevel=0);

//================================================================
module Make_demo_1part_of_2shapes(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_1part_of_2shapes"
  , "note", "<b>obj1</b> has 1 part w/ 2 shapes."
  , "partlib", [ 
                 "p1", ["units",[[ "shape", "cylinder"
                                   , "opt", ["r",1, "h",3, "$fn", 24]
                                   ]
                                 , [ "shape", "cylinder"
                                   , "opt", ["r",2, "$fn",6]
                                   ]
                                 ]
                      , "actions", ["y", 4] 
                      , "color", "green"         
                      ] 
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units",["part","p1"]          
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_1part_of_2shapes(loglevel=0);

//================================================================
module Make_demo_1part_of_2subshapes_w_cutoff(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_1part_of_2subshapes_w_cutoff"
  , "note", "<b>obj1</b> has 1 part, 2 shapes and 1 cross-shape cutoff."
  , "partlib", [ 
                 "p1", ["units",[[ "shape", "cylinder"
                                   , "opt", ["r",1, "h",2, "$fn", 24]
                                   , "visible", 1
                                   ]
                                 , [ "shape", "cylinder"
                                   , "opt", ["r",2, "$fn",6]
                                   , "visible", 1
                                   ]
                                 ]
                      , "actions", ["y", 3] 
                      , "color", "green" 
                      //, "visible", 0
                      , "cutoffs", ["units",["shape", "cylinder"
                                             ,"opt",["r",0.8, "h", 7, "$fn",12]
                                             , "visible",1
                                             , "actions", ["z",-1]
                                             ]
                                   ]        
                      ] 
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units", ["part","p1" ]         
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_1part_of_2subshapes_w_cutoff(loglevel=0);


//================================================================
module Make_demo_1part_of_2subshapes_w_cutoff_x2(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_1part_of_2subshapes_w_cutoff_x2"
  , "note", "<b>obj1</b> has 2 copies of 1 part that has 2 shapes and 1cutoff."
  , "partlib", [ 
                 "p1", ["units",[
                 		[ "shape", "cylinder"
                                   , "opt", ["r",1, "h",2, "$fn", 24]
                                   , "visible", 1
                                   ]
                                 , 
                                 [ "shape", "cylinder"
                                   , "opt", ["r",2, "$fn",6]
                                   , "visible", 1
                                   ]
                                 ]
                      , "actions", ["y", 3] 
                      , "color", "green" 
                      //, "visible", 0
                      , "cutoffs", ["units",["shape", "cube"
                                             ,"opt",["size",[1,1,5]]
                                             , "visible",1
                                             , "actions", ["t",[-0.5,-0.5,-1]]
                                             ]
                                   ]        
                      ] 
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units", ["part","p1" ] 
        , "copies", [ [] 
                    , ["actions",["mirror", [0,1,0]], "color","blue"]
                    ]          
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_1part_of_2subshapes_w_cutoff_x2(loglevel=0);


//================================================================
module Make_demo_2parts(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_2parts"
  , "note", "<b>obj1</b> has 2 parts."
  , "partlib", [ 
                 "p1", ["units",[[ "shape", "cylinder"
                                   , "opt", ["r",1, "h",3, "$fn", 24]
                                   ]
                                 ]
                      , "color", "green"         
                      ] 
                      
              , "p2", ["units",[[ "shape", "cylinder"
                                 , "opt", ["r",2, "$fn",6]
                                 ]
                                 ]
                      , "color", "blue"         
                      ]         
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units",[ ["part","p1"], ["part","p2"] ]
        , "actions", ["y", 4]    
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_2parts(loglevel=0);

//================================================================
module Make_demo_comboParts(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_comboParts"
  , "note", "<b>obj1</b> has a part that is a comboPart containing 2 parts."
  , "partlib", [ 
                 "p1", ["units",[[ "shape", "cylinder"
                                   , "opt", ["r",1, "h",3, "$fn", 24]
                                   ]
                                 ]
                      , "color", "green"         
                      ] 
                      
              , "p2", ["units",[[ "shape", "cylinder"
                                 , "opt", ["r",2, "$fn",6]
                                 ]
                                 ]
                      , "color", "blue"         
                      ]    
              , "p12", ["units", [ ["part","p1"]
                                 , ["part","p2"]
                                 ]
                       ]             
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units",[ "part","p12"]
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_comboParts(loglevel=0);


//. == demo (practical) ======
//----------------------------------------------------

module Make_demo_woodworking_mortise_tenon (loglevel=10)
{
  
  
  data=  
  [ 
   "title", "Make_demo_woodworking_mortise_tenon"
  ,"note", [ "A part, <b>tongue</b>, is used as a shape in <b>tenon</b>(green) "
           , "and a cutoff in <b>mortise</b> (blue)."
           ]
  
  , "partlib", [ "tongue"
                , [
                    "units", ["shape","cube"
                              , "opt", ["size",[2,3,1]]
                              ]
                  ]
               , "board"
                , [
                    "units", ["shape","cube"
                            ,"color",["gold",0.7]
                            , "opt", ["size",[4,4,2]]
                            ]
                  ]
               , "tenon"
               , [
                   "units", [["part","board", "actions", ["x", 8] 
                             ]
                            ,["part","tongue","actions",["transl",[6,0.5,.5]]
                             ]
                            ]
                 , "color", "green"            
                 ]
               , "mortise"
               , [
                   "units", ["part", "board","actions",["transl",[0,0,0]]]
                 , "color", "blue"
                 , "cutoffs", ["units", ["part","tongue"
                                         , "actions", ["transl",[2.01,.5,0.5]]
                                        ]
                              ]          
                 ]   
               ]  
     
   
   ,"objs" 
  , [  
      "long-side"
      , [ "units", [["part","mortise"], ["part","tenon"]]
        ]
    ]   
  
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_woodworking_mortise_tenon(loglevel=0);


//----------------------------------------------------

module Make_demo_woodworking_box_joint (loglevel=10)
{
  
  w=1;
  
  data=  
  [ 
   "title", "Make_demo_woodworking_box_joint"
  
  , "partlib", [ "board"
                , [
                    "units", [ "shape","cube"
                    	      , "opt", ["size",[10,w,6]]
                    	      ]
                  ]
               , "board1"
                , [
                    "units",["part","board"]
                    ,"actions", ["rot",[0,0,90],"transl",[w,0,0]]
                    ,"cutoffs"
                    , [ ["units",["part","tooth","actions",["transl",[-0.01,-0.005,-0.015]] ]]
                      , ["units",["part","tooth","actions",["transl",[-0.01,-0.005,2*w]] ]]
                      , ["units",["part","tooth","actions",["transl",[-0.01,-0.005,4*w]] ]]
                      ]
                    
                  ]
               , "tooth"
               , [
                    "units", ["shape","cube"
                              , "opt", ["size",[w+0.02,w+0.01,w]]
                              ,"color",["teal",1]
                              ]
                ]                  
               , "board2"
                , [
                    "units",[ ["part","board"]
                            , ["part","tooth","actions",["x",-w] ]
                            , ["part","tooth","actions",["transl",[-w,0,2*w]] ]
                            , ["part","tooth","actions",["transl",[-w,0,4*w]] ]
                            ]
                    ,"color",["darkkhaki",1]
                    ,"actions", ["transl",[w*2.5,0,0]]
                  ]
               ]  
     
   
   ,"objs" 
  , [  
      "box"
      , [ "units", [["part","board1"],["part","board2"]]
        ]
    ]   
  
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_woodworking_box_joint(loglevel=0);

//----------------------------------------------------

module Make_demo_woodworking_box_joint_2 (loglevel=10)
{
  
  w=1;
  
  data=  
  [ 
   "title", "Make_demo_woodworking_box_joint_2"
  
  , "partlib", [ "board"
                , [
                    "units", [ "shape","cube"
                    	      , "opt", ["size",[10,w,6]]
                    	      ]
                  ]
               , "board1"
                , [
                    "units",["part","board"]
                    ,"actions", ["rot",[0,0,90],"transl",[w,0,0]]
                    ,"cutoffs", [ "units",["part","teeth"], "actions",["t",[-0.005,-0.005,-0.005]]]
                    
                  ]
               , "teeth"
               , [ "units", ["shape","cube"
                              , "opt", ["size",[w+0.02,w+0.01,w]]
                              ,"color",["teal",1]
                              ]
                 , "copies", [ [] 
                             , ["actions", ["z", 2*w]] 
                             , ["actions", ["z", 4*w]] 
                             ]
               
                 ]   
                              
               , "board2", [
                    "units",[ ["part","board"]
                            , ["part","teeth", "actions", ["x",-w]]
                            ]
                    ,"color",["darkkhaki",1]
                    ,"actions", ["transl",[w*2.5,0,0]]
                  ]
               ]  
     
   
   ,"objs" 
  , [  
      "box"
      , [ "units", [ ["part","board1"]
                   , ["part","board2"]]
        ]
    ]   
  
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  

//Make_demo_woodworking_box_joint_2(loglevel=0);



//. == demo (poly) ======
//----------------------------------------------------

//================================================================
module Make_demo_poly(loglevel=0)
{
  
  data=  
  [ 
    "title", "Make_demo_poly"
  , "note", "Draw a simple poly obj without giving pts (set size to give a cubic poly)"
  , "objs" 
  , [  
      "obj1"
      , [ 
          "units", [ "shape", "poly"  // <========== set this to "poly"
                   , "opt", ["size",[10,1,4]]
                   , "islogging", 0
                   ]
        , "actions", ["y", 4] 
        , "color", "green"         
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_poly(loglevel=0);

//================================================================
module Make_demo_poly_given_pts(loglevel=0)
{
  _pts = [[0,-7,0],[4,2,0],[19,2,0],[8,6,0],[-3,8,0],[-10,1,0]];
  pts = concat( _pts, [ for(p=_pts) p+[0,0,3] ]);
  data=  
  [ 
    "title", "Make_demo_poly_given_pts"
  , "note", "Draw a simple poly obj with giving pts"
  , "objs" 
  , [  
      "obj1"
      , [ 
          "units", [ "shape", "poly"  // <========== set this to "poly"
                   , "opt", [ "pts", pts
                            , "faces", rodfaces(6)
                            ]
                   , "islogging", 0
                   ]
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_poly_given_pts(loglevel=0);

//================================================================
module Make_demo_poly_2shapes_by_shape_array(loglevel=10)
{
  
  data=  
  [ 
    "title", "Make_demo_poly_2shapes_by_shape_array"
  , "note", "<b>obj1</b>'s <b>shapes</b> has 2 shapes(gold and green cubes)"
  , "objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "poly"
                   , "opt", ["size",[10,1,4]]
                   ]
                 , ["shape", "poly"
                   , "opt", ["size",[10,1,4]]
                   , "actions", ["y", 4] 
                   , "color", "green"
                   ]
                 ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_poly_2shapes_by_shape_array(loglevel=10);


//================================================================
module Make_demo_poly_8shapes_with(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_poly_8shapes_with"
  , "note", ["Combine *shape array*,*shape copies* and *obj copies* approaches demoed above"
            ,"to generate 8 shapes"]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units",[ [ "shape", "poly"
                  , "opt", ["size",[5,1,2]]
                  , "copies", [ ["color", "red"]
                              , ["actions",["rotz",20, "y",1]
                                , "color", "orange"
                                ] 
                              ]
                  ]
                , [ "shape", "sphere"
                  , "opt", ["r",1, "$fn", 36]
                  , "actions", ["x", 5,"y",1]
                  , "copies", [ ["color", "green"]
                              , ["actions",["y",2]
                                , "color", ["green",0.7]
                                ] 
                              ]
                  ] 
                ] 
      , "copies", [ [] 
                  , [ "actions", ["roty", -45, "y", 5] 
                    //, "color", "brown"
                    ]
                  ]           
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_poly_8shapes_with(loglevel=0);

//. == demo (poly cutoffs) ======

//================================================================
module Make_demo_poly_shape_cutoff(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_poly_shape_cutoff"
  , "note", ["<b>obj1</b> has one poly shape and one solid cutoff.  "
            , "Note that this produces weird result (OpenSCAD fails)."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units", [ "shape", "poly"
                   , "opt", ["size",[10,1,4]]
                   , "actions", ["roty", -15]
                   , "cutoffs", [ "units", [ "shape", "cube"
                  		           , "opt", ["size",[2,4,2]]
                  		           , "actions", ["t",[2,-0.1,1]]
                  		           ]
                                ]   
                   ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_poly_shape_cutoff(loglevel=0);

//================================================================
module Make_demo_shape_poly_cutoff(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_poly_cutoff"
  , "note", ["<b>obj1</b> has one non-poly shape and one poly cutoff.  "
            , "Note that this produces weird result (OpenSCAD fails)."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units", [ "shape", "cube"
                   , "opt", ["size",[10,1,4]]
                   , "actions", ["roty", -15]
                   , "cutoffs", [ "units", [ "shape", "poly"
                  		           , "opt", ["size",[2,4,2]]
                  		           , "actions", ["t",[2,-0.1,1]]
                  		           ]
                                ]   
                   ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_poly_cutoff(loglevel=0);

//================================================================
module Make_demo_poly_shape_poly_cutoff(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_poly_shape_poly_cutoff"
  , "note", ["<b>obj1</b> has one poly shape and one poly cutoff.  "
            , "Note that this produces weird result (OpenSCAD fails)."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units", [ "shape", "poly" 
                   , "opt", ["size",[10,1,4]]
                   , "actions", ["roty", -15]
                   , "cutoffs", [ "units", [ "shape", "poly"
                  		           , "opt", ["size",[2,4,2]]
                  		           , "actions", ["t",[2,-0.1,1]]
                  		           ]
                                ]   
                   ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_poly_shape_poly_cutoff(loglevel=0);


//================================================================
//. == debug ==

module Make_debug_issue_3(loglevel=10)
{
  //https://bitbucket.org/runsun/scadx/issues/3/make-units-not-start-with-shape-is-ignored
  
  data=  
  [ 
    "title", "Make_debug_issue_3"
  , "note", "<b>obj1</b>'s <b>shapes</b> has 2 shapes(gold and green cubes)"
  , "objs" 
  , [  
      "obj1"
    , [ 
        "units",  [ "name", "shape1",
                    
                   "shape", "cube"
                   , "opt", ["size",[10,1,4]]
                   ]
                  
      ] 
    ]   
  ]; 

  data2=  
  [ 
    "title", "Make_demo_2shapes_by_shape_array"
  , "note", "<b>obj1</b>'s <b>shapes</b> has 2 shapes(gold and green cubes)"
  , "objs" 
  , [  
      "obj1"
    , [ 
        "units", [ [ "name", "shape1"
                   , "shape", "cube"
                   , "opt", ["size",[10,1,4]]
                   ]
                 , [ "name", "shape2"
                   , "shape", "cube"
                   , "opt", ["size",[10,1,4]]
                   , "actions", ["y", 4] 
                   , "color", "green"
                   ]
                 ] 
      ] 
    ]   
  ]; 
  

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_debug_issue_3(loglevel=10);