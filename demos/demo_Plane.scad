include <../scadx.scad>

//Plane_demo();
module Plane_demo()
{
    echom("Plane_demo");
    echo(_red("default: mode = 3"));
    pts= randPts(5);
    echo( pts = pts);
    pqr= get(pts,[0,1,2]);
    echo(pqr=pqr);
    MarkPts( pqr, [0,1,2]);
    Plane( pqr );
    
    //pts1= transN(pts, 3);
    //MarkPts( pts1, opt=["Label",["color","red"]]);
    //Plane( pts1, color="red" );
    //
    //pts2= transN(pts, 6); //randPts(5);
    //MarkPts( pts2, color="green");
    //Plane( pts2, color="red", closed=false, solid_color=true );
}

Plane_demo_modes_12();
module Plane_demo_modes_12()
{
    echom("Plane_demo_modes_12");
    
    module MLine(pts){
       MarkPts(pts, Ball=false, Label=false, Line=["color","darkblue"]); }

    cpts= [ [0.03, 0.71, -1.59], [-0.92, -0.27, -2.28], [-1.02, -1.79, -2.36] 
          , [-0.22, -2.97, -1.8], [1.02, -3.1, -0.91], [1.97, -2.12, -0.22]
          , [2.07, -0.6, -0.13], [1.27, 0.58, -0.7]];
    
    MarkPt(cpts[0], "mode=1 (all pts as a whole)");
    MLine(cpts);
    Plane(cpts, mode=1);
    
    cpts1= transN(cpts,3);
    MarkPt(cpts1[0], color="red", "mode=2 (parallelogram)");
    //MarkPts(cpts1, Label=["text",range(cpts1), "color","black"]);
    MLine(cpts1);
    Plane(cpts1, mode=2, color="red", h=0.1);
}

module Plane_demo_modes_34()
{
    echom("Plane_demo_modes_34");
    
    module MLine(pts){
       MarkPts(pts, Ball=false, Label=false, Line=["color","darkblue"]); }

    cpts3= [ [0.03, 0.71, -1.59], [-0.92, -0.27, -2.28], [-1.02, -1.79, -2.36] 
          , [-0.22, -2.97, -1.8], [1.02, -3.1, -0.91], [1.97, -2.12, -0.22]
          , [2.07, -0.6, -0.13], [1.27, 0.58, -0.7]];
    
    MarkPt(cpts3[0], color="green", "mode=3 (triangle; default)");
    MLine(cpts3);
    Plane(cpts3, color="green");
    
    cpts4= transN(cpts3,3);
    MarkPt(cpts4[0], color="blue", "mode=4 (square)");
    MLine(cpts4);
    Plane(cpts4, mode=4, color="blue");
}
//Plane_demo_modes_34();


module Plane_demo_mode_12_actions()
{
    echom("Plane_demo_mode_12_actions");
    
    module MLine(pts){
       MarkPts(pts, Ball=false,Label=false, Line=["color","darkblue"]); }

    cpts=  [[-2.89, 0.69, 2.47], [-3.84, -0.29, 1.78], [-3.94, -1.81, 1.7]
           , [-3.14, -2.99, 2.26], [-1.9, -3.12, 3.15], [-0.95, -2.14, 3.84]
           , [-0.85, -0.62, 3.93], [-1.65, 0.56, 3.36]];
    //------------------------------------------------------------
    MLine(cpts);
    Plane(cpts, mode=1);
    
    cpts1= transN(cpts,1);
    MarkPt(cpts1[0], color="red", "actions=[\'rotx\',15]");
    MLine(cpts1);
    Plane(cpts1, mode=1, actions=["rotx",15], color="red");
    //------------------------------------------------------------
    cpts2= transN(cpts,4);
    MarkPt(cpts2[0], color="green", "mode=2 (parallelogrm)");
    MLine(cpts2);
    Plane(cpts2, mode=2, color="green");

    cpts3= transN(cpts,5);
    MarkPt(cpts3[0], color="green", "actions=[\'rotx\',90]");
    MLine(cpts3);
    Plane(cpts3, mode=2, actions=["rotx",90], color="green");
    //------------------------------------------------------------
}
//Plane_demo_mode_12_actions();


//Plane_demo_mode_actions();
module Plane_demo_mode_actions()
{
    echom("Plane_demo_mode_actions");
    
    module MLine(pts){
       MarkPts(pts, Ball=false,Label=false, Line=["color","darkblue"]); }

    cpts=  [[-2.89, 0.69, 2.47], [-3.84, -0.29, 1.78], [-3.94, -1.81, 1.7]
           , [-3.14, -2.99, 2.26], [-1.9, -3.12, 3.15], [-0.95, -2.14, 3.84]//];
           , [-0.85, -0.62, 3.93]];
           //, [-1.65, 0.56, 3.36]];
    //------------------------------------------------------------
    cpts4= transN(cpts,-1);
    MarkPt(cpts4[0], color="blue", "mode=3 (triangle, default)");
    MLine(cpts4);
    Plane(cpts4, color="blue");

    cpts5= [for(p=cpts4) p+ (cpts4[1]-cpts4[0])*3]; 
    cpts5= transN2(cpts,4.5);
    MarkPt(cpts5[0], color="blue", "actions=[\'rotx\',180]");
    MLine(cpts5);
    Plane(cpts5,  actions=["rotx",180], color="blue");
    //------------------------------------------------------------
    
    cpts6= [for(p=cpts4) p+ (cpts4[1]-cpts4[0])*3]; //transN(cpts,1);
    MarkPt(cpts6[0], color="purple", "mode=4 (square)");
    MLine(cpts6);
    Plane(cpts6, mode=4, color="purple", closed=0);

    cpts7= transN2(cpts6,4.5);
    MarkPt(cpts7[0], color="purple", "actions=[\'rotx\',90]");
    MLine(cpts7);
    Plane(cpts7, mode=4, actions=["rotx",90], color="purple", closed=0);

    //------------------------------------------------------------

//    cpts8= transN(cpts,8);
//    MarkPt(cpts8[0], color="teal", "actions=[\'rotx\',90], h=0.5");
//    MLine(cpts8);
//    Plane(cpts8, mode=4, h=0.5, actions=["rotx",90], color=["teal",1]);
//
//    cpts9= transN(cpts,10);
//    MarkPt(cpts9[0], color="teal", "actions=[\'rotx\',90,\"y\",0.25], h=0.5");
//    MLine(cpts9);
//    Plane(cpts9, mode=4, h=0.5, actions=[ "rotx",90,"y",0.25], color="teal");
//    //------------------------------------------------------------
    
}


