include <../scadx.scad>

//echo($fn=$fn);
//. W/o site

module tubePts_demo_default()
{
   echo(_b("tubePts_demo_default()"));  
   pts = tubePts();
   //echo(pts=pts);
   //Frame(pts);
   MarkPts(pts, Label=false); 
   //fsaces=
   Gold(.5) polyhedron(pts, faces=faces(shape="tube", nside=12));
   //Cylinder();
   //color(undef, 0.5) cylinder($fn=8);
  
}
tubePts_demo_default();

module tubePts_demo_fn()
{
   echo(_b("tubePts_demo_fn()"));  
   
   pts = tubePts(opt=["$fn", 48]);
   //Frame(pts);
   Gold(.8) polyhedron(pts, faces=faces(shape="tube", nside=48));
   
   //color(undef, 0.7) cylinder($fn=48);
   //MarkPts(pts); 
   
//   pts2 = tubePts(3, center=true);
//   Frame(pts2, ["color", "green"]);
//   MarkPts(pts2,"l");
}
//tubePts_demo_fn();


module tubePts_demo_center()
{
   echo(_b("tubePts_demo_center()"));  
   
   pts = tubePts(center=true, opt=["$fn", 8]);
   //Frame(pts);
   Gold(.8) polyhedron(pts, faces=faces(shape="tube", nside=8));
   //color(undef, 0.5) cylinder(center=true, $fn=8);   //MarkPts(pts2,"l");
}
//tubePts_demo_center();



module tubePts_demo_center_partial()
{
   echo(_b("tubePts_demo_center_partial()"));  
   
   pts = tubePts(center=[1,0,0], opt=["$fn", 8]);
   //Frame(pts);
   //MarkPts(pts);
   Gold(.8) polyhedron(pts, faces=faces(shape="tube", nside=8));
   //color(undef, 0.5) cylinder(center=true, $fn=8);   //MarkPts(pts2,"l");
}
//tubePts_demo_center_partial();



module tubePts_demo_r()
{
   echo(_b("tubePts_demo_r()"));  
   pts = tubePts(r2=0.6, opt=["$fn", 8]);
   
   f8=faces(shape="tube", nside=8);    
   Red() DrawEdges([pts,f8], r=0.005);
   Gold() polyhedron(pts, faces=f8);
   
}
//tubePts_demo_r();


module tubePts_demo_actions()
{
   echo(_b("tubePts_demo_actions()"));  
      
   pts = tubePts(opt=["$fn", 8, "actions", [ "roty", -90, "y",2]]);
   f8=faces(shape="tube", nside=8);    
   Red() DrawEdges([pts,f8], r=0.005);
   Gold() polyhedron(pts, faces=f8);
   
   pts2 = tubePts(h=2,r1=1, r2=2, th=0.5, opt=["$fn", 8, "actions", ["z", -2]]);
   Green() DrawEdges([pts2,f8], r=0.005);
   Gold() polyhedron(pts2, faces=f8);
   
}
//tubePts_demo_actions();

//====================================================
//. With site

module tubePts_demo_with_site()
{
   echo(_b("tubePts_demo_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"PQR");
   
   pts = tubePts(  opt=["site",pqr]);
   fs=faces(shape="tube", nside=12);    
   Red() DrawEdges([pts,fs], r=0.005);
   Gold() polyhedron(pts, faces=fs);   
}
//tubePts_demo_with_site();




module tubePts_demo_center_with_site()
{
   echo(_b("tubePts_demo_center_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"PQR");  
   
   pts = tubePts(center=true, opt=["site",pqr]);
   fs=faces(shape="tube", nside=12);    
   Red() DrawEdges([pts,fs], r=0.005);
   Gold() polyhedron(pts, faces=fs);  
}   
//tubePts_demo_center_with_site();



module tubePts_demo_actions_with_site()
{
   echo(_b("tubePts_demo_actions_with_site()"));  
   fs=faces(shape="tube", nside=12);    
   
   pqr=pqr();
   MarkPts(pqr,"PQR");
   
   pts = tubePts(opt=["actions", ["x",2],"site",pqr]);
   Red() DrawEdges([pts,fs], r=0.005);
   Red(.5) polyhedron(pts, faces=fs);     
   
   pts2 = tubePts(opt=["actions",["roty",-45],"site",pqr]);
   Red() DrawEdges([pts,fs], r=0.005);
   Green(.5) polyhedron(pts2, faces=fs);     
   
   pts3 = tubePts(opt=["actions",["rotz",90, "rotx",-90, "z",-2],"site",pqr]);
   Red() DrawEdges([pts,fs], r=0.005);
   Blue(.5) polyhedron(pts3, faces=fs);  
   
}
//tubePts_demo_actions_with_site();

