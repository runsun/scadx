include <../scadx.scad>

//. get_clocked_EFs

module demo_get_clocked_EFs()
{
   echom("demo_get_clocked_EFs()");
   pts = cubePts(3);
   faces = rodfaces(4);
   for( f = faces ) Line( get(pts,f), closed=true );

   subPts = get_subPts( pts= pts, faces=faces, shrink_ratio=0.25 );
   echo( subPts = subPts );
   
   clockedEFs= get_clocked_EFs( pts, faces ); ///<======
   echo( clockedEFs = clockedEFs );
      
   subfaces= get_subfaces( clockedEFs, faces);
   echo( subfaces = subfaces );	

   polyhedron( subPts, subfaces );
}
//demo_get_clocked_EFs(); ///===> preview time: 1 sec

module demo_get_clocked_EFs_dissected()
{
   echom("demo_get_clocked_EFs_dissected()");
   pts = cubePts(3);
   faces = rodfaces(4);
   for( f = faces ) Line( get(pts,f), closed=true );

   subPts = get_subPts( pts= pts, faces=faces, shrink_ratio=0.25 );
   echo( subPts = subPts );
   
   clockedEFs= get_clocked_EFs( pts, faces );
   echo( clockedEFs = clockedEFs );   
   tip_subfaces = get_tip_subfaces( clockedEFs );
   echo( tip_subfaces = tip_subfaces );   
   edge_subfaces = get_edge_subfaces( clockedEFs, faces=faces );
   echo( edge_subfaces = edge_subfaces );
   
   subfaces= get_subfaces( clockedEFs, faces);
   
   MarkPts( pts, Ball=false, Line=false
          , Label=[ "text",range(pts), "indent",0, "color","red" 
                  , "scale",0.75] );          
   MarkPts( [ for (f = subfaces) 
               sum( get(subPts,f) )/ len(f) ]
          , Ball=false, Line=false
          , Label=[ "text",range(subfaces), "indent",0, "color","purple"
                  , "scale", 0.5] );          
   MarkPts( subPts, Ball=false, Line=false
          , Label=[ "text",range(subPts), "indent",0, "color","black"
                  , "scale", 0.5] );
   
   for( f = get_core_subfaces( faces ) )
   { Plane( get(subPts,f), color="gold" );}   
   for( f = tip_subfaces )
   { Plane( get(subPts,f), color="green" );}   
   for( f = edge_subfaces )
   { Plane( get(subPts,f),  color="blue" );}
   
}
//demo_get_clocked_EFs_dissected();

module demo_get_clocked_EFs_depth2()
{
   echom("demo_get_clocked_EFs_depth2()");
   pts = cubePts(3);
   faces = rodfaces(4);
   for( f = faces ) Line( get(pts,f), closed=true );

   //-------------------------------------------
   subPts = get_subPts( pts= pts, faces=faces, shrink_ratio=0.25 );
   //echo( subPts = subPts );
   clockedEFs= get_clocked_EFs( pts, faces ); ///<======
   //echo( clockedEFs = clockedEFs );
   subfaces= get_subfaces( clockedEFs, faces);
   //echo( subfaces = subfaces );	
   //-------------------------------------------

   subPts2 = get_subPts( pts= subPts, faces=subfaces, shrink_ratio=0.25 );
   //echo( subPts = subPts );
   clockedEFs2= get_clocked_EFs( subPts, subfaces ); ///<======
   //echo( clockedEFs = clockedEFs );
   subfaces2= get_subfaces( clockedEFs2, subfaces);
   //echo( subfaces = subfaces );	
   //-------------------------------------------

   polyhedron( subPts2, subfaces2 );
}
//demo_get_clocked_EFs_depth2();  
  ///===> preview time: 2 sec ( center_ratio approach )
  ///===> preview time: 4 sec ( edge_ratio approach )


module demo_get_clocked_EFs_depth3()
{
   echom("demo_get_clocked_EFs_depth3()");
   pts = cubePts(3);
   faces = rodfaces(4);
   for( f = faces ) Line( get(pts,f), closed=true );

   //-------------------------------------------
   subPts = get_subPts( pts= pts, faces=faces, shrink_ratio=0.25 );
   //echo( subPts = subPts );
   clockedEFs= get_clocked_EFs( pts, faces ); ///<======
   //echo( clockedEFs = clockedEFs );
   subfaces= get_subfaces( clockedEFs, faces);
   //echo( subfaces = subfaces );	
   //-------------------------------------------

   subPts2 = get_subPts( pts= subPts, faces=subfaces, shrink_ratio=0.25 );
   //echo( subPts = subPts );
   clockedEFs2= get_clocked_EFs( subPts, subfaces ); ///<======
   //echo( clockedEFs = clockedEFs );
   subfaces2= get_subfaces( clockedEFs2, subfaces);
   //echo( subfaces = subfaces );	
   //-------------------------------------------

   subPts3 = get_subPts( pts= subPts2, faces=subfaces2, shrink_ratio=0.25 );
   //echo( subPts = subPts );
   clockedEFs3= get_clocked_EFs( subPts2, subfaces2 ); ///<======
   //echo( clockedEFs = clockedEFs );
   subfaces3= get_subfaces( clockedEFs3, subfaces2);
   //echo( subfaces = subfaces );	
   //-------------------------------------------

   polyhedron( subPts3, subfaces3 );
}
//demo_get_clocked_EFs_depth3();    
   ///===> preview time: 13 sec ( center_ratio approach )
   ///===> preview time: 24 sec ( edge_ratio approach )

module demo_get_clocked_EFs_depth4()
{
   echom("demo_get_clocked_EFs_depth4()");
   pts = cubePts(3);
   faces = rodfaces(4);
   for( f = faces ) Line( get(pts,f), closed=true );

   //-------------------------------------------
   subPts = get_subPts( pts= pts, faces=faces, shrink_ratio=0.25 );
   //echo( subPts = subPts );
   clockedEFs= get_clocked_EFs( pts, faces ); ///<======
   //echo( clockedEFs = clockedEFs );
   subfaces= get_subfaces( clockedEFs, faces);
   //echo( subfaces = subfaces );	
   //-------------------------------------------

   subPts2 = get_subPts( pts= subPts, faces=subfaces, shrink_ratio=0.25 );
   //echo( subPts = subPts );
   clockedEFs2= get_clocked_EFs( subPts, subfaces ); ///<======
   //echo( clockedEFs = clockedEFs );
   subfaces2= get_subfaces( clockedEFs2, subfaces);
   //echo( subfaces = subfaces );	
   //-------------------------------------------

   subPts3 = get_subPts( pts= subPts2, faces=subfaces2, shrink_ratio=0.25 );
   //echo( subPts = subPts );
   clockedEFs3= get_clocked_EFs( subPts2, subfaces2 ); ///<======
   //echo( clockedEFs = clockedEFs );
   subfaces3= get_subfaces( clockedEFs3, subfaces2);
   //echo( subfaces = subfaces );	
   //-------------------------------------------

   subPts4 = get_subPts( pts= subPts3, faces=subfaces3, shrink_ratio=0.25 );
   //echo( subPts = subPts );
   clockedEFs4= get_clocked_EFs( subPts3, subfaces3 ); ///<======
   //echo( clockedEFs = clockedEFs );
   subfaces4= get_subfaces( clockedEFs4, subfaces3);
   //echo( subfaces = subfaces );	
   //-------------------------------------------

   polyhedron( subPts4, subfaces4 );
}
///===> preview time:  3min 21sec
//demo_get_clocked_EFs_depth4();  



//. Smooth_obj

module demo_Smooth_obj_cycle2()
{
   echom("demo_Smooth_obj_cycle2()");
   Smooth_obj( pts = cubePts(size=3)
             , faces= rodfaces(4)
             , original_frame=["r",0.01, "color","black"]
             , cycle= 2
             );
}
//demo_Smooth_obj_cycle2();


module demo_Smooth_obj_rod3()
{
   echom("demo_Smooth_obj_rod3()");
   pf = hash( POLY_SAMPLES, "rod3" );
   
   Smooth_obj( pts = hash( pf, "pts" )
             , faces= hash( pf, "faces" )
             , original_frame=["r",0.01, "color","black"]
             , shrink_ratio = 0.2
             , cycle=2
             //, is_label=true
             
             );
}
//demo_Smooth_obj_rod3();


module demo_Smooth_obj_cone4()
{
   echom("demo_Smooth_obj_cone4()");
   pf = hash( POLY_SAMPLES, "cone4" );
   
   //color("teal")
   Smooth_obj( pts = hash( pf, "pts" )
             , faces= hash( pf, "faces" )
             , original_frame=["r",0.01, "color","black"]
             , is_label=true
             , is_echo=true
             , shrink_ratio=0.2
             , cycle=2
             
             );
}
//demo_Smooth_obj_cone4();

module demo_Smooth_obj_longcube()
{
   echom("demo_Smooth_obj_longcube()");
   pf = hash( POLY_SAMPLES, "longcube" );
   
   Smooth_obj( pts = hash( pf, "pts" )
             , faces= hash( pf, "faces" )
             , original_frame= false //["r",0.01, "color","black"]
             , cycle=3
             );
}
demo_Smooth_obj_longcube();
   /// 184q: cycle=3: 23 sec
   /// 184q: cycle=4: 3 min 59 sec
   /// 184q: cycle=2: 5 sec


module demo_Smooth_obj_3x3_mesh_3_failed_184q()
{
   echom("demo_Smooth_obj_3x3_mesh_3()");
   pf = hash( POLY_SAMPLES, "3x3_mesh_3" );
   
   Smooth_obj( pts = hash( pf, "pts" )
             , faces= hash( pf, "faces" )
             , original_frame= false //["r",0.01, "color","black"]
             , cycle=1
             );
}
demo_Smooth_obj_3x3_mesh_3();