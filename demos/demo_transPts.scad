/*
  Demo the transPts() function
  
  Their actions must be consistent with that of Transform() on objects.

*/

include <../scadx.scad>

module demo_transPts_translate_xyz()
{
  echom("demo_transPts_translate_xyz()");
  opt= ["markPts","l","frame",true];
  
  function opt(color="gold") = [ "markPts","l","frame",["color", color, "r", 0.05]];
  
  pts= cubePts();
  decoratePts( pts, opt=opt);
  decoratePts( transPts(pts, ["x", 3]) , opt=opt("red"));
  decoratePts( transPts(pts, ["y", 3]) , opt=opt("green"));
  decoratePts( transPts(pts, ["z", 3]) , opt=opt("blue"));
  decoratePts( transPts(pts, ["t", [3,3,0]]) , opt=opt("teal"));
  decoratePts( transPts(pts, ["transl", [0,-2,0]]) , opt=opt("purple"));
}
//demo_transPts_translate_xyz();

module demo_transPts_rot_xyz()
{
  echom("demo_transPts_rot_xyz()");
  opt= ["markPts","l","frame",true];
  
  function opt(color="gold") = [ "markPts","l","frame",["color", color, "r", 0.05]];
  
  pts= cubePts([3,2,1]);
  decoratePts( pts,opt=opt());
  decoratePts( transPts(pts, ["rotx", 90]) , opt=opt("red"));
  decoratePts( transPts(pts, ["roty", 120]) , opt=opt("green"));
  decoratePts( transPts(pts, ["rotz", 180]) , opt=opt("blue"));
}

//demo_transPts_rot_xyz();

module demo_transPts_rot_xyz2()
{
  echom("demo_transPts_rot_xyz2()");
  opt= ["markPts","l","frame",true];
  
  function opt(color="gold") = [ "markPts","l","frame",["color", color, "r", 0.05]];
  
  pts= cubePts([3,2,1]);
  decoratePts( pts,opt=opt());
  decoratePts( transPts(pts, ["rotx", 90, "roty", 45]) , opt=opt("red"));
  decoratePts( transPts(pts, ["roty",45, "rotx", 90]) , opt=opt("green"));
}
//demo_transPts_rot_xyz2();

module demo_transPts_rot()
{
  echom("demo_transPts_rot()");
  opt= ["markPts","l","frame",true];
  
  function opt(color="gold") = [ "markPts","l","frame",["color", color, "r", 0.05]];
  
  pts= cubePts([3,2,1]);
  
  decoratePts( pts,opt=opt());
  decoratePts( transPts(pts, ["rotx", 90]) , opt=opt(["red",0.2]));
  decoratePts( transPts(pts, ["rot", [90,90,0]]) , opt=opt("red"));
 
}
//demo_transPts_rot();

module demo_transPts_rota()
{
  echom("demo_transPts_rota()");
  echo(_blue("Rotate pts about any axis"));
  opt= ["markPts","l","frame",true];
  
  function opt(color="gold") = [ "markPts","l","frame",["color", color, "r", 0.05]];
  
  pts= cubePts([3,2,1]);
  decoratePts( pts,opt=opt);
  decoratePts( transPts(pts, ["rota", [[pts[0], pts[6]] // rot about P[0-6]
                                      , 45]             // by 45 degrees
                                      ]
                        ) 
                        , opt=opt("red"));
  Line([pts[0], pts[6]], color="blue", r=0.05);
                        
}

//demo_transPts_rota();

module demo_transPts_mirror()
{
  echom("demo_transPts_mirror()");
  opt= ["markPts","l","frame",true];
  
  function opt(color="gold") = [ "markPts","l","frame",["color", color, "r", 0.05]];
  
  pts= cubePts([3,2,1]);
  decoratePts( pts,opt=opt);
  decoratePts( transPts(pts, ["m", [ pts[0], pts[3],pts[4] ]]), opt=opt("red") );
  decoratePts( transPts(pts, ["m", [ pts[2]+[0,1,0], pts[1] ]]), opt=opt("green") );
  decoratePts( transPts(pts, ["m", [ 0,-1,0 ]]), opt=opt("blue") );
                        
}

demo_transPts_mirror();




//module tt_demo_translate()
//{
//   echom("tt_demo_translate");
//   x=8; y=4; z=2;
//   
//   cube([x,y,z]);
//
//   pts = cubePts([x,y,z]);
//   
//   polyhedron( pts, faces= rodfaces(4) );
//
//   MarkPts( pts, "l");
//
//}
//tt_demo_translate();

/*
  module mirrorPt_demo()
  {
    echo(_red("mp = mirrorPt( pt, pts )"));

    pts = randPts(3);
    pt = randPt();

    mp = mirrorPt( pt, pts );
    pp = projPt( pt, pts );
    echo( mp = mp );

    MarkPts(pts, ["plane", ["color",["blue", .5]], "label","PQR"]);
    MarkPts( [pt,  pp], ["l", "label",["text",["pt","pp"]]] );
    MarkPt( mp , ["g", "label",["text","mp"]]);

    for(p=pts) Mark90( [ p, pp, pt] ); 
  
    Line( [pt, pp] );
    Line( [pp, mp] );  
  }

  module mirrorPt_demo_normalLine()
  {
    echo(_red("mp = mirrorPt( pt, pq )"));

    pq = randPts(2);
    pt = randPt();

    pl = expandPts( getPlaneByNormalLine(pq), 1);
    echo(pl=pl);
    
    mp = mirrorPt( pt, pq );
    pp = projPt( pt, pl );
    echo( mp = mp );

    //color("red") Line(pq);
  
    MarkPts( pq, ["ch","label",["text","pq"]]);
    MarkPts( [pt,mp], "ch");
    
    MarkPts(pl, ["plane", ["color",["blue", .5]]]);
    MarkPts( [pt,  pp], ["l", "label",["text",["pt","pp"]]] );
    MarkPt( mp , ["g", "label",["text","mp"]]);

    for(p=pl) Mark90( [ p, pp, pt] ); 
 
  }

  module mirrorPt_demo_pt()
  {
    echo(_red("mp = mirrorPt( pt, pt2 )"));

    pt = randPt();
    pt2 = randPt();
    pq = [ORIGIN,pt2];
    pl = expandPts( getPlaneByNormalLine( [ORIGIN, pt2]), 1);
    echo(pl=pl);
    
    mp = mirrorPt( pt, pt2 );
    pp = projPt( pt, pl );
    echo( mp = mp );

    //color("red") Line(pq);
  
    MarkPts( pq, ["ch","label",["text","pq"]]);
    MarkPts( [pt,mp], "ch");
    
    MarkPts(pl, ["plane", ["color",["blue", .5]]]);
    MarkPts( [pt,  pp], ["l", "label",["text",["pt","pp"]]] );
    MarkPt( mp , ["g", "label",["text","mp"]]);

    for(p=pl) Mark90( [ p, pp, pt] ); 
 
  }
*/