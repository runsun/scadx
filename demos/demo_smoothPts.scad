include <../scadx.scad>

ISLOG=1;
/*
function getBezierPtsAt(  // Return Cubic Bezier pts from 
                          // pts[i] to pts[i+1].
   pts
   , i //=1 
   , n //=6 // extra pts to be added 
   , closed //=false
   , ends  //= [true,true] to include both end pts
   , aH //= undef //[1,1]
   , dH  //= undef //[0.5,0.5]
   , opt=[]
*/   

module demo_getBezierPtsAt()
{
   echom("demo_getBezierPtsAt()");
   //pts = randPts(5);
   pts=[[0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2], [2.51, -0.65, 0.81], [2.27, -1.95, -0.75]];
   //echo(pts=pts);
   
   h0 = getBezierPtsAt(pts,0);
   echo(h0_keys=keys(h0));
   Red() MarkPts( h(h0, "bzpts") );
   
   h1 = getBezierPtsAt(pts,1, n=8);
   Green() MarkPts( h(h1, "bzpts"));
   
   h2 = getBezierPtsAt(pts,2,n=8);
   Blue() MarkPts( h(h2, "bzpts"));
   
   h3 = getBezierPtsAt(pts,3,n=3);
   Purple() MarkPts( h(h3, "bzpts"));
   
   MarkPts(pts, color=0.5, Line=0.1);
   
}
//demo_getBezierPtsAt();



module demo_getBezierPtsAt_dH()
{
   echom("demo_getBezierPtsAt_dH()");
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81], [2.27, -1.95, -0.75]];
   
   h0 = getBezierPtsAt(pts,0);
   _red(_s("dH = default (={_})", str(h(h0,"dH"))));
   //echo( h0= _fmth(h0));
   Red() MarkPts( h(h0, "bzpts") );
   
   h01 = getBezierPtsAt(pts,0, dH=.9);
   _green(_s("dH = {_}", str(h(h01,"dH"))));
   //echo( h01= _fmth(h01));
   Green(0.5) MarkPts( h(h01, "bzpts") );
   
   h02 = getBezierPtsAt(pts,0, dH=1.2);
   _blue(_s("dH = {_}", str(h(h02,"dH"))));
   //echo( h02= _fmth(h02));
   Blue(0.5) MarkPts( h(h02, "bzpts") );
   
   h03 = getBezierPtsAt(pts,0, dH=[.2,1]);
   _purple(_s("dH = {_}", str(h(h03,"dH"))));
   //echo( h02= _fmth(h02));
   Purple(0.5) MarkPts( h(h03, "bzpts") );
   
   MarkPts(pts, color=0.5, Line=0.1);
   
}
//demo_getBezierPtsAt_dH();



module demo_getBezierPtsAt_dH2()
{
   echom("demo_getBezierPtsAt_dH2()");
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   h0 = getBezierPtsAt(pts,1);
   _red(_s("dH = {_}  (=default)", str(h(h0,"dH"))));
   //echo( h0= _fmth(h0));
   Red() MarkPts( h(h0, "bzpts") );
   
   h01 = getBezierPtsAt(pts,1, dH=.9);
   _green(_s("dH = {_}", str(h(h01,"dH"))));
   //echo( h01= _fmth(h01));
   Green(0.5) MarkPts( h(h01, "bzpts") );
   
   h02 = getBezierPtsAt(pts,1, dH=1.2);
   _blue(_s("dH = {_} "
        , str(h(h02,"dH"))));
   //echo( h02= _fmth(h02));
   Blue(0.5) MarkPts( h(h02, "bzpts") );
   
   h03 = getBezierPtsAt(pts,1, dH=[0.2,1]);
   _purple(_s("dH = {_} // Note the next edge is more bending power than prior edge"
          , str(h(h03,"dH"))));
   //echo( h02= _fmth(h02));
   Purple(0.5) MarkPts( h(h03, "bzpts") );   
   
   ////===========================================================
   ////  Drawing ref. plane and line for easier observation
   
   P=pts[0]; Q=pts[1]; R=pts[2]; S=pts[3];
   Jp = projPt( P, [Q,R]);
   Js = projPt( S, [Q,R]);
   twist_a = angle( [P, Jp, Jp+S-Js]);
   echo(twist_a = twist_a);
   
   abPt = angleBisectPt([P, Jp, Jp+S-Js]); /// this is on the plane that 
                                           /// equally divide angles
   M = sum( [Q,R] )/2;
   abPt2 = anglePt( [Q, M, abPt], a=90 ); /// Adj angle to 90 
   MarkPts( [abPt2, 3*M-2*abPt2], Label=false );
   
   /// Now make plane pts:
   Q1= Q + 1.5*(M - abPt2);
   R1= R + 1.5*(M - abPt2);
   Plane([ Q,Q1,R1,R] );  
   ////===========================================================

   
   Black() MarkPts(pts, "PQRS", Line=0.05);
   
}
//demo_getBezierPtsAt_dH2();



module demo_getBezierPtsAt_dH3()
{
   echom("demo_getBezierPtsAt_dH3()");
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   h0 = getBezierPtsAt(pts,1, dH=.5);
   _red(_s("dH = {_} ", str(h(h0,"dH"))));
   //echo( h0= _fmth(h0));
   Red() MarkPts( h(h0, "bzpts") );
   
   h01 = getBezierPtsAt(pts,1, dH=[.5,1]);
   _green(_s("dH = {_}", str(h(h01,"dH"))));
   //echo( h01= _fmth(h01));
   Green(0.5) MarkPts( h(h01, "bzpts") );
   
   h02 = getBezierPtsAt(pts,1, dH=[.5,2]);
   _blue(_s("dH = {_} "
        , str(h(h02,"dH"))));
   //echo( h02= _fmth(h02));
   Blue(0.5) MarkPts( h(h02, "bzpts") );
   
   h03 = getBezierPtsAt(pts,1, dH=[0.5,3]);
   _purple(_s("dH = {_} "
          , str(h(h03,"dH"))));
   //echo( h02= _fmth(h02));
   Purple(0.5) MarkPts( h(h03, "bzpts") );   
   
   ////===========================================================
   ////  Drawing ref. plane and line for easier observation
   
   P=pts[0]; Q=pts[1]; R=pts[2]; S=pts[3];
   Jp = projPt( P, [Q,R]);
   Js = projPt( S, [Q,R]);
   twist_a = angle( [P, Jp, Jp+S-Js]);
   echo(twist_a = twist_a);
   
   abPt = angleBisectPt([P, Jp, Jp+S-Js]); /// this is on the plane that 
                                           /// equally divide angles
   M = sum( [Q,R] )/2;
   abPt2 = anglePt( [Q, M, abPt], a=90 ); /// Adj angle to 90 
   MarkPts( [abPt2, 3*M-2*abPt2], Label=false );
   
   /// Now make plane pts:
   Q1= Q + 1.5*(M - abPt2);
   R1= R + 1.5*(M - abPt2);
   Plane([ Q,Q1,R1,R] );  
   ////===========================================================

   
   Black() MarkPts(pts, "PQRS", Line=0.05);
   
}
//demo_getBezierPtsAt_dH3();



module demo_getBezierPtsAt_dH4()
{
   echom("demo_getBezierPtsAt_dH4()");
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   n=10;
   
   h0 = getBezierPtsAt(pts,1, n=n, dH=[1.4,.6]);
   _red(_s("dH = {_} ", str(h(h0,"dH"))));
   //echo( h0= _fmth(h0));
   Red() MarkPts( h(h0, "bzpts"), Label=false );
   
   h01 = getBezierPtsAt(pts,1, n=n, dH=[1.2,.8]);
   _green(_s("dH = {_}", str(h(h01,"dH"))));
   //echo( h01= _fmth(h01));
   Green(0.5) MarkPts( h(h01, "bzpts"), Label=false );
   
   h02 = getBezierPtsAt(pts,1, n=n, dH=[1,1]);
   _blue(_s("dH = {_} "
        , str(h(h02,"dH"))));
   //echo( h02= _fmth(h02));
   Blue(0.5) MarkPts( h(h02, "bzpts"), Label=false );
   
   h03 = getBezierPtsAt(pts,1, n=n, dH=[.8,1.2]);
   _purple(_s("dH = {_} "
          , str(h(h03,"dH"))));
   //echo( h02= _fmth(h02));
   Purple(0.5) MarkPts( h(h03, "bzpts"), Label=false );   
   
   ////===========================================================
   ////  Drawing ref. plane and line for easier observation
   
   P=pts[0]; Q=pts[1]; R=pts[2]; S=pts[3];
   Jp = projPt( P, [Q,R]);
   Js = projPt( S, [Q,R]);
   twist_a = angle( [P, Jp, Jp+S-Js]);
   echo(twist_a = twist_a);
   
   abPt = angleBisectPt([P, Jp, Jp+S-Js]); /// this is on the plane that 
                                           /// equally divide angles
   M = sum( [Q,R] )/2;
   abPt2 = anglePt( [Q, M, abPt], a=90 ); /// Adj angle to 90 
   MarkPts( [abPt2, 3*M-2*abPt2], Label=false );
   
   /// Now make plane pts:
   Q1= Q + 1.5*(M - abPt2);
   R1= R + 1.5*(M - abPt2);
   Plane([ Q,Q1,R1,R] );  
   ////===========================================================

   
   Black() MarkPts(pts, "PQRS", Line=0.05);
   
}
//demo_getBezierPtsAt_dH4();



module demo_getBezierPtsAt_dH_app_spindle_1()
{
   echom("demo_getBezierPtsAt_dH_app_spindle_1()");
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   
   spindle_size=2;
   nThread=30;
   nBzPts_per_thread=3;   
   thread_range= range( 0.2, 0.05, n= nThread);
   
   for( dHi = thread_range )
   {
     h0 = getBezierPtsAt( pts,1
                        , n=nBzPts_per_thread
                        , dH=[dHi, spindle_size-dHi]);
     Line( h(h0, "bzpts"));
   }   
   
   //Black() MarkPts(pts, "PQRS", Line=0.05);
   
}
//demo_getBezierPtsAt_dH_app_spindle_1();



module demo_getBezierPtsAt_dH_app_spindle_1_solid()
{
   echom("demo_getBezierPtsAt_dH_app_spindle_1_solid()");
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   
   spindle_size=2;
   nThread=30;
   nBzPts_per_thread=3;   
   thread_range= range( 0.2, 0.05, n= nThread);
   echo(thread_range); 
   //pts = [ for( dHi = thread_range )
   pts = [ for( dHi = range( 0.2, 0.05, n= nThread) )
           each getBezierPtsAt( pts,1
                           , n=nBzPts_per_thread
                           , dH=[dHi, spindle_size-dHi])
         ] ;
      
   Line(pts);
   //Black() MarkPts(pts, "PQRS", Line=0.05);
   
}
//demo_getBezierPtsAt_dH_app_spindle_1_solid();


module demo_getBezierPtsAt_aH()
{
   echom("demo_getBezierPtsAt_aH()");
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81], [2.27, -1.95, -0.75]];
   
   h0 = getBezierPtsAt(pts,0);
   _red(_s("dH = default (={_})", str(h(h0,"dH"))));
   //echo( h0= _fmth(h0));
   Red() MarkPts( h(h0, "bzpts") );
   
   h01 = getBezierPtsAt(pts,0, dH=.9);
   _green(_s("dH = {_}", str(h(h01,"dH"))));
   //echo( h01= _fmth(h01));
   Green(0.5) MarkPts( h(h01, "bzpts") );
   
   h02 = getBezierPtsAt(pts,0, dH=[0.2,1]);
   _blue(_s("dH = {_}", str(h(h02,"dH"))));
   //echo( h02= _fmth(h02));
   Blue(0.5) MarkPts( h(h02, "bzpts") );
   
   h03 = getBezierPtsAt(pts,0, dH=1.2);
   _purple(_s("dH = {_}", str(h(h03,"dH"))));
   //echo( h02= _fmth(h02));
   Purple(0.5) MarkPts( h(h03, "bzpts") );
   
   MarkPts(pts, color=0.5, Line=0.1);
   
}
//demo_getBezierPtsAt_aH();


//demo_smoothPts();
module demo_smoothPts()
{
echom("demo_getBezierPtsAt()");
   //pts = randPts(5);
   pts0=[[0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2], [2.51, -0.65, 0.81], [2.27, -1.95, -0.75]];
   //echo(pts=pts);
   
   pts = smoothPts(pts0,n=40);
   echo(pts=pts);
   Red() MarkPts( pts, Label=false );



}