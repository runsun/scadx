include <../scadx.scad>

module Cylinder_demo_default()
{
   echom("Cylinder_demo_default");
   cylinder();
   
   translate([3,0,0]) Cylinder(color="red");
   Cylinder(color="green", h=2, actions=["x", 6]);
   
   //--camera=[ 0.88, 0.28, 0.8 ][ 63.40, 0.00, 53.00 ],12.4
}
Cylinder_demo_default();



module Cylinder_demo_r()
{
   echom("Cylinder_demo_r");
   echo(_color( "r=0.2", "darkkhaki"));
   Cylinder( r=0.4 );
   
   echo(_red( "r1=0.2" ));
   Cylinder(r1=0.2, actions=["x", 1.8] , color=["red",0.5]);
  
   echo( _green("r1=1, r2=0"));
   Cylinder( r1=1, r2=0, actions=["x", 3.5], color=["green",0.9] );
   //--camera=[ 1.56, 0.67, 0.36 ][ 63.40, 0.00, 39.00 ],10
}
//Cylinder_demo_r();



module Cylinder_demo_center()
{
   echom("Cylinder_demo_center");
   
   echo(_red( "r1=0.2" ));
   Cylinder(r1=0.2, color=["red",0.4], center=true);
  
   echo( _green("r1=1, r2=0"));
   Cylinder( r1=1, r2=0, actions=["x", 2], color=["green",0.4], opt=["center",true] );
   //--camera=[ 1.07, -0.52, 0.55 ][ 80.90, 0.00, 13.10 ],8.2
}
//Cylinder_demo_center();



module Cylinder_demo_partial_center()
{
   echom("Cylinder_demo_partial_center");
   
   echo(_red( "r1=0.2" ));
   Cylinder(color=["red",0.3], center=true);
  
   echo( _green("r1=1, r2=0"));
   Cylinder( color=["green",0.5], opt=["center",100] );
  
   echo( _green("r1=1, r2=0"));
   Cylinder( color=["blue",0.3], opt=["center",[0,0,1]] );
   //--camera=[ 0.50, -0.37, 0.96 ][ 62.00, 0.00, 13.80 ],11
}
//Cylinder_demo_partial_center();



module Cylinder_demo_Frame()
{
   echo(_b("Cylinder_demo_Frame()"));  
      
   //echo(_color("Default: [\"r\", 0.01]", "orange"));
   Cylinder();
   
//   echo(_b("Frame set by simple arg"));
//   echo(_color("frame=0.1", "orange"));
   Cylinder(Frame=0.05,opt=["actions",["z",-2]]);
   
   Cylinder(Frame=["r", 0.02, "color", "red" ],actions=["z",-4]);
   
   //_echo(_color("frame=['color','red']", "orange"));
//   
//   echo(_b("Frames are set by opt.frame"));
//   
//   _echo(_color("['frame', false]", "red"));
    Cylinder($fn=32, opt=["actions",["x",2.5], "Frame",false, "color",["green",0.8]]);
//   
//   _echo(_color("['frame', 0.05]", "green"));
    Cylinder($fn=32, opt=["actions",["x",2.5,"z",-2], "Frame",["r",0.02], "color",["blue",0.8]]);
//   

//   _echo(_color("['frame', ['color','red']]", "blue"));
//   Cylinder([1,3,2], opt=["actions",["x",6], "frame",["color","red"], "color",["blue",0.5]]);
//   
//   echo(_b("Frames set by opt.frame, but denied by simple arg "));
//   _echo(_color("Cylinder(...frame=false, opt=['frame', ['color','red']])", "purple"));
   Cylinder($fn=32, Frame=false, opt=["actions",["x",2.5, "z",-4], "Frame",["color","red"], "color",["purple",0.8]]);
   //--camera=[ 2.00, -0.74, -0.73 ][ 57.10, 0.00, 25.00 ],17
}
//Cylinder_demo_Frame();


module Cylinder_demo_actions()
{
   echo(_b("Cylinder_demo_actions()"));  
      
   echo(_darkorange("Cylinder([3,2,1]"));   
   Cylinder( opt=["color",["gold", 0.6]]);
   
   _echo(_red("opt=['actions', ['rotx', -90, 'x', 3]]"));
   Cylinder( opt=[ "actions", ["rotx", -90, "x", 3], "color", "red"]);
   
   _echo(_darkgreen("center=true, opt=['actions', ['rotx', -90, 'z', -2]]"), "   // actions with center");
   Cylinder( center=true, opt=[ "actions", ["rotx", -90, "z", -2], "color", "green"]);
   
   _echo(_blue("opt=['actions', ['roty', -45, 'z',2]]"), " // multiple actions");
   Cylinder( opt=[ "actions", ["roty", -45,"z",2], "color", ["blue",0.3]]);
   
   _echo(_purple("action=['y',3], opt=['actions', ['roty', -45, 'z',2]]"), "// actions expanded to opt.actions");
   Cylinder( actions=["y", 3], opt=[ "actions", ["roty", -45,"z",2], "color", ["purple",0.3]]);
   //--camera=[ 0.82, -0.24, 0.84 ][ 77.40, 0.00, 33.40 ],21
}
//Cylinder_demo_actions();

//====================================================
//. With site

module Cylinder_demo_with_site()
{
   echo(_b("Cylinder_demo_with_site()"));  
   
   //pqr=pqr();
   //echo(pqr=roundPts(pqr,2));
    pqr = [[-1.75, 1.81, 0.72], [-0.44, -0.41, -1.62], [0.92, 0.38, -1.79]];
   MarkPts(pqr,"PQR");
   Cylinder(site=pqr); //opt=["site",pqr, "markPts","l", "color",["gold",0.5]]);
   
   //pqr2= pqr(); //randPts(count=3, d=[4,5]);
   //echo(pqr2=roundPts(pqr2));
   pqr2 = [[0.3, -1.57, -0.98], [0.8, 0.51, 1.13], [-0.52, -0.83, 0.88]];
   MarkPts(pqr2,"PQR");
   Cylinder(site=pqr2); //opt=["site",pqr2, "markPts","l", "color",["red",0.3]]);
   //--camera=[ 0.97, 0.13, 0.11 ][ 57.10, 0.00, 85.90 ],15
}
//Cylinder_demo_with_site();

module Cylinder_demo_center_with_site()
{
   echo(_b("Cylinder_demo_center_with_site()"));  
   
   //pqr=pqr();
   //echo(pqr=roundPts(pqr,2));
   pqr = [[-1.34, -1.08, 1.23], [0.86, -1.57, -1.44], [0.38, 1.18, 0.83]];
   MarkPts(pqr,"PQR");  
   Cylinder( center=true, opt=["site",pqr, "color",["gold",0.5]]);
   //--camera=[ -0.17, -1.12, 1.37 ][ 358.30, 0.00, 166.60 ],15
}
//Cylinder_demo_center_with_site();

module Cylinder_demo_partial_center_with_site()
{
   echo(_b("Cylinder_demo_partial_center_with_site()"));  
   
   //pqr=pqr();
   //echo(pqr=roundPts(pqr,2));
   pqr = [[1.26, 1.87, -0.75], [-1.93, -0.17, 1.31], [-1.11, 1.72, 0.06]];
   MarkPts(pqr,"PQR");  
   Cylinder(center=true, opt=["site",pqr, "color",["gold",0.5]]);

   //pqr2=pqr();
   //color("red") //MarkPts(pqr2,"l=PQR;ch");  
   Cylinder(center=100
       , opt=["center",false, "site",pqr, "color",["red",0.3]]);

   Cylinder( h=2, center=false, opt=["site",pqr, "color",["green",0.5]]);
   //--camera=[ -0.41, 0.98, 1.32 ][ 53.60, 0.00, 86.10 ],15.3  
}
//Cylinder_demo_partial_center_with_site();


module Cylinder_demo_actions_with_site()
{
   echo(_b("Cylinder_demo_actions_with_site()"));  
   size=[3,2,1];
   
   //pqr=pqr();
   //echo(pqr=pqr);
   
   pqr= [[1.32238, -2.26811, -2.29973], [-1.64044, -0.640784, 1.87238]
        , [2.1467, 0.717838, -0.00367095]];
   
   MarkPts(pqr,"PQR");
   
   Cylinder(r=0.75, opt=["site",pqr]);
   
   Cylinder(r=0.75, opt=["actions", ["x",2],"site",pqr,"color",["red",0.5]]);
   
   Cylinder(r=0.75, opt=["actions",["roty",-45],"site",pqr,"color",["green",0.5]]);
  
   Cylinder(r=0.75, opt=["actions",["rotz",90, "rotx",-90,"y",1.5],"site",pqr,"color","blue"]);

   Cylinder(r=0.75, actions=["y",-1.5], opt=["actions",["rotz",90, "rotx",-90],"site",pqr,"color",["purple",0.5]]);
   //--camera=[ -0.52, 0.19, 0.77 ][ 94.90, 0.00, 16.80 ],15.3
}
//Cylinder_demo_actions_with_site();

//. dim

//module Cylinder_demo_dim()
//{
//   echo(_b("Cylinder_demo_dim()"));  
//   Cylinder([2,3,1], dim=[4,5,0], markPts="l");
//   
//   Cylinder([2,3,1], actions=["y", 4], dim=[[7,0,6]], markPts="l", color=["red",.5]);
//   Cylinder([2,3,1], site=randPts(3, d=[-1,-2]), dim=[0,1,3], markPts="l", color=["green",.5]);
//   
//   Cylinder([2,3,1], actions=["x", 4], dim=[6, [5,1.5,1], 2], markPts="l", color=["purple",.5]);
//   
//   echo(_olive("Cylinder(...dim=[4,5,0]...)"));
//   echo(_red("Cylinder(...dim=[[7,0,6]]...) // Enter 3 indices together"));
//   echo(_green("Cylinder(...site=pts, dim=[0,1,3]...) // Works with site, too"));
//   echo(_purple("Cylinder(...dim=[ 6, [5,1.5,1], 2 ] ...) // Enter the site of any pt"));
//   
//}
////Cylinder_demo_dim();
//
//module Cylinder_demo_dim2()
//{
//   echo(_b("Cylinder_demo_dim2()"));  
//   //Cylinder([6,4,2], dim=[[4,5,0]], markPts="l");
//   
//   size=[4,2,1];
//   Cylinder(size, dim=[[6,7,3],[7,3,1],[7,0,1]],  markPts="l");
//   
//   Cylinder(size, actions=["y", 4], dim= [ "pqrs", [[7,6,1]]
//                                     , "color", "green" ]
//        , markPts="l", color=["red",.2]
//        );
//        
//   Cylinder(size, actions=["x", 6], dim=["pqrs",[[6,7,3],[7,3,1],[7,0,1]]
//                                     ,"color","red"]
//        , markPts="l", color=["green",.2]
//        );
//        
//   Cylinder(size, actions=["y", -5], dim=[ "pqrs",[["pqr",[6,7,3], "color","green"]
//                                              ,[7,3,1]
//                                              ,[7,0,1]
//                                              ]
//                                     ,"color","purple"]
//        , markPts="l", color=["blue",.2]
//        );
// 
//}
////Cylinder_demo_dim2();



//

//module Cylinder_demo_sphere()
//{
//   echo(_color( "sphere=false", "darkkhaki"));
//   Cylinder( randPts(2), sphere=false );
//  
//   echo(_red( "sphere= 0.15" ));
//   color("red") Cylinder( randPts(2), sphere=0.15 );
//  
//   echo( _green("sphere= =[0.05,0.2]"));
//   color("green") Cylinder( randPts(2), sphere=[0.05,0.2] );
//  
//   echo( _blue("sphere=[undef,0.15]"));
//   color("blue") Cylinder( randPts(2), sphere=[undef,0.15] );
//  
//   echo(_color( "sphere following different r's", "purple"));
//   color("purple") Cylinder( randPts(2), r2=0.2);
//
//   echo(_color( "Cone: r1=0, r2=0.3, sphere=false", "teal"));
//   color("teal") Cylinder( randPts(2), r1=0, r2=0.5, sphere=false);  
//}
//Cylinder_demo_sphere();
//
//module Cylinder_demo_markpts()
//{
//   echo(_color( "markpts=true", "darkkhaki"));
//   Cylinder( randPts(2), markpts=true );
//  
//   echo(_red( "markpts=\"l\"" ), _green(" //l = label"));
//   color("red") Cylinder( randPts(2), markpts="l" );
//  
//   echo( _green( "markpts=\"l=AB\""));
//   color("green") Cylinder( randPts(2), markpts="l=AB" );
//
//   echo( "markpts= [\"r\",0.3]");
//   Cylinder( randPts(2), sphere=false, markpts=["r",0.3] ); 
//}
//Cylinder_demo_markpts();
