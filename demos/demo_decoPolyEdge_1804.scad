include <../scadx.scad>

//module __show_decoPolyEdge(pts,faces,template)
//{
//   //
//   // To be called by other demos
//   //
//   echom("__show_decoPolyEdge()");
//   pts = pts?pts:transPts(hashs(POLY_SAMPLES,["cube","pts"]), ["y",2]);
//   faces = faces?faces:hashs(POLY_SAMPLES,["cube","faces"]);
//   
//   MarkPts(pts, Line=false, Ball=false
//          , Label=["text", range(pts), "color","blue"]); //, "indent", -.3]);
//   
//   for(fi=range(faces))
//   { f=faces[fi];
//     for( eg=get2(f) ) Line( get(pts,eg), color="red", r=0.005 );
//     //MarkPt( sum( get(pts,f))/len(f)
//           //, Label=["text", fi, "indent",0, "color", "red"] 
//           //, Ball=false
//           //);     
//   }
//   tmp = to3d(tmplate);
//   //----------------------------------------------
//   //tmp = [ [0,1], [0.5,0.5], [1,0.8], [2,0] ];
//   //tmp = [ [0,1], [2,0] ];
//   //tmp = [ [0,1], [0.5,0.5], [2,0] ];
//   //echo( tmp=tmp, tmp3d=to3d(tmp,-1) );
//   
//   //----------------------------------------------
//   tmp = reverse( 
//          arcPts( [[0,0,1],ORIGIN,[1,0,0]], rad=1, a=90, a2=90, n=64 )
//          );
//   Plane( concat( to3d(tmp), [ORIGIN]), mode=1);
//   //----------------------------------------------
//   
//   new_ptf= decoPolyEdge( pts, faces
//                        , pis = [5,4,0]
//                        , dir = 7
//                        , template= tmp
//                        );
//                        
//   //echo(new_ptf = new_ptf);
//   //MarkPts( new_ptf[0]);
//   
//   newpts = new_ptf[0];
//   newfaces= new_ptf[1];
//   //for( fi = range(newfaces))
//   //{
//      //Plane( get(newpts, newfaces[fi]), mode=1, color=[COLORS[fi],1] );
//   //}
//   
//   polyhedron( new_ptf[0], new_ptf[1] );
//}
////demo_decoPolyEdge();

module __show_decoPolyEdge(pts,faces,template)
{
   //
   // To be called by other demos
   //
   echom("__show_decoPolyEdge()");
   pts = pts?pts:transPts(hashs(POLY_SAMPLES,["cube","pts"]), ["y",2]);
   faces = faces?faces:hashs(POLY_SAMPLES,["cube","faces"]);
   echo(template = template);
   
   MarkPts(pts, Line=false, Ball=false
          , Label=["text", range(pts), "color","blue"]); //, "indent", -.3]);
   
   for(fi=range(faces))
   { f=faces[fi];
     for( eg=get2(f) ) Line( get(pts,eg), color="red", r=0.005 );
     //MarkPt( sum( get(pts,f))/len(f)
           //, Label=["text", fi, "indent",0, "color", "red"] 
           //, Ball=false
           //);     
   }
   
   //----------------------------------------------
   //tmp = [ [0,1], [0.5,0.5], [1,0.8], [2,0] ];
   //tmp = to3d( [[0,1], [2,0]] ) ;
   //tmp = to3d([ [0,1], ORIGIN, [2,0] ]);
   //echo( tmp=tmp, tmp3d=to3d(tmp,-1) );
   //----------------------------------------------
   //tmp = reverse(arcPts([[0,0,1],ORIGIN,[1,0,0]],rad=1,a=90,a2=90,n=64) );
   tmp = to3d(template);
   Plane( concat( tmp, [ORIGIN]), mode=1);
   MarkPts(tmp, Ball=["dec_r",0,"r",0.05]
          , Label=false, Line=["r",0.01]);
   
   //----------------------------------------------
   
   new_ptf= decoPolyEdge( pts, faces
                        , pis = [5,4,0]
                        , dir = 7
                        , template= tmp
                        );
                        
   //echo(new_ptf = new_ptf);
   //MarkPts( new_ptf[0]);
   
   newpts = new_ptf[0];
   newfaces= new_ptf[1];
   //for( fi = range(newfaces))
   //{
      //Plane( get(newpts, newfaces[fi]), mode=1, color=[COLORS[fi],1] );
   //}
   //MarkPts( newpts );
   polyhedron( new_ptf[0], new_ptf[1] );
}


module demo_decoPolyEdge_cut_corner_1()
{
   echom("demo_decoPolyEdge_cut_corner_1()");
   __show_decoPolyEdge(template= [[0,1], [2,0]] );
}
//demo_decoPolyEdge_cut_corner_1();


module demo_decoPolyEdge_cut_corner_2()
{
   echom("demo_decoPolyEdge_cut_corner_2()");
   __show_decoPolyEdge(template= [ [0,1], ORIGIN, [2,0] ] );
}
//demo_decoPolyEdge_cut_corner_2();


module demo_decoPolyEdge_cut_corner_3()
{
   echom("demo_decoPolyEdge_cut_corner_3()");
   template= [ [0,1],[0,0.5],[0.25,0.25], [0.5,0], [1,0]];
   __show_decoPolyEdge(template= template );
}
//demo_decoPolyEdge_cut_corner_3();


module demo_decoPolyEdge_arc()
{
   echom("demo_decoPolyEdge_arc()");
   template=(arcPts([[0,0,-1],ORIGIN,[0,1,0]],rad=1,a=90,a2=90,n=24) );
  __show_decoPolyEdge(template= template );
}
//demo_decoPolyEdge_arc();


module demo_decoPolyEdge_concave()
{
   echom("demo_decoPolyEdge_concave()");
   template= [ [0,1],[0,0.5],[-1,0.5], [-1,-1.5], [0.5,-1.5], [0.5,0], [1,0]];
   __show_decoPolyEdge(template= template );   
}
//demo_decoPolyEdge_concave();


module demo_decoPolyEdge_concex_arc()
{
   echom("demo_decoPolyEdge_concex_arc()");
   p3= [[0,0.5,0],[0.5,0.5,0],[0.5,1,0]];
   
   n=48;
   template= [ for(i=range(n))
               anglePt( p3, a= i*270/n )
              ];
             //( arcPts(p3,rad=1,a=90,a2=90,n=24) );
   MarkPts(p3);
   //echo( template=template );
   __show_decoPolyEdge( template= template );
}
//demo_decoPolyEdge_concex_arc();


module demo_decoPolyEdge_concex_arc2()
{
   echom("demo_decoPolyEdge_concex_arc2()");
   template=reverse(arcPts([[0,0,-1],ORIGIN,[1,0,0]],rad=1,a=90,a2=90,n=24) );
   __show_decoPolyEdge(template= template );
}
//demo_decoPolyEdge_concex_arc2();

//. site


module demo_decoPolyEdge_cut_corner()
{
   echom("demo_decoPolyEdge_cut_corner()");
   _pts = hashs(POLY_SAMPLES,["cube","pts"]);
   //pqr = pqr(3);
   pqr = [[0.025438, -0.668024, -2.33806], [-2.84666, 2.72857, 0.493522], [-1.25729, -2.45536, 1.66304]];
   echo(pqr=pqr);
   pts = movePts( _pts, to=pqr);   
   __show_decoPolyEdge( pts=pts
                      , template= [ [0,1],[0,0.5],[0.25,0.25], [0.5,0], [1,0] ] );
}
//demo_decoPolyEdge_cut_corner();

module demo_decoPolyEdge_dent()
{
   echom("demo_decoPolyEdge_dent()");
   //_pts = hashs(POLY_SAMPLES,["cube","pts"]);
   //pqr = pqr(3);
   //pqr = [[0.025438, -0.668024, -2.33806], [-2.84666, 2.72857, 0.493522], [-1.25729, -2.45536, 1.66304]];
   //echo(pqr=pqr);
   //pts = movePts( _pts, to=pqr); 
   depth = 2;
   width = 1;
   pts = addy(cubePts(3),2);  
   __show_decoPolyEdge( pts=pts
                      //, template= [ [0,1],[0,0.5],[0.25,0.25], [0.5,0], [1,0] ] 
                      , template= [ [0,1-depth], [0,0]
                                  , [width,0], [width,1]
                                  , [(3-width)/2+width,1],[(3-width)/2+width,0] ] 
                      
                      );
}
//demo_decoPolyEdge_dent();

//. dent

 
 
//. dual cuts


module demo_decoPolyEdge_dual_cuts(pts,faces,template)
{
   echom("demo_decoPolyEdge_dual_cuts()");
   pts = pts?pts:transPts(hashs(POLY_SAMPLES,["cube","pts"]), ["y",2]);
   faces = faces?faces:hashs(POLY_SAMPLES,["cube","faces"]);
   
   MarkPts(pts, Line=false, Ball=false
          , Label=["text", range(pts), "color","blue"]); //, "indent", -.3]);
   
   for(fi=range(faces))
   { f=faces[fi];
     for( eg=get2(f) ) Line( get(pts,eg), color="red", r=0.005 );
     MarkPt( sum( get(pts,f))/len(f)
           , Label=["text", fi, "indent",0, "color", "red"] 
           , Ball=false
           );     
   }
   
   //----------------------------------------------
   //tmp = [ [0,1], [0.5,0.5], [1,0.8], [2,0] ];
   //tmp = to3d( [[0,1], [2,0]] ) ;
   tmp = to3d([ [0,1], ORIGIN, [1,0] ]);
   //echo( tmp=tmp, tmp3d=to3d(tmp,-1) );
   //----------------------------------------------
   //tmp = reverse(arcPts([[0,0,1],ORIGIN,[1,0,0]],rad=1,a=90,a2=90,n=64) );
   //tmp = to3d(template);
   Plane( concat( tmp, [ORIGIN]), mode=1);
   MarkPts(tmp, Ball=["dec_r",0,"r",0.05]
          , Label=false, Line=["r",0.01]);
   
   //----------------------------------------------
   
   new_ptf= decoPolyEdge( pts, faces
                        , pis = [5,4,0]
                        , dir = 7
                        , template= tmp
                        );

   newpts = new_ptf[0];
   newfaces= new_ptf[1];
      
   new_ptf2= decoPolyEdge( pts=newpts
                        , faces=newfaces
                        , pis = [7,6,2]
                        , dir = 5
                        , template= tmp
                        );
                        
   //MarkPts( new_ptf2[0] ); 
   polyhedron( new_ptf2[0], new_ptf2[1] );
} 
//demo_decoPolyEdge_dual_cuts();