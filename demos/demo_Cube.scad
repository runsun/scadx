include <../scadx.scad>

//echo($fn=$fn);
//. W/o site

module Cube_demo_default() 
{
   //--camera=1.2,0.5,1.7,50,0,45,13;
   //--imgsize=450,375;   
   echom("Cube_demo_default()");
   Cube(); //MarkPts=true);
   translate([-2.5,00]) Cube(size=2,color="red");
   translate([0,1.5,0]) Cube(size=[3,1.5,1],color="green");
   //echo(PTS=PTS);
}
Cube_demo_default();

module Cube_demo_color()
{
   echo(_b("Cube_demo_color()")); 
   //--camera=[ 1.45, 1.65, 1.18 ],[ 65.50, 0.00, 51.10 ],21;
   
   Cube();
    
   color("red") Cube(size=3, actions=["y",2.5]);

   Cube(size=[4,2,1], color=["green",0.5], actions=["x", 2.5]);

   Cube(size=[4,2,1], actions=["y",-3], opt=["color",["blue",0.2]]);
   
   Cube(color=["teal",0.5], opt=["color", "black", "actions", ["t",[-2.5,0,0]] ] );
   	   
   echo(_color("Cube() : default", "olive"));
   echo(_color("color(\"red\") Cube(...): Using built-in coloring, which also colors the frame ", "red"));
   echo(_b("The following 2 use Cube()'s color setting, both work the same way:"));
   echo(_color("Cube(..., color=[\"green\",0.5]) // use plain arg", "darkgreen"));
   echo(_color("Cube(..., opt=[\"color\",[\"blue\",0.2]]) // use opt.color", "blue"));
   echo(_color("Cube(..., color=[\"teal\",0.5], opt=[\"color\",\"black\"]) // plain color overwrites opt.color", "teal"));
}
//Cube_demo_color();

module Cube_demo_center()
{
   echo(_b("Cube_demo_center()"));     
   Cube(center=true);
   Cube(size=[4,3,2.5], opt=["center", true, "color", ["red", 0.2]]); 
   //--camera=[ -0.63, 0.68, -1 ][ 58.50, 0.00, 47.80 ],13.8;
}
//Cube_demo_center();

module Cube_demo_partial_center()
{
   //--camera=[ 1.15, 0.63, 0.88 ],[ 50.10, 0.00, 28.00 ],15;
   //--imgsize=450,375;   
   echo(_b("Cube_demo_partial_center()"));  
   Cube([3,2,1], center=[1,0,0], opt=["color", ["red", 0.2]] );
   Cube([3,2,1], opt=["center", [0,1,1], "color", ["green", 0.2]] );
   Cube([3,2,1], center=101, color=["blue", 0.2] ); 
   Cube([3,2,1], center=1, color=["olive", 0.2] );
}
//Cube_demo_partial_center();

module Cube_demo_Frame()
{
   //--camera=[ 1.94, 1.90, 1.23 ],[ 61.30, 0.00, 37.60 ],29;
   //--imgsize=450,375;   
   echo(_b("Cube_demo_Frame()"));  
      
   echo(_color("Default: [\"r\", 0.01]", "orange"));
   Cube([1,3,2]);
   
   echo(_b("Frame set by simple arg"));
   echo(_color("Frame=0.1", "orange"));
   Cube([1,3,2], MarkPts=true, Frame=0.1,opt=["actions",["x",-2]]);
   _echo(_color("Frame=['color','red']", "orange"));
   Cube([1,3,2], Frame=["color","red"],opt=["actions",["x",-4]]);
   
   echo(_b("Frames are set by opt.Frame"));
   
   _echo(_color("['Frame', false]", "red"));
   Cube([1,3,2], opt=["actions",["x",2], "Frame",false, "color",["red",0.5]]);
   
   _echo(_color("['Frame', 0.05]", "green"));
   Cube([1,3,2], opt=["actions",["x",4], "Frame",0.05, "color",["green",0.5]]);
   
   _echo(_color("['Frame', ['color','red']]", "blue"));
   Cube([1,3,2], opt=["actions",["x",6], "Frame",["color","red"], "color",["blue",0.5]]);
   
   echo(_b("Frames set by opt.Frame, but denied by simple arg "));
   _echo(_color("Cube(...Frame=false, opt=['Frame', ['color','red']])", "purple"));
   Cube([1,3,2], Frame=false, opt=["actions",["x",8], "Frame",["color","red"], "color",["purple",0.5]]);
  
}
//Cube_demo_Frame();

module Cube_demo_opt_Frame()
{
   //--camera=[ 4.38, 3.12, 0.29 ],[ 58.50, 0.00, 36.90 ],26;
   //--imgsize=450,375;   
   echo(_b("Cube_demo_opt_Frame()"));  
      
   size=[1,3,2];
   
   echo(_orange("Default: [\"r\", 0.01]"));
   Cube(size);

   _echo(_b("opt=[ 'Frame', ['r', 0.1, 'color', ['red', 0.5]]]"));
   
   opt=[ "Frame", ["r", 0.1, "color", ["red", 0.5]]];
   _echo(_red("opt=opt"), "  // Set Frame r with opt.Frame");
   Cube(size, actions=["x",2], opt= update(["color",["red",0.5]],opt) );  
   
   _echo(_green("Frame=['r',0.2], opt=opt"), "  // opt.Frame is overwritten by Frame" );
   Cube(size, actions=["x",4], Frame=["r",0.2], opt=update(["color",["green",0.8]],opt)); 
   
   _echo(_blue("color('blue') Cube(size, Frame=['r',0.2], opt=opt);"), " // colorizes everything" );
   color("blue") Cube(size, actions=["x",6], Frame=["r",0.2], opt=update(["color",["green",0.5]],opt));
    
   _echo(_purple("Frame=['color','blue'], opt=opt"), "  // opt.Frame is overwritten by Frame" );
   Cube(size, actions=["x",8], Frame=["color","blue"], opt=update(["color",["purple",0.5]],opt)); 
   
   _echo(_olive("Frame=false, opt=opt"), "  // Frame=false disables it" );
   Cube(size, actions=["x",10], Frame=false, opt=update(["color",["olive",0.8]],opt)); 
    
}
//Cube_demo_opt_Frame();

module Cube_demo_actions()
{
   //--camera=[ 0.71, 1.57, 1.14 ],[ 52.90, 0.00, 33.40 ],23;
   //--imgsize=450,375;   
   echo(_b("Cube_demo_actions()"));  
      
   echo(_darkorange("Cube([3,2,1]"));   
   Cube([3,2,1],opt=["color",["gold", 0.6]]);
   
   _echo(_red("opt=['actions', ['rotx', -90]]"));
   Cube(size=[3,2,1], opt=[ "actions", ["rotx", -90], "color", ["red",0.3]]);
   
   _echo(_darkgreen("center=true, opt=['actions', ['rotx', -90]]"), "   // actions with center");
   Cube(size=[3,2,1], center=true, opt=[ "actions", ["rotx", -90], "color", ["green",0.3]]);
   
   _echo(_blue("opt=['actions', ['roty', -45, 'z',2]]"), " // multiple actions");
   Cube(size=[3,2,1],  opt=[ "actions", ["roty", -45,"z",2], "color", ["blue",0.3]]);
   
   _echo(_purple("action=['y',3], opt=['actions', ['roty', -45, 'z',2]]"), "// actions expanded to opt.actions");
   Cube(size=[3,2,1],  actions=["y", 3], opt=[ "actions", ["roty", -45,"z",2], "color", ["purple",0.3]]);
   
   // actions=false doesn't work in the following case, 'cos "actions" is NOT a sub
   // so setting actions=false won't disable it
   //
   // _echo(_olive("actions=false, opt=['actions', ['roty', -45, 'z',2]]"), " // actions=false disable it");
   // Cube(3,  actions=false, opt=[ "actions", ["roty", -45,"z",2], "color", ["olive",0.8]]);
}
//Cube_demo_actions();

//====================================================
//. With site

module Cube_demo_with_site()
{
   //--camera=[ 1.01, 1.19, 0.78 ],[ 52.90, 0.00, 39.70 ],17;
   //--imgsize=450,375;   
   echo(_b("Cube_demo_with_site()"));  
   
   //pqr=randPts(3,d=1.5); //pqr();
   pqr=[ [-1.10932, -0.855533, -0.748128]
       , [-0.00821536, 0.183257, -1.22133]
       , [-0.613839, 0.0589978, 0.955911]];
   //echo(pqr=pqr);
   MarkPts(pqr,Label="PQR");
   Cube(opt=["site",pqr, "markPts","l", "color",["gold",0.5]]);
   
   //pqr2= randPts(count=3, d=[1.5,2.5]);
   pqr2= [[1.52227, 2.15535, 2.35498]
         , [1.85897, 1.67035, 2.4865]
         , [2.24626, 1.89293, 1.92314]];
   
   //echo(pqr2=pqr2);
   MarkPts(pqr2,Label=true);
   Cube([3,2,1], opt=["site",pqr2, "markPts","l", "color",["red",0.3]]);
   
}
//Cube_demo_with_site();

module Cube_demo_center_with_site()
{
   //--camera=[ -1.01, 0.27, 0.27 ][ 329.60, 0.00, 198.50 ],13.5;
   //--imgsize=450,375;   
   echo(_b("Cube_demo_center_with_site()"));  
   
   //pqr=pqr();
   //echo(pqr=pqr);
   pqr=[[-1.28101, 0.494192, 0.97126]
       , [-1.15165, 0.0592333, -0.649364]
       , [1.24644, 0.341728, 1.73458]];
   MarkPts(pqr,"PQR");  
   Cube([3,2,1], center=true, opt=["site",pqr, "color",["gold",0.5]]);
     
}
//Cube_demo_center_with_site();

module Cube_demo_partial_center_with_site()
{
   echo(_b("Cube_demo_partial_center_with_site()"));  
   
   pqr=[[0.399011, -1.70855, 1.67873]
       , [0.164424, -1.06142, -1.56369]
       , [0.0152843, 1.56007, -1.22532]];
   echo(pqr=pqr);
   MarkPts(pqr,"PQR");  
   Cube([3,2,1], center=[0,1,0], opt=["site",pqr, "color",["gold",0.5]]);

   Cube([3,2,1], center=[1,0,1]
       , opt=["center",10, "site",pqr, "color",["red",0.3]]);
       
   //--camera=[ 0.43, -0.69, -0.32 ],[ 57.10, 0.00, 56.00 ],15.3;   
}
//Cube_demo_partial_center_with_site();


module Cube_demo_actions_with_site()
{
   echo(_b("Cube_demo_actions_with_site()"));  
   size=[3,2,1];
   
   pqr= [[0.612148, -1.23256, 0.639392]
        , [-1.43049, -1.67262, 0.576152]
        , [-0.159001, -0.319813, -0.546865]
        ];   
   MarkPts(pqr,"PQR");
   
   Cube(size, opt=["site",pqr]);   
   Cube(size, opt=["actions", ["x",2],"site",pqr,"color",["red",0.5]]);
   
   Cube(size, opt=["actions",["roty",-45],"site",pqr,"color",["green",0.5]]);  
   Cube(size,  opt=["actions",["rotz",90, "rotx",-90],"site",pqr,"color",["blue",0.5]]);

   Cube(size, actions=["y",-1.5], opt=["actions",["rotz",90, "rotx",-90],"site",pqr,"color",["purple",0.5], "markPts","l"]);
   //--camera=[ -0.49, -1.47, 0.20 ],[ 65.50, 0.00, 51.10 ],21;
}
//Cube_demo_actions_with_site();

//. dim

//module Cube_demo_dim()
//{
//   //--camera=1.2,0.5,1.7,50,0,45,13;
//   //--imgsize=450,375;   
//   echo(_b("Cube_demo_dim()"));  
//   Cube([2,3,1], dim=[4,5,0], markPts="l");
//   
//   Cube([2,3,1], actions=["y", 4], dim=[[7,0,6]], markPts="l", color=["red",.5]);
//   Cube([2,3,1], site=randPts(3, d=[-1,-2]), dim=[0,1,3], markPts="l", color=["green",.5]);
//   
//   Cube([2,3,1], actions=["x", 4], dim=[6, [5,1.5,1], 2], markPts="l", color=["purple",.5]);
//   
//   echo(_olive("Cube(...dim=[4,5,0]...)"));
//   echo(_red("Cube(...dim=[[7,0,6]]...) // Enter 3 indices together"));
//   echo(_green("Cube(...site=pts, dim=[0,1,3]...) // Works with site, too"));
//   echo(_purple("Cube(...dim=[ 6, [5,1.5,1], 2 ] ...) // Enter the site of any pt"));
//   
//}
////Cube_demo_dim();
//
//module Cube_demo_dim2()
//{
//   //--camera=1.2,0.5,1.7,50,0,45,13;
//   //--imgsize=450,375;   
//   echo(_b("Cube_demo_dim2()"));  
//   //Cube([6,4,2], dim=[[4,5,0]], markPts="l");
//   
//   size=[4,2,1];
//   Cube(size, dim=[[6,7,3],[7,3,1],[7,0,1]],  markPts="l");
//   
//   Cube(size, actions=["y", 4], dim= [ "pqrs", [[7,6,1]]
//                                     , "color", "green" ]
//        , markPts="l", color=["red",.2]
//        );
//        
//   Cube(size, actions=["x", 6], dim=["pqrs",[[6,7,3],[7,3,1],[7,0,1]]
//                                     ,"color","red"]
//        , markPts="l", color=["green",.2]
//        );
//        
//   Cube(size, actions=["y", -5], dim=[ "pqrs",[["pqr",[6,7,3], "color","green"]
//                                              ,[7,3,1]
//                                              ,[7,0,1]
//                                              ]
//                                     ,"color","purple"]
//        , markPts="l", color=["blue",.2]
//        );
// 
//}
////Cube_demo_dim2();
