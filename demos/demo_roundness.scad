include <../scadx.scad>

ISLOG=1;

module dev_roundness()
{
   echom("dev_roundness()");
   
   //=====================================
   // 2 random pts
     _pq = randPts(2, d=3, z=0);
     
   // Make sure its length 
     pq = [_pq[0], onlinePt(_pq,4)];
   
   // From it generate 10 random noise pts
     _pts = [ for(i=range(10)) each 
                [for(j=[0:2]) 
                onlinePt( pq, len= dist(pq)/10*i) + randPt(d=1, z=0)
                ]
            ];
        
  // Translate the whole thing such that its centroidPt is on O:
    pts = transPts( _pts, actions=["t",-1*centroidPt(_pts)] );
    // MarkPts(pts);

   //=====================================
   // Group pts by quadrant: 
     qpts = groupByQuadrant( pts ); // [ [pt,pt,..], [...],[...],[...]]
     cpts = quadCentroidPts( pts ); // [ C1, C2, C3, C4 ]
     echo(cpts = cpts );
   
     for(i=[0:3])
     {
        color(COLORS[i+1]) 
        {
          MarkPt( cpts[i], r=0.1 );
          MarkPts( qpts[i],  Label=false, r=0.01, line=0 );
        }  
     }  
   
   //==========================================
   // subMassLines
     smlines = [ get(cpts,[0,2]), get(cpts,[1,3]) ]; 
     Red() Line( smlines[0], r=0.05  );  
     Green() Line( smlines[1], r= 0.05  );  
  
   // intsec of the two subMassLines
     xpt= intsec( smlines );
     Black() MarkPt(xpt, "X", r=0.1);
     echo(xpt= xpt); 
   
   //================================================
   // Approach-1:
     a12 = angle( [ cpts[0], xpt, cpts[1] ]);
     axis= a12 < 90 ?
            [ cpts[0]+cpts[1]-xpt, cpts[2]+cpts[3]-xpt]
           :[ cpts[0]+cpts[3]-xpt, cpts[2]+cpts[1]-xpt];
     
     Red(.2) 
     Line( axis, r=0.2, $fn=16); 
  
     echo(a12=a12, axis=axis); 
   
   //================================================
   // Approach-2:
     segs = [ for(c=cpts) [xpt,c] ];
     
   //================================================
   // Approach-3:
     dists= quicksort([for(i=[0:1]) [dist( smlines[i] ),i ] ]);
     echo(dists=dists); 
     longer= dists[1][0];
     shorter= dists[0][0];
     
     
  
  //Gold(.2) 
  //Line( [pq[0]-_C, pq[1]-_C], r=0.3, $fn=16); 
  
   
}
dev_roundness();

module demo_roundness_3pts()
{
   echom("demo_roundness_3pts()");

   module Obj(x=0)
   {
     _pts = randPts(3,z=0);
     xsize = maxx(_pts)-minx(_pts);
     __pts=[ for(p=_pts) p*2/xsize ];
     minx= minx(__pts);
     miny = miny(__pts);
     pts = [for(p=__pts)p-[minx-x,miny,0]+Y*3];
     MarkPts( pts, Ball=0, Line=["closed",1, "r",0.02, "color","red"] );
     MarkPt( sum(pts)/len(pts), Label=false, color="blue");
     R = roundness(pts);
     echo(R=R);
     Blue() Text( R, site= [ X*100-Y/2+Y*3, x*X+X-Y/2+Y*3, 5*Y], halign="center",scale=0.3 );
   }

   for(i=range(7)) Obj(i*2);
}
//demo_roundness_3pts();

module demo_roundness_3pts_2()
{
   echom("demo_roundness_3pts_2()");

   triaPts = [ X*2,O, [1, sqrt(3),0]];
   echo(triaPts = triaPts);
   module Obj(pts= triaPts, x=0)
   {
     //echo(pqr=[X,O,Y]);
     echo(pts=pts);
     echo(pts2=pts2);
     pts2= [for(p=pts)p+X*x];
     MarkPts( pts2, Ball=0.05, Line=["closed",1, "r",0.02, "color","red"] );
     MarkPt( sum(pts2)/len(pts2), Label=false, color="blue");
     R = roundness(pts2);
     echo(R=R);
     Blue() Text( R
     , site= [ X*100-Y/2, x*X-Y/2+X, Y], halign="center",scale=0.3 );
   }

   Line( [Y*sqrt(3), X*15+Y*sqrt(3)] );
   for(i=[0:6]) Line( [X*i*2+X, X*i*2+X+Y*2.5] );
   Obj();
   Obj(pts = [triaPts[0], triaPts[1], triaPts[2]+0.1*X], x=2);
   Obj(pts = [triaPts[0], triaPts[1], triaPts[2]+0.2*X], x=4);
   Obj(pts = [triaPts[0], triaPts[1], triaPts[2]+0.4*X], x=6);
   Obj(pts = [triaPts[0], triaPts[1], triaPts[2]+0.8*X], x=8);
   Obj(pts = [triaPts[0], triaPts[1], triaPts[2]+0.1*Y], x=10);
   Obj(pts = [triaPts[0], triaPts[1], triaPts[2]-0.1*Y], x=12);

}
//demo_roundness_3pts_2();

module demo_roundness_3pts_map()
{
   echom("demo_roundness_3pts_map()");

   size = 4; // triangle size
   
   
   T = [size/2, size/2*sqrt(3),0]; // top pt 
   triaPts = [ X*size,O, T];
   echo(triaPts = triaPts);
   module Obj(pts= triaPts, x=0)
   {
     //echo(pqr=[X,O,Y]);
     echo(pts=pts);
     echo(pts2=pts2);
     pts2= [for(p=pts)p+X*x];
     MarkPts( pts2, Ball=0.05, Line=["closed",1, "r",0.04, "color","red"] );
     MarkPt( sum(pts2)/len(pts2), Label=false, color="blue");
     R = roundness(pts2);
     echo(R=R);
     Blue() Text( R, site= [ X*100-Y/2, x*X-Y/2+size/2*X, Y], halign="center",scale=0.3 );
   }

    /*
      map = square
                         
             +-----------------+ map_TR
             |                 |
             |        T        |
             |       / \       |
             |      /   \      |
             |     /     \     |
             |    /  obj  \    |
      map_LL +---/---------\---+
                +-----------+   
   
   
   */
   map_LL = -1*X-1*Y;
   map_TR = [ size+1, size+1,0];
   
   xlen = (map_TR-map_LL).x;
   ylen = (map_TR-map_LL).y;
   nx = 16; //xlen/map_intv;
   ny = 16; //ylen/map_intv;
   dx = xlen/nx;
   dy = ylen/ny;
   
   translate([0,-ylen,0])
   {
     Obj();
  
     //Line( [ [map_TR.x, map_LL.y,0], map_LL
     //    , [map_LL.x, map_TR.y,0], map_TR ], closed=1); 
   
     for(yi= range(ny))
       for(xi= range(nx) )
       {
           block=[ map_LL+[ dx*xi,dy*yi,0]
                 , map_LL+[ dx*xi,dy*(yi+1),0]
                 , map_LL+[ dx*(xi+1),dy*(yi+1),0]
                 , map_LL+[ dx*(xi+1),dy*yi,0]
                 ];
           bC = sum(block)/4;
           Line( block , closed=1);
           pts = [triaPts[0], triaPts[1], bC ];      
           R = roundness(pts);
           col = R>=0.9?"black":R>=0.7?"navy":R>=0.5?"blue":R<0?"red":"darkkhaki";
           echo( xi=xi, yi=yi//, block=block
                , bc=bC
               , R=_color(R,col));
           Plane(block, color=[R<0?"red":col, abs(R)] );
         
       }
   }    
}
//demo_roundness_3pts_map();


module demo_roundness_4pts()
{
   echom("demo_roundness_4pts()");

   module Obj(x=0)
   {
     _pts = randConvexPlane(n=4, site=ORIGIN_SITE); //randPts(4,z=0);
     xsize = maxx(_pts)-minx(_pts);
     __pts=[ for(p=_pts) p*2/xsize ];
     minx= minx(__pts);
     miny = miny(__pts);
     pts = [for(p=__pts)p-[minx-x,miny,0]+Y*3];
     MarkPts( pts, Ball=0, Line=["closed",1, "r",0.02, "color","red"] );
     MarkPt( sum(pts)/len(pts), Label=false, color="blue");
     R = roundness(pts);
     //echo(R=R);
     Blue() Text( R, site= [ X*100-Y/2+Y*3, x*X+X-Y/2+Y*3, 5*Y], halign="center",scale=0.3 );
   }

   for(i=range(7)) Obj(i*2);
}
//demo_roundness_4pts();

module demo_roundness_4pts_2()
{
   echom("demo_roundness_4pts_2()");

   pts = [X*2,O,Y*2,X*2+Y*2]; //randConvexPlane(n=4, site=ORIGIN_SITE);;
   xspacer=0.4;
   echo(pts = pts);
   module Obj(pts= pts, x=0)
   {
     //echo(pqr=[X,O,Y]);
     echo(pts=pts);
     echo(pts2=pts2);
     pts2= [for(p=pts)p+X*x];
     MarkPts( pts2, Ball=0.05, Line=["closed",1, "r",0.02, "color","red"] );
     MarkPt( sum(pts2)/len(pts2), Label=false, color="blue");
     R = roundness(pts2);
     echo(R=R);
     Blue() Text( R
     , site= [ X*100-Y/2, x*X-Y/2+X, Y], halign="center",scale=0.3 );
     Line( [pts2[0], pts2[1],pts2[2], pts2[0]+[0,pts2[2].y,0]], closed=1);
   }

   Obj();
   Obj(pts = [pts[0], pts[1], pts[2], pts[3]+0.1*X], x=2+xspacer*1);
   Obj(pts = [pts[0], pts[1], pts[2], pts[3]+0.2*X], x=4+xspacer*2);
   Obj(pts = [pts[0], pts[1], pts[2], pts[3]+0.4*X], x=6+xspacer*3);
   Obj(pts = [pts[0], pts[1], pts[2], pts[3]+0.8*X], x=8+xspacer*4);
   Obj(pts = [pts[0], pts[1], pts[2], pts[3]+0.1*Y], x=10+xspacer*5);
   Obj(pts = [pts[0], pts[1], pts[2], pts[3]-0.1*Y], x=12+xspacer*6);

}
//demo_roundness_4pts_2();


module demo_roundness_4pts_map()
{
   echom("demo_roundness_4pts_map()");

   size = 4; // triangle size
   
   
   TR = [size, size,0]; // top right pt 
   pts = [ X*size,O, size*Y, TR];
   echo(pts = pts);
   module Obj(pts= pts, x=0)
   {
     //echo(pqr=[X,O,Y]);
     echo(pts=pts);
     echo(pts2=pts2);
     pts2= [for(p=pts)p+X*x];
     MarkPts( pts2, Ball=0.05, Line=["closed",1, "r",0.04, "color","red"] );
     MarkPt( sum(pts2)/len(pts2), Label=false, color="blue");
     R = roundness(pts2);
     echo(R=R);
     Blue() Text( R, site= [ X*100-Y/2, x*X-Y/2+size/2*X, Y], halign="center",scale=0.3 );
   }

    /* The following graph is for demo_roundness_3pts_map():
    
      map = square
                         
             +-----------------+ map_TR
             |                 |
             |        T        |
             |       / \       |
             |      /   \      |
             |     /     \     |
             |    /  obj  \    |
      map_LL +---/---------\---+
                +-----------+   
   
   
   */
   map_LL = -2*X-2*Y;
   map_TR = [ size+2, size+2,0];
   
   xlen = (map_TR-map_LL).x;
   ylen = (map_TR-map_LL).y;
   nx = 16; //xlen/map_intv;
   ny = 16; //ylen/map_intv;
   dx = xlen/nx;
   dy = ylen/ny;
   
   translate([0,-ylen,0])
   {
     Obj();
  
     //Line( [ [map_TR.x, map_LL.y,0], map_LL
     //    , [map_LL.x, map_TR.y,0], map_TR ], closed=1); 
   
     for(yi= range(ny))
       for(xi= range(nx) )
       {
           block=[ map_LL+[ dx*xi,dy*yi,0]
                 , map_LL+[ dx*xi,dy*(yi+1),0]
                 , map_LL+[ dx*(xi+1),dy*(yi+1),0]
                 , map_LL+[ dx*(xi+1),dy*yi,0]
                 ];
           bC = sum(block)/4;
           Line( block , closed=1);
           _pts = [pts[0], pts[1], pts[2], bC ];      
           R = roundness(_pts);
           col = R>=0.9?"black"
                 :R>=0.7?"navy"
                 :R>=0.5?"blue"
                 :is0(R)?"gray"
                 :R<0?"red"
                 :"darkkhaki";
           echo( xi=xi, yi=yi//, block=block
                , bc=bC
               , R=_color(R,col));
           Plane(block, color=[R<0?"red":col, abs(R)] );
         
       }
   }    
}
//demo_roundness_4pts_map();


module demo_roundness_4pts_rect()
{
   echom("demo_roundness_4pts_rect()");

   //pts = [X*2,O,Y*2,X*2+Y*2]; //randConvexPlane(n=4, site=ORIGIN_SITE);;
   xspacer=0.4;
   //echo(pts = pts);
   module Obj(pts, x=0)
   {
     //echo(pqr=[X,O,Y]);
     echo(pts=pts);
     echo(pts2=pts2);
     pts2= [for(p=pts)p+X*x];
     MarkPts( pts2, Ball=0.05, Line=["closed",1, "r",0.02, "color","red"] );
     MarkPt( sum(pts2)/len(pts2), Label=false, color="blue");
     R = roundness(pts2);
     echo(R=R);
     Blue() Text( R
     , site= [ X*100-Y/2, x*X-Y/2+X, Y], halign="center",scale=0.3 );
     Line( [pts2[0], pts2[1],pts2[2], pts2[0]+[0,pts2[2].y,0]], closed=1);
   }

   T= 2*X+3*Y;
   w=0.1; l=.8;
   Obj(pts = [2*X, O, 3*Y, T], x=0+xspacer*0);
   Obj(pts = [2*X, O, 3*Y, T-w*X, T-w*X-l*Y, T-l*Y], x=2+xspacer*1);
   Obj(pts = [2*X, O, 3*Y, T-l*X, T-l*X-w*Y, T-w*Y], x=4+xspacer*2);
   Obj(pts = [2*X, O, 3*Y, T-2*w*X, T-2*w*X-2*l*Y, T-2*l*Y], x=6+xspacer*3);
   Obj(pts = [2*X, O, 3*Y, T-2*l*X, T-2*l*X-2*w*Y, T-2*w*Y], x=8+xspacer*4);
   
   //Obj(pts = [pts[0], pts[1], pts[2], pts[3]+0.2*X], x=4+xspacer*2);
   //Obj(pts = [pts[0], pts[1], pts[2], pts[3]+0.4*X], x=6+xspacer*3);
   //Obj(pts = [pts[0], pts[1], pts[2], pts[3]+0.8*X], x=8+xspacer*4);
   //Obj(pts = [pts[0], pts[1], pts[2], pts[3]+0.1*Y], x=10+xspacer*5);
   //Obj(pts = [pts[0], pts[1], pts[2], pts[3]-0.1*Y], x=12+xspacer*6);

}
//demo_roundness_4pts_rect();




module roundness_test()
{
    pts = [-X,X*3,X,X*2];// randPts(3, x=0); //pqr();
    _pts = [X*10,O,Y*5, X*10+Y*5];


    xx=5; 
    yy=1;
    echo(xx=xx);
    //pts = [X*10, X*2, Y*1+X*2, Y*1, Y*5, X*10+Y*5]; // 2.75
    pts = 
          //let(xx=4,yy=.5) // 0.188 
          //let(xx=4,yy=1) // 0.158 
          //let(xx=.5,yy=4) // 0.501 
          //let(xx=1,yy=4) // 0.507 
          //let(xx=1.5,yy=4) // 0.499 
          //let(xx=1,yy=.5) // .477
          let(xx=.11,yy=4) // .488
          //let(xx=2,yy=1) // .394
          //let(xx=4,yy=2) // .104
          //let(xx=6,yy=3) // .192
          //let(xx=8,yy=4) // .57
          //let(xx=0,yy=0) // .57
          [X*10, X*xx, Y*yy+X*xx, Y*yy, Y*5, X*10+Y*5]; 
          //[X*10, O, Y*5, X*10+Y*5];  // 1 

    C= sum(pts)/len(pts);
    N = N([pts[0],C,pts[1]]);
    MarkPts(pts, Line=["closed",1]);
    MarkPt(C,"C");
    MarkPt(N,"N");

    //for(p=pts) Dim([p,C,N] );
    echo(roundness = roundness(pts) );

    //pts = randPts(5);
    //echo(pts=pts);
    //echo(pts_ = roundPts(pts[0],3));
    //echo(pts0= roundPts(pts,0) );
    //echo(pts1= roundPts(pts,1) );
    //echo(pts2= roundPts(pts,2) );
    //echo(pts2= roundPts(pts,8) );
}
