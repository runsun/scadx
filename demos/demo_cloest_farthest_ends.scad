include <../scadx.scad>


module demo_sortByDist() 
{
  echom("demo_sortByDist()");
  
  pts = randPts(1000);
  Rf = centroidPt(pts);
  MarkPt(Rf, color="black", r=0.1);
  //MarkPts(pts, Line=0);
  //Line(Rf, color="black", r=0.02);
  
  sortedInfo= sortByDist(pts, Rf);
  for( i= range(sortedInfo)) 
  {
    info = sortedInfo[i];
    d_ratio= info[2]/last(sortedInfo)[2];
    MarkPt( info[1], Label=false, color=[ d_ratio<=0.33?"red"
                                        : d_ratio<=0.66? "green"
                                        : "blue"
                                        , 1- d_ratio] );
  }

}

//demo_sortByDist();

module demo_sortByDist_Rf_line() 
{
  echom("demo_sortByDist_Rf_line()");
  
  pts = randPts(1000);
  C = centroidPt(pts);
  Rf = [C+3*X+3*Y, C-3*X-3*Y];
  //MarkPt(Rf, color="black", r=0.1);
  //MarkPts(pts, Line=0);
  Line(Rf, color="black", r=0.02);
  
  sortedInfo= sortByDist(pts, Rf);
  for( i= range(sortedInfo)) 
  {
    info = sortedInfo[i];
    d_ratio= info[2]/last(sortedInfo)[2];
    MarkPt( info[1], Label=false, color=[ d_ratio<=0.3?"red"
                                        : d_ratio<=0.6? "green"
                                        : "blue"
                                        , 1- d_ratio] );
                                        
  }
}

//demo_sortByDist_Rf_line();

module demo_sortByDist_Rf_plane() 
{
  echom("demo_sortByDist_Rf_plane()");
  _red("Rf=a plane. Note that a plane is defined as a 3-pts, and dist could be negative");
  _red("So we need to set isAbs=1 to get the abs(dist)");
  
  pts = randPts(500);
  C = centroidPt(pts);
  Rf = [C+3*X+3*Y, C-3*X+3*Y, C-3*X-3*Y]; //, C+3*X-3*Y];
  MarkPt(C, color="black", r=0.1);
  //MarkPts(pts, Line=0);
  Plane( concat( Rf, [C+3*X-3*Y]), color="black", r=0.02);
  
  sortedInfo= sortByDist(pts, Rf, isAbs=1);
  //echo(sortedInfo);
  for( i= range(sortedInfo)) 
  {
    info = sortedInfo[i];
    d_ratio= info[2]/last(sortedInfo)[2];
    MarkPt( info[1], Label=false, color=[ d_ratio<=0.3?"red"
                                        : d_ratio<=0.6? "green"
                                        : "blue"
                                        , 1- d_ratio] );
  }
}

//demo_sortByDist_Rf_plane();



module demo_closestPtInfo_farthestPtInfo() 
{
  echom("demo_closestPtInfo_farthestPtInfo()");
  
  pts = randPts(30);
  Rf = randPt();
  MarkPts(pts, Line=0);
  MarkPt(Rf, color="black", r=0.2);
  
  cptinfo= closestPtInfo(pts, Rf);
  echo(cptinfo= cptinfo);
  Red(0.3)MarkPt( cptinfo[1], cptinfo[0], r=0.2 );
  Red() Line( [cptinfo[1], Rf]);
  
  fptinfo= farthestPtInfo(pts, Rf);
  echo(fptinfo= fptinfo);
  Blue(0.3) MarkPt( fptinfo[1], fptinfo[0], r=0.2 );
  Blue() Line( [fptinfo[1], Rf]);
  
  
}

//demo_closestPtInfo_farthestPtInfo();

module demo_closestPtInfo_farthestPtInfo_Rf_line() 
{
  echom("demo_closestPtInfo_farthestPtInfo_Rf_line()");
  
  pts = randPts(30);
  Rf = [randPt(), randPt()];
  MarkPts(pts, Line=0);
  Line(Rf, color="black", r=0.05);
  
  cptinfo= closestPtInfo(pts, Rf);
  echo(cptinfo= cptinfo);
  Red(0.3)MarkPt( cptinfo[1], cptinfo[0], r=0.2 );
  Red() Line( [cptinfo[1], projPt(cptinfo[1], Rf)]);
  
  fptinfo= farthestPtInfo(pts, Rf);
  echo(fptinfo= fptinfo);
  Blue(0.3) MarkPt( fptinfo[1], fptinfo[0], r=0.2 );
  Blue() Line( [fptinfo[1],  projPt(fptinfo[1], Rf)]);
}

//demo_closestPtInfo_farthestPtInfo_Rf_line();


module demo_closestPtInfo_farthestPtInfo_Rf_plane() 
{
  echom("demo_closestPtInfo_farthestPtInfo_Rf_plane()");
  
  pts = randPts(30);
  Rf = expandPts([randPt(), randPt(), randPt()]);
  MarkPts(pts, Line=0);
  Plane(Rf);
  
  cptinfo= closestPtInfo(pts, Rf);
  echo(cptinfo= cptinfo);
  Red(0.3)MarkPt( cptinfo[1], cptinfo[0], r=0.2 );
  Red() Line( [cptinfo[1], projPt(cptinfo[1], Rf)]);
  
  fptinfo= farthestPtInfo(pts, Rf);
  echo(fptinfo= fptinfo);
  Blue(0.3) MarkPt( fptinfo[1], fptinfo[0], r=0.2 );
  Blue() Line( [fptinfo[1],  projPt(fptinfo[1], Rf)]);
}

//demo_closestPtInfo_farthestPtInfo_Rf_plane();

module demo_endPtsInfoAlongLine() 
{
  echom("demo_endPtsInfoAlongLine()");
  
  pts = randPts(100);
  Rf = [randPt(), randPt()];
  MarkPts(pts, Line=0);
  Line(Rf, color="black", r=0.02);
  
  2pts = endPtsInfoAlongLine(pts, Rf);
  echo(2pts=2pts);
  
  P0= 2pts[0][1];
  J0= projPt(2pts[0][1], Rf);
  Red(0.3)MarkPt( P0, 2pts[0][0], r=0.2 );
  Red() Line( [P0, J0]);
  
  P1= 2pts[1][1];
  J1= projPt(2pts[1][1], Rf);
  Blue(0.3) MarkPt( P1, 2pts[1][0], r=0.2 );
  Blue() Line( [P1,  J1]);
  
  Line([J0,J1], color="black", r=0.005);
  
  plane0=[ onlinePt([J0,P0], len=4)
         , N( [P0,J0,J1], len=4 )
         , onlinePt([J0,P0], len=-4)
         , N( [P0,J0,J1], len=-4 )
         ];
  Red(0.2)Plane(plane0);       

  plane1=[ onlinePt([J1,P1], len=4)
         , N( [P1,J1,J0], len=4 )
         , onlinePt([J1,P1], len=-4)
         , N( [P1,J1,J0], len=-4 )
         ];
  Blue(0.2) Plane(plane1);       

}

demo_endPtsInfoAlongLine();

