/*
  Demo the mirrorPt()/mirrorPts() function

*/

include <../scadx.scad>

//mirrorPt_demo_pqr();
//mirrorPt_demo_normalLine();
//mirrorPt_demo_pt();
mirrorPts_demo_pqr();


//. mirrorPt

module mirrorPt_demo_pqr()
{
  echom("mirrorPt_demo_pqr");
  echo(_blue("mp = mirrorPt( pt, pqr )"));

  pqr = pqr();
  pt = randPt();

  mp = mirrorPt( pt, pqr );
  J = projPt( pt, pqr );
  echo( mp = mp );

  MarkPts(pqr, Label="PQR");
  Plane(pqr, color=["blue", .1]);
  MarkPts( [pt,  J], Label=["text",["pt","J"]]);
  MarkPt( mp , Grid=true, Label="mp"); 

  for(p=pqr) Mark90( [ p, J, pt] ); 
  
  Line( [pt, J, mp] );
}
//mirrorPt_demo_pqr();

module mirrorPt_demo_normalLine()
{
  echom("mirrorPt_demo_normalLine()");
  echo(_blue("mp = mirrorPt( pt, pq ) ===> create a plane on P for mirroring"));

  pq = randPts(2,d=2);
  pt = randPt();

  pl = expandPts( getPlaneByNormalLine(pq), 1);
  Plane(pl, mode=1, color=["olive",0.1]);
  echo(pl=pl);
    
  mp = mirrorPt( pt, pq );
  J = projPt( pt, pl );
  echo( mp = mp );

  //color("red") Line(pq);
  
  MarkPts( pq,Label=["text","PQ"]);
    
  MarkPts( [pt, J, mp], Label=["text",["pt","J","mp"]] );
  
  for(p=pl) Mark90( [ p, J, pt] ); 
 
}
//mirrorPt_demo_normalLine();
  
module mirrorPt_demo_pt()
{
  echom("mirrorPt_demo_pt()");
  echo(_blue("mp = mirrorPt( pt, pt2 )"));
  echo(_blue("where pt2 is the normal vector of a plane intersecting 
             the origin through which to mirror the pt, like the openSCAD mirror() did"));
    

  pt = randPt(d=[1,2]);
  pt2 = randPt(d=[1,2]);  
  //MarkPts([pt,pt2], Label=["text", ["pt","pt2"]] );
  pq = [ORIGIN,pt2];
  pl = expandPts( getPlaneByNormalLine( [ORIGIN, pt2]), 1);
  Plane(pl, mode=1, color=["olive",0.1]);
  echo(pl=pl);
    
  mp = mirrorPt( pt, pt2 );
    
  J = projPt( pt, pl );
  echo( mp = mp );

  //color("red") Line(pq);
  
  MarkPts( pq, Label=["text",["","pt2"]]);
    
  MarkPts( [pt,  J, mp], Label=["text",["pt","J", "mp"]] );

  for(p=pl) Mark90( [ p, J, pt] ); 
 
}
//mirrorPt_demo_pt();

//. mirrorPts

module mirrorPts_demo_pqr()
{
  echom("mirrorPts_demo_pqr");
  echo(_blue("mp = mirrorPts( pts, pqr )"));

  mirror = expandPts(pqr(),1);
  Plane(mirror, mode=1, color=["olive",0.2]);
  MarkPts(mirror, Label="PQR");
  
  pts0 = pqr();
  pts1 = mirrorPts( pts0, mirror );
  
  MarkPts(pts0, color="red");  
  MarkPts(pts1, color="green");  
}
//mirrorPts_demo_pqr();

module mirrorPts_demo_normalLine()
{
  echom("mirrorPts_demo_normalLine()");
  echo(_blue("mp = mirrorPts( pts, pq )"));

  normLine = randPts(2);
  pts = pqr();

  pl = expandPts( getPlaneByNormalLine(normLine), 1);
  echo(pl=pl);
    
  mps = mirrorPts( pts, normLine );
  
  MarkPts( normLine, Label="PQ");
  MarkPts( pts, color="red");
  MarkPts( mps, color="green");
    
  Plane(pl, mode=1, color=["olive",0.2]);
  
}
//mirrorPts_demo_normalLine();
  
module mirrorPts_demo_pt()
{
  echom("mirrorPts_demo_pt()");
  echo(_blue("mp = mirrorPts( pts, pt2 )"));
  echo(_blue("where V is the normal vector of a plane intersecting 
             the origin through which to mirror the pt, like the openSCAD mirror() did"));
    

  pts= pqr();
  V = randPt();  
  OV = [ORIGIN,V];
  MarkPts( OV, "OV");
  
  pl = expandPts( getPlaneByNormalLine( OV ), 1);  
    
  mps = mirrorPts( pts, V );
  MarkPts( pts, color="red"); 
  MarkPts( mps, color="green"); 
  
  Plane(pl, mode=1, color=["olive",0.2]);
  
}
//mirrorPts_demo_pt();
