/*
  Demo the randCoordPt()/Coord() function

*/

include <../scadx.scad>

module demo_subObjArg()
{
   echom("demo_subObjArg()");
   echo(_span( _color("In the following outputs, <b>frame</b> is the given arg."
                     , "darkgreen")
             , s="font-size:18px"
             )        
       );
   echo(_span( _color("It is processed by <b>subObjArg()</b> into <b>_frame</b> and sent to Frame()"
                     , "darkgreen")
             , s="font-size:18px"
             )        
       );
   echo("");
       
   module m0(frame,opt=[],marker_color, canDisable, debug)
   {
       
      _frame = subObjArg( arg = frame 
                       , opt = opt   // opt is a hash that may have ["frame", <frame_setting> ]
                       , argname = "frame" // key to opt to get the <frame_setting> outta opt
                       
                       , dfArgName = "r"   // when the type of plain frame (the arg here) = dfArgType,
                       , dfArgType = "num" // the arg given is converted to [dfArgName, arg]
                       
                       , canDisable = canDisable
                       , debug=false
                       );
                       
      pts = cubePts([1,1.5,1]);
      echo( debug=debug, _color( _s("frame={_}, opt={_}, _frame={_}", [frame,opt, _frame]), marker_color));
      //echo(_frame=_frame);
      
      //echo( _fmth(_frame));
      
      if(_frame!=false) Frame(pts, opt=_frame);
      MarkPts(pts, opt=["color",marker_color, "r",0.075] );
   }
   
   module m1(frame,opt=[],marker_color,debug){ m0(frame,opt,marker_color,debug);}
   module m2(frame,opt=[],marker_color,debug){ m0(frame=frame,opt=opt,marker_color=marker_color
                                                 , canDisable=true,debug=debug);}
   
   m1(); // frame not set, no frame
   
   echo(_red(_b("<br/>objs w/ red dots: set frame directly<br/>")));
   
   translate([2,0,0]) m1( frame=true, marker_color="red" ); // frame=true, use default in Frame()  
   translate([4,0,0]) m1( frame=0.03, marker_color="red" ); // frame=0.05, set frame.r 'cos dfArgName="r" and dfArgType="num"
   translate([4,2,0]) m1( frame="red", marker_color="red" ); // Set frame="red" won't make it red, 'cos dfArgType="num"  
   translate([6,0,0]) m1( frame=["color","red"], marker_color="red" );   
   translate([8,0,0]) m1( frame=["color","green","r",0.03], marker_color="red" );   
   
   echo(_green(_b("<br/>objs w/ green dots: set opt.frame<br/>")));
   
   translate([0,0,-2]) m1( opt=["frame", true ], marker_color="green", debug=true );
   translate([0,0,-4]) m1( opt=["frame", 0.03 ], marker_color="green" );
   translate([0,2,-4]) m1( opt=["frame", "red" ], marker_color="green" );
   translate([0,0,-6]) m1( opt=["frame", ["color","red"] ], marker_color="green" );
   translate([0,0,-8]) m1( opt=["frame", ["color","green","r",0.03] ], marker_color="green" );
   
   echo(_blue(_b("<br/>objs w/ blue dots: have plain.frame and opt.frame<br/>")));
   
   translate([-2,0,0]) m1( frame=0.03, opt=["frame", ["color","blue"] ], marker_color="blue" );
   translate([-4,0,0]) m1( frame=false, opt=["frame", ["color","blue"] ], marker_color="blue" );
   translate([-6,0,0]) m1( frame=true, opt=["frame", ["color","blue"] ], marker_color="blue" );
   
   echo(_olive("<br/>Next we use m2() that has the canDisable set to true, means if frame=false, it will be shut down<br/>"));
   
   translate([-8,0,0]) m2( frame=false, opt=["frame", ["color","blue"] ], marker_color="olive" );
   translate([-10,0,0]) m2( frame=true, opt=["frame", ["color","blue"] ], marker_color="olive" );
   translate([-12,0,0]) m2( frame=0.03, opt=["frame", ["color","blue"] ], marker_color="olive" );

}

demo_subObjArg();