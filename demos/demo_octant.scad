include <../scadx.scad>

ISLOG=0;

module demo_groupByOctant()
{
  echom("demo_groupByOctant()");
  pts = randPts(30);
  //pts = [[-0.24, 2.44, 1.51], [-0.74, -2.03, 2.2], [2.39, 0.87, 2.11], [-0.05, 0.43, 1.92], [-1.11, -2.97, 2.54], [2.67, -1.63, -1.75], [-1.73, 2.72, 2.5], [0.57, 2.45, -1.7], [0.85, 0.11, -2.95], [1.23, -0.96, -0.21], [2.79, 0.36, -2.71], [-2, -2.2, -2.42], [-0.5, 2.8, 1.76], [2.78, 1.26, 0.75], [-2.73, -1.59, -0.04], [-2.77, 0.22, -1.75]];
  Red() MarkPts(pts, Line=0, Grid=0);
  
  octpts = groupByOctant( pts); //, centroidPt(pts) );
  //echo(octpts = octpts); 
  
  for( i = range(octpts) )
  {
     color( COLORS[i])//, 0.4)
     MarkPts( octpts[i], Line=["closed",1], Label=0, Ball=0.1 );
  }

  echo( oct = octant( [0.85, 0.11, -2.95]) );
}
//demo_groupByOctant(); 
 
module demo_groupByOctant2()
{
  echom("demo_groupByOctant2()");
  pts = randPts(100);
  //pts = [[-0.24, 2.44, 1.51], [-0.74, -2.03, 2.2], [2.39, 0.87, 2.11], [-0.05, 0.43, 1.92], [-1.11, -2.97, 2.54], [2.67, -1.63, -1.75], [-1.73, 2.72, 2.5], [0.57, 2.45, -1.7], [0.85, 0.11, -2.95], [1.23, -0.96, -0.21], [2.79, 0.36, -2.71], [-2, -2.2, -2.42], [-0.5, 2.8, 1.76], [2.78, 1.26, 0.75], [-2.73, -1.59, -0.04], [-2.77, 0.22, -1.75]];
  //Red() MarkPts(pts, Line=0, Grid=0);
  
  octpts = groupByOctant( pts); //, centroidPt(pts) );
  //echo(octpts = octpts); 
  Black() MarkPt( centroidPt( pts), Ball=0.1 );
  
  for( i = range(octpts) )
  {
     C = centroidPt(octpts[i]);
     color( COLORS[i])//, 0.4)
     { MarkPts( octpts[i], Line=["r",0.002,"color",[COLORS[i],0.5]]
               , Label=0, Ball=0.05 );
      MarkPt(C, Ball=0.15);
     } 
  }

  echo( oct = octant( [0.85, 0.11, -2.95]) );
}
//demo_groupByOctant2();  

module demo_groupByOctant_to_chk_shape()
{
  echom("demo_groupByOctant_to_chk_shape()");
  pq = randPts(2);
  
  _pts = [ for(i=range(10)) each 
                [for(j=[0:2]) 
                onlinePt( pq, len= dist(pq)/10*i) + randPt(d=.8)
                ]
        ];
  _C= centroidPt(_pts);
  
  pts = transPts( _pts, actions=["t",-1*_C] );
  C = centroidPt(pts);
  
  //Line( axis, r=0.1, color="black");
  
  octpts = groupByOctant( pts ); 
  //echo(octpts = octpts); 
  Black() MarkPt( C, Ball=0.1 );

  ijs =[[0,6],[1,7],[2,4],[3,5]]; 
  
  centroidPts = [ for(o=octpts) centroidPt(o) ];
  echo( centroidPts = centroidPts );
  
  // Connect two centroidPts on the opposite octant:
  _dualMassLines = [for(ij=ijs) get(centroidPts, ij) ];
  
  // Move them to pass thru C:
  dualMassLines= [ for(line=_dualMassLines) 
                     //let(I = intsec(line, projPt(line,C)) )
                     let(I = projPt(line,C) ) 
                     echo(I=I)
                     I?[ line[0]-I+C, line[1]-I+C]:line  
                 ];

  dists= sortArrs(
         [ for(ij=[[0,6],[1,7],[2,4],[3,5]])
           [ dist( get(centroidPts,ij)), ij ]
         ], by=0);  
  echo(dists = dists);       
  
  for( i = range(ijs) )
    Line( dualMassLines[i], r=0.05, color=COLORS[i+1]);
  
  
  
  for( i = range(octpts) )
  {
     C = centroidPts[i];
     color( COLORS[i])//, 0.4)
     { MarkPts( octpts[i], Line=["r",0.002,"color",[COLORS[i],0.5]]
               , Label=0, Ball=0.05 );
      MarkPt(C, Ball=0.1);
     } 
  }

  //Gold(0.2) 
  //Line( [pq[0]-_C, pq[1]-_C], r=0.3 , $fn=16);
    
  echo( oct = octant( [0.85, 0.11, -2.95]) );
}
demo_groupByOctant_to_chk_shape();  
