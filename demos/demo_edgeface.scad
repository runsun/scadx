include <../scadx.scad>
include <../dev/scadx_edgeface.scad>

module Draw_this(pts, ratio=0.5, color="gold")
{
  pts2= DooSabin_face_shrink(pts, ratio);
  MarkPts(pts,Line=["closed",true]);
  MarkPts(pts2,Line=["closed",true, "r", 0.03,"color", color]); 
}

module Draw_this_2(pts, ratio=0.5, markpts1=[], markpts2=[])
{
  pts2= DooSabin_face_shrink(pts, ratio);
  Line(pts,closed=true); //,opt=markpts1);
  Line(pts2,closed=true); //,opt=markpts2); 
}

module demo_DooSabin_face_shrink()
{
   echom("demo_DooSabin_face_shrink()");
   Draw_this( circlePts(), color="red");   
   
   Draw_this( [ORIGIN, [0,2,2], [2,3,0], [3,2,0], [2.5,0,2] ]
            , ratio=0.3, color="green");

}
//demo_DooSabin_face_shrink();

module demo_DooSabin_face_shrink_art()
{
   echom("demo_DooSabin_face_shrink_art()");
   pts=[ORIGIN, [0,2,2], [2,3,0], [3,2,0], [2.5,0,2] ];
   
   Line(pts, closed=true, r=0.03, color="brown");
   Line(DooSabin_face_shrink(pts, ratio=0.1), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=0.2), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=0.3), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=0.4), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=0.5), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=0.6), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=0.7), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=0.8), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=0.9), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=1.0), closed=true);
   Line(DooSabin_face_shrink(pts, ratio=1.1), closed=true);
   
}
//demo_DooSabin_face_shrink_art();

module demo_DooSabin_face_shrink_art2()
{
   echom("demo_DooSabin_face_shrink_art2()");
   pts=[ORIGIN, [0,2,2], [2,3,0], [3,2,0], [2.5,0,2] ];
   
   Line(pts, closed=true, r=0.03, color="brown");
   dratio = 0.2;
   pts1=DooSabin_face_shrink(pts, ratio=dratio); 
   pts2=DooSabin_face_shrink(pts1, ratio=dratio) ;
   pts3=DooSabin_face_shrink(pts2, ratio=dratio); 
   pts4=DooSabin_face_shrink(pts3, ratio=dratio) ;
   pts5=DooSabin_face_shrink(pts4, ratio=dratio) ;
   pts6=DooSabin_face_shrink(pts5, ratio=dratio) ;
   pts7=DooSabin_face_shrink(pts6, ratio=dratio) ;
   pts8=DooSabin_face_shrink(pts7, ratio=dratio) ;
   pts9=DooSabin_face_shrink(pts8, ratio=dratio) ;
   pts10=DooSabin_face_shrink(pts9, ratio=dratio) ;
   //pts7=DooSabin_face_shrink(pts6, ratio=dratio) ;
   //pts7=DooSabin_face_shrink(pts6, ratio=dratio) ;
   Line(pts1, closed=true);
   Line(pts2, closed=true);
   Line(pts3, closed=true);
   Line(pts4, closed=true);
   Line(pts5, closed=true);
   Line(pts6, closed=true);
   Line(pts7, closed=true);
   Line(pts8, closed=true);
   Line(pts9, closed=true);
   Line(pts10, closed=true);
     
}
//demo_DooSabin_face_shrink_art2();

module demo_shrinkPts_on_cube()
{
   echom("demo_shrinkPts_on_cube()");
   pts = cubePts(3);
   faces = rodfaces(4);
   facePts = [for(f=faces)get(pts,f)];
   shrinkPts= [for(fp=facePts) shrinkPts(fp)];
   
   Frame(pts);
   for(sp=shrinkPts) MarkPts(sp,Line=["closed",true]);
   	
}
demo_shrinkPts_on_cube();