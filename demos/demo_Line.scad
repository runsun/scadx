include <../scadx.scad>

module Line_demo_default()
{
   Line( randPts(2) );
   Line( randPts(3), opt=["color", ["red",0.5] ] );
   Line( randPts(4), opt=["color",["green",0.5]]);
   Line( randPts(5), opt=["fn",16, "color","blue"]);
   
}
//Line_demo_default();

module Line_demo_closed()
{
   Line( randPts(3) );   
   Line( randPts(5),closed=true, opt=["color","red"]); 
   Line( randPts(4), opt=["color","green","closed",true]);
}
//Line_demo_closed();

module Line_demo_r()
{
   echo(_color("Line( randPts(3) );","darkkhaki"));
   Line( randPts(3) );  
  
   echo(_red( "Line( randPts(5), r=0.2)")); 
   color("red") Line( randPts(5), r=0.2);
  
   _echo(_green("Line( randPts(4), opt=['r',0.3, '$fn',12])"));
   color("green") Line( randPts(4), opt=["r",0.3, "$fn",12]);

   _echo(_color("Line( randPts(4), opt=['r',0.3, '$fn',36])", "teal"));
   color("teal") Line( randPts(4), opt=["r",0.3, "$fn",36, "Joint",true]);
   
}
//Line_demo_r();

module Line_demo_Joint()
{
   echom("Line_demo_Joint()");
   
   pqr = pqr();
   
   Line( pqr);   
   _echo("Line(pqr)", " // set join=true");
   echo("--------------");
   
   color("orange")Line( transN(pqr,1) , r=0.1, Joint=true, opt=["$fn",9]); 
   _echo(_orange("Line(..., Joint=true )"), " // set join=true");
   echo("--------------");
   
   color("red")Line( transN(pqr,2) , r=0.1, opt=["$fn",9, "Joint",true]); 
   _echo(_red("Line(..., opt=['Joint',true] )"), " // set op.join=true");
   echo("--------------");
   
   Line( transN(pqr,3), opt=["color","green", "Joint",0.2]);
   _echo(_color("Line(..., opt=['Joint',0.2] )","green")," // set Joint radius ");
   echo("--------------");
   
   Line( transN(pqr,4), opt=["color","blue", "Joint",0.2, "$fn",24]);
   _echo(_blue("Line(..., opt=['Joint',0.2, '$fn',24] )","blue")," // set fn ");
   echo("--------------");
   
   Line( transN(pqr,5), opt=["color","purple", "Joint",["color","blue", "r",0.1], "$fn",24]);
   _echo(_color("Line(..., opt=['Joint',['color','blue','r',0.1]], '$fn',24] )","purple")," // set Joint color and r");
   echo("--------------");
   
   Line( transN(pqr,6), closed=true
       , opt=["color","darkkhaki", "Joint",["color","red", "r",0.1], "$fn",24]);
   _echo(_color("Line(...closed=true, opt=['Joint',['color','red','r',0.1]], '$fn',24] )","darkkhaki")," // closed=true ");
   
}
//Line_demo_Joint();

//module Line_demo_Joint_cube()
//{
//   echom("Line_demo_Joint_cube()");
//   
//   pqr = squarePts(randPts(3, d=2)); //pqr();
//   
//   Line( pqr); MarkPts(pqr, Line=false);  
//   _echo("Line(pqr)", " // set join=true");
//   echo("--------------");
//   
//   color("orange")Line( transN(pqr,1) , r=0.1, Joint=["shape","cube"], opt=["$fn",9]); 
//   _echo(_orange("Line(..., Joint=true )"), " // set join=true");
//   echo("--------------");
//   
//   color("red")Line( transN(pqr,2) , r=0.1, opt=["$fn",4, "Joint",["shape","cube", "r", 0.3]]); 
//   _echo(_red("Line(..., opt=['Joint',true] )"), " // set op.join=true");
//   echo("--------------");
//   
//   Line( transN(pqr,3), r=0.2, opt=["color","green", "Joint",["shape","cube", "r", 0.2]]);
//   _echo(_color("Line(..., opt=['Joint',0.2] )","green")," // set Joint radius ");
//   echo("--------------");
//   
//   //Line( transN(pqr,4), opt=["color","blue", "Joint",0.2, "$fn",24]);
//   //_echo(_blue("Line(..., opt=['Joint',0.2, '$fn',24] )","blue")," // set fn ");
//   //echo("--------------");
//   //
//   //Line( transN(pqr,5), opt=["color","purple", "Joint",["color","blue", "r",0.1], "$fn",24]);
//   //_echo(_color("Line(..., opt=['Joint',['color','blue','r',0.1]], '$fn',24] )","purple")," // set Joint color and r");
//   //echo("--------------");
//   
//   Line( transN(pqr,4), closed=true
//       , opt=["color","darkkhaki", "Joint",["shape","cube","color","red", "r",0.3], "$fn",24]);
//   _echo(_color("Line(...closed=true, opt=['Joint',['color','red','r',0.1]], '$fn',24] )","darkkhaki")," // closed=true ");
//   
//}
//Line_demo_Joint_cube();


module Line_profiling()
{     
   echom("Line_profiling");
                        // preview / render
   //Line( randPts(50) ); // 1    /  13
   //Line( randPts(100) ); // 0
   //Line( randPts(200) ); // 1
   //Line( randPts(300) ); // 1
   //Line( randPts(400) ); // 1 
   //color("green") Line( randPts(400) ); // 1 
   //Line( randPts(1000), r=0.05, Joint=true ); // 1 
   //Line( randPts(2000), r=0.05, Joint=true ); // 2 
   //Line( randPts(400),Joint=["r",0.5,"fn",48] );  // 1
   
   //pts = randPts(500, d=3);
   //for(p=pts)MarkPt(p, color="blue", Grid=["mode",3, "color",["red",0.3]]);
}
//Line_profiling();

