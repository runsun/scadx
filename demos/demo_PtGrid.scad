include <../scadx.scad>


module demo_PtGrid()
{
    echom("demo_PtGrid");
    P=randPt(); 
    Q=randPt(); 
    R=randPt(); 
    
    PtGrid(P); 
    translate(P)sphere(0.1, $fn=12);
    
    PtGrid(Q, mode=1, color="red");
    translate(Q)sphere(0.1, $fn=12);

    PtGrid(R, mode=3, color="green");
    translate(R)sphere(0.1, $fn=12);
    
}
demo_PtGrid();

module demo_PtGrid_site()
{
    echom("demo_PtGrid_site");
    P=randPt(); 
    Q=randPt(); 
    R=randPt(); 
    
    site=pqr(); 
    site= [ [1.7944, -1.23497, -0.0215458]
          , [-1.37137, 1.53528, -2.02852]
          , [0.935001, -2.51079, -2.65121] ];
    Line(site); 
    LabelPt(site[0],"P", prespace=0.1);
    LabelPt(site[1],"Q", prespace=0.1);
    LabelPt(site[2],"R", prespace=0.1);
    
    PtGrid(P,site=site); 
    translate(P)sphere(0.1, $fn=12);
    
    PtGrid(Q, mode=1, color="red",site=site);
    color("red")translate(Q)sphere(0.1, $fn=12);

    PtGrid(R, mode=3, color="green",site=site);
    color("green")translate(R)sphere(0.1, $fn=12);
    
}
//demo_PtGrid_site();
