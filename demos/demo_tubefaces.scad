include <../scadx.scad>


//demo_tubefaces();
module demo_tubefaces()
{  
  echom("demo_tubefaces()");
  h=1;
  pts_b= [ O, 4*Y, 3*X+5*Y, 2*X ];
  pts_t= addz(pts_b, h);
  pts_hole= [ X+2.5*Y, X/2+3.5*Y, 2*X+3.5*Y];
  pts = concat(pts_b, pts_t, pts_hole, addz(pts_hole,h));
           
  Red()MarkPts(pts, Line=false);           
  
  faces = tubefaces(nO=len(pts_b), nI=len(pts_hole));
  echo(faces=faces);
  Gold(0.4)polyhedron(pts,faces);
}


demo_tubefaces2();
module demo_tubefaces2()
{  
  echom("demo_tubefaces2()");
  h=1;
  pts_b= [ O, 4*Y, 3*X+5*Y, 2*X ];
  pts_t= addz(pts_b, h);
  pts_hole2= [ X+2.5*Y, X/2+3.5*Y, 2*X+3.5*Y];
  
  pts_hole= [ X+.5*Y, X/2+1.5*Y, 2*X+1.5*Y];
  
  pts = concat(pts_b, pts_t
              , pts_hole
              , addz(pts_hole,h)
              
              , pts_hole2
              , addz(pts_hole2,h)
              
              );
           
  //Red()MarkPts(pts);           
  
  faces0 = tubefaces(nO=len(pts_b), nI=len(pts_hole), isflat=false);
//  faces0=  [ [4, 5, 6, 7, 4, 13, 12, 11, 13]
//           , [3, 2, 1, 0, 3, 8, 9, 10, 8]
//           , [0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]
//           , [9, 8, 11, 12], [10, 9, 12, 13], [8, 10, 13, 11] 
//           ];
  
  faces1= 
  [
    concat(faces0[0], [19,18,17,19] )
  , concat(faces0[1], [14,15,16,14] )
  , each faces0[2]    
  , each faces0[3]
  , [ 15,14,17,18 ],  [14,16,19,17], [16,15,18,19]
  ]; 
  
  faces0 = [ [4, 5, 6, 7, 4, 13, 12, 11, 13]
           , [3, 2, 1, 0, 3, 8, 9, 10, 8]
           , [[0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]]
           , [[9, 8, 11, 12], [10, 9, 12, 13], [8, 10, 13, 11]]
           ];
  faces1x = [[4, 5, 6, 7, 4, 13, 12, 11, 13, 19, 18, 17, 19]
            ,[3, 2, 1, 0, 3, 8, 9, 10, 8, 14, 15, 16, 14]
            , [0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]
            , [9, 8, 11, 12], [10, 9, 12, 13], [8, 10, 13, 11]
            , [15, 14, 17, 18], [14, 16, 19, 17], [16, 15, 18, 19]
            ];
  
  echo(faces0=faces0);
  echo(faces1=faces1);
  
  Gold(0.4)polyhedron(pts,faces1);
  
//  for(i = range(faces1))
//  {
//     if(i<2)
//     color(COLORS[i]) Plane( get(pts, faces1[i]) );
//  }
//  
}



