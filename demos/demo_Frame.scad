include <../scadx.scad>

module Frame_demo_default()
{
   pts1= cubePts( [3,2,1]);
   //echo(pts1=pts1);
   //MarkPts(pts1, Line=false);
   Frame( pts1 );

   pts2= [ for(ps= rodPts(opt=["r",1, "len",2, "nside", 5]) ) each ps ];
   echo(pts2=pts2);
   Frame( pts2, opt=["color","teal"] ); 

}
//Frame_demo_default();



//module Frame_demo_grid()
//{
//   pts= [ for(ps= rodPts(opt=["r",1, "len",0.2, "nside", 6]) ) each ps ];
//   Frame( pts, opt=["color","teal", "grid", false] );
//   
//   pts2= [ for(ps= rodPts(opt=["r",1, "len",0.5, "nside", 5]) ) each ps ];
//   Frame( pts2, opt=["color","red", "grid", ["r", 0.2,"color",["gold",0.5]]] );
//    
//   pts3= [ for(ps= rodPts(opt=["r",1, "len",1, "nside", 4]) ) each ps ];
//   Frame( pts3, opt=[ "r", 0.1
//                    , "color","teal"
//                    , "grid", ["r", 0.05,"color",["green",0.5]]
//                    ] );    
//}
//Frame_demo_grid();


module Frame_demo_Joint()
{
   pts1= [ for(ps= rodPts(randPts(3,d=1), opt=["r",1, "len",0.5, "nside", 6]) ) each ps ];
   Frame( pts1, opt=[ "r", 0.05
                    , "color","gold"
                    , "Joint", false
                    ] );   
                     
   pts2= transN(pts1, 1); 
   Frame( pts2, opt=[ "r", 0.05
                    , "color", ["green", 0.9]
                    , "$fn", 12
                    , "Joint", true//[ "color", "red"]
                    ] );  
                    
   pts3= transN(squarePts(transN(pts2,1)), 0.5, keep=true); 
   Frame( pts3, opt=[ "r", 0.05
                    , "color", ["blue", 0.7]
                    , "Joint", ["r",0.1, "color", "red", "shape", "corner"]
                    ] ); 
                    
                     
   pts4= cubePts([2,2,1], actions=["x", 2]);
   Frame( pts4, opt=[ "r", 0.1
                    , "color", ["purple", 0.9]
                    , "Joint", ["color", "yellow", "shape", "corner"]
                    ] ); 
                    
                      
}
Frame_demo_Joint();

module Frame_demo_indices()
{
   pts1= [ for(ps= rodPts(opt=["r",1, "len",0.5, "nside", 6]) ) each ps ];
   MarkPts(pts1, Line=["color",["gold",0.3]], Ball=false, opt=["Label",["scale",0.3]] );
   
   Frame( pts1, opt=[ "r", 0.05
                    , "color","red"
                    , "Joint", false
                    , "indices", [[0:3], [4,5]]
                    , "closed", false
                    ] );   
                     
   pts2= transN(pts1, 2); //[ for(ps= rodPts(opt=["r",1, "len",1, "nside", 6]) ) each ps ];
   MarkPts(pts2, Line=["color",["gold",0.3]], Ball=false,opt=["Label",["scale",0.3]] );
   Frame( pts2, opt=[ "r", 0.05
                    , "color", ["green", 0.7]
                    , "Joint", ["r",0.1, "color", "red"]
                    , "indices", [[0,1],[5:8],[9:11]]
                    , "closed", false
                    ] );    
                    
   pts3= transN(pts1, 4); //[ for(ps= rodPts(opt=["r",1, "len",1, "nside", 6]) ) each ps ];
   MarkPts(pts3, Line=["color",["gold",0.3]], Ball=false,opt=["Label",["scale",0.3]] );
   Frame( pts3, opt=[ "r", 0.05
                    , "color", ["blue", 0.7]
                    , "Joint", ["r",0.1, "color", "teal"]
                    , "indices", [[0:2:len(pts3)-1]]
                    , "closed", false
                    ] );  
                      
}
//Frame_demo_indices();

module Frame_demo_indices2()
{
   
   pts10= [ for(ps= rodPts(randPts(3,d=1), opt=["r",1, "len",0.5, "nside", 24]) ) each ps ];
   MarkPts(pts10, Line=["color",["gold",0.3]], Ball=false, opt=["Label",["scale",0.3]] );
   
    Frame( pts10, opt=[ "r", 0.01
                    , "color", ["purple", 0.7]
                    , "Joint", ["r",0.05, "color", "purple"]
                    , "indices", [[0:2:len(pts10)-1]]
                    , "closed", false
                    ] );      
    Frame( pts10, opt=[ "r", 0.01
                    , "color", ["teal", 0.7]
                    , "Joint", ["r",0.05, "color", "teal"]
                    , "indices", [[0:3:len(pts10)-1]]
                    , "closed", false
                    ] );   
                     
   
   pts11= transN2(pts10,2.5);
   Line([for(i=range(pts11)) if(i<len(pts11)/2) pts11[i]], closed=true);
   Line([for(i=range(pts11)) if(i>=len(pts11)/2) pts11[i]], closed=true);
   Frame( pts11, opt=[ "r", 0.01
                    , "color", ["teal", 0.7]
                    , "Joint", ["r",0.02, "color", "teal"]
                    , "indices", [[for(i=[0:len(pts11)/2-1]) [i,i+len(pts11)/2] ]
                                 ,[0, len(pts11)-1]   
                                 , [for(i=[0:len(pts11)/2-2]) [i,i+len(pts11)/2+1] ]
                                 ]
                    , "closed", false
                    ] );  
                      
}
//Frame_demo_indices2();

module Frame_demo_indices3()
{
   
   pts11= circlePts(randPts(3), rad=1.5, n=36);
   //MarkPts(pts11, Line=["color",["gold",0.3]], Ball=false, Label=false );
   //Line(pts11, r=LINE_R/2);
   
    Frame( pts11, opt=[ "r", 0.01
                    , "color", ["red", 0.7]
                    , "Joint", false//["r",0.05, "color", "purple"]
                    , "indices", [[0:2:len(pts11)-1]]
                    , "closed", false
                    ] );      
    Frame( pts11, opt=[ "r", 0.01
                    , "color", ["green", 0.7]
                    , "Joint", false//["r",0.05, "color", "teal"]
                    , "indices", [[0:3:len(pts11)-1]]
                    , "closed", false
                    ] );    
    Frame( pts11, opt=[ "r", 0.01
                    , "color", ["blue", 0.7]
                    , "Joint", false//["r",0.05, "color", "teal"]
                    , "indices", [[0:4:len(pts11)-1]]
                    , "closed", false
                    ] );    
                    
    Frame( pts11, opt=[ "r", 0.01
                    , "color", ["purple", 0.7]
                    , "Joint", false//["r",0.05, "color", "teal"]
                    , "indices", [[0:5:len(pts11)-1]]
                    , "closed", false
                    ] );   
                     
    Frame( pts11, opt=[ "r", 0.01
                    , "color", ["teal", 0.7]
                    , "Joint", false//["r",0.05, "color", "teal"]
                    , "indices", [[0:6:len(pts11)-1]]
                    , "closed", false
                    ] );    
                    
    Frame( pts11, opt=[ "r", 0.01
                    , "color", ["olive", 0.7]
                    , "Joint", false//["r",0.05, "color", "teal"]
                    , "indices", [[0:7:len(pts11)-1]]
                    , "closed", false
                    ] );    
                    
                      
                      
}
//Frame_demo_indices3();


module Frame_demo_cone()
{
  echom("Frame_demo_cone()");
  pts = cylinderPts();
  Frame(pts);

  pts2= [for(p=cylinderPts(r1=0))p+[2.5,0,0]];
  Frame(pts2, shape="conedown");
  
  pts3= [for(p=cylinderPts(r2=0))p+[4.5,0,0]];
  Frame(pts3, shape="cone");


}

//Frame_demo_cone();