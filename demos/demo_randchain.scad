include <../scadx.scad>

module demo_randchain()
{

  echom("demo_randchain");
  seed=pqr();
  
  ch2=chainBoneData(n=5,seed=seed);
  echo(ch2 = _fmth(ch2));
  Red() MarkPts(h(ch2,"pts" ));


  ch= chainBoneData(seed=seed);
  echo(ch = _fmth(ch));
  MarkPts(h(ch,"pts"), Line=["r",0.05, "color",["gold",0.5]]);
  
}
demo_randchain();
