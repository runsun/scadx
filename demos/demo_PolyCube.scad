include <../scadx.scad>

//echo($fn=$fn);
//. W/o site

module PolyCube_demo_default()
{
   echo(_b("PolyCube_demo_default()"));  
   PolyCube();
   translate([0,2.5,0]) PolyCube(size=3,color="red");
   translate([2.5,0,0]) PolyCube(size=[4,2,1],color="green");
   
   echo(_color("PolyCube() : default size=2", "olive"));
   echo(_color("PolyCube(size=3) : size=3", "red"));
   echo(_color("PolyCube(size=[4,2,1]) : size=[4,2,1]", "darkgreen"));
   
}
//PolyCube_demo_default();

module PolyCube_demo_color()
{
   echo(_b("PolyCube_demo_color()"));  
   PolyCube(size=[4,2,1], color=["green",0.5], actions=["x", 2.5]);

   PolyCube(size=[4,2,1], center=true, opt=["color",["blue",0.2]]);
   PolyCube();

   color("red", 0.3) PolyCube(size=3, actions=["y",2.5]);

   PolyCube(color=["teal",0.5], opt=["color", "black", "actions", ["t",[-2.2,0,2]] ] );
   	   
   echo(_color("PolyCube() : default", "olive"));
   echo(_color("color(\"red\") PolyCube(...): Using built-in coloring, which also colors the frame ", "red"));
   echo(_b("The following 2 use PolyCube()'s color setting, both work the same way:"));
   echo(_color("PolyCube(..., color=[\"green\",0.5]) // use plain arg", "darkgreen"));
   echo(_color("PolyCube(..., opt=[\"color\",[\"blue\",0.2]]) // use opt.color", "blue"));
   echo(_color("PolyCube(..., color=[\"teal\",0.5], opt=[\"color\",\"black\"]) // plain color overwrites opt.color", "teal"));
   
}
//PolyCube_demo_color();

module PolyCube_demo_center()
{
   echo(_b("PolyCube_demo_center()"));     
   PolyCube(center=true);
   PolyCube(size=[4,3,2.5], opt=["center", true, "color", ["red", 0.2]]);  
}
//PolyCube_demo_center();

module PolyCube_demo_partial_center()
{
   echo(_b("PolyCube_demo_partial_center()"));  
   PolyCube(3, center=[1,0,0], opt=["color", ["red", 0.2]] );
   PolyCube(3, opt=["center", [0,1,1], "color", ["blue", 0.2]] );
}
//PolyCube_demo_partial_center();

module PolyCube_demo_Frame()
{
   echo(_b("PolyCube_demo_Frame()"));  
      
   echo(_color("Default: [\"r\", 0.01]", "orange"));
   PolyCube([1,3,2]);
   
   echo(_b("Frame set by simple arg"));
   echo(_color("frame=0.1", "orange"));
   PolyCube([1,3,2], frame=0.1,opt=["actions",["x",-2]]);
   _echo(_color("frame=['color','red']", "orange"));
   PolyCube([1,3,2], frame=["color","red"],opt=["actions",["x",-4]]);
   
   echo(_b("Frames are set by opt.frame"));
   
   _echo(_color("['frame', false]", "red"));
   PolyCube([1,3,2], opt=["actions",["x",2], "frame",false, "color",["red",0.5]]);
   
   _echo(_color("['frame', 0.05]", "green"));
   PolyCube([1,3,2], opt=["actions",["x",4], "frame",0.05, "color",["green",0.5]]);
   
   _echo(_color("['frame', ['color','red']]", "blue"));
   PolyCube([1,3,2], opt=["actions",["x",6], "frame",["color","red"], "color",["blue",0.5]]);
   
   echo(_b("Frames set by opt.frame, but denied by simple arg "));
   _echo(_color("PolyCube(...frame=false, opt=['frame', ['color','red']])", "purple"));
   PolyCube([1,3,2], frame=false, opt=["actions",["x",8], "frame",["color","red"], "color",["purple",0.5]]);
  
}
//PolyCube_demo_Frame();

module PolyCube_demo_opt_frame()
{
   echo(_b("PolyCube_demo_opt_frame()"));  
      
   size=[1,3,2];
   echo(_orange("Default: [\"r\", 0.01]"));
   PolyCube(size);

   _echo(_b("opt=[ 'frame', ['r', 0.1, 'color', ['red', 0.5]]]"));
   
   opt=[ "frame", ["r", 0.1, "color", ["red", 0.5]]];
   _echo(_red("opt=opt"), "  // Set frame r with opt.frame");
   PolyCube(size, actions=["x",2], opt= update(["color",["red",0.5]],opt) );  
   
   _echo(_green("frame=['r',0.2], opt=opt"), "  // opt.frame is overwritten by frame" );
   PolyCube(size, actions=["x",4], frame=["r",0.2], opt=update(["color",["green",0.8]],opt)); 
   
   _echo(_blue("color('blue') PolyCube(size, frame=['r',0.2], opt=opt);"), " // colorizes everything" );
   color("blue") PolyCube(size, actions=["x",6], frame=["r",0.2], opt=update(["color",["green",0.5]],opt));
    
   _echo(_purple("frame=['color','blue'], opt=opt"), "  // opt.frame is overwritten by frame" );
   PolyCube(size, actions=["x",8], frame=["color","blue"], opt=update(["color",["purple",0.5]],opt)); 
   
   _echo(_olive("frame=false, opt=opt"), "  // frame=false disables it" );
   PolyCube(size, actions=["x",10], frame=false, opt=update(["color",["olive",0.8]],opt)); 
    
}
//PolyCube_demo_opt_frame();

module PolyCube_demo_actions()
{
   echo(_b("PolyCube_demo_actions()"));  
      
   echo(_darkorange("PolyCube([3,2,1]"));   
   PolyCube([3,2,1],opt=["color",["gold", 0.6]]);
   
   _echo(_red("opt=['actions', ['rotx', -90]]"));
   PolyCube(size=[3,2,1], opt=[ "actions", ["rotx", -90], "color", ["red",0.3]]);
   
   _echo(_darkgreen("center=true, opt=['actions', ['rotx', -90]]"), "   // actions with center");
   PolyCube(size=[3,2,1], center=true, opt=[ "actions", ["rotx", -90], "color", ["green",0.3]]);
   
   _echo(_blue("opt=['actions', ['roty', -45, 'z',2]]"), " // multiple actions");
   PolyCube(size=[3,2,1],  opt=[ "actions", ["roty", -45,"z",2], "color", ["blue",0.3]]);
   
   _echo(_purple("action=['y',3], opt=['actions', ['roty', -45, 'z',2]]"), "// actions expanded to opt.actions");
   PolyCube(size=[3,2,1],  actions=["y", 3], opt=[ "actions", ["roty", -45,"z",2], "color", ["purple",0.3]]);
   
   _echo(_olive("actions=false, opt=['actions', ['roty', -45, 'z',2]]"), " // actions=false disable it");
   PolyCube(1.5,  actions=false, opt=[ "actions", ["roty", -45,"z",2], "color", ["olive",0.8]]);
}
//PolyCube_demo_actions();

//====================================================
//. With site

module PolyCube_demo_with_site()
{
   echo(_b("PolyCube_demo_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");
   PolyCube(opt=["site",pqr, "markPts","l", "color",["gold",0.5]]);
   
   pqr2= randPts(count=3, d=[4,5]);
   MarkPts(pqr2,"l=PQR;ch");
   PolyCube([3,2,1], opt=["site",pqr2, "markPts","l", "color",["red",0.3]]);
   
}
//PolyCube_demo_with_site();

module PolyCube_demo_center_with_site()
{
   echo(_b("PolyCube_demo_center_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");  
   PolyCube([3,2,1], center=true, opt=["site",pqr, "color",["gold",0.5]]);
     
}
//PolyCube_demo_center_with_site();

module PolyCube_demo_partial_center_with_site()
{
   echo(_b("PolyCube_demo_partial_center_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");  
   PolyCube([3,2,1], center=[0,1,0], opt=["site",pqr, "color",["gold",0.5]]);

   pqr2=pqr();
   color("red") MarkPts(pqr2,"l=PQR;ch");  
   PolyCube([3,2,1], center=[1,0,0]
       , opt=["center",[0,1,0], "site",pqr2, "color",["red",0.3]]);
     
}
//PolyCube_demo_partial_center_with_site();


module PolyCube_demo_actions_with_site()
{
   echo(_b("PolyCube_demo_actions_with_site()"));  
   size=[3,2,1];
   
   //pqr=pqr();
   //echo(pqr=pqr);
   
   pqr= [[1.32238, -2.26811, -2.29973], [-1.64044, -0.640784, 1.87238]
        , [2.1467, 0.717838, -0.00367095]];
   
   MarkPts(pqr,"l=PQR;ch");
   
   PolyCube(size, opt=["site",pqr]);
   
   PolyCube(size, opt=["actions", ["x",2],"site",pqr,"color",["red",0.5]]);
   
   PolyCube(size, opt=["actions",["roty",-45],"site",pqr,"color",["green",0.5]]);
  
   PolyCube(size,  opt=["actions",["rotz",90, "rotx",-90],"site",pqr,"color",["blue",0.5]]);

   PolyCube(size, actions=["y",-1.5], opt=["actions",["rotz",90, "rotx",-90],"site",pqr,"color",["purple",0.5], "markPts","l"]);
   
}
PolyCube_demo_actions_with_site();


//. dim

module PolyCube_demo_dim()
{
   echo(_b("PolyCube_demo_dim()"));  
   PolyCube([2,3,1], dim=[4,5,0], markPts="l");
   
   PolyCube([2,3,1], actions=["y", 4], dim=[[7,0,6]], markPts="l", color=["red",.5]);
   PolyCube([2,3,1], site=randPts(3, d=[-1,-2]), dim=[0,1,3], markPts="l", color=["green",.5]);
   
   PolyCube([2,3,1], actions=["x", 4], dim=[6, [5,1.5,1], 2], markPts="l", color=["purple",.5]);
   
   echo(_olive("PolyCube(...dim=[4,5,0]...)"));
   echo(_red("PolyCube(...dim=[[7,0,6]]...) // Enter 3 indices together"));
   echo(_green("PolyCube(...site=pts, dim=[0,1,3]...) // Works with site, too"));
   echo(_purple("PolyCube(...dim=[ 6, [5,1.5,1], 2 ] ...) // Enter the site of any pt"));
   
}
//PolyCube_demo_dim();

module PolyCube_demo_dim2()
{
   echo(_b("PolyCube_demo_dim2()"));  
   //PolyCube([6,4,2], dim=[[4,5,0]], markPts="l");
   
   size=[4,2,1];
   PolyCube(size, dim=[[6,7,3],[7,3,1],[7,0,1]],  markPts="l");
   
   PolyCube(size, actions=["y", 4], dim= [ "pqrs", [[7,6,1]]
                                     , "color", "green" ]
        , markPts="l", color=["red",.2]
        );
        
   PolyCube(size, actions=["x", 6], dim=["pqrs",[[6,7,3],[7,3,1],[7,0,1]]
                                     ,"color","red"]
        , markPts="l", color=["green",.2]
        );
        
   PolyCube(size, actions=["y", -5], dim=[ "pqrs",[["pqr",[6,7,3], "color","green"]
                                              ,[7,3,1]
                                              ,[7,0,1]
                                              ]
                                     ,"color","purple"]
        , markPts="l", color=["blue",.2]
        );
 
}
//PolyCube_demo_dim2();
