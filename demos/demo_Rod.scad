include <../scadx.scad>

module Rod_demo_0()
{ _mdoc("Rod_demo_0"
,
 " More demos showing varying nside. Default: [''nside'',6]. "
);

	Rod();

    pqr = randPts(3, d=[1,4]);
	Rod(pqr, ["r",1 ] ); 
    MarkPts( pqr, opt="p,c||label=PQR");
////
	Rod( trslN(pqr,2), opt=["color","red", "r",0.5,"nside",4, "len",2] );
////
	Rod( trslN(pqr,4), opt=["color","green", "r",0.4,"nside",20, "len",3] );
}
//Rod_demo_0();

module Rod_demo_1_bone_frame_radlines()
{ 
    echom("Rod_demo_1_bone_frame_radlines");
    
    nside=6; r= 0.8;
    
    
	pqr = pqr();
	MarkPts( pqr, "p,c"); 
    Rod( pqr, ["r",r, "nside",nside] );
              //,"frame", true] );
    
    pqr1 = trslN(pqr,2.5);
	//MarkPts( pqr1, "p,c"); 
    Rod( pqr1, _s("bone||r={_}|nside={_}|color=red|transp=0.4",[r,nside]));
    LabelPt( onlinePt( p01(pqr1), len=-0.8), "bone", ["color","red"]);

    pqr1_2 = trslN2(pqr1,2.5);
	//MarkPts( pqr1, "p,c"); 
    Rod( pqr1_2
       , ["r",r, "nside",nside, "color","red","transp",0.4
         , "bone", ["chain",["n",4,"color","blue","r",0.1]]
         ]
       );  
    //LabelPt( onlinePt( p01(pqr1_2), len=-0.5), "bone", ["color","red"]);
    
    pqr2 = trslN(pqr1,2.5);
	//MarkPts( pqr, "pc"); 
    Rod( pqr2, ["r",r, "nside", nside, "transp", 0.4, "hide",true
               , "markpts",true]);
    LabelPt( onlinePt( p01(pqr2), len=-0.8), "markpts", ["color","blue"]);
        
    
    pqr3 = trslN(pqr2,2.5);
	//MarkPts( pqr, "pc"); 
    Rod( pqr3, _s("frame||r={_}|nside={_}|color=blue|transp=0.4|hide=true",[r,nside]));
    LabelPt( onlinePt( p01(pqr3), len=-0.8), "frame", ["color","red"]);

    
    pqr4 = trslN(pqr3,2.5);
	//MarkPts( pqr, "pc"); 
    Rod( pqr4, , ["r",r, "nside", nside, "transp", 0.4, "showobj",false
               ,  "radlines",true,"frame",true]);
    LabelPt( onlinePt( p01(pqr4), len=-0.8), "frame,radlines", ["color","red"]);


}
//Rod_demo_1_bone_frame_radlines();

//echo( pqr(p=1,r=2,a=90));

module Rod_demo_2_markpts()
{
    echom("Rod_demo_2_markpts");
    space = 2;
    
    pqr = pqr(p=2);
    
    Rod( pqr, opt = "markpts||transp=0.5||len=2" );
    //MarkPts( pqr, "c,p||label=PQR");
    
    pqr1= trslN( pqr, space);
    Rod( pqr1, opt = ["len=3","markpts"
                     , ["label",["tmpl","P{*`a}"
                                ,"text","ABCDEFGHILMN" //range(pqr1)
                                ]]] );
    
    pqr2= trslN( pqr, 2*space);
    Rod( pqr2, opt = ["markpts" 
                     ,  [  "hide",  true
                        ,  "label",[ "isxyz",true
                                   , "scale", 0.01
                                   , "color","black"
                                   , "tmpl","{*`a|d2}"
                                   ]
                        , "chain",["r",0.05, "closed",true]
                        ]
                        
                     ] );

//    pqr2= trslN( pqr, 2*space);
//    Rod( pqr2, opt = ["markpts" 
//                     ,  [  "hide",  true
//                        ,  "label",[ "isxyz",true
//                                   , "scale", 0.01
//                                   , "color","black"
//                                   , "tmpl","{*`a|d2}"
//                                   ]
//                        , "chain",["r",0.05, "closed",true]
//                        ]
//                        
//                     ] );
    
//	Rod(opt=["transp",0.5, "r",0.3,"markpts",true]);
//	Rod(opt=["transp",0.5, "r",0.3,"label","AB"]);
//	
//	Rod(opt=["color","red","transp",0.3, "r",0.6
//			, "labelrange", true
//			]);
//
//[{_`a|d2}]
//	Rod(opt=["color","green","transp",0.3, "r",0.3
//			, "markrange",true]);
//
//	Rod(opt=["color","blue","transp",0.3, "r",0.6
//			, "markrange",[0,2,4,6,8,10]]);
//
//	Rod(opt=["color","brown","transp",0.3, "r",0.6
//			, "labelrange",[0,6]
//			, "labelopt",["labels",["1st","2nd"]]
//			, "markrange",[0,6]
//			]
//		);
//
//	Rod(opt=["color","gray","transp",0.3, "r",0.6
//			, "labelrange",true
//			, "labelopt",["color","red","scale",0.02]
//			, "markrange",[0,6]
//			]
//		);

}
//Rod_demo_2_markpts();



module Rod_demo_3_angles()
{
_mdoc("Rod_demo_3_angles",
" p_angle, q_angle
;;
;; yellow one: 60, 45
;; red one: 30, 60
;;
;; default (when not given) = 90
");

	pqr = randPts(3);

	Chain(pqr, ["r",0.05]);

	MarkPts( pqr, ["labels", ["P(60)","Q(45)","R"]] );

	Rod(pqr, opt=["transp",0.5, "r",1, "showsideindex",true
			, "p_angle",60
			, "q_angle",45
			, "echodata", true
			]);


	pqr2 = randPts(3);

	Chain(pqr2, ["r",0.05]);

	MarkPts( pqr2, ["labels", ["P(30)","Q(60)","R"]] );

	Rod(pqr2, opt=["transp",0.5, "r",1, "showsideindex",true
			, "p_angle",30
			, "q_angle",60
			, "echodata", true
			, "color","red"
			]);
}
//Rod_demo_3_angles();



module Rod_demo_4_angles()
{
_mdoc("Rod_demo_4_angles",
" Show how can the p_angle, q_angle be applied to join rods. 
;;
;; Note that this is just for demo. The joints in this demo
;; actually have two exact same faces overlapping. 
");

	pqr = randPts(3);
	rqp = p210(pqr);

	Chain(pqr);

	M = angleBisectPt( pqr );
	angle = angle(pqr)/2;
	MarkPts(pqr, ["labels","PQR"] );
	Rod(pqr, opt=["transp",0.5, "r",0.4
				//,"label",true, "showsideindex",true
				, "q_angle", angle //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp, opt=["transp",0.5, "r",0.4
				//,"label",true, "showsideindex",true
				, "q_angle", angle // angle( [ P(pqr), R(pqr), M] )
				]);
	
	//==========================
	pqr2 = randPts(3);
	rqp2 = p210(pqr2);

	//Chain(pqr);

	M2 = angleBisectPt( pqr2 );
	angle2 = angle(pqr2)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("red",0.7){
	Rod(pqr2, opt=["transp",0.2, "r",0.3, "nside",3
				//,"label",true, "showsideindex",true
				, "q_angle", angle2 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp2, opt=["transp",0.2, "r",0.3, "nside",3
				//,"label",true, "showsideindex",true
				, "q_angle", angle2 // angle( [ P(pqr), R(pqr), M] )
				]);
	}

	//==========================
	pqr3 = randPts(3);
	rqp3 = p210(pqr3);

	//Chain(pqr);

	M3 = angleBisectPt( pqr3 );
	angle3 = angle(pqr3)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("green", 0.7){
	Rod(pqr3, opt=["transp",0.2, "r",0.3, "nside",4
				//,"label",true, "showsideindex",true
				, "q_angle", angle3 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp3, opt=["transp",0.2, "r",0.3, "nside",4
				//,"label",true, "showsideindex",true
				, "q_angle", angle3 // angle( [ P(pqr), R(pqr), M] )
				]);
	}

	//==========================
	pqr4 = randPts(4);
	rqp4 = p210(pqr4);

	//Chain(pqr);

	M4 = angleBisectPt( pqr4 );
	angle4 = angle(pqr4)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("blue", 0.7){
	Rod(pqr4, opt=["transp",0.2, "r",0.3, "nside",18
				//,"label",true, "showsideindex",true
				, "q_angle", angle4 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp4, opt=["transp",0.2, "r",0.3, "nside",18
				//,"label",true, "showsideindex",true
				, "q_angle", angle4 // angle( [ P(pqr), R(pqr), M] )
				]);
	}
}
//Rod_demo_4_angles();



module Rod_demo_5_user_q_pts()
{
_mdoc("Rod_demo_5_user_q_pts",
" This demo shows how to use customized q_pts
;; with expanded q_pts.
;;
;; The pts when angle=90
;;
;;   q_pts90 = arcPts( pqr, r=r, a=360, pl=''yz'', count=nside ); 
;;
;; Expand them to twice the r
;;
;;   ex_q_pts90= expandPts( q_pts90, dist=2*r ); 
;;
;;   Rod( pqr, [''q_pts'', ex_q_pts90, ...] ); 
");

	pqr = randPts(3);
	
	//P2 = onlinePt( p01(pqr), len=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);

	nside = 6;
	r = 0.5;
	
	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=nside );

	// Expand them to twice the r
	ex_q_pts90= expandPts( q_pts90, dist=2*r );

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",r, "q_pts", ex_q_pts90
			 , "nside",nside
			 , "showsideindex",true,"transp",0.4] );
}
//Rod_demo_5_user_q_pts();



module Rod_demo_6_user_q_pts()
{
_mdoc("Rod_demo_6_user_q_pts",
" Another demo shows how to use customized q_pts.
;;
;;   nside=24; 
;;   spark_ratio=3; 
;;
;; The pts when angle=90
;;
;;   q_pts90 = arcPts( pqr, r=r, a=360, pl=''yz'', count=nside ); 
;;
;; Construct the points:
;;
;;   pts= [for(i=range(nside))
;;          mod(i,3)==0? onlinePt( [Q, q_pts90[i]], ratio=spark_ratio)
;;                     : q_pts90[i]
;;        ];
;;
;;   Rod( pqr, [''q_pts'', pts, ...] ); 
");

	// This demo shows how to use customized headpts

	pqr = randPts(3);
	
	//P2 = onlinePt( p01(pqr), len=-1);


	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);

	nside = 24;
	r = 0.5;
	spark_ratio = 3;

	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=nside );

	pts= [for(i=range(nside))
			mod(i,3)==0? onlinePt( [Q, q_pts90[i]], ratio=spark_ratio)
						: q_pts90[i]
		];

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",r, "q_pts", pts
			 , "nside",nside
			 , "showsideindex",false
			,"transp",1] );
}
//Rod_demo_6_user_q_pts();



module Rod_demo_7_gear()
{
_mdoc("Rod_demo_7_gear",
 " This demo shows how to use Rod() to make a simple gear-like
;; structure using customized p_pts/q_pts
;;
;; Assuming each tooth has 6 pts (defined as pt_per_tooth):
;;      __
;;     /  ^      23 
;;     |  |    1    4
;;  ---+  +    0    5
;;
;; We extend the point certain distance outward according to 
;; mod(i, pt_per_tooth). 
");     
	

	_pqr = randPts(3);
	
	pqr = newx( _pqr, onlinePt( p10(_pqr), len=0.5) );
	//P2 = onlinePt( p01(pqr), dist=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);
	P2=onlinePt( [Q,P], len=1);

	teeth = 18;
	pt_per_tooth = 6;
	nside = teeth* pt_per_tooth;
	r = 1; //0.5;
	tooth_height=0.2;

	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=nside );
	
	//tailpts90 = arcPts( [P2,P,R], r=r, a=360, pl="yz", count=nside );

	gear_pts_q= [for(i=range(nside))
			 mod(i,pt_per_tooth)==1||mod(i,pt_per_tooth)==4
			  ? onlinePt( [q_pts90[i],Q], len=-tooth_height*1/2)
			  : mod(i,pt_per_tooth)==2||mod(i,pt_per_tooth)==3
			    ? onlinePt( [q_pts90[i],Q], len=-tooth_height)
			//:mod(i,pt_per_tooth)==0|| mod(i,pt_per_tooth)==5
				: q_pts90[i]
		];

	dpq = P-Q;
	gear_pts_p = [for(p=gear_pts_q) p+dpq];

	//=========================

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",5, "len",0.5
			, "q_pts", gear_pts_q
			, "p_pts", gear_pts_p
			 , "nside",nside
			 , "showsideindex",false
			,"transp",0.8] );
}
Rod_demo_7_gear();



module Rod_demo_8_rotate()
{
nside=6;
_mdoc("Rod_demo_8_rotate",
_s(" red: rotate {_} degree
;; green: rotate {_} degree
;; 
;; Note how the rotation goes (counter clock-wise)
", [360/nside/2, 360/nside])
);
	
	pqr = randPts(3);
	pqr=[[-2.73324, -2.05788, -2.44777], [-0.285864, -1.8622, -1.77504], [2.84959, 2.00984, -2.64569]];
	P=P(pqr); Q=Q(pqr); R=R(pqr);

	MarkPts( pqr, ["labels","PQR"] );
	Plane( [ onlinePt( [Q,P], ratio=2)
		   , Q(pqr)
		   ,onlinePt( [Q,R], ratio=2)
		   ] ,  , ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",1, "nside",nside, "markrange",[0],"labelrange",[0],"transp",0.9] );
//
	// 2014.8/21: 
	// Found that sometimes the rotation goes to the opposite direction
	// 
	//	It seems that it occurs in (2) case:
	//
	//	(1)
	//    
	//        R._
	//           '-._
	//	p---T------'Q
	//
	//
	//	(2)    
	//        R._
	//           '-._
	//               '-._
	//	    T  p-------'Q
	//
	// Fixed in 20140821-3

	//echo("pqr: ", pqr);
	T = othoPt( [P,R,Q] );
	echo( "Is TQ > PQ ? ", norm(T-Q)> norm(P-Q) );


	P=P(pqr); Q=Q(pqr); R=R(pqr);

	Rod( [ onlinePt( [P,Q], len=0.3)
		 , onlinePt( [Q,P], len=0.3)
		 , R ]
		, ["r",2, "nside",nside,"markrange",[0],"labelrange",[0],"color","red", "transp",0.6
			  , "rotate", 360/nside/2] );

	Rod( [ onlinePt( [P,Q], len=0.5)
		 , onlinePt( [Q,P], len=0.5)
		 , R ]
		, ["r",3, "nside",nside,"markrange",[0],"labelrange",[0],"color","green", "transp",0.3
			  , "rotate", 360/nside] );
}
//Rod_demo_8_rotate();




module Rod_demo_8_1_q_rotate() // give up
{
_mdoc("Rod_demo_8_1_q_rotate",
" We give up this appoach of making chain using Rod with both 
twistangle and p- q-angle specified. 'cos it's too difficult 
to get both of twistangle and p- q-angle right. 2014.8.29

");

	pqr = randPts(3);
	pqrs1 = concat( pqr, [randPt()] );

	shift = normalPt(pqr,len=1.5)-pqrs1[1];

	pqrs2= [for(p=pqrs1) p+shift];
	pqrs3 = [for(p=pqrs2) p+shift];

pqrs1= [[2.26761, 1.21599, 0.51161], [0.447378, -2.10168, 0.613161], [-0.582338, -1.77329, 1.33888], [-2.13944, 0.683927, -2.8544]];
pqrs2= [[3.02213, 0.840008, 1.75231], [1.20189, -2.47767, 1.85386], [0.172176, -2.14928, 2.57958], [-1.38492, 0.307941, -1.6137]];
pqrs3=  [[3.77664, 0.464021, 2.99301], [1.95641, -2.85365, 3.09457], [0.92669, -2.52527, 3.82029], [-0.630407, -0.0680459, -0.372994]];

	aPQR = angle( pqr ) /2;
	aQRS = angle( p123(pqrs1) )/2;
	taPQRS= twistangle( pqrs1 ); 

	echo("aPQR=", aPQR);
	echo("aQRS=", aQRS);
	echo("taPQRS=", taPQRS);
	 
	echo( "shift=", shift);
	echo( "pqrs1=", pqrs1);
	echo( "pqrs2=", pqrs2);
	echo( "pqrs3=", pqrs3);
	
	MarkPts( pqrs1, ["labels", "PQRS"] );

	opt= ["r",0.4, "transp",0.9];


	Chain( pqrs1 );
	Rod( p012(pqrs1) , concat(["q_angle",aPQR],opt) );
	Rod( p123(pqrs1) , concat(["p_angle",aPQR, "q_angle",aQRS
						, "rotate", -taPQRS  // Rod_QR is calc based on QRS, so 
												// need to make a twiste
						, "cutAngleAfterRotate",false // aPQR is calc based on PQR,                                                         
													 // that is BEFORE rotate
						//, "_rotate", taPQRS  // need to rotate q-end back to QRS 
							],opt) );
//	Rod( p123(pqrs1) , concat(["p_angle",aPQR, "q_angle",aQRS, "rotate", taPQRS, "cutAngleAfterRotate",true],opt) );
	Rod( p231(pqrs1) , concat(["p_angle",aPQR],opt) );

//====================================
	
//	opqr1 = randPts(4);
//	shift = normalPt(p012(opqr1),len=1.5)-opqr1[1];
//	//Normal(p012(opqr1),["color","red","len",3]);
//	opqr2 = [for(p=opqr1) p+shift];
//	opqr3 = [for(p=opqr2) p+shift];
//	//pts = [pqr, pqr2, pqr3];
//	
//	//ap = angle( opqr )/2 ;
//	aOPQ = angle( opqr1 )/2;
//	aPQR = angle( p123(opqr1) )/ 2 ;
//
//	taOPQ= twistangle( reverse(opqr1) );
//
//	echo("aPQR=", aPQR);
//	echo("aOPQ=", aOPQ);
//	echo("taOPQ=", taOPQ);
//	 
//	echo( "shift=", shift);
//	echo( "opqr1=", opqr1);
//	echo( "opqr2=", opqr2);
//	echo( "opqr3=", opqr3);
//	echo( "p123(opqr1) = ", p123(opqr1));
//	echo( "slice(opqr1,1)",slice(opqr1,1));
//
//	
//	MarkPts( opqr1, ["labels", "OPQR"] );
//
//	opt= ["r",0.4, "transp",0.9];
//
//	Chain( opqr1 );
//	Rod( p012(opqr1) , concat(["q_angle",aOPQ],opt) );
//	Rod( p123(opqr1) , concat(["p_angle",aOPQ, "q_angle",aPQR, "rotate", taOPQ, "cutAngleAfterRotate",true],opt) );
//	Rod( p231(opqr1) , concat(["p_angle",aPQR],opt) );
//	
//	Chain( opqr2, ["color","red"]);
//	Rod( p123(opqr1) , concat(["q_angle",aPQR],opt) );
//	Rod( p321(opqr1) , concat(["q_angle",aPQR],opt) );
////
////	Chain( opqr3, ["color","green"] );
////	Rod( p123(opqr3) , opt );
////	Rod( reverse(p123(opqr3)) , concat([ ], opt) );


}

//Rod_demo_8_1_q_rotate();



module Rod_demo_9_rotate_and_angles()
{
_mdoc("Rod_demo_9_rotate_and_angles"
," This demo shows cases when both rotate and p_angle/q_angle are set.
;; It is tricky 'cos the order makes difference. 
;;
;; First we make two copies of a same Rod side by side (yellow), then 
;; add a larger one to each. Make both p_angle and q_angle = 45. 
;; 
;; red: cutAngleAfterRotate= true // default :  rotate then cut
;; green: cutAngleAfterRotate= false // cut then rotate
;;
;; The blue dots on them show how the first point rotates by 30 degree.
");

	nside=6;

	pqr = randPts(3);
	pqr2 = [for(p=pqr) p+ normalPt(pqr,len=4)-Q(pqr)];
	
	for(pts=[pqr,pqr2])
	{
		Chain( pts ,["r",0.02, "color","blue"]);
		
		MarkPts( pts, ["labels","PQR"] );
		
		Plane(pts, ["mode",3]);
		
		Rod( pts, ["r",1.7, "nside",nside, "transp",1
				  ,"markrange",[0]
				  ,"labelrange",[0]
				] 
				);
	}	

	xpqr = concat( linePts( p01(pqr), dist=[0.1,0.1]), [R(pqr)]);
	xpqr2= [for(p=xpqr) p+ normalPt(xpqr,len=4)-Q(xpqr)];

	echo( "xpqr: ", xpqr );
	echo( "xpqr2:", xpqr2);

	opt=[ "r",2
		, "nside",nside
		, "labelrange",true
		, "transp",0.7, 
		, "markrange",[0]
		, "markopt",["colors",["blue"]]
		, "p_angle",45
		, "q_angle",45
		, "rotate", 360/nside/2
		];

	Rod( xpqr, concat( ["color","red"  , "cutAngleAfterRotate", true],opt ));
	Rod( xpqr2,concat( ["color","green", "cutAngleAfterRotate", false],opt) );


}
//Rod_demo_9_rotate_and_angles();


//==========================
module Rod_demo_10_join_rotate()
{
_mdoc( "Rod_demo_10_chain_rotate"
, " Show the role of cutAngleAfterRotate in making Rod joint when
;; rotation is needed. 
;;
;; dfcolor: no rotate 
;; red    : rotate. cutAngleAfterRotate= true (default)
;; green  : rotate, cutAngleAfterRotate= false
;;
");
	pqr = randPts(3);
	pqr2 = [for(p=pqr) p+ normalPt(pqr,len=1.5)-Q(pqr)];
	pqr3 = [for(p=pqr) p+ normalPt(pqr,len=3)-Q(pqr)];
	pts = [pqr, pqr2, pqr3];

	colors=[ undef, "red", "green" ];
	rotates=[ 0, 45, 45 ];
	caar  =[ undef, true, false ];

	for(i= range(3))
	{
		//Chain( pts[i] ); // ,["r",0.02, "color","blue"]);

		JRod( pts[i]
			, rotate = rotates[i]
			, opt = [ "cutAngleAfterRotate", caar[i]
					, "color",colors[i]
					, "transp", 0.8
					, "r", 0.5
					, "nside", 4
					, "label",true
					]
			);

	}	
	jr_opt= [ "transp", 0.7
			, "r", 0.5
			, "nside", 4
			, "label",true
			];

	module JRod(pqr=randPts(3), rotate=0, opt=[] )
	{
		Chainf(pqr, ["closed",false]);

		P=P(pqr); Q=Q(pqr); R=R(pqr); 
		echo("Rod_demo_10_chain_rotate.pqr=", pqr);
		angle = angle(pqr)/2; 
		//echo("angle= ", angle);
		rqp= [R,Q,P];
		Rod( pqr, concat(["rotate", rotate,"q_angle",angle],opt,jr_opt) );
		Rod( rqp, concat(["rotate",-rotate,"q_angle",angle],opt,jr_opt) );
	}		
}

//==========================
module Rod_demo_11_chain()
{
_mdoc( "Rod_demo_11_chain"
, " Show the role of cutAngleAfterRotate in making Rod joint when
;; rotation is needed. 
;;
;; dfcolor: no rotate 
;; red    : rotate. cutAngleAfterRotate= true (default)
;; green  : rotate, cutAngleAfterRotate= false
;;
");
	nside = 36;
	count = 4;
	
	linepts= randPts(count+1);

	//Chain( linepts );
	//MarkPts( linepts, ["labels",true] );

	_allrps = [for(i=range(count-1))
				let( pqr = [ linepts[i], linepts[i+1], linepts[i+2] ]
				   , opqr= [ linepts[i-1],  linepts[i], linepts[i+1], linepts[i+2] ]
				   , ang = angle(pqr)/2
				)
				rodPts( pqr
					  , ["r",0.5, "nside", nside
						,"q_angle", i>=count-2?90:ang
						,"q_rotate", i==0?0:twistangle(opqr)
						]
					  )
			 ];

//	allrps = concat( _allrps[0][0], [ for(rp=_allrps) rp[1] ]);
	allrps = concat( _allrps[0][0], joinarr( [ for(rp=_allrps) rp[1] ] ));
	//LabelPts( allrps, labels="");
	//Chain( allrps );					

//	p00  = onlinePt( p01(linepts), len=-1 );
//	plast= onlinePt( [get(linepts,-1), get(linepts,-2)] , len=-1 );
//
//	linepts2= concat( [p00], linepts, [plast] );
//
//	Chain( linepts2, ["transp",0.5, "r",0.3, "color","red"] );
//
//	//rp = rodPts( linepts,["nside",nside,"q_angle", ang, "r", 0.5] )[1];
//	//Chain( rp );
//
//	rp1 = rodPts( slice(linepts, 0,3),["nside",nside,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp1 );
//
//	rp2 = rodPts( slice(linepts, 1,4),["nside",nside,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp2 );
		
//	pts= joinarr( 
//			[for(c=range(1,count+1))
//				let( pqr=[ linepts2[c-1]
//						, linepts2[c]
//						, linepts2[c+1]
//						]
//				   , ang = angle(pqr)/2
//					)
//				rodPts(pqr,["nside",nside,"q_angle", ang] )
//			]
//		);

//[ "r", 0.05
//	  , "nside",     6
//	  , "label", "" 
//	  , "p_pts",undef  // pts on the P end
//	  , "q_pts",undef  // pts on the q end
//	  , "p_angle",90   // cutting angle on p end
//	  , "q_angle",90   // cutting angle on q end
//	  , "rotate",0  // rotate about the PQ line, away from xz plane
//	  , "cutAngleAfterRotate",true // to determine if cutting first, or
//						  // rotate first. 
//	  ]
	faces = faces("chain", nside=nside, count=count);

//	echo("faces_demo_5_chain.N=", N);
	echo("<br/>faces_demo_5_chain._allrps=", _allrps);
	echo("<br/>faces_demo_5_chain.allrps=", allrps);
	//echo("<br/>faces_demo_5_chain.pts=", pts);
	echo("<br/>faces_demo_5_chain.faces=", faces);
	//color(false, 0.8)
	polyhedron( points = allrps
			 , faces = faces
			 );

}

//Rod_demo_0();
//Rod_demo_1();
//Rod_demo_2_labels();
//Rod_demo_3_angles();
//Rod_demo_4_angles();
//Rod_demo_5_user_q_pts();
//Rod_demo_6_user_q_pts();
//Rod_demo_7_gear();
//Rod_demo_8_rotate();
//Rod_demo_8_1_q_rotate(); // given-up appoach, wait to be removed. 2014.8.29
//Rod_demo_9_rotate_and_angles();
//Rod_demo_10_join_rotate();
//Rod_demo_11_chain();
//Rod_demo_12_addfaces();

module Rod_demo_12_addfaces()
{
	echom("Rod_demo_11_addfaces");
	r = 1.5;
	nside = 6;
	pqr = randPts(3);

	Chain(pqr);
	rodpts = rodPts( pqr, ["r",1.5, "nside", nside] );
//	
//	echo( "rodpts: ", rodpts);
//	echo( "joinarr(rodpts): ", joinarr(rodpts) );
//	LabelPts( join(rodpts),true );

	MarkPts( joinarr(rodpts), ["labels",true] );
//	inner_p_pts = reverse( arcPts( p102(pqr), r=r*2/3, a=360, pl="yz", count=nside));    
//	inner_q_pts =  arcPts( pqr, r=r*2/3, a=360, pl="yz", count=nside);    

	inner_p_pts = expandPts( rodpts[0], dist=-0.5 );
	inner_q_pts = expandPts( rodpts[1], dist=-0.5 );

	echo(inner_p_pts, inner_q_pts );
	Chain(inner_p_pts);
	Chain(inner_q_pts);

	faces = rodsidefaces(nside);

	Rod( pqr,  ["r",1.5, "nside", nside, "label",true, "transp", 0.7

		,"has_p_faces",false
		,"has_q_faces",false
		,"addpts", concat( inner_p_pts, inner_q_pts)
		,"addfaces", concat( [for(f=faces) f+[nside*2,nside*2,nside*2,nside*2] ] )
	]);
}

module Rod_demo_12_hole()
{
_mdoc( "Rod_demo_12_hole"
,"
");
	nside=4;
	pqr = randPts(3);
	Chain(pqr);
	//MarkPts( pqr, ["labels","PQR"] );
	Rod( pqr, ["r",.7, "transp",0.8, "nside",nside
			  ,"label",true
				, "labelrange",true
				, "labelopt", ["begnum",nside*2, "color","red" ] 
			  ] );  

	Rod( pqr, ["r",2, "transp",0.2, "nside",nside
			  ,"label",true
				, "labelrange",true] );  

//    p_faces=	 [ [0,3,11, 8]
//			 , [1,0, 8, 9]
//			 , [2,1, 9,10]
//			 , [3,2,10,11] ];
//
//	q_faces=	 [ [4,5,13,12]
//			 , [5,6,14,13]
//			 , [6,7,15,14]
//			 , [7,4,12,15] ]

}

//Rod_demo_12_hole();
module Rod_bugfixing()
{
	pqr=[[-2.73324, -2.05788, -2.44777], [-0.285864, -1.8622, -1.77504], [2.84959, 2.00984, -2.64569]];
	rqp= [R(pqr),Q(pqr),P(pqr)];
	Rod( pqr, ["r", 0.8, "transp", 0.7, "label",true
				, "labelrange",true, "labelopt",["begnum",0,"prefix",""]]);
	Chain(pqr);
	MarkPts(pqr);

	//Rod( rqp, ["r", 0.8, "transp", 0.7, "label",true
	//			, "labelrange",true, "labelopt",["begnum",0,"prefix",""]]);

}
//Rod_bugfixing();

