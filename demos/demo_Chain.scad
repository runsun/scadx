include <../scadx.scad>


module Chain_demo_simple(){
   
   echom("Chain_demo_simple");
    
   seed = pqr();
    
   MarkPts(seed, "PQR"); 
   Gold(0.3) Chain( seed= seed, r=1, n=4, MarkPts=true );

   //color("red") 
//   Chain( seed=seed, n=16, r=.54, nside=6 , seglen=2
//        , a=120, a2=10, color="red"
//    );
    
}
//Chain_demo_simple();

module Chain_demo_simple2(){
   
   echom("Chain_demo_simple2");
    
   seed = pqr();
    
   Chain( seed= trslN(seed,-3), n=8
    );

   //color("red") 
   Chain( seed=seed, n=16, r=.54, nside=6 , seglen=2
        , a=120, a2=10, color="red"
    );
    
}
//Chain_demo_simple2();

 
module Chain_demo_input(){
    
   echom("Chain_demo_input");
   
   pqr=pqr();
    
   //
   // Input seed
   // 
   MarkPts( pqr, "ch");
   Chain( seed=pqr, n=4, r=.8, closed=true, seglen=4
        , a=90, a2=0, rot=45
        , opt=["markxpts",true, "color", ["red",0.5] ] ); //"trp=0.8;markxpts");
    
   pqr2= trslN(pqr, 4);
   MarkPts( pqr2, "ch" );
   Chain( seed=pqr2, n=6, frame=true, transp=0.5, twist=30, a=[170,185],a2=[-20,20]
        , opt= "trp=0.8;cl=red"  );
    
       
}
//Chain_demo_input();

module Chain_demo_input2(){
    
   echom("Chain_demo_input");
   
   pqr=pqr();
    
   //
   // Input seed
   // 
   MarkPts( pqr, "ch");
   Chain( seed=pqr, n=4, r=.8, closed=true, seglen=4
        , a=90, a2=0, rot=45
        , opt="trp=0.8;markxpts");
    
   pqr2= trslN(pqr, 4);
   MarkPts( pqr2, "ch" );
   Chain( seed=pqr2, n=6, frame=true, transp=0.5, twist=30, a=[170,185],a2=[-20,20]
        , opt= "trp=0.8;cl=red"  );
    
   pqr3= trslN(pqr, 8);
   MarkPts( pqr3, "ch" );
   Chain( seed=pqr3, n=6, r=1.5,r2=0.5, frame=true, a=[170,185],a2=[-20,20]
        , opt= "trp=0.8;cl=green;body=false"  );
   
   //
   // Input pts 
   // 
   pqr4= trslN(pqr, 12);
   MarkPts( pqr4, "ch" );
   pts = arcPts( pqr4, n=6, a=120, rad=6 );
   MarkPts( pts );
   Green(.7) Chain( pts, r=1); 
   
   //
   // Input randchain return
   // 
   pqr5= trslN(pqr, 16);
   MarkPts( pqr5, "ch" );
   rndch = randchain( seed=pqr5, a=[170,185],a2=[-20,20]
                    , n=8, lens=[3,1] );
   Chain( rndch, r=1, "cl=darkcyan;trp=0.7" ); 
   
   //
   // Input chainPts return 
   // 
   pqr6= trslN(pqr, 22);
   MarkPts( pqr6, "ch" );
   rndch6 = randchain( seed=pqr6, seglen=0.6, a=[175,185],a2=[-15,15]
                    , n=28 );
   //echo( rndch6=rndch6, haskey(rndch6));                 
   chrtn = chaindata( hash(rndch6,"pts"), twist=10, nside=4 );
   //echo( chrtn = chrtn);   
   //MarkPts( hash(chrtn,"bone"), "ch");
   Chain( chrtn, "trp=0.9" ); 
       
}
//Chain_demo_input2();

//module Chain_demo_rod(){    
//    echom("Chain_demo_rod");
//    
//    pqr=pqr();
//    N = N(pqr);
//    uvn= uvN(pqr);
//    uvm= uvN( [pqr[0],pqr[1],N]);
//    Rf = pqr[2];
//    
//    Chain( p01(pqr), ["Rf",pqr[2]] );
//    //MarkPts( pqr31, "pl"); 
//        
////    //-----------------
////    
//    pqr2= trslN(pqr,3);
//    Chain( p01(pqr2), ["r",0.5,"transp",0.8,"Rf",pqr2[2]]);
//    
//    pqr3= trslN(pqr,6);
//    Chain( p01(pqr3)
//    , ["r=1;cl=red","transp",0.8,"Rf",pqr3[2]]);
//    
//        pqr31= trslN2(pqr3,3);
//        MarkPts( pqr31, "pl");
//        Chain( p01(pqr31)
//        , ["r=1;cl=brown;rot=45","transp",0.8,"Rf",pqr31[2]]);
//        
//    pqr4= trslN(pqr,9);
//    Chain( p01(pqr4)
//         , ["r=1;nside=6;cl=darkcyan","transp",0.8,"Rf",pqr4[2]]);
//
//    pqr5= trslN(pqr,12);
//    Chain( p01(pqr5)
//         , ["r=1;nside=24;cl=blue","transp",0.8,"Rf",pqr5[2]]);
//
//}
//Chain_demo_rod();

//echo("Before Chain_demo_polygon()");
module Chain_demo_polygon(){    
    echom("Chain_demo_polygon");
    
    pqr=pqr();
    
    pqr3= trslN(pqr,3);
    Chain( seed=pqr3, a2=0, closed=true
          , r=0.58, n=3, a= 180*(3-2)/3, seglen=5);

    pqr4= trslN(pqr,6);
    Chain( seed=pqr4, a2=0, closed=true
          , r=0.58, n=4, a= 180*(4-2)/4, seglen=4, opt="cl=red");
    pqr5= trslN(pqr,9);
    Chain( seed=pqr5, a2=0, closed=true
          , r=0.58, n=5, a= 180*(5-2)/5, rot=45, seglen=3.5, opt="cl=green");

    pqr7= trslN(pqr,12);
    Chain( seed=pqr7, a2=0, closed=true
          , r=0.58, n=7, a= 180*(7-2)/7, rot=30, seglen=3, opt="cl=blue");

    pqr36= trslN(pqr,16);
    Chain( seed=pqr36, a2=0, closed=true
          , r=0.58, n=36, a= 180*(36-2)/36, seglen=.6, opt="cl=darkcyan");

}
//Chain_demo_polygon();


module Chain_demo_rod_rot_twist(){    
    echom("Chain_demo_rod_rot_twist");
    
    pqr=pqr();
    N = N(pqr);
    uvn= uvN(pqr);
    uvm= uvN( [pqr[0],pqr[1],N]);
    Rf = pqr[2];
    
    MarkPts( pqr, "PQR");
    Chain( (pqr), color=0.7,opt=["r",1, "Rf",pqr[2], "markxpts",["rng",[0]]] );
        
    //-----------------
    
    pqr2= trslN(pqr,3);
    MarkPts( pqr2, "PQR");
    Chain( (pqr2), ["r",1, "color",["pink",.6], "rot",45,"Rf",pqr2[2]
                  , "markxpts",["rng",[0]]]
                  );
    
    pqr3= trslN(pqr,6);
    MarkPts( pqr3, "PQR");
    Chain( (pqr3), color=["green",0.6]
    , ["r",1, "twist",30, "Rf",pqr3[2], "markxpts",["rng",[0]]]);
   
    pqr4= trslN(pqr,9);
    MarkPts( pqr4, "PQR");
    Chain( (pqr4), color=["blue",.6] 
         , opt=["r",1,"rot",45 
               ,"twist",30,"Rf",pqr4[2], "markxpts",["rng",[0]]]);
 
}
//Chain_demo_rod_rot_twist();

ISLOG=0;
//module Chain_demo_hcut(){   // 2018.10.22: This feature seems to be replaced by tilt 
//                            // on 20150805 
//                              
//    echom("Chain_demo_hcut");
//    
//    marking=true;
//    xptsopt= marking? ["rng",[0]]:false;
//    
//    _pqr=pqr(); Q= onlinePt(p01(_pqr),len=5);
//    pqr= [ _pqr[0], Q, _pqr[2] ];
//    N = N(pqr);
//    uvn= uvN(pqr);
//    uvm= uvN( [pqr[0],pqr[1],N]);
//    Rf = pqr[2];
//    
//    //if(marking) MarkPts( pqr, "PQR");
//    //Chain( p01(pqr), ["r",1, "color",0.4,"Rf",pqr[2], "markxpts",xptsopt] );
//        
//    
//    pqr2= trslN(pqr,3);
//    if(marking) MarkPts( pqr2, "PQR");
//    Chain( p01(pqr2), hcut= 60
//                    , opt=["r",1, "color",0.8,"hcut",60,"Rf",pqr2[2], "markxpts",xptsopt]);
//
//    //-----------------
////    
////    pqr3= trslN(pqr,8);
////    if(marking) MarkPts( pqr3, "PQR");
////    Chain( p01(pqr3), ["r=1;trp=0.8;cl=darkcyan","hcut",30,"Rf",pqr3[2], "markxpts",xptsopt]);
////
////    pqr4= trslN(pqr,11);
////    if(marking) MarkPts( pqr4, "PQR");
////    Chain( p01(pqr4), ["r=1;trp=0.8;cl=darkcyan","hcut",[45,45],"Rf",pqr4[2], "markxpts",xptsopt]);
////
////    //-----------------
////    pqr5= trslN(pqr,16);
////    if(marking) MarkPts( pqr5, "PQR");
////    Chain( p01(pqr5), ["r=1;trp=0.8;cl=green","hcut",[30,45],"Rf",pqr5[2], "markxpts",xptsopt]);
////
////        pqr51= trslN2(pqr5,3);
////        if(marking) MarkPts( pqr51, "PQR");
////        Chain( p01(pqr51)
////          , markxpts= [ "rng",[0], "r",0.1, "chain",["closed", "r",0.05]]
////          , opt=["r=1;trp=0.8;cl=green;rot=45","hcut",30,"Rf",pqr51[2]]);
//}
//Chain_demo_hcut();
//


module Chain_demo_r_r2(){    
    echom("Chain_demo_r_r2");
    
    randchrtn=randchain(n=6, lenseg=1.5, a=[120,240], a2=[-60,60]);
    pts= hash(randchrtn, "pts");
    
    echo( pts = pts );
    
    Chain(pts);
    
    pqr2=trslN(pts,3);
    Chain(pqr2, color="red", r=0.1); //"cl=red;r=0.1");

    pqr3=trslN(pts,7);
    Chain(pqr3, color="green", opt=["r",1, "r2",.2, "nside",8]) ;
    
//    pqr4=trslN(pts,11);
//    Chain(pqr4, color="blue", "cl=darkcyan;nside=36;r=1;r2=0.01");
//    
//    pqr5=smoothPts(n=3, trslN(pts,15));
//    Chain(pqr5, color="darkcyan", "cl=blue;nside=24;r=1;r2=0.01");

}
Chain_demo_r_r2();

module Chain_demo_closed_nside(){    
    echom("Chain_closed_nside");
    
    pqr = pqr();
    rchrtn= randchain( n=10, len=2, seed=pqr
                    , a=[ 120,160 ], a2=[0,30]);
    pts = hash( rchrtn, "pts");
    MarkPt( pqr[0] );
    
    //pts=randPts(5);
//    pts=[[-0.254608, 2.94226, -0.265922], [-1.79069, 1.79273, 0.359467], [-2.8474, -2.51477, -1.47868], [-2.3854, 0.84323, 1.80297], [1.53019, -1.42591, -1.42311]];
//    pts = [[0.0335302, -2.27956, 2.68029], [-1.49161, 2.95183, 2.97162], [2.89684, -2.40119, -2.71649], [0.998082, -2.8189, -2.22895], [-2.98195, -1.33313, -0.273813]];
    echo( pts = pts );
    Chain(pts,"closed=false");
    
    pqr2=trslN(pts,5);
    Chain(pqr2, "cl=red;closed;r=.6;trp=1");

    pqr3=trslN(pts,10);
    Chain(pqr3, "cl=green;nside=9;closed;r=.6;trp=1");
    
    pqr4=trslN(pts,15);
    Chain(pqr4, "cl=blue;nside=20;closed;r=.6;trp=1");
    
//	path= [ [0,5,1]
//			,[3,5,0],[3.7,4.7,0], [4,4.3,1] //, [ 4,5,0]
//			, [4,3,-1], [1,3,2], [0,0,3], [4,0,-1] ];

}
//Chain_demo_closed_nside();

module Chain_demo_frame(){
    echom("Chain_demo_frame");
    
    n= 5;
    nside = 4; 
    
    pqr=pqr();
    rndch = randchain( n=n, len=2, seed=pqr
                    , a=[ 170,190 ], a2=[-50,50]);
    bone= hash(rndch, "pts");
//    bone = [[0.799646, 1.00731, 1.60879], [-0.0701428, 1.1432, 2.69276], [-0.57967, 1.28045, 3.29968], [-2.3397, 1.87979, 4.96158], [-3.16322, 2.2767, 6.10881]];
    
    echo(bone=bone);
    //------------
    Chain(bone);
  
    //------------
//    bone2 = trslN(bone, 2.5);
//    Chain(bone2, opt="r=1;r2=0.1");  
//    
//    //------------
//    bone3 = trslN(bone, 5);
//    Chain(bone3, opt=
//    "r=1;r2=0.1;trp=0.3;edges;body=false");  
//    
//        bone31 = trslN2(bone3, 4);
//        Chain(bone31, opt=
//        "r=1;r2=0.1;trp=0.3;edges=[r,0.1,color,red,rng,[0,2]];body=false");  
//   
//    //------------
//    bone4 = trslN(bone, 9);
//    Chain(bone4, opt=
//    "r=1;r2=0.1;trp=0.3;markxpts;body=false");  
//    
//        bone41 = trslN2(bone4, 4);
//        Chain(bone41, opt=
//        "r=1;r2=0.1;trp=0.3;markxpts=[plane,color,green, rng,[0,-2]];body=false");  
//////    
//////////
//    //------------
    bone5 = trslN(bone, 14);
    Chain(bone5, opt=
    "r=1;r2=0.1;trp=0.3;frame;body=false");  

        bone51 = trslN2(bone5, 4);
        Chain(bone51, opt=
        "r=1;trp=0.3;frame=[r,0.2];body=false");  

}
//Chain_demo_frame();

module Chain_demo_bone_seed(){
    echom("Chain_demo_bone_seed");
    
    n= 6;
    nside = 4; 
    
    pqr = randPts(3, [-1.5,1.5]);
    rndch = randchain( n=n, len=2, seed=pqr
                    , a=[ 150,180 ], a2=[-70,70]);
    bone = hash(rndch, "pts");
    echo(bone=bone);
    //------------
    //Chain(bone);
  
//    //------------
//    bone2 = trslN(bone, 4);
//    Chain(bone2, opt="r=0.7");  
//    
//    //------------
//    bone3 = trslN(bone, 8);
//    Chain(bone3, opt=
//    "r=0.8;trp=0.3;bone");  
//    //echo(bone3=bone3);
//    //MarkPts(bone3,"ch");
//    
//        bone31 = trslN2(bone3, -4);
//        Chain(bone31, opt=
//    "r=0.7;trp=0.5;closed;bone");  
////   
    //------------
    bone4 = trslN(bone, 12);
    Chain(bone4, opt=
    "r=0.8;trp=0.3;seed=[chain,false,plane,false,label,true];markxpts=[rng,[0],label,true,chain,[color,blue,closed,true]]");  
//    
//        bone41 = trslN2(bone4, 4);
//        Chain(bone41, opt=
//        "r=1;r2=0.1;trp=0.3;markxpts=[plane,color,green, rng,[0,-2]];body=false");  
////    
////////
////    //------------
//    bone5 = trslN(bone, 14);
//    Chain(bone5, opt=
//    "r=1;r2=0.1;trp=0.3;frame;body=false");  
//
//        bone51 = trslN2(bone5, 4);
//        Chain(bone51, opt=
//        "r=1;trp=0.3;frame=[r,0.2];body=false");  

}
//Chain_demo_bone_seed();