include <../scadx.scad>

//demo_Text();
//demo_Text_site();
//demo_Text_site_as_box();
//demo_Text_format();
//demo_Text_template();
//demo_Text_template_hash_data();
//demo_Text_template_w_format();
//demo_Text_actions();
//demo_Text_scale();
demo_Text_actions_with_site();

module demo_Text()
{
   echom("demo_Text()");
   d=[0,1.5,0];

   Text("Scadx");
   translate(d*1) Text([1,2,3], color=COLORS[1]);
   translate(d*2) Text(0.4    , color=COLORS[2]);
   translate(d*3) Text(true   , color=COLORS[3]);
   translate(d*4) Text(undef  , color=COLORS[4]);
 
}
//demo_Text();

module demo_Text_site()
{
   echom("demo_Text_site()");
   site = pqr();
   MarkPts(site, Label="PQR", color="red");
   d=[0,1.5,0];

   Text( "Scadx",site=site );
}

module demo_Text_site_as_box() // 2018.5.29 
{
   echom("demo_Text_site_as_box()");
   
   //site = squarePts(randPts(5));
   //echopts(site);
   site1=[ [2.51,1.19,1.52], [-2.02,-2.51,-1.49], [-3.2,-3.58,1.6], [1.33,0.12,4.61] ];
   MarkPts(site1, Label="PQRS", color="red", Line=["closed",true]);
   Text( "Fixed square",site=site1, scale=.6);
   
   translate( 5*X ) {
   site2= squarePts(pqr());
   MarkPts(site2, Label="0123", color="green", Line=["closed",true]);
   Text( "squarePts(pqr())",site=site2, color="green" );
   }
   
   translate( 3*Y ) {
   site3= randPts(4);
   MarkPts(site3, Label="0123", color="blue", Line=["closed",true]);
   Text( "randPts(4)",site=site3, color="blue" );
   }
}
//demo_Text_site_as_box();


module demo_Text_format()
{
   echom("demo_Text_format()");
   d=[0,-1.5,0];
   
   g=[0,3,13,20];
   
   translate(d*g[0]) color(COLORS[1]) Text("Set format below. See _f() in scadx_string.scad");
   translate(d*(g[0]+1)) color(COLORS[2]) Text(0.4, format="%"); 
   
   translate(d*(g[1]+0)) color(COLORS[3]) Text(0.4, format="|d3"); 
   translate(d*(g[1]+1)) color(COLORS[3]) Text(0.42356, format="|d3"); 
   translate(d*(g[1]+2)) color(COLORS[4]) Text(0.4, format="|w8"); 
   translate(d*(g[1]+3)) color(COLORS[4]) Text(0.4, format="|w8p="); 
   translate(d*(g[1]+4)) color(COLORS[4]) Text(0.4, format="|w8p=al"); 
   translate(d*(g[1]+5)) color(COLORS[4]) Text(0.4, format="|w8p=ac"); 
   translate(d*(g[1]+6)) color(COLORS[4]) Text(0.4, format="%|w8p=ac"); 
   translate(d*(g[1]+7)) color(COLORS[5]) Text([1,2], format="|w8p=ac"); 
   translate(d*(g[1]+8)) color(COLORS[5]) Text("[1,2]", format="|w8p=ac");
   
   translate(d*(g[2]+0)) color(COLORS[6]) Text(0.4, format="||wr={,}");
   translate(d*(g[2]+1)) color(COLORS[6]) Text(0.4, format="||wr=<,>");
   translate(d*(g[2]+2)) color(COLORS[6]) Text(0.4, format="%||wr=<,>");
   translate(d*(g[2]+3)) color(COLORS[6]) Text(0.4, format="%|w9acp-|wr=<,>");
   translate(d*(g[2]+4)) color(COLORS[6]) Text([1,2], format="|w9acp-|wr=<,>");
   translate(d*(g[2]+5)) color(COLORS[6]) Text("[1,2]", format="|w9acp-|wr=<,>");
   
   translate(d*(g[3]+0)) color(COLORS[7]) Text("[1,2]", format="||a=<,>");
   translate(d*(g[3]+1)) color(COLORS[7]) Text([1,2], format="||a=<,>");
   translate(d*(g[3]+2)) color(COLORS[7]) Text([1,2], format="||n=<,>");
   translate(d*(g[3]+3)) color(COLORS[7]) Text(0.4, format="||n=<,>");
   translate(d*(g[3]+4)) color(COLORS[7]) Text(0.4, format="%|w9acp-|n=<,>");
}
//demo_Text_format();

module demo_Text_template()
{
  translate(3*X){
   echom("demo_Text_template()");
   d=[0,-1.5,0];   
   g=[0,3,13,20];   
   
   Red() 
   translate(d*g[0]) Text("Set template below. See _s2() in scadx_string.scad");
   
   color(COLORS[2]) {
   translate(d*(g[0]+2)) Text("=== Fill blanks one item at a time:");                                           
   translate(d*(g[0]+3)) Text([12,3], template="date:{_}/{_}"); 
   
   translate(d*(g[0]+4)) Text([12,3,1900], template="{_}/{_}/{_}"); 
   translate(d*(g[0]+5)) Text(["a","b","c","d"], template="{_}/{_}/{_}"); 
   }
   color(COLORS[3]) {
   translate(d*(g[0]+7)) Text("=== Use indices:");                                           
   translate(d*(g[0]+8)) Text(["a","b","c","d"], template="{3}/{1}/{0}"); 
   translate(d*(g[0]+9)) Text(["a","b","c","d"], template="{2}/{1}/{-1}"); 
   }
   color(COLORS[4]) {
   translate(d*(g[0]+11)) Text("=== Use \"*\" to fill in entire array:");                                           
   translate(d*(g[0]+12)) Text(["a","b","c"], template="{2}/{1}/{*}"); 
   translate(d*(g[0]+13)) Text(["a","b","c"], template="{*}/{1}/{*}"); 
   translate(d*(g[0]+14)) Text([3,4,5], template="pt={*}, ptz={2}, ptx={0}"); 
   }
  }
}
//demo_Text_template();


module demo_Text_template_hash_data()
{
   echom("demo_Text_template_hash_data()");
   d=[0,-1.5,0];   
   g=[0,3,13,20];   
   
   color(COLORS[1]) 
   translate(d*(g[0]+1)) Text(["x",0.345,"y",0456,"z",0.567]
                              , template="x={x}, z={z}, x+y={x}+{y}"
                              );   
}
//demo_Text_template_hash_data();


module demo_Text_template_w_format()
{
   echom("demo_Text_template_w_format()");
   
   _red("Format seems not working completely as expected (2020.6.27)");
   d=[0,-1.5,0];   
   g=[0,3,13,20];  
    
   color(COLORS[5]) {
   translate(d*(g[0]+0)) Text("=== Use format in the template:");                                           
   translate(d*(g[0]+2)) Text("text is an array : [\"x\",0.345,\"y\",0456,\"z\",0.567] ");   
                                           
   translate(d*(g[0]+4)) Text("No template:");
   translate(d*(g[0]+5)) Text(["x",0.345,"y",0456,"z",0.567]
                              //, template="No format: {_}/{x}/{*}"
                              ); 
                              
   translate(d*(g[0]+7)) Text("template=\"{z`|d2}/{y`|d1}/{-1`|d4}\":");
   translate(d*(g[0]+8)) Text(["x",0.345,"y",0456,"z",0.567]
                              , template="{z`|d2}/{y`|d1}/{-1`|d4}"
                              ); 
   
   translate(d*(g[0]+10)) Text("template=\"{z`|d2}, {0`|d1}, {-1`|d4}, {0`%}, {0`%|d1}\":");
   translate(d*(g[0]+11)) Text([0.34567]
                              , template="{z`|d2}, {0`|d1}, {-1`|d4}, {0`%}, {0`%|d1}"
                              );
   }                                        
}
//demo_Text_template_w_format();


module demo_Text_actions()
{
   echom("demo_Text_actions()");
   Text("Scadx");
   
   Text("Scadx", color="red", actions=["t",[1,1,0]]);

   Text("Scadx", color="green", halign="center", valign="center", actions=["rotz",-45]);
   
   Text("Scadx", color="blue",actions=["z",-2], opt=["actions",["rotx",90]] );
   
}
//demo_Text_actions();

module demo_Text_scale()
{
   echom("demo_Text_scale()");
   Text("Scadx");
   
   Text("Scadx scale=1.5", color=["red",1], actions=["y",1.5], scale=1.5);
   Text("Scadx opt.scale=2", color=["green",1], actions=["y",4], opt=["scale",2]);
   Text("Scadx scale=2,opt.scale=2", color=["blue",1], actions=["y",7], scale=2, opt=["scale",2]);
   Text("Scadx scale=[1,2,10]",color=["teal",1], actions=["y",12,"z",-0.5], scale=[1,2,10]);
   
}
//demo_Text_scale();

module demo_Text_actions_with_site()
{
   echom("demo_Text_actions_with_site()");
   //Text("Scadx");
   
   _red("Note that opt.actions=[...] works AFTER (in addition to) actions=[ ... ]"); 
   
   pqr=pqr(); 
   MarkPts(pqr, Label="PQR");
   Text("actions=['t',[2,0,0]]", color="red", scale=0.3, site=pqr, actions=["t",[2,0,0]]);

   pqr2=transN(pqr,2);
   MarkPts(pqr2, Label="PQR");
   Text("actions=['rotz',45]", color="green", scale=0.3, site=pqr2, actions=["rotz",45]);
   
   pqr3=transN(pqr,4);
   MarkPts(pqr3, Label="PQR");
   Text("First rotz 45, then rotx 90", color="blue", scale=0.3, actions=["rotx",90]
       , site=pqr3, opt=["actions",["rotz",45]] );
   
}
//demo_Text_actions_with_site();
