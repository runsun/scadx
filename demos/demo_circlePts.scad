include <../scadx.scad>

ISLOG=1;

//demo_circlePts();
module demo_circlePts()
{
  echom("demo_circlePts()");
  
  //echo(log_b("demo_circlePts"));
  pqr=[[0.43, -0.67, -2.37], [0.97, 1.15, 0.55], [2.39, 0.21, -3.39]];
  //echo(log_(["pqr",pqr]));
  MarkPts( pqr, "PQR", Line=["r",0.03,"color","black"] );
  
  d=0.3;
  
  pts= circlePts(pqr);
  MarkPts(pts, Line=["closed",0]);
  echo(default = "circlePts(pqr)");
  echo(pts=pts);
  
  pts2= circlePts(pqr, rad=1, n=6);
  echo(pts2=pts2);
  Red() MarkPts(pts2, Ball=true, Line=["closed",0]);
  _red("circlePts( pqr, rad=2, n=12 )");
  
  pts_r= circlePts(pqr, ratio=0.5, n=5);
  Green() MarkPts(pts_r, Line=["closed",1]);
  _green("circlePts( pqr, ratio=0.5 )");

  pts_a= circlePts(pqr, rad=2, a=60, n=18);
  Blue() MarkPts(pts_a, Line=["closed",1]);
  _blue("circlePts( pqr, rad=1, a=60, n=18 )");
        
  pts_a2= circlePts(pqr, a=90, rad=2.5, n=36);
  Purple() MarkPts(pts_a2, Label=false, Line=["closed",1]);
  _purple("circlePts( pqr, a=90, rad=2.5, n=36 )");
  //echo(log_e("demo_circlePts"));
  
}

demo_circlePts_show();
module demo_circlePts_show()
{
  echom("demo_circlePts_show()");
  
  //echo(log_b("demo_circlePts"));
  pqr=[[0.43, -0.67, -2.37], [0.97, 1.15, 0.55], [2.39, 0.21, -3.39]];
  //echo(log_(["pqr",pqr]));
  MarkPts( pqr, "PQR", Line=["r",0.03,"color","black"] );
  
  n=12;
  
  pts= circlePts(pqr, ratio=.5, n=n);
  MarkPts(pts, Label=0, Ball=0, Line=["closed",1, "r",0.005]);
  
  for(i=range(n))
  {
    pts= circlePts(pqr, ratio=.5, a=360/n*i, n=n);
    color(COLORS[i])
    MarkPts(pts, Label=0, Ball=0.02, Line=["closed",1]);
  }
}
