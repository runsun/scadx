/*
  Demo the randCoordPt()/Coord() function

*/

include <../scadx.scad>

//Coord_demo();
module Coord_demo()
{
  echom("Coord_demo()");
  Coord();
}


//Coord_demo_X();
module Coord_demo_X()
{
  echom("Coord_demo_X()");
  Coord(Z=undef);
}

//demo_UnitCoord();
module demo_UnitCoord()
{
   echom("demo_UnitCoord()");
   pqr=pqr();
   MarkPts(pqr, "PQR");
   UnitCoord(pqr);
}