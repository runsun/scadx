include <../scadx.scad>

ISLOG=1;


//demo_arcPts_1();
module demo_arcPts_1()
{
  echom("demo_arcPts_1()");
  
  //echo(log_b("demo_arcPts"));
  pqr=pqr();
  pqr = [[1.31, -0.37, 0.3], [1.67, 0.73, 1.16], [-1.82, -1.3, 1.19]];
  //echo(log_(["pqr",pqr]));
  MarkPts( pqr, "PQR", Line=["r",0.03,"color","black"] );
  
  d=0.3;
  
  pts= arcPts(pqr);
  MarkPts(pts);
  //MarkPt( pts[0], "arcPts(pqr)", Link= pts[0]+ [d,d,0] );
  echo(angle = angle(pts));
  
  pts2= arcPts(pqr, rad=2);
  Red() MarkPts(pts2);
  MarkPt( pts2[0], "arcPts(pqr, rad=2)", Link= pts2[0]+ [d,d,0] );
  
  pts_r= arcPts(pqr, ratio=0.5, n=3);
  Green() MarkPts(pts_r);
  MarkPt( pts_r[0], "arcPts(pqr, ratio=0.5, n=3)"
        , Link= pts_r[0]+ [d,d,0] );
  
  pts_a= arcPts(pqr, rad=2.5, a=90, n=10);
  Blue() MarkPts(pts_a);
  MarkPt( pts_a[0], "arcPts(pqr, rad=2.5, a=90, n=10)"
        , Link= pts_a[0]+ [d,d,0] );
  Mark90( newz(pqr, last(pts_a) ));
        
  pts_a2= arcPts(pqr, rad=3, a2=90, n=4);
  Purple() MarkPts(pts_a2);
  MarkPt( last(pts_a2), "arcPts(pqr, rad=3, a2=45, nseg=4)"
        , Link= last(pts_a2)+ [d,d,0] );
  Mark90( [pts_a2[0], pqr[1], last(pts_a2)] );      
  //echo(angle_pts_a2 = angle( [pts_a2[0], pqr[1], last(pts_a2)]) );      
        
  pts_aa2= arcPts(pqr, rad=3, a=30, a2=-45, n=4);
  Purple() MarkPts(pts_aa2);
  MarkPt( last(pts_aa2), "arcPts(pqr, rad=3, a=30, a2=-45, n=4)"
        , Link= last(pts_aa2)+ [d,d,0] );
  //echo(log_e("demo_arcPts"));
}


demo_arcPts_2();
module demo_arcPts_2()
{
  echom("demo_arcPts_2()");
  
  //echo(log_b("demo_arcPts"));
  pqr=pqr();
  pqr = [[1.31, -0.37, 0.3], [1.67, 0.73, 1.16], [-1.82, -1.3, 1.19]];
 
  MarkPts( pqr, Label=["text","PQR","scale",2,"color","purple"]
         //, Line=["r",0.03,"color","black"]
         , Grid=["r",0.01,"color",["red",0.5],"mode",2]); 
  MarkPts([pqr[1],N(pqr)],Label=["text",["","N"]]);
  Mark90( [ N(pqr),pqr[1],pqr[0] ] );
  Mark90( [ N(pqr),pqr[1],pqr[2] ] );
  
  d=0.3;
  
  pts= arcPts(pqr, a=360, n=6);
  MarkPts(pts,Label=false, Line=["color","red"]);
  Red() MarkPt( pts[0], "arcPts(pqr,a=360,n=6)", Link= pts[0]+ [0,-1,0] );
  echo(len(pts), pts=pts);
  
  pts2= arcPts(pqr, a=45, a2=360, n=8);
  //Red() 
  MarkPts(pts2, Line=["color","green"]); 
  Green() MarkPt(pts2[1], "artPts(pqr,a=45,a2=360,n=6)"
                 //, Link= pts2[0]+ [-1,1,0]
                 ); 
  Gold()Plane(pqr);
  echo(len(pts2), pts2=pts2, last = last(pts2));
  pqr2=[[0.507374, -0.109894, 1.01141], [1.67, 0.73, 1.16], [1.28988, 1.37369, 0.495794]];
  //Green(0.2) MarkPts(pqr2,Label=["texta","pqr","scale",2],r=0.05);
  
}


//debug_arcPts_2();
module debug_arcPts_2()
{
  echom("debug_arcPts_2()");
  
  //echo(log_b("demo_arcPts"));
  pqr=pqr();
  Plane(pqr);
  pqr = [[1.31, -0.37, 0.3], [1.67, 0.73, 1.16], [-1.82, -1.3, 1.19]];
  //echo(log_(["pqr",pqr]));
  MarkPts( pqr, "PQR", Line=["r",0.03,"color","black"], Grid=["mode",2,"color","green"] );
  MarkPts([pqr[1],N(pqr)],Label=["text",["","N"]]);
  Mark90( [ N(pqr),pqr[1],pqr[0] ] );
  Mark90( [ N(pqr),pqr[1],pqr[2] ] );
  
  d=0.3;
  
//  pts= arcPts(pqr, a=360, n=36);
//  MarkPts(pts,Label=false);
//  MarkPt( pts[0], "arcPts(pqr)", Link= pts[0]+ [d,d,0] );
//  echo(len(pts), pts=pts);
  
  //pts2= arcPts(pqr, a=45, a2=360, n=36);
  pts2= arcPts(pqr, a=45, a2=360, n=6);
  //Red() 
  MarkPts(pts2, Line=["closed",1], Grid=2); //, Label=false);
  //MarkPt( pts2[0], "arcPts(pqr, rad=2)", Link= pts2[0]+ [d,d,0] );
  echo(len(pts2), pts2=pts2, last = last(pts2));
  pqr2=[[0.507374, -0.109894, 1.01141], [1.67, 0.73, 1.16], [1.28988, 1.37369, 0.495794]];
  Green(0.2) MarkPts(pqr2,Label=["texta","pqr","scale",2],r=0.05);
  
//  pts_r= arcPts(pqr, a=45, s2=45, a3=360, n=36);
//  Green() MarkPts(pts_r, Label=false);
//  MarkPt( pts_r[0], "arcPts(pqr, ratio=0.5, n=3)"
//        , Link= pts_r[0]+ [d,d,0] );
  
//  pts_a= arcPts(pqr, rad=2.5, a=75, n=10);
//  Blue() MarkPts(pts_a);
//  MarkPt( pts_a[0], "arcPts(pqr, rad=2.5, a=75, n=10)"
//        , Link= pts_a[0]+ [d,d,0] );
//        
//  pts_a2= arcPts(pqr, rad=3, a2=-45, n=4);
//  Purple() MarkPts(pts_a2);
//  MarkPt( last(pts_a2), "arcPts(pqr, rad=3, a2=-45, n=4)"
//        , Link= last(pts_a2)+ [d,d,0] );
//        
//  pts_aa2= arcPts(pqr, rad=3, a=30, a2=-45, n=4);
//  Purple() MarkPts(pts_aa2);
//  MarkPt( last(pts_aa2), "arcPts(pqr, rad=3, a=30, a2=-45, n=4)"
//        , Link= last(pts_aa2)+ [d,d,0] );
  //echo(log_e("demo_arcPts"));
  
}
