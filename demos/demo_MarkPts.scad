include <../scadx.scad>

//MarkPts_demo_default();
MarkPts_demo_Label();

//=======================================================
//
//. MarkPts()
//
//=======================================================
module MarkPts_test( ops ){ doctest(MarkPts,ops=ops);}

module MarkPts_demo_default()
{

  echom("MarkPts_demo_default()");
  
  MarkPts( pqr() );
  MarkPts( pqr() );  
  
}
//MarkPts_demo_default();

module MarkPts_demo_subobjs()
{

  echom("MarkPts_demo_subobjs()");
  
  pqr= pqr(d=1);
  MarkPts( pqr, "PQR");
  MarkPts( transN(pqr,0.5), Label=false, color="red" );
  MarkPts( transN(pqr,1),  "ABC", Ball=false, color="green" );
  MarkPts( transN(pqr,1.5), "DEF", Line=false, color="blue" );
  
  //MarkPts( pqr() );
}
//MarkPts_demo_subobjs();


module MarkPts_demo_Ball()
{
  echom("MarkPts_demo_Ball()");
  
  n=20;
  
  pts = [ for(i=[0:n]) [i/5,0,0.2]];
  MarkPts(pts,Label=false,Line=false);  
  
  pts2 = [ for(i=[0:n]) [i/5,0,0.4]];
  MarkPts(pts2,Label=false,Line=false, Ball=["dec_r",0]);  

  pts3 = [ for(i=[0:n]) [i/5,0,0.6]];
  MarkPts(pts3,Label=false,Line=false, Ball=["dec_r",0, "dec_t",0]); 
   
  pts4 = [ for(i=[0:n]) [i/5,0,0.8]];
  MarkPts(pts4,Label=false,Line=false, Ball=["dec_r",0, "dec_t",0,"color","teal"]);  
  
  pts5 = [ for(i=[0:n]) [i/5,0,1]];
  MarkPts(pts5,Label=false,Line=false, Ball=["color",["teal",0.3], "$fn", 36]);  
  
  pts6 = [ for(i=[0:n]) [i/5,0,1.2]];
  MarkPts(pts6,Label=false,Line=false, opt=["Ball",["color","olive", "$fn", 4]]); 
   
  pts7 = [ for(i=[0:n]) [i/5,0,1.4]];
  MarkPts(pts7,Label=false,Line=false, Ball=["r",0.15]);  
  //
  MarkPt_opt = ["Ball",false
               , "Label",["followView", true, "valign", "center", "scale", 0.5, "indent",0.1]];
  //
  MarkPt( [n/5,0,0.2], "1.default", opt=MarkPt_opt );
  MarkPt( [n/5,0,0.4], "2.Ball=[\"dec_r\",0]", opt=MarkPt_opt );
  MarkPt( [n/5,0,0.6], "3.Ball=[\"dec_r\",0, \"dec_t\",0]", opt=MarkPt_opt );
  MarkPt( [n/5,0,0.8], "4.Ball=[\"dec_r\",0, \"dec_t\",0,\"color\",\"teal\"]", opt=MarkPt_opt );
  MarkPt( [n/5,0,1.0], "5.Ball=[\"color\",[\"teal\",0.3], \"$fn\", 36]", opt=MarkPt_opt );
  MarkPt( [n/5,0,1.2], "6.opt=[\"Ball\",[\"color\",\"teal\", \"$fn\", 4]]", opt=MarkPt_opt );
  MarkPt( [n/5,0,1.4], "7.Ball=[\"r\",0.15]", opt=MarkPt_opt );
  
  //Line(pts);
}
//MarkPts_demo_Ball();

module MarkPts_demo_Label()
{

  echom("MarkPts_demo_Label()");
  echo(_red("Note:"));
  echo(_b(str("MarkPts(pts, ",_red("Label=true)"), "=> show indices [0,1,2...]")));
  echo(_b(str("MarkPts(pts, ",_red("Label=[\"text\",true])"), "=> show pts [[2.34, 0.12,1.01],[...],...]")));
  pqr1= pqr(); //d=1);
  
  texts=[ 
          "PQR"
        , true  
        , range(3)
        , [0,1]
        , ["text",range(3,6)]//,"color","red"]
        , ["text",["test1","test2","test3"], "halign","center"] //"actions", ["rotx",2]]//,"color","red"]
        , ["text",true, "halign","center", "actions", ["rotz",90]
            , "followView", false
            , "color", "red"]
        //, ["scale",2]
        
        ];
  
  for(i=range(texts))
  {
    echo(i=i, texts_i=texts[i]);
    //if (i==7)
   // MarkPts( transN(pqr1, i*1), opt=["label",["scale",2]], Ball=false, color=COLORS[i]
   //                             );
   // else
    MarkPts( transN(pqr1, i*1), Label=texts[i], Ball=false, color=COLORS[i]
                                );
                                
  }
   
}
//MarkPts_demo_Label();


module MarkPts_demo_more_points()
{
	echom( "MarkPts_demo_more_points" );
	MarkPts( randPts(30,d=5));
	
	MarkPts( randPts(15,d=2), Line=["color","red", "r",0.05, "$fn",24] );
}
//MarkPts_demo_more_points();


//module MarkPts_demo_plane()
//{
//    
//    echom( "MarkPts_demo_plane" );
//	
//    pqr2=pqr();
//    MarkPts( trslN(pqr2,-2), "pl");
//  
//    //pts2= app( pqr2, cornerPt(pqr2) );
//	//MarkPts( pts2,"l;pl=[color,red]");
////
//    //pts3= trslN( pts2, 3 );
//	//MarkPts( pts3,"l;pl=[th,0.5,color,green]");
//    //
//    //pts4= trslN( pts2, 6 );
//	//MarkPts( pts4,"l;pl=[th,0.5,color,blue,shift,1]");
//	
//    pts2= app( pqr2, cornerPt(pqr2) );
//	MarkPts( pts2,["l", "plane",["color","red"]]);
//
//    pts3= trslN( pts2, 3 );
//	MarkPts( pts3,["l","plane",["th",0.5,"color","green"]]);
//    
//    pts4= trslN( pts2, 6 );
//	MarkPts( pts4,["l", "plane",["th",0.5,"color","blue","shift",1]]);
//  
//  MarkPts( randPts(7, x=[-10,-5]), "l;ch;pl" );
//    
//}    
////MarkPts_demo_plane();


module MarkPts_demo_indices()
{
   echom("MarkPts_demo_indices");
   
   pts = cubePts([3,1,1]);
   MarkPts(pts, indices=[2,4]);
   MarkPts(pts, Line=false, indices=[1,3,5,7], color="red"
              , Ball=["r",0.15]
              );
   
   pts2= [ for(p=pts) p+[0,2,0] ];
   MarkPts(pts2, indices=[ [0:2], [5:7]]);
   
   pts3= [ for(p=pts2) p+[0,2,0] ];
   MarkPts(pts3, indices=[2:2:6], color="green");
   MarkPts(pts3, indices=[3,7], Line=false
            , Label=["text",true, "color","purple"]
            , Ball=["color","black"]
            );
      
}
//MarkPts_demo_indices();

module MarkPts_demo_art()
{
   echom("MarkPts_demo_art");
   //pqr=pqr();
   pqr= [[-2.23957, -2.90208, -1.82225], [-0.555392, 1.5994, 0.622352]
        , [-2.26106, -2.04128, 1.27974]];
   echo(pqr=pqr);
   
   pts = circlePts( pqr, 5, 96);
   color("red")Line(pts, closed=true);
   MarkPts(pts, Label=false
              , Ball=false
              , Grid=["mode", 1, "r", 0.02 ]
                );

}
//MarkPts_demo_art();

