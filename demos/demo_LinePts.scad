include <../scadx.scad>

ISLOG=1;

module demo_linePts_default()
{
   echom("demo_linePts_default()");
   pqr =  [[-0.85, -1.57, 1.42], [0.63, -0.67, 0.55], [-1.88, 0.88, -1.51]];
   
   data = linePts(site=pqr, isReturnHash=true);
   _olive( _b("linePts(isReturnHash=true)" ));
   _olive( "Set <b>isReturnHash=true</b> to return a hash containing pts, site and other info" );
   _olive( str("hash keys = ", _b(keys(data)) ));
   //echo(data=data);
   //site= h(data, "site");
   //echo( pqr = site );
   MarkPts( pqr, "PQR" );
   
   data2= linePts(site=pqr, nseg=4);
   _red( _b("linePts()  --- this returns pts"));
   Line( data2, r=0.25, color=["red",0.3] );
   Line( h(data, "pts"), r=0.5, color=0.25);
   
}
demo_linePts_default();



module demo_linePts_len()
{
   echom("demo_linePts_len()");
   _red("Set len=num for fixed seg len");
   _olive( "len= 3" );
   _red( "len = 2.5" );
   _green( "len = 2 " );
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",4, "a",120];
   
   pts = linePts(site=pqr, len=3, opt=opt);
   pts2 = linePts(site=pqr, len=2.5, opt=opt);
   pts3 = linePts(site=pqr, len=2, opt=opt);
   MarkPts( pqr, "PQR" );
   
   Line( pts3, r=0.3, color="green");
   Line( pts2, r=0.4, color=["red",0.3]);
   Line( pts, r=0.5, color=0.25);
}
//demo_linePts_len();



module demo_linePts_len_range()
{
   echom("demo_linePts_len_range()");
   _red("Set len=[a,b] for seg lens varying between a and b");
   _red("Note that segs have different lens");
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",4, "a",120, "isReturnHash", true 
       ];
   
   data = linePts(site=pqr, len=[2,4], opt=opt);
   data2 = linePts(site=pqr, len=[2,3], opt=opt);
   data3 = linePts(site=pqr, len=[1,2], opt=opt);
   
   _olive( str("len= ", h(data, "len")) );
   _red( str("len= ", h(data2, "len")) );
   _green( str("len= ", h(data3, "len")) );
   
   MarkPts( pqr, "PQR" );
   
   //echo(data = data);
   Line( h(data3,"pts"), r=0.3, color="green");
   Line( h(data2,"pts"), r=0.4, color=["red",0.3]);
   Line( h(data,"pts"), r=0.5, color=0.25);
}
//demo_linePts_len_range();



module demo_linePts_lens()
{
   echom("demo_linePts_lens()");
   _red("Set lens=arr for predefined seg lens");
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",4, "a",120, "isReturnHash", true 
       ];
   
   data = linePts(site=pqr, lens=[5,4,3,2], opt=opt);
   data2 = linePts(site=pqr, lens=[3.5,1.5,3.5,1.5], opt=opt);
   data3 = linePts(site=pqr, lens=range(1,5), opt=opt);
   
   _olive( str("lens= ", h(data, "lens")) );
   _red( str("lens= ", h(data2, "lens")) );
   _green( str("lens= ", h(data3, "lens")) );
   
   MarkPts( pqr, "PQR" );
   
   //echo(data = data);
   Line( h(data3,"pts"), r=0.3, color="green");
   Line( h(data2,"pts"), r=0.4, color=["red",0.3]);
   Line( h(data,"pts"), r=0.5, color=0.25);
}
//demo_linePts_lens();



module demo_linePts_a()
{
   echom("demo_linePts_a()");
   _red("Set a=num for a fixed angle")
   _red(" All on the plane of 'site' since a2=0");
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",4, "len",2 , "isReturnHash", true 
       ];
   
   data = linePts(site=pqr, a=150, opt=opt);
   data2 = linePts(site=pqr, a=120, opt=opt);
   data3 = linePts(site=pqr, a=100, opt=opt);
   
   _olive( str("a= ", h(data, "a")) );
   _red( str("a= ", h(data2, "a")) );
   _green( str("a= ", h(data3, "a")) );
   
   MarkPts( pqr, "PQR" );
   
   //echo(data = data);
   Line( h(data3,"pts"), r=0.3, color="green");
   Line( h(data2,"pts"), r=0.4, color=["red",0.3]);
   Line( h(data,"pts"), r=0.5, color=0.25);
}
//demo_linePts_a();


module demo_linePts_a_range()
{
   echom("demo_linePts_a_range()");
   _red("Set a=[c,d] for angle a varying between c and d");
   _red("All on the plane of 'site'");
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",4, "len",2 , "isReturnHash", true 
       ];
   
   data = linePts(site=pqr, a=[130,150], opt=opt);
   data2 = linePts(site=pqr, a=[100,120], opt=opt);
   data3 = linePts(site=pqr, a=[70,100], opt=opt);
   
   _olive( str("a= ", h(data, "a")) );
   _red( str("a= ", h(data2, "a")) );
   _green( str("a= ", h(data3, "a")) );
   
   MarkPts( pqr, "PQR" );
   
   //echo(data = data);
   Line( h(data3,"pts"), r=0.3, color="green");
   Line( h(data2,"pts"), r=0.4, color=["red",0.3]);
   Line( h(data,"pts"), r=0.5, color=0.25);
}
//demo_linePts_a_range();



module demo_linePts_as()
{
   echom("demo_linePts_as()");
   _red("Set as=arr for predefined a's");
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",4, "len",3, "a",120, "isReturnHash", true 
       ];
   
   data = linePts(site=pqr, as=[135, 225,135], opt=opt);
   data2 = linePts(site=pqr, as=[91,225,-91], opt=opt);
   //data3 = linePts(site=pqr, lens=range(1,5), opt=opt);
   
   _olive( str("lens= ", h(data, "lens")) );
   _red( str("lens= ", h(data2, "lens")) );
   //_green( str("lens= ", h(data3, "lens")) );
   
   MarkPts( pqr, "PQR" );
   
   //echo(data = data);
   //Line( h(data3,"pts"), r=0.3, color="green");
   Line( h(data2,"pts"), r=0.4, color=["red",0.3]);
   Line( h(data,"pts"), r=0.5, color=0.25);
}
//demo_linePts_as();



module demo_linePts_a2()
{
   echom("demo_linePts_a2()");
   _red("Set a2=num for a fixed angle in addition and perpendicular to angle a");
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",4, "len",2 , "a", 120, "isReturnHash", true 
       ];
   
   data = linePts(site=pqr, a2=0, opt=opt);
   data2 = linePts(site=pqr, a2=30, opt=opt);
   data3 = linePts(site=pqr, a2=90, opt=opt);
   
   _olive( str("a2= ", h(data, "a2")) );
   _red( str("a2= ", h(data2, "a2")) );
   _green( str("a2= ", h(data3, "a2")) );
   
   MarkPts( pqr, "PQR" );
   
   //echo(data = data);
   Line( h(data3,"pts"), r=0.3, color="green");
   Line( h(data2,"pts"), r=0.4, color=["red",0.3]);
   Line( h(data,"pts"), r=0.5, color=0.25);
}
//demo_linePts_a2();



module demo_linePts_a2_range()
{
   echom("demo_linePts_a2_range()");
   _red("Set a2=[c,d] for varying a2 angle");
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",4, "len",2 , "a", 120, "isReturnHash", true 
       ];
   
   data = linePts(site=pqr, a2=[0,20], opt=opt);
   data2 = linePts(site=transN(pqr,1.5), a2=[30,90], opt=opt);
   data3 = linePts(site=transN(pqr,3), a2=[-90,90], opt=opt);
   
   _olive( str("a2= ", h(data, "a2")) );
   _red( str("a2= ", h(data2, "a2")) );
   _green( str("a2= ", h(data3, "a2")) );
   
   MarkPts( pqr, "PQR" );
   
   //echo(data = data);
   Line( h(data3,"pts"), r=0.3, color="green");
   Line( h(data2,"pts"), r=0.4, color=["red",0.3]);
   Line( h(data,"pts"), r=0.5, color=0.25);
}
//demo_linePts_a2_range();



module demo_linePts_app_polygon()
{
   echom("demo_linePts_app_polygon()");
   //_red("Set a2=[c,d] for varying a2 angle");
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",4, "len",3 , "a", 120, "isReturnHash", true 
       ];
   
   data = linePts(site=pqr, nseg=2, a=(3-2) * 180 / 3, opt=opt);
   data2 = linePts(site=transN(pqr,2), nseg=3, a=(4-2) * 180 / 4, opt=opt);
   data3 = linePts(site=transN(pqr,4), nseg=4, a=(5-2) * 180 / 5, opt=opt);
   data4 = linePts(site=transN(pqr,6), nseg=5, a=(6-2) * 180 / 6, opt=opt);
   
   MarkPts( pqr, "PQR" );
   
   //echo(data = data);
   Line( h(data4,"pts"), r=0.3, color="blue", closed=true);
   Line( h(data3,"pts"), r=0.3, color="green", closed=true);
   Line( h(data2,"pts"), r=0.4, color=["red",0.3], closed=true);
   Line( h(data,"pts"), r=0.5, color=0.25, closed=true);
}
//demo_linePts_app_polygon();



module demo_linePts_app_spring()
{
   echom("demo_linePts_app_spring()");
   //_red("Set a2=[c,d] for varying a2 angle");
   
   pqr=[[1.07, -1.13, -1.69], [-0.17, -0.26, 0.12], [-1.28, -0.062, -1.52]];
   //echo( pqr=pqr );
   opt=["nseg",40, "len",1 , "a", 120, "a2", 5, "isReturnHash", true 
       ];
   
   data = linePts(site=pqr, opt=opt);
   data2 = linePts(site=transP(pqr,2.75), lens=range(0.5,0.025,20), opt=opt);
   data3 = linePts(site=transP(pqr,6), nseg=40
                                     , lens=concat( range(0.5,0.05, n=20)
                                                  , range(0.5+20*0.05, -0.05, n=20)
                                                  )
                                     , opt=opt);
   data4 = linePts(site=transP(pqr,10.5), nseg=40
                                     , lens=[ for(i=range(20)) 0.5+20*0.05-i*0.05
                                            , for(i=range(20)) 0.5+i*0.05
                                            ], opt=opt);
   //echo(data=data);
   
   
   _olive( str("lens= ", h(data, "lens")) );
   _red( "lens= range(0.5,0.025,20)" );
   
   MarkPts( pqr, "PQR" );
   
   Line( h(data,"pts"), r=0.1);
   Line( h(data2,"pts"), r=0.1, color="red");
   Line( h(data3,"pts"), r=0.1, color="green");
   Line( h(data4,"pts"), r=0.1, color="blue");
}
//demo_linePts_app_spring();




