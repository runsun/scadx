include <../scadx.scad>

ISLOG=1;
/*
function getBezierPtsAt(  // Return Cubic Bezier pts from 
                          // pts[i] to pts[i+1].
   pts
   , i //=1 
   , n //=6 // extra pts to be added 
   , closed //=false
   , ends  //= [true,true] to include both end pts
   , aH //= undef //[1,1]
   , dH  //= undef //[0.5,0.5]
   , opt=[]
*/   

module demo_getBezierPtsAt()
{
   echom("demo_getBezierPtsAt()");
   //pts = randPts(5);
   pts=[[0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2], [2.51, -0.65, 0.81], [2.27, -1.95, -0.75]];
   //echo(pts=pts);
   
   h0 = getBezierPtsAt(pts,0);
   echo(h0_keys=keys(h0));
   Red() MarkPts( h(h0, "bzpts") );
   
   h1 = getBezierPtsAt(pts,1, n=8);
   Green() MarkPts( h(h1, "bzpts"));
   
   h2 = getBezierPtsAt(pts,2,n=8);
   Blue() MarkPts( h(h2, "bzpts"));
   
   h3 = getBezierPtsAt(pts,3,n=3);
   Purple() MarkPts( h(h3, "bzpts"));
   
   MarkPts(pts, color=0.5, Line=0.1);
   
}
//demo_getBezierPtsAt();

//. dH

module demo_getBezierPtsAt_dH()
{
   echom("demo_getBezierPtsAt_dH()");
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   h0 = getBezierPtsAt(pts,0);
   _purple( _h3("When in the end segments (the 1st or last), ratio dHi/dHj doesn't <br/> 
                  seem to matter (all pts on the same plane),
                  But dHi+dHj does") );
   
   _red(_s("dH = default (={_})", str(h(h0,"dH"))));
   
   //echo( h0= _fmth(h0));
   Red() MarkPts( h(h0, "bzpts") );
   
   h01 = getBezierPtsAt(pts,0, dH=.9);
   _green(_s("dH = {_}", str(h(h01,"dH"))));
   //echo( h01= _fmth(h01));
   Green(0.5) MarkPts( h(h01, "bzpts") );
   
   h02 = getBezierPtsAt(pts,0, dH=1.2);
   _blue(_s("dH = {_}", str(h(h02,"dH"))));
   //echo( h02= _fmth(h02));
   Blue(0.5) MarkPts( h(h02, "bzpts") );
   
   h03 = getBezierPtsAt(pts,0, dH=[.2,1]);
   _purple(_s("dH = {_}", str(h(h03,"dH"))));
   //echo( h02= _fmth(h02));
   Purple(0.5) MarkPts( h(h03, "bzpts") );
   
   Black() MarkPts(pts, "PQRS", Line=0.025);   
}
//demo_getBezierPtsAt_dH();



module demo_getBezierPtsAt_dH2()
{
   echom("demo_getBezierPtsAt_dH2()");
   
   _purple( _h3("Align the red loop w/ your eye sight to see <br/>
                that the purple line drifts away due to hight dHj. <br/>
                NOTE: the height of loop is decided by dHi+dHj") );
   
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   h0 = getBezierPtsAt(pts,1);
   _red(_s("dH = {_}  (=default), sum={_}", [str(h(h0,"dH")), sum(h(h0,"dH"))]));
   //echo( h0= _fmth(h0));
   Red() MarkPts( h(h0, "bzpts") );
   
   h01 = getBezierPtsAt(pts,1, dH=.9);
   _green(_s("dH = {_}, sum={_}", [str(h(h01,"dH")), sum(h(h01,"dH"))]));
   //echo( h01= _fmth(h01));
   Green(0.5) MarkPts( h(h01, "bzpts") );
   
   h02 = getBezierPtsAt(pts,1, dH=1.2);
   _blue(_s("dH = {_}, sum={_}"
        , [str(h(h02,"dH")), sum(h(h02,"dH"))]));
   //echo( h02= _fmth(h02));
   Blue(0.5) MarkPts( h(h02, "bzpts") );
   
   h03 = getBezierPtsAt(pts,1, dH=[0.2,1]);
   _purple(_s("dH = {_}, sum={_} // Note the next edge is more bending power than prior edge"
          , [str(h(h03,"dH")), sum(h(h03,"dH"))]));
   //echo( h02= _fmth(h02));
   Purple(0.5) MarkPts( h(h03, "bzpts") );   
   
   ////===========================================================
   ////  Drawing ref. plane and line for easier observation
   
   P=pts[0]; Q=pts[1]; R=pts[2]; S=pts[3];
   Jp = projPt( P, [Q,R]);
   Js = projPt( S, [Q,R]);
   twist_a = angle( [P, Jp, Jp+S-Js]);
   echo(twist_a = twist_a);
   
   abPt = angleBisectPt([P, Jp, Jp+S-Js]); /// this is on the plane that 
                                           /// equally divide angles
   M = sum( [Q,R] )/2;
   abPt2 = anglePt( [Q, M, abPt], a=90 ); /// Adj angle to 90 
   Red() MarkPts( [abPt2, 3*M-2*abPt2], Label=false );
   
   /// Now make plane pts:
   Q1= Q + 1.5*(M - abPt2);
   R1= R + 1.5*(M - abPt2);
   Plane([ Q,Q1,R1,R] );  
   ////===========================================================

   
   Black() MarkPts(pts, "PQRS", Line=0.025);
   
}
//demo_getBezierPtsAt_dH2();



module demo_getBezierPtsAt_varying_dHj()
{
   echom("demo_getBezierPtsAt_varying_dHj()");
   _purple( _h3("Fixed dHi, varying dHj") );
   
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   h0 = getBezierPtsAt(pts,1, dH=.5);
   _red(_s("dH = {_} ", str(h(h0,"dH"))));
   //echo( h0= _fmth(h0));
   Red() MarkPts( h(h0, "bzpts") );
   
   h01 = getBezierPtsAt(pts,1, dH=[.5,1]);
   _green(_s("dH = {_}", str(h(h01,"dH"))));
   //echo( h01= _fmth(h01));
   Green(0.5) MarkPts( h(h01, "bzpts") );
   
   h02 = getBezierPtsAt(pts,1, dH=[.5,2]);
   _blue(_s("dH = {_} "
        , str(h(h02,"dH"))));
   //echo( h02= _fmth(h02));
   Blue(0.5) MarkPts( h(h02, "bzpts") );
   
   h03 = getBezierPtsAt(pts,1, dH=[0.5,3]);
   _purple(_s("dH = {_} "
          , str(h(h03,"dH"))));
   //echo( h02= _fmth(h02));
   Purple(0.5) MarkPts( h(h03, "bzpts") );   
   
   ////===========================================================
   ////  Drawing ref. plane and line for easier observation
   
   P=pts[0]; Q=pts[1]; R=pts[2]; S=pts[3];
   Jp = projPt( P, [Q,R]);
   Js = projPt( S, [Q,R]);
   twist_a = angle( [P, Jp, Jp+S-Js]);
   echo(twist_a = twist_a);
   
   abPt = angleBisectPt([P, Jp, Jp+S-Js]); /// this is on the plane that 
                                           /// equally divide angles
   M = sum( [Q,R] )/2;
   abPt2 = anglePt( [Q, M, abPt], a=90 ); /// Adj angle to 90 
   MarkPts( [abPt2, 3*M-2*abPt2], Label=false );
   
   /// Now make plane pts:
   Q1= Q + 1.5*(M - abPt2);
   R1= R + 1.5*(M - abPt2);
   Plane([ Q,Q1,R1,R] );  
   ////===========================================================

   
   Black() MarkPts(pts, "PQRS", Line=0.05);
   
}
//demo_getBezierPtsAt_varying_dHj();



module demo_getBezierPtsAt_constant_sum_dH()
{
   echom("demo_getBezierPtsAt_constant_sum_dH()");
   _purple( _h3("Keep dHi+dHj constant") );
                
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   n=10;
   
   h0 = getBezierPtsAt(pts,1, n=n, dH=[1.4,.6]);
   _red(_s("dH = {_} ", str(h(h0,"dH"))));
   //echo( h0= _fmth(h0));
   Red() MarkPts( h(h0, "bzpts"), Label=false );
   
   h01 = getBezierPtsAt(pts,1, n=n, dH=[1.2,.8]);
   _green(_s("dH = {_}", str(h(h01,"dH"))));
   //echo( h01= _fmth(h01));
   Green(0.5) MarkPts( h(h01, "bzpts"), Label=false );
   
   h02 = getBezierPtsAt(pts,1, n=n, dH=[1,1]);
   _blue(_s("dH = {_} "
        , str(h(h02,"dH"))));
   //echo( h02= _fmth(h02));
   Blue(0.5) MarkPts( h(h02, "bzpts"), Label=false );
   
   h03 = getBezierPtsAt(pts,1, n=n, dH=[.8,1.2]);
   _purple(_s("dH = {_} "
          , str(h(h03,"dH"))));
   //echo( h02= _fmth(h02));
   Purple(0.5) MarkPts( h(h03, "bzpts"), Label=false );   
   
   ////===========================================================
   ////  Drawing ref. plane and line for easier observation
   
   P=pts[0]; Q=pts[1]; R=pts[2]; S=pts[3];
   Jp = projPt( P, [Q,R]);
   Js = projPt( S, [Q,R]);
   twist_a = angle( [P, Jp, Jp+S-Js]);
   echo(twist_a = twist_a);
   
   abPt = angleBisectPt([P, Jp, Jp+S-Js]); /// this is on the plane that 
                                           /// equally divide angles
   M = sum( [Q,R] )/2;
   abPt2 = anglePt( [Q, M, abPt], a=90 ); /// Adj angle to 90 
   MarkPts( [abPt2, 3*M-2*abPt2], Label=false );
   
   /// Now make plane pts:
   Q1= Q + 1.5*(M - abPt2);
   R1= R + 1.5*(M - abPt2);
   Plane([ Q,Q1,R1,R] );  
   ////===========================================================

   Black() MarkPts(pts, "PQRS", Line=0.05);
   
}
//demo_getBezierPtsAt_constant_sum_dH();



module demo_getBezierPtsAt_dH_spindle()
{
   echom("demo_getBezierPtsAt_dH_spindle()");
   _purple( _h3("Make a spindle. spindle_size = dHi+dHj= constant") );
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   
   spindle_size=2;
   nThread=30;
   nBzPts_per_thread=20;   
   thread_range= range( 0.2, 0.05, n= nThread);
   
   for( dHi = thread_range )
   {
     h0 = getBezierPtsAt( pts,1
                        , n=nBzPts_per_thread
                        , dH=[dHi, spindle_size-dHi]);
     Line( h(h0, "bzpts"));
   }   
   
   //Black() MarkPts(pts, "PQRS", Line=0.05);
   
}
//demo_getBezierPtsAt_dH_spindle();

module demo_getBezierPtsAt_varying_dHj()
{
   echom("demo_getBezierPtsAt_varying_dHj()");
   _purple( _h3("Make a spindle. dHi fixed at 1, dHj =[-1,2]") );
   pts0=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   
   dHi=1;
   nThread=30;
   nBzPts_per_thread=20;   
   thread_range= range( -1, 0.1, n= nThread);
   
//   for( dHj = thread_range )
//   {
//     h0 = getBezierPtsAt( pts0,1
//                        , n=nBzPts_per_thread
//                        , dH=[dHi, dHj]);
//     Line( h(h0, "bzpts"));
//   }   
   ptss = [ for(dHj=thread_range)
            h(getBezierPtsAt( pts0
                            , 1
                              , n= nBzPts_per_thread
                              , dH= [dHi, dHj]
                              )                              
             ,"bzpts")
         ];
   
   PlotMesh( ptss, Line=true );
         
   //Black() MarkPts(pts, "PQRS", Line=0.05);
   Black() for(i=range(nThread))
   MarkPts( pts0, "PQRS", Line=0.025);   

}
//demo_getBezierPtsAt_varying_dHj();



module demo_getBezierPtsAt_dH_spindle_plane()
{
   echom("demo_getBezierPtsAt_dH_spindle_plane()");
   _purple( _h3("Make a spindle plane. spindle_size = dHi+dHj= constant") );
   
   pts0=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   
   spindle_size=2; // how fat the spindle is
   nBzPts_per_thread=20;   
   nThread=30;
   
   thread_range= range( 0.2, 0.05, n= nThread);
   ptss = [ for(x=thread_range)
            h(getBezierPtsAt( pts0, 1
                              , n= nBzPts_per_thread
                              , dH= [x, spindle_size-x]
                              )
             ,"bzpts")
         ];         
   
   PlotMesh( ptss, Line_y=true );
   
}
//demo_getBezierPtsAt_dH_spindle_plane();



module demo_getBezierPtsAt_dH_spindle_plane_extended()
{
   echom("demo_getBezierPtsAt_dH_spindle_plane_extended()");
   _purple( _h3("Spreading the base (pts0) to extend the spindle plane. spindle_size=dHi+dHj remains constant") );
   
   pts0=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   ////===========================================================
   ////  Drawing ref. plane and line for easier observation
   
   P=pts0[0]; Q=pts0[1]; R=pts0[2]; S=pts0[3];
   Jp = projPt( P, [Q,R]);
   Js = projPt( S, [Q,R]);
   twist_a = angle( [P, Jp, Jp+S-Js]);
   echo(twist_a = twist_a);
   
   abPt = angleBisectPt([P, Jp, Jp+S-Js]); /// this is on the plane that 
                                           /// equally divide angles
   M = sum( [Q,R] )/2;
   abPt2 = anglePt( [Q, M, abPt], a=90 ); /// Adj angle to 90 
   MarkPts( [abPt2, 3*M-2*abPt2] );
   
   /// Now make plane pts:
   Q1= Q + 1.5*(M - abPt2);
   R1= R + 1.5*(M - abPt2);
   //Plane([ Q,Q1,R1,R] );  
   
   ////===========================================================
   
   // We will move the pts0 incrementally with this vector:
   base_moving_line= normalLn([abPt2,projPt(abPt2,[Q,R]),R], len=0.15);
   Red() Arrow( base_moving_line);
   base_moving_v=base_moving_line[1]-base_moving_line[0];  
   ////===========================================================
   
   spindle_size=2; // how fat the spindle is
   nBzPts_per_thread=20;   
   nThread=40;
   
   thread_range= range( 0.5, 0.05, n= nThread);
   //echo(thread_range = thread_range);
   //echo(thread_range = len(thread_range));
   ptss = [ for(i= range(thread_range))
            h(getBezierPtsAt( [for(p=pts0)p+ base_moving_v*i]
                            , 1
                              , n= nBzPts_per_thread
                              , dH= [ thread_range[i]
                                    , spindle_size-thread_range[i]
                                    ]
                              )                              
             ,"bzpts")
         ];
         
   //pts = [for(arr=ptss) each arr];   
   
   PlotMesh( ptss, Line=true); //_y=0.02, Line_x=["color","blue"] ); //, Line=true );
   
   //Red(.5)for(th=ptss) Line(th, r=0.01);
   Black() for(i=range(nThread))
   MarkPts( pts0, "PQRS", Line=0.025);   

}
//demo_getBezierPtsAt_dH_spindle_plane_extended();



//. aH


module demo_getBezierPtsAt_aH_endseg()
{
   echom("demo_getBezierPtsAt_aH_endseg()");
   _purple( _h3("Set dH=0.75 and varying aH in end segments (i=0 or i=len(pts)-2): 
                 <br/>   getBezierPtsAt( pts, i=0, dH=.75, aH=x )
                 <br/> which will give aH=[x,x]  
                 <br/> Note-1: In end segments like this, all loops are on the same plane 
                 <br/> Note-2: aH gets larger, the loop will swing back (teal) 
                 ") );
   
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   nBzPts_per_thread=20;   
   
   data=[ ["olive", 0.25, ""]
        , ["red",   0.5,  "= default"]
        , ["green", 0.75, ""]
        , ["blue",  1  ,  " // aH=1, the loop is a straight extension of neighbor segments (r u sure?)"]
        , ["purple",1.5, ""]
        , ["orange", 2,   ""]
        , ["teal",   4,   ""]
        ]; 
   
   for(d=data)
   {
     color= isstr(d[0])?d[0]:d[0][0]; 
     aH= d[1]; cmt= d[2];
     hash= getBezierPtsAt(pts, 0, n=nBzPts_per_thread, dH=.75, aH=aH);
     str = str(_s( "aH ={_}", str(h(hash,"aH"))),d[2]);
     _color( str, color);
     Color(color)
     MarkPts( h(hash, "bzpts"), Label=false, Ball=false );   
   }      
   Black() MarkPts(pts, "PQRS", Line=0.05);  
   
}
//demo_getBezierPtsAt_aH_endseg();



module demo_getBezierPtsAt_aH_endseg_2()
{
   echom("demo_getBezierPtsAt_aH_endseg_2()");
   _purple( _h3("Set dH=0.75, aHi=0.25 and varying aHj in end segments: <br/>
                   getBezierPtsAt( pts, i=0, dH=.75, aH= [0.25,x] ) <br/>
                ") );
   
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   nBzPts_per_thread=20;   
   
   data=[ ["olive", 0.25, ""]
        , ["red",   [0.25,0.75],""]
        , ["green", [0.25,1], " => Straight extension from neighbor seg"]
        , ["blue",  [0.25,1.5],  " // Crossing over PQ line"]
        , ["purple",[0.25,2], " // On the other side of PQ"]
        , ["orange",[0.25,2.5], " On the other side of PQ"]
        , ["teal",[0.25,2.8], " // Crossing back again"]
        ]; 
   
   for(d=data)
   {
     color= isstr(d[0])?d[0]:d[0][0]; 
     aH= d[1]; cmt= d[2];
     hash= getBezierPtsAt(pts, 0, n=nBzPts_per_thread, dH=.75, aH=aH);
     str = str(_s( "aH={_}", str(h(hash,"aH"))),d[2]);
     _color( str, color);
     Color(color)
     MarkPts( h(hash, "bzpts"), Label=false, Ball=false );   
   }      
   Black() MarkPts(pts, "PQRS", Line=0.05);  
   
}
//demo_getBezierPtsAt_aH_endseg_2();

module demo_getBezierPtsAt_aH_endseg_3_1()
{
   echom("demo_getBezierPtsAt_aH_endseg_3_1()");
   _purple( _h3("Set dH=0.75, aHi=0.25 and varying aHj in end segments: <br/>
                   getBezierPtsAt( pts, i=0, dH=.75, aH= [.25,x] ) <br/>
                   -- The teal line shows the path of the 4th pts. <br/>
                   Note how it brings the loop around and back after aHj>2.6 <br/>
                   Increasing aHi doesn't seem to affect how aHj behaves.
                ") );
   
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; 
   
   nBzPts_per_thread=20;   
   nThread = 14;
      
   aHi = .25;
   aHjs = range( 0, 0.2, n=nThread);
   ptss = [ for( aHj = aHjs )
            h( getBezierPtsAt(pts, 0, n=nBzPts_per_thread, dH=.75, aH=[aHi,aHj])
             , "bzpts"
             )
          ];
   PlotMesh( ptss, Line_y=true, Plane=false );
   
   4th_pts = [ for( th = ptss ) last(th,4) ];
   Teal()
   MarkPts(4th_pts, Label=["text",aHjs, "scale",0.75]
          , Ball=["r",0.025]);    
    
   Black() MarkPts(pts, "PQRS", Line=0.025);  
   
}
//demo_getBezierPtsAt_aH_endseg_3_1();



module demo_getBezierPtsAt_aH_endseg_3_2()
{
   echom("demo_getBezierPtsAt_aH_endseg_3_2()");
   _purple( _h3("Set dH=0.75, aHi=0.75 and varying aHj in end segments: <br/>
                   getBezierPtsAt( pts, i=0, dH=.75, aH= [.75,x] ) <br/>
                   -- The blue line shows the path of the 4th pts. <br/>
                   Note how it brings the loop around and back after aHj>2.6 <br/>
                   Increasing aHi doesn't seem to affect how aHj behaves.
                ") );
   
   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; 
   
   nBzPts_per_thread=20;   
   nThread = 14;
      
   aHi = .75;
   aHjs = range( 0, 0.2, n=nThread);
   ptss = [ for( aHj = aHjs )
            h( getBezierPtsAt(pts, 0, n=nBzPts_per_thread, dH=.75, aH=[aHi,aHj])
             , "bzpts"
             )
          ];
   PlotMesh( ptss, Line_y=true, Plane=false );
   
   4th_pts = [ for( th = ptss ) last(th,4) ];
   
   Blue()
   MarkPts(4th_pts, Label=["text",aHjs, "scale",0.5]
          , Ball=["r",0.025]);    
    
   Black() MarkPts(pts, "PQRS", Line=0.025);  
   
}
//demo_getBezierPtsAt_aH_endseg_3_2();



module demo_getBezierPtsAt_aH_midseg()
{
   echom("demo_getBezierPtsAt_aH_midseg()");
   _purple( _h3("Set dH=0.75 and varying aH in mid segments: <br/>
                   getBezierPtsAt( pts, i=1, dH=.75, aH=x ) <br/>
                 which will give aH=[x,x] <br/>  
                 All loops are NOT on the same plane") );

   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; //, [2.27, -1.95, -0.75]];
   
   
   nBzPts_per_thread=20;   
   
   data=[ ["olive", 0.25, ""]
        , ["red",   0.5,  ""]
        , ["green", 0.75, ""]
        , ["blue",  1  ,  " // aH=1, the loop is a straight extension of neighbor segments"]
        , ["purple",1.5, ""]
        , ["orange",  2,   ""]
        ]; 
   
   for(d=data)
   {
     color= isstr(d[0])?d[0]:d[0][0]; 
     aH= d[1]; cmt= d[2];
     hash= getBezierPtsAt(pts, 1, n=nBzPts_per_thread, dH=.75, aH=aH);
     str = str(_s( "aH={_}", str(h(hash,"aH"))),d[2]);
     _color( str, color);
     Color(color)
     MarkPts( h(hash, "bzpts"), Label=false, Ball=false );   
   }
      
   Black() MarkPts(pts, "PQRS", Line=0.05);   
}
//demo_getBezierPtsAt_aH_midseg();




module demo_getBezierPtsAt_aH_midseg_spread()
{
   echom("demo_getBezierPtsAt_aH_midseg_spread()");
   _purple( _h3("Set dH=0.75 and varying aH in mid segments: <br/>
                   getBezierPtsAt( pts, i=1, dH=.75, aH=x ) <br/>
                 which will give aH=[x,x] <br/>  
                 All loops are NOT on the same plane") );

   pts=[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92], [0.52, -1.45, 2.2]
       , [2.51, -0.65, 0.81]]; 
       
   nBzPts_per_thread=20;   
   nThread = 14;
      
   aHi = .75;
   aHjs = range( 0, 0.2, n=nThread);
   ptss = [ for( aHj = aHjs )
            h( getBezierPtsAt(pts, 1, n=nBzPts_per_thread, dH=1, aH=aHj) //[aHi,aHj])
             , "bzpts"
             )
          ];
   PlotMesh( ptss, Line_y=["r",0.02], Plane=.8 );
   
   4th_pts = [ for( th = ptss ) last(th,4) ];
   Blue()
   MarkPts(4th_pts, Label=["text",aHjs, "scale",0.75, "color","red"]
          , Ball=["r",0.025]);    
    
   Black() MarkPts(pts, "PQRS", Line=0.025);  
}
//demo_getBezierPtsAt_aH_midseg_spread();

//. dH aH


module demo_getBezierPtsAt_app()
{
   echom("demo_getBezierPtsAt_app()");
   _purple( _h3("Application of getBezierPtsAt on making a mesh") );
   
   pts0 =[ [0.87, 2.42, 1.34], [0.97, 1.75, -1.92]
         , [0.52, -1.45, 2.2], [0.79, 1.87, 2.05]];
         
   P=pts0[0]; Q=pts0[1]; R=pts0[2]; S=pts0[3];   
   
   // X: length = thread_intv * nThread
   //    resolution = inc nThread and dec thread_intv
   // Y: length  = dist(Q,R)
   //    resolution = nBzPts_per_thread
   // Height (z) = height
    
   thread_intv = 0.1;  // dist btw neighboring thread;
   nThread=60;
   
   height=.75; 
   nBzPts_per_thread=30;   
   
   aH_beg= -1.4; // why this number makes a cycle sin-like wave ? 
   aH_end= 1.4;
   aH_span = aH_end-aH_beg;
   
   aHs = range(aH_beg, aH_span/nThread, aH_beg+aH_span); //+aH_span/nThread);
   
   M = sum( [Q,R] )/2;
   Nm = normalLn([Q,M,P], len=thread_intv);
   // Move the pts0 incrementally with this vector:
   base_moving_line= Nm; 
   base_moving_v=base_moving_line[1]-base_moving_line[0];  
   ////===========================================================
   
   
   ptss = [ for(i= range(nThread))
            //echo( i=i, aH=aHs[i] )
            h(getBezierPtsAt( [for(p=pts0)p+ base_moving_v*i]
                            , 1
                              , n= nBzPts_per_thread
                              , dH= height
                              , aH= [aHs[i], aHs[i]]      
                              )                              
             ,"bzpts")
         ];
         
   PlotMesh( ptss ); 
   //Black() for(i=range(nThread))
   //        MarkPts( pts0, "PQRS", Line=0.025);   

}
demo_getBezierPtsAt_app();
