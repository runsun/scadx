include <../scadx.scad>

demo_decoPolyEdge_how();
module demo_decoPolyEdge_how()
{  
   echom("demo_decoPolyEdge_how()");
   temp= to3d( [ [-.5,0], [-.5,-0.1],[-.2,-.1],  [0,-.5] ] );
   pts = addy(cubePts(2),0.5);
   faces = rodfaces(4);
   MarkPts(pts, Line=false, Ball=false
              , Label=["text",range(pts), "halign", "center", "color","red"]
          );
   DrawEdges(pts=pts, faces=faces);
   
   pf = decoPolyEdge( pts = pts
                    , faces = faces
                    , template = temp
                    , edge = [4,7]
                    );
   pf2 = decoPolyEdge( pts = pf[0]
                    , faces = pf[1]
                    , template = temp
                    , edge = [0,3]
                    );

   Black(){
   MarkPt( (pts[7]+pts[6])/2, Ball=false
         , Label=["text", "pf= decoPolyEdges(pts,faces,temp,edge= [4,7])", "indent",0.3]);
   MarkPt( pts[3], Ball=false, Label=["text", "edge= [0,3]", "indent",0.3]);
   MarkPt( -1*Z, Ball=false, Label=["text", str("template= ",temp), "indent",0.3, "halign","center"]);
   }
   MarkPts(temp,Line=0, r=0.01, Label=["text",range(temp), "scale",0.5]);
   Blue(.2) Plane( concat( [ORIGIN], temp), mode=1);
   
   //MarkPts( pf2[0], Ball=false, Line=false);
   Line( [ORIGIN,[0,0,-1]], color=["red",.3], r=0.03 );
   Line( [ORIGIN,[0,-.5,0]], color=["red",.3], r=0.03 );
   Line( [ORIGIN,[-.5,0,0]], color=["red",.3], r=0.03 );
   Line( [pts[4],pts[7]], color=["green",.3], r=0.03 );
   Line( [pts[4],pf[0][4]], color=["green",.3], r=0.03 );
   Line( [pts[4],pf[0][10]], color=["green",.3], r=0.03 );
   Line( [pts[0],pts[3]], color=["blue",.3], r=0.03 );
   Line( [pts[0],pf2[0][0]], color=["blue",.3], r=0.03 );
   Line( [pts[0],pf2[0][16]], color=["blue",.3], r=0.03 );
   
   color("gold",0.9) polyhedron( pf2[0], pf2[1] );
   
}

//demo_decoPolyEdge_pis();
module demo_decoPolyEdge_pis()
{  
   echom("demo_decoPolyEdge_pis()");
   _teal("Demo the index redistribution");
   
   temp= to3d( [ [-.7,0], [-.5,-0.2],[-.2,-.2],  [0,-.5] ] );
   //Plane( concat( [ORIGIN], temp), mode=1, color="blue");
   pts = addy(cubePts(2),0.5);
   faces = rodfaces(4);
   MarkPts(pts, Line=false, Ball=false
              , Label=["text",range(pts), "scale", .6, "color","red"]
          );
   DrawEdges(pts=pts, faces=faces, r=0.01, color="red");
   
   pf = decoPolyEdge( pts = addx(pts,3)
                    , faces = faces
                    , template = temp
                    , edge = [4,7]
                    );
   DrawEdges( pf, r=0.01, color="green" );
   MarkPts(pf[0], Line=false, Ball=false
              , Label=["text",range(pf[0]), "scale", .6, "indent",0.03, "color","green"]
          );
          
   pf2 = decoPolyEdge( pts = addx(pf[0],3)
                    , faces = pf[1]
                    , template = temp
                    , edge = [0,3]
                    );
   DrawEdges( pf2, r=0.01, color="blue" );
   MarkPts(pf2[0], Line=false, Ball=false
              , Label=["text",range(pf2[0]), "scale", .6, "indent",0.03, "color","blue"]
          );
}

module __show_decoPolyEdge(pts,faces,edge
                          , template, showtemplate=1, debug=0)
{
   //
   // To be called by other demos
   //
   echom("__show_decoPolyEdge()");
   pts = pts?pts:transPts(hashs(POLY_SAMPLES,["cube","pts"]), ["y",1]);
   faces = faces?faces:hashs(POLY_SAMPLES,["cube","faces"]);
   //echo(template = template);
   if(debug)
   {
   MarkFaces(pts, faces, color="purple");   
   MarkPts(pts, Line=false, Ball=false
          , Label=["text", range(pts), "color","blue"]); //, "indent", -.3]);
   DrawEdges( pts, faces );
   }
   
   //----------------------------------------------
   //tmp = [ [0,1], [0.5,0.5], [1,0.8], [2,0] ];
   //tmp = to3d( [[0,1], [2,0]] ) ;
   //tmp = to3d([ [0,1], ORIGIN, [2,0] ]);
   //echo( tmp=tmp, tmp3d=to3d(tmp,-1) );
   //----------------------------------------------
   //tmp = reverse(arcPts([[0,0,1],ORIGIN,[1,0,0]],rad=1,a=90,a2=90,n=64) );
   tmp = to3d(template);
   if(showtemplate)
   {
     Plane( concat( tmp, [ORIGIN]), mode=1);
     MarkPts(tmp);
   }
   if(debug)
   MarkPts(tmp, Ball=["dec_r",0,"r",0.05]
          , Label=false, Line=["r",0.01]);
   
   //----------------------------------------------
   
   new_ptf= decoPolyEdge( pts, faces
                        , edge = edge
                        //, pis = [2,6,7] 
                        //, dir = 7
                        , template= tmp
                        , debug= debug
                        );
                        
   //echo(new_ptf = new_ptf);
   //MarkPts( new_ptf[0]);
   
   newpts = new_ptf[0];
   newfaces= new_ptf[1];
   
   if(debug)
   {
   MarkPts( newpts, Ball=false, Line=false
          , Label=["text", range(newpts)
                  , "color", "red"
                  , "indent", 0.3] 
          );
     DrawEdges( newpts, newfaces, color="red" );     
   }       
   //for( fi = range(newfaces))
   //{
      //Plane( get(newpts, newfaces[fi]), mode=1, color=[COLORS[fi],1] );
   //}
   //MarkPts( newpts );
   color("gold",debug?0.5:0.9)
   polyhedron( new_ptf[0], new_ptf[1] );
}


//demo_decoPolyEdge_cut_corner_1();
module demo_decoPolyEdge_cut_corner_1()
{
   echom("demo_decoPolyEdge_cut_corner_1()");
   __show_decoPolyEdge(template= [[-2,0], [0,-1]]
                      , edge=[4,7]
                      //, edge=[5,4]
                      //, edge=[3,0]
                      //, edge=[1,0]
                      //, edge=[3,7]
                      , debug=0 );
}


//demo_decoPolyEdge_cut_corner_2();
module demo_decoPolyEdge_cut_corner_2()
{
   echom("demo_decoPolyEdge_cut_corner_2()");
   __show_decoPolyEdge(template= [ [-2,0] , [-2,-1], [0,-1] ]
                      , edge=[4,7]
                      , debug=0 );
}


//demo_decoPolyEdge_cut_corner_3();
module demo_decoPolyEdge_cut_corner_3()
{
   echom("demo_decoPolyEdge_cut_corner_3()");
   template= [[-1, 0], [-1, -0.5], [-0.5, -0.5], [-0.5, -1], [0, -1]];
   __show_decoPolyEdge(template= template
                      , edge=[4,7]
                       , debug=0);
}

//demo_decoPolyEdge_cut_corner_3_use_opt();
module demo_decoPolyEdge_cut_corner_3_use_opt(showtemplate=1, debug=0)
{
   echom("demo_decoPolyEdge_cut_corner_3_use_opt()");
   template= [[-1, 0], [-1, -0.5], [-0.5, -0.5], [-0.5, -1], [0, -1]];
   template= [ -1*X, -1*(X+Y)/2, -2*Y];
   //
   // To be called by other demos
   //
   pts = transPts( cubePts(3), ["y",1]);
   faces = CUBEFACES;
   //echo(template = template);
   if(debug)
   {
     MarkPts(pts, Line=false, Ball=false, color="red"); //, "indent", -.3]);
     DrawEdges( pts=pts, faces=faces );
   }
   tmp = to3d(template);
   if(showtemplate)
   {
     Plane( concat( tmp, [ORIGIN]), mode=1);
     MarkPts(tmp);
   }
   if(debug)
   MarkPts(tmp, Ball=["dec_r",0,"r",0.05]
          , Label=false, Line=["r",0.01]);
   
   //----------------------------------------------
   
   new_ptf= decoPolyEdge( opt=["pts", pts, "faces",faces
                              , "edge", [4,7]
                              , "template", tmp
                              ]
                        , debug= debug
                        );
                        
   //echo(new_ptf = new_ptf);
   //MarkPts( new_ptf[0]);
   
   newpts = new_ptf[0];
   newfaces= new_ptf[1];
   
   if(debug)
   {
   MarkPts( newpts, Ball=false, Line=false
          , Label=["text", range(newpts)
                  , "color", "blue"
                  , "indent", 0.3] 
          );
     DrawEdges( newpts, newfaces, color="blue" );     
   }       
   //for( fi = range(newfaces))
   //{
      //Plane( get(newpts, newfaces[fi]), mode=1, color=[COLORS[fi],1] );
   //}
   //MarkPts( newpts );
   color("gold",debug?0.5:0.9)
   polyhedron( new_ptf[0], new_ptf[1] );
}


//demo_decoPolyEdge_arc();
module demo_decoPolyEdge_arc()
{
   echom("demo_decoPolyEdge_arc()");
   pqr = [[-1,0,0],[-1,-1,0],[0,-1,0]];  
   MarkPts(pqr, Label="PQR"); 
   template=[ for(i=[0:23]) anglePt(pqr, len=1, a= i*90/24) ];
   //echo(template=template);
  __show_decoPolyEdge(template= template
                      , edge=[4,7]
                       , debug=0);
}


//demo_decoPolyEdge_concave();
module demo_decoPolyEdge_concave()
{
   echom("demo_decoPolyEdge_concave()");
   template=  [[-1, 0], [-1, -0.5], [-2, -0.5], [-2, -2.5], [-0.5, -2.5], [-0.5, -1], [0, -1]];
   __show_decoPolyEdge(template= template
                      , edge=[4,7]
                       , debug=0);   
}

//demo_decoPolyEdge_concave2();
module demo_decoPolyEdge_concave2()
{
   echom("demo_decoPolyEdge_concave2()");
   template=[ [0,0], [0,-0.5], [-2, -0.5], [-2, -2.5]
            , [-0.5, -2.5], [-0.5, -1], [0, -1]];
   __show_decoPolyEdge(template= template
                      , edge=[4,7]
                       , debug=0);   
}

//demo_decoPolyEdge_concave3();
module demo_decoPolyEdge_concave3()
{
   echom("demo_decoPolyEdge_concave3()");
   template=[ [0,0], [0,-0.5], [-2.5, -0.5], [-2.5, -2.5]
            ,  [0, -2.5]];
   __show_decoPolyEdge(template= template
                      , edge=[4,7]
                        , debug=0);   
}

//demo_decoPolyEdge_concave4();
module demo_decoPolyEdge_concave4()
{
   echom("demo_decoPolyEdge_concave4()");
   template=[ [-2.5,0], [-2.5,-2.5], [-0.5, -2.5], [-0.5, 0]
            ,  [0, 0]];
   __show_decoPolyEdge(template= template
                      , edge=[4,7]
                        , debug=0);   
}

//demo_decoPolyEdge_convex_arc();
module demo_decoPolyEdge_convex_arc()
{
   echom("demo_decoPolyEdge_convex_arc()");
   p3=   [[-0.5, 0, 0], [0, 0, 0], [0, 0.5, 0]];
   echo([for(p=p3)p+[0.5,0.5,0.5]]);
   n=48;
   template= [ for(i=range(n))
               anglePt( p3, a= i*270/n )
              ];
             //( arcPts(p3,rad=1,a=90,a2=90,n=24) );
   MarkPts(p3);
   //echo( template=template );
   __show_decoPolyEdge( template= template
                      //, edge=[4,7]
                      , edge=[7,6]
                      //, edge=[3,0]
                        , debug=0);
}


//demo_decoPolyEdge_convex_arc2();
module demo_decoPolyEdge_convex_arc2()
{
   echom("demo_decoPolyEdge_convex_arc2()");
   template=arcPts([[0,0,-1],ORIGIN,[1,0,0]],rad=1,a=90,a2=90,n=24 );
   __show_decoPolyEdge(template= concat( [ORIGIN], template )
                      , edge=[4,7]
                       , debug=0 );
}


//. site


//module demo_decoPolyEdge_cut_corner()
//{
//   echom("demo_decoPolyEdge_cut_corner()");
//   _pts = hashs(POLY_SAMPLES,["cube","pts"]);
//   //pqr = pqr(3);
//   pqr = [[0.025438, -0.668024, -2.33806], [-2.84666, 2.72857, 0.493522], [-1.25729, -2.45536, 1.66304]];
//   echo(pqr=pqr);
//   pts = movePts( _pts, to=pqr);   
//   __show_decoPolyEdge( pts=pts
//                      , template= [ [0,1],[0,0.5],[0.25,0.25], [0.5,0], [1,0] ] );
//}
////demo_decoPolyEdge_cut_corner();
//
//module demo_decoPolyEdge_dent()
//{
//   echom("demo_decoPolyEdge_dent()");
//   //_pts = hashs(POLY_SAMPLES,["cube","pts"]);
//   //pqr = pqr(3);
//   //pqr = [[0.025438, -0.668024, -2.33806], [-2.84666, 2.72857, 0.493522], [-1.25729, -2.45536, 1.66304]];
//   //echo(pqr=pqr);
//   //pts = movePts( _pts, to=pqr); 
//   depth = 2;
//   width = 1;
//   pts = addy(cubePts(3),2);  
//   __show_decoPolyEdge( pts=pts
//                      //, template= [ [0,1],[0,0.5],[0.25,0.25], [0.5,0], [1,0] ] 
//                      , template= [ [0,1-depth], [0,0]
//                                  , [width,0], [width,1]
//                                  , [(3-width)/2+width,1],[(3-width)/2+width,0] ] 
//                      
//                      );
//}
//demo_decoPolyEdge_dent();

//. dent

 
 
//. dual cuts


//demo_decoPolyEdge_dual_cuts();
module demo_decoPolyEdge_dual_cuts(pts,faces,template)
{
   echom("demo_decoPolyEdge_dual_cuts()");
   pts = pts?pts:transPts(hashs(POLY_SAMPLES,["cube","pts"]), ["y",2]);
   faces = faces?faces:hashs(POLY_SAMPLES,["cube","faces"]);
   
   Red(){ 
     MarkPts(pts, Line=false, Ball=false); 
     DrawEdges( pts=pts, faces = faces, r=0.02 );
   }
   
//   for(fi=range(faces))
//   { f=faces[fi];
//     for( eg=get2(f) ) Line( get(pts,eg), color="red", r=0.005 );
//     MarkPt( sum( get(pts,f))/len(f)
//           , Label=["text", fi, "indent",0, "color", "red"] 
//           , Ball=false
//           );     
//   }
   
   //----------------------------------------------
   //tmp = [ [0,1], [0.5,0.5], [1,0.8], [2,0] ];
   //tmp = to3d( [[0,1], [2,0]] ) ;
   //tmp = to3d([ [0,1], ORIGIN, [1,0] ]);
   tmp= to3d( [ [-1,0],[-1,-1], [0,-1]] );
   //echo( tmp=tmp, tmp3d=to3d(tmp,-1) );
   //----------------------------------------------
   //tmp = reverse(arcPts([[0,0,1],ORIGIN,[1,0,0]],rad=1,a=90,a2=90,n=64) );
   //tmp = to3d(template);
   Plane( concat( tmp, [ORIGIN]), mode=1);
   MarkPts(tmp, Ball=["dec_r",0,"r",0.05]
          , Label=false, Line=["r",0.01]);
   
   //----------------------------------------------
   
   new_ptf= decoPolyEdge( pts, faces
                        , edge = [4,7]
                        //, dir = 7
                        , template= tmp
                        );

   newpts = new_ptf[0];
   newfaces= new_ptf[1];
   Blue() MarkPts(newpts, Line=false, Ball=false); 
   Blue(0.3) DrawEdges( pts=newpts, faces = newfaces, r=0.03 );
      
   new_ptf2= decoPolyEdge( pts=newpts
                        , faces=newfaces
                        , edge = [0,9]
                        //, dir = 5
                        , template= tmp
                        , debug=0
                        );
                        
   //MarkPts( new_ptf2[0] ); 
   Gold(.8)
   polyhedron( new_ptf2[0], new_ptf2[1] );
} 
//============================================ 

//demo_decoPolyEdges();
module demo_decoPolyEdges(showtemplate=1, debug=0)
{
   echom("demo_decoPolyEdges()");
   template1= [[-1, 0], [-1, -0.5], [-0.5, -0.5], [-0.5, -1], [0, -1]];
   //template2= [ -1*X, -1*(X+Y)/2, -2*Y];
   template2= [ -.5*X,  -1*Y];
   //
   // To be called by other demos
   //
   pts = transPts( cubePts(3), ["y",1]);
   faces = CUBEFACES;
   //echo(template = template);
   if(debug)
   {
     MarkPts(pts, Line=false, Ball=false, color="red"); //, "indent", -.3]);
     DrawEdges( pts=pts, faces=faces );
   }
   tmp = to3d(template1);
   if(showtemplate)
   {
     Plane( concat( tmp, [ORIGIN]), mode=1);
     MarkPts(tmp);
   }
   if(debug)
   MarkPts(tmp, Ball=["dec_r",0,"r",0.05]
          , Label=false, Line=["r",0.01]);
   
   //----------------------------------------------
   
   new_ptf= decoPolyEdges( decos=[ [[4,7],tmp, 0]
                                 , [[0,3], to3d(template2)] 
                                 , [1,2]
                                 ]
                         , opt=["pts", pts, "faces",faces
                               ]
                        , debug= debug
                        );
                        
   //echo(new_ptf = new_ptf);
   //MarkPts( new_ptf[0]);
   
   newpts = new_ptf[0];
   newfaces= new_ptf[1];
   
   if(debug)
   {
   MarkPts( newpts, Ball=false, Line=false
          , Label=["text", range(newpts)
                  , "color", "blue"
                  , "indent", 0.3] 
          );
     DrawEdges( newpts, newfaces, color="blue" );     
   }       
   //for( fi = range(newfaces))
   //{
      //Plane( get(newpts, newfaces[fi]), mode=1, color=[COLORS[fi],1] );
   //}
   //MarkPts( newpts );
   color("gold",debug?0.5:0.9)
   polyhedron( new_ptf[0], new_ptf[1] );
}




