include <../scadx.scad>


ISLOG=1;

log_demo_core();
//log_demo_array_boldface_mode();
//log_demo_runtime_indent();
//log_demo_displayed_levels();
//log_demo_user_defined_layers();
//log_demo_extending_core_modules();
//log_demo_debug();


//. Demos 

module log_demo_core()
{
  echom("log_demo_core()");
  
  log_b("log_demo_core(): Some text without given level= log_b(text)");
  log_();
  log_("Some text without given level: log_(text)");
  log_();
  
  log_("level 1, log_(text,1)", 1);
  log_(layer=1); //<===== this is an empty string at layer=1
  log_b("level 1, log_b(text,1)", 1);
  log_("level 1, log_(text,1)", 1);
  
  log_b("level 2, log_b(text,2)", 2);
  log_("level 2, log_(text,2)", 2, debug=0);
  log_("level 2, log_(text,2)", 2);
  
  log_b("level 3, log_b(text,3)", 3);
  log_("level 3, log_(text,3)", 3);
  log_("level 4, log_(text,4)", 4);
  log_("level 4, log_(text,4)", 4);
  log_("level 5, log_(text,5)", 5);
  log_("level 5, log_(text,5)", 5);
  log_("level 6, log_(text,6)", 6);
  log_("level 6, log_(text,6)", 6);
  log_("level 7, log_(text,7)", 7);
  log_("level 7, log_(text,7)", 7);
  log_b("level 8, log_b(text,8)", 8, debug=0);
  log_("level 8, log_(text,8)", 8);
  log_("level 9, log_(text,9)", 9);
  log_("level 9, log_(text,9)", 9);
  log_("level 8, log_(text,8)", 8);
  log_e("level 8, log_e(text,8)", 8);
  log_("level 3, log_(text,3)", 3);
  log_e("level 3, log_e(text,3)", 3);
  
  log_("level 2, log_(text,2)", 2);
  log_e("level 2, log_e(text,2)", 2);
  
  log_("level 1, log_(text,1)", 1);
  
  log_e("level 1, log_e(text,1)", 1);
  log_();
  log_e("log_demo_core()");
}  

//log_demo_core();

//==========================================================
         
module log_demo_array_boldface_mode()
{
  log_b("log_demo_array_boldface_mode()");
  
  log_("");
  log_( "The first argument, <b>ss</b>, could be either a string, or an array. ");
  log_( "When it is an array, set <b>mode</b> to <u>0,1 or 2</u> to decide bold-faced items:");
  log_("");
  log_b("For array [\"length\",10, \"width\",5] :",1);
  log_("",1);
  
  log_b( "", 2);
  log_(layer=2);
  log_( ["mode=0 normal face): length",10, "width",5], 2, mode=0 );
  log_( ["mode=1 (bold keys): length",10, "width",5], 2, mode=1 );
  log_( ["mode=2 (bold values): length",10, "width",5], 2, mode=2 );
  log_(layer=2);
  log_e( "", 2);
  log_e(layer=1);
  
  log_("");

  log_e( "log_demo_array_boldface_mode()" );
}  
//log_demo_array_boldface_mode();


//==========================================================
         
module log_demo_runtime_indent()
{
  log_b("log_demo_runtime_indent()",1);
  
  log_("",1);
  log_( "The indent is determined by level (set in hParams.hLayers). But",1);
  log_( "it can be adjusted at run-time by setting <b>add_ind</b>:",1);
  log_("",1);
  
  log_( "level 1 Normal indent",1 );
  log_( "log_( ...add_ind=-1)",1, add_ind=-1 );
  log_( "level 1 Normal indent",1 );
  log_( "log_( ... add_ind=1)",1, add_ind=1 );
  log_( "log_( ... add_ind=2)",1, add_ind=2 );
  
  log_("",1);
  log_("",1);
  log_("",1);
  
  log_b("level 2", 2);
  log_( "level 2 Normal indent",2 );
  log_( "level 2 Normal indent",2 );
  log_( "log_( ... add_ind=1)",2, add_ind=1 );
  log_( "log_( ... add_ind=2)",2, add_ind=2 );
  log_( "level 3 Normal indent",3 );  
  log_( "log_( ... add_ind=-1)",2, add_ind=-1 );
  log_( "level 2 Normal indent",2 );
  log_( "level 2 Normal indent",2 );
  log_e("level 2",2);
  
  log_("",1);
  log_("",1);
  
  log_e( "log_demo_runtime_indent()",1 );
}  
//log_demo_runtime_indent();

//==========================================================

module log_demo_debug()
{

  log_("Debugging log_()");
  log_();
  
  log_b("log_demo_debug(): Some text without given level= log_b(text)");
  log_();
  log_("Some text without given level: log_(text)");
  log_();
  
  log_("level 1, log_(text,1)", 1);
  log_(layer=1);
  log_b("level 1, log_b(text,1)", 1);
  log_("level 1, log_(text,1)", 1);
  
  log_b("level 2, log_b(text,2)", 2);
  log_("level 2, log_(text,2)", 2);
  log_("",2);
  log_("Next one sets debug=1: log_(..., debug=1)",2); 
  log_("",2);
  log_("level 2, log_(text,2)", 2, debug=1);

  
  log_("level 2, log_(text,2)", 2);
  log_e("level 2, log_e(text,2)", 2);
  
  log_("level 1, log_(text,1)", 1);
  log_e("level 1, log_(text,1)", 1);
  
  log_();
  log_e("log_demo_debug()");
}  

//log_demo_debug();

//==========================================================

module log_demo_displayed_levels(log_level=10)
{
  
  hParams= [ "LOG_LEVEL", log_level ];
  
  echo("");
  log_b("<b>log_demo_displayed_levels()</b>", hParams=hParams);
  log_("");
  log_( "Use <b>hParams= [ \"LOG_LEVEL\", log_level ]</b> to control level of display");
  log_( _span(_green( _b("Turn on the animation to see the effect")), "font-size:14px"));
  log_( _span(_red( _b(str( "log_level= ", log_level))), "font-size:14px"));
  log_("");

  log_(hParams=hParams);
  log_("Some text without given level: log_(text)", hParams=hParams);
  log_(hParams=hParams);
  
  log_("level 1, log_(text,1)", 1, hParams=hParams);
  log_(layer=1, hParams=hParams);
  log_b("level 1, log_b(text,1)", 1, hParams=hParams);
  log_("level 1, log_(text,1)", 1, hParams=hParams);
  
  log_b("level 2, log_b(text,2)", 2, hParams=hParams);
  log_("level 2, log_(text,2)", 2, hParams=hParams);
  log_("level 2, log_(text,2)", 2, hParams=hParams);
  
  log_b("level 3, log_b(text,3)", 3, hParams=hParams);
  log_("level 3, log_(text,3)", 3, hParams=hParams);
  log_("level 4, log_(text,4)", 4, hParams=hParams);
  log_("level 5, log_(text,5)", 5, hParams=hParams);
  log_("level 6, log_(text,6)", 6, hParams=hParams);
  log_("level 7, log_(text,7)", 7, hParams=hParams);
  log_b("level 8, log_b(text,8)", 8, hParams=hParams);
  log_("level 9, log_(text,9)", 9, hParams=hParams);
  log_("level 9, log_(text,9)", 9, hParams=hParams);
  log_("level 8, log_(text,8)", 8, hParams=hParams);
  log_e("level 8, log_e(text,8)", 8, hParams=hParams);
  log_("level 3, log_(text,3)", 3, hParams=hParams);
  log_e("level 3, log_e(text,3)", 3, hParams=hParams);
  
  log_("level 2, log_(text,2)", 2, hParams=hParams);
  log_e("level 2, log_e(text,2)", 2, hParams=hParams);
  
  log_("level 1, log_(text,1)", 1, hParams=hParams);
  
  log_e("level 1, log_e(text,1)", 1, hParams=hParams);
  log_(hParams=hParams);
  log_e("log_demo_core()", hParams=hParams);
}  

//log_demo_displayed_levels(10*$t);

//==========================================================

module log_demo_user_defined_layers()
{

  hParams= [ "hLayers", [ "obj", [3, "red"]
                        , "setting", [5, "green"]
                        ]
           ];
  
  log_b("log_demo_user_defined_layers()", 1, hParams=hParams);
  
  log_b( "obj block", "obj", hParams=hParams );
  log_("level 3", 3);
  log_("level 3", 3);
  log_("level 3", 3); 
  log_("Still level 3 but with user-defined color", "obj", hParams=hParams);
  log_("some text", "obj", hParams=hParams);
  log_("some text", "obj", hParams=hParams);
  log_("some text", "obj", hParams=hParams);
  
    log_("level 5", 5);
    log_("level 5", 5);
    log_("level 5", 5);
    log_("Still level 5 but with user-defined color", "setting", hParams=hParams);
    log_b("Setting block", "setting", hParams=hParams);
    log_("Setting content", "setting", hParams=hParams);
    log_("Setting content, add_ind=2", "setting", add_ind=2, hParams=hParams);
    log_e("Setting block", "setting", hParams=hParams);
  
  log_("some text", "obj", hParams=hParams);
  log_e( "obj block", "obj", hParams=hParams );
  
  log_e("log_demo_user_defined_layers()", 1);
}  

//log_demo_user_defined_layers();

//==========================================================

module log_demo_extending_core_modules()
{

  hParams= [ "hLayers", [ "obj", [3, "red"]
                        , "setting", [5, "green"]
                        ]
           ];
  
  module log_obj(ss,layer, mode, add_ind, prompt, debug)
                   {  log_(ss,"obj", mode, add_ind, prompt, debug, hParams=hParams); }  
  module log_obj_b(ss,layer, mode, add_ind, debug)
                   {  log_b(ss,"obj", mode, add_ind, debug=debug, hParams=hParams); }  
  module log_obj_e(ss,layer, mode, add_ind, debug)
                   {  log_e(ss,"obj", mode, add_ind, debug=debug, hParams=hParams); }  
  module log_setting(ss,layer, mode, add_ind, prompt, debug)
                   {  log_(ss,"setting", mode, add_ind, prompt, debug, hParams=hParams); }  
  module log_setting_b(ss,layer, mode, add_ind, debug)
                   {  log_b(ss,"setting", mode, add_ind, debug=debug, hParams=hParams); }  
  module log_setting_e(ss,layer, mode, add_ind, debug)
                   {  log_e(ss,"setting", mode, add_ind, debug=debug, hParams=hParams); }  
  
  
  log_b("log_demo_extending_core_modules()");
  log_();
  log_("In previous demos (log_demo_displayed_levels(), log_demo_user_defined_layers()), ");
  log_("which use core modules (log_b, log_, log_e) and requires the user hParams ");
  log_("be entered in each call:");
  log_();
  log_("  log_(\"some text\", 2, <b>hParams=hParams\"</b>);",1);
  log_();
  log_("In case of log_demo_user_defined_layers(), additionally a user layer has to be entered:");
  log_();
  log_("  log_(\"some text\", <b>\"obj\", hParams=hParams\"</b>);",1);
  log_();
  log_("In this demo, both can be avoided by using extended core modules:");
  log_();
  log_("  log_obj(\"some text\");",1);
  log_();
                   
                   
  log_obj_b( "obj block" );
  log_obj("some text");
  log_obj("some text");
  
    log_setting_b("Setting block");
    log_setting("Setting content");
    log_setting("Setting content, add_ind=2", add_ind=2);
    log_setting_e("Setting block");
  
  log_obj("some text");
  log_obj("some text");
  log_obj_e( "obj block");
  
  log_();
  log_e("log_demo_extending_core_modules()");
}  
//log_demo_extending_core_modules();
