/*
  Demo the scalePt()/scalePts() function

*/

include <../scadx.scad>


//. scalePt
module demo_scalePt()
{
  echom("demo_scalePt()");
  echo(_b("scalePt(pt, base=ORIGIN, len=undef, ratio=0.5)"));
  
  pts = randPts(4);
  P=pts[0];
  Q=pts[1];
  R=pts[2];
  S=pts[3];  
  //MarkPts( pts, "l=PQRS");
  
  color("gold")
  {
  P2= scalePt(P); MarkPts([P,P2], ["label", ["text",["P","P2"]]] );
  Line([P,ORIGIN],r=0.01 );
  }
  
  color("red")
  {
  basePt = randPt(); 
  Q2= scalePt(Q, basePt); 
  MarkPts([Q,basePt,Q2], ["label", ["text",["Q","basePt","Q2"]]]);
  Line([Q,basePt],color="red", r=0.02 );
  }
   
  color("green")
  {
  baseLine = randPts(2); ;
  projOnLine = projPt(baseLine, R);
  R2= scalePt(R, baseLine); 
  MarkPts([R,projOnLine,R2], ["label", ["text",["R","projOnLine","R2"]]]);
  Line([R,projOnLine],color="green", r=0.02 );
  Line( linePts( [projOnLine, baseLine[ dist(projOnLine,baseLine[0])
                               > dist(projOnLine,baseLine[1])? 0: 1 ] ]
              , len=2
              )                 
      );
  }
  
  color("blue")
  {
  basePlane = pqr(); Plane(basePlane);
  projOnPlane = projPt(basePlane, S);
  S2= scalePt(S, basePlane); 
  MarkPts([S,projOnPlane,S2], ["label", ["text",["S","projOnPlane","S2"]]]);
  Line([S,projOnPlane],r=0.02 );
  }

  echo(_olive("scalePt(P)")); 
  echo(_red("scalePt(Q, basePt)"));
  echo(_green("scalePt(R, baseLine)"));
  echo(_blue("scalePt(S, basePlane)"));
}
//demo_scalePt();

module demo_scalePt_negative_len()
{
  echom("demo_scalePt_negative_len()");
  
  pts = randPts(4);
  P=pts[0];
  Q=pts[1];
  R=pts[2];
  S=pts[3];  
  //MarkPts( pts, "l=PQRS");
  
  color("gold")
  {
  P2= scalePt(P,len=-1); MarkPts([P,P2], ["label", ["text",["P","P2"]]] );
  Line([P,P2],r=0.01 );
  }
  
  color("red")
  {
  basePt = randPt(); 
  Q2= scalePt(Q, basePt,len=-1); 
  MarkPts([Q,basePt,Q2, ORIGIN], ["label", ["text",["Q","basePt","Q2","ORIGIN"]]]);
  Line([Q,Q2],color="red", r=0.02 );
  }
   
  color("green")
  {
  baseLine = randPts(2); ;
  projOnLine = projPt(baseLine, R);
  R2= scalePt(R, baseLine,len=-1); 
  MarkPts([R,projOnLine,R2], ["label", ["text",["R","projOnLine","R2"]]]);
  Line([R,R2],color="green", r=0.02 );
  Line( linePts( [projOnLine, baseLine[ dist(projOnLine,baseLine[0])
                               > dist(projOnLine,baseLine[1])? 0: 1 ] ]
              , len=2
              )                 
      );
  }
  
  color("blue")
  {
  basePlane = pqr(); Plane(basePlane);
  projOnPlane = projPt(basePlane, S);
  S2= scalePt(S, basePlane,len=-1); 
  MarkPts([S,projOnPlane,S2], ["label", ["text",["S","projOnPlane","S2"]]]);
  Line([S,S2],r=0.02 );
  }

  echo(_olive("scalePt(P,len=-1)")); 
  echo(_red("scalePt(Q, basePt,len=-1)"));
  echo(_green("scalePt(R, baseLine,len=-1)"));
  echo(_blue("scalePt(S, basePlane,len=-1)"));
}
//demo_scalePt_negative_len();


//. scalePts

module demo_scalePts_based_on_ORIGIN()
{
  echom("demo_scalePts_based_on_ORIGIN()");
  echo(_b("scalePt(pt, base=ORIGIN, len=undef, ratio=0.5, opt=[])"));
  
  pts = randPts(3, d=[1,3]);
  P=pts[0];
  Q=pts[1];
  R=pts[2];
  //S=pts[3];
  //T=pts[4];
  //U=pts[5];
  color("olive") MarkPts( pts, "l=PQR");
  
  scalePts= scalePts(pts);
  for(p=pts)Line([ORIGIN,p], r=0.02);
  
  //echo(pts=pts);
  //echo(scalePts = scalePts);
  
  MarkPts( scalePts, ["label",["text",["P2","Q2","R2"]]]);
  
  Plane(pts);
  color("red") Plane(scalePts);
  
}
//demo_scalePts_based_on_ORIGIN();

module demo_scalePts_based_on_pt_template(data)
{
  // data = [ ["len",len, "ratio", ratio]
  //        , ["len",len, "ratio", ratio]
  //        , ["len",len, "ratio", ratio]
  //        , ["len",len, "ratio", ratio]
  //        ]
  //echom("demo_scalePts_ratio_based_on_any_pt()");
  
  pqr =  [[1.35094, 1.64005, 2.77328], [2.26162, -1.14864, -0.523694], [0.212686, -0.709662, 1.16295]]; 
  color("olive") MarkPts( pqr, "l=PQR");
  
  
  labelstrs = [for(d=data) hash(d,"len")?str("len=",hash(d,"len"))
                                        :str("ratio=",hash(d,"ratio")) ];
  
  basePt = [0.5,1,-0.5]; //randPt();
  //echo(basePt=basePt);
  MarkPt(basePt, "l=basePt");
  scalePts= scalePts(pqr,base=basePt, opt=data[0]);
  for(p=scalePts)Line([basePt,p], r=0.02);
  MarkPt(scalePts[0], ["label",["text", _s("scalePts(pqr,basePt,{_})",labelstrs[0])]]);
  //MarkPts( scalePts, ["label",["text",["P2","Q2","R2"]]]);
  Plane(pqr);
  color("red") //Plane(scalePts);
  Line(scalePts,closed=true);
  
  basePt2= basePt+ [-4,0,0];
  pqr2= transPts(pqr, ["t",[-4,0,0]] );
  Plane(pqr2);
  color("green")
  {
  incenterPt= incenterPt(pqr2);
  MarkPt(incenterPt, "l=incenterPt");
  scalePts2= scalePts(pqr2,base=incenterPt, opt=data[1]);
  for(p=scalePts2)Line([incenterPt,p], r=0.02);
  MarkPt(scalePts2[0], ["label",["text",_s("scalePts(pqr,incenterPt,{_})",labelstrs[1])]]);
  //MarkPts( scalePts2, ["label",["text",["P3","Q3","R3"]]]);
  Line(scalePts2,closed=true);
  }
  
  //if(len(data)>=3)
  //{
    pqr3 = transPts( pqr2, ["t", [-4,0,0]]);
    
    Plane(pqr3);
    basePt3= pqr3[0];
    scalePts3= scalePts(pqr3,base=basePt3, opt=data[2]);
    color("blue")
    {
    //for(p=scalePts2)Line([incenterPt,p], r=0.02);
    MarkPt(scalePts3[0], ["label",["text",_s("scalePts(pqr,P,{_})",labelstrs[2])]]);
    Line(scalePts3,closed=true);
    }
  //}
    echo(pqr3=pqr3);
    echo(basePt3=basePt3);
    echo(scalePts3=scalePts3);
    
  if(len(data)>=4)
  {
    pqr4 = transPts( pqr3, ["t", [-4,0,0]]);
    Plane(pqr4);
    basePt4= midPt([pqr4[0],pqr4[1]]);
    scalePts4= scalePts(pqr4,base=basePt4, opt=data[3]);
    color("purple")
    {
    MarkPts( pqr4, "l=PQR");
    MarkPt( basePt4, ["label",["text", "midPt(PQ)"],"ball",["r",0.1]]);
    MarkPt(scalePts4[0], ["label",["text",_s("scalePts(pqr,midPt(PQ),{_})",labelstrs[3])]]);
    Line(scalePts4,closed=true);
    }
  }
}


module demo_scalePts_ratio_based_on_any_pt()
{
  echom("demo_scalePts_ratio_based_on_any_pt()");
  demo_scalePts_based_on_pt_template(
      data = [ ["ratio",1.5]
             , ["ratio",1.5]
             , ["ratio",0.75]
             , ["ratio",1.5]
             ]
      );
//  pqr =  [[1.35094, 1.64005, 2.77328], [2.26162, -1.14864, -0.523694], [0.212686, -0.709662, 1.16295]]; 
//  color("olive") MarkPts( pqr, "l=PQR");
//  
//  basePt = [0.5,1,-0.5]; //randPt();
//  //echo(basePt=basePt);
//  MarkPt(basePt, "l=basePt");
//  scalePts= scalePts(pqr,base=basePt, ratio=1.5);
//  for(p=scalePts)Line([basePt,p], r=0.02);
//  MarkPt(scalePts[0], ["label",["text","scalePts(pqr,basePt,ratio=1.5)"]]);
//  //MarkPts( scalePts, ["label",["text",["P2","Q2","R2"]]]);
//  Plane(pqr);
//  color("red") //Plane(scalePts);
//  Line(scalePts,closed=true);
//  
//  basePt2= basePt+ [-4,0,0];
//  pqr2= transPts(pqr, ["t",[-4,0,0]] );
//  Plane(pqr2);
//  color("green")
//  {
//  incenterPt= incenterPt(pqr2);
//  MarkPt(incenterPt, "l=incenterPt");
//  scalePts2= scalePts(pqr2,base=incenterPt, ratio=1.5);
//  for(p=scalePts2)Line([incenterPt,p], r=0.02);
//  MarkPt(scalePts2[0], ["label",["text","scalePts(pqr,incenterPt,ratio=1.5)"]]);
//  //MarkPts( scalePts2, ["label",["text",["P3","Q3","R3"]]]);
//  Line(scalePts2,closed=true);
//  }
//  
//  pqr3 = transPts( pqr2, ["t", [-4,0,0]]);
//  Plane(pqr3);
//  basePt3= pqr3[0];
//  scalePts3= scalePts(pqr3,base=basePt3, ratio=0.75);
//  color("blue")
//  {
//  //for(p=scalePts2)Line([incenterPt,p], r=0.02);
//  MarkPt(scalePts3[0], ["label",["text","scalePts(pqr,P,ratio=0.75)"]]);
//  Line(scalePts3,closed=true);
//  }
//  
//  pqr4 = transPts( pqr3, ["t", [-4,0,0]]);
//  Plane(pqr4);
//  basePt4= midPt([pqr4[0],pqr4[1]]);
//  scalePts4= scalePts(pqr4,base=basePt4, ratio=1.5);
//  color("purple")
//  {
//  MarkPts( pqr4, "l=PQR");
//  MarkPt( basePt4, ["label",["text", "midPt(PQ)"],"ball",["r",0.1]]);
//  MarkPt(scalePts4[0], ["label",["text","scalePts(pqr,midPt(PQ),ratio=1.5)"]]);
//  Line(scalePts4,closed=true);
//  }
  
}
//demo_scalePts_ratio_based_on_any_pt();

module demo_scalePts_neg_ratio_based_on_any_pt()
{
  echom("demo_scalePts_neg_ratio_based_on_any_pt()");
  echo(_blue("Negative ratio. This lookes like mirrorPts, but capable of changing the size"));
  
    demo_scalePts_based_on_pt_template(
      data = [ ["ratio",-1.5]
             , ["ratio",-1.5]
             , ["ratio",-0.75]
             , ["ratio",-1.5]
             ]
      );     
}
//demo_scalePts_neg_ratio_based_on_any_pt();

module demo_scalePts_len_based_on_any_pt()
{
  echom("demo_scalePts_len_based_on_any_pt()");
  //echo(_blue("Negative ratio. This lookes like mirrorPts, but capable of changing the size"));
  
    demo_scalePts_based_on_pt_template(
      data = [ ["len",0.5]
             , ["len",0.5]
             , ["len",1]
             , ["len",0.5]
             ]
      );
}      
demo_scalePts_len_based_on_any_pt();

module demo_scalePts_neg_len_based_on_any_pt()
{
  echom("demo_scalePts_neg_len_based_on_any_pt()");
  //echo(_blue("Negative ratio. This lookes like mirrorPts, but capable of changing the size"));
  
    demo_scalePts_based_on_pt_template(
      data = [ ["len",-0.5]
             , ["len",-0.5]
             , ["len",-0.75]
             , ["len",-0.5]
             ]
      );
}      
//demo_scalePts_neg_len_based_on_any_pt();
