include <../scadx.scad>

//Arrow_demo_1();
module Arrow_demo_1()
{
    echom("Arrow_demo_1");
    
    d=1.5;
    pqr=pqr();
    pqr= [[1.2, 2.39, -2.32], [-1.68, 2.67, -0.19], [-2.09, -1.47, -2.92]];
    MarkPts(pqr, "PQR");
    //echopts("pqr= ", pqr);    
  
    pq = get( transN(pqr, 0.5*d), [0,1]) ;
    Arrow( pq ); 
    MarkPt( pq[0], Label="Arrow( [P,Q] )", Line=false, Ball=false);
    
    pqr2 = transN(pqr, 1*d) ;
    Red() { Arrow( pqr2 ); 
            MarkPt( pqr2[0], Label="Arrow( pqr )", Line=false, Ball=false);
          } 
    
    Green(){ Arrow( transN(pqr, 1.8*d), r=0.3);
             MarkPt( transN(pqr, 2*d)[0], "   Arrow( pqr, r=0.3)", Ball=false);
           }  
    
    Blue(){ Arrow( transN(pqr, 3*d), r=0.1, $fn=16);
             MarkPt( transN(pqr, 3*d)[0], "   Arrow( pqr, r=0.1, $fn=16)", Ball=false);
           }  
    
    pts= randPts(5);
    Purple() {Arrow(pts);
              MarkPt(pts[0], "Arrow(pts)");
              }
}    

//Arrow_demo_tail();
module Arrow_demo_tail()
{
    echom("Arrow_demo_tail");
    
    d=1.3;
    pqr=pqr();
    
    MarkPts(pqr, "PQR");
    
    //pqr= [[1.2, 2.39, -2.32], [-1.68, 2.67, -0.19], [-2.09, -1.47, -2.92]];
    pqr= [[-0.88, 1.27, 1.76], [-0.24, 0.72, -0.65], [-0.24, -0.59, 1.67]];
    //pqr = [[0,3,0],[0,0,0],[3,0,0]];
    //echopts("pqr= ", pqr);    
  
    pq = get(pqr,[0,1]) ;
    Arrow2( pq ); 
    MarkPts( pq, Ball=false, Label="PQ");
    
    Arrow2( transN(pqr, 1*d), r=0.2,color="red"); 
    MarkPts( transN(pqr, 1*d), Ball=false
    , Label=["text",["P(Tail)", "Q", "R(Head)"],"color","red","indent",-0.3] );
        
    Arrow( transN(pqr, 2.5*d), r=0.1, $fn=16,  Tail=true, color="green" );    
    Arrow( transN(pqr, 4*d), r=0.1, $fn=6,  Tail=true, color="blue", Head=["r",0.2] );    
    Arrow( transN(pqr, 5.5*d), r=0.2, $fn=6,  Tail=true, Head=false, color="teal"  );    
    Arrow( transN(pqr, 7*d), r=0.5, $fn=6,  Tail=true, opt=["Head",false], color="olive"  );    
       
}    



//Arrow2_demo_tail();
module Arrow2_demo_tail()
{
    echom("Arrow2_demo_tail");
    
    d=1.3;
    pqr=pqr();
    //pqr= [[1.2, 2.39, -2.32], [-1.68, 2.67, -0.19], [-2.09, -1.47, -2.92]];
    pqr= [[-0.88, 1.27, 1.76], [-0.24, 0.72, -0.65], [-0.24, -0.59, 1.67]];
    //pqr = [[0,3,0],[0,0,0],[3,0,0]];
    //echopts("pqr= ", pqr);    
  
    pq = get(pqr,[0,1]) ;
    Arrow2( pq ); 
    MarkPts( pq, Ball=false, Label="PQ");
    
    Arrow2( transN(pqr, 1*d), r=0.2,color="red"); 
    MarkPts( transN(pqr, 1*d), Ball=false
    , Label=["text",["P(Tail)", "Q", "R(Head)"],"color","red","indent",-0.3] );
        
    Arrow2( transN(pqr, 2.5*d), r=0.1, $fn=16,  Tail=true, color="green" );    
    Arrow2( transN(pqr, 4*d), r=0.1, $fn=6,  Tail=true, color="blue", Head=["r",0.2] );    
    Arrow2( transN(pqr, 5.5*d), r=0.1, $fn=6,  Tail=true, Head=false, color="teal"  );    
    Arrow2( transN(pqr, 7*d), r=0.1, $fn=6,  Tail=true, opt=["Head",false], color="olive"  );    
       
}    

module Arrow2_demo_tail2()
{
    echom("Arrow2_demo_tail2");
    pts = randPts(5);
    //pts = [[-0.97, 0.16, -1.24], [2.95, 0.51, -0.02], [-0.72, 1.79, 0.68]
    //      , [2.87, -2.22, -1.34], [-0.86, 1, -2.46]];
    pts= [[-0.64, 2.77, -1.94], [0.61, 2.62, 2.79], [-2.43, -2.85, 2.87]
         , [-2.1, 0.27, -0.75], [0.4, -1.22, -2]];
     
    echo(pts=pts);
    Arrow2( pts,Tail=["r",0.4],Head=["r",0.4], $fn=12, r=0.2,color="teal"); 
}    
//Arrow2_demo_tail2();


module Arrow_demo_2_defaultArrowSettings()
{
 _mdoc("Arrow_demo_2_defaultArrowSettings"
, "The arrow rad and len are auto set, if not specified. 
;; The following are Arrow( pq, [''r'',r]) 
;; at  r= 0.01, 0.025, 0.05, 0.1, 0.2, 0.3, 0.5
;; This could be used as a guide line to set arrow rad/len.
");   

    rs= [0.01, 0.025, 0.05, 0.1, 0.2, 0.3, 0.5];
    
    //LabelPt( [2,-1,4],
    //  "Arrows with different r:[0.01,0.025,0.05,0.1,0.2,0.3,0.5]"
    //);
    p = randPt(d=[0,3]);
    q = p + [2,2,2];
    pq3 = app([p,q], randPt());
    pq3=   [[-0.1, -0.3, -0.85], [2.9, 2.68, 2.85], [-0.9, -2, -2.33]];
    echopts("pq3= ",pq3);
     
    d = 15;    
    for( i = range(rs) )
    {  _pq = get(transN(pq3, d*rs[i]),[0,1]);
       pq = [_pq[0], onlinePt(_pq, ratio=rands(0.5,2,1)[0])];
       r = rs[i];
       L = d01(pq); 
       echo( _color(str("r= ", rs[i]
           , ", arr_r= ", (3*exp(-0.8*r)) *r
           , ", arr_l= ", min(10*exp(-0.5*r)*r, 2*L/5) 
           ), COLORS[i+1]
           )
           );   
       Arrow( pq, opt=["r", rs[i], "color", COLORS[i+1]]);
    }
  
}     
//Arrow_demo_2_defaultArrowSettings();

module Arrow_demo_3_lineProp()
{
 _mdoc("Arrow_demo_3_lineProp"
," Yellow: Arrow( pq, [''r'', 0.1, ''fn'',8, ''transp'', 0.8 ])
;; Red: Arrow( pq, [''r'', 0.1, ''heads'', false, ''color'',''red''] )
;; Green: Arrow( pq, [''r'', 0.1, ''headP'', false, ''color'',''green''] )
;; Blue: Arrow( pq, [''r'', 0.1, ''headQ'', false, ''color'',''blue''] )
"); 
//    Arrow(randPts(2));
//    Arrow( randPts(2)
//         , ["r", 0.1, "fn",8, "transp", 0.8 ]);   
    
    pq0= randPts(2);
    //MarkPts(pq0);
    //LabelPt( pq0[0], "  heads=false", ["color","red"]);
    Arrow( pq0        , opt= ["r", 0.1, "heads", false, "color","red"] );

    pq= randPts(2);
    MarkPts(pq);
    LabelPt( pq[0], "  P (headP=false)", ["color","green"]);
    Arrow( pq
        , opt=["r", 0.1, "headP", false, "color","green"] );

    pq2= randPts(2);
    MarkPts(pq2);
    LabelPt( pq2[1], "  Q (headQ=false)", ["color","blue"]);
    Arrow( pq2
        , opt=["r", 0.1, "headQ", false, "color","blue"] );
 
}    
//Arrow_demo_3_lineProp();

module Arrow_demo_4_arrowProp()
{
 _mdoc("Arrow_demo_4_arrowProp"
," Yellow: Arrow( pq, [''r'', 0.1,  ''headP'', [''color'',''red'']])
;; Red: Arrow( pq, [''r'', 0.1, ''color'',''blue''
;;                 , ''headQ'', [''color'',''green''
;;                               ,''r'', 0.5
;;                               ,''len'', 1.5
;;                               ,''fn'', 4
;;                               ]] )
"); 

    pq= randPts(2);
    MarkPts(pq);
    LabelPts( pq, [" P", " Q"] );
    Arrow( pq
         , opt=["r", 0.1,  "headP", ["color","red"]] );
    LabelPt( pq[0], "   Arrow(pq,[\"r\",0.1,\"headP\",[\"color\",\"red\"]])");
    
    pq2= randPts(2);
    MarkPts(pq2);
    LabelPts( pq2, [" P", " Q"] );
    
    Arrow( pq2
        , opt=["r", 0.1, "color","blue"
          , "headQ", ["color","green"
                      ,"r", 0.5
                      ,"len", 1.5
                      , "fn", 4
    ]] );
    
    color("blue")
    {LabelPt( pq2[0], "   Arrow( pq2, [\"r\", 0.1, \"color\",\"blue\" ");
    LabelPt( addy(pq2[0],-1.5), "   , \"headQ\", [\"color\",\"green\",\"r\",0.5,\"len\",1.5,\"fn\",4]])");
    }                  
 
}   
//Arrow_demo_4_arrowProp();    

module Arrow2_demo_curved()
{   echom("Arrow2_demo_curved");
    
    pqr = pqr();
    MarkPts( pqr, "ch;l=PQR");
    rad = 3;
    a = anglebylen( rad,rad, dist(pqr));
    pts = arcPts( pqr, rad=rad, a=a, n=18);
    Arrow2(pts);    
}
//Arrow2_demo_curved();    
