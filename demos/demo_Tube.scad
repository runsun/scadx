include <../scadx.scad>

//echo($fn=$fn);
//. W/o site

//Tube_demo_default();
//Tube_demo_color();
//Tube_demo_center();
//Tube_demo_partial_center();
//Tube_demo_Frame();
Tube_demo_actions();

module Tube_demo_default() 
{
   //--camera=1.2,0.5,1.7,50,0,45,13;
   //--img450,375;   
   echom("Tube_demo_default()");
   Tube(); //MarkPts=true);
   
   Tube($fn=36, Frame=false, th=0.5, actions=["x",3]);
   Tube($fn=4, th=0.5, actions=["y",3]);
   //translate([-2.5,00]) Tube(2,color="red");
   //translate([0,1.5,0]) Tube([3,1.5,1],color="green");
   //echo(PTS=PTS);
}

module Tube_demo_color()
{
   echo(_b("Tube_demo_color()")); 
   //--camera=[ 1.45, 1.65, 1.18 ],[ 65.50, 0.00, 51.10 ],21;
   
   Tube();
    
   color("red") Tube(3, actions=["y",2.5]);

   Tube([4,2,1], color=["green",0.5], actions=["x", 2.5]);

   Tube([4,2,1], actions=["y",-3], opt=["color",["blue",0.2]]);
   
   Tube(color=["teal",0.5], opt=["color", "black", "actions", ["t",[-2.5,0,0]] ] );
   	   
   echo(_color("Tube() : default", "olive"));
   echo(_color("color(\"red\") Tube(...): Using built-in coloring, which also colors the frame ", "red"));
   echo(_b("The following 2 use Tube()'s color setting, both work the same way:"));
   echo(_color("Tube(..., color=[\"green\",0.5]) // use plain arg", "darkgreen"));
   echo(_color("Tube(..., opt=[\"color\",[\"blue\",0.2]]) // use opt.color", "blue"));
   echo(_color("Tube(..., color=[\"teal\",0.5], opt=[\"color\",\"black\"]) // plain color overwrites opt.color", "teal"));
}

module Tube_demo_center()
{
   echo(_b("Tube_demo_center()"));     
   Tube(center=true);
   Tube(opt=["center", true, "color", ["red", 0.2]]); 
   //--camera=[ -0.63, 0.68, -1 ][ 58.50, 0.00, 47.80 ],13.8;
}

module Tube_demo_partial_center()
{
   //--camera=[ 1.15, 0.63, 0.88 ],[ 50.10, 0.00, 28.00 ],15;
   //--img450,375;   
   echo(_b("Tube_demo_partial_center()"));  
   Tube( center=[1,0,0], opt=["color", ["red", 0.2]] );
   Tube(opt=["center", [0,1,1], "color", ["green", 0.2]] );
   Tube(center=101, color=["blue", 0.2] ); 
   Tube( center=1, color=["olive", 0.2] );
}

module Tube_demo_Frame()
{
   //--camera=[ 1.94, 1.90, 1.23 ],[ 61.30, 0.00, 37.60 ],29;
   //--img450,375;   
   echo(_b("Tube_demo_Frame()"));  
      
   echo(_color("Default: [\"r\", 0.01]", "orange"));
   Tube();
   
   echo(_b("Frame set by simple arg"));
   echo(_color("Frame=0.1", "orange"));
   Tube( MarkPts=true, Frame=0.1,opt=["actions",["x",-2]]);
   _echo(_color("Frame=['color','red']", "orange"));
   Tube( Frame=["color","red"],opt=["actions",["x",-4]]);
   
   echo(_b("Frames are set by opt.Frame"));
   
   _echo(_color("['Frame', false]", "red"));
   Tube( opt=["actions",["x",2], "Frame",false, "color",["red",0.5]]);
   
   _echo(_color("['Frame', 0.05]", "green"));
   Tube( opt=["actions",["x",4], "Frame",0.05, "color",["green",0.5]]);
   
   _echo(_color("['Frame', ['color','red']]", "blue"));
   Tube( opt=["actions",["x",6], "Frame",["color","red"], "color",["blue",0.5]]);
   
   echo(_b("Frames set by opt.Frame, but denied by simple arg "));
   _echo(_color("Tube(...Frame=false, opt=['Frame', ['color','red']])", "purple"));
   Tube( Frame=false, opt=["actions",["x",8], "Frame",["color","red"], "color",["purple",0.5]]);
  
}
//Tube_demo_Frame();

//module Tube_demo_opt_Frame()
//{
//   //--camera=[ 4.38, 3.12, 0.29 ],[ 58.50, 0.00, 36.90 ],26;
//   //--img450,375;   
//   echo(_b("Tube_demo_opt_Frame()"));  
//      
//   1.5;
//   
//   echo(_orange("Default: [\"r\", 0.01]"));
//   Tube();
//
//   _echo(_b("opt=[ 'Frame', ['r', 0.1, 'color', ['red', 0.5]]]"));
//   
//   opt=[ "Frame", ["r", 0.1, "color", ["red", 0.5]]];
//   _echo(_red("opt=opt"), "  // Set Frame r with opt.Frame");
//   Tube( actions=["x",2], opt= update(["color",["red",0.5]],opt) );  
//   
//   _echo(_green("Frame=['r',0.2], opt=opt"), "  // opt.Frame is overwritten by Frame" );
//   Tube( actions=["x",4], Frame=["r",0.2], opt=update(["color",["green",0.8]],opt)); 
//   
//   _echo(_blue("color('blue') Tube( Frame=['r',0.2], opt=opt);"), " // colorizes everything" );
//   color("blue") Tube( actions=["x",6], Frame=["r",0.2], opt=update(["color",["green",0.5]],opt));
//    
//   _echo(_purple("Frame=['color','blue'], opt=opt"), "  // opt.Frame is overwritten by Frame" );
//   Tube( actions=["x",8], Frame=["color","blue"], opt=update(["color",["purple",0.5]],opt)); 
//   
//   _echo(_olive("Frame=false, opt=opt"), "  // Frame=false disables it" );
//   Tube( actions=["x",10], Frame=false, opt=update(["color",["olive",0.8]],opt)); 
//    
//}
////Tube_demo_opt_Frame();


module Tube_demo_actions()
{
   //--camera=[ 0.71, 1.57, 1.14 ],[ 52.90, 0.00, 33.40 ],23;
   //--img450,375;   
   echo(_b("Tube_demo_actions()"));  
      
   echo(_darkorange("Tube([3,2,1]"));   
   Tube(opt=["color",["gold", 0.6]]);
   
   _echo(_red("opt=['actions', ['rotx', -90]]"));
   Tube( opt=[ "actions", ["rotx", -90], "color", ["red",0.3]]);
   
   _echo(_darkgreen("center=true, opt=['actions', ['rotx', -90]]"), "   // actions with center");
   Tube( center=true, opt=[ "actions", ["rotx", -90], "color", ["green",0.3]]);
   
   _echo(_blue("opt=['actions', ['roty', -45, 'z',2]]"), " // multiple actions");
   Tube(  opt=[ "actions", ["roty", -45,"z",2], "color", ["blue",0.3]]);
   
   _echo(_purple("action=['y',3], opt=['actions', ['roty', -45, 'z',2]]"), "// actions expanded to opt.actions");
   Tube(  actions=["y", 3], opt=[ "actions", ["roty", -45,"z",2], "color", ["purple",0.3]]);
   
   // actions=false doesn't work in the following case, 'cos "actions" is NOT a sub
   // so setting actions=false won't disable it
   //
   // _echo(_olive("actions=false, opt=['actions', ['roty', -45, 'z',2]]"), " // actions=false disable it");
   // Tube(3,  actions=false, opt=[ "actions", ["roty", -45,"z",2], "color", ["olive",0.8]]);
}
//Tube_demo_actions();

//====================================================
//. With site

module Tube_demo_with_site()
{
   //--camera=[ 1.01, 1.19, 0.78 ],[ 52.90, 0.00, 39.70 ],17;
   //--img450,375;   
   echo(_b("Tube_demo_with_site()"));  
   
   //pqr=randPts(3,d=1.5); //pqr();
   pqr=[ [-1.10932, -0.855533, -0.748128]
       , [-0.00821536, 0.183257, -1.22133]
       , [-0.613839, 0.0589978, 0.955911]];
   //echo(pqr=pqr);
   MarkPts(pqr,Label="PQR");
   Tube(opt=["site",pqr, "markPts","l", "color",["gold",0.5]]);
   
   //pqr2= randPts(count=3, d=[1.5,2.5]);
   pqr2= [[1.52227, 2.15535, 2.35498]
         , [1.85897, 1.67035, 2.4865]
         , [2.24626, 1.89293, 1.92314]];
   
   //echo(pqr2=pqr2);
   MarkPts(pqr2,Label=true);
   Tube( opt=["site",pqr2, "markPts","l", "color",["red",0.3]]);
   
}
//Tube_demo_with_site();

module Tube_demo_center_with_site()
{
   //--camera=[ -1.01, 0.27, 0.27 ][ 329.60, 0.00, 198.50 ],13.5;
   //--img450,375;   
   echo(_b("Tube_demo_center_with_site()"));  
   
   //pqr=pqr();
   //echo(pqr=pqr);
   pqr=[[-1.28101, 0.494192, 0.97126]
       , [-1.15165, 0.0592333, -0.649364]
       , [1.24644, 0.341728, 1.73458]];
   MarkPts(pqr,"PQR");  
   Tube( center=true, opt=["site",pqr, "color",["gold",0.5]]);
     
}
//Tube_demo_center_with_site();

module Tube_demo_partial_center_with_site()
{
   echo(_b("Tube_demo_partial_center_with_site()"));  
   
   pqr=[[0.399011, -1.70855, 1.67873]
       , [0.164424, -1.06142, -1.56369]
       , [0.0152843, 1.56007, -1.22532]];
   echo(pqr=pqr);
   MarkPts(pqr,"PQR");  
   Tube( center=[0,1,0], opt=["site",pqr, "color",["gold",0.5]]);

   Tube( center=[1,0,1]
       , opt=["center",10, "site",pqr, "color",["red",0.3]]);
       
   //--camera=[ 0.43, -0.69, -0.32 ],[ 57.10, 0.00, 56.00 ],15.3;   
}
//Tube_demo_partial_center_with_site();


module Tube_demo_actions_with_site()
{
   echo(_b("Tube_demo_actions_with_site()"));  
   
   pqr= [[0.612148, -1.23256, 0.639392]
        , [-1.43049, -1.67262, 0.576152]
        , [-0.159001, -0.319813, -0.546865]
        ];   
   MarkPts(pqr,"PQR");
   
   Tube( opt=["site",pqr]);   
   Tube( opt=["actions", ["x",2],"site",pqr,"color",["red",0.5]]);
   
   Tube( opt=["actions",["roty",-45],"site",pqr,"color",["green",0.5]]);  
   Tube(  opt=["actions",["rotz",90, "rotx",-90],"site",pqr,"color",["blue",0.5]]);

   Tube( actions=["y",-1.5], opt=["actions",["rotz",90, "rotx",-90],"site",pqr,"color",["purple",0.5], "markPts","l"]);
   //--camera=[ -0.49, -1.47, 0.20 ],[ 65.50, 0.00, 51.10 ],21;
}
//Tube_demo_actions_with_site();

//. dim

//module Tube_demo_dim()
//{
//   //--camera=1.2,0.5,1.7,50,0,45,13;
//   //--img450,375;   
//   echo(_b("Tube_demo_dim()"));  
//   Tube([2,3,1], dim=[4,5,0], markPts="l");
//   
//   Tube([2,3,1], actions=["y", 4], dim=[[7,0,6]], markPts="l", color=["red",.5]);
//   Tube([2,3,1], site=randPts(3, d=[-1,-2]), dim=[0,1,3], markPts="l", color=["green",.5]);
//   
//   Tube([2,3,1], actions=["x", 4], dim=[6, [5,1.5,1], 2], markPts="l", color=["purple",.5]);
//   
//   echo(_olive("Tube(...dim=[4,5,0]...)"));
//   echo(_red("Tube(...dim=[[7,0,6]]...) // Enter 3 indices together"));
//   echo(_green("Tube(...site=pts, dim=[0,1,3]...) // Works with site, too"));
//   echo(_purple("Tube(...dim=[ 6, [5,1.5,1], 2 ] ...) // Enter the site of any pt"));
//   
//}
////Tube_demo_dim();
//
//module Tube_demo_dim2()
//{
//   //--camera=1.2,0.5,1.7,50,0,45,13;
//   //--img450,375;   
//   echo(_b("Tube_demo_dim2()"));  
//   //Tube([6,4,2], dim=[[4,5,0]], markPts="l");
//   
//   [4,2,1];
//   Tube( dim=[[6,7,3],[7,3,1],[7,0,1]],  markPts="l");
//   
//   Tube( actions=["y", 4], dim= [ "pqrs", [[7,6,1]]
//                                     , "color", "green" ]
//        , markPts="l", color=["red",.2]
//        );
//        
//   Tube( actions=["x", 6], dim=["pqrs",[[6,7,3],[7,3,1],[7,0,1]]
//                                     ,"color","red"]
//        , markPts="l", color=["green",.2]
//        );
//        
//   Tube( actions=["y", -5], dim=[ "pqrs",[["pqr",[6,7,3], "color","green"]
//                                              ,[7,3,1]
//                                              ,[7,0,1]
//                                              ]
//                                     ,"color","purple"]
//        , markPts="l", color=["blue",.2]
//        );
// 
//}
////Tube_demo_dim2();
