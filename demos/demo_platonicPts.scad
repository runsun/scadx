include <../scadx.scad>

module demo_platonicPts_icosa()
{
  echom("demo_platonicPts_icosa()");
  pf = platonicPts("icosa", debug=0);
  MarkPts(pf[0]);
  echo(pts=pf[0]);
  echo(faces=pf[1]);
  //P1j = [0.894427, 0, 0];
  //Mark90( [pf[0][0], P1j, pf[0][2]] );
   
  MarkFaces( pf[0], pf[1] , color="purple");
   
  color("gold",0.5)
  polyhedron( pf[0],pf[1]);
  
  //for(i=range(pf[0]))
  //echo(i=i, dist(ORIGIN, pf[0][i]) );
  
  CEF0=get_clocked_EFs_at_pi(pf[1],0);
  echo(CEF0=CEF0);
  
  efis = get_edgefis( faces = pf[1] );
  echo(efis = efis );
  
}
//demo_platonicPts_icosa();

module demo_platonicPts_icosa_trialgulate()
{
  echom("demo_platonicPts_icosa_trialgulate()");
  pf = platonicPts("icosa", debug=0);
  MarkPts(pf[0]);
  echo(pts=pf[0]);
  echo(faces=pf[1]);
  //P1j = [0.894427, 0, 0];
  //Mark90( [pf[0][0], P1j, pf[0][2]] );
   
  MarkFaces( pf[0], pf[1] , color="purple");
   
  color("gold",0.5)
  polyhedron( pf[0],pf[1]);
  
  edges = get_edges( pf[1] );
  echo(edges = edges, len(edges) );
  
  h_edge_mps=[
  
  
  ]
  
//  for(i=range(pf[0]))
//  echo(i=i, dist(ORIGIN, pf[0][i]) );
//  
//  CEF0=get_clocked_EFs_at_pi(pf[1],0);
//  echo(CEF0=CEF0);
//  
//  efis = get_edgefis( faces = pf[1] );
//  echo(efis = efis );
  
}
demo_platonicPts_icosa_trialgulate();
