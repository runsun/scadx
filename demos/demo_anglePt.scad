include <../scadx.scad>

ISLOG=1;

//demo_anglePts();
module demo_anglePts()
{
  echom("demo_anglePts()");
  
  size=4;
  P=X*size;
  R=Y*size+X;
  S=Z*size/2;
  
  MarkPts( [P,O,R], "PQR", Line=["r",0.015,"color","black"] );
  //MarkPts( [O,S], " S", Line=["r",0.015,"color","black"] );
  
  data = [[30,45],3];//,[60,30],2,[-30,-30],2,[0,90],1]; //,60,undef, -60, 1];
  
  for( i= range(keys(data)) )//as= keys(data) )
  {
    //echo(log_b(["For i=",i]));
    as = data[2*i];
    len = data[2*i+1];
    a= as[0]; a2=as[1]; a3=as[2];
    pt= anglePt( [P,O,R], a=a, a2=0, len= len );
    pt2= anglePt( [P,O,R], a=a, a2=a2, len= len );
    pt3= anglePt( [P,O,R], a=a, a2=a2, a3=as[2], len= len );
    label= "anglePt ( pqr, a={_}, a2={_}, a3={_}, len={_} )";
    
    
    //echo(log_(["i",i, "pt",pt]));
    //J2= projPt(pt2, [P,O]);
    //MarkPt(J2, "J2");
    
    Red(){
      MarkPt( pt, Label=_s(label,[a,0,0,len] ), Grid=2 ); 
      Line( [O,pt] );
      //Arrow2( arcPts( [P,O,pt], n=5, rad= 3 ));
      _red( _s( "pt={_}, angle={_}", [pt, angle([pt,O,P])]));
      
    }
    
    Green(){
      MarkPt( pt2, Label=_s(label,[a,,a2,0,len] ), Grid=2 ); 
      Line( [O,pt2] );
      //Arrow2( arcPts( [pt,O,pt2], n=5 ));
      Mark90( [pt2, projPt(pt2,[pt,O]), O] );
      _green(_s( "pt2={_}, angle={_}", [pt2, angle([pt2,O,pt])]));
    }
        
    
    //echo(log_e(""));
  }
}


//demo_anglePts_a();
module demo_anglePts_a()
{
  echom("demo_anglePt_a()");
  
  P=X*4;
  R=Y*3+X;
  MarkPts( [P,O,R], "PQR", Line=["r",0.03,"color","black"] );
  
  data = [0,4,15,2,30,2,45,2,60,2,75,2.5,90,3
         ,120,2,150,2,180,2
         ,210,2,240,2,270,2,300,2,330,2, 360,2, 370,2]; //[45,undef, 30,-1,-60, 1, 150,2, 180,4.5]; //, 90,4, -90, 2 ];
  //for( a= keys(data) )
  for( i= range(keys(data))) //a= keys(data) )
  {
    a = data[i*2];
    len = h(data, a);
    pt= anglePt( [P,O,R], a=a, len= len );
    lab= _s("a={_},len= {_}",[a,len] );
    color(COLORS[i])
    MarkPts( [ O,pt]
           , Label=["text",["",lab ]]  );
    _color( angle( [P,O,pt] ), COLORS[i] );          
  }
}


//demo_anglePts_negative_a();
module demo_anglePts_negative_a()
{
  echom("demo_anglePts_negative_a()");
  
  P=X*3;
  R=Y*3+X;
  MarkPts( [P,O,R], "PQR", Line=["r",0.03,"color","black"] );
  
  data = [0,4, 15,2,30,2,45,2,60,2,75,2.5,90,3
         ,120,2,150,2,180,2
         ,210,2,240,2,270,2,300,2
         ,330,2, 360,2, 370,2
         ]; 
         
  for( i= range(keys(data))) 
  {
    a = -data[i*2];
    len = data[i*2+1]; //h(data, a);
    pt= anglePt( [P,O,R], a=a, len= len );
    lab= _s("a= {_},len= {_}",[a,len] );
    color(COLORS[i])
    MarkPts( [ O,pt]
           , Label=["text",["",lab ]]  );
    _color( angle( [P,O,pt] ), COLORS[i] );          
  }
}


//echo(test = -370%360);
//demo_anglePts_more();
module demo_anglePts_more()
{
  echom("demo_anglePts_more()");
  
  P=X*3;
  R=Y*3;
  MarkPts( [P,O,R], "PQR", Line=["r",0.03,"color","black"] );
  
  data = [45,undef,30,-1,-60, 1, 150,2, 180,4.5]; //, 90,4, -90, 2 ];
  //for( a= keys(data) )
  for( i= range(keys(data))) //a= keys(data) )
  {
    a = data[i*2];
    len = data[i*2+1];
    pt= anglePt( [P,O,R], a=a, len= len );
    lab= _s("anglePt ( pqr, a={_}, len= {_} )",[a,len] );
    color(COLORS[i])
    MarkPts( [ O,pt]
           , Label=["text",["",lab ]]  );
    _color( angle( [P,O,pt] ), COLORS[i] );          
  }

}


//demo_anglePts_a2();
module demo_anglePts_a2()
{
  echom("demo_anglePt_a2()");
  
  P=X*4;
  R=Y*3+X;
  MarkPts( [P,O,R], "PQR", Line=["r",0.03,"color","black"] );
  
  data = [0,4,15,2,30,2,45,2,60,2,75,2.5,90,3
         ,120,2,150,2,180,2
         ,210,2,240,2,270,2,300,2,330,2, 360,2, 370,2]; //[45,undef, 30,-1,-60, 1, 150,2, 180,4.5]; //, 90,4, -90, 2 ];
  
  P0= anglePt( [P,O,R], a=45, len=3); echo(P0=P0); MarkPt(P0, "P0");
  for( i= range(keys(data))) //a= keys(data) )
  {
    a2 = data[i*2];
    len = data[i*2+1];
    pt= anglePt( [P,O,R], a=45, a2=a2, len= len );
    lab= _s("a=45, a2={_},len= {_}",[a2,len] );
    color(COLORS[i])
    MarkPts( [ O,pt]
           , Label=["text",["",lab ]]  );
    got_a = angle( [P0,O,pt] );       
    _color( _s("a2: want: {_} got: {_} ", [a2, got_a==a2? _green(got_a): _red(got_a)] )
           , COLORS[i] );          
  }
}



//debug_anglePts_a2();
module debug_anglePts_a2()
{
  echom("debug_anglePts_a2()");
  
  //P=X*4;
  //R=Y*3+X;
  pqr = [[1.31, -0.37, 0.3], [1.67, 0.73, 1.16], [-1.82, -1.3, 1.19]];
  P= pqr[0];
  Q= pqr[1];
  R= pqr[2];
  MarkPts( pqr, "PQR", Line=["r",0.03,"color","black"] );
  
  data = [[180,0],undef, [45,180], undef]; //[45,undef, 30,-1,-60, 1, 150,2, 180,4.5]; //, 90,4, -90, 2 ];
  
  //P0= anglePt( pqr, a=45, len=3); echo(P0=P0); MarkPt(P0, "P0");
  for( i= range(keys(data))) //a= keys(data) )
  {
    a = data[i*2][0];
    a2 = data[i*2][1];
    len = data[i*2+1];
    //pt= anglePt( pqr, a=45, a2=a2, len= len );
    pt= anglePt( pqr, a=a, a2=a2, len= len );
    lab= _s("a={_}, a2={_}, len= {_}",[a,a2, len] );
    color(COLORS[i])
    MarkPts( [ Q,pt]
           , Label=["text",["",lab ]]  );
    got_a = angle( [P0,Q,pt] );       
   // _color( _s("a2: want: {_} got: {_} ", [a2, got_a==a2? _green(got_a): _red(got_a)] )
   //        , COLORS[i] );          
  }
}


//demo_anglePts_neg_a2();
module demo_anglePts_neg_a2()
{
  echom("demo_anglePts_neg_a2()");
  
  P=X*4;
  R=Y*3+X;
  MarkPts( [P,O,R], "PQR", Line=["r",0.03,"color","black"] );
  
  data = [0,4,15,2,30,2,45,2,60,2,75,2.5,90,3
         ,120,2,150,2,180,2
         ,210,2,240,2,270,2,300,2,330,2, 360,2, 370,2]; //[45,undef, 30,-1,-60, 1, 150,2, 180,4.5]; //, 90,4, -90, 2 ];
  
  P0= anglePt( [P,O,R], a=45, len=3); echo(P0=P0); MarkPt(P0, "P0");
  for( i= range(keys(data))) //a= keys(data) )
  {
    a2 = data[i*2];
    len = data[i*2+1];
    pt= anglePt( [P,O,R], a=45, a2=-a2, len= len );
    lab= _s("a=45, a2={_},len= {_}",[-a2,len] );
    color(COLORS[i])
    MarkPts( [ O,pt]
           , Label=["text",["",lab ]]  );
    got_a = angle( [P0,O,pt] );       
    _color( _s("a2: want: {_} got: {_} ", [a2, got_a==a2? _green(got_a): _red(got_a)] )
           , COLORS[i] );          
  }
}


//demo_anglePts_a2_more();
module demo_anglePts_a2_more()
{
  echom("demo_anglePts_a2_more()");
  
  size=4;
  P=X*size;
  R=Y*size;
  S=Z*size/2;
  
  MarkPts( [P,O,R], "PQR", Line=["r",0.015,"color","black"] );
  MarkPts( [O,S], " S", Line=["r",0.015,"color","black"] );
  
  data = [[30,0],2,[30,30],2,[60,30],2,[-30,-30],2,[0,90],1]; //,60,undef, -60, 1];
  for( as= keys(data) )
  {
    len = h(data, as);
    a= as[0]; a2=as[1];
    pt= anglePt( [P,O,R], a=a, a2=a2, len= len );
    lab= _s("anglePt ( pqr, a={_}, a2={_}, len={_} )",[a,a2,len] );
    MarkPt( pt, Label=lab, Grid=3 ); 
    Line( [O,pt] );
  }

}
