include <../scadx.scad>

//demo_quadrant();
module demo_quadrant() 
{
  echom("demo_quadrant()");
  
  pts0 = randPts(40);
  //Rf = randPt();
  pts = [for(pt=pts0) newz(pt,0) ];
  //MarkPt(Rf, color="black");
  Line( [Rf-5*X, Rf+5*X] );
  Line( [Rf-5*Y, Rf+5*Y] );
  for(i=range(pts))
  {
    pt=pts[i];
    q= quadrant(pt); //, Rf);
    MarkPt( pt,  q==0?"++":q==1?"-+":q==2?"+-":q==3?"--":undef, color=COLORS[q+1]);
  }
}


//demo_quadrant_translated_site();
module demo_quadrant_translated_site() 
{
  echom("demo_quadrant_translated_site()");
  
  Rf = randPt();
  
  pqr = translatePts( [ X,O,Y], Rf);   
  pts0 = randPts(40);
  pts = [for(pt=pts0) newz(pt,0)+Rf ];
  //MarkPt(Rf, color="black");
  Line( [Rf-5*X, Rf+5*X] );
  Line( [Rf-5*Y, Rf+5*Y] );
  for(i=range(pts))
  {
    pt=pts[i];
    q= quadrant(pt, site=pqr); //, Rf);
    MarkPt( pt,  q==0?"++":q==1?"-+":q==2?"+-":q==3?"--":undef, color=COLORS[q+1]);
  }
}


//demo_quadrant_for_non_planar_pts();
module demo_quadrant_for_non_planar_pts() 
{
  echom("demo_quadrant_for_non_planar_pts()");
  
  Rf = randPt();
  
  pqr = translatePts( [ X,O,Y], Rf);   
  pts0 = randPts(40);
  pts = [for(pt=pts0) pt+Rf ];
  //MarkPt(Rf, color="black");
  Line( [Rf-5*X, Rf+5*X] );
  Line( [Rf-5*Y, Rf+5*Y] );
  for(i=range(pts))
  {
    pt=pts[i];
    q= quadrant(pt, site=pqr); //, Rf);
    MarkPt( pt,  q==0?"++":q==1?"-+":q==2?"+-":q==3?"--":undef, color=COLORS[q+1]);
  }
}


//demo_quadrant_any_site();
module demo_quadrant_any_site() 
{
  echom("demo_quadrant_any_site()");
  
  pqr=pqr();
  pqr= [[-1.35, -1.29, -1.95], [-1.77, -1.82, -1.03], [-1.17, 1.52, -1.14]];
  echo(pqr=pqr);
  P=pqr[0]; Q=pqr[1]; R=pqr[2]; 
  N= N(pqr);
  
  pts0 = randPts(50);
  pts = [for(pt=pts0) projPt(pqr, pt+pqr[1]) ];
  
  Black()
  { MarkPts(pqr, "PQR");
    MarkPts([N,Q],Label=["text",["N",""]]);
    Mark90([N,Q,P]);
    Mark90([N,Q,R]);
  }
  Gold(1) Plane(pqr);
  
  ex = 4;
  DashLine( [ ex*Q-(ex-1)*R, -(ex-1)*Q+ex*R] );
  DashLine( [ ex*Q-(ex-1)*P, -(ex-1)*Q+ex*P] );
  
  for(i=range(pts))
  {
    pt=pts[i];
    q= quadrant(pt, pqr);
    MarkPt( pt, q==0?"++":q==1?"-+":q==2?"+-":q==3?"--":undef, color=COLORS[q+1]);
  }
}


//dev_quadrant_any_site_for_non_planar_pts_NOT_WORKING();
module dev_quadrant_any_site_for_non_planar_pts_NOT_WORKING() 
{
  echom("dev_quadrant_any_site_for_non_planar_pts_NOT_WORKING()");
  
  _h3(_red("Determine non_planar_pts's quadrant only works when the site is parallel to [X,O,Y]" ));
  _pqr=pqr();
  _pqr= [[-1.35, -1.29, -1.95], [-1.77, -1.82, -1.03], [-1.17, 1.52, -1.14]];
  pqr = _pqr; //newz(_pqr, N(_pqr));
  echo(pqr=pqr);
  P=pqr[0]; Q=pqr[1]; R=pqr[2]; 
  N= N(pqr);
  
  pts0 = randPts(40);
  pts = [for(pt=pts0) pt+pqr[1] ];
  
  Black()
  { MarkPts(pqr, "PQR");
    MarkPts([N,Q],Label=["text",["N",""]]);
    Mark90([N,Q,P]);
    Mark90([N,Q,R]);
  }
  Gold(1) Plane(pqr);
  
  ex = 4;
  DashLine( [ ex*Q-(ex-1)*R, -(ex-1)*Q+ex*R] );
  DashLine( [ ex*Q-(ex-1)*P, -(ex-1)*Q+ex*P] );
  
  for(i=range(pts))
  {
    pt=pts[i];
    q= quadrant(pt, pqr);
    MarkPt( pt, q==0?"++":q==1?"-+":q==2?"+-":q==3?"--":undef, color=COLORS[q+1]);
  }
}



//demo_groupByQuadrant();
module demo_groupByQuadrant() 
{
  echom("demo_groupByQuadrant()");
  
  pts0 = randPts(80);
  Rf = randPt();
  MarkPt(Rf, color="black");
  Red() Line( [Rf-5*X, Rf+5*X] );
  Red() Line( [Rf-5*Y, Rf+5*Y] );

  pts = [for(pt=pts0) pt+Rf ];
  gs = groupByQuadrant(pts, Rf);
  
  for(i=[0:3])
  {
     g=gs[i];
     Color(COLORS[i+1]) MarkPts( g, Label=0);  
  }
  
}



//demo_groupByQuadrant_missing_pts();
module demo_groupByQuadrant_missing_pts() 
{
  echom("demo_groupByQuadrant_missing_pts()");
  
  pts0 = randPts(80);
  Rf = randPt();
  MarkPt(Rf, color="black");
  Red() Line( [Rf-5*X, Rf+5*X] );
  Red() Line( [Rf-5*Y, Rf+5*Y] );

  pts = [for(pt=pts0) if(!(pt.x<0 && pt.y<0)) pt+Rf ];
  gs = groupByQuadrant(pts, Rf);
  
  for(i=[0:3])
  {
     g=gs[i];
     Color(COLORS[i+1]) MarkPts( g, Label=0);  
  }
  
}


module demo_octant() 
{
  echom("demo_octant()");
  
  pts0 = randPts(100);
  Rf = randPt();
  pts = [for(pt=pts0) pt+Rf ];
  MarkPt(Rf, color="black");
  Line( [Rf-5*X, Rf+5*X] );
  Line( [Rf-5*Y, Rf+5*Y] );
  Line( [Rf-5*Z, Rf+5*Z] );
  for(i=range(pts))
  {
    pt=pts[i];
    q= octant(pt, Rf);
    MarkPt( pt, i, color=COLORS[q]);
  }
  
  r=2.5;
  
  Gold(.8) Plane( [ r*X+r*Y+Rf, -r*X+r*Y+Rf, -r*X-r*Y+Rf, r*X-r*Y+Rf] );
  Gold(.8) Plane( [ r*X+r*Z+Rf, -r*X+r*Z+Rf, -r*X-r*Z+Rf, r*X-r*Z+Rf] );
  Gold(.8) Plane( [ r*Z+r*Y+Rf, -r*Z+r*Y+Rf, -r*Z-r*Y+Rf, r*Z-r*Y+Rf] );
  
}
//demo_octant();

module demo_groupByOctant() 
{
  echom("demo_groupByOctant()");
  
  pts0 = randPts(100);
  Rf = randPt();
  pts = [for(pt=pts0) pt+Rf ];
  MarkPt(Rf, color="black");
  Line( [Rf-5*X, Rf+5*X] );
  Line( [Rf-5*Y, Rf+5*Y] );
  Line( [Rf-5*Z, Rf+5*Z] );
  
  gs = groupByOctant(pts, Rf);
  
  echo(len_gs = len(gs));
  
    for(i=[0:7])
  {
     g=gs[i];
     Color(COLORS[i]) MarkPts( g, Label=0);  
  }
  
  r=2.5;
  
  Gold(.8) Plane( [ r*X+r*Y+Rf, -r*X+r*Y+Rf, -r*X-r*Y+Rf, r*X-r*Y+Rf] );
  Gold(.8) Plane( [ r*X+r*Z+Rf, -r*X+r*Z+Rf, -r*X-r*Z+Rf, r*X-r*Z+Rf] );
  Gold(.8) Plane( [ r*Z+r*Y+Rf, -r*Z+r*Y+Rf, -r*Z-r*Y+Rf, r*Z-r*Y+Rf] );
  
}
//demo_groupByOctant();
