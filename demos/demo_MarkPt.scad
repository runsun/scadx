include <../scadx.scad>

//demo_MarkPt_Ball();
//MarkPt_demo_Label();
//MarkPt_demo_Label2();
//MarkPt_demo_Label_format();
//demo_MarkPt_Grid();
MarkPt_demo_scale();
//demo_MarkPt_Link();
//demo_MarkPt_Link_steps();

//demo_MarkPt_Link_steps();


module demo_MarkPt_Ball()
{ 
  echom("demo_MarkPt_Ball()");
  
  pt = randPt();
  d=[0.5,0.5,0];//MarkPt(pt);
  
  //PtGrid(pt); 
  Line([ORIGIN,pt]);
  //PtGrid(pt, mode=3);
  
  MarkPt( pt, Label="default" );
  MarkPt( pt-d, text=true );
  
  MarkPt( pt-d, "Ball=true", Ball=true, color="red" );
  MarkPt( pt-2*d, "Ball=true, r=0.1", Ball=true, color="green", r=0.1 );
  
  MarkPt( pt-3*d, "Ball=[\"color\",\"blue]", Ball=["color","blue"] );
  MarkPt( pt-4*d, "color=\"green\", Ball=[\"color\",\"purple]",color="green", Ball=["color","purple"] );
  MarkPt( pt-5*d, "Ball=[\"color\",\"teal\", \"r\", LINE_R]", Ball=["color","teal", "r", LINE_R] );
  
  MarkPt( pt-6*d, "<-- Ball=false", Ball=false, color="orange", opt=["Ball", ["r", 6*LINE_R]] );
  
  MarkPt( pt-7*d, "<-- opt=[\"Ball\",false]", color="red", opt=["Ball", false] );
  
  MarkPt( pt-8*d, "Ball=[\"r\", 6*LINE_R]", color="olive", opt=["Ball", ["r", 6*LINE_R]] );
  
}    
//demo_MarkPt_Ball();


module MarkPt_demo_Label()
{
    echom("MarkPt_demo_Label");
    P=randPt(d=1); 
    d= [-0.35,0.35,0];
    
    /* 3 ways to show coordinate:
    
    MarkPt( P, Label=true );
    MarkPt( P, opt=[ "Label", true] );
    MarkPt( P, opt=[ "Lable", ["text", true] ] );
    
    */
    
    MarkPt(P, Label=true, Grid=["mode",3]); 
    MarkPt(P+d, "Q"); 
    MarkPt(P+2*d, "text"); 
    MarkPt(P+2.5*d, 33); 
    MarkPt(P+3*d, "followView=false", opt=["Label",["followView",false]]); 
    
    MarkPt(P+4*d, color="red", opt=["Label", true]); 
    MarkPt(P+5*d, color="red", opt=["Label", "opt.text"]); 
    
    MarkPt(P+6*d, color="green", opt=["Label", 3]); 
    MarkPt(P+7*d, color="green", opt=["Label", "3"]); 
    MarkPt(P+8*d, color="green", opt=["Label", [3]]); 
    MarkPt(P+9*d, color="green", opt=["Label", [0,1,2]]); 
    
    MarkPt(P+10*d, color="blue", opt=["Label", ["text",4]]); // <=== this shows up
    MarkPt(P+11*d, color="blue", opt=["Label", ["text",true]]); 
    MarkPt(P+12*d, color="blue", opt=["Label", ["text",[0,1,2]]]); 
    
}
//MarkPt_demo_Label();

module MarkPt_demo_Label2()
{
    echom("MarkPt_demo_Label2");
    P=randPt(d=1); 
    d= [-0.35,0.35,0];
    
    //MarkPt(P, Label=true, Grid=["mode",3]); 
    MarkPt(P+d, "halign: center", color="red", opt=["Label",["halign","center"]]); 
    MarkPt(P+2.5*d, "indent: 0.5", color="green", opt=["Label",["indent",0.5]]); 
    MarkPt(P+4*d, "indent: 0.5", color="green", Label=["indent",0.5]); 
    MarkPt(P+6*d, "spacing: 2", color="blue", opt=["Label",["spacing",2]]); 
    MarkPt(P+8*d, Label=["text", "halign: right"
                        , "halign", "right"
                        , "indent", -0.2
                        ], color="purple"
               ); 
    MarkPt(P+10*d, Label=["text", "valign: bottom"
                        , "valign", "bottom"
                        ], color="purple"
               ); 
    MarkPt(P+12*d, Label=["text", "valign: top"
                        , "valign", "top"
                        ], color="purple"
               ); 
    MarkPt(P+16*d, "scale: [2,4,20]", color="teal", opt=["Label",["scale", [2,4,20]]]); 
    
}
//MarkPt_demo_Label2();


module MarkPt_demo_Label_format()
{
    echom("MarkPt_demo_Label_format");
    P = randPt(d=1);
    d= [0.5,-0.5,0];
    Q=P+d; 
    R=Q+d;
    S=R+d;
    T=S+d;
    U=T+d;
    
    
    MarkPt( P, "format features are still under construction", color="red" );
    MarkPt( Q, Label=["format","{*}"], color="green" );
    MarkPt( R, Label=["format","{*`a|d1|wr=/,/"], color="blue" );
    MarkPt( S, Label=["format","{*`a%|d2}"], color="purple" );
    MarkPt( T, Label=["format","{*`a%|d2}"], color="teal" );
    MarkPt( U, Label=["format","{*`a%|d2}"], color="olive", opt=["text",true] );

}
//MarkPt_demo_Label_format();


module demo_MarkPt_Grid()
{
  echom("demo_MarkPt_Grid()");
  
  pt = randPt();
  d=[0.5,0.5,-0.2];
  
  MarkPt( pt );
  MarkPt( pt-d, color="red", Grid=true );
  MarkPt( pt-2*d, opt=["Grid",1, "color","green"] );
  MarkPt( pt-3*d, opt=["Grid",true, "color","blue"] );
  MarkPt( pt-4*d, Grid=3, opt=["color","purple"] );
  
  MarkPt( pt-5*d, Grid=["color","red"], opt=["color","teal"] );
  
  MarkPt( pt-6*d, Grid=["color",["blue",0.2], "r",3*LINE_R, "mode",3]);
  
} 
//demo_MarkPt_Grid();




//module MarkPt_demo_sopt()
//{
//    echom("MarkPt_demo_sopt");
//        
//    P = randPt();
//    d= [-0.5,0.5,0];
//    Q=P+d; 
//    R=Q+d;
//    S=R+d;
//    MarkPt(P);
//    MarkPt(Q);
//    MarkPt(R);
//    MarkPt(S);
//        
//    MarkPt( P, opt=["color","red"] );
//    MarkPt( Q, opt="color=green"); 
//    MarkPt( R, opt="color=blue;text=testing the sopt feature" );
//    MarkPt( S, color="purple", opt="indent=1" );
//}
//MarkPt_demo_sopt();


module MarkPt_demo_scale()
{
    echom("MarkPt_demo_scale");
        
    P = ORIGIN; //randPt();
    d= [0,0.5,0];
    Q=P+d; 
    R=Q+d;
    S=R+d;
    T=S+d+d;
    U=T+2*d;
        
    MarkPt( P, "|scadx| default" );
    MarkPt( Q, Label=["text","|scadx| scale=0.5", "scale", 0.5], opt=["color","red"]); 
    MarkPt( R, "|scadx| scale=1", opt=["Label",["scale",1], "color","green"] );
    MarkPt( S, "|scadx| scale=2", opt=["Label",["scale",2]], color="blue" );
    MarkPt( T, Label=["text","|scadx| scale=2, opt.scale=2", "scale",[2,1,15], "color","teal"]
          , opt=["Label",["scale",2]] );
}
//MarkPt_demo_scale();

module demo_MarkPt_Link()
{
  echom("demo_MarkPt_Link()");
  pts = cubePts([2,2,1]);
  faces = rodfaces(4);
  P=pts[5]; Q=pts[6]; R=pts[1];

  MarkPt(Q, "MarkPt(Q,... Link=pt)", Link=Q+[-1,1,0.5], opt=["color", "red"] );
  MarkPt(Q, "MarkPt(Q,... Link=[p1,p2])", Link=[Q+[-1,1,1],Q+[-1,2,1]] , opt=["color", "green"] );
  
  lpts= growPts( P+[0,-1,1], ["z", 1, "y", 5] );
  Blue() MarkPt(P, "MarkPt(Q,... Link=linkpts)"
         , Ball=0.2
         , Link = lpts
         //, opt=["color", "blue"]
         );
         
  MarkPt(pts[7], "MarkPt(...Ball=0,...)", Ball=0, Link= pts[7]+[0,1,0.5] );
         
  MarkPt(pts[3], "MarkPt(...Link=[...\"Arrow\",false])", Ball=0
                 , Link= ["pts",pts[3]+[0,1,1],"Arrow",false] );
                        
  MarkPt(pts[3], Ball=0
        , Label=["text", "MarkPt(...Link=[...\"color\",\"red\"])"
                ,"followView", false
                ]
       , Link= ["pts",[pts[3]+[0,0.5,-.5], pts[3]+[0,1,-.5]]
               ,"color","red"] );       
  
  MarkPts(pts, label=true, line=false);  
  echo( "faces = ", faces);     
  //DrawEdges(pts=pts,faces=faces);
  Teal(0.5) polyhedron(pts, faces);

}
//demo_MarkPt_Link();

module demo_MarkPt_Link_steps()
{
  echom("demo_MarkPt_Link_steps()");
  echo("Using the 'steps' in Link to generate pts");
  
  pts = cubePts([2,2,1]);
  faces = rodfaces(4);
  P=pts[5]; Q=pts[6]; R=pts[1];
 
 MarkPts([P,Q,R],"PQR", Line=0);

 Red() MarkPt(Q, "MarkPt(Q,... Link=pt)", Link=Q+[-1,1,0.5] );
 Green() MarkPt(Q, "MarkPt(Q,... Link=[\"steps\",[\"z\",1.5,\"y\",1]])"   
            , Link=["steps", ["z",1.2,"y",1]]  );
  
  lpts= growPts( P+[0,-1,1], ["z", 1.5, "y", 5] );
  Blue() MarkPt(P, "MarkPt(Q,... Link=[\"steps\",[\"t\",[0,-1,1], \"z\",1,\"y\", .5])"
         , Ball=0.2
         , Link = ["steps",["t",[0,-1,1], "z",1,"y", .5] ]
         );
  Teal() MarkPt(pts[7], "MarkPt(...Ball=0, Link=[\"steps\",[\"y\",1]])"
               , Ball=0, Link= ["steps",["y",1]] );
         
  DrawEdges(pts=pts,faces=faces);
  Teal(0.5) polyhedron(pts, faces);

}


module MarkPt_demo_actions()
{
    echom("MarkPt_demo_actions");
        
    P = ORIGIN; //randPt();
    d= [0,0.5,0];
    Q=P+d; 
    R=Q+d;
    S=R+d;
    T=S+d;
    U=T+d;
    V=U+2*d;
    W=V+2*d;
    A=W+2*d;
        
    MarkPt( P, "default" );
    MarkPt( Q, Label=["text", "Q. actions=[\"rotx\", 90] failed cos followView must be false"
                       ,"actions",["rotx", 90]] ); 
    MarkPt( R, color="red"
               , Label=["text", "R. actions=[\"rotx\", 90], followView=false"
                       ,"actions",["rotx", 90], "followView", false] );
                        
    MarkPt( S, color="green", Label="S.followView=false",opt=["Label",["followView",false]] ); 
    
    MarkPt( T, color="blue", Label=["text","T.actions=[\"rotz\",45, \"x\",0.5],followView=false"
                                   , "actions", ["rotz", 45,"x",0.5]
                                   , "followView", false
                                   ] );
                                   
                                   
    MarkPt( U, color="purple", "U. actions=[\"x\",0.5, \"rotz\",45],followView=false",actions=["x",0.5, "rotz", 45], followView=false ); 
    MarkPt( V, color="teal"
              , "V. actions=[\"rotz\",45, \"rotx\",90,\"y\",0.2], halign=\"center\",followView=false"
              , opt=[ "Label"
                    , [ "halign", "center"
                      , "actions", ["rotz", 45, "rotx",90, "y",0.2]
                      , "followView", false]
                    ] ); 
    
    MarkPt( W, color="black"
            , "W. opt=[\"actions\",[\"rotz\",45]], halign=\"center\",followView=false"
            , opt=["Label", ["halign","center"
                            ,"actions",["rotz", 45]
                            ,"followView", false
                            ]
                  ]); 

    
}
//MarkPt_demo_actions();
