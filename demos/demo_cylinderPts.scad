include <../scadx.scad>

//echo($fn=$fn);
//. W/o site

module cylinderPts_demo_default()
{
   echo(_b("cylinderPts_demo_default()"));  
   pts = cylinderPts();
   MarkPts(pts); 
}
//cylinderPts_demo_default();



module cylinderPts_demo_fn()
{
   echo(_b("cylinderPts_demo_fn()"));    
   pts = cylinderPts(opt=["$fn", 48]);
   Frame(pts);
   Poly( [pts, rodfaces(48)] );
}
//cylinderPts_demo_fn();



module cylinderPts_demo_center()
{
   echo(_b("cylinderPts_demo_center()"));  
   
   pts = cylinderPts(opt=["$fn", 8]);
   Gold(.5) Poly( [pts, rodfaces(8)] );

   pts2 = cylinderPts(center=true, opt=["$fn", 8]);
   Red() Frame(pts2); 

   pts3 = cylinderPts(center=false, opt=["$fn", 8]);
   Green() Frame(pts3); 

}
//cylinderPts_demo_center();



module cylinderPts_demo_center_partial()
{
   echo(_b("cylinderPts_demo_center_partial()"));  
   
   pts = cylinderPts(center=[1,0,0], opt=["$fn", 8]);
   MarkPts(pts);
   Poly( [pts, rodfaces(8)] );
}
//cylinderPts_demo_center_partial();



module cylinderPts_demo_r()
{
   echo(_b("cylinderPts_demo_r()"));  

   pts = cylinderPts(r2=0.6, opt=["$fn", 8]);
   color("red") Frame(pts);
   Red(.2) Poly( [pts, rodfaces(8)] );
   
   pts2 = cylinderPts(h=2, d1=0.7, d2=1, center=true, opt=["$fn", 8]
                     , actions=["y",2] );
   Blue() Frame(pts2);
   Gold(.2) Poly( [pts2, rodfaces(8)] );
 
   pts3 = cylinderPts(r2=0, actions=["x", 2.5], debug=0);
   //echo(pts3=pts3);
   //echo(fs= faces("cone",4, nseg=1));
   //MarkPts(pts3);
   Black() Frame(pts3, shape="cone");
   Green() Poly( [pts3, faces("cone",12, nseg=1)] );

   pts4 = cylinderPts(h=2,r1=0,center=true, actions=["x", 5], debug=0);
   //echo(pts4=pts4);
   //echo(fs=faces("conedown",4, nseg=1));
   //MarkPts(pts4);
   //color("black") Frame(pts4, shape="conedown");
   Blue() Frame(pts4, shape="conedown");
   Teal() Poly( [pts4, faces("conedown",12, nseg=1)] );
   
}
//cylinderPts_demo_r();


module cylinderPts_demo_actions()
{
   echo(_b("cylinderPts_demo_actions()"));  
      
   pts = cylinderPts(opt=["$fn", 8, "actions", ["rotx", -90, "y",2]]);
   Frame(pts);
  // Transforms( ["rotx", -90,"y",2])
  // color(undef, 0.8) cylinder( $fn=8);   
  // MarkPts(pts, "l"); 

   pts2 = cylinderPts(h=2,r1=0,  opt=["$fn", 8, "actions", ["z", -2]]);
   color("red") Frame(pts2, shape="conedown");
  // Transforms( ["z", -2])
  // color("red", 0.8) cylinder( h=2,r1=0, $fn=8);   
  // MarkPts(pts2, "l"); 
   
   
}
//cylinderPts_demo_actions();

//====================================================
//. With site

module cylinderPts_demo_with_site()
{
   echo(_b("cylinderPts_demo_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");
   
   pts = cylinderPts(  opt=["site",pqr]);
   Frame(pts);
   //MarkPts(pts,"l"); 
   
}
//cylinderPts_demo_with_site();




module cylinderPts_demo_center_with_site()
{
   echo(_b("cylinderPts_demo_center_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");  
   
   pts = cylinderPts(center=true, opt=["site",pqr]);
  
   Frame(pts);
   //MarkPts(pts,"l"); 
  
   //color(undef, 0.5) cylinder(center=true, $fn=8); 
   
}
//cylinderPts_demo_center_with_site();



module cylinderPts_demo_actions_with_site()
{
   echo(_b("cylinderPts_demo_actions_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");
   
   pts = cylinderPts(opt=["actions", ["x",2],"site",pqr]);
   Frame(pts);
   color("red", 0.5) polyhedron( pts, faces = rodfaces(12));
   
   //MarkPts(pts,["label",["scale",0.01]]); 
   
   pts2 = cylinderPts(opt=["actions",["roty",-45],"site",pqr]);
   Frame(pts2);
   color("green", 0.5) polyhedron( pts2, faces = rodfaces(12));
   //MarkPts(pts2,["label",["scale",0.01]]);
   
   pts3 = cylinderPts(opt=["actions",["rotz",90, "rotx",-90, "z",-2],"site",pqr]);
   Frame(pts3);
   color("blue", 0.5) polyhedron( pts3, faces = rodfaces(12));
   //MarkPts(pts3,["label",["scale",0.01]]);
   
}
//cylinderPts_demo_actions_with_site();

