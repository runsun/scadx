include <../scadx.scad>


module demo_isConvex_old()
{
   echom("demo_isConvex()");
   pts = randPts(3);
   pt = randPt([1,2]);
   Rf = onlinePt( [ORIGIN, pt], ratio= rands(0.7,1.3,1)[0]);
   
   MarkPts( pts, Line=false, color="black" );
   for(p=pts) Line([p,pt]);
   MarkPt( pt, Label="Pt", color="black");
   isConvex = isConvex( concat( [pt], pts ), pi=0, Rf = Rf );
                       
   MarkPt( Rf, Label="Rf", color= isConvex?"green":"red");
}
//demo_isConvex();


//dev_isConvex_no_Rf();
module dev_isConvex_no_Rf()
{
   echom("------------- dev_isConvex_no_Rf()--------------");
   pts = randPts(8);
   pts= [[-0.315754, 2.18321, 0.0165775], [-1.83316, -0.461837, -2.4851], [-0.250761, -0.707988, 1.40556], [-2.71441, 2.61027, 1.81194], [-1.35138, -2.97306, 2.55987], [-2.85625, 2.42117, -2.24881], [-1.8127, 2.11271, -0.169752], [-0.43634, 2.67442, -1.86039]];
      pts=[[0.954102, -2.52069, 2.86141], [-1.58446, 0.731409, 0.831557]
      , [1.14213, -0.814758, 0.481223], [0.326093, 2.92951, -2.28178]
      , [-0.737669, 1.28368, 1.14409], [0.0167644, 1.17775, 1.47647]
      , [1.06618, 2.12602, -0.0956963], [1.09691, 2.20065, -2.15693]
      , [-2.28315, 0.470324, 2.14885], [-0.370817, 1.36549, 0.924967]];
   
   echo(pts = pts );
   pi = 0;
   C = sum(pts)/len(pts);
   
   //Line( pts );
   for(pi=range(pts)) 
   {

     //pt = pts[pi];
     //pts_wo_pi= [for(i=range(pts)) if(i!=pi) pts[i]];
     echo(_blue(str("..................... checking pi = ",pi)));
     for( pj = range(pts) )
     {
        if(pj!=pi)
          echo( pi=pi, pj=pj
              , _s("angle [Rf, i{_},i{_}]", [pi,pj] )
              , _angle_ = angle( [Rf, pts[pi], pts[pj]])
              );
     }
     Rf = onlinePt( [C, pts[pi]], ratio= 2); //rands(0.7,1.3,1)[0]);
     MarkPt(Rf, Label="Rf");
   
     isConvex = isConvex( pts=pts , pi=pi, Rf=Rf);
     color( isConvex?"green":"red")
     MarkPt( pts[pi], Label=pi );
     echo( _color( _s("is pts[{_}] convex? {_}", [pi ,isConvex]), isConvex?"green":"red") );                    
     Line([C,pts[pi]], r=0.01, color=isConvex?"green":"red");   
   
     ///
     /// Debugging: 
     ///
//     if(pi==5 || pi==6 || pi==9)
//     {
//        pts_wo_pi = [ for(i=[0:len(pts)-1])
//                      if(i!=pi) pts[i]
//                    ] ;  
//        ais = [ for(i=range(pts_wo_pi))
//                [angle( [Rf,pts[pi], pts_wo_pi[i] ]), i ]
//              ];
//        min_ai = quicksort( ais )[0][1];  // i of pt that has smallest angle 
//      
//        normalPt = N( [Rf, pts[pi], pts_wo_pi[ min_ai ] ]);
//     
//        plane = [ pts_wo_pi[ min_ai] 
//                , pts[ pi ] 
//                , normalPt
//                ];
//     
//        Plane(plane);
//     }
     
     if(pi==0)
     { N5 = N([Rf, pts[5], pts[8]]);
      // color("gold", 0.5)
      //Plane( expandPts( [N5,pts[5], pts[8]], 4 ), mode=1);
     } 
   }
}


module demo_isConvex()
{
   echom("------------- demo_isConvex()--------------");
   pts = randPts(20);
   //pts= [[-0.315754, 2.18321, 0.0165775], [-1.83316, -0.461837, -2.4851], [-0.250761, -0.707988, 1.40556], [-2.71441, 2.61027, 1.81194], [-1.35138, -2.97306, 2.55987], [-2.85625, 2.42117, -2.24881], [-1.8127, 2.11271, -0.169752], [-0.43634, 2.67442, -1.86039]];
   echo(pts = pts );
   pi = 0;
   C = sum(pts)/len(pts);
   
   //Line( pts );
   for(pi=range(pts)) 
   {

     //pt = pts[pi];
     //pts_wo_pi= [for(i=range(pts)) if(i!=pi) pts[i]];
     echo(_blue(str("..................... checking pi = ",pi)));
//     for( pj = range(pts) )
//     {
//        if(pj!=pi)
//          echo( pi=pi, pj=pj, _angle_ = angle( [Rf, pts[pi], pts[pj]]));
//     }
     //Rf = onlinePt( [C, pts[pi]], ratio= 2); //rands(0.7,1.3,1)[0]);
     //MarkPt(Rf, Label="Rf");
   
     isConvex = isConvex( pts=pts , pi=pi); //, Rf=Rf);
     color( isConvex?"green":"red")
     MarkPt( pts[pi], Label=pi );
     echo( _color( _s("is pts[{_}] convex? {_}", [pi ,isConvex]), isConvex?"green":"red") );                    
     Line([C,pts[pi]], r=0.01, color=isConvex?"green":"red");   
   
     ///
     /// Debugging: 
     ///
//     if(pi==2 || pi==6)
//     {
//        pts_wo_pi = [ for(i=[0:len(pts)-1])
//                      if(i!=pi) pts[i]
//                    ] ;  
//        ais = [ for(i=range(pts_wo_pi))
//                [angle( [Rf,pts[pi], pts_wo_pi[i] ]), i ]
//              ];
//        min_ai = quicksort( ais )[0][1];  // i of pt that has smallest angle 
//      
//        normalPt = N( [Rf, pts[pi], pts_wo_pi[ min_ai ] ]);
//     
//        plane = [ pts_wo_pi[ min_ai] 
//                , pts[ pi ] 
//                , normalPt
//                ];
//     
//        Plane(plane);
//     }
   }
}
//demo_isConvex();

module demo_isConvex_no_Rf()
{
   echom("------------- demo_isConvex_no_Rf()--------------");
   pts = randPts(20);
   //pts= [[-0.315754, 2.18321, 0.0165775], [-1.83316, -0.461837, -2.4851], [-0.250761, -0.707988, 1.40556], [-2.71441, 2.61027, 1.81194], [-1.35138, -2.97306, 2.55987], [-2.85625, 2.42117, -2.24881], [-1.8127, 2.11271, -0.169752], [-0.43634, 2.67442, -1.86039]];
   echo(pts = pts );
   pi = 0;
   C = sum(pts)/len(pts);
   
   for(pi=range(pts)) 
   {
     echo(_blue(str("..................... checking pi = ",pi)));
     isConvex = isConvex( pts=pts , pi=pi); 
     color( isConvex?"green":"red")
     MarkPt( pts[pi], Label=pi );
     echo( _color( _s("is pts[{_}] convex? {_}"
         , [pi ,isConvex]), isConvex?"green":"red") );                    
     Line([C,pts[pi]], r=0.01, color=isConvex?"green":"red");   
   }
}
//demo_isConvex_no_Rf();

module demo_isConvex_no_Rf_2()
{
   echom("------------- demo_isConvex_no_Rf_2()--------------");
   pts = randPts(10);
   //pts= [[-0.315754, 2.18321, 0.0165775], [-1.83316, -0.461837, -2.4851], [-0.250761, -0.707988, 1.40556], [-2.71441, 2.61027, 1.81194], [-1.35138, -2.97306, 2.55987], [-2.85625, 2.42117, -2.24881], [-1.8127, 2.11271, -0.169752], [-0.43634, 2.67442, -1.86039]];
   
   
   /// Error pts:
   pts=[[0.954102, -2.52069, 2.86141], [-1.58446, 0.731409, 0.831557]
      , [1.14213, -0.814758, 0.481223], [0.326093, 2.92951, -2.28178]
      , [-0.737669, 1.28368, 1.14409], [0.0167644, 1.17775, 1.47647]
      , [1.06618, 2.12602, -0.0956963], [1.09691, 2.20065, -2.15693]
      , [-2.28315, 0.470324, 2.14885], [-0.370817, 1.36549, 0.924967]];
   
   
   echo(pts = pts );
   pi = 0;
   C = sum(pts)/len(pts);
   
   //for(pi=range(pts)) 
   //{
     //echo(_blue(str("..................... checking pi = ",pi)));
     //isConvex = isConvex( pts=pts , pi=pi); 
     //color( isConvex?"green":"red")
     //MarkPt( pts[pi], Label=pi );
     //echo( _color( _s("is pts[{_}] convex? {_}"
         //, [pi ,isConvex]), isConvex?"green":"red") );                    
     //Line([C,pts[pi]], r=0.01, color=isConvex?"green":"red");   
   //}
   
   convs= [ for(pi=range(pts)) isConvex( pts=pts, pi=pi)?1:0 ];
   outpts = [ for(pi=range(pts)) if(convs[pi]==1)pts[pi]];
   echo(outpts = outpts);
   for(pi=range(pts))
   {
      echo(pi=pi, isconvex = convs[pi]);
      color( convs[pi]?"green":"red") 
      { MarkPt( pts[pi], Label=pi);
        Line( [C, pts[pi]], r=0.01);
      }
      
      if(convs[pi])
      {
        outpts_wo_pi = [for(pj=range(pts)) if(pj!=pi) pts[pj] ];
        sortpts = sortByDist(outpts, pts[pi]);
        //echo(sortpts=sortpts);
        //closestpi = closestPi( pts_wo_pi, pts[pi] );
        //Line( [pts[pi],pts[closestpi]], color="blue" );
        pl = [pts[pi],  sortpts[2], sortpts[1]]; 
        //echo(pl=pl);
        //echo("Drawing a plane");
        //Plane( pl, mode=1);
      } 
   }
   echo(convs = convs);
   cxpts = [ for(pi=range(pts)) if(convs[pi]) pts[pi] ];
   
}
//demo_isConvex_no_Rf_2();


module demo_isInConvexPlane()
{
  echom("demo_isInConvexPlane()");
  a = 3;
  pts = [ [a/2,0,0],[-a/2,-a/2,0],[-a/2,a/2,0],[a/2,a/2,0] ];
  n=100;
 
  for(i=range(n))
  {
     pt = randPt([-2,2],z=0) ;
     isIn = isInConvexPlane( pts, pt );
     //echo(_color( str(i, ":", pt), isIn?"green":"red"));
     MarkPt( pt, Label=false, color= [isIn?"green":"red",0.3]);
  }
  
  Line(pts, closed=true, color="black");
  
}
//demo_isInConvexPlane();

//demo_isOnPQR();
module demo_isOnPQR()
{
   echom("demo_isOnPQR()");
   pqr= pqr();
   MarkPts(pqr, "PQR", Line=["closed",1] );
   
   pts= randPts(20);
   for( p = pts )
   {
      J = projPt(pqr,p);
      isOnPQR = isOnPQR(pqr, J);
      color(isOnPQR?"green":"red")
        MarkPt(J, Label=false);
   }
}

//demo_isInPQR();
module demo_isInPQR()
{
   echom("demo_isInPQR()");
   pqr= pqr();
   MarkPts(pqr, "PQR", Line=["closed",1] );
   
   pts= randPts(40);
   for( p = pts )
   {
      J = projPt(pqr,p);
      isOnPQR = isInPQR(pqr, J);
      color(isOnPQR?"green":"red")
        MarkPt(J, Label=false);
   }
}
