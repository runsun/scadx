include <../scadx.scad>


module ArcArrow_demo(){
    echom("ArcArrow_demo");
    
    pqr = pqr();
    pqr1 = trslN(pqr, -9);
    MarkPts( p01(pqr1),"r=0.05;l=PQ");
    //MarkPts( pqr1, "ch;l=PQR;ball=0" );
    ArcArrow( pqr1 );
    
    pqr2= trslN(pqr,-6);
    MarkPts( p01(pqr2),"r=0.05;l=PQ");
    //MarkPts( pqr2, "ch;l=PQR;cl=red;ball=0" );
    ArcArrow( pqr2, rad=4, opt=["color","red"]);
    
    pqr3= trslN(pqr,-3);
    MarkPts( p01(pqr3),"r=0.05;l=PQ");
    //MarkPts( pqr3, "ch;l=PQR;cl=green;ball=0" );
    ArcArrow( pqr3, rad=8, opt=["color","green"]);
    
    pqr4= trslN(pqr,0);
    MarkPts( p01(pqr4),"r=0.05;l=PQ");
    //MarkPts( pqr4, "ch;l=PQR;cl=blue;ball=0" );
    ArcArrow( pqr4, rad= dist(pqr4)/2+0.2, opt=["color","blue"]);
    
    pqr5= trslN(pqr,3);
    MarkPts( p01(pqr5),"r=0.05;l=PQ");
    //MarkPts( pqr5, "ch;l=PQR;cl=purple;ball=0" );
    ArcArrow( pqr5, rad= dist(pqr5), opt="ch;cl=purple");
    //["chain",true,"color","purple"]);
    
    pqr6= trslN(pqr,6);
    MarkPts( p01(pqr6),"r=0.05;l=PQ");
    ArcArrow( pqr6, opt="ch;cl=darkcyan;chainext=1");
    
}    
ArcArrow_demo();