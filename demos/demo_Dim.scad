include <../scadx.scad>

//demo_Dim_summary();
//demo_Dim_1();
//demo_Dim_Link_i6r();
//demo_Dim_Link_using_steps_i6m();
//demo_Dim_followSlope();
//demo_Dim_pts_spacer();
//_Dim_h_spacer_Guides_template();
//demo_Dim_spacer_default_h();
//demo_Dim_spacer0_h();
//demo_Dim_spacer1_h();
//demo_Dim_spacer_n02_h();
//demo_Dim_spacer_n04_h();
//demo_Dim_spacer_n1_h();
//demo_Dim_gap();
//demo_Dim_arrows_Guides();
//demo_Dim_Label();
//demo_Dim_Label_actions();
//demo_Dim_chain();
//demo_Dim_chain2();
//demo_Dim_chain_chainRatio();
//demo_Dim_chain_chainRatio2();
//demo_Dims();
demo_Dims_individual_opt();


//. Dim (2018.5.18)

module demo_Dim_summary()
{
  echom("demo_Dim_summary()");
  pts = cubePts([2,3,1]);
  faces = rodfaces(4);
  P=pts[5]; Q=pts[6]; R=pts[1];
  
  //--------------------------------------------
  Dim( [P,Q,R], spacer=0.5 );
  MarkPt( P+[0,-0.2,.7], Ball=0
        , Label=["text","1.Dim( [P,Q,R] ) ","color","blue"
                ,"halign", "right"] );
  //--------------------------------------------
  Dim( [R,P,Q], Link=true, spacer=0.4 );
  MarkPt( (R+P)/2+ [0,-1.3,0], Ball=0
        , Label=["text","2.Dim( [R,P,Q], Link=true, spacer=0.4 ) ","color","blue"
                , "halign", "right"] );              
  //--------------------------------------------
  Dim( [0,3,7], pts=pts);
  MarkPt( pts[0]+[0,-0.2,-.3], Ball=0
        , Label=["text","3.Dim( [0,3,7], pts=pts) ","color","blue"
                ,"halign", "right"] );              
  //--------------------------------------------
  Dim( [0,7,3], pts=pts);
  MarkPt( pts[7]+[0, 0.2,0], Ball=0
        , Label=["text","4.Dim( [0,7,3], pts=pts) ","color","blue"
                ] );              
  //--------------------------------------------
  Dim( [P+[0,1.15,.8], P+[0,1.85,.8], P+[0,1.2,-1] ], stem=0 
     , Label=["text","5.gap", "color","gold"], color=["gray",.5]);
  

  //--------------------------------------------
  Dim( [ Q+[0,0,.7], Q+[0,0,.5], P]
     , color=["gray",0.5]
     , Label=["text", "6.h (ratio of height, default 0.5)"
             , "color","gold", "actions",["x",0.1,"rotz",90]
             , "halign", "left"
             ] , spacer=0.5, Link=["Arrow",true, "spacer",0, "stem",0.4]
     , stem=true     
     );
  
  //--------------------------------------------
  Dim( [ Q+[0,0,.5], Q, P]
     , color=["gray",0.5]
     , Label=["text", "7.spacer=0.5 (default 0.1)"
             , "actions", ["x",0.1, "rotz", 90]
             , "halign", "left"
             , "color","gold"
             ] 
     , Guides=["len",0.3]        
     , stem=true        
     );
  //--------------------------------------------
  //MarkPt( P+[0,0,1], "Guides", Link=[ P+[0,1.5,2], P+[0,3.5,2]], Ball=0 );
  //MarkPt( Q+[0,0,1], "", Link=[ P+[0,1.5,2], P+[0,3.5,2]], Ball=0 );
  
//  Dim( [P,Q,R], Link=true, gap=0, h=1, spacer=1.5
//      , Label=["text", "Guides", "halign","left"]
//      , Link= [ P+[0,1.5,2], P+[0,3.5,2]]
//      );
//  
//  MarkPt( P+[0,0,1.2], "GuideP", Link=[ P+[0,0,2.5], P+[0,1,2.5]], Ball=0 );
//  MarkPt( Q+[0,0,.8], "GuideQ", Link= Q+[0,1,1.5], Ball=0 );
//  
//  //--------------------------------------------
//  MarkPt( P+[0,0.8,.6], Link=[ P+[0,.8,.25], P+[0,-1,.25]], Ball=0
//        , Label=["text","Arrows", "halign","right" ] 
//        );
//  MarkPt( P+[0,2.2,.6], "", Link=[ P+[0,2.2,.25], P+[0,-1,.25]], Ball=0
//        );
        
  //==================================================
  MarkPts(get(pts,[0,4,7,3]),[0,4,7,3], Line=0,Ball=0);
  Blue() MarkPts( get(pts,[5,6,1])
                , Label=["text","PQR","halign","right", "indent",-0.1]
                , Line=0, Ball=0);
  DrawEdges(pts=pts,faces=faces);
  Teal(0.5) polyhedron(pts, faces);
 
  
}


module demo_Dim_1()
{ 
    echom("demo_Dim_1()");
    pqr = [ [-1.3,-1.89,2.46], [0,2.69,0.22], [1.12,0.98,-0.28]];
    
    MarkPts( pqr, Label="PQR");
    Dim(pqr);
    Dim(roll(pqr,-1));
    Dim(roll(pqr,-2));
}


module demo_Dim_Link_i6r()
{ 
  echom("demo_Dim_Link_i6r()");
  pts = [ [-1.3,-1.89,2.46], [0,2.69,0.22], [1.12,0.98,-0.28]
        ,  [-0.18, -3.6, 1.96]
        ];
  pts = cubePts([2,3,1]);
  faces = rodfaces(4);
  P=pts[0]; Q=pts[1]; R=pts[2]; S=pts[3];
  T=pts[4]; U=pts[5]; V=pts[6]; W=pts[7];
  
  MarkPts( pts, Label="PQRSTUVW", Line=0);
  
  Gold()  Dim( [Q,U,V]);
  
  
  Gold()   Dim( [U,.8*U+.2*V,Q], Label="1.Link=true"
             , Link=true);
  Gold()   Dim( [U,.8*U+.2*V,Q], Label="2.Link=0.1 (stem len)"
             , Link=0.1, spacer=.9);
  Gold(.6) Dim( [U,.8*U+.2*V,Q], Label=["text","3.Link=[\"stem\",1.5]", "halign","right"]
             , Link=["stem",1.5], spacer=1.8 );            
             

  Red(.5) Dim( [.6*U+.4*V, .4*U+.6*V, Q]
               , Label=["text", "4.Link=0"]
               , Link=0 );
  Red(.3) Dim( [.6*U+.4*V, .4*U+.6*V, Q]
               , Label=["text", "5.spacer=0.8"]
               ,Link=.5, spacer=.8 );
  Red(.5) Dim( [.6*U+.4*V, .4*U+.6*V, Q]
               , Label=["text", "6.Link=[\"spacer\",0.5]"]
               , Link=["spacer",0.5], spacer=1.9 );   
              
  Green(.5) Dim( [.3*U+.7*V,V, Q]
               , Label=["text", "7.Link=[\"Arrow\",true]", "halign","left"]
               ,Link=["Arrow",true], spacer=.3 );
  Green(.3) Dim( [.3*U+.7*V,V, Q]
               , Label=[ "text", "8.Link=[\"Arrow\",true], Arrows=[\"Head\",true]"
                        , "halign","left" ] 
               , Link=["Arrow",true]         
               , Arrows=["Head",true],spacer=1.3 );
               
  Blue()  Dim( [V,.6*V+.4*W,T], Label="Link=[0.5,4,2]", Link=[.5,4,2]);
  Blue(.5)  Dim( [.4*V+.6*W,.1*V+.9*W,T], Label="Link=[ [1.5,4,2], [1.5,5.5,2] ]"
              , Link=[[1.5,4,2], [1.5,5.5,2]]);
  Blue(.2)  Dim( [.6*V+.4*W,W,T]
              , Label="Link=[\"pts\",[[2,4,1.5], [2,5,1.5]], \"stem\",0]"
              , Link=["pts",[[2,4.5,1.5], [2,5,1.5]], "stem",0], spacer=0.8);
  
  Purple()  Dim( [P,T,W]
              , Link=["steps", ["t",[0,-.5,-.5], "z",-.8, "y",1.5,"z",-0.5]]
              , Label=["text","Link=[\"steps\", [\"t\",[0,-.5,-.5], \"z\",-.8, \"y\",1.5,\"z\",-0.5]]"
                      ,"actions",["rotz",-90]]
              );
  Purple(.5)  Dim( [P, .7*P+.3*S,W]
              , Link=["steps", ["t",[0,.3,-.3], "y",1], "Arrow",true, "stem",0]
              , Label=["text","Link=[\"steps\",[\"t\",[0,.3,-.3], \"y\",1]], \"Arrow\",true, \"stem\",0]"
                      , "actions",["rotx",180]
                      , "halign","left"]
              );  
  
  //==================================================
  DrawEdges(pts=pts,faces=faces);
  Teal(0.5) polyhedron(pts, faces);
}


module demo_Dim_Link_using_steps_i6m()
{ 
  echom("demo_Dim_Link_using_steps_i6m()");
  pts = [ [-1.3,-1.89,2.46], [0,2.69,0.22], [1.12,0.98,-0.28]
        ,  [-0.18, -3.6, 1.96]
        ];
  pts = cubePts([2,3,1]);
  faces = rodfaces(4);
  P=pts[0]; Q=pts[1]; R=pts[2]; S=pts[3];
  T=pts[4]; U=pts[5]; V=pts[6]; W=pts[7];
  
  MarkPts( pts, Label="PQRSTUVW", Line=0);
  Gold()  Dim( [Q,U,V]);
  Red()   Dim( [U,.8*U+.2*V,Q]
             , Link=["steps",["t",[0,-1,0.2], "z",0.5], "stem",0]
             //, Label=["valign","bottom"]
             );
  Red(.5) Dim( [.7*U+.3*V,.4*U+.6*V,Q]
             , Link=["steps",["t",[0,-1,0.2], "z",0.5]]
             //, Label=["valign","bottom"]
             );
  Red(.2) Dim( [.3*U+.7*V,V,Q]
             , Link=["steps",["t",[0,-1,0.2], "z",0.5], "stem",0.5, "Arrow",true]
             //, Label=["valign","bottom"]
             );
             
  Green()  Dim( [V,W,P], Label="V W P"
             , Link= [ "stem",0,"Arrow",true
                     , "steps",["t",[0.2,1,0]]
                     ]
             );
  Green(.4)  Dim( [V,.6*V+.4*W,P], Link=[0,5,2], spacer=0.6);
  Green(.2)  Dim( [.4*V+.6*W,W,P]
               , Link=["pts",[1,5,2],"stem",0, "Arrow",true], spacer=0.6);
  Blue() Dim( [P,T,S], Link=[ [2,-1,0.5], [2,-1,-0.5],[2,2,-0.5]]
              , Label=["actions",["rotz",-90], "halign","left"]
              ); 
  Blue(.4) Dim( [W,S,P], Link= [ "pts", [ [2,4,.5],[2,4,1.2], [2,5,1.2]]
                      , "stem",0, "Arrow",true]
              , Label=["actions",["rotz",90], "halign","left"]
              , spacer=.5
              );            
  
  //==================================================
  DrawEdges(pts=pts,faces=faces);
  Teal(0.5) polyhedron(pts, faces);
}


module demo_Dim_followSlope()
{  echom("demo_Dim_followSlope");

   pts= [ [0,0,2], [3,0,1], [3,0,0], [0,0,0], [0,3,2], [3,3,1] 
        , [3,3,0], [0,3,0], [0,2,2], [3,2,1], [3,2,0]];

   P=pts[0]; Q=pts[1]; R=pts[2]; S=pts[3]; T=pts[4]; U=pts[5]; 
   V=pts[6]; W=pts[7]; D=pts[8]; E=pts[9]; F=pts[10]; 

   color("black") 
   MarkPts( pts, Label="PQRSTUVWDEF", Ball=["r",0.05], Line=false);
   
    Dim( [P,Q,R], followSlope=false, color="red" );
    Line( [Q, S+Q-R], color="red");
    
    Line([P,Q,R], color="blue");
    Dim( [P,Q,R], opt=["followSlope", true] );
       
    //Line([E,D,F], color="green");
    //Dim( [E,D,F], opt=["followSlope", true, "color", "green"] );
    
    Line([T,U,W], color="purple");
    Dim( [T,U,W], opt=["followSlope", false, "color","purple" ], Guides=["len",1], stem=true );

    Line( [P, S+Q-R], color="black");
    Dim( [S+Q-R, P, P-S+R], color="black");
    
    DrawEdges([pts, CUBEFACES]);
    
    color(undef,0.2) polyhedron( points =pts, faces=CUBEFACES );
   
}    

module demo_Dim_pts_spacer()
{
   echom(_b("demo_Dim_pts_spacer()"));  
   echo(_red("If pts is given, pqr could contain indices to pts. Also demo spacers"));
   cubepts = cubePts([2,3,1]);
   
   color("black")
   //MarkPts( cubepts, Line=false, Ball=["r",0.05] );
   MarkPts( cubepts, Line=false, Ball=0.05 );
   Cube( [2,3,1], color=["teal",0.7] );
   
   Dim( [5,6,2], pts=cubepts );
   Dim( [5,(cubepts[5]+cubepts[6])/2,1], pts=cubepts, spacer=1.2, color="red" ); 
   Dim( [centerPt( get(cubepts,[5,6,7,4])),7,3], pts=cubepts, spacer=0, color="green" ); 
   Dim( [4,7,3], pts=cubepts, spacer=-0.5, color="purple" );
    
}

//. Dim spacer/h w or w/o Guides

module _Dim_h_spacer_Guides_template(spacer)
{
   //echom(_b("demo_Dim_h_spacer0_h()"));  
   echo(_red(str("Chech h and Guides at spacer= ",spacer)));

   spacer = spacer=="default"? 0.1: spacer;
   
   pts = concat( reverse( [for(i=[0:3]) [i*1.5,0,0] ] )
               , [ each for(i=[0:3]) [ for(j=[0:3]) [ i*1.5, j*1.5, 1 ] ]]
               );  
   color("black") 
   MarkPts( pts, Line=false, Label=["text", range(pts), "scale",0.5], Ball=0.05 );
   //echo( pts = pts );              
   
   color("red") Dim( [4,5,3], pts=pts, spacer=spacer, h=0 );
   color("red") Dim( [5,6,3], pts=pts, spacer=spacer+.5, h=0.5);
   color("red") Dim( [6,7,3], pts=pts, spacer=spacer+1, h=1 ); 
      
   color("blue") Dim( [8,9,2], pts=pts, spacer=spacer, h=0,      GuideP=false );
   color("gold") Dim( [9,10,2], pts=pts, spacer=spacer+.5, h=0.5,   GuideP=false);
   color("blue") Dim( [10,11,2], pts=pts, spacer=spacer+1, h=1,    GuideP=false );
   
   color("green")Dim( [12,13,1], pts=pts, spacer=spacer, h=0,    GuideQ=false );
   color("gold")  Dim( [13,14,1], pts=pts, spacer=spacer+.5, h=0.5, GuideQ=false);
   color("green")Dim( [14,15,1], pts=pts, spacer=spacer+1, h=1,    GuideQ=false );
   
   color("purple")Dim( [16,17,0], pts=pts, spacer=spacer, h=0,   Guides=false );
   color("gold")  Dim( [17,18,0], pts=pts, spacer=spacer+.5, h=0.5, Guides=false);
   color("purple")Dim( [18,19,0], pts=pts, spacer=spacer+1, h=1,   Guides=false );
      
   Cube( [ pts[0].x,last(pts).y,1], color=["teal",0.4] );
}

module demo_Dim_spacer_default_h()
{
   echom(_b("demo_Dim_spacer02_h()"));  
   _Dim_h_spacer_Guides_template(spacer= "default");  
}

module demo_Dim_spacer0_h()
{
   echom(_b("demo_Dim_spacer0_h()"));
   _Dim_h_spacer_Guides_template(spacer= 0);  
}

module demo_Dim_spacer1_h()
{
   echom(_b("demo_Dim_spacer1_h()"));  
   _Dim_h_spacer_Guides_template(spacer= 1);  
}


module demo_Dim_spacer_n02_h()
{
   echom(_b("demo_Dim_spacer_n02_h()")); 
   _Dim_h_spacer_Guides_template(spacer= -0.2);  
}

module demo_Dim_spacer_n04_h()
{
   echom(_b("demo_Dim_spacer_n04_h()")); 
   _Dim_h_spacer_Guides_template(spacer= -0.4);  
}

module demo_Dim_spacer_n1_h()
{
   echom(_b("demo_Dim_spacer_n1_h()")); 
   _Dim_h_spacer_Guides_template(spacer= -1);  
}


//. Dim the rest

module demo_Dim_gap()
{
   echom(_b("demo_Dim_gap()"));  
   echo(_red("Set gap."));
   cubepts = cubePts([2,3,1]);
   
   color("black")
   //MarkPts( cubepts, Line=false, Ball=["r",0.05] );
   MarkPts( cubepts, Line=false, Ball=0.05 );
   Cube( [2,3,1], color=["teal",0.7] );
   
   S = onlinePt(get(cubepts,[5,6]),ratio=1/3);
   T = onlinePt(get(cubepts,[5,6]),ratio=2/3);
   MarkPts([S,T], "ST");
   
   Dim( [5,S,2], pts=cubepts, gap=0, spacer=0, color="red" );
   Dim( [S,T,2], pts=cubepts, gap=0.3, spacer=0, color="green" );
   Dim( [T,6,2], pts=cubepts, gap=1, spacer=0 );
   Dim( [5,S,2], pts=cubepts, gap=1.5, spacer=0.5, color="purple" );   
   Dim( [T,6,2], pts=cubepts, gap=3, spacer=1, color="teal" );
   
}

module demo_Dim_arrows_Guides()
{
   echom(_b("demo_Dim_arrows_Guides()"));  
   echo(_red("Set arrows and Guides len."));
   cubepts = cubePts([2,3,1]);
   
   color("black")
   //MarkPts( cubepts, Line=false, Ball=["r",0.05] );
   MarkPts( cubepts, Line=false, Ball=0.05 );
   Cube( [2,3,1], color=["teal",0.7] );
   
   S = onlinePt(get(cubepts,[5,6]),ratio=1/3);
   T = onlinePt(get(cubepts,[5,6]),ratio=2/3);
   
   
   C = centerPt( get( cubepts,[4,5,6,7] ));
   Red() MarkPt( x(C, -.3)
               , Label=["text","Arrows=false or =['Head',false]"
                       ,"halign","center", "valign", "bottom"], Ball=false);
   
   Dim( [5,S,2], pts=cubepts, Arrows=false, spacer=0, color="red" );
   Dim( [T,6,2], pts=cubepts, Arrows=["Head",false], Guides=["len",0.2], spacer=0.1, color="green" );
   Dim( [S,T,2], pts=cubepts, Arrows=["Head",false], Guides=["len",0.2], h=1, spacer=0.5 );
   Dim( [5,6,2], pts=cubepts, Arrows=["Head",false], Guides=["len",0.6], h=1, spacer=0.4, color="purple" );

   Dim( [0,3,7], pts=cubepts, Arrows=true );
   

}

module demo_Dim_Label()
{
   echom(_b("demo_Dim_Label()"));  
   echo(_red("Set Label."));
   cubepts = cubePts([2,3,1]);
   
   color("black")
   //MarkPts( cubepts, Line=false, Ball=["r",0.05] );
   MarkPts( cubepts, Line=false, Ball=0.05 );
   Cube( [2,3,1], color=["teal",0.7] );
   
   S = onlinePt(get(cubepts,[5,6]),ratio=1/3);
   T = onlinePt(get(cubepts,[5,6]),ratio=2/3);   
   Dim( [5,S,2], pts=cubepts, spacer=0, color="red" );
   Dim( [S,T,2], pts=cubepts, Label=["text","str"], spacer=0.3, color="green" );
   Dim( [T,6,2], pts=cubepts, Label=["text","String"], spacer=0, gap= 1.4, color="blue" );
   Dim( [5,6,2], pts=cubepts, Label="No Guides", Guides=false, spacer=0.5, h=1, color="red" );
}


module demo_Dim_Label_actions()
{
   echom(_b("demo_Dim_Label_actions()"));  
   echo(_red("Set Label."));
   cubepts = cubePts([2,3,1]);
   
   color("black")
   //MarkPts( cubepts, Line=false, Ball=["r",0.05] );
   MarkPts( cubepts, Line=false, Ball=0.05 );
   Cube( [2,3,1], color=["teal",0.7] );
   
   S = onlinePt(get(cubepts,[5,6]),ratio=1/3);
   T = onlinePt(get(cubepts,[5,6]),ratio=2/3);   
   U = onlinePt(get(cubepts,[5,6]),ratio=7/8);   
    
   Dim( [5,S,2], pts=cubepts, spacer=0, color="red" );
   Dim( [S,T,2], pts=cubepts, Label=["actions",["y",0.2]], gap=0, spacer=0.3, color="green" );
   Dim( [U,6,2], pts=cubepts, Label=["actions",["rotz",90,"y", 0.35]], spacer=0, gap=0, color="blue" );
   Dim( [5,U,2], pts=cubepts, Label=["actions",["y",-0.3],"scale",2], gap=0, Arrows=["Head",false], Guides=["len",1], spacer=0.5, h=1, color="red" );
   Dim( [U,6,2], pts=cubepts, Label=["actions",["x", 0.55]], spacer=1, gap=0, color="teal" );
}


module demo_Dim_Label_format()
{
   echom(_b("demo_Dim_Label_format()"));  
   echo(_red("Set Label."));
   cubepts = cubePts([2,3,1]);
   
   color("black")
   //MarkPts( cubepts, Line=false, Ball=["r",0.05] );
   MarkPts( cubepts, Line=false, Ball=0.05 );
   Cube( [2,3,1], color=["teal",0.7] );
   
   S = onlinePt(get(cubepts,[5,6]),ratio=1/3);
   T = onlinePt(get(cubepts,[5,6]),ratio=2/3);   
   U = onlinePt(get(cubepts,[5,6]),ratio=7/8);   
    
   /* --- to be done --- */
    
//   Dim( [5,S,2], pts=cubepts, spacer=0, color="red" );
//   Dim( [S,T,2], pts=cubepts, Label=["actions",["y",0.2]], gap=0, spacer=0.3, color="green" );
//   Dim( [U,6,2], pts=cubepts, Label=["actions",["rotz",90,"y", 0.35]], spacer=0, gap=0, color="blue" );
//   Dim( [5,U,2], pts=cubepts, Label=["actions",["y",-0.3],"scale",2], gap=0, Arrows=["Head",false], Guides=["len",1], spacer=0.5, h=1, color="red" );
}
// to do: demo_Dim_Label_format();


//. Dim chain

module demo_Dim_chain()
{
   echom(_b("demo_Dim_chain()"));  
   _red("Set dim for a chain of pts.");
   _red("If there's n pts, it uses the last pt to dim the 0~n-1 pts.");
   pq = [O, 5*Y]; 
   pts = concat( onlinePts(pq,ratios=[0,0.2,0.35,0.6,0.75,1])
               , [[0,5,-1]] ) ;
   MarkPts(pts);
   //echo(pts=pts);
   Red() Dim( site= pts);
   Dim( site= pts, color="green", spacer=0.5);
   Dim( site= pts, color="blue", spacer=1, Link=true, Label=["actions",["rotz",45],"halign","left"]);
}   

module demo_Dim_chain2()
{
   echom(_b("demo_Dim_chain2()"));  
   echo(_red("Set dim for a chain of pts."));
   pts = cubePts([2,3,1]);
   
   color("black")
   MarkPts( pts, Line=false, Ball=0.05 );
   Cube( [2,3,1], color=["teal",0.7] );
   
   Dim([5,6,7,4,0], pts=pts);
} 

module demo_Dim_chain_chainRatio()
{
   echom(_b("demo_Dim_chain_chainRatio()"));  
   _red("Set dim for a chain of pts and report the ratio of each dist.");
   pq = [ORIGIN, [0,5,0]];
   pts = concat( onlinePts(pq,ratios=[0,0.2,0.35,0.6,0.75,1])
               , [[0,5,-1]] ) ;
   MarkPts(pts);
   //echo(pts=pts);
   Red() Dim( site= pts);
   echo("==========");
   Dim( site= pts, color="green", spacer=0.5, Label=["chainRatio",true]);
   //Dim( site= pts, color="blue", spacer=1, Link=true, Label=["actions",["rotz",45],"halign","left"]);
}   

module demo_Dim_chain_chainRatio2()
{
   echom(_b("demo_Dim_chain_chainRatio2()"));  
   _red("Set dim for a chain of pts and report the ratio of each dist.");
   pq = [ORIGIN, [0,5,0]];
   pts = concat( onlinePts(pq,ratios=[0,0.2,0.4,0.6,0.8,1])
               , [[0,5,-1]] ) ;
   MarkPts(pts);
   //echo(pts=pts);
   Red() Dim( site= get(pts, [0, 1, 5,-1]), Label=["chainRatio",true]);
   echo("==========");
   Dim( site= get(pts, [0, 2, 5,-1]), color="green", spacer=0.5, Label=["chainRatio",true]);
   Dim( site= get(pts, [0, 1, 4,-1]), color="blue", spacer=1, Link=true
                  , Label=["actions",["rotz",45],"chainRatio",true, "halign","left"]);
}   


//. Dims 

module demo_Dims()
{
   echom(_b("demo_Dims()"));  
   _red("opts: array of [ site, opt ], each representing a single Dim");
   
   pts= [ [0,0,2], [3,0,1], [3,0,0], [0,0,0], [0,3,2], [3,3,1] 
        , [3,3,0], [0,3,0]];
        //, [0,2,2], [3,2,1], [3,2,0]];

   color("black") 
   MarkPts( pts, Ball=["r",0.05], Line=false);

   Dims( pts= pts
       , opts= [[0,1,2],[0,4,3],[4,5,6]]   
       , debug=0        
       );
       
    //DrawEdges([pts, CUBEFACES]);
    
    color(undef,0.8) polyhedron( points =pts, faces=CUBEFACES );
}

module demo_Dims_individual_opt()
{
   echom(_b("demo_Dims_individual_opt()"));  
   _red("Compare to Dim(): Dims() allows for individual opt.");
   
   pts= [ [0,0,2], [3,0,1], [3,0,0], [0,0,0], [0,3,2], [3,3,1] 
        , [3,3,0], [0,3,0]];
        //, [0,2,2], [3,2,1], [3,2,0]];

   color("black") 
   MarkPts( pts, Ball=["r",0.05], Line=false);

   Dims( pts= pts
       , opts= [
                 [0,1,2]
                 , [[0,4,7], ["spacer",0.5, "color","green"]]
                 , [[0,5,6],["followSlope",false]]
               ]   
       , debug=0        
       //, opt= ["color","red"]   
       );
       
    //DrawEdges([pts, CUBEFACES]);
    
    color(undef,0.8) polyhedron( points =pts, faces=CUBEFACES );
}
