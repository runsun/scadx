include <../scadx.scad>

module rodPts_demo_default()
{
  echom("rodPts_demo_default");
  pts0 = rodPts();
  pts = [ for(ps=pts0)each ps ];
  echo(pts=pts);
  MarkPts( pts, "l;ch");
}
rodPts_demo_default();