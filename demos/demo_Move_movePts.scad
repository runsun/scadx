include <../scadx.scad>

//demo_MoveTo();
//dev_movePtsToOrigin_1867();
//dev_MoveToOrigin_func();
dev_Move_via_mirrors_186a();

//move_dev2();
//move_dev5();
//demo_MoveTo();
module demo_MoveTo()
{
   echom("demo_MoveTo(site)");
   echo(_blue("Move obj from the ORIGIN_SITE to site"));
    
   site = pqr(p=6,r=4);
   MarkPts(site, "PQR");
   
   MoveTo(site)
   scale(0.08)
   text("demo_MoveTo"); 

}

module dev_movePtsToOrigin_1867( ) 
{
   // Move obj from ORIGIN_SITE to *to*
   echom("dev_movePtsToOrigin_1867()");
   
   
   echo(_red("=========================== setup pqr "));
   _site = pqr();//p=4,r=6);
   pqr=pqr();
   //pqr= [ [1.37,0.8,-3.3], [0.2,-1.34,-0.13], [-1.48,2.07,4.51]];
   //pqr = [_site[0], _site[1], B(_site) ];
   
   //pqr = [for(p= [X,O,Z])p+[2,3,4]];
   //pqr = [for(p= [-1*X,O,Z])p+[2,3,4]];
   //pqr = [for(p= [-1*X,O,-1*Z])p+[2,3,4]];
   //pqr = [for(p= [X,O,-1*Z])p+[2,3,4]];
   
   //pqr = [for(p= [Y,O,Z])p+[2,3,4]];
   //pqr = [for(p= [-1*Y,O,Z])p+[2,3,4]];
   //pqr = [for(p= [-1*Y,O,-1*Z])p+[2,3,4]];
   //pqr = [for(p= [Y,O,-1*Z])p+[2,3,4]];
   
   //pqr = [for(p= [X,O,Y])p+[2,3,4]];
   //pqr = [for(p= [-1*X,O,Y])p+[2,3,4]];
   //pqr = [for(p= [-1*X,O,-1*Y])p+[2,3,4]];
   //pqr = [for(p= [X,O,-1*Y])p+[2,3,4]];
   
   //===================================================
   
   P=pqr[0]; Q=pqr[1]; R=pqr[2]; 
   aPQR= angle(pqr);
   
   Mark90( [P,Q, B(pqr) ] );
   MarkPts(pqr, "PQR");
   Plane(pqr);
   
   echo(pqr=pqr);
   echo(aPQR=aPQR);
   
   echo(_red("=========================== move pqr to O => pqrO = [S,O,T] "));
   
   pqr_O = [ for(p=pqr) p-pqr[1] ];
   S= pqr_O[0]; T=pqr_O[2];
   echo( pqr_O = pqr_O );
   echo( S=S, T=T );
   
   Red() MarkPts( pqr_O, "SOT" );
     
   //===========================================
   // Rot site0[0] (about [ site0[1], site0[2] ] )
   //  to X
   
   echo(_red("=========================== Rotate stu to align on X = pqr_X = [U,O,V] "));

   axisS2X = [O,-1*N([S,O,X])];
   rotaS2X = angle([S,O,X]); 
   pqr_X =  is0(S.y) && is0(S.z)? pqr_O
            : [for(i=[0:len(pqr)-1])
                    rotPt( pqr_O[i], axisS2X, a=rotaS2X ) ];         
   U = pqr_X[0];
   V = pqr_X[2];

   echo( axisS2X = axisS2X );
   echo( rotaS2X = rotaS2X );
   echo( pqr_X = pqr_X );
   echo( V=V );
                        
   Green() MarkPts( [U,V], "UV", Line=false);
   Green(0.2) Plane(pqr_X);
   
   echo(_red("=========================== Rotate V to (+X +Y) plane => [U,O,W] "));
   
   //===========================================
   Jvx = projPt( pqr_X[2],[O,X]); 
   VonXY = anglePt( [X,O,Y], a=aPQR, len= dist( Q,R ) );
   W = VonXY; 
   rotaV2XY = angle( [V, Jvx, W ] );
   echo( Jvx = Jvx );
   echo( VonXY = VonXY );
   echo( W = W );
   echo( rotaV2XY = rotaV2XY );   
   
   MarkPt(Jvx, "Jvx");
   MarkPt(VonXY, "VonXY");
   
   a= ( pqr_X[2].z>=0 ? -1:1 )*rotaV2XY;
   
   pqrFinal= concat( [ pqr_X[0]
            , O ]
            
            , V.y>0 && V.z ==0? slice(pqr_X,2)
               : [for(i=[2:len(pqr)-1])
                    rotPt( pqr_X[i], [X,O], a=a )
              ]
            );
   
   echo( aFinal = angle(pqrFinal) );
   echo( pqrFinal = pqrFinal );
   
   //echo(_red("CHECK:"), F_octant, rotaF2XY, a, is0( siteFinal[2].z) );
                     
   Blue() MarkPts( pqrFinal, "PQR");
   Blue(0.2) Plane(pqrFinal);
   
   echo(_red("=========================== Checking octants of site:"));
   
   echo( is_aFinal_eq_aPQr = _color(isequal(angle(pqrFinal), aPQR)
                                   , isequal(angle(pqrFinal), aPQR)?"green":"red") );
                                   
   echo( is_pqrFinal_3rd_pt_y_positive = _color( pqrFinal[2].y>0
                                               , pqrFinal[2].y>0? "green":"red" ) );
                                               
   echo( is_pqrFinal_3rd_pt_z_0 = _color( is0( pqrFinal[2].z) 
                                        , is0( pqrFinal[2].z) ?"green":"red") );
                                        
                                        
   //echo(octants_F = _color(octant(siteOnX[2]), octant(siteOnX[2])==[0,0,1]?"red":"green"));
   //echo(octants_siteOnX = [for(p=siteOnX) octant(p)] );

   //Green(.4) Plane( expandPts([ A, J_A_OB, N([A,J_A_OB,O])],4 ));
   //Gold(0.1) Plane( expandPts([X*5,Z*5,X*-5,Z*-5],3) );
  
} 
//dev_movePtsToOrigin_1867();


module dev_MoveToOrigin_func( ) // 2018.6.5
{
   // Move obj from ORIGIN_SITE to *to*
   echom("dev_MoveToOrigin_func()");
   site = pqr();//p=4,r=6);
   //site= [ [1.37,0.8,-3.3], [0.2,-1.34,-0.13], [-1.48,2.07,4.51]];
   aSite= angle(site);
   echopts(site,"site");
   MarkPts(site, "PQR");
   Plane(site);
   
   //===========================================
   // Move site to O
   
   site0 = [ for(p=site) p-site[1] ];
   A=site0[0]; B=site0[2];
   //Red() MarkPts( site0, "AOB" );
     
   //===========================================
   // Rot site0[0] (about [ site0[1], site0[2] ] )
   //  to X
   
   axisA2X = [O,-1*N([A,O,X])];
   rotaA2X = angle([A,O,X]); 
   Line( axisA2X, r=0.1, color=["blue",0.3] );
   siteOnX= concat( [ onlinePt( [O,X], len=dist(site[0],site[1]) )
            , O ]
            
            , [for(i=[2:len(site)-1])
                    rotPt( site0[i], axisA2X, a=rotaA2X )
            ]
            //, rotPts( slice(siteOnX,2) axisForAx, a=angle([A,O,X]) )      
            );
   DEF = siteOnX;          
   echo(siteOnX=siteOnX);
   echo( rotaA2X= _green(rotaA2X));
   echo( aSite = aSite );
   echo( aDEF = angle(siteOnX) );
                     
   Green() MarkPts( siteOnX, "DEF");
   //Green(0.2) Plane(siteOnX);
   
   //===========================================
   J_F_X = projPt( DEF[2],[O,X]); 
   FonXY = anglePt( [X,O,Y], a=aSite, len= dist( site[1], site[2] ) );
   MarkPt(J_F_X, "J_F_X");
   MarkPt(FonXY, "FonXY");
   Mark90([DEF[2], J_F_X, FonXY] );
   rotaF2XY = angle( [DEF[2], J_F_X, FonXY ] );
   
   F_octant = octant(siteOnX[2]);
   a = F_octant==[0,0,0]
      || F_octant==[1,0,0] || F_octant==[0,1,0 ] || F_octant==[1,1,0] ? rotaF2XY
      : -rotaF2XY;
   
   
   siteFinal= concat( [ siteOnX[0]
            , O ]
            
            , [for(i=[2:len(site)-1])
                    rotPt( siteOnX[i], [X,O], a=a )
              ]
            );
   
   echo( FonXY = FonXY );
   echo( rotaF2XY = rotaF2XY, a=a );
   echo( aSite = aSite );
   echo( aFinal = angle(siteFinal) );
   echo(siteFinal=siteFinal);
   
   echo(_red("CHECK:"), F_octant, rotaF2XY, a, is0( siteFinal[2].z) );
                     
   Blue() MarkPts( siteFinal, "GHI");
   Blue(0.2) Plane(siteFinal);
   
   echo(_red("Checking octants of site:"));
   //echo(octants_site = [for(p=site) octant(p)] );
   
   echo(octants_F = _color(octant(siteOnX[2]), octant(siteOnX[2])==[0,0,1]?"red":"green"));
   //echo(octants_siteOnX = [for(p=siteOnX) octant(p)] );
   
   
   
         
   //J_A_OB = projPt( A, [O,B] );
   //echo(J_A_OB=J_A_OB);
   //MarkPt(J_A_OB,"J_A_OB");
   //Mark90( [A, J_A_OB,O] );
   
   //J_Z_ 
   //echo(A=A,O=O);  
   //echo(N=N(A,J_A_OB,O)); 
   
   //cpts = circlePts( [B,J_A_OB,A], rad=dist(J_A_OB,A), n=36 );
   //MarkPts(cpts, Label=false);
   //Red(0.1) Plane( expandPts([ B,O,Z],2) );
     
     
   //Green(.4) Plane( expandPts([ A, J_A_OB, N([A,J_A_OB,O])],4 ));
   Gold(0.1) Plane( expandPts([X*5,Z*5,X*-5,Z*-5],3) );
  
     //rotate( )
     
//     mvdata = getMoveData( from=site, to=ORIGIN_SITE );
//     m1 = rotM( hash(mvdata, "rotaxis")
//              , -hash(mvdata, "rota")
//              );
//     m2 = rotM( hash(mvdata, "rotaxis2")
//              , -hash(mvdata, "rota2")
//              );
//     echo(mvdata= _fmth(mvdata));
//     echo(m1= m1);
//     echo(m2= m2);         
//     translate( site[1] )
//     multmatrix( m= m2 ) 
//     multmatrix( m= m1 )   
//     children();
//   }
  // else children();
} 
//dev_MoveToOrigin_func();

module dev_Move_via_mirrors_186a()
{
  echom("dev_Move_via_mirrors_186a() -- try to move by mirrors. 2018.6.10");
  _blue("First, check how it works around the ORIGIN --- Ronaldo's RotFromTo()");
  _blue("We move obj on OP to OR ");
   
  P= randPt(d=5); R= randPt(d=5);
  echo(P=P, R=R);
  MarkPts( [P,O,R], "POR"); 
  
  
  _cmt("Make a cube on PO");
  cpts = cubePts( [2,1,0.5], site= [P,O,R] );
  echo(cpts = cpts);
  
  module Obj()
  {
    MarkPts(cpts);
    Gold(0.3) polyhedron(cpts, rodfaces(4));
  }
  Obj();
  
  
  _cmt("Mirror #1");
  
  //pl1= expandPts(getPlaneByNormalLine([O,P]), 1);
  //Red(0.1) Plane(pl1);
  module Obj2()
  {
    Red() mirror(P) Obj();
  }
  //Obj2();
  
  _cmt("Mirror #2");
  S= P/norm(P)+R/norm(R); // angleBysectPt between P and R
  echo(P_n = P/norm(P), R_n= R/norm(R) );
  echo(S=S);
  Green()Arrow([O,S], len=4, r=0.01);
  
  pl2= expandPts(getPlaneByNormalLine([O,S]), 1);
  //Green(0.1) Plane(pl2);
  Green(0.3) mirror(S) Obj2();
  echo(aPOS_is_aROS = isequal(angle([P,O,S]), angle([R,O,S]) ) );
}
//dev_Move_via_mirrors_186a();

module dev_Move_via_mirrors_186b()
{
  echom("dev_Move_via_mirrors_186b() -- try to move by mirrors.");
  _blue("In _186a, we move obj on OP to OR ");
  _blue("Now we move obj from POR to SOU ");
   
  P= randPt(d=5); R= anglePt( [P,O,randPt(d=5)], a=45, len=4) ;
  echo(P=P, R=R);
  MarkPts( [P,O,R], "POR"); 
  
  S= randPt(d=5); U= anglePt( [S,O,randPt(d=5)], a=30, len=4) ;
  echo(S=S,U=U);
  Green() MarkPts( [S,O,U], "SOU"); 
  
  
  _cmt("Make a cube on PO");
  cpts = cubePts( [2,1,0.5], site= [P,O,R] );
  echo(cpts = cpts);
  module Obj()
  {
    MarkPts(cpts);
    Gold(0.3) polyhedron(cpts, rodfaces(4));
  }
  Obj();
  
  
  _cmt("Mirror #1");
  
  //pl1= expandPts(getPlaneByNormalLine([O,P]), 1);
  //Red(0.1) Plane(pl1);
  module Obj2()
  {
    Red() mirror(P) Obj();
  }
  //Obj2();
  
  _cmt("Mirror #2");
  A= P/norm(P)+S/norm(S); // angleBysectPt between P and S
  echo(P_n = P/norm(P), A_n= A/norm(A) );
  echo(A=A);
  Red()Arrow([O,A], len=4, r=0.02);
  
  pl2= expandPts(getPlaneByNormalLine([O,A]), 1);
  Green(0.3) mirror(A) Obj2();
  echo(aPOS_is_aROS = isequal(angle([P,O,A]), angle([A,O,S]) ) );
}
//dev_Move_via_mirrors_186b();

module dev_Move2_180613()
{
   echom("dev_Move2_180613");
   _blue("Make a Move using RotObjFromTo");
   
   Obj();
   //1_setup();
   //2_translate_pqr_2_T();
   //3_rot_TA_2_TS();
   //4_find_rotated_B();
   //5_rot_from_Binormal_of_STBr_to_that_of_STU();
   Obj5();
   
   _cmt("Setup");
   pqr = pqr(4); 
   stu = pqr(4); 
   //pqr = [[2.31, 2.06, -2.36], [-1.04, 1.59, -1.85], [-3.05, 2.69, 2.28]];
   //stu = [[-1.61, -2.11, -0.53], [-3.66, -0.87, -0.79], [-0.27, 3.39, -1.95]];
   //pqr = [[0.3, 1.6, 3.78], [-3.2, -2.04, 0.07], [0.97, -2, 0.4]];
   //stu = [[-3.14, -3.92, -0.42], [2.5, -1.78, 0.24], [-1.21, 3.97, -0.15]];
   //pqr = [[-1.32, -1.42, 0.74], [2.2, -0.59, 0.07], [-1.12, 0.73, -2.66]];
   //stu = [[2.33, -3.49, -3.8], [-0.83, -2.11, -1.33], [-0.46, -1.04, -2.96]];
   echo(pqr=pqr);
   echo(stu=stu);
   Gold() MarkPts(pqr, "PQR"); 
   Purple() MarkPts(stu, "STU", r=0.03); 
   Purple(.2) Plane(stu);

   P=pqr[0];Q=pqr[1];R=pqr[2]; 
   S=stu[0];T=stu[1];U=stu[2]; 
   
   
   module Obj(){ Gold(.6) Cube([3,2,1], site=pqr); }
   

   module 1_setup()
   {
      Obj(); Plane(pqr);
   }
   

   A= P+T-Q; B=R+T-Q; 
   atb = [A,T,B];
   module Obj2() { Red(.3) translate(T-Q)  Obj(); }
   module 2_translate_pqr_2_T()
   {  
     Red(.2) Arrow( [Q,T]);
     Red(.3) Plane(atb);
     Obj2();
     Red() MarkPts(atb, "ATB"); 
   }
   
   
   module Obj3() { Green(.3) RotObjFromTo( [A, T,S] )
                   Obj2(); }
                   
   module 3_rot_TA_2_TS()
   {                
     Obj3();   
     Green() ArcArrow( [A,S,T] );   
   }
   

   Br= rotPt( B, axis= [T,N([S,T,A])], a= angle([S,T,A]) );
   
   module 4_find_rotated_B()
   {
     Blue(0.2) MarkPt( Br, "Br", r=0.1 );
     Blue() MarkPt( Br,"Br");
     Blue(0.2) Plane( [S,T,Br] );
   }
   
   
   B_STBr= B([S,T,Br]);
   B_STU=  B([S,T,U]);
   module Obj5() { Purple(.6)  RotObjFromTo( [B_STBr,T,B_STU] )
                   Obj3(); }
                   
   module 5_rot_from_Binormal_of_STBr_to_that_of_STU()
   {
      Line([T,B_STBr]); MarkPt(B_STBr, "B_STBr");
      Line([T,B_STU]); MarkPt(B_STU, "B_STU");
      Purple() ArcArrow( [B_STBr,B_STU,T] );
      Obj5();
   }

}
//dev_Move2_180613();

module dev_Move2_module_180614()
{
   echom("dev_Move2_module_180614");
   _blue("Make a module based on dev_Move2_180613");
   
   pqr = pqr(4); 
   stu = pqr(4); 
   //pqr = [[2.31, 2.06, -2.36], [-1.04, 1.59, -1.85], [-3.05, 2.69, 2.28]];
   //stu = [[-1.61, -2.11, -0.53], [-3.66, -0.87, -0.79], [-0.27, 3.39, -1.95]];
   //pqr = [[0.3, 1.6, 3.78], [-3.2, -2.04, 0.07], [0.97, -2, 0.4]];
   //stu = [[-3.14, -3.92, -0.42], [2.5, -1.78, 0.24], [-1.21, 3.97, -0.15]];
   //pqr = [[-1.32, -1.42, 0.74], [2.2, -0.59, 0.07], [-1.12, 0.73, -2.66]];
   //stu = [[2.33, -3.49, -3.8], [-0.83, -2.11, -1.33], [-0.46, -1.04, -2.96]];
   echo(pqr=pqr);
   echo(stu=stu);
   Gold() MarkPts(pqr, "PQR"); 
   Purple() MarkPts(stu, "STU", r=0.03); 
   Purple(.2) Plane(stu); 
        
   module Obj(){ Gold(.6) Cube([3,2,1], site=pqr); }
  
   Obj();
   Purple(.6) Move2( pqr, stu ) Obj();

   module Move2( pqr, stu )
   {   
     P=pqr[0]; Q=pqr[1]; R=pqr[2]; 
     S=stu[0]; T=stu[1]; U=stu[2];
      
     V= P+T-Q;  W= R+T-Q; // translating from pqr to stu => VTW
     
     Wr= rotPt( W    // The new W after rot from TV to TS
              , axis= [T,N([S,T,V])]
              , a= angle([S,T,V]) 
              );
              
     B_STWr= B([S,T,Wr]); // Binormal pt of [S,T,Wr]
     B_STU=  B([S,T,U]);  // Binormal pt of [S,T,U]
     
     RotObjFromTo( [B_STWr,T,B_STU] ) 
     RotObjFromTo( [V,T,S] )   // Rot from TA to TS  
     translate(T-Q)            // Moved from pqr to stu => atb
     children();
   }

}
//dev_Move2_module_180614();

module dev_Move_Move2_profiling_180615(which_Move=1)
{
   echom("dev_Move_Move2_profiling_180615");
   _blue("Compare the speed of Move and Move2");
   /*
      n   Move   Move2 (sec)
     ---  -----  -----  
     20     4      4
     50     9      9
    100    19~20     19~20 
   
   */
   
   n=100;
   to = pqr(4); 
   //echo(to=to);
   
   module Obj(from){ Gold(.6) Cube([3,2,1], site=from); }
     
   for(i=[1:n])
   {
     from = pqr(d=[4,7]); 
     //echo(i=i, from=from);
        
     Obj(from);
     
     
     if(which_Move==1)
     {
       Red(.3) Move(from=from,to=to) Obj(from);
     }
     else
     {
       Green(0.3) Move2(from=from,to=to) Obj(from);
     }  
   }
}
//dev_Move_Move2_profiling_180615(2);

module demo_MoveToOrigin()
{
   echom("demo_MoveToOrigin(site)");
   echo(_blue("Move obj from site to the ORIGIN_SITE"));
    
   site = pqr(p=6,r=4);
   site= [[-3.11384, 1.64205, -4.09101], [1.13, -0.24, -0.29], [-0.969185, -2.6181, -2.72682]];
   
   MarkPts(site, "PQR");
   
   module Obj()
   { MoveTo(site)
     scale(0.08)
     text("demo_MoveTo"); 
   }

   Obj();
   
   MoveToOrigin(site)
   Red() Obj();
}
//demo_MoveToOrigin();

module Move_demo(){
    
    echom("Move_demo");
    pqr=pqr(p=8,r=4);
    //pqr= [[-4.39732, 2.8604, 2.98893], [1.94, 0.94, -1.5], [-1.37592, 0.94, 0.737111]];
    
    MarkPts( pqr, "PQR"); //Plane(pqr, mode=1);
    
    module Obj()
    {      
      scale(0.08)
      text("Move_demo");
    }
    
    Obj();
    
    module Obj2()
    {
      Move( to=pqr )
      Red() Obj();
    }
    
    Obj2();
    
    scale(1.1) Move(from=pqr, to=ORIGIN_SITE)
    Green(.4) Obj2();    
}    
//Move_demo();

//Move_any_site_demo();
module Move_any_site_demo(){  //// BUGGY !!
    
    echom("Move_any_site_demo");
                 
   pqr=pqr(p=4,r=2);
   MarkPts( pqr, "PQR");
   pqr2=pqr(d=5);
   color("red") MarkPts( pqr2, "PQR");
    
   module obj1(site)
   { 
     Move(to=site) Cube([3,2,1]);
     // Or you can do:
     //  Cube([3,2,1], site=site);
   }      
   
   obj1(pqr);   
   Move(from=pqr,to=pqr2) color("red",0.5) obj1(pqr);

}    


//========================================================

//movePts_demo();
module movePts_demo(){
    
   echom("movePts_demo");
    
   pqr = pqr();
   pqr = [[-1.06, -1.84, -0.91], [1.37, -0.71, -1.25], [-1.12, -0.93, -1.47]];
   echo(pqr=pqr);
   MarkPts( pqr, "PQR" ); Plane(pqr);
   
   cube1 = cubePts( site=pqr );
   //echo(cube1=cube1);
   MarkPts(cube1, Line=false);
   Cube(site=pqr);
   //color(undef, 0.8)
   //polyhedron( points= cube1, faces=faces("rod", nside=4,nseg=1)); 
    
   d=3; 
   stu = [for(p=pqr(p=8,r=6)) p +[d,d,d]] ;
   MarkPts( stu, "STU" ); Plane(stu, color=["red",0.3]);
   cube2 = movePts( cube1, to=stu, from=pqr );
   MarkPts(cube2, Line=false);
   
   color("red", 0.8)
   polyhedron( points= cube2, faces=faces("rod", nside=4,nseg=1)); 
  
//   cube3 = movePts( cube2, from=stu, to=ORIGIN_SITE);
//   MarkPts(cube3, Line=false);
//   color("green", 0.8)
//   polyhedron( points= cube3, faces=faces("rod", nside=4,nseg=1)); 
  
}    

//========================================================
//module movePts_demo_from_ORIGIN(){
//    
//   echom("movePts_demo_from_ORIGIN");
//   //echo("Move ) 
//   //MarkPts( pqr, "ch=[r,0.05];l=PQR" );
//   cube1 = cubePts( [5,2,1]); //pqr );
//   echo(cube1=cube1);
//   MarkPts( cube1, "l");
//   color(undef, 0.8)
//   polyhedron( points= cube1, faces=faces("rod", nside=4,nseg=1)); 
//    
//   d=4; 
//   pqr = randPts(3, d=[4,8]);
//   //stu = [for(p=pqr(p=8,r=6)) p +[d,d,d]] ;
//   MarkPts( pqr, "ch=[r,0.05];l=PQR" );
//   cube2 = movePts( cube1, pqr ); // <==== arg "from" is skipped
//   MarkPts( cube2, "l");
//   echo( cube2=cube2 );
//   color("red", 0.5)
//   polyhedron( points= cube2, faces=faces("rod", nside=4,nseg=1)); 
//  
//}    
//movePts_demo_from_ORIGIN();

//========================================================
module movePts_demo_no_from(){
    
   echom("movePts_demo_no_from");
   echo("movePts( cube1, pqr2 ) where cube1 is at any location and *from* is not given" );
   echo("In this case, cube1 is moved from its location to the new location"); 
   pqr = pqr();
   MarkPts( pqr, "PQR" );
   cube1 = cubePts( site=pqr );
   MarkPts( cube1, Line=false);
   color(undef, 0.8)
   polyhedron( points= cube1, faces=faces("rod", nside=4,nseg=1)); 
    
   d=4; 
   pqr2 = randPts(3, d=[4,8]);
   //stu = [for(p=pqr(p=8,r=6)) p +[d,d,d]] ;
   MarkPts( pqr2, "PQR" );
   cube2 = movePts( cube1, to=pqr2 ); // <==== arg "from" is skipped
   MarkPts( cube2, Line=false);
   echo( cube2=cube2 );
   color("red", 0.5)
   polyhedron( points= cube2, faces=faces("rod", nside=4,nseg=1)); 
  
}    
//movePts_demo_no_from();


//========================================================
module movePts_demo_from_ORIGIN(){
    
   echom("movePts_demo_from_ORIGIN");
   //echo("movePts( cube1, pqr2, from ) where cube1 is at a location away from *from* (ORIGIN)" );
   //echo("Note that cube1 is y=3 away from ORIGIN, and this distance is kept after the move"); 
   
   echo(_color("cube1 is <b>y=3</b> away from ORIGIN", "brown"));
   echo(_red("cube2= <b>movePts(cube1, to=pqr)</b> // no 'from', treated as moving from cube1 position to pqr, means <b>y=3 is ignored</b>."));
   echo(_blue("cube3= <b>movePts(cube1, from=ORIGIN_SITE, to=pqr)</b> // <b>y=3 is kept</b>."));
   
   cube1 = transPts(cubePts( [5,2,1] ), ["y", 3]);
   MarkPts( cube1, Line=false);
   color(undef, 0.8)
   polyhedron( points= cube1, faces=faces("rod", nside=4,nseg=1)); 
    
   from = [ [1,0,0],ORIGIN, [-1,1,0] ];
    
   d=4; 
   pqr2 = randPts(3, d=[4,8]);
   //stu = [for(p=pqr(p=8,r=6)) p +[d,d,d]] ;
   MarkPts( pqr2, "PQR" );
   cube2 = movePts( cube1, to=pqr2 ); // <==== arg "from" is skipped
   MarkPts( cube2,  Line=false);
   echo( cube2=cube2 );
   color("red", 0.5)
   polyhedron( points= cube2, faces=faces("rod", nside=4,nseg=1)); 
   
   cube3 = movePts( cube1, from=ORIGIN_SITE, to=pqr2 ); // <==== arg "from" is skipped
   MarkPts( cube3,  Line=false);
   //echo( cube2=cube2 );
   color("blue", 0.5)
   polyhedron( points= cube3, faces=faces("rod", nside=4,nseg=1)); 
  
}    
//movePts_demo_from_ORIGIN();

//========================================================
module movePts_demo_from_any_loc(){
    
   echom("movePts_demo_from_any_loc");
   echo("movePts( cube1, pqr2, from ) where cube1 is at a location away from *from* (any pqr)" );
   echo("Note that the relationship between cube1 and *from* is kept after the move"); 
   
   pqr = pqr();
   MarkPts( pqr, "PQR" );
   cube0 = cubePts( site=pqr );
   //MarkPts( cube0,  Line=false);
   //color(undef, 0.8)
   // polyhedron( points= cube0, faces=faces("rod", nside=4,nseg=1)); 
   //Cube(site=pqr);
    
   cube1 = transPts(cube0, ["t",(pqr[0]-pqr[1])/3] );
   Cube(site= cube1, color=["red",0.3]);
   MarkPts( cube1,  Line=false);
   
   pqr2 = pqr(); 
   MarkPts( pqr2, "PQR");
   cube2 = movePts( cube1, from=pqr , to=pqr2); 
   Cube(site= cube2, color=["green",0.3]);
  
}    
//movePts_demo_from_any_loc();


//========================================================
module movePts_demo_move_pqr_itself(){
    
   echom("movePts_demo_move_pqr_itself");
    
   pqr = pqr();
   MarkPts( pqr, "PQR" );
   Plane( pqr );
//   cube1 = cubePts( pqr );
//   color(undef, 0.8)
//   polyhedron( points= cube1, faces=faces("rod", nside=4,nseg=1)); 
   
   //d=4; 
   stu = pqr(); //[for(p=pqr(p=8,r=6)) p +[d,d,d]] ;
   MarkPts( stu, "STU" );
   pqr2 = movePts( pqr, to=stu ); // <==== arg "from" is skipped
   echo( pqr2=pqr2 );
   color("red", 0.8)
   Plane( pqr2); 
  
}    
//movePts_demo_move_pqr_itself();

module dev_movePts_more_checkups_move_to_ORIGIN_1868()
{
   echom("dev_movePts_more_checkups_move_to_ORIGIN_1868()");
   echo(_red("=========================== setup pqr "));
   _site = pqr();//p=4,r=6);
   pqr=pqr();
   pqr= [ [1.37,0.8,-3.3], [0.2,-1.34,-0.13], [-1.48,2.07,4.51]];
   pqr = [_site[0], _site[1], B(_site) ];
   //
   pqr = [for(p= [X,O,Z])p+[2,3,4]];
   pqr = [for(p= [-1*X,O,Z])p+[2,3,4]]; // error : P.x < 0
   pqr = [for(p= [-1*X,O,-1*Z])p+[2,3,4]]; // error : P.x < 0 
   pqr = [for(p= [X,O,-1*Z])p+[2,3,4]];
   //
   pqr = [for(p= [Z,O,X])p+[2,3,4]];
   pqr = [for(p= [-1*Z,O,X])p+[2,3,4]]; // error : P.x < 0
   pqr = [for(p= [-1*Z,O,-1*X])p+[2,3,4]]; // error : P.x < 0 
   pqr = [for(p= [Z,O,-1*X])p+[2,3,4]];
  //
   //
   pqr = [for(p= [Y,O,Z])p+[2,3,4]];
   pqr = [for(p= [-1*Y,O,Z])p+[2,3,4]];
   pqr = [for(p= [-1*Y,O,-1*Z])p+[2,3,4]];
   pqr = [for(p= [Y,O,-1*Z])p+[2,3,4]];
//
   pqr = [for(p= [Z,O,Y])p+[2,3,4]];
   pqr = [for(p= [-1*Z,O,Y])p+[2,3,4]];
   pqr = [for(p= [-1*Z,O,-1*Y])p+[2,3,4]];
   pqr = [for(p= [Z,O,-1*Y])p+[2,3,4]];
      //
   //
   pqr = [for(p= [X,O,Y])p+[2,3,4]];
   pqr = [for(p= [-1*X,O,Y])p+[2,3,4]];  // error : P.x < 0
   pqr = [for(p= [-1*X,O,-1*Y])p+[2,3,4]]; // error : P.x < 0
   pqr = [for(p= [X,O,-2*Y])p+[2,3,4]];
   //
   pqr = [for(p= [Y,O,X])p+[2,3,4]];
   pqr = [for(p= [-1*Y,O,X])p+[2,3,4]];  // error : P.x < 0
   pqr = [for(p= [-1*Y,O,-1*X])p+[2,3,4]]; // error : P.x < 0
   pqr = [for(p= [Y,O,-2*X])p+[2,3,4]];

   P=pqr[0]; Q=pqr[1]; R=pqr[2]; 
   aPQR = angle(pqr);
   Mark90( [P,Q, B(pqr) ] );
   MarkPts(pqr, "PQR");
   Plane(pqr);
   
   from2= [for(p=pqr)p-pqr[1]];
   MarkPts( from2);
   
   pqrFinal = movePts(pqr, to=ORIGIN_SITE);
   
   MarkPts(pqrFinal, Label=["PF","QF","RF"] );
   Plane(pqrFinal, color="red");    
   echo(pqrFinal = pqrFinal );
   
   echo(_red("=========================== Checking octants of site:"));
   PF= pqrFinal[0];
   RF= pqrFinal[2];
   
   a_correct = isequal(angle(pqrFinal), aPQR);
   PF_on_pos_X = PF.x>0 && is0(PF.y) && is0(PF.z) ;
   RF_lt_0 = RF.y>0;
   RF_on_XY = is0(RF.z);
   
   
   echo( a_correct = _color( a_correct, a_correct?"green":"red") );
                                   
   echo( PF_on_pos_X = _color( PF_on_pos_X, PF_on_pos_X? "green":"red" ) );
                                               
   echo( RF_lt_0 = _color( RF_lt_0, RF_lt_0? "green":"red" ) );
                                               
   echo( RF_on_XY = _color( RF_on_XY, RF_on_XY ?"green":"red") );
                  
   mvdata = getMoveData(from=pqr, to=ORIGIN_SITE, debug=1);
   echo(mvdata = _fmth(mvdata));               

}
//dev_movePts_more_checkups_move_to_ORIGIN_1868();

//. move dev  (moved from scadx_obj.scad on 2018.5.31 )


module move_dev(){
   echom("move_dev");
   //ColorAxes(["len",8]); 
   O = ORIGIN;
    
   x=5; y=2; z=1; 
   fillet=0.05;  // thickness of red surface
   
   X = [x,0,0];
   Y = [0,y,0];
   Z = [0,0,z];
   MarkPts( [X,O,Y], r=0.03, Label=["text","XOY","scale",2,"color","black"]); 
   //Plane( [X,O,Y], "trp=0.1");
    
   module obj(color){
        
       union(){
         color("red",1) cube([x,y,fillet]); // fillet     
         translate( [0,0,fillet]) 
           color(color, 0.6)
           cube([x,y,z-fillet]);    
       } 
   }

   pqr = [for(p=pqr(r=5,p=8)) p+ [-3,-3,3]];
   pqr = [[-8.57692, 1.2036, 0.868205], [-3.42518, -4.10183, 3.91976], [-7.23403, -0.929802, 3.26329]];    
   echo(pqr = pqr );    
   MarkPts(pqr, r=0.03, Label=["text","PQR","scale",2,"color","black"]);
   obj();   

   //============================================
   // Translate pqr to ORIGIN: 
   //
   pqr0 = [for(p=pqr)p-pqr[1]]; // pqr moved to ORIGIN
   MarkPts(pqr0,  r=0.03, Label=["text",["P0","Q0","R0"],"scale",2,"color","black"]);
   Arrow( [pqr[1],pqr0[1]], r=0.03,color=["black",0.6] );
   
   //============================================
   // Rotate obj to pqr0 
   // 
   rotpl = [onlinePt( [O, pqr0[0]],len=x),O,X];
   ArcArrow( p201(rotpl)
            , rad=x,opt="cl=green") ;
   N = N( rotpl );
   Mark90( [X,O,N] );
   Mark90( [pqr0[0],O,N] );
   
   rotAxis = [ O, N];
   Arrow2( rotAxis, opt="cl=gold" );
   
   echo( a = angle(rotpl) );
   multmatrix( m= rotM( rotAxis, angle(rotpl))){ 
       obj("green");
   }
   //============================================
   /*
   Next step requires a rotation for an angle that
   is difficult (if possible) to obtain. Try another
   approach by rotating pqr to fit obj (but not rotating
   obj to fit pqr). See moveto_dev2()
   */
}  
//move_dev();

module move_dev2(){
   echom("move_dev2");
   echo(_blue("This mod moves pqr to xy-plane, placing PQ on x"));
   //ColorAxes(["len",8]); 
    
   O = ORIGIN;
    
   x=5; y=2; z=1; fillet=0.05;
   X = [x,0,0];
   Y = [0,y,0];
   Z = [0,0,z];
   MarkPts( [X,Y], r=0.03, Line=false
          , Label=["text","XY","scale",2,"color","black"]); 
   //Plane( [X,O,Y], "trp=0.1");
    
   module obj(color){
        
       union(){
         color("red",0.4) cube([x,y,fillet]); // fillet     
         translate( [0,0,fillet]) 
           color(color, 0.3)
           cube([x,y,z-fillet]);    
       } 
   }

   pqr = [for(p=pqr(r=5,p=8)) p+ [-3,-3,3]];
   
   //pqr = [[-8.57692, 1.2036, 0.868205], [-3.42518, -4.10183, 3.91976], [-7.23403, -0.929802, 3.26329]];    
   pqr= [ [-0.52,1.71,0.11], [-4.39,-4.79,2.71], [-4.61,0.17,3.26]];
   echopts( pqr, "pqr= " );    
   MarkPts(pqr,  r=0.03, Label=["text","PQR","scale",2,"color","black"]);
   Plane(pqr);
   
   //============================================
   // Translate pqr to ORIGIN (pqr0): 
   //
   pqr0 = [for(p=pqr)p-pqr[1]];
   MarkPts(pqr0,  r=0.03, Label=["text",["P0","Q0","R0"],"scale",2,"color","black"]);
   Plane(pqr0, color=["red",0.2]);
   MarkCenter( pqr0, "pqr0", color="purple");
   Arrow( [pqr[1],pqr0[1]], r=0.03,color=["black",0.6] );
   
   //============================================
   // Rotate pqr0 to x ==> pqr_rot
   // 
   rotpl = [ X,O,pqr0[0]];
   MarkCenter( rotpl, "rotpl", color="purple");
   Plane(rotpl, color=["gray",0.2]);
   
   N = N( rotpl );
   //Arrow2( rotAxis, opt="cl=gold" );
   //Mark90( [X,O,N] );
   //Mark90( [pqr0[0],O,N] );
   
   rotAxis = [ O, N*4];// <==============
   Arrow(rotAxis, r=0.05);
   rota = -angle(rotpl, Rf=N);// <==============

   //echo( rotpt = rotPt( pqr0[0], rotAxis, 
   pqr_rot = rotPts( pqr0, axis=rotAxis, a = rota);
   MarkCenter( pqr_rot, "pqr_rot", color="purple");
   Plane( pqr_rot, color=["green",0.2]);
   
      
   echo( rota = rota, rotAxis=rotAxis, pqr_rot=pqr_rot );
   ArcArrow( [ pqr0[0]
             , onlinePt( p10(pqr_rot), dist(pqr_rot))
             , O //pqr_rot[2]
             ]
            , rad= dist(pqr0), opt="cl=green") ;
   
   //============================================
   // Rotate pqr_rot to obj
   // 
   Rrot = pqr_rot[2];
   Jpl = projPt( Rrot, ORIGIN_SITE); //[[1,0,0],O,[0,1,0]] );
   Jx = projPt( Rrot, [[1,0,0],O] );
   MarkPt(Jpl, "Jpl");
   Mark90( [ Rrot,Jpl,Jx]);
   //Mark90( [ Rrot,Jpl,Jx], "ch");
   
   rota2 = angle( [Rrot,Jx,Jpl]  // <==============
                , Rf= onlinePt([Jx,O],-1));
   rotAxis2= [ Jx,O]; // <=================             
   Arrow(rotAxis2, r=0.05);
   echo(rota2=rota2);
   Rrot2= rotPt( Rrot, rotAxis2, rota2 );
   //MarkPts( replace(pqr_rot,2,Rrot2),  r=0.03, Label=["text","PQR","scale",2,"color","black"]);
   //MarkPts( replace(pqr_rot,2,Rrot2), "ch;pl=[color,blue,transp,0.2]");
   ArcArrow( [ Rrot, Rrot2,Jx], rad=dist( Rrot,O) );   
  
   //============================================
   echo( _red(str("Is Rrot2 on the xy-plane? ", isOnPlane( [[1,0,0],[0,1,0],O],Rrot2 ))));
   
   //MarkPts( pqr_rot, "ch;pl=[transp,0.2,color,green];l=PQR");
   //obj();   

}  
//move_dev2();

module move_dev3(){
   echom("move_dev3");
   echo(_blue("Like moveto_dev2, this mod moves pqr to xy-plane, placing PQ on x. Unlike it, we skip all the middle steps, calc 2 sets of rotAxis/rota, and use them directly "));
   //echo(_blue("Like moveto_dev2, this mod moves pqr to xy-plane, placing PQ on x. Unlike it, we skip all the middle steps, calc 2 sets of rotAxis/rota, and use them directly "));
   //ColorAxes(["len",8]); 
    
   O = ORIGIN;
    
   x=5; y=2; z=1; fillet=0.05;
   X = [x,0,0];
   Y = [0,y,0];
   Z = [0,0,z];
   MarkPts( [X,Y], "l=XY"); 
   //Plane( [X,O,Y], "trp=0.1");
    
   module obj(color){
        
       union(){
         color("red",0.4) cube([x,y,fillet]); // fillet     
         translate( [0,0,fillet]) 
           color(color, 0.3)
           cube([x,y,z-fillet]);    
       } 
   }

   pqr = [for(p=pqr(r=5,p=8)) p+ [-3,-3,3]];
   //pqr = [[-8.57692, 1.2036, 0.868205], [-3.42518, -4.10183, 3.91976], [-7.23403, -0.929802, 3.26329]];    
   // Correct dir:
   //pqr= [ [-4.51,-8.35,6.59], [-3.79,-1.83,2.01], [-1.6,-6.3,2.5]];
   // Wrong dir:
   pqr =[ [-1.55,-1.91,9.17], [-3.66,-3.2,1.56], [-7.01,-6.26,3.66]];
   echopts(pqr);
   MarkPts(pqr,  r=0.03, Label=["text","PQR","scale",2,"color","black"]);
   Plane(pqr);
   
   //============================================
   // Translate pqr to ORIGIN (pqr0): 
   //
   pqr0 = [for(p=pqr)p-pqr[1]];
   //MarkPts(pqr0, "ch;pl=[transp,0.1,color,red];l=PQR");
   //Arrow2( linePts([pqr[1],pqr0[1]], len=-0.2), "trp=0.5;cl=red" );
   
   //============================================
   // Rotate pqr0 to x ==> pqr_rot
   // 
   rotpl = [ pqr0[0],O,X];
   N = N( rotpl );
   //Arrow2( rotAxis, opt="cl=gold" );
   //Mark90( [X,O,N] );
   //Mark90( [pqr0[0],O,N] );
   
   rotAxis = [ O, N];// <==============
   rota = -angle(rotpl, Rf=N);// <==============

   //echo( rotpt = rotPt( pqr0[0], rotAxis, 
   pqr_rot = rotPts( pqr0, axis=rotAxis, a = rota);   
   //echo( rota = rota, rotAxis=rotAxis, pqr_rot=pqr_rot );
//   ArcArrow( [ pqr0[0]
//             , onlinePt( p10(pqr_rot), dist(pqr_rot))
//             , O //pqr_rot[2]
//             ]
//            , rad= dist(pqr0), opt="cl=green") ;
   
   //============================================
   // Rotate pqr_rot to obj
   // 
   Rrot = pqr_rot[2];
   Jpl = projPt( Rrot, [[1,0,0],O,[0,1,0]] );
   Jx = projPt( Rrot, [[1,0,0],O] );
   //Mark90( [ Rrot,Jpl,Jx], "ch");
   
   rota2 = angle( [Rrot,Jx,Jpl]  // <==============
                , Rf= onlinePt([Jx,O],-1));
   rotAxis2= [ Jx,O]; // <=================             
   //echo(rota2=rota2);
   Rrot2= rotPt( Rrot, rotAxis2, rota2 );
   pqr_final = replace(pqr_rot,2,Rrot2);
   MarkPts(pqr_final,  r=0.03, Label=["text","PQR","scale",2,"color","black"]);
   Plane(pqr_final);
   
   //MarkPts( , "ch;pl=[color,blue,transp,0.2]");
   //ArcArrow( [ Rrot, Rrot2,Jx], rad=dist( Rrot,O) );   
  
   //============================================
   echo( _red(str("Is on the xy-plane? ", isOnPlane( [[1,0,0],[0,1,0],O],Rrot2 ))));
   
   //MarkPts( pqr_rot, "ch;pl=[transp,0.2,color,green];l=PQR");
   //obj();   

}  
//move_dev3();

module move_dev4(){
   echom("move_dev4");
   echo(_blue("Try moving pqr to xy-plane, and move back using the calc rotAxis and rota"));
   //ColorAxes(["len",8]); 
    
   O = ORIGIN;
    
   x=5; y=2; z=1; fillet=0.05;
   X = [x,0,0];
   Y = [0,y,0];
   Z = [0,0,z];
   MarkPts( [X,Y], "l=XY"); 
   //Plane( [X,O,Y], "trp=0.1");
    
   module obj(color){
        
       union(){
         color("red",0.4) cube([x,y,fillet]); // fillet     
         translate( [0,0,fillet]) 
           color(color, 0.3)
           cube([x,y,z-fillet]);    
       } 
   }

   pqr = [for(p=pqr(r=5,p=8)) p+ [-3,-3,3]];
   //pqr = [[-8.57692, 1.2036, 0.868205], [-3.42518, -4.10183, 3.91976], [-7.23403, -0.929802, 3.26329]];    
   echo(pqr = pqr );    
   MarkPts(pqr, "ch;l=PQR;pl=[transp,0.1]");
   
   //============================================
   // Translate pqr to ORIGIN (pqr0): 
   //
   pqr0 = [for(p=pqr)p-pqr[1]];
   //MarkPts(pqr0, "ch;pl=[transp,0.1,color,red];l=PQR");
   //Arrow2( linePts([pqr[1],pqr0[1]], len=-0.2), "trp=0.5;cl=red" );
   
   //============================================
   // Rotate pqr0 to x ==> pqr_rot
   // 
   rotpl = [ pqr0[0],O,X];
   N = N( rotpl );
   //Arrow2( rotAxis, opt="cl=gold" );
   //Mark90( [X,O,N] );
   //Mark90( [pqr0[0],O,N] );
   
   rotAxis = [ O, N];// <==============
   rota = -angle(rotpl, Rf=N);// <==============

   //echo( rotpt = rotPt( pqr0[0], rotAxis, 
   pqr_rot = rotPts( pqr0, axis=rotAxis, a = rota);   
   //echo( rota = rota, rotAxis=rotAxis, pqr_rot=pqr_rot );
//   ArcArrow( [ pqr0[0]
//             , onlinePt( p10(pqr_rot), dist(pqr_rot))
//             , O //pqr_rot[2]
//             ]
//            , rad= dist(pqr0), opt="cl=green") ;
   
   //============================================
   // Rotate pqr_rot to obj
   // 
   Rrot = pqr_rot[2];
   Jpl = projPt( Rrot, [[1,0,0],O,[0,1,0]] );
   Jx = projPt( Rrot, [[1,0,0],O] );
   //Mark90( [ Rrot,Jpl,Jx], "ch");
   
   rota2 = angle( [Rrot,Jx,Jpl]  // <==============
                , Rf= onlinePt([Jx,O],-1));
   rotAxis2= [ Jx,O]; // <=================             
   //echo(rota2=rota2);
   Rrot2= rotPt( Rrot, rotAxis2, rota2 );
   pqr_rot2= replace(pqr_rot,2,Rrot2);
   MarkPts( pqr_rot2, "ch;pl=[color,blue,transp,0.2]");
   //ArcArrow( [ Rrot, Rrot2,Jx], rad=dist( Rrot,O) );   
  
   //============================================
   //echo( _red(str("Is on the xy-plane? ", isOnPlane( [[1,0,0],[0,1,0],O],Rrot2 ))));
   echo( str("<b>Is moved onto the xy-plane? </b>", let(x=isOnPlane( [[1,0,0],[0,1,0],O],Rrot2 )) _color(x,x?"green":"red")));

   //MarkPts( pqr_rot, "ch;pl=[transp,0.2,color,green];l=PQR");
   //obj();   

   //============================================
   // MOVE BACK
   //============================================
   
   pqr_rot_b = rotPts( pqr_rot2, rotAxis2, -rota2);
   pqr0_b = rotPts( pqr_rot_b, rotAxis, -rota);
   pqr_b = [ for(p=pqr0_b) p+pqr[1]];
   MarkPts( pqr_b, "ch=[r,0.08,transp,1];r=0.1;trp=0.3");    
   echo( str("<b>Is P back ? </b>", let(x=isequal( pqr[0], pqr_b[0])) _color(x,x?"green":"red")));
   echo( str("<b>Is Q back ? </b>", let(x=isequal( pqr[1], pqr_b[1])) _color(x,x?"green":"red")));
   echo( str("<b>Is R back ? </b>", let(x=isequal( pqr[2], pqr_b[2])) _color(x,x?"green":"red")));
   
}  
//move_dev4();

module move_dev2_1(){
   echom("move_dev2_1");
   echo(_blue("Unlike move_dev2, this mod moves pqr to ANY-plane,stu, placing PQ on ST"));

   pqr = [for(p=pqr(r=3,p=4)) p+ [-3,-3,3]];
   stu = pqr(r=5,p=8);   
   
   pqr = [[-3.5475, -5.27314, 4.54913], [-3.80132, -2.63203, 1.55578], [-2.64029, 0.0900704, 2.04789]];
   stu = [[-2.59527, -0.723103, -5.27971], [0.459888, 2.32008, 1.45862], [1.81857, -1.8535, -0.936208]];
   
   /* settings produce correct result::
   
 pqr = [[-3.99387, -3.82432, 2.15561], [-5.93779, -4.8339, 5.50254], [-4.8328, -4.69235, 2.71705]]
ECHO: stu = [[-2.21788, -0.734268, -5.94684], [2.9459, -0.302786, 0.148174], [0.905777, -1.82086, -4.15686]]
ECHO: rota = -71.9295, rotAxis = [[2.9459, -0.302786, 0.148174], [2.69616, 0.65479, 0.291969]], pqr_rot = [[0.364009, -0.518527, -2.89933], [2.9459, -0.302786, 0.148174], [0.826741, -1.12649, -1.80903]]
   */    
   
   /* settings produce WRONG result::
   
   -- wrong side over rotaAxis2--
   
pqr = [[-6.90511, -0.95146, 1.66337], [-4.22967, -1.63174, 4.55807], [-4.39002, -3.63422, 2.32998]]
ECHO: stu = [[-2.71239, -3.13321, 2.0501], [2.92712, -0.169749, -2.78868], [-1.24995, 1.14936, -0.377869]]
ECHO: rota = -91.6738, rotAxis = [[2.92712, -0.169749, -2.78868], [2.76185, 0.745345, -2.42087]], pqr_rot = [[0.107368, -1.65148, -0.369288], [2.92712, -0.169749, -2.78868], [2.0766, -3.01104, -3.24]]
ECHO: "<Arrow>:---------------------"
ECHO: opt = ["color", "green"]
ECHO: rota2 = 36.2894
   
   */  
   
   //pqr = [[-6.90511, -0.95146, 1.66337], [-4.22967, -1.63174, 4.55807], [-4.39002, -3.63422, 2.32998]];
   //stu = [[-2.71239, -3.13321, 2.0501], [2.92712, -0.169749, -2.78868], [-1.24995, 1.14936, -0.377869]];
   //============================================


   O = stu[1];
   //pqr = [[-8.57692, 1.2036, 0.868205], [-3.42518, -4.10183, 3.91976], [-7.23403, -0.929802, 3.26329]];    
   echo(pqr = pqr );    
   echo(stu= stu );
   
   //============================================
   // Translate pqr to O (pqr0): 
   //
   pqr0 = [for(p=pqr)p-pqr[1]+O];
   MarkPts(pqr0, "ch;pl=[transp,0.1,color,red];l=PQR");
   
   //============================================
   // Rotate pqr0 to x ==> pqr_rot
   // 
   rotpl = [ pqr0[0],O,stu[0] ];
   N = N( rotpl );
   //Arrow2( rotAxis, opt="cl=gold" );
   //Mark90( [X,O,N] );
   //Mark90( [pqr0[0],O,N] );
   
   rotAxis = [ O, N];// <==============
   rota = -angle(rotpl, Rf=N);// <==============

   //echo( rotpt = rotPt( pqr0[0], rotAxis, 
   pqr_rot = rotPts( pqr0, axis=rotAxis, a = rota);   
   echo( rota = rota, rotAxis=rotAxis, pqr_rot=pqr_rot );
   ArcArrow( [ pqr0[0]
             , onlinePt( p10(pqr_rot), dist(pqr_rot))
             , O //pqr_rot[2]
             ]
            , rad= dist(pqr0), opt="cl=green") ;
   
   //============================================
   // Rotate pqr_rot to obj
   // 
   Rrot = pqr_rot[2];
   Jst = projPt( Rrot, p01(stu)); //[[1,0,0],O] );
   //Jpl = projPt( Rrot, stu); //[[1,0,0],O,[0,1,0]] );
   Jstu= anglePt( [ stu[0],Jst, stu[2]], a=90, len = dist( Rrot, Jst));
   
   MarkPt(Jst,"l=Jst");
   //Mark90( [ Rrot,Jpl,Jx], "ch");
   //rotAxis2= [ Jx,O]; // <=================             
   rotAxis2 = p10(stu);
   
   rota2 = angle( [Rrot,Jst,Jstu]  // <==============
                ,Rf= onlinePt( p10(stu)
                     , len=-2*dist(Jst,stu[0]))// x2 to ensure correct angle
                );
                
   //rota2= twistangle( Rrot,Jx, projPt(stu[2],p01(stu)), stu[2]);
   Arrow2( rotAxis2, opt="r=0.05;cl=gold" );
   echo(rota2=rota2, rotAxis2=rotAxis2);
   Rrot2= rotPt( Rrot, rotAxis2, rota2 );
   MarkPts( replace(pqr_rot,2,Rrot2), "ch;pl=[color,blue,transp,0.2];ball=0");
   ArcArrow( [ Rrot, Rrot2,Jst], rad=dist( Rrot,O) );   
  
   //============================================
    echo( str("<b>Is moved onto the st-plane? </b>"
    , let(x=isOnPlane(stu,Rrot2 )) _color(x,x?"green":"red")));
   
    echo( str("<b>Are R's same side? </b>"
    , let(x=isSameSide([stu[2],Rrot2]
                      , replace(stu,2,N(stu)) 
                      )) _color(x,x?"green":"red")));
   
   MarkPts( pqr_rot, "ch;pl=[transp,0.5,color,green];ball=0");
   MarkPt( Rrot, "ball=0;l=[text,Rrot]");
   
   MarkPts(stu, "pl=[transp,0.1];l=STU"); //;pl=[transp,0.1]");
   
   //Arrow2( linePts([pqr[1],pqr0[1]], len=-0.2), "trp=0.5;cl=red" );
   //MarkPts(pqr, "ch;l=PQR;pl=[transp,0.1]");
     

}  
//move_dev2_1();

module move_dev5(){
   echom("move_dev5");
   echo(_blue("Like move_dev2_1, this mod moves pqr to ANY-plane,stu. With display of transition steps skipped "));

   pqr = [for(p=pqr(r=3,p=4)) p+ [-3,-3,3]];
   stu = pqr(r=5,p=8);   

   pqr = [[-3.5475, -5.27314, 4.54913], [-3.80132, -2.63203, 1.55578], [-2.64029, 0.0900704, 2.04789]];
   stu = [[-2.59527, -0.723103, -5.27971], [0.459888, 2.32008, 1.45862], [1.81857, -1.8535, -0.936208]];

   O = stu[1];
   echo(pqr = pqr );    
   echo(stu= stu );
   
   //============================================
   // Translate pqr to O (pqr0): 
   //
   pqr0 = [for(p=pqr)p-pqr[1]+O];
   //MarkPts(pqr0, "ch;pl=[transp,0.1,color,red];l=PQR");
   
   //============================================
   // Rotate pqr0 to x ==> pqr_rot
   // 
   rotpl = [ pqr0[0],O,stu[0] ];
   N = N( rotpl );
   //Arrow2( rotAxis, opt="cl=gold" );
   //Mark90( [X,O,N] );
   //Mark90( [pqr0[0],O,N] );
   
   rotAxis = [ O, N];// <==============
   rota = -angle(rotpl, Rf=N);// <==============

   //echo( rotpt = rotPt( pqr0[0], rotAxis, 
   pqr_rot = rotPts( pqr0, axis=rotAxis, a = rota);   
   echo( rota = rota, rotAxis=rotAxis, pqr_rot=pqr_rot );
//   ArcArrow( [ pqr0[0]
//             , onlinePt( p10(pqr_rot), dist(pqr_rot))
//             , O //pqr_rot[2]
//             ]
//            , rad= dist(pqr0), opt="cl=green") ;
   
   //============================================
   // Rotate pqr_rot to obj
   // 
   Rrot = pqr_rot[2];
   Jst = projPt( Rrot, p01(stu)); //[[1,0,0],O] );
   //Jpl = projPt( Rrot, stu); //[[1,0,0],O,[0,1,0]] );
   Jstu= anglePt( [ stu[0],Jst, stu[2]], a=90, len = dist( Rrot, Jst));
   
   //MarkPt(Jst,"l=Jst");
   //Mark90( [ Rrot,Jpl,Jx], "ch");
   //rotAxis2= [ Jx,O]; // <=================             
   rotAxis2 = p10(stu);
   
   rota2 = angle( [Rrot,Jst,Jstu]  // <==============
                ,Rf= onlinePt( p10(stu)
                     , len=-2*dist(Jst,stu[0]))// x2 to ensure correct angle
                );
                
   //rota2= twistangle( Rrot,Jx, projPt(stu[2],p01(stu)), stu[2]);
   //Arrow2( rotAxis2, opt="r=0.05;cl=gold" );
   echo(rota2=rota2, rotAxis2=rotAxis2);
   Rrot2= rotPt( Rrot, rotAxis2, rota2 );
   result = replace(pqr_rot,2,Rrot2);
   MarkPts( result, "ch;pl=[color,blue,transp,0.2];ball=0");
   //ArcArrow( [ Rrot, Rrot2,Jst], rad=dist( Rrot,O) );   
  
   //============================================
    echo( str("<b>Is moved onto the st-plane? </b>"
    , let(x=isOnPlane(stu,Rrot2 )) _color(x,x?"green":"red")));
   
    echo( str("<b>Are R's same side? </b>"
    , let(x=isSameSide([stu[2],Rrot2]
                      , replace(stu,2,N(stu)) 
                      )) _color(x,x?"green":"red")));
   
   //MarkPts( pqr_rot, "ch;pl=[transp,0.5,color,green];ball=0");
   //MarkPt( Rrot, "ball=0;l=[text,Rrot]");
   
   MarkPts(stu, "pl=[transp,0.1];l=STU"); //;pl=[transp,0.1]");
   
   //Arrow2( linePts([pqr[1],pqr0[1]], len=-0.2), "trp=0.5;cl=red" );
   MarkPts(pqr, "ch;l=PQR;pl=[transp,0.1]");
     

}  
//move_dev5();

module move_dev6(){
   echom("move_dev6");
   echo(_blue("Move pqr to any stu and move back "));

   pqr = [for(p=pqr(r=3,p=4)) p+ [-3,-3,3]];
   stu = pqr(r=5,p=8);   

   pqr = [[-3.5475, -5.27314, 4.54913], [-3.80132, -2.63203, 1.55578], [-2.64029, 0.0900704, 2.04789]];
   stu = [[-2.59527, -0.723103, -5.27971], [0.459888, 2.32008, 1.45862], [1.81857, -1.8535, -0.936208]];

   O = stu[1];
   echo(pqr = pqr );    
   echo(stu= stu );
   
   //============================================
   // Translate pqr to O (pqr0): 
   //
   pqr0 = [for(p=pqr)p-pqr[1]+O];
   //MarkPts(pqr0, "ch;pl=[transp,0.1,color,red];l=PQR");
   
   //============================================
   // Rotate pqr0 to x ==> pqr_rot
   // 
   rotpl = [ pqr0[0],O,stu[0] ];
   N = N( rotpl );
   //Arrow2( rotAxis, opt="cl=gold" );
   //Mark90( [X,O,N] );
   //Mark90( [pqr0[0],O,N] );
   
   rotAxis = [ O, N];// <==============
   rota = -angle(rotpl, Rf=N);// <==============

   //echo( rotpt = rotPt( pqr0[0], rotAxis, 
   pqr_rot = rotPts( pqr0, axis=rotAxis, a = rota);   
   echo( rota = rota, rotAxis=rotAxis, pqr_rot=pqr_rot );
//   ArcArrow( [ pqr0[0]
//             , onlinePt( p10(pqr_rot), dist(pqr_rot))
//             , O //pqr_rot[2]
//             ]
//            , rad= dist(pqr0), opt="cl=green") ;
   
   //============================================
   // Rotate pqr_rot to obj
   // 
   Rrot = pqr_rot[2];
   Jst = projPt( Rrot, p01(stu)); //[[1,0,0],O] );
   //Jpl = projPt( Rrot, stu); //[[1,0,0],O,[0,1,0]] );
   Jstu= anglePt( [ stu[0],Jst, stu[2]], a=90, len = dist( Rrot, Jst));
   
   //MarkPt(Jst,"l=Jst");
   //Mark90( [ Rrot,Jpl,Jx], "ch");
   //rotAxis2= [ Jx,O]; // <=================             
   rotAxis2 = p10(stu);
   
   rota2 = angle( [Rrot,Jst,Jstu]  // <==============
                ,Rf= onlinePt( p10(stu)
                     , len=-2*dist(Jst,stu[0]))// x2 to ensure correct angle
                );
                
   //rota2= twistangle( Rrot,Jx, projPt(stu[2],p01(stu)), stu[2]);
   //Arrow2( rotAxis2, opt="r=0.05;cl=gold" );
   echo(rota2=rota2, rotAxis2=rotAxis2);
   Rrot2= rotPt( Rrot, rotAxis2, rota2 );
   pqr_rot2 = replace(pqr_rot,2,Rrot2);
   MarkPts( pqr_rot2, "ch;pl=[color,blue,transp,0.2];ball=0");
   //ArcArrow( [ Rrot, Rrot2,Jst], rad=dist( Rrot,O) );   
  
   //============================================
    echo( str("<b>Is moved onto the st-plane? </b>"
    , let(x=isOnPlane(stu,Rrot2 )) _color(x,x?"green":"red")));
   
    echo( str("<b>Are R's same side? </b>"
    , let(x=isSameSide([stu[2],Rrot2]
                      , replace(stu,2,N(stu)) 
                      )) _color(x,x?"green":"red")));
   
   //MarkPts( pqr_rot, "ch;pl=[transp,0.5,color,green];ball=0");
   //MarkPt( Rrot, "ball=0;l=[text,Rrot]");
   
   MarkPts(stu, "pl=[transp,0.1];l=STU"); //;pl=[transp,0.1]");
   
   //Arrow2( linePts([pqr[1],pqr0[1]], len=-0.2), "trp=0.5;cl=red" );
   MarkPts(pqr, "ch;l=PQR;pl=[transp,0.1]");
     
   //============================================
   // MOVING BACK
   //============================================
   echo( "MOVING BACK: ");
   pqr_rot_b = rotPts( pqr_rot2, rotAxis2, -rota2);
   pqr0_b = rotPts( pqr_rot_b, rotAxis, -rota);
   pqr_b = [ for(p=pqr0_b) p+pqr[1]-pqr0_b[1]];
   echo( pqr_b = pqr_b );    
   MarkPts( pqr_b, "ch=[r,0.08,transp,1];r=0.1;trp=0.3");    
   echo( str("<b>Is P back ? </b>", let(x=isequal( pqr[0], pqr_b[0])) _color(x,x?"green":"red")));
   echo( str("<b>Is Q back ? </b>", let(x=isequal( pqr[1], pqr_b[1])) _color(x,x?"green":"red")));
   echo( str("<b>Is R back ? </b>", let(x=isequal( pqr[2], pqr_b[2])) _color(x,x?"green":"red")));
      
   
}  
//move_dev6();

module getMoveData_test_poly(){
    echom("getMoveData_test_poly");
    
    d=2; // d: how far two sets of poly are apart
    
    pqr= [for(p=randPts(4, p=4,r=3)) p+[-d,-d,-d] ];
    stu= [for(p=pqr(p=6,r=5)) p+[d,d,d] ];
    
    //pqr = [[1,0,0],[0,0,0],[0,1,0]];
    //stu = [[3, 2.6, 2], [1.5, 2.6, 2], [1.25279, 3.49443, 2]];
    
    mvdata = getMoveData( pqr, stu );
//    echo(mvdata=mvdata);
//    
//    Plane( [[2.5, 2.6, 2], [1.5, 2.6, 2], [3, 2.6, 2]] );
//    MarkPts( [[2.5, 2.6, 2], [1.5, 2.6, 2], [3, 2.6, 2]] 
//           , "pl;ch");
//    Plane( [[2.5, 2.6, 2], [1.5, 2.6, 2], [1.5, 3.6, 2]] );
//    //color("red")
    //Plane(  [[1.5, 3.6, 2], [1.5, 2.6, 2], [0.5, 2.6, 2]] );
    
    got1 = [for(p=pqr)p+stu[1]-pqr[1]];
        
    got2= rotPts( got1
                , hash(mvdata, "rotaxis")
                , hash(mvdata, "rota")
                ); 

    got3= rotPts( got2
                , hash(mvdata, "rotaxis2")
                , hash(mvdata, "rota2")
                ); 
    
    //MarkPts(got2, "ch;pl=[color,red, transp,0.2];l=PQR" );
    MarkPts(got3, "ch=[r,0.05,color,green];pl=[color,green, transp,0.2];l=PQR" );
    MarkPts(pqr, "ch=[r,0.05];pl=[transp,0.2];l=PQR" );
    MarkPts(stu, "ch=[color,red];l=STU" );
    
    module testmsg( label, TF, comment )
    {
       echo( str("<b>",label, " </b>"
        , _color(TF,TF?"green":"red")
        , comment? _green( str(" // ", comment)):"")
        );
    }
    
    testmsg( "P on st? ", iscoline( p01(stu), got3[0]));
    testmsg( "Q == st[1] ? ", isequal( got3[1], stu[1]));
    testmsg( "R and U same side ? ", 
               isSameSide( [got3[2], stu[2]]
                         , replace(stu, 2, N(stu)) ) );
    testmsg( "angle_P kept ? "
           , isequal( angle(p102(pqr)), angle(p102(got3))), angle(p102(got3)) );
    testmsg( "angle_Q kept ? "
           , isequal( angle(pqr), angle(got3)), angle(got3) );
    testmsg( "angle_R kept ? "
           , isequal( angle(p120(pqr)), angle(p120(got3))), angle(p120(got3)) );
           
}    
//getMoveData_test_poly();

module getMoveData_demo_obj(){
    echom("getMoveData_demo_obj");

   //===================================  
   O = ORIGIN;
   x=5; y=2; z=1; fillet=0.05;
   X = [x,0,0];
   Y = [0,y,0];
   Z = [0,0,z];
   MarkPts( [X,Y], "l=XY");
    
    module obj(color){
        
       union(){
         color("red",0.4) cube([x,y,fillet]); // fillet     
         translate( [0,0,fillet]) 
           color(color, 0.3)
           cube([x,y,z-fillet]);    
       } 
    }
   //===================================  
   
    d=4; // d: how far two sets of poly are apart
    
    pqr= [X,O,Y]; //[for(p=randPts(5, p=4,r=3)) p+[-d,-d,-d] ];
    stu= [for(p=pqr(p=8,r=6)) p+[d,d,d] ];
    
    mvdata = getMoveData( pqr, stu );
    
    
//   multmatrix( m= rotM( hash(mvdata, "rotaxis")
//                      , hash(mvdata, "rota")
//                      )
//                      ){ 
//       obj("red");
//   }
   
   translate( stu[1] )
   multmatrix( m= rotM( hash(mvdata, "rotaxis2")
                      , hash(mvdata, "rota2")
                      )
             ){ 
       multmatrix( m= rotM( hash(mvdata, "rotaxis")
                      , hash(mvdata, "rota")
                      )
                 ){ 
                    obj("green");
   }
   }
 
    MarkPts(stu, "ch=[color,red];pl;l=STU" );

    obj();        
}    
//getMoveData_demo_obj();

module getMoveData_demo_obj_text(){
    echom("getMoveData_demo_obj_text");
   ColorAxes(["len",8]);
   //===================================  
   O = ORIGIN;
   x=5; y=2; z=1; fillet=0.05;
   X = [x,0,0];
   Y = [0,y,0];
   Z = [0,0,z];
   //MarkPts( [X,Y], "l=XY");
    
    module obj(color){
        
       union(){
         //color("red",0.4) cube([x,y,fillet]); // fillet     
         translate( [0,0,fillet]) 
           scale(0.08)
           color(color)
           text("getMoveData"); //cube([x,y,z-fillet]);    
       } 
    }
   //===================================  
   
    d=3; // d: how far two sets of poly are apart
    
    pqr= [X,O,Y]; //[for(p=randPts(5, p=4,r=3)) p+[-d,-d,-d] ];
    stu= [for(p=pqr(p=8,r=6)) p+[d,d,d] ];
    
    mvdata = getMoveData( pqr, stu );
    
    
//   multmatrix( m= rotM( hash(mvdata, "rotaxis")
//                      , hash(mvdata, "rota")
//                      )
//                      ){ 
//       obj("red");
//   }
   
   translate( stu[1] )
   multmatrix( m= rotM( hash(mvdata, "rotaxis2")
                      , hash(mvdata, "rota2")
                      )
             ){ 
       multmatrix( m= rotM( hash(mvdata, "rotaxis")
                      , hash(mvdata, "rota")
                      )
                 ){ 
                    obj("green");
   }
   }
 
    MarkPts(stu, "ch=[color,red];pl=[transp,0.2];l=STU" );

    obj();        
}    
//getMoveData_demo_obj_text();

module Move_dev_180531()
{
  echom("Move_dev_180531");
  echo(_blue("The previous approach seems incorrect in some cases."));
  echo(_blue("Using a new approach by move to ORIGIN and use rotObj"));
  
  O = ORIGIN;
  
  pqr0 = pqr(d=4);
  //echopts(pqr0);
  //MarkPts(pqr0,  r=0.03, Label=["text","PQR","scale",2,"color","black"]);
  //MarkCenter(pqr0, "pqr0", color="purple");
  //Plane(pqr0);
  
  to = pqr(d=4);
  //echopts(to);
  //MarkPts(to,  r=0.03, Line=["r",0.1], Label=["text","PQR","scale",2,"color","black"]);
  MarkCenter(to,"to", color="purple");
  //Plane(to, color="gray");
  
  pqr1= [for(p=pqr0) p+ to[1]-pqr0[1] ];
  MarkPts(pqr1,  r=0.03, Label=["text","PQR","scale",2,"color","black"]);
  MarkCenter(pqr1, "pqr1", color="purple");
  Plane(pqr1, color="red");
  //Arrow( [ pqr0[1], to[1] ] );
  
  
  Npqr1= normalPt( pqr1,len=4);
  Nto = normalPt( to,len=4 );
  echo(Npqr1=Npqr1);
  echo(Nto=Nto);
  //Arrow( [pqr1[1],Npqr1], color="red", r=0.05 );
  //Arrow( [to[1],Nto], color="gray", r=0.05 );
  
  Naxis= angleBisectPt( [pqr1[0], pqr1[1], to[0]], len=6);
  rotAxis = [to[1], Naxis ];
  echo(rotAxis = rotAxis); 
  Arrow( rotAxis, color="blue", r=0.05 );
  
  expected = [ onlinePt( [to[1],to[0]], len= dist( pqr1[0],pqr1[1]) )
             , to[1]
             , anglePt( to, angle(pqr0), len= dist( pqr1[2],pqr1[1]) )
             ];
  MarkCenter(expected, "expected", color="purple");
  Plane(expected, color="green");
  
  //Arrow( [pqr[0],expected[0]],color="red" );
  //Arrow( [pqr[2],expected[2]],color="red" );
               
  final = rotPts( pqr1, axis= rotAxis
                , a= angle([pqr1[0], pqr1[1], to[0]]));
  MarkPts(final,  r=0.03, Label=["text","pqr","scale",2,"color","purple"]);
  
  
  

}
//Move_dev_180531();