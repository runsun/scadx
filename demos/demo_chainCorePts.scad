include <../scadx.scad>

module demo_chainCorePts()
{

  echom("demo_chainCorePts");
  seed=pqr();
  
//  ch2=chainBoneData(n=5,seed=seed);
//  echo(ch2 = _fmth(ch2));
//  Red() MarkPts(h(ch2,"pts" ));


  chpts= chainCorePts(seed=seed);
  echo(chpts = chpts);
  MarkPts(chpts, Line=["r",0.05, "color",["gold",0.5]]);
  
}
demo_chainCorePts();
