include <../scadx.scad>

module PolyCylinder_demo_default()
{
   echom("PolyCylinder_demo_default");
   cylinder();
   translate([2.5,0,0]) PolyCylinder();
   translate([5,0,0]) PolyCylinder(h=2, markPts="l");
}
PolyCylinder_demo_default();


module PolyCylinder_demo_r()
{
   echom("PolyCylinder_demo_r");
   echo(_color( "r=0.2", "darkkhaki"));
   PolyCylinder( r=0.2 );
  
   echo(_red( "r1=0.2" ));
   PolyCylinder($fn=8, r1=0.2, actions=["x", 2] , color=["red",0.9] );
  
   echo( _green("r1=0, r2=1"));
   PolyCylinder( $fn=5, r1=0, r2=1, actions=["x", 5], color=["green",0.9], markPts="l" );
  
   echo( _blue("r1=1, r2=0"));
   PolyCylinder( $fn=4, r1=1, r2=0, actions=["x", 8], color=["blue",0.9], markPts="l" );
  
}
//PolyCylinder_demo_r();

module PolyCylinder_demo_center()
{
   echom("PolyCylinder_demo_center");
   
   echo(_red( "r1=0.2" ));
   PolyCylinder(r1=0.2, color="red", center=true);
  
   echo( _green("r1=1, r2=0"));
   PolyCylinder( r1=1, r2=0, actions=["x", 2], color="green", opt=["center",true] );
  
}
//PolyCylinder_demo_center();

module PolyCylinder_demo_partial_center()
{
   echom("PolyCylinder_demo_partial_center");
   
   echo(_red( "r1=0.2" ));
   PolyCylinder(color=["red",0.3], center=true);
  
   echo( _green("r1=1, r2=0"));
   PolyCylinder( color=["green",0.5], opt=["center",[1,0,0]] );
  
   echo( _green("r1=1, r2=0"));
   PolyCylinder( color=["blue",0.3], opt=["center",[0,0,1]] );
  
}
//PolyCylinder_demo_partial_center();


module PolyCylinder_demo_Frame()
{
   echo(_b("PolyCylinder_demo_Frame()"));  
      
   //echo(_color("Default: [\"r\", 0.01]", "orange"));
   PolyCylinder();
   
//   echo(_b("Frame set by simple arg"));
//   echo(_color("frame=0.1", "orange"));
   PolyCylinder(frame=0.05,opt=["actions",["z",-2]]);
   
   PolyCylinder(frame=["r", 0.02, "color", "red" ],actions=["z",-4]);
   
   //_echo(_color("frame=['color','red']", "orange"));
//   
//   echo(_b("Frames are set by opt.frame"));
//   
//   _echo(_color("['frame', false]", "red"));
    PolyCylinder($fn=32, opt=["actions",["x",2.5], "frame",false, "color",["green",0.8]]);
//   
//   _echo(_color("['frame', 0.05]", "green"));
    PolyCylinder($fn=32, opt=["actions",["x",2.5,"z",-2], "frame",["r",0.02,"grid",false], "color",["blue",0.8]]);
//   
//   _echo(_color("['frame', ['color','red']]", "blue"));
//   PolyCylinder([1,3,2], opt=["actions",["x",6], "frame",["color","red"], "color",["blue",0.5]]);
//   
//   echo(_b("Frames set by opt.frame, but denied by simple arg "));
//   _echo(_color("PolyCylinder(...frame=false, opt=['frame', ['color','red']])", "purple"));
   PolyCylinder($fn=32, frame=false, opt=["actions",["x",2.5, "z",-4], "frame",["color","red"], "color",["purple",0.8]]);
  
}
//PolyCylinder_demo_Frame();


module PolyCylinder_demo_actions()
{
   echo(_b("PolyCylinder_demo_actions()"));  
      
   echo(_darkorange("PolyCylinder([3,2,1]"));   
   PolyCylinder( opt=["color",["gold", 0.6]]);
   
   _echo(_red("opt=['actions', ['rotx', -90, 'x', 3]]"));
   PolyCylinder( opt=[ "actions", ["rotx", -90, "x", 3], "color", ["red",0.3]]);
   
   _echo(_darkgreen("center=true, opt=['actions', ['rotx', -90, 'z', -2]]"), "   // actions with center");
   PolyCylinder( center=true, opt=[ "actions", ["rotx", -90, "z", -2], "color", ["green",0.3]]);
   
   _echo(_blue("opt=['actions', ['roty', -45, 'z',2]]"), " // multiple actions");
   PolyCylinder( opt=[ "actions", ["roty", -45,"z",2], "color", ["blue",0.3]]);
   
   _echo(_purple("action=['y',3], opt=['actions', ['roty', -45, 'z',2]]"), "// actions expanded to opt.actions");
   PolyCylinder( actions=["y", 3], opt=[ "actions", ["roty", -45,"z",2], "color", ["purple",0.3]]);
   
   _echo(_olive("actions=false, opt=['actions', ['roty', -45, 'z',2]]"), " // actions=false disable it");
   PolyCylinder(h=1.5,r=0.5,actions=false, opt=[ "actions", ["roty", -45,"z",2], "color", ["olive",0.8]]);
   
}
//PolyCylinder_demo_actions();

//====================================================
//. With site

module PolyCylinder_demo_with_site()
{
   echo(_b("PolyCylinder_demo_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");
   PolyCylinder(site=pqr); //opt=["site",pqr, "markPts","l", "color",["gold",0.5]]);
   
   pqr2= pqr(); //randPts(count=3, d=[4,5]);
   MarkPts(pqr2,"l=PQR;ch");
   PolyCylinder(site=pqr2); //opt=["site",pqr2, "markPts","l", "color",["red",0.3]]);
   
}
//PolyCylinder_demo_with_site();

module PolyCylinder_demo_center_with_site()
{
   echo(_b("PolyCylinder_demo_center_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");  
   PolyCylinder( center=true, opt=["site",pqr, "color",["gold",0.5]]);
   
}
//PolyCylinder_demo_center_with_site();

module PolyCylinder_demo_partial_center_with_site()
{
   echo(_b("PolyCylinder_demo_partial_center_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");  
   PolyCylinder(center=true, opt=["site",pqr, "color",["gold",0.5]]);

   //pqr2=pqr();
   //color("red") //MarkPts(pqr2,"l=PQR;ch");  
   PolyCylinder(center=[1,0,0]
       , opt=["center",false, "site",pqr, "color",["red",0.3]]);

   PolyCylinder( h=2, center=[0,0,0], opt=["site",pqr, "color",["red",0.5]]);
     
     
}
//PolyCylinder_demo_partial_center_with_site();


module PolyCylinder_demo_actions_with_site()
{
   echo(_b("PolyCylinder_demo_actions_with_site()"));  
   size=[3,2,1];
   
   //pqr=pqr();
   //echo(pqr=pqr);
   
   pqr= [[1.32238, -2.26811, -2.29973], [-1.64044, -0.640784, 1.87238]
        , [2.1467, 0.717838, -0.00367095]];
   
   MarkPts(pqr,"l=PQR;ch");
   
   PolyCylinder(r=0.75, opt=["site",pqr]);
   
   PolyCylinder(r=0.75, opt=["actions", ["x",2],"site",pqr,"color",["red",0.5]]);
   
   PolyCylinder(r=0.75, opt=["actions",["roty",-45],"site",pqr,"color",["green",0.5]]);
  
   PolyCylinder(r=0.75, opt=["actions",["rotz",90, "rotx",-90],"site",pqr,"color",["blue",0.5]]);

   PolyCylinder(r=0.75, actions=["y",-1.5], opt=["actions",["rotz",90, "rotx",-90],"site",pqr,"color",["purple",0.5]]);
   
}
//PolyCylinder_demo_actions_with_site();

//. dim

//module PolyCylinder_demo_dim()
//{
//   echo(_b("PolyCylinder_demo_dim()"));  
//   PolyCylinder([2,3,1], dim=[4,5,0], markPts="l");
//   
//   PolyCylinder([2,3,1], actions=["y", 4], dim=[[7,0,6]], markPts="l", color=["red",.5]);
//   PolyCylinder([2,3,1], site=randPts(3, d=[-1,-2]), dim=[0,1,3], markPts="l", color=["green",.5]);
//   
//   PolyCylinder([2,3,1], actions=["x", 4], dim=[6, [5,1.5,1], 2], markPts="l", color=["purple",.5]);
//   
//   echo(_olive("PolyCylinder(...dim=[4,5,0]...)"));
//   echo(_red("PolyCylinder(...dim=[[7,0,6]]...) // Enter 3 indices together"));
//   echo(_green("PolyCylinder(...site=pts, dim=[0,1,3]...) // Works with site, too"));
//   echo(_purple("PolyCylinder(...dim=[ 6, [5,1.5,1], 2 ] ...) // Enter the site of any pt"));
//   
//}
////PolyCylinder_demo_dim();
//
//module PolyCylinder_demo_dim2()
//{
//   echo(_b("PolyCylinder_demo_dim2()"));  
//   //PolyCylinder([6,4,2], dim=[[4,5,0]], markPts="l");
//   
//   size=[4,2,1];
//   PolyCylinder(size, dim=[[6,7,3],[7,3,1],[7,0,1]],  markPts="l");
//   
//   PolyCylinder(size, actions=["y", 4], dim= [ "pqrs", [[7,6,1]]
//                                     , "color", "green" ]
//        , markPts="l", color=["red",.2]
//        );
//        
//   PolyCylinder(size, actions=["x", 6], dim=["pqrs",[[6,7,3],[7,3,1],[7,0,1]]
//                                     ,"color","red"]
//        , markPts="l", color=["green",.2]
//        );
//        
//   PolyCylinder(size, actions=["y", -5], dim=[ "pqrs",[["pqr",[6,7,3], "color","green"]
//                                              ,[7,3,1]
//                                              ,[7,0,1]
//                                              ]
//                                     ,"color","purple"]
//        , markPts="l", color=["blue",.2]
//        );
// 
//}
////PolyCylinder_demo_dim2();



//

//module PolyCylinder_demo_sphere()
//{
//   echo(_color( "sphere=false", "darkkhaki"));
//   PolyCylinder( randPts(2), sphere=false );
//  
//   echo(_red( "sphere= 0.15" ));
//   color("red") PolyCylinder( randPts(2), sphere=0.15 );
//  
//   echo( _green("sphere= =[0.05,0.2]"));
//   color("green") PolyCylinder( randPts(2), sphere=[0.05,0.2] );
//  
//   echo( _blue("sphere=[undef,0.15]"));
//   color("blue") PolyCylinder( randPts(2), sphere=[undef,0.15] );
//  
//   echo(_color( "sphere following different r's", "purple"));
//   color("purple") PolyCylinder( randPts(2), r2=0.2);
//
//   echo(_color( "Cone: r1=0, r2=0.3, sphere=false", "teal"));
//   color("teal") PolyCylinder( randPts(2), r1=0, r2=0.5, sphere=false);  
//}
//PolyCylinder_demo_sphere();
//
//module PolyCylinder_demo_markpts()
//{
//   echo(_color( "markpts=true", "darkkhaki"));
//   PolyCylinder( randPts(2), markpts=true );
//  
//   echo(_red( "markpts=\"l\"" ), _green(" //l = label"));
//   color("red") PolyCylinder( randPts(2), markpts="l" );
//  
//   echo( _green( "markpts=\"l=AB\""));
//   color("green") PolyCylinder( randPts(2), markpts="l=AB" );
//
//   echo( "markpts= [\"r\",0.3]");
//   PolyCylinder( randPts(2), sphere=false, markpts=["r",0.3] ); 
//}
//PolyCylinder_demo_markpts();
