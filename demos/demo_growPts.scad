
include <../scadx.scad>

//. All modules
//demo_growPts();
//demo_growPts_t();
//demo_growPts_a();
//demo_growPts_NBL();
//demo_growPts_PR();
//demo_growPts_arc();
//demo_growPts_arc2();
//demo_growPts_hole();
//demo_growPts_hole2();
demo_growPts_repeat();

//. Main
module demo_growPts()
{
   echom("demo_growPts()");
   steps= ["x",1,"y",1,"z",2];
   
   pt = randPt();
   MarkPt(pt, "pt");
   pts1= growPts(pt, steps);
   MarkPts(pts1, Label=["text",["","x,1","y,1","z,2"], "color","red"]
          , Line=["r",0.05, "color",["red",0.3]]);

   
   pqr=pqr();
   MarkPts(pqr, "PQR", Line=["color","black"]);
   pts2= growPts(pqr, steps);
   MarkPts(pts2, Label=["text",["","","","x,1","y,1","z,2"], "color","green"]
          , Line=["r",0.05, "color",["green",0.2]]);
}


module demo_growPts_t()
{
   echom("demo_growPts_t()");
   steps= ["t",[1,2,0]];
   
   pt = randPt();
   MarkPt(pt, "pt");
   pts1= growPts(pt, steps);
   MarkPts(pts1, Label=["text",["","t,[1,2,0]"], "color","red"]
          , Line=["r",0.05, "color",["red",0.3]]);

   
   pqr=pqr();
   MarkPts(pqr, "PQR", Line=["color","black"]);
   pts2= growPts(pqr, steps);
   MarkPts(pts2, Label=["text",["","","","t,[1,2,0]"], "color","green"]
          , Line=["r",0.05, "color",["green",0.2]]);
}


module demo_growPts_a()
{
   echom("demo_growPts_a()");
     
   pqr=pqr();
   //pqr= [[-0.1, -1.09, -0.18], [-0.76, -0.55, 0.45], [1.36, -1.37, -1.91]];
   //pqr = [[-0.84, 1.61, 1.87], [-1.31, 1.18, -0.6], [-1.36, -0.49, -1.01]];
   echo(pqr=pqr);
   MarkPts(pqr, "PQR", Line=["color","red"]);
   
   data=[ ["a",45] 
        , ["a",90,"a2",45]
        , ["a",90, "len",2]
        , ["a",45,"a2",60, "len",2]
        ]; 
   
   for( i = range(data) )
   {
      d = data[i];
      pts = growPts(pqr, ["a",d]);
      
      color(COLORS[i], 0.3)
      MarkPts(pts, Label=["text",["","","",join(d,",")]] 
          , Line=["r",0.05]); 
      
   }
   
   
   pqr2= y(pqr(),3);
   data2= joinarr([ for(args= data) joinarr([["a"],[args]])] );
   pts2= growPts( pqr2, data2);
   echo(pts2= pts2, data2=data2);
   MarkPts(pqr2, "PQR", Line=["color","black"]);
   label = concat([""],[for(d=hashkvs(data2)) str(d)]);
   echo(label=label);
   color("green",0.5)
   MarkPts(slice(pts2,2), Label=["text",label] 
          , Line=["r",0.05]); 
     
   
//   pts1= growPts(pqr, ["a",["a",45]]);
//   MarkPts(pts1, Label=["text",["","","","a,45"], "color","red"]
//          , Line=["r",0.05, "color",["red",0.2]]);
//   Red() Mark90( get(pts1, [1,2,3]) );
//   
//   pts2= growPts(pqr, ["a",["a",90]]);
//   MarkPts(pts2, Label=["text",["","","","a,90"], "color","green"]
//          , Line=["r",0.05, "color",["green",0.2]]);
//          
//   Mark90( [last(pts2), pts2[2], pts2[0]] ); 
//   
//   //echo(angle( [last(pts1), last(pqr), last(pts2)] ));      
//   //J = projPt(last(pts2), get(pts2,[0,1]) );
//   //DashLine([last(pts2),J,pts2[0]]);
//   //Mark90( [last(pts2),J,pts2[0]] );   
//   
//   pts3= growPts(pqr, ["a",["a",90,"a2",45, "rad",2]]);
//   MarkPts(pts3, Label=["text",["","","","a,90,a2,45,rad,2"], "color","blue"]
//          , Line=["r",0.05, "color",["blue",0.2]]);
//   Blue() Mark90( [last(pts3), pqr[2],pqr[1]]);   
//   
//   pts4= growPts(pqr, ["a",["a",45,"a2",90, "len",2]]);
//   MarkPts(pts4, Label=["text",["","","","a,45,a2,90,len,2"], "color","blue"]
//          , Line=["r",0.05, "color",["blue",0.2]]);
//   //Blue() Mark90( [last(pts3), pqr[2],pqr[1]]);   
}


module demo_growPts_NBL()
{
   echom("demo_growPts_NBL()");
     
   pqr=pqr();
   //pqr= [[-0.1, -1.09, -0.18], [-0.76, -0.55, 0.45], [1.36, -1.37, -1.91]];
   pqr = [[-0.84, 1.61, 1.87], [-1.31, 1.18, -0.6], [-1.36, -0.49, -1.01]];
   echo(pqr=pqr);
   MarkPts(pqr, "PQR", Line=["color","black"], Grid=["mode",1]);
   
   pts1= growPts(pqr, ["N",1,"B",2]);
   MarkPts(pts1, Label=["text",["","","","[\"N\",1] (Normal)","[\"B\",2](Binormal)"], "color","red"]
          , Line=["r",0.05, "color",["red",0.2]]);
   Red() Mark90( get(pts1, [1,2,3]) );
   
   pts2= growPts(pqr, ["B",1]);
   MarkPts(pts2, Label=["text",["","","","[\"B\",1]"], "color","green"]
          , Line=["r",0.05, "color",["green",0.2]]);
   J = projPt(last(pts2), get(pts2,[0,1]) );
   DashLine([last(pts2),J,pts2[0]]);
   Mark90( [last(pts2),J,pts2[0]] );   
   
   pts3= growPts(pqr, ["L",1]);
   MarkPts(pts3, Label=["text",["","","","[\"L\",1](vert.to QR on pqr plane)"], "color","blue"]
          , Line=["r",0.05, "color",["blue",0.2]]);
   Blue() Mark90( [last(pts3), pqr[2],pqr[1]]);   
}

module demo_growPts_PR()
{
   echom("demo_growPts_PR()");
     
   pqr=pqr();
   //pqr= [[-0.1, -1.09, -0.18], [-0.76, -0.55, 0.45], [1.36, -1.37, -1.91]];
   pqr = [[-0.84, 1.61, 1.87], [-1.31, 1.18, -0.6], [-1.36, -0.49, -1.01]];
   echo(pqr=pqr);
   MarkPts(pqr, "PQR", Line=["color","black"]);
   
   pts1= growPts(pqr, ["N",1,"R",1,"P",1, "R",-2]);
   MarkPts(pts1, Label=["text",["","","","N,1","R,1", "P,1", "R,-2"], "color","red"]
          , Line=["r",0.05, "color",["red",0.2]]);
   Red() Mark90( get(pts1, [1,2,3]) );
   
}


module demo_growPts_arc()
{
   echom("demo_growPts_arc()");
     
   pqr=pqr();
   //pqr= [[-0.1, -1.09, -0.18], [-0.76, -0.55, 0.45], [1.36, -1.37, -1.91]];
   pqr = [[-0.84, 1.61, 1.87], [-1.31, 1.18, -0.6], [-1.36, -0.49, -1.01]];
   echo(pqr=pqr);
   Teal() MarkPts(pqr, "PQR", Line=["color","black"], Grid=["r",0.01, "mode",1]);
   
   
   pts1= growPts(pqr, ["arc",["n",6]]);
   MarkPts(pts1, Ball=["color","red"], Label=false
          , Line=["r",0.05, "color",["red",0.2]]);
   Red() MarkPt(last(pts1), "[\"arc\",[\"n\",6]]");       
   
   
   pts2= growPts(pqr, ["arc",["n",4,"rad",1,"a",180]]);
   MarkPts(pts2, Ball=["color","green"], Label=false
          , Line=["r",0.05, "color",["green",0.2]]);
   Green() MarkPt(last(pts2)
           , Label=["text" , "[\"arc\",[\"n\",4,\"rad\",1,\"a\",180]]"
                   , "halign","center"]); 
   
   pts3= growPts(pqr, ["arc",["n",8,"rad",1,"a2",90]]);
   MarkPts(pts3, Ball=["color","blue"], Label=false
          , Line=["r",0.05, "color",["blue",0.2]]);
   Blue() MarkPt(last(pts3)
           , Label=["text" , "[\"arc\",[\"n\",8,\"rad\",1,\"a2\", 90]]"
                   , "halign","center"]); 
                   
   pts4= growPts(pqr, ["arc",["a", 180,"n",8,"rad",0.6], "arc",["n",4,"a2",-90]]);
   echo(len=len(pts4), pts4=pts4);
   echo(0_8 = slice(pts4,0,8));
   //MarkPts( pts4, Ball=["color","blue"], Label=false
   //       , Line=["r",0.05, "color",["blue",0.2]]);
   MarkPts( slice(pts4, 3,11), Ball=["color","red"], Label=false
          , Line=["r",0.05, "color",["red",0.2]]);
   MarkPts( slice(pts4, 10), Ball=["color","purple"], Label=false
          , Line=["r",0.05, "color",["purple",0.2]]);
   Purple()
   MarkPt( last(pts4)
         , "[\"arc\",[\"a\", 180,\"n\",8,\"rad\",0.6], \"arc\",[\"n\",4,\"a2\",-90]]"); 
   //MarkPts( slice(pts4, [0, 8] );       
//   Blue() MarkPt(last(pts4)
//           , Label=["text" , "[\"arc\",[\"n\",8,\"rad\",1,\"a2\", 90]]"
//                   , "halign","center"]); 
//                   
}


module demo_growPts_arc2()
{
   echom("demo_growPts_arc2()");
   _red(_span("Use new pqr in arc", s="font-size:20px"));
     
   pts1= growPts(O, ["y",3, "arc",["n",4, "rad",1]
                    , "x",1.5,"arc",["n",4, "rad",0.5]
                    , "y",-0.5,"arc",["pqr",[[2.5,2.5,0], [2.5,2,0], Y]
                                   , "n",8, "rad",.5, "a",180]
                    , "y",-0.5,"arc",["n",4, "rad",0.5]               
                    ]);
   MarkPts(pts1, Ball=["color","red"], Label=false, Ball=false
          , Line=["r",0.05, "color",["red",0.2]]);

   Blue()
   MarkPt( pts1[6], Link=true, Label=str("arc",["pqr",[[2.5,2.5,0], [2.5,2,0], Y]
                                   , "n",8, "rad",.5, "a",180]));
}


module demo_growPts_hole()
{
   echom("demo_growPts_hole()");
     
   pts1= growPts(O, ["y",3, "arc",["n",4, "rad",0.5]
                    , "x",1.5,"arc",["n",4, "rad",0.5]
                    , "y",-0.5,"arc",["pqr",[[2.5,2.5,0], [2.5,2,0], Y+5*X]
                                   , "n",8, "rad",.5, "a",180]
                    , "y",-0.5,"arc",["pqr", [[2.5,1,0], [2,1,0],Y/2]
                                     , "n",4, "rad",0.5]  
                    , "x",-1, "t", [-0.5,-0.5,0]
                    , "arc", ["pqr", [[0.5,2,0], [1,2,0],3*Y], "a",360, "n",8]              
                    ]);
                    
   MarkPt( last(pts1), Label=["text",true] );                 
   MarkPts(pts1, Ball=["color","red"], Label=false
          , Line=["r",0.05, "color",["red",0.2]]);
}


module demo_growPts_hole2()
{
   echom("demo_growPts_hole()2");
     
   nCircle=8;
     
   pts1= growPts(O, ["y",3, "arc",["n",4, "rad",0.5]
                    , "x",1.5,"arc",["n",4, "rad",0.5]
                    , "y",-0.5,"arc",["pqr",[[2.5,2.5,0], [2.5,2,0], Y+5*X]
                                   , "n",8, "rad",.5, "a",180]
                    , "y",-0.5,"arc",["pqr", [[2.5,1,0], [2,1,0],Y/2]
                                     , "n",4, "rad",0.5]  
                    , "x",-1, "t", [-0.5,-0.5,0]
                    , "arc", ["pqr", [[0.5,2,0], [1,2,0],3*Y], "a",360
                             , "n",nCircle]              
                    ]);
                
   pts2= addz(pts1,1);
    
   pts = concat(pts1,pts2);
   nOuter= len(pts1)-nCircle;
   Red() MarkPts(pts, Ball=false, Line=false, Label=["text",range(pts), "scale",0.5]);
   
   faces = faces(shape="tube2", idx_out= nOuter, idx_in= nCircle);
   Gold(0.8)polyhedron( points=pts, faces=faces); 
         
}
module demo_growPts_repeat()
{
   echom("demo_growPts_repeat() --- new 2020.7.16");
     
   echo(_span(" Use of repeat will fail if the previous steps contains absolute pt", s="color:red;font-size:20px"));
   echo(_span(" like the val of the pt key, or the 'pqr' value of 'arc' key.", s="color:red;font-size:20px"));  
     
   nCircle=8;
     
   pts1= growPts(O, ["y",3, "arc",["n",4, "rad",0.5]  // 0,1
                    , "x",1.5,"arc",["n",4, "rad",0.5] // 2,3
                    , "y",-0.5,"arc",["pqr",[[2.5,2.5,0], [2.5,2,0], Y+5*X]
                                   , "n",8, "rad",.5, "a",180]
                    , "y",-0.5,"arc",["pqr", [[2.5,1,0], [2,1,0],Y/2]
                                     , "n",4, "rad",0.5]  
                    , "x",-1, "t", [-0.5,-0.5,0]
                    , "arc", ["pqr", [[0.5,2,0], [1,2,0],3*Y], "a",360
                             , "n",nCircle] 
                    , "pt",Z, "repeat", [0,5]                      
                    ]);
                
   //pts2= addz(pts1,1);
    
   //pts = concat(pts1,pts2);
   pts= pts1;
   //echo( pts = pts );
   
   MarkPts( slice( pts, 0, 35), Ball=["r",0.03, "samesize",true]    
          , opt=[ "Label", ["scale",.5]] 
          );
   Red(0.3) MarkPts( slice( pts, 35), Line=["r", 0.1]);
   //echo( partial= slice( pts, 34));
   
   nOuter= len(pts1)-nCircle;
   //Red() MarkPts(pts, Ball=false, Line=true, Label=["text",range(pts), "scale",0.5]);
   
   faces = faces(shape="tube2", idx_out= nOuter, idx_in= nCircle);
   //Gold(0.8)polyhedron( points=pts, faces=faces); 
         
}
