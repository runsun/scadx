include <../scadx.scad>

/*
3D plot examples:
https://www.benjoffe.com/code/tools/functions3d/examples
https://www.mathworks.com/help/symbolic/mupad_ref/plot-function3d.html
https://www.mathsisfun.com/data/grapher-3d-function.html
*/

module demo_Plot_sin()
{
   echom("demo_Plot_sin()");
   Plot("sin(180*x)", x=[0:0.1:4], Line=true );   
}
//demo_Plot_sin();


module demo_Plot_cond()
{
   echom("demo_Plot_cond");
   Plot("x>2?sin(180*x):(2-x)", x=[0:0.1:4], Line=true );
}
//demo_Plot_cond();



module demo_Plot_3d()
{
   echom("demo_Plot_3d()");
   Plot("sin(180*x)*y", x=range(0,0.05,2), y=range(1,1,4) );
   //Plot("sin(180*x)*y", x=[0:0.05:2], y=[1:1:3] );
}
//demo_Plot_3d();



module demo_Plot_3d_2()
{
   echom("demo_Plot_3d_2()");
   //x = range(0,0.05,4);
   //Plot("sin(90*x)*y", x=x, y=[1:1:3]);
   Plot("sin(90*x)*y", x=range(0,0.05,4), y=range(1,4));
}
//demo_Plot_3d_2();



module demo_Plot_3d_3()
{
   echom("demo_Plot_3d_3()");
   Plot("(x^2+y^2)/4", x=range(0,.5,4),y=range(-4,.5,4));   
}
//demo_Plot_3d_3();



module demo_Plot_3d_4()
{
   echom("demo_Plot_3d_4()");
   Plot("(x^2-y^2)/4", x=range(-4,1,5),y=range(-4,1,5) );   
}
//demo_Plot_3d_4();



module demo_Plot_3d_5()
{
   echom("demo_Plot_3d_5()");
   Plot("(x>-2&(x<2))?-4:(x^2-y^2)/4", x=range(-4,.5,4.5),y=range(-4,.5,4.5) );   
}
//demo_Plot_3d_5();



module demo_Plot_3d_6()
{
   // #### Expensive Calc ####
   echom("demo_Plot_3d_6()");
   Plot("(x>-2&(x<2)&(y>-2)&(y<2))?4:(x^2+y^2)/4", x=range(-4,.2,4.5),y=range(-4,.2,4.5) );   
}
//demo_Plot_3d_6(); // Expensive (2min 2 sec)



module demo_Plot_3d_tower()
{
   echom("demo_Plot_3d_tower()");
   
   //density=45;
   size = 7;
   resolution= .5;
   vars = [-size/2:resolution:size/2];
   
   Plot( "(x>-2&(x<2)&(y>-2)&(y<2))?4:0" 
       , x=vars, y=vars
       , MarkPts=true
       );   
}
//demo_Plot_3d_tower(); // 13 sec



module demo_Plot_3d_ripple()
{
   // https://www.benjoffe.com/code/tools/functions3d/examples
   echom("demo_Plot_3d_ripple()");
   
   ripple_density=20;
   size = 10;
   resolution= .25; // Takes 5 min if set to 0.1
   vars = range(-size/2, resolution, size/2+resolution);
   
   Plot(_s("sin({_}(x^2+y^2))", ripple_density)
       , x=vars, y=vars
       );
   
}
//demo_Plot_3d_ripple(); // 5 minutes, 10 seconds


module demo_Plot_3d_cone_cup()
{
   echom("demo_Plot_3d_cone_cup()");
   
   cup_height=10;
   size = 10;
   resolution= .5; 
   vars = range(-size/2, resolution, size/2+resolution);
   
   Plot(_s("sqrt({_}(x^2+y^2))", cup_height)
       , x=vars, y=vars
       );
   
}
//demo_Plot_3d_cone_cup(); 



module demo_Plot_3d_cone_cup_w_surface()
{
   echom("demo_Plot_3d_cone_cup_w_surface()");
   
   cup_height=10;
   size = 10;
   resolution= .5; 
   vars = range(-size/2, resolution, size/2+resolution);
   
   Plot(_s("((x<-2.3|(x>2.3))|((y<-2.3)|(y>2.3))?9:sqrt({_}(x^2+y^2))", cup_height)
       , x=vars, y=vars
       );   
}
//demo_Plot_3d_cone_cup_w_surface(); // 36 sec



module demo_Plot_3d_dom()
{
   echom("demo_Plot_3d_dom()");
   
   size = 1;
   resolution= .05;
   vars = [-size/2:resolution:size/2];
   
   Plot("4*sqrt(1-x^2-y^2)-3" 
       , x=vars, y=vars
       );   
}
//demo_Plot_3d_dom(); 



module demo_Plot_3d_wave()
{
   echom("demo_Plot_3d_wave()");
   
   size = 10;
   resolution= .5;
   vars = [-size/2:resolution:size/2];
   
   Plot("cos(x+y)+(x^3)/20-(y^2)/5" 
       , x=vars, y=vars
       );   
}
//demo_Plot_3d_wave(); 



module demo_Plot_3d_wave2()
{
   echom("demo_Plot_3d_wave2()");
   
   size = 2;
   resolution= .1;
   vars = [-size/2:resolution:size/2];
   
   Plot("x*y*exp(-x^2-y^2)" 
       , x=vars, y=vars
       );   
}
//demo_Plot_3d_wave2(); // 24 sec



module demo_Plot_3d_wave3()
{
   echom("demo_Plot_3d_wave3()");
   
   size = 2;
   resolution= .1;
   vars = [-size/2:resolution:size/2];
   
   Plot("cos(180*(abs(x)+abs(y)))" 
       , x=vars, y=vars
       );   
}
//demo_Plot_3d_wave3(); // 18 sec 


//hull_profiling_results();
module hull_profiling_results() // 1919
{
  ns= [20,30,40,50,75,100];
  Rod_renders = [7,13,20,26,68,101];
  Line_renders= [2,4,8,9,25,36];
  ratios = [ for(i=range(Rod_renders))
             Rod_renders[i]/Line_renders[i]
           ]; 
  echo( ratios= ratios, avg = avg(ratios) );
  
  xys1= [ns,Rod_renders];  
  xys2= [ns,Line_renders];  
    
  pts1=zip( xys1[0], xys1[1] ); 
  pts2=zip( xys2[0], xys2[1] ); 
  
  markpts=[ "r", .5
          , "Ball",["r",1]
          , "Label", ["scale", 15, "indent", 2]
          ];
  
  y_scale_down = 0.7;
  
  Red()  MarkPts( [for(p=pts1) [p.x, p.y*y_scale_down,0] ]
                , Label=["text",pts1], opt= markpts );
  Blue() MarkPts( [for(p=pts2) [p.x, p.y*y_scale_down,0] ]
                , Label=["text",pts2, "actions",["t",[-5,-50,0]]], opt= markpts );
    
  textopt= ["halign","center","scale",5, "color","black"];  
  Text("Render Time of hull-based and "
      , site= [ [100,88,0], [60,88,0], [60,120,0] ], opt=textopt
      );  
  Text("Render Time of hull-based and "
      , site= [ [101,88,0], [60.3,88,0], [60,120,0] ], opt=textopt
      );  
  Text("Cylinder-based Line Drawings"
      , site= [ [100,81,0], [60,81,0], [60,120,0] ], opt=textopt
      );  
  Text("Cylinder-based Line Drawings"
      , site= [ [100,81,0], [60.3,81,0], [60,120,0] ], opt=textopt
      );  
  //------------------------
  MarkPt( [16,75,0], Label=["text","[n (# of pts), render time(sec) ]", "scale",20, "color","black"], Ball=false); 
  
  //-------------------------
  
  MarkPt( [55,60,0], Label=["text","hull-based", "scale",20, "color","red"], Ball=false);

  MarkPt( [85,33,0], Label=["text","cylinder-based", "scale",20, "color","blue"], Ball=false); 

  // Conclusion
  MarkPt( [15,-10,0], Label=["text","ratios = [3.5, 3.25, 2.5, 2.89, 2.72, 2.81]", "scale",15, "color","black"], Ball=false); 

  MarkPt( [15,-15,0], Label=["text","avg= 2.94 => Cylinder-based is 3 times faster", "scale",15, "color","black"], Ball=false); 
  
}   
