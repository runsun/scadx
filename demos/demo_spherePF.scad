include <../scadx.scad>

module demo_spherePts()
{
  echom("demo_spherePts()");
  pf = spherePF(r=3, n=3);
  DrawEdges(pf);
  Poly(pf);
  
}
demo_spherePts();
