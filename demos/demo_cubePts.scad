include <../scadx.scad>


//. W/o site

module cubePts_demo_default()
{
   echo(_b("cubePts_demo_default()"));  
   pts = cubePts();
   Frame(pts);
   MarkPts(pts); 
}
//cubePts_demo_default();



module cubePts_demo_cubic()
{
   echo(_b("cubePts_demo_cubic()"));  
   
   pts = cubePts(3);
   Frame(pts);
   MarkPts(pts); 
   
   echo(pts = pts );
   
}
//cubePts_demo_cubic();



module cubePts_demo_center()
{
   echo(_b("cubePts_demo_center()"));  
   
   pts = cubePts(3, center=true);
   Frame(pts); //, ["color", ["blue", 0.2]]);
   MarkPts(pts); 
   //echo(pts = pts );
   
   pts2 = cubePts([2.5,2,1], opt=["center",true]);
   color("red") Frame(pts2);   

}
//cubePts_demo_center();



module cubePts_demo_partial_center()
{
   echo(_b("cubePts_demo_partial_center()"));  
   
   pts = cubePts(3, center=[1,0,0]);
   Frame(pts);
   MarkPts(pts); 
   
   pts2 = cubePts(3, center=[0,1,1]);
   Red() Frame(pts2);
}
//cubePts_demo_partial_center();



module cubePts_demo_actions()
{
   echo(_b("cubePts_demo_actions()"));  
   
   cube(); MarkPts(cubePts());
   
   pts = cubePts(opt=["actions", ["x",2]]);
   Red(.5) Frame(pts);
   MarkPts(pts, Line=false); 
   
   pts2 = cubePts(opt=["actions",["roty",-45]]);
   Green(.7) Frame(pts2);
   MarkPts(pts2, Line=false);
   
   pts3 = cubePts(opt=["actions",["rotz",90, "rotx",-90, "z",-1]]);
   Blue(.5) Frame(pts3);
   MarkPts(pts3, Line=false);
   
}
//cubePts_demo_actions();



module cubePts_demo_extended_actions()
{
   echo(_b("cubePts_demo_extended_actions()"));  
   echo( ">>> cubePts(actions= a2, opt=[\"actions\",a1] )");
   echo( "==> final actions = a1+a2 " );
   
   cube();
   
   pts = cubePts(opt=["actions", ["x",2]]);
   Red() Frame(pts);
   MarkPts(pts, Line=false); 
   
   pts2 = cubePts(actions=["roty", -45], opt=["actions", ["x",2]]);
   Green(0.7) Frame(pts2);
   MarkPts(pts2, Line=false);
   
}
//cubePts_demo_extended_actions();

//====================================================
//. With site

module cubePts_demo_with_site()
{
   echo(_b("cubePts_demo_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"PQR");
   
   pts = cubePts(site=pqr) ; //opt=["site",pqr]);
   Red() Frame(pts);
   //MarkPts(pts); 
   
   pts2= cubePts(size=1.5, site=pqr) ; //opt=["site",pqr]);
   Green() Frame(pts2);
   
   pts3= cubePts(2, opt=["site",pqr]);
   Blue() Frame(pts3);
    
}
//cubePts_demo_with_site();



module cubePts_demo_center_with_site()
{
   echo(_b("cubePts_demo_center_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"PQR");
   
   pts = cubePts([3,2,1], opt=["site",pqr]);
   Frame(pts);
   //ptsdata = ptsHandler(cubeSize=3, opt=["site",pqr] );
   //MarkPts(hash(ptsdata,"pts"),"l"); 
   //echo(ptsdata=_fmth(ptsdata));
   
   pts2 = cubePts([2.5,1.5,0.8], center=true, opt=["site",pqr]);
   Frame(pts2, opt=["color", "red"]);

   //ptsdata2 = ptsHandler(cubeSize=3, center=true, opt=["site",pqr] );
   //echo(ptsdata2=_fmth(ptsdata2));
   //MarkPts(hash(ptsdata2,"pts"),"l"); 
   
}
//cubePts_demo_center_with_site();



module cubePts_demo_partial_center_with_site()
{
   echo(_b("cubePts_demo_partial_center_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");
   
   pts = cubePts(3, center=[1,0,0], opt=["site",pqr]);
   Frame(pts);
   //MarkPts(pts,"l"); 
   
   pts2 = cubePts(3, center=[0,1,1], opt=["site",pqr]);
   Frame(pts2, opt=["color", ["blue", 0.2]]);
   //MarkPts(pts2,"l");
}
//cubePts_demo_partial_center_with_site();



module cubePts_demo_actions_with_site()
{
   echo(_b("cubePts_demo_actions_with_site()"));  
   
   pqr=pqr();
   MarkPts(pqr,"l=PQR;ch");
   
   size=[3,2,1];
   
   Cube(size, opt=["site",pqr]);
   
   pts = cubePts(size, opt=["actions", ["x",2],"site",pqr]);
   Frame(pts, opt=["color",["red",0.5]]);
   MarkPts(pts,"l"); 
   
   pts2 = cubePts(size, opt=["actions",["roty",-45],"site",pqr]);
   Frame(pts2, opt=["color", "green"]);
   MarkPts(pts2,"l");
   
   pts3 = cubePts(size, opt=["actions",["rotz",90, "rotx",-90, "z",-1],"site",pqr]);
   Frame(pts3, opt=["color", ["blue", 0.5]]);
   MarkPts(pts3,"l");
   
}
//cubePts_demo_actions_with_site();
