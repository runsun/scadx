//include <scadx_obj.scad>
include <../scadx.scad>


//. a, b

module angle_demo_basic()
{  echom( "angle_demo_basic" ); 

    P = [2,0,0];
    Q = ORIGIN;
    R = [0,2*sqrt(3),0];
	
    pqr = [P,Q,R];
	MarkPts( pqr, ["label", ["text",["aP=60","aQ=90","aR=30"]]
                  , "chain",["closed",true]] );
	echo("angle( pqr )=", angle([P,Q,R]));
	echo("angle( rpq )=", angle([R,P,Q]));
    echo("angle( prq )=", angle([P,R,Q]));
}
//angle_demo_basic();

module angle_demo_refPt()
{
    
   pqr = randPts(3);
   N = normalPt(pqr); 
   pqrn = app(pqr, N);
    
   MarkPts( pqrn ); 
   LabelPts( pqrn , "PQRN");
   Plane(pqr, ["mode", 3] ); Chain(pqr, ["closed", false]);
   Line( [N,Q(pqr)]);
    
   // 
   //   Check the sign of angle at different ref pts  
   //
   refs = randPts(5);
   
   angles = [ for(ref=refs) //[isSameSide( [ref,N], pqr),
                          angle(pqr, refPt=ref)
            ];
       
   MarkPts( refs );
   LabelPts( refs, angles );
 
}
//angle_demo_refPt();



module angleBisectPt_demo_1()
{
	echom( "angleBisectPt_demo_1" );
	pqr = randPts(3);
	
	p=pqr[0];
	q=pqr[1];
	r=pqr[2];

	d = angleBisectPt(pqr);

	s=_s("<br/>|>--------&lt;  angleBisectPt_demo_1  >--------
     <br/>|> Get 3 random pts: 
     <br/>|> pqr={_}
     <br/>|> d = angleBisectPt(pqr)= {_}
     <br/>|> Check if two angles divided by QD are equal:
     <br/>|> angle( [p,q,d] ) = {_}
     <br/>|> angle( [r,q,d] ) = {_}
     <br/>|>"
      ,[ _fmt(pqr)
	  , _fmt(angleBisectPt(pqr))
       , _fmt( angle([p,q,d]) )
       , _fmt( angle([r,q,d]) )
       ]
    );
 
	echo(s);

	/*echo_("pqr= {_}", [_fmt(pqr)]);
	echo( "angleBisectPt(pqr)= d =  ", _fmt(angleBisectPt(pqr)  ));	
	echo( "angle([p,q,d]) = ", _fmt( angle([p,q,d]) ));
	echo( "angle([r,q,d]) = ", _fmt( angle([r,q,d]) ));
	*/
}

module angleBisectPt_demo_2()
{
	echom("angleBisectPt_demo_2");
    ops= ["r",0.04,"markpts",true];
	
	pqr=[ [0,-1,0 ], [1,2,0 ],[4,0,0 ]];
    pqr = randPts(3);
    Q = pqr[1];
    Chainf( pqr, ops=["closed",true] );
    
    D= angleBisectPt( pqr );
    
    E = angleBisectPt( pqr, ratio=0.5 );
    F = angleBisectPt( pqr, len=5 );

    MarkPts([D,E,F], ["labels","DEF"] );
    Line0( [Q,F], ["r", 0.01] ); 
    
    Dim2( [Q,F, N([ P(pqr),D,F])] ); 
    
}



module anglePt_demo_basic()
{ echom("anglePt_demo_basic");
    
    pqr = randPts();
    P= P(pqr);
    Q= Q(pqr);
    R= R(pqr);
    
    MarkPts( pqr, ["ch=[r,0.02]", "label","PQR"] );
    
    A30 = anglePt( pqr, 30, len=1.5);
    A60 = anglePt( pqr, 60, len=3);
    
    MarkPts( [A30,Q, A60]
    , ["ch=[color,red]","label",
      ["text",[" A30,len=1.5",""," A60, len=3"]]] );    

}
//anglePt_demo_basic();


module anglePt_demo_circle()
{ echom("anglePt_demo_circle");
    
	pqr = randPts(3);
	nseg = 6;
	MarkPts( pqr,"ch;l=PQR" );
	P = pqr[0];
	Q = pqr[1];
	R = pqr[2];

    rad = min( d01(pqr), d02(pqr),d12(pqr));
    
    pts = [ for(i=[0:nseg-1])
            anglePt( pqr, a= i*360/nseg, len=rad ) ];
    MarkPts( pts, "ch=closed;l");

}
//anglePt_demo_circle();

module anglePt_demo_circle2()
{ echom("anglePt_demo_circle2");
    
	pqr = randPts(3);
	nseg = 6;
	MarkPts( pqr,"ch;l=PQR" );
	P = pqr[0];
	Q = pqr[1];
	R = pqr[2];

    rad = min( d01(pqr), d02(pqr),d12(pqr))/2;
    
    pts = [ for(i=[0:nseg-1])
            anglePt( pqr, a=90, a2= i*360/nseg, len=rad ) ];
    MarkPts( pts, "ch=closed;l");
  Mark90( [P,Q, pts[0]] );
  Cylinder( [Q, pts[0]], r=0.01);
}
//anglePt_demo_circle2();

module anglePt_demo_ball()
{ echom("anglePt_demo_ball");
    
	pqr = randPts(3);
	nseg = 12;
	MarkPts( pqr,"ch;l=PQR" );
	P = pqr[0];
	Q = pqr[1];
	R = pqr[2];

    rad = min( d01(pqr), d02(pqr),d12(pqr))/2;
    
    pts = [ for(i=[0:nseg-1])
              for(j=[0:nseg-1])
            anglePt( pqr, a=i*360/nseg, a2= j*360/nseg, len=rad ) ];
    arrPts= [ for(i=[0:nseg-1])
              [ for(j=[0:nseg-1])
                anglePt( pqr, a=i*360/nseg, a2= j*360/nseg, len=rad ) ]
              ];
    for( pts= arrPts )
       //MarkPts( pts, "ch=closed;ball=false");
       Line( pts, r=0.01 );
    for( pts= transpose( arrPts ) )
       Line( pts, r=0.01 );
       
       //MarkPts( pts, "ch=closed;ball=false");
    
    //MarkPts( pts, "ch=closed;ball=false");
  //Mark90( [P,Q, pts[0]] );
  //Cylinder( [Q, pts[0]], r=0.01);
}
//anglePt_demo_ball();


module anglePt_demo_90()
{ _mdoc("anglePt_demo_90"
," First we made a random right-angle pqr using
;; randRightAnglePts(), then make a S that is on the pqr
;; plane using anglePt(pqr), then using anglePt(pqs, 90) 
;; to make a pt90 that is right angle to line PQ.
"    
);

	echom("anglePt_demo_3_90");
	pqr = randRightAnglePts(r=4); // make a right triangle pqr
	echo("pqr", pqr);
    
	echo("angle pqr", angle(pqr));

	P = pqr[0];
	Q = pqr[1];
	R = pqr[2];
	S = anglePt(pqr, a=45, r=2); // create a 45 deg pt, S, on pqr plane
		
	echo("pqs", [P,Q,S]);
	pt90 = anglePt( [P,Q,S], a=90, r=2 ); // use pqs as new 3-pointer to get 
									 // a 90-deg pt
	echo("pt90", pt90);
	MarkPt(pt90, ["r",0.1, "transp",0.2,"label","pt90"]);

    MarkPts( [P,Q,R,S], "l=PQRS;ch");
	Mark90( pqr );
}
//anglePt_demo_90();

module anglePt_demo_4_90_debug()
{_mdoc("anglePt_demo_4_90_debug"
," This is a debug version of demo_3_90. It simply checks
;; several cases of pqr in which anglePt(pqr, a=90) returns 
;; [nan,nan,nan]. It should have been fixed by now. 
");
    
// NOTE (7/7/2014) : use the example cases in demo_90 that generate errors:
//
// anglePt(pqr, a=90) sometimes produces [nan,nan,nan]
//
// 4 example cases in which it goes wrong:
//
	pqr1= [[2.06056, 0.685988, 6.36888], [3.00336, 1.06282, 2.49988], [1.95778, -0.257458, 3.5786]];
	pqr2= [[4.62891, -4.93576, 0.252527], [2.45234, -1.79303, 1.42973], [2.24605, -2.05891, 1.75812]];
	pqr3= [[1.15245, -2.62106, 1.36749], [1.01775, -2.65868, -2.63006], [1.05541, -3.91357, -2.61952]];
	pqr4= [[-0.394555, 0.856362, -1.03282], [0.291087, -0.669823, 2.60045], [-1.23265, 0.295526, 1.73654]];
    
    pt1= anglePt( pqr1, a=90, r=2 );
    pt2= anglePt( pqr2, a=90, r=2 );
    pt3= anglePt( pqr3, a=90, r=2 );
    pt4= anglePt( pqr4, a=90, r=2 );
    
    echo("pqr1=>", pt1);
    echo("pqr2=>", pt2);
    echo("pqr3=>", pt3);
    echo("pqr4=>", pt4);
    
    module Mark(pqr, P90, color){
        MarkPts( pqr, ["l=PQR","chain",["r",0.06,"color",color]
                      ]);
        Mark90( [pqr[0],pqr[1],P90]);
        MarkPts( [P90, pqr[1]], "ch;l=[text,[,P90]]");
    }

    Mark( pqr1,pt1);
    Mark( pqr2,pt2,"red");
    Mark( pqr3,pt3,"green");
    Mark( pqr4,pt4,"blue");
    
	
//	//echom("anglePt_demo_4_90_debug");
//	pqr = pqr2;
//	echo("pqr1", pqr);
//	echo("angle pqr1", angle(pqr));
//
//	P = pqr[0];
//	Q = pqr[1];
//	R = pqr[2];
//	S = anglePt(pqr, a=45, r=2); // create a 45 deg pt, S, on pqr plane
//		
//	echo("pqs", [P,Q,S]);
//	echo("angle pqs", angle([P,Q,S]));
//
//	pt90 = anglePt( [P,Q,S], a=90, r=2 ); // use pqs as new 3-pointer to get 
//									 // a 90-deg pt
//	echo("pt90", pt90);
//	MarkPts([pt90], ["r",0.1, "transp",0.2]);
//	Line0( [Q, pt90], ["transp",0.2] );
//
//	MarkPts( [P,Q,R,S], ["labels", "PQRS"] );
//	Line0( [Q,S], ["r", 0.02] );
//	Mark90( pqr );
//	Line0( [P,Q] ); //Chain( pqr );

	// Check the code in anglePt() that is sent to squarePts:
	//
	//	squarePts(
	//	[ 
	//		// U
	//		onlinePt( p10(pqr) // [Q,P] // squarePts( pqr )[2] ]  // [Q,A]
	//		    , len= r*cos(a)
	//			)
	//		// Q	
	//		, pqr[1]
	//
	//		// S
	//		,onlinePt( [ pqr[1], squarePts( pqr )[2] ]  // [Q,A]
	//		    , len= r*sin(a)
	//			)	
	//	])[3]

//	U = onlinePt( p10([P,Q,S]) , len= 2*cos(90) );
//	Q2= [P,Q,S][1];
//	S2= onlinePt( [ pqr[1], squarePts( [P,Q,S] )[2] ], len= 2*sin(90));
//
//	echo("[U,Q2,S2]", [U,Q2,S2]);
//	echo("squarePts([U,Q2,S2])", squarePts([U,Q2,S2]));


}
//anglePt_demo_4_90_debug();


module anglePt_demo_coln(){
    
    echom("anglePt_demo_coln");
    
    pq = randPts(2);
    R = onlinePt( p10(pq), len=-1);
    pqr = app( pq, R);
//    pqr = [[2.47886, -2.42713, -1.05472], [0.795781, 1.82491, -0.815167], [0.42824, 2.75345, -0.762855]];
//    pqr =  [[-0.182627, 1.17879, 1.71043], [-0.482769, 1.41008, 1.86168], [-1.21843, 1.97698, 2.23239]];
    
    // angle(pqr) reports this pqr has angle = 
    //  180-0.000136074, which is very imprecisive 
    //  angle(pqr)-180 =  -0.000136074
//    pqr=  [[-2.02466, 0.862595, 0.15329], [-2.77813, 0.166835, -1.57319], [-3.15334, -0.17964, -2.43294]];
    
    MarkPts( pqr, "ch;l=PQR");
    
    echo(pqr=pqr);
    P90 = anglePt( pqr, a=90);
    MarkPts([ P90, pqr[1]], "ch;l=[text,[P90,]");
    echo( angle_PQR90 = _red(angle([ pqr[0], pqr[1], P90]))); 
    Mark90( [ pqr[0], pqr[1], P90] );
    br="<br/>";
    echo( 
          br, iscoline_pqr = iscoline(pqr)
        , br, angle_pqr= angle(pqr)
        , br, angle_prq= angle( p021(pqr))
        , br, x10000= angle(pqr)*1e4
        , br, minus180 = angle(pqr)-180
        , br, is0_minus180= is0(angle(pqr)-180)
        , br, is0_minus180= is0(angle(pqr)/10000000-180/10000000)
        , br, minus180 = angle(pqr)/100-180/100
        , br, cross = cross( pqr[0]-pqr[1], pqr[2]-pqr[0] )
        , br, sum = sum(cross( pqr[0]-pqr[1], pqr[2]-pqr[0] ))
        , br, square_diff= pow(dist(P90,pqr[0]),2)
                     - pow(dist(P90,pqr[1]),2)
                     - pow(dist(pqr[0],pqr[1]),2)
        , br, ZERO=ZERO             
        );
    
}
//anglePt_demo_coln();

//anglePt( pqr, a=undef, a2=0, len=undef, ratio=1
//                , Rf=undef  // Rf could be used when pqr is coln
//                )



module arcPts_demo_1()
{_mdoc("arcPts_demo_1",
" arcPts( pqr=randPts(3), a=90, r=1, count=6, pl=''xy'', cycle, pitch=0)
;; -- random pqr
;; -- red  : arcPts( pqr )
");
	pqr = randPts(3);
    echo(pqr=pqr);
    
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=1);

	MarkPts(pqr, ["chain",true, "transp",0.4] );
	Line0([Q,N],["color","green"]);
	Line0([Q,M],["color","blue"]);
	MarkPts( [ P,Q,R, N, M]
           , ["label", "PQRNM"
             ,"colors", ["red","green","blue", "yellow","purple"]] );

	//----------------------------------
	pts = arcPts( pqr );
	echo("pts",pts);
	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true
                , "chain", true, "label",range(pts)] );
	for( i = range( pts) ) 
        Line0( [Q, pts[i]], ["transp",0.4]);
    //LabelPt( R, "arcPt() default:n=6,aPQR=undef(pts on PQR),rad=dPQ");
    Write( "arcPt() default:n=6, a=undef,rad=dPQ"
         , halign="center", shift=[0,1,0]
         //, at=[ get(pts,-2), get(pts,-1), extPt([pqr[1],pqr[2]])]
         , at=[ get(pts,0.5), get(pts,0.7), extPt([pqr[1],pqr[2]])]
         , opt=["scale",0.06] 
         ); 	

}
//arcPts_demo_1();



module arcPts_demo_1_2()
{_mdoc("arcPts_demo_1_2",
" arcPts( pqr=pqr(), a=90, r=1, count=6, pl=''xy'', cycle, pitch=0)
;; -- random pqr
;; -- green: arcPts( pqr, r=2, a=150, count=10 ) 
");
	pqr = pqr();
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=1);

	//Chain(pqr);
	//Line0([Q,N],["color","green"]);
	//Line0([Q,M],["color","blue"]);

    MarkPts( pqr, "ball=0;ch");
	MarkPts([ P,Q,R, N, M], ["label", "PQRNM"] );


//	//----------------------------------
	pts2= arcPts( pqr, rad=2, a2=90, n=10 );
	//echo("pts",pts);
	MarkPts( pts2, ["colors",["green"], "samesize",true
				   , "sametransp",true, "r",0.05
                   , "chain",true
                   , "label", range(pts2)
                   ] );
	for( i = range( 10) ) Line0( [Q, pts2[i]], ["r",0.01, "transp",0.4]);	

    
//
//	//----------------------------------
//	pts3= arcPts( pqr, r=3, a=360, count=12, pl="yz" );
//	//echo("pts",pts);
//	MarkPts( pts3, ["colors",["blue"], "samesize",true
//				, "sametransp",true, "r",0.05] );
//	Chain( pts3, ["r",0.01, "color","blue"] );
//	for( i = range( 12 ) ) Line0( [Q, pts3[i]], ["r",0.01, "transp",0.4]);	
//	LabelPts( pts3, range(pts3) );
}
//arcPts_demo_1_2();

module arcPts_demo_1_circle()
{
    echom("arcPts_demo_1_circle");
    pqr= pqr();
    MarkPts(pqr, ["label","PQR","chain",["r",0.05]] );
    
    pts1= arcPts( pqr, a=360*(11/12), n=12, rad=1);
    MarkPts( pts1, "ch=[color,red,r,0.025];l");
//        ["chain", ["color","red","r",0.025], "label", range(pts1),"markpts",false] );
    
    pts2= arcPts( pqr, a2=360*(11/12), n=12, rad=d12(pqr));
    MarkPts( pts2, , "ch=[color,green,r,0.025];l");
//    MarkPts( pts2, 
//        ["chain", ["color","green","r",0.025], "label", range(pts1),"markpts",false] );

    pts3= arcPts( pqr, a=90, a2=360*(11/12), n=12, rad=3);
    MarkPts( pts3, "ch=[color,blue,r,0.025];l");
    //Line0( [pts3[0],Q(pqr)] );
    Mark90( [P(pqr),Q(pqr),pts3[0]]);
//    MarkPts( pts3, ["chain"
//        , ["color","blue","r",0.025], "label", range(pts1),"markpts",false] );
    
}
//arcPts_demo_1_circle();

module arcPts_demo_2()
{_mdoc("arcPts_demo_2",
"Use the cycle and pitch to make a spiral arc:
;;
;; arcPts( pqr, r=2, count=count, pl=''yz'', cycle=3, pitch= 2)
;;
;; pitch: distance between 2 cycles
");
	echom("arcPts_demo_2");
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	MarkPts( pqr, "ch=[r,0.1];l=PQR");
    //MarkPts( [M,Q,N], 
    //Chain(pqr, ["r",0.1,"transp",0.4, "closed",false] );
	Line0([Q,N],["color","green","r",0.06, "transp",0.2]);
	Line0([Q,M],["color","blue","r",0.06, "transp",0.2]);

// 	Mark90( [P,Q,N]);
//	Mark90( [P,Q,M] );
//	Mark90( [N,Q,M] );
//	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	// We are gonna make a spiral here. 
	cycles = 5; 
	pts_per_cycle   = 12;  // Each cycle has ? pts.
	pitch= 3;    // The distance between the start of this cycle
                           // to the next one
    //shift_per_pt = pitch / pts_per_cycle;
	
	//pts = [ for (c=range(cycles))
			
	 
	count = 36;
	pts = arcPts( pqr, r=2, count=count, pl="yz", cycle=3, pitch= 3);
	//echo("pts",pts);
	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05, "transp",0.5] );
	Chain( pts, ["r",0.01, "color","red", "transp", 0.5] );
	for( i = range( count) ) 
		Line0( [ pts[i], othoPt( [P, pts[i], Q]) ]
			, ["r",0.03, "transp",0.8]);	

}
//arcPts_demo_2();

module borderPts_old_demo(){

    echom("borderPts_old_demo");
    pts = randPts(15);

    MarkPts( pts, "samesize;cl=darkcyan;sametransp" );
    
    bpminx = borderPts(pts, "min","x");
    bpmaxx = borderPts(pts, "max","x");
        
    bpminy = borderPts(pts, "min","y");
    bpmaxy = borderPts(pts, "max","y");
    
    bpminz = borderPts(pts, "min","z");
    bpmaxz = borderPts(pts, "max","z");
    
    r=0.2; trp=0.3;
    
    MarkPts( joinarr( [bpminx,bpmaxx]), opt=["r",r,"transp",trp,"color","red"]);
    MarkPts( joinarr( [bpminy,bpmaxy]), opt=["r",r,"transp",trp,"color","green"]);
    MarkPts( joinarr( [bpminz,bpmaxz]), opt=["r",r,"transp",trp,"color","blue"]);
    
    bpts = joinarr( [ bpminx, bpminy, bpminz, bpmaxx,bpmaxy,bpmaxz ]);
    Chain( bpts, opt=["closed",true, "r",0.01] );
    echo( bpts = bpts );
    
//    Plane( joinarr( bpminx, bpminy, bpminz ) );
    
    
}
//borderPts_old_demo();


//. c

module centerPt_demo(){
 
    echom("centerPt_demo");
    
    pqr= pqr();
    
    C=centerPt(pqr);
    MarkPt(C);
    
    N = N( [ pqr[0], C, pqr[1]] );
    
    cir = circlePts( [N,C,pqr[0]], rad = dist(C,pqr[0]), n=36);
    
    MarkPts( pqr, "ch" );
    MarkPts( cir, "ch;ball=false");
    
    
}   
//centerPt_demo();


module chainbone_demo(){

    echom("chainbone_demo");
    
    pqr= pqr();
    
    pts= randPts(20, d=[-5,-2]); //chainBoneData(20);
    //echo( pts = arrprn(pts) );
    MarkPts(pts,"ch");
    
    module Mark(pqr, n,seglen,a,a2,color,lbl){
        //pts2= chainBoneData(seed=pqr, len=len, n=n, a=a, a2=a2);
        chret= chainBoneData( n=n, a=a, a2=a2
              , opt=["seed",pqr, "seglen",seglen]);
        echo( chret = chret);
        pts = hash(chret, "pts");
        MarkPts(pts,["chain",["color",color,"r",0.05]]);
        if(lbl)
            LabelPt( pts[0], lbl,["color",color]);
    }    
    
    Mark( pqr= trslN( pqr, 3), 
          n = 8, a=[120,240], a2=0
        , lbl="a=[120,240],a2=0 (=>coplane)");
    
    Mark( pqr= trslN( pqr, 6),
          n = 8, seglen=2, a=0, a2=[120,240], color="red"
        , lbl= "len=2, a2=0, a=[120,240] (fixed seg len)");
    
    Mark( pqr= trslN( pqr, 9),
          n =20, a=[-45,45], a2=[-45,45], color="green"
       , lbl="a=a2=[-45,45] packed");
    
    Mark( pqr= trslN( pqr, 12),
          n =20, //x=[2,5],y=[-5,-2],z=[2,5]
                   , a=[165,195], a2=[255,280], color="blue"
                   ,lbl="zig-zag");

//    Mark( pqr= trslN( pqr, 17),
//      n =12, seglen=2,  
//               , a=240, a2=60, color="purple"
//               ,lbl="spiral");
}
//chainbone_demo();

//pts = chainBoneData();
//echo(pts= arrprn(pts,2));

module chainbone_demo_coln(){

    echom("chainbone_demo_coln");
    
    seed= pqr();
    
    module Mark(seed, n,len,a,a2,color,lbl){
        
        echo( n=n, seed = seed );
        chret= chainBoneData(seed=seed, len=len, n=n, a=a, a2=a2);
        echo( chret= chret);
        pts= hash(chret, "pts");
        MarkPts(pts);
        //MarkPts(pts2,["chain",["color",color,"r",0.05]]);
        if(lbl)
            LabelPt( pts[0], lbl,["color",color]);
    }    
    
    Mark( seed= trslN( seed, 3), 
          n = 6, a=180, a2=0
        , lbl="   a=180,a2=0 (=coline)");
    
}
//chainbone_demo_coln();


module chainbone_demo_pqr_tree(){

    echom("chainbone_demo_pqr_tree");
    nline= 8;
    
    
    pqr = randPts(3, d=[0,3]);
    for( n= range(nline))
    {    
       pts= chainBoneData(rand(4,6)
              , pqr=pqr, a=[175,185], a2=[-20,20]);
       MarkPts(pts,"ch=[color,red,r,0.05];ball=false");
    }   
    
    nH = 5;
    nbranch = 15;
    min_nseg = 2; // min segments for a branch;
    
    pts0 = chainBoneData(nH, d=[-3,0]
           , pqr = trslN(pqr, 6)
              , a=[160,200], a2=[-45,45]);
    MarkPts(pts0,"ch=[color,brown,r,0.1];ball=false");
    
    for( n= range(nbranch))
    {    
       i = rand( 1,nH-1); // where the branch branches
       pqr = get( pts0, [i-1,i,i+1] ); 
       pts= chainBoneData(
              // branch nseg
              rand( min_nseg, min_nseg+nH-i)
              , pqr=pqr
              , a=[140,220], a2=[-45,45]);
       MarkPts(pts,"ch=[color,green,r,0.05,transp,0.3];ball=false");
    }   
    
    
       // LabelPt( last(pts2), "a=[120,240],a2=0 (=>coplane)");
//
//    // extended, Nearly co-plane
//    pts3= chainBoneData(15, a=0, a2=[120,240]);
//    MarkPts(pts3,"ch=[color,green,r,0.05]");
//    LabelPt( last(pts3)
//            , "a2=0, a=[120,240] (not coplane ??)");
//    
//    // packed 
//    pts4= chainBoneData(25, d=[2,5], a=[-45,45], a2=[-45,45]);
//    MarkPts(pts4,"ch=[color,blue,r,0.04]");
//    LabelPt( last(pts4)
//            , "a=a2=[-45,45] packed");
//
//    pts5= chainBoneData(25, x=[2,5],y=[-5,-2],z=[2,5]
//                   , a=[165,195], a2=[255,280]);
//    MarkPts(pts5,"ch=[color,purple,r,0.04]");
//    LabelPt( last(pts5)
//            , "a2=[165,195], a=[255,280] ");

}
//freeze pc: chainbone_demo_pqr_tree();

module chainbone_demo_smooth(){

    echom("chainbone_demo_smooth");
    
    pqr= pqr();
    
    pts= randPts(20, d=[-5,-2]); //chainBoneData(20);
    //echo( pts = arrprn(pts) );
    //MarkPts(pts,"ch");
    
    module Mark(pqr, n,len,a,a2,color,smooth,lbl){
        rtn= chainBoneData(seed=pqr, n=n, a=a, a2=a2
                       , smooth=smooth);
        echo(rtn=rtn);
        smpts = hash(rtn,"pts");
        unsmoo = hash(rtn, "unsmoothed");
        
        //echo( smpts = smpts);
        echo( smooth= hash(rtn, "smooth") );
        
        MarkPts(unsmoo,["chain",["color",color,"r",0.02,"transp",0.3]]);
        MarkPts(smpts,["samesize;samecolor;r=0.04;ball=false","chain"
                ,["color","red","r",0.015,"closed",false]]);
        
//        if(lbl)
//            LabelPt( pts2[0], lbl,["color",color]);
    }    
    
    Mark( pqr= trslN( pqr, 3), 
          n = 8, a=[120,240], a2=0 , smooth=true
        , lbl="a=[120,240],a2=0 (=>coplane)");
    
    Mark( pqr= trslN( pqr, 6),
          n = 8, len=2, a=0, a2=[120,240], color="red", smooth=true
        , lbl= "len=2, a2=0, a=[120,240] (fixed seg len)");
    Mark( pqr= trslN( pqr, 10),
          n =10, a=[-45,45], a2=[-45,45], color="green", smooth=true
       , lbl="a=a2=[-45,45] packed");
    
    Mark( pqr= trslN( pqr, 13),
          n =20 , smooth=true
                   , a=[165,195], a2=[255,280], color="blue"
                   ,lbl="zig-zag");
}
//chainbone_demo_smooth();


module chainbone_demo_lens(){

    echom("chainbone_demo_lens");
    
    pqr= pqr();
    n = 8;
    
    //pts= randPts(20, d=[-5,-2]); //chainBoneData(20);
    //echo( pts = arrprn(pts) );
    //MarkPts(pts,"ch");
    
    module Mark(pqr, n,seglen,lens,a,a2,color,smooth,lbl){
        rtn= chainBoneData( seed=pqr, seglen=seglen, lens=lens
                      , smooth=smooth, n=n, a=a, a2=a2
                      );
        pts2 = hash( rtn, "pts"); //smooth?rtn[0]:rtn;
        smpts = rtn[1];
        echo( pts2 = pts2);
        MarkPts(pts2
        ,["samecolor", "color",color,"chain",["color",color,"r",0.02,"transp",0.3]]);
//        MarkPts(smpts,["samesize;samecolor;r=0.04","chain"
//                ,["color","red","r",0.015,"closed",false]]);
        
        if(lbl)
            LabelPt( pts2[0], lbl,["color",color]);
    }    
    
    Mark( pqr= trslN( pqr, 3), 
          n = n, a=[120,240], a2=0, color="darkkhaki"
        , lbl="rand seg lens"
    );

    Mark( pqr= trslN( pqr, 6),
          n = n, seglen=2, a=[120,240], a2=0, color="red"
        , lbl= "seg len=2"
    );
    
    lens = [for(i=[1:n-1]) 3*(1- i/n) ];
    echo(lens = lens );    
    Mark( pqr= trslN( pqr, 10),
          n =n, a=[120,240], a2=0, color="green"
         , lens=lens
       , lbl=str("lens=",lens)
    );
    
//    Mark( pqr= trslN( pqr, 13),
//          n =20, x=[2,5],y=[-5,-2],z=[2,5]
//                   , a=[165,195], a2=[255,280], color="blue"
//                   ,lbl="zig-zag");

}
//chainbone_demo_lens();





module chaindata_demo_Rf_rot(){
    
 echom("chaindata_demo_Rf_rot");   
 nseg = 2;
 nside=4;
 r = 0.8;
    
 rchrtn = chainBoneData( nseg+1, a=[ 130,170], a2=[-80,80] ); 
 echo(cbd = rchrtn ); 
 bone = hash(rchrtn, "pts");
 echo(ispts = ispts(bone));   
 MarkPts( bone, "ch=[r,0.05];l");   
 echo(bone= arrprn(bone));
 

 chrtn = chaindata( bone, r=r, nside=nside );
 //echo( chrtn = chrtn);
 pl = hash(chrtn, "pl");
 MarkPts( pl, "ch");
    
    
  cuts = hash(chrtn,"cuts");
  pts = joinarr(cuts);
  MarkPts( [pts[0],bone[0],bone[1], cuts[1][0], ], "ch;pl=[color,blue]");
   
 faces = faces("chain", nside=nside, nseg=len(cuts)-1);
 color(false,0.7)   
 polyhedron( points= pts, faces=faces);   
    
 //=============================================   
 bone2 = trslN(bone,4);
 MarkPts( bone2, "ch=[r,0.05];r=0.05");  
    
chrtn2 = chaindata( bone2, r=r, nside=nside,rot=45 ); //<=== rot 45
cuts2 = hash( chrtn2, "cuts"); 
pts2 = joinarr(cuts2);

MarkPts( [pts2[0],bone2[0],bone2[1], cuts2[1][0] ]
          , "ch;pl=[color,blue]");
// 
 color("red",0.7)   
 polyhedron( points= pts2, faces=faces);   
  
}
//chaindata_demo_Rf_rot();

module Chain_demo_Rf_rot(){
    
 echom("Chain_demo_Rf_rot");   

 rchrtn = chainBoneData( 5, a=[ 130,170], a2=[-80,80] ); 
 bone = hash(rchrtn, "pts");
 echo( bone = bone );
    
 MarkPts( bone, "ch=[r,0.05];l");   
 Chain( bone, "trp=0.3" );
      
 bone2 = trslN(bone,4);
 MarkPts( bone2, "ch=[r,0.05];l");   
 Chain( bone2, "cl=red;trp=0.3;rot=45" );   

}
//Chain_demo_Rf_rot();

module chaindata_demo_Rf_rot2(){
    
 echom("chaindata_demo_Rf_rot2");   
 nseg = 5;
 nside=4;
 r = 0.5;
    
    bone = arcPts( pqr(), a=360*(nseg-1)/nseg
                   , rad= 3 );
    echo( bone = arrprn( bone, dep=3 ) );
    MarkPts( bone, "ch");
 
    // Note that the pts on the starting crosec 
    // start at random position
    chrtn = chaindata( bone, r=d01(bone)/4, nside=nside );
    pts = hash(chrtn, "cuts");
    MarkPts(joinarr(pts), "ch" );   
    faces = faces("chain", nside=nside, nseg=nseg);
    color(undef,0.6)   
    polyhedron( points= joinarr(pts), faces=faces);   

////    // Set Rf to determine the 1st pt of a crosec starts
////    bone2= trslN(bone, 3);
////    MarkPts( bone2, "ch");
////    chrtn2 = chaindata( bone2
////                  , Rf=bone2[2]
////                  , r=d01(bone2)/4, nside=nside );
////    pts2 = hash(chrtn2, "cuts");
////    MarkPts(joinarr(pts2), "ch" );   
////    faces2 = faces("chain", nside=nside, nseg=nseg);
////    color("red",0.6)   
////    polyhedron( points= joinarr(pts2), faces=faces2);   

//
    // Set rot to change the 1st pt of a crosec starts
    bone3= trslN(bone, 3);
    MarkPts( bone3, "ch");
    chrtn3 = chaindata( bone3
                  //, Rf=bone3[2]
                  , rot=45
                  , r=d01(bone3)/4, nside=nside );
    pts3= hash(chrtn3, "cuts");              
    MarkPts(joinarr(pts3), "ch" );   
    faces3 = faces("chain", nside=nside, nseg=nseg);
    color("green",0.6)   
    polyhedron( points= joinarr(pts3), faces=faces3); 

}
//chaindata_demo_Rf_rot2(); //6"

module chaindata_testing_2pointer(isdetails=0, isdraw=0){
    
    echom("chaindata_testing_2pointer");
   
    //=====================================================
    pq= randPts(2);
    chdata= chaindata(pq, r=0.8);
    function chdata(k,df)=hash(chdata,k,df);
    cuts = chdata("cuts");
    setting1= [ "label", "1) 2-pointer"
              , "color", "olive"
              , "transp" , 1
              , "chdata", chdata
              , "wants", [ "a_cut00_bone",90
                         , "a_cut01_bone",90
                         , "twist_seg0", 0
                         , "a_per_side", 90
                        // , "a_cut00_pqr",0
                         ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
    //=====================================================
    settings= [setting1
              ];
    chaindata_testing( label="chaindata_testing_2pointer"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw
    );    
}   
//chaindata_testing_2pointer(isdetails=1, isdraw=0);

module chaindata_testing_rot(isdetails, isdraw){  
    echom("chaindata_testing_rot");

    nseg = 4;
    nside= 4;
    r = 0.6;

    rchrtn = chainBoneData( nseg+1, a=[ 130,170], a2=[-60,60] ); 
    bone1 = hash(rchrtn, "pts");

    //=====================================================
    chdata1 = chaindata( bone1, r=r, nside=nside );
    cuts1 = hash( chdata1, "cuts" );
    setting1= [ "label", "1) nside=4"
              , "color", "olive", "transp",0.8
              , "chdata", chdata1
              //, "shape", "chain"
              , "wants", [ 
                       "a_cut00_bone",90
                     , "a_cut01_bone",90
                     , "a_cut00_pqr",0
                     , "a_cut01_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
    //=====================================================
    chdata2 = chaindata( trslN(bone1,4), r=r, nside=nside, rot=45 );
    cuts2 = hash( chdata2, "cuts" );
    setting2= [ "label", "2) nside=4,rot=45"
              , "color", "red", "transp",0.5
              , "chdata", chdata2
              //, "shape", "chain"
              , "wants", [ 
                       "a_cut00_bone",90
                     , "a_cut01_bone",90
                     , "a_cut00_pqr", 45
                     , "a_cut01_pqr",45
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
    
    //=====================================================
    settings= [setting1
              ,setting2
              ];
    chaindata_testing( label="chaindata_testing_rot"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw
                     ); 
    
}
//chaindata_testing_rot(isdetails=1, isdraw=0);

module chaindata_testing_tilt(isdetails, isdraw){  
    echom("chaindata_testing_tilt");

    nseg = 3;
    nside= 4;
    r = 0.6;

    rchrtn = chainBoneData( nseg+1, a=[ 130,170], a2=[-60,60] ); 
    bone1 = hash(rchrtn, "pts");

    //=====================================================
    chdata1 = chaindata( bone1, r=r, nside=nside );
    cuts1 = hash( chdata1, "cuts" );
    setting1= [ "label", "1) No tilt"
              , "color", "olive", "transp",0.5
              , "chdata", chdata1
              //, "shape", "chain"
              , "wants", [ 
                       "a_cut00_bone",90
                     , "a_cut01_bone",90
                     , "a_cut00_pqr",0
                     , "a_cut01_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
             // , "isdraw", 1
              ];
    //=====================================================
    chdata2 = chaindata( trslN(bone1,2), r=r, nside=nside, tilt=60 );
    cuts2 = hash( chdata2, "cuts" );
    setting2= [ "label", "2) tilt=60"
              , "color", "red", "transp",0.3
              , "chdata", chdata2
              //, "shape", "chain"
              , "wants", [ 
                       "a_cut00_bone",60
                     , "a_cut01_bone",90
                     , "a_cut00_pqr",0
                     , "a_cut01_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
             //, "isdraw", 1
              ];

//    //=====================================================
//    chdata21 = chaindata( trslN2(bone1,2), r=r, nside=nside
//                                                , tilt=[0,45,30] );
//    cuts21 = hash( chdata21, "cuts" );
//    setting21= [ "label", "21) tilt=[0,45,30]"
//              , "color", "red", "transp",0.3
//              , "chdata", chdata21
//              //, "shape", "chain"
//              , "wants", [ 
//                       "a_cut00_bone",60
//                     , "a_cut01_bone",90
//                     , "a_cut00_pqr",0
//                     , "a_cut01_pqr",0
//                     , "twist_seg0", 0
//                     , "a_per_side", 90 
//                     ]
//              , "isdetails", 1
//             , "isdraw", 1
//              ];
              
    //=====================================================
    chdata3 = chaindata( trslN(bone1,4), r=r, nside=nside, rot=60 );
    cuts3 = hash( chdata3, "cuts" );
    setting3= [ "label", "3) rot=60"
              , "color", "green", "transp",0.3
              , "chdata", chdata3
              //, "shape", "chain"
              , "wants", [ 
                       "a_cut00_bone",90
                     , "a_cut01_bone",90
                     , "a_cut00_pqr",60
                     , "a_cut01_pqr",30
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];

    //=====================================================
    /* :::::NOTE:::::
       The first num in tilt, 90, is an indication of which direction
       the tilt is to operate. This does NOT mean that the data points
       are rotated by 90. Note that the pl pts starts at a pt the same
       as default.
       To rotate the data pts, use rot=n. See next: setting5
    */ 
    chdata4 = chaindata( trslN(bone1,6), r=r, nside=nside, tilt=[90,45] );
    cuts4 = hash( chdata4, "cuts" );
    setting4= [ "label", "4) tilt=[90,45]"
              , "color", "blue", "transp",0.3
              , "chdata", chdata4
              //, "shape", "chain"
              , "wants", [ 
                       "a_cut00_bone",90
                     , "a_cut01_bone",45
                     , "a_cut00_pqr",0
                     , "a_cut01_pqr",0
                     , "twist_seg0", 0
                     
                    // , "a_per_side",  
                     ]
             // , "isdetails", 1
             // , "isdraw", 1
              ];
              
    //=====================================================
    
    chdata5 = chaindata( trslN(bone1,8), r=r,nside=nside, rot=90, tilt=45);//,tilt=90);
    cuts5 = hash( chdata5, "cuts" );
    
//    chdata51 = chaindata( trslN(bone1,8), r=r,nside=nside,tilt=[90,45]);
//    cuts51 = hash( chdata51, "cuts" );
//    MarkPts( cuts51[0], "ch=[r,0.005];pl=[transp,0.1];ball=false");
//
//    Rf= hash( chdata5, "Rf");
//    echo( Rf=Rf );
//    MarkPt( Rf, "l=[color,darkcyan,text,Rf]" );
    
    setting5= [ "label", "5) rot=90, tilt=45"
              , "color", "purple", "transp",0.3
              , "chdata", chdata5
              //, "shape", "chain"
              , "wants", [ 
                       "a_cut00_bone",45
                     , "a_cut01_bone",90
                     , "a_cut00_pqr",0
                     , "a_cut01_pqr",0
                     , "twist_seg0", 0
                     
                     //, "a_per_side",  
                     ]
              //, "isdetails", 1
             // , "isdraw", 1
              ];

              
    //=====================================================
    settings= [setting1
              ,setting2
              //,setting21
              ,setting3
              ,setting4
              ,setting5
              ];
    chaindata_testing( label="chaindata_testing_tilt"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw 
                     ); 
    
}
//chaindata_testing_tilt(isdetails=0, isdraw=0); //24"

module chaindata_testing_tiltlast(isdetails, isdraw){  
    echom("chaindata_testing_tiltlast");

    nseg = 3;
    nside= 4;
    r = 0.6;

    rchrtn = chainBoneData( nseg+1, a=[ 130,170], a2=[-60,60] ); 
    bone1 = hash(rchrtn, "pts");

    //=====================================================
    chdata1 = chaindata( bone1, r=r, nside=nside, tiltlast=90 );
    cuts1 = hash( chdata1, "cuts" );
    setting1= [ "label", "1) No tilt"
              , "color", "olive", "transp",0.5
              , "chdata", chdata1
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                     //, "tilt", 90
                      "a_lastcut0_bone",90
                     , "a_lastcut1_bone",90
                     //, "tiltlast", 90
                     //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
    //=====================================================
    chdata2 = chaindata( trslN(bone1,2), r=r, nside=nside, tiltlast=60 );
    cuts2 = hash( chdata2, "cuts" );
    setting2= [ "label", "2) tiltlast=60"
              , "color", "red", "transp",0.3
              , "chdata", chdata2
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                      //"tilt", 90
                      "a_lastcut0_bone",60
                     , "a_lastcut1_bone",90
                     //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];

    //=====================================================
    chdata3 = chaindata( trslN(bone1,4), r=r, nside=nside, rot=90 );
    cuts3 = hash( chdata3, "cuts" );
    setting3= [ "label", "3) rot=90"
              , "color", "green", "transp",0.3
              , "chdata", chdata3
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                       "a_cut00_bone",90
                     , "a_cut01_bone",90
                    //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
    //=====================================================
    chdata4 = chaindata( trslN(bone1,6), r=r, nside=nside
                                            , tiltlast=[90,45] );
    cuts4 = hash( chdata4, "cuts" );
    setting4= [ "label", "4) tiltlast=[90,45]"
              , "color", "blue", "transp",0.3
              , "chdata", chdata4
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                     "a_lastcut0_bone",90
                     , "a_lastcut1_bone",45
                     //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                      ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
              
    //=====================================================
    chdata5 = chaindata( trslN(bone1,8), r=r, nside=nside
                                            , rot=90, tiltlast=45 );
    cuts5 = hash( chdata5, "cuts" );
    setting5= [ "label", "5) rot=90,tiltlast=45"
              , "color", "purple", "transp",0.3
              , "chdata", chdata5
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                     "a_lastcut0_bone",45
                     , "a_lastcut1_bone",90
                     //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                      ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
              
    //=====================================================
    settings= [setting1
              ,setting2
              ,setting3
              ,setting4
              ,setting5
              ];
    chaindata_testing( label="chaindata_testing_tiltlast"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw 
                     ); 
    
}
//chaindata_testing_tiltlast(isdetails=0, isdraw=0); //24"

module chaindata_testing_closed(isdetails, isdraw){  
    echom("chaindata_testing_closed");

    nseg = 6;
    nside= 4;
    r = 0.8;

    rchrtn = chainBoneData( nseg+1, seglen=2,a=[ 120,150], a2=[0,30] ); 
    bone1 = hash(rchrtn, "pts");

    //=====================================================
    chdata1 = chaindata( bone1, r=r, nside=nside );
    cuts1 = hash( chdata1, "cuts" );
    setting1= [ "label", "1) nside=4"
              , "color", "olive", "transp",0.9
              , "chdata", chdata1
              //, "shape", "chain"
              , "wants", [ 
                       "a_cut00_bone",90
                     , "a_cut01_bone",90
                     , "a_cut00_pqr",0
                     , "a_cut01_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
 
    //=====================================================
    chdata2 = chaindata( trslN(bone1,4), r=r, nside=nside, closed=true );
    cuts2 = hash( chdata2, "cuts" );
    setting2= [ "label", "2) closed=true"
              , "color", "red", "transp",0.9
              , "chdata", update(chdata2, ["shape","ring"])
              //, "shape", "ring"
              , "wants", let( bone= hash(chdata2,"bone")
                            , p3_0 = get3( bone, 0 )
                            , p3_last= get3( bone,-1)
                            )
                     [  //"a_cut00_bone",angle(p3_0)/2
                     //, "a_cut01_bone",90
                      "a_cut00_pqr", 0
                     , "a_cut01_pqr",0
                     , "twist_seg0", 0
                    // , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
              
    //=====================================================
    chdata3 = chaindata( trslN(bone1,8), r=r, nside=nside 
                                          , closed=true, rot=45 );
    cuts3 = hash( chdata3, "cuts" );
    setting3= [ "label", "3) closed=true, rot=45"
              , "color", "green", "transp",0.9
              , "chdata", update(chdata3, ["shape","ring"])
              //, "shape", "ring"
              , "wants", [ 
                      // "a_cut00_bone",90
                    // , "a_cut01_bone",90
                      "a_cut00_pqr", 45
                     , "a_cut01_pqr",45
                     , "twist_seg0", 0
                     //, "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
    
    //=====================================================
    settings= [setting1
              ,setting2
              ,setting3
              ];
    chaindata_testing( label="chaindata_testing_closed"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw
                     ); 
    
}
//chaindata_testing_closed(isdetails=1, isdraw=1);

module chaindata_tests(isdetails=0, isdraw=0){
    
    chaindata_testing_2pointer( isdetails=isdetails, isdraw=isdraw );
    chaindata_testing_rot( isdetails=isdetails, isdraw=isdraw );
    chaindata_testing_tilt( isdetails=isdetails, isdraw=isdraw );
    chaindata_testing_tiltlast( isdetails=isdetails, isdraw=isdraw );
    chaindata_testing_closed( isdetails=isdetails, isdraw=isdraw );
}    
//chaindata_tests(); // 4"

module chaindata_demo_tilt(){
    
 echom("chaindata_demo_tilt");   
 nseg = 2;
 nside=4;
 r = 0.8;
    
    module Mark( bone, tilt=0,tiltlast=0
               ,color=undef, label=""){
        
        MarkPts( bone, "ch=[r,0.03]");
        chrtn = chaindata( bone
                      , tilt=tilt
                      , tiltlast=tiltlast
                      , r=r, nside=nside );
        pts= hash(chrtn,"cuts");
        echo( chrtn = chrtn );           
        //echofh( chrtn );           
        MarkPt( pts[0][0]
           , ["label",label,"shift",0.5]);
        
        Rf=hash(chrtn,"Rf");
                   echo(Rf=Rf);
       // MarkPts( [Rf], "r=0.3;cl=black;trp=0.2");           
        // tilt axis 
        tiltaxes= chrtn[1];
        //MarkPts( tiltaxes[0], "ch=[r,0.03]");
        if(tilt)        
        Line0( linePts( tiltaxes[0],len=[0.5,r+0.5])
           ,["r",0.02,"color","black","transp",0.5]);            
//        Line0( linePts([pts[0][1],pts[0][3]],len=0.5)
//           ,["r",0.02,"color","black","transp",0.2]);
        
        Mark90( [bone[1],bone[0], pts[0][0]],"cl=red;r=0.03;trp=1" );
        Mark90( [bone[1],bone[0], pts[0][1]],"cl=red;r=0.03;trp=1" );
        
        //echo(pts2= arrprn(pts,3));              
        MarkPts(pts[0], "ch" );   
        
        // Reference plane
        color("blue",0.5)  
        Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
        
        faces = faces("chain", nside=nside, nseg=nseg);
        color(color,0.6)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    
    //---------------------------------------------
    rndch=chainBoneData( nseg+1, seglen=2.5, a=[ 185,170], a2=[-60,60] );
    bone = hash(rndch,"pts");
    
    //===================================
    Mark( bone );

    //---------------------------------------------
    // Set tilt (head tilt angle)
    bone2= trslN(bone, 3.5);
     Mark( bone2, tilt=60,color="red"
         ,label="tilt=60" );
    //---------------------------------------------
    // Set tilt (head tiltlast angle)
    bone3= trslN(bone, 7);
    Mark( bone3, tilt=[45,45],color="green"
         ,label="tilt=[45,45]" );  
       
    //---------------------------------------------
    bone4= trslN(bone, 10.5);
    Mark( bone4, tilt=[180,60],color="blue"
         ,label="tilt=[180,60]" );  

}
//chaindata_demo_tilt();//5"

module chaindata_demo_tiltlast(){
    
 echom("chaindata_demo_tiltlast");   
 nseg = 3;
 nside=4;
 r = 0.8;
    
    module Mark( bone, tilt=0,tiltlast=0
               ,color=undef, label=""){
        
        MarkPts( bone, "ch=[r,0.03]");
        chrtn = chaindata( bone
                      , tilt=tilt
                      , tiltlast=tiltlast
                      , r=r, nside=nside );
        pts= hash(chrtn,"cuts");
        echo( pts = arrprn(pts,3));           
        echo( chrtn= arrprn( chrtn, 3));
            
        MarkPt( last(pts)[0], 
               ["label", label] );
                   
        for(xs=pts) MarkPt(xs[0],"r=0.08");
        
        Mark90( [get(bone,-2), last(bone), last(pts)[0] ],"cl=red;r=0.03;trp=1" );
        Mark90( [get(bone,-2), last(bone), last(pts)[1] ],"cl=red;r=0.03;trp=1" );
        
//        MarkPt( bone[0]
//           , ["label",label,"shift",0.5]);
//        
//        // tilt axis 
//        tiltaxes= linePts( chrtn[1][1],len=[0.5,r+0.5]); //chrtn[1];
//        MarkPts( tiltaxes, "ch=[r,0.03,color,black,transp,0.1];ball=0");
//        echo(tiltaxes = tiltaxes);           
////        if(tilt)        
////        Line0( tiltaxes //linePts( tiltaxes[1],len=[0.5,r+0.5])
////           ,["r",0.1,"color","black","transp",0.1]);            
////        Line0( linePts([pts[0][1],pts[0][3]],len=0.5)
////           ,["r",0.02,"color","black","transp",0.2]);
//        
//        echo(pts2= arrprn(pts,3));              
//        MarkPts(last(pts), "ch" );   
//        
        // Reference plane
        color("blue",0.5)  
        //Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
        Plane( [ get(bone,-1), get(bone,-2)
               , get(pts,-2)[0],get(pts,-1)[0] ]);
        
        faces = faces("chain", nside=nside, nseg=nseg);
        color(color,0.5)   
        polyhedron( points= joinarr(pts), faces=faces);  
       
        Rf = hash(chrtn,"Rf");
        echo(Rf = Rf);           
        //MarkPt(Rf,"r=0.2;cl=black;trp=0.2");           
       // echo(_debug = hash(chrtn, "_debug")); 
    }
    
    
    //bone = arcPts( pqr(), a=360*(nseg-1)/nseg
    //               , rad= 3 );
    rndch=chainBoneData( nseg+1, seglen=2, a=[ 185,170], a2=[-60,60] );
    bone = hash(rndch, "pts");
    //    error bone:
    
//       If r too small, the tilt might be too large for r, 
//       such that the tilted face interferes with the 
//       previous cut. Reducing r will solve this.  
    
//    bone= [[0.63183, 0.111251, 1.6763], [0.111685, -0.222291, 1.79372], [-1.37732, -1.79013, 3.65888], [-1.3784, -2.28214, 3.75537], [-1.17935, -2.70396, 4.08363]];
//    
    
//    This one doesn't have the problem of r too large, but still
//    produce error ... :(
    
//    bone=[[1.77587, -1.57462, -2.84241], [2.81478, -0.871177, -0.327539], [4.25501, -1.41681, 1.66574], [4.68453, -2.02393, 2.91524], [5.67899, -3.39539, 4.1074]];
    
    
    
    echo(bone = bone);
    Mark( bone );

    //---------------------------------------------
    // Set tilt (head tilt angle)
    bone2= trslN(bone, 3.5);
     Mark( bone2, tiltlast=60,color="red"
         ,label="tiltlast=60" );
    //---------------------------------------------
    // Set tilt (head tiltlast angle)
    bone3= trslN(bone, 7);
    Mark( bone3, tiltlast=[90,45],color="green"
         ,label="tiltlast=[90,45]" );  
//       
//    bone4= trslN(bone, 10.5);
//    Mark( bone4, tiltlast=[45,180],color="blue"
//         ,label="tiltlast=[45,180]" );  
//
    bone5= trslN(bone, 14);
    Mark( bone5, tiltlast=[45,45],color="purple"
         ,label="tiltlast=[45,45]" );  

}
//chaindata_demo_tiltlast();

module chaindata_demo_ring(){
    
 echom("chaindata_demo_ring");   
 nseg = 8;
 nside= 4;
 r = 1;
    
    module Mark( bone, r2=undef, tilt=0,tiltlast=0
               ,color=undef, label=""){
        
        //MarkPts( bone, "ch=[r,0.03]");
        chrtn = chaindata( bone
                      , r2=r2
                      , tilt=tilt
                      , tiltlast=tiltlast
                      , closed=true
                      , r=r, nside=nside );
        cuts= hash(chrtn,"cuts");
        echo( chrtn = chrtn );
                   
        echo(cuts= arrprn(cuts,3));              
        //MarkPts(last(pts), "ch" );   
        
        // Reference plane
        color("blue",0.5)  
        Plane( [bone[0],bone[1],cuts[1][0],cuts[0][0] ]);
        
        faces = faces("ring", nside=nside, nseg= len(cuts)-1);
        color(color,0.9)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    
    //bone = arcPts( pqr(), a=360*(nseg-1)/nseg
    //               , rad= 3 );
    pqr= pqr();
    
    rndch=chainBoneData( seed=pqr
            , n=3
            , seglen=8, a=60, a2=0
            , closed=true );
    echo(rndch=rndch);
    bone = hash( rndch, "pts" ); 
    //bone = [[-0.819723, -1.23043, 1.3898], [-4.14277, 5.92576, 2.71129], [0.810393, 4.89865, -3.48639]];
    echo(bone = bone);
    MarkPts( bone, "ch");
    
//    // pl
//    MarkPts(  [[-2.99047, 1.95303, 2.90711], [0.810393, 4.89865, -3.48639], [1.58611, 5.15214, -2.90845]], "ch;plane");
    
    Mark( bone );
    

    rndch4=chainBoneData( seed= trslN(pqr,10)
            , n=4
            , seglen=8, a=90, a2=0
            , closed=true );
    echo(rndch4=rndch4);
    bone4 = hash( rndch4, "pts" ); 
    echo(bone4 = bone4);
    MarkPts( bone4, "ch");
    Mark( bone4 );
//
//
    rndch5=chainBoneData( seed= trslN(pqr,20)
            , n=5
            , seglen=8, a=180*(5-2)/5, a2=0
            , closed=true );
    echo(rndch5=rndch5);
    bone5 = hash( rndch5, "pts" ); 
    echo(bone5 = bone5);
    MarkPts( bone5, "ch");
    Mark( bone5 );


    rndch7=chainBoneData( seed= trslN(pqr,30)
            , n=7
            , seglen=8, a=180*(7-2)/7, a2=0
            , closed=true );
    echo(rndch7=rndch7);
    bone7 = hash( rndch7, "pts" ); 
    echo(bone7 = bone7);
    MarkPts( bone7, "ch");
    Mark( bone7 );
    
    rndch24=chainBoneData( seed= trslN(pqr,40)
            , n=36
            , seglen=1, a=180*(36-2)/36, a2=0
            , closed=true );
    echo(rndch24=rndch24);
    bone24 = hash( rndch24, "pts" ); 
    echo(bone24 = bone24);
    MarkPts( bone24, "ch");
    Mark( bone24 );
}
//chaindata_demo_ring();

module Chain_demo_ring(){
    
    echom("Chain_demo_ring");   
// nseg = 8;
// nside= 4;
// r = 1;
    
    pqr= pqr();
    
    pts3= circlePts( pqr, rad=4, n=3 );
    echo(pts3 = pts3);
    chdata3 = chaindata( pts3, closed=true );
    echo(chdata3=chdata3);
    Chain( chdata3,  "closed" );
    
    pts4 = circlePts( trslP(pqr,5), rad=4, n=4);
    Chain( pts4, r=1, "closed");
    
    pts5 = circlePts( trslP(pqr,10), rad=4, n=5);
    Chain( pts5, "closed");

    pts7 = circlePts( trslP(pqr,15), rad=4, n=7);
    Chain( pts7, "closed");

    pts36 = circlePts( trslP(pqr,20), rad=4, n=24);
    Chain( pts36, "closed");

    pts362 = circlePts( trslP(pqr,25), rad=4, n=24);
    Chain( pts362, rot=45, "closed");

}
//Chain_demo_ring();

module chaindata_demo_rlast(){
    
 echom("chaindata_demo_rlast");   
 nseg = 6;
 nside= 4;
 r = 1.5;
    
    module Mark( bone, rlast=undef, tilt=0,tiltlast=0
               ,color=undef, label=""){
        

        chrtn = chaindata( bone
                      , rlast=rlast
                      , tilt=tilt
                      , tiltlast=tiltlast
                      , r=r, nside=nside );
        //echo( chrtn = chrtn );
                   
        pts= hash(chrtn,"cuts");
                   
        // Reference plane
        color("blue",0.5)  
        Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
        
        faces = faces("chain", nside=nside, nseg=nseg);
        color(color,0.9)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    
    //bone = arcPts( pqr(), a=360*(nseg-1)/nseg
    //               , rad= 3 );
    rndch=chainBoneData( nseg+1, seglen=1.5, a=[ 185,170], a2=[-60,60] );
    bone = hash( rndch, "pts" ); 
    //    error bone:
    
//       If r too small, the tilt might be too large for r, 
//       such that the tilted face interferes with the 
//       previous cut. Reducing r will solve this.  
    
//    bone= [[0.63183, 0.111251, 1.6763], [0.111685, -0.222291, 1.79372], [-1.37732, -1.79013, 3.65888], [-1.3784, -2.28214, 3.75537], [-1.17935, -2.70396, 4.08363]];
//    
    
//    This one doesn't have the problem of r too large, but still
//    produce error ... :(
    
//    bone=[[1.77587, -1.57462, -2.84241], [2.81478, -0.871177, -0.327539], [4.25501, -1.41681, 1.66574], [4.68453, -2.02393, 2.91524], [5.67899, -3.39539, 4.1074]];
    
    echo(bone = bone);
    Mark( bone );

    //---------------------------------------------
    // Set tilt (head tilt angle)
    bone2= trslN(bone, 5);
    MarkPts( bone2 );
     Mark( bone2, color="red", rlast= r/20
         ,label="rlast=r/20" );
    //---------------------------------------------
    bone3= trslN(bone, 12);
    Mark( bone3, color="green", rlast=r*1.5 // tiltlast=[45,90],color="green"
         ,label="rlast=r*1.5" );  

}
//chaindata_demo_rlast();

module Chain_demo_rlast(){
    
 echom("Chain_demo_rlast");   
 r = 1.5;

    rndch=chainBoneData( n=7, seglen=1.5, a=[ 185,170], a2=[-60,60] );
    bone = hash( rndch, "pts" );
    
    //---------------------------------------------
    Chain( bone ); 
    //---------------------------------------------
    bone2= trslN(bone, 5);
    Chain( bone2, "cl=red", rlast=r/20);
    
    //[2015.8.1]: the following 3 should have been working:
    //Chain( bone2, ["rlast",r/20]);
    //Chain( bone2, ["cl=red","rlast",r/20]);
    //Chain( bone2, str("cl=red;rlast=",r/20));
    
    //---------------------------------------------
    bone3= trslN(bone, 10);
    Chain( bone3, rlast=r*1.5, "cl=green", nside=5);
    //Chain( bone3, ["cl=green","rlast",r*1.5]);
}
//Chain_demo_rlast();

module chaindata_demo_rlast_hires(){
    
 echom("chaindata_demo_rlast_hires");   
 nseg = 8;
 nside= 4;
 r = 1.5;
 rlast=r/20;   
 nsmooth=8;   
    
    module Mark( bone, rlast=undef, rs=[], tilt=0,tiltlast=0
               ,color=undef, label=""){
        
        MarkPts( bone, "ch=[r,0.03]");
        chrtn = chaindata( bone
                      , rlast=rlast, rs=rs
                      , tilt=tilt
                      , tiltlast=tiltlast
                      //, smooth=true
                      , r=r, nside=nside );
        pts= hash(chrtn,"cuts");
                   
//        MarkPt( bone[0]
//           , ["label",label,"shift",0.5]);

        // Mark all pts:
        // MarkPts( joinarr(pts), "ch");
                   
        // Mark cut:
        // for( ps = pts) Plane( ps );
        MarkPts( pts[0], "ch");
        // Draw frame:           
//        for( l = transpose(pts))
//            MarkPts( l, "ch;ball=0"); 
        
        // tilt axis 
        //tiltaxes= linePts( chrtn[1][1],len=[0.5,r+0.5]); //chrtn[1];
        //MarkPts( tiltaxes, "ch=[r,0.03,color,black,transp,0.1];ball=0");
        //echo(tiltaxes = tiltaxes);           
//        if(tilt)        
//        Line0( tiltaxes //linePts( tiltaxes[1],len=[0.5,r+0.5])
//           ,["r",0.1,"color","black","transp",0.1]);            
//        Line0( linePts([pts[0][1],pts[0][3]],len=0.5)
//           ,["r",0.02,"color","black","transp",0.2]);
        
        echo(pts2= arrprn(pts,3));              
        //MarkPts(last(pts), "ch" );   
        
        // Reference plane
        //color("blue",0.5)  
        //Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
        
        faces = faces("chain", nside=nside
        , nseg=nseg*(nsmooth+1));
        color(color,1)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    
    //bone = arcPts( pqr(), a=360*(nseg-1)/nseg
    //               , rad= 3 );
    rndch=chainBoneData( nseg+1, seglen=2, a=[ 185,170], a2=[-60,60]
                  , smooth=["n",nsmooth] );
   
    bone = hash( rndch, "pts" );
    
    echo( smooth= hash(rndch,"smooth")
        , lenbone=len(bone), bone = bone);
    //  Mark( bone, rlast= rlast  );
    //---------------------------------------------
    n_per_cycle= 8;
    dr = abs((rlast-r)/len(bone));
    rs = [ for (i=range(bone))
            (r-i*dr)/r *sin( i*360/n_per_cycle)/2
            
         ];
    echo(rs=rs);
    bone2= trslN(bone, 5);
     Mark( bone2, color="red", rlast= rlast, rs=rs );
         //,label="rlast=r/20" 
   // );
         
    //---------------------------------------------

}
//chaindata_demo_rlast_hires();

module Chain_demo_rlast_hires(){
    
 echom("Chain_demo_rlast_hires");   
 nseg = 6;
 nside= 4;
 r = 1.5;
 rlast=r/20;   
 nsmooth=8;   
       
    rndch=chainBoneData( nseg+1, seglen=2, a=[ 185,170], a2=[-60,60]
                  , smooth=["n",nsmooth] );
   
    bone = hash( rndch, "pts" );
    
//    echo( smooth= hash(rndch,"smooth")
//        , lenbone=len(bone), bone = bone);
//    n_per_cycle= 8;
//    dr = abs((rlast-r)/len(bone));
//    rs = [ for (i=range(bone))
//            (r-i*dr)/r *sin( i*360/n_per_cycle)/2
//            
//         ];
    //---------------------------------------------
    //Chain( bone, rs=rs );
    //---------------------------------------------
    Chain( trslN(bone, 5), r=r, rlast=rlast, "cl=red" );

}
//Chain_demo_rlast_hires();

module chaindata_demo_r_is_arr(){
    
 echom("chaindata_demo_r_is_arr");   
 
 //nsmooth=8;   
    
    module Mark( bone, rlast=undef, r=[], tilt=0,tiltlast=0
               ,color=undef, label=""){
        
        MarkPts( bone, "ch=[r,0.03]");
        chrtn = chaindata( bone
                     // , rlast=rlast
                       , r=r
                     // , tilt=tilt
                     // , tiltlast=tiltlast
                      //, smooth=true
                     // , r=r
                     , nside=nside );
        pts= hash(chrtn,"cuts");
        echo( color=color, len_pts = len(pts), len_r=len(r), r=r );     

        MarkPts( pts[0], "ch");

        faces = faces("chain", nside=nside
        , nseg= len(pts)-1); //nseg);
        color(color,0.8)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    nseg = 16;
    nside= 4;
    r = 1.5;
    rlast=r/20;  
    
    //bone = arcPts( pqr(), a=360*(nseg-1)/nseg
    //               , rad= 3 );
    rndch=chainBoneData( nseg+1, seglen=2, a=[ 140,220], a2=[-30,30]
                   );
    bone = hash( rndch, "pts" );
    echo(lenbone=len(bone), bone = bone);
    
    bone1= [for(i=[0:4:len(bone)-4]) bone[i] ];
    Mark( bone1 ,  r= repeat( [r], len(bone1) ) );
    
    //---------------------------------------------
    n_per_cycle= 6;
    dr = abs((rlast-r)/len(bone));
    rs = [ for (i=range(bone))
            (r-i*dr)/r *sin( i*360/n_per_cycle)+1
            
         ];
    
    echo("red",len_bone= len(bone), range(bone), len_rs=len(rs), rs=rs);
    bone2= trslN(bone, 8);
     Mark( bone2, color="red", r=rs
    );
         
    bone3= trslN(bone, 16);
     Mark( bone3, color="green", r=[ 1.5,0.5]
    );

    bone4= trslN(bone, 24);
     Mark( bone4, color="blue", r=[ 0.5, 2.5, 2.5,0.5], nside=12
    );    

}
//chaindata_demo_r_is_arr();

module chaindata_demo_rs_debug(){
    /* BUG:
         
    When rs is given as [ r,r,r, ...] (i.e., all the same), 
    Chain( pts, rs=rs ) fails to show the turn with consistent r. 
    
    */
    echom("chaindata_demo_rs_debug");
    
    n=5;
    r=0.5; 
    
    //pts = randPts(n);
    seed= pqr();
    pts = hash( chainBoneData( seed=seed, n=n, seglen=4, a=[100,280], a2=120 ), "pts");
    pts = [[2.79341, -2.61265, -1.25961], [-1.05696, -1.78544, -1.95988], [-1.64317, -4.13419, 1.22441], [0.126422, -6.87174, -1.09386], [-3.83384, -7.21593, -0.649088]];
    //pts= [[-0.0224354, -0.403744, -2.10676], [2.38007, -2.17212, -2.72282], [0.513128, 2.01547, -0.105874]];
    //pts2 = [[2.63305, -2.61629, -0.179879], [2.622, 1.876, 1.73897], [-2.01246, -2.62299, -1.756], [-2.10277, 1.70691, 0.163054], [0.0288981, -0.754972, -1.78388], [-0.538602, 2.76021, -0.929619]];
    //pts =  [[0.928418, -1.66482, -2.15952], [-1.74313, -0.844951, 2.19403], [-2.15605, -0.689082, 1.14351], [-0.336884, -2.66913, 2.71468]];
    echo( pts = pts );
    //MarkPts( pts ); 
    
    
    //Chain(pts, r=r);
    
    
    pts2= trslN(pts, 4);
    MarkPts( pts2, "ch" );
    rs2 = repeat( [r], len(pts2) );
    chdata= chaindata( pts2, r=rs2); //rs=rs2, r=0.1);
    echo( chdata = chdata );
    
    //pl90
    //MarkPts( [[-2.36088, -7.2422, -1.23635], [-1.30608, -4.97619, -4.35926], [-1.36836, -5.77388, -4.95911]], "ch;plane");
    
    // cut90
//    MarkPts( [[-1.4643, -5.3161, -3.89082], [-1.34345, -5.45481, -4.71917], [-1.14786, -4.63629, -4.8277], [-1.26871, -4.49758, -3.99935]], "ch;plane");
    
    // cut90_r_adjusted
//    MarkPts( [[2.04876, -5.74316, -3.68765], [2.0637, -5.76031, -3.79005], [2.08788, -5.65913, -3.80346], [2.07294, -5.64198, -3.70106]], "ch;plane");
    
    //MarkPts( lastcut, "ch;l");
    //MarkPts( lastcut_r_adjusted, "ch;ball=false;l");
    //MarkPts( lastcut_projected, "ch;l");
    
    Chain( chdata, "trp=0.6" );
    
}
//chaindata_demo_rs_debug();

module Chain_demo_rs_is_arr(){
    
 echom("Chain_demo_rs_is_arr");   
 nseg = 16;
 nside= 4;
 r = 1.5;
 rlast=r/20; 
    
    rndch=chainBoneData( nseg+1, seglen=2, a=[ 185,170], a2=[-30,30]
                   );
    bone = hash( rndch, "pts" );
    //echo(lenbone=len(bone), bone = bone);
    Chain( bone );
    
    //---------------------------------------------
    n_per_cycle= 6;
    dr = abs((rlast-r)/len(bone));
    rs = [ for (i=range(bone))
            (r-i*dr)/r *sin( i*360/n_per_cycle)+r
            
         ];
    echo( rs = rs );
    //echo(len_bone= len(bone), range(bone), len_rs=len(rs), rs=rs);
    bone2= trslN(bone, 8);    
    Chain( bone2, color="red", r=rs );
        
    
    bone3= trslN(bone, 16);
     Chain( bone3, "cl=green", r=[ 1.5,0.5]
    );

    bone4= trslN(bone, 24);
     Chain( bone4, "cl=blue", r=[ 0.5, 2.5, 2.5,0.5], nside=12
    );    

}
//Chain_demo_rs_is_arr();

module chaindata_demo_r_has_arr(){
    
 echom("chaindata_demo_r_has_arr");   
 nseg = 16;
 nside= 4;
 r = 1.5;
 rlast=r/20;   
 //nsmooth=8;   
    
    module Mark( bone, rlast=undef, r=r, tilt=0,tiltlast=0
               ,color=undef, label=""){
        
        MarkPts( bone, "ch=[r,0.03]");
        chrtn = chaindata( bone
                       , r=r
                     , nside=nside );
        //echo( chrtn = chrtn );           
        pts= hash(chrtn,"cuts");
        echo( len_pts = len(pts) );     
        //echo( debug = hash(chrtn, "debug"));

        MarkPts( pts[0], "ch");

        faces = faces("chain", nside=nside
        , nseg= len(pts)-1); //nseg);
        color(color,1)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    
    seed= pqr();
    
    //bone = arcPts( pqr(), a=360*(nseg-1)/nseg
    //               , rad= 3 );
    rndch=chainBoneData( seed=seed, nseg+1
    , seglen=2, a=[ 183,177], a2=[-20,20]
                   );
    bone = hash( rndch, "pts" );
    echo(lenbone=len(bone), bone = bone);
    // Mark( bone  );
    //---------------------------------------------
 
    bone5= trslN(bone, 8);
     Mark( bone5, color="red", r=[ [1,2], [2,1]], nside=12);
    
    bone51= trslN(bone, 16);
     Mark( bone51, color="green", r=[ [0.2,1],2.5, [1,0.2]], nside=12);

    bone52= trslN2(bone51, 8);
     Mark( bone52, color="blue", r=[ [0.2,1],1,2.5,1,[1,0.2]], nside=12);
    
    
    rndch53=chainBoneData( seed= trslN2( p012(bone51), 16)
                     , n=24, seglen=2, a=[ 183,177], a2=[-20,20]
                     , lens= concat( repeat([0.4],6), [6])
                   );
    bone53 = hash( rndch53, "pts" );
    
    Mark( bone53, nseg=23, color="darkcyan" //, r=0.8
         , r= [[0.2,0.5],1, 1.2, 1.3, 1.2, 1, [0.5,0.2] ] 
         , nside=12);
    //echo(lens_bone53= hash();
    
    bone60= trslN(bone, 28);
    rndch2= chainBoneData( seed=bone60, a=180,a2=0, n=8
                      , lens= repeat([0.8,5],4)
                      );
    bone601= hash( rndch2, "pts");
    Mark( bone601, color="purple", r=[ [0.2,2], [2,0.2]], nside=12);
    
    bone6= trslN(bone, 38);
    bone61= [ bone6[0]
            , onlinePt( p01(bone6), 2)
            , onlinePt( p01(bone6), 22)
            , onlinePt( p01(bone6), 24)
            ];
    Mark( bone61, color="darkcyan", r=[ 2,[2,1], [1,2],2], nside=36
            
    );    
    //---------------------------------------------

}
//chaindata_demo_r_has_arr(); //57"

module Chain_demo_r_has_arr(){
    
 echom("Chain_demo_r_has_arr");   
 nseg = 10;
 nside= 4;
 r = 1.5;
 rlast=r/20;   
 //nsmooth=8;   
    
        
    seed= pqr();
    
    rndch=chainBoneData( seed=seed, nseg+1
    , seglen=2, a=[ 183,177], a2=[-20,20]
                   );
    bone = hash( rndch, "pts" );
    echo(lenbone=len(bone), bone = bone);
    Chain( bone  );
    //---------------------------------------------
    n_per_cycle= 6;
    dr = abs((rlast-r)/len(bone));
    rs = [ for (i=range(bone))
            (r-i*dr)/r *sin( i*360/n_per_cycle)
            
         ];
 
    //---------------------------------------------
    bone5= trslN(bone, 8);
     Chain( bone5, "cl=red", r=[ [0.5,1], [1,0.5]], nside=12);
//    
    //---------------------------------------------
    bone51= trslN(bone, 16);
     Chain( bone51, "cl=green", r=[ [0.5,1],2.5, [1,0.5]], nside=12);

    //---------------------------------------------
    bone52= trslN2(bone51, 8);
     Chain( bone52, "cl=blue", r=[ [0.5,1],1,2.5,1,[1,0.5]], nside=12);
        
    //---------------------------------------------
    rndch53=chainBoneData( seed= trslN2( p012(bone51), 16)
                     , n=24, seglen=2, a=[ 183,177], a2=[-20,20]
                     , lens= concat( repeat([0.8],6), [6])
                   );
    //rndch53= trslN2( bone51, 16);
    //bone53 = hash( rndch53, "pts" );
    //bone53 = trslN2( bone51, 16);
    
    Chain( rndch53, nseg=23, "cl=darkkhaki"
         , r= [[0.2,0.5],1, 1.2, 1.3, 1.2, 1, [0.5,0.2] ] 
         , nside=12);
    //echo(lens_bone53= hash();
    
    //---------------------------------------------
    bone60= trslN(bone, 28);
    rndch2= chainBoneData( seed=bone60, a=180,a2=0, n=8
                      , lens= repeat([0.8,5],4)
                      );
    //---------------------------------------------
    bone601= hash( rndch2, "pts");
    Chain( bone601, "cl=purple", r=[ [0.8,2], [2,0.8]], nside=12);
    
    //---------------------------------------------
    bone6= trslN(bone, 38);
    bone61= [ bone6[0]
            , onlinePt( p01(bone6), 2)
            , onlinePt( p01(bone6), 22)
            , onlinePt( p01(bone6), 24)
            ];
    Chain( bone61, "cl=darkcyan", r=[ 2,[2,0.8], [0.8,2],2], nside=36
            
    );    
    //---------------------------------------------

}
//Chain_demo_r_has_arr();

module chaindata_demo_closed(){
    
 echom("chaindata_demo_closed");   
 nseg = 6;
 nside= 4;
 r = 1.5;
 rlast=r/20;   
 nsmooth=8;   
    
    module Mark( bone, closed , smooth
               ,color=undef, label=""
               , testing=undef){
        echom("Mark");
        MarkPts( bone, "ch=[r,0.03];r=0.2");
        //echo(lenbone=len(bone), bone = bone);           
        chrtn = chaindata( bone
                      , closed=closed
                      , smooth=smooth
                      , r=r, nside=nside );
        _pts= hash(chrtn, "cuts");
        //echo(debug= hash(chrtn, "debug"));   
        pts= _pts; //testing? slice(_pts,1,-1):_pts;
                   
      
        // Mark beg and ending faces
        //fopt= str( "pl=[color,black];"
        //         , "ch=[r,0.05,color,blue,closed,true]");
        fopt= str( "ch=[r,0.04,color,blue,closed,true];r=0.15");
        MarkPts( pts[0],fopt);
        MarkPts( last(pts),fopt);
       
        //echo(_pts=_pts, pts= arrprn(pts,3));              
        //MarkPts(last(pts), "ch" );   
        
        for(xs=pts) MarkPts(xs,"ch");
        
        // Reference plane
        //color("blue",0.5)  
        //Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);

        color("blue",1){
          Plane( pts[0] );
          Plane( last(pts));
        }  
        
        
            /* tailroll

            tailroll is the amount(int) of pts for the tailface
            to roll in order to match the tail face to the head
            face to overcome twisting in cases of closed chain

            twistangle 

            */
            //MarkPts( bone, "l");
  
            closinglink= [  pts[0][0]
                            , bone[0]
                            , last(bone)
                            , last(pts)[0]
                          ]; 
            twistangle = twistangle(closinglink);
            //twistangle = _twistangle; //?_twistangle:0;

            tailroll =  -floor( twistangle/ ( 360/ nside));

            echo(closinglink = closinglink
                 ,twistangle = twistangle
                 , tailroll=tailroll);

        faces = faces( closed?"ring":"chain", nside=nside
                     , tailroll = tailroll
                     , nseg= smooth? nseg*(nsmooth+2):nseg);
        color(color,0.9)   
        polyhedron( points= joinarr(pts), faces=faces);  
       
// pl90= [[-7.31704, 2.96341, -8.05994], [-8.17659, 1.84222, -7.99578], [-8.02258, 1.6445, -9.3876], [-7.16303, 2.76568, -9.45177]];
//        
//    MarkPts( pl90, "ch=[r,0.05];" );
//        
//    pl_0=[[-6.52969, 2.75568, -7.64358], [-7.66983, 2.30395, -8.72376], [-8.1766, 1.84222, -7.99576]];
//        
//    MarkPts( pl_0, "ch=[r,0.05];cl=black" );
//        
//    Rf=[-7.8197, 3.72338, -7.92783];
//    MarkPt( Rf, ["r",0.3,"color","gray","transp",0.5]);
//           
//    preP=[-7.57812, 1.4354, -9.2108];
//    MarkPt( preP, ["r",0.3,"color","green","transp",0.2]);
    }
    
    
    //bone = arcPts( pqr(), a=360*(nseg-1)/nseg
    //               , rad= 3 );
    rndch=chainBoneData( nseg+1, seglen=5, a=[ 120,150], a2=[15,45]
                 // , smooth=["n",nsmooth] 
                 );
    bone = hash( rndch, "pts" );
//  The following bone is closed with a link that   
//    bone = [[-0.72711, -2.30335, 0.638652], [3.10403, -0.407617, 3.23261], [7.94094, -1.46607, 3.92826], [10.9758, -3.89925, 0.786742], [9.10391, -7.97636, -1.42078], [4.25715, -9.19902, -1.53911], [1.6016, -6.29077, 1.5415]];
    
    echo(lenbone=len(bone), bone = bone);
    Mark( bone 
         //,label="rlast=r/20" 
         );
    //---------------------------------------------

     
     Mark( trslN(bone, 10) , color="darkkhaki", closed=true
         //,label="rlast=r/20" 
    );
   
    //---------------------------------------------

//    _bone1= trslN(bone, 12);
//    bone1 = concat( [last(_bone1)], _bone1, [_bone1[0]] );  
//    Mark( bone1, color="red", closed=true,testing=true);

    //---------------------------------------------

//    bone2= smoothPts(trslN(bone, 24), n=nsmooth, closed=true);
//     Mark( bone2, color="green", closed=true, smooth=true
//         //,label="rlast=r/20" 
//    );         
    //---------------------------------------------

}
//chaindata_demo_closed();

module Chain_demo_closed(){
    
 echom("Chain_demo_closed");   
 nseg = 6;
 nside= 4;
 r = 1.5;
 rlast=r/20;   
 nsmooth=8;   
    
 
    rndch=chainBoneData( nseg+1, seglen=5, a=[ 120,150], a2=[15,45]
                 );
    bone = hash( rndch, "pts" );
    MarkPts( bone,"ch" );
    
    echo(lenbone=len(bone), bone = bone);
    Chain( bone , r=2, "transp=0.5"
         );
    //---------------------------------------------

    bone2=  trslN2(bone, 10);
    MarkPts( bone2,"ch" );
     Chain( bone2 , r=2, "cl=darkcyan;transp=0.5", closed=true
    );
   
    //---------------------------------------------

    bone1= trslN2(bone, 20);
    //bone1 = concat( [last(_bone1)], _bone1, [_bone1[0]] );  
    Chain( bone1, "cl=red", nside=24, r=2, closed=true,testing=true);

    //---------------------------------------------

    bone3= smoothPts(trslN2(bone, 30), n=nsmooth, closed=true);
     Chain( bone3, "cl=green", r=2, closed=true, smooth=true
         //,label="rlast=r/20" 
    );         
    //---------------------------------------------

}
//Chain_demo_closed();

module chaindata_demo_twist(){
    
 echom("chaindata_demo_twist");   
 nseg = 7;
 nside=4;
 r = 1;
 nsmooth=20;
    
    module Mark( bone, rlast, twist=0
               ,color=undef, label=""){
        
        MarkPts( bone, "ch=[r,0.03]");
        chrtn = chaindata( bone
                      , twist=twist
                      , r=r, rlast=rlast?rlast:r, nside=nside );
        pts= hash(chrtn,"cuts");
          
        edges = transpose(pts);
 
        MarkPt( last(bone)
           , ["label",label,"shift",0.5]);

        //echo(pts2= arrprn(pts,3));              
        MarkPts(pts[0], "ch=[color,blue,r,0.03,closed,true]" );   
        MarkPts(last(pts), "ch=[color,blue,r,0.03,closed,true]" );   
        
        
        // Mark seed plane
        MarkPts( [ bone[0], pts[0][0]]
                 , "ch=[r,0.03,color,red]");
        MarkPts( [ last(bone), last(pts)[0]]
                 , "ch=[r,0.03,color,red]");
               
        // Reference plane
        color("blue",0.5)  
        Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
        
        faces = faces("chain", nside=nside
                     , nseg=len(pts)-1); //nseg);
        color(color,1)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    

    rndch=chainBoneData( nseg+1, seglen=1.5, a=[ 185,170], a2=[-60,60] );
    bone= hash(rndch, "pts");
    
    Mark( bone );

    //---------------------------------------------
    // Set twist 
    bone2= trslN(bone, 6);
     Mark( bone2, twist=135, color="red" 
         );
    //---------------------------------------------
    bone3= smoothPts( trslN(bone, 12) );
     Mark( bone3, twist=-135, rlast=0.1, color="green" 
         );

}
//chaindata_demo_twist();

module Chain_demo_twist(){
    
 echom("Chain_demo_twist");   
 nseg = 7;
// nside=4;
// r = 1;
// nsmooth=20;
    

    rndch=chainBoneData( nseg+1, seglen=1.5, a=[ 185,170], a2=[-60,60] );
    bone= hash(rndch, "pts");
    
    Chain( bone );

    //---------------------------------------------
    // Set twist 
    bone2= trslN(bone, 6);
     Chain( bone2, twist=135, "cl=red" 
         );
    //---------------------------------------------
    bone3= smoothPts( trslN(bone, 12) );
     Chain( bone3, twist=-135, rlast=0.1, "cl=green" 
         );

}
//Chain_demo_twist();


module chaindata_demo_cut0(){
    
 echom("chaindata_demo_cut0");   
 nseg = 5;
 nside=4;
 r = 1.5;
 nsmooth=8;
    
    module Mark( bone, cut0, rot=0, twist=0,
               ,color=undef, label=""){
        echom("Mark");
        echo( len_cut0=len(cut0), cut0 = cut0 );
        MarkPts( bone, "ch=[r,0.05,color,red];r=0.08");
        chrtn = chaindata( bone, cut0=cut0
                      , twist=twist, rot=rot
                      //, r=r
                   , nside=nside );
        echo(chrtn = chrtn );
        pts= hash(chrtn, "cuts");

        
        Rf= hash(chrtn,"Rf");
        MarkPt(Rf, "cl=gray;r=0.2;trp=0.2");
           
        MarkPts(pts[0], "ch=[color,blue,r,0.04,closed,true]" );   
        MarkPts(last(pts), "ch=[r,0.02,closed,true]" );   
        
        
        // Mark seed plane
        MarkPts( [ bone[0], pts[0][0]]
                 , "ch=[r,0.03,color,red]");
        MarkPts( [ last(bone), last(pts)[0]]
                 , "ch=[r,0.03,color,green]");
               
        // Reference plane
        color("blue",0.5)  
        Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
        
        faces = faces("chain", nside= cut0?len(cut0):nside
                     , nseg=len(pts)-1); //nseg);
        //echo( len_cut0= len(cut0), faces = arrprn(faces,3));             
        color(color,0.8)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    
    //bone = arcPts( pqr(), a=360*(nseg-1)/nseg
    //               , rad= 3 );
    rndch=chainBoneData( nseg+1, seglen=5, a=[ 185,170], a2=[-30,30] );
    bone= hash(rndch,"pts"); //smoothPts( _bone, n=nsmooth );
    
//    bone=[ [1.01829, 0.792751, -1.60139]
//, [-0.125335, -0.630424, -1.68873]
//, [-1.25707, -0.637699, -0.777778]
//, [-1.93241, -0.644702, 0.373311]
//];
    
    //Mark( bone );
    //---------------------------------------------

    // Set cut
    cut0= [ [1,0],[1,1.8],[2,2],[1.8,3],[1,4],[0,0]]; 
    //MarkPts( addz(cut0), "ch;l");
    bone2= trslN(bone, 7);
    Mark( bone2, cut0=cut0 );

    //---------------------------------------------
    bone4= trslN(bone, 14); 
            //smoothPts( trslN(bone, 15) );
     Mark( bone4, rot=45, color="green" 
         , cut0=cut0
         );
    //---------------------------------------------
    bone3= smoothPts(trslN(bone, 21),n=3); 
            //smoothPts( trslN(bone, 15) );
     Mark( bone3, twist=135, color="darkcyan" 
         , cut0=cut0
         );         
    //---------------------------------------------
    bone5= trslN(bone, 28); 
     Mark( bone5, color="darkcyan" 
         , cut0= smoothPts( addz(cut0,0),n=3,closed=true )
         );

    //---------------------------------------------
    /*
        -2       1      3
             _o_            2
          _-' | `o------o   1
         o----+---------|  
          `-_ | _-------o
             `o`
    */
    
    cut6= [ [3,0], [3,0.5],[1,0.5],[0,2],[-2,0],[0,-2],[1,-0.5],[3,-0.5]];
    MarkPts( addz(cut6), "ch;l");  
    bone6= smoothPts(trslN(bone, 40), n=2); 
    //bone6= trslN(bone, 40); 
     Mark( bone6
         , twist=360, cut0= cut6
         );

//    //---------------------------------------------
    
    pts = [[2.79341, -2.61265, -1.25961], [-1.05696, -1.78544, -1.95988], [-1.64317, -4.13419, 1.22441], [0.126422, -6.87174, -1.09386], [-3.83384, -7.21593, -0.649088]];
    cut7 = [ for(p=cut6) p/3 ];
    Chain( pts,  cut0=cut7);
    
    // Try the following. They work, but might not be pretty at
    // this resolution.
    //Chain( pts,  closed=true, cut0=cut7);
    //Chain( pts, twist=45, cut0=cut7);
    
    
}
//chaindata_demo_cut0();

module Chain_demo_cut0(){
    
 echom("Chain_demo_cut0");   
 nseg = 5;
 nside=4;
 r = 1.5;
 nsmooth=8;
    
    rndch=chainBoneData( nseg+1, seglen=5, a=[ 185,170], a2=[-30,30] );
    bone= hash(rndch,"pts"); //smoothPts( _bone, n=nsmooth );
    
    //Mark( bone );
    //---------------------------------------------

    // Set cut
    cut0= [ [1,0],[1,1.8],[2,2],[1.8,3],[1,4],[0,0]]; 
    MarkPts( addz(cut0), "ch;l");
    bone2= trslN(bone, 7);
    Chain( bone2, cut0=cut0 );

    //---------------------------------------------
    bone4= trslN(bone, 14);     
     Chain( bone4, rot=45, "cl=green" 
         , cut0=cut0
         );
    //---------------------------------------------
    bone3= smoothPts(trslN(bone, 21),n=3);
     Chain( bone3, twist=135, "cl=darkcyan" 
         , cut0=cut0
         );         
    //---------------------------------------------
    bone5= trslN(bone, 28); 
     Chain( bone5, "cl=darkcyan" 
         , cut0= smoothPts( addz(cut0,0),n=3,closed=true )
         );

    //---------------------------------------------
    /*
        -2       1      3
             _o_            2
          _-' | `o------o   1
         o----+---------|  
          `-_ | _-------o
             `o`
    */
    
    cut6= [ [3,0.5],[1,0.5],[0,2],[-2,0],[0,-2],[1,-0.5],[3,-0.5]];
      
    bone6= smoothPts(trslN(bone, 40), n=8); 
     Chain( bone6
         , twist=360, cut0= cut6
         );

    pts = [[2.79341, -2.61265, -1.25961], [-1.05696, -1.78544, -1.95988], [-1.64317, -4.13419, 1.22441], [0.126422, -6.87174, -1.09386], [-3.83384, -7.21593, -0.649088]];
    cut7 = [ for(p=cut6) p/3 ];
    Chain( pts,  cut0=cut7);
    
    // Try the following. They work, but might not be pretty at
    // this resolution.
    //Chain( pts,  closed=true, cut0=cut7);
    //Chain( pts, twist=45, cut0=cut7);
}
//Chain_demo_cut0();


module chaindata_demo_cut0_2(){
    
 echom("chaindata_demo_cut0_2");   
 nseg = 5;
 nside=4;
 r = 1.5;
 nsmooth=8;
    
    module Mark( bone, cut0, rot=0, twist=0,
               ,color=undef, label=""){
        echom("Mark");
        echo( len_cut0=len(cut0), cut0 = cut0 );
        MarkPts( bone, "ch=[r,0.05,color,red];r=0.08");
        chrtn = chaindata( bone, cut0=cut0
                      , twist=twist, rot=rot
                      , r=r, nside=nside );
        pts= hash(chrtn, "cuts");
              
                   
        // frame           
//        edges = transpose(pts);
//        for( i=range(edges))
//           Chain( edges[i]
//             , ["color",i==0?"blue":"green",
//             "r", i==0?0.03:0.01]);
//        Chain( edges[0]
//             , ["color","blue"
//               ,"r", 0.03]);
           
                   
        //for( cut = pts) MarkPts(cut, "ch=[closed,true]"); 
        
        Rf= hash(chrtn,"Rf");
        MarkPt(Rf, "cl=gray;r=0.2;trp=0.2");
        
        //cut0= hash(chrtn, "cut0");
        
        //echo( debug = hash(chrtn, "debug"));
        
        
        
       // MarkPt( last(bone)
       //    , ["label",label,"shift",0.5]);
        
        // tilt axis 
        //tiltaxes= chrtn[1];
        //MarkPts( tiltaxes[0], "ch=[r,0.03]");
//        if(tilt)        
//        Line0( linePts( tiltaxes[0],len=[0.5,r+0.5])
//           ,["r",0.02,"color","black","transp",0.5]);            
////        Line0( linePts([pts[0][1],pts[0][3]],len=0.5)
////           ,["r",0.02,"color","black","transp",0.2]);
        
        //echo(pts2= arrprn(pts,3));  
        echo( len_pts = len(pts) );        
        MarkPts(pts[0], "ch=[color,blue,r,0.04,closed,true]" );   
        MarkPts(last(pts), "ch=[r,0.02,closed,true]" );   
        
        
        // Mark seed plane
        MarkPts( [ bone[0], pts[0][0]]
                 , "ch=[r,0.03,color,red]");
        MarkPts( [ last(bone), last(pts)[0]]
                 , "ch=[r,0.03,color,green]");
               
        // Reference plane
        color("blue",0.5)  
        Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
        
        faces = faces("chain", nside= cut0?len(cut0):nside
                     , nseg=len(pts)-1); //nseg);
        echo( len_cut0= len(cut0), faces = arrprn(faces,3));             
        color(color,0.8)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    
    //bone = arcPts( pqr(), a=360*(nseg-1)/nseg
    //               , rad= 3 );
    rndch=chainBoneData( nseg+1, seglen=5, a=[ 185,170], a2=[-30,30] );
    bone= hash( rndch, "pts") ; //smoothPts( _bone, n=nsmooth );
    
    
//    bone=[ [1.01829, 0.792751, -1.60139]
//, [-0.125335, -0.630424, -1.68873]
//, [-1.25707, -0.637699, -0.777778]
//, [-1.93241, -0.644702, 0.373311]
//];
    
    //Mark( bone );
    //---------------------------------------------

    //---------------------------------------------
    /*
        -2       1      3
             _o_            2
          _-' | `o------o   1
         o----+---------|  
          `-_ | _-------o
             `o`
    */
    
    // make a circle:
    c_nside=24;
//    cir = arcPts( [ [0,0,1], ORIGIN, [1,0,0] ], nside=c_nside, a=90
//                , a2 = 360/c_nside, rad= 2 
//                );
    cir = [ for(i=[ 0: c_nside-2])
            anglePt( [ [0,0,1], ORIGIN, [1,0,0] ]
                   , a=90, a2 = (i+1)*360/c_nside, len=1 )
          ];
        
    cir2 = concat( [[ 4, cir[0].y,0]], cir, [[ 4, -cir[0].y,0]] );
    echo( cir2 = cir2);
    MarkPts( cir2, "ch;l");
    //cut6= [ [3,0.5],[1,0.5],[0,2],[-2,0],[0,-2],[1,-0.5],[3,-0.5]];
      
    bone6= smoothPts(trslN(bone, 10), n=20); 
     Mark( bone6
         , twist=720, cut0= cir2 //cut6
         );

}
//chaindata_demo_cut0_2(); // could take >= 3.5 MIN ///1.5 min

module Chain_demo_cut0_2(){
    
 echom("Chain_demo_cut0_2");   
 nseg = 5;
 nside=4;
 r = 1.5;
 nsmooth=8;
    

    rndch=chainBoneData( nseg+1, seglen=5, a=[ 185,170], a2=[-30,30] );
    bone= hash( rndch, "pts") ; //smoothPts( _bone, n=nsmooth );
    
    //---------------------------------------------
    /*
        -2       1      3
             _o_            2
          _-' | `o------o   1
         o----+---------|  
          `-_ | _-------o
             `o`
    */
    
    // make a circle:
    c_nside=24;
//    cir = arcPts( [ [0,0,1], ORIGIN, [1,0,0] ], nside=c_nside, a=90
//                , a2 = 360/c_nside, rad= 2 
//                );
    cir = [ for(i=[ 0: c_nside-2])
            anglePt( [ [0,0,1], ORIGIN, [1,0,0] ]
                   , a=90, a2 = (i+1)*360/c_nside, len=1 )
          ];
        
    cir2 = concat( [[ 4, cir[0].y,0]], cir, [[ 4, -cir[0].y,0]] );
    echo( cir2 = cir2);
    MarkPts( cir2, "ch;l");
      
    bone6= smoothPts(trslN(bone, 10), n=20); 
     Chain( bone6
         , twist=720, cut0= cir2 //cut6
         );

}
//Chain_demo_cut0_2(); // could take more than 1.5 min

module chaindata_demo_cut0_ring(){
 // This is the case in :
 //http://forum.openscad.org/Please-help-with-path-extrusion-td13322.html
   
 echom("chaindata_demo_cut0_ring");  
    
    module Mark( bone, cut0){
        echom("Mark");
        echo( len_cut0=len(cut0), cut0 = cut0 );
        //MarkPts( bone, "ch=[r,0.05,color,red];r=0.08");
        chrtn = chaindata( bone, cut0=cut0, closed=true);
        //echo(chrtn = chrtn );
        pts= hash(chrtn, "cuts");
        MarkPts( pts[0], "ch;l");      
        nseg = len(bone);           

        Rf= hash(chrtn,"Rf");
        faces = faces("ring", nside= cut0?len(cut0):nside
                     , nseg=nseg);
        //echo( len_cut0= len(cut0), faces = arrprn(faces,3));             
        color(color,1)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    n = 6;

    bone = circlePts( pqr=[ORIGIN, [0,0,5], [0,5,5]], rad=3, n=6);
    MarkPts( bone, "ch");
//http://forum.openscad.org/Please-help-with-path-extrusion-td13322.html
    cut0_rough=[    
                [10.00,0.00]
                ,[12.00,0.00]
                ,[12.00,4.00]
                ,[14.00,4.00]
                ,[14.00,6.00]
                ,[16.00,6.00]
                ,[16.00,4.00]
                ,[18.00,4.00]
                ,[18.00,10.00]
                ,[17.00,12.80]
                ,[16.00,14.20]
                ,[15.00,15.00]
                ,[13.00,15.70]
                ,[11.00,15.95]
                ,[10.00,16.00]
                ,[10.00,0.00]
                ];
                
    cut0 = cut0_rough; //[ for(p=cut0_rough) -p/10 ];
    echo( cut0 = cut0 );

    chrtn = chaindata( bone, cut0=cut0, closed=true);
    Chain( chrtn, closed=true );
        //echo(chrtn = chrtn );
//        pts= hash(chrtn, "cuts");
//        MarkPts( pts[0], "ch;l");      
//        nseg = len(bone);           
//
//        Rf= hash(chrtn,"Rf");
//        faces = faces("ring", nside= cut0?len(cut0):nside
//                     , nseg=nseg);
//        //echo( len_cut0= len(cut0), faces = arrprn(faces,3));             
//        color(color,1)   
//        polyhedron( points= joinarr(pts), faces=faces);   
        
    //MarkPts( addz(cut0), "ch;l");
    //bone2= bone; //trslN(bone, 7);
    //MarkPts( bone2, "ch;l");
    //Mark( bone2, cut0=cut0 );


}
//chaindata_demo_cut0_ring(); // 

module Chain_demo_cut0_ring(){
 // This is the case in :
 //http://forum.openscad.org/Please-help-with-path-extrusion-td13322.html
   
 echom("Chain_demo_cut0_ring");  
    
    n = 6;

    bone = circlePts( pqr=[ORIGIN, [0,0,5], [0,5,5]], rad=3, n=24);
    cut0_rough= [
        [10.00,0.00]
        ,[12.00,0.00]
        ,[12.00,4.00]
        ,[14.00,4.00]
        ,[14.00,6.00]
        ,[16.00,6.00]
        ,[16.00,4.00]
        ,[18.00,4.00]
        ,[18.00,10.00]
        ,[17.00,12.80]
        ,[16.00,14.20]
        ,[15.00,15.00]
        ,[13.00,15.70]
        ,[11.00,15.95]
        ,[10.00,16.00]
        ,[10.00,0.00]
        ];
    cut0 = cut0_rough; //[ for(p=cut0_rough) -p/10 ];
    echo( cut0 = cut0 );    
    //MarkPts( addz(cut0), "ch;l");
    bone2= bone; //trslN(bone, 7);
    //MarkPts( bone2, "ch;l");
    Chain( bone2, cut0=cut0, closed=true );

}
//Chain_demo_cut0_ring(); 

module chaindata_demo_cone(){

    echom("chaindata_demo_cone");
    nseg=8;
    nside=30;
    r=1;

    module Mark( bone,cut0, rlast, twist, nside=nside, r,color=undef, label=""){
        
       // MarkPts( bone, "ch=[r,0.03]");
        //Chain0( bone ); 
        
        chrtn = chaindata( bone, r=r, nside=nside, twist=twist, nside=nside );
        pts= hash(chrtn, "cuts");
       // echo(pts= arrprn(pts,2));

        MarkPts( pts[0], "ch=[color,blue,closed,true]");

        // Reference plane
        color("blue",0.5)  
        Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
        
        faces = faces("cone", nside= cut0?len(cut0):nside
                     , nseg=len(pts)-1); //nseg);
        color(color,1)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    pqr=pqr();
    rndch = chainBoneData(n=nseg+1, seed=pqr, 
                    seglen=1.5,a=[170,185],a2=[-13,30]);
    bone = hash( rndch, "pts");
    echo(bone=bone);
    
    //==========================================
    //Mark( bone, nside=6 ); 
    Chain(bone, r=1, rlast=0);

    //==========================================
    bone2 = trslN( bone, 6 );
    Mark( bone2, twist=90, nside=6, color="red" );

    
    //==========================================
    rs3 =  concat( [2.2, [2.2,1]], repeat([1],4), [[1,1.6],1.3, 1.15,1]);
    echo(rs3=rs3); 
    bone3 = trslN( bone, 12 );
    Mark( bone3,  r=rs3, nside=30, color="green" ); 
    
    
    //==========================================
    rndch4 = chainBoneData(n=nseg+1, seed= trslN(pqr, 8)
             , seglen=1.5,a=180, a2=360/8/nseg);
    bone4 = hash(rndch4, "pts");
    //MarkPts(bone4,"ch");
    Mark( bone4, nside=30, color="blue" );
    

    //==========================================
    // Take last 3 seg to make a cone
    rs5 =  concat( repeat([0.3],6), 
                   [[0.3,0.6],0.3, 0.3]);
    echo(rs5=rs5);
    rndch5 = chainBoneData(n=nseg+1, seed= trslN(pqr, 16)
        , seglen=1.5, a=180, a2=360/8/nseg);
        
    //MarkPts(bone4,"ch");
    bone5 = hash(rndch5, "pts");
    Mark( bone5,r=rs5, rlast=undef, nside=30, color="darkcyan" );
   
}

//chaindata_demo_cone();

module Chain_demo_cone(){

    echom("Chain_demo_cone");
    nseg=8;
    nside=30;
    r=1;
    
    pqr=pqr();
    rndch = chainBoneData(n=nseg+1, seed=pqr, 
                    seglen=1.5,a=[170,185],a2=[-13,30]);
    bone = hash( rndch, "pts");
    //echo(bone=bone);
    
    Chain( bone, nside=6 ); 

    bone2 = trslN( bone, 6 );
    Chain( bone2, twist=90, nside=6, "cl=red" );

    
    
    rs3 =  concat( [1.2, [1.2,0]], repeat([0],3), [[0,.6],0.3, 0.15,0]);
    bone3 = trslN( bone, 12 );
    Chain( bone3,  r=rs3, nside=4, "cl=green" ); 


    // Take last 3 seg to make a cone
    rs5 =  concat( repeat([0],6), 
                   [[0,0.3],0, - 0.3]);
    rndch5 = chainBoneData(n=nseg+1, seed= trslN(bone, 16)
        , seglen=1.5, a=180, a2=360/8/nseg);
    bone5 = hash(rndch5, "pts");
    Chain( bone5,r=rs5, rlast=undef, nside=12, "cl=blue" );
   
}
//Chain_demo_cone();

module chaindata_demo_conedown(){

    echom("chaindata_demo_conedown");
    nseg=6;
    nside=30;
    //r=1;
    rlast=1;

    module Mark( bone,cut0, r=0, twist, nside=nside, rs,color=undef, label=""){
        
        MarkPts( bone, "ch=[r,0.03]");
        //Chain0( bone ); 
        
        chrtn = chaindata( bone, r=r, rlast=rlast, nside=nside, rs=rs, twist=twist, nside=nside );
        pts= hash(chrtn, "cuts");
        //echo(pts= arrprn(pts,2));

//        //MarkPts( joinarr( pts), "l;cl=black;ball=false");
//        
//        MarkPts( pts[0], "ch=[color,blue,closed,true]");
//
//        MarkPts( last(pts), "l")
//    
        // Reference plane
        color("blue",0.5)  
        Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
        
        faces = faces("conedown", nside= cut0?len(cut0):nside
                     , nseg=len(pts)-1); //nseg);
        //echo( len_faces=len(faces), faces = arrprn(faces,2));
        color(color,1)   
        polyhedron( points= joinarr(pts), faces=faces);   
    }
    
    pqr=pqr();
    rndch = chainBoneData(n=nseg+1, seed=pqr, 
                    seglen=1.5,a=[170,185],a2=[-13,30]);
    bone= hash(rndch, "pts");
    echo(bone=bone);
    
    Mark( bone, nside=6 ); 
    
    //---------------------------------------
    bone2 = trslN( bone, 4 );
    Mark( bone2, twist=90, nside=6, color="red" );

//    rs3 =  concat( [1.2, [1.2,0]]
//              , repeat([0],3), [[0,.6],.3, 0.15,0]);
//    echo(rs3=rs3); 
//    bone3 = trslN( bone, 12 );
//    Mark( bone3,  rs=rs3, nside=30 ); 
//    
//    rndch4 = chainBoneData(n=nseg+1, seed= trslN(pqr, 8)
//        , seglen=1.5,a=180, a2=360/8/nseg);
//    bone4= hash(rndch4,"pts");
//    Mark( bone4, nside=30 );
//    
//
    // Take 1st 3 seg to make a cone
    rs5 =  concat( [- 0.3, 0, [0.3,0]], repeat([0],nseg-2)
                   );
    echo(rs5=rs5);
    rndch5 = chainBoneData(n=nseg+1, seed= trslN(bone, 8)
        , seglen=1.5, a=180, a2=360/8/nseg);
        
    bone5= hash(rndch5,"pts");
    Mark( bone5,rs=rs5,r=0.3, rlast=0.3, nside=6, color="green" );
   
}
//chaindata_demo_conedown();


module Chain_demo_conedown(){

    echom("Chain_demo_conedown");
    nseg=6;
    nside=30;
    //r=1;
    rlast=1;
    
    pqr=pqr();
    rndch = chainBoneData(n=nseg+1, seed=pqr, 
                    seglen=1.5,a=[170,185],a2=[-13,30]);
    bone= hash(rndch, "pts");
    echo(bone=bone);
    MarkPts( bone, "ch");
    Chain( bone, nside=6 ); 
    
    //---------------------------------------
    bone2 = trslN( bone, 4 );
    MarkPts( bone2, "ch");
    Chain( bone2, r=0, rlast=1, twist=90, nside=6, color="red" );

    //---------------------------------------
    // Take 1st 3 seg to make a cone
    rs5 =  concat( [- 0.3, 0, [0.3,0]], repeat([0],nseg-2)
                   );
    echo(rs5=rs5);
    rndch5 = chainBoneData(n=nseg+1, seed= trslN(bone, 8)
        , seglen=1.5, a=180, a2=360/8/nseg);
        
    bone5= hash(rndch5,"pts");
    MarkPts( bone5, "ch");
    Chain( bone5,rs=rs5,r=0.3, rlast=0.3, nside=6, color="green" );
   
}
//Chain_demo_conedown();


module chaindata_demo_coln_arrow_old(){

    echom("chaindata_demo_coln_arrow: straight line and arrow");
    nseg=8;
    nside=4;
    r=1;

    module Mark( bone,cut0, twist,r,rlast, rs,color=undef, label=""){
        
        echom("Mark");
        //MarkPts( bone, "ch=[r,0.03]");
        //Chain0( bone ); 
        
        chrtn = chaindata( bone, r=r, rlast=rlast,  rs=rs, twist=twist, nside=nside );
        // this takes a very long time: echofh(chrtn);
        //echo(chrtn = chrtn);
        cuts= hash(chrtn, "cuts");
        //echo(cuts= _colorPts(cuts));
        pts = joinarr( cuts );
        //MarkPts(pts, "ball=false;l");
        
//        for( xs = cuts ) {
//            if( len(xs)==1)
//                MarkPt( xs[0] );
//            else
//              MarkPts( xs, "ch" );
//        }
        
//        MarkPt( last(bone)
//           , ["label",label,"shift",0.5]);
//
//        // Reference plane
//        //color("blue",0.5)  
//        //Plane( [bone[0],bone[1],cuts[1][0],cuts[0][0] ]);
//        
        faces = faces(cuts, nside= cut0?len(cut0):nside
                     , nseg=len(cuts)-1);
        //echo(lenfaces = len(faces), faces= _colorPts( faces )); 
        //nseg);
        color(color,1)   
        polyhedron( points= joinarr(cuts), faces=faces);   
    }
    
    seed=pqr();
    //seed= [[2.31097, -1.6816, 2.70487], [2.53807, 1.92841, -1.50974], [1.02005, 0.826528, 1.66818]];
    echo(seed = seed);
    //seed = [[0.853134, -2.78929, 2.25968], [-0.646558, -0.330627, -0.0296415], [-0.414802, -0.148976, 0.512681]];
    echo(seed=seed);
    
    rndch = chainBoneData(seed=seed, n=nseg+1, seglen=1.5,a=180,a2=0);
    bone = hash( rndch, "pts" );
    echo(bone=bone);
    
    Mark( bone ); 

    bone2 = trslN( bone, 6 );
    Mark( bone2, twist=60,color="red" );

    rs3 =  concat( [1.2, [1.2,0]], repeat([0],5), [[0,1.2],1.2]);
    echo(rs3=rs3); 
    bone3 = trslN( bone, 12 );
    Mark( bone3, twist=60, rs=rs3, color="green" );

    // 
    // Set r=0 allows rs to express all by itself
    // (otherwise each r in rs will have to add r
    //
    rs31 =  concat( [1.2, [1.2,0.5]], repeat([0.5],5), [[0.5,1.2],1.2]);
    echo(rs31=rs31); 
    bone31 = trslN( bone, 18);
    Mark( bone31, twist=60, r=0,rs=rs31, color="darkcyan" ); 
    
    //
    // Make an arrow
    //
    rs4 =  [.5,[.5,1.5],0];
    echo(rs4=rs4); 
    bone4 = onlinePts( p01(trslN( bone, 24)), lens=[0,8,10]);
    //bone4= slice( trslN( bone, 24), 0, len(rs4));
    //echo( bone4= _colorPts(bone4));
    Mark( bone4,  r=0,rs=rs4, color="blue" ); 

    //
    // backward arrow
    //
    rs41 =  [0, [1.5,.3],0.3];
    echo(rs41=rs41); 
    bone41 = onlinePts( p01(trslN( bone, 30)), lens=[0,3,10]);
    //echo( bone41= _colorPts(bone41));
    Mark( bone41,  r=0,rs=rs41, color="purple" ); 

    //
    // Make a 2-headed arrow
    //
    rs5 =  [0, [2,0.5],[0.5,2],0];
    echo(rs5=rs5); 
    bone5 = onlinePts( p01(trslN( bone, 36)), lens=[0, 3,10,13]);
    echo( bone5= _colorPts(bone5));
    Mark( bone5,  r=0,rs=rs5,nside=5, color="darkcyan" ); 


    //
    // Make a spindle
    //
    rs6 =  [0, 1.5,3, 1.5,0];
    echo(rs6=rs6); 
    bone6 = onlinePts( p01(trslN( bone, 42)), lens=[0, 5.5,6,6.5,12 ]);
    echo( bone6= _colorPts(bone6));
    Mark( bone6,  r=0,rs=rs6,nside=8, color="darkcyan" ); 

    
}

//chaindata_demo_coln_arrow_old();

module chaindata_testing_coln_arrow(isdetails, isdraw){  
    echom("chaindata_testing_coln_arrow");

    nseg = 6;
    nside= 4;
    _r = 0.8;
    r = concat( repeat([_r], nseg-1) ,[[_r,_r*2],0]);  
    echo(r = r ); 
    seed= pqr();
    rchrtn = chainBoneData(seed=seed, n=nseg+1, a=180,a2=0); 
    bone1 = hash(rchrtn, "pts");
    echo(bone1=bone1);
    
    //=====================================================
    chdata1 = chaindata( bone1, r= r
                       , nside=nside );
    cuts1 = hash( chdata1, "cuts" );
    //echo( cuts1= arrprn(cuts1));
    setting1= [ "label", "1) nside=4"
              , "color", "olive", "transp",0.5
              , "chdata", update(chdata1, ["shape","cone"])
              //, "shape", "chain"
              , "wants", [ 
                       "a_cut00_bone",90
                     , "a_cut01_bone",90
//                     , "a_cut00_pqr",0
//                     , "a_cut01_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              , "isdetails", 1
              //, "isdraw", 1
              ];
 
    //=====================================================
    r2= concat( [[0, [_r*2,_r]], repeat([_r], nseg-1)]);
    echo(r2=r2);
    chdata2 = chaindata( trslN(bone1,4)
                        , r= r2
                        , nside=nside );
    cuts2 = hash( chdata2, "cuts" );
    setting2= [ "label", "2) closed=true"
              , "color", "red", "transp",0.9
              , "chdata", update(chdata2, ["shape","conedown"])
              //, "shape", "ring"
              , "wants", let( bone= hash(chdata2,"bone")
                            , p3_0 = get3( bone, 0 )
                            , p3_last= get3( bone,-1)
                            )
                     ["a_cut00_bone",90
                     , "a_cut01_bone",90
//                     , "a_cut00_pqr",0
//                     , "a_cut01_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
//              
//    //=====================================================
//    chdata3 = chaindata( trslN(bone1,8), r=r, nside=nside 
//                                          , closed=true, rot=45 );
//    cuts3 = hash( chdata3, "cuts" );
//    setting3= [ "label", "3) closed=true, rot=45"
//              , "color", "green", "transp",0.9
//              , "chdata", update(chdata3, ["shape","ring"])
//              //, "shape", "ring"
//              , "wants", [ 
//                      // "a_cut00_bone",90
//                    // , "a_cut01_bone",90
//                      "a_cut00_pqr", 45
//                     , "a_cut01_pqr",45
//                     , "twist_seg0", 0
//                     //, "a_per_side", 90 
//                     ]
//              //, "isdetails", 1
//              //, "isdraw", 1
//              ];
//    
    //=====================================================
    settings= [setting1
              ,setting2
              //,setting3
              ];
    chaindata_testing( label="chaindata_testing_coln_arrow"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw
                     ); 
    
}
//chaindata_testing_coln_arrow(isdetails=0, isdraw=1);

module chaindata_demo_arrow_0(){
    chbone = chainBoneData( seed=pqr(), seglen=4, n=3, a=180,a2=0);
    bone1 = hash(chbone,"pts");
    chdata = chaindata( bone1, r=[ 1,[1,1.5],0], nside=4 );
    pts = joinarr( hash(chdata,"cuts"));
    //echo(pts = pts );
    //MarkPts( pts, "ch;ball=0");
    faces = faces("cone", nside=4, nseg=3);
    //echo(faces = faces );
    polyhedron(points = pts, faces=faces );
    
    
    bone2 = [ for(p=bone1) p+ [0,5,0]];
    //MarkPts(bone2,"ch;l");
    chdata2 = chaindata( bone2, r=[ 0,[1.5,1],1], nside=3 );
    //echo(chdata2=chdata2);
    pts2 = joinarr( hash(chdata2,"cuts"));
    //echo(pts2 = pts2 );
    //MarkPts( pts2, "ch;ball=0");
    faces2 = faces("conedown", nside=3, nseg=3);
    //echo(faces2 = faces2 );
    color("red",0.8)
    polyhedron(points = pts2, faces=faces2 );

    
    _bone3 = [ for(p=bone2) p+ bone2[0]-bone1[0] ];
    bone3 = concat( [onlinePt( p01(_bone3),-1)], _bone3);
    //echo(bone3=bone3);
    //MarkPts(bone3,"ch;l");
    chdata3 = chaindata( bone3, r=[ 0,[1.5,1],[1,2],0], nside=48 );
    //echo(chdata3=chdata3);
    pts3 = joinarr( hash(chdata3,"cuts"));
    //echo(pts3 = pts3 );
    //MarkPts( pts3, "ch;ball=0");
    faces3 = faces("spindle", nside=48, nseg=5);
    //echo(faces3 = faces3 );
    color("darkcyan",1)
    polyhedron(points = pts3, faces=faces3 );
        
    
}
//chaindata_demo_arrow_0();

module Chain_demo_coln_arrow(){

    echom("Chain_demo_coln_arrow: straight line and arrow");
    nseg=8;
    nside=4;
    r=1;
    
    seed=pqr();
    //seed= [[2.31097, -1.6816, 2.70487], [2.53807, 1.92841, -1.50974], [1.02005, 0.826528, 1.66818]];
    echo(seed = seed);
    //seed = [[0.853134, -2.78929, 2.25968], [-0.646558, -0.330627, -0.0296415], [-0.414802, -0.148976, 0.512681]];
    echo(seed=seed);
    
    rndch = chainBoneData(seed=seed, n=nseg+1, seglen=1.5,a=180,a2=0);
    bone = hash( rndch, "pts" );
    echo(bone=bone);
    
    Chain( trslN(bone,-24) ); 

    bone2 = trslN( bone, -18 );
    Chain( bone2, twist=60,color="red" );

    rs3 =  concat( [1.2, [1.2,0]], repeat([0],5), [[0,1.2],1.2]);
    //echo(rs3=rs3); 
    bone3 = trslN( bone, -12 );
    Chain( bone3, twist=60, rs=rs3, color="green" );

    // 
    // Set r=0 allows rs to express all by itself
    // (otherwise each r in rs will have to add r
    //
    rs31 =  concat( [1.2, [1.2,0.5]], repeat([0.5],5), [[0.5,1.2],1.2]);
    //echo(rs31=rs31); 
    bone31 = trslN( bone, -6);
    Chain( bone31, twist=60, r=0,rs=rs31, color="darkcyan" ); 
    
    //
    // Make an arrow
    //
    rs4 =  [.5,[.5,1.5],0];
    //echo(rs4=rs4); 
    bone4 = onlinePts( p01(  bone ), lens=[0,8,10]);
    //bone4= slice( trslN( bone, 24), 0, len(rs4));
    //echo( bone4= _colorPts(bone4));
    Chain( bone4,  r=0,rs=rs4, color="blue" ); 

    //
    // backward arrow
    //
    rs41 =  [0, [1.5,.3],0.3];
    //echo(rs41=rs41); 
    bone41 = onlinePts( p01(trslN( bone, 6)), lens=[0,3,10]);
    //echo( bone41= _colorPts(bone41));
    Chain( bone41,  r=0,rs=rs41, color="purple" ); 

    //
    // Make a 2-headed arrow
    //
    rs5 =  [0, [2,0.5],[0.5,2],0];
    //echo(rs5=rs5); 
    bone5 = onlinePts( p01(trslN( bone, 12)), lens=[0, 3,10,13]);
    //echo( bone5= _colorPts(bone5));
    Chain( bone5,  r=0,rs=rs5,nside=5, color="darkcyan" ); 

    //
    // Make a spindle
    //
    rs6 =  [0, 1.5,3, 1.5,0];
    //echo(rs6=rs6); 
    bone6 = onlinePts( p01(trslN( bone, 18)), lens=[0, 5.5,6,6.5,12 ]);
    //echo( bone6= _colorPts(bone6));
    Chain( bone6,  r=0,rs=rs6,nside=8, color="darkcyan" ); 
    
}

//Chain_demo_coln_arrow();

module chaindata_demo_arrow(){

    echom("chaindata_demo_arrow");
    nseg=8;
    nside=30;
    r=1;

    module Mark( bone,cut0, rlast=undef, twist, nside=nside, rs,color=undef, label=""){
        
       echom("Mark"); 
       // MarkPts( bone, "ch=[r,0.03]");
        //Chain0( bone ); 
        
        chrtn = chaindata( bone, r=r, rlast=rlast
        , nside=nside, rs=rs, twist=twist, nside=nside );
        echo(chrtn = chrtn);
        
        cuts= hash(chrtn, "cuts");
        echo( len_cuts= len(cuts), cuts=_colorPts(cuts) );
        pts = joinarr( cuts );
//         // echo(pts= arrprn(pts,2));
//
//        MarkPts( cuts[0], "ch=[color,blue,closed,true]");
////        MarkPt( last(bone)
////           , ["label",label,"shift",0.5]);
//
//        
//        
////        echo(pts= arrprn(pts,3));              
////        MarkPts(pts[0], "ch=[color,blue,r,0.04,closed,true]" );   
////        MarkPts(last(pts), "ch=[color,red,r,0.02,closed,true]" );   
////        
////        
        // Reference plane
        color("blue",0.5)  
        Plane( [bone[0],bone[1],cuts[1][0],cuts[0][0] ]);
        shape = len(cuts[0])==1 && len(last(cuts))==1?
                        "cubesides"
                       :len(cuts[0])==1 ? "conedown"
                       :len(last(cuts))==1? "cone"
                       :"chain";
        echo( shape = shape );
        faces = faces( shape 
                        , nside= cut0?len(cut0):nside
                     , nseg=len(cuts)-1); //nseg);
        color(color,1)   
        polyhedron( points= pts, faces=faces);   
    }
    
    
    pqr=pqr();
    rndch = chainBoneData(n=nseg+1, seed=pqr, 
                    seglen=1.5,a=[170,185],a2=[-13,30]);

//    echo("");
//    bone = hash( rndch, "pts");
//    echo(bone= _colorPts(bone));
//    
//    Mark( bone, rlast=0, nside=6 ); 


    // Take last 2 seg to make a cone
    echo("");
    rs1 =  concat( repeat([0],7), 
                   [[0,0.3],-0.3]);
    echo(rs1=rs1);
    rndch1 = chainBoneData(n=nseg+1, seed= trslN(pqr, 6)
        , seglen=1.5, a=180, a2=360/8/nseg);
    //MarkPts(bone4,"ch");
    bone1 = hash(rndch1, "pts");
    Mark( bone1,rs=rs1,r=0.3, rlast=undef, nside=4, color="red" );
//
//  
//   
    echo("");
     rs2 = [ 0.3, [0.3,0.6],0];
    echo(rs2=rs2);
    rndch2 = chainBoneData(n=3, seed= trslN(pqr, 12)
        , lens=[ 8,3], a=0, a2=180);
    bone2 = hash(rndch2, "pts");
    MarkPts(bone2,"ch");
    echo(bone2= bone2);
    Mark( bone2,rs=rs2,r=0, rlast=undef, nside=12, color="green" );
  

    echo("");
    rs3 = [ 0, [0.6,0.3],0.3];
    echo(rs3=rs3);
    rndch3 = chainBoneData(n=3, seed= trslN(pqr, 18)
        , lens=[ 3,8], a=0, a2=180);
    bone3 = hash(rndch3, "pts");
    MarkPts(bone3,"ch");
    echo(bone3= bone3);
    Mark( bone3,rs=rs3,r=0, rlast=undef, nside=12, color="blue" );
     
}
//chaindata_demo_arrow();

module Chain_demo_arrow(){

    echom("Chain_demo_arrow");
    nseg=8;
    nside=30;
    r=1;

    pqr=pqr();
    rndch = chainBoneData(n=nseg+1, seed=pqr, 
                    seglen=1.5,a=[170,185],a2=[-13,30]);

    // Take last 2 seg to make a cone
    echo("");
    rs1 =  concat( repeat([0],7), 
                   [[0,0.3],-0.3]);
    //echo(rs1=rs1);
    rndch1 = chainBoneData(n=nseg+1, seed= trslN(pqr, -6)
        , seglen=1.5, a=180, a2=360/8/nseg);
    //MarkPts(bone4,"ch");
    bone1 = hash(rndch1, "pts");
    Chain( bone1,rs=rs1,r=0.3, rlast=undef, nside=4, color="red" );


    echo("");
     rs2 = [ 0.3, [0.3,0.6],0];
    //echo(rs2=rs2);
    rndch2 = chainBoneData(n=3, seed= pqr 
        , lens=[ 8,3], a=0, a2=180);
    bone2 = hash(rndch2, "pts");
    MarkPts(bone2,"ch");
    echo(bone2= bone2);
    Chain( bone2,rs=rs2,r=0, rlast=undef, nside=12, color="green" );
  

    echo("");
    rs3 = [ 0, [0.6,0.3],0.3];
    //echo(rs3=rs3);
    rndch3 = chainBoneData(n=3, seed= trslN(pqr, 6)
              , lens=[ 3,8], a=0, a2=180);
    bone3 = hash(rndch3, "pts");
    MarkPts(bone3,"ch");
    echo(bone3= bone3);
    //Chain( bone3,rs=rs3,r=0, rlast=undef, nside=12, color="blue" );
    Chain( bone3,rs=rs3,opt="r=0;rlast=undef;nside=12;cl=blue"
         );
     
}
//Chain_demo_arrow();


module chaindata_demo_loft(){
    
    echom("chaindata_demo_loft");
    
    //=================================================
    module Mark( chdata, color ){
     
        cuts = hash( chdata, "cuts" );
        echo( len= len(cuts), cuts= arrprn(cuts, dep=4, nl=1) );
//        for( i=range(5))            
//          color(COLORS[i])
//          MarkPts( cuts[i], "ch"); 
    }
    //=================================================
    
    pqr = pqr();
    //pqr=[[0.776599, 2.18196, -0.844985], [-2.65393, -0.7034, -1.5362], [-2.11121, -1.20895, 1.91439]];
    echo(pqr=pqr);
    nseg= 5; 
    bonedata= chainBoneData(n=nseg+1, seglen=1.5
               ,seed= pqr, a=[170,190]);
    bone = hash(bonedata, "pts");
//    bone = [[-0.605664, 1.34442, 2.20736], [-1.59941, 0.456823, 0.179087], [-2.24952, 0.74287, -0.519912], [-2.71019, 1.46523, -1.64494], [-2.77274, 1.52165, -2.21687], [-3.21933, 1.23491, -3.17352]];
    //bone = [[0.776599, 2.18196, -0.844985], [-0.736115, 0.909641, -1.14978], [-1.2267, -1.00371, -1.46349], [-1.37518, -2.5941, -2.66707]];
    MarkPts( bone, "ch");
    echo(lenbone=len(bone), bone=bone);
    
    chdata= chaindata( bone, r=0.8
                     , nside= 4
                     , nsidelast= 24
                     );
    //color("khaki", 0.1) Chain( chdata );
    //echo( chdata = chdata );
    
    cuts_loft = hash(chdata, "cuts");
    echo( cuts_loft = arrprn(cuts_loft,4, nl=1));
    //for( f = cuts_loft ) MarkPts( f, "ch");

    faces = faces("chain", nside=len(cuts_loft[0])
                         , nseg=len(cuts_loft)-1 );
    //color("red",0.8)
    polyhedron( points = joinarr( cuts_loft), faces = faces );
    //Mark( chdata );
        
}
//chaindata_demo_loft(); // 9sec

module Chain_demo_loft(){
    
    echom("Chain_demo_loft");
    
    //=================================================
    module Mark( chdata, color ){
     
        cuts = hash( chdata, "cuts" );
        echo( len= len(cuts), cuts= arrprn(cuts, dep=4, nl=1) );
//        for( i=range(5))            
//          color(COLORS[i])
//          MarkPts( cuts[i], "ch"); 
    }
    //=================================================
    
    pqr = pqr();
    //pqr=[[0.776599, 2.18196, -0.844985], [-2.65393, -0.7034, -1.5362], [-2.11121, -1.20895, 1.91439]];
    echo(pqr=pqr);
    nseg= 5; 
    bonedata= chainBoneData(n=nseg+1, seglen=1.5
               ,seed= pqr, a=[170,190]);
    bone = hash(bonedata, "pts");
//    bone = [[-0.605664, 1.34442, 2.20736], [-1.59941, 0.456823, 0.179087], [-2.24952, 0.74287, -0.519912], [-2.71019, 1.46523, -1.64494], [-2.77274, 1.52165, -2.21687], [-3.21933, 1.23491, -3.17352]];
    //bone = [[0.776599, 2.18196, -0.844985], [-0.736115, 0.909641, -1.14978], [-1.2267, -1.00371, -1.46349], [-1.37518, -2.5941, -2.66707]];
    MarkPts( bone, "ch");
    echo(lenbone=len(bone), bone=bone);
    
    chdata= chaindata( bone, r=0.8
                     , nside= 4
                     , nsidelast=24
                     );
    Chain( chdata );
    
    // Why this doesn't work ?
    //Chain( bone, n=len(bone), r=0.8, nside=4, nsidelast=6 );
    
}
//Chain_demo_loft(); // 9sec


module chaindata_demo_loft_rlast(){
    
    echom("chaindata_demo_loft_rlast");
    
    //=================================================
    module Mark( chdata, color ){
     
        cuts_loft = hash(chdata, "cuts");
        echo( cuts_loft = arrprn(cuts_loft,4, nl=1));
        //for( f = cuts_loft ) MarkPts( f, "ch");

        faces = faces("chain", nside=len(cuts_loft[0])
                             , nseg=len(cuts_loft)-1 );
        color(color) //,0.8)
        polyhedron( points = joinarr( cuts_loft), faces = faces );
        //Mark( chdata );
        
    }
    //=================================================
    
    pqr = pqr();    
    echo(pqr=pqr);
    nseg=5; 
    bonedata= chainBoneData(n=nseg+1, seglen=1.5
               ,seed= pqr, a=[170,190]);
    bone = hash(bonedata, "pts");
    
    //---------------------------------
    MarkPts( bone, "ch");
    echo(lenbone=len(bone), bone=bone);
    chdata= chaindata( bone, r=0.5, rlast=1
                     , nside= 4
                     , nsidelast= 36
                     );
    Mark( chdata );
    //---------------------------------
    bone2 = trslN(bone, 5);
    chdata2= chaindata( bone2, r=1.2, rlast=0.6
                     , nside= 3
                     , nsidelast= 24
                     );
    Mark( chdata2, "darkcyan" );
    
    
//    cuts_loft = hash(chdata, "cuts");
//    echo( cuts_loft = arrprn(cuts_loft,4, nl=1));
//    //for( f = cuts_loft ) MarkPts( f, "ch");
//
//    faces = faces("chain", nside=len(cuts_loft[0])
//                         , nseg=len(cuts_loft)-1 );
//    //color("red",0.8)
//    polyhedron( points = joinarr( cuts_loft), faces = faces );
//    //Mark( chdata );
//        
}
//chaindata_demo_loft_rlast(); //21sec

module Chain_demo_loft_rlast(){
    
    echom("Chain_demo_loft_rlast");
    
    //=================================================
    module Mark( chdata, color ){
     
        cuts_loft = hash(chdata, "cuts");
        echo( cuts_loft = arrprn(cuts_loft,4, nl=1));
        //for( f = cuts_loft ) MarkPts( f, "ch");

        faces = faces("chain", nside=len(cuts_loft[0])
                             , nseg=len(cuts_loft)-1 );
        color(color) //,0.8)
        polyhedron( points = joinarr( cuts_loft), faces = faces );
        //Mark( chdata );
        
    }
    //=================================================
    
    pqr = pqr();    
    echo(pqr=pqr);
    nseg=5; 
    bonedata= chainBoneData(n=nseg+1, seglen=1.5
               ,seed= pqr, a=[170,190]);
    bone = hash(bonedata, "pts");
    
    //---------------------------------
    MarkPts( bone, "ch");
    echo(lenbone=len(bone), bone=bone);
    chdata= chaindata( bone, r=0.5, rlast=1
                     , nside= 4
                     , nsidelast= 36
                     );
    Chain( chdata );
    //---------------------------------
    bone2 = trslN(bone, 5);
    chdata2= chaindata( bone2, r=1.2, rlast=0.6
                     , nside= 3
                     , nsidelast= 24
                     );
    Chain( chdata2, color="darkcyan" );
    
}
//Chain_demo_loft_rlast(); //26sec


module chaindata_demo_loftpower(){
    
    echom("chaindata_demo_loftpower");
    
    //=================================================
    module Mark( chdata, color ){
     
        cuts_loft = hash(chdata, "cuts");
        echo( cuts_loft = arrprn(cuts_loft,4, nl=1));
        //for( f = cuts_loft ) MarkPts( f, "ch");

        faces = faces("chain", nside=len(cuts_loft[0])
                             , nseg=len(cuts_loft)-1 );
        color(color) //,0.8)
        polyhedron( points = joinarr( cuts_loft), faces = faces );
        //Mark( chdata );
        
    }
    //=================================================
    
    pqr = pqr();    
    echo(pqr=pqr);
    nseg=8; 
    nsidelast = 7;
    loftpower= 0.99;
    
    bonedata= chainBoneData(n=nseg+1, seglen=1
               ,seed= pqr, a=[170,190]);
    bone = hash(bonedata, "pts");
    
    //---------------------------------
    MarkPts( bone, "ch");
    echo(lenbone=len(bone), bone=bone);
    chdata= chaindata( bone, r=0.5, rlast=1.5
                     , nside= 4
                     , nsidelast= nsidelast
                     );
    Mark( chdata );
    
        //---------------------------------
        bone1= trslN2( bone, 4);
        chdata1= chaindata( bone1, r=0.5, rlast=1.5
                         , nside= 4
                         , nsidelast= nsidelast
                         , loftpower= loftpower
                         );
        Mark( chdata1 );
        
        //---------------------------------
        bone12= trslN2( bone, 8);
        chdata12= chaindata( bone12, r=0.5, rlast=1.5
                         , nside= 4
                         , nsidelast= nsidelast
                         , loftpower= -loftpower
                         );
        Mark( chdata12 );
        
    //---------------------------------
    bone2 = trslN(bone, 5);
    MarkPts( bone2, "ch");
    chdata2= chaindata( bone2, r=1.5, rlast=0.6
                     , nside= 3
                     , nsidelast= nsidelast
                     );
    Mark( chdata2, "darkcyan" );
    
        //---------------------------------
        bone21= trslN2( bone2, 4);
        chdata21= chaindata( bone21, r=1.5, rlast=0.6
                         , nside= 3
                         , nsidelast= nsidelast
                         , loftpower= loftpower
                         );
        Mark( chdata21, "darkcyan" );
        
        //---------------------------------
        bone22= trslN2( bone2, 8);
        chdata22= chaindata( bone22, r=1.5, rlast=0.6
                         , nside= 3
                         , nsidelast= nsidelast
                         , loftpower= -loftpower
                         );
        Mark( chdata22, "darkcyan" );
        
}
//chaindata_demo_loftpower();  // 38sec


module Chain_demo_loftpower(){
    
    echom("Chain_demo_loftpower");
    
    //=================================================
    module Mark( chdata, color ){
     
        cuts_loft = hash(chdata, "cuts");
        echo( cuts_loft = arrprn(cuts_loft,4, nl=1));
        //for( f = cuts_loft ) MarkPts( f, "ch");

        faces = faces("chain", nside=len(cuts_loft[0])
                             , nseg=len(cuts_loft)-1 );
        color(color) //,0.8)
        polyhedron( points = joinarr( cuts_loft), faces = faces );
        //Mark( chdata );
        
    }
    //=================================================
    
    pqr = pqr();    
    echo(pqr=pqr);
    nseg=8; 
    nsidelast = 7;
    loftpower= 0.99;
    
    bonedata= chainBoneData(n=nseg+1, seglen=1
               ,seed= pqr, a=[170,190]);
    bone = hash(bonedata, "pts");
    
    //---------------------------------
    MarkPts( bone, "ch");
    echo(lenbone=len(bone), bone=bone);
    chdata= chaindata( bone, r=0.5, rlast=1.5
                     , nside= 4
                     , nsidelast= nsidelast
                     );
    Chain( chdata );
    
        //---------------------------------
        bone1= trslN2( bone, 4);
        chdata1= chaindata( bone1, r=0.5, rlast=1.5
                         , nside= 4
                         , nsidelast= nsidelast
                         , loftpower= loftpower
                         );
        Chain( chdata1 );
        
        //---------------------------------
        bone12= trslN2( bone, 8);
        chdata12= chaindata( bone12, r=0.5, rlast=1.5
                         , nside= 4
                         , nsidelast= nsidelast
                         , loftpower= -loftpower
                         );
        Chain( chdata12 );
        
    //---------------------------------
    bone2 = trslN(bone, 5);
    MarkPts( bone2, "ch");
    chdata2= chaindata( bone2, r=1.5, rlast=0.6
                     , nside= 7
                     , nsidelast= 3
                     );
    Chain( chdata2, color="darkcyan" );
    
        //---------------------------------
        bone21= trslN2( bone2, 4);
        chdata21= chaindata( bone21, r=1.5, rlast=0.6
                         , nside= 7
                         , nsidelast= 3
                         , loftpower= loftpower
                         );
        Chain( chdata21, color="darkcyan" );
        
        //---------------------------------
        bone22= trslN2( bone2, 8);
        chdata22= chaindata( bone22, r=1.5, rlast=0.6
                         , nside= 7
                         , nsidelast= 3
                         , loftpower= -loftpower
                         );
        Chain( chdata22, color="darkcyan" );
        
}
//Chain_demo_loftpower();  // 38sec

module chaindata_demo_loftpower_hires(){
    
    echom("chaindata_demo_loftpower_hires");
    
    //=================================================
    module Mark( chdata, color ){
     
        cuts_loft = hash(chdata, "cuts");
        echo( cuts_loft = arrprn(cuts_loft,4, nl=1));
        //for( f = cuts_loft ) MarkPts( f, "ch");

        faces = faces("chain", nside=len(cuts_loft[0])
                             , nseg=len(cuts_loft)-1 );
        color(color) //,0.8)
        polyhedron( points = joinarr( cuts_loft), faces = faces );
        //Mark( chdata );
        
    }
    //=================================================
    
    pqr = pqr();    
    echo(pqr=pqr);
    nseg=8; 
    nsidelast = 24;
    loftpower= 2; // NOTE: if <-1, it will shrink 
    
    bonedata= chainBoneData(n=nseg+1, seglen=1
               ,seed= pqr, a=[170,190]);
    bone = hash(bonedata, "pts");
    
    //---------------------------------
    MarkPts( bone, "ch");
    echo(lenbone=len(bone), bone=bone);
    chdata= chaindata( bone, r=0.5, rlast=1.2
                     , nside= 4
                     , nsidelast= nsidelast
                     );
    Mark( chdata );
    
        //---------------------------------
        bone1= trslN2( bone, 4);
        chdata1= chaindata( bone1, r=0.5, rlast=1.2
                         , nside= 4
                         , nsidelast= nsidelast
                         , loftpower= loftpower
                         );
        Mark( chdata1 );
        
        //---------------------------------
        bone12= trslN2( bone, 8);
        chdata12= chaindata( bone12, r=0.5, rlast=1.2
                         , nside= 4
                         , nsidelast= nsidelast
                         , loftpower= -loftpower
                         );
        Mark( chdata12 );
       
        
}
//chaindata_demo_loftpower_hires(); // 40 sec

module Chain_demo_cup(){

    echom("Chain_demo_cup");
    
    pq = randPts(2);
    pqr= app( pq, onlinePt(p10(pq), -2));
    pts= concat(pqr
               , [onlinePt( p20(pqr), ratio=0.8) ]
               );
    
    bone = pts; 
    echo( bone = arrprn(bone) );
    
    //===================================
    chdata= chaindata( bone
            , n=4
            , rs= [3,3,[3,2],2]
            //, lens=[2,-1]
            );
    cuts= hash(chdata, "cuts");
    echo( cuts= arrprn(cuts ) );
    
    Chain(chdata, "trp=1" );

    //===================================
    chdata2= chaindata( trslN(bone,15)
            , n=4
            , rs= [3,3,[3,2],2]
            //, lens=[2,-1]
            , nside=12
            );
    cuts2= hash(chdata2, "cuts");
    echo( cuts2= arrprn(cuts2 ) );
    //for (f=cuts) MarkPts(f,"ch");
    Chain(chdata2, "trp=0.5;cl=green" );
    
    //===================================
    chdata3= chaindata( trslN( bone,30)
            , n=4
            , rs= [2,4,[4,2],1]
            //, lens=[2,-1]
            , nside=6
            );
    cuts3= hash(chdata3, "cuts");
    echo( cuts3= arrprn(cuts3 ) );
    //for (f=cuts) MarkPts(f,"ch");
    Chain(chdata3, "trp=1;cl=darkcyan" );

}
//Chain_demo_cup();

module Chain_demo_cup2(){

    echom("Chain_demo_cup2");
    n=6; // if n=3: outside/inside n both are 3
         // means inside/outside nseg are both 2
         // ==> n for bone = 2n+1 = 5
    thick = 0.3;
    r_out = 1;
    nside = 24;
    
    //=========================================
    // Building a bone that goes reversed in the midway
    chbone = chainBoneData( n=n, seglen=2, a=[170,190] );
    pts0 = hash(chbone,"pts"); // bone for outside faces
    
    innerlastpt = onlinePt(pts0, len=thick);
    
    pts0r= reverse( replace( pts0,0, innerlastpt) );
    MarkPts( pts0r, "ch;r=0.05");
    MarkPts( pts0, "ch=[r,0.1,transp,0.2];trp=0.2");
    pts = concat( pts0, slice(pts0r,1));
    echo( lenpts = len(pts) );
    
    //=========================================
    // building outer r
    rs_out = repeat( [r_out], len(pts0) );
    
    // Building inner r, which requires nside:
    ang_per_i= 180*(nside-2)/nside;
    a = ang_per_i/2; 
    //
    // formula: sin(a) = thick/ r_diff
    //
    r_diff= thick/ sin(a);
    echo( ang_per_i = ang_per_i, a=a, nside=nside, r_diff= r_diff);

    rs_in= [for( i=[len(pts0)-1:-1:0])
              i==len(pts0)-1? [ rs_out[i], rs_out[i]-r_diff ]
                  : rs_out[i]-r_diff             
           ];
    rs = concat( slice( rs_out,0,-1), rs_in );
    //rs = concat( rs_out, rs_in );
    echo(lenrs=len(rs), rs = rs);
    //=========================================

    chdata = chaindata( pts
             , n= len(rs)
             , nside= nside
             , rs = rs
             );
    //echo( chdata= chdata );
    cuts= hash( chdata, "cuts");
    echo( cuts= arrprn(cuts,2));
    Chain( chdata, "trp=1" );

}
//Chain_demo_cup2();

module chaindata_debug(){
    
  
  pts = randPts(4);
  MarkPts(pts,"ch");
  r=0.2;
  rs = concat( [r], [[r,r*2]],
             ,repeat([r], len(pts)-2)
             );

  echo(rs= rs );
    
  chdata = chaindata( pts, rs=rs );
  echo( chdata = arrprn( chdata,3));
    
  Chain( chdata, "trp=0.5" );  
    
    
    
}    
//chaindata_debug();



module chaindata_demo_bevel_dev(){
 echom("chaindata_demo_bevel_dev");
 //nseg=9;
 nside=4;
    
 r=4;   
 
 bvr = 1; // bvr: radius for bevel   
 an  = 16;     // # of pts on the arc
    
 lens= concat( [1],
             , [ for( i= [1:an-1]) //range(an))
                  let( a0 = ((i-1)*90)/(an-1)
                     , a = (i*90)/an
                     , dL0= i==0?0:sin(a0)*bvr
                     , dL= sin(a)*bvr
                     )
                  dL-dL0
               ]  
              );
 echo(lens = lens );
             
 rs  = concat( [r]
             , [ for( i= range(an))
                  let( a = (i*90)/(an-1)
                     , dr= bvr- cos(a)*bvr
                     //, dL= sin(a)*bvr
                     )
                  r-dr
                ] );
  
 nseg= an;  

 n = len(lens)+1;
    
 rndch = chainBoneData( n=nseg+1, lens=lens, a=180, a2=0 );
 bone = hash( rndch, "pts");
 echo(bone = bone ); 
    
 chpts = chaindata( bone, nside=nside, nseg= nseg
          , rs=rs
          , a=180,a2=0);
 
 cuts = hash( chpts, "cuts");
 pts = joinarr( cuts );
 
 ridge = [for(xs=cuts) xs[0]];
 MarkPts( ridge, "ch=[color,blue];r=0.03;samesize;sametransp;color=red");
 
 faces = faces( "chain", nseg = nseg, nside= nside );
 polyhedron( points= pts, faces= faces ); 
    
    
}   
//chaindata_demo_bevel_dev();

module chaindata_demo_app(){

    echom("chaindata_demo_app: append more chaindata");
//    nseg=8;
//    nside=5;
//    r=1;

    module Mark( chrets, r,rlast, color=undef) {
       
       echom("Mark, color=", color); 
       // ,cut0, twist, rs,color=undef, label=""){
        
//        MarkPts( bone, "ch=[r,0.03]");
//        //Chain0( bone ); 
//        
//        chrtn = chaindata( bone, r=r, rlast=rlast, rs=rs, twist=twist, nside=nside );
//        pts= hash(chrtn, "cuts");
//        echo(pts=pts);
//
//        MarkPt( last(bone)
//           , ["label",label,"shift",0.5]);
//
//        
//        
////        echo(pts= arrprn(pts,3));              
////        MarkPts(pts[0], "ch=[color,blue,r,0.04,closed,true]" );   
////        MarkPts(last(pts), "ch=[color,red,r,0.02,closed,true]" );   
////        
////        
//        // Reference plane
//        color("blue",0.5)  
//        Plane( [bone[0],bone[1],pts[1][0],pts[0][0] ]);
//        
//        faces = faces("chain", nside= cut?len(cut):nside
//                     , nseg=len(pts)-1); //nseg);
//        color(color,1)   
//        polyhedron( points= joinarr(pts), faces=faces);   
    }

        
    seed = pqr();
    
    bone = chainBoneData(seed=seed, n=nseg+1, seglen=1.5,a=180,a2=0);
    echo(bone=bone);    
    Mark( bone ); 

    
    bone2 = trslN( bone, 6 );
    Mark( bone2, twist=60, color="red" );

    
    rs3 =  concat( [1.2, [1.2,0]], repeat([0],5), [[0,1.2],1.2]);
    echo(rs3=rs3); 
    bone3 = trslN( bone, 20 );
    Mark( bone3, twist=60, rs=rs3, color="green" ); 

    
    rs31 =  concat( [1.2, [1.2,0.5]], repeat([0.5],5), [[0.5,1.2],1.2]);
    echo(rs31=rs31); 
    bone31 = trslN2( bone3, 6 );
    Mark( bone31, twist=60, r=0,rlast=0, rs=rs31, color="darkcyan" ); 

//    rs4 = [1.2, [1.2,0]]  ;
//    echo(rs4=rs4); 
//    bone4 = trslN( bone, 12 );
//    Mark( bone4, twist=60, rs=rs4 );  
}

//chaindata_demo_app();


module Coord_demo()
{
    echom("Coord_demo()");
    
 
//    pqr = randPts(3, d=3);
//    Coord(pqr, ["labelo","1:Coord(pqr())"]);
//
//    pqr2 = randPts(3, d=5);
//    Coord(pqr2, ["color","blue"
//                , "labelo","2:Coord(pqr(),[\"color\",\"blue\"])"]);

    pqr3 = randPts(3, d=7);
    Coord(pqr3, ["x",["r",0.05],"y",false
                , "labelo","2:Coord(pqr(),[\"x\",[\"r\",0.05],\"y\",false])"]);

    
    //LabelPt(P(pqr),"1:Coord(pqr())");
////    
//    pqr2 = pqr();
//    Coord(pqr2, ["color","blue"]);
//    LabelPt(P(pqr2),"2:Coord(pqr(),[\"color\",\"blue\"])");
////    
//    pqr3 = pqr();
//    //Coord(pqr3, ["axes",["x",["r",0.3]]]);
//    Coord(pqr3, ["x",["r",0.3]]);
//    LabelPt(P(pqr3),"3:Coord(pqr(),[\"color\",\"black\"])");
//    
//    pqr4= pqr();
//    LabelPt(P(pqr4), "mark90=true");
//    coord = coordPts(pqr4);
//    Coord(coord, ops=["mark90",true]);
//    
//    pqr3 = pqr();
//    Coord(pqr3, ops=["len",2,"r",0.03]);
//    LabelPt( onlinePt( p10(pqr3), len=2)
//      ,"Coord(pqr(),ops=[\"len\",2,\"r\",0.03])");
//    
//    
//    pqr4 = pqr();
//    Coord(pqr4, ops=["len",2,"x",4]);
//    LabelPt(P(pqr4)
//      ,"Coord(pqr(),ops=[\"len\",2,\"x\",4])");
    
    
    //mark90( shrink(coord) );
    //MarkPts( pqr2 );
    
}
//Coord_demo();


module Cube_demo()
{
    echom("Cube_demo");
    echo(_b("Demo Cube"));

    pqr=randPts(3, d=[3,6]);
    MarkPts(pqr, "l=PQR;ch" );

    
    echo(_color("Cube(3)","darkkhaki"));
    Cube(3);
	
    echo(_red("Cube([5,3,1], opt=[\"actions\",[\"x\",5],\"color\", [\"red\",0.5]] );"));	
    Cube([5,3,1], opt=["actions",["x",5]
		      ,"color", ["red",0.5]
		      ] );
	
    
    echo( _green(_b("<br/>>> Draw a green Cube @pqr, overriding the frame color.")));	
    echo( _green("color(\"green\", 0.5) Cube([5,2,1], opt=[\"coord\", pqr] )"));	
    color("green", 0.5)
    Cube([5,2,1], opt=["coord", pqr] );


               
    echo( _blue(_b("<br/>>> Draw a blue Cube @ y=5 away from the pqr, with markPts info")));	
    echo( _blue("Cube([5,2,1], opt=[\"coord\", pqr, \"color\",[\"blue\",0.5],\"actions\",[\"y\", 5]], \"markPts\", \"l\" )"));	
    Cube([5,2,1], opt=[ "coord", pqr
                      , "color",["blue",0.5]
                      , "actions",["y", 5]
                      , "markPts","l"
                      ] );
           
    echo( _color(_b("<br/>>> Draw a purple Cube @pqr2 with thicker blue frame"), "purple"));	
    echo( _color("Cube([5,2,1], opt=[\"coord\", pqr2, \"frame\", [\"r\",0.1, \"color\",\"blue\"], \"color\", [\"purple\",0.7])", "purple"));	
    pqr2=randPts(3, d=[-1,-4]);
    MarkPts(pqr2, "l=PQR;ch" );
    Cube( [5,2,1], opt=[ "coord", pqr2
                       , "frame", ["r",0.1, "color","blue"]
                       , "color", ["purple",0.7]
                       ] );

    echo( _color(_b("<br/>>> Draw a teal Cube, centered @pqr"), "teal"));	
    echo( _color("Cube([5,2,1], center=true, opt=[\"coord\", pqr, \"color\",[\"teal\",0.5]] )","teal"));	
    Cube([5,2,1], center=true, opt=["coord", pqr, "color", ["teal",0.5]] );    

           
}
//Cube_demo();

// UNDER CONSTRUCT
//module Cube_demo_use_cubePts()
//{
    //echom("Cube_demo_use_cubePts");
    //echo(_b("Demo Cube using cubePts"));
//
    //pts = cubePts();
    //Cube(pts);
//}
//Cube_demo_use_cubePts();


module Cube_demo_dim()
{
    echom("Cube_demo_dim");
    echo(_b("Demo Cube"));

    pqr=randPts(3, d=[3,6]);
    //MarkPts(pqr, "l=PQR;ch" );

    
    _echo( _color("Cube(3, opt=['dim', [0,1,2]])","orange" )
         , " // dim=indices");
    Cube(2, opt=["dim", [0,1,2], "markPts", "l"]);
	
	
    _echo(_red("Cube([5,3,1], opt=['dims', [ 'pqr', [6,4,2], 'color','red']]);")
         , " // More options");	
    Cube([5,3,1], opt=["actions",["x",5]
		      ,"color", ["red",0.5]
		      , "dim", ["pqr",[6,4,2], "color","green"  ] 
		      , "markPts", "l"          
		      ] );
	
    _echo(_blue("Cube([5,3,1], opt=['dims', [6,[2,3,-2],2] ]);")
         , " // Use a pt [2,3,-2] instead of index. This allows to dim any place.");	
    Cube([4,3,1], opt=["actions",["z",-3]
		      ,"color", ["blue",0.5]
		      , "dim", [6,[2,3,-2],2] 
		      , "markPts", "l"          
		      ] );	
	    
    _echo( _color("opt=['dims', [ [6,4,2], [6,7,4], ['pqr',[3,0,1]], ['pqr',[1,5,3], 'color','black']]]]","purple")
         , " // Use 'dims' for multiple dim");	
    Cube([5,2,1], opt=["coord", pqr
                      , "color", ["purple",0.5]
                      , "dims" , [ [6,4,2] 
		                , [6,7,4]
		                , ["pqr",[3,0,1], "color","blue" ]
		                , ["pqr",[1,5,3], "color","black"]
		                ]
		      , "markPts", "l"
		      ] );
}

//Cube_demo_dim();


module cubePts_demo()
{
	module demo(color="red", p=undef, r=undef, h=2)
	{
		pqr = randPts(3,r=4);
		MarkPts([ R(pqr)], ["labels",["R0"]] );
		Chain( pqr, ["r",0.02]);

		echo("p,r,h= ", p, r, h);

		cpts = cubePts(pqr,p=p,r=r,h=h);
		MarkPts( cpts, ["labels", "PQRSTUVW"] );
		Chain( slice(cpts, 0, 4 ), ["r",0.01]);
		Chain( slice(cpts, 4, 8 ), ["r",0.01]);
	
		Mark90( p3(cpts,4,0,1) );
		Mark90( p3(cpts,4,0,3) );
	
		//Normal( pqr );
		//MarkPts( [normalPt(pqr,len=1)], ["r",0.2,"transp",0.3] );//
//		Chain( addv( pqr, normalPt(pqr, len=1)-Q(pqr) ) );
//		Chain( 
//			squarePts( addv( pqr, normalPt(pqr, len=1)-Q(pqr)),p,r )
//			,["r",0.2,"transp",0.3] );
	
		pqrs = squarePts( pqr, p=p,r=r );
		Chain( pqrs, ["r",0.1, "transp",0.3] );		

		//echo("pq, qr = ", dist
		color(color, 0.2)
		polyhedron( points= cpts, faces = CUBEFACES );
	
		Dim( p3(cpts, 0,1,2),["color","red", "prefix", "p="] ); // measure pq
		Dim( p3(cpts, 2,1,0),["color","green", "prefix","r="]  ); // measure qr
		Dim( p3(cpts, 4,0,3),["color","blue", "prefix","h="]  ); // measure pt

	}

	//demo();
	//demo("green",p=3);
	//demo("blue",r=3);
	demo("purple",p=5,r=3);

}

//cubePts_demo();

module distPtPl_demo()
{
	pt = randPts(1);
	pqr= randPts(3);
}
//distPtPl_test(["mode",22]);


//module distPts_demo() // FAILED
//{
//    echom("distPts_demo");
//    
//    pq= randPts(2);
//    rs= randPts(2);
//    ab = distPts(pq,rs);
//    
//    P=pq[0]; Q=pq[1]; R=rs[0]; S=rs[1]; A=ab[0]; B=ab[1]; J=ab[2];
//    
//    //color("darkkhaki",0.5) Plane( [P,Q,R] );
//    
//    J = ab[2]; MarkPts( [J,R], "ch;ball=0");
//    K = ab[3]; MarkPts( [K,S], "ch;ball=0");
//    color("blue", 0.2)
//    MarkPts([J,K],"ch=[r,0.1];l=JK");
//    echo(JK = [J,K]);
//    
//    color("red")
//    MarkPts( pq, "ch=[color,red];l=PQ");Line0( linePts(pq));
//    
//    color("green")
//    MarkPts( rs, "ch=[color,green];l=RS");Line0( linePts(rs));
//    //MarkPts( [S,J,R], "ch");
//    MarkPts( p01(ab), "ch=[r,0.05];l=AB");
//    
//    //Mark90( [S,J,R] );
//    Mark90( [P,A,B] );
//    Mark90( [R,B,A] );
//    
//    echo( dist_AB = dist(A,B) );
//    echo( dist_pq_rs = dist(pq,rs) );
//    
//}
//distPts_demo();


//. e 

module echo__demo()
{
	echo_("echox");
	echo_("{_},{_}", ["name","scadex"]);
	echo_("{_}", ["scadex"]);
}
//echo__demo();

module echoblock_demo()
{
	echom( "echoblock_demo" );
	echo( "echoblock([\"line1\",\"line2\",\"line3\"]) shows:");
	echoblock(["line1","line2","line3"]);
}
//echoblock_demo();

module echoh_demo()
{
	echoh("{name},{ver}", ["name","scadex","ver",3.3]);
	echoh("{name},{ver}", ["name","scadex","ver",3.3], true);
}
//echoh_demo();

//========================================================
module echom_demo()
{
	echom("echom");
}

//echom_demo();

module expandPts_demo_1_open()
{
	echom("expandPts");
	pts = randOnPlanePts( randPts(3), 5);
	pts = randPts(6);
	larger= expandPts(pts, dist=0.5);
	smaller= expandPts(pts,dist=-0.5);
	Chain( pts, ["color","red", "r",0.01, "closed",false] );
	//Chain( larger, ["color","red","transp",0.3]);
	//Chain( smaller);
	MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [pts[i], larger[i]], ["r",0.01, "head",["style","cone"]] );
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1), larger[i] ]);

	}
	//Dim( [pts[0], larger[0]] );
}


module expandPts_demo_2()
{
	echom("expandPts");
	pts = randOnPlanePts( 5 );
	pts = randPts(6);
	larger = expandPts(pts, dist=0.3);
	smaller= expandPts(pts, dist=-0.3);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	//Chain( larger, ["color","red","transp",0.3]);
	//Chain( smaller);
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}
	//Dim( [pts[0], larger[0]] );
}

module expandPts_demo_3()
{
	echom("expandPts");
	pts = randOnPlanePts(5);
	//pts = randPts(4);
	larger = expandPts(pts, dist=0.1);
	smaller= expandPts(pts, dist=-0.1);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green","r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01, "closed",true] );
	MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}
	//Dim( [pts[0], larger[0]] );
}

module expandPts_demo_4_arc()
{
	pts = arcPts(r=3, a=120, count=24);
	//echo("pts:", pts);
	larger = expandPts(pts, dist=0.1);
	smaller= expandPts(pts, dist=-0.1);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green","r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01, "closed",true] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}

}
module expandPts_demo()
{
	echom("expandPts_demo");
	//p6 = randPts(6);
	//echo( "p6:",p6);
	//echo( "xp6:",expandPts( p6 ));

	//expandPts_demo_1_open();
	//expandPts_demo_2();
	//expandPts_demo_3();
	expandPts_demo_4_arc();
}	

//  


//. f, i

module faces_demo_1()
{
	pqr = randPts(3);
	S= cornerPt(pqr);
	pqrs = concat( pqr, [S]);
	
	vector = randPt();

	tuvw = [for(p=pqrs) p+vector ];

	pts = concat( pqrs, tuvw );

	MarkPts( pts, ["label",true] );
    
    LabelPt( pts[0], "  faces=faces(\"rod\",sides=4)");
	polyhedron( points = pts
			 , faces = faces("rod", nside=4)
			 );
}
//faces_demo_1();

module faces_demo_2_cube()
{
	pqr = randRightAnglePts();
	S= cornerPt(pqr);
	pqrs = concat( pqr, [S]);
	
	vector = normal(pqr);

	tuvw = [for(p=pqrs) p+vector ];

	pts = concat( pqrs, tuvw );

	MarkPts( pts, ["labels",true] );

	polyhedron( points = pts
			 , faces = faces("rod", nside=4)
			 );
}
//faces_demo_2_cube();

module faces_demo_3_cylinder()
{
	pqr = randPts(3);
    MarkPts( pqr );
    
	Q = Q(pqr);
	N = normalPt( pqr );
	Chain(pqr);
	nside = 8;

	pts= joinarr(rodPts( pqr, ["r",2,"nside",nside] )); //arcPts( pqr, r=2, a=360, count=sides, plane="yz");
	//q_pts= [for(p=p_pts) p+(N-Q)];	

	//pts = concat( p_pts,q_pts );

	MarkPts( pts, ["labels",true] );

	faces = faces("rod", nside=nside);

//	echo("faces_demo_3_cylinder.N=", N);
//	echo("faces_demo_3_cylinder.p_pts=", p_pts);
//	echo("faces_demo_3_cylinder.q_pts=", q_pts);
	echo("faces_demo_3_cylinder.faces=", faces);
	color(false, 0.6)
	polyhedron( points = pts
			 , faces = faces
			 );
}
//faces_demo_3_cylinder();


module faces_demo_4_tube()
{
	pqr = randPts(3);
	MarkPts( pqr, ["label","PQR","chain",true] );

	P = P(pqr);
    Q = Q(pqr);
	N = N( pqr );
	G = onlinePt( [Q,N], len=3 );
	nside = 4;
   
    vPQ = Q-P; 
    
    pts_outQ= [ for(i=range(nside))
             anglePt( pqr, a=90, a2= i*360/nside, len=3 )
             ];
    pts_outP= [for(p=pts_outQ) p-vPQ ]; 
    
    echo( vPQ=vPQ, pts_outQ=pts_outQ, pts_outP=pts_outP );
    
    pts_inQ= [ for(i=range(nside))
             anglePt( pqr, a=90, a2= i*360/nside, len=2 )
          ];
    pts_inP= [for(p=pts_inQ) p-vPQ ]; 
    
	pts_out= concat( pts_outP, pts_outQ);
    pts_in= concat( pts_inP, pts_inQ);
    
    
	pts = concat(pts_out,pts_in);

    MarkPts( pts, ["label",["text",range(pts),"color","black"]] );
    
	faces = faces("tube", nside=nside);
    echo(faces = faces );
    
	color(false, 0.7)
	polyhedron( points = pts
			 , faces = faces
			 );
}
//faces_demo_4_tube();

module faces_demo_4_tube_chk_faces()
{
	pqr = randPts(3);
	MarkPts( pqr, ["label","PQR","chain",true] );

	P = P(pqr);
    Q = Q(pqr);
	N = N( pqr );
	G = onlinePt( [Q,N], len=3 );
	nside = 4;
   
    vPQ = Q-P; 
    
    pts_outQ= [ for(i=range(nside))
             anglePt( pqr, a=90, a2= i*360/nside, len=3 )
             ];
    pts_outP= [for(p=pts_outQ) p-vPQ ]; 
    
    echo( vPQ=vPQ, pts_outQ=pts_outQ, pts_outP=pts_outP );
    
    pts_inQ= [ for(i=range(nside))
             anglePt( pqr, a=90, a2= i*360/nside, len=2 )
          ];
    pts_inP= [for(p=pts_inQ) p-vPQ ]; 
    
	pts_out= concat( pts_outP, pts_outQ);
    pts_in= concat( pts_inP, pts_inQ);
    
    
	pts = concat(pts_out,pts_in);

    MarkPts( pts, ["label",["text",range(pts),"color","black"]] );
    
	faces = faces("tube", sides=nside);
    echo(faces = faces );
    
	color(false, 0.3)
	polyhedron( points = pts
			 , faces = faces
			 );
}
//faces_demo_4_tube_chk_faces();

module faces_demo_5_chain()
{
	sides = 4;
	count = 4;
	
	linepts= randPts(count+1);

	//Chain( linepts );
	//MarkPts( linepts, ["labels",true] );

	_allrps = [for(i=range(count-1))
				let( pqr = [ linepts[i], linepts[i+1], linepts[i+2] ]
				   , ang = angle(pqr)/2
				)
				rodPts( pqr
					  , ["r",0.5, "sides", sides
						,"q_angle", i>=count-2?90:ang
						]
					  )
			 ];

//	allrps = concat( _allrps[0][0], [ for(rp=_allrps) rp[1] ]);
	allrps = concat( _allrps[0][0], joinarr( [ for(rp=_allrps) rp[1] ] ));
	LabelPts( allrps, labels="");
	//Chain( allrps );					

//	p00  = onlinePt( p01(linepts), len=-1 );
//	plast= onlinePt( [get(linepts,-1), get(linepts,-2)] , len=-1 );
//
//	linepts2= concat( [p00], linepts, [plast] );
//
//	Chain( linepts2, ["transp",0.5, "r",0.3, "color","red"] );
//
//	//rp = rodPts( linepts,["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	//Chain( rp );
//
//	rp1 = rodPts( slice(linepts, 0,3),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp1 );
//
//	rp2 = rodPts( slice(linepts, 1,4),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp2 );
		
//	pts= joinarr( 
//			[for(c=range(1,count+1))
//				let( pqr=[ linepts2[c-1]
//						, linepts2[c]
//						, linepts2[c+1]
//						]
//				   , ang = angle(pqr)/2
//					)
//				rodPts(pqr,["sides",sides,"q_angle", ang] )
//			]
//		);

//[ "r", 0.05
//	  , "sides",     6
//	  , "label", "" 
//	  , "p_pts",undef  // pts on the P end
//	  , "q_pts",undef  // pts on the q end
//	  , "p_angle",90   // tiltting angle on p end
//	  , "q_angle",90   // tiltting angle on q end
//	  , "rotate",0  // rotate about the PQ line, away from xz plane
//	  , "tiltAngleAfterRotate",true // to determine if tiltting first, or
//						  // rotate first. 
//	  ]
	faces = faces("chain", sides=sides, count=count);

//	echo("faces_demo_5_chain.N=", N);
	echo("<br/>faces_demo_5_chain._allrps=", _allrps);
	echo("<br/>faces_demo_5_chain.allrps=", allrps);
	//echo("<br/>faces_demo_5_chain.pts=", pts);
	echo("<br/>faces_demo_5_chain.faces=", faces);
	color(false, 0.8)
	polyhedron( points = allrps
			 , faces = faces
			 );
}
//faces_demo_5_chain();

module faces_demo_6_tubeEnd(){
    
    echom("faces_demo_6_tubeEnd");
    
    
    module Mark( pqr, nside=4, nside_in=6){
                 
        pts_in= [ for(i=range(nside_in)) 
                anglePt( pqr, a=90, a2 = i*360/nside_in ,len=2) ];
        pts_out= [ for(i=range(nside)) 
                anglePt( pqr, a=90, a2 = i*360/nside, len=4 ) ];
        
        echo( pts_in = pts_in );
        echo( pts_out= pts_out);
        
        Li = nside_in; 
        Lo = nside;
        
        pts_small = Li<Lo? pts_in:pts_out;
        
        pts = concat( pts_out, pts_in);
        
        echo( len_out= len(pts_out), len_in=len(pts_in) );
        MarkPts(pts_out, "ch=[closed,true];l" );
        MarkPts(pts_in, ["ch=[closed,true]]"
                        ,"label",["text", range(Lo,Lo+Li)]
                        ]  );

        midpts = [ for(i=range( min(Lo,Li) )) 
                   midPt( get(pts_small, [i, pts_small[i+1]?i+1:0]))
                 ];
        //echo(midpts= midpts);

        MarkPts( get(midpts,[0,2]), "ch;ball=false");
        MarkPts( get(midpts,[1,3]), "ch;ball=0");


        tube_faces = faces("tubeEnd", nside=nside, nside_in=nside_in);
        echo( tube_faces = arrprn(tube_faces,2));
        echo( range_pts = range(pts) );
        
        ptss= ([ 
         for(i = range(tube_faces)) 
            let(faces= tube_faces[i]
               )
              get(pts, faces)
        ]);
         
        echo(ptss = arrprn(ptss,1));
         
        for( i=range(ptss)) MarkPts( ptss[i]
            , ["ball",false, "plane",["color",randc(),"transp",0.3]]);  
    }
    
    pqr = pqr();
    Mark(pqr, nside=4, nside_in=3);
    
    pqr2= trslP(pqr,4);
    Mark(pqr2, nside=4, nside_in=4);

    pqr3= trslP(pqr,8);
    Mark(pqr3, nside=4, nside_in=5);

    pqr4= trslP(pqr,12);
    Mark(pqr4, nside=4, nside_in=6);
    
    pqr5= trslP(pqr,16);
    Mark(pqr5, nside=4, nside_in=7);

    pqr6= trslP(pqr,20);
    Mark(pqr6, nside=4, nside_in=9);
    
    pqr7= trslP(pqr,25);
    Mark(pqr7, nside=4, nside_in=15);
    
}//faces_demo_6_tubeEnd  
//faces_demo_6_tubeEnd();

module faces_demo_7_tubeEnd_more_out_size(){
    
    echom("faces_demo_7_tubeEnd_more_out_size");
    
    
    module Mark( pqr, nside=4, nside_in=6){
                 
        pts_in= [ for(i=range(nside_in)) 
                anglePt( pqr, a=90, a2 = i*360/nside_in ,len=1.2) ];
        pts_out= [ for(i=range(nside)) 
                anglePt( pqr, a=90, a2 = i*360/nside, len=4 ) ];
        
        echo( pts_in = pts_in );
        echo( pts_out= pts_out);
        
        Li = nside_in; 
        Lo = nside;
        
        pts_small = Li<Lo? pts_in:pts_out;
        
        pts = concat( pts_out, pts_in);
        
        echo( len_out= len(pts_out), len_in=len(pts_in) );
        MarkPts(pts_out, "ch=[closed,true];l" );
        MarkPts(pts_in, ["ch=[closed,true]]"
                        ,"label",["text", range(Lo,Lo+Li)]
                        ]  );

        midpts = [ for(i=range( min(Lo,Li) )) 
                   midPt( get(pts_small, [i, pts_small[i+1]?i+1:0]))
                 ];
        //echo(midpts= midpts);

        MarkPts( get(midpts,[0,2]), "ch;ball=false");
        MarkPts( get(midpts,[1,3]), "ch;ball=0");


        tube_faces = faces("tubeEnd", nside=nside, nside_in=nside_in);
        echo( tube_faces = arrprn(tube_faces,2));
        echo( range_pts = range(pts) );
        
        ptss= ([ 
         for(i = range(tube_faces)) 
            let(faces= tube_faces[i]
               )
              get(pts, faces)
        ]);
         
        echo(ptss = arrprn(ptss,1));
         
        for( i=range(ptss)) MarkPts( ptss[i]
            , ["ball",false, "plane",["color",randc(),"transp",0.8]]);  
    }
    
    pqr = pqr();
    Mark(pqr, nside=3, nside_in=4);
    
    pqr3= trslP(pqr,5);
    Mark(pqr3, nside=5, nside_in=4);

    pqr4= trslP(pqr,10);
    Mark(pqr4, nside=6, nside_in=4);
    
    pqr5= trslP(pqr,15);
    Mark(pqr5, nside=7, nside_in=4);

    pqr6= trslP(pqr,20);
    Mark(pqr6, nside=8, nside_in=4);
    
    pqr7= trslP(pqr,25);
    Mark(pqr7, nside=15, nside_in=4);

    pqr8= trslP(pqr,30);
    Mark(pqr8, nside=15, nside_in=15);
    
}//faces_demo_7_tubeEnd_more_out_size  
//faces_demo_7_tubeEnd_more_out_size();

//echo(test= range(3*2,(3+4)*2) );
module faces_demo_8_tube_w_tubeEnd(){
    
    echom("faces_demo_8_tube_w_tubeEnd");
    
    
    module Mark( pqr, nside=4, nside_in=6){
    
        tubelen = 1.5;
        
        pts_out1= [ for(i=range(nside)) 
                anglePt( pqr, a=90, a2 = i*360/nside, len=4 ) ];
        
        pts_out2= trslN( pts_out1, tubelen);
        
        pts_in1= [ for(i=range(nside_in)) 
                anglePt( pqr, a=90, a2 = i*360/nside_in ,len=2) ];
        pts_in2= trslN( pts_in1, tubelen);
        
        pts_out = concat( pts_out1, pts_out2);
        pts_in  = concat( pts_in1, pts_in2);
        pts = concat( pts_out, pts_in );
        
        echo( pts_in = pts_in );
        echo( pts_out= pts_out);
        
        
        faces = faces("tube", nside= nside, nside_in=nside_in);
        echo( faces= arrprn(faces,2, nl=true));
        polyhedron( points= pts, faces=faces ); 
        
        Li = nside_in; 
        Lo = nside;  
    }
    
    pqr = pqr();
    Mark(pqr, nside=4, nside_in=3);
    
    pqr3= trslN(pqr,9);
    Mark(pqr3, nside=4, nside_in=5);

    pqr4= trslN(pqr,18);
    Mark(pqr4, nside=4, nside_in=6);
    
    pqr5= trslN(pqr,27);
    Mark(pqr5, nside=4, nside_in=7);

    pqr6= trslN(pqr,36);
    Mark(pqr6, nside=4, nside_in=8);
    
    pqr7= trslN(pqr,45);
    Mark(pqr7, nside=4, nside_in=15);

    
}//faces_demo_8_tube_w_tubeEnd  
//faces_demo_8_tube_w_tubeEnd();

module faces_demo_9_tube_w_tubeEnd_more_outside(){
    
    echom("faces_demo_9_tube_w_tubeEnd_more_outside");
    
    
    module Mark( pqr, nside=4, nside_in=6){
    
        tubelen = 1.5;
        
        pts_out1= [ for(i=range(nside)) 
                anglePt( pqr, a=90, a2 = i*360/nside, len=4 ) ];
        
        pts_out2= trslN( pts_out1, tubelen);
        
        pts_in1= [ for(i=range(nside_in)) 
                anglePt( pqr, a=90, a2 = i*360/nside_in ,len=1.6) ];
        pts_in2= trslN( pts_in1, tubelen);
        
        pts_out = concat( pts_out1, pts_out2);
        pts_in  = concat( pts_in1, pts_in2);
        pts = concat( pts_out, pts_in );
        
        echo( pts_in = pts_in );
        echo( pts_out= pts_out);
        
        
        faces = faces("tube", nside= nside, nside_in=nside_in);
        echo( faces= arrprn(faces,2, nl=true));
        polyhedron( points= pts, faces=faces ); 
        
        Li = nside_in; 
        Lo = nside;  
    }
    
    pqr = pqr();
    Mark(pqr, nside=3, nside_in=4);
    
    pqr3= trslN(pqr,9);
    Mark(pqr3, nside=5, nside_in=4);

    pqr4= trslN(pqr,18);
    Mark(pqr4, nside=6, nside_in=4);
    
    pqr5= trslN(pqr,27);
    Mark(pqr5, nside=7, nside_in=4);

    pqr6= trslN(pqr,36);
    Mark(pqr6, nside=8, nside_in=4);
    
    pqr7= trslN(pqr,45);
    Mark(pqr7, nside=15, nside_in=4);

    pqr8= trslN(pqr,54);
    Mark(pqr8, nside=15, nside_in=15);
    
}//faces_demo_9_tube_w_tubeEnd_more_outside  
//faces_demo_9_tube_w_tubeEnd_more_outside();

module faces_demo_10_tube_cone(){
    
    echom("faces_demo_10_tube_cone");
    
    
    module Mark( pqr, nside=4, ){
    
        tubelen = 1.5;
        
        pts_out1= [ for(i=range(nside)) 
                anglePt( pqr, a=90, a2 = i*360/nside, len=4 ) ];
        
        pts_out2= trslN( pts_out1, tubelen);
        
        pts_in1= [ for(i=range(nside_in)) 
                anglePt( pqr, a=90, a2 = i*360/nside_in ,len=1.6) ];
        pts_in2= trslN( pts_in1, tubelen);
        
        pts_out = concat( pts_out1, pts_out2);
        pts_in  = concat( pts_in1, pts_in2);
        pts = concat( pts_out, pts_in );
        
        echo( pts_in = pts_in );
        echo( pts_out= pts_out);
        
        
        faces = faces("tube", nside= nside, nside_in=nside_in);
        echo( faces= arrprn(faces,2, nl=true));
        polyhedron( points= pts, faces=faces ); 
        
        Li = nside_in; 
        Lo = nside;  
    }
    
    pqr = pqr();
    Mark(pqr, nside=3, nside_in=4);
    
//    pqr3= trslN(pqr,9);
//    Mark(pqr3, nside=5, nside_in=4);
//
//    pqr4= trslN(pqr,18);
//    Mark(pqr4, nside=6, nside_in=4);
//    
//    pqr5= trslN(pqr,27);
//    Mark(pqr5, nside=7, nside_in=4);
//
//    pqr6= trslN(pqr,36);
//    Mark(pqr6, nside=8, nside_in=4);
//    
//    pqr7= trslN(pqr,45);
//    Mark(pqr7, nside=15, nside_in=4);
//
//    pqr8= trslN(pqr,54);
//    Mark(pqr8, nside=15, nside_in=15);
    
}//faces_demo_10_tube_cone  
//faces_demo_10_tube_cone();

module _fmt_demo()
{
	echom("_fmt");
	echo( str("# ", _fmt(345), " and array ", _fmt([2,"a",[3,"b"], 4]), " are ", _fmt("formatted"), " by ", _fmt("fmt with     5 spaces") 
			) ); 
	echo( _fmt(true), _fmt(false), _fmt(undef));
}

module func2d_demo(){
 
 pqr = pqr();
 MarkPts(pqr, "ch;l=PQR;r=0.04");   
 baseline1 = onlinePt( p12(pqr),-2); // baseline
 baseline0 = pqr[0] + baseline1- pqr[1];
 baseline= [baseline0,baseline1];
 MarkPts( baseline, "l=[text,[A,B]];r=0.04");   
 
 pts1 = func2d("line", pq=pqr); //, baseline=baseline);
 echo(pts1=pts1);
 MarkPts( pts1,"r=0.06;trp=0.2");
    
    
}    
//func2d_demo();


module incenterPt_demo()
{
	pqr = randPts(3);
	ic  = incenterPt(pqr);
	MarkPts( concat(pqr,[ic]) );
	Chain( pqr );
}
//incenterPt_demo();

module intersectPt_demo()
{
	echom("intersectPt");

	rstu=  [[-2.65857, 0.0436345, -1.84591], [-1.33038, -1.84482, 1.12422], [3.58766, 0.935956, 0.693008], [2.25947, 2.8244, -2.27714]];

	T = rstu[2];

	MarkPts( rstu, ["labels","RSTU"] );
	
	pq= [[-0.591133, -2.27006, -1.82472], [2.36348, 2.54317, 0.528988]];

	pq2 = [ onlinePt( pq, ratio=0.25 ), onlinePt( pq, ratio= 0.75 ) ];
	P = pq2[0];
	Q = pq2[1];
	//echo( "pq2", pq2 );

	J = projPt( P, rstu );

	Line0( [P,J], ["r",0.01] );
	Mark90([P,J,T]);

	aJPQ = angle([J,P,Q]);
	echo("aJPQ", aJPQ);
	PX = dist([P,J])/cos( aJPQ);
	//X = onlinePt( [P,Q], dist = PX);

	X = onlinePt( [P,Q], len= dist([P,projPt( P, rstu )])/cos( angle([projPt( P, rstu ),P,Q])) );

	X = intersectPt( pq, rstu );

	MarkPts([P,Q,J,X], ["labels", "PQJX"]); 

	Line0( pq, ["r", 0.01] );
	Plane( rstu );
}


module intersectPt_demo_2()
{
	echom( "intersectPt_demo_2" );
	pq = randPts(2);
 	pq = [[-1.21325, 0.501097, -1.23254], [1.71766, 1.58609, 0.690459]];

	P = pq[0];
	Q = pq[1];
	rstu = randOnPlanePts(4);
	//rstu= [[2.51256, 0.0578135, -3.09367], [1.32865, 4.43852, -1.8887], [-5.60561, 0.027173, 3.29636],[0.429039, -0.251856, -1.47247] ];

	echo("pq", pq);
	echo("rstu",rstu);

	J = projPt( pq[0], rstu );
	echo("J", J );

	aJPQ = angle( [J,P,Q]);
	echo("aJPQ", aJPQ);

	//Dim( [P,J,rstu[1] ] );
	
	PJ = dist([P,J]);
	echo("PJ",PJ);

	PX = PJ / cos(aJPQ);
	echo("PX", PX);

	X = onlinePt( [P,Q], len = PX );
	//echo( "X", X);
	MarkPts([J,X], ["labels","JX"]);

	Line0( [J, rstu[0]], ["r",0.01] );	
	Line0( [J, rstu[1]], ["r",0.01] );	
	Line0( [J, rstu[2]], ["r",0.01] );	
	Line0( [J, rstu[3]], ["r",0.01] );	

	Plane( rstu );
	MarkPts( pq, ["labels", "PQ"] );
	MarkPts( rstu, ["labels","RSTU"] );
	//X = intersectPt( pq, rstu );
	//MarkPts( X, ["labels","X"] );
	Line0(pq, ["r",0.02] );
}

//intersectPt_demo();
//intersectPt_demo_2();


module is90_demo()
{
	pqr = randPts(3);

	sqPts = 	squarePts(pqr);   // This gives 4 points of a square, must be perpendicular

	//MarkPts(sqPts);

	pq = [ pqr[0], pqr[1]   ];
	pq2= [ pqr[1], sqPts[2] ];
	
	//echo(( pqr[0]-pqr[1])*(pqr[1]-sqPts[2] ));
	p3 = concat( pq, [pq2[1]] ); // 3 points that the middle is the 90-deg angle 

	//v=pow(norm( p3[0]-p3[1]),2)+ pow(norm(p3[1]-p3[2]),2)- pow( norm(p3[0]-p3[2]),2);
	//echo(v, v<1e-10, v ==0);

	Plane( p3, [["mode",3]] );
	MarkPts( p3 );
	echo( is90( pq, pq2 ) );

}

module isSameSide_demo()
{
    pqr = randPts(3);
    Plane( pqr, ["mode",3]); Chain(pqr);
    MarkPts( pqr ); LabelPts( pqr, "PQR");
    
    
    pts = randPts(3);
    
    MarkPts( pts );
    ss= [ for( p = pts )  sign( distPtPl( p, pqr ) ) ];
    LabelPts( pts, ss );
    
    LabelPt(ORIGIN, isSameSide( pts, pqr ) );    
    
}

//isSameSide_demo();

module intsec_demo_lnln(){
   
   //ColorAxes();
   echom("intsec_demo_lnln"); 
   pqr=pqr();
    
    P=pqr[0]; 
    
   Q=onlinePt( pqr, len= rand([-1,-8,1,8]));
   R=pqr[2]; S= onlinePt( [R,pqr[1]], len=2);
   
   MarkPts( [P,Q], ["chain",true, "label","PQ"]);   
   MarkPts( [R,S], ["chain",true, "label","RS"]);   
   
   ln1= linePts( p01(pqr), len=5);
   ln2= linePts( p12(pqr), len=5);
   
   Line0( ln1, ["r",0.005,"color","red"]);    
   Line0( ln2, ["r",0.005,"color","green"]);    
    
   M = intsec( [P,Q],[R,S] );
   echo(M=M);  
   MarkPt( M, ["color","black", "r", 0.06
              ,"label"," intsec"] ); 
    
   MarkPts( pqr, ["r", 0.2,"transp",0.2]); 
    
}    

//intsec_demo_lnln();


//. g
//==================================================


module getAnglePtData_demo(){
    echom("getAnglePtData_demo");
    echo(_red("Demo how to get anglePtData, and transfer it to any new pqr") );
    
    pqr=pqr(); 
    pqr = [[1.51508, 2.76534, -2.29657], [-1.52943, -1.2732, 0.638371], [1.08919, 2.99655, 1.93264]];
    MarkPts(pqr, "ch;l=PQR");
    echo(pqr=pqr);

    N = N(pqr);
    MarkPt(N,"l=N;cl=blue");
    
    X= randPt();
    echo(X = X );
    MarkPt(X, "l=X");
    //Plane( [Q,J,X] );
    
    P=P(pqr); Q=Q(pqr); R=R(pqr);
    //---------------------------------------
    
    apdata = anglePtData( pqr, X);
    echo(apdata = apdata);    
    J = apdata[3];
    MarkPt(J,"l=J;cl=green");
    Mark90( [X,J,P],"ch");
    Mark90( [X,J,Q],"ch");
    Mark90( [X,J,R], "ch");
    
    aPQJ = angle([P,Q,J], Rf= N); // R is critical
    aJQX = angle([J,Q,X], Rf= onlinePt([Q,P],len=sign(aPQJ))) ; 
    
    echo(apdata = apdata);    
    echo( aPQJ = aPQJ );
    echo( aJQX = aJQX );
    // P is critical
    //echo(aJQR = angle([J,Q,X], Rf= R) ); // P is critical
    
    //####################################################
    //
    //  SHOW HOW TO TRANSFORM X on one pqr to another 
    //
    //####################################################
    pqr3 = trslN(pqr(),4);
    echo(pqr3=pqr3);
    Q3=Q(pqr3); R3=R(pqr3);
    P3=onlinePt( p10(pqr3), dist( pqr) );
    MarkPts( pqr3, "ch=[color,red]");
    
    abc = apdata;
    X3 = anglePt( pqr3, a=apdata[0], a2=apdata[1], len=apdata[2] );
    MarkPt(X3,"l=X3;cl=blue");
    J3 = projPt(X3, pqr3);
    MarkPt(J3,"l=J3;cl=green");
    Mark90( [X3,J3,P3],"ch");
    Mark90( [X3,J3,Q3],"ch");
    Mark90( [X3,J3,R3], "ch");
    
    Plane([P,Q,X]);
    echo( tria_props= tria_props( [P,Q,X] ) );
    color("red")
    Plane([P3,Q3,X3]);
    echo( tria_props3= tria_props( [P3,Q3,X3] ) );
    
}
//getAnglePtData_demo();




module getBezierPts_demo(){
   
   echom( "getBezierPts_demo"); 
    
   n = 10;
    
   module MarkBzPts( pts, n=n
                   , markMain=true
                   , markT=true // T: tangent and abi lines 
                   , markH=true
                   )                      // =true,false, [true,true]  
   {
       
       
   bzpts = getBezierPts( pts, n=n);
           /*  [ bzpts
               , [P,Q,R,S]
               , [tlnQ, tlnR]
               , aH
               , dH 
               , [Hq,Hr]
               ]        */
   pqrs= bzpts[1];
   tlnQ_R = bzpts[2];
   aH = bzpts[3];
   dH = bzpts[4];
   Hq_Hr = bzpts[5];
   as = bzpts[6];
    
   tlnQ = tlnQ_R[0];
   tlnR = tlnQ_R[1];
   
   echo( bzpts = bzpts[0]); 
   echo( tlnQ_R= tlnQ_R );
   echo( aH = aH );
   echo( dH = dH );
   echo( Hq_Hr = Hq_Hr );
   echo( aPQR = as[0], aQRS=as[1]);
   echo( last = last(bzpts));
   Hq1 = Hq_Hr[0];
   Hr1 = Hq_Hr[1];
   
   //--------------------------------------
   // Given pqrs and generated Bezier curve pts
   
   MarkPts( bzpts[0], "cl=red;ch;r=0.05" );
   
   if(markMain){
       MarkPts( pqrs, ["l=PQRS", "r",0.05
                 ,"chain",["r",0.02]] );
   }
   //--------------------------------------
   // abi and tangent lines  
   
   markT = isarr(markT)?markT:[markT,markT];
   
   abiOpt = "ch=[r,0.015];cl=green;l;ball=false;trp=0.1";
   
   if(markT[0]){
       abiQ = angleBisectPt( p012(pqrs) );
       Chain( [pqrs[1], abiQ], abiOpt); 
       Mark90( [ tlnQ[1],pqrs[1], abiQ], abiOpt );
       MarkPts( tlnQ, abiOpt); 
   }
   
   if(markT[1]){
       abiR = angleBisectPt( qrs);
       Chain( [pqrs[2], abiR], abiOpt); 
       Mark90( [ tlnR[0],pqrs[2], abiR],abiOpt );
       MarkPts( tlnR, abiOpt); 
   }
   
   //--------------------------------------
   // HCP (Handle Control Pt) 
   markH = isarr(markH)?markH:[markH,markH];
   if(markH[0]){  
      Chain( [pqrs[1], Hq1], "r=0.01;cl=gray"); 
      MarkPt( Hq1, "r=0.05;cl=gray;l=Hq");
   }
   if(markH[1]){
      Chain( [pqrs[2], Hr1], "r=0.01;cl=gray"); 
      MarkPt( Hr1, "r=0.05;cl=gray;l=Hr");
   }    
  } //MarkBzPts()
  
  qrs= randPts(3, d=[1,5]);
  Chain( qrs, ["r",0.05] );
  MarkBzPts( qrs, n, markT=true);
  MarkBzPts( get(qrs, [2,1,0]), n=n, markMain=false, markT=[1,0] );
  
//  qrs2 = trslN2(qrs,5);
//  Chain( qrs2, ["r",0.05,"color","red"] );
//  MarkBzPts( qrs2, n, markMain=false, markT=0);
//  MarkBzPts( get(qrs2, [2,1,0]), n=n, markMain=false, markT=false );
//  
//  qrs3 = trslN2(qrs,10);
//  Chain( qrs3, ["r",0.05,"color","green"] );
//  MarkBzPts( qrs3, n, markH=false, markMain=false, markT=0);
//  MarkBzPts( get(qrs3, [2,1,0]), n=n, markH=false, markMain=false, markT=false );
//  
  //==================================
 
  
//   bzpts2 = getBezierPts( 
//          get(qrs, [2,1,0], n=n));
//   echo( bzpts2 = bzpts2[0]); 
//   echo( Hp_Hq = bzpts2[1] );
//   H21 = bzpts2[1][0];
//   H22 = bzpts2[1][1];
   
   //MarkPts( bzpts2[1], "r=0.1;cl=gray;l=qr");
   //MarkPts( bzpts2[0], "ch;r=0.05;trp=0.3" );

//   Chain( [pqr[0],H11], "r=0.01;cl:green");
//   Chain( [pqr[1],H12], "r=0.01;cl:green");
//   Chain( [pqr[1],H22], "r=0.01;cl:green");
//   Chain( [pqr[2],H21], "r=0.01;cl:green");
    
    
//   //===============================

//   pqr1= trslN(pqr,3);
//   MarkPts( pqr1, ["l=PQR;r=0.05;cl=red"
//                  ,"chain",["r",0.02]] );
//   
//   bzpts21 = getBezierPts( pqr1, n=5, cv=1);
//   echo( bzpts21 = bzpts21); 
//   MarkPts( bzpts21, "ch;r=0.05;trp=0.3" );
// 
//   bzpts22 = getBezierPts( 
//                get(pqr1, [2,1,0]
//              , n=5, cv=1));
//   echo( bzpts22 = bzpts22); 
//   MarkPts( bzpts22, "ch;r=0.05;trp=0.3" );

    
}  
//getBezierPts_demo();

module getBezierPtsAt_demo(){ 

    echom("getBezierPtsAt_demo");
    
    pts = chainBoneData(5, d=[2,5], a=[45,135], a2=[0,45]);

    echo( pts= arrprn( pts, 3));
    
    MarkPts( pts, "r=0.05;ch=[r,0.015,color,blue];l" );
        
    module markbz(i,n=10){
        bzrtn = getBezierPtsAt( pts, i, n=n, closed=false
                ,aH=0.5
                ,dH=0.3
                );
        // bzrtn keys: bzpts,pts, Hi, Hj, aH, dH, i, n pqrs  
        
        atend= i==len(pts)-1;
        bz = hash(bzrtn, "bzpts");
        MarkPts( bz, "r=0.02;ch=[r,0.005, color,red]" );
        
        //MarkPts( last(dataj),"ch;l=qrs;closed;trp=0.2");
        
        // H pt and line
        Hi = hash( bzrtn, "Hi" );
        Hj = hash( bzrtn, "Hj" );
        h_op_i = ["ch;ball=false","color","gray","transp",0.2
                 ,"label",["text",["","Hi"],"scale",0.01]];
        h_op_j = ["ch;ball=false","color","gray","transp",0.2
                 ,"label",["text",["","Hj"],"scale",0.01]];
        
        MarkPts( [pts[i], Hi], h_op_i );
        MarkPts( [ pts[atend?0:i+1], Hj], h_op_j);
        
//        MarkPts( [ pts[atend?0:i+1], dataj[0]]
//               , ["ch;ball=false","color","gray","transp",0.2
//               ,"label",["text",["","Hj"],"scale",0.01]] );
    }       
    markbz(0);
    markbz(1);
    markbz(2);
    markbz(3);
    //markbz(4);
    
    //======================
//    pts2 = trslN( pts, 3);
//    echo("<b>pts2</b>");
//    
//    bzpts = [for(i=range(pts2))
//        getBezierPtsAt( pts2, i, n=10, closed=true
//            ,aH=0.5
//            ,dH=0.3
//            )[0]
//            ];
//    echo( bzpts = bzpts);
//    MarkPts( bzpts, "ch" );
 
}
//getBezierPtsAt_demo();


module getBezier_demo(){
   
   echom( "getBezier_demo"); 
    
   pqr= randPts(3, d=[-3,0]);
   P=P(pqr);
   Q=Q(pqr);
   R=R(pqr); 
   S=anglePt( pqr, a= rand(360));

   MarkPts( p01(pqr),"ball=false;ch=[r,0.03];trp=0.3" );
   MarkPts( [P,R],"ch=[r,0.008];l=PR;r=0.05;cl=gray");  
   MarkPts( [Q,S],"ch=[r,0.008];l=QS;r=0.05;cl=gray");  
    
   pts = getBezier( [P,Q], [R, S] ,n=15 );
   echo( pts = pts );
   MarkPts( pts, "ch;cl=red;r=0.04;samesize;sametransp"); 
   
   //===============================
    
   pqr2 = trslN(pqr,1.5);
   
   pts2= getBezier( pqr2, n=10 );
   echo( pts2=pts2);
   MarkPts( pqr2,"ch=[r,0.03];ball=false;l=PQR;trp=0.3");  
   MarkPts( pts2, "ch;color=red;r=0.04;samesize;sametransp"); 
}    

//getBezier_demo();

//module getBezier_old_demo_d(){
//    
//   echom( "getBezier_old_demo_d"); 
//
//   //===============================
//    
//   pqr2 = randPts(3, d=[0,3]);
//   
//   pts2= getBezier_old( pqr2, n=10 );
//   //echo( pts2=pts2);
//   MarkPts( pqr2,"ch=[r,0.03];ball=false;l=PQR");  
//   MarkPts( pts2, "ch;color=black;r=0.03"); 
//    
//   pqr3 = trslN(pqr2,2);
//   pts3= getBezier_old( pqr3, n=10, d=0.5 );
//   MarkPts( pqr3,"ch=[r,0.03,color,red];ball=false;l=PQR;color=red");  
//   MarkPts( pts3, "ch;color=black;r=0.03"); 
//
//   pqr4 = trslN(pqr3,2);
//   pts4= getBezier_old( pqr4, n=10, d=3 );
//   MarkPts( pqr4,"ch=[r,0.03,color,green];ball=false;l=PQR;color=green");MarkPts( pts4, "ch;color=black;r=0.03"); 
//    
//   pqr5 = trslN(pqr4,2);
//   pts5= getBezier_old( pqr5, n=10, d=5 );
//   MarkPts( pqr5,"ch=[r,0.03,color,blue];ball=false;l=PQR;color=blue");MarkPts( pts5, "ch;color=black;r=0.03"); 
//     
//}    

//getBezier_old_demo_d();

module gerBezier_demo_knot(){
    
   echom("gerBezier_demo_knot"); 
    // knot data taken from 
    // http://www.colab.sfu.ca/KnotPlot/KnotServer/coord/2.html    
    kpts= [ [-5.00478, -2.20299, 0.670091]
    , [-4.88258, -3.10157, -0.092889]
    , [-4.39016, -3.83889, -0.879547]
    , [-3.58148, -4.33844, -1.58905]
    , [-2.5438, -4.54905, -2.12783]
    , [-1.39221, -4.4512, -2.4161]
    , [-0.260491, -4.06523, -2.39773]
    , [0.719727, -3.45674, -2.05399]
    , [1.46218, -2.72936, -1.41635]
    , [1.98739, -1.97697, -0.572567]
    , [2.40977, -1.20091, 0.328836]
    , [2.75931, -0.295204, 1.10407]
    , [2.94144, 0.797771, 1.61339]
    , [2.86544, 1.99092, 1.78476]
    , [2.50415, 3.1243, 1.61289]
    , [1.8892, 4.0411, 1.14695]
    , [1.09079, 4.62244, 0.471398]
    , [0.20147, 4.7968, -0.308471]
    , [-0.677249, 4.54313, -1.07874]
    , [-1.4442, 3.89142, -1.72581]
    , [-2.01031, 2.92229, -2.14655]
    , [-2.31117, 1.76358, -2.2603]
    , [-2.32623, 0.577388, -2.02584]
    , [-2.09856, -0.483797, -1.46105]
    , [-1.73098, -1.36235, -0.652872]
    , [-1.3028, -2.14421, 0.241411]
    , [-0.749772, -2.91679, 1.03701]
    , [0.035892, -3.6493, 1.6018]
    , [1.04906, -4.23677, 1.86424]
    , [2.19005, -4.58035, 1.80777]
    , [3.32658, -4.62323, 1.46205]
    , [4.32915, -4.35433, 0.888819]
    , [5.08931, -3.80048, 0.169054]
    , [5.52611, -3.01879, -0.604689]
    , [5.59217, -2.0899, -1.33538]
    , [5.27824, -1.11183, -1.92633]
    , [4.61907, -0.192878, -2.28828]
    , [3.6983, 0.561824, -2.35037]
    , [2.64638, 1.07048, -2.07684]
    , [1.61169, 1.31239, -1.4844]
    , [0.683465, 1.36521, -0.652044]
    , [-0.189789, 1.35858, 0.268235]
    , [-1.13209, 1.28192, 1.07708]
    , [-2.17837, 1.00458, 1.62779]
    , [-3.22565, 0.462146, 1.84751]
    , [-4.12242, -0.314411, 1.72631]
    , [-4.74167, -1.23833, 1.30823]
    ];

    MarkPts( kpts
           , ["chain",["closed",true,"transp",0.5]
             , "samesize",true, "sametransp",true, "color","blue"
             , "r", 0.06]
           );

    ps = concat( [last(kpts)], app( kpts, kpts[0] ));
    //ps = kpts; 

    function pik(indices)=
        [ for(i=range(ps)) if(has(indices,i)) ps[i]];

    MarkPts( p01(ps), "l");
        
    bzpts = [ for(i= [0:len( ps)-3] )
              let( X = i==0? get(ps,-3):ps[i-1]
                 , P = ps[i]
                 , Q = i==len(ps)-2? ps[0]:ps[i+1]
                 , R = i==len(ps)-2? ps[1]:ps[i+2]
                 , dPQx = dist( [P,Q] )/3
                 , abiP = angleBisectPt( [X,P,Q] )
                 , abiQ = angleBisectPt( [P,Q,R] )
                 , Hp = anglePt([ abiP,P,Q], a=90, len=dPQx) 
                 , Hr = anglePt([ abiQ,Q,R], a=90, len=-dPQx) 
                 , mpt= midPt( [Hp,Hr] )
                // , bz = getBezierPts( [ps[i],mpt,ps[i+1]],[Hp,Hr], n=5)[0]
                 , bz = getBezier( [ps[i],ps[i+1]],[Hp,Hr], n=5)
                 )
              slice(bz,0,-1)
           ];
    echo( bzpts= arrprn(bzpts,dep=3));

    MarkPts( joinarr(bzpts)
           , ["chain",["closed",true,"color","red"]
             , "samesize",true, "sametransp",true,"color","red"
             , "r", 0.03]
           );
            
}
//gerBezier_demo_knot();



module getBoxData_demo(){

    echom("getBoxData_demo");
    pts = randPts(50);

    MarkPts( pts, "samesize;cl=darkcyan;sametransp" );
    
    pqr = pqr();
    echo( pqr = pqr );
    MarkPts( pqr, "ch");
    boxdata = getBoxData( pts, pqr );
    echo( boxdata = boxdata );
    
    outerPts = hash( boxdata, "outerPts" );
    echo( outerPts=outerPts );
    MarkPts( outerPts, "r=0.2;trp=0.3" );
    outerPtIndices = hash( boxdata, "outerPtIndices" );
    echo(outerPtIndices=outerPtIndices);
    
    boxpts = hash(boxdata, "boxpts" );
    echo(boxpts = boxpts );
    MarkPts( boxpts, "color=red;l=EFGHIJKL");
    
    center = hash(boxdata, "center");
    MarkPt( center, "color=black;r=0.2");
    
    color(undef, 0.3)
    polyhedron( points = boxpts, faces=faces("rod",nside=4));
    
    
//    Plane( joinarr( bpminx, bpminy, bpminz ) );
    
    
}
//getBoxData_demo();

module getPairingData_sorted_afips_demo()
{
    echom("getPairingData_sorted_afips_demo");
    
    pqr= pqr();
    pqr =  [[2.02391, 0.977828, 2.76615], [2.51219, -1.8059, -1.15628], [1.7283, -2.1596, 0.479486]];
    echo( pqr = pqr );
    MarkPts( pqr, "ch=[r,0.05];l=PQR");
    
    n=4;
    pl1= arcPts( pqr, rad=4, a=90, a2= 360/n*(n-1), n=n);
    //pl1= arcPts( pqr, rad=4, a=90, a2= 360/3*2, n=3);
    
    m=5;
    pl2= arcPts( pqr, rad=1.5, a=90, a2= 360/m*(m-1), n=m);

    
    MarkPts( pl1, "ch");
    MarkPts( pl2, "ch");

    afipsdata = getPairingData_sorted_afips( pl1,pl2,C1=pqr[1] );
    echo( afipsdata= afipsdata );
    afips = hash( afipsdata, "afips" );
    echo( "afips: ", len(afips) );
    //for(x=afips[0]) echo( x );    
    for(i= range(afips)) {
        x= afips[i];
        echo(  x );//[afips[0][i-1][0]== afips[0][i][0], x] );    
        
    }
    
    afips1 = hash( afipsdata, "afips1" );
    afips2 = hash( afipsdata, "afips2" );
    
    echo( "afips1: ", len(afips1) );
    for(x=afips1) echo(x);    
    echo( "afips2: ", len(afips2) );
    for(x=afips1) echo(x); 
// 
//    conc= concat( afips[1], afips[2]);
//    echo( "concat: ", len(conc) );
//    for(x=conc) echo(x); 
//     
//    sorted= sort( conc,0,1);
//    echo( "sorted: ", len(sorted) );
//    //for(x=sorted) echo(x); 
//    echo( sorted = arrprn( sorted, nl=1 ) );
    
}    
//getPairingData_sorted_afips_demo();


module getPairingData_ang_seg_demo()
{
    echom("getPairingData_ang_seg_demo");
    
    pqr = pqr();
    //pqr =  [[2.10451, -2.78307, -1.49051], [2.67476, -0.221936, -0.88846], [-0.962814, 0.992293, -1.91018]];
    echo( pqr = pqr );
    
    pts = circlePts( pqr, rad=3, n=5 );
    MarkPts( pts, "ch" );
    
    //asegs= getParingData_ang_seg( pts, C, rot, Rf)
    asegs= getPairingData_ang_seg( pts=pts);
    echo( asegs = asegs );
    for(i=range(asegs)){
        x = asegs[i];
        a = x[0]; 
        ln= x[1];
       
        color(COLORS[i], 0.1)
        MarkPts( ln, "ch=[r,0.15,transp,0.1]");
        echo(x);
        
    }    
            
}
//getPairingData_ang_seg_demo();


module getPairingData_demo1()
{
    echom("getPairingData_demo1");
    
    pqr= pqr();
    //pqr =  [[2.02391, 0.977828, 2.76615], [2.51219, -1.8059, -1.15628], [1.7283, -2.1596, 0.479486]];
    //pqr= [[2.90273, 1.20523, -2.91328], [-2.67702, -2.57288, 1.23009], [-1.19684, -0.691646, -0.702125]];
    
    pqr25= [ pqr[0], pqr[1], rotPt( pqr[2], p01(pqr), 30 )];
    echo( pqr = pqr );

    pl1= circlePts( pqr, rad=2, n= 5);//int(rand(3,10)));
    pl2= circlePts( pqr, rad=4, n=4);//int(rand(3,10)));
    echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch");Line0( get(pl1,[0,-1]));
    MarkPts( pl2, "ch");Line0( get(pl2,[0,-1]));

    pl1=[ [-1.12721, 2.18993, 2.09288]
        , [-1.3542, 0.955396, 2.74434]
        , [-0.0841191, 0.498911, 2.32184]
        , [0.142873, 1.73344, 1.67038]
        ];
    pl2= [ [-3.46691, 2.18869, -3.34382]
        , [-4.1315, 1.38987, -2.79414]
        , [-3.53551, 0.376895, -2.76874]
        , [-2.50257, 0.549672, -3.30274]
        , [-2.46017, 1.66942, -3.65816]
        ];

    pairdata = getPairingData( pl1,pl2,c1=pqr[1] );
    echo( pairdata= arrprn(pairdata,3) );
    C1 = hash(pairdata, "C1");
    C2 = hash(pairdata, "C2");
    
    afips = hash(pairdata, "afips");
    afips1 = hash(pairdata, "afips1");
    afips2 = hash(pairdata, "afips2");
    //echo("afips:");
    //for( x = afips ) echo(x );
   
    echo(""); 
    //echo("afips2:");
    //for( x = afips2 ) echo(x );
        
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");
    
    MarkPts( pl1new, ["r=0.15;trp=0.2;sametransp"] );
    //    ,"label",["color","red","transp",1]] );
    
        //for( p = pl1new ) color("khaki", 0.3) Line0( [C1, p] );
        for( i = range(pl1new) ){ 
                p1= pl1new[i];
                p2= pl2new[i];
            color("khaki", 0.3) Line0( [C1, p1] );
            color("khaki", 0.3) Line0( [C2, p2] );
          //  color("khaki", 0.3) Line0( [p1, p2] );
        }
        
    MarkPts( pl2new, ["r=0.15;trp=0.2;sametransp"]);
        
    echo( len=len(pl1new), pl1new= _colorPts(pl1new));
    echo( len=len(pl2new), pl2new= _colorPts(pl2new) );
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    color(undef, 0.3)
    polyhedron( points = hash( pairdata, "pts")
              , faces = faces );    
    
}    
//getPairingData_demo1();

module getPairingData_demo2()
{
    echom("getPairingData_demo2");
    
    pqr= pqr();
    echo( pqr = pqr );
    MarkPts( pqr, "ch;l=PQR");
    
    n1 = 36;//int(rand(3,12));
    n2 = 4; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=1.5, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch;sametransp");Line0( get(pl1,[0,-1]));
    MarkPts( pl2, "ch;sametransp;r=0.05");Line0( get(pl2,[0,-1]));

    pairdata = getPairingData( pl1,pl2); //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
    C1 = hash(pairdata, "C1");
    C2 = hash(pairdata, "C2");
    
    //afips = hash(pairdata, "afips");
    //afips1 = hash(pairdata, "afips1");
    //afips2 = hash(pairdata, "afips2");
    //echo("afips:");
    //for( x = afips ) echo(x );
   
    echo(""); 
    //echo("afips2:");
    //for( x = afips2 ) echo(x );
        
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");
    
    // Raise/Sink pl2new by some distance 
    pl2new2 = trslN( pl2new, rand(0,8));
    MarkPts( pl2new2, "ch;sametransp");Line0( get(pl2,[0,-1]));

    MarkPts( pl1new, ["r=0.1;trp=0.2;sametransp"] );
    //MarkPts( pl2new, ["r=0.1;trp=0.2;sametransp"]);
        
    //echo( pl1new= _colorPts(pl1new), len(pl1new));
    echo( pl2new= _colorPts(pl2new), len(pl2new));
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new2); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
    for(i = range(pl1new) )
        color( get(COLORS, i, cycle=true), 0.6 )
        Plane( get(pts, faces[i]));
}    
//getPairingData_demo2();

module getPairingData_demo_loft()
{
    echom("getPairingData_demo_loft");

    pqr= pqr();
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n1 = 500;
    n2 = 6; 
    pl1= circlePts( pqr, rad=3, n= n1 );
    pl2= circlePts( pqr, rad=2.8, n=n2);
    pairdata = getPairingData( pl1,pl2); 
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2"); 
 
    h = 2;
    v= onlinePt( p10(pqr),len=h)-Q(pqr);
    pts = concat( [for(p=pl1new) p+ v], pl2new);
    
    faces = faces("rod", nside=len(pl1new));
    
    polyhedron( points= pts, faces=faces );   
    
}    
//getPairingData_demo_loft(); // 3'3"

module getPairingData_demo_loft2()
{
    echom("getPairingData_demo_loft2"
          , "Try dividing height to levels"
          );
    
    pqr= pqr();
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n1 = 144;
    n2 = 5; 
    nlevel = 30;
    pl1= circlePts( pqr, rad=3, n= n1 );
    pl2= circlePts( pqr, rad=2, n=n2);
    
              
    pairdata = getPairingData( pl1,pl2); 
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2"); 
    
    h = 5;
    v= onlinePt( p10(pqr),len=h)-Q(pqr);
    pl2new_raised = [for(p=pl2new) p+v] ;
    
   cuts = [ for (lv = range(nlevel+1))
              [ for (i= range(pl1new))
                 onlinePt( [pl1new[i],pl2new_raised[i]]
                         , ratio= lv/nlevel)
              ]
           ];
 

    pts = joinarr( cuts );
    
    faces = faces("chain", nside=len(pl1new), nseg= nlevel);
    polyhedron( points= pts, faces=faces );
    
}    
//getPairingData_demo_loft2(); // 

module getPairingData2_dev_demo()
{
    echom("getPairingData2_dev_demo");
    
    pqr= pqr();
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n = 6; 
    radlnPts = circlePts( pqr, rad=5, n=n );
    //MarkPts( radlnPts, "ch" );
    for( i=range(n) ) 
        MarkPts( [pqr[1], radlnPts[i]], ["ch=[color,red];ball=0","label",["text",["",i]],"color","black"]);
    
    n1 = 5;//int(rand(3,12));
    n2 = 4; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=1.5, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch;sametransp;ball=0");
    MarkPts( pl2, "ch;sametransp;ball=0");

    pairdata = getPairingData2( pl1,pl2, n=n);
    echo( status = //_b(_red(replace( 
                    hash( pairdata, "debug" ) 
                    //, "<", "&lt;")))
        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");

    MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;l=[color,red];ball=0" );
    MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;l=[color,green];ball=0");
        
    echo( len(pl1new), pl1new= _colorPts(pl1new) );
    echo( len(pl2new), pl2new= _colorPts(pl2new) );
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    for( i=range(pl1new))
        MarkPts( [pl1new[i],pl2new[i]], "ch;ball=0" );
    
}    
//getPairingData2_dev_demo();

module getLoftData_dev_20150920()
{
    echom("getLoftData_dev_20150920");
    
    pqr= pqr();
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    pqr = [ [1,0,0], ORIGIN, [0,1,0]]; 
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n = 6; 
    radlnPts = circlePts( pqr, rad=5, n=n );
    //MarkPts( radlnPts, "ch" );
    for( i=range(n) ) 
        MarkPts( [pqr[1], radlnPts[i]], ["ch=[r,0.02,color,blue];ball=0","label",["text",["",i]],"color","blue"]);
    
    n1 = 5; //int(rand(3,12));
    n2 = 4; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=1.5, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch=[closed,true,r,0.03,color,red];sametransp;ball=0");
    MarkPts( pl2, "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");

    pairdata = getLoftData_dev_20150920( pl1,pl2, n=n);
    echo( status = //_b(_red(replace( 
                    hash( pairdata, "debug" ) 
                    //, "<", "&lt;")))
        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");

    MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;l=[color,red];ball=0" );
    MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;l=[color,green];ball=0");
        
    echo( len(pl1new), pl1new= _colorPts(pl1new) );
    echo( len(pl2new), pl2new= _colorPts(pl2new) );
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    for( i=range(pl1new))
        MarkPts( [pl1new[i],pl2new[i]], "ch;ball=0" );
    
    //---------------------------------------------
    //
    // On graph doc
    //
    //---------------------------------------------
    C = centerPt(pl1);
    // dP: a vertical vector 
    dP = anglePt( [radlnPts[0],C,radlnPts[1]],a=90)- C;     
    Write("getLoftData_dev_20150920( )"
         , at=[ for(p=[ radlnPts[0] 
                      , C
                      , anglePt( [radlnPts[0],C,radlnPts[1]],a=90)
                      ]) p+ dP*1.1 ]
         , size=0.5, th=0.1, halign="center" );
    

    //---------------------------------------------
    txtpqr = [ radlnPts[0] - dP*1.1
              , C- dP*1.1 //radlnPts[ floor(3*n/4) ]
              , C
              ];
    //MarkPts( txtpqr, "ch=[r,0.5];l=PQR");
    
   Write(_s("red: pts1 (n1={_})",n1)
            , at = [for(p=txtpqr) p]
            , size=0.5, th=0.1, color="red",halign="center" );
    
    Write( _s("green: pts2 (n1={_})",n2)
            ,at=[for(p=txtpqr) p-dP*0.2 ]
           , size=0.5, th=0.1, color="green",halign="center" );
    
    Write(_s("blue: n={_} for resolution",n)
            , at = [for(p=txtpqr) p-dP*0.4 ]
            , size=0.5, th=0.1, color="blue",halign="center" );
    //---------------------------------------------

    
}    
//getLoftData_dev_20150920();

module getLoftData_dev_20150921()
{
    echom("getLoftData_dev_20150921");
    
    pqr= pqr();
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    pqr = [ [1,0,0], ORIGIN, [0,1,0]]; 
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n = 9; 
    radlnPts = circlePts( pqr, rad=5, n=n );
    //MarkPts( radlnPts, "ch" );
    for( i=range(n) ) 
        MarkPts( [pqr[1], radlnPts[i]], ["ch=[r,0.02,color,blue];ball=0","label",["text",["",i]],"color","blue"]);
    
    n1 = 5; //int(rand(3,12));
    n2 = 4; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=1.8, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch=[closed,true,r,0.03,color,red];sametransp;ball=0");
    MarkPts( pl2, "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");

    pairdata = getLoftData( pl1,pl2, n=n);
    echo( status = //_b(_red(replace( 
                    hash( pairdata, "debug" ) 
                    //, "<", "&lt;")))
        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");

    MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;l=[color,red];ball=0" );
    MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;l=[color,green];ball=0");
        
    echo( len(pl1new), pl1new= _colorPts( [for(ps=pl1new)
                                            [ for(p=ps) is0(p)?0:p
                                            ]
                                          ]    
                                        ) 
        );
    echo( len(pl2new), pl2new= _colorPts( [for(ps=pl2new)
                                            [ for(p=ps) is0(p)?0:p
                                            ]
                                          ]    
                                        ) 
        );
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    for( i=range(pl1new))
        MarkPts( [pl1new[i],pl2new[i]], "ch;ball=0" );
    
    //---------------------------------------------
    //
    // On graph doc
    //
    //---------------------------------------------
    // Title
//    C = centerPt(pl1);
//    // dP: a vertical vector 
//    dP = anglePt( [radlnPts[0],C,radlnPts[1]],a=90)- C;     
//    Write("getLoftData_dev_20150921( )"
//         , at=[ for(p=[ radlnPts[0] 
//                      , C
//                      , anglePt( [radlnPts[0],C,radlnPts[1]],a=90)
//                      ]) p+ dP*1.2 ]
//         , size=0.5, th=0.1, halign="center" );
//    
//
//    //---------------------------------------------
//    txtpqr = [ radlnPts[0] - dP*1.2
//              , C- dP*1.2 //radlnPts[ floor(3*n/4) ]
//              , C
//              ];
//    //MarkPts( txtpqr, "ch=[r,0.5];l=PQR");
//    
//   Write(_s("red: pts1 (n1={_})",n1)
//            , at = [for(p=txtpqr) p-dP*0.1]
//            , size=0.5, th=0.1, color="red",halign="center" );
//    
//    Write( _s("green: pts2 (n2={_})",n2)
//            ,at=[for(p=txtpqr) p-dP*0.3 ]
//           , size=0.5, th=0.1, color="green",halign="center" );
//    
//    Write(_s("blue: for resolution (n={_})",n)
//            , at = [for(p=txtpqr) p-dP*0.5 ]
//            , size=0.5, th=0.1, color="blue",halign="center" );
    //---------------------------------------------

    
}    
//getLoftData_dev_20150921();

module getLoftData_demo()
{
    echom("getLoftData_demo");
    
    pqr= pqr();
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    //pqr = [ [1,0,0], ORIGIN, [0,1,0]]; 
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n = 6; 
    radlnPts = circlePts( pqr, rad=5, n=n );
    //MarkPts( radlnPts, "ch" );
    for( i=range(n) ) 
        MarkPts( [pqr[1], radlnPts[i]], ["ch=[r,0.02,color,blue];ball=0","label",["text",["",i]],"color","blue"]);
    
    n1 = 5; //int(rand(3,12));
    n2 = 4; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=1.8, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch=[closed,true,r,0.03,color,red];sametransp;ball=0");
    MarkPts( pl2, "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");

    pairdata = getLoftData( pl1,pl2, n=n);
    echo( status = //_b(_red(replace( 
                    hash( pairdata, "debug" ) 
                    //, "<", "&lt;")))
        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");


    MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;ball=0;l" );
    MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;ball=0;l");
        
//    echo( len(pl1new), pl1new= _colorPts( [for(ps=pl1new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
//    echo( len(pl2new), pl2new= _colorPts( [for(ps=pl2new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    for( i=range(pl1new))
        MarkPts( [pl1new[i],pl2new[i]], "ch;ball=0" );
    
    //---------------------------------------------
    //
    // On graph doc
    //
    //---------------------------------------------
    // Title
//    C = centerPt(pl1);
//    // dP: a vertical vector 
//    dP = anglePt( [radlnPts[0],C,radlnPts[1]],a=90)- C;     
//    Write("getLoftData_dev_20150921( )"
//         , at=[ for(p=[ radlnPts[0] 
//                      , C
//                      , anglePt( [radlnPts[0],C,radlnPts[1]],a=90)
//                      ]) p+ dP*1.2 ]
//         , size=0.5, th=0.1, halign="center" );
//    
//
//    //---------------------------------------------
//    txtpqr = [ radlnPts[0] - dP*1.2
//              , C- dP*1.2 //radlnPts[ floor(3*n/4) ]
//              , C
//              ];
//    //MarkPts( txtpqr, "ch=[r,0.5];l=PQR");
//    
//   Write(_s("red: pts1 (n1={_})",n1)
//            , at = [for(p=txtpqr) p-dP*0.1]
//            , size=0.5, th=0.1, color="red",halign="center" );
//    
//    Write( _s("green: pts2 (n2={_})",n2)
//            ,at=[for(p=txtpqr) p-dP*0.3 ]
//           , size=0.5, th=0.1, color="green",halign="center" );
//    
//    Write(_s("blue: for resolution (n={_})",n)
//            , at = [for(p=txtpqr) p-dP*0.5 ]
//            , size=0.5, th=0.1, color="blue",halign="center" );
    //---------------------------------------------

    
}    
//getLoftData_demo();

module getLoftData_demo_debug()
{
    echom("getLoftData_demo_debug");
    
    pqr= pqr();
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    //pqr = [ [1,0,0], ORIGIN, [0,1,0]]; 
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n = 6; 
    radlnPts = circlePts( pqr, rad=5, n=n );
    //MarkPts( radlnPts, "ch" );
    for( i=range(n) ) 
        MarkPts( [pqr[1], radlnPts[i]], ["ch=[r,0.02,color,blue];ball=0","label",["text",["",i]],"color","blue"]);
    
    n1 = 10; //int(rand(3,12));
    n2 = 10; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=1.8, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch=[closed,true,r,0.03,color,red];sametransp;ball=0");
    MarkPts( pl2, "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");

    pairdata = getLoftData( pl1,pl2, n=n, debug=[2,4]);
    echo( status = //_b(_red(replace( 
                    hash( pairdata, "debug" ) 
                    //, "<", "&lt;")))
        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");

    MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;ball=0;l" );
    MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;ball=0;l");
    
    //echo( "<br/>","pl1new=<br/>", arrprn( pl1new) );
    //echo( "<br/>","pl2new=<br/>", arrprn( pl2new) );
    
//    echo( len(pl1new), pl1new= _colorPts( [for(ps=pl1new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
//    echo( len(pl2new), pl2new= _colorPts( [for(ps=pl2new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    for( i=range(pl1new))
        MarkPts( [pl1new[i],pl2new[i]], "ch;ball=0" );
    
    //---------------------------------------------
    //
    // On graph doc
    //
    //---------------------------------------------
    // Title
//    C = centerPt(pl1);
//    // dP: a vertical vector 
//    dP = anglePt( [radlnPts[0],C,radlnPts[1]],a=90)- C;     
//    Write("getLoftData_dev_20150921( )"
//         , at=[ for(p=[ radlnPts[0] 
//                      , C
//                      , anglePt( [radlnPts[0],C,radlnPts[1]],a=90)
//                      ]) p+ dP*1.2 ]
//         , size=0.5, th=0.1, halign="center" );
//    
//
//    //---------------------------------------------
//    txtpqr = [ radlnPts[0] - dP*1.2
//              , C- dP*1.2 //radlnPts[ floor(3*n/4) ]
//              , C
//              ];
//    //MarkPts( txtpqr, "ch=[r,0.5];l=PQR");
//    
//   Write(_s("red: pts1 (n1={_})",n1)
//            , at = [for(p=txtpqr) p-dP*0.1]
//            , size=0.5, th=0.1, color="red",halign="center" );
//    
//    Write( _s("green: pts2 (n2={_})",n2)
//            ,at=[for(p=txtpqr) p-dP*0.3 ]
//           , size=0.5, th=0.1, color="green",halign="center" );
//    
//    Write(_s("blue: for resolution (n={_})",n)
//            , at = [for(p=txtpqr) p-dP*0.5 ]
//            , size=0.5, th=0.1, color="blue",halign="center" );
    //---------------------------------------------

    
}    
//getLoftData_demo_debug();

module getLoftData_demo2()
{
    echom("getLoftData_demo2");
    
    pqr= pqr();
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    //pqr = [ [1,0,0], ORIGIN, [0,1,0]]; 
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n = 48; 
    radlnPts = circlePts( pqr, rad=5, n=n );
    //MarkPts( radlnPts, "ch" );
    //for( i=range(n) ) 
    //    MarkPts( [pqr[1], radlnPts[i]], ["ch=[r,0.02,color,blue];ball=0","label",["text",["",i]],"color","blue"]);
    
    h = 5;
    dP = onlinePt( p10(pqr), len=h) - pqr[1];
    
    
    n1 = 7; //int(rand(3,12));
    n2 = 4; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=1.8, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch=[closed,true,r,0.03,color,red];sametransp;ball=0");
    MarkPts( [for(p=pl2)p+dP], "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");

    pairdata = getLoftData( pl1,pl2, n=n);
    echo( status = //_b(_red(replace( 
                    hash( pairdata, "debug" ) 
                    //, "<", "&lt;")))
        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    _pl2new = hash(pairdata,"pts2");

    pl2new = [ for(p=_pl2new) p+dP ];
        
    MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;ball=0" );
    MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;ball=0");
        
//    echo( len(pl1new), pl1new= _colorPts( [for(ps=pl1new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
//    echo( len(pl2new), pl2new= _colorPts( [for(ps=pl2new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    for( i=range(pl1new))
        MarkPts( [pl1new[i],pl2new[i]], "ch;ball=0" );
    
    //---------------------------------------------
    //
    // On graph doc
    //
    //---------------------------------------------
    // Title
//    C = centerPt(pl1);
//    // dP: a vertical vector 
//    dP = anglePt( [radlnPts[0],C,radlnPts[1]],a=90)- C;     
//    Write("getLoftData_dev_20150921( )"
//         , at=[ for(p=[ radlnPts[0] 
//                      , C
//                      , anglePt( [radlnPts[0],C,radlnPts[1]],a=90)
//                      ]) p+ dP*1.2 ]
//         , size=0.5, th=0.1, halign="center" );
//    
//
//    //---------------------------------------------
//    txtpqr = [ radlnPts[0] - dP*1.2
//              , C- dP*1.2 //radlnPts[ floor(3*n/4) ]
//              , C
//              ];
//    //MarkPts( txtpqr, "ch=[r,0.5];l=PQR");
//    
//   Write(_s("red: pts1 (n1={_})",n1)
//            , at = [for(p=txtpqr) p-dP*0.1]
//            , size=0.5, th=0.1, color="red",halign="center" );
//    
//    Write( _s("green: pts2 (n2={_})",n2)
//            ,at=[for(p=txtpqr) p-dP*0.3 ]
//           , size=0.5, th=0.1, color="green",halign="center" );
//    
//    Write(_s("blue: for resolution (n={_})",n)
//            , at = [for(p=txtpqr) p-dP*0.5 ]
//            , size=0.5, th=0.1, color="blue",halign="center" );
    //---------------------------------------------

    
}    
//getLoftData_demo2();

module getLoftData_demo3_apply_h()
{
    echom("getLoftData_demo3_apply_h");
    
    pqr= pqr();
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    //pqr = [ [1,0,0], ORIGIN, [0,1,0]]; 
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n = 200; 
    radlnPts = circlePts( pqr, rad=5, n=n );
    //MarkPts( radlnPts, "ch" );
    //for( i=range(n) ) 
    //    MarkPts( [pqr[1], radlnPts[i]], ["ch=[r,0.02,color,blue];ball=0","label",["text",["",i]],"color","blue"]);
    
    h = 5;
    dP = onlinePt( p10(pqr), len=h) - pqr[1];
    
    
    n1 = 7; //int(rand(3,12));
    n2 = 4; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=1.8, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    //MarkPts( pl1, "ch=[closed,true,r,0.03,color,red];sametransp;ball=0");
    //MarkPts( [for(p=pl2)p+dP], "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");

    pairdata = getLoftData( pl1,pl2, n=n);
    echo( status = //_b(_red(replace( 
                    hash( pairdata, "debug" ) 
                    //, "<", "&lt;")))
        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    _pl2new = hash(pairdata,"pts2");

    pl2new = [ for(p=_pl2new) p+dP ];
        
    //MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;ball=0" );
    //MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;ball=0");
        
//    echo( len(pl1new), pl1new= _colorPts( [for(ps=pl1new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
//    echo( len(pl2new), pl2new= _colorPts( [for(ps=pl2new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    //for( i=range(pl1new))
    //    MarkPts( [pl1new[i],pl2new[i]], "ch;ball=0" );
    
    faces = faces("rod", nside= len( pl1new) );
    polyhedron( points = pts, faces=faces );
    //---------------------------------------------
    //
    // On graph doc
    //
    //---------------------------------------------
    // Title
//    C = centerPt(pl1);
//    // dP: a vertical vector 
//    dP = anglePt( [radlnPts[0],C,radlnPts[1]],a=90)- C;     
//    Write("getLoftData_dev_20150921( )"
//         , at=[ for(p=[ radlnPts[0] 
//                      , C
//                      , anglePt( [radlnPts[0],C,radlnPts[1]],a=90)
//                      ]) p+ dP*1.2 ]
//         , size=0.5, th=0.1, halign="center" );
//    
//
//    //---------------------------------------------
//    txtpqr = [ radlnPts[0] - dP*1.2
//              , C- dP*1.2 //radlnPts[ floor(3*n/4) ]
//              , C
//              ];
//    //MarkPts( txtpqr, "ch=[r,0.5];l=PQR");
//    
//   Write(_s("red: pts1 (n1={_})",n1)
//            , at = [for(p=txtpqr) p-dP*0.1]
//            , size=0.5, th=0.1, color="red",halign="center" );
//    
//    Write( _s("green: pts2 (n2={_})",n2)
//            ,at=[for(p=txtpqr) p-dP*0.3 ]
//           , size=0.5, th=0.1, color="green",halign="center" );
//    
//    Write(_s("blue: for resolution (n={_})",n)
//            , at = [for(p=txtpqr) p-dP*0.5 ]
//            , size=0.5, th=0.1, color="blue",halign="center" );
    //---------------------------------------------

    
}    
//getLoftData_demo3_apply_h(); //1'14"

module getLoftData_demo4_apply_nlevel()
{
    echom("getLoftData_demo4_apply_nlevel");
    
    pqr= pqr();
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    //pqr = [ [1,0,0], ORIGIN, [0,1,0]]; 
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n = 96; 
    radlnPts = circlePts( pqr, rad=5, n=n );
    //MarkPts( radlnPts, "ch" );
    //for( i=range(n) ) 
    //    MarkPts( [pqr[1], radlnPts[i]], ["ch=[r,0.02,color,blue];ball=0","label",["text",["",i]],"color","blue"]);
    
    h = 5;
    nlevel= 20;
    dP = onlinePt( p10(pqr), len=h) - pqr[1];
    
    
    n1 = 96; //int(rand(3,12));
    n2 = 4; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=1.8, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    //MarkPts( pl1, "ch=[closed,true,r,0.03,color,red];sametransp;ball=0");
    //MarkPts( [for(p=pl2)p+dP], "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");

    pairdata = getLoftData( pl1,pl2, n=n, debug=0);
    echo( debug =  hash( pairdata, "debug" ) 
        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");

    pl2new_raised = [ for(p=pl2new) p+dP ];
        
    //MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;ball=0" );
    //MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;ball=0");
        
//    echo( len(pl1new), pl1new= _colorPts( [for(ps=pl1new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
//    echo( len(pl2new), pl2new= _colorPts( [for(ps=pl2new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
    
    faces = hash( pairdata, "faces" );
    //echo( lenface= len(faces), faces = faces );
        
    //pts = concat( pl1new, pl2new_raised); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    //for( i=range(pl1new))
    //    MarkPts( [pl1new[i],pl2new[i]], "ch;ball=0" );
    
      cuts = [ for (lv = range(nlevel+1))
              [ for (i= range(pl1new))
                 onlinePt( [pl1new[i],pl2new_raised[i]]
                         , ratio= lv/nlevel)
              ]
           ];
 

    pts = joinarr( cuts );
    
    faces = faces("chain", nside=len(pl1new), nseg= nlevel);
    polyhedron( points= pts, faces=faces );
              

    //---------------------------------------------
    //
    // On graph doc
    //
    //---------------------------------------------
    // Title
//    C = centerPt(pl1);
//    // dP: a vertical vector 
//    dP = anglePt( [radlnPts[0],C,radlnPts[1]],a=90)- C;     
//    Write("getLoftData_dev_20150921( )"
//         , at=[ for(p=[ radlnPts[0] 
//                      , C
//                      , anglePt( [radlnPts[0],C,radlnPts[1]],a=90)
//                      ]) p+ dP*1.2 ]
//         , size=0.5, th=0.1, halign="center" );
//    
//
//    //---------------------------------------------
//    txtpqr = [ radlnPts[0] - dP*1.2
//              , C- dP*1.2 //radlnPts[ floor(3*n/4) ]
//              , C
//              ];
//    //MarkPts( txtpqr, "ch=[r,0.5];l=PQR");
//    
//   Write(_s("red: pts1 (n1={_})",n1)
//            , at = [for(p=txtpqr) p-dP*0.1]
//            , size=0.5, th=0.1, color="red",halign="center" );
//    
//    Write( _s("green: pts2 (n2={_})",n2)
//            ,at=[for(p=txtpqr) p-dP*0.3 ]
//           , size=0.5, th=0.1, color="green",halign="center" );
//    
//    Write(_s("blue: for resolution (n={_})",n)
//            , at = [for(p=txtpqr) p-dP*0.5 ]
//            , size=0.5, th=0.1, color="blue",halign="center" );
    //---------------------------------------------

    
}    
//getLoftData_demo4_apply_nlevel(); //35" // 22"



//t=$t;
//nstep=30;
//i = round( t*nstep);
module getLoftData_demo_animate()
{
    echo("\n\n");
    echo( str("nstep= ",nstep
        , ", i= ",i
        , ", t= ",t
        , ", i/nstep= ", i/nstep
        , ", [n1,n2]= ", [n1,n2]
        )
        , "\n");
    /*
    
    python ../runscad.py scadx_demo.scad loft_demo.png D="i=%(n)s;nstep=%(n)s;t=%(t)s;getLoftData_demo_animate();" camera=2,2,0,90,0,160,30 n=30 2> log.txt
    
    convert -delay 50 -loop 0 ./loft_demo000??.png loft_demo.gif 

    */
    
//    echom("getLoftData_demo_animate"
//         ,"For animation that will change n1 from 3 to 96
//           and n2 from 96 to 4.
//          The animation is to be executed from the command line
//          from which i (index of step), n(total steps) and t
//          (time fraction) will be sent to this function. So we 
//          will use i,n,t here without defining them here.  
//          "
//         );
    
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    //MarkPts( pqr, "ch;l=PQR");
    
    nRes = 96; 
    radlnPts = circlePts( pqr, rad=5, n=nRes );
    //MarkPts( radlnPts, "ch" );
    //for( i=range(n) ) 
    //    MarkPts( [pqr[1], radlnPts[i]], ["ch=[r,0.02,color,blue];ball=0","label",["text",["",i]],"color","blue"]);
    
    h = 5;
    nlayer= 10;
    dP = onlinePt( p10(pqr), len=h) - pqr[1];
    
    // n1 will change from 0 to nRes then from nRes back to 0
    // during the span of i from 0 to n (n = number of steps
    // of animation). So n1 change for each step is :
    //   dn1 = when t (or, i/n) < 0.5: nRes/n/2 
    //         when t (or, i/n) >= 0.5: -nRes/n/2
    // Check: n1 @ t=0.5 (where step, n, is n/2) should be 96: 
    //        dn1* n/2 = nRes
    
    n1beg = 4;
    n1end = 20;
    dn1 = (n1end-n1beg)/nstep*2;
    
    n2beg = 15;
    n2end = 3;
    dn2 = (n2end-n2beg)/nstep*2;
    
    i = round( t*nstep);
    n1 = round(  i/nstep <0.5 ? 
                    n1beg + i * dn1
                   : n1end -( i-nstep/2)*dn1
              );
              
    n2 = round(  i/nstep <0.5 ? 
                    n2beg + i * dn2
                   : n2end -( i-nstep/2)*dn2
              );
              
    pl1= circlePts( pqr, rad=4, n= n1 );
    pl2= circlePts( pqr, rad=2, n=n2);
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    //echo(pl1=pl1);
    //echo(pl2=pl2);
    //MarkPts( pl1, "ch=[closed,true,r,0.03,color,red];sametransp;ball=0");
    //MarkPts( [for(p=pl2)p+dP], "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");

    pairdata = getLoftData( pl1,pl2, n=nRes);
//    echo( status = //_b(_red(replace( 
//                    hash( pairdata, "debug" ) 
//                    //, "<", "&lt;")))
//        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");

    pl2new_raised = [ for(p=pl2new) p+dP ];
        
    
    //MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;ball=0" );
    //MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;ball=0");
        
//    echo( len(pl1new), pl1new= _colorPts( [for(ps=pl1new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
//    echo( len(pl2new), pl2new= _colorPts( [for(ps=pl2new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
    
    faces = hash( pairdata, "faces" );
    //echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new_raised); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    //for( i=range(pl1new))
    //    MarkPts( [pl1new[i],pl2new[i]], "ch;ball=0" );
    
      cuts = [ for (lv = range(nlayer+1))
              [ for (i= range(pl1new))
                 onlinePt( [pl1new[i],pl2new_raised[i]]
                         , ratio= lv/nlayer)
              ]
           ];
 
    //echo(cuts = cuts );
    //pts = joinarr( cuts );
    
//    faces = faces("chain", nside=len(pl1new), nseg= 1);//nlevel);
//    polyhedron( points= pts, faces=faces );
             
              
//   cuts = [ for (lv = range(nlayer+1))
//          [ for (i= range(pl1new))
//             onlinePt( [pl1new[i],pl2new_raised[i]]
//                     , ratio= lv/nlayer)
//          ]
//       ];
          
    faces = faces("chain", nside= len( pl1new), nseg= nlayer );
    color("olive")
    polyhedron( points = joinarr(cuts), faces=faces );
    
    //ColorAxes(["len",6,"r",0.5]);
    //---------------------------------------------
    //
    // On graph doc
    //
    //---------------------------------------------
    // Title
//    C = centerPt(pl1);
//    // dP: a vertical vector 
//    dP = anglePt( [radlnPts[0],C,radlnPts[1]],a=90)- C;     
//    Write("getLoftData_dev_20150921( )"
//         , at=[ for(p=[ radlnPts[0] 
//                      , C
//                      , anglePt( [radlnPts[0],C,radlnPts[1]],a=90)
//                      ]) p+ dP*1.2 ]
//         , size=0.5, th=0.1, halign="center" );
//    
//
//    //---------------------------------------------
//    txtpqr = [ radlnPts[0] - dP*1.2
//              , C- dP*1.2 //radlnPts[ floor(3*n/4) ]
//              , C
//              ];
//    //MarkPts( txtpqr, "ch=[r,0.5];l=PQR");
//    
//   Write(_s("red: pts1 (n1={_})",n1)
//            , at = [for(p=txtpqr) p-dP*0.1]
//            , size=0.5, th=0.1, color="red",halign="center" );
//    
//    Write( _s("green: pts2 (n2={_})",n2)
//            ,at=[for(p=txtpqr) p-dP*0.3 ]
//           , size=0.5, th=0.1, color="green",halign="center" );
//    
//    Write(_s("blue: for resolution (n={_})",n)
//            , at = [for(p=txtpqr) p-dP*0.5 ]
//            , size=0.5, th=0.1, color="blue",halign="center" );
    //---------------------------------------------

    //echo( len_pl1new= len(pl1new), len_pl2new=len(pl2new) );
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    //echo( pl1=pl1);
    //echo(pl2=pl2);
    //echom("getLoftData_demo_animate");
    
}    
//getLoftData_demo_animate(); // 22"

module getLoftData_demo4_apply_nlevel2()
{
    echom("getLoftData_demo4_apply_nlevel2");
    
    pqr= pqr();
    pqr =   [[2.77027, 2.01597, -0.78783], [0.195623, -0.286237, -0.183822], [-1.16913, 1.2855, -2.17057]];
    //pqr = [ [1,0,0], ORIGIN, [0,1,0]]; 
    echo( pqr = pqr );
    //MarkPts( pqr, "ch;l=PQR");
    
    n = 6; 
    radlnPts = circlePts( pqr, rad=5, n=n );
    //MarkPts( radlnPts, "ch" );
//    for( i=range(n) ) 
//        MarkPts( [pqr[1], radlnPts[i]], ["ch=[r,0.02,color,blue];ball=0","label",["text",["",i]],"color","blue"]);
    
    h = 5;
    nlevel= 5;
    dP = onlinePt( p10(pqr), len=h) - pqr[1];
    
    
    n1 = 5; //int(rand(3,12));
    n2 = 4; //int( n1<7? rand(7,12):rand(3,8));
    pl1= circlePts( pqr, rad=4, n= n1 );
    C = centerPt(pl1);
    
    //pl2= circlePts( pqr, rad=1.8, n=n2);
    _pl2 = hash(chainBoneData( seed=p012(pl1), a=[120,150],a2=0, n=4),"pts");
    echo( pl2 = _pl2 );
    C2 = centerPt(_pl2);
    pl2 = [for(p=_pl2)p+C-C2];
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch=[closed,true,r,0.03,color,red];sametransp;ball=0");
    //MarkPts( [for(p=pl2)p+dP], "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");
    MarkPts( pl2, "ch=[closed,true,r,0.03,color,green];sametransp;ball=0");

    pairdata = getLoftData( pl1,pl2, n=n);
    echo( status = //_b(_red(replace( 
                    hash( pairdata, "debug" ) 
                    //, "<", "&lt;")))
        );   
    
    //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
   
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");

    pl2new_raised = pl2new; //[ for(p=pl2new) p+dP ];
    echo( pl2new_raised = pl2new_raised );
    //MarkPts( pl2new_raised, "ch=[closed,true,r,0.03,color,green];ball=0;l");
        
    //MarkPts( pl1new, "r=0.3;trp=0.5;sametransp;ball=0" );
    //MarkPts( pl2new, "r=0.15;trp=0.5;sametransp;ball=0");
        
//    echo( len(pl1new), pl1new= _colorPts( [for(ps=pl1new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
//    echo( len(pl2new), pl2new= _colorPts( [for(ps=pl2new)
//                                            [ for(p=ps) is0(p)?0:p
//                                            ]
//                                          ]    
//                                        ) 
//        );
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new_raised); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
//    for(i = range(pl1new) )
//        color( get(COLORS, i, cycle=true), 0.6 )
//        Plane( get(pts, faces[i]));

    for( i=range(pl1new))
        MarkPts( [pl1new[i],pl2new_raised[i]], "ch;ball=0" );
    
//      cuts = [ for (lv = range(nlevel+1))
//              [ for (i= range(pl1new))
//                 onlinePt( [pl1new[i],pl2new_raised[i]]
//                         , ratio= lv/nlevel)
//              ]
//           ];
// 
//
//    pts = joinarr( cuts );
//    
//    faces = faces("chain", nside=len(pl1new), nseg= nlevel);
//    polyhedron( points= pts, faces=faces );
//              

    //polyhedron( points= pts, faces = faces("rod", nside= len(pl1new) ) );
    
    //---------------------------------------------
    //
    // On graph doc
    //
    //---------------------------------------------
    // Title
//    C = centerPt(pl1);
//    // dP: a vertical vector 
//    dP = anglePt( [radlnPts[0],C,radlnPts[1]],a=90)- C;     
//    Write("getLoftData_dev_20150921( )"
//         , at=[ for(p=[ radlnPts[0] 
//                      , C
//                      , anglePt( [radlnPts[0],C,radlnPts[1]],a=90)
//                      ]) p+ dP*1.2 ]
//         , size=0.5, th=0.1, halign="center" );
//    
//
//    //---------------------------------------------
//    txtpqr = [ radlnPts[0] - dP*1.2
//              , C- dP*1.2 //radlnPts[ floor(3*n/4) ]
//              , C
//              ];
//    //MarkPts( txtpqr, "ch=[r,0.5];l=PQR");
//    
//   Write(_s("red: pts1 (n1={_})",n1)
//            , at = [for(p=txtpqr) p-dP*0.1]
//            , size=0.5, th=0.1, color="red",halign="center" );
//    
//    Write( _s("green: pts2 (n2={_})",n2)
//            ,at=[for(p=txtpqr) p-dP*0.3 ]
//           , size=0.5, th=0.1, color="green",halign="center" );
//    
//    Write(_s("blue: for resolution (n={_})",n)
//            , at = [for(p=txtpqr) p-dP*0.5 ]
//            , size=0.5, th=0.1, color="blue",halign="center" );
    //---------------------------------------------

    
}    
//getLoftData_demo4_apply_nlevel2(); 


//p= [-4.1315, 1.38987, -2.79414];
//C1=[-0.605661, 1.34442, 2.20736];
//C2=[-3.21933, 1.23491, -3.17352];
//X= C1 + p-C2;
//pq= [ [-1.12721, 2.18993, 2.09288]
//      , [-1.3542, 0.955396, 2.74434]
//      ];
//
//MarkPts( [C1,p], "ch;l=[text, Cp]");
//MarkPts( [C2,p], "ch;l=[text, Dp]");
//MarkPts( [C1,X], "ch;l=[text,CX]");
//
//MarkPts( pq, "ch;l=[text,PQ]");
//
//J = projPt( C1, pq, [C2,p] );
//echo( J = J );


module getHShapePts_demo_default()
{
  echom("getHShapePts_demo_default");
  pts = getHShapePts();
  //echo( pts );
  MarkPts( pts, "l;ch");
  polyhedron( pts, faces = rodfaces(12));
}
//getHShapePts_demo_default();

module getHShapePts_demo_width(pos=[0,0,0])
{
  echom("getHShapePts_demo_width");
  faces = rodfaces(12);  
  df = [for(p=getHShapePts())p+pos];
  polyhedron( df, faces);

  data=[ "red", ["Set all widths: getHShapePts(w=1)"
                ,  getHShapePts(w=1)
                , [7,0,0]+pos //<=== translation
                ]
       , "green",["Set all widths: getHShapePts(w=5)"
                ,  getHShapePts(w=5)
                , [12,0,0]+pos //<=== translation
                ]
       , "blue", ["Set mid bar width: getHShapePts(midw=0.2)"
                ,  getHShapePts(midw=0.2)
                , [25,0,0]+pos //<=== translation
                ]
       ]; 
  
  for( key= keys(data) )
  {  
     items= hash(data, key);
     echo( _color( items[0], key ));
     pts = [ for(p= items[1] ) p+items[2] ];
     color(key)
     polyhedron( pts, faces = faces );
  }
   
}
//getHShapePts_demo_width();

module getHShapePts_demo_width2( pos=[0,0,0] )
{
  echom("getHShapePts_demo_width2");
  faces = rodfaces(12);  
  df = [for(p=getHShapePts())p+pos];
  polyhedron( df, faces);

  data=[ "red", ["Set leg width: getHShapePts(opt=[\"leg1\",[\"w\",1],\"leg3\",[\"w\",1]])"
                ,  getHShapePts( opt=["leg1",["w",1],"leg3",["w",1]])
                , [7,0,0]+pos //<=== translation
                ]
       , "green",["Set leg width: getHShapePts(opt=[\"leg1\",[\"w\",1]])"
                ,  getHShapePts( opt=["leg1",["w",1]])
                , [13,0,0]+pos //<=== translation
                ]
       , "blue", ["Set leg width: getHShapePts(opt=[\"leg3\",[\"w\",1]])"
                ,  getHShapePts( opt=["leg3",["w",1]])
                , [20,0,0]+pos //<=== translation
                ]
       , "purple", ["Set leg width: getHShapePts(opt=[\"leg1\",[\"w\",1],\"leg4\",[\"w\",1]])"
                ,  getHShapePts( opt=["leg1",["w",1],"leg4",["w",1]])
                ,[27,0,0]+pos //<=== translation
                ]
       ]; 
  
  for( key= keys(data) )
  {  
     items= hash(data, key);
     echo( _color( items[0], key ));
     pts = [ for(p= items[1] ) p+items[2] ];
     color(key)
     polyhedron( pts, faces = faces );
  }
  
}
//getHShapePts_demo_width2();

module getHShapePts_demo_len( pos=[0,0,0] )
{
  echom("getHShapePts_demo_len");
  faces = rodfaces(12);
  df = [for(p=getHShapePts())p+pos];
  polyhedron( df, faces);

  data=[ 
  "red", ["Set leg len: getHShapePts(len=2)"
          ,  getHShapePts( len=2 )
          , [7,0,0]+pos //<=== translation
          ]
  ,"green",["Set leg len: getHShapePts(opt=[\"leg1\",[\"len\",1]])"
          ,  getHShapePts( opt=["leg1",["len",1]])
          , [14,0,0]+pos //<=== translation
          ]
  ,"blue", ["Set leg len: getHShapePts(opt=[\"leg1\",[\"len\",0],\"leg4\",[\"len\",0]])"
          ,  getHShapePts( opt=["leg1",["len",0],"leg4",["len",0]])
          , [21,0,0]+pos //<=== translation
          ]
  ,"purple", ["Set leg width: getHShapePts(opt=[\"leg1\",[\"len\",0],\"leg2\",[\"len\",0]])"
          ,  getHShapePts( opt=["leg1",["len",0],"leg2",["len",0]])
          ,[28,0,0]+pos //<=== translation
          ]
       ]; 
  
  for( key= keys(data) )
  {  
     items= hash(data, key);
     echo( _color( items[0], key ));
     pts = [ for(p= items[1] ) p+items[2] ];
     color(key)
     polyhedron( pts, faces = faces );
  }

}
//getHShapePts_demo_len();

module getHShapePts_demo_len2( pos=[0,0,0] )
{
  echom("getHShapePts_demo_len2");
  faces = rodfaces(12);
  df = [for(p=getHShapePts())p+pos];
  polyhedron( df, faces);

  data=[ 
  "red", ["Set negative leg len: getHShapePts(opt=[\"leg1\",[\"len\",-1]])"
          ,  getHShapePts( opt=["leg1",["len",-1]])
          , [7,0,0]+pos //<=== translation
          ]
  ,"green",["getHShapePts(opt=[\"leg1\",[\"len\",-1],\"leg2\",[\"len\",-1]])"
          ,  getHShapePts( opt=["leg1",["len",-1],"leg2",["len",-1]])
          , [14,0,0]+pos //<=== translation
          ]
  ,"blue", ["getHShapePts(midw=6, len=-2)"
          ,  getHShapePts( midw=6, len=-2)
          , [21,0,0]+pos //<=== translation
          ]
  ];
  
  for( key= keys(data) )
  {  
     items= hash(data, key);
     echo( _color( items[0], key ));
     pts = [ for(p= items[1] ) p+items[2] ];
     color(key)
     polyhedron( pts, faces = faces );
  }

}
//getHShapePts_demo_len2();

module getHShapePts_demo_gap( pos=[0,0,0] )
{
  echom("getHShapePts_demo_gap");
  faces = rodfaces(12);
  df = [for(p=getHShapePts())p+pos];
  polyhedron( df, faces);
  
  data=[ 
  "red", ["Set gap: getHShapePts( gap=0.4 )"
          ,  getHShapePts( gap=0.4)
          , [7,0,0]+pos //<=== translation
          ]
  ,"green",["getHShapePts( gap=0.3, gap2=1.4,  midw=4
                         , opt=[\"leg1\", [\"w\",1]
                               ,\"leg2\", [\"w\",6.1]
                               ,\"leg3\", [\"w\",5,\"len\",2]
                               ,\"leg4\", [\"w\",1, \"len\",2]]
                         )"
          ,  getHShapePts( gap=0.3, gap2=1.4,  midw=4
                         , opt=["leg1", ["w",1]
                               ,"leg2", ["w",6.1]
                               ,"leg3", ["w",5,"len",2]
                               ,"leg4", ["w",1, "len",2]]
                         )
          , [12.5,0,0]+pos //<=== translation
          ]
  ];
  
  for( key= keys(data) )
  {  
     items= hash(data, key);
     echo( _color( items[0], key ));
     pts = [ for(p= items[1] ) p+items[2] ];
     color(key)
     polyhedron( pts, faces = faces );
  }
}
//getHShapePts_demo_gap();

module getHShapePts_demo_T( pos=[0,0,0] )
{
  echom("getHShapePts_demo_T");
  faces = rodfaces(12);
  df = [for(p=getHShapePts())p+pos];
  polyhedron( df, faces);
  
  data=[ 
  "red", ["getHShapePts( opt=[\"leg2\",[\"len\",0],\"leg4\",[\"len\",0]] )"
          ,  getHShapePts( opt=["leg2",["len",0],"leg4",["len",0]])
          , [7,0,0]+pos //<=== translation
          ]
  ,"green",["getHShapePts( midw=6
                         , opt=[\"leg1\", [\"len\",-4]
                               ,\"leg2\", [\"len\",-4]
                               ,\"leg3\", [\"len\",0]
                               ,\"leg4\", [\"len\",0]]
                         )"
          ,  getHShapePts( midw=6
                         , opt=["leg1", ["len",-4]
                               ,"leg2", ["len",-4]
                               ,"leg3", ["len",0]
                               ,"leg4", ["len",0]]
                         )
          , [12.5,0,0]+pos //<=== translation
          ]
  ];
  
  for( key= keys(data) )
  {  
     items= hash(data, key);
     echo( _color( items[0], key ));
     pts = [ for(p= items[1] ) p+items[2] ];
     MarkPts(pts);  
     color(key)
     polyhedron( pts, faces = faces );
  }
}
//getHShapePts_demo_T();

module getHShapePts_demo_L( pos=[0,0,0] )
{
  echom("getHShapePts_demo_L");
  faces = rodfaces(12);
  df = [for(p=getHShapePts())p+pos];
  polyhedron( df, faces);
  
  data=[ 
  "red", ["getHShapePts( len=0, opt=[\"leg2\",[\"len\",6]] )"
          ,  getHShapePts( len=0, opt=["leg2",["len",6]])
          , [7,0,0]+pos //<=== translation
          ]
  ,"green",["getHShapePts( len=0
                         , opt=[\"leg4\", [\"len\",3]]
                         )"
          ,  getHShapePts( len=0
                         , opt=["leg4", ["len",3]]
                         )
          , [14,0,0]+pos //<=== translation
          ]
  ];
  
  for( key= keys(data) )
  {  
     items= hash(data, key);
     echo( _color( items[0], key ));
     pts = [ for(p= items[1] ) p+items[2] ];
     MarkPts(pts);  
     color(key)
     polyhedron( pts, faces = faces );
  }
}
//getHShapePts_demo_L();

module getHShapePts_demo_all()
{
  //getHShapePts_demo_default();
  getHShapePts_demo_width();
  getHShapePts_demo_width2( pos=[33,0,0] );
  getHShapePts_demo_len( pos=[0,0,-12] );
  getHShapePts_demo_len2( pos=[36,0,-12] );
  getHShapePts_demo_gap(pos=[0,0,-24]);
  getHShapePts_demo_T(pos=[23,0,-24]);
  getHShapePts_demo_L(pos=[44,0,-24]);
  
}  
//getHShapePts_demo_all();

//===============================================

module getLShapePts_demo_default()
{
  pts = getLShapePts();
  echo( pts );
  MarkPts( pts, "l;ch");
  polyhedron( pts, faces = rodfaces(6));
}
//getLShapePts_demo_default();

module getLShapePts_demo_width()
{
  faces = rodfaces(6);
  df = getLShapePts();
  polyhedron( df, faces);

  echo(_red("Set width of all parts: getLShapePts(w=1)")); 
  pts = [ for(p= getLShapePts(w=1) ) p+[0, -5,0] ];
//  //MarkPts( pts, "l;ch");
  color("red")
  polyhedron( pts, faces = faces);

  
  echo(_green("Set width of leg1: getUShapePts(opt=[\"leg1\", [\"w\",5]])")); 
  _pts2=getLShapePts(opt=["leg1", ["w",5]]);
  //echo(_pts2=_pts2);
  pts2 = [ for(p= _pts2 ) p+[8, 0,0] ];
  //echo(pts2=pts2);
  //MarkPts( pts2, "l;ch");
  color("green")
  polyhedron( pts2, faces = faces);
  
  echo(_blue("Set width of leg2: getUShapePts(opt=[\"leg2\", [\"w\",5]])")); 
  _pts3=getLShapePts(opt=["leg2", ["w",5]]);
  //echo(_pts2=_pts2);
  pts3 = [ for(p= _pts3 ) p+[8, -5,0] ];
  //echo(pts2=pts2);
  //MarkPts( pts2, "l;ch");
  color("blue")
  polyhedron( pts3, faces = faces);
}
//getLShapePts_demo_width();

module getLShapePts_demo_len()
{
  faces = rodfaces(6);
  df = getLShapePts();
  polyhedron( df, faces);

  echo(_red("Set len of both legs: getLShapePts(len=1.5)")); 
  pts = [ for(p= getLShapePts(len=1.5) ) p+[0, -5,0] ];
//  //MarkPts( pts, "l;ch");
  color("red")
  polyhedron( pts, faces = faces);

  echo(_green("Set len of leg1: getUShapePts(len=1), ")); 
  pts2 = [ for(p= getLShapePts(opt=["leg1", ["len",1]]) ) p+[0, -10,0] ];
  MarkPts( pts2, "l;ch");
  color("green", 0.5)
  polyhedron( pts2, faces = faces);
  
  echo(_blue("Set len of leg1: getLShapePts(opt=[\"leg2\", [\"len\",1]])")); 
  _pts3=getLShapePts(opt=["leg2", ["len",1]]);
  //echo(_pts2=_pts2);
  pts3 = [ for(p= _pts3 ) p+[8, 0,0] ];
  //echo(pts2=pts2);
  //MarkPts( pts2, "l;ch");
  color("blue")
  polyhedron( pts3, faces = faces);
}
//getLShapePts_demo_len();

module growPts_demo_pt()
{
   pt = randPt();
   //MarkPt(pt); 
  //echo(pt=pt);
  //echo( growPts( pt ) );
  
  pts1= growPts( pt , ["x",2, "y",3, "t",[-1,-1,1],"z",2] );
  //echo(pts1=pts1);
  MarkPts( pts1, "l;ch");
  
  pts2= growPts( ORIGIN, ["x", 6, "z", 4, "x", -2, "z", -3, "x", -2, "z", 2, "x", -2] );
  echo(pts2=pts2);
  MarkPts( pts2, "ch");
  
}  
//growPts_demo_pt();

module growPts_demo_NBR()
{
   pts = randPts(4);
   //MarkPt(pt); 
  echo(pts=pts);
  //echo( growPts( pt ) );
  
  //pts1= growPts( pts , ["N",2]);//, "y",3]); //, "t",[-1,-1,1],"z",2] );
  //echo(pts1=pts1);
  //MarkPts( pts1, "l;ch");
  
  pts2= growPts( pts , ["N",1]);//, "y",3]); //, "t",[-1,-1,1],"z",2] );
  //echo(pts1=pts1);
  MarkPts( pts2, "l;ch");
  
}  
//growPts_demo_NBR();

module growPts_demo_double_grow()
{
  pt = randPt();
  
  pts = growPts( 
          growPts( pt, ["x", 3, "z", 1, "t", [-2,0,0.5]
                       ,"t", [2,0,0.5], "z",1,"x",-3] 
                 )
        , ["N", 2, "N", 1.5, "N",1]
       );
  MarkPts( pts, "ch;"); 
  Dims( get(pts,[0,7,14,21]), opt=["Rf",pts[27]]);
  
}
//growPts_demo_double_grow();

module growPts_demo_arc1()
{
  
  echo(_color("growPts( pt, [\"arc\",[]] );", "darkkhaki"));
  pt = randPt();
  //echo(pt=pt);
  //MarkPt(pt,"r=0.2");
  pts= growPts( pt, ["arc",[]] );
  //echo(pts=pts);
  MarkPts( pts, "ch"); 
  
  echo(_red("growPts( pt2, [\"x\",1, \"t\", [1,1,1], \"arc\",[]] );"));
  pt2 = randPt();
  //echo(pt2=pt2);
  //MarkPt(transPts(pt2,["x",2]),"l;r=0.2");
  pts2= growPts( pt2, ["x",1, "t", [1,1,1], "arc",[]] );
  //echo(len=len(pts2), pts2=pts2);
  MarkPts( pts2, ["r", 0.07, "chain","color=red;r=0.03"]); 
  

  echo(_green("growPts( pt2, [\"x\",1, \"t\", [1,1,1], \"arc\",[\"n\",9, \"a\",180, \"rad\",0.5]] );"));
  pt3 = pt2+N(pts2)-pts2[1]; //randPt();
  //echo(pt3=pt3);
  //MarkPt(pt3,"r=0.2");
  pts3= growPts( pt3, ["x",1, "t", [1,1,1]
                      , "arc",["n",9, "a",180, "rad",0.5]] );
  //echo(len=len(pts3), pts3=pts3);
  MarkPts( pts3, ["r", 0.07, "chain","color=green;r=0.03"]); 


  echo(_blue("growPts( pt2, [\"x\",1, \"t\", [1,1,1], \"arc\",[], \"arc\",[\"n\",9, \"a\",180, \"rad\",0.5]] );"));
  pt4 = pt3+N(pts3)-pts3[1]; //randPt();
  //echo(pt4=pt4);
  //MarkPt(pt3,"r=0.2");
  pts4= growPts( pt4, ["x",1, "t", [1,1,1]
                      , "arc", []
                      , "arc",["n",9, "a",180, "rad",0.5]
                      ] );
  //echo(len=len(pts4), pts4=pts4);
  MarkPts( pts4, ["r", 0.07, "chain","color=blue;r=0.03"]); 

}
//growPts_demo_arc1();


module growPts_demo_arc_a2()
{
 
  echo(_red("growPts( pt2, [\"x\",1, \"t\", [1,1,1], \"arc\",[]] );"));
  pt2 = randPt();
  //echo(pt2=pt2);
  //MarkPt(transPts(pt2,["x",2]),"l;r=0.2");
  pts2= growPts( pt2, ["x",1, "t", [1,1,1], "arc",[]] );
  //echo(len=len(pts2), pts2=pts2);
  MarkPts( pts2, ["r", 0.07, "chain","color=red;r=0.03"]); 
  

  echo(_green("growPts( pt2, [\"x\",1, \"t\", [1,1,1], \"arc\",[\"n\",12, \"a\",180]] );"));
  pt3 = pt2+N(pts2)-pts2[1]; //randPt();
  //echo(pt3=pt3);
  //MarkPt(pt3,"r=0.2");
  pts3= growPts( pt3, ["x",1, "t", [1,1,1]
                      , "arc",[ "n",12,"a",180]] );
  //echo(len=len(pts3), pts3=pts3);
  MarkPts( pts3, ["r", 0.07, "chain","color=green;r=0.03"]); 


  echo(_blue("growPts( pt2, [\"x\",1, \"t\", [1,1,1], \"arc\",[\"a2\",90]] );"));
  pt4 = pt3+N(pts3)-pts3[1]; //randPt();
  //echo(pt4=pt4);
  //MarkPt(pt3,"r=0.2");
  pts4= growPts( pt4, ["x",1, "t", [1,1,1]
                      , "arc",["a2",90]
                      ] );
  //echo(len=len(pts4), pts4=pts4);
  MarkPts( pts4, ["r", 0.07, "chain","color=blue;r=0.03"]); 

}
//growPts_demo_arc_a2();


module growPts_demo_arc1_animate()
{
  
  rad0=1;
  a0=170;
  //amax = 170;
  //amin = 10;
  rad1= rad0+4*sin($t*180);
  a1 = rad0*a0/rad1;
  echo(t= $t, rad1=rad1, a1=a1 );
  rad2= 0.2+sin($t*180);
  //echo(_blue("growPts( pt2, [\"x\",1, \"t\", [1,1,1], \"arc\",[], \"arc\",[\"n\",9, \"a\",180, \"rad\",0.5]] );"));
  pt4 = [0.589387, 1.10147, -1.86969];//randPt(); //pt3+N(pts3)-pts3[1]; //randPt();
  echo(pt4=pt4);
  //MarkPt(pt3,"r=0.2");
  pts4= growPts( pt4, ["x",1, "t", [1,1,1]
                      , "arc", ["a",a1,"rad", rad1]
                      //, "arc",[ "a",180, "rad",rad2]//0.5]
                      ] );
  //echo(len=len(pts4), pts4=pts4);
  MarkPts( pts4, ["r", 0.07, "chain","color=blue;r=0.03"]); 

}
//growPts_demo_arc1_animate();

//echo(sin($t*180));

module growPts_demo_arc1_2()
{
  pt = randPt();
  echo(pt=pt);
  //MarkPt(pt,"r=0.2");
  
  
//  pts= growPts( pt, ["arc",[]] );
//  echo(pts=pts);
//  MarkPts( pts, "ch"); 
  
  pts2_ = randPts(3);
  echo(pts2_ = pts2_);
  MarkPts( pts2_, ["r", 0.2,"transp",0.3] );
  pts2= growPts( pts2_, ["arc", ["n",4]] );
  echo( len_pts2 = len(pts2), pts2 = pts2);
  MarkPts( pts2, "l;ch" );
  
}
//growPts_demo_arc1_2();


module getUShapePts_demo_default()
{
  pts = getUShapePts();
  //echo( pts );
  MarkPts( pts, "l;ch");
  polyhedron( pts, faces = rodfaces(8));
}
//getUShapePts_demo_default();

module getUShapePts_demo_width()
{
  faces = rodfaces(8);
  df = getUShapePts();
  polyhedron( df, faces);

  echo(_red("Set width of all parts: getUShapePts(w=1)")); 
  pts = [ for(p= getUShapePts(w=1) ) p+[0, -5,0] ];
//  //MarkPts( pts, "l;ch");
  color("red")
  polyhedron( pts, faces = faces );

  
  echo(_green("Set width of leg1: getUShapePts(opt=[\"leg1\", [\"w\",5]])")); 
  _pts2=getUShapePts(opt=["leg1", ["w",5]]);
  //echo(_pts2=_pts2);
  pts2 = [ for(p= _pts2 ) p+[8, 0,0] ];
  //echo(pts2=pts2);
  //MarkPts( pts2, "l;ch");
  color("green")
  polyhedron( pts2, faces = faces);

  echo(_blue("getUShapePts( w=0.5, opt=[\"leg2\", [\"w\",5]])")); 
  _pts3=getUShapePts( w=0.5, opt=["leg2", ["w",5]]);
  //echo(_pts2=_pts2);
  pts3 = [ for(p= _pts3 ) p+[8, -5,0] ];
  //echo(pts2=pts2);
  //MarkPts( pts2, "l;ch");
  color("blue")
  polyhedron( pts3, faces = faces);

}
//getUShapePts_demo_width();

module getUShapePts_demo_len()
{
  faces = rodfaces(8);
  df = getUShapePts();
  polyhedron( df, faces);

  echo(_red("Set len of both legs: getUShapePts(len=3)")); 
  pts = [ for(p= getUShapePts(len=3) ) p+[0, -5,0] ];
//  //MarkPts( pts, "l;ch");
  color("red")
  polyhedron( pts, faces = faces);

  echo(_green("Set NEGATIVE len of all legs: getUShapePts(len=-0.5), ")); 
  echo(_green("  converts the H-shape to +-shape")); 
  pts2 = [ for(p= getUShapePts(len=-0.5) ) p+[0, -10,0] ];
  MarkPts( pts2, "l;ch");
  color("green", 0.5)
  polyhedron( pts2, faces = faces);
  
  echo(_blue("Set len of leg1: getUShapePts(opt=[\"leg1\", [\"len\",8]])")); 
  _pts3=getUShapePts(opt=["leg1", ["len",8]]);
  //echo(_pts2=_pts2);
  pts3 = [ for(p= _pts3 ) p+[8, 0,0] ];
  //echo(pts2=pts2);
  //MarkPts( pts2, "l;ch");
  color("blue")
  polyhedron( pts3, faces = faces);
  
  echo(_color("Set small len of leg2: getUShapePts(opt=[\"leg2\", [\"len\",0.5]])","purple")); 
  _pts4=getUShapePts(opt=["leg2", ["len",0.5]]);
  //echo(_pts2=_pts2);
  pts4 = [ for(p= _pts4 ) p+[8, -5,0] ];
  //echo(pts2=pts2);
  //MarkPts( pts2, "l;ch");
  color("purple")
  polyhedron( pts4, faces = faces);
  
  echo(_color("Set leg2 len = 0: getUShapePts(opt=[\"leg2\", [\"len\",0]])","darkkhaki")); 
  echo(_color("  Now we got a L-shape","darkkhaki")); 
  _pts5=getUShapePts(opt=["leg2", ["len",0]]);
  //echo(_pts2=_pts2);
  pts5 = [ for(p= _pts5 ) p+[8, -10,0] ];
  //echo(pts2=pts2);
  //MarkPts( pts5, "l;ch");
  color("darkkhaki")
  polyhedron( pts5, faces = faces);

  echo(_color("Set leg2 len = 0: getUShapePts(opt=[\"leg2\", [\"len\",0]])","black")); 
  echo(_color("  Now we got a L-shape","darkkhaki")); 
  _pts5=getUShapePts(gap=6, leg=0.5, opt=["leg1", ["len",0]]);
  //echo(_pts2=_pts2);
  pts5 = [ for(p= _pts5 ) p+[16, 0,0] ];
  //echo(pts2=pts2);
  //MarkPts( pts5, "l;ch");
  color("black")
  polyhedron( pts5, faces = faces);
  
}
//getUShapePts_demo_len();

module getUShapePts_demo_gap()
{
  faces = rodfaces(8);
  df = getUShapePts();
  polyhedron( df, faces);

  echo(_red("Set gap: getUShapePts( gap=0.2 )")); 
  pts = [ for(p= getUShapePts(gap=0.2) ) p+[0, -5,0] ];
  //MarkPts( pts, "l;ch");
  color("red")
  polyhedron( pts, faces = rodfaces(8));  
}
//getUShapePts_demo_gap();


module getTurnDataAtQ_demo(){
    echom("getTurnDataAtQ_demo");
    
    pqr=pqr(); 
    pqr = [[1.51508, 2.76534, -2.29657], [-1.52943, -1.2732, 0.638371], [1.08919, 2.99655, 1.93264]];
    
    MarkPts(pqr, "ch;l=PQR");
    echo(pqr=pqr);

    Rf= randPt();
    Plane( [P,Q,Rf]); MarkPt(Rf,"l=Rf");
    echo(Rf = Rf );
    
    P=P(pqr); Q=Q(pqr); R=R(pqr);
    //---------------------------------------
    
//    N = N(pqr);
//    td = getTurnDataAtQ( pqr );
//    echo( a_pqr=angle(pqr), td = td );
    
    //---------------------------------------
    
    
    td2 = getTurnDataAtQ( pqr, Rf=Rf);
    J = td2[3];
    MarkPt(J,"l=J;cl=green");
    echo( a_pqr=angle(pqr), td2 = td2 );
    Mark90( [R,J,P],"ch");
    Mark90( [R,J,Q],"ch");
    Mark90( [R,J,Rf], "ch");
    
    echo(aPQJ = angle([P,Q,J], Rf= R) ); // R is critical
    echo(aJQR = angle([J,Q,R], Rf= P) ); // P is critical
    
    //--------------------------------------
    
    pqr3 = trslN(pqr(),4);
    echo(pqr3=pqr3);
    P3=P(pqr3); Q3=Q(pqr3); R3=R(pqr3);
    MarkPts( pqr3, "ch=[color,red]");
    
    newQ = pqr3[1];
    newP = onlinePt( p10(pqr3), len=dist(P,Q) );
    newR = anglePt( [newP,newQ,Rf]
                  , a=td2[0]
                  , a2=td2[1]
                  , len=td2[2]
                  );
                  
    newpqr = [newP,newQ,newR];
    MarkPts( newpqr, "ball=false;ch=[r,0.05,transp,0.2]" );
    
    
    
}
//getTurnDataAtQ_demo();


//. l,m,n,o

module lineCrossPt_demo_1()
{
	echom("lineCrossPt_demo");
	pqm = randPts(3);
	P = pqm[0];
	Q = pqm[1];
	R = onlinePt( [P,Q], ratio= rands(0.2,1,1)[0] ); 
	M = pqm[2];
	N = onlinePt( [M,R], ratio = rands(1.5,2,1)[0]);
	
	//echo("[P,Q,M,N] = ", [P,Q,M,N]);

	MarkPts([P,Q,M,N,R]);
	Line0( [P,Q], ["r",0.01, "color","red"] );
	Line0( [M,N], ["r",0.01, "color","red"] );

	A = lineCrossPt(P,Q,M,N);
	B = crossPt( [P,Q], [M,N] );
	//echo("R: ", R );
	//echo("A: ", A );
	//echo("B: ", B );
	MarkPts( [A], ["r",0.2, "transp",0.4] );
	MarkPts( [B], ["r",0.3, "colors",["green"], "transp",0.2] );
	LabelPts( [P,Q,M,N,R,A], "PQMNRA" );
}


module lineCrossPt_demo_2_nocontact()
{
	echom("lineCrossPt_demo");
	pqm = randPts(3);
	P = pqm[0];
	Q = pqm[1];
	R = onlinePt( [P,Q], ratio= rands(0.2,1,1)[0] ); 
	M = pqm[2];
	N = onlinePt( [M,R], ratio = rands(0,0.8,1)[0]);
	
	//echo("[P,Q,M,N] = ", [P,Q,M,N]);

	MarkPts([P,Q,M,N,R]);
	Line0( [P,Q], ["r",0.02, "color","green"] );
	Line0( [M,N], ["r",0.02, "color","green"] );

	A = lineCrossPt(P,Q,M,N);
	B = crossPt( [P,Q], [M,N] );
	//echo("R: ", R );
	//echo("A: ", A );
	//echo("B: ", B );
	MarkPts( [A], ["r",0.2, "transp",0.4] );
	MarkPts( [B], ["r",0.3, "colors",["green"], "transp",0.2] );
	LabelPts( [P,Q,M,N,R,A], "PQMNRA" );
}
module lineCrossPt_demo()
{
	lineCrossPt_demo_1();
	lineCrossPt_demo_2_nocontact();
}
//lineCrossPt_demo();

module lpCrossPt_demo()
{
	pq = randPts(2);
	tuv = randPts(3);
	Rod( pq, ["label",true] );
	Plane( squarePts(tuv), ["mode",4] );

	X = lpCrossPt( pq, tuv );
	//MarkPts( [X], ["labels","X"] );

}


//lpCrossPt_demo();



module linePts_demo_1_len()
{_mdoc("linePts_demo_1_len",
 " yellow: linePts(pq, len=0.5)
;; red   : linePts(pq, len=[1,0])
;; blue  : linePts(pq, len=[1.5,1.5])
");

	pq=randPts(2);
	MarkPts( pq, ["labels","PQ", "r",0.2] );
	Line0(pq);
    
	xpq = linePts(pq, len=0.5);
	Line0(xpq, ["r",0.25,"transp",0.5,"color","red"] );

	x2pq = linePts(pq, len=[1,0]);
	Line0(x2pq, ["r",0.4,"color","green", "transp",0.3] );

	x3pq = linePts(pq, len=[1.5,3]);
	Line0(x3pq, ["r",0.55,"color","blue", "transp",0.2] );
    
    P= pq[0]; Q=pq[1]; R= randPt();
    pqr = concat( pq, [R] );
    LabelPt( anglePt( [P, midPt([P,Q]), R] , a=90, len=1.5), "linePts(pq)" );
    LabelPt( anglePt( pqr, a=110, len=1.5), "linePts(pq,len=0.5)"
            , ["color","red"] );
    LabelPt( anglePt( [Q,P,R], a=120, len=2), "linePts(pq,len=[1,0])"
            , ["color","green"] );
    LabelPt( anglePt( [x3pq[0], x3pq[1],R], a=120, len=2), "linePts(pq,len=[1.5,3])"
            , ["color","blue"] );    
    
}
module linePts_demo_2_ratio()
{
	pq=randPts(2);
	MarkPts( pq, ["labels","PQ"] );
	Line0(pq);
	xpq = linePts(pq, ratio=0.2);
	Line0(xpq, ["r",0.25,"transp",0.5] );

	x2pq = linePts(pq, ratio=[0.2,0]);
	Line0(x2pq, ["r",0.4,"color","red", "transp",0.3] );

	x3pq = linePts(pq, ratio=[0.4, 0.4]);
	Line0(x3pq, ["r",0.55,"color","blue", "transp",0.2] );
}
module linePts_demo_3_shrink()
{
	_pq=randPts(2);
	P= _pq[0];
    Q= onlinePt(_pq, len=3);
    pq = [P,Q];
    
	x2pq = linePts(pq, ratio=[-0.2,0]);
	Line0(x2pq, ["r",0.4,"color","red", "transp",0.3] );

	x3pq = linePts(pq, ratio=[-0.4, -0.4]);
	Line0(x3pq, ["r",0.55,"color","blue", "transp",0.2] );
    
    MarkPts( pq, ["labels","PQ"] );
	xpq = linePts(pq, ratio=-0.2);
	Line0(xpq, ["r",0.25,"transp",0.5] );
    Line0(pq, ["transp",0.3]);
	
}

//linePts_demo_1_len();
//linePts_demo_2_ratio();
//linePts_demo_3_shrink();




//Mark90_demo();


module N_demo()
{
  echom("N_demo()");
 
  pqr=pqr(); 
  //LabelPts(pqr, "PQR");
  echo(pqr=pqr);  
  N= N(pqr);
 
  Chain( pqr );
  MarkPts( pqr );
  Line0( [Q(pqr),N]);  
  MarkPt( N, ["color","yellow" ] );
   
//  N2= N2(pqr);
//  Line( [Q(pqr),N2]);  
//  LabelPts( [N,N2], ["N","N2"] );  
    
}    
//N_demo();


module normal_demo()
{
    /*
    pqr that seems to give wrong direction normal:
    
    pqr = [[1.99947, -2.27903, 0.953238], [1.19398, -0.274014, -0.410872], [-1.8453, 0.093535, 1.57612]]
ECHO: nm = [-4.48533, -5.74642, -5.79773] 

    */
    
    echom( "normal_demo" );
    //ColorAxes();
	//pqr = [[-1, -2, 2], [1, -4, -1], [2, 3, 3]];
	pqr = randPts(3);

	//PtGrid( pqr[0] );
	//PtGrid( pqr[1] );
	//PtGrid( pqr[2] );
      
    echo( pqr = pqr );
    
	Chain( pqr, ["closed",false]);
    MarkPts( pqr, ["labels","PQR"] ); //, ["r",0.05] );
  
	//PtGrid( normal(pqr) );
    
    nm = normal(pqr);
    nmpt= pqr[1]+nm/3;
    MarkPts([nmpt],["colors",["yellow"],"labels","N"]);
    echo(nm = nm );
    ln_nm= [pqr[1], nmpt];
	color("blue")
    Line0(ln_nm);
    
    Mark90( replace(pqr,2,nmpt), ["r", 0.03] );
    
    //PtGrid( nmpt );
    
    //Plane(pqr, ["mode",3, "transp",1]);
    
    pqr2 = [ for(p=pqr) p+ nm/80 ];
        
    //polyhedron( points= concat(pqr,pqr2), faces=faces("rod",3));
    
    //Plane( [ P(pqr),R(pqr), pqr[1]+nm/3], ["mode",3] );
    
    
	//Line0( [pqr[1],normalPt(pqr)] );
    
    //MarkPt( normalPt( pqr ) );
    
	// Plane(pqr, ["mode",3] );

    
//    // We create a line that is perpendicular to the normal and 
//    // passing through pqr[0]. This line has to be on the plane [pqr]
//
//    // othopt = pqr[1]'s projection on line [ORIGIN:nm],
//    //        = normal's projection on the plane
//    othopt = othoPt( [ ORIGIN
//                    , pqr[1]
//                      , nm ] );
//                      
//    // Red line: the line between one corner and the othopt                  
//    Line0( [othopt, pqr[1]], ["r",0.05, "color","red"] );                   
//    
//    
//    // Yellow line; extension of the normal to reach the plane:
//    Line( [nm, othopt], ["r",0.1, "color","yellow", "transp",0.3] );
//    
//    // If an L-shape shows up, means it's 90-degree:
//    Mark90( [pqr[1], othopt, nm],["color","black", "r",0.02] );
//
//
//	// len = 1
//	nm2 = normal(pqr, len=1);
//	Line0( [ORIGIN, nm2], ["r",0.2, "transp",0.3] );
//
//	MarkPts( pqr, ["labels","PQR"] );
//	MarkPts( [nm,nm2], ["labels",["N","N1"]]);
}
//normal_test();
//normal_demo();

module normalPt_demo()
{
	echom("normalPt");
	pqr = randPts(3);
	P = pqr[0];  Q=pqr[1]; R=pqr[2];
	Chain( pqr, ["closed", false] );
	N = normalPt( pqr );
	echo(pqr=pqr, N=N);
	MarkPts( concat( pqr,[N]) );
    //MarkPts( concat( pqr,[N]) , ["labels","PQRN"] );
    
//	//echo( [Q, N] );
//	Mark90( [N,Q,P] );
//	Mark90( [N,Q,R] );
//	Dim( [N, Q, P] );
    
	Line0( [Q, N], ["r",0.02,"color","red"] );
    
//	Nr = normalPt( pqr, len=1, reversed=true);
//	echo("Nr",Nr);
//	Mark90( [Nr,Q,P] );
//	Mark90( [Nr,Q,R] );
//	Dim( [Nr, Q, P] );
//	Line0( [Q, Nr], ["r",0.02,"color","green"] );

}
//normalPt_demo();
//Dim_test( );


module normalPts_demo_1()
{_mdoc("normalPts_demo_1",
"");

	pts = randOnPlanePts( 4 ); //randPts(3), 4);
	tripts= subarr( pts, cover=[1,1],cycle=true);
	
	norms= [for(pqr=tripts) normalPt(pqr,len=1)-Q(pqr) ];
	

	angles = [for(pqr=tripts) angle(pqr) ];

	up = normalPts(pts, len=1);
	down= normalPts(pts, len=-0.5);

	Chain( pts, ["color","red", "r",0.03, "closed",true] );
	Chain( up, ["color","green","r",0.02,"closed",true]);
	MarkPts( pts, ["labels",true, "r",0.05] );
	
	range_planeIndices= range(pts,cover=[0,1],cycle=true);
	rngp = range_planeIndices;
	echo("rngp",rngp);

	for (i=range(pts)){

		Line( [down[i], up[i]], ["r",0.005, "color","blue"] );
		Plane ( [ up  [ rngp[i][0] ]
				, up  [ rngp[i][1] ]
				, down[ rngp[i][1] ]
				, down[ rngp[i][0] ]
				]);	
	}
	//Dim( [pts[0], larger[0]] );
}

module normalPts_demo_2_arc()
{
	pts2d = arcPts(r=3, a=120, count=9);
	
	pts= pts2d;
	//echo("pts:", pts);
	larger = normalPts(pts, len=0.5);
	smaller= normalPts(pts, len=-0.5);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green","r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01, "closed",true] );
	MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}

}

module normalPts_demo_3_arc()
{
	pts = arcPts(r=3, a=120, count=9);
	//echo("pts:", pts);
	larger = expandPts(pts, dist=0.2);
	smaller= expandPts(pts, dist=-0.2);
	up = normalPts(pts, len=0.2);
	down= normalPts(pts, len=-0.2);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green", "r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01,"closed",true] );
	Chain( up, ["color","green", "r",0.01,"closed",true]);
	Chain( down, ["color","blue", "r",0.01,"closed",true] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(up,i+1, cycle=true), up[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(down,i+1, cycle=true), down[i] ]);
	}

}

module normalPts_demo_4_arc()
{
	count=12;
	pts = arcPts(r=3, a=120, count=count);
	
	larger = expandPts(pts, dist=0.2);
	smaller= expandPts(pts, dist=-0.2);
	up = normalPts(pts, len=0.2);
	down= normalPts(pts, len=-0.2);
//	Chain( pts, ["color","red", "r",0.01, "closed",true] );
//	Chain( larger, ["color","green", "r",0.01,"closed",true]);
//	Chain( smaller, ["color","blue", "r",0.01,"closed",true] );
//	Chain( up, ["color","khaki", "r",0.01,"closed",true]);
//	Chain( down, ["color","purple", "r",0.01,"closed",true] );
	MarkPts( up, ["r",0.05] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );
	//LabelPts( larger, labels=range(larger) );
	//LabelPts( up, labels=repeat([len(smaller)],len(smaller))+ range(smaller));
	//LabelPts( smaller, labels=repeat([len(smaller)*2],len(smaller))+ range(smaller));
	//LabelPts( down, labels=repeat([len(smaller)*3],len(smaller))+ range(smaller));
 
	echo( "len pts:", len(pts));
    allpts = concat( larger, up, smaller, down );
	function getfaces(_I_=0)=
	(	_I_<len(pts)-1?
		concat(  [ [_I_, _I_+1, _I_+len(pts)+1, _I_+len(pts) ]
				, [ _I_+len(pts), _I_+len(pts)+1, _I_+2*len(pts)+1, _I_+2*len(pts)]
				, [ _I_+len(pts)*2, _I_+len(pts)*2+1, _I_+len(pts)*3+1, _I_+len(pts)*3]
				, [ _I_+len(pts)*3, _I_+len(pts)*3+1, _I_+1, _I_] //_I_+len(pts)*4+1, _I_+len(pts)*4]
				]
			  , getfaces(_I_+1)
			):[]
	); 
	faces = concat( [ ]//[0, count, 2*count, 3*count ]]
//				    , [count-1,
				 , getfaces());
	echo("faces", faces);

	polyhedron( points = allpts, faces = faces );

}
module normalPts_demo_5_convexity()
{
	//
	// show normal sign(s) along a path for convexity determination later
	// 

	echom("normalPts_demo_5_convexity");

	
	pts2d = arcPts(r=3, a=120, count=9);
	
	function to3dPts(pts,z=0,_i_=0)=
	( _i_<len(pts)? concat(
		 [[pts[_i_][0], pts[_i_][1], z]]
		, to3dPts( pts, z, _i_+1)
		):[]
	);
	pts = to3dPts(pts2d);
	//echo("pts:", pts);
	larger = expandPts(pts, dist=0.2);
	smaller= expandPts(pts, dist=-0.2);
	up = normalPts(pts, len=0.2);
	down= normalPts(pts, len=-0.2);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green", "r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01,"closed",true] );
	Chain( up, ["color","green", "r",0.01,"closed",true]);
	Chain( down, ["color","blue", "r",0.01,"closed",true] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(up,i+1, cycle=true), up[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(down,i+1, cycle=true), down[i] ]);
	}

}
//normalPts_demo_1();
//normalPts_demo_2_arc();
//normalPts_demo_3_arc();
//normalPts_demo_4_arc();


module onlinePt_demo()
{
	pr = randPts(2);
	q = onlinePt( pr, len=-0.8 );
	Line(pr);
	MarkPts( [pr[0],q,pr[1]], ["grid",true,"echopts",true] );

	eg = randPts(2); 
	f = onlinePt( eg, ratio=0.33 );
	Line(eg);
	MarkPts( [eg[0],f,eg[1]], ["grid",true,"echopts",true] );

}
//onlinePt_test( ["mode",22] );
//onlinePt_demo();


module onlinePts_demo_1()
{
	pq = randPts(2);
	lens = [0.5, 1, 2, 2.5];
	pts = onlinePts( pq, lens = lens );
	
	Line0(pq, ["r",0.05, "color", "red", "transp",0.5] );
	MarkPts( pts );

	ratios = accumulate(repeat([0.2],5));
	pq2 = randPts(2);
	pts2 = onlinePts( pq2, ratios=ratios );
	//echo("in onlinePts, ratios = ", ratios);
	//echo("in onlinePts, pts2 = ", pts2);
	
	Line0(pq2, ["r",0.05, "color", "green", "transp",0.5] );
	MarkPts( pts2 );
}

module onlinePts_demo_2_evenly_divide()
{
	module evenDiv(count, color="red")
	{
		ratios = accumulate(repeat([1/count],count));
		pq2 = randPts(2);
		pts2 = onlinePts( pq2, ratios=ratios );	
		Line0(pq2, ["r",0.05, "color", color, "transp",0.5] );
		MarkPts( pts2 );
	}

	
	evenDiv( 4 );
	evenDiv( 6, "green");
}
module onlinePts_demo()
{
	//onlinePts_demo_1();
	onlinePts_demo_2_evenly_divide();
}

//onlinePts_test( ["mode",22] );
//onlinePts_demo();
//doc( onlinePts );

module othoPt_demo(){
    ops = [["r",0.02],["color","blue"]];

	//pqr=[ [0,-1,0], [1,2,3],[4,0,-1]];
	pqr=randPts(3);
	otho = othoPt(pqr);
	
	Chain( pqr, ["closed",true] );
	Line0( [pqr[1],othoPt( pqr)], ops);
	MarkPts( pqr);
	Mark90( [ norm(pqr[0]-pqr[1])>norm(pqr[1]-pqr[2])?pqr[0]:pqr[2], otho, pqr[1] ] );
	//echo("pqr= ", pqr);
	//echo("is90 pqr: ", is90([ pqr[0], pqr[1]], [ pqr[1], othoPt(pqr)] ) );
	
	//pqr2=[ [3,1,-1], [1,0,2],[-4,0,1]];
	pqr2= randPts(3);
	otho2 = othoPt(pqr2);
	//echo("pqr2= ", pqr2);
	MarkPts( pqr2 );
	Plane( pqr2, ["mode",3] ) ;//Chain( pqr2, [["closed",true]]);
	Line0( [pqr2[1],othoPt( pqr2)], ops);
	Mark90( [ norm(pqr2[0]-pqr2[1])>norm(pqr2[1]-pqr2[2])?pqr2[0]:pqr2[2], otho2, pqr2[1] ] );
	
	//echo("is90 pqr2: ", is90([ pqr2[0], pqr2[1]], [ pqr2[1], othoPt(pqr2)] ) );
	
}
//othoPt_demo();
//othoPt_test( ["mode",22] );
/*
    , rad= undef
    , ratio= 1
    , a = undef
    , a2 = undef
    , n=6
*/


//. p
module planecoefs_demo()
{
	// How do we know if the result of planecoefs are correct ?
	// 1. get random 3 points (randPts(3))
	// 2. Calc planecoefs
	// 3. Calc otho pt 
	// 4. Chk planecoefs using otho pt

	pqr = randPts(3);
	Plane(pqr, ["mode",4] );
	MarkPts( pqr );
	pcs = planecoefs(pqr); // =[ a,b,c,k ]
	a= pcs[0];
	b= pcs[1];
	c= pcs[2];
	k= pcs[3];
	
	otho = othoPt( pqr );
	MarkPts( [otho ], ["colors",["purple"], "transp",0.4] );

	isOnPlane= a*otho.x + b*otho.y + c*otho.z+k <= ZERO;

	echo("isOnPlane = ", isOnPlane);
}



module planeDataAt_demo_at_0(isdetails, isdraw){
    echom("planeDataAt_demo_at_0");    
    /*####################################################
      Same as planeDataAtQ_demo1
    */
    //####################################################
    n= 5;
    i= 0;
    
    seed = pqr();
    rcret= chainBoneData( n=n, seed= seed, seglen=3
                  , a=[120,190],a2=[50,80]);
    pts = hash(rcret, "pts"); 
    //pts = [[-2.44656, 1.28466, -2.48421], [-1.4147, 0.377128, -0.565859], [-1.24668, 0.898739, -0.706648], [0.218361, -0.970359, -2.39864], [2.68618, -1.3803, -0.796815]]; 
    //pts= [[-5.2014, -3.45814, 1.13765], [-4.99626, -3.92181, 1.45484], [-5.31156, -2.94075, 2.14715], [-3.57363, -1.29283, 2.4698], [-1.64335, -0.95359, 4.10967]]; 
    //echo(pts=pts);
    
    pqr = get3(pts,0); 
    
    //echo( pqr = pqr);
    //####################################################
 
    pts1= trslN(pts, -8);
    pldata1 = planeDataAt(pts1,i=i);
    echo(pldata1=pldata1);
    //pl = hash( pldata1, "pl") ;
    
    setting1=[ "label", "1) Default"
              , "color", "olive"
              , "pldata", pldata1
              , "wants", //let(aPQR= angle( get(pts1,i)))
                         [
                         //, "aPQR", 
                         // "aPQA", 90 
                          "aQPA", 90 
                         ,"aQPB", 90
                         //,"aRQA", aPQR/2
                         ,"aBPN", 0
                         ,"aPQAj_pqr", 45
                         //,"aRQAj_pqr", aPQR/2
                         ,"A_rotaway_pqr", 0
                         ,"pl_on_abipl", false
                         ]
              //, "isdetails", true
              //, "isdraw", true
              ];
            
    //---------------------------------------------------
    pts2= trslN(pts, -4);
    pldata2 = planeDataAt(pts2,i=i, tilt=30);
    //echo(pldata=pldata);
    //pl2 = hash( pldata2, "pl") ;
    //MarkPts( pl2, "ch;l=DEF");
    
    setting2=[ "label", "2) tilt=30"
              , "color", "red"
              , "pldata", pldata2
              , "wants", //let(aPQR= angle( get(pts1,i)))
                         [ 
                         // "aPQA", 90 
                          "aQPA", 30 
                         ,"aQPB", 90
                         //,"aRQA", aPQR/2
                         ,"aBPN", 0
                         ,"aPQAj_pqr", 75
                         //,"aRQAj_pqr", aPQR/2
                         ,"A_rotaway_pqr", 0
                         ,"pl_on_abipl", false
                         ]
              //, "isdetails", true
             // , "isdraw", true
              ];
    //---------------------------------------------------
    pts3= trslN(pts, -0);
    pldata3 = planeDataAt(pts3,i=i, rot=30);
    setting3=[ "label", "3) rot=30"
              , "color", "green"
              , "pldata", pldata3
              , "wants", //let(aPQR= angle( get(pts1,i)))
                         [
                         //, "aPQR", 
                         // "aPQA", 90 
                          "aQPA", 90 
                         ,"aQPB", 90
                         //,"aRQA", aPQR/2
                         ,"aBPN", 30
                         ,"aPQAj_pqr", anglebylen(2,sqrt(3),sqrt(7))
                         //,"aRQAj_pqr", aPQR/2
                         ,"A_rotaway_pqr", 30
                         ,"pl_on_abipl", false
                         ]
             // , "isdetails", true
              //, "isdraw", true
              ];
    //---------------------------------------------------
    pts4= trslN(pts, 4);
    pldata4 = planeDataAt(pts4,i=i, tilt=45, rot=60);
    setting4=[ "label", "4) rot=60,tilt=45"
              , "color", "blue"
              , "pldata", pldata4
              , "wants", let( pts=hash(pldata4,"pts")
                            , abc=hash(pldata4,"pl")
                            , A =abc[0]
                            , AjPQ = projPt(A,p01(pts))
                            , AjPQR= projPt(A,p012(pts))
                            )
                         [
                         //, "aPQR", 
                         // "aPQA", 90 
                          "aQPA", 45
                         ,"aQPB", 90
                         //,"aRQA", aPQR/2
                         ,"aBPN", 60
                         //,"aPQAj_pqr", anglebylen(2,sqrt(3),sqrt(7))
                         //,"aRQAj_pqr", aPQR/2
                        // ,"A_rotaway_pqr",60 //angle([ AjPQR,AjPQ,A])
                         ,"pl_on_abipl", false
                         ]
             // , "isdetails", true
              //, "isdraw", true
              ];
    //---------------------------------------------------
    pts5= trslN(pts, 8);
    //this turns wront side:
    //pts5 = [[-0.101288, 9.96727, -1.86359], [-1.10553, 9.6634, -3.94467], [-0.00820743, 9.3868, -4.70453], [-0.616616, 11.485, -4.54933], [-1.44349, 11.12, -5.6788]];
    // this turns correct side:
    //pts5 = [[6.37476, 3.21024, -6.43533], [6.25322, 2.43915, -6.75422], [7.8788, 1.52919, -6.07716], [9.41028, 1.21553, -7.27768], [9.36026, 0.706845, -7.43838]];
    //echo(pts5=pts5);
    pldata5 = planeDataAt(pts5,i=i, tilt=45, posttilt_rot=30);
    
    //preP = onlinePt(pts5, -1);
    //MarkPts( [ pts5[0], preP],"ch;l=[text,[x,]]");
    setting5=[ "label", "5) tilt=45,posttilt_rot=30"
              , "color", "purple"
              , "pldata", pldata5
              
              , "wants", let( abc= hash(pldata5,"pl")
                            ) //aPQR= angle( get(pts1,i)))
                         [
                         //, "aPQR", 
                         // "aPQA", 90 
                          "aQPA", angleAfter2Rots(45,30)
                         //,"aRQA", aPQR/2
                         ,"aBPN", 30
                         //,"aPQAj_pqr", let( AjPQR= projPt( anglebylen(2,sqrt(3),sqrt(7))
                          
                         //,"aRQAj_pqr", aPQR/2
                         ,"A_rotaway_pqr", 30
                         ,"pl_on_abipl", false
                         ]
              //, "isdetails", true
             // , "isdraw", true
              ];
    //---------------------------------------------------
    pts6= trslN2(pts, 6);    
    pldata6 = planeDataAt(pts6, i=i,closed=true);
    
    setting6=[ "label", "6) closed=true"
          , "color", "darkcyan"
          , "pldata", pldata6
          , "wants", let( pts=hash(pldata6, "pts")
                        , p3 = get3(pts,0)
                        //, aPQR= angle(p3)
                        )
                     [//"",""
                     //, "aPQR",aPQR 
                     //"aPQA", angle(p3)/2
                     "aQPA", angle(p3)/2
                     ,"aBPN", 0
                     //,"aPQAj_pqr", anglebylen( sqrt(3.5),1,2)
                     //,"aPQAj_pqr", 30
                     //,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", true
                     ]
          //, "isdetails", true
         // , "isdraw", true
          ];
    //---------------------------------------------------
    pts7= trslN2(pts, 8);
    pldata7 = planeDataAt(pts7, i=i,tilt=90);
    
    setting7=[ "label", "7) tilt=90"
          , "color", "MediumOrchid"
          , "pldata", pldata7
          , "wants", let( pts=hash(pldata7, "pts")
                        , p3 = get3(pts,0)
                        //, aPQR= angle(p3)
                        )
                     [//"",""
                     //, "aPQR",aPQR 
                     //"aPQA", angle(p3)/2
                     "aQPA", 90
                     ,"aBPN", 0
                     //,"aPQAj_pqr", anglebylen( sqrt(3.5),1,2)
                     //,"aPQAj_pqr", 30
                     //,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
            
            
    
    //####################################################
    settings = [ setting1
               , setting2
               , setting3
               , setting4
               , setting5 
               , setting6
               , setting7
               ];
     
    planeDataAt_testing( "planeDataAt_demo_at_0"
                        , settings
                        , isdetails=isdetails
                        , isdraw=isdraw
                        );

}    
//planeDataAt_demo_at_0( isdraw=0, isdetails=0);

module planeDataAt_demo_at_1(isdetails, isdraw){
    echom("planeDataAt_demo_at_1");    
    /*####################################################
      Same as planeDataAtQ_demo1
    */
    //####################################################
    n= 3;
    i=1;
    
    seed = pqr();
    rcret= chainBoneData( n=n, seed= seed
                  , a=[170,210],a2=[30,120]);
    pts = hash(rcret, "pts");    
    pqr = get3(pts,1); 
    
    //echo( pqr = pqr);
    //####################################################
 
    pqr1= trslN(pqr, -8);
    pl = hash( planeDataAt(pqr1,i=1), "pl") ;
    
    setting1=[ "label", "1_Default"
              , "color", "olive"
              , "pqr", pqr1
              , "abc", pl
              , "wants", let(aPQR= angle(pqr1))
                         [//"",""
                         //, "aPQR", 
                          "aPQA", aPQR/2
                         , "aRQA", aPQR/2
                         ,"aBQN", 0
                         ,"aPQAj_pqr", aPQR/2
                         ,"aRQAj_pqr", aPQR/2
                         ,"A_rotaway_pqr", 0
                         ,"pl_on_abipl", true
                         ]
              //, "isdetails", true
              //, "isdraw", true
              ];
    //---------------------------------------------------
 
    pqr2= trslN(pqr, -4);
    pldata2 = planeDataAt(pqr2, i=1, rot=45) ;
    
    //echo(pldata2=pldata2);
    pl2 = hash( pldata2, "pl") ;
    setting2=[ "label", "2) rot=45 (tilt auto set to 90)"
          , "color", "red"
          , "pqr", pqr2
          , "abc", pl2
          , "wants", let(aPQR= angle(pqr2))
                     [//"",""
                     //, "aPQR", 
                      "aPQA", 90
                     //, "aRQA", aPQR/2
                     ,"aBQN", 45
                     ,"aPQAj_pqr", 90//aPQR/2
                     //,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", false
                     ]
          , "isdetails",0//true
          , "isdraw", 0//true
          
          ];
    //---------------------------------------------------
    pqr21= trslN2(pqr2, -4);
    pl21 = hash( planeDataAt(pqr21,i=1, posttilt_rot=45), "pl") ;
    
    setting21=[ "label", "21) posttilt_rot=45"
          , "color", "brown"
          , "pqr", pqr21
          , "abc", pl21
          , "wants", let(aPQR= angle(pqr21))
                     [//"",""
                     //, "aPQR", 
                      "aPQA", angleAfter2Rots(aPQR/2, 45)
                     //, "aRQA", aPQR/2
                     ,"aBQN", 45
                     ,"aPQAj_pqr", aPQR/2
                     //,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", true
                     ]
          , "isdetails", 0//true
          , "isdraw", 0//true
                     
          ];
    //---------------------------------------------------
    pqr3= pqr; //trslN(pqr, -8);
    pl3 = hash( planeDataAt(pqr3, i=1,tilt=30), "pl") ;
    
    setting3=[ "label", "3) tilt=30"
          , "color", "green"
          , "pqr", pqr3
          , "abc", pl3
          , "wants", let(aPQR= angle(pqr3))
                     [//"",""
                     //, "aPQR",30 
                      "aPQA", 30
                     //, "aRQA", aPQR/2
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 30//aPQR/2
                     //,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          , "isdetails", 0//true
          , "isdraw", 0//true
          ];
    //---------------------------------------------------
    pqr4= trslN(pqr, 4);
    pl4 = hash( planeDataAt(pqr4,i=1,tiltforward=30), "pl") ;
    
    setting4=[ "label", "4) tiltforward=30"
          , "color", "blue"
          , "pqr", pqr4
          , "abc", pl4
          , "wants", let(aPQR= angle(pqr4))
                     [//"",""
                     //, "aPQR",30 
                     // "aPQA", 30
                      "aRQA", 30
                     ,"aBQN", 0
                     //,"aPQAj_pqr", 30
                     ,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          , "isdetails", 0//true
          , "isdraw", 0//true
                   
          ];
    //---------------------------------------------------
                
    //---------------------------------------------------
    //pqr= [[2.23583, 1.61866, -2.98579], [1.38282, -0.0594514, -1.77349], [1.12692, -0.562883, -1.40981]];
    pqr5= pqr; //trslN(pqr, 8);
    pldata5 =  planeDataAt(pqr5,i=1, tilt=0) ;
    //echo(pldata5= pldata5);
    pl5 = hash( pldata5, "pl") ;
    
    setting5=[ "label", "5) tilt=0"
          , "color", "purple"
          , "pqr", pqr5
          , "abc", pl5
          , "wants", let(aPQR= angle(pqr5))
                     [//"",""
                     //, "aPQR",30 
                     "aPQA", 0
                     // "aRQA", 30
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 0
                     //,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
                 
    pqr6= trslN(pqr, 4);
    pldata6 = planeDataAt(pqr6, i=1,tiltforward=30, posttilt_rot=45);
    //echo( pldata6=pldata6);
    //echo(Rf6=Rf6);
    pl6 = hash( pldata6, "pl") ;
    
    setting6=[ "label", "6) tiltforward=30, posttilt_rot=45"
          , "color", "darkcyan"
          , "pqr", pqr6
          , "abc", pl6
          , "wants", let(aPQR= angle(pqr6))
                     [//"",""
                     //, "aPQR",30 
                     //"aPQA", 30
                      "aRQA", angleAfter2Rots(30,45)
                     ,"aBQN", 45
                     //,"aPQAj_pqr", anglebylen( sqrt(3.5),1,2)
                     ,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
             
 
    pqr7= trslN(pqr, 0);
    pl7 = hash( planeDataAt(pqr7,i=1, tilt=90), "pl") ;
    
    setting7=[ "label", "7) tilt=90"
          , "color", "darkorange"
          , "pqr", pqr7
          , "abc", pl7
          , "wants", let(aPQR= angle(pqr7))
                     [//"",""
                     //, "aPQR",30 
                     "aPQA", 90
                     // "aRQA", angleAfter2Rots(30,45)
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 90
                     //,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
   
    pqr8= trslN(pqr, 0);
    pl8 = hash( planeDataAt(pqr8, i=1, tiltforward=90), "pl") ;
    
    setting8=[ "label", "8) tiltforward=90"
          , "color", "MidnightBlue"
          , "pqr", pqr8
          , "abc", pl8
          , "wants", let(aPQR= angle(pqr8))
                     [//"",""
                     //, "aPQR",30 
                     //"aPQA", 90
                      "aRQA", 90
                     ,"aBQN", 0
                     //,"aPQAj_pqr", 90
                     ,"aRQAj_pqr", 90
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
   
   
    //####################################################
    settings = [ setting1
                ,setting2
                , setting21
                , setting3
                , setting4
               , setting5 
               , setting6
               , setting7
               , setting8
               ];
     
    planeDataAtQ_testing( "planeDataAt_demo_at_1", settings
                        , isdetails=isdetails
                        , isdraw=isdraw
                        );

}    
//planeDataAt_demo_at_1( isdraw=0, isdetails=0);

module planeDataAt_demo_at_last(isdetails, isdraw){
    echom("planeDataAt_demo_at_last");    
    /*####################################################
      Same as planeDataAtQ_demo1
    */
    //####################################################
    n= 5;
    i=n-1;
    
    seed = pqr();
    rcret= chainBoneData( n=n, seed= seed
                  , a=[170,210],a2=[30,120]);
    pts = hash(rcret, "pts");    
    //pqr = get3(pts,1); 
    
    //echo( pqr = pqr);
    //####################################################
 
    pts1= trslN(pts, -8);
    //echo(pqr1=pqr1);
    pldata1 = planeDataAt(pts1,i=i);
    //echo(pldata1=pldata1);
    //pl = hash( planeDataAt(pqr1,i=1), "pl") ;
    
    setting1=[ "label", "1_Default"
              , "color", "olive"
              , "pldata",pldata1
              , "wants", let(aPQR= angle(pts1))
                         [//"",""
                         //, "aPQR", 
                          "aQRA", 90
                         ,"aQRB", 90
                         ,"aBRN", 0
                         //,"aPQAj_pqr", aPQR/2
                         ,"aRQAj_pqr", 45
                         ,"A_rotaway_pqr", 0
                         ,"pl_on_abipl", false
                         ]
              //, "isdetails", true
              //, "isdraw", true
              ];
    //---------------------------------------------------
 
    pts2= trslN(pts, -4);
    pldata2 = planeDataAt(pts2, i=i, rot=45) ;
    
    //echo(pldata2=pldata2);
    //pl2 = hash( pldata2, "pl") ;
    setting2=[ "label", "2) rot=45 (tilt auto set to 90)"
          , "color", "red"
          , "pldata",pldata2
          , "wants", let(aPQR= angle(pts2))
                     [//"",""
                     //, "aPQR", 
                      "aQRA", 90
                     ,"aQRB", 90
                     ,"aBRN", 45
                     //,"aPQAj_pqr", aPQR/2
                     //,"aRQAj_pqr", 45
                     //,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails",true
          //, "isdraw", true
          
          ];
    //---------------------------------------------------
    pts3= trslN2(pts2, -4);
    pldata3 = planeDataAt(pts3,i=i, tilt=30) ;
    
    setting3=[ "label", "3) tilt=30"
          , "color", "green"
          , "pldata",pldata3
          , "wants", //let(aPQR= angle(pts3))
                     [//"",""
                     //, "aPQR", 
                      "aQRA", 30
                     ,"aQRB", 90
                     ,"aBRN", 0
                     //,"aPQAj_pqr", aPQR/2
                     //,"aRQAj_pqr", 45
                     //,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails",true
          //, "isdraw", true
          
          ];
    //---------------------------------------------------
    pts4= trslN2(pts3, -4);
    pldata4 = planeDataAt(pts4,i=i, tilt=45, rot=60) ;
    
    setting4=[ "label", "4) tilt=45,rot=60"
          , "color", "blue"
          , "pldata",pldata4
          , "wants", let(aPQR= angle(pts4))
                     [//"",""
                     //, "aPQR", 
                      "aQRA", 45
                     ,"aQRB", 90
                     ,"aBRN", 60
                     //,"aPQAj_pqr", aPQR/2
                     //,"aRQAj_pqr", 45
                     //,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails",true
          //, "isdraw", true
          
          ];   
    //---------------------------------------------------
    pts5= trslN2(pts3, -8);
    pldata5 = planeDataAt(pts5,i=i, tilt=45,posttilt_rot=30) ;
    //pldata5_tilt45 = planeDataAt(pts5,i=i, tilt=45) ;
    //MarkPts( hash(pldata5_tilt45,"pl"), "ball=false;pl=[transp,0.5,color,green]");
    
    setting5=[ "label", "5) tilt=45,posttilt_rot=30"
          , "color", "brown"
          , "pldata",pldata5
          , "wants", let(aPQR= angle(pts5))
                     [//"",""
                     //, "aPQR", 
                      "aQRA", angleAfter2Rots(45,30)
                    // ,"aQRB", angleAfter2Rots(120,-45)
                     ,"aBRN", 30
                     //,"aPQAj_pqr", aPQR/2
                     //,"aRQAj_pqr", 45
                     //,"A_rotaway_pqr", 30
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails",true
          //, "isdraw", true
          
          ]; 
     
    //---------------------------------------------------
    pts6= trslN2(pts, 0);
    pldata6 = planeDataAt(pts6,i=i, closed=true) ;
    
    setting6=[ "label", "6) closed=true"
          , "color", "darkcyan"
          , "pldata",pldata6
          , "wants", let(aPQR= angle(get3(pts,-1)))
                     [//"",""
                     //, "aPQR", 
                      "aQRA", aPQR/2
                    // ,"aQRB", angleAfter2Rots(120,-45)
                     ,"aBRN", 0
                     //,"aPQAj_pqr", aPQR/2
                     //,"aRQAj_pqr", 45
                     //,"A_rotaway_pqr", 30
                     ,"pl_on_abipl", true
                     ]
         // , "isdetails",true
         // , "isdraw", true
          
          ];

   
    //####################################################
    settings = [ setting1
               , setting2
               , setting3
               , setting4
               , setting5 
               , setting6
               ];
     
    planeDataAt_testing( "planeDataAt_demo_at_last", settings
                        , isdetails=isdetails
                        , isdraw=isdraw
                        );

}    
//planeDataAt_demo_at_last( isdraw=1, isdetails=0);

 
module planeDataAt_demo_i_coln(){
    echom("planeDataAt_demo_i_coln");
   
    module Mark( pts, plrtn, i, color){
        echom("Mark");
        pl = hash(plrtn,"pl");
        //echo( i=i, color=color );
        //echo( pts=pts, "<br/>",plrtn=plrtn, "<br/>",pl=pl);
        
        //echofh( plrtn );
        //Plane( get3(pts,i),["transp",0.3] );
        
        MarkPts(pts, ["chain",["color",color,"r",0.02]] );
        p4 = [pl[0],pl[2], onlinePt( p10(pl),-1)
                         , onlinePt( p12(pl),-1)];   
        MarkPts( p4, ["r=0.05;trp=0.5;ch"
                     , "plane",["color",color,"transp",0.2]]);
//        Mark90( [ p4[0], pts[i], pts[i-1]] );
//        Mark90( [ p4[3], pts[i], pts[i-1]] );
    
        // To indicate the orientation of the object:
        //N = N(get3(pts,i+1));
        //Plane( [ N, pts[i+1], pts[i]]);   
        
//        j3= [pts[2],projPt( pts[2], p01(pts)), pts[1]];
//        MarkPts( j3, "ch;ball=0");
//        Mark90(j3);
        
//        J = projPt( pl[0], p01(pts));
//        //Line0( [pl[0],J]);
//        Mark90( [ pl[0],J,pts[0]]);
//        
//        Mark90( [ pl[0],pts[i], pts[i+1]]);  
//        Mark90( [ pl[1],pts[i], pts[i+1]]);  
//        
        Mark90( [ pl[0],pts[i], pts[i-1]]);        
        Mark90( [ pl[2],pts[i], pts[i-1]]);  
        Mark90( [ pl[0],pts[i], pts[i+1]]);        
        Mark90( [ pl[2],pts[i], pts[i+1]]);
    }        

        
    seed = randPts(2, [-2,2]);
    pts= concat( seed, [ onlinePt( seed, ratio=1.3)
                       , onlinePt( seed, ratio=2) ]);
    
    Rf = randOfflinePts( p01(pts)); 
    dP = N( [pts[0], pts[1], Rf] )- pts[1];
    
    
    n= 4;
    i= 0;

    //-----------------------------
    echo( _color( str("Default, closed=",false), "darkkhaki"));
    pldata= planeDataAt( pts, closed=false, i=i, Rf=Rf );
    //echofh( pldata );
    pl = hash( pldata, "pl") ;
    Mark( pts, pldata, i=i );    

//        echo(_color(str("closed=",true),"darkkhaki"));
//        pts11=trslN2(pts, 4);
//        pl11 = planeDataAt( pts11,closed=true,  i=i );
//        //echofh( pl11);
//        Line0( get(pts11, [0,-1]));
//        Mark( pts11, pl11, i=i);
//        MarkPt( pts11[2], "ball=false;l=closed");
        
    //-----------------------------
    echo(_red(str("a=30, closed=",false)));
    
    pts2=  [for(p=pts)p+ 3*dP ]; //trslN(pts, 4);
    pl2 = planeDataAt( pts2,closed=false,  i=i, a=30, Rf=Rf+3*dP );
    //echofh(pl2);
    Mark( pts2, pl2, i=i, color="red");
    echo( "a(pl0,i,i-1)= "
         , angle( [hash(pl2,"pl")[0], pts2[i], get(pts2,i-1)] )
         );
    MarkPts( [hash(pl2,"pl")[0], pts2[i], pts2[i-1]]
           , "r=0.2;trp=0.3");    
    
        echo(_red(str("a=30, rot2=45, closed=",true)));
        pts21=[for(p=pts)p+ 6*dP ]; //trslN2(pts2, 4);
        pl21 = planeDataAt( pts21,closed=true,  i=i
                          , a=30, rot2=45
                          , Rf=Rf+6*dP );
        //echofh(pl21);
        //Line0( get(pts21, [0,-1]));
        Mark( pts21, pl21, i=i, color="red");
        MarkPt( pts21[2], "ball=false;a22next:30");
        echo( "a(pl0,-1,+1)= "
             , angle( [ hash(pl21,"pl")[0]
                      , pts21[i], pts21[i+1]] )
             );

        
    //-----------------------------
    echo(_green(str("a=90, closed=",false)));
    pts3= [for(p=pts)p+ 9*dP ]; //trslN(pts, 8);
    pl3 = planeDataAt( pts3, closed=false, i=i, a=90, Rf=Rf+9*dP );
    Mark( pts3, pl3, i=i, color="green");

        echo(_green(str("a2next=90, closed=",true)));
        pts31=[for(p=pts)p+ 12*dP ]; //trslN2(pts3, 4);
        pl31 = planeDataAt( pts31, closed=true, i=i, a2next=90
                          , Rf=Rf+12*dP );
        Mark( pts31, pl31, i=i, color="green");
        MarkPt( pts31[2], "ball=false;l=a2next:90");

    //-----------------------------
    echo(_blue(str("rot2=45, closed=",false)));
    pts4=[for(p=pts)p+ 15*dP ]; //trslN(pts,12);
    pl4 = planeDataAt( pts4, closed= false, i=i, rot2=45
                     , Rf=Rf+15*dP );
    Mark( pts4, pl4, i=i, color="blue");
    //echo( pl4=pl4);

//        echo(_blue(str("rot2=45, closed=",true)));
//        pts41=trslN2(pts4, 4);
//        pl41 = planeDataAt( pts41, closed=true, i=i, rot2=45 );
//        Line0( get(pts41, [0,-1]));
//        Mark( pts41, pl41, i=i, color="blue");
//        //echo( pl41=pl41);
//        MarkPt( pts41[2], "ball=false;l=closed,rot2:45");
      
}
//planeDataAt_demo_i_coln();

module planeDataAtQ_demo1(isdetails, isdraw){
    echom("planeDataAtQ_demo1");    

    //####################################################
    pqr= pqr();
    // echo( pqr = pqr);
    //####################################################
 
    pqr1= trslN(pqr, -8);
    pl = hash( planeDataAtQ(pqr1), "pl") ;
    
    setting1=[ "label", "1_Default"
              , "color", "olive"
              , "pqr", pqr1
              , "abc", pl
              , "wants", let(aPQR= angle(pqr1))
                         [//"",""
                         //, "aPQR", 
                          "aPQA", aPQR/2
                         , "aRQA", aPQR/2
                         ,"aBQN", 0
                         ,"aPQAj_pqr", aPQR/2
                         ,"aRQAj_pqr", aPQR/2
                         ,"A_rotaway_pqr", 0
                         ,"pl_on_abipl", true
                         ]
              //, "isdetails", true
              //, "isdraw", true
              ];
    //---------------------------------------------------
 
    pqr2= trslN(pqr, -4);
    pl2 = hash( planeDataAtQ(pqr2, rot=45), "pl") ;
    
    setting2=[ "label", "2_rot=45 (tilt auto set to 90)"
          , "color", "red"
          , "pqr", pqr2
          , "abc", pl2
          , "wants", let(aPQR= angle(pqr2))
                     [//"",""
                     //, "aPQR", 
                      "aPQA", 90
                     //, "aRQA", aPQR/2
                     ,"aBQN", 45
                     ,"aPQAj_pqr", 90//aPQR/2
                     //,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
    pqr21= trslN2(pqr2, -4);
    pl21 = hash( planeDataAtQ(pqr21, posttilt_rot=45), "pl") ;
    
    setting21=[ "label", "21_posttilt_rot=45"
          , "color", "brown"
          , "pqr", pqr21
          , "abc", pl21
          , "wants", let(aPQR= angle(pqr21))
                     [//"",""
                     //, "aPQR", 
                      "aPQA", angleAfter2Rots(aPQR/2, 45)
                     //, "aRQA", aPQR/2
                     ,"aBQN", 45
                     ,"aPQAj_pqr", aPQR/2
                     //,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", true
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
    pqr3= pqr; //trslN(pqr, -8);
    pl3 = hash( planeDataAtQ(pqr3, tilt=30), "pl") ;
    
    setting3=[ "label", "3_tilt=30"
          , "color", "green"
          , "pqr", pqr3
          , "abc", pl3
          , "wants", let(aPQR= angle(pqr3))
                     [//"",""
                     //, "aPQR",30 
                      "aPQA", 30
                     //, "aRQA", aPQR/2
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 30//aPQR/2
                     //,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
    pqr4= trslN(pqr, 4);
    pl4 = hash( planeDataAtQ(pqr4, tiltforward=30), "pl") ;
    
    setting4=[ "label", "4_tiltforward=30"
          , "color", "blue"
          , "pqr", pqr4
          , "abc", pl4
          , "wants", let(aPQR= angle(pqr4))
                     [//"",""
                     //, "aPQR",30 
                     // "aPQA", 30
                      "aRQA", 30
                     ,"aBQN", 0
                     //,"aPQAj_pqr", 30
                     ,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
                
    pqr5= trslN(pqr, 8);
    pldata5 = planeDataAtQ(pqr5, tilt=0) ;
    //echo(pldata5 = pldata5);
    pl5 = hash(pldata5, "pl") ;
    
    setting5=[ "label", "5_tilt=0"
          , "color", "purple"
          , "pqr", pqr5
          , "abc", pl5
          , "wants", let(aPQR= angle(pqr5))
                     [//"",""
                     //, "aPQR",30 
                     "aPQA", 0
                     // "aRQA", 30
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 0
                     //,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
                 
    pqr6= trslN(pqr, 0);
    pl6 = hash( planeDataAtQ(pqr6, tiltforward=30, posttilt_rot=45), "pl") ;
    
    setting6=[ "label", "6_tiltforward=30, posttilt_rot=45"
          , "color", "darkcyan"
          , "pqr", pqr6
          , "abc", pl6
          , "wants", let(aPQR= angle(pqr6))
                     [//"",""
                     //, "aPQR",30 
                     //"aPQA", 30
                      "aRQA", angleAfter2Rots(30,45)
                     ,"aBQN", 45
                     //,"aPQAj_pqr", anglebylen( sqrt(3.5),1,2)
                     ,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
    
    
    pqr7= trslN(pqr, 0);
    pl7 = hash( planeDataAtQ(pqr7, tilt=90), "pl") ;
    
    setting7=[ "label", "7_tilt=90"
          , "color", "darkorange"
          , "pqr", pqr7
          , "abc", pl7
          , "wants", let(aPQR= angle(pqr7))
                     [//"",""
                     //, "aPQR",30 
                     "aPQA", 90
                     // "aRQA", angleAfter2Rots(30,45)
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 90
                     //,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
   
    pqr8= trslN(pqr, 0);
    pl8 = hash( planeDataAtQ(pqr8, tiltforward=90), "pl") ;
    
    setting8=[ "label", "8_tiltforward=90"
          , "color", "MidnightBlue"
          , "pqr", pqr8
          , "abc", pl8
          , "wants", let(aPQR= angle(pqr8))
                     [//"",""
                     //, "aPQR",30 
                     //"aPQA", 90
                      "aRQA", 90
                     ,"aBQN", 0
                     //,"aPQAj_pqr", 90
                     ,"aRQAj_pqr", 90
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
   
//   
//    //####################################################
    settings = [ 
                setting1
                ,setting2
                , setting21
                , setting3
                , setting4
               , 
               setting5 
               , setting6
               , setting7
               , setting8
               ];
     
    planeDataAtQ_testing( "planeDataAtQ_demo1", settings
                        , isdetails=isdetails
                        , isdraw=isdraw
                        );

}    
//planeDataAtQ_demo1(isdraw=0, isdetails=0);//25"

module planeDataAtQ_demo_coln(isdetails, isdraw){
    echom("planeDataAtQ_demo_coln");    
    /*####################################################
      Same as planeDataAtQ_demo1 but p-q-r is a straight line.
      It is critical for something like Arrow 
    
      Make sure an additional Rf is added to each setting.
    */
    //####################################################
    pq = randPts(2);
    pqr = app( pq, onlinePt( pq, ratio=1.3 ) ); 
    Rf = onlinePt( [pqr[1],randOfflinePt( pqr ) ], 2);
    //echo( pqr = pqr);
    //####################################################
 
    pqr1= trslN(pqr, -8);
    pl = hash( planeDataAtQ(pqr1, Rf=Rf), "pl") ;
    
    setting1=[ "label", "1) Default"
          , "color", "olive"
          , "pqr", pqr1
          , "Rf", Rf
          , "abc", pl
          , "wants", let(aPQR= angle(pqr1))
                     [//"",""
                     //, "aPQR", 
                      "aPQA", 90
                     , "aRQA", 90
                     ,"aBQN", 0
                     ,"aPQAj_pqr", aPQR/2
                     ,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", true
                     ]
          , "isdetails", 0//true
          , "isdraw", 0//true
          
          ];
    //---------------------------------------------------
 
    pqr2= trslN(pqr, -4);
    Rf2= Rf+pqr2[0]-pqr1[0];
    //echo(Rf=Rf);
    pldata2 = planeDataAtQ(pqr2, Rf=Rf2
                            , rot=45) ;
    //echo(pldata2=pldata2);
    pl2 = hash( pldata2, "pl") ;
    setting2=[ "label", "2) rot=45 (tilt auto set to 90)"
          , "color", "red"
          , "pqr", pqr2
          , "Rf", Rf2
          , "abc", pl2
          , "wants", let(aPQR= angle(pqr2))
                     [//"",""
                     //, "aPQR", 
                      "aPQA", 90
                     , "aRQA", 90
                     ,"aBQN", 45
                     ,"aPQAj_pqr", 90//aPQR/2
                     //,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", true
                     ]
          , "isdetails",0//true
          , "isdraw", 0//true
          
          ];
    //---------------------------------------------------
    pqr21= trslN2(pqr2, -4);
    Rf21= Rf+pqr21[0]-pqr2[0];
    pl21 = hash( planeDataAtQ(pqr21, Rf=Rf21, posttilt_rot=45), "pl") ;
    
    setting21=[ "label", "21) posttilt_rot=45"
          , "color", "brown"
          , "pqr", pqr21
          , "Rf", Rf21
          , "abc", pl21
          , "wants", let(aPQR= angle(pqr2))
                     [//"",""
                     //, "aPQR", 
                      "aPQA", 90
                     , "aRQA", 90
                     ,"aBQN", 45
                     ,"aPQAj_pqr", 90//aPQR/2
                     //,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", true
                     ]
          , "isdetails", 0//true
          , "isdraw", 0//true
                     
          ];
    //---------------------------------------------------
    pqr3= pqr; //trslN(pqr, -8);
    Rf3= Rf+pqr3[0]-pqr[0];
    pl3 = hash( planeDataAtQ(pqr3, Rf=Rf3,tilt=30), "pl") ;
    
    setting3=[ "label", "3) tilt=30"
          , "color", "green"
          , "pqr", pqr3
          , "Rf", Rf3
          , "abc", pl3
          , "wants", let(aPQR= angle(pqr3))
                     [//"",""
                     //, "aPQR",30 
                      "aPQA", 30
                     //, "aRQA", aPQR/2
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 30//aPQR/2
                     //,"aRQAj_pqr", aPQR/2
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          , "isdetails", 0//true
          , "isdraw", 0//true
          ];
    //---------------------------------------------------
    pqr4= trslN(pqr, 4);
    Rf4= Rf+pqr4[0]-pqr[0];
    pl4 = hash( planeDataAtQ(pqr4, Rf=Rf4, tiltforward=30), "pl") ;
    
    setting4=[ "label", "4) tiltforward=30"
          , "color", "blue"
          , "pqr", pqr4
          , "Rf", Rf4
          , "abc", pl4
          , "wants", let(aPQR= angle(pqr4))
                     [//"",""
                     //, "aPQR",30 
                     // "aPQA", 30
                      "aRQA", 30
                     ,"aBQN", 0
                     //,"aPQAj_pqr", 30
                     ,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          , "isdetails", 0//true
          , "isdraw", 0//true
                   
          ];
    //---------------------------------------------------
                
    //---------------------------------------------------
    //pqr= [[2.23583, 1.61866, -2.98579], [1.38282, -0.0594514, -1.77349], [1.12692, -0.562883, -1.40981]];
    pqr5= pqr; //trslN(pqr, 8);
    Rf5= Rf; //+pqr5[0]-pqr[0];
    pldata5 =  planeDataAtQ(pqr5, Rf=Rf5, tilt=0) ;
    //echo(pldata5= pldata5);
    pl5 = hash( pldata5, "pl") ;
    
    setting5=[ "label", "5) tilt=0"
          , "color", "purple"
          , "pqr", pqr5
          , "Rf", Rf5
          , "abc", pl5
          , "wants", let(aPQR= angle(pqr5))
                     [//"",""
                     //, "aPQR",30 
                     "aPQA", 0
                     ,"aRQA", 180
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 0
                     ,"aRQAj_pqr", 180
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
                 
    pqr6= trslN(pqr, 4);
    Rf6= Rf+pqr6[0]-pqr[0];
    pldata6 = planeDataAtQ(pqr6, Rf=Rf6, tiltforward=30, posttilt_rot=45);
    //echo( pldata6=pldata6);
    //echo(Rf6=Rf6);
    pl6 = hash( pldata6, "pl") ;
    
    setting6=[ "label", "6) tiltforward=30, posttilt_rot=45"
          , "color", "darkcyan"
          , "pqr", pqr6
          , "Rf", Rf6
          , "abc", pl6
          , "wants", let(aPQR= angle(pqr6))
                     [//"",""
                     //, "aPQR",30 
                     //"aPQA", 30
                      "aRQA", angleAfter2Rots(30,45)
                     ,"aBQN", 45
                     //,"aPQAj_pqr", anglebylen( sqrt(3.5),1,2)
                     ,"aRQAj_pqr", 30
                     ,"A_rotaway_pqr", 45
                     ,"pl_on_abipl", false
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
             
 
    pqr7= trslN(pqr, 0);
    Rf7= Rf+pqr7[0]-pqr[0];
    pl7 = hash( planeDataAtQ(pqr7, Rf=Rf7, tilt=90), "pl") ;
    
    setting7=[ "label", "7) tilt=90"
          , "color", "darkorange"
          , "pqr", pqr7
          , "Rf", Rf7
          , "abc", pl7
          , "wants", let(aPQR= angle(pqr7))
                     [//"",""
                      //"aPQR",90 
                     "aPQA", 90
                     // "aRQA", angleAfter2Rots(30,45)
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 90
                     ,"aRQAj_pqr", 90
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", true
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
   
    pqr8= trslN(pqr, 0);
    Rf8= Rf+pqr8[0]-pqr[0];
    pl8 = hash( planeDataAtQ(pqr8, Rf=Rf8, tiltforward=90), "pl") ;
    
    setting8=[ "label", "8) tiltforward=90"
          , "color", "MidnightBlue"
          , "pqr", pqr8
          , "Rf",Rf8
          , "abc", pl8
          , "wants", let(aPQR= angle(pqr8))
                     [//"",""
                      //"aPQR",90 
                     "aPQA", 90
                     // "aRQA", angleAfter2Rots(30,45)
                     ,"aBQN", 0
                     ,"aPQAj_pqr", 90
                     ,"aRQAj_pqr", 90
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", true
                     ]
          //, "isdetails", true
          //, "isdraw", true
          ];
    //---------------------------------------------------
   
   
    //####################################################
    settings = [ setting1
                ,setting2
                , setting21
                , setting3
                , setting4
               , setting5 
               , setting6
               , setting7
               , setting8
               ];
     
    planeDataAtQ_testing( "planeDataAtQ_demo_coln", settings
                        , isdetails=isdetails
                        , isdraw=isdraw
                        );

}    
//planeDataAtQ_demo_coln( isdraw=0, isdetails=0);//25"

module planeDataAt_tests(isdetails){
  planeDataAt_demo_at_0( isdraw=0, isdetails=isdetails);  
  planeDataAt_demo_at_1( isdraw=0, isdetails=isdetails); 
  planeDataAt_demo_at_last( isdraw=0, isdetails=isdetails);  
}   
//planeDataAt_tests( true );

module planeDataAtQ_tests(isdetails){
   planeDataAtQ_demo1(isdraw=0, isdetails=isdetails);
   planeDataAtQ_demo_coln( isdraw=0, isdetails=isdetails); 
}
//planeDataAtQ_tests();

module planeData_tests(isdetails){
   echom("planeData_tests");
   planeDataAtQ_tests(isdraw=0, isdetails=isdetails);
   planeDataAt_tests( isdraw=0, isdetails=isdetails); 
}
//planeData_tests();
//planeData_tests(isdetails=0);


module pqr90_demo()
{
    for(i=range(5)) 
    {
        pqr= pqr90();
        color("yellow") {
            Chain( pqr,["closed",false]);    
            if(i==0) LabelPt( pqr[0], "pqr90()" );   
        }    
    }

    for(i=range(2)) 
    {
        pqr= pqr90(d=1);
        color("red") {
            Chain( pqr,["closed",false]);    
            if(i==0) LabelPt( pqr[0], "pqr90(d=1)" );   
        }    
    }

    for(i=range(3)) 
    {
        pqr= pqr90(d=[4,6]);
        color("green") {
            Chain( pqr,["closed",false]);    
            if(i==0) LabelPt( pqr[0], "pqr90(d=[4,6])" );   
        }    
    }

    for(i=range(3)) 
    {
        pqr= pqr90(d=[2,6],z=0);
        color("blue") {
            Chain( pqr,["closed",false]);    
            if(i==0) LabelPt( pqr[0], "pqr90(d=[2,6],z=0)" );   
        }    
    }    
     
}
//pqr90_demo();

module projPl_demo()
{ echom("projPl_demo");

    //--------------------------------------------------
    // Data setup
    
        nside = 6;
        r = 1.5;
        aP = 60;
        aQ = 45;
    
    // data pts
        pq=randPts(2,d=5);
        _P=pq[0];
        _Q=pq[1];
        R=randPt();
        pqr=[_P,_Q,R];
        P=pqr[0];
        Q=pqr[1];
        echo(pqr = pqr);
        MarkPts(pq, "ch;l=PQ");
    
        RfP = anglePt(p102(pqr), aP);
        seedP = [Q,P,RfP];
        RfQ = anglePt(pqr, aQ, len=0.8);
        seedQ = [P,Q,RfQ];
        
    // pts90P        
       
        pts90P= reverse(circlePts( seedP, rad=r,n=nside ));
        MarkPts(pts90P, "ch;ball=false;pl=[transp,0.1];r=0.05");
        Mark90( [Q,P,pts90P[0]] );
        Mark90( [Q,P,pts90P[floor(nside/4)]] );
        MarkPts( [pts90P[0],P,pts90P[floor(nside/4)]],"ch;ball=false");
    
    // pts_P
        pts_P = projPts( pts90P, [P,RfP,N(seedP)], [P,Q] );
        MarkPts( pts_P, "pl=[transp,0.1,color,red];ch;r=0.08"); 
     
    // pl_Q
        pl_Q = [Q,RfQ, N(pqr)];
        MarkPts( expandPts( pl_Q, 2), "ball=false;pl=[transp,0.1,color,green]");
    //--------------------------------------------------
    
    pts_Q = projPl( pl1=pts_P
                  , pl2=pl_Q
                  , along=[P,Q]
                  , newr= r*0.5
                  , twist=30
                  );
    
    MarkPts( pts_Q, "ch;pl=[transp,0.2,color,blue]");

}
//projPl_demo();


module projPt_demo( )
{
	echom("projPt_demo");

	pqrs = randPts(4 );
	echo( pqrs );
	P= pqrs[0];  Q=pqrs[1];  R=pqrs[2];  S=pqrs[3];
	pqr = [P,Q,R];

	J = projPt( S, pqr);
	echo("J: ", J);
	Plane( pqr, "trp=0.1" );

	C= incenterPt(pqr);

	MarkPts( pqrs );
	MarkPts( [J,C] );
	MarkPts( [P,Q,R,S,J,C], "l=PQRSJC" );
//	
	//Chain( [C,J,S], ["closed",false] );
    MarkPts( [C,J,S], "ch;ball=false" );
	Mark90( [C,J,S]);

	echo( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;Check if the distance = distPtPl(...): ");
	echo( "norm( J - S )  = ", norm(J-S) );
	echo( "distPtPl(S,pqr)= ", distPtPl( S, pqr ) );


    //------------------------------------
    // If arg along is given
    _I = onlinePt( [P,Q],ratio=0.5);
     along = [S, S+uv([S,_I])];
    MarkPts( along, "ch=[r,0.05,transp,0.2];ball=false" );

    I = projPt( S, pqr, along);
    MarkPt( I, "l=I;cl=purple");
    MarkPts( linePts( [I,S], len=1), "ch=[r,0.01];cl=red;ball=false");

}

//projPt_demo();

module projPt_demo_debug( )
{
	echom("projPt_demo_debug");

//    projPt([-1.25551, 1.51645, -1.10501]
//    
//    ,[[-0.0252844, 0.46384, -1.15598], [-0.672118, 1.02266, -1.93917], [-1.44695, 1.35544, -1.14358], [-0.800113, 0.796626, -0.360394]]
//    ,[[0.776599, 2.18196, -0.844985], [-0.736115, 0.909641, -1.14978]])
    
	pqrs = randPts(4 );
	echo( pqrs );
	P= pqrs[0];  Q=pqrs[1];  R=pqrs[2];  S=pqrs[3];
	pqr = [P,Q,R];

	J = projPt( S, pqr);
	echo("J: ", J);
	Plane( pqr, "trp=0.1" );

	C= incenterPt(pqr);

	MarkPts( pqrs );
	MarkPts( [J,C] );
	MarkPts( [P,Q,R,S,J,C], "l=PQRSJC" );
//	
	//Chain( [C,J,S], ["closed",false] );
    MarkPts( [C,J,S], "ch;ball=false" );
	Mark90( [C,J,S]);

	echo( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;Check if the distance = distPtPl(...): ");
	echo( "norm( J - S )  = ", norm(J-S) );
	echo( "distPtPl(S,pqr)= ", distPtPl( S, pqr ) );


    //------------------------------------
    // If arg along is given
    _I = onlinePt( [P,Q],ratio=0.5);
     along = [S, S+uv([S,_I])];
    MarkPts( along, "ch=[r,0.05,transp,0.2];ball=false" );

    I = projPt( S, pqr, along);
    MarkPt( I, "l=I;cl=purple");
    MarkPts( linePts( [I,S], len=1), "ch=[r,0.01];cl=red;ball=false");

}

//projPt_demo_debug();


module PtGrid_demo_1_mode()
{
	pts = randPts(3, d=4);
	MarkPts( pts );
	PtGrid( pts[0], ["mode",1] );
	PtGrid( pts[1], ["mode",2] );
	PtGrid( pts[2], ["mode",3] ); // Note that mark90 is disabled automatically

    //LabelPts( pts, ["mode=1", "2(default)", "3"]);
}
//PtGrid_demo_1_mode();

module PtGrid_demo_1_coord()
{
    ColorAxes(["labels",true]);
	pt = randPt();
    pqr = randPts(3, d=4);
    Coord(pqr,["labels",true]);
    
    PtGrid( pt, ["mode",3] );
    PtGrid( pt, ["mode",3, "coord",pqr] );
	
//	MarkPts( pts );
//	PtGrid( pts[0], ["mode",1] );
//	PtGrid( pts[1], ["mode",2] );
//	PtGrid( pts[2], ["mode",3] ); // Note that mark90 is disabled automatically
//
//    LabelPts( pts, ["mode=1", "2(default)", "3"]);
}
// NEED FIX: PtGrid_demo_1_coord();

module PtGrid_demo_2_mark90()
{
	pts = randPts(4);
	MarkPts( pts );
	PtGrid( pts[0] );
	PtGrid( pts[1], ["mark90",true] );
	PtGrid( pts[2], ["mark90",["color","red", "r",0.05] ] );
    PtGrid( pts[3], ["mark90",["len",0.15] ] );
    
//    LabelPts( pts, [""
//                    , " [\"mark90\",true]"
//                    , " [\"mark90\",[\"color\",\"red\",\"r\",0.05]]"
//                    , " [\"mark90\",[\"len\",0.15]]"
//                    ], ops=["scale",0.02,"color","black"]);
}
//PtGrid_demo_2_mark90();


module PtGrid_demo_2_style()
{
	pts = randPts(4, d=4);
	MarkPts( pts );
	PtGrid( pts[0], ["color","blue"] );
	PtGrid( pts[1], ["r",0.04] );
	PtGrid( pts[2], ["r",0.02, "transp", 0.2] ); 
	PtGrid( pts[3], ["color","red", "mark90",true] ); 

//    LabelPts( pts, ["[\"color\",\"blue\"]"
//                   ,"[\"r\",0.04]"
//                   ,"[\"r\",0.02,\"transp\",0.2]"
//                   ,"[\"color\",\"red\",\"mark90\",true]"
//                   ]
//            ,ops=["scale",0.02,"color","black"]);
}
//PtGrid_demo_2_style();

module PtGrid_demo_3_proj()
{
	pts = randPts(4);
    ColorAxes();
	PtGrid( pts[0], ["mode",1] );
	PtGrid( pts[1], ["mode",1, "proj","xz", "color","red"] );
//	PtGrid( pts[2], ["mode",1, "proj","xz,yz"] );
//	PtGrid( pts[3], ["mode",1, "proj","xz,yz", "mark90", false] );
//	MarkPts( pts, ["transp",0.5] );
//    
//    op= ["scale",0.015,"color","black"];
//    LabelPt( pts[0], " mode=1", op );
//    LabelPt( pts[1], " 1,proj=\"xz\"", op );
//    LabelPt( pts[2], " 1,proj=\"xz,yz\"", op ); 
//    LabelPt( pts[3], " 1,proj=\"xz,yz\",mark90=false", op ); 
}
//PtGrid_demo_3_proj();


module PtGrid_demo_4_highlightaxes()
{
	pts = randPts(3);
	PtGrid( pts[0]);
	PtGrid( pts[1], ["highlightaxes",false] );
	PtGrid( pts[2], ["highlightaxes", ["r",0.1, "transp", 0.5, "color", "purple" ]] );
	MarkPts( pts, ["transp",0.5] );

//    op= ["scale",0.015,"color","black"];
//    LabelPt( pts[2], "[\"highlightaxes\",[\"r\",0.08,\"transp\", 0.1,\"color\",\"purple\"]]"
//    , op ); 
    
}
// Need fix: PtGrid_demo_4_highlightaxes();


module PtGrid_demo()
{
	//PtGrid_demo_1_mode();
    PtGrid_demo_1_coord();
    //PtGrid_demo_2_mark90();
	//PtGrid_demo_2_style();
	//PtGrid_demo_3_proj();
	//PtGrid_demo_4_highlightaxes();
}
module PtGrid_test( ops ){ doctest(PtGrid,ops=ops);}

//PtGrid_demo();
//PtGrid_test();
//doc(PtGrid);



//========================================================
PtGrids=["PtGrids", "pts,ops", "n/a", "3D, Point, Line, Geometry",
"
 Given a series of points, draw grid based on ops.
;; Refer to PtGrid (for a single pt) for ops definition.
"];

module PtGrids(pts, ops)
{
	for(i=[0:len(pts)-1]){
		//echo_("In PtGrids, i={_}, pts[i]={_}",[i,pts[i]] );
		PtGrid( pts[i], ops );
	}
}

module PtGrids_demo()
{
	pts = randPts(3);
	PtGrids(pts, ["mode",3]);
	MarkPts(pts, ["transp",0.3]);
}

module PtGrids_test( ops ) { doctest( PtGrids, ops=ops ); }
//PtGrids_demo();
//PtGrids_test();
//doc(PtGrids);


module quadrant_demo()
{

	pqr = randPts(3); S=randPt();
	P=P(pqr); Q=Q(pqr); R=R(pqr);
	N=N(pqr); Z=N( [N,Q,P] );


	T = projPt( S, pqr );
	U = projPt( S, [N,Q,P] );

	Chain( [Q,T,S ], ["r",0.02] ); Mark90( [Q,T,S] );
	Chain( [Q,U,S ], ["r",0.02] ); Mark90( [Q,U,S] );
	Line0( [S,Q] );

	Chain( pqr );
	MarkPts( [P,Q,R,N,S,Z,T,U], ["labels", "PQRNSZTU"] );	

	color(false, 0.2){

	Plane( pqr, ["mode",4] );
	Plane( [P,Q,N], ["mode",4] );
	Plane( [P,Q,N], ["mode",4] );
	Plane( [P,Q,N], ["mode",4] );

	}

	echo(quadrant(pqr,S)) ;
}

//quadrant_demo();

//. r

module randc_demo()
{
	light = [0.8,1];
	dark  = [0,0.4];
	translate(randPt()) color(randc(r=dark,g=dark,b=dark)) sphere(r=0.5);
	translate(randPt()) color(randc()) sphere(r=1);
	translate(randPt()) color(randc(r=light, g=light,b=light)) sphere(r=1.5);
}
//randc_test( ["mode",22] );
//randc_demo();


module randPt_demo()
{
    
    pqrs = [ for(i=range(10)) randPt() ];
    color("yellow") {
        MarkPts( pqrs, ["samesize",true] );    
        LabelPt( pqrs[0], "randPt()=randPt(3)" );   
    }
    pqr_1s = [ for(i=range(5)) randPt(1) ];
    color("red") {
        MarkPts( pqr_1s, ["samesize",true] );    
        LabelPt( pqr_1s[0], "randPt(1)" );   
    }
    
    pqr_45s = [ for(i=range(5)) randPt([4,5]) ];
    color("green") {
        MarkPts( pqr_45s, ["samesize",true] ); 
        LabelPt( pqr_45s[0], "randPt(4,5)" );   
    }
    
    pqr_z0s = [ for(i=range(5)) randPt([2,3],z=0) ];
    color("blue") {
        MarkPts( pqr_z0s, ["samesize",true] );    
        LabelPt( pqr_z0s[0], "randPt([2,3],z=0)" );   
    }

    pqr_ys = [ for(i=range(5)) randPt(0, y=[2,4]) ];
    color("purple") {
        MarkPts( pqr_ys, ["samesize",true] );    
        LabelPt( pqr_ys[0], "randPt(0, y=[2,4])" );   
    }
    
}
//randPt_test( ["mode", 22] );
//randPt_demo();


module randPts_demo()
{
    pqrs = randPts(); //[ for(i=range(10)) randPt() ];
    color("yellow") {
        MarkPts( pqrs, ["samesize",true] );    
        LabelPt( pqrs[0], "randPts()=randPts(3)" );   
    }
    pqr_1s = randPts(5,1);
    color("red") {
        MarkPts( pqr_1s, ["samesize",true] );    
        LabelPt( pqr_1s[0], "randPts(5,1)" );   
    }
    
    pqr_45s = randPts(5,[4,5]) ;
    color("green") {
        MarkPts( pqr_45s, ["samesize",true] ); 
        LabelPt( pqr_45s[0], "randPts(5,[4,5])" );   
    }
    
    pqr_z0s = randPts(5, [2,3],z=0);
    color("blue") {
        MarkPts( pqr_z0s, ["samesize",true] );    
        LabelPt( pqr_z0s[0], "randPts(5, [2,3],z=0)" );   
    }

    pqr_ys = randPts(5, 0, y=[2,4]);
    color("purple") {
        MarkPts( pqr_ys, ["samesize",true] );    
        LabelPt( pqr_ys[0], "randPts(5, 0, y=[2,4])" );   
    }
}
//randPts_demo();



module randInPlanePt_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	S = randInPlanePt(pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	LabelPts( concat(pqr,[S]) , "PQRS");
	MarkPts( [P,Q,R,S] , ["grid",false]);	
	
}
//randInPlanePt_demo();

module randInPlanePts_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randInPlanePts(pqr, 4); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	LabelPts( ps );
	MarkPts( concat( pqr, ps));	
}
module randInPlanePts_demo_2()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randInPlanePts(pqr, 100); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps) { MarkPts([p], ["r",0.08]); }
}

//randInPlanePts_demo();
//randInPlanePts_demo_2();

module randOnPlanePt_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	S = randOnPlanePt(pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	pqrs = concat(pqr,[S]);
	LabelPts( pqrs , "PQRS");
	MarkPts( pqrs , ["grid",false]);	
	Chain( pqrs, ["r", 0.08, "color", "red", "transp",0.1]);
	
}
//randOnPlanePt_demo();

module randOnPlanePts_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randOnPlanePts(4, pqr=pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	LabelPts( ps );
	MarkPts( pqr);
	MarkPts(ps, ["r",0.2, "transp", 0.4]);	
}
module randOnPlanePts_demo_2()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	MarkPts( [incenterPt(pqr)], ["r",.6, "color", "blue"] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randOnPlanePts(100, pqr=pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps) { MarkPts([p], ["r",0.05]); }
}

module randOnPlanePts_demo_3_stars()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	MarkPts( [Q], ["r",.6, "colors", ["blue"], "transp",0.3] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randOnPlanePts(100, pqr, r=[2.2, 3]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps) { MarkPts([p], ["r",0.05, "colors", [randc()]]); }
}


module randOnPlanePts_demo_4_angle()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//MarkPts( [incenterPt(pqr)], ["r",.6, "color", "blue"] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps30 = randOnPlanePts(30, pqr, a=30); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	ps4560 = randOnPlanePts(30, pqr, a=[75, 90]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	ps = randOnPlanePts(30, pqr, a=[100, 120], r=[2.3, 3]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps30) { MarkPts([p], ["r",0.05, "color", ["red"]]); }
	for(p=ps4560) { MarkPts([p], ["r",0.05, "colors", ["green"]]); }
	for(p=ps) { MarkPts([p], ["r",0.05, "colors", ["blue"]]); }

}

module randOnPlanePts_demo_5_angle()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];
	MarkPts( pqr, ["labels","PQR"] );

	//pt2 = anglePt( pqr, a=90, r=1); randOnPlanePts(2, pqr, a=[90,90] );
	P2 = normalPt( pqr );
	R2 = normalPt( [P,Q,P2] ); //anglePt( pqr, a=90, r=1);

//	pqr2 =  [ pt2[0], pqr[1], pt2[1] ] ;


//	P2= pqr[0];
	//Q= pqr[1];
//	R2= pqr[2];
	//echo( P2, Q, R2 );

	pqr2 = [ P2, Q, R2 ];

	Chain( pqr, ["r", 0.02]);

	Chain( pqr2, ["r", 0.02, "color", "green"]);


	pts = randOnPlanePts( 20, pqr );
	//MarkPts( pts, ["r",0.05,"samesize",true,"colors",["red"]] );

	pts2 = randOnPlanePts(100, pqr2);
	//echo(pts2);
	MarkPts( pts2, ["r",0.05,"samesize",true,"colors",["green"]] );
	

	//MarkPts( [incenterPt(pqr)], ["r",.6, "color", "blue"] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	//ps30 = randOnPlanePts(30, pqr, a=30); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
//	ps4560 = randOnPlanePts(30, pqr, a=[75, 90]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
//	ps = randOnPlanePts(30, pqr, a=[100, 120], r=[2.3, 3]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
//	
//	LabelPts( pqr , "PQR");
//	//LabelPts( ps );
//	//MarkPts(  ps);	
//	for(p=ps30) { MarkPts([p], ["r",0.05, "color", ["red"]]); }
//	for(p=ps4560) { MarkPts([p], ["r",0.05, "colors", ["green"]]); }
//	for(p=ps) { MarkPts([p], ["r",0.05, "colors", ["blue"]]); }

}


//randOnPlanePts_demo();
//randOnPlanePts_demo_2();
//randOnPlanePts_demo_3_stars();
//randOnPlanePts_demo_4_angle();
//randOnPlanePts_demo_5_angle();


//========================================================
module randRightAnglePts_demo()
{
	echom("randRightAnglePts");
	as = [ rand(360), rand(360), rand(360)];
	rm = RM(as);
	
	//echo( "as = ", as);
	//echo( "rm = ", rm);

	pqr = randRightAnglePts();
	echo( "pqr", pqr );

	Chain( pqr, ["r",0.02] );
	MarkPts( pqr,["grid",true] );
	Mark90 (pqr );
	LabelPts (pqr, "PQR" );
	ColorAxes();
}

//randRightAnglePts_demo();

module ribbonPts_demo_1()
{
	nPt=5;
	echom("ribbonPts_demo_1");
	pts = randOnPlanePts(nPt);
	echo("pts", pts);
	MarkPts( pts, ["labels", range(pts)] );

	rbpairs = ribbonPts( pts, len=0.3);
	
	echo("len(rbpairs)", len(rbpairs));
	Chain( pts, ["closed", false, "r",0.01, "color","black"] );
	rbs = transpose( rbpairs ); 

	echo("transpose correct?", transpose(rbs)==rbpairs );
	echo("rbs = ", len(rbs));
	Chain( rbs[0], 
			["closed", false, "color", "red", "r", 0.02, "transp",0.2] );
	//Chain( rbs[1], 
	//		["closed", false, "color", "green", "r", 0.02, "transp",0.2] );
	for(i=[0:len(rbpairs)-1]){
		echo_("i={_}, pts[i]={_}, rbpairs[{_}] = {_}"
					, [i,pts[i],i, rbpairs[i] ]) ;
		MarkPts( rbpairs[i] );
		Line0 ( rbpairs[i], ["r",0.01, "color","black"] );

		if( i>0 && i<len(rbpairs)-1)
			LabelPt( rbpairs[i][0], 
				str("aN =", angle([ normal(neighbors(pts,i-1))
								, ORIGIN
								,normal(neighbors(pts,i))
								] )
					)
			); 
		if( i< len(rbpairs)-1)
		Plane( concat( rbpairs[i], reverse(get(rbpairs, i+1, cycle=false)))  );
		//Line0( [pts[i],rbpairs[i][0] ] );
	}

}
//ribbonPts_demo_1();


module RMs_demo()
{
	echom("RMs");
	a3 = randPt(r=1)*360;
	echo("in RMs, a3= ",a3);
	rm = RM(a3);
	echo(" rm = ", rm);
	p = randPt();
	echo( " p = ", p);
	echo( "rm*p = ", rm*p );
	echo( "RMs(a3, [p]) = ", RMs(a3, [p]) );
	
}
//RMs_demo();

module RM2z_demo()
{
	pt= randPt();
		//If wanna check a pt on the xy-plane :
		//pt= [randPt()[0],randPt()[0],0];
	Line( [O,pt] );
	pt2 = RM2z(pt)*pt;
	Line( [O,pt2] );
	//echo_("In RM2z_demo, pt={_}, pt2= {_}", [pt,pt2]);
	//echo_("In RM2z_demo, len pt - len pt2= {_}", [norm(pt)-norm(pt2)]);
	//echo_("In RM2z_demo, RM2z= {_}", [RM2z(pt)]);
	MarkPts( [pt, pt2] );

	pt3 = transpose( RM2z(pt))*pt2;
	translate(pt3)
	color("blue", 0.2)
	sphere(r=0.3);
	PtGrid(pt3);

}
//RM2z_demo();

module RM2z_demo2()
{
	pt= randPts(1)[0];
	ptz= [0,0,pt.z];
}
module RM2z_test(ops)
{
	doctest( RM2z
	,[
	
	],ops );
}
//RM2z_demo();
//RM2z_test( ["mode",22] );


module RMz2p_demo()
{
	pt= randPts(1)[0];  // random pt
	Line( [O,pt] );
	PtGrid(pt);

	echo("pt = ", pt);
	L = norm(pt);       
	ptz = [0,0,sign(pt.z)*L];      // ptz: projection pt on z-axis
	Line( [O,ptz]); 

	// rot=[ 0, -atan( z/slopelen(x,y) ), x==0?90:atan(y/x)];
	// ay = -atan( z/slopelen(x,y) );  // rotate@y to xz-plane
	// az = x==0?90:atan(y/x); 		   // rotate from xz-plane to target 	
	/*
		rotate from x to xz-plane to target
		x ==> xz ==> target
		
		x, y, z
		   ay = -atan( z/slopelen(x,y) );  // rotate@y to xz-plane
		      az = x==0?90:atan(y/x); 	// rotate from xz-plane to target 	
	
		z, x, y
		   ax = -atan( y/slopelen(z,x) );  // rotate@y to xz-plane
		      ay = z==0?90:atan(x/z); 	// rotate from xz-plane to target 	
		
		
		

	*/

	sloxz = slopelen(pt.x,pt.z);
	sloyz = slopelen(pt.y,pt.z);

	// rotate at y to yz-plane
	//ax = -atan(pt.z/sloyz);   // rotate about x-axis from z-axis
	//ay = pt.x==0?90:atan(pt.z/pt.x) ;   // rotate about y axis from
											// where ax already applied
    //ax = -sign(pt.z)*sign(pt.y)*atan( abs(pt.y/slopelen(pt.z,pt.x)) );  // rotate@y to xz-plane
	ax = -sign(pt.z)*asin( pt.y/L );  // rotate@y to xz-plane
	ay = pt.z==0?90:atan(pt.x/pt.z); 	// rotate from xz-plane to target 	
	/*
	++ -
	+- +
	-+ +
	-- -

	pt.y	, pt.z, slozx, 
	  +    -    +       +
	  -    +    -       +
	  -    -    -       - 
      +    +    +       - 

	*/
	echo("pt.y = ", pt.y);
	echo("pt.z = ", pt.z);
	echo("slozx= ", pt.y/slopelen(pt.z,pt.x) );

	echo("ax", ax);
	echo("ay", ay);
	//=======================
	// rotate ax from z-axis 
	rotate( [ax, 0, 0 ] )
	translate( ptz )
	color("green", 0.3)
	sphere(r=0.1);

	// pt after rotating ax from z-axis. This pt must be yz plane
	pt2 = RMx(ax)*ptz;
	//Line([O,pt2]);

	echo("pt2=", pt2);

	// translate to pt2 directly
	translate( pt2 )
	color("green", 0.2)
	sphere(r=0.15);
	color("green", 0.3)
	Line( [O, pt2] );
	//=============================

	
	//rotate( [0, ay, 0 ] )
	//rotate( [ax, 0, 0 ] )
	rotate( [ax, ay, 0 ] )
	translate( ptz )
	color("blue", 0.3)
	sphere(r=0.2);


	pt3 = RMy(ay)*RMx(ax)*ptz;
	translate( pt3 )
	color("blue", 0.2)
	sphere(r=0.3);
	
	//=============================


	//MarkPts([pt,ptz, pt2]);


/*
	RMz2p = RMx(ax); //RMy(ay)*RMx(ax);

	pt3 = RMz2p*ptz;

	MarkPts( [pt, ptz] );

	Line([O,pt3]);
	translate(pt3 )
	color("blue", 0.2)
	sphere(r=0.3);

	*/	
	/*
	pt2 = RM2z(pt)*pt;
	Line( [O,pt2] );
	echo_("In RM2z_demo, pt={_}, pt2= {_}", [pt,pt2]);
	echo_("In RM2z_demo, len pt - len pt2= {_}", [norm(pt)-norm(pt2)]);
	echo_("In RM2z_demo, RM2z= {_}", [RM2z(pt)]);
	MarkPts( [pt, pt2] );

	pt3 = transpose( RM2z(pt))*pt2;
	translate(pt3)
	color("blue", 0.2)
	sphere(r=0.3);
	*/
}
//RMz2p_demo();

module RM2z_demo2()
{
	pt= randPts(1)[0];
	ptz= [0,0,pt.z];
}


module rotpqr_demo_chk_a2_rot()
{  echom( "rotpqr_demo_chk_a2_rot" ); 

    P = [3,0,0];
    Q = [0,0,0];
    R1= [1.5,2,0];    
	R2= [5,2,0];
    R3= [-2,2,0];
    R4= [3,2,0]; 
    
    pqr1 = [P,Q,R1];
	MarkPts( pqr1, ["label", ["text",["P","Q","R1"]]
                  , "chain",["r",0.05]] );
    J1= projPt( R1,[P,Q] );
    pqr1_new= rotpqr(pqr1, 30);
    T1= pqr1_new[2];
    MarkPts([R1,J1,T1],["label",["text",["","J1","T1"]]
                       , "chain",["r",0.005]]);

    
    pqr2 = [P,Q,R2];
	MarkPts( pqr2, ["label", ["text",["","","R2"]]
                  , "chain",true] );
    J2= projPt( R2,[P,Q] );
    pqr2_new= rotpqr(pqr2, 30);
    T2= pqr2_new[2];
    MarkPts([R2,J2,T2],["label",["text",["","J2","T2"]]
                       , "chain",["r",0.005]]);    
                    
                    
    pqr3 = [P,Q,R3];
	MarkPts( pqr3, ["label", ["text",["","","R3"]]
                  , "chain",true] );
    J3= projPt( R3,[P,Q] );
    pqr3_new= rotpqr(pqr3, 30);
    T3= pqr3_new[2];
    MarkPts([R3,J3,T3],["label",["text",["","J3","T3"]]
                       , "chain",["r",0.005]]);    
                       
    pqr4 = [P,Q,R4];
	MarkPts( pqr4, ["label", ["text",["","","R3"]]
                  , "chain",true] );
    J4= projPt( R4,[P,Q] );
    pqr4_new= rotpqr(pqr4, 30);
    T4= pqr4_new[2];
    MarkPts([R4,J4,T4],["label",["text",["","J4","T4"]]
                       , "chain",["r",0.005]]);    
                       
                                   
                       
}
//rotpqr_demo_chk_a2_rot();


module rodPts_demo_twistangle()
{
 echom("rodPts_demo_twistangle");

 pqr = randPts(3);
}



module rodsidefaces_demo_1()
{
	//==== sides=4
	pqr = randPts(3); 
	bps = cubePts(pqr ); // function cubePts( pqr, p=undef, r=undef, h=1)
    *MarkPts( bps );
	*LabelPts( bps, range(bps) );
	fs = rodsidefaces( 4 );
	color(undef, 0.4)
	*polyhedron( points = bps, faces = fs ); 

	//==== sides= 5
	c=5;
	pqr2 = randPts(3);
	
	cps1 = arcPts( pqr2, r=2, count=c, pl="yz", cycle=1 );

	newp = onlinePt(p01(pqr2), len=-0.2);
	cps2 = arcPts( [ newp, P(pqr2), R(pqr2) ] , r=2, count=c, pl="yz", cycle=1 );

	cps = concat( cps1, cps2);
	MarkPts( cps );
	LabelPts( cps, range(cps) );
	fs2 = rodsidefaces(c);
	echo("fs2 = ", fs2);
  	
	//LabelPts( cps1);
	//LabelPts( cps2);
	color(undef, 0.4)
	polyhedron( points = cps, faces = fs2 ); 
	Chain( cps1, ops=["closed",true]);
	Chain( cps2, ops=["closed",true]);
}

//rodsidefaces_demo_1();

//module rotM_demo()
//{
//   /* This demo is for rotating a non-polyhe object that
//      OpenScad made. For rotating polyhe, check out rotPts_demo()
//   */
//   echom("rotM_demo()");   
// 
//    
//   lens = [3,2,1];
//   //cube(lens);
//    
//   module demo( location=3  // coord range where random pts are created
//              , axisindices=[0,6] // indices of pts to form the rot axis
//              , a=30   // angle of rotation
//              , p= 2 // indiex of the point where the rot pointer 
//                     // (a line to connect a point before and after)
//                     // is drawn: Line( [ pts[p], pts2[p] ] )
//                     // This is for documentating the rot
//              , clr="red"
//              )
//   {
//       // Set up base obj 
//       
//       loc= randPt( location ); // point to where the cube build
//       
//       
//       
//       pqr = randPts( 3, d = location );
//       
//       pts = [ [ loc.x+ lens.x, loc.y, loc.z]
//             , loc
//             , [ loc.x, loc.y+lens.y, loc.z]
//             , [ loc.x+lens.x, loc.y+lens.y, loc.z]
//             , [ loc.x+ lens.x, loc.y, loc.z+lens.z]
//             ,  loc + [0,0,lens.z]
//             , [ loc.x, loc.y+lens.y, loc.z+lens.z]
//             , [ loc.x+lens.x, loc.y+lens.y, loc.z+lens.z]
//             ];
//       
//       MarkPts( pts, ["r",0.05] );
//       LabelPts(pts, "PQRSTUVW", ["scale",0.02] );
//       
//       axis= get(pts,axisindices); // select the two 
//       _axis= linePts(axis, len=2); // select the two pts to form axis
//       MarkPts(_axis, ["labels", get("PQRSTUVW", axisindices)] );
//       Line( _axis, ["color", clr]);
//       
//       color(undef, 0.2)
//         translate( loc ) cube( lens );
//       
//       // rot 
//       
//       pts2= rotPts( pts, axis, a );
//       for( f= CUBEFACES )
//           Chain( get(pts2, f), ["transp",0.3] );
//
//       translate( axis[1] ) 
//        multmatrix(m= rotM( axis,a) ) 
//        {
//           translate( loc-axis[1] ) // center: where obj is in xyz coord
//           color(clr, 0.4) cube( lens );
//        }  
//        
////       m = rotM( axis, a );
////      
////          translate( axis[1] ) 
////          multmatrix(m=m)
////          {
////           color(clr, 0.4) cube( lens );   
////          }// 
//       
//       //polyhedron(points= pts2, faces= CUBEFACES);
//          
//       // rot pointer: a line to connect two pts to indicate the rot
//       
//       D = othoPt( [axis[0], pts[p], axis[1]] ); // projection of point p on axis
//       Chain( [ pts[p], D, pts2[p]], ["closed",false, "color","black"]);
//       Arrow( [ pts[p], pts2[p]], ["headP",false] );
//       
//       // It will mark 90 only when 90 degree
//       Mark90( [ pts[p], D, pts2[p]] );
//      
//   } 
//
//   demo( location = [4,7], axisindices= [7,5], a=120, p=3, clr="red"); 
//   demo( location = [2,4], axisindices= [0,5], a=90, p=3, clr="green" );
////   demo( location = [-3,0], axisindices= [0,1], a=120, p=3, clr="blue" );
////   demo( location = [-7,-3], axisindices= [0,1], a=-30, p=3, clr="purple" );
//
//   
////   demo( location = [3,7], axisindices= [0,1], a=90, p=3, clr="red"); 
////   demo( location = [0,3], axisindices= [0,5], a=90, p=3, clr="green" );
////   demo( location = [-3,0], axisindices= [0,1], a=90, p=3, clr="blue" );
////   demo( location = [-7,-3], axisindices= [0,1], a=-90, p=3, clr="purple" );
//   
//    
//}    
//
////rotM_demo();


//module rotObj_demo()
//{
//    /* This demo is for rotating a non-polyhe object that
//      OpenScad made. For rotating polyhe, check out rotPts_demo()
//   */
//    ColorAxes();
//   
//   echom("rotObj_demo()"); 
//    
//   lens = [3,2,1];  // cube sizes
//    
//   module demo( 
//        location=3  // where the cube is. This is the d arg for randPt()
//      , axisindices=[0,6] // indices of pts ( among the 8 pts of 
//                          // the cube) to form the rot axis
//      , axisshift=0 // integer for making a random shift of the axis
//                    // The axis points will shift randomly by this amount
//                    // Default=0: the axis always passes thru obj corners
//      , a=30 // angle of rotation
//      , p= 2 // indiex of the point where the rot pointer 
//             // (a line to connect a point before and after)
//             // is drawn: Line( [ pts[p], pts2[p] ] )
//             // This is for documentating the rot
//      , clr="red"
//      )
//   {
//       // Set up base obj ------------------
//       
//       loc= randPt( location ); // random point to where the cube build
//       
//                                
//       //pqr = randPts( 3, d = location );
//       
//       // pts: the corners. We make pts so we can label the cube
//       pts = [ [ loc.x+ lens.x, loc.y, loc.z]
//             , loc
//             , [ loc.x, loc.y+lens.y, loc.z]
//             , [ loc.x+lens.x, loc.y+lens.y, loc.z]
//             , [ loc.x+ lens.x, loc.y, loc.z+lens.z]
//             ,  loc + [0,0,lens.z]
//             , [ loc.x, loc.y+lens.y, loc.z+lens.z]
//             , [ loc.x+lens.x, loc.y+lens.y, loc.z+lens.z]
//             ];       
//             
//       //pqr=pqr();
//       //pts = cubePts(pqr);      
//       //MarkPts( pts, ["l","PQRSTUVW", "r",0.05] );
//       MarkPts( pts, "l");
//       //LabelPts(pts, "PQRSTUVW", ["scale",0.02] );
//       
//       // Set up rotation axis ----------------------
//       
//       axis0= get(pts,axisindices);  // Select 2 pts for axis 
//       axis = [ axis0[0]+ randPt(axisshift)
//              , axis0[1]+ randPt(axisshift) ]; 
//       _axis= linePts(axis, len=2); // The two pts to draw axis
//       MarkPts(_axis, ["labels", get("PQRSTUVW", axisindices)] );
//       Line( _axis, ["color", clr]);
//                   
//       // Corners of rotated cube
//       pts2= rotPts( pts, axis, a );
//       for( f= CUBEFACES )
//           Chain( get(pts2, f), ["transp",0.3] );
//
//       // rotate -------------------------
//       
////       m = rotM( axis,a);
////       translate( axis[1] ) 
////           multmatrix(m= m) 
////           translate( loc-axis[1] )
////           color(clr, 0.4) 
////           cube( lens );
//       rotObj( axis, a, center=loc )
//           color(clr, 0.4) 
//           cube( lens );       
//       
//       // Display rotation ---------------
//       // This is the black V-shape indicating the angle of rot
//       D = othoPt( [axis[0], pts[p], axis[1]] ); // proj of p on axis
//       Chain( [ pts[p], D, pts2[p]], ["closed",false, "color","black"]);
//       Arc( [ pts[p], D, pts2[p]], ["r",0.01] );
//        
//       // It will mark 90 only when 90 degree
//       Mark90( [ pts[p], D, pts2[p]] );
//       
//       color(undef, 0.2)
//         translate( loc ) cube( lens );
//      
//   } // demo()
//
//   demo( location = [4,7], axisindices= [0,2], a=30, p=4, clr="red"); 
//   //demo( location = [2,4], axisindices= [0,5], a=90, p=3, clr="green" );
//   //demo( location = [-3,0], axisindices= [0,1], a=120, p=3, clr="blue" );
//   //demo( location = [-7,-3], axisindices= [0,1], a=-30, p=3, clr="purple" );
//}    
module rotObj_demo()
{
    /* This demo is for rotating a non-polyhe object that
      OpenScad made. For rotating polyhe, check out rotPts_demo()
   */
    //ColorAxes();
   
   echom(_b("rotObj_demo()")); 
   echo(_b("Rotate obj or pts about any axis"));
   echo(_s("Show the actions of both {_} that applied to pts, and {_} that applied to solid obj"
       , [_b("rotPts()"), _b("rotObj()")]) );
   echo(_s("Frames are {_}, solid objs are {_}"
       , [_b("pts"), _b("polyhedron")]) );
    
   lens = [3,2,1];  // cube sizes
    
   module demo( 
        location=3  // where the cube is. This is the d arg for randPt()
      , axisindices=[0,6] // indices of pts ( among the 8 pts of 
                          // the cube) to form the rot axis
      , axisshift=0 // integer for making a random shift of the axis
                    // The axis points will shift randomly by this amount
                    // Default=0: the axis always passes thru obj corners
      , a=30 // angle of rotation
      , p= 2 // indiex of the point where the rot pointer 
             // (a line to connect a point before and after)
             // is drawn: Line( [ pts[p], pts2[p] ] )
             // This is for documentating the rot
      , clr="red"
      )
   {
       echo(_color( _s("Rotate {_} degree", a),clr));    
       
       //
       // Set up original obj 
       //
       pqr=randPts(3, d=location);       
       pts = cubePts(pqr);      
       color("gold") Frame(pts);  // original frame
       
       color(undef,0.4) polyhedron(pts, rodfaces(4)); // original solid obj
       
       //
       // Rotation axis 
       //
       axis0= get(pts,axisindices);  // Select 2 pts for axis 
       axis = [ axis0[0]+ randPt(axisshift)
              , axis0[1]+ randPt(axisshift) ]; 
       _axis= linePts(axis, len=2); // The two pts to draw axis
       MarkPts(_axis,"l");
       color(clr) Line( _axis); 
                   
       // 
       // Rotation
       //                   
       pts2= rotPts( pts, axis, a );
       color(clr) Frame(pts2);
       
       rotObj( axis, a) 
           color(clr,0.4) polyhedron(pts, rodfaces(4));       
      
   } // demo()

   demo( location = [4,7], axisindices= [0,2], a=30, p=4, clr="red"); 
   demo( location = [2,4], axisindices= [0,5], a=90, p=3, clr="green" );
   demo( location = [0,2], axisindices= [0,1], a=120, p=3, clr="blue" );
   demo( location = [-3,-1], axisindices= [0,1], a=-30, p=3, clr="purple" );
}    
//rotObj_demo();


module rotPts_demo()
{
   
   echom("rotPts_demo()");   
   
   module demo( 
        location=3  // coord range where random pts are created
      , axisindices=[0,6] // indices of pts to form the rot axis
      , a=30   // angle of rotation
      , p= 2 // indiex of the point where the rot pointer 
             // (a line to connect a point before and after)
             // is drawn: Line( [ pts[p], pts2[p] ] )
             // This is for documentating the rot
      , clr="red"
      )
   {
       // Set up base obj 
       
       pqr = randPts( 3, d = location );       
       pts = cubePts( pqr );
       //MarkPts( pts );
       MarkPts(pts, ["label",["text",range(pts), "scale",0.02]] );
       
       axis= linePts(get(pts,axisindices), len=2); // select the two pts to form axis
       
       MarkPts(axis, ["labels", get("PQRSTUVW", axisindices)] );
       Line0( axis, ["color", clr]);
       color(undef, 0.2)
          polyhedron(points= pts, faces= CUBEFACES);
       
       // rot 
       
       pts2= rotPts( pts, axis, a );
       color(clr, 0.4)
          polyhedron(points= pts2, faces= CUBEFACES);
       
       // rot pointer: a line to connect two pts to indicate the rot
       
       D = othoPt( [axis[0], pts[p], axis[1]] ); // projection of point p on axis
       Chain( [ pts[p], D, pts2[p]], ["closed",false, "color","black"]);
      // Arrow( [ pts[p], pts2[p]], ["headP",false] );
       Arc( [ pts[p], D, pts2[p]], ["r",0.01] );
       
       // It will mark 90 only when 90 degree
       Mark90( [ pts[p], D, pts2[p]] );
      
   } 
   
   demo( location = [3,7], axisindices= [0,1]
       , a=60, p=3, clr="red"); 
   demo( location = [0,3], axisindices= [0,5]
        , a=90, p=3, clr="green" );
   demo( location = [-3,0], axisindices= [0,1]
        , a=120, p=3, clr="blue" );
   demo( location = [-7,-3], axisindices= [0,1]
        , a=-30, p=3, clr="purple" );
   
   
}    
//rotPts_demo( );

module rotPts_tester()
{
   echom("rotPts_tester()");   
    
   pts = [[1.65157, 2.89689, 2.62566], [1.22884, 0.0179469, 1.89561], [1.50007, -0.753806, 4.78192]];
   axis= [[1.95105, 0.0864353, 1.20733], [-0.215588, -0.11903, 3.27215]];
   angle= 85.9129;
   
   module demo( location=3  // coord range where random pts are created
              , axisindices=[0,6] // indices of pts to form the rot axis
              , a=angle   // angle of rotation
              , p= 2 // indiex of the point where the rot pointer 
                     // (a line to connect a point before and after)
                     // is drawn: Line( [ pts[p], pts2[p] ] )
                     // This is for documentating the rot
              , clr="red"
              )
   {
       // Set up base obj 
       
//       pqr = randPts( 3, d = location );       
//       pts = cubePts( pqr );
       
       //MarkPts( pts );
       LabelPts(pts, [P(pts),"Q",R(pts)], ["scale",0.02] );
       
//       axis= linePts(get(pts,axisindices), len=2); // select the two pts to form axis
       MarkPts(axis, ["labels", axis] );
       Line( axis, ["color", clr]);
       color(undef, 0.2)
          polyhedron(points= pts, faces= CUBEFACES);
       
       echo(_fmth( 
       ["calculated angle",angle([pts[0],pts[1],pts2[0]])] )); 
       // rot 
       
       pts2= rotPts( pts, axis, a );
       color(clr, 0.4)
          polyhedron(points= pts2, faces= CUBEFACES);
       echo( pts2=pts2);
       // rot pointer: a line to connect two pts to indicate the rot
       
       D = othoPt( [axis[0], pts[p], axis[1]] ); // projection of point p on axis
       Chain( [ pts[p], D, pts2[p]], ["closed",false, "color","black"]);
      // Arrow( [ pts[p], pts2[p]], ["headP",false] );
       Arc( [ pts[p], D, pts2[p]], ["r",0.01] );
       
       // It will mark 90 only when 90 degree
       Mark90( [ pts[p], D, pts2[p]] );
      
   } 
   
   demo( );;
   
}  
//rotPts_tester();


module rotangles_p2z_demo()
{
	echom("rotangles_p2z");

	module demo( label, pt, col)
	{
	    echo_("<{_}>, {_} line:  pt={_}, "
			,[label, col, pt ] );
 
		Line0([O,pt],[["markpts",false],["color",col]]);
		PtGrid(pt);

		// get the angles
		r2za= rotangles_p2z(pt, true 
				//,pt.x==0 && pt.y==0 && pt.z<0?false:true
			);   
		//echo_("pt={_}",[pt]);
		echo_("r2za ={_} ", [r2za]);


		// Demo two ways of rotating a pt to the z-axis using r2za:
		// Two spheres should be at the same spot

		// #1  Rotate from pt to z   (shown as small, dense sphere)
		rotate( r2za )     
		translate(pt)
		color( col, 1)
		sphere(r=0.1);

		// #2   Translate to the rotated pt (shown as large, transparant sphere )
		pt_on_z = RM(r2za)*pt; //RMy(r2za.y)*RMx(r2za.x) * pt;
		translate( pt_on_z )
		color(col,0.2)
		sphere(r=0.2);

	}

    //// The transparant ball must match 
    //// the solid ball on the z-axis:

	//demo( "Random pt", randPt(), "orange");
	//demo( "On xy-plane", pxy0(randPt()), "red");
	//demo( "On yz-plane", p0yz(randPt()), "green");
	//demo( "On xz-plane", px0z(randPt()), "blue");
	//demo( "On x", px00(randPt()), "red");
	//demo( "On y", p0y0(randPt()), "lightgreen");
	//demo( "On z", p00z(randPt()), "lightblue");


	module demo_reverse( label, pt, col)  // z=>p
	{
	    echo_("<{_}>, {_} line:  pt={_}, "
			,[label, col, pt ] );
 
		Line0([O,pt],[["markpts",false],["color",col]]);
		PtGrid(pt);
		MarkPts([ pt ],[["colors",[col]]] );

		r2za= rotangles_p2z(pt);   // get the angles
	
		RM_p2z = RM( r2za );              // RM for p to z
		RM_z2p = transpose( RM_p2z );	  // RM for z to p

		//echo("RM_z2p = ", RM_z2p);

		pz = [0,0, norm(pt)];
		translate( pz )
		sphere(r=0.1);		

		//echo("pz = ", pz);

		pt2 = RM_z2p* pz ;

		//echo("pt2 = ", pt2);

		translate( pt2 )
		color(col,0.2)
		sphere(r=0.2);		


	}

    //// The transparant ball must match 
    //// the solid ball on the end of line:

	//demo_reverse( "Random pt", randPt(), "orange");
	//demo_reverse( "On xy-plane", pxy0(randPt()), "red");
	//demo_reverse( "On yz-plane", p0yz(randPt()), "green");
	//sdemo_reverse( "On xz-plane", px0z(randPt()), "blue");
	//demo_reverse( "On x", px00(randPt()), "red");
	//demo_reverse( "On y", p0y0(randPt()), "lightgreen");  // <======
	//demo_reverse( "On z", p00z(randPt()), "lightblue");
}

module rotangles_z2p_demo()
{
	echom("rotangles_z2p_demo");

	module demo( label, pt, col)
	{
	    echo_("<{_}>, {_} line:  pt={_}, "
			,[label, col, pt ] );
 
		Line0([O,pt],[["markpts",false],["color",col]]);
		PtGrid(pt);
	translate( pt )
	color(col)
	sphere(r=0.1);

	echo("pt = ", pt);
	a_p2z= rotangles_p2z(pt, keepPositive=false);
	echo("a_p2z = ", a_p2z);

	ptz = RMy(a_p2z.y)*RMx(a_p2z.x) * pt;
	//MarkPts( [ pt, ptz ], [["grid",true]] );
	translate( ptz )
	sphere(r=0.1);


	a_z2p = rotangles_z2p( pt );
	echo("a_z2p", a_z2p);

	color(col,0.2)
	rotate( a_z2p )
	translate( ptz )
	sphere(r=0.3);

	}

	//demo( "Random pt", randPt(), "orange");
	//demo( "On xy-plane", pxy0(randPt()), "red");
	//demo( "On yz-plane", p0yz(randPt()), "green");
	//demo( "On xz-plane", px0z(randPt()), "blue");
	//demo( "On x", px00(randPt()), "red");   // <=======
	//demo( "On y", p0y0(randPt()), "lightgreen");
	demo( "On z", p00z(randPt()), "lightblue");
}

//

//. s


module show_coor_concaveness(){
    
    d=1; 
    r0=d/100;
    r1=d/20; 
    X=[d,0,0];
    Y=[0,d,0];
    Z=[0,0,d];
    
    color("red"){
        hull(){ sphere( r=r0 );
                translate(X) sphere( r=r1 );
              }
        translate(X+[d/5,0,0]) scale(r0) text("x");
    }

    color("green"){
        hull(){ sphere( r=r0 );
                translate(Y) sphere( r=r1 );
              }
        translate(Y+[0,d/5,0]) scale(r0) text("y");
    }    
    
    color("blue"){
        hull(){ sphere( r=r0 );
                translate(Z) sphere( r=r1 );
              }
        translate(Z+[0,0,d/5]) scale(r0) text("z");
    }         
   
    sh=-2.2*Z;
    translate(sh) sphere(r=r1*1.5);
    
    color("red"){
        hull(){ translate( sh) sphere( r=r1 );
                translate( sh+ Y ) sphere( r=r0 );
              }
        translate(sh+ Y+[0,d/5,0]) scale(r0) text("x");
    }   
     
    color("green"){
        hull(){ translate( sh) sphere( r=r1 );
                translate( sh+ X ) sphere( r=r0 );
              }
        translate(sh+ X+[d/5,0, 0]) scale(r0) text("y");
    }  

    color("blue"){
        hull(){ translate(sh)sphere( r=r1 );
                translate(sh+Z) sphere( r=r0 );
              }
        translate(sh+Z+[0,0,d/5]) scale(r0) text("z");
    }      
    
    
}

//show_coor_concaveness();

module sinpqr_demo()
{
	pqr= randPts(3);
	//pqr= randRightAnglePts();

	P=P(pqr); Q=Q(pqr); R=R(pqr);
	T= othoPt( [P,R,Q] );
	Chain( pqr );
	MarkPts( [P,Q,R,T], ["labels", "PQRT"] );
	Plane( pqr, ["mode",4]);
	n = sinpqr( pqr ); 
	a = asin(n);
	scale(0.03) text( str( n ) );

_mdoc("sinpqr_demo",
_s(" RT={_}
;; RQ={_}
;; a(pqr)= {_}
;; onQside={_}
", [ dist([R,T]), dist([R,Q]), angle(pqr), norm(T-P) > norm(Q-P)?1:-1]
)); 
	
}	
//sinpqr_demo();

module smoothPts_demo_basic(){
  // Used in forum:
  // http://forum.openscad.org/evenly-cutting-two-curves-to-make-surfaces-td13702.html
  echom("smoothPts_demo_basic");
  npt=6;
  nbz=10;
    
  pts0 = randPts(6);
  //smpts0 = smoothPts( pts0, n=20 );
  //MarkPts( smpts0, "ch;ball=false" );  
    
  chret= chainBoneData(npt, d=[-5,5], a=[100,150], a2=[0,15]);
  pts = hash( chret, "pts");
  pts = [ [1.89532, 0.194069, 0.858702]
, [0.781977, -1.05189, 0.587205]
, [-1.67616, 0.073181, -0.658208]
, [-1.48968, 2.93547, -0.46197]
, [-1.06636, 3.00855, -0.0766559]
, [-0.675592, 2.31157, 0.386013]
];
  echo( pts= arrprn(pts) );
  
    
  MarkPts(pts,"ch=[r,0.01];r=0.04;l=ABCDEF");

  opt = ["n",nbz];
  //---------------------------------------  
  smpts = smoothPts( pts,opt=opt); //n=nbz ); 
  // api: smoothPts( pts, n=10, aH=.95, dH=0.35, details=false )
    
  echo(smpts = arrprn(smpts,2) ); 
  // total pts of smpts should be: (npt-1)*nbz+npt   
  echo( npt = npt, nbz=nbz, lensmpts = len(smpts));    
  MarkPts( smpts,     "ch=[color,red,r,0.005];r=0.02;cl=red;samesize;sametransp");
    
 
}    
//smoothPts_demo_basic();

module smoothPts_demo(){
  
  echom("smoothPts_demo");
  npt=6;
  nbz=10;
    
  pts0 = randPts(6);
  smpts0 = smoothPts( pts0, n=20 );
  //MarkPts( smpts0, "ch;ball=false" );  
    
  chret= chainBoneData(npt, d=[-5,5], a=[45,135], a2=[0,45]);
  pts = hash( chret, "pts");
 
  echo( pts= arrprn(pts) );
  
    
  MarkPts(pts,"ch=[r,0.01];r=0.04");

  opt = ["n",nbz];
  //---------------------------------------  
  smpts = smoothPts( pts,opt=opt); //n=nbz ); 
  // api: smoothPts( pts, n=10, aH=.95, dH=0.35, details=false )
    
  echo(smpts = arrprn(smpts,2) ); 
  // total pts of smpts should be: (npt-1)*nbz+npt   
  echo( npt = npt, nbz=nbz, lensmpts = len(smpts));    
  MarkPts( smpts,     "ch=[color,red,r,0.005];r=0.02;cl=red;samesize;sametransp");
    
  //---------------------------------------  
  pts2 = trslN(pts, 3);
  MarkPts(pts2,"ch=[color,blue,r,0.01];r=0.05");

  //smpts2 = smoothPts( pts2,n=nbz,closed=true ); 
  //smpts2 = smoothPts( pts2, opt=["closed", "n",nbz]);
  smpts2 = smoothPts( pts2, closed=true, opt=opt);//opt=["closed", "n",nbz]);
  //for(smpt=smpts2) echofh( smpt);
  // api: smoothPts( pts, n=10, aH=.95, dH=0.35, details=false )
    
  //echo(smpts = smpts ); 
  // total pts of smpts should be: (npt-1)*nbz+npt   
  //echo( npt = npt, nbz=nbz, lensmpts2 = len(smpts2));    
  MarkPts( smpts2,     "ch=[color,green,r,0.005,closed,true];r=0.02;cl=green;samesize;sametransp");
//    "ch=[color,green,closed,false,r,0.01];r=0.02;sametransp");

  //---------------------------------------  
//  pts3 = arcPts( trslN(pts2,3) , a= 360*(4-1)/4, n=4, rad=2);
//  MarkPts(pts3,"ch=[color,blue,r,0.01,closed,true];r=0.03");
//
//  smpts3 = smoothPts( pts3,n=nbz,closed=true ); 
//  // api: smoothPts( pts, n=10, aH=.95, dH=0.35, details=false )
//    
//  //echo(smpts = smpts ); 
//  // total pts of smpts should be: (npt-1)*nbz+npt   
//  echo( npt = npt, nbz=nbz, lensmpts2 = len(smpts2));    
//  MarkPts( smpts3, 
//    "ch=[color,blue,r,0.005];r=0.02;cl=green;samesize;sametransp"); 
 
}    
//smoothPts_demo();


//module smoothPts_demo_rng(){
//  
//  echom("smoothPts_demo_rng");
//  npt=6;
//  nbz=10;
//    
//  chret= chainBoneData(npt, d=[-5,5], a=[45,135], a2=[0,45]);
//  pts = hash( chret, "pts");
//
//  echo( pts= arrprn(pts) );
//  
//    
//  MarkPts(pts,"ch=[r,0.01];r=0.04");
//
//  opt = ["n",nbz];
//  //---------------------------------------  
//  smpts = smoothPts( pts,opt=opt); //n=nbz ); 
//  // api: smoothPts( pts, n=10, aH=.95, dH=0.35, details=false )
//    
//  echo(smpts = arrprn(smpts,2) ); 
//  // total pts of smpts should be: (npt-1)*nbz+npt   
//  echo( npt = npt, nbz=nbz, lensmpts = len(smpts));    
//  MarkPts( smpts,     "ch=[color,red,r,0.005];r=0.02;cl=red;samesize;sametransp");
//    
//  //---------------------------------------  
//  pts2 = trslN(pts, 3);
//  MarkPts(pts2,"ch=[color,blue,r,0.01];r=0.05");
//
//  smpts2 = smoothPts( pts2, closed=true, opt=opt
//                    );
//  MarkPts( smpts2,     "ch=[color,green,r,0.005,closed,true];r=0.02;cl=green;samesize;sametransp");
//  
//  //---------------------------------------  
//  pts3 = arcPts( trslN(pts2,3) , a= 360*(4-1)/4, n=4, rad=2);
//  MarkPts(pts3,"ch=[color,blue,r,0.01,closed,true];r=0.03");
//
//  smpts3 = smoothPts( pts3,n=nbz,closed=true ); 
//  // api: smoothPts( pts, n=10, aH=.95, dH=0.35, details=false )
//    
//  //echo(smpts = smpts ); 
//  // total pts of smpts should be: (npt-1)*nbz+npt   
//  echo( npt = npt, nbz=nbz, lensmpts2 = len(smpts2));    
//  MarkPts( smpts3, 
//    "ch=[color,blue,r,0.005];r=0.02;cl=green;samesize;sametransp"); 
// 
//}    
////smoothPts_demo_rng();

module smoothPts_demo_knot(){
  
  echom("smoothPts_demo_knot");
  //npt=4;
  nbz=5;
    
  // knot data taken from 
// http://www.colab.sfu.ca/KnotPlot/KnotServer/coord/2.html    
kpts= [ [-5.00478, -2.20299, 0.670091]
, [-4.88258, -3.10157, -0.092889]
, [-4.39016, -3.83889, -0.879547]
, [-3.58148, -4.33844, -1.58905]
, [-2.5438, -4.54905, -2.12783]
, [-1.39221, -4.4512, -2.4161]
, [-0.260491, -4.06523, -2.39773]
, [0.719727, -3.45674, -2.05399]
, [1.46218, -2.72936, -1.41635]
, [1.98739, -1.97697, -0.572567]
, [2.40977, -1.20091, 0.328836]
, [2.75931, -0.295204, 1.10407]
, [2.94144, 0.797771, 1.61339]
, [2.86544, 1.99092, 1.78476]
, [2.50415, 3.1243, 1.61289]
, [1.8892, 4.0411, 1.14695]
, [1.09079, 4.62244, 0.471398]
, [0.20147, 4.7968, -0.308471]
, [-0.677249, 4.54313, -1.07874]
, [-1.4442, 3.89142, -1.72581]
, [-2.01031, 2.92229, -2.14655]
, [-2.31117, 1.76358, -2.2603]
, [-2.32623, 0.577388, -2.02584]
, [-2.09856, -0.483797, -1.46105]
, [-1.73098, -1.36235, -0.652872]
, [-1.3028, -2.14421, 0.241411]
, [-0.749772, -2.91679, 1.03701]
, [0.035892, -3.6493, 1.6018]
, [1.04906, -4.23677, 1.86424]
, [2.19005, -4.58035, 1.80777]
, [3.32658, -4.62323, 1.46205]
, [4.32915, -4.35433, 0.888819]
, [5.08931, -3.80048, 0.169054]
, [5.52611, -3.01879, -0.604689]
, [5.59217, -2.0899, -1.33538]
, [5.27824, -1.11183, -1.92633]
, [4.61907, -0.192878, -2.28828]
, [3.6983, 0.561824, -2.35037]
, [2.64638, 1.07048, -2.07684]
, [1.61169, 1.31239, -1.4844]
, [0.683465, 1.36521, -0.652044]
, [-0.189789, 1.35858, 0.268235]
, [-1.13209, 1.28192, 1.07708]
, [-2.17837, 1.00458, 1.62779]
, [-3.22565, 0.462146, 1.84751]
, [-4.12242, -0.314411, 1.72631]
, [-4.74167, -1.23833, 1.30823]
];
  //echo( pts= arrprn(pts) );
  
    
  //MarkPts(pts,"ch=[color,blue,r,0.01];r=0.03");
  MarkPts( kpts
       , ["chain",["closed",true,"transp",0.5]
         , "samesize",true, "sametransp",true, "color","blue"
         , "r", 0.06]
       );
  //---------------------------------------  
  smpts = smoothPts( kpts,n=nbz ); 
  // api: smoothPts( pts, n=10, aH=.95, dH=0.35, details=false )
    
  //echo(smpts = smpts ); 
  // total pts of smpts should be: (npt-1)*nbz+npt   
  echo( nbz=nbz, lensmpts = len(smpts));    
  MarkPts( smpts, 
    "ch=[color,red,r,0.05];r=0.02;cl=red;samesize;sametransp");
    
  //---------------------------------------  

  
}    
//smoothPts_demo_knot();

module smoothPts_demo_circle(){
  
  echom("smoothPts_demo_circle");
  npt=8;
  nbz=10;
  r = 3;
  
  pqr=pqr();
  pqr=[ [1,0,0], ORIGIN, [0,1,0]];
    
  module makecircle(pqr, n=3, aH=0.5, dH=0.38, color=undef){
      
      pts = arcPts(pqr, a= 360*(n-1)/n, n=n, rad=r);
        
      MarkPts(pts,"ch=[color,blue,r,0.01, closed,true];r=0.1");
      smpts = smoothPts( pts,n=nbz , aH=aH, dH=dH
                       , closed=true );
      echo(n=n, nsmpts=len(smpts), pqr=pqr, smpts= smpts); 
      MarkPts( smpts, ["r=0.015;cl=red;samesize;sametransp"
                    , "chain", ["color",color,"r",0.02,"closed",true]]);   
  }
  
  //
  // Make a real circle for comparison
  //
  n0=36;
  c0 = [for(i=range(n0)) 
          anglePt( pqr, a= (i*360)/n0, len=r)
       ];
  //MarkPts( c0,"ch=[closed,true,r,0.06];ball=false;trp=0.2");    
  
  
  // 
  // Make circles on different polyhedra using smoothPts
  //  
  makecircle( trslN(pqr, 2), n=3, aH=0.5, dH=0.445);
  makecircle( trslN(pqr, 4), n=4, aH=0.5, dH=0.385, color="red" );
  makecircle( trslN(pqr, 6), n=5, aH=0.5, dH=0.37, color="red" );
  makecircle( trslN(pqr, 8), n=6, aH=0.5, dH=0.362, color="green" );
  makecircle( trslN(pqr, 10), n=7, aH=0.5, dH=0.357, color="blue" );

  acut= linePts( [ N(pqr),pqr[1]], len=[12, 2]);
  Line0( acsec, ["r",0.1] );
  
}    
//smoothPts_demo_circle();

module smoothPts_demo_csec_MA_Petronas_Towers(){
    
    echom("smoothPts_demo_csec_MA_Petronas_Towers");
/*
http://www.architectureweek.com/cgi-bin/awimage?dir=2003/0219&article=building_1-2.html&image=12072_image_10.jpg
*/

  pqr=pqr();
    
  plan_2sq = joinarr(
             [ for(i=range(4))  
               [ anglePt( pqr, a=90, a2= (i*360)/4, len=5)
               , anglePt( pqr, a=90, a2= (i*360)/4+45, len=5) ]
             ]  
         );
  echo( plan_2sq = arrprn( plan_2sq,2) );
  
  plan_2sq2 = joinarr(
           [ for(i=range(8))
           let( vln = get( plan_2sq, [i-1, i==7?0:i+1])
              , postln= get( plan_2sq, [i, i==6?0:i==7?1:i+2] )
              , itc= intsec( vln, postln)
              )
            [ plan_2sq[i], itc]
         ]);
  echo( plan_2sq2 = arrprn( plan_2sq2,2) );
               
  plan = joinarr(
           [ for(i=range(0,2,len(plan_2sq2)-1))
           let( pts = plan_2sq2
              , L = len(pts)
              , lasti= L-1
              , dpq = dist( p01(pts) )
              , P = pts[i]
              , O = pts[i+1]
//              , newpts= arcPts(  
//              , ni = i==lasti?0:i+1
//              , n2i= i>=lasti-1?(i+2-L):i+2
//              , Q = onlinePt( get(pts,[ni,i]), dpq/3 )
//              , R = onlinePt( get(pts,[ni,n2i]), dpq/3 )
//              , S = pts[i+2>lasti? i+2-L : i+2]
//              , smpts= getBezierPtsAt( 
//                         [ rotPt(P, [Q,R], 180),Q,R
//                         , rotPt(S, [Q,R], 180) ]
//                         ,1
//                       ,aH=1.5, dH=1) 
              )
            //[P,Q,R,S ] 
           concat( [P], hash(smpts,"bzpts") )
         ]
           );
  echo( plan = arrprn( plan,2) );
           
  MarkPts( plan, "ch=[closed, true]" );
           
           
             
}
//UNDER CONSTRUCT: smoothPts_demo_csec_MA_Petronas_Towers();

//pts= [[-0.620005, -2.14803, 5.63525], [0.727142, -2.33525, 5.39754], [1.96004, -2.50812, 5.6755], [3.07869, -2.66665, 6.46914]];
//smpts= getBezierPtsAt(pts,

module squarePts_demo()
{
    echom("squarePts_demo");
	ops= ["closed",true,"r",0.03];
	ops2= ["closed",true,"r",0.2,"transp",0.3];

	pqr = randPts(3,[-1,2]);
	//Chain0( pqr,ops );
    
    MarkPts( pqr, "ch=[closed,false,r,0.06];l=PQR");
    
	p4 = squarePts(pqr);
	MarkPts(p4,["ch=[closed,true]", "label","ABCD"]);
    Mark90( get3(p4,0) );
    Mark90( get3(p4,1) );
    Mark90( get3(p4,2) );
    Mark90( get3(p4,3) );
    //echo( get3_3 = get3(p4,3));
    
	//Chain0( p4,ops2 );

	pqr2 = randPts(3,[-2,1]);
	//Chain0( pqr,ops );
    
    MarkPts( pqr2, "ch=[closed,false,r,0.06,color,red];l=PQR");
    
	p42 = squarePts(pqr2);
	MarkPts(p42,["ch=[color,red,closed,true]", "label","ABCD"]);
    Mark90( get3(p42,0) );
    Mark90( get3(p42,1) );
    Mark90( get3(p42,2) );
    Mark90( get3(p42,3) );
}

//squarePts_demo();


module squarePts_demo_2_debug()
{
	echom("squarePts_demo_2_debug");
// 7/7/2014: 
// anglePt(a=90), that uses squarePts, sometimes goes wrong. See anglePt_demo_3_90() and anglePt_demo_4_90_debug().

// Example wrong cases:

// [[3.00336, 1.06282, 2.49988], [3.00336, 1.06282, 2.49988], [1.99609, -0.615917, 2.09092]]

// bug found: it turns out that the P and Q are the same !!! Fix this in
// anglePt()

	pqr1= [[3.00336, 1.06282, 2.49988], [3.00336, 1.06282, 2.49988], [1.99609, -0.615917, 2.09092]];

	ops= ["closed",true,"r",0.03];

	echo("pqr1", pqr1);
	MarkPts(pqr1, ["grid",true]);
	//p4_1 = squarePts(pqr1);
	//echo("p4_1", p4_1);

}

module squarePts_demo_3_setsize()
{
	//ops= ["closed",true,"r",0.03];
	pqr = randPts(3,r=4);
	MarkPts(pqr,["labels","PQR"]);
	pqrs = squarePts(pqr);
	Chain( pqrs,["r",0.02,"color","red"] );


	pqrs2= squarePts(pqr, p=2);
	//MarkPts(pqrs2);
	//pp4 = squarePts(pqr2);
	Chain( pqrs2, ["r",0.06, "transp",0.3, "color","green"] );

	pqrs3= squarePts(pqr, r=2);
	//MarkPts(pqrs2);
	//pp4 = squarePts(pqr2);
	Chain( pqrs3, ["r",0.1, "transp",0.3, "color","blue"] );

	pqrs4= squarePts(pqr, p=3,r=3);
	//MarkPts(pqrs2);
	//pp4 = squarePts(pqr2);
	Chain( pqrs4, ["r",0.15, "transp",0.3, "color","purple"] );
	Dim( p012(pqrs4),["spacer",1], ["color","purple"]);
	Dim( p123(pqrs4),["spacer",1], ["color","purple"]);


	Dim( p301(pqrs), ["color","red"] );
	Dim( p230(pqrs), ["color","red"] );
	Dim( p012(pqrs2), ["color","green"]);
	Dim( p123(pqrs3), ["color","blue"]);

}
//

//. t

module tangLn_demo(){
    
    echom("tangLn_demo");
  
    pqr = pqr();
    tLn = tangLn(pqr);
  
    MarkPts( pqr, ["l=PQR","chain",["r",0.025]]);
    MarkPts( tLn, ["color","lightgray", "chain",["r",0.0015,"color","green"] ] );  
    
    abiPt = angleBisectPt(pqr);
    
   MarkPts( [pqr[1], abiPt],["chain",["r",0.0015,"color","green"] ] );  
   Mark90( [ abiPt, pqr[1], tLn[1] ] ); 
    
}

//tangLn_demo();


module tanglnPt_P_demo()
{
    echom("tanglnPt_P_demo");
    pqr=pqr();
    MarkPts( pqr, ["chain",true, "label","PQR"] );
    
    Arc( pqr, ["r",0.01] );
    
    T= tanglnPt_P(pqr);
    MarkPt(T, ["label","T"]);
    Line0( [P(pqr),T] );
    Mark90( [Q(pqr),P(pqr),T] );
    
    //------------------------------
    
}    

//tanglnPt_P_demo();


module Transforms_demo()
{
  cube( [ 10, 4, 2 ] ); 
  
  Transforms( ["rot", [0,90,0], "transl", [5,0,10]] ) cube( [ 10, 4, 2 ] );
}  

//Transforms_demo();


module tocoord_demo()
{
   echom("tocoord_demo()"); 
   pts= pqr();
   coord= coordPts(pqr());
       
   pts2 = tocoord( pts, coord );
   echo( pts2=pts2); 
   Coord(coord, ["mark90",true, "len",5]);
//   
   Chain(pts); MarkPts(pts); LabelPts(pts);
   Chain(pts2); MarkPts(pts2); LabelPts(pts2);  
}

//tocoord_demo();

//P=[1,1,1]; Q=[2,2,2]; R=[3,3,3]; S=[1,2,3];
//echo( isonline( P, [Q,R] ) );
//echo( isonline( [Q,R], P ) );
//echo( isonline( [R,P], Q ) );
//echo( isonline( Q,[R,P]));
//echo( isonline( S,[R,P]));


module trsfPts_dev() // This works nicely (2015.8.6)
{   echom("trsfPts_dev");
    
    pts = randPts(5);
    //pts = [[1.51404, -0.0622942, -0.684195], [-1.29786, 2.52805, 1.01567], [-2.05047, -0.6085, -1.93373], [-1.10846, 2.51599, -0.39901]];
    MarkPts( pts, "ch=[r,0.06]");
    Plane( p012(pts));
    
    pqr2 = trslN(pqr(),8);
    MarkPts( pqr2, "ch=[r,0.03,color,red]");
    pts2= trsfPts( pts, pqr2);

    //---------------------------- Translate to make pl_red
    v= pqr2[1]-pts[1];
    pl_red = [ for(p=pts) p+v];
    echo(len=len(pl_red), pl_red=pl_red);    
    MarkPts( p012(pl_red) , "ch;pl=[color,red,transp,0.2]" ); 
    
    //----------------------------
    rotpl = [ pl_red[0], pqr2[1], pqr2[0] ];
    axis = [  N( rotpl ),pqr2[1] ];
    rota = angle(rotpl);
    
    MarkPts( linePts(axis, 1), "ch=[color,green];ball=false" );

    pl_green = rotPts( pl_red, axis, rota);
    echo(pl_green= pl_green); 
    MarkPts( p012(pl_green), "ch;pl=[color,green,transp,0.2]" ); 
    //----------------------------

    axis2 = p01(pl_green);
    rota2= twistangle( pl_green[2], pl_green[0], pl_green[1], pqr2[2] );
    //rotpl2 = [ pl_green[0], axis2, rota2 ];
    
    pl_blue = rotPts( pl_green, axis2, rota2);
    echo(pl_blue= pl_blue); 
    MarkPts( p012(pl_blue), "ch;pl=[color,blue,transp,0.2]" ); 
    
    MarkPts( pl_blue, "ch=[r,0.1,transp,0.6,color,blue]"); 

    echo( _red("Is pqr[2] on the final plane?")
        , isOnPlane( pl_blue, pqr2[2] )
        );
    
}
//trsfPts_dev();


module trsfPts_demo2()
{   echom("trsfPts_demo2");
    
    seed = pqr();
    
    chbone = chainBoneData( seed=seed, a=[120,200], n=5);
    bone = hash(chbone, "pts");
    //bone =  [[-1.67522, -2.11041, 1.38088], [-0.350102, 0.0791487, 0.897667], [0.726573, 0.322555, 0.796033], [2.84902, -0.876988, 1.34354], [3.09429, -1.30339, 2.28015]];
    //bone = [[1.53807, -1.59882, -1.38625], [0.669703, -0.0161568, 0.53531], [-0.101587, -0.498961, 1.68992], [-2.4891, -1.13809, 2.71329], [-5.02203, -0.297938, 2.88669]];
    echo( bone=bone );
    MarkPts(bone);
    
    chdata = chaindata( bone );
    Chain(chdata,"trp=0.6");
    cuts = hash( chdata, "cuts");
    pts = joinarr( cuts );
    echo( len=len(pts), pts=arrprn(pts));
    
    seed2 = pqr();
    MarkPts(seed2, "ch=[r,0.02,color,red]");
    pts2 = trsfPts( pts, seed2);
    echo( len=len(pts2), pts2=arrprn(pts2));
    MarkPts( pts2, "ch;l");
    
    bone2 = trsfPts( bone, seed2);
    MarkPts( bone2, "ch=[r,0.4,transp,0.1]");
    
}
//trsfPts_demo2();


module trsfPts_demo_polyh_cube()
{
    /* Error sets:
    
    1. ECHO: "[pts=[[-2.0189, 2.5282, -2.48549], [2.35482, -0.42459, -0.964848], [2.04981, -1.55096, -2.27477], [-2.32391, 1.40183, -3.79541], [-1.43966, 3.07471, -3.09031], [2.93406, 0.121926, -1.56966], [2.62905, -1.00445, -2.87958], [-1.74467, 1.94834, -4.40022]]]"
ECHO: "[pqr=[[-2.55023, 2.9108, -2.80757], [0.498712, 2.89254, 0.413551], [-2.30129, -1.85285, -0.821948]]]"
    
    2. ECHO: "[pts=[[0.383177, -0.289641, 1.45697], [-2.59659, 1.6975, 1.82253], [-2.82353, 1.39896, 1.59548], [0.156235, -0.588179, 1.22993], [0.16646, -0.770865, 2.30636], [-2.81331, 1.21628, 2.67191], [-3.04025, 0.917741, 2.44487], [-0.0604818, -1.0694, 2.07932]]]"
ECHO: "[pqr=[[1.59171, 1.92459, -2.57988], [-2.19432, 2.2309, 1.86824], [2.64705, 0.81638, 1.45363]]]"

   3 d= 0.0512621
    
   ECHO: "[pts=[[-1.79219, -2.9288, 0.161657], [1.39165, 1.585, -0.911543], [0.690153, 1.9895, -1.29136], [-2.49368, -2.5243, -0.218155], [-2.04658, -2.53894, 1.0467], [1.13726, 1.97486, -0.0265029], [0.435765, 2.37936, -0.406315], [-2.74807, -2.13444, 0.666885]]]"
ECHO: "[pqr=[[-2.90627, -0.0315336, 2.16692], [1.0908, 2.96722, 1.99115], [0.960261, 1.31462, -2.72605]]]" 
    
    4 d= -0.822201
    
    ECHO: "[pts=[[-0.840547, -1.45962, -2.49484], [-0.451124, 2.95307, -1.18848], [-0.663536, 2.29779, 1.08831], [-1.05296, -2.11491, -0.218049], [0.151882, -1.56559, -2.43275], [0.541305, 2.84711, -1.12639], [0.328894, 2.19182, 1.1504], [-0.0605287, -2.22087, -0.155958]]]"
ECHO: "[pqr=[[1.75585, 2.06671, 2.29612], [2.08069, -2.87726, 2.9681], [-2.76984, -1.8333, 2.3496]]]"
    
    
    */
    
    pts = cubePts(randPts(3));
    _pqr = pqr();
    pqr_x = [ onlinePt( p10(_pqr), len=5 )
          , Q(_pqr)
          , onlinePt( p12(_pqr), len=5 )
          ];
    pqr = trslN( pqr_x, 4);
    
    pts2 = trsfPts(pts, pqr);
    
//    echo( _fmth( ["pts",pts]) );
//    echo( _fmth( ["pqr",pqr]) );
//    echo( _fmth( ["pts2",pts2]) );
//    
    //Plane( pqr, ["mode",3] ); //LabelPts( pqr, "PQR" );  
    MarkPts( pqr, "ch;pl");
    
    color("yellow", 0.8) polyhedron( points= pts, faces=CUBEFACES ); MarkPts( slice(pts,0,4) );
    color("red", 0.4) polyhedron( points= pts2, faces=CUBEFACES ); MarkPts( slice(pts2,0,4) );
    
    // Chk if R(pqr) is on cube surface
    d = distPtPl( R(pqr), get( pts2,[0,1,2]) );
    echo(_fmth(["d(R~pts2) must be 0: ", d] ));
    
}
//trsfPts_demo1();
//trsfPts_demo_polyh_cube();


module getbonedata_demo(){
    echom("getbonedata_demo");
    
    pqr = pqr();
    
    chbone = chainBoneData( seed=pqr, a=[120,200], a2=60, n=5 );
    pts = hash( chbone, "pts" );
    
    //pts = [[-2.26605, 2.27385, -1.25736], [-1.89564, 0.533304, -1.83458], [-0.83294, 0.268142, -3.39299], [-1.58173, 0.517864, -4.8928], [-3.31782, -1.63569, -5.97169]];
    echo( pts = pts );
    MarkPts( pts, "ch" );   
    
    bds = getbonedata( pts );
    //echo( len=len(bds), nas = arrprn(bds,3,nl=true) );
    
    for( i = range(pts) )
        echo( i=i, dist_i= dist( [ pts[i], pts[i+1]] ));
  
    //------------------------------------------------------    
    //    Test for individual node
    //    
    //    i=3;
    //    
    //    bd = bds[i];
    //    function bd(k)=hash(bd,k);
    //    echo( i=i, bd = bd );
    //    refpl = bd("refpl"); 
    //    Plane( refpl );
    //    //MarkPts( refpl, "r=0.2;plane;trp=0.2");
    //    pt = anglePt( refpl, a=bd("a"), a2=bd("a2"), len=bd("len") );
    //    //pt = anglePt( pl, a=aad[0], a2=0, len=aad[2] );
    //    
    //    color( COLORS[i+1], 0.3 )
    //    MarkPt( pt, "r=0.2;trp=0.2");
    //    
    //    J = bd("J"); 
    //    MarkPt( J, "l=J");
    //    
    //    Mark90( [refpl[0],J,pts[i+1]],"ch" );
    //    Mark90( [refpl[2],J,pts[i+1]],"ch" );
    //    //echo( ispl=ispl([refpl[0],J,pts[i+1]]));
    //    //echo( ispl=ispl([refpl[0],J,pts[i+1]]));
    //------------------------------------------------------    
    
    //------------------------------------------------------    
    //    Test for all nodes except i=1
    //
    //    seed = hash(bds[0],"seed");
    //    
    //    for( i= [1:len(pts)-2] )
    //    {
    //       bd = bds[i]; 
    //       echo( i=i, bd=bd );
    //       refpl = hash( bd, "refpl");
    //       a = hash(bd, "a");
    //       a2 = hash(bd,"a2");
    //       len = hash(bd,"len");
    //        
    //       pt = anglePt( refpl, a=a,a2=a2,len=len);
    //       echo( i=i, pt = pt );
    //       color( COLORS[i+1], 0.4 )
    //       MarkPt( pt, "r=0.2;trp=0.2"); 
    //    }
    //------------------------------------------------------    
    

    seed = hash(bds[0],"seed");
    
    //------------------------------------------------------    
    //    By giving original seed back, it can make an exact clone
    //
    //    clonePts = trslocPts( pts, seed= seed);
    //    echo(clonePts = clonePts);
    //    MarkPts( clonePts,"ch;r=0.2;trp=0.2" );          
    //    echo(pts = pts );
    //------------------------------------------------------    


    seed2 = trslN(seed,1);
    clonePts2 = trsfbonePts( pts, seed= seed2);
    echo( clonePts2 = clonePts2 );           
    MarkPts( clonePts2,"ch;r=0.2;trp=0.2" );
    
    seed3 = get(pts,[1,0,2]);
    clonePts3 = trsfbonePts( pts, seed= seed3);
    echo( clonePts3 = clonePts3 );           
    MarkPts( clonePts3,"ch;r=0.2;trp=0.2" );
}    
//getbonedata_demo();

module trsfbonePts_demo(){
 
    echom("trsfbonePts_demo");
    
    pqr = pqr();
    
    chbone = chainBoneData( seed=pqr, a=[120,200], a2=60, n=5 );
    pts = hash( chbone, "pts" );
    
    //pts = [[-2.26605, 2.27385, -1.25736], [-1.89564, 0.533304, -1.83458], [-0.83294, 0.268142, -3.39299], [-1.58173, 0.517864, -4.8928], [-3.31782, -1.63569, -5.97169]];
    echo( pts = pts );
    MarkPts( pts, "ch" );   
    
//    bds = getbonedata( pts );
//    //echo( len=len(bds), nas = arrprn(bds,3,nl=true) );
//    
//
//    seed = hash(bds[0],"seed");
    
    //------------------------------------------------------    
    //    By giving original seed back, it can make an exact clone
    //
    //    clonePts = trslocPts( pts, seed= seed);
    //    echo(clonePts = clonePts);
    //    MarkPts( clonePts,"ch;r=0.2;trp=0.2" );          
    //    echo(pts = pts );
    //------------------------------------------------------    
    seed = p012(pts);
    
    seed2 = trslN(seed,1);
    clonePts2 = trsfbonePts( pts, seed= seed2);
    echo( clonePts2 = clonePts2 );           
    MarkPts( clonePts2,"ch;r=0.2;trp=0.2" );
    
    seed3 = get(pts,[1,0,2]);
    clonePts3 = trsfbonePts( pts, seed= seed3);
    echo( clonePts3 = clonePts3 );           
    MarkPts( clonePts3,"ch;r=0.2;trp=0.2" );
    
}    
//trsfbonePts_demo();


module twistangle_demo()
{	
    echom("twistangle_demo");
    
	pqrs = randPts(4);
    pqrs = hash( chainBoneData( 4, a=[70, 180], seglen=3, a2=[-150,150]), "pts" );
    MarkPts( pqrs, "ch=[r,0.05];l=[text,PQRS,scale,0.02]" );
	P=P(pqrs); Q=Q(pqrs); R=R(pqrs);
	S=pqrs[3];
	
    Jp = projPt( P, [Q,R] );
    Js = projPt( S, [Q,R] );
    MarkPts([Jp,Js], "l=[text,[Jp,Js]scale,0.02]");
    color("red") Mark90( [P,Jp,Q], "ch");
    color("red") Mark90( [S,Js,R], "ch");
    
    P2 = P-Jp+Js;
    MarkPt( P2,"l=[text, P2,scale,0.02]");
    MarkPts( [P2,Js], "ch=[color,green];ball=0" );
    Arrow( [ onlinePt( [P,Jp], ratio=0.5)
           , onlinePt( [P2,Js], ratio=0.5)
           ], "cl=green;r=0.03" );
    
    arclen = min( dist( P2,Js ), dist( Js,S) );
    ArcArrow( [ onlinePt( [Js,P2], arclen)
              , onlinePt( [Js,S], arclen)
              , Js], rad= arclen, opt="r=0.03;cl=blue" );
    
    a = twistangle( pqrs );
    echo( twistangle = _red(a) );
    arc_midpt = anglePt( [P2,Js,S], abs(a/2), len= arclen, Rf=Q );
//    MarkPts( [S,arc_midpt, extPt([Js,arc_midpt])], "ch;r=0.3;trp=0.5" );
//    scale(0.05) Write( str(int(a*10)/10), pqr=[S,arc_midpt, extPt([Js,arc_midpt])]
//    ,valign="center",halign="center") ;
    MarkPt( arc_midpt, ["ball=0","color","blue","label",["scale",0.02, "text",str(int(a*10)/10)]] );
}
//twistangle_demo();





module vlinePt_demo_0()
{_mdoc("vlinePt_demo_0",
"");

	pqr= randPts(3);
	P=P(pqr); Q= Q(pqr); R=R(pqr);


	T = vlinePt(pqr);
	echo("T=",T);
	//Line0( [Q,T] );
	pqt = [P,Q,T];
	Mark90( pqt, ["r",0.05, "color","red"] );
	Plane( pqt, ["mode",3] );
	MarkPts( [P,Q,R,T], ["labels","PQRT"] );
	Chain(pqt, ["r",0.05, "transp",1,"color","red"]); 
	Chain(pqr, ["r",0.15, "transp",0.5]); 
	
}
//vlinePt_demo_0();
