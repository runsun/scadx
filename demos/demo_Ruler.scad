include <../scadx.scad>

module demo_Ruler()
{
    echom("demo_Ruler");
    
    Ruler();
    Ruler([-3,4], Majortick=["scalelen",2]   
         , site=mvPts( ORIGIN_SITE, [0,0.8,0])
         , color="red");
         
    Ruler([2,6], Minortick=false   
         , site=mvPts( ORIGIN_SITE, [0,1.6,0])
         , color="green"); 
                 
    Ruler( site= [[2,2,1],[0,1,1],[0,1,3]]
         , color="purple");         
   
}
//demo_Ruler();

module demo_Ruler_scale()
{
    echom("demo_Ruler_scale");
    
    Ruler(color="red");
    Ruler(scale=0.5   
         , site=mvPts( ORIGIN_SITE, [0,0.8,0])
         , color="green");
         
    Ruler(scale=1   
         , site=mvPts( ORIGIN_SITE, [0,1.6,0])
         , color="blue");
         
    Ruler(scale=2   
         , site=mvPts( ORIGIN_SITE, [0,2.4,0])
         , color="purple");
         
      
//    pqr=pqr(0.5);
//    Ruler(site=pqr);
//    
//    Ruler(site=transN(pqr,0.5), r=LINE_R/5, Minortick=false, color="red");
//    Ruler(site=transN(pqr,1), Majortick=["scalelen",2], r=LINE_R/5, color="green");
//    Ruler(site=transN(pqr,1.5), Minortick=["scalelen",0.1], r=LINE_R/5, color="purple");
    
    //MarkPts(pqr, Line=["r",0.2, "color",["red",0.1]], Label=false);
}
//demo_Ruler_scale();


module demo_Ruler_2()
{
    echom("demo_Ruler_2");
    pqr = [[0.23, -0.22, -0.47], [-0.83, -0.96, -0.86], [-0.39, 0.47, 0.04]];
     
    echo(pqr=pqr);
    Ruler(site=pqr);
    Ruler(site=transN(pqr,0.75), r=LINE_R/5, Minortick=false, color="red");
    Ruler(site=transN(pqr,1.5), negative=true, Majortick=["scalelen",2], r=LINE_R/5, color="green");
    
    MarkPts(pqr, Line=["r",0.05, "color",["red",0.1]], Label=false);
}
demo_Ruler_2();
