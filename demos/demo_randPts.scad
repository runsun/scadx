include <../scadx.scad>



module demo_randInPlanePts()
{
  echom("demo_randInPlanePts");
  
  //pqr = randPts(3);
  _pqr=   [[1.58, -2.37, -2.36], [2.61, -1.01, -1.15], [0.15, -2.24, -0.4]];
  pqr = [for(p=_pqr) p- (_pqr[2]+_pqr[0])/2];
  echo(pqr=pqr);
  MarkPts(pqr, Line=["closed",1]);
  pts = randInPlanePts(pqr, 100);
  //echo(pts=pts);
  MarkPts(pts, Line=false, Label=false, Ball=["dec_r",0]);
  
//  line0=  [[-1.35, -1.46, 2.31], [-0.01, -0.15, -0.35]]; 
//  echo(line0=line0);
//  MarkPts(line0);
//  
//  line= [for(p=line0)p+[0,.5,0.1]];
//  pts = randPtsOnline(line);
//  echo(len_pts= len(pts));
//  Red(){
//   MarkPts(line, Label=false, color="red", r=0.005);
//   MarkPts(pts, Label=true, Line=false);
//  }
//  
//  line2= [for(p=line)p+[0,.5,0.1]];
//  pts2 = randPtsOnline(line2, keep_pts=0);
//  echo(len_pts= len(pts));
//  Blue(){
//   MarkPts(line2, Ball=false, Label=false, color="red", r=0.005);
//   MarkPts(pts2, Label=true, Line=false);
//  }
  
}
//demo_randInPlanePts();

module demo_randPtsOnline()
{
  echom("demo_randPtsOnline");
  
  line0=  [[-1.35, -1.46, 2.31], [-0.01, -0.15, -0.35]]; 
  echo(line0=line0);
  MarkPts(line0);
  
  line= [for(p=line0)p+[0,.5,0.1]];
  pts = randPtsOnline(line);
  echo(len_pts= len(pts));
  Red(){
   MarkPts(line, Label=false, color="red", r=0.005);
   MarkPts(pts, Label=true, Line=false);
  }
  
  line2= [for(p=line)p+[0,.5,0.1]];
  pts2 = randPtsOnline(line2, keep_pts=0);
  echo(len_pts= len(pts));
  Blue(){
   MarkPts(line2, Ball=false, Label=false, color="red", r=0.005);
   MarkPts(pts2, Label=true, Line=false);
  }
  
}
//demo_randPtsOnline();

module demo_randPtsOnline_polyline()
{
  echom("demo_randPtsOnline_polyline");
  
  line0=  [[1, 2.18, .5], [1.58, .87, -0.94], [-2.06, -0.02, -0.09]
          , [-2, .02, 2.56]];
  echo(line0=line0);
  MarkPts(line0);
  
  count=10;
  d= [0,1.5,1];
  
  line= [for(p=line0)p+d];
  pts = randPtsOnline(line, count=count);
  echo(len_pts= len(pts));
  Red(){
   //MarkPts(line, Label=false, color="red", r=0.005);
   MarkPts(pts, Label=true, Line=["r",0.005]);
   MarkPt(pts[3], "    ... count= 10, keep_pts= 1");
  }
  
  line2= [for(p=line)p+d];
  pts2 = randPtsOnline(line2, count=count, keep_pts=0);
  echo(len_pts2= len(pts2));
  Blue(){
   //MarkPts(line2, Ball=false, Label=false, color="red", r=0.005);
   MarkPts(pts2, Label=true, Line=["r",0.005]);
   MarkPt(pts2[0], "    ... count= 10, keep_pts= 0");
  }
  
  Gray(.2)
  for( li=range(3))
  {
     Line( [line0[0], line[0], line2[0]], r=0.005 );
     Line( [line0[1], line[1], line2[1]], r=0.005 );
     Line( [line0[2], line[2], line2[2]], r=0.005 );
     Line( [line0[3], line[3], line2[3]], r=0.005 );
  }
  }
//demo_randPtsOnline_polyline();

module demo_randPtsOnline_polyline_closed()
{
  echom("demo_randPtsOnline_polyline_closed");
  
  line0=  [[1, 2.18, .87], [1.58, .87, -0.94], [-2.06, -0.02, -0.09]
          , [-3, 1.02, 2.56]];
  echo(line0=line0);
  MarkPts(line0, Line=["closed",true]);
  
  count=12;
  d= [0,1.5,0];
  
  line= [for(p=line0)p+d];
  pts = randPtsOnline(line, count=count, closed=true);
  echo(len_pts= len(pts));
  Red(){
   Line(line, Label=false, color="red", r=0.005, closed=true);
   MarkPts(pts, Label=true, Line=false);
   MarkPt(pts[3], _s("    ... count= {_}, keep_pts= 1, closed=true", count));
  }
  
  line2= [for(p=line)p+d];
  pts2 = randPtsOnline(line2, count=count, closed=true, keep_pts=0);
  echo(len_pts2= len(pts2));
  Blue(){
   Line(line2, Ball=false, Label=false, color="red", r=0.005, closed=true);
   MarkPts(pts2, Label=true, Line=["r",0.005, "closed", true]);
   MarkPt(pts2[0], _s("    ... count= {_}, keep_pts= 0, closed=true", count));
  }
  
  Gray(.2)
  for( li=range(3))
  {
     Line( [line0[0], line[0], line2[0]], r=0.005 );
     Line( [line0[1], line[1], line2[1]], r=0.005 );
     Line( [line0[2], line[2], line2[2]], r=0.005 );
     Line( [line0[3], line[3], line2[3]], r=0.005 );
  }
  }
//demo_randPtsOnline_polyline_closed();

module demo_pqr90()
{
  echom("demo_pqr90");
  
  pqr = pqr90();
  MarkPts(pqr, "PQR");
  _red( str("angle(pqr)= ", angle(pqr)) );
  Mark90(pqr);
}
//demo_pqr90();


module demo_randPtsAlongLine()
{
  echom("demo_randPtsAlongLine");
  _red("demo arguments: <b>count, d, fixed_ends</b>");
  _red("<b>fixed_ends</b>: default 1. = false if <b>closed</b>=true");
  
  line0=  [[2, 1.18, -1.87], [1.58, .87, -0.94], [-2.06, -0.02, -0.09]
          , [-3, -0.5, 2.56]];
  echo(line0=line0);
  MarkPts(line0);
  
  count=50;
  dp= [0,1.5,1];
  
  line= [for(p=line0)p+dp/2];
  pts = randPtsAlongLine(line, count=count);
  _red( str( "len_pts=", len(pts)));
  //_red( str( "pts=", pts));
  Red(){
   MarkPts(pts, Ball=false, Label=false);
   MarkPt(pts[0], Label= _s(" count={_}, d=0.2 (default)", count));
   MarkPt(last(pts), Label= false);
  }
  
  line2= [for(p=line)p+dp*.75];
  pts2 = randPtsAlongLine(line2, count=count, d=.5);
  _green( str( "len_pts2=", len(pts2)));
  //_green( str( "pts2=", pts2));
  Green(){
   MarkPts(pts2, Ball=false, Label=false);
   MarkPt(pts2[0], Label= _s(" count={_}, d=0.5", count));
   MarkPt(last(pts2), Label= false);
  }
  
  line3= [for(p=line2)p+dp];
  pts3 = randPtsAlongLine(line3, count=count, d=1, fixed_ends=0);
  _blue( str( "len_pts3=", len(pts3)));
  //_blue( str( "pts3=", pts3));
  Blue(){
   MarkPts(pts3, Ball=false, Label=false);
   MarkPt(pts3[0], Label= _s(" count={_}, d=1, fixed_ends=0", count));
   MarkPt(last(pts3), Label= false);
  }

  line4= [for(p=line3)p+dp];
  pts4 = randPtsAlongLine(line4, count=150, d=.2);
  _purple( str( "len_pts4=", len(pts4)));
  Purple(){
   for(p=pts4) MarkPt(p, Label=false, Line=false);
   MarkPt(pts4[0], Label= _s(" count={_}, d=0.2", 150));
  }
//
  
  Gray(.2)
  for( li=range(3))
  {
     Line( [line0[0], line[0], line2[0], line3[0], line4[0]], r=0.005 );
     Line( [line0[1], line[1], line2[1], line3[1], line4[1]], r=0.005 );
     Line( [line0[2], line[2], line2[2], line3[2], line4[2]], r=0.005 );
     Line( [line0[3], line[3], line2[3], line3[3], line4[3]], r=0.005 );
  }
}
//demo_randPtsAlongLine();

module demo_randPtsAlongLine_closed()
{
  echom("demo_randPtsAlongLine_closed");
  _red("demo arguments: <b>closed, keep_pts</b>");
  
  line0=  [[2, 1.18, 0.37], [1.58, .87, -0.94], [-2.06, -0.02, -0.09]
          , [-3, -0.5, 2.56]];
  echo(line0=line0);
  MarkPts(line0);
  
  count=10;
  dp= [0,1.5,1];
  
  line= [for(p=line0)p+dp/2];
  pts = randPtsAlongLine(line, count=count, closed=true);
  //echo(pts=pts);
  _red( str( "len_pts=", len(pts)));
  Red(){
   MarkPts(pts, Line=["closed",true]); 
   MarkPt(pts[1], Label="   ... keep_pts=1 (default)");
  }
  
  line1= [for(p=line)p+dp];
  pts1 = randPtsAlongLine(line1, count=count, keep_pts=false, closed=true);
  //echo(pts=pts);
  _orange( str( "len_pts1=", len(pts1)));
  Orange(){
   MarkPts(pts1, Line=["closed",true]); 
   MarkPt(pts1[0], Label= "   ... keep_pts=false");
  }
  
  line2= [for(p=line1)p+dp*1.2];
  pts2 = randPtsAlongLine(line2, count=count);
  _green( str( "len_pts2=", len(pts2)));
  Green(){
   MarkPts(pts2, Line=["closed",false]); 
   MarkPt(pts2[0], Label= "  ... closed=false(default), keep_pts=1 (default)");
  }
  
  line3= [for(p=line2)p+dp*1];
  pts3 = randPtsAlongLine(line3, count=count, keep_pts=false);
  _blue( str( "len_pts3=", len(pts3)));
  Blue(){
   MarkPts(pts3);
   MarkPt(pts3[0], Label= "  ... closed=false(default), keep_pts=false");
  }

  Gray(.2)
  for( li=range(3))
  {
     Line( [line0[0], line[0], line1[0] ], r=0.005 );
     Line( [line0[1], line[1], line1[1] ], r=0.005 );
     Line( [line0[2], line[2], line1[2] ], r=0.005 );
     Line( [line0[3], line[3], line1[3] ], r=0.005 );
  }  
}
//demo_randPtsAlongLine_closed();
