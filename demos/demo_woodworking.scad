include <../scadx.scad>
include <../woodworking/scadx_woodworking.scad>

ISLOG=0;
ISCUTLIST=0;
//demo_littleBench();
//demo_littleBench_label();
demo_littleBench_draw();
//demo_littleBench_frame();
//demo_littleBench_markpts();
//demo_littleBench_dims();
//demo_littleBench_cutlist();
//demo_littleBench_cutlist_2();
//Box_24x18x11_187q();

   
module demo_littleBench()
{ 
  echom("demo_littleBench()"); 
  LittleBench( L=10
              //Label=true
            // , drawopts=[ "Top", [ "isdraw",0, "Frame",false]
            //            ]
             );
}

module demo_littleBench_label()
{ 
  echom("demo_littleBench_label()"); 
  
  LittleBench( L=10, leg_offset=0
             , Label=1 ); //<=== global label 

  translate([0, 14,0])
  LittleBench( L=10, leg_offset=0
             , Label=["color","red"] ); //<=== global label 

  translate([12,0,0])
  LittleBench( L=10, leg_offset=0
             , Label=1
             , drawopts= [ "Top"
                         , ["Label",0] ///<=== dis-label ALL copies of Top
                         ]
                         );
  
  translate([24,0,0])
  LittleBench( L=10, leg_offset=0
             , drawopts= [ "Top"
                         , ["Label",1 ] /// <=== label ALL copies of Top
                         ] 
             );
             
  translate([36,0,0])
  LittleBench( L=10, leg_offset=0
             , drawopts= [ "Leg"
                         , ["Label",[ 0, []  //<=== Label copy-0 w/ default            
                                    , 1, ["color","red"] //<=== Label copy-1 w red
                                    ] 
                           ]
                         ] 
             );
}

module demo_littleBench_draw()
{ 
  echom("demo_littleBench_draw()"); 
  
  Blue() translate([0,11,11]) rotate(a = 90, v = X)
  {
  Text("isdraw=0" );
  Text("isdraw=0, Leg:['Label',1,'isdraw',1]", actions=["x",8] );
  }
  LittleBench( L=10, leg_offset=0
             , isdraw=0 
             //, Label=1
             ); //<=== dis-draw all  

  translate([12,0,0])
  LittleBench( L=10, leg_offset=0
             , isdraw=0
             , drawopts= [ "Leg"
                         , ["Label",1, "isdraw",1] ///<=== Draw legs only
                         ]
                         );
  
  translate([24,0,0])
  LittleBench( L=10, leg_offset=0
             , isdraw=0
             , drawopts= [ "Leg"
                         , ["Label",[0,[],1,[]]
                           ,"isdraw",[0,1] ] /// <=== draw leg copy-0 and -1
                         ] 
             );
             
  translate([36,0,0])
  LittleBench( L=10, leg_offset=0
             , isdraw=0
             , drawopts= [ "Leg"
                         , ["Label",[2,[],3,[]]
                           ,"isdraw",[2,3] ] /// <=== draw leg copy-0 and -1
                         , "L_bar"
                         , ["Label",[1,[],3,[]]
                           ,"isdraw",[1,3] ] /// <=== draw leg copy-0 and -1
                         
                         ] 
             );
             
//  translate([36,0,0])
//  LittleBench( L=10, leg_offset=0
//             , drawopts= [ "Leg"
//                         , ["Label",[ 0, []  //<=== Label copy-0 w/ default            
//                                    , 1, ["color","red"] //<=== Label copy-1 w red
//                                    ] 
//                           ]
//                         ] 
//             );
}
module demo_littleBench_frame()
{ 
  echom("demo_littleBench_frame()"); 
  
  LittleBench( L=10, leg_offset=0
             , Frame=true
             , isdraw=0
             //, Label=1
             ); //<=== dis-draw all 
           
  translate([0,15,0])            
  LittleBench( L=10, leg_offset=0
             , Frame=0.02 
             , isdraw=0
             //, Label=1
             ); //<=== dis-draw all  

  translate([12,0,0])
  LittleBench( L=10, leg_offset=0
             , Frame=0
             , drawopts= [ "Leg"
                         , ["Frame",true] ///<=== Draw legs frame only
                         ]
                         );
  
  translate([24,0,0])
  LittleBench( L=10, leg_offset=0
             , Frame=0
             , drawopts= [ "Leg"
                         , ["Label",[0,[],1,[]]
                           ,"Frame",[0,[],1,[]] ] /// <=== draw leg copy-0 and -1
                         ] 
             );
             
  translate([36,0,0])
  LittleBench( L=10, leg_offset=0
             , isdraw=0
             , Frame=0
             , drawopts= [ "Leg"
                         , ["Label",[0,[],1,[]]
                           ,"Frame",[0,[],1,["color","red"]] ] /// <=== draw leg copy-0 and -1
                         , "L_bar"
                         , ["Label",[0,[],2,[]]
                           ,"Frame",[0,[],2,["r",0.1]] ] /// <=== draw leg copy-0 and -1
                         
                         ] 
             );
             
  translate([48,0,0])
  LittleBench( L=10, leg_offset=0
             , isdraw=0
             //, Frame=1
             , drawopts= [ "Top", ["Frame",["color","red"]] 
                         , "Leg", ["Frame",["r",0.07]]
                         , "L_bar", ["Frame",["r",0.02]] 
                         , "W_bar", ["Frame",["r",0.02, "color","blue"]]
                         ] 
             );
             
}

module demo_littleBench_markpts()
{ 
  echom("demo_littleBench_markpts()"); 
  
  LittleBench( L=10, leg_offset=0
             , drawopts=[ "Top", ["Markpts",1]]
             );   

  translate([12,0,0])
  LittleBench( L=10, leg_offset=0
             , isdraw=0
             , Frame=0
             , drawopts=[ "L_bar", [ "Frame",[0,[]]
                                 , "Markpts",[0,[]] ]
                        , "Leg", [ "Frame",[3,[]]
                                 , "Markpts",[3,[]] ]
                        ]
             );
           
}

module demo_littleBench_dims()
{ 
  echom("demo_littleBench_dims()"); 
  
  LittleBench( L=10, leg_offset=0
             , isdraw=0
             , Frame=1
             , drawopts=[ 
                  "Leg", [ //"Frame",[0,[]]
                         
                          "Markpts",[0,[],3,[]] 
                         , "isdraw",[0,3]
                         , "Label",[0,[],3,[]]
                         , "Dims",[ 0, [[6,7,4]]
                                  , 3, [[[ [10,11.06,10],1,O], ["r",0.08] ]]
                                  ]
                         ]
		, "Top", [ "Markpts",[1,[]] 
                         , "isdraw",[1]
                         , "Label",[1,[]]
                         , "Dims",[ 1, [  
                                         [ 
                                           [6,7,4], ["r",0.05]
                                         ]
                                       , 
                                         [ 
                                           [5,6,7], ["r",0.05, "color","red"]
                                         ]
                                       ] 
                                  ]
                         ]
                ]
             );
             
//  
//  translate([24,0,0])
//  LittleBench( L=10, leg_offset=0
//             , Frame=0
//             , drawopts= [ "Leg"
//                         , ["Label",[0,[],1,[]]
//                           ,"Frame",[0,[],1,[]] ] /// <=== draw leg copy-0 and -1
//                         ] 
//             );
//             
//  translate([36,0,0])
//  LittleBench( L=10, leg_offset=0
//             , isdraw=0
//             , Frame=0
//             , drawopts= [ "Leg"
//                         , ["Label",[0,[],1,[]]
//                           ,"Frame",[0,[],1,["color","red"]] ] /// <=== draw leg copy-0 and -1
//                         , "L_bar"
//                         , ["Label",[0,[],2,[]]
//                           ,"Frame",[0,[],2,["r",0.1]] ] /// <=== draw leg copy-0 and -1
//                         
//                         ] 
//             );
//             
//  translate([48,0,0])
//  LittleBench( L=10, leg_offset=0
//             , isdraw=0
//             //, Frame=1
//             , drawopts= [ "Top", ["Frame",["color","red"]] 
//                         , "Leg", ["Frame",["r",0.07]]
//                         , "L_bar", ["Frame",["r",0.02]] 
//                         , "W_bar", ["Frame",["r",0.02, "color","blue"]]
//                         ] 
//             );
             
}

module demo_littleBench_cutlist()
{ 
  echom("demo_littleBench_cutlist()"); 
  
  LittleBench( L=10, leg_offset=0
             , isCutlist=1
             );
}

module demo_littleBench_cutlist_2()
{ 
  echom("demo_littleBench_cutlist_2()"); 
  
  LittleBench( L=10, leg_offset=0
             , isCutlist=1
             , avail_mats = ["B1x2x8", [20,30] ]
  );
}


   
module LittleBench( // Add support bars
   
     isdraw=1
   , Frame=true  
   , Label=0
   
   , L = 14
   , W = 10
   , H = 10
   , leg_offset=1
   
   , offset=0    // distance to disassemble parts to show each part
   , isCutlist=ISCUTLIST // =1: Draw cutlist instead of product
   
   , avail_mats  // = ["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
                
   , cutlist_part_spacer=1 // the space seperates parts. This allows for considerations
                           // for saw-width, bad spot in lumber, etc. It's better to 
                           // print out cutlists of different part_spacer values
                              
   , drawopts // Overwrite the setting of global isdraw and Frame
      //=[    
         //"Bottom", ["isdraw",[2], "Frame",1, "Label",1]
       //, "Vbar", ["isdraw", [1,5]
                //, "Frame",1, "Label",1, "Markpts",[6,[]]
                //, "dims",[6, [[[0,1,5],["r",0.05]]]  ] 
                //]
       // "Hbar", ["isdraw",1, "Frame",0, "Label",1]
      //, "Side_L", ["isdraw",0, "Markpts",0
                  //]
      //, "Side_W", ["isdraw",0, "Frame", 1, "Markpts",[2,[], 4,[]]
                  //]
       //]
              
   )
{
  echom("LittleBench()");
  
  ////=====================================================
  //// Check WOODS definitions
  //echo( WOODS= _fmth( WOODS ) );
  
  ////=====================================================
  //// Define Units
  
  pktsize = h(WOODS, "cedPkt_1x6_Lb"); 

  TH = pktsize[0];
  W = pktsize[1]*2;
  
  //leg_offset = 1;
  
//  L = 24;
//  W = pktsize[1]*3+TH*2; //12;
//  H = pktsize[1]*2;
  title= _s( "LittleBench ({_}x{_}x{_})",[L,W,H]); 
  
  BottomRaise = LEN2*2; // leave a space so the bottom is not
                   // in direct concact w/ the ground 
  
  
  //os = offset;
  
  ////=====================================================
  //// Echo Title to console 
  echo(_div( _b(title) , s="font-size:24px" ));
      
  
  ////=====================================================
  //// Define parts. 
  _parts=  
  [
  
    "Leg"
      , [ "mat", "B2x2x8" 
        , "len", H-TH
        , "color","gold"
        , "count", 4
        ]
        
  , "L_bar"
      , [ "mat", "B1x2x8" 
        , "len", L-leg_offset*2-LEN2*2
        , "color","teal"
        , "count", 4
        ]
        
  , "W_bar"
      , [ "mat", "B1x2x8" 
        , "len", W-leg_offset*2-LEN2*2
        , "color",["darkkhaki",.8]
        , "count", 4
        ]
        
  , "Top"  
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", L
        , "color",["Olive",0.9]
        , "count", 2
        ]
        
  ];
  
  parts= processParts(_parts, isdraw, Frame, Label, drawopts);
  ////=====================================================
  //// Main 
  if(isCutlist) 
   DrawCutlist( title= title //_s( "Box ( L{_}in x W{_}in x H{_}in )", [L,W,H])
                , parts=parts//2                 
                , avail_mats = avail_mats //["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
                , mat_spacer=1
                , part_spacer=cutlist_part_spacer 
                , debug= 0
                );                  
  else {
    DrawParts( parts=parts );
  }
    
  ////=====================================================
  //// Define modules to draw parts
   module DrawParts( parts ) 
  {
    //echom("DrawParts()");
    //echo(log_b("DrawParts()"), drawopts=drawopts);
    log_dps_b();
    log_dps(["parts", parts]);
    log_dps( ["isdraw", isdraw, "Frame", Frame, "Label", Label
             , "drawopts", drawopts] );         
   
    Legs();
    Bars();
    Top();
       
   module Legs()
   {
      act= [ "roty", -90, "rotz", 90
           , "t", [LEN2+leg_offset, LEN2+leg_offset,0]
           ];      
      sas= spreadActions( objlen=[LEN2,LEN2]
                        , spreadlen=[L-leg_offset*2, W-leg_offset*2]
                        , n=[2,2]);      
      for(i = range(sas)) 
      { 
        DrawPart( i=i, partname="Leg", parts=parts
                , actions=concat(act,sas[i]));
      }   
   }
   module Bars()
   {   
      //==================================== bars 
      
      act2= ["rotx",90, "t",[leg_offset+LEN2, leg_offset+ LEN2, 1.5] ];
      sas2= spreadActions( objlen=[ LEN1 , LEN2]
                        , spreadlen=[ W- leg_offset*2-LEN1*2, H-TH-1.5]
                        , dir = "yz"            
                        , n=[2,2]
                        );      
      for(i = range(sas2)) 
      { 
        DrawPart( i=i, partname="L_bar", parts=parts
                , actions=concat(act2,sas2[i]));
      }   
      
      //====================================  
      act3= ["rotz",90,"roty",90
            , "t", [ leg_offset
                   , leg_offset+LEN2
                   , 1.5+LEN2]
            ];

      sas3= spreadActions( objlen=[ LEN1 , LEN2]
                        , spreadlen=[ L- leg_offset*2, H-TH-1.5-LEN2]
                        , dir = "xz"            
                        , n=[2,2]
                        );      
      for(i = range(sas3)) 
      { 
        DrawPart( i=i, partname="W_bar", parts=parts
                , actions=concat(act3,sas3[i]));
      }  
      
                  
//       DrawPart( i=1, partname="W_bar", parts=parts
//                , actions= act3
//                );
//      
//       DrawPart( i=2, partname="W_bar", parts=parts
//                , actions= concat(act3,["x", L-LEN2*2-leg_offset*2+LEN1])
//                );
   }
   
   module Top()
   {
      DrawPart( i=0, partname="Top", parts=parts
              , actions=[ "z",H-TH] 
              );
      DrawPart( i=1, partname="Top", parts=parts
              , actions=[ "t",[0,pktsize[1],H-TH]] 
              );
   }
   
   
    log_dps_e();
    
  } //<= DrawParts()

 
}


module Box_24x18x11_187q( // Add support bars
   
     isdraw=1
   , Frame=1  
   , Label //= ["color","navy"]
   
   , L = 24
   , W = 18
   , H = 11
   , nVBar = 4
   , nHBar = 5
   
   , offset=0    // distance to disassemble parts to show each part
   , isCutlist=0 // =1: Draw cutlist instead of product
   
   , avail_mats  // = ["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
                
   , cutlist_part_spacer=1 // the space seperates parts. This allows for considerations
                           // for saw-width, bad spot in lumber, etc. It's better to 
                           // print out cutlists of different part_spacer values
                              
   , drawopts // Overwrite the setting of global isdraw and Frame
      =[    
//         "Bottom", ["isdraw",[2], "Frame",1, "Label",1]
//       , "Vbar", ["isdraw", [1,5]
//                , "Frame",1, "Label",1, "Markpts",[6,[]]
//                , "dims",[6, [[[0,1,5],["r",0.05]]]  ] 
//                ]
//       // "Hbar", ["isdraw",1, "Frame",0, "Label",1]
//      , "Side_L", ["isdraw",0, "Markpts",0
//                  ]
//      , "Side_W", ["isdraw",0, "Frame", 1, "Markpts",[2,[], 4,[]]
//                  ]
       ]
              
   )
{
  echom("Box_24x18x11_187q()");
  
  ////=====================================================
  //// Check WOODS definitions
  echo( WOODS= _fmth( WOODS ) );
  
  ////=====================================================
  //// Define Units
  
  pktsize = h(WOODS, "cedPkt_1x6_Lb"); 

  TH = pktsize[0];
  
//  L = 24;
//  W = pktsize[1]*3+TH*2; //12;
//  H = pktsize[1]*2;
  title= _s( "Box_24x18x11 ({_}x{_}x{_})",[L,W,H]); 
  
  BottomRaise = LEN2*2; // leave a space so the bottom is not
                   // in direct concact w/ the ground 
  
  
  //os = offset;
  
  ////=====================================================
  //// Echo Title to console 
  echo(_div( _b(title) , s="font-size:24px" ));
      
  
  ////=====================================================
  //// Define parts. 
  _parts=  
  [
  
    "Side_L"
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", L
        , "color",["gold",.8]
        , "count", 4
        ]
    
  , "Side_W"
  
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", pktsize[1]*3 //W-TH*2
        , "color",["khaki",.8]
        , "count", 4
        ]    
        
  , "Bottom"
  
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", L-TH*2
        , "color",["olive",0.9]
        , "count", 2
        ]
        
  // enhanced w/ support bars:          
  
  , "Vbar"
  
      , [ "mat", "B1x2x8" 
        , "len", H-TH-BottomRaise
        , "color","teal"
        , "count", 4
        ]
  , "Hbar"
  
      , [ "mat", "B1x2x8" 
        , "len", W-TH*2
        , "color","teal"
        , "count", 3
        ]
  
  ];
  
  parts= processParts(_parts, isdraw, Frame, Label, drawopts);
  ////=====================================================
  //// Main 
  if(isCutlist) 
   DrawCutlist( title= title //_s( "Box ( L{_}in x W{_}in x H{_}in )", [L,W,H])
                , parts=parts//2                 
                , avail_mats = avail_mats //["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
                , mat_spacer=1
                , part_spacer=cutlist_part_spacer 
                , debug= 0
                );                  
  else {
    DrawParts( parts=parts );
  }
    
  ////=====================================================
  //// Define modules to draw parts
   module DrawParts( parts ) 
  {
    //echom("DrawParts()");
    //echo(log_b("DrawParts()"), drawopts=drawopts);
    log_dps_b();
    log_dps(["parts", parts]);
    log_dps( ["isdraw", isdraw, "Frame", Frame, "Label", Label
             , "drawopts", drawopts] );         
   
   VBars();
   HBar();
   Side_L();
   Side_W();
   Bottom();
   
   module Side_L()
   {
      DrawPart( i=1, partname="Side_L", parts=parts
              , actions=[ "rotx", 90, "y", TH] );
      DrawPart( i=2, partname="Side_L", parts= parts //updatekey(parts, partnamt
                                          //, "dims", [ [1] 
              , actions=[ "rotx", 90, "y", TH, "z", pktsize[1]] );
   
      DrawPart( i=3, partname="Side_L"
               , parts= parts
              , actions=[ "rotx", 90, "y", W ] );
      DrawPart( i=4, partname="Side_L"
              , parts= updatekey( parts
                                 , "Side_L"
                                 , ["dims"
                                   , [ [[2,3,7],["r",.075]]
                                     , [[3,[L,W,0],2],["r",.075]]
                                     , [[[0,0,H],2,3],["r",.075]]
                                     ]
                                   ]  
                                 )
              , actions=[ "rotx", 90, "y", W , "z", pktsize[1]] );
   }
   
   module Side_W()
   {
      DrawPart( i=1, partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH] 
              );
      DrawPart( i=2, partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "z", pktsize[1]] 
              );
      DrawPart( i=3, partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "x", L-TH] 
              );
      DrawPart( i=4, partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "z", pktsize[1], "x", L-TH] 
              );
   }
   
   module Bottom()
   {
      DrawPart( i=1, partname="Bottom", parts=parts
              , actions=[ "t", [TH,TH,BottomRaise] ] 
              );
      DrawPart( i=2, partname="Bottom", parts=parts
              , actions=[ "t", [TH,TH+pktsize[1],BottomRaise] ] 
              );
      DrawPart( i=3, partname="Bottom", parts=parts
              , actions=[ "t", [TH,TH+pktsize[1]*2,BottomRaise] ] 
              );
   }
   
   module VBars()
   {
      acts= [ "roty", -90 , "rotz", 90 
            , "t", [TH+LEN2,TH+LEN1,BottomRaise+TH] 
            ];
      sas = spreadActions( objlen=LEN2, spreadlen=L-TH*2,n=nVBar);
      
      for(i = range(sas)) 
      { 
        act= sas[i];
        DrawPart( i=i+1, partname="Vbar", parts=parts
                , actions=concat(acts,sas[i]));
        DrawPart( i=i+nVBar, partname="Vbar", parts=parts
                , actions=concat(acts,["y",W-LEN1-TH*2], sas[i]));
      }
      
   }
   module HBar()
   {
      acts=[ "rotx", 90, "rotz",90, "t", [TH,TH, BottomRaise-LEN2] ];
      sas = spreadActions( objlen=LEN1, spreadlen=L-TH*2,n=nHBar);
      
      for(i = range(sas)) 
      { 
        act= sas[i];
        DrawPart( partname="Hbar", parts=parts
                , actions=concat(acts,sas[i]));
      }   
   }
   
   
    log_dps_e();
    
  } //<= DrawParts()

 
}
