include <../scadx.scad>

module demo_vecOp()
{  
   /*
      vecOp( refPts, indices )
      
      Generate a pt by vectoring based on points in refPts(yellow line)
   */   
   echom("demo_vecOp()");
   
   /* pts below is the refPts */
   pts = [[0.61, -0.56, 0.74], [-0.17, -0.38, 0.02], [0.18, -0.67, -0.59]];
   MarkPts( pts, Ball=false );
 
   /* Start w/ pts[2], add vector pts[1]=>pts[0] */ 
   P= vecOp( refPts= pts, indices=[2,[1,0]] ); 
   MarkPt(P, "P", color="red");
   
   Q= vecOp( refPts= pts, indices=[0,[1,2],[1,2]] );
   MarkPt(Q, "Q",  color="green");
   
   R= vecOp( refPts= pts, indices=[ORIGIN, [1,2],[1,2]] );
   MarkPt(R, "R", Grid=false, color="blue");
   
   S= vecOp( refPts= pts, indices=[[1,-1],[0,1]] ); // index could be negative
   MarkPt(S, "S", Grid=false, color="purple");
   
   Line( [ pts[0], Q], color=["green",0.1] );
   Line( [ ORIGIN, R], color=["blue",0.1] );
   Line( [ pts[2], S], color=["purple",0.1] );
   
   echo(P=P);
   echo(Q=Q);
   echo(R=R);
   echo(S=S);
   //--imgds=250,250
   //--imgcs=600,600
   //--camera=[ 0.78, 0.51, -0.90 ][ 59.90, 0.00, 342.30 ],8
   //--axes=0
}
//demo_vecOp();

module demo_vecOp_external_ref()
{
   /*
      Use an external ref pt (A) other than 
      indexing the refPts
   */   
   echom("demo_vecOp_external_ref()");
   pts = [[0.61, -0.56, 0.74], [-0.17, -0.38, 0.02], [0.18, -0.67, -0.59]];
   MarkPts( pts, Ball=false ); 
   
   P= vecOp( refPts= pts, indices=[2,[1,0]] ); 
   MarkPt(P, "P", color="green");
   
   A=[1.6, -0.93, 0.41];             
   MarkPt(A,"A", color="red");
   Q= vecOp( refPts= pts, indices=[2,[1,A]] ); // Use an external ref pt
   MarkPt(Q, "Q", color="red");
   
   Line( [ pts[1], A], color=["red",0.1] );
   Line( [ pts[2], Q], color=["red",0.1] );
   Line( [ pts[2], P], color=["green",0.1] );
   //--imgds=250,250
   //--imgcs=600,600
   //--camera=[ 1.5, 0.8, -0.64 ][ 64.10, 0.00, 333.90 ],11
   //--axes=0
}
//demo_vecOp_external_ref();
