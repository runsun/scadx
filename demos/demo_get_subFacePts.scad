include <../scadx.scad>

module _draw_gridlines(pts,shrink_ratio=0.25)
{
   edges = get2(pts);
   for(ei=range(edges))
   {
      prev= prev( edges, edges[ei]);
      next= next( edges, edges[ei]);
      
      Line([ onlinePt( prev, ratio=1-shrink_ratio)
           , onlinePt( next, ratio=shrink_ratio)
           ]
          , r=0.003, color=["red",0.5]);
   }
}

module demo_get_subFacePts_default()
{
   echom("demo_get_subFacePts_default()");
   pts= [ for(p=squarePts([for(p=ORIGIN_SITE)p*2])) p+randPt(d=0.4) ];
   MarkPts(pts, Line=["closed",true]);
   
   subpts = get_subFacePts(pts);
   MarkPts(subpts, Ball=["r",0.02], Line=["closed",true]);
   
   _draw_gridlines(pts, shrink_ratio=0.25);
}
//demo_get_subFacePts_default();

module demo_get_subFacePts_ratio()
{
   echom("demo_get_subFacePts_ratio()");
   pts= [ for(p=squarePts([for(p=ORIGIN_SITE)p*2])) p+randPt(d=0.4) ];
   Line(pts, closed=true);
   
   subpts = get_subFacePts(pts, shrink_ratio=0.1);
   Line(subpts, closed=true, color="red", r=0.01);
   _draw_gridlines(pts, shrink_ratio=0.1);
   
   subpts2 = get_subFacePts(pts, shrink_ratio=0.2);
   Line(subpts2, closed=true, color="green", r=0.01);
   color("green") _draw_gridlines(pts, shrink_ratio=0.2);
   
   subpts3 = get_subFacePts(pts, shrink_ratio=0.3);
   Line(subpts3, closed=true, color="blue", r=0.01);
   color("blue") _draw_gridlines(pts, shrink_ratio=0.3);
   
}
//demo_get_subFacePts_ratio();

module demo_get_subFacePts_negative_ratio()
{
   echom("demo_get_subFacePts_negative_ratio()");
   pts= [ for(p=squarePts([for(p=ORIGIN_SITE)p])) p+randPt(d=0.4) ];
   Line(pts, closed=true);
   
   subpts = get_subFacePts(pts, shrink_ratio=-0.1);
   Line(subpts, closed=true, color="red", r=0.01);

   subpts2 = get_subFacePts(pts, shrink_ratio=-0.2);
   Line(subpts2, closed=true, color="green", r=0.01);
   
   subpts3 = get_subFacePts(pts, shrink_ratio=-0.3);
   Line(subpts3, closed=true, color="blue", r=0.01);

}
//demo_get_subFacePts_negative_ratio();

module demo_get_subFacePts_border()
{
   echom("demo_get_subFacePts_border()");
   echo(_red("borders=[0]"));
   pts= [ for(p=squarePts(ORIGIN_SITE)) p*2+randPt(d=0.4) ];
   MarkPts(pts, Ball=false, Line=["closed",true]);
   
   subpts = get_subFacePts(pts, border_shrink_ratio=0.25);
   MarkPts(subpts, Ball=false
          , Label=false, Line=["closed",true, "color", "red"]);
   
   subpts2 = get_subFacePts(pts, border_eis=[0], border_shrink_ratio=0.25);
   MarkPts(subpts2, Ball=false, Label=false, Line=["closed",true, "color", "green"]);

   center = sum(pts)/len(pts);
   for(p=pts) Line([p,center], color=["red",0.5],r=0.0015);
}
//demo_get_subFacePts_border();

module demo_get_subFacePts_borders()
{
   echom("demo_get_subFacePts_borders()");
   echo(_red("borders=[0,1]"));
   pts= [ for(p=squarePts(ORIGIN_SITE)) p*2+randPt(d=0.4) ];
   MarkPts(pts, Ball=false, Line=["closed",true]);
   
   subpts = get_subFacePts(pts, border_shrink_ratio=0.25);
   MarkPts(subpts, Ball=false, Label=false, Line=["closed",true, "color", "red"]);
   
   subpts2 = get_subFacePts(pts, border_eis=[0,1], border_shrink_ratio=0.25);
   MarkPts(subpts2, Ball=false, Label=false, Line=["closed",true, "color", "green"]);

   center = sum(pts)/len(pts);
   for(p=pts) Line([p,center], color=["red",0.5],r=0.0015);
}
//demo_get_subFacePts_borders();

module demo_get_subFacePts_border_shrink_ratio_0()
{
   echom("demo_get_subFacePts_border_shrink_ratio_0()");
   echo(_red("borders=[0]"));
   pts= [ for(p=squarePts(ORIGIN_SITE)) p*2+randPt(d=0.4) ];
   MarkPts(pts, Ball=false, Line=["closed",true]);
   
   subpts = get_subFacePts(pts, border_shrink_ratio=0.25);
   MarkPts(subpts, Ball=false, Label=false, Line=["closed",true, "color", "red"]);
   
   subpts2 = get_subFacePts(pts, border_eis=[0], border_shrink_ratio=0);
   MarkPts(subpts2, Ball=false, Label=false, Line=["closed",true, "color", "green"]);

   center = sum(pts)/len(pts);
   for(p=pts) Line([p,center], color=["red",0.5],r=0.0015);
}
//demo_get_subFacePts_border_shrink_ratio_0();

module demo_get_subFacePts_borders_shrink_ratio_0()
{
   echom("demo_get_subFacePts_borders_shrink_ratio_0()");
   echo(_red("borders=[0,1]"));
   pts= [ for(p=squarePts(ORIGIN_SITE)) p*2+randPt(d=0.4) ];
   MarkPts(pts, Ball=false, Line=["closed",true]);
   
   subpts = get_subFacePts(pts, border_shrink_ratio=0.25);
   MarkPts(subpts, Ball=false, Label=false, Line=["closed",true, "color", "red"]);
   
   subpts2 = get_subFacePts(pts, border_eis=[0,1], border_shrink_ratio=0);
   MarkPts(subpts2, Ball=false, Label=false, Line=["closed",true, "color", "green"]);

   center = sum(pts)/len(pts);
   for(p=pts) Line([p,center], color=["red",0.5],r=0.0015);
}
//demo_get_subFacePts_borders_shrink_ratio_0();

module demo_get_subFacePts_border_mesh_no_border()
{
   echom("demo_get_subFacePts_border_mesh_no_border()");
   
   size=4;
   all_pts= meshPts(size, noise=["x",0.1,"y",0.1,"z",0.4]);
   MarkPts(all_pts, Ball=false, Line=false);
   faces = faces(shape="mesh", nx=size,ny=size, isTriangle=0);

   for(fi=range(faces))
   {
     pts = get(all_pts,faces[fi]);
     center = sum(pts)/len(pts);
   
     MarkPt(center, Label=["text",fi,"indent",0, "color","red"], Ball=false); 
     
     subpts = get_subFacePts( pts  );
   
     MarkPts(pts, Label=false
            , Line=["closed",true, "color","black", "r",0.005], Ball=false);
     MarkPts(subpts, Line=["closed",true], Label=false);
   }  
}
//demo_get_subFacePts_border_mesh_no_border();

module demo_get_subFacePts_border_mesh()
{
   echom("demo_get_subFacePts_border_mesh()");
   
   size=4;
   all_pts= meshPts(size, noise=["x",0.1,"y",0.1,"z",0.4]);
   MarkPts(all_pts, Ball=false, Line=false);
   faces = faces(shape="mesh", nx=size,ny=size, isTriangle=0);
   
   arr_borders=[  
        [0,3], [0], [0]
      , [3], [], []
      , [3],[],[] 
      ];
      
   for(fi=range(faces))
   {
     pts = get(all_pts,faces[fi]);
     center = sum(pts)/len(pts);
   
     MarkPt(center, Label=["text",fi,"indent",0, "color","red"], Ball=false); 
     
     subpts = get_subFacePts( pts, border_eis= arr_borders[fi], border_shrink_ratio=.25 );
   
     MarkPts(pts, Label=false
            , Line=["closed",true, "color","black", "r",0.005], Ball=false);
     MarkPts(subpts, Line=["closed",true], Label=false);
   }  
}
//demo_get_subFacePts_border_mesh();

module demo_get_subFacePts_border_mesh_ratio_0()
{
   echom("demo_get_subFacePts_border_mesh_ratio_0()");
   
   size=4;
   all_pts= meshPts(size, noise=["x",0.1,"y",0.1,"z",0.4]);
   MarkPts(all_pts, Ball=false, Line=false);
   faces = faces(shape="mesh", nx=size,ny=size, isTriangle=0);
   
   arr_borders=[  
        [0,3], [0], [0]
      , [3], [], []
      , [3],[],[] 
      ];
      
   for(fi=range(faces))
   {
     pts = get(all_pts,faces[fi]);
     center = sum(pts)/len(pts);
   
     MarkPt(center, Label=["text",fi,"indent",0, "color","red"], Ball=false); 
     
     subpts = get_subFacePts( pts, border_eis= arr_borders[fi], border_shrink_ratio=0 );
   
     MarkPts(pts, Label=false
            , Line=["closed",true, "color","black", "r",0.005], Ball=false);
     MarkPts(subpts, Line=["closed",true], Label=false);
   }  
}
demo_get_subFacePts_border_mesh_ratio_0();




