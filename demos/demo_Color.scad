include <../scadx.scad>

module demo_Color()
{
   echom("demo_Color()");
   Color("red") cube();
   Color(["green",0.7]) translate([2,0,0]) cube();
   
   
   Color( opt=["color", "blue"] ) translate([4,0,0]) cube();
   Color( undef, ["color", "blue"] ) translate([4,2,0]) cube();
   Color( "teal", ["color", "blue"] ) translate([4,4,0]) cube();
   
   
   Color( opt=["color", ["purple",0.5]] ) translate([6,0,0]) cube();
   Color( undef, ["color", ["purple",0.5]] ) translate([6,2,0]) cube();
   Color( "olive", ["color", ["purple",0.5]] ) translate([6,4,0]) cube();

   Color( df=["black",0.5] ) translate([8,0,0]) cube();


}
demo_Color();
