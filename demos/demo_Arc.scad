include <../scadx.scad>

//Arc_demo_1();
//Arc_demo_2_rPrRrs();
LabelPt_test();
module Arc_demo_1()
{  echom("Arc_demo_1");
    
	pqr = randPts(3);
    Arc(pqr, "echopts"); 
	MarkPts(pqr, "pqr");
    LabelPt( Q(pqr), "Default");
        
    pqr2= trslN(pqr,3); 
    Arc(pqr2, "a=90;fn=3;rad=3;r=1;cl=red;echopts");
//    Arc(pqr2, ["a",90, "fn",3, "rad",3,"r",1, "color","red"
//              ,"echopts", true]); 
	MarkPts(pqr2, ["chain",true]);
    //LabelPt( Q(pqr2), "a=90,;fn=3;rad=3;r=1");
//    
}
//Arc_demo_1();

module Arc_demo_2_rPrRrs()
{ 
    echom("Arc_demo_2_rPrRrs");
    _red( "Varying radius" );
    echo( "" );
//    opt=[ "a"  , 120
//        , "rad", 4
//        , "fn" , 12
//        , "nseg", 36
//        ];
    opt= "a=120;rad=4;fn=12;nseg=36";
    
    pqr = randPts(3,d=[-3,2]);
    Arc( pqr, str( opt, ";rP=0.5;rR=0.01")
         //update( opt, ["rP",0.5, "rR",0.01])
       );
	MarkPts(pqr, ["chain",true]);
    LabelPt(Q(pqr), "rP=0.5,rR=0.01,a=60,rad=4" );    
    //echo("Q(pqr)= ", Q(pqr));
    
    rs= [ for(i=range(37)) sin(i*60)/5+0.5 ];
    //echo(rs=rs);
    pqr2= trslN( pqr,3); 
    Arc( pqr2, [opt, "color","red","transp",0.8,"rs",rs]
         //update( opt, ["color","red","transp",0.8,"rs",rs])
       ); 
	MarkPts(pqr2, ["chain",true, "transp", 0.3]);
    LabelPt(Q(pqr2), "rs=[ for(i=range(37)) sin(i*60)/5+0.5 ]" );    
 
}
//Arc_demo_2_rPrRrs();

module Arc_demo_2_rss()
{ 
    echom("Arc_demo_2_rss");
    function opt(h)=
        update( [ "a", 120, "rad",4
                , "fn", 24
                , "nseg", 18
                ], h) ;
                 	
    fn=24;
    rss= [ for(i=range(37)) 
            [for(j= range(fn))
                sin(j*45)/2+0.6
            ] 
        ];
    //echo(rss=rss);
    pqr= pqr();
    Arc(pqr, opt(["rss", rss])); 
	MarkPts(pqr, ["chain",true, "transp", 1]);
    LabelPt(Q(pqr),"Set rss (symmetric rs pattern for each xsec)");
            
    rss2= [ for(i=range(37)) 
            [for(j= range(fn))
                sin(j*60)/2+0.8
            ] 
        ];
    //echo(rss=rss);
    pqr2= trslN( pqr, 3.5); 
    Arc(pqr2, opt(["color","red","rss", rss2])); 
	MarkPts(pqr2, ["chain",true, "transp", 1]); 
    LabelPt(Q(pqr2),"For example, rss=[for(i=nseg)[for(j=fn)...]]");
            
}
//Arc_demo_2_rss();

module Arc_demo_3_rads()
{ 
    echom("Arc_demo_3_rads");
    
    pqr3= randPts(3);
    rads= [ for(i=range(25)) sin(i*60)/10+0.2+ d01(pqr3) ];
    //echo(rads=rads);
    Arc(pqr3, ["a",120, "fn",4, "nseg",24 ,"transp",1
              ,"rads", rads, "r",1
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 0.3]);    
    LabelPt(Q(pqr3),"Set rads (main curve pts)");

    
    pqr4= trslN(pqr3,3); //randPts(3);
    rads2= [ for(i=range(25)) sin(i*60)/10+0.2+ d01(pqr4) ];
    //echo(rads=rads2);
    Arc(pqr4, ["a",120, "fn",6, "rP", 1, "rR",0.01
              , "nseg",24 ,"transp",1
              ,"rads", rads2, "color","red"
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 0.3]);        
    LabelPt(Q(pqr4),"Set rads & rP, rR");
 
}
//Arc_demo_3_rads();

module Arc_demo_4_rads_rs()
{ 
    echom("Arc_demo_4_rads_rs");
    
    pqr3= randPts(3,d=[-2,3]);
    rs= [ for(i=range(25)) sin(i*60)/10+0.5 ];
    rads= [ for(i=range(25)) sin(i*60)/10+1+ d01(pqr3) ];
    echo(rads=rads);
    Arc(pqr3, ["a",180, "fn",12, "nseg",12 ,"transp",0.6
              ,"rads", rads, "rs",rs 
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 0.3]);    
    LabelPt(Q(pqr3),"Set both rs and rads");


    pqr4= trslN(pqr3,3);
    Arc(pqr4, ["a",180, "fn",12, "nseg",12 ,"transp",0.6
              ,"rads", rads, "rs",rs
              ,"hide",true, "frame", true
              ]); 
//  Doesn't know why frame is treated as false below:
//   Arc(pqr4, ["hide;a=180;fn=12;nseg=12","rads", rads
//              , "rs",rs , "frame", ["label",false]
//              ]); 

    
	MarkPts(pqr4, ["chain",true, "transp", 0.3]);    
    LabelPt(Q(pqr4),"hide obj and set frame");
    
    
    pqr5= trslN(pqr4,3); //randPts(3,d=[1,5]);
    rads2= [ for(i=range(13)) rand(4,5) ]; // sin(i*60)/10+0.2+ d01(pqr4) ];
    echo(rads=rads2);
    Arc(pqr5, ["a",180, "fn",6, "rP", 0.8, "rR",0.01
              , "nseg",12 ,"transp",0.8
              ,"rads", rads2, "color","green"
              ]); 
	MarkPts(pqr5, ["chain",true, "transp", 0.3]);        
    LabelPt(Q(pqr5),"Give noise: rads=[ for(i=range(13)) rand(4,5) ]");
    
}
//Arc_demo_4_rads_rs(); 

module Arc_demo_5_rotate()
{ 
    echom("Arc_demo_5_rotate");
    
    pqr3= pqr(); //randPts(3,d=[-4,-1]);
    rads3= [ for(i=range(13)) rand(4,5) ]; 
    Arc(pqr3, ["a",120, "fn",4, "rP", 1, "rR",0.01
              , "nseg",12 ,"transp",0.8
              ,"rads", rads3, "frame",["label",false]
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 0.3]);  
    
    
    uvN = uvN(pqr3)*3; 
    pqr4= [for(p=pqr3)p+uvN]; //randPts(3,d=[-3,0]);
    rads2= [ for(i=range(13)) rand(4,5) ]; // sin(i*60)/10+0.2+ d01(pqr4) ];
    echo(rads=rads2);
    Arc(pqr4, ["a",120, "fn",4, "rP", 1, "rR",0.01
              , "nseg",12 ,"transp",0.8
              ,"rads", rads2, "color","red", "rotate",45
              , "frame",,["label",false]
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 0.3]);
    LabelPt( pqr4[1], "rotate=45");
    
    
    
    pqr1= [for(p=pqr4)p+uvN];
    Arc(pqr1, ["a",45, "fn",6, "nseg",1, "rad",3
              , "r",1, "color","green","transp",0.8]); 
	MarkPts(pqr1, ["chain",true]);
        
    
    
    pqr2= [for(p=pqr1)p+uvN];;
    Arc(pqr2, ["a",45, "fn",6, "nseg",1
              , "r",1,"color","blue"
              ,"transp",0.8, "rotate",30
              , "rad",3
              ]); 
	MarkPts(pqr2, ["chain",true]);
    LabelPt( pqr2[1], "rotate=30");
}

//Arc_demo_5_rotate();

module Arc_demo_6_xsecpts_rotate()
{ 
    echom("Arc_demo_6_xsecpts_rotate");  
	
    xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    nseg=10;
    a=180;
    
    pqr2= randPts(3, d=[-3,0]);
    Arc(pqr2, ["a",a, "nseg",nseg ,"transp",1
              ,"xsecpts",xsecpts, "rotate",30
              , "color","red", "showxsec2d", true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    LabelPt(Q(pqr2),"  rotate=30");

    
    uvN = uvN(pqr2)*6;
    pqr3= [ for(p=pqr2) p+uvN ]; //randPts(3,d=[0,3]);
    Arc(pqr3, ["a",a, "nseg",nseg ,"transp",1
              ,"xsecpts",xsecpts
             // ,"frame",true
              , "markxsec",[0,-2]
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    LabelPt(Q(pqr3),"  no rotate");

    
    pqr4= [ for(p=pqr3) p+uvN ]; 
    Arc(pqr4, ["a",a, "nseg",nseg ,"transp",1
              ,"xsecpts",xsecpts, "rotate",-75
              //,"frame",true
              , "color","green"
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    LabelPt(Q(pqr4),"  rotate=-75");
    
}
//Arc_demo_6_xsecpts_rotate();

module Arc_demo_6_xsecpts_random()
{ 
    echom("Arc_demo_6_xsecpts_random");  
	
    fn=24;
    nseg=30; 
    
    pqr2= randPts(3,d=[3,7]);
    rads2= [ for(i=range(nseg+1)) rand(4,5) ];
    xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    Arc(pqr2, ["a",350, "fn",fn, "nseg",nseg ,"transp",0.9
              ,"xsecpts",xsecpts, "rads",rads2
              ,"frame", ["label",range(nseg),"color","black"]  
              , "showxsec2d",true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
                   
}
//Arc_demo_6_xsecpts_random();

module Arc_demo_7_markxsec()
{ 
    echom("Arc_demo_7_markxsec");  
	
    r=0.8;
    fn=6;
    nseg=10; 
    
    pqr2= randPts(3,d=[1,5]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    Arc(pqr2, ["a",90, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              //,"xsecpts",xsecpts//, "rads",rads2
              ,"frame", true //["label",range(nseg)]
              ,"markxsec", true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    LabelPt( Q(pqr2), "frame=true, markxsec=true");

    
    
    pqr3= [for(p=pqr2)p+uvN];
    Arc(pqr3, ["a",90, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"color","red", "markxsec", [1,-2,-1]
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    LabelPt( Q(pqr3), "markxsec=[1,-2,-1]");
        
}
//Arc_demo_7_markxsec();

module Arc_demo_8_closed()
{
    echom("Arc_demo_8_closed");  
	
    r=0.8;
    fn=4;
    nseg=10; 
    a=270;
    
    pqr2= randPts(3,d=[1,5]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //---------------------------------------------------
    //xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    Arc(pqr2, ["a",a, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              //,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr2), "frame=[\"label\",range(nseg)], markxsec=true");
    
    //---------------------------------------------------
    /*
       When it closes, the total segs will be nseg+1
    */
    
    pqr3= [for(p=pqr2)p+uvN];
    Arc(pqr3, ["a",a, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"color","red", "markxsec", [0,-1]
              ,"closed",true 
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr3), "markxsec=[1,-2,-1]");    

    //---------------------------------------------------
    // We will make it looks smooth like a ring:
    ac = 360/(nseg+1)*nseg;
    pqr4= [for(p=pqr3)p+uvN];
    Arc(pqr4, ["a",ac, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"color","green", "markxsec", [0,-1]
              ,"closed",true 
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr4), "markxsec=[1,-2,-1]");  

    //---------------------------------------------------
    
    pqr5= [for(p=pqr4)p+uvN*2];
    Arc(pqr5, ["a",120, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"color","blue", "markxsec", [0,-1]
              ,"closed",true 
              ]); 
	MarkPts(pqr5, ["chain",true, "transp", 1]);
    LabelPt( Q(pqr5), "Can't close if angle too small (a=120)");      
} 

//Arc_demo_8_closed();

module Arc_demo_8_closed2()
{
    echom("Arc_demo_8_closed2");  
	
    r=0.8;
    fn=4;
    nseg=10; 
    a=270;
    aclosed = 360/(nseg+1)*nseg;
    
    pqr2= randPts(3,d=[-3,-6]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //---------------------------------------------------
    //xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    xsecpts= [[ 2,0],[2,2],[0,2],[0,0]];
    
    Arc(pqr2, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", true //["label",range(nseg)]
              //,"markxsec", true
              ,"closed",true
              ,"echopts",true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr2), "frame=[\"label\",range(nseg)], markxsec=true");

    //---------------------------------------------------
    xsecpts2= [[0,0],[ 2,0],[3,1],[3,2],[2,3]
             ,[ 1.5,4],[2,5],[4,6], [5,6],[ 4,6.6]
             , [3,6.5],[2,6.3],[1,5.5],[0,3.5]
             ];
    
    pqr3= trslN( pqr2, 7); 
    Arc(pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.8, "r",r, "rad",4
              ,"xsecpts",xsecpts2//, "rads",rads2
              //,"frame", true//["label",range(nseg)]
              //,"markxsec", true
              ,"showxsec2d", true
              //,"closed",true
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    
    pqr4= trslN( pqr3, 12);
    Arc(pqr4, ["a",aclosed, "fn",fn, "nseg",nseg,"rad",2, "rad",4
              ,"xsecpts",xsecpts2//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", true
              //,"showxsec2d", true
              ,"closed",true
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    
} 

//Arc_demo_8_closed2();

module Arc_demo_9_rads_ellipse()
{ 
    echom("Arc_demo_9_rads_ellipse");
    /*
       2D Formula of an ellipse around the ORIGIN:
      
       (x/a)^2 + (y/b)^2 = 1    (a,b: r on x,y)
    
       In order to get the angle-dependent distance 
       (between center and the curve bone), we need 
       to find functions: 
    
            x=f(g) and y=f(x)
    
       So, 
    
       (y/b)^2 = 1-(x/a)^2
                  _____________ 
        y/b  =  ./( 1-(x/a)^2 )
                  _____________
        y =  b  ./( 1-(x/a)^2 )   //<==== got one 
                                ______________ 
        So each pt is: [ x,  b./(1-x/a)(1+x/a) ]
        
        The distance from [0,0] to pt is:
              ________________________
        d=  ./ x^2 + b^2(1-x/a)(1+x/a) 
    
        Since for each pt [X,Y], 
 
        x = cos(g)*d
              ________________________
        d=  ./ x^2 + b^2(1-x/a)(1+x/a)  = x/cos(g)
    
        x^2 + b^2(1-x/a)(1+x/a) = x^2/cos(g)^2
        x^2 + b^2(1-(x/a)^2) = x^2/cos(g)^2
        x^2 + b^2- b^2(x/a)^2 = x^2/cos(g)^2
        x^2 + b^2- (bx/a)^2 = x^2/cos(g)^2
        x^2 + b^2- (b/a)^2x^2 = x^2/cos(g)^2
        b^2  = x^2/cos(g)^2 + (b/a)^2x^2 -x^2
        b^2  = (1/cos(g)^2 + (b/a)^2 -1) x^2
        x^2 = b^2 / (1/cos(g)^2 + (b/a)^2 -1) 
                  _________________________  
        x = b / ./(1/cos(g)^2 + (b/a)^2 -1)  //<==== solved 
        
    */
    
    nseg = 47;
    
    pqr3= randPts(3,d=[3,7]);
    a = 4; //dist(p01(pqr3));  // ellipse rad x
    b = a/1.5;  // ellipse rad y
    
    rads= [ for(i=range(nseg+1)) 
              let(g= i*360/nseg
                 ,x= b / sqrt( 1/pow(cos(g),2) + pow(b/a,2)-1 )
                 ,y= b * sqrt( (1-x/a)*(1+x/a) )
                 )
              sqrt( pow(x,2)+pow(y,2))
          ];
    
    echo(rads= arrprn(rads,dep=21) );
    
    xsecpts=[ [0,0], [2,0],[3,1], [1.8,1.2], [0,2.5]];
    // We will make it looks smooth like a ring:
    ac = 360/(nseg+1)*nseg;
    Arc(pqr3, ["a",ac, "fn",4, "nseg",nseg ,"transp",.9
              ,"rads", rads, "xsecpts",xsecpts
              //,"frame", ["label",false]
              ,"markxsec",[0,-1]
              ,"closed", true
              ,"showxsec2d", true
              ]); 
	//MarkPts(pqr3, ["chain",true, "transp", 0.3]);    
    //LabelPt(Q(pqr3),"Set rads (main curve pts)");

    
//    pqr4= trslN(pqr3,3); //randPts(3);
//    rads2= [ for(i=range(25)) sin(i*60)/10+0.2+ d01(pqr4) ];
//    //echo(rads=rads2);
//    Arc(pqr4, ["a",120, "fn",6, "rP", 1, "rR",0.01
//              , "nseg",24 ,"transp",1
//              ,"rads", rads2, "color","red"
//              ]); 
//	MarkPts(pqr4, ["chain",true, "transp", 0.3]);        
//    LabelPt(Q(pqr4),"Set rads & rP, rR");
 
}
//Arc_demo_9_rads_ellipse();

module Arc_demo_10_radpts() // radpts not very successful
{                           // This should be Chain's territory
    echom("Arc_demo_10_radpts");
        
    nseg = 47;
    
    pqr3= randPts(3,d=[3,7]);
    
    xsecpts=[ [0.5,-0.5], [3,0],[3,1], [1.8,1.2], [0,2.5]];
    MarkPts( addz(xsecpts) 
           , ["label",range(xsecpts), "chain",true] );
    radpts = [ for(p=xsecpts) p*3 ];
    echo(radpts=radpts);
    // We will make it looks smooth like a ring:
    ac = 360/(nseg+1)*nseg;
    Arc(pqr3, ["a",ac, "fn",4, "nseg",nseg ,"transp",0.3, "r",0.8
              ,"radpts", radpts//, "xsecpts",xsecpts
              //,"frame" //, ["label",false]
              ,"markxsec",[0,-1]
              //,"closed", true
              ,"showxsec2d", true
              ,"echopts",true
              ]); 
//	MarkPts(pqr3, ["chain",true, "transp", 0.3]); 

}
//Arc_demo_10_radpts();

module Arc_demo_11_sealed()
{
    echom("Arc_demo_11_sealed");  
	
    r=0.8;
    fn=4;
    nseg=4; 
    a=270;
    aclosed = 360/(nseg+1)*nseg;
    
    pqr2= randPts(3,d=[-3,-6]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //---------------------------------------------------
    xsecpts= [[ 3,0],[3,0.3],[2,1],[1.5,2],[1,2]
             /*,[1,0.2], [0.9,-0.1],[0.5,-0.2]*/, [0.5,0]  ,[0,-0.5]
             /* , [0.7,-0.6], [0.9,-0.8]*/,[1,-1],[2,-1],[2,0]];
    
    Arc(pqr2, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1, "r",r, "rad",4
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", range(nseg+1)
              ,"closed",true
              ,"showxsec2d",true
              ,"sealed",[5,6]
                //["label", ["isxyz",true,"scale",0.01]] 
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr2), "frame=[\"label\",range(nseg)], markxsec=true");
    ColorAxes();
    
    
    pqr3= trslN(pqr2,8);
    Arc(pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1, "r",r, "rad",4
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", range(nseg+1)
              ,"closed",true
              ,"showxsec2d",true
              ,"sealed",[6,7]
                //["label", ["isxyz",true,"scale",0.01]] 
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);

    
    pqr4= trslN(pqr3,8);
    Arc(pqr4, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1, "r",r, "rad",4
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", range(nseg+1)
              //,"closed",true
              ,"showxsec2d",true
              //,"sealed",[6,7]
                //["label", ["isxyz",true,"scale",0.01]] 
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    
    //---------------------------------------------------
//    xsecpts2= [[0,0],[ 2,0],[3,1],[3,2],[2,3]
//             ,[ 1.5,4],[2,5],[4,6], [5,6],[ 4,6.6]
//             , [3,6.5],[2,6.3],[1,5.5],[0,3.5]
//             ];
//    
//    //---------------------------------------------------
//    pqr3= trslN( pqr2, 7); 
//    Arc(pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.8, "r",r, "rad",4
//              ,"xsecpts",xsecpts2//, "rads",rads2
//              ,"frame", ["label",range(nseg)]
//              ,"markxsec", true
//              ,"showxsec2d", true
//              //,"closed",true
//              ]); 
//	MarkPts(pqr3, ["chain",true, "transp", 1]);
//    
//    //---------------------------------------------------
//    pqr4= trslN( pqr3, 12);
//    Arc(pqr4, ["a",aclosed, "fn",fn, "nseg",nseg,"rad",2, "rad",4
//              ,"xsecpts",xsecpts2//, "rads",rads2
//              //,"frame", ["label",range(nseg)]
//              //,"markxsec", true
//              //,"showxsec2d", true
//              ,"closed",true
//              ]); 
//	MarkPts(pqr4, ["chain",true, "transp", 1]);
    
} 

//Arc_demo_11_sealed();

module Arc_demo_12_showobj_frame()
{
    echom("Arc_demo_12_showobj_frame");  
	
    r=0.8;
    fn=4;
    nseg=3; 
    a=270;
    aclosed = 360/(nseg+1)*nseg;
    
    pqr2= randPts(3,d=[-3,-6]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //---------------------------------------------------
    xsecpts= [[ 3,0],[3,0.3],[2,1],[1.5,2],[1,2]
             /*,[1,0.2], [0.9,-0.1],[0.5,-0.2]*/, [0,-0.2]  ,[0,-0.5]
             /* , [0.7,-0.6], [0.9,-0.8]*/,[1,-1],[2,-1],[2,0]];
    
    Arc(pqr2, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.5, "r",r, "rad",3
              ,"xsecpts",xsecpts//, "rads",rads2
//              ,"frame", ["label",range(nseg)]
//              ,"markxsec", range(nseg+1)
              ,"closed",true
              ,"showxsec2d",true
              ,"sealed",[5,6]
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr2), "frame=[\"label\",range(nseg)], markxsec=true");
    //ColorAxes();
    //---------------------------------------------------

//    //---------------------------------------------------
    pqr3= trslN( pqr2, 7); 
    Arc(pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.1, "r",r, "rad",3
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg),"chain",["r",0.05,"closed",true]]
              ,"markxsec", range(nseg+1)
              ,"closed",true
              //,"showxsec2d",true
              //,"sealed",[5,6]
              ,"frame",true
              ,"hide",true
              ]);
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    
//    //---------------------------------------------------

    pqr4= trslN( pqr3, 7); 
    Arc(pqr4, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.1, "r",r, "rad",3
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg),"chain",["r",0.05,"closed",true]]
              //,"markxsec", range(nseg+1)
              ,"closed",true
              //,"showxsec2d",true
              ,"frame",true
              //,"sealed",[5,6]
             // ,"hide",true
              ]);
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    
//    //--------------------------------------------------- 

    axisln = linePts( [ Q(pqr2),Q(pqr4)], 5);
    Line0( axisln, ["r",0.02,"color","red"] );
} 
//Arc_demo_12_showobj_frame();

module Arc_demo_13_markxsec_plane(){
    
    echom("Arc_demo_13_markxsec_plane");
    
    r=0.8;
    fn=4;
    nseg=8; 
    a=270;
    aclosed = 360/(nseg+1)*nseg;
    
    xsecpts= [[0,-0.8],[1,-0.95],[2,-1.2],[3,-1],[3.5,-0.5],[3.7,-0.2]
             ,[3.7,0.2],[3.5,0.5],[3,1],[2,1.2],[1,0.95],[0,0.8]
             ];
    
    
    pqr= randPts(3, d=[-4,-7]);
    Arc( pqr, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1
              , "r",r, "rad",2
              ,"xsecpts",xsecpts
              ,"closed",true
              ,"showxsec2d",true
              ]);    
    MarkPts(pqr, "ch;l"); 
 
 
 
    pqr1=trslN( pqr, 5);
    Arc( pqr1, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1
              , "r",r, "rad",2
              ,"xsecpts",xsecpts
              ,"closed",true
              //,"showxsec2d",true
              
              ,"hide",true
              ,"markxsec", true
              ,"xsecopt", ["ball",false
                          , "chain",false
                          , "plane",["th",0.3,"color","red"]]
              ]);    
    MarkPts(pqr1, "ch;l"); 
 

    pqr2=trslN( pqr1, 4);
    Arc( pqr2, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1
              , "r",r, "rad",2
              ,"xsecpts",xsecpts
              ,"closed",true
              //,"showxsec2d",true
              
              ,"hide",true
              ,"markxsec", true
              ,"xsecopt", ["ball",false
                          , "chain",false
                          , "plane",["th",0.1, "color", "green"
                                    , "rotate",30
                                    ,"rotaxis", [0,-1]
                                    ]]
              
              ]);    
    MarkPts(pqr2, "ch;l"); 
  

    pqr3=trslN( pqr2, 5);
    Arc( pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1
              , "r",r, "rad",2
              ,"xsecpts",xsecpts
              ,"closed",true
              //,"showxsec2d",true
              
              ,"hide",true
              ,"markxsec", true
              ,"xsecopt", ["ball",false
                          , "chain",false
                          , "plane",["th",0.1, "color", "blue"
                                    , "rotate",30
                                    ,"rotaxis", [ 0, 6]
                                    ]]
              
              ]);    
    MarkPts(pqr3, "ch;l"); 
 
  //--------------------------------------------------- 

    axisln = linePts( [ Q(pqr),Q(pqr3)], 5);
    Line0( axisln, ["r",0.1,"color","black"] ); 
}
//Arc_demo_13_markxsec_plane();
