include <../scadx.scad>

module demo_func_sin()
{
   echom("demo_func_sin()");
   pts=  func("sin(180*x)", ["x", range(0,0.1,4)], returnWithVars=1  );
   Line(pts, Joint="sphere");
}
//demo_func_sin();

module demo_func_cond()
{
   echom("demo_func_cond");
   pts=  func("x>2?sin(180*x):(2-x)", ["x", range(0,0.1,4)], returnWithVars=1  );
   Line(pts, Joint="sphere");
}
//demo_func_cond();

module demo_func_3d()
{
   echom("demo_func_3d()");
   xyzs= func("sin(180*x)*y"
         , ["x",range(0,0.05,2),"y",range(1,1,4)], returnWithVars=1 );
   MarkPts(xyzs, Label=false, Ball=["dec_r",false], Line=true); 
}
//demo_func_3d();

module demo_func_3d_2()
{
   echom("demo_func_3d_2()");
   xs = range(0,0.05,4);
   xyzs= func("sin(90*x)*y"
         , ["x",xs,"y",range(1,1,4)], returnWithVars=1 );
   
   faces = faces(shape="mesh", nx=len(xs), ny=3);
   Teal(.5)for(f=faces) Plane( get(xyzs,f) );
   
   Black()MarkPts(xyzs, Label=false, Ball=["dec_r",false], Line=false); 
}
//demo_func_3d_2();

module demo_func_3d_3()
{
   echom("demo_func_3d_3()");
   xs = range(0,.5,4);
   ys = range(-4,.5,4);
   xyzs= func("(x^2+y^2)/4"
         , ["x",xs,"y",ys], returnWithVars=1 );
   
   faces = faces(shape="mesh", nx=len(xs), ny=len(ys));
   Teal(.5)for(f=faces) Plane( get(xyzs,f) );
   
   //MarkPts(xyzs);
   Black()MarkPts(xyzs, Label=false, Ball=["dec_r",false], Line=false); 
}
//demo_func_3d_3();

module demo_func_3d_4()
{
   echom("demo_func_3d_4()");
   xs = range(-4,1,5);
   ys = range(-4,1,5);
   xyzs= func("(x^2-y^2)/4"
         , ["x",xs,"y",ys], returnWithVars=1 );
   
   faces = faces(shape="mesh", nx=len(xs), ny=len(ys));
   Teal(.5)for(f=faces) Plane( get(xyzs,f) );
   
   //MarkPts(xyzs);
   Black()MarkPts(xyzs, Label=false, Ball=["dec_r",false], Line=false); 
}
//demo_func_3d_4();
