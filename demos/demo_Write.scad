include <../scadx.scad>


module Write_demo(){
    echom("Write_demo");
    pqr=randPts(3, d=[15,20],x=20,y=15);
    MarkPts(pqr, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    
    Write( "Arg *at* is not given", shift=[0,0,0.2]);
    Write( "Arg *at* is given", at=pqr, color="red", shift=[0,0,0.2]);
    
}
//Write_demo();

module Write_demo_text_as_children(){
    echom("Write_demo_text_as_children");
    echo(_blue("-- Allows for more flexible text"));
    echo(_blue("-- This is same as Move(...) "));
    
    pqr=randPts(3, d=[15,20],x=20,y=15);
    Write("text", at=pqr);
    
    // NOTE that the color arg has no effect on the 
    // children:
    Write( at=trslN(pqr, -5), color="darkcyan" ) 
    translate( [0,0,-0.5])
    rotate( [90,0,0] )
    text( "This text is not by Write, but a child obj.",size=4 ); 
}
//Write_demo_text_as_children();

module Write_demo_rot(){
    echom("Write_demo_rot");
    pqr=randPts(3, d=[15,20],x=20,y=15);
    MarkPts(pqr, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("Write", at=pqr);
    
    pqr2= trslN(pqr, -15);
    MarkPts(pqr2, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("rot=[90,0,0]", at=pqr2, rot=[90,0,0], color="red" ); 
    
    pqr3= trslN(pqr, -25);
    MarkPts(pqr3, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("rot=[90,0,0],shift=[0,0,-5]", at=pqr3, rot=[90,0,0], shift=[0,0,-5], color="green" ); 
    
    pqr4= trslN(pqr, -40);
    MarkPts(pqr4, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("rot=[180,0,0]", at=pqr4, rot=[180,0,0],color="blue" ); 

    pqr5= trslN(pqr, -55);
    MarkPts(pqr5, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("rot=[0,0,180]", at=pqr5, rot=[0,0,180],color="darkcyan" );
}
//Write_demo_rot();

module Write_demo_scale_size(){
    echom("Write_demo_scale_size");
    pqr=randPts(3, d=[15,20],x=20,y=15);
    MarkPts(pqr, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("Write", at=pqr);
    
    pqr2= trslN(pqr, -15);
    MarkPts(pqr2, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("scale=1", at=pqr2, scale=1, color="red" ); 
    
    pqr3= trslN(pqr, -25);
    MarkPts(pqr3, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("scale=0.5", at=pqr3, scale=0.5, color="green" ); 
    
    pqr4= trslN(pqr, -40);
    MarkPts(pqr4, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("size=1", at=pqr4, size=2, color="blue" ); 

    pqr5= trslN(pqr, -55);
    MarkPts(pqr5, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("size=10", at=pqr5, size=10,color="darkcyan" );
}
//Write_demo_scale_size();

module Write_demo_align(){
    echom("Write_demo_align");
    pqr=randPts(3, d=[15,20],x=20,y=15);
    MarkPts(pqr, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("Write", at=pqr);
    
    pqr2= trslN(pqr, -15);
    MarkPts(pqr2, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("valign=center", at=pqr2, valign="center", color="red" ); 
    
    pqr3= trslN(pqr, -25);
    MarkPts(pqr3, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("halign=center", at=pqr3, halign="center", color="green" ); 
    
    pqr4= trslN(pqr, -40);
    MarkPts(pqr4, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
    Write("valign=top, halign=right", at=pqr4, valign="top", halign="right", color="blue" ); 

//    pqr5= trslN(pqr, -55);
//    MarkPts(pqr5, "r=0.3;ch=[r,0.1];pl=[transp,0.2];l=PQR");
//    Write("size=10", at=pqr5, size=10,color="darkcyan" );
}
//Write_demo_align();

module Write_demo_text_with_other_children(){
    echom("Write_demo_text_with_other_children");
    pqr=randPts(3, d=[15,20],x=20,y=15);
    Write("Scadx", at=pqr);
    
    Write("Scadx w/a cube", at=trslN(pqr, -15), color="darkcyan" ) 
    translate( [0,0,-1])
    color("gold", transp=0.5)
    cube( [ textWidth( "Scadx w/a cube",size=4 ), 5,0.2]);

    Write("Scadx w/a cube", at=trslN(pqr, -30), th=3, color="darkcyan") 
    translate( [0,0,-3])
    color("gold", transp=0.5)
    cube( [ textWidth( "Scadx w/a cube",size=4 ), 5,3]);
        
}
//Write_demo_text_with_other_children();


module Write_demo_text_childcare(){
    echom("Write_demo_text_childcare");
    pqr=randPts(3, d=[15,20],x=20,y=15);
    //Write("Scadx", at=trslN(pqr, 15));
    
    Write("No childcare", at=trslN2(pqr, 7)
       ,color="darkcyan" ) 
    translate( [0,1,-1])
    color("darkcyan", 0.8)
    cube( [ textWidth( "No childcare",size=4 ), 0.4,5]);

    Write("childcare=\"intersection\"", at=trslN2(pqr, 0)
       , childcare="intersection", color="darkcyan" ) 
    translate( [0,1,-1])
    //color("yellow", 0.2)
    cube( [ textWidth( "childcare=\"intersection\"",size=4 ), 0.4,5]);
    
    Write("childcare=\"difference\"", at=trslN2(pqr, -7)
       , childcare="difference", color="darkcyan" ) 
    translate( [0,1,-1])
    //color("yellow", 0.2)
    cube( [ textWidth( "childcare=\"difference\"",size=4 ), 0.4,5]);
        
    Write("childcare=\"-difference\"", at=trslN2(pqr, -14), th=4,childcare="-difference" ) 
    translate( [0,-0.3,0.5])
    color("gold", transp=0.5)
    cube( [ textWidth( "childcare= \"-difference\"",size=4 ), 5,1]);
        
}
//Write_demo_text_childcare();


module Write_debug(){
 echom("Write_debug");
    
 pqr = [[3, 2.6, 2], [1.5, 2.6, 2], [1.25279, 3.49443, 2]];
 MarkPts( pqr, "ch;l=PQR");    
 Write( "Scadx", pqr );
        
}  
//Write_debug();