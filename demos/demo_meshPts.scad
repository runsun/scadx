include <../scadx.scad>

module demo_meshPts_default()
{
  echom("demo_meshPts_default()");
  pts = meshPts();
  //echo(pts=pts);
  MarkPts(pts, Line=false);
}
//demo_meshPts_default();

module demo_meshPts_4x4()
{
  echom("demo_meshPts_4x4()");
  pts = meshPts(4);
  //echo(pts=pts);
  MarkPts(pts, Line=false);
}
//demo_meshPts_4x4();

module demo_meshPts_4x3()
{
  echom("demo_meshPts_4x3()");
  pts = meshPts([4,3]);
  //echo(pts=pts);
  MarkPts(pts, Line=false);
}
//demo_meshPts_4x3();

//. Noise

module demo_meshPts_noise_x()
{
  echom("demo_meshPts_noise_x()");
  faces = faces(shape="mesh",nx=3,ny=3);
  
  pts = meshPts(); 
  for(f=faces)Line(get(pts,f), closed=true, r=0.01);
  //MarkPts(pts, Ball=["r",0.02], Label=["text",range(pts), "scale",0.5], Line=false);
  
  pts1 = meshPts(noise=["x",0.3]);
  color("red"){
  for(f=faces)Line(get(pts1,f), closed=true, r=0.01);
  //MarkPts(pts1, Ball=["r",0.02], Label=["text",range(pts), "scale",0.5], Line=false);
  }
}
//demo_meshPts_noise_x();

module demo_meshPts_noise_yz()
{
  echom("demo_meshPts_noise_yz()");
  faces = faces(shape="mesh",nx=3,ny=3);
  
  pts = meshPts(); 
  for(f=faces)Line(get(pts,f), closed=true, r=0.01);
  //MarkPts(pts, Ball=["r",0.02], Label=["text",range(pts), "scale",0.5], Line=false);
  
  pts1 = meshPts(noise=["y",0.3]);
  color("green"){
  for(f=faces)Line(get(pts1,f), closed=true, r=0.01);
  //MarkPts(pts1, Ball=["r",0.02], Label=["text",range(pts), "scale",0.5], Line=false);
  }
  
  pts2 = meshPts(noise=["z",0.5]);
  color("blue"){
  for(f=faces)Line(get(pts2,f), closed=true, r=0.01);
  //MarkPts(pts2, Ball=["r",0.02], Label=["text",range(pts), "scale",0.5], Line=false);
  }
}
//demo_meshPts_noise_yz();

module demo_meshPts_noise_xz2()
{
  echom("demo_meshPts_noise_xz2()");
  faces = faces(shape="mesh",nx=3,ny=3);
  n= 30;
  
  pts = meshPts(); 
  for(f=faces)Line(get(pts,f), closed=true, r=0.01);
  //MarkPts(pts, Ball=["r",0.02], Label=["text",range(pts), "scale",0.5], Line=false);
  
  for(i=range(n))
  {
     pts1 = meshPts(noise=["x",0.15, "z",0.15]);
     //MarkPts(pts1, Ball=["r",0.02]
     //         , Label=false, Line=false);
     for(j=range(pts1))
       Line( [pts[j],pts1[j]], color=COLORS[j], r=0.005) ;        
  }
  
}
//demo_meshPts_noise_xz2();

module demo_meshPts_noise_z2()
{
  echom("demo_meshPts_noise_z2()");
  faces = faces(shape="mesh",nx=3,ny=3);
  n= 30;
  
  pts = meshPts(); 
  for(f=faces)Line(get(pts,f), closed=true, r=0.01);
  //MarkPts(pts, Ball=["r",0.02], Label=["text",range(pts), "scale",0.5], Line=false);
  
  for(i=range(n))
  {
     pts1 = meshPts(noise=[ "z",0.15]);
     //MarkPts(pts1, Ball=["r",0.02]
     //         , Label=false, Line=false);
     for(i=range(faces))  
        Line(get(pts1,faces[i]), color=COLORS[i], closed=true, r=0.005);
  }
}
//demo_meshPts_noise_z2();

module demo_meshPts_noise_xyz()
{
  echom("demo_meshPts_noise_xyz()");
  faces = faces(shape="mesh",nx=3,ny=3);
  n= 30;
  
  pts = meshPts(); 
  for(f=faces)Line(get(pts,f), closed=true, r=0.01);
  //MarkPts(pts, Ball=["r",0.02], Label=["text",range(pts), "scale",0.5], Line=false);
  
  a_pts= [ for(i=range(n))meshPts(noise=[ "x",0.2,"y",0.2, "z",0.2]) ];
  
  for(pi=range(pts))
   Line( [for(i=range(n)) a_pts[i][pi]], r=0.005, color=COLORS[pi] );
}
//demo_meshPts_noise_xyz();

module demo_meshPts_noise_plane()
{
  echom("demo_meshPts_noise_plane()");
  faces = faces(shape="mesh",nx=5,ny=4);
  pts = meshPts([5,4], noise=["z",0.3]); 
  //echo(pts=pts);
  polyhedron( pts, faces );
}
demo_meshPts_noise_plane();


module demo_meshPts_formula()
{
   echom("demo_meshPts_formula()");
   nx = 10;
   ny = 8;
   xs = [for(i=range(nx)) sin( 90*i/nx )];    
   ys =  [for(i=range(ny)) cos( 90*i/ny )] ;    
   //xs = [for(i=range(nx)) i+(2*i*0.2)];    
   //ys = [for(i=range(ny)) cos( 90*i/ny )];    
   echo(xs=xs, ys=ys);
   pts = meshPts(xs=xs, ys=ys);
   faces = faces(shape="mesh",nx=nx,ny=ny);
   for(f=faces)Line(get(pts,f), closed=true, r=0.01);
}
//demo_meshPts_formula();

