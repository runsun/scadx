include <../scadx.scad>

module demo_circleCenter() // 2018.6.5
{
  echom("demo_circleCenter()");
  
  pqr=pqr();
  MarkPts(pqr,"PQR");
  echopts(pqr, "pqr");
  
  C = circleCenter(pqr, debug=0);
  echo(C=C);
  Red()
  { MarkPt(C, Label=false);
    for(p=pqr) Line([p,C], r=0.005);
  }
  Dim( [C, pqr[0], pqr[1]] );
  Dim( [C, pqr[1], pqr[2]] );
  Dim( [C, pqr[2], pqr[0]] );
}
//demo_circleCenter();


module demo_circlePtsOnPQR()
{
  echom("demo_circlePtsOnPQR()");
  echo(_red("pts of a circle made out of pqr") );
  pqr= pqr();
  MarkPts(pqr); 
  
  cpts = circlePtsOnPQR(pqr, n=8);
  echo(cpts = cpts);
  
  Red(0.2) MarkPts( cpts, Ball=["color","blue", "r", 0.05] );
}
//demo_circlePtsOnPQR();



//demo_circumCenterPt();
module demo_circumCenterPt()
{
  echom("demo_circumCenterPt()");
  echo(_red("Center of a circle made out of pqr") );
  pqr= pqr();
   pqr = [[1.12, 1.78, -1.31], [-0.21, -0.18, 0.62], [-0.79, -1.29, 0.29]];
  echo(pqr=pqr);
  MarkPts(pqr, Line=["closed",1]); 
  
  C = circumCenterPt(pqr, n=8);
  echo(C);
  MarkPt(C,"C");
  DashLine( [C, pqr[0]] );  
  DashLine( [C, pqr[1]] );  
  DashLine( [C, pqr[2]] );  
}




module demo_closestPi()
{
  echom("demo_closestPi()");
  _red("Find the index of Pt (blue) cloest to ref (red)");
  pts= randPts(5);
  ref = randPt();
  
  MarkPts(pts); 
  MarkPt(ref, Label="R", Ball=["color",["red", 0.25], "r", 0.25]);
  
  cpi = closestPi(pts, ref);
  MarkPt( pts[cpi], Ball=["color",["blue", 0.25], "r", 0.25] );
}
//demo_closestPi();

module demo_cutHolePts()
{
  echom("demo_cutHolePts()");
  echo(_red("Demo how a hole can be cut on a plane"));
  pqr = pqr();
  pts = concat(pqr, [R(pqr)+P(pqr)-Q(pqr)]);
  //pts = [[1.28289, 0.126739, 0.827979], [0.383984, 0.492185, -0.185277], [1.10578, 1.53366, 0.718386], [2.00468, 1.16822, 1.73164]];
  pts= [[-1.87747, -1.17049, -1.67034], [0.7571, 0.417271, -0.408456]  
       , [-0.266998, -1.79828, 1.61989], [-2.90157, -3.38604, 0.358004]];
  
  center = sum(pts)/len(pts);
  _holePts = expandPts( pts, dist=-dist(pts[0],center)/2 );
  rot_axis = [ N( [pts[2], center, pts[1]])
             , center ];
  rot_a = rands(0,270,1)[0];  
         
  Line(rot_axis);  
  
  holePts =  rotPts(_holePts,rot_axis,a = rot_a);
  cutHolePts = cutHolePts( pts, holePts );
  
  //echo(pts=pts);
  //echo(rot_a=_red(rot_a), rot_axis=rot_axis);
  //echo(_holePts=_holePts);
  //echo(holePts=holePts);
  //echo(cutHolePts=cutHolePts);
  
  MarkPt( center, Label="C");
  MarkPts( cutHolePts);
  Plane( holePts, mode=1, color=["gold", 0.1]);
}   
//demo_cutHolePts();

module dev_cutHolePts_application() 
{
  // 2018.5.3: 
  // cutHolePts doesn't seem to be necessary. See comments in it
  // and the example below:
  //   A face [ 0,3,2,1,0, 6,7,4,5,6 ] is as valid as 
  //          [ 0,3,2,1,0, 4,5,6,7,4 ] 
  
  
  echom("dev_cutHolePts_application()");
  echo(_red("Demo how to punch a hole with cutHolePts()"));
  pqr = pqr();
  pts = concat(pqr, [R(pqr)+P(pqr)-Q(pqr)]);
  pts= [[-1.87747, -1.17049, -1.67034], [0.7571, 0.417271, -0.408456]  
       , [-0.266998, -1.79828, 1.61989], [-2.90157, -3.38604, 0.358004]];
  
  L= len(pts);
  rng = range(L);
  
  center = sum(pts)/L;
  _holePts = expandPts( pts, dist=-dist(pts[0],center)/2 );
  rot_axis = [ N( [pts[2], center, pts[1]])
             , center ];
  rot_a = rands(0,270,1)[0];  
         
  Line(rot_axis);  
  
  holePts =  rotPts(_holePts,rot_axis,a = rot_a);
  cutHolePts = cutHolePts( pts, holePts );
  
  Lh = len(holePts);
  Lc = len(cutHolePts);
  //------------------------------------
  // Make it 3d:
  cutHolePts2= transN(cutHolePts, d=1);
  
  outerfaces=[for( i= rng )
              [ i
              , i==L-1? 0:(i+1)
              , i==len(pts)-1? Lc: i+Lc+1
              , i+Lc
              ]
            ];
             
  frontfaces = concat(reverse(concat( range(pts), 0)), concat( range(L, Lc), L ));
  backfaces = concat( range(Lc,Lc+L), Lc,  reverse(concat(range(L+Lc, Lc*2), L+Lc)) );
                          
  faces = [ frontfaces 
          , backfaces          
          // Outer sides
          , each outerfaces
          // Inner sides
          , each [ for( of = outerfaces )
               [for(pi=of) pi+len(pts) ]
            ]
          ];
  echo( faces = faces );
  //  faces = [ [0, 3, 2, 1, 0, 4, 5, 6, 7, 4]
  //          , [8, 9, 10, 11, 8, 12, 15, 14, 13, 12]
  //          , [0, 1, 9, 8], [1, 2, 10, 9], [2, 3, 11, 10], [3, 0, 8, 11]
  //          , [4, 5, 13, 12], [5, 6, 14, 13], [6, 7, 15, 14], [7, 4, 12, 15]
  //          ]  
    faces = [ [0, 3, 2, 1, 0, 6, 7, 4,5,6]
            , [8, 9, 10, 11, 8, 12, 15, 14, 13, 12]
            , [0, 1, 9, 8], [1, 2, 10, 9], [2, 3, 11, 10], [3, 0, 8, 11]
            , [4, 5, 13, 12], [5, 6, 14, 13], [6, 7, 15, 14], [7, 4, 12, 15]
            ];  

  //echo(pts=pts);
  //echo(rot_a=_red(rot_a), rot_axis=rot_axis);
  //echo(_holePts=_holePts);
  //echo(holePts=holePts);
  //echo(cutHolePts=cutHolePts);
  
  allpts = concat(cutHolePts, cutHolePts2);
  MarkPt( center, Label="C");
  //MarkPts( cutHolePts);
  //MarkPts( cutHolePts2);
  MarkPts( allpts, Label=["text", range(allpts), "color","blue"] );
  color("gold",0.5) polyhedron( allpts, faces );
}   
//dev_cutHolePts_application();



module demo_getPlaneByNormalLine()
{
  echom("demo_getPlaneByNormalLine()");
  PQ= randPts(2);
  echo(PQ=PQ);
  //PQ=  [[2.58822, 1.00425, 0.0897126], [1.45698, 0.131129, 2.11266]];
  MarkPts(PQ, Label="PQ");
  
  pl1= expandPts(getPlaneByNormalLine(PQ),1);
  
  //echo(pl1=pl1);
  MarkPts( pl1 );
  Plane( pl1, mode=1);
  //echo( angle1= angle([pl1[1],PQ[0],PQ[1]])
      //, angle2= angle([pl1[2],PQ[0],PQ[1]])
      //);
  Mark90( [pl1[1],PQ[0], PQ[1]] );
  Mark90( [pl1[2],PQ[0], PQ[1]] );
  Mark90( [pl1[0],PQ[0], PQ[1]] );
}
//demo_getPlaneByNormalLine();


module demo_intsec_cirpl()
{
  echom("demo_intsec_cirpl()");
  pqr= pqr();
  stu= pqr();
  MarkPts(pqr, false, Line=false);
  Plane(expandPts(pqr));
  
  Red(.5) { MarkPts(stu, false, Line=false); Plane(expandPts(stu)); }
  
  //intline = intsec_plpl(pqr,stu, debug=0);
  intline = intsec_cirpl(pqr,stu, debug=0);
  echo(intline = intline);
  if(intline)
  {  
     MarkPts(intline,  color=["blue",0.3], Line=["r",0.1]);
  }
  
}
//demo_intsec_cirpl();

module demo_intsec_plpl()
{
  echom("demo_intsec_plpl()");
  pqr= pqr();
  stu= pqr();
  MarkPts(pqr, false, Line=false);
  Plane(expandPts(pqr));
  
  Red(.5) { MarkPts(stu, false, Line=false); Plane(expandPts(stu)); }
  
  //intline = intsec_plpl(pqr,stu, debug=0);
  intline = intsec(pqr,stu, debug=0);
  echo(intline = intline);
  if(intline)
  {  
     MarkPts(intline,  color=["blue",0.3], Line=["r",0.1]);
  }
  
}
//demo_intsec_plpl();


module demo_lineBridge()
{
  echom("demo_lineBridge()");
  L1=randPts(2);
  L2=randPts(2);

  MarkPts(L1, color="red");
  MarkPts(L2, color="green");
  
  br= lineBridge(L1,L2);
  MarkPts( br );
  Mark90( [br[0],br[1],L2[0]] );
  Mark90( [br[1],br[0],L1[0]] );


}
//demo_lineBridge();

//module demo_normal

module demo_normalPt()
{
    echom("demo_normalPt");
    pqr = randPts(3);
    P = pqr[0];  Q=pqr[1]; R=pqr[2];
    
    MarkPts( pqr, Label="PQR", Ball=false);
    
    N = normalPt( pqr );
    echo("N",N);
    MarkPts( [pqr[1],N], Label=["text",["","N"]], Ball=false );
    
    //echo( [Q, N] );
    Mark90( [N,Q,P] );
    Mark90( [N,Q,R] );
    echo(angle([N,Q,P]), angle([N,Q,R] ));
    
    //Dim( [N, Q, P] );
    //Line( [Q, N], ["r",0.02,"color","red"] );
    Nr = normalPt( pqr, len=1, reversed=true);
    MarkPts( [pqr[1],Nr], Line=["color","green"]
            ,Label=["text",["","Nr"]], Ball=false );
    //echo("Nr",Nr);
    Mark90( [Nr,Q,P] );
    Mark90( [Nr,Q,R] );
    //Dim( [Nr, Q, P] );
    //Line( [Q, Nr], ["r",0.02,"color","green"] );

}
//demo_normalPt();

//demo_othoPt();
module demo_othoPt()
{
  echom("demo_othoPt()");
  pqr=pqr();
  MarkPts(pqr, "PQR", Line=["closed",1, "r",0.025, "color", "black"]);
  
  for(i=[0,1,2])
  {
     //is= i==0?[2,0,1]:i==1?[0,1,2]:[1,2,0];
     is= i==0?[1,0,2]:i==1?[2,1,0]:[0,2,1];
     P= pqr[is[0]];     
     Q= pqr[is[1]];     
     R= pqr[is[2]];
     
     J= othoPt(pqr, tip=i);
     color(COLORS[i+1], 0.5)
     { 
       MarkPt(J, str("tip=",i));
       Mark90( [R,J,Q] );
     }
  }
}


module demo_randc()
{
    light = [0.8,1];
    dark  = [0,0.4];
    translate(randPt()) color(randc(r=dark,g=dark,b=dark)) sphere(r=0.5);
    translate(randPt()) color(randc()) sphere(r=1);
    translate(randPt()) color(randc(r=light, g=light,b=light)) sphere(r=1.5);
}

module demo_randConvexPlane()
{
   echom("demo_randConvexPlane()");
   site = [ [-1.04, 1.27, 0.24], [-1.04, 1.85, -1.94]
          , [1.94, 0.57, -1.26]];
   echo(site=site);
   color("red") MarkPts(site);
   
   //for(i=range(10))
   //{
   pts = randConvexPlane(n=5, d=[2,3], site=site);
   echo(pts=pts);
   MarkPts(pts, Line=["closed",true]);
   
   //spts = smoothPts(pts, closed=1);
   //Line(spts, closed=1);
   //}
}
demo_randConvexPlane();



module demo_sortByDist()
{
   echom("demo_sortByDist()");
   pts = randPts(5);
   spts = sortByDist( pts, ORIGIN );
   MarkPts(pts);
   MarkPts(spts, Line=["r",0.1, "color",["red",0.3]]
               , Label=["text", range(spts), "indent",0.1, "color","red"] );
}
//demo_sortByDist();


module demo_twistangle()
{	
    pqrs = randPts(4);
    P=P(pqrs); Q=Q(pqrs); R=R(pqrs);
    S=pqrs[3];
	
    MarkPts([P,Q,R,S],["labels","PQRS"]);
    Chain( [P,Q,R,S ] );
    //Line0( [Q,S] );

    echo( "quad:", quadrant( [Q,R,P],S), "twistangle:", twistangle( [P,Q,R, S] ));
}
//demo_twistangle();



