include <../scadx.scad>

ISLOG=1;

module demo_cirIntsecPts()
{

  echom("demo_cirIntsecPts()");
  
  pts= randConvexPlane(n=4, site=ORIGIN_SITE);
  pts = [[1.1,1.53,0],[-1.6,1,0],[0.77,-1.72,0],[1.36,1.3,0]];
  echo(pts=pts);
  n=5;
  

  //Error pts (2018.11.1):
  //
  // DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated. 
  // WARNING: Incorrect arguments to norm() 
  // WARNING: Invalid type of parameters for cross() 
  //pts= [[-0.0731514, 2.39498, 0], [-1.15507, 2.09931, 0], [-0.87755, -2.22962, 0], [1.86007, -1.51044, 0], [2.28042, 0.735517, 0]];
  //n=12;
  //
  // pts = [[-0.30251, 2.38451, 0], [-1.43942, 1.92496, 0], [-1.79605, -1.59737, 0], [-0.557228, -2.33814, 0], [2.2, -0.968194, 0]];
  // n=8;
  //
  // pts = [[-1.56, 3.17, 0], [-3.49, -0.59, 0], [1.73, -3.08, 0], [3.5, 0.47, 0]]
  // n=4;
  //
  // DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated. 
  //pts=  [[0.939556, 2.89549, 0], [-2.75088, 1.30355, 0], [-3.04147, -0.126848, 0], [0.427853, -3.01389, 0], [3.03419, 0.245523, 0]]; 
  //n=4;
  
  
  MarkPts(pts, Line=["closed",1, "color", "red"]);
  C = sum(pts)/len(pts);
  N = N([ pts[0], C, pts[1] ]);
  MarkPt(C,"C");
  echo(pqr= [N,C,pts[0]]);
  cpts = circlePts( pqr=[pts[0], C, pts[1]], n=n );
  MarkPts(cpts, Line=["closed",1]);
  
  for(p=pts) Line( [p,C] );
  for(i=range(n)) Line( [cpts[i],C], color=["green",0.3], r=0.01 );
  
  
  cis = cirIntsecPts( pts=pts,n=n,C=C);
  echo(cis=cis);
  
  MarkPts( cis, Ball=["r",0.2, "color",["green",0.5]], Label=0, Line=0);
}
demo_cirIntsecPts();