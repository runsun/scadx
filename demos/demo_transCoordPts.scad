include <../scadx.scad>


module demo_transCoordPts_1()
{
  echom("demo_transCoordPts_1()");

  //======================================================
  newX=[2,1,0];
  coo_1=[newX,Y,Z ];
  Purple(0.4)
  {
    MarkPts( coo_1,"PQR", Line=["closed",1] );
    Plane(coo_1);
  }

  Red()   Arrow( [O,coo_1[0]], r=0.025, len= norm(O,coo_1) );
  Green() Arrow( [O,coo_1[1]], r=0.025, len=1 );
  Blue()  Arrow( [O,coo_1[2]], r=0.025, len=1 );

  Arrow( [O,coo_1[0]], len=5 );
  Arrow( [O,coo_1[1]], len=5 );
  Arrow( [O,coo_1[2]], len=5 );
  Red()
  Dim( [ newX,O,Y+Z]
       , Label=["text", "New unit vector for X"
               ,"actions", ["rotz", 180]
               ]
       , Link=true 
       );
  _green( str("coord = PQR = ", coo_1) );
  _blue(_mono(  
           str("PQR = new unit vectors ( a transformation matrix !!!):<br/>", join( coo_1, "<br/>")) 
       , s="font-size:16px"));     

  //======================================================

  cpts = [for(p=cubePts(2)) p+[1,1,0]];
  //MarkPts(cpts, Line=false);
  Black() MarkPt(cpts[1], Label=["text",true], Link=["steps", ["t",[2,-1,0]] 
               ]  );
  DrawEdges( [cpts, CUBEFACES], r=0.025);
  
  
  
  pts= transCoordPts( cpts, coo_1 );
  Green(0.2) DrawEdges( [pts, CUBEFACES], r=0.05);
  Green() DashLine( [ newX, newX+Y, Y] );
  Green()
  MarkPt( pts[1], str( cpts[1], " in the new coord" )
        , Link=["steps", ["t",[2,-1,0]] 
               ] 
        );  
  _h2( str( _red("Each "),"pt",_red(" in "), "cpts"
          , _red(" serves as the *scalar* [a,b,c] in aP+bQ+cR to make the ")
          , _green("corresponding pt" )));      
        
};
//demo_transCoordPts_1();



module demo_transCoordPts_shear()
{
  echom("demo_transCoordPts_shear()");

  cpts = cubePts(1);
  DrawEdges( [cpts, CUBEFACES], r=0.015);

  Green() Plane(pqr2);
  

  //======================================================
  pqr=[ [1,1,0]
      , [0,1,0]
      , [0,0,1] ];
  _red(_mono( str("PQR = <br/>", join( pqr, "<br/>")) 
              , s="font-size:16px")); 
  _red( "Shear to Y on the XY surface (along [1,0,0]=>[1,1,0]) " );
  Red() Plane(pqr); //Line(pqr, closed=1);
                   
  //------------------------------------------------------
  pts= transCoordPts( cpts, pqr );
  Red(0.2) //DrawEdges( [pts, CUBEFACES], r=0.075);
  Poly( [pts,CUBEFACES] );
  Red() { Arrow( [[1.1,0,0], [1.1,1,0]] );
         MarkPt( [1.1,0,0], str( X, " => ", pqr[0]), Ball=false );
         }

  //======================================================
  pqr2=[ [1,0,0]
       , [0,1,0]
       , [0,1,1] ];
  _green(_mono( str("PQR2 = <br/>", join( pqr2, "<br/>")) 
              , s="font-size:16px"));     
  _green( "Shear to Y on the ZY surface (along [0,0,1]=>[0,1,1]) " ); 
                  
  //------------------------------------------------------
  pts2= transCoordPts( cpts, pqr2 );
  Green(0.5) DrawEdges( [pts2, CUBEFACES], r=0.025);
  Green() { Arrow( [[-0.1,0,1.1], [-0.1, 1,1.1]] );
            MarkPt( [0,2,1.2], Label=[ "text", str( Z, " => ", pqr2[2])
                                        , "halign", "center"
                                        ]
                    , Ball=false );
          }
        
};
//demo_transCoordPts_shear();



module demo_transCoordPts_shear2()
{
  echom("demo_transCoordPts_shear2()");

  cpts = [for(p=cubePts(1)) p]; //p-[.5,0,.5]];
  DrawEdges( [cpts, CUBEFACES], r=0.015);

  //Green() Plane(pqr2);
  

  //======================================================
  pqr=[ [2,0,0]
      , [1,2,0]
      , [0,0,1] ];
  _red(_mono( str("PQR = <br/>", join( pqr, "<br/>")) 
              , s="font-size:16px")); 
  _red( "Shear to Y on the XY surface (along [1,0,0]=>[1,1,0]) " );
  //Red() Plane(pqr); //Line(pqr, closed=1);
                   
  //------------------------------------------------------
  pts= transCoordPts( cpts, pqr );
  Red(0.2) { DrawEdges( [pts, CUBEFACES], r=0.025);
             //Poly( [pts,CUBEFACES] );
           }  
  Red() { Arrow( [[1.1,0,0], [1.1,1,0]] );
          MarkPt( [1.1,0,0], str( X, " => ", pqr[0]), Ball=false );
        }

  //======================================================
  pqr2=[ [1,0,0]
       , [0,1,0]
       , [0,1,1] ];
  _green(_mono( str("PQR2 = <br/>", join( pqr2, "<br/>")) 
              , s="font-size:16px"));     
  _green( "Shear to Y on the ZY surface (along [0,0,1]=>[0,1,1]) " ); 
                  
  //------------------------------------------------------
  pts2= transCoordPts( cpts, pqr2 );
  Green(0.5) DrawEdges( [pts2, CUBEFACES], r=0.025);
  Green() { Arrow( [[-0.1,0,1.1], [-0.1, 1,1.1]] );
            MarkPt( [0,2,1.2], Label=[ "text", str( Z, " => ", pqr2[2])
                                        , "halign", "center"
                                        ]
                    , Ball=false );
          }
        
};
//demo_transCoordPts_shear2();


