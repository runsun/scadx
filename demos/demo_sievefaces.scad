include <../scadx.scad>


//demo_sievefaces_2holes_how();
module demo_sievefaces_2holes_how()
{
  echom("demo_sievefaces_2holes_how()");
   h=2;
  pts_b= [ O, 4*Y, 3*X+5*Y, 2*X ];
  pts_t= addz(pts_b, h);
  
  pts_hole= [ X+.5*Y, 0.4*X+1.2*Y
            , .5*X+2.1*Y//, 1.8*X+2.5*Y, 1.5*X+2*Y
            , 2*X+1.5*Y];
  pts_hole2= [ 1.5*X+2.5*Y, X/2+3.5*Y, 2*X+3.5*Y];
  
  //pts_hole= [ X+.5*Y, 0.4*X+1.2*Y, 2*X+1.5*Y];
  //pts_hole2= [ .5*X+1.8*Y, .5*X+2.1*Y, 1.8*X+2.2*Y, 1.5*X+2*Y];
  //pts_hole3= [ X+2.75*Y, X/2+3.5*Y, 2*X+3.5*Y];
  
  pts = concat(pts_b, pts_t
              , pts_hole
              , addz(pts_hole,h)
              
              , pts_hole2
              , addz(pts_hole2,h)
              
             // , pts_hole3
             // , addz(pts_hole3,h)
              
              );
  echo(pts=pts);
  Black()MarkPts(addz(pts,0.1), Line=false);           
           
  Red(0.5) Arrow2( get(pts, [4,5,6,7,4] ), r=0.05 );
  DashLine( get(pts, [4,15]), r=0.05);
  Green(.5) Arrow2( get(pts, [15,14,13,12,15] ), r=0.05 );
  DashLine( get(pts, [4,21]), r=0.05);
  Blue(.5) Arrow2( get(pts, [21,20,19,21] ), r=0.05 );
  
  faces = sievefaces( nO=len(pts_b)
                    , nI= [ len(pts_hole)
                          , len(pts_hole2)
                          //, len(pts_hole3)
                          ]
                    );
  echo(faces=faces);
  
  //Gold(.2)
  polyhedron(pts,faces);
  /*
   pts = [[0, 0, 0], [0, 4, 0], [3, 5, 0], [2, 0, 0], [0, 0, 2], [0, 4, 2], [3, 5, 2], [2, 0, 2], [1, 0.5, 0], [0.4, 1.2, 0], [0.5, 2.1, 0], [2, 1.5, 0], [1, 0.5, 2], [0.4, 1.2, 2], [0.5, 2.1, 2], [2, 1.5, 2], [1.5, 2.5, 0], [0.5, 3.5, 0], [2, 3.5, 0], [1.5, 2.5, 2], [0.5, 3.5, 2], [2, 3.5, 2]];
   faces = [[4, 5, 6, 7, 4, 15, 14, 13, 12, 15, 4, 21, 20, 19, 21], [3, 2, 1, 0, 3, 8, 9, 10, 11, 8, 3, 16, 17, 18, 16], [0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7], [9, 8, 12, 13], [10, 9, 13, 14], [11, 10, 14, 15], [8, 11, 15, 12], [17, 16, 19, 20], [18, 17, 20, 21], [16, 18, 21, 19]];
   polyhedron(pts,faces);
  
  
  
  */
  
  
}


//demo_sievefaces_2holes();
module demo_sievefaces_2holes()
{
  echom("demo_sievefaces_2holes()");
   h=1;
  pts_b= [ O, 4*Y, 3*X+5*Y, 2*X ];
  pts_t= addz(pts_b, h);
  
  pts_hole= [ X+.5*Y, 0.4*X+1.2*Y
            , .5*X+2.1*Y, 1.8*X+2.5*Y, 1.5*X+2*Y, 2*X+1.5*Y];
  pts_hole2= [ X+2.75*Y, X/2+3.5*Y, 2*X+3.5*Y];
  
  //pts_hole= [ X+.5*Y, 0.4*X+1.2*Y, 2*X+1.5*Y];
  //pts_hole2= [ .5*X+1.8*Y, .5*X+2.1*Y, 1.8*X+2.2*Y, 1.5*X+2*Y];
  //pts_hole3= [ X+2.75*Y, X/2+3.5*Y, 2*X+3.5*Y];
  
  pts = concat(pts_b, pts_t
              , pts_hole
              , addz(pts_hole,h)
              
              , pts_hole2
              , addz(pts_hole2,h)
              
             // , pts_hole3
             // , addz(pts_hole3,h)
              
              );
           
  //Red()MarkPts(slice(pts, 0,14), Line=["closed",1]);           
  //Green()MarkPts(slice(pts, 14,22), Label=["text", range(14,22)], Line=["closed",1] );           
  //Blue()MarkPts(slice(pts, 22), Label=["text", range(22, 30)], Line=["closed",1] );           
  
  faces = sievefaces( nO=len(pts_b)
                    , nI= [ len(pts_hole)
                          , len(pts_hole2)
                          //, len(pts_hole3)
                          ]
                    );
  echo(faces=faces);
  
  Gold()polyhedron(pts,faces);

  /* This fails to render:
  
    union(){
    Gold()polyhedron(pts,faces);
    translate( 2.4*X) cube(3);
    }
    
  */
  
}



//demo_sievefaces_3holes();
module demo_sievefaces_3holes()
{
  echom("demo_sievefaces_3holes()");
   h=1;
  pts_b= [ O, 4*Y, 3*X+5*Y, 2*X ];
  pts_t= addz(pts_b, h);
  
  pts_hole= [ X+.5*Y, 0.4*X+1.2*Y, 2*X+1.5*Y];
  pts_hole2= [ .5*X+1.8*Y, .5*X+2.1*Y, 1.8*X+2.2*Y, 1.5*X+2*Y];
  
  pts_hole3= [ X+2.75*Y, X/2+3.5*Y, 2*X+3.5*Y];
  
  pts = concat(pts_b, pts_t
              , pts_hole
              , addz(pts_hole,h)
              
              , pts_hole2
              , addz(pts_hole2,h)
              
              , pts_hole3
              , addz(pts_hole3,h)
              
              );
           
  Red()MarkPts(slice(pts, 0,14));           
  Green()MarkPts(slice(addz(pts,0.1), 14,22), Label=["text", range(14,22)] );           
  Blue()MarkPts(slice(addz(pts,0.1), 22), Label=["text", range(22, 30)] );           
  
  faces = sievefaces( nO=len(pts_b)
                    , nI= [len(pts_hole), len(pts_hole2), len(pts_hole3)]
                    );
  echo(pts=pts);
  echo(faces=faces);
  
  
  Gold(0.8)polyhedron(pts,faces);
}


demo_sievefaces_multiholes_has_bug(3,2,1/10);
demo_sievefaces_multiholes_has_bug(3,2,1/8, 6*X);
//demo_sievefaces_multiholes_has_bug(3,1,1/10, 6.5*Y);
//demo_sievefaces_multiholes_has_bug(1,3,1/10, 6.5*Y+6*X);
//demo_sievefaces_multiholes_has_bug(2,2,1/10, -6.5*Y);
//demo_sievefaces_multiholes_has_bug(2,2,1/8, -6.5*Y+6*X);
//demo_sievefaces_multiholes_has_bug(2,2,1/5, -6.5*Y+12*X);
//demo_sievefaces_multiholes_has_bug(2,2,1/4, -6.5*Y+18*X);
module demo_sievefaces_multiholes_has_bug( nHoles_x=3, nHoles_y=2, fw_ratio=1/10, translate=O)
{
  echom("demo_sievefaces_multiholes_has_bug()");
  x=5;
  y=5;
  h=2;
  
  nHoles_x = nHoles_x;
  nHoles_y = nHoles_y; 
  fw = x * fw_ratio; // width of frame
  _x = x-(nHoles_x+1)*fw; // x length to be occupied by holes
  _y = y-(nHoles_y+1)*fw; // y length to be occupied by holes
  
  hx = _x/nHoles_x; // size x of hole
  hy = _y/nHoles_y; // size y of hole
  
  hole_ptss = 
  [
    for( c=range(nHoles_x)
       , r=range(nHoles_y) )
    let( pts= [ (fw+c*(fw+hx))*X+ (fw+r*(fw+hy))*Y 
              , (fw+c*(fw+hx))*X+ (r+1)*(fw+hy)*Y 
              , (c+1)*(fw+hx)*X+ (r+1)*(fw+hy)*Y 
              , (c+1)*(fw+hx)*X+ (fw+r*(fw+hy))*Y 
              ]
       )
    each [ each pts, each addz(pts, h) ]          
  ];
  
  
  pts_b= [ O, y*Y, x*X+y*Y, x*X ];
  pts_t= addz(pts_b, h);
   
  //echo(hole_ptss=hole_ptss);
  //for(pts = addz(hole_ptss,1.2)) MarkPts(pts);
  
  Cb = avg(pts_b);      
  Ct = avg(pts_t);      
  pts = [ for(pt= [ each pts_b
                  , each pts_t
                  , each hole_ptss
                 // , Cb,Ct
                  ])
          pt + translate        
         ]         ;

  //Red()MarkPts( addz(pts,0.05), Line=false);
  echo(pts=pts);
  
  faces = sievefaces( nO= len(pts_b)
                    , nI= repeat([4], nHoles_x*nHoles_y) 
                    );
   
  faces2= [[4,5,6,7,4,4, 15,14,13,12,15, 4, 23,22,21,20,23, 4, 31,30,29,28,31, 4, 39,38,37,36,39]
         ,[3, 2, 1, 0, 3, 8, 9, 10, 11, 8, 3, 16, 17, 18, 19, 16, 3, 24, 25, 26, 27, 24, 3, 32, 33, 34, 35, 32], [0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7], [9, 8, 12, 13], [10, 9, 13, 14], [11, 10, 14, 15], [8, 11, 15, 12], [17, 16, 20, 21], [18, 17, 21, 22], [19, 18, 22, 23], [16, 19, 23, 20], [25, 24, 28, 29], [26, 25, 29, 30], [27, 26, 30, 31], [24, 27, 31, 28], [33, 32, 36, 37], [34, 33, 37, 38], [35, 34, 38, 39], [32, 35, 39, 36]];
                    
  echo(faces=faces);
  
  echo(fw=fw, [ [x,-1,h], [0,-1,h], [x,0] ]);
  Black()
  Text( text=str("fw=", str(fw))
      , site= [ [x,-1,h]+translate, [x/2,-1,h]+translate, [x,0,h]+translate ]
      , halign="center"
      , scale=0.75 );
  
  DrawEdges( [pts,faces] );
  //Green()
  polyhedron(pts,faces, convexity=10);
}

