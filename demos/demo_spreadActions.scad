include <../scadx.scad>

size=[.5,.3,1];

//demo_spreadActions_1d();
//demo_spreadActions_1d_dir();
//demo_spreadActions_2d();
demo_spreadActions_3d();

module Obj(color="gold"){
  pts = cubePts(size);
  //MarkPts(pts);
  //echo(pts=pts);
  Color(color) cube(size);
  MarkCenter( get(pts, [0,1,5,4])
            , Label=[ "text","Cube", "color","blue"
                    , "followView", false
                    , "actions", ["rotx",90, "roty", -90] 
                    ] 
            );
}

module demo_spreadActions_1d()
{
   echom("demo_spreadActions_1d()");
   L = 5;
   
   Dim( [ O, L*X, Y], Label=["actions",["rotx",180]] );  
       
   sas2= spreadActions( objlen= size.x, spreadlen=L, n = 2);
   for(i=range(sas2)) Transforms(sas2[i]) Obj();

   sas3= spreadActions( objlen= size.x, spreadlen=L, n = 3, offset=[0, size.y*2,0]);
   for(i=range(sas3)) 
     Transforms( sas3[i] ) Obj("red");

   sas4= spreadActions( objlen= size.x, spreadlen=L, n = 4, offset=[0, size.y*4,0]);
   for(i=range(sas4)) 
     Transforms( sas4[i] ) Obj("teal");
     
   for(sa=spreadActions( objlen= size.x, spreadlen=L, n = 5, offset=[0, size.y*6,0])) 
     Transforms( sa ) Obj("olive");
   
}


module demo_spreadActions_1d_dir()
{
   echom("demo_spreadActions_1d_dir");
   L = 5;
   W = 4;
   H = 5;
   
   Dim( [ O, L*X, Y], Label=["actions",["rotx",180]] );         
   sasx= spreadActions( objlen= size.x, spreadlen=L, n = 4);
   for(i=range(sasx)) Transforms(sasx[i]) Obj();
   
   Dim( [ O, W*Y, X],spacer=1 );         
   sasy= spreadActions( objlen= size.y, spreadlen=W, n = 5, dir="y"
                      , offset= [-size.x-0.1,0,0]);
   for(i=range(sasy)) Transforms(sasy[i]) Obj("red");

   Dim( [ O, W*Y, X],spacer=1 );         
   sasz= spreadActions( objlen= size.z, spreadlen=H, n = 4, dir="z"     
                      , offset= [0,size.y+0.1,0] );
   for(i=range(sasz)) Transforms( sasz[i] ) Obj("teal");   
}
//demo_spreadActions_1d_dir();

module demo_spreadActions_2d()
{
   echom("demo_spreadActions_2d()");
   L = 5;
   W = 4;
   H = 5;
   
   Plane( [L*X, O, W*Y, L*X+W*Y] );
   sas2= spreadActions( objlen= [size.x,size.y], spreadlen=[L,W], n = [4,3]);
   for(i=range(sas2)) Transforms(sas2[i]) Obj();
   
   
   dx = -X*0.8;
   color("orange",0.3) Plane( [ dx, Z*H+dx, Y*W+Z*H+dx, Y*W+dx ] );
   sasyz= spreadActions( objlen= [size.y,size.z], spreadlen=[W,H]
                       , n = [4,3],dir="yz"
                       , offset = dx );
   for(i=range(sasyz)) 
   { 
     Transforms( sasyz[i] ) Obj("red");
   }
   
   
   dy = Y*W+ 0.5*Y;
   color("green",0.2) 
   Plane( [ X*L+dy+size.y*Y, O+dy+size.y*Y, O+dy+H*Z+size.y*Y, X*L+dy+H*Z+size.y*Y ] );
   sasxz= spreadActions( objlen= [size.x,size.z]
                       , spreadlen=[L,H], n = [6,4],dir="xz"
                       , offset= dy);
   for(i=range(sasxz)) 
   { 
     Transforms( sasxz[i]) Obj("teal");
   }
}
//demo_spreadActions_2d();

module demo_spreadActions_3d()
{
   echom("demo_spreadActions_3d()");
   L = 5;
   W = 4;
   H = 5;
   offset = [1,1,-1];
      
   dx = O;
   sas= spreadActions( objlen= size, spreadlen=[L,W,H], n = [4,5,3], offset=offset);
   for(i=range(sas)) 
   { 
     Transforms( concat(sas[i],["t",dx])) Obj();
   }
   
   Cube([L,W,H], color=["gold",0.2], actions=["t",offset]);
      
}
//demo_spreadActions_3d();
