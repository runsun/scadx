// scadx constants


ISLOG = 0;

mm          = 1;      // millimeter
GAPFILLER   = 0.0001; // used to extend len to cover gap

X = [1,0,0];
Y = [0,1,0];
Z = [0,0,1];
O = [0,0,0];
ORIGIN      = [0,0,0];

UNIT_AXES   = [X,Y,Z];
IM          = UNIT_AXES; //Identity matrix (3x3)
ORIGIN_SITE = [X,ORIGIN,Y]; //[-1,1,0]];
ORIGIN_COORD= [ORIGIN,X,Y,Z];


/*
Note: If a symbol displays its code but not the symbol, 
      try wrapping it in _span()
-----------------------------------------------------
echo("&#x2192;")                       "&#x2192;" 
echo("&#x2192;", "&#x2192;")           "&#x2192;","&#x2192;"
echo("&#x2192;", _span("&#x2192;"))    "", ""
-----------------------------------------------------
*/

// ================================================
// Box drawing symbols for _matrix
// https://en.wikipedia.org/wiki/Box-drawing_character
BOX_TL = "&#9487;";
BOX_TR = "&#9491;";
BOX_BL = "&#9495;";
BOX_BR = "&#9499;";
BOX_H = "&#9473;";
BOX_V = "&#9475;";
BOX_V_H= "&#9547;";
BOX_LEFT_T= "&#9507;";
BOX_TOP_T= "&#9523;";
BOX_RIGHT_T= "&#9515;";
BOX_BOTTOM_T= "&#9531;";

// https://www.w3schools.com/charsets/ref_utf_math.asp
MATH_DOT_PROD="&sdot;";
MATH_TENSOR_PROD= "&otimes;";

// ================================================
// Unicode version 11.0
// https://en.wikipedia.org/wiki/Supplemental_Arrows-A
L_ARROW = "&#x27f5;";       // <--
R_ARROW = "&#x27f6;";       // -->
LR_ARROW = "&#x27f7;";      // <->
L_DBL_ARROW = "&#x27f8;";   // <==
R_DBL_ARROW = "&#x27f9;";   // ==>
LR_DBL_ARROW = "&#x27fa;";  // <=>
BL_ARROW = "&#x27fb;";      // <-|
BR_ARROW = "&#x27fc;";      // |=>
BL_DBL_ARROW = "&#x27fd;";  // <=|
BR_DBL_ARROW = "&#x27fe;";  // |=>



_SP = "&nbsp;";
_SP2 = "&nbsp;&nbsp;";
_SP3 = "&nbsp;&nbsp;&nbsp;";
_S43 = "&nbsp;&nbsp;&nbsp;&nbsp;";
_BR = "<br/>";
_BR2 = "<br/><br/>";

//echo( _mono(str(BOX_TL, BOX_H, BOX_TOP_T, BOX_H,BOX_TR)) );
//echo( _mono(str(BOX_V)) );
//echo( _mono(str(BOX_LEFT_T, "&nbsp;", BOX_V_H, "&nbsp;",BOX_RIGHT_T)) );
//echo( _mono(str(BOX_BL, BOX_H, BOX_BOTTOM_T, BOX_H,BOX_BR)) );




// heavy vertical 	&#9474;


//UNIT_AXES   = [[1,0,0],[0,1,0],[0,0,1]];
//IM          = [[1,0,0],[0,1,0],[0,0,1]]; //Identity matrix (3x3)
//ORIGIN      = [0,0,0];
//ORIGIN_SITE = [[1,0,0],ORIGIN,[0,1,0]]; //[-1,1,0]];
//ORIGIN_COORD= [ORIGIN,[1,0,0],[0,1,0],[0,0,1]];


SCALE       = 0.1; // Global scale, reflecting on the font size, line radius, etc
                   // Introduced 2018.1.29, still need time to implant it. 
                   // Applied to Text/LabelPt/Line
LINE_R      = SCALE/5; // Global radius for line. Applied to line, MarkPts, etc

$fn    = 12; 
ZERO   = 1e-5; //1e-10;     // used to chk a value that's supposed to be 0: abs(v)<zero
               // Changed to 1e-6 cos' iscoline (angle?anglePt?) needs it
               // See anglePt_demo_coln() in scadx_demo.scad 2015.6.24
PI     = 3.1415926535897932384626433832795028841971693993751058209;
PHI    = (1+sqrt(5))/2; // golden retangular: 1:PHI
COLORS = ["gold", "red","green","blue", "MediumOrchid"
         , "brown","DarkSlateBlue", "olive", "steelblue", "purple"
         , "darkorange", "darkcyan", "MidnightBlue" //, "DarkSlateBlue"
         // , "RebeccaPurple" <== openscad doesn't recognize this
         //, "deeppink","Magenta" 
         ];   // default colors
         // https://en.wikipedia.org/wiki/Web_colors
   
function colors(i)= COLORS[ i%len(COLORS) ];   
module Colors(i,t=1){ color(colors(i), t) children(); } 
          
A_Z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
a_z = "abcdefghijklmnopqrstuvwxyz";
A_z = str(A_Z,a_z);
A_zu = str(A_z,"_");
0_9 = "0123456789";
0_9d= str(0_9,".");

chDEG = chr(176); // The symbol of degree (tiny superscript round circie)

CUBEFACES=[ [3,2,1,0], [4,5,6,7]
	  , [0,1,5,4], [1,2,6,5]
	  , [2,3,7,6], [3,0,4,7]  
	  ];  
          
/*
   Function names available to calc(). Note: to add functions, add name here,
   and add it's action inside run().
*/
CALC_FUNCS= [
    "abs", "sign", "sin", "cos", "tan", "acos", "asin", "atan", "atan2"
    , "floor", "round", "ceil", "ln"
    //, "len", "let"
    , "log", "pow", "sqrt", "exp", "rands"
    , "min", "max"
    ];

CALC_OPS= ["^","*","/","+","-"];

//CALC_BLOCKS = joinarr(concat( [["(",")"]]
//                    ,  [for(f=CALC_FUNCS) [ str(f,"("), ")"] ] ));
  
CALC_BLOCKS = [ for(i=[0:len(CALC_FUNCS)]) // NOTE: item 0 => ["(",")"]
                 [ str( i==0?"":CALC_FUNCS[i+1],"("),")" ]
              ];

// A common option for shapes
OPS=[ "r" , 0.1
	, "h", 1
	, "w", 1
	, "len",1 
	, "color", undef 
	, "transp", 1
	, "fn", $fn
	, "rotate", 0
	, "rotates", [0,0,0]
	, "translate", [0,0,0]	 
	];
 
scadx_categories=
[
	  "Array"
	, "Doctest"
	, "Geometry"
	, "Hash"
	, "Index"
	, "Inspect"
	, "Math"
	, "Number"
	, "Point"
	, "2D_Object"
	, "3D_Object"
	, "String"
	, "Type"
];


POLY_SAMPLES=
[ "plane", ["pts", [ORIGIN, [0,3,0], [2.5,3,0], [2.5,0,0]] ]
, "non-plane", ["pts", [ORIGIN, [0,3,0], [2.5,3,0], [2.5,0,1]] ]
, "plane pantagon", ["pts", [ORIGIN, [0,2,0], [2,3,0], [3,2,0], [2.5,0,0] ]]
, "nonplane pantagon", ["pts", [ORIGIN, [0,2,-1], [2,3,0], [3,2,0], [2.5,0,1] ]]
, "nonplane pantagon2", ["pts", [ORIGIN, [0,2,2], [2,3,0], [3,2,0], [2.5,0,2] ]]
, "poly_patch", ["pts", [[0.0954915, -0.293893, -0.951057], [0.282301, -0.125689, -0.951057] 
                        , [0.282301, 0.125689, -0.951057], [0.0954915, 0.293893, -0.951057]
                        , [0.25, -0.769421, -0.587785], [0.739074, -0.329057, -0.587785]
                        , [0.739074, 0.329057, -0.587785], [0.25, 0.769421, -0.587785]
                        , [0.309017, -0.951057, 0], [0.913545, -0.406737, 0]
                        , [0.913545, 0.406737, 0], [0.309017, 0.951057, 0]
                        , [0.25, -0.769421, 0.587785], [0.739074, -0.329057, 0.587785]
                        , [0.739074, 0.329057, 0.587785], [0.25, 0.769421, 0.587785]
                        , [0.0954915, -0.293893, 0.951057], [0.282301, -0.125689, 0.951057]
                        , [0.282301, 0.125689, 0.951057], [0.0954915, 0.293893, 0.951057]
                        ]
                ,"faces", [[4, 5, 1, 0], [5, 6, 2, 1], [6, 7, 3, 2], [8, 9, 5, 4]
                          , [9, 10, 6, 5], [10, 11, 7, 6], [12, 13, 9, 8], [13, 14, 10, 9]
                          , [14, 15, 11, 10], [16, 17, 13, 12], [17, 18, 14, 13]
                          , [18, 19, 15, 14]
                          ]
                ]  
, "L_poly", ["pts", [ [3,0,0], ORIGIN, [0,4,0], [1.5,4,0], [1.5,2,0], [3,2,0]
                    , [3,0,1], [0,0,1], [0,4,1], [1.5,4,1], [1.5,2,1], [3,2,1]
                    ]
            ,"faces", [ [0,1,7,6], [7,1,2,8], [2,3,9,8], [3,4,10,9]
                      , [4,5,11,10], [0,6,11,5]
                      , [6,7,8,9,10,11], [5,4,3,2,1,0]
                      ] 
            ] 
, "rod3", ["pts", [[3,0,0], ORIGIN, [1,3,0],[3,0,2], [0,0,2], [1,3,2] ]
          , "faces", rodfaces(3)
          ]  
, "cone4", ["pts", [[3,0,0], [0,-3,0], [-3,0,0], [0,3,0], [0,0,5]]
           ,"faces", faces( shape= "cone", nseg=1) 
           ]  
, "cube", [ "pts", [ [3,0,0], ORIGIN, [0,3,0], [3,3,0] 
                   , [3,0,3], [0,0,3], [0,3,3], [3,3,3] ]
          , "faces",  [[3, 2, 1, 0], [4, 5, 6, 7], [0, 1, 5, 4]
                      , [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]
                      ]
          ]            
, "longcube", [ "pts", [ [2,0,0], ORIGIN, [0,6,0], [2,6,0] 
                       , [2,0,1], [0,0,1], [0,6,1], [2,6,1] ]
          , "faces",  [[3, 2, 1, 0], [4, 5, 6, 7], [0, 1, 5, 4]
                      , [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]
                      ]
          ] 
, "3x3_mesh_3", [ "pts",  [[-0.21, -0.198, -0.49], [0.198, 1.11, 0.398]
                          , [-0.21, 2.13, 0.15], [1.24, -0.100, -0.264]
                          , [1.26, 0.729, -0.316], [0.88, 2.09, 0.173]
                          , [2.07, -0.24, -0.467], [2.27, 1.0377, 0.288]
                          , [2.12, 2.19, 0.267]]
          , "faces",   [[0, 1, 3], [1, 4, 3], [1, 2, 4], [2, 5, 4]
                      , [3, 4, 6], [4, 7, 6], [4, 5, 7], [5, 8, 7]]
          ]                                                                          
, "4x4_mesh_4", [ "pts", [ [0.167432, -0.121429, -0.157176], [-0.228411, 0.984279, -0.213098]
                         , [0.222301, 2.24341, -0.43025], [-0.209068, 2.87681, -0.336333]
                         , [0.782771, 0.265366, 0.315995], [1.17082, 1.06957, -0.301265]
                         , [1.17268, 2.23089, 0.0404906], [0.712367, 2.85974, -0.0429549]
                         , [1.94983, -0.102781, 0.0845342], [2.03727, 0.868525, -0.139603]
                         , [1.85755, 1.71039, -0.250609], [2.17674, 3.1451, 0.181561]
                         , [3.21868, -0.0414034, -0.053141], [2.80275, 1.13893, -0.129871]
                         , [3.14134, 1.86852, -0.207466], [3.27215, 2.97606, -0.403544]
                         ]
           , "faces",  [ [0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [4, 5, 9, 8]
                       , [5, 6, 10, 9], [6, 7, 11, 10], [8, 9, 13, 12], [9, 10, 14, 13]
                       , [10, 11, 15, 14]]
                       ]                                                              
, "4x4_mesh_3", [ "pts", [[-0.125533, -0.108768, -0.381803], [0.167552, 0.815685, -0.0189231]
                         , [0.192314, 1.75868, -0.36021], [0.174524, 3.04846, -0.357288]
                         , [0.891714, 0.294612, 0.218843], [0.728378, 0.780245, -0.161098]
                         , [0.93044, 1.9983, -0.208512], [0.91716, 3.14322, -0.0410461]
                         , [2.28478, -0.205703, -0.0139599], [2.11044, 1.25785, 0.18962]
                         , [2.05241, 2.2289, 0.239665], [1.8519, 3.06835, -0.355956]
                         , [3.27072, -0.189835, 0.243447], [3.09598, 0.863299, -0.366084]
                         , [2.95846, 2.227, -0.126486], [3.03812, 3.2658, -0.234319]
                         ]
                 ,"faces", [[0, 1, 4], [1, 5, 4], [1, 2, 5], [2, 6, 5], [2, 3, 6], [3, 7, 6]
                           , [4, 5, 8], [5, 9, 8], [5, 6, 9], [6, 10, 9], [6, 7, 10]
                           , [7, 11, 10], [8, 9, 12], [9, 13, 12], [9, 10, 13], [10, 14, 13]
                           , [10, 11, 14], [11, 15, 14]
                           ] 
                 ]                                                                     
];


REFS=
[ "polygon_tools",
    ["ProtoRabbit","http://www.protorabbit.nl/flash/polygonrabbit/PolygonRabbit.html"]
];

