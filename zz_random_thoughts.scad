/*
  ishash: hash was used to be just even-number-count array, now it has to be pairs of [<str>,x]
  
  new xtype: to an array, distinguish armong array, hash and nums 
  
  
  



*/



/*
  Fixed minor Color() issue. Add feature to istype to allow it to take both float and int as type num. 


*/

/*
 func_mod( arg, opt=["argname",arg] )
 
 arg type based on the relationship between arg and opt.arg:
 
 args  + Simple -- x=1, x=2
 
                 -- arg replaces opt.arg
                 Handled by und_opt(arg,opt,varname,df)
                            
       + Pair -- only for color="red" | ["red",0.5]  
       
                 -- arg replaces opt.arg
                 -- make sure it's an array: [color, transp]    
                 Handled by Color(colorArg,opt,df)
                  
       + extended -- only for actions
       
                 -- arg extended to opt.arg
                 
                 Handled by und_opt_concat(arg,opt,varname,df)
                 
       + Compound -- frame, markPts, dim, edge, etc
                 
                 -- opt.arg updated by arg    
 
                
  
function simpleArg(arg,opt,argname,df)=
   arg==undef?hash(opt,argname,df):arg;  


function und_opt_concat(x, opt, varname, df)= 
( 
  let( optdata = hash(opt,varname,df) )
  x==undef?optdata: concat(isarr(optdata)?optdata:[], x) 
);

function extendedArg(arg,opt,argname,df)=
(   
   let( optarg = hash(opt,argname,df) )
   arg==undef?
     optarg
   : concat( isarr(optarg)?optarg:[], arg) 
);

function compoundArg(arg,opt,argname,df)=
(
   let( optarg = hash(opt,argname,df) )
   arg==undef?
     optarg
   : update( isarr(optarg)?optarg:[], arg) 
);

*/