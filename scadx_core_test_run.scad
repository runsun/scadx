include <scadx_core_test.scad>

module doctest_tools_tests( 
            title="Doctest of doctest tools"
            , mode=MODE
            , opt=[] 
            , FOR=FOR
            )
{
    //echo(str("doctest_tools_tests(), FOR= ",FOR_MAP[FOR]));
       
    opt = dt_update( ["showmode",false,"silent",SILENT], opt );
                
    results= [
        dt_arrprn_test( mode,opt )
       ,dt_begwith_test( mode,opt )
       ,dt_combine_test( mode,opt )
       ,dt_countType_test( mode, opt )
       ,dt_countStr_test( mode,opt )
       ,dt_diff_test( mode, opt )
       ,dt_fmt_test( mode, opt )
       ,dt_hash_test( mode, opt )
       ,dt_index_test( mode, opt )
       ,dt_inrange_test( mode, opt )
       ,dt_isarr_test( mode, opt )
       ,dt_isequal_test( mode, opt )
       ,dt_isint_test( mode, opt )
       ,dt_isnum_test( mode, opt )
       ,dt_isstr_test( mode, opt )
       ,dt_join_test( mode, opt )
       ,dt_keys_test( mode, opt )
       ,dt_or_test( mode, opt )
       ,dt_repeat_test( mode, opt )
       ,dt_replace_test( mode, opt )
       ,dt_s_test( mode, opt )
       ,dt_slice_test( mode, opt )
       ,dt_split_test( mode, opt )
       ,dt_sum_test( mode, opt )
       ,dt_type_test( mode, opt )
       ,dt_update_test( mode, opt )
    ];    
    
    doctests( title=title
            , subtitle= str("doctest_tool version: ", DOCTEST_TOOLS_VERSION)
            , mode=MODE
            , results=results
            , htmlstyle=htmlstyle
            , FOR=FOR );
}