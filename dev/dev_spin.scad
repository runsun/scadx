include <../scadx.scad>
/*
-- 2018.6.1
   
   Dev an action (called "spin" here) that goes through 2 rotations 
   about 2 axis that has an intersection.  
   
   Ref:
   1. scadx_geometry.scad/anglePt()
   2. http://forum.openscad.org/rotate-45-45-0-td24013.html
*/

module dev_spin_setup()
{
   echom("dev_spin_setup()");
   pqr = randPts(3);
   //pqr =[ [-1.58,-0.11,-1.56], [0.41,1.62,0.93], [-0.52,-1.65,-0.04]];
   // pqr=[ [1.76,-2.81,2.28], [2.09,-1.21,-1.77], [-2.03,1.05,2.87]];
   pqr=[ [0.24,-0.45,1.57], [1.69,0.95,1.88], [-1.31,-1.75,-2.31]];
   echopts(pqr, "pqr=");
   MarkCenter(pqr, "pqr");
   MarkPts(pqr, Label="PQR");
   Plane(pqr);
   
   pqr2 = rotPts( pqr, [N(pqr),pqr[1]], 90);
//   MarkPts(pqr2, color="red");
//   Plane(pqr2, color="red");

   pqr3= rotPts( pqr2, [pqr2[0],pqr2[1]], 120);
   MarkCenter(pqr3, "pqr3 = final");
   color("gray")MarkPts(pqr3, Label=["text",["P3","Q3","R3"], "indent",0.5]);
   Plane(pqr3, color="gray");
   
}
//dev_spin_setup();

module dev_spin_setup2()
{
   echom("dev_spin_setup2()");
   //
   // Setup
   //
   pqr = randPts(3);
   //pqr =[ [-1.58,-0.11,-1.56], [0.41,1.62,0.93], [-0.52,-1.65,-0.04]];
   // pqr=[ [1.76,-2.81,2.28], [2.09,-1.21,-1.77], [-2.03,1.05,2.87]];
   pqr=[ [0.24,-0.45,1.57], [1.69,0.95,1.88], [-1.31,-1.75,-2.31]];
   echopts(pqr, "pqr=");
   MarkCenter(pqr, "pqr");
   MarkPts(pqr, Label="PQR");
   Plane(pqr);
   
   pqr2 = rotPts( pqr, [N(pqr),pqr[1]], 90);

   pqr3= rotPts( pqr2, [pqr2[0],pqr2[1]], 120);
   MarkCenter(pqr3, "pqr3 = final");
   color("gray")MarkPts(pqr3, Label=["text",["P3","Q3","R3"], "indent",0.5]);
   Plane(pqr3, color="gray");

   //
   // Setup 2
   //
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   P3= pqr3[0]; Q3=pqr3[1]; R3=pqr3[2];
   arc_P_P3 = arcPts( [P,Q,P3] );
   Line(arc_P_P3);
   
   M = angleBisectPt( [P,Q,P3], len=3 );
   color("red"){
     MarkPt(M,"M = angleBisectPt of [P,Q,P3]", color="purple");
     Arrow( [Q, M], len=5 );
     MarkPt( onlinePt([Q,M], len=5)
           , "Axis QM can rotate P to P3"
           );
   }
   
   N = N([Q,M,P],len=3);
   color("green"){
     MarkPt(N,"N", color="purple");
     Arrow( [M,N] );
   }
   Plane( [M,N,Q], color="green");
   Mark90( [N,M,Q] );
   
}
//dev_spin_setup2();

module dev_spin_setup3()
{
   echom("dev_spin_setup3()");
   //
   // Setup
   //
   pqr = randPts(3);
   //pqr =[ [-1.58,-0.11,-1.56], [0.41,1.62,0.93], [-0.52,-1.65,-0.04]];
   // pqr=[ [1.76,-2.81,2.28], [2.09,-1.21,-1.77], [-2.03,1.05,2.87]];
   pqr=[ [0.24,-0.45,1.57], [1.69,0.95,1.88], [-1.31,-1.75,-2.31]];
   echopts(pqr, "pqr=");
   MarkCenter(pqr, "pqr");
   MarkPts(pqr, Label="PQR");
   Plane(pqr);
   
   pqr2 = rotPts( pqr, [N(pqr),pqr[1]], 90);

   pqr3= rotPts( pqr2, [pqr2[0],pqr2[1]], 120);
   MarkCenter(pqr3, "pqr3 = final");
   color("gray")MarkPts(pqr3, Label=["text",["P3","Q3","R3"], "indent",0.5]);
   Plane(pqr3, color="gray");

   //
   // Setup 2
   //
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   P3= pqr3[0]; Q3=pqr3[1]; R3=pqr3[2];
   arc_P_P3 = arcPts( [P,Q,P3] );
   Line(arc_P_P3);
   
   M = angleBisectPt( [P,Q,P3], len=3 );
   color("red"){
     MarkPt(M,"M = angleBisectPt of [P,Q,P3]", color="purple");
     Arrow( [Q, M], len=5 );
     MarkPt( onlinePt([Q,M], len=5)
           , "Axis QM can rotate P to P3"
           );
   }   
   N = N([Q,M,P],len=3);
   color("green"){
     MarkPt(N,"N", color="purple");
     Arrow( [M,N] );
   }
   Plane( [M,N,Q], color="green");
   Mark90( [N,M,Q] );
   
   //
   // Setup 3
   //
   arc_M_N = arcPts( [M,Q,N], a=90, rad=3, n=6);
   Line(arc_M_N);
   for(pi=range(arc_M_N)){
     if(pi>0) Arrow( [Q,arc_M_N[pi]], color=COLORS[pi] );
   }
   MarkPts(arc_M_N, Label=["text",["a0","a1","a2","a3","a4","a5"]]);
   
   MarkPt( last(arc_M_N,2)
         , Label=["text"
           ,"Chk how pqr rotates about different axes on NMQ   "
           ,"halign","right"
           , "color","green"
           ]
         );     
}
//dev_spin_setup3();

module dev_spin_rotations()
{
   echom("dev_spin_rotations()");
   //
   // Setup
   //
   pqr = randPts(3);
   //pqr =[ [-1.58,-0.11,-1.56], [0.41,1.62,0.93], [-0.52,-1.65,-0.04]];
   // pqr=[ [1.76,-2.81,2.28], [2.09,-1.21,-1.77], [-2.03,1.05,2.87]];
   //pqr=[ [0.24,-0.45,1.57], [1.69,0.95,1.88], [-1.31,-1.75,-2.31]];
   pqr=[ [-1.31,-1.75,-2.31], [1.69,0.95,1.88], [0.24,-0.45,1.57]];
   echopts(pqr, "pqr=");
   MarkCenter(pqr, "pqr");
   MarkPts(pqr, Label="PQR");
   Plane(pqr);
   
   pqr2 = rotPts( pqr, [N(pqr),pqr[1]], 90);

   pqr3= rotPts( pqr2, [pqr2[0],pqr2[1]], 120);
   MarkCenter(pqr3, "pqr3 = final");
   //MarkPts(pqr3, Label=["text",["P3","Q3","R3"], "indent",0.5], color="blue");
   //Plane(pqr3, color="gray");
   color("gray")MarkPts(pqr3, Label=["text",["P3","Q3","R3"], "indent",0.5]);
   Plane(pqr3, color="gray");
   

   //
   // Setup 2
   //
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   P3= pqr3[0]; Q3=pqr3[1]; R3=pqr3[2];
   arc_P_P3 = arcPts( [P,Q,P3] );
   Line(arc_P_P3);
   
   M = angleBisectPt( [P,Q,P3], len=3 );
   color("red"){
     MarkPt(M,"M = angleBisectPt of [P,Q,P3]", color="purple");
     Arrow( [Q, M], len=5 );
     MarkPt( onlinePt([Q,M], len=5)
           , "Axis QM can rotate P to P3"
           );
   }   
   N = N([Q,M,P],len=3);
   //color("green"){
   //  MarkPt(N,"N", color="purple");
   //  Arrow( [M,N] );
   //}
   //Plane( [M,N,Q], color="green");
   //Mark90( [N,M,Q] );
   
   //
   // Setup 3
   //
   n = 12;
   arc_M_N = arcPts( [M,Q,N], a=270, rad=3, n=n);
   Line(arc_M_N);
   for(pi=range(arc_M_N)){
     if(pi>0) Arrow( [Q,arc_M_N[pi]], color=COLORS[pi] );
   }
   MarkPts(arc_M_N, Label=["text",["a0","a1","a2","a3","a4","a5"]]);
   
   MarkPt( last(arc_M_N,2)
         , Label=["text"
           ,"Chk how pqr rotates about different axes on NMQ   "
           ,"halign","right"
           , "color","green"
           ]
         );
   
   // 
   // Rotations
   //
   rotpts=[ for(i=range(arc_M_N))
            rotPts( pqr, [ Q, arc_M_N[i]], a=180/(n-1))
          ];
   for(i=range(arc_M_N))
   {
      pts = rotpts[i];
      MarkPt( pts[0], i);
      Plane(pts, color=[COLORS[i+1],1]);
   } 
   
   Line( [for(pts=rotpts) pts[0]] );           
}
//dev_spin_rotations();

module dev_spin_try2()
{
   echom("dev_spin_try2()");
   
   //--- dev ---
   Draw_arcs();
   Proj_R3_PQP3();
   
   //=========================== dev ===============================
   //
   // Setup
   // 
   pqr = randPts(3);
   //pqr =[ [-1.58,-0.11,-1.56], [0.41,1.62,0.93], [-0.52,-1.65,-0.04]];
   // pqr=[ [1.76,-2.81,2.28], [2.09,-1.21,-1.77], [-2.03,1.05,2.87]];
   pqr=[ [-1.31,-1.75,-2.31], [1.69,0.95,1.88], [0.24,-0.45,1.57] ];
   echopts(pqr, "pqr=");
   MarkCenter(pqr, "pqr");
   MarkPts(pqr, Label="PQR");
   Plane(pqr);
   P = pqr[0]; Q=pqr[1]; R=pqr[2];
   
   pqr2 = rotPts( pqr, [N(pqr),pqr[1]], 90);
//   MarkPts(pqr2, color="red");
//   Plane(pqr2, color="red");

   pqr3= rotPts( pqr2, [pqr2[0],pqr2[1]], 120);
   P3 = pqr3[0]; Q3=pqr3[1]; R3=pqr3[2];
   MarkCenter(pqr3, "pqr3 = final");
   color("gray")MarkPts(pqr3,Line=false, Label=["text",["P3","Q3","R3"], "indent",-0.5]);
   Plane(pqr3, color="gray");
   
   //
   //  Draw arcs 
   // 
   
   module Draw_arcs()
   {
      echom(_red("Draw_arcs()"));
      echo(_red("Draw 2 arcs, 1 for P-P3, 1 for R-R3"));
   
      arcP = arcPts( [P,Q,P3] );
      arcR = arcPts( [R,Q,R3] );
      
      color("red")
      {
        ArcArrow([P,P3,Q]);
        ArcArrow([R,R3,Q]);
      }
   }
   
   module Proj_R3_PQP3()
   {
      echom(_color("Proj_R3_PQP3()","darkgreen"));
      JR3= projPt(R3, [P,Q,P3]);
      J = onlinePt( [Q,JR3], len= dist(Q,R) );
      isSameSide_J_P3 = isSameSide( [J,P3], [P,Q3,N([P,Q3,P3])] );
      echo(isSameSide_J_P3=isSameSide_J_P3); 
        
      color("green")
      {
        MarkPt(JR3, "JR3");
        //Mark90( [P,JR3,R3] );
        Mark90( [Q,JR3,R3] );
        //Mark90( [P3,JR3,R3] );
      
        MarkPt(J, "J");
      }
      echo(_color("When we rot P to P3, we want to rot R to J by aRQJ", "darkgreen"));
      echo(_color("and then from J to R3 by aJQR3, we want to rot R to J by aRQJ", "darkgreen"));
      echo(_color("All other pts should also go through aRQJ and aJQR3 rotations", "darkgreen"));
  
      rot_R_R3(J);
   }
   
   module rot_R_R3(J)
   {
      echom(_blue("rot_R_R3()"));
      MarkPts( [R,Q,J], Ball=false, Label=false
             , Line=["r",0.1, "color", ["blue",0.3]] );
      R3x = anglePt( [R,Q,J]
                   , a= angle([R,Q,J])
                   , a2= angle([J,Q,R3])
                   , len = dist( Q,R )
                   );
      echo(aRQJ=angle([R,Q,J]));
      echo(aJQR3=angle([J,Q,R3]));
      color("blue")
      {
        ArcArrow( [R,J,Q] );
        ArcArrow( [J,R3x, Q] );
      }             
      MarkPt( R3x, "R3x", r=0.1, color=["blue",0.3]);   
   }
   
}
//dev_spin_try2();

module dev_spin_try2_random_site()
{
   echom("dev_spin_try2_random_site()");
   echo("Previous one, dev_spin_try2(), makes pqr3 by 2 rots from pqr." );
   echo("This one will try more random pqr and pqr3(=site)." );
   
   //=========================== dev ===============================
   //
   // Setup
   // 
   pts = randPts(3);
   p4= randPt();
   echo(p4=p4);
   //pts=[ [-0.31, -0.85, -0.91], [1.69,0.95,1.88], [0.24,-0.45,1.57] ];
   //pts=[ [-0.31, -0.85, -0.91], [1.69,0.95,1.88], [0.24,-0.45,1.57], randPt() ];
   pts=[ [-0.31, -0.85, -0.91], [1.69,0.95,1.88], [0.24,-0.45,1.57], [1.81,-1.38,0.32]  ];
   
   site=randPts(3);
   site=[ [2.71,2.95,0.67], [-2.48,2.72,-2.09], [2.42,-1.13,1.32]];
   //site=[ [0.86,1.94,-2.14], [-0.04,2.26,1.51], [0.61,-2.18,2.34]];
   
   
   //--- setup ---
   Setup();
   transfer_pts_to_site();
   Draw_arcs();
   Get_projs();
   Proj_Rs_PQPs();
   Rot_R_Rs();
   //--- app ---
   //Apply_spinPts();
   
   
   
   module Setup()
   {
     echom("Setup()");
     echopts(p4,"p4");
     echopts(pts, "pts");
     //MarkCenter(pts, "pts");
     //MarkPts(pts);
     //Plane(slice(pts,0,3));
     
     echopts(site,"site");
     MarkCenter(site, "site");
     color("gray")
     MarkPts([Ps,Qs,Rs],Line=false
       , Label=["text",["Ps","Qs","Rs"], "indent",-0.5]);
     Plane(site, color="gray");
   }
   
   // pts transfered to site 
       
   pts_t = [for(p=pts) p+ site[1]-pts[1] ];
   P = pts_t[0]; Q=pts_t[1]; R=pts_t[2];
   
   Qs= site[1]; 
   Ps= onlinePt( [Qs,site[0]], len=dist(P,Q) );
   Rs= onlinePt( [Qs,site[2]], len=dist(R,Q) );
   
   plane_P = [P,Q,Ps];
   
   //MarkCenter(pts_t, "pts");
   module transfer_pts_to_site()
   {
       echom(_red("transfer_pts_to_site()"));
       echo(_red("Draw 2 arcs, 1 for P-P3, 1 for R-R3"));
       echopts( pts_t, "pts_t");
       echo(_red(str("[P,Q,R]=",str([P,Q,R]) ) ) );
       echo(_red(str("[Ps,Qs,Rs]=",str([Ps,Qs,Rs]) ) ) );
   
       color("red")
       {
         MarkPts([P,Q,R], "PQR");
         Arrow( [ pts[1],Q]);
         MarkPts( slice(pts_t,2), Label=["text", range(3,len(pts)-3) ] );  
         echo("slice(pts_t,2)=", slice(pts_t,2));
       }
       Plane( [P,Q,R], color=["red",0.2]);
       
   }
   
   //
   //  Draw arcs 
   // 
   
   module Draw_arcs()
   {
      echom(_green("Draw_arcs()"));
      echo(_green("Draw 2 arcs, 1 for P-Ps, 1 for R-Rs"));
      echo(_green("The plane_P, [P,Q,Ps], will be the plane of rotations for the rest pts."));
      echo(_green(str("[P,Ps,Q]=",str([P,Ps,Q]) ) ) );
      
      color("green")
      {
        ArcArrow([P,Ps,Q]);
        ArcArrow([R,Rs,Q]);
      }
      Plane([ P,Ps,Q], color=["green",0.3]);
      
   }
   JR = projPt(R,plane_P);
   JRs= projPt(Rs, plane_P);
   
   JR_true = onlinePt([Q,JR], len=dist(Q,R) );
   JRs_true= onlinePt([Q,JRs], len=dist(Q,R) );
   
    //_J = onlinePt( [Q,JRs], len= dist(Q,R) );
    //isSameSide_J_Ps = isSameSide( [_J,Ps], [P,Qs,N([P,Qs,Ps])] );
    //isSameSide_J_Ps = isSameSide( [_J,Ps], [P,Qs,R] );
    //echo(isSameSide_J_Ps_over_PNQs= _blue(isSameSide_J_Ps)  ); 
    //J= isSameSide_J_Ps? _J:
          //projPt(_J, [P,Qs,N([P,Qs,Ps])]);
          //let(JJ=projPt(_J, [P,Qs,R]))
          //onlinePt( [JJ,_J], len=-dist(JJ,_J) ) ;
           //
    //J = projPt( Rs, [R,Q,Ps] );  
   
   module Get_projs()
   {
      echom(_blue("Get_projs()"));
      echom(_blue("Make JR,JRs the projs of R,Rs onto plane_P"));
      echom(_blue("From JR,JRs we get JR_true and JR_true "));
      echom(_blue("that are to be used to calc the needed rot angle"));
      color("blue")
      {
        MarkPts( [JR,JRs,JR_true,JRs_true]
                , Line=false, Label=["text", ["JR","JRs","JR_true","JRs_true"]] );
        Mark90( [Q,JR,R] );
        Mark90( [Q,JRs,Rs] );
        ArcArrow( [JR_true,JRs_true, Q] );
      }  
   
   }
   
   module Proj_Rs_PQPs()
   {
      echom(_blue("Proj_Rs_PQPs()"));
      
      //echo(_blue("When we rot P to Ps, we want to rot R to J by aRQJ"));
      //echo(_blue("and then from J to Rs by aJQRs, we want to rot R to J by aRQJ"));
      //echo(_blue("All other pts should also go through aRQJ and aJQRs rotations"));
  
      //rot_R_Rs(J);
   }
   
   a= angle([JR_true,Q,JRs_true]);
   a2= angle([JRs_true,Q,Rs]);
   Rfinal = anglePt( [JR_true,Q,JRs_true]
                   , a= a
                   , a2= a2
                   , len = dist( Q,R )
                   );
   
   module Rot_R_Rs(J)
   {
      echom(_purple("Rot_R_Rs()"));
      
      //echo(aRQJ=angle([R,Q,J_true]));
      //echo(aJQRs=angle([J_true,Q,Rs]));
      color("purple")
      {
        //ArcArrow( [R,J,Q] );
        ArcArrow( [JRs_true,Rfinal, Q] );
      }             
      MarkPt( Rfinal, "Rfinal", r=0.1, color=["purple",0.3]);   
   }
   
   //=========================== app ===============================
   
   function spinPts( pts, site, debug=1 )=
   (
      let( pts2= [for(p=pts) p+ site[0]-pts[0] ] // Moved pts to "site"
         , P = pts2[0]
         , Q = pts2[1]
         , R = pts2[2]
         , Pe = site[0]  // e=end
         , Qe = site[1]
         , Re = site[2]
         //, a0 = angle( [P,Q,Pe] ) // angle for the first pt
         , JRe= projPt(Re, [P,Q,Pe])
         , J = onlinePt( [Q,JRe], len= dist(Q,R) )
         , aRQJ = angle( [R,Q,J] )
         , aJQRe = angle( [J,Q,Re] )
         , rtn= [ onlinePt( [Q,site[0]], len=dist(P,Q) )
                , Q
                , each [ for(i= [2:len(pts)-2])
                          anglePt( [R,Q,J], a=aRQJ, a2=aJQRe, len=dist( pts[i],Q ) )
                       ]
                ]              
         )
     debug?
       echo(_purple("debugging spinPts"))
       echo(pts=pts)  
       echo(site=site)
       echo(pts2=pts2)
       echo(JRe=JRe)
       echo(aRQJ=aRQJ)
       echo(aJQRe=aJQRe)
       echo(rtn=rtn)
       rtn
     : rtn      
   );
   
   module Apply_spinPts()
   {
      echom(_purple("Apply_spinPts()"));
      _pqr = [for(p=pqr) p+[3,1,0]];
      pts_final = spinPts( _pqr, pqr3 );
      echo(pts_final = pts_final );
      
      Plane(pts_final, color="purple");
   
      ptsx = [[-1.19729, -2.09511, 5.90951], [1.80271, 0.604893, 10.0995], [0.352709, -0.795107, 9.78951]];
      MarkPts(ptsx);
   
   }
   
}
dev_spin_try2_random_site();
