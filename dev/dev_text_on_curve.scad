include <../scadx.scad>

module Write_on_curve_dev(){
  echom("Write_on_curve_dev: // Write on a curve"); 

  X = [1,0,0]; Y=[0,1,0]; Z=[0,0,1];  
 
  txt = "Curved Scad.x   ";
  w = textWidth(txt,scale=1,size=10);
    
      /// dim the txt width:  
      pqr =[ [0,-10,0],[w,-10,0], -20*Y ];
      echo( pqr = pqr,w=w ); 
      //Dim( pqr );
  
  /// array of char widths  
  ws = [ for(i=range(txt)) textWidth(txt[i],scale=1,size=10) ];
  echo( sum_of_ws = sum(ws), ws=ws
      , dotwidth=textWidth(".",scale=1,size=10) );
  
  /// curve center  
  rad = 30;  
  O = [ w/2,0,-rad];
  J = [ w/2,0,0];
  //MarkPts([O,J],"r=0.5;ch=[r,0.1];l=OJ");
  
  /// rotaxis
  N = N( [ORIGIN,O,J]);
  rotaxis = linePts([O,N],8);
  //Arrow( rotaxis, "r=0.3;cl=yellow" );
    
  nseg = 10; /// resolution
  
  /// angle span
  cirlen = 2*PI*rad; 
  angle_span = 360 * w / cirlen; 

  // write txt to make txt surface on the circle plane 
  for( i = range(txt) ){
     l = ws[i];
     lenb4 = i==0?0:sum(slice(ws,0,i));   
     ang =  360 * lenb4 / cirlen; 
     P= anglePt( [J,O,ORIGIN], angle_span/2- ang );
     MarkPt(P,"r=0.5"); 
     M = anglePt( [O,P,[ 100,0,0]],a=90, len=rad/2);
     MarkPts( [O,P,M] );     
     Write( txt[i], [M,P, extPt([O,P]) ], scale=1,size=10);
     echo(ch=txt[i], l, ang);

  }    

  /// write txt to make txt surface 90d to the circle plane 
//  for( i = range(txt) ){
//     l = ws[i];
//     lenb4 = i==0?0:sum(slice(ws,0,i));   
//     ang =  360 * lenb4 / cirlen; 
//     P= anglePt( [J,O,ORIGIN], angle_span/2- ang );
//     MarkPt(P,"r=0.5"); 
//     M = anglePt( [O,P,[ 100,0,0]],a=90, len=rad/2);
//     
//     Write( txt[i], halign="center", valign="center"
//          , [for(p= [M,P, P+[0,1,0] ]) p+[0,-15,-15]]
//          , scale=1,size=10);
//  }  

  module Txt(){  
    //linear_extrude(-5)  
    translate( [0,20,0] )  
    text( txt); //, valign="center");
  }  
  intv = 1;
  Txt();
   
    
    
}    
Write_on_curve_dev();
