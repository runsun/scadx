include <scadx_obj_basic_dev.scad>



module alignface_try()
{
	p0 = [2,2,1];
	pqr1 = [ p0, [4,1,2], [3,2,-1]];
	pqr2 = [ p0, [3,0,-1], [1,2,0]];

	
	Chain(pqr1, r=0.01, closed=true);
	color("blue")
	Chain(pqr2, r=0.01, closed=true);

	echo( "planecoefs pqr1: ", planecoefs(pqr1));
	echo( "planecoefs pqr2: ", planecoefs(pqr2));

	pc1 = planecoefs([pqr1[0]-p0,pqr1[1]-p0,pqr1[2]-p0]);
	pc2 = planecoefs([pqr2[0]-p0,pqr2[1]-p0,pqr2[2]-p0]);
	echo( "planecoefs pqr1-p0: ", pc1);
	echo( "planecoefs pqr2-p0: ", pc2);

	translate(-p0)
	//rotate( pc1)
	{color("khaki")
	rotate(slice(pc1,0))//,-1))
	Chain(pqr1, r=0.02, closed=true);
	color("lightblue")
	Chain(pqr2, r=0.02, closed=true);
 	}

	polyhedron( points = pqr1, faces=[[0,1,2]] );
	polyhedron( points = pqr2, faces=[[0,1,2]] );
	PtGrid(p0);
}
//alignface_try();


module FontWindthCheck(s){ // THIS IS TOO TROUBLSOME
    echom("FontSizeCheck");
    
    O=ORIGIN;
    
    ss = repeat(s,10);
    text( ss); //, valign="center");
    //w = guessTextWidth( ss );
    w = textWidth( ss );
    echo( ss=ss, w=w );
    
    //translate( [w/2,5,0]) text(ss);

    //------------------------------------------
    beg2= 6*w/10;// floor(w)-5;         
    color("red")
    for( i =[0:0.2:1.4] ){
        MarkPts( [[i+beg2 ,-3,0], [i+beg2 ,12,0]], ["chain",["r",0.01], "ball",0] );
         //MarkPt(  [i-0.02 ,10.1,0],["ball",0, "label",["text",str(i),"scale",0.01]]);
         Write( i, [[i+beg2 ,11.1,0], [i+beg2 ,10.1,0], [-200,0,0]]
              , opt=["size",1, "scale",0.15, "valign","center"] );
    }    
    
    //------------------------------------------
    beg3= 0; //floor(w);
    color("purple")
    for( i =[0:0.2:1.4] ){
        MarkPts( [[i ,-3,0], [i ,12,0]], ["chain",["r",0.01,"color","purple"], "ball",0] );
         //MarkPt(  [i-0.02 ,10.1,0],["ball",0, "label",["text",str(i),"scale",0.01]]);
         Write( i, [[i ,11.1,0], [i ,10.1,0], [-200,0,0]]
              , opt=["size",1, "scale",0.1,"color","purple", "valign","center"] );
    }    
    

      
    //------------------------------------------       MarkPts( [ [w,-1,0], [0,-1,0]], "ch=[r,0.5,color,red];ball=0");
    
    color("blue")
             
    for( i =[0:10] ){
        MarkPts( [[i*w/10 ,-3,0], [i*w/10 ,12,0]], ["chain",["r",0.05,"color","blue"], "ball",0] );
        //Write( 
        
        MarkPt( [i*w/10-0.1 ,-1.2,0],["ball",0, "label",["text",str(i),"scale",0.1]]);
    }    
        
               
}    

//FontWindthCheck(s="9 ");

module verifyFontWidth(){
    echom("verifyFontWidth");
    
    /*
      
      Guessed width too large: 
         TdVFkPAbyP, obmYqrIzMC, vERQkUDVaS, VUaLVONanB, iYustNDGGg
         a~a(0EGYo2, 
      Guessed width too small: 
         VmPRRZLDof, mo1B X\!fS, ,pfprQ}EX}, DXrp010D&S, {i$D]QGtT3 
         Gt2&!&7x{[, r_{:}t,v"x, 5]#;H}9A&z
    
      But if we add some spacing (= sp below), then it looks ok
    
    */ 
    function randtext( n= 10
                     , chrIndexRange= concat( range(32, 127))
                     , _ret=""
                     )=
    (
        let( ch = chr(rand( chrIndexRange ))
           , _ret= str(_ret,ch)
           , n= n-1
           )
        n==0? _ret: randtext(n,chrIndexRange,_ret)
    );
    
    for(i=range(5)){
        txt = randtext();
        scale = 0.8;
        size = 15;
        bw=3;  // border width
        
        //w = guessTextWidth(txt, scale=scale, size=size);
        w = textWidth(txt, scale=scale, size=size);
        spaceratio = 0.34;
        gap = 15;
        z = 0; //-gap*i;
        h = size*scale; //*(1+spaceratio*2);
        sp = h*spaceratio;
        y = -(gap+sp*2+bw*2)*i;
        asciis = join([for(i=range(txt)) asc(txt[i])],",");
        echo( asciis, ": ", txt );
        
        linear_extrude(8){
            
            translate( [sp*2,y,0])
            scale(scale)
            text(txt, size=size);
           
            
            // Draw box around text to show covered area
            TL = [sp+bw, y+h+sp ];
            TR = [w+sp*3, y+h+sp];
            BR = [w+sp*3, y-sp];
            BL = [sp+bw,y-sp];
            
            TLo= [ sp, y+h+sp+bw ];
            TRo= [ w+sp*3+bw, y+h+sp+bw ];
            BRo= [ w+sp*3+bw, y-sp-bw ];
            BLo= [ sp, y-sp-bw];
            
            //echo( addz([BL,TL,TR,BR,BLo,TLo,TRo,BRo]));
            //MarkPts( addz([BLo,BRo,TRo,TLo, BL,BR,TR,TL],0), "r=1;l=[scale,0.4,color,black]");
            
            polygon( points= //[BL,TL,TR,BR,BLo,TLo,TRo,BRo]
                             [BLo,BRo,TRo,TLo, BL,BR,TR,TL]
                            , paths=[ [0,1,2,3], [4,5,6,7]
                                   ]  );
        }
        
        /* 
        // Draw vertical lines to show width for EACH char. This is slow:
        
        ws = [ for(i=range(txt)) guessTextWidth(txt[i]) ];
        accws = accum(ws); //concat([0],accum(ws));
        echo(accws = accws);
        for(x=accws){ MarkPts( [[x,y,z], [x,y+10,z]]
                             , , "ch=[r,0.1,closed,true];pl=[transp,0.2];ball=0" ); 
        }
        */
    }    
} 
//verifyFontWidth();


RodCircle=["RodCircle", "ops", "n/a", "3D, Shape",
"
 Draw rods around a circle based on the given ops. Other than the
;; settings defined in the common ops OPS, this ops has the following
;; defaults:
;;
;; [ ''style'', ''rod'' // rod|block
;; , ''count'',  12
;; , ''normal'', [0,0,1] // the circle normal
;; , ''target'', ORIGIN  // the circle center
;; , ''dir''  , ''out'' // direction(out|in|up|down)
;; , ''radius'', 3
;; , ''r''    ,  0.1    // for style = rod
;; , ''w''    ,  0.4    // for style = block
;; , ''h''    ,  0.5    // for style = block
;; , ''markcolor'',false  // example:[1,''red'',4,''blue'']
;; , ''rodrotate'', 0
;;      // Rotation of individual rods about its own axis. Only works
;;      // for style=block. Note that OPS has a ''rotate'',too, that 
;;      // would be for the rotate of entire RodCircle about circle axis.
;; ]
"];



module RodCircle( ops )
{
	
	//echom("RodCircle");
	ops=updates( OPS,[
		[ "style", "rod"     // rod|block|cone
		, "count",  12  
		, "normal", [0,0,1]
		, "target", ORIGIN 
		, "dir"  , "out"     // out|in|up|down  (outward|inward...)
		, "radius", 3  
		, "r"    ,  0.1      // for style = rod
		, "w"    ,  0.4      // for style = bar
		, "h"    ,  0.5      // for style = bar
		, "markcolor",false  // [1,"red",4,"blue"]
		, "rodrotate", 0     // Rotation of individual rod about its axis. 
						   // Only works
						   //  for style=block. Note that OPS has a 
						   //  "rotate",too, that would be for the
						   //  rotate of entire RodCircle about its axis.
				
		], ops 
		]);	
	function ops(k)= hash(ops,k);

	style = ops("style");
	count = ops("count");
	color = ops("color");
	transp= ops("transp");
	fn    = ops("fn");
	normal= ops("normal");
	target= ops("target");
	dir   = ops("dir");
	rlen  = ops("len");
	r     = ops("r");
	h     = ops("h");
	radius= ops("radius");
	markcolor = ops("markcolor");
	rodrotate= ops("rodrotate");

	isupdown = dir=="up" || dir=="down";

	//echo("ops = ", ops );
	//echo("isupdown? ", isupdown);

	// For style = up|down, rods align with the normal. Because of the order
	// of the points that form the normal (or, the direction of the normal),
	// going upward or downward as desired requires special treatment. What
	// we do here is to, kind of, sort the points like what's been done in 
	// Line(), and chk if the two points has to switch, which will be the 
	// factor to set rod direction. 
	pq = [ target, normal + target ];
	switched= minyPts(minxPts(minzPts(pq)))[0]==pq[1];

	//echo("switched? ", switched );

	// Setup the points for the rods. Each rod starts from a begPt
	// and ends in a endPt. These pts are first created as 2d series
	// using arcPts then converted to 3d. All are on the xy-plane.  
	begPts = addz( arcPts( r=radius, a=360, count=ops("count") ) );
	endPts =  dir=="down"? addz( begPts, switched?-rlen:rlen )
		    : dir=="up"? addz( begPts, switched?rlen:-rlen )
			: addz(
				dir=="out"? arcPts( r=radius+rlen, a=360, count=ops("count") )
			            : arcPts( r=radius-rlen, a=360, count=ops("count") ) // in
			  );  

	//echo( "begPts: ", begPts);
	//echo( "endPts: ", endPts);

	//MarkPts( begPts );
	//MarkPts( endPts );

	translate( ops("target") )
	rotate( rotangles_z2p( ops("normal") ) )
	rotate( [0,0, ops("rotate") ] )
	{

	//=============================================
	// Start of translation/rotation
	//=============================================
	if( style=="rod" ){

		for( i= [0:len(begPts)-1] ){
			p= begPts[i];
			q= endPts[i];
			rodcolor= haskey(markcolor,i)
								?hash(markcolor,i) 
								:color;
					
//			  assign( p= begPts[i]
//					,q= endPts[i]
//					, rodcolor= haskey(markcolor,i)
//								?hash(markcolor,i) 
//								:color
//					)
			   //MarkPts( [p,q] );
			   Line( [ p,q ]	
			     , update( ops, ["color", rodcolor, "transp",1] )
					
                   );	
			}
	}
	if( style == "block") {
			for( i= [0:len(begPts)-1] ){
			  assign ( p= begPts[i]
					, q= endPts[i]
					, L= len(begPts)
					, FL=floor( len(begPts) /4) 
						// FL is needed to adjust block orientation 
						// when rotation is set for block
					, rodcolor= haskey(markcolor,i)
								?hash(markcolor,i)
								:color
					 //Extends to cover line-curve gap
					, contactsealer = dir=="out"?
									(radius-shortside(radius, ops("w")/2 )
									+ GAPFILLER)
									:dir=="in"?
									 -radius+shortside(radius, ops("w")/2 )
									+GAPFILLER
									 :0

					){
			//echo("rodcolor = ", rodcolor );
             //echo("outrodmarkcolor = ", outrodmarkcolor ); 
			//echo_( "{_}: pq = {_}", [i, pq] );  
			//echo_("L={_}, FL={_}",[L,FL]); 
			//echo("contactsealer = ", contactsealer );
			//echo_( "isupdown={_}, rodrotate={_}, rotate={_} ", [isupdown, rodrotate, rodrotate+(isupdown?30:0)] );
			   Line( [ p,q ]
				  // , onlinePt( [ pq, ORIGIN ]
				  //             , len= -rlen )
				//	] 
			     ,  update( ops, ["style","block"
							   , "shift", isupdown?"mid2":"center"
								 ,"color",rodcolor
								 ,"width",ops("w")
								  ,"depth",h 
								 // , "markpts", true	
								  ,"extension0", contactsealer												
								, "rotate",  isupdown  // rotate for up/down
											?(i*360/count+rodrotate)
											: (rodrotate*(
												(i>L-FL)||((i>=0)&&(i<=FL))?-1:1 
											  ))+ (i==0?90:0)
											// The last part, rotrotate by 90,
											// is needed for i=0 in block style 

								])
                   );	
			}}
	}


	

	}//=============================================
	//  END of translation/rotation
	//=============================================

}
//echo( [3,4]+repeat([2],2) );
//echo( inc([3,4],2) );

module RodCircle_demo_1_dir()
{
	RodCircle(["markcolor",[0,"red"], "color", "red"]);
	RodCircle(["markcolor",[0,"red"], "dir", "in", "color","lightgreen"]);
	RodCircle(["markcolor",[0,"red"], "dir", "up", "color","blue"]);
	RodCircle(["markcolor",[0,"red"], "dir", "down", "color","purple"]);
}

module RodCircle_demo_2a_normal_out()
{
	echom("RodCircle_demo_2a_normal_out");
	pq = randPts(2);
	radius = rands( 1,20,1)[0]/10;

	RodCircle( [ "normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts(pq);
	Line(pq, ["r", radius
			,"extension0",0.5, "color", randc(), "transp",0.1] );

}

module RodCircle_demo_2b_normal_in()
{
	echom("RodCircle_demo_2b_normal_in");
	pq = randPts(2);
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ "dir","in"
		   ,"normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts(pq);
	Line(pq, ["r", radius 
			,"extension0",0.5, "color", randc(), "transp",0.1] );

}

module RodCircle_demo_2c_normal_up()
{
	echom("RodCircle_demo_2c_normal_up");
	pq = randPts(2);
	radius = rands( 6,20,1)[0]/10;

	RodCircle( [ "dir","up"
		   ,"normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts(pq);
	Line0(pq, ["r", radius 
			//,"extension0",0.5
			, "color", randc(), "transp",0.2] );

}

module RodCircle_demo_2d_normal_down()
{
	echom("RodCircle_demo_2d_normal_down");
	pq = randPts(2);
	//echo("pq = ", pq); //pq = minzPt(pq_)==pq_[0]?pq_:reverse(pq_);
	//echo("normal = ", normal );
	radius = rands( 6,20,1)[0]/10;

	RodCircle( [ "dir","down"
		   ,"normal",pq[1]-pq[0]// sign(pq[1].z-pq[0].z)*( pq[1]-pq[0] )
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts( pq );
	Line0(pq, ["r", radius 
			//,"extension0",0.5
			, "color", randc(), "transp",0.2] );	
}

module RodCircle_demo_3a_block_out()
{
	echom("RodCircle_demo_3a_block_out");
	pq = randPts(2);
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.8
			,"markcolor",[0,"red"]
		     , "markpts", true
			] );
	MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0
			, "color", randc()
			, "transp",0.2
			, "markpts", true
			] );


}


module RodCircle_demo_3b_block_in()
{
	echom("RodCircle_demo_3b_block_in");
	pq = randPts(2);
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "in"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.4
			,"h", 1
			,"markcolor",[0,"red"]
		   ] );
	MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0, "color", randc(), "transp",0.2] );

}


module RodCircle_demo_3c_block_up()
{
	echom("RodCircle_demo_3c_block_up");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "up"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.8
			,"markcolor",[0,"red"]
			,"markpts", true
			//,"rotate", 90
		   ] );
	MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0, "color", randc(), "transp",0.2] );
}


module RodCircle_demo_3c2_block_up_rotate()
{
	echom("RodCircle_demo_3c2_block_up_rotate");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "up"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			//,"w", 0.8
			,"markcolor",[0,"red"]
			//,"markpts", true
			,"rodrotate", 30
			,"w", 2
			,"h",0.1
			,"len",2
			,"transp",0.5
			
		   ] );
	//MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0, "color", randc(), "transp",0.2] );
}

module RodCircle_demo_3d_block_down()
{
	echom("RodCircle_demo_3d_block_down");
	pq = randPts(2);
	//pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "down"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.8
			,"markcolor",[0,"red"]
			,"markpts", true
		   ] );
	//MarkPts( pq );
	//Line(pq, ["r", radius
	//		,"extension0",2, "color", randc(), "transp",0.2] );
	Line(pq, ["r", radius,
			"extension0",0
			//, "color", randc()
			, "transp",0.2] );

}


module RodCircle_demo_3d2_block_down_rotate()
{
	echom("RodCircle_demo_3d2_block_down_rotate");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle([ 
			  "style", "block"
			, "dir", "down"
			, "count", 24
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  "blue" //randc()
		   	, "radius", 1 //radius
			, "w", 1.2
			, "h",0.1
			, "len",2
			, "transp",0.5
			, "markcolor",[0,"red"]
			//,"markpts", true
			, "rodrotate", 30
		   ] );
	//MarkPts( pq );
	Line(pq, ["r", radius, 
			"extension0",0
			//, "color", randc()
			, "transp",0.6] );

}


module RodCircle_demo_4a_block_out_rotate()
{
	echom("RodCircle_demo_4a_block_out_rotate");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = 2; //rands( 15,20,1)[0]/10;
/*
L	L/4 
2	.5	-+
3	.75	-++
4	1	-++-
5		--++-
6		--+++-
7		--++++-
8	2	--++++--
9   		---++++--
10  		---+++++--
11  		---++++++--
12  	3	---++++++---
13  		----++++++---
14  		----+++++++---
15  		----++++++++---
16  	4	----++++++++----

-- The last - part: i>=(L-floor(L/4)) 
-- The first - part: i>=1, <floor(L/4) 

*/
	RodCircle( [ 
			  "style", "block"
			, "dir", "out"
			, "count", 12
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  "blue" // randc()
		   	, "radius", radius
			,"w", 2
			,"len",2
			,"h", 0.2
			,"transp", 0.8
			,"markcolor",[0,"red", 9,"green"]
			,"markpts", true
			,"rodrotate", 45
		   ] );
	MarkPts( pq );
	Line(pq, [ "r", radius, 
			"extension0",2, "color", randc(), "transp",0.2] );


}

module RodCircle_demo_4b_block_in_rotate()
{
	echom("RodCircle_demo_4b_block_in_rotate");
	pq = randPts(2);
	//pq = [ORIGIN, [0,0,3]];  
	radius = 2; //rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "in"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  "blue" // randc()
		   	, "radius", radius
			,"w", 1
			,"len",1
			,"h", 0.2
			,"markcolor",[0,"red"]
			,"markpts", true
			,"rodrotate", 45
		   ] );
	MarkPts( pq );
	Line(pq, [ "r", radius, 
			"extension0",2, "color", randc(), "transp",0.2] );

}

module RodCircle_demo()
{
	RodCircle_demo_1_dir();
	//RodCircle_demo_2a_normal_out();
	//RodCircle_demo_2b_normal_in();
	//RodCircle_demo_2c_normal_up();
	//RodCircle_demo_2d_normal_down();
	//RodCircle_demo_3a_block_out();
	//RodCircle_demo_3b_block_in();
	//RodCircle_demo_3c_block_up();
	//RodCircle_demo_3c2_block_up_rotate();
	//RodCircle_demo_3d_block_down();
	//RodCircle_demo_3d2_block_down_rotate();

	//RodCircle_demo_4a_block_out_rotate();
	//RodCircle_demo_4b_block_in_rotate();
}

module RodCircle_test( ops ){ doctest( RodCircle, ops=ops);}

//RodCircle_demo();
//doc(RodCircle);
