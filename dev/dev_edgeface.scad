//include <../scadx.scad>
include <scadx_edgeface.scad>



module dev_DooSabin_face_shrink__plane()
{ 
   echom("dev_DooSabin_face_shrink__plane()");
   size=4;
   pts = [ORIGIN, [0,3,0], [2.5,3,0], [2.5,0,0] ];
   MarkPts(pts, Line=["closed", true]
   );

   nodepairs=[ [ onlinePt( [pts[0], pts[1]], ratio=0.25) 
               , onlinePt( [pts[0], pts[1]], ratio=0.75)]
             , [ onlinePt( [pts[1], pts[2]], ratio=0.25) 
               , onlinePt( [pts[1], pts[2]], ratio=0.75)]
             , [ onlinePt( [pts[2], pts[3]], ratio=0.25) 
               , onlinePt( [pts[2], pts[3]], ratio=0.75)]
             , [ onlinePt( [pts[3], pts[0]], ratio=0.25) 
               , onlinePt( [pts[3], pts[0]], ratio=0.75)]
             ];
   
   MarkPts( [for(np=nodepairs) each np]
          , Label=false
          , Ball=["r",0.05]
          , Line=false);             

   grids = [ [nodepairs[3][1], nodepairs[1][0] ]
           , [nodepairs[0][1], nodepairs[2][0] ]
           , [nodepairs[1][1], nodepairs[3][0] ]
           , [nodepairs[2][1], nodepairs[0][0] ]
           , ];
   for(i=[0:len(grids)-1]) Line(grids[i], r=0.01,color=COLORS[i]); // color=["red",0.2]);        
   
   gridpts = [ intsec( grids[0], grids[3] )
             , intsec( grids[0], grids[1] )
             , intsec( grids[2], grids[1] )
             , intsec( grids[2], grids[3] )
             ];
                    
   MarkPts(gridpts, Line=["closed",true, "r", 0.03,"color", "black"]);          
           
}
//dev_DooSabin_face_shrink__plane();

module dev_DooSabin_face_shrink__plane_formulated()
{ 
   echom("dev_DooSabin_face_shrink__plane_formulated()");
   size=4;
   pts = [ORIGIN, [0,3,0], [2.5,3,0], [2.5,0,0] ];
   lasti = len(pts)-1;
   rng= [0:len(pts)-1];
   rng_1=[0:len(pts)-2];
   
   MarkPts(pts, Line=["closed", true]
   );

   //nodepairs=[ [ onlinePt( [pts[0], pts[1]], ratio=0.25) 
               //, onlinePt( [pts[0], pts[1]], ratio=0.75)]
             //, [ onlinePt( [pts[1], pts[2]], ratio=0.25) 
               //, onlinePt( [pts[1], pts[2]], ratio=0.75)]
             //, [ onlinePt( [pts[2], pts[3]], ratio=0.25) 
               //, onlinePt( [pts[2], pts[3]], ratio=0.75)]
             //, [ onlinePt( [pts[3], pts[0]], ratio=0.25) 
               //, onlinePt( [pts[3], pts[0]], ratio=0.75)]
             //];
   
   nodepairs=[ for(i=rng_1) 
                  [ onlinePt( [pts[i], pts[i+1]], ratio=0.25) 
                  , onlinePt( [pts[i], pts[i+1]], ratio=0.75)
                  ]
             , [ onlinePt( [ last(pts), pts[0]], ratio=0.25) 
               , onlinePt( [ last(pts), pts[0]], ratio=0.75)
               ]                   
             ];
   for(i=rng)  Line(nodepairs[i], r=0.1, color=[COLORS[i],0.5]);
   
   MarkPts( [for(np=nodepairs) each np]
          , Label=false
          , Ball=["r",0.05]
          , Line=false);             

   //grids = [ [ nodepairs[3][1], nodepairs[1][0] ]
           //, [ nodepairs[0][1], nodepairs[2][0] ]
           //, [ nodepairs[1][1], nodepairs[3][0] ]
           //, [ nodepairs[2][1], nodepairs[0][0] ]
           //];
           
   grids = [ for(i=rng) 
             [ nodepairs[ i==0?(len(pts)-1):(i-1) ][1]
             , nodepairs[ i==(len(pts)-1)?0:(i+1) ][0]
             ]
           ];
   
   echo( grids=grids );
                     
   for(i=rng) Line(grids[i], r=0.01,color=COLORS[i]);         
   
   //gridpts = [ intsec( grids[0], grids[3] )
             //, intsec( grids[0], grids[1] )
             //, intsec( grids[2], grids[1] )
             //, intsec( grids[2], grids[3] )
             //];
   gridpts = [ for(i=rng) intsec( grids[i], grids[i==0?lasti:(i-1)]) ];
                    
   MarkPts(gridpts, Line=["closed",true, "r", 0.03,"color", "black"]);          
           
}
//dev_DooSabin_face_shrink__plane_formulated();

module dev_DooSabin_face_shrink__non_plane()
{ 
   echom("dev_DooSabin_face_shrink__non_plane()");
   size=4;
   pts = [ORIGIN, [0,3,0], [2.5,3,0], [2.5,0,1] ];
   lasti = len(pts)-1;
   rng= [0:len(pts)-1];
   rng_1=[0:len(pts)-2];
   
   MarkPts(pts, Line=["closed", true]
   );
   
   nodepairs=[ for(i=rng_1) 
                  [ onlinePt( [pts[i], pts[i+1]], ratio=0.25) 
                  , onlinePt( [pts[i], pts[i+1]], ratio=0.75)
                  ]
             , [ onlinePt( [ last(pts), pts[0]], ratio=0.25) 
               , onlinePt( [ last(pts), pts[0]], ratio=0.75)
               ]                   
             ];
   for(i=rng)  Line(nodepairs[i], r=0.1, color=[COLORS[i],0.5]);
   
   MarkPts( [for(np=nodepairs) each np]
          , Label=false
          , Ball=["r",0.05]
          , Line=false);             

   grids = [ for(i=rng) 
             [ nodepairs[ i==0?(len(pts)-1):(i-1) ][1]
             , nodepairs[ i==(len(pts)-1)?0:(i+1) ][0]
             ]
           ];
   
   echo( grids=grids );
                     
   for(i=rng) Line(grids[i], r=0.01,color=COLORS[i]); 
           
   gridpts = [ for(i=rng) intsec( grids[i], grids[i==0?lasti:(i-1)]) ];
                    
   MarkPts(gridpts, Line=["closed",true, "r", 0.03,"color", "black"]);          
           
}
//dev_DooSabin_face_shrink__non_plane();

module dev_DooSabin_face_shrink__plane_pantagon()
{ 
   echom("dev_DooSabin_face_shrink__plane_pantagon()");
   //size=4;
   pts = [ORIGIN, [0,2,0], [2,3,0], [3,2,0], [2.5,0,0] ];
   lasti = len(pts)-1;
   rng= [0:len(pts)-1];
   rng_1=[0:len(pts)-2];
   
   MarkPts(pts, Line=["closed", true]
   );
   
   nodepairs=[ for(i=rng_1) 
                  [ onlinePt( [pts[i], pts[i+1]], ratio=0.25) 
                  , onlinePt( [pts[i], pts[i+1]], ratio=0.75)
                  ]
             , [ onlinePt( [ last(pts), pts[0]], ratio=0.25) 
               , onlinePt( [ last(pts), pts[0]], ratio=0.75)
               ]                   
             ];
   for(i=rng)  Line(nodepairs[i], r=0.1, color=[COLORS[i],0.5]);
   
   MarkPts( [for(np=nodepairs) each np]
          , Label=false
          , Ball=["r",0.05]
          , Line=false);             

   grids = [ for(i=rng) 
             [ nodepairs[ i==0?(len(pts)-1):(i-1) ][1]
             , nodepairs[ i==(len(pts)-1)?0:(i+1) ][0]
             ]
           ];
   
   echo( grids=grids );
                     
   for(i=rng) Line(grids[i], r=0.01,color=COLORS[i]); 
           
   gridpts = [ for(i=rng) intsec( grids[i], grids[i==0?lasti:(i-1)]) ];
                    
   MarkPts(gridpts, Line=["closed",true, "r", 0.03,"color", "black"]);          
           
}
//dev_DooSabin_face_shrink__plane_pantagon();

module dev_DooSabin_face_shrink__non_plane_pantagon_failed()
{ 
   echom("dev_DooSabin_face_shrink__non_plane_pantagon_failed()");
   //size=4;
   pts = [ORIGIN, [0,2,-1], [2,3,0], [3,2,0], [2.5,0,1] ];
   lasti = len(pts)-1;
   rng= [0:len(pts)-1];
   rng_1=[0:len(pts)-2];
   
   MarkPts(pts, Line=["closed", true]
   );
   
   nodepairs=[ for(i=rng_1) 
                  [ onlinePt( [pts[i], pts[i+1]], ratio=0.25) 
                  , onlinePt( [pts[i], pts[i+1]], ratio=0.75)
                  ]
             , [ onlinePt( [ last(pts), pts[0]], ratio=0.25) 
               , onlinePt( [ last(pts), pts[0]], ratio=0.75)
               ]                   
             ];
   for(i=rng)  Line(nodepairs[i], r=0.1, color=[COLORS[i],0.5]);
   
   MarkPts( [for(np=nodepairs) each np]
          , Label=false
          , Ball=["r",0.05]
          , Line=false);             

   grids = [ for(i=rng) 
             [ nodepairs[ i==0?(len(pts)-1):(i-1) ][1]
             , nodepairs[ i==(len(pts)-1)?0:(i+1) ][0]
             ]
           ];
   
   echo( grids=grids );
                     
   for(i=rng) Line(grids[i], r=0.01,color=COLORS[i]); 
           
   gridpts = [ for(i=rng) intsec( grids[i], grids[i==0?lasti:(i-1)]) ];
   echo(gridpts = gridpts);                 
   MarkPts(gridpts, Line=["closed",true, "r", 0.03,"color", "black"]);          
           
}
//dev_DooSabin_face_shrink__non_plane_pantagon_failed();

module dev_DooSabin_face_shrink__non_plane_pantagon()
{ 
   echom("dev_DooSabin_face_shrink__non_plane_pantagon()");
   /* When it is non-planar, sometimes it's impossible to find the 
      grid pts because of distortion of the plane. In such a case,
      we find the line-bridge and take the mid pt
   */
   
   ratio=0.6;
   
   pts = [ORIGIN, [0,2,2], [2,3,0], [3,2,0], [2.5,0,2] ];
   lasti = len(pts)-1;
   rng= [0:len(pts)-1];
   rng_1=[0:len(pts)-2];
   1rng =[1:len(pts)-1];
   1rng1 =[1:len(pts)];
   
   MarkPts(pts, Line=["closed", true] );
   
   nodepairs=[ for(i=rng) 
                [ onlinePt( get2(pts,i), ratio=ratio/2) //0.25) 
                , onlinePt( get2(pts,i), ratio=1-ratio/2) //0.75)
                ]
             ];
   for(i=rng)  Line(nodepairs[i], r=0.1, color=[COLORS[i],0.2]);
   
   MarkPts( [for(np=nodepairs) each np]
          , Label=false
          , Ball=["r",0.05]
          , Line=false);             

   //nodepairss = concat([last(nodepairs)], nodepairs,[nodepairs[0]]);
   
   //grids = [ for(i=rng) 
             //[ nodepairs[ i==0?(len(pts)-1):(i-1) ][1]
             //, nodepairs[ i==(len(pts)-1)?0:(i+1) ][0]
             //]
           //];
   //grids = [ for(i=1rng1) //[1:len(nodepairs)]) 
             //[ nodepairss[ i-1 ][1]
             //, nodepairss[ i+1 ][0]
             //]
           //];
   grids = [ for(i=rng) //[1:len(nodepairs)]) 
             [ get(nodepairs, i-2)[1]
             , get(nodepairs, i  )[0]
             ]
           ];
   
   echo( grids=grids );
                     
   for(i=rng) Line(grids[i], r=0.01,color=COLORS[i]); 

   //midpts= [ for(i=rng) let( a= grids[i]
                           //, b= grids[i==0?lasti:(i-1)]
                           //) 
               //midPt(lineBridge( a,b)) 
           //];
   midpts= [ for(i=rng) midPt(lineBridge( get2(grids,i-1))) 
           ];
           
   //gridss = concat( [last(grids)], grids);        
   //echo(gridss=gridss);
   //midpts= [ for(i=1rng1)  
               //midPt(lineBridge( gridss[i],gridss[i-1])) 
           //];
           //
   //midpts= [ for(i=1rng1)  
               //[ gridss[i],gridss[i-1] ] 
           //];
   //echo(midpts= midpts);        
   MarkPts(midpts, Line=["closed",true, "r", 0.03,"color", "black"]);          
   
}
//dev_DooSabin_face_shrink__non_plane_pantagon();

module dev_DooSabin_face_shrink__function()
{ 
   echom("dev_DooSabin_face_shrink__function()");
   /* When it is non-planar, sometimes it's impossible to find the 
      grid pts because of distortion of the plane. In such a case,
      we find the line-bridge and take the mid pt
   */
   
   ratio=0.5;
   
   pts = [ORIGIN, [0,2,2], [2,3,0], [3,2,0], [2.5,0,2] ];
   lasti = len(pts)-1;
   rng= [0:len(pts)-1];
   rng_1=[0:len(pts)-2];
   1rng =[1:len(pts)-1];
   1rng1 =[1:len(pts)];
   
   MarkPts(pts, Line=["closed", true] );
   
   ptpairs = get2(pts);
   
   nodepairs=[ for(i=rng) 
                onlinePts( ptpairs[i], ratios=[ratio/2, 1-ratio/2])
             ];
   for(i=rng)  Line(nodepairs[i], r=0.1, color=[COLORS[i],0.2]);
   
   //MarkPts( [for(np=nodepairs) each np]
          //, Label=false
          //, Ball=["r",0.05]
          //, Line=false); 
   MarkPts( [for(np=nodepairs) each np]
          , Label=false
          , Ball=["r",0.05]
          , Line=false); 
                      
   grids = [ for(i=rng) 
             [ get(nodepairs, i-2)[1]
             , get(nodepairs, i  )[0]
             ]
           ];
   /*
   , ptpairs = get2(pts)
      , nodepairs=[ for(i=rng) 
                    onlinePts( ptpairs[i], ratios=[ratio/2, 1-ratio/2])
                  ]
      , rtn= [ for(i=rng)
                midPt( lineBridge( [nodepairs[i][0], get(nodepairs,i-2)[1]]
                                 , [get(nodepairs,i-1)[1], get(nodepairs,i+1)[0]]
                                 )
                     )            
      
      
             ]  
   */          
   echo( grids=grids );
                     
   for(i=rng) Line(grids[i], r=0.01,color=COLORS[i]); 
   
   //midpts= [ for(i=rng) 
   //          midPt(lineBridge( get2(grids,i-1))) 
   //        ];
   //echo(nodepairs = nodepairs);        
   //echo("get(nodepairs,i-2)=",get(nodepairs,-2));               
   midpts=  [ for(i=rng)
                midPt( lineBridge( [nodepairs[i][0], get(nodepairs,i-2)[1]]
                                 , [get(nodepairs,i-1)[1], get(nodepairs,i+1)[0]]
                                 )
                     )   
             ];
   //i=4;    
   //Line( [nodepairs[i][0], get(nodepairs,i-2)[1]], r=0.05, color="red" );          
   //Line( [get(nodepairs,i-1)[1], get(nodepairs,i+1)[0]], r=0.05, color="green" );          
   function DooSabin_face_shrink(pts,ratio=0.5)=
   ( /* Check out dev_edgeface.scad to see how this 
        formula is derived
     */
     let( ptpairs = get2(pts)
        , nodepairs=[ for(i=rng) 
                      onlinePts( ptpairs[i], ratios=[ratio/2, 1-ratio/2])
                    ]
        //, grids= [ for(i=rng) 
                   //[ get(nodepairs, i-2)[1]
                   //, get(nodepairs, i  )[0]
                   //]
                 //]          
        //, rtn = [ for(i=rng) 
                 //midPt(lineBridge( get2(grids,i-1))) 
                //]
        )
        [ for(i=rng)
           midPt( lineBridge( [nodepairs[i][0], get(nodepairs,i-2)[1]]
                            , [get(nodepairs,i-1)[1], get(nodepairs,i+1)[0]]
                            )
                ) 
        ]
   );
   
   MarkPts( DooSabin_face_shrink(pts)
          , Line=["closed",true, "r", 0.03,"color", "black"]);          
   
}
//dev_DooSabin_face_shrink__function();

//:
module dev_DooSabin_face_shrink__polypts()
{
  echo("test");
  echom("dev_DooSabin_face_shrink__polypts()");
  // pts and faces taken from 
  // http://forum.openscad.org/3D-parametric-plot-with-Open-SCAD-tp23732p23744.html
  // author: NateTG
  
  pts = [[0.0954915, -0.293893, -0.951057], [0.282301, -0.125689, -0.951057], [0.282301, 0.125689, -0.951057], [0.0954915, 0.293893, -0.951057], [0.25, -0.769421, -0.587785], [0.739074, -0.329057, -0.587785], [0.739074, 0.329057, -0.587785], [0.25, 0.769421, -0.587785], [0.309017, -0.951057, 0], [0.913545, -0.406737, 0], [0.913545, 0.406737, 0], [0.309017, 0.951057, 0], [0.25, -0.769421, 0.587785], [0.739074, -0.329057, 0.587785], [0.739074, 0.329057, 0.587785], [0.25, 0.769421, 0.587785], [0.0954915, -0.293893, 0.951057], [0.282301, -0.125689, 0.951057], [0.282301, 0.125689, 0.951057], [0.0954915, 0.293893, 0.951057]];
  faces = [[4, 5, 1, 0], [5, 6, 2, 1], [6, 7, 3, 2], [8, 9, 5, 4], [9, 10, 6, 5], [10, 11, 7, 6], [12, 13, 9, 8], [13, 14, 10, 9], [14, 15, 11, 10], [16, 17, 13, 12], [17, 18, 14, 13], [18, 19, 15, 14]];
  color("blue", 0.1)polyhedron(points=pts,faces=faces);
  MarkPts( pts,r=0.01
         , Ball=["r",0.02]
         , Label=["text",range(pts),"scale",0.3, "indent",0.05]
         , Line=false 
         );
  for( i= [0:4] )
  {
    Line( [for(j=[0:4]) if(i<4) pts[4*j+i]], r=0.005 );
    Line( [for(j=[i*4:i*4+3]) pts[j]] , r=0.005);
  }       
  //-------------------------------------
  ef = get_edgeface(faces);
  //echo(ef=ef);
  /* ef=
  [ [4, 5], ["r", [0, [4, 5, 1, 0]], "l", [3, [8, 9, 5, 4]]]
  , [5, 1], ["r", [0, [4, 5, 1, 0]], "l", [1, [5, 6, 2, 1]]]
  , [1, 0], ["r", [0, [4, 5, 1, 0]]]
  , [0, 4], ["r", [0, [4, 5, 1, 0]]]
  , [5, 6], ["r", [1, [5, 6, 2, 1]], "l", [4, [9, 10, 6, 5]]]
  , [6, 2], ["r", [1, [5, 6, 2, 1]], "l", [2, [6, 7, 3, 2]]]
  , [2, 1], ["r", [1, [5, 6, 2, 1]]]
  , [6, 7], ["r", [2, [6, 7, 3, 2]], "l", [5, [10, 11, 7, 6]]]
  , [7, 3], ["r", [2, [6, 7, 3, 2]]]
  , [3, 2], ["r", [2, [6, 7, 3, 2]]]
  , [8, 9], ["r", [3, [8, 9, 5, 4]], "l", [6, [12, 13, 9, 8]]]
  , [9, 5], ["r", [3, [8, 9, 5, 4]], "l", [4, [9, 10, 6, 5]]]
  , [4, 8], ["r", [3, [8, 9, 5, 4]]]
  , [9, 10], ["r", [4, [9, 10, 6, 5]], "l", [7, [13, 14, 10, 9]]]
  , [10, 6], ["r", [4, [9, 10, 6, 5]], "l", [5, [10, 11, 7, 6]]]
  , [10, 11], ["r", [5, [10, 11, 7, 6]], "l", [8, [14, 15, 11, 10]]]
  , [11, 7], ["r", [5, [10, 11, 7, 6]]]
  , [12, 13], ["r", [6, [12, 13, 9, 8]], "l", [9, [16, 17, 13, 12]]]
  , [13, 9], ["r", [6, [12, 13, 9, 8]], "l", [7, [13, 14, 10, 9]]]
  , [8, 12], ["r", [6, [12, 13, 9, 8]]]
  , [13, 14], ["r", [7, [13, 14, 10, 9]], "l", [10, [17, 18, 14, 13]]]
  , [14, 10], ["r", [7, [13, 14, 10, 9]], "l", [8, [14, 15, 11, 10]]]
  , [14, 15], ["r", [8, [14, 15, 11, 10]], "l", [11, [18, 19, 15, 14]]]
  , [15, 11], ["r", [8, [14, 15, 11, 10]]]
  , [16, 17], ["r", [9, [16, 17, 13, 12]]]
  , [17, 13], ["r", [9, [16, 17, 13, 12]], "l", [10, [17, 18, 14, 13]]]
  , [12, 16], ["r", [9, [16, 17, 13, 12]]]
  , [17, 18], ["r", [10, [17, 18, 14, 13]]]
  , [18, 14], ["r", [10, [17, 18, 14, 13]], "l", [11, [18, 19, 15, 14]]]
  , [18, 19], ["r", [11, [18, 19, 15, 14]]]
  , [19, 15], ["r", [11, [18, 19, 15, 14]]]
  ]
  
  */
  
  
  //------------------------------------ 
  shFacePts= [ for(face=faces) shrinkPts( get(pts,face), ratio=0.3 )];
  
  for(i=range(shFacePts)) //facepts= shFacePts) 
  {
     facepts = shFacePts[i];
     //echo(facepts = facepts);
      //[[0.869927, -0.193659, -0.146946], [0.869927, 0.193658, -0.146946], [0.782692, 0.174238, -0.440839], [0.782692, -0.174239, -0.440839]]
     //echo( facepts=facepts);     
      MarkPts( facepts, Line=["closed", true]
            , r=0.01
            , Ball=["r", 0.01]
            , Label=["text", range(facepts), "scale", 0.2, "indent",0.04
                    ,"color","black"] 
            
            );
     Plane( facepts, color=[COLORS[i],0.5], mode=1, h=0.02, actions=["z",-0.01] );
    
  }
         
}

//dev_DooSabin_face_shrink__polypts();

module dev_DooSabin_face_shrink__L_polypts()
{
  echo("test");
  echom("dev_DooSabin_face_shrink__L_polypts()");
  // pts and faces taken from 
  // http://forum.openscad.org/3D-parametric-plot-with-Open-SCAD-tp23732p23744.html
  // author: NateTG
  
  
  pts=[ [3,0,0], ORIGIN, [0,4,0], [1.5,4,0], [1.5,2,0], [3,2,0]
      , [3,0,1], [0,0,1], [0,4,1], [1.5,4,1], [1.5,2,1], [3,2,1]
      ];
      
  faces=[ [0,1,7,6], [7,1,2,8], [2,3,9,8], [3,4,10,9]
        , [4,5,11,10], [0,6,11,5]
        , [6,7,8,9,10,11], [5,4,3,2,1,0]
          ];    
  pts = hashs(POLY_SAMPLES, ["L_poly", "pts"]);
  faces = hashs(POLY_SAMPLES, ["L_poly", "faces"]); 
  
  color("blue", 0.1)polyhedron(points=pts,faces=faces);
  MarkPts( pts,r=0.01
         , Ball=["r",0.02]
         , Label=["text",range(pts),"scale",0.3, "indent",0.05]
         , Line=false 
         );
//  for( i= [0:4] )
//  {
//    Line( [for(j=[0:4]) if(i<4) pts[4*j+i]], r=0.005 );
//    Line( [for(j=[i*4:i*4+3]) pts[j]] , r=0.005);
//  }       
  //-------------------------------------
  ef = get_edgeface(faces);
  //echo(ef=ef);
  /* ef=
  [ [4, 5], ["r", [0, [4, 5, 1, 0]], "l", [3, [8, 9, 5, 4]]]
  , [5, 1], ["r", [0, [4, 5, 1, 0]], "l", [1, [5, 6, 2, 1]]]
  , [1, 0], ["r", [0, [4, 5, 1, 0]]]
  , [0, 4], ["r", [0, [4, 5, 1, 0]]]
  , [5, 6], ["r", [1, [5, 6, 2, 1]], "l", [4, [9, 10, 6, 5]]]
  , [6, 2], ["r", [1, [5, 6, 2, 1]], "l", [2, [6, 7, 3, 2]]]
  , [2, 1], ["r", [1, [5, 6, 2, 1]]]
  , [6, 7], ["r", [2, [6, 7, 3, 2]], "l", [5, [10, 11, 7, 6]]]
  , [7, 3], ["r", [2, [6, 7, 3, 2]]]
  , [3, 2], ["r", [2, [6, 7, 3, 2]]]
  , [8, 9], ["r", [3, [8, 9, 5, 4]], "l", [6, [12, 13, 9, 8]]]
  , [9, 5], ["r", [3, [8, 9, 5, 4]], "l", [4, [9, 10, 6, 5]]]
  , [4, 8], ["r", [3, [8, 9, 5, 4]]]
  , [9, 10], ["r", [4, [9, 10, 6, 5]], "l", [7, [13, 14, 10, 9]]]
  , [10, 6], ["r", [4, [9, 10, 6, 5]], "l", [5, [10, 11, 7, 6]]]
  , [10, 11], ["r", [5, [10, 11, 7, 6]], "l", [8, [14, 15, 11, 10]]]
  , [11, 7], ["r", [5, [10, 11, 7, 6]]]
  , [12, 13], ["r", [6, [12, 13, 9, 8]], "l", [9, [16, 17, 13, 12]]]
  , [13, 9], ["r", [6, [12, 13, 9, 8]], "l", [7, [13, 14, 10, 9]]]
  , [8, 12], ["r", [6, [12, 13, 9, 8]]]
  , [13, 14], ["r", [7, [13, 14, 10, 9]], "l", [10, [17, 18, 14, 13]]]
  , [14, 10], ["r", [7, [13, 14, 10, 9]], "l", [8, [14, 15, 11, 10]]]
  , [14, 15], ["r", [8, [14, 15, 11, 10]], "l", [11, [18, 19, 15, 14]]]
  , [15, 11], ["r", [8, [14, 15, 11, 10]]]
  , [16, 17], ["r", [9, [16, 17, 13, 12]]]
  , [17, 13], ["r", [9, [16, 17, 13, 12]], "l", [10, [17, 18, 14, 13]]]
  , [12, 16], ["r", [9, [16, 17, 13, 12]]]
  , [17, 18], ["r", [10, [17, 18, 14, 13]]]
  , [18, 14], ["r", [10, [17, 18, 14, 13]], "l", [11, [18, 19, 15, 14]]]
  , [18, 19], ["r", [11, [18, 19, 15, 14]]]
  , [19, 15], ["r", [11, [18, 19, 15, 14]]]
  ]
  
  */
  
  
  //------------------------------------ 
  shFacePts= [ for(face=faces) shrinkPts( get(pts,face), ratio=0.15 )];
  
  for(i=range(shFacePts)) //facepts= shFacePts) 
  {
     facepts = shFacePts[i];
     //echo(facepts = facepts);
      //[[0.869927, -0.193659, -0.146946], [0.869927, 0.193658, -0.146946], [0.782692, 0.174238, -0.440839], [0.782692, -0.174239, -0.440839]]
     //echo( facepts=facepts);     
      MarkPts( facepts, Line=["closed", true]
            , r=0.01
            , Ball=["r", 0.01]
            , Label=["text", range(facepts), "scale", 0.2, "indent",0.04
                    ,"color","black"] 
            
            );
     Plane( facepts, color=[COLORS[i],0.5], mode=1, h=0.02, actions=["z",-0.01] );
    
  }
         
}

//dev_DooSabin_face_shrink__L_polypts();


module dev_shrinkPts_on_cube()
{
   echom("dev_shrinkPts_on_cube()");
   pts = cubePts();
   faces = rodfaces(4);
   facePts = [for(f=faces)get(pts,f)];
   shrinkPts= [for(fp=facePts) shrinkPts(fp)];
   echo(pts=pts);
   echo(shrinkPts=shrinkPts);
   Frame(pts);
   for(sp=shrinkPts) Line(sp,closed=true);
   	
}
//dev_shrinkPts_on_cube();



module dev_shrinkPts_on_cube_labeled()
{
   echom("dev_shrinkPts_on_cube_labeled()");
   pts = cubePts(3);
   faces = re_align_a_pis(rodfaces(4));
   echo("rodfaces(4)=",rodfaces(4));
   echo(faces=faces);
   facePts = [for(f=faces)get(pts,f)];
   echo(facePt = facePts);
   
   MarkPts(pts,Line=false);
   
   shrinkPts= [for(fp=facePts) shrinkPts(fp, 0.4)];   
   
   Frame(pts);
   
   for(i=[0:5]) //sp=shrinkPts) 
   {
     sp = shrinkPts[i];
     Plane(sp, color=[COLORS[i],0.3], mode=4);
     MarkPts(sp,Line=["closed",true]);
   }	
}
//dev_shrinkPts_on_cube_labeled();

module dev_shrinkPts_on_rod3_connected()
{
   echom("dev_shrinkPts_on_rod3_connected()");
   //pts = cubePts(3);
   //faces = rodfaces(4);
   pts = [[3,0,0], ORIGIN, [1,3,0] 
         ,[3,0,2], [0,0,2], [1,3,2] 
         ];
   old_faces = rodfaces(3);
   rng= [0:len(old_faces)-1];
   old_facePts = [for(f=old_faces)get(pts,f)];
   
   a_oldFaceBegi_newFacePts=  [for(i=rng) 
                 [ old_faces[i][0]
                 , shrinkPts(old_facePts[i], 0.4)
                 ]
             ];
   //new_facePts= [ for(i=rng) a_oldFaceBegi_newFacePts[i][1]];
   //echo(new_facePts= new_facePts);

   //a_shpts=  [for(fp=facePts) shrinkPts(fp, 0.4)];
             
   //echo(a_oldFaceBegi_newFacePts= a_oldFaceBegi_newFacePts); 
   /*
   NOTE:
     
   -- a_shpts: arr of [beg_i, shpts]: 
      
        [ [i,[P,Q,R]  ]
        , [j,[S,T,U,V]] ...]
      
      beg_i : the pt index in an old face from where the 
               first pt of the new faces starts 
      shpts : pts on the new face
      
      a_shpts[1]= [j,[S,T,U,V]]
      a_shpts[1][0]= j --- old_faces[1][j] is the pt index from
                           where the face pts start a clock-wise
                           circling. 
      a_shpts[1][1]= [S,T,U,V] --- pts of the new face                     
      
   -- The pts in a shpts are generated in a way that they are already
      clock-wise. 
      
   -- So if we get all pts from a_shpts: [P,Q,R,S,T,U,V ...], 
      get their indices, and place them back to a_shpts, it becomes
      the new faces of the a_shpts:
      [ [0,1,2], [3,4,5,6]... ]
      
   -- This new faces can also be obtained by:
    
        re_align_a_pis(old_faces)
         
   -- So we have old and new faces : 
      
      old: [[2,1,0], [3,4,5], [0,1,4,3], [ 1, 2, 5, 4], [ 2, 0, 3, 5]]
      new: [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
      
   -- Both have 5 faces, and new_faces[i] is corresponding to old_faces[i]
      
   -- So:
   
      re_align_a_pis(old_faces) ==> new_faces: [[0,1,2], [3,4,5], [6,7,8,9]...]    
      
      From pts and old_faces ==> old_facePts: [ [q,p,r], [o,s,t,u]...]
      
      shrink_pts(over old_facePts)==> new_facePts: [[P,Q,R],[S,T,U,V]...]
      
      new_facePts => new_pts: [P,Q,R,S,T,U,V ...]
      
      Now, more difficult is to:
      
       (1) connect new_facePts 'cos they -- unlike old_facePts all next to
           one another --- are independent faces and a new face needs to be 
           created for each pair of faces.
       (2) find the new edgeface
       
       
      (1) Connect new_facePts
      
      *  old_faces => a_oldFaceBegi_newFacePts: 
      
      		[ [i,[P,Q,R]  ]  // 1st face starting from i
                , [j,[S,T,U,V]] 
                , ...]
        
         new_facePts= [ [P,Q,R],[S,T,U,V], ...]
        
         new_faces[0]= [0,1,2]
         
         We start with the 1st new edge [0,1] and need to locate what
         old edge it corresponds to.  
         
         This face starts with new_pts[0] = P, but its corresponding
         old_faces[0] starts a pt that could be any pt in the 
         facePts[0]. But we have a pt-index i, i.e., old_pts[i] to
         point to it. 
         
         So if the k-th old face, old_faces[k], [pt_i, pt_j, pt_k],
         starts with pt-index pt_j, then the current old edge 
         corresponding to the is [pt_j, pt_k]
         
         From this [pt_j, pt_k], we can find the surrounding old_faces
         of old_faces[0].  
          
        get old_EF (edgeface)  
          [ [2, 1], ["R", [0, [2, 1, 0]], "L", [3, [1, 2, 5, 4]]]
          , [1, 0], ["R", [0, [2, 1, 0]], "L", [2, [0, 1, 4, 3]]]
          , ...
          ]   
   
        Starting from 1st edge, [2,1], of old_EF:
        
        
   */
   
   new_core_faces = re_align_a_pis(old_faces);
   new_core_pts = [ for(fi=rng) each a_oldFaceBegi_newFacePts[fi][1] ];
   //echo(new_core_pts= new_core_pts);
   MarkPts(new_core_pts, Line=false);
   //echo(old_faces=old_faces);
   
   new_core_edgeface = get_edgeface(new_core_faces);
   //echo(new_core_edgeface=new_core_edgeface);
//   function get_new_edgeface(
//                old_edgeface
//              , new_core_faces
//              , new_core_edgeface
//              , _i=0
//              )=
//   (  // _i will loop thru old edges(= keys(old_edgeface) )
//      // For each old edge [i,j], we add 2 faces:
//      // a. The face that below the old_edge 
//      //   (= the "L" face of the corresponding new_edge of the old_edge)
//      // b. the face (most likely, a trangle) connects 
//      //    1. i (1st pt of old_faces) 
//      //    2. The 1st pt of current new_face
//      //    3. The 2nd pt of "L" new_face of the current edge .
//       
//      _i>=len(old_edgeface)/2? new_core_edgeface
//      : let( h_current_old_faces= vals(old_edgeface)[_i] // ["R",[fi,fpi], "L",[fi,fpi] ]
//           , right_old_fi_fpi= hash( h_current_old_faces, "R") 
//           , left_old_fi_fpi= hash( h_current_old_faces, "L") 
//           , right_fi = right_old_fi_fpi[0] 
//           , left_fi = left_old_fi_fpi[0] 
//           , new_right_face = new_core_faces[ right_fi ]
//           , new_left_face  = new_core_faces[ left_fi ]
//           , newEF = [ ]  
//           )
//      
//   );

//old: [[2,1,0], [3,4,5], [0,1,4,3], [ 1, 2, 5, 4], [ 2, 0, 3, 5]]
//new: [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
     
//;     
   function get_a_subpis(old_faces, new_core_faces)=
   ( /*
     For each pt in old_pts, obtain 3 or more "subpis" ---
     the indices of new pts that are corresponding to pt. 
    
     Each pt corresponds to at least 3 subpis
    
     subpis for rod3 pts:
     
      [[3, 8, 21], [2, 9, 12], [1, 13, 16], [0, 17, 20], [4, 11, 22], [5, 10, 15], [6, 14, 19], [7, 18, 23]]
      
     means, old_pts[0] corresponds to 
     new_pts[3],new_pts[8] and new_pts[12]
     */ 
     let( pi_rng = [0:max(flatten(old_faces))]
        , old_a_fis_of_pt= get_a_fis_on_pts(old_faces)
        )
     let( rtn= [ for( pi=pi_rng )
              // echo("----------------------------- pi=", pi)
              let(nearFaceIndices  // like:[0,5,4] for faces 0,5,4
                    = old_a_fis_of_pt[pi]                  
                 )
             // echo(nearFaceIndices=nearFaceIndices)   
              [ for(fi=nearFaceIndices)
                  //echo("....... f=", fi, ", color= ", COLORS[fi] )
                  let( newf= new_core_faces[fi]
                     , old_pi_order_in_face= idx( old_faces[fi],pi)
                     , new_pi = newf[old_pi_order_in_face] 
                     )   
                 //echo(newf=newf)    
                  new_pi
              ]
            ]         
        )  
     rtn
   );         
   echo(old_faces);
   echo(new_core_faces);
   echo(max(flatten(old_faces)));
   a_subpis= get_a_subpis(old_faces, new_core_faces );
   echo(a_subpis = a_subpis);
   for(subpis=a_subpis) Line(get(new_core_pts,subpis), closed=true);
   
   
   //polyhedron(points=new_core_pts, faces= new_core_faces);
   
   //shrinkFaces= [for(i=rng) ]
   //echo(a_iFacePts = a_iFacePts);
   MarkPts(pts,Line=false);
   Frame(pts);
   
   for(i=rng) //[0:5]) //sp=shrinkPts) 
   {
     sp = a_oldFaceBegi_newFacePts[i][1];
     Plane(sp, color=[COLORS[i],0.3], mode=1);
     //MarkPts(sp, Ball=false, Line=["closed",true]);
   }
   
   //MarkPts( [for(sp=shrinkPts)each sp]
   //       , Ball=false, Line=false);
   
   //---------------------------------------
   
   old_ef = get_edgeface(old_faces);   
   
   for(egfa= hashkvs(old_ef)) 
   {  
      //// egfa: [ [x,y], [ "l",[0,[2,1,4]]
      ////                , "r",[3,[5,6,7]]  ] 
      ////       ]
   
      eg= egfa[0];
      right_fifa= hash(egfa[1],"r");
       
   }
   	
}
//dev_shrinkPts_on_rod3_connected();

module dev_shrinkPts_on_cube_connected()
{
   echom("dev_shrinkPts_on_cube_connected()");
   pts = cubePts(3);
   old_faces = rodfaces(4);
   //pts = [[3,0,0], ORIGIN, [1,3,0] 
         //,[3,0,2], [0,0,2], [1,3,2] 
         //];
   //old_faces = rodfaces(3);
   frng= [0:len(old_faces)-1];
   old_facePts = [for(f=old_faces)get(pts,f)];
   
   a_oldFaceBegi_newFacePts=  [for(fi=frng) 
                 [ old_faces[fi][0]
                 , shrinkPts(old_facePts[fi], 0.4)
                 ]
             ];
     
   new_core_faces = re_align_a_pis(old_faces);
   new_core_pts = [ for(fi=frng) each a_oldFaceBegi_newFacePts[fi][1] ];
   //echo(new_core_pts= new_core_pts);
   MarkPts(new_core_pts, Line=false, Label=["text",range(new_core_pts), "color","blue"]);
   //echo(old_faces=old_faces);
   
   //new_core_edgeface = get_edgeface(new_core_faces);

   function get_a_subpis(old_faces, new_core_faces)=
   ( /*
     For each pt in old_pts, obtain 3 or more "subpis" ---
     the indices of new pts that are corresponding to pt. 
    
     Each pt corresponds to at least 3 subpis
    
     subpis for rod3 pts:
     
      [[3, 8, 21], [2, 9, 12], [1, 13, 16], [0, 17, 20], [4, 11, 22], [5, 10, 15], [6, 14, 19], [7, 18, 23]]
      
     means, old_pts[0] corresponds to 
     new_pts[3],new_pts[8] and new_pts[12]
     */ 
     let( pi_rng = [0:max(flatten(old_faces))]
        , old_a_fis_of_pt= get_a_fis_on_pts(old_faces)
        )
     let( rtn= [ for( pi=pi_rng )
              // echo("----------------------------- pi=", pi)
              let(nearFaceIndices  // like:[0,5,4] for faces 0,5,4
                    = old_a_fis_of_pt[pi]                  
                 )
             // echo(nearFaceIndices=nearFaceIndices)   
              [ for(fi=nearFaceIndices)
                  //echo("....... f=", fi, ", color= ", COLORS[fi] )
                  let( newf= new_core_faces[fi]
                     , old_pi_order_in_face= idx( old_faces[fi],pi)
                     , new_pi = newf[old_pi_order_in_face] 
                     )   
                 //echo(newf=newf)    
                  new_pi
              ]
            ]         
        )  
     rtn
   );         
   echo(old_faces);
   echo(new_core_faces);
   echo(max(flatten(old_faces)));
   a_subpis= get_a_subpis(old_faces, new_core_faces );
   echo(a_subpis = a_subpis);
   for(subpis=a_subpis) Line(get(new_core_pts,subpis), closed=true);
   
   //polyhedron(points=new_core_pts, faces= new_core_faces);
   
   MarkPts(pts,Line=false);
   Frame(pts);
   
   for(fi=frng)
   {
     sp = a_oldFaceBegi_newFacePts[fi][1];
     Plane(sp, color=[COLORS[fi],0.3], mode=1);
   }
   
   //---------------------------------------
   
   	
}
//dev_shrinkPts_on_cube_connected();

module dev_shrinkPts_on_rod3_connected_w_faces()
{
   echom("dev_shrinkPts_on_rod3_connected_w_faces()");
   //pts = cubePts(3);
   //old_faces = rodfaces(4);
   pts = [[3,0,0], ORIGIN, [1,3,0] 
         ,[3,0,2], [0,0,2], [1,3,2] 
         ];
   old_faces = rodfaces(3);
   frng= [0:len(old_faces)-1];
   old_facePts = [for(f=old_faces)get(pts,f)];
   
   a_oldFaceBegi_newFacePts=  [for(fi=frng) 
                 [ old_faces[fi][0]
                 , shrinkPts(old_facePts[fi], 0.4)
                 ]
             ];
     
   new_core_faces = re_align_a_pis(old_faces);
   echo(new_core_faces=new_core_faces);
   /* =[[0, 1, 2],[3, 4, 5],[6, 7, 8, 9],[10, 11, 12, 13],[14, 15, 16, 17]]
   */
   
   new_core_pts = [ for(fi=frng) each a_oldFaceBegi_newFacePts[fi][1] ];
   //echo(new_core_pts= new_core_pts);
   MarkPts(new_core_pts, Line=false, Label=["text",range(new_core_pts), "color","blue"]);
   //echo(old_faces=old_faces);
   
   function get_a_subpis_that_works(old_faces, new_core_faces)=
   ( /*
     For each pt in old_pts, obtain 3 or more "subpis" ---
     the indices of new pts that are corresponding to pt. 
    
     Each pt corresponds to at least 3 subpis
    
     subpis for rod3 pts:
     
      [[3, 8, 21], [2, 9, 12], [1, 13, 16], [0, 17, 20], [4, 11, 22], [5, 10, 15], [6, 14, 19], [7, 18, 23]]
      
     means, old_pts[0] corresponds to 
     new_pts[3],new_pts[8] and new_pts[12]
     */ 
     let( pi_rng = [0:max(flatten(old_faces))]
        , old_a_fis_of_pt= get_a_fis_on_pts(old_faces)
        )
     let( rtn= [ for( pi=pi_rng )
                 echo("----------------------------- pi=", pi)
                let(nearFaceIndices  // like:[0,5,4] for faces 0,5,4
                      = old_a_fis_of_pt[pi]                  
                   )
                  echo(nearFaceIndices=nearFaceIndices)   
                  [ for(fi=nearFaceIndices)
                      echo("....... f=", fi, ", color= ", COLORS[fi] )
                      let( newf= new_core_faces[fi]
                         , old_pi_order_in_face= idx( old_faces[fi],pi)
                         , new_pi = newf[old_pi_order_in_face] 
                         )   
                     echo(newf=newf)    
                      new_pi
                ]
            ]         
        )  
     rtn
   );         
   //echo(old_faces);
   //echo(new_core_faces);
   //echo(max(flatten(old_faces)));
   //a_subpis= get_a_subpis(old_faces, new_core_faces );
   //echo(a_subpis = a_subpis);
   //a_subpis= [[2,6,15],[1,7,10],[0,11,14],[3,9,16],[4,8,13],[5,12,17]]
   //for(subpis=a_subpis) Line(get(new_core_pts,subpis), closed=true);
   
 
   //new_core_subpi_faces= concat( new_core_faces, a_subpis);
   //echo(new_core_subpi_faces=new_core_subpi_faces);
   
   //===========================================================
     
   /* 
      Trying something new (other than get_a_subpis_that_works())
   
      An edgefi: 
      
      [ eg0, ["L",Lfi0, "R", Rfi0, "F", [i0,j0,..], "B", [k0,l0...] ] ]
      , eg1, ["L",Lfi1, "R", Rfi1, "F", [i1,j1,..], "B", [k1,l1...] ] ]
      , ... ]
                 
      This Lfi,Rfi correspond to the fi's of new_core_faces.
      
      So eg0 (an old edge) corresponds to two new faces:
      
         new_core_faces[ Lfi0 ]
         new_core_faces[ Rfi0 ]
         
      For example, rodfaces(3),
      
      Old faces:
      [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
      
      aligned new faces:
      [[0, 1, 2], [3, 4, 5], [6, 7, 8, 9], [10, 11, 12, 13], [14, 15, 16, 17]]
          
      Note that old and new faces have one-to-one relationship AND
         the pts on both faces are arrange in the same order.
           
      Which means :
      
      [1] If we take an old pt, old_pts[3], its subpts will be:
      
         on the old_faces[1]: new_core_pts[3]
         on the old_faces[2]: new_core_pts[9]
         on the old_faces[4]: new_core_pts[16]
         
      [2] if we take an old edge, [3,4]      
     
     
   */
   
   
     
   //polyhedron(points=new_core_pts, faces= new_core_faces);
   
   MarkPts(pts,Line=false);
   Frame(pts);
   
   for(fi=frng)
   {
     sp = a_oldFaceBegi_newFacePts[fi][1];
     Plane(sp, color=[COLORS[fi],0.3], mode=1);
   }
   
   //---------------------------------------
   //
   //   New get the new faces
   //
   new_core_edgeface = get_edgeface(new_core_faces);
   echo(new_core_edgeface=new_core_edgeface);
   /* [ [0, 1], ["R", [0, [0, 1, 2]]]
      , [1, 2], ["R", [0, [0, 1, 2]]]
      , [2, 0], ["R", [0, [0, 1, 2]]]
      , [3, 4], ["R", [1, [3, 4, 5]]]
      , [4, 5], ["R", [1, [3, 4, 5]]]
      , [5, 3], ["R", [1, [3, 4, 5]]]
      , [6, 7], ["R", [2, [6, 7, 8, 9]]]
      , [7, 8], ["R", [2, [6, 7, 8, 9]]]
      , [8, 9], ["R", [2, [6, 7, 8, 9]]]
      , [9, 6], ["R", [2, [6, 7, 8, 9]]]
      , [10, 11], ["R", [3, [10, 11, 12, 13]]]
      , [11, 12], ["R", [3, [10, 11, 12, 13]]]
      , [12, 13], ["R", [3, [10, 11, 12, 13]]]
      , [13, 10], ["R", [3, [10, 11, 12, 13]]]
      , [14, 15], ["R", [4, [14, 15, 16, 17]]]
      , [15, 16], ["R", [4, [14, 15, 16, 17]]]
      , [16, 17], ["R", [4, [14, 15, 16, 17]]]
      , [17, 14], ["R", [4, [14, 15, 16, 17]]]]
      
   NOte that they contain only "R" face, 'cos all core faces
   are seperated 
   */	
   
   
   //new_extra_edgeface =
    //[ for(old_pi= [0:len(a_subpis)-1])
       //let(subpis= a_subpis[old_pi]
          //, subpt_face = subpis
          //, subpt_edges= get2(subpis)
          //)
       //each [for(eg=subpt_edges)
             //let( begpi= eg[0]
                //, begpi_face = [for(f=new_core_faces) 
                                 //if(has(f,begpi)) f
                               //][0]
                //, endpi = eg[1]
                //, endpi_face = [for(f=new_core_faces) 
                                 //if(has(f,endpi)) f
                               //][0]
                //, L_face= [ eg[1],eg[0]
                          //, get(begpi_face, idx(begpi_face, eg[0])-1)
                          //, get(endpi_face, idx(endpi_face, eg[1])+1)
                          //]                   
                //)
             //each [eg, ["R", subpt_face, "L", L_face] ]
            //]   
        //
    //];
    //
    //echo(new_extra_edgeface=new_extra_edgeface);
    ///*
     //[ [2, 6], ["R", [2, 6, 15], "L", [6, 2, 1, 7]]
     //, [6, 15], ["R", [2, 6, 15], "L", [15, 6, 9, 16]]
     //, [15, 2], ["R", [2, 6, 15], "L", [2, 15, 14, 0]]
     //, [1, 7], ["R", [1, 7, 10], "L", [7, 1, 0, 8]]
     //, [7, 10], ["R", [1, 7, 10], "L", [10, 7, 6, 11]]
     //, [10, 1], ["R", [1, 7, 10], "L", [1, 10, 13, 2]]
     //, [0, 11], ["R", [0, 11, 14], "L", [11, 0, 2, 12]]
     //, [11, 14], ["R", [0, 11, 14], "L", [14, 11, 10, 15]]
     //, [14, 0], ["R", [0, 11, 14], "L", [0, 14, 17, 1]]
     //, [3, 9], ["R", [3, 9, 16], "L", [9, 3, 5, 6]]
     //, [9, 16], ["R", [3, 9, 16], "L", [16, 9, 8, 17]]
     //, [16, 3], ["R", [3, 9, 16], "L", [3, 16, 15, 4]]
     //, [4, 8], ["R", [4, 8, 13], "L", [8, 4, 3, 9]]
     //, [8, 13], ["R", [4, 8, 13], "L", [13, 8, 7, 10]]
     //, [13, 4], ["R", [4, 8, 13], "L", [4, 13, 12, 5]]
     //, [5, 12], ["R", [5, 12, 17], "L", [12, 5, 4, 13]]
     //, [12, 17], ["R", [5, 12, 17], "L", [17, 12, 11, 14]]
     //, [17, 5], ["R", [5, 12, 17], "L", [5, 17, 16, 3]]]
     //
    //*/
    
   function get_new_edgeface(old_faces, new_core_faces
                            , newEF=new_core_edgeface
                            , _i=0)=
   ( /*
     For each pt in old_pts, obtain 3 or more "subpis" ---
     the indices of new pts that are corresponding to pt. 
    
     Each pt corresponds to at least 3 subpis
    
     subpis for rod3 pts:
     
      [[3, 8, 21], [2, 9, 12], [1, 13, 16], [0, 17, 20], [4, 11, 22], [5, 10, 15], [6, 14, 19], [7, 18, 23]]
      
     means, old_pts[0] corresponds to 
     new_pts[3],new_pts[8] and new_pts[12]
     */ 
     let( pi_rng = [0:max(flatten(old_faces))]
        , old_a_fis_of_pt= get_a_fis_on_pts(old_faces)
        )
     let( rtn= [ for( pi=pi_rng )
              // echo("----------------------------- pi=", pi)
              let(nearFaceIndices  // like:[0,5,4] for faces 0,5,4
                    = old_a_fis_of_pt[pi]                  
                 )
             // echo(nearFaceIndices=nearFaceIndices)   
              [ for(fi=nearFaceIndices)
                  //echo("....... f=", fi, ", color= ", COLORS[fi] )
                  let( newf= new_core_faces[fi]
                     , old_pi_order_in_face= idx( old_faces[fi],pi)
                     , new_pi = newf[old_pi_order_in_face] 
                     )   
                 //echo(newf=newf)    
                  new_pi
              ]
            ]         
        )  
     rtn
   );    
    
}
//dev_shrinkPts_on_rod3_connected_w_faces();

module dev_shrinkPts_get_all_subfaces()
{
   echom("dev_shrinkPts_get_all_subfaces()");
   echo(_red("Failed to get edge_subfaces right"));
   /*             
                    i6 
                _.-'`'-._ C
           _.-'` _.-9-._ '-._
         i7._  8:_  c   '-._ '-._ 
          :  `-._ `-._     10  _.:\i5   
         |  13._ '-._ '7-'` _-'    \
         :  :   `-._ `-._.-'    .5  \ 
      D |  |  d   11  /i1    _-'  \  \  B           
        :  :     /   /   \  4      \  \
       |  |    /   /  2   \  \   b  \  \
       :  :  /   /   / \   \  \    _-6  \
      |  | /   /   /    \   \  \-'     _-'i4
      : 12   /   /   a   \   \ 3   _-'
     |     /   /    __.--0    \_-'   
     :   /   1'--''`  __...--'i0    
    |  /    ____...-''
    i3.--''`     A     
      
    //-----------------------------------------
    
    3 types of subfaces:
    
    1. Inherit --- a,b,c,d (same number with nFaces)
    2. Tip --- [4,2,11,7], etc, (same number as the nPts)
    3. Edge --- [4,3,0,2], etc, (same number as the nEdges)
    
    subfaces = Inherit + Tip + Edge
    
    //-----------------------------------------
    
    pts: original pts,  indexed by i0,i1,i2 .... 
    subpts: shrunk from pts, indexed by 0,1,2 ... 
    
    a_fis= faces = [A,B,C,D] 
         = [ [i0,i3,i1], [i0,i1,i5,i4], [i1,i7,i6,i5], [i1,i3,i7]]
    a_fis[0]= faces[0]= 1st fis = A = [i0,i3,i1]
    
    a_subfis = [a,b,c,d] 
         = [ [0,1,2], [3,4,5,6],[7,8,9,10],[11,12,13]]

    Each pt in pts has at least 3 subpts:
       subpts of pts[i1]: get(subpts, [2,11,7,4])
    
    Each edge in pts has exactly 2 subedges :
       subedges of edge [i0,i1]: [ [3,4], [2,0] ]
       that forms the subfis of [i0,i1]: [3,4,2,0]
       => each edge has 2 subedges and 1 subfis     
         
    NOTE:
    -- a_subfis are "shrunk" version of a_fis
    -- a_fis indices might not be ordered, but indices in a_subfis
       are always in order: start with 0 and end with len(subpts)-1
    -- a_subfis has one-to-one relationship with a_fis 
    -- Indices in each fis has the same order with the indices
       of its corresponding subfis. So, 
       
         pts[i7]: shows up in 2 fis: fis 2 and fis 3 
                  a_fis[2] = [i1,i7,i6,i5]
                  a_fis[3] = [i1,i3,i7]
                  
         Corresp. subfis: a_subfis[2] = [7,8,9,10]
                          a_subfis[3] = [11,12,13]     
              
         pts[i7] is:
            the 2nd item in a_fis[2] ==> has i=1
            the 3rd item in a_fis[3] ==> has i=2
          
         So subpts of pts[i7] are:
           
            a_subfis[2][1] = 8
            a_subfis[3][2] = 13
            
            That are, subpts[8] and subpts[13]
            
        This can be obtained with:
        
            get_subpis_on_pi(a_fis, pi, subfi)         
            
   */
   
   
   shrink_ratio=0.2;
   
   //pts = cubePts(3);
   //old_faces = rodfaces(4);
   
   pts = [[3,0,0], ORIGIN, [1,3,0] 
         ,[3,0,2], [0,0,2], [1,3,2] 
         ];
   faces = rodfaces(3);
   // faces = 
   //   [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]

   
   pts = [ [3,0,0], ORIGIN, [0,3,0], [3.5,3,0], [1,2,3]];
   faces = faces( shape= "cone", nseg=1);
   
   
   subfaces = re_align_a_pis(faces);
   
   frng= [0:len(faces)-1];
   prng= [0:len(pts)-1];
   
   MarkPts(pts,Line=false);
   //Frame(pts);
   
   //================================================   
   
   //for(fi=frng)
   //{
     //sp = a_oldFaceBegi_newFacePts[fi][1];
     //Plane(sp, color=[COLORS[fi],0.3], mode=1);
   //}
   
   //----------------------------------------------
   
   a_facePts = [for(f=faces)get(pts,f)];
   // a_facePts = 
   // [ [[1, 3, 0], [0, 0, 0], [3, 0, 0]]
   // , [[3, 0, 2], [0, 0, 2], [1, 3, 2]]
   // , [[3, 0, 0], [0, 0, 0], [0, 0, 2], [3, 0, 2]]
   // , [[0, 0, 0], [1, 3, 0], [1, 3, 2], [0, 0, 2]]
   // , [[1, 3, 0], [3, 0, 0], [3, 0, 2], [1, 3, 2]]
   // ]
   
   //----------------------------------------------
   
   a_subPts_by_inherited_faces=
     [for(facePts=a_facePts) shrinkPts(facePts, ratio=shrink_ratio) ];
   // echo(a_subPts_by_inherited_faces=a_subPts_by_inherited_faces);
   // a_subPts_by_inherited_faces=
   // [ [[1.15, 2.1, 0], [0.6, 0.45, 0], [2.25, 0.45, 0]]
   // , [[2.25, 0.45, 2], [0.6, 0.45, 2], [1.15, 2.1, 2]]
   // , [[2.55, 0, 0.3], [0.45, 0, 0.3], [0.45, 0, 1.7], [2.55, 0, 1.7]]
   // , [[0.15, 0.45, 0.3], [0.85, 2.55, 0.3], [0.85, 2.55, 1.7], [0.15, 0.45, 1.7]]
   // , [[1.3, 2.55, 0.3], [2.7, 0.45, 0.3], [2.7, 0.45, 1.7], [1.3, 2.55, 1.7]]
   // ]
   
   for(subfi = frng) 
   { subPts= a_subPts_by_inherited_faces[subfi];
     MarkPt( len(subPts)==3? incenterPt(subPts)
                           : midPt( [subPts[0], subPts[2]])
           , Label= ["text",subfi, "color", "black", "indent",0]
           , Ball=false
           );                        
     Plane(subPts, mode=1, color=[COLORS[subfi], 0.2] );
   }
   
   //----------------------------------------------

   subPts = [for(subPts=a_subPts_by_inherited_faces) each subPts ];
   echo(subPts=subPts);
   MarkPts( subPts, Label=["text",range(subPts), "color","blue"]
          , Line=false, Ball=false);
   
   //----------------------------------------------
   
   inherited_subfaces = re_align_a_pis(faces);
   // inherited_subfaces= 
   //   [[0, 1, 2], [3, 4, 5], [6, 7, 8, 9], [10, 11, 12, 13], [14, 15, 16, 17]]
   
   //----------------------------------------------
   
   tip_subfaces = [ for(pi= prng) get_subpis_on_pi(faces, pi) ];
   // tip_subfaces= 
   //  [[2, 6, 15], [1, 10, 7], [0, 14, 11], [3, 16, 9], [4, 8, 13], [5, 12, 17]]
  
   for(subface=tip_subfaces) 
   {
     //Plane( get(new_core_pts, subface), mode=1);
     tip_subPts= get(subPts, subface);
     Plane( tip_subPts , mode=1);
     MarkPts( tip_subPts, Label=false);
   }   
   //echo(inherited_subfaces=inherited_subfaces);
   //echo(tip_subfaces=tip_subfaces);
   
   //----------------------------------------------
   edges = get_edges_from_faces(faces);
   echo( edges = edges );
   // edges =
   //  [ [2, 1], [1, 0], [0, 2]
   //  , [3, 4], [4, 5], [5, 3]
   //  , [1, 4], [3, 0], [2, 5]
   //  ]
   //for(edge=edges) Line(get(pts, edge), r=0.06);     
   for(eg=edges) Line( get(pts,eg) );
   
   
   edge_faces = [
      for(eg=edges)
        let( begPi = eg[0]
           , beg_subfaces = tip_subfaces[ begPi ] 
           , endPi = eg[1]
           , end_subfaces = tip_subfaces[ endPi ] 
           , edge_face = concat( get(beg_subfaces, [0,-1]) 
                               , get(end_subfaces, [1,0])
                               )   
           )
        edge_face
   ];
   echo(edge_faces=edge_faces);
   // edge_faces= 
   //   [ [0, 11, 10, 1], [1, 7, 6, 2], [2, 15, 14, 0]
   //   , [3, 9, 8, 4], [4, 13, 12, 5], [5, 17, 16, 3]
   //   , [1, 7, 8, 4], [3, 9, 6, 2], [0, 11, 12, 5]
   //   ]
   for( edge_face = edge_faces )
   {
      Plane( get(subPts,edge_face), mode=1, color=["teal",1]);
   }
   
    
}
//dev_shrinkPts_get_all_subfaces();


module dev_shrinkPts_get_all_subfaces_using_edgefi()
{
   echom("dev_shrinkPts_get_all_subfaces_using_edgefi()");
   /*             
                    i6 
                _.-'`'-._ C
           _.-'` _.-9-._ '-._
         i7._  8:_  c   '-._ '-._ 
          :  `-._ `-._     10  _.:\i5   
         |  13._ '-._ '7-'` _-'    \
         :  :   `-._ `-._.-'    .5  \ 
      D |  |  d   11  /i1    _-'  \  \  B           
        :  :     /   /   \  4      \  \
       |  |    /   /  2   \  \   b  \  \
       :  :  /   /   / \   \  \    _-6  \
      |  | /   /   /    \   \  \-'     _-'i4
      : 12   /   /   a   \   \ 3   _-'
     |     /   /    __.--0    \_-'   
     :   /   1'--''`  __...--'i0    
    |  /    ____...-''
    i3.--''`     A     
      
    //-----------------------------------------
    
    3 types of subfaces:
    
    1. Inherit --- a,b,c,d (same number with nFaces)
    2. Tip --- [4,2,11,7], etc, (same number as the nPts)
    3. Edge --- [4,3,0,2], etc, (same number as the nEdges)
    
    subfaces = Inherit + Tip + Edge
    
    //-----------------------------------------
    
    pts: original pts,  indexed by i0,i1,i2 .... 
    subpts: shrunk from pts, indexed by 0,1,2 ... 
    
    a_fis= faces = [A,B,C,D] 
         = [ [i0,i3,i1], [i0,i1,i5,i4], [i1,i7,i6,i5], [i1,i3,i7]]
    a_fis[0]= faces[0]= 1st fis = A = [i0,i3,i1]
    
    a_subfis = [a,b,c,d] 
         = [ [0,1,2], [3,4,5,6],[7,8,9,10],[11,12,13]]

    Each pt in pts has at least 3 subpts:
       subpts of pts[i1]: get(subpts, [2,11,7,4])
    
    Each edge in pts has exactly 2 subedges :
       subedges of edge [i0,i1]: [ [3,4], [2,0] ]
       that forms the subfis of [i0,i1]: [3,4,2,0]
       => each edge has 2 subedges and 1 subfis     
         
    NOTE:
    -- a_subfis are "shrunk" version of a_fis
    -- a_fis indices might not be ordered, but indices in a_subfis
       are always in order: start with 0 and end with len(subpts)-1
    -- a_subfis has one-to-one relationship with a_fis 
    -- Indices in each fis has the same order with the indices
       of its corresponding subfis. So, 
       
         pts[i7]: shows up in 2 fis: fis 2 and fis 3 
                  a_fis[2] = [i1,i7,i6,i5]
                  a_fis[3] = [i1,i3,i7]
                  
         Corresp. subfis: a_subfis[2] = [7,8,9,10]
                          a_subfis[3] = [11,12,13]     
              
         pts[i7] is:
            the 2nd item in a_fis[2] ==> has i=1
            the 3rd item in a_fis[3] ==> has i=2
          
         So subpts of pts[i7] are:
           
            a_subfis[2][1] = 8
            a_subfis[3][2] = 13
            
            That are, subpts[8] and subpts[13]
            
        This can be obtained with:
        
            get_subpis_on_pi(a_fis, pi, subfi)         
            
   */
   
   
   shrink_ratio=0.15;
   
   //............................
   //pts = cubePts(3);
   //old_faces = rodfaces(4);
   //...............................
   pts = [[3,0,0], ORIGIN, [1,3,0] 
         ,[3,0,2], [0,0,2], [1,3,2] 
         ];
   faces = rodfaces(3);
   // faces = 
   //   [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
   //...............................
   //pts = [ [3,0,0], ORIGIN, [0,3,0], [3.5,3,0], [1,2,3]];
   //faces = faces( shape= "cone", nseg=1);
   //...............................
   
   
   frng= [0:len(faces)-1];
   prng= [0:len(pts)-1];
   
   MarkPts(pts,Line=false, Label=["text",range(pts), "indent",0], Ball=false);
   //Frame(pts);
   
   //================================================   
   
   edgefi= get_edgefi(faces);
   // edgefi = 
   // [ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
   // , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
   // , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
   // , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
   // , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
   // , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
   // , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
   // , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
   // , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
   // ]
   
   edges = keys(edgefi);
   //echo(edges=edges);
   for(eg=edges) Line( get(pts,eg), r=0.01, color="black");
   
   //----------------------------------------------
   //  subfaces has 3 parts:
   //  
   //   inherited_subfaces
   //   tip_subfaces
   //   edge_subfaces
   //
   
   inherited_subfaces = re_align_a_pis(faces);
   // echo(inherited_subfaces=inherited_subfaces);
   //  inherited_subfaces= 
   //  [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
   
   a_subfacePts= [ for(face=faces)
              shrinkPts( get(pts, face), ratio = shrink_ratio ) ];
   //echo(a_subfacePts=a_subfacePts);
    
   subPts = [for(subfacePts=a_subfacePts) each subfacePts] ;
                  
   MarkPts( subPts, Label=["text", range(subPts), "color","blue"]
          , Line=false, Ball=false
          );
          
   for( fi= range(inherited_subfaces) )
   {
      //face = inherited_subfaces[fi];
      facePts= a_subfacePts[fi];
      MarkPt( len(facePts)==3? incenterPt(facePts)
                             : midPt([facePts[0], facePts[2]])
            , Label=["text", fi, "color","black", "indent",0 ]
            , Ball=false
            );
            
      Plane( facePts, mode=1, color=[COLORS[fi], 0.2] );
      MarkPts( facePts, Label=false, Line=false, Ball=["r",0.05] );
   }
   //------------------------------------------------------
   function get_subpi_of_pi_on_fi(pi,fi)=
   (  
      let( i_on_face= idx(faces[fi],pi ) ) 
      inherited_subfaces[fi][i_on_face]   
   );
   
   function get_edge_subface(edge)=
   (
      let( cur_ef= hash(edgefi, edge) // ["R",3,"L",2,"F",[4,5],"B",[0,1]]
         , L_fi= hash(cur_ef,"L") 
         , R_fi= hash(cur_ef,"R")
         )
      [ get_subpi_of_pi_on_fi( edge[0],L_fi)
      , get_subpi_of_pi_on_fi( edge[1],L_fi)
      , get_subpi_of_pi_on_fi( edge[1],R_fi)
      , get_subpi_of_pi_on_fi( edge[0],R_fi)
      ]
   );
   
   edge_subfaces= [ for(edge=edges) get_edge_subface(edge) ];
   echo(edge_subfaces=edge_subfaces);
   // edge_subfaces= 
   // [ [11, 10, 1, 0], [7, 6, 2, 1], [15, 14, 0, 2], [9, 8, 4, 3]
   // , [13, 12, 5, 4], [17, 16, 3, 5], [10, 13, 8, 7], [16, 15, 6, 9]
   // , [14, 17, 12, 11]]
   
   for( fi= range(edge_subfaces) )
   {
      face = edge_subfaces[fi];
      facePts= get(subPts, face);
      //echo(fi=fi);
      //echo(face=face);
      //echo(facePts=facePts);
            
      Plane( facePts, mode=1, color=["teal",1] );
   }             

   //------------------------------------------------------
   
   tip_subfaces = [ for(pi= prng) get_subpis_on_pi(faces, pi) ];
   // tip_subfaces= 
   //  [[2, 6, 15], [1, 10, 7], [0, 14, 11], [3, 16, 9], [4, 8, 13], [5, 12, 17]]
  
   for(subface=tip_subfaces) 
   {
     //Plane( get(new_core_pts, subface), mode=1);
     tip_subPts= get(subPts, subface);
     Plane( tip_subPts , mode=1, color=["red",1]);
     MarkPts( tip_subPts, Label=false, Ball=false);
   }     

}
//dev_shrinkPts_get_all_subfaces_using_edgefi();

   //pts = [ [3,0,0], ORIGIN, [0,3,0], [2,3,0], [1,2,3]];
   //faces = faces( shape= "cone", nseg=1);
   //echo(faces=faces);
   //polyhedron(points=pts, faces=faces);
   //MarkPts( pts );

module dev_subdivide_all_faces()
{
   echom("dev_subdivide_all_faces()");
   echo("Following previous successful try: dev_shrinkPts_get_all_subfaces_using_edgefi()");
    
   shrink_ratio=0.15;
   
   //............................
   pts = cubePts(3);
   faces = rodfaces(4);
   //...............................
   //pts = [[3,0,0], ORIGIN, [1,3,0] 
   //      ,[3,0,2], [0,0,2], [1,3,2] 
   //      ];
   //faces = rodfaces(3);
   // faces = 
   //   [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
   //...............................
   pts = [ [3,0,0], ORIGIN, [0,3,0], [3.5,3,0], [1,2,3]];
   faces = faces( shape= "cone", nseg=1);
   //...............................
   //pts = hashs(POLY_SAMPLES, ["L_poly", "pts"]); 
   //faces = hashs(POLY_SAMPLES, ["L_poly", "faces"]);
   
   
   frng= [0:len(faces)-1];
   prng= [0:len(pts)-1];
   
   //MarkPts(pts,Line=false, Label=["text",range(pts), "indent",0], Ball=false);
   //Frame(pts);
   
   //================================================   
   
   edgefi= get_edgefi(faces);
   // edgefi = 
   // [ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
   // , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
   // , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
   // , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
   // , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
   // , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
   // , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
   // , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
   // , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
   // ]
   
   edges = keys(edgefi);
   //echo(edges=edges);
   for(eg=edges) Line( get(pts,eg), r=0.01, color="black");
   
   //----------------------------------------------
   //  subfaces has 3 parts:
   //  
   //   inherited_subfaces
   //   tip_subfaces
   //   edge_subfaces
   //
   
   //------------------------------------------------------
   //
   //  Get inherited_subfaces
   //
 
   inherited_subfaces = re_align_a_pis(faces);
   // echo(inherited_subfaces=inherited_subfaces);
   //  inherited_subfaces= 
   //  [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
   
   a_subfacePts= [ for(face=faces)
              shrinkPts( get(pts, face), ratio = shrink_ratio ) ];
   //echo(a_subfacePts=a_subfacePts);
    
   subPts = [for(subfacePts=a_subfacePts) each subfacePts] ;
                  
   //MarkPts( subPts, Label=["text", range(subPts), "color","blue"]
          //, Line=false, Ball=false
          //);
          
   for( fi= range(inherited_subfaces) )
   {
      //face = inherited_subfaces[fi];
      facePts= a_subfacePts[fi];
      //MarkPt( len(facePts)==3? incenterPt(facePts)
                             // : midPt([facePts[0], facePts[2]])
            //, Label=["text", fi, "color","black", "indent",0 ]
            //, Ball=false
            //);
            
      Plane( facePts, mode=1, color=["red",1]); //[COLORS[fi], 1] );
      //MarkPts( facePts, Label=false, Line=false, Ball=["r",0.05] );
   }
 
   //------------------------------------------------------
   //
   //  Get tip_subfaces
   //
   
   tip_subfaces = [ for(pi= prng) get_subpis_on_pi(faces, pi) ];
   // tip_subfaces= 
   //  [[2, 6, 15], [1, 10, 7], [0, 14, 11], [3, 16, 9], [4, 8, 13], [5, 12, 17]]
  
   for(subface=tip_subfaces) 
   {
     //Plane( get(new_core_pts, subface), mode=1);
     tip_subPts= get(subPts, subface);
     Plane( tip_subPts , mode=1, color=["green",1]);
     //MarkPts( tip_subPts, Label=false, Ball=false);
   }   
    
   //------------------------------------------------------
   //
   //  Get edge_subfaces
   //
   
   function get_subpi_of_pi_on_fi(pi,fi)=
   (  
      let( i_on_face= idx(faces[fi],pi ) ) 
      inherited_subfaces[fi][i_on_face]   
   );
   
   function get_edge_subface(edge)=
   (
      let( cur_ef= hash(edgefi, edge) // ["R",3,"L",2,"F",[4,5],"B",[0,1]]
         , L_fi= hash(cur_ef,"L") 
         , R_fi= hash(cur_ef,"R")
         )
      [ get_subpi_of_pi_on_fi( edge[0],L_fi)
      , get_subpi_of_pi_on_fi( edge[1],L_fi)
      , get_subpi_of_pi_on_fi( edge[1],R_fi)
      , get_subpi_of_pi_on_fi( edge[0],R_fi)
      ]
   );
   
   edge_subfaces= [ for(edge=edges) get_edge_subface(edge) ];
   //echo(edge_subfaces=edge_subfaces);
   // edge_subfaces= 
   // [ [11, 10, 1, 0], [7, 6, 2, 1], [15, 14, 0, 2], [9, 8, 4, 3]
   // , [13, 12, 5, 4], [17, 16, 3, 5], [10, 13, 8, 7], [16, 15, 6, 9]
   // , [14, 17, 12, 11]]
   
   for( fi= range(edge_subfaces) )
   {
      face = edge_subfaces[fi];
      facePts= get(subPts, face);
      //echo(fi=fi);
      //echo(face=face);
      //echo(facePts=facePts);
            
      Plane( facePts, mode=1, color=["blue",1] );
   }     
}
//dev_subdivide_all_faces();

module 1847_dev_subdivide_all_faces_loop_less()
{
   echom("1847_dev_subdivide_all_faces_loop_less()");
   echo("Check if we can loop through pts or edges and get all faces at once");
    
   shrink_ratio=0.15;
   
   //............................
   pts = cubePts(3);
   faces = rodfaces(4);
   //...............................
   //pts = [[3,0,0], ORIGIN, [1,3,0] 
   //      ,[3,0,2], [0,0,2], [1,3,2] 
   //      ];
   //faces = rodfaces(3);
   // faces = 
   //   [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
   //...............................
   pts = [ [3,0,0], ORIGIN, [0,3,0], [3.5,3,0], [1,2,3]];
   faces = faces( shape= "cone", nseg=1);
   //...............................
   //pts = hashs(POLY_SAMPLES, ["L_poly", "pts"]); 
   //faces = hashs(POLY_SAMPLES, ["L_poly", "faces"]);
   
   
   frng= [0:len(faces)-1];
   prng= [0:len(pts)-1];
   
   MarkPts(pts,Line=false, Label=["text",range(pts), "indent",0], Ball=false);
   //Frame(pts);
   
   //================================================   
   
   edgefi= get_edgefi(faces);
   // edgefi = 
   // [ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
   // , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
   // , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
   // , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
   // , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
   // , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
   // , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
   // , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
   // , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
   // ]
   
   edges = keys(edgefi);
   //echo(edges=edges);
   for(eg=edges) Line( get(pts,eg), r=0.01, color="black");
   
   //----------------------------------------------
   //  subfaces has 3 parts:
   //  
   //   inherited_subfaces
   //   tip_subfaces
   //   edge_subfaces
   //
   
   //------------------------------------------------------
   //
   //  Get inherited_subfaces
   //
 
   inherited_subfaces = re_align_a_pis(faces);
   // echo(inherited_subfaces=inherited_subfaces);
   //  inherited_subfaces= 
   //  [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
   
   a_subfacePts= [ for(face=faces)
              shrinkPts( get(pts, face), ratio = shrink_ratio ) ];
   //echo(a_subfacePts=a_subfacePts);
    
   subPts = [for(subfacePts=a_subfacePts) each subfacePts] ;
                  
   MarkPts( subPts, Label=["text", range(subPts), "color","blue"]
          , Line=false, Ball=false
          );
          
   for( fi= range(inherited_subfaces) )
   {
      //face = inherited_subfaces[fi];
      facePts= a_subfacePts[fi];
      MarkPt( len(facePts)==3? incenterPt(facePts)
                              : midPt([facePts[0], facePts[2]])
            , Label=["text", fi, "color","black", "indent",0 ]
            , Ball=false
            );
            
      Plane( facePts, mode=1, color=["red",0.5]); //[COLORS[fi], 1] );
      //MarkPts( facePts, Line=false, Ball=["r",0.05] );
   }
 
   //------------------------------------------------------
   //
   //  Get tip_subfaces
   //
   
   tip_subfaces = [ for(pi= prng) get_subpis_on_pi(faces, pi) ];
   // tip_subfaces= 
   //  [[2, 6, 15], [1, 10, 7], [0, 14, 11], [3, 16, 9], [4, 8, 13], [5, 12, 17]]
  
   for(subface=tip_subfaces) 
   {
     //Plane( get(new_core_pts, subface), mode=1);
     tip_subPts= get(subPts, subface);
     Plane( tip_subPts , mode=1, color=["green",0.5]);
     //MarkPts( tip_subPts, Label=false, Ball=false);
   }   
    
   //------------------------------------------------------
   //
   //  Get edge_subfaces
   //
   
   function get_subpi_of_pi_on_fi(pi,fi)=
   (  
      let( i_on_face= idx(faces[fi],pi ) ) 
      inherited_subfaces[fi][i_on_face]   
   );
   
   function get_edge_subface(edge)=
   (
      let( cur_ef= hash(edgefi, edge) // ["R",3,"L",2,"F",[4,5],"B",[0,1]]
         , L_fi= hash(cur_ef,"L") 
         , R_fi= hash(cur_ef,"R")
         )
      [ get_subpi_of_pi_on_fi( edge[0],L_fi)
      , get_subpi_of_pi_on_fi( edge[1],L_fi)
      , get_subpi_of_pi_on_fi( edge[1],R_fi)
      , get_subpi_of_pi_on_fi( edge[0],R_fi)
      ]
   );
   
   edge_subfaces= [ for(edge=edges) get_edge_subface(edge) ];
   //echo(edge_subfaces=edge_subfaces);
   // edge_subfaces= 
   // [ [11, 10, 1, 0], [7, 6, 2, 1], [15, 14, 0, 2], [9, 8, 4, 3]
   // , [13, 12, 5, 4], [17, 16, 3, 5], [10, 13, 8, 7], [16, 15, 6, 9]
   // , [14, 17, 12, 11]]
   
   for( fi= range(edge_subfaces) )
   {
      face = edge_subfaces[fi];
      facePts= get(subPts, face);
      //echo(fi=fi);
      //echo(face=face);
      //echo(facePts=facePts);
            
      Plane( facePts, mode=1, color=["teal",0.5] );
   } 
   
   //================== see if we can loop less ===============
   
   function edges_on_faces(faces)=
   (
      [ for(f=faces) get2(f) ]
   );
   
   module approach_loop_less() 
   {
      frng = [0:len(faces)-1];
      erng = [0:len(edges)-1];

      edges = get_edges_from_faces(faces);
      // edges = 
      // [[2, 1], [1, 0], [0, 2]
      // , [3, 4], [4, 5], [5, 3]
      //  , [1, 4], [3, 0], [2, 5]]
      
      edges_on_faces = [ for(f=faces) get2(f) ];
      
      
      inherited_subfaces = re_align_a_pis(faces);

      other_subfaces =[
      
        for(eg=edges)
          let( R_fi = [ for(fi=frng) 
                         if( has( edges_on_faces[fi], eg)) fi 
                      ][0]
             , L_fi = [ for(fi=frng) 
                         if( has( edges_on_faces[fi], [eg[1],eg[0]])) fi 
                      ][0]
                            
             )
         3     
        
        
      
      
      ];
   
   
   
   
   }
   
       
}
//1847_dev_subdivide_all_faces_loop_less();

module dev_subdivide_all_faces_L_poly()
{
   echom("dev_subdivide_all_faces_L_poly()");
   echo("Following previous successful try: dev_shrinkPts_get_all_subfaces_using_edgefi()");
    
   shrink_ratio=0.15;
   
   //............................
   pts = cubePts(3);
   faces = rodfaces(4);
   //...............................
   //pts = [[3,0,0], ORIGIN, [1,3,0] 
   //      ,[3,0,2], [0,0,2], [1,3,2] 
   //      ];
   //faces = rodfaces(3);
   // faces = 
   //   [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
   //...............................
   pts = [ [3,0,0], ORIGIN, [0,3,0], [3.5,3,0], [1,2,3]];
   faces = faces( shape= "cone", nseg=1);
   //...............................
   pts = hashs(POLY_SAMPLES, ["L_poly", "pts"]); 
   faces = hashs(POLY_SAMPLES, ["L_poly", "faces"]);
   
   
   frng= [0:len(faces)-1];
   prng= [0:len(pts)-1];
   
   //MarkPts(pts,Line=false, Label=["text",range(pts), "indent",0], Ball=false);
   //Frame(pts);
   
   //================================================   
   
   edgefi= get_edgefi(faces);
   // edgefi = 
   // [ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
   // , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
   // , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
   // , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
   // , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
   // , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
   // , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
   // , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
   // , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
   // ]
   
   edges = keys(edgefi);
   //echo(edges=edges);
   for(eg=edges) Line( get(pts,eg), r=0.01, color="black");
   
   //----------------------------------------------
   //  subfaces has 3 parts:
   //  
   //   inherited_subfaces
   //   tip_subfaces
   //   edge_subfaces
   //
   
   //------------------------------------------------------
   //
   //  Get inherited_subfaces
   //
 
   inherited_subfaces = re_align_a_pis(faces);
   // echo(inherited_subfaces=inherited_subfaces);
   //  inherited_subfaces= 
   //  [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
   
   a_subfacePts= [ for(face=faces)
              shrinkPts( get(pts, face), ratio = shrink_ratio ) ];
   //echo(a_subfacePts=a_subfacePts);
    
   subPts = [for(subfacePts=a_subfacePts) each subfacePts] ;
                  
   //MarkPts( subPts, Label=["text", range(subPts), "color","blue"]
          //, Line=false, Ball=false
          //);
          
   for( fi= range(inherited_subfaces) )
   {
      //face = inherited_subfaces[fi];
      facePts= a_subfacePts[fi];
      //MarkPt( len(facePts)==3? incenterPt(facePts)
                             // : midPt([facePts[0], facePts[2]])
            //, Label=["text", fi, "color","black", "indent",0 ]
            //, Ball=false
            //);
            
      Plane( facePts, mode=1, color=["red",1]); //[COLORS[fi], 1] );
      //MarkPts( facePts, Label=false, Line=false, Ball=["r",0.05] );
   }
 
   //------------------------------------------------------
   //
   //  Get tip_subfaces
   //
   
   tip_subfaces = [ for(pi= prng) get_subpis_on_pi(faces, pi) ];
   // tip_subfaces= 
   //  [[2, 6, 15], [1, 10, 7], [0, 14, 11], [3, 16, 9], [4, 8, 13], [5, 12, 17]]
  
   for(subface=tip_subfaces) 
   {
     //Plane( get(new_core_pts, subface), mode=1);
     tip_subPts= get(subPts, subface);
     Plane( tip_subPts , mode=1, color=["green",1]);
     //MarkPts( tip_subPts, Label=false, Ball=false);
   }   
    
   //------------------------------------------------------
   //
   //  Get edge_subfaces
   //
   
   function get_subpi_of_pi_on_fi(pi,fi)=
   (  
      let( i_on_face= idx(faces[fi],pi ) ) 
      inherited_subfaces[fi][i_on_face]   
   );
   
   function get_edge_subface(edge)=
   (
      let( cur_ef= hash(edgefi, edge) // ["R",3,"L",2,"F",[4,5],"B",[0,1]]
         , L_fi= hash(cur_ef,"L") 
         , R_fi= hash(cur_ef,"R")
         )
      [ get_subpi_of_pi_on_fi( edge[0],L_fi)
      , get_subpi_of_pi_on_fi( edge[1],L_fi)
      , get_subpi_of_pi_on_fi( edge[1],R_fi)
      , get_subpi_of_pi_on_fi( edge[0],R_fi)
      ]
   );
   
   edge_subfaces= [ for(edge=edges) get_edge_subface(edge) ];
   //echo(edge_subfaces=edge_subfaces);
   // edge_subfaces= 
   // [ [11, 10, 1, 0], [7, 6, 2, 1], [15, 14, 0, 2], [9, 8, 4, 3]
   // , [13, 12, 5, 4], [17, 16, 3, 5], [10, 13, 8, 7], [16, 15, 6, 9]
   // , [14, 17, 12, 11]]
   
   for( fi= range(edge_subfaces) )
   {
      face = edge_subfaces[fi];
      facePts= get(subPts, face);
      //echo(fi=fi);
      //echo(face=face);
      //echo(facePts=facePts);
            
      Plane( facePts, mode=1, color=["blue",1] );
   }     
}
//dev_subdivide_all_faces_L_poly();

module dev_subdivide_formula()
{
   echom("dev_subdivide_formula()");
   echo("Following previous successful try: dev_shrinkPts_get_all_subfaces_using_edgefi()");
    
   //MarkPts(pts,Line=false, Label=["text",range(pts), "indent",0], Ball=false);
   //Frame(pts);
   
   function get_subpi_of_pi_on_fi(faces, inherited_subfaces, pi,fi)=
   (  
     /*
        faces[2]    faces[4]
       ----------4-----------
         9====7  |  3====2
         |    |  |  |    |
         |    |  |  |    |
        10====6  |  2====1
       ----------3-----------
       
       get_subpi_of_pi_on_fi(faces, inherited_subfaces, 4,2) = 3
       get_subpi_of_pi_on_fi(faces, inherited_subfaces, 3,2) = 2
        
     */
      let( i_on_face= idx(faces[fi],pi ) ) 
      inherited_subfaces[fi][i_on_face]   
   );
   
   function get_edge_subface(faces, inherited_subfaces, edgefi, edge)=
   (
      /*
        -------4-------
        ====7  |  3====
            |  |  |  
            |  |  |  
        ====6  |  2====
        -------3--------
            
        For edge [3,4], return its edge_subface [2,3,7,6]
        get_edge_subface(faces, inherited_subfaces, edgefi, [3,4])= [2,3,7,6]       

      */
   
      let( cur_ef= hash(edgefi, edge) // ["R",3,"L",2,"F",[4,5],"B",[0,1]]
         , L_fi= hash(cur_ef,"L") 
         , R_fi= hash(cur_ef,"R")
         )
      [ get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[0],L_fi)
      , get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[1],L_fi)
      , get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[1],R_fi)
      , get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[0],R_fi)
      ]
   );
   
   //================================================   
   function get_smooth_poly(pts,faces)=
   (
      let( 
        prng= [0:len(pts)-1]
      , frng= [0:len(faces)-1]
      , edgefi= get_edgefi(faces)
       // edgefi = 
       // [ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
       // , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
       // , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
       // , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
       // , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
       // , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
       // , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
       // , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
       // , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
       // ]
       
     , edges = keys(edgefi)
       //----------------------------------------------
       //  subfaces has 3 parts:
       //  
       //   inherited_subfaces
       //   tip_subfaces
       //   edge_subfaces
   
       //------------------------------------------------------
       //
       //  Get inherited_subfaces
       //
       //------------------------------------------------------
       
     , inherited_subfaces = re_align_a_pis(faces)
       //  inherited_subfaces= 
       //  [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
         
     , a_subfacePts= [ for(face=faces)
                       shrinkPts( get(pts, face), ratio = shrink_ratio ) ]
       //echo(a_subfacePts=a_subfacePts);
    
     , subPts = [for(subfacePts=a_subfacePts) each subfacePts] 
 
       //------------------------------------------------------
       //
       //  Get tip_subfaces
       //
       //------------------------------------------------------
       
     , tip_subfaces = [ for(pi= prng) get_subpis_on_pi(faces, pi) ]
       // tip_subfaces= 
       //  [[2, 6, 15], [1, 10, 7], [0, 14, 11], [3, 16, 9], [4, 8, 13], [5, 12, 17]]
 
       //------------------------------------------------------
       //
       //  Get edge_subfaces
       //
       //------------------------------------------------------
        
     , edge_subfaces= [ 
          for(edge=edges) 
          get_edge_subface(faces, inherited_subfaces, edgefi, edge)
          ]
          
       //------------------------------------------------------
          
       //echo(edge_subfaces=edge_subfaces);
       // edge_subfaces= 
       // [ [11, 10, 1, 0], [7, 6, 2, 1], [15, 14, 0, 2], [9, 8, 4, 3]
       // , [13, 12, 5, 4], [17, 16, 3, 5], [10, 13, 8, 7], [16, 15, 6, 9]
       // , [14, 17, 12, 11]]
     , all_subfaces = concat( inherited_subfaces
                            , tip_subfaces
                            , edge_subfaces )  
     )
     //echo(inherited_subfaces=inherited_subfaces)
     //echo(tip_subfaces=tip_subfaces)
     //echo(edge_subfaces=edge_subfaces)
     ["pts", subPts,"faces", all_subfaces]
   );   
   
   shrink_ratio=0.20;
   
   //............................
   pts = cubePts(3);
   faces = rodfaces(4);
   //...............................
   pts = hashs(POLY_SAMPLES, ["rod3", "pts"]); //[[3,0,0], ORIGIN, [1,3,0],[3,0,2], [0,0,2], [1,3,2] ];
   faces = hashs(POLY_SAMPLES, ["rod3", "faces"]); //rodfaces(3);
   // faces = 
   //   [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
   //...............................
   pts = [ [3,0,0], ORIGIN, [0,3,0], [3.5,3,0], [1,2,3]];
   faces = faces( shape= "cone", nseg=1);
   //...............................
   
   new_data = get_smooth_poly(pts,faces); 
   Frame(pts, shape="cone");
   ptsx= hash(new_data,"pts");
   facesx= hash(new_data,"faces");
   
   echo(_red( str("shrink_ratio= ", shrink_ratio)));
   echo(_s("First cycle --- {_} pts, {_} faces", [len(ptsx),len(facesx)] ));
   
   //color("olive")
   polyhedron( points= ptsx
             , faces = facesx
             ); 

             
   pts2= [for(p=ptsx) p+[4,0,0]];
   new_data2 = get_smooth_poly(pts2,facesx); 
   //echo(pts2=pts2);
   //echo(new_data2=new_data2);
   //Frame(ptsx, shape="cone");
   pts2x= hash(new_data2,"pts");
   faces2x= hash(new_data2,"faces");
   echo(_s("2nd cycle --- {_} pts, {_} faces", [len(pts2x),len(faces2x)] ));
   polyhedron( points= pts2x
             , faces = faces2x
             ); 
   // time taken: 9 sec          
     
   /*          
   pts3= [for(p=pts2x) p+[4,0,0]];
   new_data3 = get_smooth_poly(pts3,faces2x); 
   //echo(pts2=pts2);
   //echo(new_data2=new_data2);
   //Frame(ptsx, shape="cone");
   pts3x= hash(new_data3,"pts");
   faces3x= hash(new_data3,"faces");
   echo(_s("3rd cycle --- {_} pts, {_} faces", [len(pts3x),len(faces3x)] ));
   polyhedron( points= pts3x
             , faces = faces3x
             ); 
   // Time taken: 2 min 1 sec          
   */          
}
//dev_subdivide_formula();

function 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, pi,fi)=
(  
   /* Internal function for 1848_get_edge_subface()
   
      faces[2]    faces[4]
     ----------4-----------
       9====7  |  3====2
       |    |  |  |    |
       |    |  |  |    |
      10====6  |  2====1
     ----------3-----------
       
     get_subpi_of_pi_on_fi(faces, inherited_subfaces, 4,2) = 3
     get_subpi_of_pi_on_fi(faces, inherited_subfaces, 3,2) = 2
     
     
        
   */
    let( i_on_face= idx(faces[fi],pi ) ) 
    inherited_subfaces[fi][i_on_face]   
);
   
function 1848_get_edge_subface(faces, inherited_subfaces, edgefi, edge)=
(
    /*
      -------4-------
      ====7  |  3====
          |  |  |  
          |  |  |  
      ====6  |  2====
      -------3--------
            
      For edge [3,4], return its edge_subface [6,7,3,2]
      get_edge_subface(faces, inherited_subfaces, edgefi, [3,4])= [6,7,3,2]       

    */
   
    let( cur_ef= hash(edgefi, edge) // ["R",3,"L",2,"F",[4,5],"B",[0,1]]
       , L_fi= hash(cur_ef,"L") 
       , R_fi= hash(cur_ef,"R")
       )
    [ 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[0],L_fi)
    , 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[1],L_fi)
    , 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[1],R_fi)
    , 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[0],R_fi)
    ]
);

function 1848_get_smooth_PtsFaces(pts, faces, shrink_ratio=0.15, depth=1
                                 , ptface 
                                 , _i=0)=
(
    // return [subPts, subfaces ]
    //_i>=depth? ["pts", pts, "faces", faces ]
    _i>=depth? [pts, faces ] // Change final rtn to array 
                             // for easier access later
                             // 2018.4.25
    :let( 
    , pts = _i==0 && ptface? hash(ptface, "pts"): pts
    , faces = _i==0 && ptface ? hash(ptface, "faces"):faces
    , prng= [0:len(pts)-1]
    , frng= [0:len(faces)-1]
    , edgefi= get_edgefi(faces)
     // edgefi = 
     // [ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
     // , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
     // , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
     // , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
     // , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
     // , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
     // , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
     // , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
     // , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
     // ]
       
   , edges = keys(edgefi)
     //----------------------------------------------
     //  subfaces has 3 parts:
     //  
     //   inherited_subfaces
     //   tip_subfaces
     //   edge_subfaces
   
     //------------------------------------------------------
     //
     //  Get inherited_subfaces
     //
     //------------------------------------------------------
       
   , inherited_subfaces = re_align_a_pis(faces)
     //  inherited_subfaces= 
     //  [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
         
   , a_subfacePts= [ for(face=faces)
                     shrinkPts( get(pts, face), ratio = shrink_ratio ) ]
     //echo(a_subfacePts=a_subfacePts);
    
   , subPts = [for(subfacePts=a_subfacePts) each subfacePts] 
 
     //------------------------------------------------------
     //
     //  Get tip_subfaces
     //
     //------------------------------------------------------
       
   , tip_subfaces = [ for(pi= prng) get_subpis_on_pi(faces, pi) ]
     // tip_subfaces= 
     //  [[2, 6, 15], [1, 10, 7], [0, 14, 11], [3, 16, 9], [4, 8, 13], [5, 12, 17]]
 
     //------------------------------------------------------
     //
     //  Get edge_subfaces
     //
     //------------------------------------------------------
        
   , edge_subfaces= [ 
        for(edge=edges) 
        1848_get_edge_subface(faces, inherited_subfaces, edgefi, edge)
        ]
          
     //------------------------------------------------------
          
     //echo(edge_subfaces=edge_subfaces);
     // edge_subfaces= 
     // [ [11, 10, 1, 0], [7, 6, 2, 1], [15, 14, 0, 2], [9, 8, 4, 3]
     // , [13, 12, 5, 4], [17, 16, 3, 5], [10, 13, 8, 7], [16, 15, 6, 9]
     // , [14, 17, 12, 11]]
   , all_subfaces = concat( inherited_subfaces
                          , tip_subfaces
                          , edge_subfaces )
   //, _ptsfaces= [subPts,all_subfaces]
                         
   )
   
   1848_get_smooth_PtsFaces( pts = subPts
                           , faces = all_subfaces
                           , shrink_ratio=shrink_ratio, depth=depth
                           , ptface = ptface
                           , _i=_i+1)
   //echo(inherited_subfaces=inherited_subfaces)
   //echo(tip_subfaces=tip_subfaces)
   //echo(edge_subfaces=edge_subfaces)
);   

module testing_1848_get_smooth_PtsFaces()
{
  echo("testing_1848_get_smooth_PtsFaces()");
  ptface = hash(POLY_SAMPLES,"cone4");
  depth = 2;
  aPoly= 1848_get_smooth_PtsFaces(     
                           //pts = hash(ptface,"pts") //cubePts(3)
                          //, faces = hash(ptface, "faces") // rodfaces(4)
                          , ptface = ptface
                          , shrink_ratio=0.2
                          , depth=depth
                          ); 

  subPts = aPoly[0];
  subfaces = aPoly[1];
  echo(subPts=subPts);
  echo(subfaces=subfaces);
  echo(_s("Got <b>{_}</b> pts and <b>{_}</b> faces after depth <b>{_}</b>"
               , [len(subPts), len(subfaces), depth ]
               )
            );                         
  //
  for( fi = range(subfaces) ) 
  {
    f = subfaces[fi];
    fpts = get(subPts, f);
    MarkPt( sum( fpts ) / len(f)
          , Ball=false
          , Label=["text",fi, "indent",0, "color","black", "scale",0.5] 
          ); 
    if( fi==30 || fi == 61 )      
      Line( fpts, closed=false, color="red" );
  }
  color("gold", 0.8)
  polyhedron( subPts, subfaces );                        
}
testing_1848_get_smooth_PtsFaces();

module 1849_dev_DooSabin_face_shrink_for_patch(
       pts, faces, ptsfaces   
       , shrink_ratio=0.15)
{
  echom("1849_dev_DooSabin_face_shrink_for_patch()");
  //ptsfaces = hash(POLY_SAMPLES,"poly_patch");

  //-----------------------------------------------
  
  pts   = hash(ptsfaces,"pts", pts);
  faces = hash(ptsfaces,"faces", faces);
  a_pts= [ for(pis=faces) get(pts, pis) ];
  //echo(a_pts=a_pts);
  echo(faces=faces);
  
  frng = [0:len(faces)-1];
  edgefi = get_edgefi( faces );
  //echo(edgefi=edgefi);
  edges = keys(edgefi);
  
  //-----------------------------------------------
  
  // Label face indices
  for(fi=frng)
  {
     fpts = a_pts[fi];
     //echo(fpts=fpts);
     MarkPt( sum(fpts)/len(fpts), Ball=false
            , Label = ["text", fi, "indent",0, "color","black", "scale",0.5]
            );
  
  }
  
  // Label pts
  MarkPts(pts, Ball=false, Line=false
         , Label=["text", range(pts)
                 , "scale", 0.5
                 , "indent", 0
                 ] 
         );
  
  Poly( ptsfaces=ptsfaces, color=["green",0.1] );

  // Frame
  for(eg=edges) Line( get(pts,eg), color="black", r=0.005);
  
  //-----------------------------------------------
  
  
  // Test drive :
  
  for(fi= frng)
  {
     pis = faces[fi];
     fedges = get2(pis);
     //fpts = get(pts,pis);
     
     skip_pis = [ for(pi=[0:len(fedges)-1])
                     let( eg = fedges[pi]
                        , edgefi_data = hash(edgefi,eg
                                        , hash(edgefi, [eg[1],eg[0]])
                                        )
                        , has_left_face= haskey( edgefi_data, "L") 
                        )
                     if(!has_left_face) eg[0] ];
                     
     echo( fi=fi, skip_pis=skip_pis);  
     /*
      For faces[fi=0], the edges start with pi=1 or 0 have NO left face. 
      For faces[fi=1], the edges start with pi=2 have NO left face. 
     
      ECHO: fi = 0, skip_pis = [1, 0]
      ECHO: fi = 1, skip_pis = [2]
      ECHO: fi = 2, skip_pis = [7, 3]
      ECHO: fi = 3, skip_pis = [4]
      ECHO: fi = 4, skip_pis = []
      ECHO: fi = 5, skip_pis = [11]
      ECHO: fi = 6, skip_pis = [8]
      ECHO: fi = 7, skip_pis = []
      ECHO: fi = 8, skip_pis = [15]
      ECHO: fi = 9, skip_pis = [16, 12]
      ECHO: fi = 10, skip_pis = [17]
      ECHO: fi = 11, skip_pis = [18, 19]
     */                 
  }
  
  //
  // Now we make this a function:
  //
  function skip_pis_over_faces(faces, edgefi)=
  (
     [ for(fi=[0:len(faces)-1])
       let(pis = faces[fi]
          , fedges= get2(pis)
          //, fpts = get(pts,pis)
          , skip_pis= [ for(pi=[0:len(fedges)-1])
                       let( eg = fedges[pi]
                          , edgefi_data = hash(edgefi,eg
                                          , hash(edgefi, [eg[1],eg[0]])
                                          )
                          , has_left_face= haskey( edgefi_data, "L") 
                          )
                       if(!has_left_face) eg[0] 
                     ]
                     
          )
       skip_pis       
     ]
  );
  skip_pis_over_faces= skip_pis_over_faces(faces, edgefi);
  echo(skip_pis_over_faces = skip_pis_over_faces);
  
  function shrink_for_patch( fpts // pts on the target face
                           , pis  // = face
                           , skip_pis=[], ratio=0.15)=
  (         
     /*
        faces= [[4, 5, 1, 0], [5, 6, 2, 1], [6, 7, 3, 2], [8, 9, 5, 4]
               , [9, 10, 6, 5]
               , [10, 11, 7, 6], [12, 13, 9, 8], [13, 14, 10, 9]
               , [14, 15, 11, 10], [16, 17, 13, 12]
               , [17, 18, 14, 13], [18, 19, 15, 14]]
         
        for a patch containing faces 0,1,3,4, subfaces shown in the following
        will eventually reduce the patch size 
       
         |          |          |
         8----------9----------10--
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6--
         |  +====+  |  +====+  |
         |  |  0 |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2--
        
        We need to make subfaces like this:
          
         |          |          |
         |          |          |
         8----------9----------10-----
         +=======+  |  +====+  |
         |   3   |  |  | 4  |  |
         +=======+  |  +====+  |
         4----------5----------6-----
         +=======+  |  +====+  |
         |   0   |  |  | 1  |  |
         |       |  |  |    |  |
         0=======+--1--+====+--2-----
        
         That is, special treatment needed for the displayed 4 border 
         [2,1], [1,0], [0,4], [4,8]
         
         faces[4] shrinks as usual -- for each edge calc nodepairs(@)
         that are determined by the edges before and after it.  
        
             4---@----@---5
             |   :    :   |
             @...+====+...@ 
             |   |  0 |   |  
             @...+====+...@
             |   :    :   |
             0---@----@---1
        
         
         For the rest, for each edge, check if the edge[0] (first pi) of 
         it, AND those BEFORE and AFTER it, shows up in the skip_pis list.
                      
         
      */ 
     echo("=================== shrink_for_patch()")
     echo(fpts=fpts)
     echo(pis=pis)
     echo(skip_pis=skip_pis)
     echo(ratio=ratio)
     let( fprng=[0:len(fpts)-1]
        , p2s = get2(fpts)
        , nodepairs=[ for(ei=fprng) 
                      echo(".... getting nodepairs, ei=", ei,"pi=", pis[ei]
                           , "p2s_ei", p2s[ei])
                      onlinePts( p2s[ei], ratios=[ratio, 1-ratio])
                    ]
                    
        , Bs = [ ]   
        , rtn= [ for(ei=fprng)
                 echo(__ei = ei, pi= pis[ei])
                 echo(skip_pis=skip_pis, skip_pis_has_pi=has(skip_pis, pis[ei]))
                 echo(nodepairs_ei = nodepairs[ei])
                 has( skip_pis, pis[ei] )? 
                  ( 
                   echo("---", next_pi=get(pis, ei+1) )
                   echo("---", next_skip_pis_has_pi=has(skip_pis, get(pis, ei-1)))
                   has(skip_pis, get(pis, ei-1))? // ei is the last, need 
                     fpts[ei]                   
                    :nodepairs[ei][0]
                  )  
                 :has(skip_pis, get(pis, ei-1))? // ei is the last, need 
                   get(nodepairs, ei-1)[1] 
                 :midPt( lineBridge( [nodepairs[ei][0], get(nodepairs,ei-2)[1]]
                       , [get(nodepairs,ei-1)[1], get(nodepairs,ei+1)[0]]
                       )
              ) 
              ]
        )
        echo(p2s=p2s)
        //echo(nodepairs_1=nodepairs[1])
        //echo(nodepairs=nodepairs)
        echo(shrink_for_patch__rtn=rtn)
        echo("=================== shrink_for_patch(): END")
        
        rtn
  );
  
  //echo(a_pts=a_pts);
  echo(frng=frng, frng=[for(fi=frng)fi]);
   a_subfacePts= [ 
     for(fi=frng) //face=faces)
     shrink_for_patch(
         fpts= a_pts[fi] //get(pts, faces[fi])
        ,pis = faces[fi] 
        ,skip_pis=skip_pis_over_faces[fi] //(faces, edgefi)
        , ratio=shrink_ratio)
     ];
   //echo( a_subfacePts ); 
   
   for(fi=frng)
   {
     subfacePts = a_subfacePts[fi];
     Line(subfacePts, Joint=true, r=0.01, closed= true);
   
   }             
   
}
//1849_dev_DooSabin_face_shrink_for_patch(
//   ptsfaces = hash(POLY_SAMPLES,"poly_patch")
//   );

module 1849_dev_DooSabin_face_shrink_for_patch_2(
       pts, faces, ptsfaces   
       , shrink_ratio=0.15)
{
  echom("1849_dev_DooSabin_face_shrink_for_patch_2()");
  echo("Intend to make shrink_for_patch() more efficient");
  
  //ptsfaces = hash(POLY_SAMPLES,"poly_patch");

  //-----------------------------------------------
  
  pts   = hash(ptsfaces,"pts", pts);
  faces = hash(ptsfaces,"faces", faces);
  a_pts= [ for(pis=faces) get(pts, pis) ];
  //echo(a_pts=a_pts);
  echo(faces=faces);
  
  frng = [0:len(faces)-1];
  edgefi = get_edgefi( faces );
  //echo(edgefi=edgefi);
  edges = keys(edgefi);
  echo(edgefi=edgefi);
  
  //-----------------------------------------------
  
  // Label face indices
  for(fi=frng)
  {
     fpts = a_pts[fi];
     //echo(fpts=fpts);
     MarkPt( sum(fpts)/len(fpts), Ball=false
            , Label = ["text", fi, "indent",0, "color","black", "scale",0.5]
            );
  
  }
  
  // Label pts
  MarkPts(pts, Ball=false, Line=false
         , Label=["text", range(pts)
                 , "scale", 0.5
                 , "indent", 0
                 ] 
         );
  
  Poly( ptsfaces=ptsfaces, color=["green",0.1] );

  // Frame
  for(eg=edges) Line( get(pts,eg), color="black", r=0.005);
  
  //-----------------------------------------------
  
  
  // Test drive :
  
//  for(fi= frng)
//  {
//     pis = faces[fi];
//     fedges = get2(pis);
//     //fpts = get(pts,pis);
//     
//     skip_pis = [ for(pi=[0:len(fedges)-1])
//                     let( eg = fedges[pi]
//                        , edgefi_data = hash(edgefi,eg
//                                        , hash(edgefi, [eg[1],eg[0]])
//                                        )
//                        , has_left_face= haskey( edgefi_data, "L") 
//                        )
//                     if(!has_left_face) eg[0] ];
//                     
//     echo( fi=fi, skip_pis=skip_pis);  
//     /*
//      For faces[fi=0], the edges start with pi=1 or 0 have NO left face. 
//      For faces[fi=1], the edges start with pi=2 have NO left face. 
//     
//      ECHO: fi = 0, skip_pis = [1, 0]
//      ECHO: fi = 1, skip_pis = [2]
//      ECHO: fi = 2, skip_pis = [7, 3]
//      ECHO: fi = 3, skip_pis = [4]
//      ECHO: fi = 4, skip_pis = []
//      ECHO: fi = 5, skip_pis = [11]
//      ECHO: fi = 6, skip_pis = [8]
//      ECHO: fi = 7, skip_pis = []
//      ECHO: fi = 8, skip_pis = [15]
//      ECHO: fi = 9, skip_pis = [16, 12]
//      ECHO: fi = 10, skip_pis = [17]
//      ECHO: fi = 11, skip_pis = [18, 19]
//     */                 
//  }
  
  //
  // Now we make this a function:
  //
  function border_pis_over_faces(faces, edgefi)=
  (
     [ for(fi=[0:len(faces)-1])
       let(pis = faces[fi]
          , fedges= get2(pis)
          //, fpts = get(pts,pis)
          , border_pis= [ for(pi=[0:len(fedges)-1])
                       let( eg = fedges[pi]
                          , edgefi_data = hash(edgefi,eg
                                          , hash(edgefi, [eg[1],eg[0]])
                                          )
                          , has_left_face= haskey( edgefi_data, "L") 
                          )
                       if(!has_left_face) eg[0] 
                     ]
                     
          )
       border_pis       
     ]
  );
  border_pis_over_faces= border_pis_over_faces(faces, edgefi);
  echo(border_pis_over_faces = border_pis_over_faces);
  
  function shrink_for_patch( fpts // pts on the target face
                           , pis  // = face
                           , border_pis=[], ratio=0.15)=
  (         
     /*
       
        faces= [[4, 5, 1, 0], [5, 6, 2, 1], [6, 7, 3, 2], [8, 9, 5, 4]
               , [9, 10, 6, 5]
               , [10, 11, 7, 6], [12, 13, 9, 8], [13, 14, 10, 9]
               , [14, 15, 11, 10], [16, 17, 13, 12]
               , [17, 18, 14, 13], [18, 19, 15, 14]]
         
        for a patch containing faces 0,1,3,4, subfaces shown in the following
        will eventually reduce the patch size 
       
         |          |          |
         8----------9----------10--
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6--
         |  +====+  |  +====+  |
         |  |  0 |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2--
        
        We need to make subfaces like this:
          
         |          |          |
         |          |          |
         8----------9----------10-----
         +=======+  |  +====+  |
         |   3   |  |  | 4  |  |
         +=======+  |  +====+  |
         4----------5----------6-----
         +=======+  |  +====+  |
         |   0   |  |  | 1  |  |
         |       |  |  |    |  |
         0=======+--1--+====+--2-----
        
         That is, special treatment needed for the displayed 4 border 
         [2,1], [1,0], [0,4], [4,8]
         
         faces[4] shrinks as usual -- for each edge calc nodepairs(@)
         that are determined by the edges before and after it.  
        
             4---@----@---5
             |   :    :   |
             @...+====+...@ 
             |   |  0 |   |  
             @...+====+...@
             |   :    :   |
             0---@----@---1
        
         
         For the rest, for each edge, check if the edge[0] (first pi) of 
         it, AND those BEFORE and AFTER it, shows up in the border_pis list.
         
         For this purpose, we make a array Bs (means: is border):
               
           face  pis        Bs
           0    [4,5,1,0]   [0,0,1,1] 
           3    [8,9,5,4]   [0,0,0,1]
           4    [9,10,6,5]  [0,0,0,0]
           1    [5,6,2,1]   [0,0,1,0]
           
         //===============================================
         
         We use faces[0] as example:
         
         shrink_for_patch( fpts = [ pts[4], pts[5], pts[1], pts[0] ]
                         , pis  = face
                         , border_pis=[]
                           
         edges: [4,5], [5,1], [1,0], [0,4]
         
         With Bs= [0,0,1,1], it means,
         
         Edges [4,5] and [5,1] are NOT on border;
         Edges [1,0] and [1,4] ARE on border;
                            
         We make a triplet B3:
         
           bi =   0, 1, 2, 3    // index for border 
           Bs = [ 0, 0, 1, 1 ] 	
           
           bi   B3= [ Bs[bi-1], Bs[bi], Bs[bi+1] ]  
           0   	[0,0,1]
           1   	[0,0,1]
           2   	[0,1,1]
           3  	[1,1,0]             
           
            
          B3s= get3m( Bs )= [ [0,0,1],[0,0,1],[0,1,1],[1,1,0] ]                  
                            
          We then loop through B3s determine subpts                   
                             
                            
         
      */ 
     echo("=================== shrink_for_patch()")
     echo(fpts=fpts)
     echo(pis=pis)
     echo(border_pis=border_pis) // :: [2,0]
     //echo(ratio=ratio)
     let( fprng=[0:len(fpts)-1]
        , p2s = get2(fpts)
        , nodepairs=[ for(ei=fprng) 
                      //echo(".... getting nodepairs, ei=", ei,"pi=", pis[ei], "p2s_ei", p2s[ei])
                      onlinePts( p2s[ei], ratios=[ratio, 1-ratio])
                    ]
        , Bs = [for(i=fprng) has( border_pis, pis[i])?1:0 ] // [0,1,1,0]=> edge 1,2 are border            
        , B3s = get3m( Bs )   
        /*        
                   |          |          |
                   |          |          |
                   8----------9----------10-----
                   +=======+  |  +====+  |
      for[4,8]:    |   3   |  |  | 4  |  |
          [0,1,0]  +=======+  |  +====+  |
                 \ 4----------5----------6-- [0,0,x]   5: [0,0,1]
      for[4,5]:  / +=======+  |  +====+  |
          [1,0,x]  |   0   |  |  | 1  |  |
                   |       |  |  |    |  |
                   0=======+--1--+====+--2-----
                  /          /          /
              [1,1,x]    [ 0,1,1]    [ 0,1,0]]
               
               
        */     
        , rtn = [ for(ei=fprng) 
                  // cur.edge = the edge represented by the pt, fpts[ei]
                  //          = [ pis[ei], <end_pi> ]
                 echo("..........")
                 echo(Bs=Bs, B3s= B3s)
                 echo(__ei = ei, pi= pis[ei])
                 echo(border_pis=border_pis, border_pis_has_pi=has(border_pis, pis[ei]))
                 echo(nodepairs_ei = nodepairs[ei])

                  B3s[ei]==[0,0,0] // Edges prior and after cur.edge are NOT a border
                                   // Like pts[9] (faces[2])
                                   //    , pts[10] (faces[2]) above
                                    
                  || B3s[ei]==[0,0,1]? // The edge next to cur.edge is border         
                                       // like pts[5] (faces[0])
                                       //      pts[6] (faces[1])
                                       // Same as [0,0,0]                      
                                     
                   midPt( lineBridge( [nodepairs[ei][0], get(nodepairs,ei-2)[1]]
                       , [get(nodepairs,ei-1)[1], get(nodepairs,ei+1)[0]]
                       ))
                 
                :B3s[ei]==[0,1,1]    // The cur.edge itself is a border 
                 ||B3s[ei]==[0,1,0]? // But prior edge is not.
                                     // Like pts[1], pts[2]
                                         
                   nodepairs[ei][0]
                   
                : B3s[ei]==[1,0,0] ? // || B3s[ei]==[1,1,0] ?
                  
                   get(nodepairs,ei-1)[1]
                   
                //:B3s[ei]==[1,1,0]
                // || B3s[ei]==[1,1,1]? 
                 
                : fpts[ei]   
        
        
                ]     
        )
        echo(p2s=p2s)
        //echo(nodepairs_1=nodepairs[1])
        //echo(nodepairs=nodepairs)
        echo(shrink_for_patch__rtn=rtn)
        echo("=================== shrink_for_patch(): END")
        
        rtn
  );
  
  //echo(a_pts=a_pts);
  echo(frng=frng, frng=[for(fi=frng)fi]);
   a_subfacePts= [ 
     for(fi=frng) //face=faces)
     shrink_for_patch(
         fpts= a_pts[fi] //get(pts, faces[fi])
        ,pis = faces[fi] 
        ,border_pis=border_pis_over_faces[fi] //(faces, edgefi)
        , ratio=shrink_ratio)
     ];
   //echo( a_subfacePts ); 
   
   for(fi=frng)
   {
     subfacePts = a_subfacePts[fi];
     Line(subfacePts, Joint=true, r=0.01, closed= true);
   
   }             
   
}
//1849_dev_DooSabin_face_shrink_for_patch_2(
//   ptsfaces = hash(POLY_SAMPLES,"poly_patch")
//   );

module 180410_dev_DooSabin_face_shrink_for_patch(
       pts, faces, ptsfaces   
       , shrink_ratio=0.15)
{
  echom("180410_dev_DooSabin_face_shrink_for_patch()");
  echo("Intend to make shrink_for_patch uses only fpts, faces and edgefi that are");
  echo("usually available in subdiv calc. (So users don't have to calc border_pis)");
  
  
  //ptsfaces = hash(POLY_SAMPLES,"poly_patch");

  //-----------------------------------------------
  
  pts   = hash(ptsfaces,"pts", pts);
  faces = hash(ptsfaces,"faces", faces);
  a_pts= [ for(pis=faces) get(pts, pis) ];
  //echo(a_pts=a_pts);
  echo(faces=faces);
  
  frng = [0:len(faces)-1];
  edgefi = get_edgefi( faces );
  //echo(edgefi=edgefi);
  edges = keys(edgefi);
  echo(edgefi=edgefi);
  
  //-----------------------------------------------
  
  // Label face indices
  for(fi=frng)
  {
     fpts = a_pts[fi];
     //echo(fpts=fpts);
     MarkPt( sum(fpts)/len(fpts), Ball=false
            , Label = ["text", fi, "indent",0, "color","black", "scale",0.5]
            );
  
  }
  
  // Label pts
  MarkPts(pts, Ball=false, Line=false
         , Label=["text", range(pts)
                 , "scale", 0.5
                 , "indent", 0
                 ] 
         );
  
  Poly( ptsfaces=ptsfaces, color=["green",0.1] );

  // Frame
  for(eg=edges) Line( get(pts,eg), color="black", r=0.005);
  
  //-----------------------------------------------
  
  
  // Test drive :
  
//  for(fi= frng)
//  {
//     pis = faces[fi];
//     fedges = get2(pis);
//     //fpts = get(pts,pis);
//     
//     skip_pis = [ for(pi=[0:len(fedges)-1])
//                     let( eg = fedges[pi]
//                        , edgefi_data = hash(edgefi,eg
//                                        , hash(edgefi, [eg[1],eg[0]])
//                                        )
//                        , has_left_face= haskey( edgefi_data, "L") 
//                        )
//                     if(!has_left_face) eg[0] ];
//                     
//     echo( fi=fi, skip_pis=skip_pis);  
//     /*
//      For faces[fi=0], the edges start with pi=1 or 0 have NO left face. 
//      For faces[fi=1], the edges start with pi=2 have NO left face. 
//     
//      ECHO: fi = 0, skip_pis = [1, 0]
//      ECHO: fi = 1, skip_pis = [2]
//      ECHO: fi = 2, skip_pis = [7, 3]
//      ECHO: fi = 3, skip_pis = [4]
//      ECHO: fi = 4, skip_pis = []
//      ECHO: fi = 5, skip_pis = [11]
//      ECHO: fi = 6, skip_pis = [8]
//      ECHO: fi = 7, skip_pis = []
//      ECHO: fi = 8, skip_pis = [15]
//      ECHO: fi = 9, skip_pis = [16, 12]
//      ECHO: fi = 10, skip_pis = [17]
//      ECHO: fi = 11, skip_pis = [18, 19]
//     */                 
//  }
  
  //
  // Now we make this a function:
  //
  function border_pis_over_faces(faces, edgefi)=
  (
     [ for(fi=[0:len(faces)-1])
       let(pis = faces[fi]
          , fedges= get2(pis)
          //, fpts = get(pts,pis)
          , border_pis= [ for(pi=[0:len(fedges)-1])
                       let( eg = fedges[pi]
                          , edgefi_data = hash(edgefi,eg
                                          , hash(edgefi, [eg[1],eg[0]])
                                          )
                          , has_left_face= haskey( edgefi_data, "L") 
                          )
                       if(!has_left_face) eg[0] 
                     ]
                     
          )
       border_pis       
     ]
  );
  //border_pis_over_faces= border_pis_over_faces(faces, edgefi);
  //echo(border_pis_over_faces = border_pis_over_faces);
  
  function shrink_for_patch( fpts // pts on the target face
                           , pis  // = face
                           , edgefi
                           //, border_pis=[]
                           , ratio=0.15)=
  (         
     /*
       
        faces= [[4, 5, 1, 0], [5, 6, 2, 1], [6, 7, 3, 2], [8, 9, 5, 4]
               , [9, 10, 6, 5]
               , [10, 11, 7, 6], [12, 13, 9, 8], [13, 14, 10, 9]
               , [14, 15, 11, 10], [16, 17, 13, 12]
               , [17, 18, 14, 13], [18, 19, 15, 14]]
         
        for a patch containing faces 0,1,3,4, subfaces shown in the following
        will eventually reduce the patch size 
       
         |          |          |
         8----------9----------10--
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6--
         |  +====+  |  +====+  |
         |  |  0 |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2--
        
        We need to make subfaces like this:
          
         |          |          |
         |          |          |
         8----------9----------10-----
         +=======+  |  +====+  |
         |   3   |  |  | 4  |  |
         +=======+  |  +====+  |
         4----------5----------6-----
         +=======+  |  +====+  |
         |   0   |  |  | 1  |  |
         |       |  |  |    |  |
         0=======+--1--+====+--2-----
        
         That is, special treatment needed for the displayed 4 border 
         [2,1], [1,0], [0,4], [4,8]
         
         faces[4] shrinks as usual -- for each edge calc nodepairs(@)
         that are determined by the edges before and after it.  
        
             4---@----@---5
             |   :    :   |
             @...+====+...@ 
             |   |  0 |   |  
             @...+====+...@
             |   :    :   |
             0---@----@---1
        
         
         For the rest, for each edge, check if the edge[0] (first pi) of 
         it, AND those BEFORE and AFTER it, shows up in the border_pis list.
         
         For this purpose, we make a array Bs (means: is border):
               
           face  pis        Bs
           0    [4,5,1,0]   [0,0,1,1] 
           3    [8,9,5,4]   [0,0,0,1]
           4    [9,10,6,5]  [0,0,0,0]
           1    [5,6,2,1]   [0,0,1,0]
           
         //===============================================
         
         We use faces[0] as example:
         
         shrink_for_patch( fpts = [ pts[4], pts[5], pts[1], pts[0] ]
                         , pis  = face
                         , border_pis=[]
                           
         edges: [4,5], [5,1], [1,0], [0,4]
         
         With Bs= [0,0,1,1], it means,
         
         Edges [4,5] and [5,1] are NOT on border;
         Edges [1,0] and [1,4] ARE on border;
                            
         We make a triplet B3:
         
           bi =   0, 1, 2, 3    // index for border 
           Bs = [ 0, 0, 1, 1 ] 	
           
           bi   B3= [ Bs[bi-1], Bs[bi], Bs[bi+1] ]  
           0   	[0,0,1]
           1   	[0,0,1]
           2   	[0,1,1]
           3  	[1,1,0]             
           
            
          B3s= get3m( Bs )= [ [0,0,1],[0,0,1],[0,1,1],[1,1,0] ]                  
                            
          We then loop through B3s determine subpts                   
                             
                            
         
      */ 
     echo("=================== shrink_for_patch()")
     echo(fpts=fpts)
     echo(pis=pis)
     //echo(border_pis=border_pis) // :: [2,0]
     //echo(ratio=ratio)
     let( fprng=[0:len(fpts)-1]
        , p2s = get2(fpts)
        , edges = get2(pis)
        , nodepairs=[ for(ei=fprng) 
                      //echo(".... getting nodepairs, ei=", ei,"pi=", pis[ei], "p2s_ei", p2s[ei])
                      onlinePts( p2s[ei], ratios=[ratio, 1-ratio])
                    ]
        , Bs= [ for( ei= range(edges)) // eg= get2(pis)) 
                 //echo( edgefi_eg = hash(edgefi,_eg) )   
                 let( eg= edges[ei]
                    , _eg = haskey(edgefi,eg)?eg:[eg[1],eg[0]] 
                    )
                 haskey( hash(edgefi,_eg), "L" )?0:1
              ]             
        //, Bs = [for(i=fprng) has( border_pis, pis[i])?1:0 ] // [0,1,1,0]=> edge 1,2 are border            
        , B3s = get3m( Bs )   
        /*        
                   |          |          |
                   |          |          |
                   8----------9----------10-----
                   +=======+  |  +====+  |
      for[4,8]:    |   3   |  |  | 4  |  |
          [0,1,0]  +=======+  |  +====+  |
                 \ 4----------5----------6-- [0,0,x]   5: [0,0,1]
      for[4,5]:  / +=======+  |  +====+  |
          [1,0,x]  |   0   |  |  | 1  |  |
                   |       |  |  |    |  |
                   0=======+--1--+====+--2-----
                  /          /          /
              [1,1,x]    [ 0,1,1]    [ 0,1,0]]
               
               
        */     
        , rtn = [ for(ei=fprng) 
                  // cur.edge = the edge represented by the pt, fpts[ei]
                  //          = [ pis[ei], <end_pi> ]
                 echo("..........")
                 echo(Bs=Bs, B3s= B3s)
                 echo(__ei = ei, pi= pis[ei])
                 echo(edges = edges)
                 //echo(border_pis=border_pis, border_pis_has_pi=has(border_pis, pis[ei]))
                 echo(nodepairs_ei = nodepairs[ei])

                  B3s[ei]==[0,0,0] // Edges prior and after cur.edge are NOT a border
                                   // Like pts[9] (faces[2])
                                   //    , pts[10] (faces[2]) above
                                    
                  || B3s[ei]==[0,0,1]? // The edge next to cur.edge is border         
                                       // like pts[5] (faces[0])
                                       //      pts[6] (faces[1])
                                       // Same as [0,0,0]                      
                                     
                   midPt( lineBridge( [nodepairs[ei][0], get(nodepairs,ei-2)[1]]
                       , [get(nodepairs,ei-1)[1], get(nodepairs,ei+1)[0]]
                       ))
                 
                :B3s[ei]==[0,1,1]    // The cur.edge itself is a border 
                 ||B3s[ei]==[0,1,0]? // But prior edge is not.
                                     // Like pts[1], pts[2]
                                         
                   nodepairs[ei][0]
                   
                : B3s[ei]==[1,0,0] ? // || B3s[ei]==[1,1,0] ?
                  
                   get(nodepairs,ei-1)[1]
                   
                //:B3s[ei]==[1,1,0]
                // || B3s[ei]==[1,1,1]? 
                 
                : fpts[ei]   
        
        
                ]     
        )
        echo(p2s=p2s)
        //echo(nodepairs_1=nodepairs[1])
        //echo(nodepairs=nodepairs)
        echo(shrink_for_patch__rtn=rtn)
        echo("=================== shrink_for_patch(): END")
        
        rtn
  );
  
  //echo(a_pts=a_pts);
  echo(frng=frng, frng=[for(fi=frng)fi]);
   a_subfacePts= [ 
     for(fi=frng) //face=faces)
     shrink_for_patch(
         fpts= a_pts[fi] //get(pts, faces[fi])
        ,pis = faces[fi] 
        ,edgefi = edgefi
        //,border_pis=border_pis_over_faces[fi] //(faces, edgefi)
        , ratio=shrink_ratio)
     ];
   //echo( a_subfacePts ); 
   
   for(fi=frng)
   {
     subfacePts = a_subfacePts[fi];
     Line(subfacePts, Joint=true, r=0.01, closed= true);
   
   }             
   
}
//180410_dev_DooSabin_face_shrink_for_patch(
//   ptsfaces = hash(POLY_SAMPLES,"poly_patch") );
   
module 180414_dev_DooSabin_face_shrink_for_mesh3(
       pts, faces, ptsfaces   
       , shrink_ratio=0.15)
{
  echom("180410_dev_DooSabin_face_shrink_for_patch()");
  echo("Intend to make shrink_for_patch uses only fpts, faces and edgefi that are");
  echo("usually available in subdiv calc. (So users don't have to calc border_pis)");
  
  
  //ptsfaces = hash(POLY_SAMPLES,"poly_patch");

  //-----------------------------------------------
  
  pts   = hash(ptsfaces,"pts", pts);
  faces = hash(ptsfaces,"faces", faces);
  a_pts= [ for(pis=faces) get(pts, pis) ];
  //echo(a_pts=a_pts);
  //echo(faces=faces);
  
  frng = [0:len(faces)-1];
  edgefi = get_edgefi( faces );
  //echo(edgefi=edgefi);
  edges = keys(edgefi);
  //echo(edgefi=edgefi);
  
  //-----------------------------------------------
  
  // Label face indices
  for(fi=frng)
  {
     fpts = a_pts[fi];
     //echo(fpts=fpts);
     MarkPt( sum(fpts)/len(fpts), Ball=false
            , Label = ["text", fi, "indent",0, "color","black", "scale",0.5]
            );
  
  }
  
  // Label pts
  MarkPts(pts, Ball=false, Line=false
         , Label=["text", range(pts)
                 , "scale", 0.5
                 , "indent", 0
                 ] 
         );
  
  
  // Frame
  for(eg=edges) Line( get(pts,eg), color="black", r=0.005);
  
  //-----------------------------------------------
  
  
  // Test drive :
  
//  for(fi= frng)
//  {
//     pis = faces[fi];
//     fedges = get2(pis);
//     //fpts = get(pts,pis);
//     
//     skip_pis = [ for(pi=[0:len(fedges)-1])
//                     let( eg = fedges[pi]
//                        , edgefi_data = hash(edgefi,eg
//                                        , hash(edgefi, [eg[1],eg[0]])
//                                        )
//                        , has_left_face= haskey( edgefi_data, "L") 
//                        )
//                     if(!has_left_face) eg[0] ];
//                     
//     echo( fi=fi, skip_pis=skip_pis);  
//     /*
//      For faces[fi=0], the edges start with pi=1 or 0 have NO left face. 
//      For faces[fi=1], the edges start with pi=2 have NO left face. 
//     
//      ECHO: fi = 0, skip_pis = [1, 0]
//      ECHO: fi = 1, skip_pis = [2]
//      ECHO: fi = 2, skip_pis = [7, 3]
//      ECHO: fi = 3, skip_pis = [4]
//      ECHO: fi = 4, skip_pis = []
//      ECHO: fi = 5, skip_pis = [11]
//      ECHO: fi = 6, skip_pis = [8]
//      ECHO: fi = 7, skip_pis = []
//      ECHO: fi = 8, skip_pis = [15]
//      ECHO: fi = 9, skip_pis = [16, 12]
//      ECHO: fi = 10, skip_pis = [17]
//      ECHO: fi = 11, skip_pis = [18, 19]
//     */                 
//  }
  
  //
  // Now we make this a function:
  //
  function border_pis_over_faces(faces, edgefi)=
  (
     [ for(fi=[0:len(faces)-1])
       let(pis = faces[fi]
          , fedges= get2(pis)
          //, fpts = get(pts,pis)
          , border_pis= [ for(pi=[0:len(fedges)-1])
                       let( eg = fedges[pi]
                          , edgefi_data = hash(edgefi,eg
                                          , hash(edgefi, [eg[1],eg[0]])
                                          )
                          , has_left_face= haskey( edgefi_data, "L") 
                          )
                       if(!has_left_face) eg[0] 
                     ]
                     
          )
       border_pis       
     ]
  );
  //border_pis_over_faces= border_pis_over_faces(faces, edgefi);
  //echo(border_pis_over_faces = border_pis_over_faces);
  
  function shrink_for_patch( fpts // pts on the target face
                           , pis  // = face
                           , edgefi
                           //, border_pis=[]
                           , ratio=0.15)=
  (         
     /*
       
        faces= [[4, 5, 1, 0], [5, 6, 2, 1], [6, 7, 3, 2], [8, 9, 5, 4]
               , [9, 10, 6, 5]
               , [10, 11, 7, 6], [12, 13, 9, 8], [13, 14, 10, 9]
               , [14, 15, 11, 10], [16, 17, 13, 12]
               , [17, 18, 14, 13], [18, 19, 15, 14]]
         
        for a patch containing faces 0,1,3,4, subfaces shown in the following
        will eventually reduce the patch size 
       
         |          |          |
         8----------9----------10--
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6--
         |  +====+  |  +====+  |
         |  |  0 |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2--
        
        We need to make subfaces like this:
          
         |          |          |
         |          |          |
         8----------9----------10-----
         +=======+  |  +====+  |
         |   3   |  |  | 4  |  |
         +=======+  |  +====+  |
         4----------5----------6-----
         +=======+  |  +====+  |
         |   0   |  |  | 1  |  |
         |       |  |  |    |  |
         0=======+--1--+====+--2-----
        
         That is, special treatment needed for the displayed 4 border 
         [2,1], [1,0], [0,4], [4,8]
         
         faces[4] shrinks as usual -- for each edge calc nodepairs(@)
         that are determined by the edges before and after it.  
        
             4---@----@---5
             |   :    :   |
             @...+====+...@ 
             |   |  0 |   |  
             @...+====+...@
             |   :    :   |
             0---@----@---1
        
         
         For the rest, for each edge, check if the edge[0] (first pi) of 
         it, AND those BEFORE and AFTER it, shows up in the border_pis list.
         
         For this purpose, we make a array Bs (means: is border):
               
           face  pis        Bs
           0    [4,5,1,0]   [0,0,1,1] 
           3    [8,9,5,4]   [0,0,0,1]
           4    [9,10,6,5]  [0,0,0,0]
           1    [5,6,2,1]   [0,0,1,0]
           
         //===============================================
         
         We use faces[0] as example:
         
         shrink_for_patch( fpts = [ pts[4], pts[5], pts[1], pts[0] ]
                         , pis  = face
                         , border_pis=[]
                           
         edges: [4,5], [5,1], [1,0], [0,4]
         
         With Bs= [0,0,1,1], it means,
         
         Edges [4,5] and [5,1] are NOT on border;
         Edges [1,0] and [1,4] ARE on border;
                            
         We make a triplet B3:
         
           bi =   0, 1, 2, 3    // index for border 
           Bs = [ 0, 0, 1, 1 ] 	
           
           bi   B3= [ Bs[bi-1], Bs[bi], Bs[bi+1] ]  
           0   	[0,0,1]
           1   	[0,0,1]
           2   	[0,1,1]
           3  	[1,1,0]             
           
            
          B3s= get3m( Bs )= [ [0,0,1],[0,0,1],[0,1,1],[1,1,0] ]                  
                            
          We then loop through B3s determine subpts                   
                             
                            
         
      */ 
     echo("=================== shrink_for_patch()")
     echo(fpts=fpts)
     echo(pis=pis)
     //echo(border_pis=border_pis) // :: [2,0]
     //echo(ratio=ratio)
     let( fprng=[0:len(fpts)-1]
        , p2s = get2(fpts)
        , edges = get2(pis)
        , nodepairs=[ for(ei=fprng) 
                      //echo(".... getting nodepairs, ei=", ei,"pi=", pis[ei], "p2s_ei", p2s[ei])
                      onlinePts( p2s[ei], ratios=[ratio, 1-ratio])
                    ]
        , Bs= [ for( ei= range(edges)) // eg= get2(pis)) 
                 //echo( edgefi_eg = hash(edgefi,_eg) )   
                 let( eg= edges[ei]
                    , _eg = haskey(edgefi,eg)?eg:[eg[1],eg[0]] 
                    )
                 haskey( hash(edgefi,_eg), "L" )?0:1
              ]             
        //, Bs = [for(i=fprng) has( border_pis, pis[i])?1:0 ] // [0,1,1,0]=> edge 1,2 are border            
       // , B3s = get3m( Bs )   
        /*        
                   |          |          |
                   |          |          |
                   8----------9----------10-----
                   +=======+  |  +====+  |
      for[4,8]:    |   3   |  |  | 4  |  |
          [0,1,0]  +=======+  |  +====+  |
                 \ 4----------5----------6-- [0,0,x]   5: [0,0,1]
      for[4,5]:  / +=======+  |  +====+  |
          [1,0,x]  |   0   |  |  | 1  |  |
                   |       |  |  |    |  |
                   0=======+--1--+====+--2-----
                  /          /          /
              [1,1,x]    [ 0,1,1]    [ 0,1,0]]
               
               
        */     
        , rtn = [ for(ei=fprng) 
                  // cur.edge = the edge represented by the pt, fpts[ei]
                  //          = [ pis[ei], <end_pi> ]
                 echo("..........")
                 echo(__ei = ei, pi= pis[ei])
                 echo(Bs=Bs, B3s= B3s)
                 echo(edges = edges)
                 //echo(border_pis=border_pis, border_pis_has_pi=has(border_pis, pis[ei]))
                 echo(nodepairs_ei = nodepairs[ei])
                 let(B3= get3m(Bs,ei))
  
                  B3==[0,0,0] // Edges prior and after cur.edge are NOT a border
                                   // Like pts[9] (faces[2])
                                   //    , pts[10] (faces[2]) above
                                    
                  || B3==[0,0,1]? // The edge next to cur.edge is border         
                                       // like pts[5] (faces[0])
                                       //      pts[6] (faces[1])
                                       // Same as [0,0,0]                      
                                     
                   midPt( lineBridge( [nodepairs[ei][0], get(nodepairs,ei-2)[1]]
                       , [get(nodepairs,ei-1)[1], get(nodepairs,ei+1)[0]]
                       ))
                 
                :B3==[0,1,1]    // The cur.edge itself is a border 
                 ||B3==[0,1,0]? // But prior edge is not.
                                     // Like pts[1], pts[2]
                                         
                   nodepairs[ei][0]
                   
                   
                : B3==[1,0,0] 
               ||B3==[1,0,1]? // || B3s[ei]==[1,1,0] ?
                  
                   get(nodepairs,ei-1)[1]
                 
                : fpts[ei]   
        
        
                ]     
        )
        echo(p2s=p2s)
        //echo(nodepairs_1=nodepairs[1])
        //echo(nodepairs=nodepairs)
        echo(shrink_for_patch__rtn=rtn)
        echo("=================== shrink_for_patch(): END")
        
        rtn
  );
   
  //echo(a_pts=a_pts);
  //echo(frng=frng, frng=[for(fi=frng)fi]);
   a_subfacePts= [ 
     for(fi=frng) //face=faces)
     shrink_for_patch(
         fpts= a_pts[fi] //get(pts, faces[fi])
        ,pis = faces[fi] 
        ,edgefi = edgefi
        //,border_pis=border_pis_over_faces[fi] //(faces, edgefi)
        , ratio=shrink_ratio)
     ];
   //echo( a_subfacePts ); 
   
   
   
   
   for(fi=frng)
   {
     subfacePts = a_subfacePts[fi];
     Line(subfacePts, Joint=true, r=0.01, closed= true, color="red");
   
   }             
  
   Poly( ptsfaces=ptsfaces, color=["green",0.1] );
 
}
//180414_dev_DooSabin_face_shrink_for_mesh3(
//   ptsfaces = hash(POLY_SAMPLES,"4x4_mesh_3") );
//   //ptsfaces = hash(POLY_SAMPLES,"4x4_mesh_4") );
//   //ptsfaces = hash(POLY_SAMPLES,"3x3_mesh_3") );
   
