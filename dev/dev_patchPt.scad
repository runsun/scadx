include <../scadx.scad>

module 180414_test()
{
  echom("180414_test()");
  xm=3;
  ym=3;
  _pts = [ for(x=range(xm)) for(y=range(ym)) [x,y,0] ];
  pts = [ for(pt=_pts) pt+randPt(opt=["x",0.1, "y",0.1,"z",0.3])]; //x=0.3,y=0.3,z=0.8) ];
  echo(pts=pts);
  
  //MarkPts(_pts);
  MarkPts(pts, Ball=false, Line=false);
  
           // 0,1,2        // 0,1
  _faces= [for(x=range(xm-1)) 
           each [for(y=range(ym-1))
                 [ x*ym+y
                 , x*ym+y+1
                 , (x+1)*ym+y+1
                 , (x+1)*ym+y
                 ]
                ]
         ];
  faces= [for(x=range(xm-1)) 
           each [for(y=range(ym-1))
                 each [ [ x*ym+y, x*ym+y+1, (x+1)*ym+y ]
                      , [ x*ym+y+1, (x+1)*ym+y+1, (x+1)*ym+y ]
                      ]
                ]
         ];
  
  faces = faces(shape="mesh", nx=xm, ny=ym, isTriangle=1);
         
  //, ptsfaces = hash(POLY_SAMPLES,"4x4_mesh_3") ;
  //ptsfaces = hash(POLY_SAMPLES,"4x4_mesh_4") ;
  //ptsfaces = hash(POLY_SAMPLES,"3x3_mesh_3") ;
                               
  //pts= hash(ptsfaces,"pts");
  //faces = hash( ptsfaces, "faces");
  
  /*
    x  y
    0  0   [0,1,4,3]
    0  1   [1,2,5,4]
    1  0   [3,4,7,6]
    1  1   [4,5,8,7]
    2  0   [6,7,10,9]
    2  1   [7,8,11,10]
  
  
  */
  edgefi = get_edgefi( faces );
  
  edges = keys(edgefi);
  color("black") 
  for(eg = edges) 
     Line(get(pts,eg), r=0.01);
  
  
  echo(faces=faces);
  echo(edgefi= edgefi );
  
  sm_pts = get_smooth_PtsFaces(pts, faces
                              , shrink_ratio=0.2
                              , depth=1
                              );
  
  subpts = hash(sm_pts, "pts");
  subfaces= hash(sm_pts,"faces");
  for(fi=range(subfaces))
  {  f=subfaces[fi];
     fpts= get(subpts,f);
     MarkPt( sum(fpts)/len(fpts), Ball=false
           , Label=["text",fi, "indent",0, "color","red", "scale",0.5]);
     Line( fpts, color="red", r=0.005 );
     Plane( fpts,color=COLORS[fi], mode=1);
  }   
  //polyhedron(pts, faces);
  //color("olive", 0.9)
  //Poly( ptsfaces = sm_pts );
  
}

180414_test();