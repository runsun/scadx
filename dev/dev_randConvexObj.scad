include <../scadx.scad>

/*
     Develop a way to make a random convex obj
     
     2018.4.7: failed 
*/

module randConvObj_try1()
{
    echom("randConvObj_try1()");
    nPts= 5;

    Coord();
    
    pts= randPts( nPts );
    pts= [ [0.67285, -0.759524, -0.5]
         , [-0.973564, 0.767779, 1.37641]
         , [-2.75501, -1.31529, 1.16545]
         , [-2.33974, -0.197278, 3.14805]
         , [0.481721, -2.46017, 2.03219]
         ];
    pts=  [[0.658614, -1.49544, 2.38048], [2.07415, -0.527136, -1.58525], [-2.21612, 2.80328, -0.0881382], [-2.44849, -2.70109, -0.199671], [-1.26712, -1.62824, -0.201532]]
          ;     
    pts = cubePts(3); // <=== failed
    pts= randPts(5);
    
    echo(pts=pts);
    MarkPts(pts);//, Label=["text",true]);

    //xrng= [ min( [for(p=pts)p.x]), max( [for(p=pts)p.x]) ]; 
    yrng= [ min( [for(p=pts)p.y]), max( [for(p=pts)p.y]) ]; 
    zrng= [ min( [for(p=pts)p.z]), max( [for(p=pts)p.z]) ]; 
    frame_yzs=[ [yrng[0], zrng[0]] 
              , [yrng[1], zrng[0]]
              , [yrng[1], zrng[1]]
              , [yrng[0], zrng[1]]
              ];

    pts_with_i = [ for(i=range(pts)) concat( pts[i], [i] ) ];
    //echo(pts_with_i=pts_with_i);
    // pts_with_i = 
    // [[1.67285, -0.759524, -0.5, 0], [-0.973564, 0.767779, 1.37641, 1]
    //, [-2.75501, -1.31529, 1.16545, 2], [-2.33974, -0.197278, 3.14805, 3]
    //, [0.481721, -2.46017, 2.03219, 4]]

    pts_sortx = sort(pts_with_i);
    echo(pts_sortx= pts_sortx);
    // pts_sortx = 
    // [[-2.75501, -1.31529, 1.16545, 2]
    //, [-2.33974, -0.197278, 3.14805, 3]
    //, [-0.973564, 0.767779, 1.37641, 1]
    //, [0.481721, -2.46017, 2.03219, 4]
    //, [1.67285, -0.759524, -0.5, 0]
    //  ]
    
    pis_sortx = [ for(p=pts_sortx) p[3] ];
    echo(pis_sortx=pis_sortx);
    //  pis_sortx = [2, 3, 1, 4, 0]
    
    
    pi3s = get3(pis_sortx);
    echo(pi3s=pi3s);
    // pi3s = [[2, 3, 1], [3, 1, 4], [1, 4, 0], [4, 0, 2], [0, 2, 3]]


    for(i = [0:len(pi3s)-2]) //range(pi3s))
    {
       pi3= pi3s[i];
       pt3= get(pts, pi3);
       MarkPt( sum(pt3)/3, Ball=false, Label=["text",i,"color",COLORS[i]] );
       Plane( pt3, mode=3, color=[COLORS[i],1] );
    }
    
    function count_pis(pi3s)= // Return an arr: [len(pi_0),len(pi_1),...]
    (
      let( flat_pi3s = flatten(pi3s)
         , pirng = [0:max(flat_pi3s)]
         )
      [ for( pi=pirng ) count(pi, flat_pi3s) ]   
      //_i>=len(pi3s)? _a
      // : let( pirng = [0:max(flatten(pi3s))]
           //,  _a= _a==[]? [for( i=pirng ) 0 ]
                       // : _a pi3= pi3s[_i]
           //, count_is( pi3s
                     //, [ for(i= pirng) 
    
    );
    echo( "pi frequency= ", count_pis(pi3s) );
    
    //================= check obj center
    cPt = sum(pts)/len(pts);
    MarkPt( cPt);
    
    // We put a vertical plane on each pt :
    
    for( pi= pis_sortx )
    {
       frame_pts= [for(i=[0:3]) concat( [pts[pi].x], frame_yzs[i])];
       //echo(pi=pi, frame_pts=frame_pts);
       Plane( frame_pts, mode=4, color=["green",0.1] 
            );
    }
}
randConvObj_try1();

module randConvObj_try2()
{
    echom("randConvObj_try2()");
    nPts= 5;

    Coord();
    
    pts= randPts( nPts );
    pts= [ [0.67285, -0.759524, -0.5]
         , [-0.973564, 0.767779, 1.37641]
         , [-2.75501, -1.31529, 1.16545]
         , [-2.33974, -0.197278, 3.14805]
         , [0.481721, -2.46017, 2.03219]
         ];
    //pts = cubePts(3);
    echo(pts=pts);
    MarkPts(pts);//, Label=["text",true]);

   //================= check obj center
    cPt = sum(pts)/len(pts);
    MarkPt( cPt);
    
    //=======================================
    pts_with_i = [ for(i=range(pts)) concat( pts[i], [i] ) ];
    //echo(pts_with_i=pts_with_i);
    // pts_with_i = 
    // [[1.67285, -0.759524, -0.5, 0], [-0.973564, 0.767779, 1.37641, 1]
    //, [-2.75501, -1.31529, 1.16545, 2], [-2.33974, -0.197278, 3.14805, 3]
    //, [0.481721, -2.46017, 2.03219, 4]]

    pts_sortx = sort(pts_with_i);
    echo(pts_sortx= pts_sortx);
    // pts_sortx = 
    // [[-2.75501, -1.31529, 1.16545, 2]
    //, [-2.33974, -0.197278, 3.14805, 3]
    //, [-0.973564, 0.767779, 1.37641, 1]
    //, [0.481721, -2.46017, 2.03219, 4]
    //, [1.67285, -0.759524, -0.5, 0]
    //  ]
    
    pis_sortx = [ for(p=pts_sortx) p[3] ];
    echo(pis_sortx=pis_sortx);
    //  pis_sortx = [2, 3, 1, 4, 0]
    
    
    pi3s = get3(pis_sortx);
    echo(pi3s=pi3s);
    // pi3s = [[2, 3, 1], [3, 1, 4], [1, 4, 0], [4, 0, 2], [0, 2, 3]]


    for(i = [0:len(pi3s)-2]) //range(pi3s))
    {
       pi3= pi3s[i];
       pt3= get(pts, pi3);
       MarkPt( sum(pt3)/3, Ball=false, Label=["text",i,"color",COLORS[i]] );
       //Plane( pt3, mode=3, color=[COLORS[i],1] );
    }
    

    /*
    //xrng= [ min( [for(p=pts)p.x]), max( [for(p=pts)p.x]) ]; 
    yrng= [ min( [for(p=pts)p.y]), max( [for(p=pts)p.y]) ]; 
    zrng= [ min( [for(p=pts)p.z]), max( [for(p=pts)p.z]) ]; 
    frame_yzs=[ [yrng[0], zrng[0]] 
              , [yrng[1], zrng[0]]
              , [yrng[1], zrng[1]]
              , [yrng[0], zrng[1]]
              ];

    
    function count_pis(pi3s)= // Return an arr: [len(pi_0),len(pi_1),...]
    (
      let( flat_pi3s = flatten(pi3s)
         , pirng = [0:max(flat_pi3s)]
         )
      [ for( pi=pirng ) count(pi, flat_pi3s) ]   
      //_i>=len(pi3s)? _a
      // : let( pirng = [0:max(flatten(pi3s))]
           //,  _a= _a==[]? [for( i=pirng ) 0 ]
                       // : _a pi3= pi3s[_i]
           //, count_is( pi3s
                     //, [ for(i= pirng) 
    
    );
    echo( "pi frequency= ", count_pis(pi3s) );
    
    //================= check obj center
    cPt = sum(pts)/len(pts);
    MarkPt( cPt);
    
    // We put a vertical plane on each pt :
    
    for( pi= pis_sortx )
    {
       frame_pts= [for(i=[0:3]) concat( [pts[pi].x], frame_yzs[i])];
       //echo(pi=pi, frame_pts=frame_pts);
       Plane( frame_pts, mode=4, color=["green",0.1] 
            );
    }
    */
}
//randConvObj_try2();
