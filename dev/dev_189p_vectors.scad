include <../scadx.scad>

ISLOG=1;

//. pt2

module demo_3variables_eq()
{ 
  echom("demo_3variables_eq()");
  
  P=[4,-3,1]; Q=[2,1,3]; R=[-1,2,-5];
  pqr = [P,Q,R];
  pqr2 = transpose(pqr);
  abc= pqr;
  ds= [-10,0,17];
  xyz = solve3( abc, ds );
  ijk = [1,2,-1];
  S= ijk * pqr;
   
  //xyz= pqr*xyz;
  
  //_abc = _matrix([abc],"d=1");
  _abc = _matrix(abc, "d=0;w=4");
  _pqr2 = _matrix(pqr2, "d=0;w=4");
  _xyz = _matrix([xyz], "d=0;w=8");
  _ds= _matrix([ ds ], cellfmt="d=0");
  _ijk = _matrix([ijk], "d=0;w=4");
  _S = _matrix([S], "d=0;w=4");
  
  
  echo(join(
  [ 
    _h2("Considering a 3-variable equation solution:")
  , _formula(
    [ _table( [["a0x+b0y+c0z = d0"],["a1x+b1y+c1z = d1"],["a2x+b2y+c2z = d2"] ]
              , cellfmt="w=40;al=l", rowwraps="" )
    ])
  , _h3("where:")
  , _formula( ["abc=", _abc] )
  , _formula( ["ds= ", _ds ] )
  , _formula( [ _matrix( [ ["a0", "b0", "c0"]
                        , ["a1", "b1", "c1"]
                        , ["a2", "b2", "c2"]
                        ] )
             , "  x  "
             , _matrix( [ ["x", "y", "z"] ])
             , " ="
             , _matrix( [ ["  a0<u>x</u> + b0<u>y</u> + c0<u>z</u> "]
                        , ["a1x + b1y + c1z<u></u><u></u><u></u>"]
                        , ["a2x + b2y + c2z<u></u><u></u><u></u>"]
                        ], "w=40" )
             , " ="
             , _matrix([ abc*xyz ], cellfmt="d=0")
             , "......", "(1)"
             ]
           )      , _h3("xyz can be solved using solve3():")
             
  , _formula( ["xyz= solve3( abc,ds )= ", _xyz ] )
  , _formula( [ _abc, "  x  ", _xyz, " ="
             , _matrix([ abc*xyz ], cellfmt="d=0"), " = xyz"
             ]
           ) 
           
  , "<hr/>"
  
  , _h2("Now, apply to coordinate transition case:")
  , _h3(  "<br/> iP + jQ + kR = S" )
  , _formula( ["PQR (=abc)=", _red(" = unit axes in a new pqr-coordinate")] )
  , _formula( ["Let ijk = ", _red(" = the scalar of point S in the new pqr-coord = the xyz of S IN the pqr-coord")] )
  , _formula( [ "S = "
              , _red("the xyz of the new point S in the Cartesian coord")
              ]
            )         
  
  
  , _h3(  "i [Px, Py, Pz] + j [Qx, Qy, Qz] + k [Rx, Ry, Rz] = [Sx, Sy, Sz]"  )
  , _formula( [ _matrix( [ ["i", "j", "k"] ])
             , "  x  "
             , _matrix([ ["Px", "Py", "Pz"]
                        , ["Qx", "Qy", "Qz"]
                        , ["Rx", "Ry", "Rz"]
                        ] )
             , " ="
             , _matrix( [ [" iP<u>x</u> + jQx + kRx "]
                        , [" iP<u>y</u> + jQy + kRy "]
                        , [" iP<u>z</u> + jQz + kRz "]
                        ], "w=25" )
             , " ="
             , _matrix([ ["Sx, Sy, Sz"] ], cellfmt="w=20")
             , " ...... (2)"
             //, _red("[a,b,c] can be solved with solve3()")
             ]
           )
  , _h3("Let: ")
  , _formula( ["ijk = ", _ijk] )
  , _formula( ["PQR (=abc)=", _abc] )
  
  , _h3("Calc S: ")
  , _formula( [ _ijk
             , "x"
             , _abc
             , "="
             , _matrix( [ [_s(" {_}P<u>x</u> + {_}Qx + {_}Rx ", ijk)]
                        , [_s(" {_}P<u>y</u> + {_}Qy + {_}Ry ", ijk)]
                        , [_s(" {_}P<u>z</u> + {_}Qz + {_}Rz ", ijk)]
                        ], "w=25" )                       
             , "="
             , _matrix( [ [_s(" {_}*{_} + {_}*{_} + {_}*{_} ", zip(ijk,P,isflat=1))]
                        , [_s(" {_}*{_} + {_}*{_} + {_}*{_} ", zip(ijk,Q,isflat=1))]
                        , [_s(" {_}*{_} + {_}*{_} + {_}*{_} ", zip(ijk,R,isflat=1))]
                        ], "w=25" )                       
             , "="
             , _matrix( [ ["4 + 4 + 1"]
                        , ["-3 + 2 - 2"]
                        , ["1 + 6 + 5"]
                        ], "w=12" )             
             , "="
             , _S
             , "= S"
             //, _red("the xyz of the new point S in the Cartesian coord")
             ]
           )   
  , _h3(_s("So, known scalars, ijk= {_}, in the PQR-coord leads us to the xyz of S, {_}",[ijk, S]))               
  
  , "<hr/>"
  
  , _h2("If we know S in advance, how do we get to ijk?")               
  , _h3("We can use <u>solve3()</u>")
  , _h3("NOTE the difference between eq. (1) and (2) --- direction of x,y,z")
  
  , _formula( [ _matrix( [ ["  a0<u>x</u> + b0<u>y</u> + c0<u>z</u> "]
                         , ["a1x + b1y + c1z<u></u><u></u><u></u>"]
                         , ["a2x + b2y + c2z<u></u><u></u><u></u>"]
                         ], "w=40" 
                        )
              , "...."
              , _matrix( [ ["x y z"] ])  
              , _red("xyz x first row [a0,b0,c0] => what solve3() needs.")        
              ]          
            )            
  , _formula( [ _matrix( [ [_s(" iP<u>x</u> + jQx + kRx ", ijk)]
                         , [_s(" iP<u>y</u> + jQy + kRy ", ijk)]
                         , [_s(" iP<u>z</u> + jQz + kRz ", ijk)]
                         ], "w=25" 
                       )
              , "...."
              , _matrix( [ ["Px"], ["Py"], ["Pz"] ], "w=3" )          
              , _red("ijk x first column [Px,Qx,Rx]")        
              ]
             )
  , _h3("==> We need to transpose PQR before applying the solve3()")
  , _formula( ["pqr2= transpose(pqr) ", " = ",  _pqr2] )
         
  , _formula( [ _h3("Calculated ijk"), " = solve3( pqr2, S) = "
             , _matrix( [ solve3(pqr2, S) ], "d=0" )
             , " = ijk"]
           )   
  
  , "<hr/>"
  , _h2("Transfer a pt [i,j,k] to a PQR-coord system:")
  , _h2(_red("iP + jQ + kR = [i,j,k] * [P,Q,R] = S"))
  , _h2("Transfer a pt S from a PQR-coord system to Cartesien:")
  , _h2(_red("ijk = [i,j,k] = solve3( transpose(pqr),S)" ))

  , "<hr/>"
  , _h2( "N M= M<sup>T</sup>N")
  , _h2( "[i,j,k] * [P,Q,R] = [P,Q,R]<sup>T</sup> *[i,j,k]" )
  //, ijk*pqr
  //, transpose(pqr)*ijk    
  ]));
    
}

//demo_3variables_eq();

module dev_onlinePt_summary()
{
   echom("dev_onlinePt_summary");
   _h1("M=aP+bQ");
   
   P= 4*X+3*Y; Q= X+4*Y;
   R= P/2; S= Q/2;
   T= 2*P; U=2*Q;
    
   Line([T,O,U]);
   
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
   Red()
   {
   Dim( [Q,P,R], Label="a+b=1, a,b>=0", Link=true);
   
   Line([1.5*P-0.5*Q, -0.5*P+1.5*Q]);
   Dim( [P, 1.5*P-0.5*Q,R], Label=["text","a+b=1, b<0 (1.5P-0.5Q) ","halign","left"]
       , spacer=0.5,Link=["steps",["x",0.5]]);
   Dim( [-0.5*P+1.5*Q,Q, R], Label=["text","a+b=1, a<0 (-0.5P+1.5Q)","halign","left"]
       , spacer=0.5,Link=["steps",["x",0.5]]);
   
   //Line([R,S]);
   //Line([0.9*R,0.9*S]);
   Line([1.5*R-0.5*S,-0.5*R+1.5*S]);
   
   Dim( [S,R,O], Label="a+b=k<1, a,b>=0 (0.25P+0.25Q)", Link=true);
   //R1=  
   //Dim( [R, 1.5*R-0.5*S,P]
   Dim( [R, .75*P-0.25*Q,P]
       , Label=["text","a+b=k, b<0 (0.75P-0.25Q) ","halign","left"
               ,"actions",["rotx",180]
               ]
       , Link=true); //["steps",["t",[0.25,-0.125,0]]]);
   Dim( [-0.25*P+0.75*Q,S, Q]
        , Label=["text","a+b=k, a<0 (-0.25P+0.75Q)","halign","right"
                ,"actions",["rotx",180]
                ]
       , Link=true);
   
   
   }
   
   V= 0.25*Q;
   W= 2*P+ 0.25*Q;
   
   Green()
   {
     Line([V,W]);
     MarkPt( 1.5*P+.25*Q, Link=["steps",["t",[0.3,-0.3,0],"x",1]], Label="?P+0.25Q", Ball=false);
   }
   
   D=(P+Q)/2;
   E=.75*P+.75*Q;
   F= P+Q;
   
   G= 0.25*P+0.5*Q;
   H= 0.5*P + Q;
   I= 0.75*P + 1.5*Q;
   
   Blue()
   {
     MarkPts([O,D,E,F], Label=["text",["","","0.75*P + 0.75Q"," P + Q"]]);
     MarkPt( (E+F)/2, Link=["steps",["t",[0.3,-0.3,0],"x",.2]], Label="aP+aQ", Ball=false);
   
     MarkPts([O,G,H,I], Label=["text",["","","0.5*P + Q"," 0.75P + 1.5Q"]]);
     MarkPt( .2*H+.8*I, Link=["steps",["t",[0.3,-0.3,0],"x",1]], Label="2aP+aQ", Ball=false);
   }

   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
   Purple() MarkPts( [R,S], Label=["text","RS","scale",1, "indent",0.2] );

   }
dev_onlinePt_summary();



module dev_onlinePt_summary2()
{
   echom("dev_onlinePt_summary2");
   _h1("M=aP+bQ");
   
   P= 4*X+3*Y; Q= X+4*Y;
   R= P/2; S= Q/2;
   T= 2*P; U=2*Q;
    
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
   pl0= [T,O,U];
   MarkCenter(pl0, Label=["text","Quad 0, a>0, b>0","scale",3]);
   Gold(0.1) Plane( pl0 );
   
   
   pl1= [ U,O, -1.3*P, -1*P+2.5*Q] ;
   MarkCenter(pl1, Label=["text"," Quad 1, a<0, b>0","scale",3]);
   //MarkPts(pl1);
   Red(0.1) Plane( pl1 );
   
   pl2= [ -1.5*P, O, -1.5*Q] ;
   MarkCenter(pl2, Label=["text","Quad 2, a<0, b<0","scale",3]);
   //MarkPts(pl2);
   Green(0.1) Plane( pl2 );

   pl3= [ 1.5*P, 3.25*P-1.75*Q, -2*Q, O] ;
   MarkCenter(pl3, Label=["text","Quad 3, a>0, b<0","scale",3]);
   //MarkPts(pl3);
   Blue(0.1) Plane( pl3 );
   
   Line([T,O,U],r=0.1);   
   Black() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
}
//dev_onlinePt_summary2();


module dev_onlinePt_summary3()
{
   echom("dev_onlinePt_summary3");
   _h1("M=aP+bQ");
   
   P= [4,3]*[X,Y]; //4*X+3*Y; 
   Q= [1,4]*[X,Y]; //X+4*Y;
   R= P/2; 
   S= Q/2;
   T= 2*P; U=2*Q;
    
   pq=[P,Q]; 
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
   n=50;
   
   pl0= [T,O,U];
   MarkCenter(pl0, Label=["text","Quad 0, a>0, b>0","scale",3]);
   Gold(0.1) Plane( pl0 );
   Black()
     for(i=range(n))
     {
        r2= rands(0,1.2,2);
        MarkPt( r2*[P,Q], Label=false);
     }
   
   pl1= [ U,O, -1.3*P, -1*P+2.5*Q] ;
   MarkCenter(pl1, Label=["text"," Quad 1, a<0, b>0","scale",3]);
   //MarkPts(pl1);
   Red(0.1) Plane( pl1 );
   Red()
     for(i=range(n))
     {
        r2= rands(0,1.5,2);
        MarkPt( [-r2[0], r2[1]]*[P,Q], Label=false);
     }
     
     
   pl2= [ -1.5*P, O, -1.5*Q] ;
   MarkCenter(pl2, Label=["text","Quad 2, a<0, b<0","scale",3]);
   //MarkPts(pl2);
   Green(0.1) Plane( pl2 );
   Green()
     for(i=range(n))
     {
        r2= rands(0,1.2,2);
        MarkPt( -1*r2*[P,Q], Label=false);
     }
     
     

   pl3= [ 1.5*P, 3.25*P-1.75*Q, -2*Q, O] ;
   MarkCenter(pl3, Label=["text","Quad 3, a>0, b<0","scale",3]);
   //MarkPts(pl3);
   Blue(0.1) Plane( pl3 );
   Blue()
     for(i=range(n))
     {
        r2= rands(0,2,2);
        MarkPt( [r2[0], -r2[1]]*[P,Q], Label=false);
     }
     
   
   Line([T,O,U],r=0.1);   
   Black() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
}
//dev_onlinePt_summary3();


module dev_onlinePt()
{
   echom("onlinePt");
   _h2("aP+bQ=M, a&lt;1, b&lt;1, a+b=1");
   
   P= 4*X+4*Y;
   Q= X+4*Y;
   pq = [P,Q];
   
   Arrow( [O,P] );
   Arrow( [O,Q] );

   R= (P+O)/2;
   S= (Q+O)/2;
   MarkPts( [R,S], "RS", Line=false );
   Red(.2) Arrow( [O,R], r=0.05);
   Green(.3) Arrow( [O,S], r=0.05);

   M = (P+Q)/2;
   MarkPt(M, "M");
   Green(.4) Arrow( [R,M], r=0.05);
   
   dim = ["spacer",0.3, "Link",["stem",0.1], "Label", ["chainRatio",true]];            
   
   Gray(.5)
   {
   Dim( [Q,M,P,S], opt=dim); // spacer=0.3, Link=true, Label=["chainRatio",true] );
   Dim( [O,S,Q,M], opt=dim); // spacer=0.3, Link=true, Label=["chainRatio",true] );
   Dim( [O,R,P,M], spacer=0.3, Link=true
        , Label=["chainRatio",true, "actions",["rot",[0,180,180]]] );
   }
   
   echo(_span( "mid pt = M = 0.5*(P-O) + 0.5*(Q-O) = (P+Q)/2", s="font-size:20px;color:red" ));
   _blue(_span("1) (P+Q)/2 is the midpoint between P and Q", s="font-size:20px;" ));
   //------------------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
}
//dev_onlinePt();

module dev_onlinePt2()
{
   echom("dev_onlinePt2");
   _h2("aP+bQ, a+b=1, a&lt;1, b&lt;1");
   
   P= 4*X+4*Y; Q= X+4*Y;
   Line( [P,O,Q] );

   module get_onlinePt( a_ratio=.75, Label="A", color="red")
   {
     A1= P*a_ratio;
     A2= Q*(1-a_ratio);
     MarkPts( [A1,A2], [str(Label,"1"), str(Label,"2")],, Line=false );
     Red(.2) Arrow( [O,A1], r=0.05);
     Green(.3) Arrow( [O,A2], r=0.05);
     
     M = A1+A2;
     Blue(.3) Arrow( [A1,M], r=0.05 );
     Purple(.3) Arrow( [A2,M], r=0.05 );

     MarkPt(M, Label);
     echo(_span(_s("{_} = {_}*P + {_}*Q", [Label, a_ratio, 1-a_ratio])
               , s=str("font-size:20px;color:",color) )); 
   }
   
   Red(.3) get_onlinePt(.75, "A", "red");
   Green(.4) get_onlinePt(.25, "B", "green");

   dim = ["spacer",0.2, "Link",["stem",0], "Label", ["chainRatio",true]];            
   Red(.2)
   {
     Dim( [Q,(.75*P+.25*Q),P,O], opt=dim); 
     Dim( [O, .75*P,P,Q], Label=["chainRatio",true, "actions",["rot",[0,180,180]]]);
   }                  
   Green(.3)
   {
     Dim( [Q,(.25*P+.75*Q),P,O], Link=0, spacer=0.8, Label=["chainRatio",true]); 
     Dim( [O,0.75*Q,Q,P], opt=dim );
   }
   
   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
      
   echo("-----------------------");
   _blue(_span("1) (P+Q)/2 = midpoint between P and Q", s="font-size:20px;color:blue" ));
   echo("-----------------------");
   _blue(_span("2) a,b in 'aP+bQ' are 'Pulling Force'"
               , s="font-size:20px;"));        
   _blue(_span("  ==> 0.75P + 0.25Q' means P has 3 times of Pulling Force than Q has"
               , s="font-size:20px;"));  
   _blue(_span("'3) When a+b=1 and both a,b&lt;1, aP+bQ bounces between P and Q."
               , s="font-size:20px;"));  
                           
}
//dev_onlinePt2();



module dev_onlinePt3()
{
   echom("dev_onlinePt3");
   _h2("aP+bQ, a+b=1, a&lt;1, b&lt;1");
   
   P= 4*X+4*Y; Q= X+4*Y;
   Line( [P,O,Q] );

   module get_onlinePt( a_ratio=.75, Label="A", color="red")
   {
     A1= P*a_ratio;
     A2= Q*(1-a_ratio);
     //MarkPts( [A1,A2], [str(Label,"1"), str(Label,"2")],, Line=false );
     Red(.2) Arrow( [O,A1], r=0.05);
     //Green(.3) Arrow( [O,A2], r=0.05);
     
     M = A1+A2;
     Blue(.3) Arrow( [A1,M], r=0.05 );
     //Purple(.3) Arrow( [A2,M], r=0.05 );

     MarkPt(M, Label);
     echo(_span(_s("{_} = {_}*P + {_}*Q", [Label, a_ratio, 1-a_ratio])
               , s=str("font-size:20px;color:",color) )); 
   }
   
   Red(.3) get_onlinePt(-.25, "M", "red");
   //Green(.4) get_onlinePt(1.25, "B", "green");

   dim = ["spacer",0.2, "Link",["stem",0], "Label", ["chainRatio",true]];            
//   Red(.2)
//   {
//     Dim( [Q,(.75*P+.25*Q),P,O], opt=dim); 
//     Dim( [O, .75*P,P,Q], Label=["chainRatio",true, "actions",["rot",[0,180,180]]]);
//   }                  
   
   M= (-.25*P+1.25*Q);
   Green(.6)
   {
     Dim( [M,Q,O], Link=0, spacer=0.3); 
     Dim( [Q, P,O], Link=0, spacer=0.3); 
   }
   Blue() 
   MarkPt(M
         , Link=["steps", ["y",1.5,"x",1]]
         , Label="-0.25P + 1.25Q"); 
         
   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
      
   echo("-----------------------");
   _blue(_span("1) (P+Q)/2 = midpoint between P and Q", s="font-size:20px;color:blue" ));
   echo("-----------------------");
   _blue(_span("2) a,b in 'aP+bQ' are 'Pulling Force'"
               , s="font-size:20px;"));        
   _blue(_span("  ==> 0.75P + 0.25Q' means P has 3 times of Pulling Force than Q has."
               , s="font-size:20px;"));  
   _blue(_span("3) When a+b=1 and both a,b&lt;1, aP+bQ bounces between P and Q."
               , s="font-size:20px;"));  
   echo("-----------------------");
   _blue(_span("4) When a+b=1 and a&lt;0, a is a 'Pushing force', push M beyond Q."
               , s="font-size:20px;"));  
                           
}
//dev_onlinePt3();



module dev_onlinePt4()
{
   echom("dev_onlinePt4");
   _h2("aP+bQ, a+b=1, a&lt;1, b&lt;1");
   
   P= 4*X+4*Y; Q= X+4*Y;
   
   
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
   Red(){
   MarkPt(A,r=0.1, Label=["text","-0.5*P + 1.5*Q","scale",2],Link=["steps",["y",-2, "x",.2]] );
   MarkPt(B,r=0.1, Label=["text","0.25*P + 0.75*Q","scale",2],Link=["steps",["y",-1, "x",.2]] );
   MarkPt(C,r=0.1, Label=["text","1.25*P -0.25*Q","scale",2],Link=["steps",["y",1, "x",.2]] );
   }
   
   
   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
      
   echo("-----------------------");
   _blue(_span("1) (P+Q)/2 = midpoint between P and Q", s="font-size:20px;color:blue" ));
   echo("-----------------------");
   _blue(_span("2) a,b in 'aP+bQ' are 'Pulling Force'"
               , s="font-size:20px;"));        
   _blue(_span("  ==> 0.75P + 0.25Q' means P has 3 times of Pulling Force than Q has."
               , s="font-size:20px;"));  
   _blue(_span("3) When a+b=1 and both a,b&lt;1, aP+bQ bounces between P and Q."
               , s="font-size:20px;"));  
   echo("-----------------------");
   _blue(_span("4) When a+b=1 and a&lt;0, a is a 'Pushing force', push M beyond Q."
               , s="font-size:20px;")); 
   echo("-----------------------");
   _blue(_span("5) When a+b=1, aP+bQ is a pt on the PQ line."
               , s="font-size:20px;"));   
}
//dev_onlinePt4();



module dev_onlinePt5()
{
   echom("dev_onlinePt5");
   _h2("aP+bQ, a+b=1, a&lt;1, b&lt;1");
   
   P= 4*X+3*Y; Q= X+4*Y;
   Line([P,O,Q]);
   
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
   Red(){
   MarkPt(A,r=0.1, Label=["text","-0.5*P + 1.5*Q","scale",1],Link=["steps",["y",.5, "x",.2]] );
   MarkPt(B,r=0.1, Label=["text","0.25*P + 0.75*Q","scale",1],Link=["steps",["y",.5, "x",.2]] );
   MarkPt(C,r=0.1, Label=["text","1.25*P -0.25*Q","scale",1],Link=["steps",["y",.5, "x",.2]] );
   }
   
   //--------------------------------
   R= P/2;
   S= Q/2;
   
   D= -0.2*P + 0.7*Q;
   E= .25*P + 0.25*Q;
   F= .6*P - 0.1*Q;
   
   Green(){
   MarkPt(D,r=0.1, Label=["text","-0.2*P + 0.7*Q","scale",1],Link=["steps",["y",-1.8, "x",1.5]] );
   MarkPt(E,r=0.1, Label=["text","0.25*P + 0.25*Q","scale",1],Link=["steps",["y",-.8, "x",.5]] );
   MarkPt(F,r=0.1, Label=["text","0.6*P -0.1*Q","scale",1],Link=["steps",["y",.5, "x",1]] );
   }
   
   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
   Purple() MarkPts( [R,S], Label=["text","RS","scale",2, "indent",0.2],r=0.025 );
      
   echo("-----------------------");
   _blue(_span("1) (P+Q)/2 = midpoint between P and Q", s="font-size:20px;color:blue" ));
   echo("-----------------------");
   _blue(_span("2) a,b in 'aP+bQ' are 'Pulling Force'"
               , s="font-size:20px;"));        
   _blue(_span("  ( 0.75P + 0.25Q' means P has 3 times of Pulling Force than Q has)"
               , s="font-size:20px;"));  
   _blue(_span("  ==> When a+b=1 and both a,b&lt;1, aP+bQ bounces between P and Q."
               , s="font-size:20px;"));  
   echo("-----------------------");
   _blue(_span("3) When a+b=1 and a&lt;0, a is a 'Pushing force' from P, which pushes M beyond Q."
               , s="font-size:20px;")); 
   echo("-----------------------");
   _red(_span("4) When a+b=1, aP+bQ is a pt on the PQ line."
               , s="font-size:20px;"));   
   echo("-----------------------");
   _green(_span("5) For Lines PQ and L=[a1P+b1Q, a2P+b2Q], if a1+b1=a2+b2=k: "
               , s="font-size:20px;"));   
   _green(_span("... 5.1) L is parallel to [P,Q]"
               , s="font-size:20px;"));   
   _green(_span("...... ==> [2P+3Q, 3P+2Q] is a line parallel to PQ, 'cos 2+3=3+2=5 "
               , s="font-size:20px;"));   
   _green(_span("... 5.2) k is proportational to dist between O and PQ"
               , s="font-size:20px;"));   
   _green(_span("...... ==> let a1+b1=a2+b2=0.5, [a1P+b1Q, a2P+b2Q]= a line that is half way from O to PQ"
               , s="font-size:20px;"));   
   _green(_span("... 5.3) a>=0 & b>=0: M bounces between R and S where R,S are on line OP,OQ."
               , s="font-size:20px;")); 

}
//dev_onlinePt5();



module dev_onlinePt6xx()
{
   echom("dev_onlinePt6");
   _h2("aP+bQ, a+b=1, a&lt;1, b&lt;1");
   
   P= 4*X+3*Y; Q= X+4*Y;
   Line([P,O,Q]);
   
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
//   Red(){
//   MarkPt(A,r=0.1, Label=["text","-0.5*P + 1.5*Q","scale",1],Link=["steps",["y",.5, "x",.2]] );
//   MarkPt(B,r=0.1, Label=["text","0.25*P + 0.75*Q","scale",1],Link=["steps",["y",.5, "x",.2]] );
//   MarkPt(C,r=0.1, Label=["text","1.25*P -0.25*Q","scale",1],Link=["steps",["y",.5, "x",.2]] );
//   }
   
   //--------------------------------
   R= P/2;
   S= Q/2;
   
   D= -0.2*P + 0.7*Q;
   E= .25*P + 0.25*Q;
   F= .6*P - 0.1*Q;
   
//   Green(){
//   MarkPt(D,r=0.1, Label=["text","-0.2*P + 0.7*Q","scale",1],Link=["steps",["y",-1.8, "x",1.5]] );
//   MarkPt(E,r=0.1, Label=["text","0.25*P + 0.25*Q","scale",1],Link=["steps",["y",-.8, "x",.5]] );
//   MarkPt(F,r=0.1, Label=["text","0.6*P -0.1*Q","scale",1],Link=["steps",["y",.5, "x",1]] );
//   }
   
   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
   Purple() MarkPts( [R,S], Label=["text","RS","scale",2, "indent",0.2],r=0.025 );
      
   echo("-----------------------");
   _blue(_span("1) (P+Q)/2 = midpoint between P and Q", s="font-size:20px;color:blue" ));
   echo("-----------------------");
   _blue(_span("2) a,b in 'aP+bQ' are 'Pulling Force'"
               , s="font-size:20px;"));        
   _blue(_span("  ( 0.75P + 0.25Q' means P has 3 times of Pulling Force than Q has)"
               , s="font-size:20px;"));  
   _blue(_span("  ==> When a+b=1 and both a,b&lt;1, aP+bQ bounces between P and Q."
               , s="font-size:20px;"));  
   echo("-----------------------");
   _blue(_span("3) When a+b=1 and a&lt;0, a is a 'Pushing force' from P, which pushes M beyond Q."
               , s="font-size:20px;")); 
   echo("-----------------------");
   _red(_span("4) When a+b=1, aP+bQ is a pt on the PQ line."
               , s="font-size:20px;"));   
   echo("-----------------------");
   _green(_span("5) For Lines PQ and L=[a1P+b1Q, a2P+b2Q], if a1+b1=a2+b2=k: "
               , s="font-size:20px;"));   
   _green(_span("... 5.1) L is parallel to [P,Q]"
               , s="font-size:20px;"));   
   _green(_span("...... ==> [2P+3Q, 3P+2Q] is a line parallel to PQ, 'cos 2+3=3+2=5 "
               , s="font-size:20px;"));   
   _green(_span("... 5.2) k is proportational to dist between O and PQ"
               , s="font-size:20px;"));   
   _green(_span("...... ==> let a1+b1=a2+b2=0.5, [a1P+b1Q, a2P+b2Q]= a line that is half way from O to PQ"
               , s="font-size:20px;"));   
   _green(_span("... 5.3) a>=1 & b>=1: M bounces between R and S where R,S are on line OP,OQ."
               , s="font-size:20px;"));   
   
   echo("-----------------------");
   _purple(_span("6) With a1+b1=a2+b2, let d1=abs(a1-b1), d2=abs(a2-b2):"
               , s="font-size:20px;"));   
   _purple(_span("... 6.1) if d1=d2= r, then r= dist(L)/dist(PQ)"
               , s="font-size:20px;"));   
               
   d= dist([P,Q]);            
   module parallel_lines(a1,b1,a2,b2, color)
   {
     R= a1*P+b1*Q;
     S= a2*P+b2*Q;
     
     color(color) MarkPts([R,S], Ball=false, r=0.1); 
    _color( _s( "[{_}P+{_}Q, {_}P+{_}Q]=> a1+b1={_}, a2+b2={_} ==> abs(a1-b1)={_}, abs(a2-b2)={_}, dist(L)/dist(PQ) = {_}" 
            , [ _b(a1), _b(b1), _b(a2), _b(b2)
              , _b(a1+b1), _b(a2+b2)
              , _b(abs(a1-b1)), _b(abs(a2-b2)), _b(dist([R,S])/d)
              ] )
            , color);
   }
   parallel_lines(.1,.3,.3,.1,"red");
   parallel_lines(.8,.4,.4,.8,"green");
   parallel_lines(1.5,-0.7,-0.7,1.5,"blue");

   _purple(_span("... 6.2) else,  abs(d1-d2)/2= dist(L)/dist(PQ)"
               , s="font-size:20px;"));   
   parallel_lines(1.8,-0.3,1,0.5,"purple");
   parallel_lines(1.8,-0.3,1,0.5,"purple");

}
//dev_onlinePt6xx();



module dev_onlinePt_absum_lt_1()
{
   echom("dev_onlinePt_absum_lt_1");
   P= 4*X+4*Y; Q= X+4*Y;
   
   module get_pts_w_sum( sum=1 )
   {
      a_values= [-.25, 0, .25, .5, .75, 1];
      Ms= [ for(a=a_values) sum*a*P+ sum*(1-a)*Q ];
      MarkPts(Ms, false);//, Label=a_values);
      MarkPt( last(Ms), Link=last(Ms)+X, Label=["text",_s("a+b={_}", sum),"scale",2] );
      
      MarkPt(Ms[0],Link=["steps", ["t",[-0.5,.5,0],"x",-0.5]],  Label=["text",_s("{_}P + {_}Q", [ a_values[0]*sum,(1-a_values[0])*sum] ), "halign","right"]);       
      MarkPt(Ms[1],Link=["steps", ["t",[-0.8,.8,0],"x",-0.8]],  Label=["text",_s("{_}P + {_}Q", [ a_values[1]*sum,(1-a_values[1])*sum] ), "halign","right"]);       
      MarkPt(Ms[2],Link=["steps", ["t",[-1.1,1.1,0],"x",-1.1]], Label=["text",_s("{_}P + {_}Q", [ a_values[2]*sum,(1-a_values[2])*sum] ), "halign","right"]);       
      MarkPt(Ms[3],Link=["steps", ["t",[-1.4,1.4,0],"x",-1.4]], Label=["text",_s("{_}P + {_}Q", [ a_values[3]*sum,(1-a_values[3])*sum] ), "halign","right"]);
   }
   _red(_h2("a+b= constant"));
   Red() {get_pts_w_sum(sum=0.5);
          get_pts_w_sum(sum=1);
          get_pts_w_sum(sum=2);
         }
   
   module get_pts_w_fixed_b( b=1 )
   {
      a_values= [-.5, 0, .5, 1, 1.5 ];
      Ms= [ for(a=a_values) a*P+ b*Q ];
      MarkPts(Ms, false);//, Label=a_values);
      MarkPt( last(Ms), Link=["steps",["t",[1,.5,0]] ], Label=["text",_s("b={_}", b),"scale",2] );

      MarkPt(Ms[0],Link=["steps", ["t",[1,-.5,0],"x",1.5]],  Label=_s("{_}P + {_}Q", [ a_values[0],b] ));       
      MarkPt(Ms[1],Link=["steps", ["t",[1,-.5,0],"x",1.5]],  _s("{_}P + {_}Q", [ a_values[1],b] ));       
      MarkPt(Ms[2],Link=["steps", ["t",[1,-.5,0],"x",1.5]], _s("{_}P + {_}Q", [ a_values[2],b] ));       
      MarkPt(Ms[3],Link=["steps", ["t",[1,-.5,0],"x",1.5]],  _s("{_}P + {_}Q", [ a_values[3],b] ));

   }
   _green(_h2("b= constant"));
   Green() {get_pts_w_fixed_b(b=0.25);
          get_pts_w_fixed_b(b=.5);
            }

   
   
   //=======================================================
   Blue()
   {
     Arrow( [P,P+ 1.5*Q], r=0.05 );
     MarkPt( P+1.5*Q, Label=["text","Fixed a=1 (P has 100% pulling force), changing b ( = P + ?Q )","scale",2] );
   }
   Blue(.3)
   {
     Arrow( [P/2+Q/2,P/2+1.8*Q], r=0.05 );
     MarkPt( P/2+1.8*Q, Label=["text","Fixed a=0.5 (P has 50% pulling force), changing b ( = 0.5P + ?Q )","scale",2] );
   }
   Purple(0.3)
   {
     Arrow( [Q,1.7*P+ Q], r=0.05 );
     MarkPt( 1.7*P+Q, Label=["text","Fixed b=1 (Q has 100% pulling force), changing a ( = ?P + Q )","scale",2] );
   }
   
   Black() MarkPts( [P,Q], Label=["text",[" P"," Q"],"scale",2], r=0.05 );
   Line( [2*P,O,2*Q] );
          
}
//dev_onlinePt_absum_lt_1();



module dev_vec_quiz_1_parallel_line()
{
   echom("dev_vec_quiz_1_parallel_line");
   _purple( "Given P,Q,R, draw a line TV that is parallel to PQ and <br/>
          the dist between TV~PQ is 1/4 of R~PQ"  );
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   //echo(pqr=pqr);
   Black() MarkPts( pqr, "PQR", Line=["closed",1]);
          
   T = [0.25,1]* [R-Q,P-Q]+Q;
   U = [0.25,0.75]* [R,P];
   V = [0.25,-.5]* [R-Q,P-Q]+Q;
   MarkPts([T,U,V],"TUV", Line=false);
   DashLine([T,V]);   
   _red("T = [0.25,1]* [R-Q,P-Q]+Q;");
   _green("U = [0.25,0.75]* [R,P];");
   _blue("V = [0.25,-.5]* [R-Q,P-Q]+Q;");
}
//dev_vec_quiz_1_parallel_line();



module dev_vec_quiz_2_inner_triangle()
{
   echom("dev_vec_quiz_2_inner_triangle");
   _red( "Given P,Q,R, draw an internal triangle having all sides parallel to its parent"  );
   _purple("To make sure they are parallel, a+b need to be constant");
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   //echo(pqr=pqr);
   Black() MarkPts( pqr, "PQR", Line=["closed",1]);
        
   Blue() 
   DashLine( [ [.6,.2]*[P-R,Q-R]+R
             , [.6,.2]*[Q-P,R-P]+P
             , [.6,.2]*[R-Q,P-Q]+Q  
             
             , [.6,.2]*[P-R,Q-R]+R
              
             ]);
}
//dev_vec_quiz_2_inner_triangle();



module dev_vec_quiz_3_outer_triangle()
{
   echom("dev_vec_quiz_3_outer_triangle");
   _purple( "Given P,Q,R, draw an outer triangle"  );
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   //echo(pqr=pqr);
   Black() MarkPts( pqr, "PQR", Line=["closed",1]);
          
   Red() 
   DashLine( [ [1.2, -0.1]*[P-R,Q-R]+R
             , [1.2, -0.1]*[Q-P,R-P]+P
             , [1.2, -0.1]*[R-Q,P-Q]+Q  
             
             , [1.2, -0.1]*[P-R,Q-R]+R
              
             ]);
   
   pts2=[ for(i=[0:2]) 
          let( p= next(pqr, P,i)
             , q= next(pqr, P,i+1)
             , r= next(pqr, P,i+2)
             )
          each [[-0.3, 1.3]*[p-r,q-r]+r
               ,[-0.3, 1.3]*[p-q,r-q]+q ]
       ];
   Green() DashLine( range(pts2, cycle=1, returnitems=1), dash=0.005);
   
   Purple(0.1) Line( [[-0.3,1.3]*[Q,P], [-0.3,1.3]*[Q,R] ], r=0.1);
   
}
//dev_vec_quiz_3_outer_triangle();


//. pt3

module dev_inTrianglePt_coPlane_0()
{
   echom("dev_inTrianglePt_coPlane_0");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, b=c, varying a")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [1/2, 1/4, 1/4], "red" );
   Black() MarkPt( C, Label=["text","ijk=[1/3,1/3,1/3]= centroid Pt"]);
   _Draw( [1/10, 9/20, 9/20], "green");
   _Draw( [0, 1/2, 1/2], "blue");
   _Draw( [-2/5, 7/10, 7/10], "purple");
   _Draw( [1.2, -.1, -.1], "teal");
   _Draw( [1.4, -.2, -.2], "olive");
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}
//dev_inTrianglePt_coPlane_0();


module dev_inTrianglePt_coPlane_01()
{
   echom("dev_inTrianglePt_coPlane_01");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, b=4c, Q has stronger pulling")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [1.5, -.4, -.1], "red" );
   _Draw( [0.5, .4, .1], "red" );
   _Draw( [0,   .8, .2], "red" );
   _Draw( [-0.5, 1.2, .3], "red");
   Red() {
     Line( [[1.5,-.4, -.1]*pqr, [-1, 1.6,.4]*pqr] );
     MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}
//dev_inTrianglePt_coPlane_01();

module dev_inTrianglePt_centroid_1()
{
   echom("dev_inTrianglePt_centroid_1");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, b=c, varying a")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
   Arrow( normalLn( pqr ) );  
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   //_Draw( [1/2, 1/4, 1/4], "red", " centroid" );
   Black() MarkPt( C, Label=["text","abc=[1/3,1/3,1/3]= centroid Pt"]);
   
   _Draw( [1, 1/3, 1/3], "red");
   Red()
   { S= [1, 1/3,1/3]*pqr;
     Js = projPt( S, pqr );
     Mark90( [S,Js,C] );
     Line( [Js,C,S]);
   }
   
   _Draw( [2,1/3, 1/3], "green");
   Green(.4)
   { T= [2, 1/3,1/3]*pqr;
     Jt = projPt( T, pqr );
     Mark90( [T,Jt,C] );
     Line( [Jt,C,T]);
   }
   
   _Draw( [2,2/3, 2/3], "blue");
   Blue(.4)
   { U= [2, 2/3,2/3]*pqr;
     Ju = projPt( U, pqr );
     Mark90( [U,Ju,C] );
     Line( [Ju,C,U]);
   }
   
   _Draw( [2,1,1], "purple" );
   Purple(.4)
   { V= [2, 1,1]*pqr;
     Jv = projPt( V, pqr );
     Mark90( [V,Jv,C] );
     Line( [Jv,C,V]);
   }   
   
   _Draw( [2,0,0], "olive" );
   Olive(.4)
   { W= [2, 0,0]*pqr;
     Jw = projPt( W, pqr );
     Mark90( [W,Jw,C] );
     Line( [Jw,C,W]);
   }   
   //_Draw( [0, 1/2, 1/2], "blue");
   //_Draw( [-2/5, 7/10, 7/10], "purple");
   //_Draw( [1.2, -.1, -.1], "teal");
   //_Draw( [1.4, -.2, -.2], "olive");
     //
   MarkPts( [[2, -.3, -.3]*pqr,[2, 1.5, 1.5]*pqr]
          , Label=["text", [[2, -.3, -.3],[2, 1.5, 1.5]]] );
}
//dev_inTrianglePt_centroid_1();


module dev_inTrianglePt_cone_1()
{
   echom("dev_inTrianglePt_cone_1");
   _h2(str( "Pt= aP + bQ + cR, ", _red(" If all a,b,c >0, Pt will be inside the cone")));
   _h2(_purple( "===> The cone is the new coordinate plane"));
   _h2(_purple( "===> Every new PQR is a new coordinate system. <br/>
                 ===> A pt [a,b,c] in that system can be converted <br/>
                      back to Cartesien system by: aP+bQ+cR
                      "));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
   Arrow( normalLn( pqr ) );  
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   Olive(.5){
     Plane( [3*P,O,3*Q ]);
     Plane( [3*P,O,3*R ]);
     Plane( [3*R,O,3*Q ]);
   }
   
   pts = randPts(400,d=2);
   
   for(p=pts)
   {
      color= (p[0]>0 && p[1]>0 && p[2]>0)?"green":"red";
      MarkPt( p*pqr, color=color);
   }  
   
}
//dev_inTrianglePt_cone_1();



module dev_inTrianglePt_cone_2()
{
   echom("dev_inTrianglePt_cone_2");
   _h2(str( "Pt= aP + bQ + cR, ", _red(" If all a,b,c >0, Pt will be inside the cone")));
   _h2(_red("which is equivilent to: if all x,y,z>0, [x,y,z] will be in the 1st octant"));
   _h2(_purple( "==> Every new PQR is a new coordinate system. <br/>
                 ==> [a,b,c] is a coordinate for a pt in that system <br/>
                 ==> A pt [a,b,c] in that system can be converted <br/>
                      back to Cartesien system by: aP+bQ+cR <br/>
                 ==> A pt S in the Cartesien system can be converted to a pt [a,b,c] in PQR system by:<br/>
                     [a,b,c] = solve3( PQR, S)       
                "));
   _h2(_red("# Convert any pt [x,y,z] (=[a,b,c]) to PQR coordinate system <br/>
                     (but still displayed in Cartesian (as S here) 'cos that's what we have):<br/>
                  Known a,b,c :  aP + bQ + cR = S => [a,b,c]*[P,Q,R] = S<br/>                "));
   _h2(_red("# Convert any pt [a,b,c] in a PQR system back to Cartesien: <br/>
            Known S:  [a,b,c] = solve3( PQR, S) <br/>"));
   
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
   Arrow( normalLn( pqr ) );  
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   Olive(.4){
     Plane( [3*P,3*Q, -3*P,-3*Q ]);
     Plane( [3*P,3*R, -3*P,-3*R ]);
     Plane( [3*R,3*Q, -3*R,-3*Q ]);
   }
   
   MarkCenter( [P,Q,R]*3, Label=["text","+++","scale",3] );
   MarkCenter( [-P,Q,R]*3, Label=["text","-++","scale",3] );
   MarkCenter( [P,-Q,R]*3, Label=["text","+-+","scale",3] );
   MarkCenter( [P,Q,-R]*3, Label=["text","++-","scale",3] );
   MarkCenter( [-P,-Q,R]*3, Label=["text","--+","scale",3] );
   MarkCenter( [-P,Q,-R]*3, Label=["text","-+-","scale",3] );
   MarkCenter( [P,-Q,-R]*3, Label=["text","+--","scale",3] );
   MarkCenter( [-P,-Q,-R]*3, Label=["text","---","scale",3] );
   
   pts = randPts(400,d=2);
   colors=[ "111", "gold"
          , "-111", "red"
          , "1-11", "green"
          , "11-1", "blue"
          , "-1-11", "teal"
          , "-11-1", "darkkhaki"
          , "-1-11", "purple"
          , "-1-1-1", "black"
          ];
   for(p=pts)
   {
      color= (p[0]>0 && p[1]>0 && p[2]>0)?"green":"red";
      colorcode = str( sign(p[0]), sign(p[1]), sign(p[2]) );
      MarkPt( p*pqr, color= h(colors, colorcode));
   }  
   
}
//dev_inTrianglePt_cone_2();



module dev_inTrianglePt_02()
{
   echom("dev_inTrianglePt_02");
   _h2(str( "M = aP + bQ + cR,   ", _red("b=c, change a")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
   
   Arrow( normalLn(pqr) );  
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("b+c= ", abc[1]+abc[2], ", abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [0.5, .5, .5], "red" );
   _Draw( [0,   .5, .5], "red" );
   _Draw( [-0.5, .5, .5], "red");
   Red() {
     Line( [[1.5,.5, .5]*pqr, [-1, .5, .5]*pqr] );
    // MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
   }
     
   _Draw( [0.5, .8, .8], "green" );
   _Draw( [0,   .8, .8], "green" );
   _Draw( [-0.5, .8, .8], "green");
   Green() {
     Line( [[1.5,.8, .8]*pqr, [-1, .8, .8]*pqr] );
    // MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
   }
     
   _Draw( [0.5, -.8, -.8], "blue" );
   _Draw( [0,   -.8, -.8], "blue" );
   _Draw( [-0.5, -.8, -.8], "blue");
   Blue() {
     Line( [[1.5,-.8, -.8]*pqr, [-1, -.8, -.8]*pqr] );
    // MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
   }
          
//   _Draw( [0.5, 0, 0], "purple" );
//   _Draw( [0,   0, 0], "purple" );
//   _Draw( [-0.5, 0, 0], "purple");
//   Purple() {
//     Line( [[1.5,0, 0]*pqr, [-1, 0, 0]*pqr] );
//    // MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
//   }
//     

   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}
//dev_inTrianglePt_02();


module dev_inTrianglePt_parallel_1()
{
   echom("dev_inTrianglePt_parallel_1");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, a=0.25")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [0.25, -.25, 1], "red" );
   _Draw( [0.25, 0, .75], "red" );
   _Draw( [0.25, .25, .5], "red" );
   _Draw( [.25, .75, 0], "red");
   _Draw( [.25, 1.25, -.5], "red");
   Red() {
     Line( [[.25, -.5, 1.25]*pqr, [.25,1.5,-.75]*pqr] );
     MarkPt( [.25,1.5,-.75]*pqr, " a+b+c=1, a=0.25 => parallel to RQ");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}
//dev_inTrianglePt_parallel_1();



module dev_inTrianglePt_parallel_11()
{
   echom("dev_inTrianglePt_parallel_11");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1 (on PQR plane), a=0.25 or 0.5")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, label, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str(label, ", abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [0.25, -.05, .8], "S", "red" );
   _Draw( [0.25, 1.25, -0.5], "T", "red" );
   
   _Draw( [0.5, -.25, .75], "U", "green" );
   _Draw( [0.5, .95, -.45], "V", "green" );
//   _Draw( [0.25, 0, .75], "red" );
//   _Draw( [0.5, .25, .5], "red" );
//   _Draw( [.25, .75, 0], "red");
//   _Draw( [.25, 1.25, -.5], "red");
   Red() {
     Line( [[.25, -.5, 1.25]*pqr, [.25,1.5,-.75]*pqr] );
     MarkPt( [.25,1.5,-.75]*pqr, " a=0.25 for S,T");
   }
   Green() {
     Line( [[.5, -.5, 1]*pqr, [.5,1.5,-1]*pqr] );
     MarkPt( [.5,-.5,1]*pqr, " a= 0.5 for both U, V");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}
//dev_inTrianglePt_parallel_11();


module dev_inTrianglePt_parallel_12()
{
   echom("dev_inTrianglePt_parallel_12");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, a=0.25")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   //_Draw( [1, -.25, 1], "red" );
   //_Draw( [1, 0, .75], "red" );
   //_Draw( [1, .25, .5], "red" );
   //_Draw( [1, .75, 0], "red");
   //_Draw( [1, 1.25, -.5], "red");
   Red() {
     Line( [[.25, -.5, 1.25]*pqr, [.25,1.5,-.75]*pqr] );
     MarkPt( [.25,1.5,-.75]*pqr, " a+b+c=1, a=0.25 => on PQR plane");
   }
   Green() {
     Line( [[1.25, -.5, 1.25]*pqr, [1.25,1.5,-.75]*pqr] );
     MarkPt( [1.25,1.5,-.75]*pqr, " a+b+c=2, a=125 => above plane");
   }
   Blue() {
     Line( [[-.5, -.5, 1.25]*pqr, [-.5,1.5,-.75]*pqr] );
     MarkPt( [-.5,1.5,-.75]*pqr, " a+b+c= -.75, a= -0.5 => below plane");
   }
     
   Gold(1)Plane(pqr);  
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
   
   Green(0.3)
   Plane( [ [1.25, -.5, 1.25]*pqr, [1.25,1.5,-.75]*pqr
          , [-.5,1.5,-.75]*pqr, [-.5, -.5, 1.25]*pqr
          ]
        );
}
//dev_inTrianglePt_parallel_12();


module dev_inTrianglePt_perpendicular_1_under_construct()
{
   echom("dev_inTrianglePt_perpendicular_1");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1 (on PQR plane), a=0.25 or 0.5")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [2.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, label, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str(label, ", abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   m= [0,.5,.5];
   s= [1,1.25,-1.25];  
   j= [1,.75,-.75];
   //t= [0.5, .75, -.75];
   M = m*pqr;
   S = s*pqr;
   J = j*pqr;
   //T = t*pqr;
   
   _Draw( m, "M", "red" );
   _Draw( s, "S", "red" );
   _Draw( j, "J", "blue" );
   
   _Draw( [0.5, 0.25,0.25], "T", "purple");
   //_Draw( t, "T", "purple" );
   Line( [M,J] );
   DashLine( [J,R] );
   
   _h2("Want to find a pt, J, on line SP such that aPJM=90 
   <br/> 1. a+b+1 = 1 (so it's on PQR plane)
   <br/> 2. a = 1 (so it's on line PS)
   <br/> 3. b=-c (so it fullfils #1 above) => n,-n or -n,n
   <br/> 4. JR^2-MR^2= PM^2-PJ^2
   ");
   
   echo(aPJM = angle( [P,J,M] ) ); 
   Mark90( [M,J,P]);
    
   
  // _Draw( [0.5, -.25, .75], "U", "green" );
   //_Draw( [0.5, .95, -.45], "V", "green" );
//   _Draw( [0.25, 0, .75], "red" );
//   _Draw( [0.5, .25, .5], "red" );
//   _Draw( [.25, .75, 0], "red");
//   _Draw( [.25, 1.25, -.5], "red");
   Red() {
     Line( [s*pqr, [1, -.5,.5]*pqr] );
     //MarkPt( [.25,1.5,-.75]*pqr, " a=0.25 for S,T");
   }
   Green() {
     //Line( [[.5, -.5, 1]*pqr, [.5,1.5,-1]*pqr] );
     //MarkPt( [.5,-.5,1]*pqr, " a= 0.5 for both U, V");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}
//dev_inTrianglePt_perpendicular_1();


module dev_inTrianglePt_dist_1()
{
   echom("dev_inTrianglePt_dist_1");
   _h2(str( "M = aP + bQ + cR => study the relationship between a,b,c and absolute dist (among P,Q,R) "
   ));
   lv=1;
   
   pqr=pqr();
   pqr= [[.7, 2.91, 1.01], [-0.9, 1.77, 1.8], [-0.08, -0.15, -0.35] ];
   //pqr = [[-0.91, 0.68, -1.66], [2.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, label, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str(label, ", abc= ",abc, cmt=cmt?cmt:"") );
   }     
     
   s= [1,1.25,-1.25];  
   j= [1,.75,-.75];
   //t= [0.5, .75, -.75];
   
   m= [.2,.4,.4];
   M = m*pqr;
   _Draw( m, "M", "red" );
   abc_m = solve3(pqr,M);
   echo(abc_m = abc_m, M=M, abc_m_pqr = abc_m*pqr);
   _Draw( abc_m, "M'", "green");
   //echo( m*pqr, );
   
   //S = s*pqr;
   //J = j*pqr;
   //T = t*pqr;
   
   //_Draw( s, "S", "red" );
   ////_Draw( j, "J", "blue" );
   
   //_Draw( [0.5, 0.25,0.25], "T", "purple");
   //_Draw( t, "T", "purple" );
   //Line( [M,J] );
   //DashLine( [J,R] );
   
   //echo(aPJM = angle( [P,J,M] ) ); 
   //Mark90( [M,J,P]);
    
   
  // _Draw( [0.5, -.25, .75], "U", "green" );
   //_Draw( [0.5, .95, -.45], "V", "green" );
//   _Draw( [0.25, 0, .75], "red" );
//   _Draw( [0.5, .25, .5], "red" );
//   _Draw( [.25, .75, 0], "red");
//   _Draw( [.25, 1.25, -.5], "red");
   Red() {
     //Line( [s*pqr, [1, -.5,.5]*pqr] );
     //MarkPt( [.25,1.5,-.75]*pqr, " a=0.25 for S,T");
   }
   Green() {
     //Line( [[.5, -.5, 1]*pqr, [.5,1.5,-1]*pqr] );
     //MarkPt( [.5,-.5,1]*pqr, " a= 0.5 for both U, V");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}
//dev_inTrianglePt_dist_1();

/*

P=[p0,p1,p3];
Q=[q0,q1,q2];
dPQ = sqrt[ (p0-q0)^2+ (p1-q1)^2 + (p2-q2)^2 ]

M=[m0,m1,m3];
J=[j0,j1,j2];
J= P+bQ-bR = [p0,p1,p3] + b[q0,q1,q2]-b[r0,r1,r2];
  = [p0,p1,p3] + b[q0,q1,q2]-b[r0,r1,r2];
  =   
dJM = sqrt[ (m0-j0)^2+ (m1-j1)^2 + (m2-j2)^2 ]



*/

module dev_inTrianglePt_1x()
{
   echom("dev_inTrianglePt_1");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, none the same, b=a+1")));
   _h2("a+b+c=1 ensures the point is on the pqr plane!!!");
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [.1, .2, .7], "red" );
   _Draw( [.2, .3, .5], "green");
   _Draw( [.3, .4, .3], "blue");
   _Draw( [.4, .5, .1], "purple");
   _Draw( [-0.3, -0.2, 1.5], "teal");
   _Draw( [.6, .7, -.3], "olive");

   Line( [[-0.5,-0.4, 1.9]*pqr,[.7,.8,-.5]*pqr] );  
}
//dev_inTrianglePt_1();



module dev_inTrianglePt01x()
{
   echom("dev_inTrianglePt01");
   _h2(str( "M = iP + jQ + kR,   ", _red("i+j+k=1, j!=k, varying i")));
   lv=1;
   
   module test( Rf=O)
   {   
     log_b("test",lv);
     
     pqr = [ [1.63, -0.93, -0.38], [-0.91, 0.68, -1.66],[1.82, 1.03, 1.83]];       
     P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
     T= 2*P; 
     U=2*Q;
     C = sum(pqr)/3;
     
     log_( ["pqr",pqr] );
     Black() MarkPts(pqr, "PQR", Line=["closed",1] );
     
     Red() Line( [[1.6, -.3, -.3]*pqr,  [-0.6, .8, .8]*pqr] );
     Red() MarkPt( [1.4, -.2, -.2]*pqr, "i+j+k=1, ijk=[?, j, j] (j=k)", Ball=false);
     Red() MarkPt( [-0.6, .8, .8]*pqr, "i+j+k=1, ijk=[?, j, j] (j=k)", Ball=false);
     
     Green() Line( [[1.5, -.1, -.4]*pqr,[-0.5, .3, 1.2]*pqr] );
     Green() MarkPt( [-0.2, .2, 1]*pqr, "i+j+k=1, ijk=[ ?, j, k=4j ] => R has more pulling force", Ball=false);

     Blue() Line( [[1.25, -.2, -.05]*pqr,[-1,1.6,.4]*pqr] );
     Blue() MarkPt( [-1,1.6,.4]*pqr, "i+j+k=1, ijk=[ ?, j=4k, k ] => Q has more pulling force", Ball=false);
     
     Black() MarkPt( C, "ijk=[1/3,1/3,1/3]= centroid Pt");
     
   log_e("",lv);  
   } //test()
   
   test();
   
   
}
//dev_inTrianglePt01x();

module dev_inTrianglePt02x()
{
   echom("dev_inTrianglePt02");
   _h2(str( "M = iP + jQ + kR,   ", _red("j=k, varying i on different sum (=i+j+k) ")));
   lv=1;
   
   module test( Rf=O, sum=1, color="red")
   {   
     log_b("test",lv);
     
     P= [2.5,.5]*[X,Y]+Rf; 
     Q= [0.2,2]*[X,Y]+Rf;
     R= [2.2,2.5]*[X,Y]+Rf; 
     pqr=[P,Q,R];
     T= 2*P; 
     U=2*Q;
     C = sum(pqr)/3;
     
     log_( ["pqr",pqr] );
     Black() MarkPts(pqr, "PQR", Line=["closed",1] );
     
     
     Black() MarkPt( C, "centroid");
     log_( _color( str("sum = ", sum), color ));
     //Red() MarkPt( (sum*[1/2, 1/4, 1/4])*pqr, "ijk=[0.5, 0.25, 0.25]");
     //Green() MarkPt( (sum*[1/10, 9/20, 9/20])*pqr, "ijk=[1/10, 9/20, 9/20]");
     //Blue() MarkPt( (sum*[0, 1/2, 1/2])*pqr, "ijk=[0, 0.5, 0.5]");
     //Purple() MarkPt( (sum*[-2/5, 7/10, 7/10])*pqr, "ijk=[-0.4, 0.7, 0.7]");
     //
     //Teal() MarkPt( (sum*[1.2, -.1, -.1])*pqr, "ijk=[1.2, -0.1, -0.1]");
     //Olive() MarkPt( (sum*[1.4, -.2, -.2])*pqr, "ijk=[1.4, -0.2, -0.2]");
     
     //Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
     
     Line( [(sum*[1.6, -.3, -.3])*pqr,(sum*[-0.6, .8, .8])*pqr] );
     color(color) MarkPt( (sum*[-0.6, .8, .8])*pqr, str( "sum= ", sum), Ball=false );
     
   log_e("",lv);  
   } //test()
   
   Red() test([4.5,0], sum=1, color="red");
   Green() test([4.5,0], sum=0.95, color="green");
   Blue() test([4.5,0], sum=1.05, color="blue");
   Purple() test([4.5,0], sum=1.25, color="purple");
   
}
//dev_inTrianglePt02();

module dev_inTrianglePt1()
{
   echom("dev_inTrianglePt1");
   
   module test( Rf=O, pair="pq")
   {   
     P= [2.5,.5]*[X,Y]+Rf; 
     Q= [0.2,2]*[X,Y]+Rf;
     R= [2.2,1.5]*[X,Y]+Rf; 
     pqr=[P,Q,R];
     T= 2*P; 
     U=2*Q;
     Line(pqr, closed=1);
     /*
        
     
     
     */
     ab = hash( ["pq",[P,Q], "pr",[P,R], "rq",[R,Q] ], pair);
     A= ab[0];
     B= ab[1];
          
     //A= pair=="qr"? Q: P; // || pair=="pr"?P:Q;
     //B= pair=="pq"? Q: R;
     
     //echo(pqr=pqr);
     //echo(ab = [A,B]);
     
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
     
     Black() 
     { 
       MarkPt(P, "P"); 
       MarkPt(Q, Label=["text", "Q","halign","right", "indent",-0.1]);
       MarkPt(R, "R");
       //Line(pair=="pq"?[P,Q]:"pair"=="pr"?[P,R]:[Q,R], r=0.05);
       Line([A,B], r=0.05);
     }
     //pl0= [1.5*(Q-Rf)+Rf,Rf,1.5*(P-Rf)+Rf];
     pl0= [gpt(1.5,B),Rf,gpt(1.5,A)];
     
     Line( pl0 );
   
     n=10;
   
     Gold(0.1) Plane( pl0 );
     ij= hash( ["pq",["i","j"], "pr",["i","k"], "rq",["k","j"] ], pair);
     
     MarkCenter(pl0, Label=["text",_s("{_}>0, {_}>0",ij),"scale",1]);
     Black()
       for(i=range(n))
       {
          r2= rands(0,1,2);
         // MarkPt( r2*[A-Rf,B-Rf]+Rf, Label=false);
       }
   
     //pl1= [ 1.5*(Q-Rf)+Rf,Rf, -1.3*(P-Rf)+Rf, -1*(P-Rf)+Rf+1.5*(Q-Rf)+Rf] ;
     //pl1= [ gpt(1.5,Q), Rf, gpt(-1.3,P), gpt(-1,P)+gpt(1.5,Q)] ;
     pl1= [ gpt(1.5,B), Rf, gpt(-1.2,A), gpt(-1,A)+gpt(1.5,B)-Rf] ;
     MarkCenter(pl1, Label=["text",_s("{_}<0, {_}>0",ij),"scale",1]);
     Red(0.1) Plane( pl1 );
     Red()
       for(i=range(n))
       {
          r2= rands(0,1.5,2);
          //MarkPt( [-r2[0], r2[1]]*[A-Rf,B-Rf]+Rf, Label=false);
       }

   //pl2= [ -1.2*P, Rf, -1.5*Q] ;
   //pl2= [ gpt(-1.2,P), Rf, gpt(-1.5,Q) ];
   pl2= [ gpt(-1.2,A), Rf, gpt(-1.5,B) ];
   MarkCenter(pl2, Label=["text",_s("{_}<0, {_}<0",ij),"scale",1]);
   //MarkPts(pl2);
   Green(0.1) Plane( pl2 );
   Green()
     for(i=range(n))
     {
        r2= rands(0,1.2,2);
       // MarkPt( -1*r2*[A-Rf,B-Rf]+Rf, Label=false);
     }

   //pl3= [ 1.5*P, 2.25*P-1.75*Q, -1.5*Q, Rf] ;
   //pl3= [ 1.5*A, 2.25*A-1.75*B, -1.5*B, Rf] ;
   pl3= [gpt(1.5,A), gpt(2,A)-gpt(0.5,B)+Rf, gpt(-1.5,B), Rf];
   //echo(pl3=pl3);
   MarkCenter(pl3, Label=["text",_s("{_}>0, {_}<0",ij),"scale",1]);
   //MarkPts(pl3);
   Blue(0.1) Plane( pl3 );
   Blue()
     for(i=range(n))
     {
        r2= rands(0,2,2);
       // MarkPt( [r2[0], -r2[1]]*[A-Rf,B-Rf]+Rf, Label=false);
     }   
     
   } //test()
   
   test([0,6,0], pair="pq");
   test([0,0,0], pair="pr");
   test([7,3,0], pair="rq");
   
}
//dev_inTrianglePt1();

module dev_inTrianglePt2(Rf=O, pair=["+-+","-+-"], )
{
   echom("dev_inTrianglePt2");
   _h1("M=aP+bQ");
   lv=1;
   
   module test(Rf=Rf)
   {
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
     
       P= [3.1,.5]*[X,Y]+Rf; 
       Q= [0.5,2.5]*[X,Y]+Rf;
       R= [2.8,2.6]*[X,Y]+Rf; 
       pqr=[P,Q,R];
       stu=[gpt(1.5,P), gpt(1.5,Q), gpt(1.5,R)];
       S=stu[0];
       T=stu[1];
       U=stu[2];
     
       MarkPt(Rf, "Rf", color="black");
       Line(pqr, closed=1);
       Line( [P,Rf,R] );
       Line( [Q,Rf] );
     
       log_(["pqr", pqr],lv);
       log_(["stu",stu],lv );
   
     n=50;
     pqr2=[for(p=pqr)p-Rf];
     
     sign1= [for(c=range(pair[0])) pair[0][c]=="+"?1:-1];
     sign2= [for(c=range(pair[1])) pair[1][c]=="+"?1:-1];
     
     echo(sign1=sign1, sign2=sign2);   
     for(i=range(n))
       {
          r3= rands(0,.5,3);
          Green() MarkPt( zipx(sign1, r3)*pqr2+Rf, Label=false);
          Red() MarkPt( zipx(sign2,r3)*pqr2+Rf, Label=false);
       }
   
     planes= [ "+++", [R, 1.5*P, 1.5*Q,Rf] 
             , "---", [-1.5*Q, Rf, -1.5*P]
             , "-++", [R,Rf,2*Rf-P,Q]
             , "+--", [2*Rf-Q,2*Rf-R,Rf,P]
             , "+-+", [P,R,Rf, 2*Rf-Q]
             , "-+-", [2*Rf-R, 2*Rf-P,Q,Rf]
             , "++-", [P,Q,2*Rf-P,2*Rf-Q]
             , "--+", [2*Rf-Q, P,Q,2*Rf-P]
             ];
     pl_pos = h(planes, pair[0]);
     pl_neg = h(planes, pair[1]);
             
             
     Green() MarkPt(pl_pos[0], Label=["text", pair[0],"scale",3], Ball=false );
     Red()   MarkPt(pl_neg[0], Label=["text", pair[1],"scale",3], Ball=false );

     Green(0.1) Plane( to3d(pl_pos) );
     Red(0.1) Plane( to3d(pl_neg) );
 
     Line([P,2*Rf-P]);   
     Line([R,2*Rf-R]);   
     Line([Q,2*Rf-Q]);   
     Black() MarkPts( pqr, Label=["text","PQR","scale",1, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );

   }// test
   
   test( );
   
}
//dev_inTrianglePt2(Rf=O, pair=["+++","---"]);
//dev_inTrianglePt2(Rf=[0,6,0], pair=["-++","+--"]);
//dev_inTrianglePt2(Rf=[6,6,0], pair=["+-+","-+-"]);
//dev_inTrianglePt2(Rf=[6,0,0], pair=["++-","--+"]);

module dev_inTrianglePt3(Rf=O, pair=["+-+","-+-"], )
{
   echom("dev_inTrianglePt3");
   _h1("M=aP+bQ");
   lv=1;
   
   module test(Rf=Rf)
   {
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
     
       P= [1,-2]*[X,Y]+Rf; 
       Q= [-2,.5]*[X,Y]+Rf;
       R= [2,1.8]*[X,Y]+Rf; 
       pqr=[P,Q,R];
       stu=[gpt(1.5,P), gpt(1.5,Q), gpt(1.5,R)];
       S=stu[0];
       T=stu[1];
       U=stu[2];
     
       MarkPt(Rf, "Rf", color="black");
       Line(pqr, closed=1);
       Line( [P,Rf,R] );
       Line( [Q,Rf] );
     
       log_(["pqr", pqr],lv);
       log_(["stu",stu],lv );
   
     n=50;
     pqr2=[for(p=pqr)p-Rf];
     
     sign1= [for(c=range(pair[0])) pair[0][c]=="+"?1:-1];
     sign2= [for(c=range(pair[1])) pair[1][c]=="+"?1:-1];
     
     echo(sign1=sign1, sign2=sign2);   
     for(i=range(n))
       {
          r3= rands(0,1,3);
          Green() MarkPt( zipx(sign1, r3)*pqr2+Rf, Label=false);
          Red() MarkPt( zipx(sign2,r3)*pqr2+Rf, Label=false);
       }
   
     planes= [ "+++", [R, 2*Rf-P,1.5*Q,2*Rf-R, 1.5*P,2*Rf-Q] 
             , "---", [1.5*P,2*Rf-Q, R, 2*Rf-P,1.5*Q,2*Rf-R]
             , "-++", [R,Rf,2*Rf-P,Q]
             , "+--", [2*Rf-Q,2*Rf-R,Rf,P]
             , "+-+", [P,R,Rf, 2*Rf-Q]
             , "-+-", [2*Rf-R, 2*Rf-P,Q,Rf]
             , "++-", [P,2*Rf-R,Q, Rf]
             , "--+", [2*Rf-Q, Rf,2*Rf-P,R]
             ];
     pl_pos = h(planes, pair[0]);
     pl_neg = h(planes, pair[1]);
             
             
     Green() MarkPt(pl_pos[0], Label=["text", pair[0],"scale",3], Ball=false );
     Red()   MarkPt(pl_neg[0], Label=["text", pair[1],"scale",3], Ball=false );

     Green(0.1) Plane( to3d(pl_pos) );
     Red(0.1) Plane( to3d(pl_neg) );
 
     Line([P,2*Rf-P]);   
     Line([R,2*Rf-R]);   
     Line([Q,2*Rf-Q]);   
     Black() MarkPts( pqr, Label=["text","PQR","scale",1, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );

   }// test
   
   test( );
   
}
//dev_inTrianglePt3(Rf=O, pair=["+++","---"]);
//dev_inTrianglePt3(Rf=[0,6,0], pair=["-++","+--"]);
//dev_inTrianglePt3(Rf=[6,6,0], pair=["+-+","-+-"]);
//dev_inTrianglePt3(Rf=[6,0,0], pair=["++-","--+"]);

module dev_inTrianglePt4(Rf=O, pair=["+-+","-+-"], )
{
   echom("dev_inTrianglePt4");
   _h1("M=aP+bQ");
   lv=1;
   
   module test(Rf=Rf, ijksum=1, color="red" )
   {
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
     
       P= [3.1,.5]*[X,Y]+Rf; 
       Q= [0.5,2.5]*[X,Y]+Rf;
       R= [3,3.5]*[X,Y]+Rf; 
       pqr=[P,Q,R];
       stu=[gpt(1.5,P), gpt(1.5,Q), gpt(1.5,R)];
       S=stu[0];
       T=stu[1];
       U=stu[2];
     
       MarkPt(Rf, "Rf", color="black");
       Line(pqr, closed=1);
       Line( [P,Rf,R] );
       Line( [Q,Rf] );
     
       log_(["pqr", pqr],lv);
       log_(["stu",stu],lv );
   
     n=50;
     pqr2=[for(p=pqr)p-Rf];
     
     //sign1= [for(c=range(pair[0])) pair[0][c]=="+"?1:-1];
     //sign2= [for(c=range(pair[1])) pair[1][c]=="+"?1:-1];
     //
     //echo(sign1=sign1, sign2=sign2);   
     for(i=range(n))
       {
          //r3= [ rands( h(ijk,"i")[0],h(ijk,"i")[1],1)[0]
              //, rands( h(ijk,"j")[0],h(ijk,"k")[1],1)[0]
              //, rands( h(ijk,"k")[0],h(ijk,"k")[1],1)[0]
              //];
          _r3= rands(0,1,3);
          sum = sum(_r3);   
          r3= _r3*(ijksum/sum); 
          //echo(sum=sum(r3));
          color(color) MarkPt( r3*pqr2+Rf, Label=false);
          //Red() MarkPt( zipx(sign2,r3)*pqr2+Rf, Label=false);
       }
     if(ijksum==0.5)
     {
       Blue() MarkPt( .75*Rf+.25*P, "i,j,k>0, i+j+k=0.5", Ball=false); 
     } 
     else if (ijksum==1)
     {
       Red() MarkPt( (0.75*P+0.25*R), "i,j,k>0, i+j+k=1", Ball=false); 
     } 
     else
     {
       Green() MarkPt( 1*P+0.85*Q, "i,j,k>0, i+j+k=2", Ball=false); 
     }
     //planes= [ "+++", [R, 1.5*P, 1.5*Q,Rf] 
             //, "---", [-1.5*Q, Rf, -1.5*P]
             //, "-++", [R,Rf,2*Rf-P,Q]
             //, "+--", [2*Rf-Q,2*Rf-R,Rf,P]
             //, "+-+", [P,R,Rf, 2*Rf-Q]
             //, "-+-", [2*Rf-R, 2*Rf-P,Q,Rf]
             //, "++-", [P,Q,2*Rf-P,2*Rf-Q]
             //, "--+", [2*Rf-Q, P,Q,2*Rf-P]
             //];
     //pl_pos = h(planes, pair[0]);
     //pl_neg = h(planes, pair[1]);
             //
             //
     //Green() MarkPt(pl_pos[0], Label=["text", pair[0],"scale",3], Ball=false );
     //Red()   MarkPt(pl_neg[0], Label=["text", pair[1],"scale",3], Ball=false );
//
     //Green(0.1) Plane( to3d(pl_pos) );
     //Red(0.1) Plane( to3d(pl_neg) );
 
     //Line([P,2*Rf-P]);   
     //Line([R,2*Rf-R]);   
     //Line([Q,2*Rf-Q]);   
     Black() MarkPts( pqr, Label=["text","PQR","scale",1, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );

   }// test
   
   test( Rf=O, ijksum=1, color="red");
   test( Rf=O, ijksum=2, color="green");
   test( Rf=O, ijksum=0.5, color="blue");
   
}
//dev_inTrianglePt4(Rf=O, pair=["+++","---"]);

module dev_inTrianglePt5(Rf=O, pair=["+-+","-+-"], )
{
   echom("dev_inTrianglePt5");
   _h1("M=aP+bQ");
   lv=1;
   
   module test(Rf=Rf, sign=[-1,1,1], ijksum=1, color="red" )
   {
     log_b(_color(_s("test(sign={_}) ",[sign]), color), lv);
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
     
       P= [3.1,.5]*[X,Y]+Rf; 
       Q= [0.5,2.5]*[X,Y]+Rf;
       R= [3,3.5]*[X,Y]+Rf; 
       pqr=[P,Q,R];
       stu=[gpt(1.5,P), gpt(1.5,Q), gpt(1.5,R)];
       S=stu[0];
       T=stu[1];
       U=stu[2];
     
       MarkPt(Rf, "Rf", color="black");
       Line(pqr, closed=1);
       Line( [P,Rf,R] );
       Line( [Q,Rf] );
     
       //log_(["pqr", pqr],lv);
       //log_(["stu",stu],lv );
   
     n=5;
     pqr2=[for(p=pqr)p-Rf];
     
     //sign1= [for(c=range(pair[0])) pair[0][c]=="+"?1:-1];
     //sign2= [for(c=range(pair[1])) pair[1][c]=="+"?1:-1];
     //
     //echo(sign1=sign1, sign2=sign2);   
     for(i=range(n))
       {
          //r3= [ rands( h(ijk,"i")[0],h(ijk,"i")[1],1)[0]
              //, rands( h(ijk,"j")[0],h(ijk,"k")[1],1)[0]
              //, rands( h(ijk,"k")[0],h(ijk,"k")[1],1)[0]
              //];
          _r3= zipx(sign, rands(0,1,3));
          sum = sum(_r3);   
          r3= _r3*(ijksum/sum); 
          echo(r3=r3, sum=sum(r3));
          color(color) MarkPt( r3*pqr2+Rf, Label=false);
          //Red() MarkPt( zipx(sign2,r3)*pqr2+Rf, Label=false);
       }
     if(ijksum==0.5)
     {
       Blue() MarkPt( .75*Rf+.25*P, "i,j,k>0, i+j+k=0.5", Ball=false); 
     } 
     else if (ijksum==1)
     {
       Red() MarkPt( (0.75*P+0.25*R), "i,j,k>0, i+j+k=1", Ball=false); 
     } 
     else
     {
       Green() MarkPt( 1*P+0.85*Q, "i,j,k>0, i+j+k=2", Ball=false); 
     }
     //planes= [ "+++", [R, 1.5*P, 1.5*Q,Rf] 
             //, "---", [-1.5*Q, Rf, -1.5*P]
             //, "-++", [R,Rf,2*Rf-P,Q]
             //, "+--", [2*Rf-Q,2*Rf-R,Rf,P]
             //, "+-+", [P,R,Rf, 2*Rf-Q]
             //, "-+-", [2*Rf-R, 2*Rf-P,Q,Rf]
             //, "++-", [P,Q,2*Rf-P,2*Rf-Q]
             //, "--+", [2*Rf-Q, P,Q,2*Rf-P]
             //];
     //pl_pos = h(planes, pair[0]);
     //pl_neg = h(planes, pair[1]);
             //
             //
     //Green() MarkPt(pl_pos[0], Label=["text", pair[0],"scale",3], Ball=false );
     //Red()   MarkPt(pl_neg[0], Label=["text", pair[1],"scale",3], Ball=false );
//
     //Green(0.1) Plane( to3d(pl_pos) );
     //Red(0.1) Plane( to3d(pl_neg) );
 
     //Line([P,2*Rf-P]);   
     //Line([R,2*Rf-R]);   
     //Line([Q,2*Rf-Q]);   
     Black() MarkPts( pqr, Label=["text","PQR","scale",1, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );

     log_e("",lv);
   }// test
   
   test( Rf=O, ijksum=1, color="red");
   test( Rf=O, ijksum=2, color="green");
   test( Rf=O, ijksum=0.5, color="blue");
   
}
//dev_inTrianglePt5(Rf=O, pair=["+++","---"]);


module dev_offTrianglePts()
{
   function offTrianglePts(pqr,n)=
   (
     [for(i=range(n))
       //let( r3= zipx([1,-1,1], rands(0,.5,3) )) 
       let( r3= rands(0,1,3) ) 
       (r3/sum(r3))*pqr
     ]     
   
   
   );
   
   n = 20;

   //pqr=pqr(); 
   _pqr = [[1.14, 2.55,.5], [0.37, .2, -.75], [-1.55, 1.5, 0]];
   pqr= [ for(p=_pqr) p+ [2,1,0] ];
   echo(pqr=pqr); 
   MarkPts(pqr, "PQR", Line=["closed",1]);
   
   
   pts0 = offTrianglePts(pqr, n);
   Red() MarkPts(pts0, Label=false, Line=false);

   pts= randInPlanePts(pqr, n );
   Green() MarkPts(pts, Label=false, Line=false);

}
//dev_offTrianglePts();
