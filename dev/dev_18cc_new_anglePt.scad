include <../scadx.scad>
/*
-- 2018.12.12
   
   Dev new anglePt using dot/cross products
   
       ;;  a2 not fiven:
    ;; 
    ;;           R     _ S    
    ;;         -'   _-'  |
    ;;       -'  _-' len |
    ;;     -' _-'        |
    ;;   -'_-'  a        | 
    ;;  Q----------------J--P

    (P-Q)*(S-Q) = dPQ*dSQ*cos(a) = dJQ * dPQ
    
    dSQ = L
    
    L*cos(a) = dJQ
    
    r = dJQ / dPQ  
      = L*cos(a)/dQP
*/

new_anglePt();
module new_anglePt()
{

    function anglePt( pqr, a=undef, a2=0, len=undef
                    , ratio=1
                    , Rf=undef  // Rf could be used when pqr is coln
                    )=
    (    
    
    /*  



        ;;  a2 not fiven:
        ;; 
        ;;           R     _ S    
        ;;         -'   _-'  |
        ;;       -'  _-' len |
        ;;     -' _-'        |
        ;;   -'_-'  a        | 
        ;;  Q----------------J--P

        (P-Q)*(S-Q) = dPQ*dSQ*cos(a) = dJQ * dPQ
    
        dSQ = L
    
        L*cos(a) = dJQ
    
        r = dJQ / dPQ  
          = L*cos(a)/dQP
      
        J = r*P + (1-r)*Q
        S = B-Q+J
 
    */

      let( P=pqr[0], Q=pqr[1], R=pqr[2]
         , dPQ = norm(P-Q)
         , L = ratio* (len?len:dPQ )
         , r = L*cos(a)/dPQ
         , J = r*P +(1-r)*Q
         , S = B(pqr)-Q+J
         , rtn= S
         )
      rtn
    );


  pqr=pqr();
  pqr = [[0.41, 1.05, -0.73], [-1.7, -0.6, 0.78], [1.04, 1.41, 1.4]];
  echo(pqr=pqr);
  
  P=pqr[0]; Q=pqr[1]; R=pqr[2];
  dPQ = norm(P-Q);
  L = dPQ; //ratio* (len?len:dPQ );
  r = L*cos(45)/dPQ;
  J = r*P +(1-r)*Q;
  B = B(pqr);      
  A = onlinePt([J, B-Q+J], sqrt(L*L-norm(J-Q)*norm(J-Q)));
  MarkPt(J,"J"); 
  MarkPt(B,"B");
        
  echo(A=A, angle = angle([pqr[0],pqr[1],A]));
  MarkPts(pqr, "PQR");
  MarkPts([pqr[1], A], Label=["text", ["","A"]] );
  Mark90( [A,J,Q] );
  Mark90( [B,Q,P] );

}