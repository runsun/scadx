include <../scadx.scad>


module demo_spherePts()
{
  echom("demo_spherePts()");
  r= 2;
  pf = icosahedronPF(r=r, debug=1);
  pts= pf[0];
  faces= pf[1];
  
  //MarkPts(pf[0], Line=false);
  echo(pts=pf[0]);
  echo(faces=pf[1]);
  //P1j = [0.894427, 0, 0];
  //Mark90( [pf[0][0], P1j, pf[0][2]] );
   
  //MarkFaces( pf[0], pf[1] , color="purple");
   
  //color("gold",0.5)
  //polyhedron( pf[0],pf[1]);

  
  
  for(i=range(pf[0]))
  {
    echo(i=i, dist(ORIGIN, pf[0][i]) );
    //Line( [ORIGIN, pf[0][i]] );
  }  
  
  $f = 4;  // $f = frequency
  edges = get_edges(faces);
  
  aPOQ = angle([ pts[0], ORIGIN, pts[1] ]);
  
  h_egDivPts = [ for(eg=edges) 
                 each[ eg
                     , arcPts( [pts[eg[0]], ORIGIN, pts[eg[1]] ]
                              , a= aPOQ, n= $f+1 
                              )
                 
                     ]       
               ]; 
          
  //echo(h_egDivPts=h_egDivPts);        
  for( v = vals( h_egDivPts ) )
  {
     MarkPts(v);
  }
  
  
  for(fi = faces)
  {
     egs= get2(faces[fi]);
      
  
  }
        
  for(eg=edges) 
  { 
    p2= get(pts,eg);
    //Line( p2, r=0.0075, color="black");
    mp = onlinePt( [ORIGIN, sum(p2)/2], len= r );
    Line( [p2[0],mp,p2[1]], r=0.0075, color="black");
    
  }
  
  
  for( i=[1:len(pts)-1] )
    echo( i=[i-1,i], d = dist( get(pts, [i-1,i])) );
    
 
    
//  CEF0=get_clocked_EFs_at_pi(pf[1],0);
//  echo(CEF0=CEF0);
//  
//  efis = get_edgefis( faces = pf[1] );
//  echo(efis = efis );
  
}
demo_spherePts();
