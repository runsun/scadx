include <../scadx.scad>

/*
 [ "0", ["a", "b"] ]
 
 [ "0", [ ".00","b" ] 
 , "00", ["c","d"]
 ]
 
 [ "0", [ ".00","01"] ] 
 , "00", ["c","d"]
 , "01", ["f","g"]
 ]

 [ "0", [ ".00",".01"] ] 
 , "00", [ ".000", "d"]
 , "01", ["f","g"]
 , "000", ["p","q]
 ]

 [ "0", [ ".00",".01"] ] 
 , "00", [ ".000", ".001"]
 , "01", ["f","g"]
 , "000", ["p","q"]
 , "001", ["a","b"]
 ]
 

*/


function btAdd( tree =[]
              , node  // [<nodename>,<spot>] => ["000",0] or ["000",1]
              , spot = 0  // 0||1
              , data  // [v1,v2]
              )=
(
   node==undef? ["0",data]
   : update( tree, [ node, spot==0? 
                            [ str(node,spot), h(tree, node)[spot==0?1:0] ]
                           :[ h(tree, node)[spot==0?1:0], str(node,spot) ]
                   , str(node,spot), data
                   ] )
);

bt = btAdd(data=["a","b"]);
echo( _fmth(bt) );

bt2= btAdd( bt, node="0",spot=0, data=["c","d"] );
echo( _fmth(bt2) );

bt22= btAdd( bt2, node="0",spot=1, data=["p","q"] );
echo( _fmth(bt22) );

bt3= btAdd( bt22, node="01",spot=1, data=["f","g"] );
echo( _fmth(bt3) );

bt32= btAdd( bt3, node="00",spot=0, data=["x","y"] );
echo( bt32, bt_nLayer(bt32) );
echo( _fmth(bt32) );

bt4= btAdd( bt32, node="000", spot=1, data=["m","n"] );
echo( _fmth(bt4) );
bt42= btAdd( bt4, node="000", spot=0, data=["c","d"] );
echo( _fmth(bt42) );


function bt_nLayer(data)=
(
   max( [ for(k=keys(data)) len(k)] )
);


module bt_prn(data)
{
  /*
     [ "0", ["00", "01"]
     , "00", ["000", "d"], "01", ["010", "q"]
     , "000", ["f", "g"]
     , "010", ["x", "y"]
     ]
     
     0 -+ 00 -+ 000 -+ 0000 -+ C 
        |     |      |       + d 
        |     |      + 0001 -+ m 
        |     |              + n
        |     + d
        |    
        + 01 -+ p
              |
              + 010 -+ f
                     + g
                  
  */   
   nodes= keys(data);
   _len_nodes = [for(node=nodes) 
                    [ len(node)
                    , sum([for(ci=range(node)) num(node[ci])])
                    , node ] 
                ];
   len_nodes = sortArrs(sortArrs( _len_nodes, by=1 ), by=0);
   sorted_keys= [for(x=len_nodes) last(x) ];
   
   echo(nodes = nodes);
   echo(_len_nodes = _len_nodes);
   echo(len_nodes = len_nodes);
   echo(sorted_keys = sorted_keys);

   
}


bt_prn(bt42);


