include <../scadx.scad>


module 1847_convex_pt()
{
  echom("1847_convex_pt()");
  pts = randPts(6);
  T= randPt();  // target pt (aim: check if this is convex)
  C = sum(pts)/len(pts);  // center of pts
  
  xplane = getPlaneByNormalLine([C,T]);
  echo(C=C,T=T);
  echo(xplane=xplane);
  
  MarkPts(pts, Line=["closed",true] );
  MarkPts([C,T], Label="CT"); //["text",["C","T"]]);
  Mark90( [xplane[0],C,T]);
  //MarkPt(cpt, Label="center");
  //MarkPt(targetPt, Label="target");
 
  Plane(xplane, mode=3);

}
1847_convex_pt();
