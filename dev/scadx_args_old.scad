
//. Below are old arg pattern codes waiting to be discarded. 2018.1.25



/*
=================================================================
               Argument design pattern
=================================================================

1. Undef arg with default val

    module obj( ops=[] )
    {
        ops= update( [ "r", 3 ], ops );
    }

2. Two or more related SIMPLE args, set it at once or seperate

    module obj( ops=[] )
    {
        ops = update( ["r", 3], ops );
        function ops(k,df) = hash(ops, k, df);
        r = ops("r");
        rP= ops("rP", r);  // if rP undef, use r
        rQ= ops("rQ", r);  // if rQ undef, use r
        ...
    }
    
3. ONE complex arg

    module obj( ops=[] )
    {
        u_ops  = ops;
        df_ops = [...];
        df_head = ["r",2];
        
        ops = update( df_ops, u_ops );
        function ops(k,df) = hash(ops, k, df);

        ops_head= update( df_head, ops("head",[]) ); 
        ...
    }    
               
4. two or more complex args 
   (example, heads in Arrow. Or Dim2() ):

    module obj( ops=[] )
    {
        u_ops = ops;
        df_ops= [ "r",0.02
                , "heads",true
                , "headP",true
                , "headQ",true
                ];
        df_heads= ["r", 0.5, "len", 1];  
        df_headP= [];
        df_headQ= [];
                
        ops = update( df_ops, u_ops );
        function ops(k,df)= hash(ops,k,df);
        
        com_keys = [ "color","transp","fn"];
        
        ops_headP= getsubops( ops=u_ops, df_ops=df_ops
                    , com_keys= com_keys;
                    , sub_dfs= ["heads", df_heads, "headP", df_headP ]
                    );
        ops_headQ= getsubops( ops=u_ops, df_ops=df_ops
                    , com_keys=com_keys;
                    , sub_dfs= ["heads", df_heads, "headQ", df_headQ ]
                    );
            
        function ops_headP(k) = hash( ops_headP,k );
        function ops_headQ(k) = hash( ops_headQ,k ); 
    }
    
    NOTE:
        
        For multiple layer sub-units, like:
        
        body - arm - finger
        
        An each approach is to allow setting, for example, fingers like:
        
        obj( ["arms", ["finger", ["count",3]]] ) 
        
=================================================================

*/

module ArgumentPatternDemo_simple()
{
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= concat( ops
            , [ "color", "blue" ]  // <== set default here
            , OPS );
        function ops(k,df) = hash(ops,k,df);
        color = ops("color");
        len   = ops("len", 2); // <== or here. Note that len doesn't
                               //     need to be defined in ops
        translate(transl) color( ops("color"), 0.4) 
        cube(x=0.5,y=0.5,z=1 );
    }  
    Obj();    
    Obj( ["color","red"], [0,1,0] );
}    

module ArgumentPatternDemo_multiple_simples()
{
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= concat( ops
            , [ "limlen", 2
              ], OPS );
        function ops(k,df) = hash(ops,k,df);
        lim = ops("limlen");
        arm = ops("armlen",lim);
        leg = ops("leglen",lim); 
        
        //echo("ll, arm,leg", ll, arm, leg);
        translate(transl)
        color( ops("color"), 0.4) { 
            cube( size=[0.5,1,2] );
            translate( [0,1, 1]) cube( size=[0.5,arm,0.5] );
            translate( [0,0.5, -leg]) cube( size=[0.5,0.5,leg] );
        }    
    }  
    Obj();    
    Obj( ["limlen",1, "color","red"], [2,0,0] );
    Obj( ["armlen",0.5, "color","blue"], [4,0,0] );
    LabelPt( [0, 0, 2], "Obj()=> both lims= default 2" );
    LabelPt( [2, 1, 1.5], "Obj([\"limlen\",1])=> both lims=1",["color","red"] );
    LabelPt( [4, 1, 1.5], "Obj([\"armlen\",0.5])=> arm len=0.5", ["color","blue"] ); 
}    

module ArgumentPatternDemo_complex()
{_mdoc("ArgumentPatternDemo_complex","");
  
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= update( ["arm", true]
                    ,ops);
        function ops(k,df)= hash(ops,k,df);

        df_arm=["len",3];
        
        ops_arm= getsubops( ops
                    , com_keys= ["transp"]
                    , sub_dfs= ["arm", df_arm ]
                    );
        function ops_arm(k,df) = hash(ops_arm,k,df);
        
        echo("ops_arm",ops_arm);
        
        translate(transl) color(ops("color"), ops("transp"))
        cube( size=[0.5,1,2] );
        if ( ops_arm )
        {   translate( transl+[0,1, 1]) 
            color(ops_arm("color"), ops_arm("transp"))
            cube( size=[0.5,ops_arm("len"),0.5] );
        }   
    }  
    Obj();    
    Dim( [ [0,4,1.5], [0,1,1.5], [2,1,1.5]] );
    Obj( ["arm",false], [1.5,0,0] );
    Obj( ["arm",["len",2]], [3,0,0] );
    Obj( ["arm",["color","red"]], [4.5,0,0] );
    Obj( ["color", "green"], [6,0,0] );
    Obj( ["transp", 0.5], [7.5,0,0] );
    
    LabelPt( [0.5,4.2,1.5], "1.Obj()");
    LabelPt( [1.8,3,1.2], "2.Obj([\"arm\",false])");
    LabelPt( [3.6,3.5,1.5], "3.Obj([\"arm\",[\"len\",2]])");
    LabelPt( [4.8,4.2,1.5], "4.Obj([\"arm\",[\"color\",\"red\"]])",["color","red"]);
    LabelPt( [6.5,4.2,1.5], "5.Obj([\"color\",\"green\"])",["color","green"]);
    
    LabelPt( [8,4.2,1.5], "6.Obj([\"transp\",0.5])");
    
    LabelPt( [10, 0, 0], "Note #5,#6: #5 color set body color but not"
    ,["color","blue"]);
    LabelPt( [10, 0, -0.6], "arm color, but #6 set transp of both, "
    ,["color","blue"]);
    LabelPt( [10, 0, -1.2], "because transp is in com_keys "
    ,["color","blue"]);
    
}  


module ArgumentPatternDemo_multiple_complex()
{
  _mdoc("ArgumentPatternDemo_complice"
," 0: armL has transp=0.6 'cos  df_armL=[''transp'',0.6]
;; 1,2,4: no armL 'cos armL= false 
;; 2    : no arms 'cos arms= false
;; 3~7  : both len = 2 'cos arms=[''len'',2]
;; 5,6  : armL ops placed inside arms [] have no effect
;; 7    : armL ops should have one [] by its own 
;; 8    : set color on obj-level doesn't apply to arms but transp,
;;        does 'cos transp is in com_keys but color is not.
;; 9    : transp set on obj-level can be overwritten by that 
;;      : set in armL [].
");
    
    module Obj( ops=[], transl=[0,0,0] )
    {
        df_ops= ["arms", false
               , "armL", false
               , "armR", true
               ];
        com_keys = ["transp","color"];
        df_arms=["len",3];
        df_armL=["transp",0.6];
        df_armR=[];
        
        u_ops = ops;
        _ops= update(df_ops, u_ops); // Needed only if func ops() is used 
        function ops(k,df)= hash( _ops,k,df);
        
        function subops(sub_dfs)=
               getsubops( 
                      u_ops= u_ops  
                    , df_ops = df_ops
                    , com_keys= com_keys
                    , sub_dfs= sub_dfs
                    );
        ops_armL = subops(["arms", df_arms, "armL", df_armL ]);
        ops_armR = subops(["arms", df_arms, "armR", df_armR ] );
        
        function ops_armL(k,df) = hash(ops_armL,k,df);
        function ops_armR(k,df) = hash(ops_armR,k,df);

        //echo("");
        echo(args= ops );
        echo("ops", ops);
        echo("df_ops", df_ops);
        echo("ops_armL", ops_armL);
        echo("ops_armR", ops_armR);
        echo( ops_armL= getsubops_debug(ops  
                    , df_ops = df_ops
                    , com_keys= ["transp","color"]
                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]
                    )); 
        
        translate(transl) color(ops("color"), ops("transp"))
        cube( size=[0.5,1,2] );
        if ( ops_armR )
        {   
            translate( transl+[0,1, 1]) 
            color(ops_armR("color"), ops_armR("transp"))
            cube( size=[0.5,ops_armR("len"),0.5] );
        }    
        if ( ops_armL )
        {    translate( transl+[0,-ops_armL("len"), 1]) 
            color(ops_armL("color"), ops_armL("transp"))
            cube( size=[0.5,ops_armL("len"),0.5] );
            
        }   
    }  
    Obj();    
    //Dim( [ [0,4,1.5], [0,1,1.5], [2,1,1.5]] );
    
    all_ops= [
        []  // 1
        ,["arms",false]  // 1
        ,["armL",false]  // 2
        ,["arms",["len",2]] // 3
        ,["arms",["len",2], "armL", false] // 4
        ,["arms",["len",2, "armL", false]] // 5
        ,["arms",["len",2, "armL", ["color","red"]]] // 6
        ,["arms",["len",2], "armL", ["color","red", "len",1]] // 7
        ,["color","red", "transp", 0.3] // 8
        ,["transp", 0.5, "armL", ["transp", 1] ] // 9  
        ,["armR",["color", "blue"]]
        ,["armR",["len",1]]
    ];
    for(i=range(all_ops)){
        echo(i= str("<br/>===================== #",i+1, "========"));
        Obj( all_ops[i], [1.5*(i+1), 0, 0] );
        LabelPt( [(i+1)*1.5+0.3, 4.3, 1.5], str(i+1, " ops=", all_ops[i]) 
               , ["color", "blue"] );
    }
 
}  
//ArgumentPatternDemo_simple();
//ArgumentPatternDemo_multiple_simples();
//ArgumentPatternDemo_complex();
//ArgumentPatternDemo_multiple_complex();

//========================================================
getsubops= ["getsubops","ops, com_keys, sub_dfs","hash or false", "args"
 ," Define the options of multiple sub objs for situations like :
;;
;;   obj.arms.armL
;;   obj.arms.armR
;;
;; in which you want a module Obj that allows for:
;;
;;  Obj([''arms'',false]) ==> disable arms
;;  Obj([''armL'',true) ==> enable both with default arm properties.
;;  Obj([''arms'',[''r'',1]) ==> set both arm properties at once
;;  Obj([''armL'',[''r'',2]) ==> set armL properties
;;  Obj([''arms'',[''r'',1], ''armL'',false) ==> set all arms, but disable armL
;;  Obj([''arms'',[''r'',1], ''armL'',true) ==> set all arms, but armL uses default
;;  Obj([''arms'',[''r'',1], ''armL'',[''r'',2]) ==> set all arms, but override armL
;;
;;    module Obj( ops=[] )
;;    {
;;        //---------------------- default
;;        df_arms=[''len'',3];
;;        df_armL=[''transp'',0.6];
;;        df_armR=[];
;;        df_ops= [''arms'', true
;;               , ''armL'', true
;;               , ''armR'', true
;;               ];
;;        com_keys= [''transp'',''r'']j;     
;;        //----------------------- user
;;        u_ops = ops;   
;;        //_ops= update(df_ops, ops);
;;        //function ops(k,df)= hash(_ops,k,df);
;;        //----------------------- compose func
;;        function subops(sub_dfs)=
;;           getsubops( u_ops  
;;                    , df_ops = df_ops
;;                    , com_keys= com_keys
;;                    , sub_dfs= sub_dfs
;;                    );
;;        ops_armL = subops([''arms'', df_arms, ''armL'', df_armL ]);
;;        ops_armR = subops([''arms'', df_arms, ''armR'', df_armR ] );
;;        function ops_armL(k,df) = hash(ops_armL,k,df);
;;        function ops_armR(k,df) = hash(ops_armR,k,df);
;;        //----------------------- use func
;;        if(ops_armR){ //use ops_armR(''color''), ops_armR(''len'') )...}
;;        if(ops_armL){ //use ops_armL(''color''), ops_armL(''len'') )...}
;;    }  
"
//," Define the options of multiple sub objs. The scenario is that an obj O 
//;; can have n common subobjs, such as left-arm, right-arm. They belong to
//;; ''arms''. We want :
//;;
//;; 1. Both arms can be turned off by Obj( ...["arms",false] )
//;; 2. Common arm props can be set on the obj level by Obj(...["r",1] )
//;;    that applies to entire obj including other parts
//;; 3. Common arm props can also be set on the obj level by
//;;    by Obj(... ["arms", ["r",2]) that will only apply to arms. 
//;; 4. Left arm can be turned off by Obj(...["leftarm",false])
//;; 5. Left arm can be set by Obj(...["leftarm", ["r",1]] )
//;;
//;; ops: obj-level ops 
//;; subs: a hash of 2 k-v pairs: [''arms'', df_arms, ''armL'', df_armL]
//;;       ''arms'': name used in ops as ops=[ ... ''arms'', true ...]
//;;       df_arms : hash for default arms 
//;;       ''armL'': name  used in ops as ops=[ ... ''armL'', true ...]
//;;       df_armL : hash for default left arm
//;; com_keys: like ''len'',''color''. This is the common keys that
//;;           will set on entire obj and also carries over to all arms
//;;
//;; NOTE: com_keys SHOULD shows up in ops but NEVER in df_arms or df_armL
//"
];

function getsubops( 
      u_ops
    , df_ops=[] // default ops given at the design time
    , sub_dfs   // a hash showing layers of sub op defaults
                // ["arms", df_arms, "armL", df_armL]
                // The 1st k-v pair, ("arms",df_arms) is the group [name
                // , default], to which the next ("armL",df_armL), belongs.
    , com_keys  // names of sub ops, which you want them to be also be set 
                // from the obj level. Ex,["r", "color"]
    )=
(  let(subnames = keys( sub_dfs ) // name of subunit   ["arms", "armL"]
      ,df_subs  = vals( sub_dfs ) // df opt of subunit 
                                  // [df_arms, df_armL, df_armR]
      ,df_com    = com_keys?[ for(i=range(df_ops)) // part of user ops that have 
                                           // keys showing up in com_keys
                      let( key= df_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) df_ops[i]
                  ]:[] // ["r",1] 
      ,u_com    = com_keys?[ for(i=range(u_ops)) // part of user ops that have keys 
                                      // showing up in com_keys
                      let( key= u_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) u_ops[i]
                  ] // ["r",1]
                  :[]    
      ,u_subops = [ for(sn= subnames)  // Extract subops settings from 
                    hash(u_ops,sn) ]     // user input ops ops, like 
                                       // ["arms",t
//      ,u_subops = [ for(sn= subnames)  // Extract subops settings from 
//                    hash(ops,sn) ]     // user input ops ops, like 
//                                       // ["arms",true, "armL",["r",1]]
      // order of reversed priority:
      // [  df_ops, df_arms, df_armL, u_com,u_arms, u_armL ] 
      ,subops = updates(
          df_com
        , [ df_subs[0]
          , df_subs[1]
          , u_com
          , u_subops[0]==true? []:ishash(u_subops[0])?u_subops[0]:false
          , u_subops[1]==true? []:ishash(u_subops[1])?u_subops[1]:false
          ])
      
       // re-write: 2015.8.3
       , show= hash(u_ops,subnames[1] // if "armL" defined in args
                   ,hash(u_ops,subnames[0] // if not, if "arms" defined in args
                        , hash( df_ops, subnames[1] ) //if not, check df_ops
                        )
                   ) // 
               
               
//               und( 
//                 hash(u_ops,subnames[1] // if "armL" defined in args
//                     ,hash(u_ops,subnames[0]) ) // if not, if "arms" defined in args
//                  , hash( df_ops, subnames[1] ) // 
//                  )
       //,show= sum( [ for(k=subnames) hash(u_ops,k)==false?1:0])==0
       //,show = [ for(k=subnames) if(hash(u_ops,k)) 1]
       )
       //_//df_ops
       //[ for(s=u_subops) s==false?1:0] //u_subops
    show? subops 
        : false
);
    
       
function getsubops_debug( 
      u_ops
    , df_ops=[] // default ops given at the design time
    , com_keys  // names of sub ops, which you want them to be also be set 
                // from the obj level. Ex,["r", "color"]
    , sub_dfs   // a hash showing layers of sub op defaults
                // ["arms", df_arms, "armL", df_armL]
                // The 1st k-v pair, ("arms",df_arms) is the group [name
                // , default], to which the next ("armL",df_armL), belongs.
    )=
  let(subnames = keys( sub_dfs ) // name of subunit   ["arms", "armL"]
      ,df_subs  = vals( sub_dfs ) // df opt of subunit 
                                  // [df_arms, df_armL, df_armR]
      ,df_com    = [ for(i=range(df_ops)) // part of user ops that have 
                                           // keys showing up in com_keys
                      let( key= df_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) df_ops[i]
                  ] // ["r",1] 
      ,u_com    = com_keys?[ for(i=range(u_ops)) // part of user ops that have keys 
                                      // showing up in com_keys
                      let( key= u_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) u_ops[i]
                  ] // ["r",1]
                  :[]    
      ,u_subops = [ for(sn= subnames)  // Extract subops settings from 
                    hash(u_ops,sn) ]     // user input ops ops, like 
                                       // ["arms",true, "armL",["r",1]]
      // order of reversed priority:
      // [  df_ops, df_arms, df_armL, u_com,u_arms, u_armL ] 
      ,subops = updates(
          df_com
        , [df_subs[0]
          , df_subs[1]
          , u_com
          , u_subops[0]==true? []:ishash(u_subops[0])?u_subops[0]:false
          , u_subops[1]==true? []:ishash(u_subops[1])?u_subops[1]:false
          ])

       ,show= sum( [ for(k=subnames) hash(u_ops,k)==false?1:0])==0
       //,show = len([ for(k=subnames) if(hash(u_ops,k)!=false) 1])>0
       )
(  _fmth(
     [
       "u_ops", u_ops
      ,"df_ops",df_ops
      ,"com_keys", com_keys
      ,"sub_dfs", sub_dfs
       
      ,"subnames", subnames // name of subunit   ["arms", "armL"]
      ,"df_subs",  df_subs // df opt of subunit 
                           // [df_arms, df_armL, df_armR]
      ,"df_com",  df_com 
      ,"u_com",  u_com    
      ,"u_subops", u_subops  // Extract subops settings from 
                             // user input ops ops, like 
                                       // ["arms",true, "armL",["r",1]]
      // order of reversed priority:
      // [  df_ops, df_arms, df_armL, u_com,u_arms, u_armL ] 
      ,"subops",  subops
      ,"show",  show
     ] 
     , pairbreak="<br/>,"  
    )   
);    


//========================================================
function subopts( df_opt=[], u_opt=[], touse=[], dfs=[], com_keys=[] )=
(  // example: Chain
   
   let( df_dfs= [ "markpts", []
                , "labels", []
                , "frame", []
                , "edges", []
                , "xsecs", []
                , "chain", []
                , "plane", []
                , "markxpts", []
                ]
      , df_com_keys= [ "markpts", []
                , "labels", []
                , "frame", [] //["color","transp"]
                , "edges", []
                , "xsecs", []
                , "chain", ["color","transp"]
                , "plane", ["color","transp"]
                , "markxpts", []
                ]
      , dfs = update( 
                joinarr( [ for(k=keys(df_dfs)) if(has(touse,k)) [k,[]] ] )
               ,dfs)    
   )
   joinarr(
     [ for( k= keys(dfs) )
       [k, subopt1( 
              df_opt= df_opt
            , u_opt = u_opt
            , subname = k
            , df_subopt= update( hash(df_dfs,k,[])
                               , hash( dfs, k, []) )
            , com_keys= update( hash(df_com_keys,k,[])
                              , hash( com_keys, k, [] ) )
            )
       ]
  ])
);

//=================================================
subopt1=["subopt1","df_opt,u_opt,subname,[df_subopt,com_keys,mainprop]", "arr","Core",
"Return an arr as the opt of a subunit (named subname) of an object.
;;
;; df_opt: df obj-level opt. Created at obj creation time:
;;
;;         module Obj( u_opt ){
;;            df_opt=[...]          
;;
;; u_opt: user-input obj-level opt at run-time: 
;; 
;;         Obj( u_opt=[...])
;;        
;; subname: name of the sub object, like 'grid' below:
;;
;;         module Obj( u_opt ){
;;            df_opt= [ 'grid',false ...]   // @ create time
;;
;;         Obj( u_opt=[...'grid',true ])    // @ run-time
;;
;;         Note: if the grid is always part of the obj, then set it to true;
;;               if it is not there to start with, but can be added at run-time;
;;               set it to false; 2016.11.23  
;;    
;; df_subopt: default opt for sub object, like:
;;
;;         module Obj( u_opt ){
;;            df_opt= [ 'grid',false ...]   // @ create time
;;            gridopt= subopt1( df_opt, u_opt, 'grid'
;;                            , df_subopt= ['mode',2] )
;;
;; com_keys: default: ['color','transp']
;;
;; mainprop: name of the main prop of sub obj. For example, 
;;           'text' could be the main prop of sub obj 'label'.
;;           A label can be set: 
;;                Obj( u_opt=[ 'label', ['text','P0']] )    
;;           If mainprop set to 'text', you can do:
;;                Obj( u_opt=[ 'label', 'P0'] )    
;;           I.e., val given to 'label' will be assigned to label/text
;;
;; Example usage: 'grid' and 'label' in MarkPt()    
"];
    
function subopt1( df_opt
                , u_opt
                , subname
                , df_subopt=[]
                , com_keys=["color","transp"]
                , mainprop="" )=
(   
    /* Set opt of a single sub. Usage (see MarkPt):
   
       module Obj(u_opt){
         df_opt = [ ... 
                  , "grid", undef | false | true | [ hash ] ==> not true: no grid
                  ]
         gridopt = subopt1( df_opt= df_opt
                          , u_opt = u_opt
                          , subname= "grid"
                          , df_subopt= ["mode",2]  ===> grid default
                          , com_keys= ["color"]
                          );
       }
      
    */
    
    let( df_com = hashex( df_opt, com_keys)  //= hash | []
       , u_com = hashex( u_opt, com_keys)    //= hash | []
       , _df_subopt = updates( [df_com, df_subopt, u_com] )
       , rt_subopt = hash( u_opt, subname, hash( df_opt, subname) )
       )
//    _fmth([ "df_com", df_com
//          , "u_com", u_com
//          , "df_subopt", df_subopt
//          , "_df_subopt", _df_subopt
//          , "rt_subopt",rt_subopt ]) 

    mainprop && isstr(rt_subopt)? update( _df_subopt, [mainprop, rt_subopt] )
    : rt_subopt==true? _df_subopt
    : rt_subopt? update( _df_subopt, rt_subopt)
    : false   // rt_subopt= undef : subname not defined in either u_ or df_opt
              // rt_subopt= false : defined but turned off 
);

//
////=============================================================
//subops=["subops", "parentops=false, subopsname='''',default=[]", "hash", "Args",
//"
// Set the ops of a sub-component of a parent (like, leg features of a
//;; robot). Given a parent ops (for robot properties), a subopsname (''leg''),
//;; and a default hash (like [''count'',4]), return:
//;;
//;;   [],      if parentops[subopsname] is false; 
//;;   default, if parentops[subopsname] is true; 
//;; else:
//;;   update( default, parentops[subopsname])
//;;
//;; A case study: F below has a subunit called Arm that is not added by
//;; default. Users can do F( [''arm'',true] ) to show it in predefined
//;; default, Arm ops, or give runtime ops to customize it.
//;;
//;; module F(ops){
//;;   ops= update( [ ( F's default ops here )
//;;                , ''arm'',false              // disabled arm
//;;                ], ops );                   // updated with runtime ops
//;;   armops= subops( ops, ''arm'', [''len'',5]); // set armops default and allows
//;;                                           // it be updated at runtime
//;;   if(armops) Arm( armops ); // armops has one of the following values:
//;;                             // false, [''len'',5], or updated version like [''len'',7]
//;; } 
//"
//];
//
function subopt( 
      u_opt  
    , df_opt   /* Default obj opt set at the design time, containing the
                  settings for show/hide of group and members 
      
                  [ "r",1          // general option
                  , "arms", true   // Show all arms
                  , "armL", false  // But hide armL
                  ]  
                  
                  Note that only true|false are allowed. Or you can skip it
                  to let the other settings decide.
               */
      , df_subs  /* A hash of two pairs showing both group and member defaults. 
                   It's arranged as group first, and member the 2nd:
                    
                    [ group_name, df_group, memb_name, df_memb]
                   
                    Example: [ "arms", df_arms, "armL", df_armL ] 
                 */
    , com_keys  /* Names of sub opt, which you want them to be also be set 
                   from the obj level. Ex,["r", "color"]
                   Setting these options at the obj level, like 
                   
                   Obj( ... opt= ["r",1] )  
                   
                   set group and member r to 1. Note: 
                   
                   -- This doesn't affect the display setting
                   -- Different groups can have different com_keys
                */
    , debug=false            
    )=
(  let( keys  = keys( df_subs )    // name of subunit   ["arms", "armL"]
      , gn = keys[0]            // group name, "arms"
      , mn = keys[1]            // member name, "armL"
      , df_com= com_keys?
            joinarr( [ for(k=com_keys) [k, hash(df_opt,k)] ] )
            :[]    
//            [ for(i=range(df_opt)) // part of user opt that have 
//                                           // keys showing up in com_keys
//                      let( key= df_opt[ round(i/2)==i/2? i:i-1 ] )
//                      if( index( com_keys, key)>=0) df_opt[i]
//                  ]:[] // ["r",1]                       
      , df = joinarr(updates( [ df_com, df_subs[1], df_subs[3] ] ))
              
      // Chk show/hide
      , df_show_grp = hash( df_opt, str("show_",gn) )
      , df_show_mem = hash( df_opt, str("show_",mn) )
      , df_show = !(  df_show_mem== false
                   || ( df_show_mem== undef
                      && df_show_grp==false
                      )
                   )          
      // Take care of user input below
      , u_grp = hash( u_opt, gn,[] )
      , u_mem = hash( u_opt, mn,[] )
      , u_show = isbool(u_mem)? u_mem
                 : [u_grp, u_mem]==[[],[]]?  df_show
                 : [u_grp, u_mem]==[false,[]]?  false
                 : true     // not found(=Everything else):true
      , u_com= com_keys?
                [ for(i=range(u_opt)) // part of user opt that have keys 
                                      // showing up in com_keys
                      let( key= u_opt[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) u_opt[i]
                  ] // ["r",1]
                  :[] 
      , user = joinarr( updates([u_com, u_grp, u_mem ]) )
      
      ,rtn= u_show? joinarr(update( df, user  )):false
   )           
   debug?
   _fmth( 
   [ "###", _red(" OUTPUT OF subopt_debug() ")
   , "///", "subopt(): get the opt of a group member. This function,"
   , "///", "subopt_debug(), is the same, but outputs all vars that are "
   , "///", "either defined (for checking if defined wrong) or calculated"
   , "///", "(for verifying calc)"           
   , "###", "The Args You Give When Calling subopt()"
   , "///", "These are args of subopt() called inside Obj() to set opt"
   , "///", " of a group member:"
   , "///", " opt_armL = subopt( u_opt, df_opt, df_subs, com_keys )" 
              
   , "u_opt", u_opt  
       , "///", "u_opt is taken from Obj args:"
       , "///", "  Obj(u_opt){ ... opt_armL= subopt(u_opt) ... }"           
   , "///", "The following 3 are defined in Obj(). See below."       
   , "df_opt", df_opt
   , "df_subs", df_subs
   , "com_keys", com_keys
   , "///",""    
   , "###", "Define Default Options"
   , "///", "The following need to be defined in Obj() for subopt()"

   , "df_opt", df_opt
       , "///", "df_opt: obj level default, defining everything obj needs."
   , "df_subs", df_subs
       , "///", "df_subs: define defaults of group and member as"
       , "///", "   df_arms=xxx; df_armL=ooo:"
       , "///", "  and sent to subopt as:"
       , "///", "   df_subs= [ \"arms\",df_arms, \"armL\",df_armL ]"           
   , "com_keys", com_keys
       , "///", "com_keys: member properties that can be set at obj level at RT"
              
   , "###", "Set Default Display"
   , "///", "Display default is defined in df_opt as:"
   , "///", "  df_opt= [...\"show_arms\", true, \"show_armL\", false...]"
   , "///", "They are extracted as df_show_grp, df_show_mem"
   , "df_show_grp", df_show_grp
   , "df_show_mem", df_show_mem
   , "df_com", df_com
   , "///", "df_com is value of com_keys in df_opt. Like: "
   , "///", "[\"color\", \"red\", \"transp\", 1]"
   , "df", df
   , "///", "df_is the final result of default settings."
   , "df_show", df_show
   , "///", "df_show: show or hide decided in default."
   
   , "###", "User input @ RT"
   , "u_opt", u_opt
   , "///", "u_opt: entered by user"
   , "u_com", u_com
   , "///", "Hash of the com_keys defined in u_opt as:"
   , "///", "  u_opt=[ ... \"color\",xxx ...] "
   , "///", "If it's [], means user doesn't set com_keys"
   , "u_grp", u_grp
   , "///", "u_grp: user-given group opt, enter in u_opt as:"
   , "///", "  u_opt=[ ... \"arms\",xxx ...] where xxx could be"
   , "///", "true|false|hash, or it could be [] if not given"
   , "u_mem", u_mem
   , "///", "u_mem: user-given member opt, enter in u_opt as:"
   , "///", "  u_opt=[ ... \"armL\",xxx ...] where xxx could be"
   , "///", "true|false|hash, or it could be [] if not given"
   , "user", user
   , "###", "Result"
   , "u_show",u_show
   , "rtn", rtn 
  ], pairbreak="<br/>,")
  :rtn
);    

    subopt=["subopt","u_opt,df_opt,df_subs,com_key","arr","core",
    "Set child component opt.
    ;; 
    ;; u_opt:  user input opt  
    ;; df_opt: Default obj opt set at the design time, have
    ;;           settings for show/hide of group and members 
    ;;      
    ;;           [ 'r',1          // general option
    ;;           , 'arms', true   // Show all arms
    ;;           , 'armL', false  // But hide armL
    ;;           ]  
    ;;                  
    ;;         Note that only true|false are allowed. Or you 
    ;;         can skip it to let the other settings decide.
    ;;               
    ;; df_subs: A hash of two pairs showing both group and member 
    ;;          defaults: group first, and member the 2nd:
    ;;                    
    ;;           [ group_name, df_group, memb_name, df_memb]
    ;;                 
    ;;          Example: [ 'arms', df_arms, 'armL', df_armL ] 
    ;;
    ;; com_keys: names of the settings that children inherited  
    ;;           from the root (main) opt, like ['color','transp']
    "
    ];
    module subopt_test( mode=MODE, opt=[] ){
        echom("subopt_test");
        opt=[];
        df_arms=["len",3,"r",1];
        df_armL=["fn",5];
        df_armR=[];
        df_opt= [ "len",2
                , "arms", true
                , "armL", true
                , "armR", true
                ];
        com_keys= ["fn"];
        
        /*
            setopt: To simulate obj( opt=[...] )
            args  : opt  
                    // In real use, following args are set inside obj()
                    df_opt
                    df_subopt // like ["arms",[...], "armL",[...]] 
                    com_keys 
                    objname   // like "obj","armL","armR"          
                    propname  // like "len"
        */
        function setopt(   
                opt=opt
                , df_opt=df_opt
                , df_subopt=[ "arms",df_arms, "armL",df_armL,"armR",df_armR]
                , com_keys = com_keys
                
                //
                // The following two are for this testing only.
                //
                , objname ="obj" // what opt you want to test
                                 //  "obj"   : _opt
                                 //  "df_opt": df_opt
                                 //  "armL"  : opt_armL
                                 //  "armR"  : opt_armR
                , propname=undef // If not set, return entire opt indicated
                                 //  with objname; if set, find its value
                                 
                )=
            let(
                 _opt   = update( df_opt, opt )
               , df_arms= hash(df_subopt, "arms")  
               , df_armL= hash(df_subopt, "armL")  
               , df_armR= hash(df_subopt, "armR")  
               , opt_armL= subopt( _opt
                                 , df_opt = df_opt
                                 , sub_dfs= ["arms", df_arms, "armL", df_armL ]                         , com_keys= com_keys
                                 )
               , opt_armR= subopt( _opt
                                 , df_opt = df_opt
                                 , sub_dfs= ["arms", df_arms, "armR", df_armR ]
                                 , com_keys= com_keys
                                 )
               , opt= objname=="obj"?_opt
                        :objname=="df_opt"?df_opt
                        :objname=="armL"?opt_armL
                        :opt_armR
                )
            // ["opt",_opt,"df_arms", df_arms, "opt_armL",opt_armL, "opt_armR",opt_armR];
            propname==undef? opt:hash(opt, propname);
          
         /**
            Debugging Arrow(), in which a getsubopt call results in the warning:
            DEPRECATED: Using ranges of the form [begin:end] with begin value
            greater than the end value is deprecated.
                
         */       
         function debug_Arrow(opt=["r", 0.1, "heads", false, "color","red"])=
            let (
            
               df_opt=[ "r",0.02
                   , "fn", $fn
                   , "heads",true
                   , "headP",true
                   , "headQ",true
                   , "debug",false
                   , "twist", 0
                   ]
              ,_opt= update(df_opt,opt) 
              ,df_heads= ["r", 1, "len", 2, "fn",$fn]    
              , opt_headP=subopt( _opt
                          , df_opt=df_opt
                          , com_keys= [ "color","transp","fn"]
                          , sub_dfs= ["heads", df_heads, "headP", [] ]
                    ) 
            )
            opt_headP;
            
         rtn= doctest(subopt
            , [ ""
                ,"In above settings, opt is the user input, the rest are"
                ,"set at design time. With these:"
                ,""
                ,"// The default opt:"     
                ,[setopt(objname="df_opt")
                     , ["len",2,"arms",true,"armL", true, "armR", true]
                     ,"df_opt"
                     ]

                ,"// The object level (root) opt:"
                ,[setopt(objname="obj", opt=["color","red"])
                     , ["len",2,"arms",true,"armL", true, "armR", true, "color","red"]
                     ,"obj, opt=['color','red']"
                     ]
                ,[setopt()
                     , ["len",2,"arms",true,"armL", true, "armR", true]
                     , "opt"
                     ]
                ,[ setopt(objname="armL")
                     , ["len", 3, "r", 1, "fn", 5]
                     , "opt_armL"
                     ]
                ,[ setopt(objname="armR")
                     , [ "len", 3, "r", 1]
                     , "opt_armR"
                     ]
    //                            
    //            ,""
    //            ,"Set r=4 at the obj level. Since r doesn't show up in com_keys, "
    //            ,"it only applies to obj, but not either arm. "
    //            ,""
    //            
    //            ,[ setopt(objname="df_opt")
    //                 , ["len",2,"arms",true,"armL", true, "armR", true]
    //                 , "df_opt"
    //                 ]
    //            ,[ setopt(opt=["r",4])
    //                 , ["len",2,"arms",true,"armL", true, "armR", true,"r",4]
    //                 , "opt"
    //                 ]
    //            ,[ setopt(opt=["r",4],objname="armL")
    //                 , ["len", 3, "r", 1,"fn", 5]
    //                 , "opt_armL"
    //                 ]
    //            ,[ setopt(opt=["r",4],objname="armR")
    //                 , [ "len", 3, "r", 1]
    //                 , "opt_armR"
    //                 ]
    //                            
    //            ,""
    //            ,"Set fn=10 at the obj level. Since fn IS in com_keys, "
    //            ,"it applies to obj and both arms. "
    //            ,""
    //            
    //            ,[ setopt(objname="df_opt")
    //                 , ["len",2,"arms",true,"armL", true, "armR", true]
    //                 , "df_opt"
    //                 ]
    //            ,[ setopt(opt=["fn",10])
    //                 , ["len",2,"arms",true,"armL", true, "armR", true, "fn",10]
    //                 , "opt"
    //                 ]
    //            ,[ setopt(opt=["fn",10],objname="armL")
    //                 , ["len", 3, "r", 1, "fn", 10]
    //                 , "opt_armL"
    //                 ]
    //            ,[ setopt(opt=["fn",10],objname="armR")
    //                 , ["len", 3, "r", 1, "fn", 10]
    //                 , "opt_armR"
    //                 ]
    //        
    //            ,""
    //            ,"Disable armL:"
    //            ,""
    //            
    //            ,[ setopt(objname="df_opt")
    //                 , ["len",2,"arms",true,"armL", true, "armR", true]
    //                 , "df_opt"
    //                 ]
    //            ,[ setopt(opt=["armL",false])
    //                 , ["len",2,"arms",true,"armL", false, "armR", true]
    //                 , "opt"
    //                 ]
    //            ,[ setopt(opt=["armL",false],objname="armL")
    //                 , false
    //                 , "opt_armL"
    //                 ]
    //            ,[ setopt(opt=["armL",false],objname="armR")
    //                 , ["len", 3, "r", 1]
    //                 , "opt_armR"
    //                 ]
    //
    //
    //            ,""
    //            ,"Disable both arms:"
    //            ,""
    //            
    //            ,[ setopt(objname="df_opt")
    //                 , ["len",2,"arms",true,"armL", true, "armR", true]
    //                 , "df_opt"
    //                 ]
    //            ,[ setopt(opt=["arms",false])
    //                 , ["len",2,"arms",false,"armL", true, "armR", true]
    //                 , "opt"
    //                 ]
    //            ,[ setopt(opt=["arms",false],objname="armL")
    //                 , false
    //                 , "opt_armL"
    //                 ]
    //            ,[ setopt(opt=["arms",false],objname="armR")
    //                 , false
    //                 , "opt_armR"
    //                 ]
            
            
    //            ,""
    //            ,"Debugging Arrow(), in which a getsubopt call results in the warning:"
    //            ,"DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated."
    //            ,""
    //            
    //            ,["debug_Arrow(opt=['r', 0.1, 'heads', false, 'color','red']"
    //             , debug_Arrow(opt=["r", 0.1, "heads", false, "color","red"]),[]
    //             ]
                                
              ]
              
            , mode=mode, opt=opt, scope= [ "df_opt",df_opt, 
                   , "df_arms", df_arms
                   , "df_armL", df_armL
                   , "df_armR", df_armR
                   , "com_keys", com_keys
                   , "opt",opt
                   ]
        );
        //echo("$$$");
        echo( rtn[1] );
    }

//function subops(parentops=false, subopsname="",default=[])=
//(
//	hash(parentops, subopsname)==false?[]
//	:hash(parentops, subopsname)==true? default	
//		:update( default, hash(parentops, subopsname) )
//);
//
//module subopt_test(opt)
//{
//	scope=[ "tire_dft", ["a",10]
//		 , "car1", ["seat",2, "tire",false ]
//		 , "car2", ["seat",2,"tire",true]
//		 , "car3", ["seat",2,"tire",["a",8, "d",15] ]
//		  ];
//	car1= hash(scope, "car1");
//	car2= hash(scope, "car2");
//	car3= hash(scope, "car3");
//	dft = hash(scope, "tire_dft");
//
//	doctest( subopt,
//		[	
//		 "// When tire=false :"
//		, ["parentopt=car1, suboptname=''tire'', default= tire_dft"
//			, subopt( parentopt=car1, suboptname="tire", default=dft)
//			,[]
//			]
//		,"// When tire is set to true at runtime :"
//		,["parentopt=car2, suboptname=''tire'', default= tire_dft"
//			, subopt( parentopt=car2, suboptname="tire", default=dft)
//			,["a",10]
//			]
//		,"// When tire is given a value at runtime :"
//		,["parentopt=car3, suboptname=''tire'', default= tire_dft"
//			, subopt( parentopt=car3, suboptname="tire", default=dft)
//			,["a", 8, "d", 15]
//			]
//		,"// When parentopt not given:"
//		,["default=tire_dft"
//			, subopt( default=dft)
//			,["a", 10]
//			]
//		], opt,["scope",scope]
//		);
//}
////doc(subops);
//subopt_test();     


//function subops_test( opt=[] )=
//(
//    /*
//       Let an obj has 2 sub unit groups: arms and legs, each has L and R
//       Obj: arms{armL,armR}
//          : legs{legL,legR}
//       
//       com_keys for arms: ["color","r"]
//       com_keys for legs: ["color","len"]
//       
//       
//    
//    
//    */
//    //=========================================
//    //=========================================
//    
//    function tester( 
//    // The following are to set defaults on the fly
//    //   for checking how different default settings work.
//    //   I.e., in real use, don't do it this way.
//    // Usage: as a test rec: 
//    //   [
//              u_ops
//              , df_color = undef
//              , df_r     = undef
//              , df_len   = undef
//              , df_show_arms = undef 
//              , df_show_armL = undef 
//              , df_show_armR = undef
//              , df_show_legs = undef
//              , df_show_legL = undef
//              , df_show_legR = undef 
//              
//              , df_arms = undef 
//              , df_armL = undef 
//              , df_armR = undef
//              , df_legs = undef
//              , df_legL = undef
//              , df_legR = undef 
//              
//              , com_keys_arms= ["color","r"]
//              , com_keys_legs= ["color","len"]
//    ){
//        df_ops= updates(
//            [
//              df_color==undef? []:["color", df_color]
//            , df_r    ==undef? []:["r",     df_r]
//            , df_len  ==undef? []:["len",   df_len]
//        
//            , df_show_arms ==undef?[]:["df_arms", df_show_arms]
//            , df_show_armL ==undef?[]:["df_armL", df_show_armL]
//            , df_show_armR ==undef?[]:["df_armR", df_show_armR]
//            , df_show_legs ==undef?[]:["df_legs", df_show_legs]
//            , df_show_legL ==undef?[]:["df_legL", df_show_legL]
//            , df_show_legR ==undef?[]:["df_legR", df_show_legR]
//            ]
//        );
//        
//        function get_ops( df_subs, com_keys )=
//            subops( u_ops
//                  , df_ops = df_ops
//                  , df_subs= df_subs
//                  , com_keys=com_keys
//                  );
//        function get_arm_ops( armname, df)=
//            get_ops( df_subs= [ "arms", df_arms, armname,df]
//                   , com_keys= com_keys_arms
//                   );
//        function get_leg_ops( legname, df)=
//            get_ops( df_subs= [ "legs", df_legs, legname,df]
//                   , com_keys= com_keys_legs
//                   );
//                   
//        // We need to set 4 ops:
//        
//        ops_armL = get_arm_ops( "armL", df_armL );
//        ops_armR = get_arm_ops( "armR", df_armR );
//        ops_legL = get_leg_ops( "legL", df_legL );
//        ops_legR = get_leg_ops( "legR", df_legR );
//            
//        
//    }
////    
////    opt=[];
////    df_arms=["len",3,"r",1];
////    df_armL=["fn",5];
////    df_armR=[];
////    df_ops= [ "len",2
////            , "arms", true
////            , "armL", true
////            , "armR", true
////            ];
////    com_keys= ["fn"];
////    
////    /*
////        setops: To simulate obj( ops=[...] )
////        args  : ops  
////                // In real use, following args are set inside obj()
////                df_ops
////                df_subops // like ["arms",[...], "armL",[...]] 
////                com_keys 
////                objname   // like "obj","armL","armR"          
////                propname  // like "len"
////    */
////    function setops(   
////            ops=opt
////            , df_ops=df_ops
////            , df_subops=[ "arms",df_arms, "armL",df_armL,"armR",df_armR]
////            , com_keys = com_keys
////            , objname ="obj"
////            , propname=undef
////            )=
////        let(
////             _ops= update( df_ops, ops )
////           , df_arms= hash(df_subops, "arms")  
////           , df_armL= hash(df_subops, "armL")  
////           , df_armR= hash(df_subops, "armR")  
////           , ops_armL= subops( _ops, df_ops = df_ops
////                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]                    
////                    , com_keys= com_keys
////                    )
////            , ops_armR= subops( _ops, df_ops = df_ops
////                        , sub_dfs= ["arms", df_arms, "armR", df_armR ]
////                        , com_keys= com_keys
////                        )
////            , ops= objname=="obj"?_ops
////                    :objname=="df_ops"?df_ops
////                    :objname=="armL"?ops_armL
////                    :ops_armR
////            )
////        // ["ops",_ops,"df_arms", df_arms, "ops_armL",ops_armL, "ops_armR",ops_armR];
////        propname==undef? ops:hash(ops, propname);
////      
////     /**
////        Debugging Arrow(), in which a getsubops call results in the warning:
////        DEPRECATED: Using ranges of the form [begin:end] with begin value
////        greater than the end value is deprecated.
////            
////     */       
////     function debug_Arrow(ops=["r", 0.1, "heads", false, "color","red"])=
////        let (
////        
////           df_ops=[ "r",0.02
////               , "fn", $fn
////               , "heads",true
////               , "headP",true
////               , "headQ",true
////               , "debug",false
////               , "twist", 0
////               ]
////          ,_ops= update(df_ops,ops) 
////          ,df_heads= ["r", 1, "len", 2, "fn",$fn]    
////          , ops_headP= subops( _ops, df_ops=df_ops
////                , com_keys= [ "color","transp","fn"]
////                , sub_dfs= ["heads", df_heads, "headP", [] ]
////                ) 
////        )
////        ops_headP;
////        
////     doctest(subops
////        , [ ""
////            ,"In above settings, ops is the user input, the rest are"
////            ,"set at design time. With these:"
////            ,""
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops"
////                 , setops()
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops_armL"
////                 , setops(objname="armL")
////                 , ["len", 3, "r", 1, "fn", 5]]
////            ,["ops_armR"
////                 , setops(objname="armR")
////                 , [ "len", 3, "r", 1]]
////                            
////            ,""
////            ,"Set r=4 at the obj level. Since r doesn't show up in com_keys, "
////            ,"it only applies to obj, but not either arm. "
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true],]
////            ,["ops"
////                 , setops(ops=["r",4])
////                 , ["len",2,"arms",true,"armL", true, "armR", true,"r",4],]
////            ,["ops_armL"
////                 , setops(ops=["r",4],objname="armL")
////                 , ["len", 3, "r", 1,"fn", 5]]
////            ,["ops_armR"
////                 , setops(ops=["r",4],objname="armR")
////                 , [ "len", 3, "r", 1]]
////                            
////            ,""
////            ,"Set fn=10 at the obj level. Since fn IS in com_keys, "
////            ,"it applies to obj and both arms. "
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true],]
////            ,["ops"
////                 , setops(ops=["fn",10])
////                 , ["len",2,"arms",true,"armL", true, "armR", true, "fn",10],]
////            ,["ops_armL"
////                 , setops(ops=["fn",10],objname="armL")
////                 , ["len", 3, "r", 1, "fn", 10]]
////            ,["ops_armR"
////                 , setops(ops=["fn",10],objname="armR")
////                 , ["len", 3, "r", 1, "fn", 10]]  
////                 
////        
////            ,""
////            ,"Disable armL:"
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops"
////                 , setops(ops=["armL",false])
////                 , ["len",2,"arms",true,"armL", false, "armR", true]]
////            ,["ops_armL"
////                 , setops(ops=["armL",false],objname="armL")
////                 , false]
////            ,["ops_armR"
////                 , setops(ops=["armL",false],objname="armR")
////                 , ["len", 3, "r", 1]]
////
////
////            ,""
////            ,"Disable both arms:"
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops"
////                 , setops(ops=["arms",false])
////                 , ["len",2,"arms",false,"armL", true, "armR", true]]
////            ,["ops_armL"
////                 , setops(ops=["arms",false],objname="armL")
////                 , false]
////            ,["ops_armR"
////                 , setops(ops=["arms",false],objname="armR")
////                 , false]
////        
////        
//////            ,""
//////            ,"Debugging Arrow(), in which a getsubops call results in the warning:"
//////            ,"DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated."
//////            ,""
//////            
//////            ,["debug_Arrow(ops=['r', 0.1, 'heads', false, 'color','red']"
//////             , debug_Arrow(ops=["r", 0.1, "heads", false, "color","red"]),[]
//////             ]
////                            
////          ]
////          
////        , ops, [ "df_ops",df_ops, 
////               , "df_arms", df_arms
////               , "df_armL", df_armL
////               , "df_armR", df_armR
////               , "com_keys", com_keys
////               , "ops",opt
////               ]
////    )
//);
