include <../scadx.scad>
//include <../scadx_subdiv.scad>

module demo_get_smooth_PtsFaces()
{
  echom("demo_get_smooth_PtsFaces()");
  
  //echo("Try the following data:");
  //echo(keys(POLY_SAMPLES));
  // [ "plane", "non-plane", "plane pantagon", "nonplane pantagon"
  // , "nonplane pantagon2", "poly_patch", "L_poly", "rod3", "cone4"
  // , "cube", "longcube"]
  
  //ptsfaces= hash(POLY_SAMPLES,"longcube");
  ptsfaces= hash(POLY_SAMPLES,"poly_patch");
  pts     = hash(ptsfaces, "pts");
  faces   = hash(ptsfaces, "faces");
  inherited_subfaces = re_align_a_pis(faces);
  echo(inherited_subfaces=inherited_subfaces);
       
  h_poly= get_smooth_PtsFaces(     
                          ptsfaces = ptsfaces
                          , shrink_ratio=0.25
                          //, _isPatch=false
                          , depth=2
                          );  

  subPts = hash( h_poly, "pts");
  subfaces = hash( h_poly, "faces");
  
  module mark(pts)
  {
  MarkPts(pts, Ball=false, Line=false
         , Label=["text", range(subPts), "indent",0, "scale",0.4, "color","black"]
         );
  }
  // [subdiv] Make demo_get_smooth_PtsFaces able to take patch, not completely successful (degenerate polygon). [range] get3m"
//  MarkPts(subPts, Ball=false, Line=false
//         , Label=["text", range(subPts), "indent",0, "scale",0.2, "color","black"]
//         );

  color("black")
  for(face= hash(ptsfaces,"faces"))
    for( edge = get2(face) )
    {  
      Line( get(pts, edge), r=0.01);
    }   
 
 //for(face = inherited_subfaces)
 //   Plane( get(subPts,face), mode=1, color="red");
     
//  color("gold")polyhedron( subPts
//            , hash( h_poly, "faces" )
//            );   
//            

//  for(fi=range(subfaces)) //face=faces) 
//  { subface=subfaces[fi];
//    fpts = get(subPts,subface);
//    //MarkPts(fpts, Line=false, Ball=false);           
//                   
//    MarkPt( sum(fpts)/len(fpts)
//          , Ball=false
//          , Label=["text",fi,"indent",0, "color","red", "scale", 0.3] );        
//    //mark(pts);
//  }
  
     // color("green", 0.3)
       polyhedron( subPts
                 , subfaces
                 );
   //echo(subfaces=subfaces);
}
//demo_get_smooth_PtsFaces();