include <../scadx.scad>
/* 
  I.
  
  This file is created for the sole purpose of comparing two different 
  smoothing approaches differ in the ways the subPts and subfaces are obtained: 
  
  (1) EdgeFace approach:
  
      Use edgeFace data structure to get subfaces, and 
      the following approach (edge_ratio) to get subPts:
     
             P2               P3
        +----*--------*---+ ---
        |    :        :   |  | ----> shrink_ratio
        *...S2-------S3...* ---
        |    |        |   |
        |    |        |   |
        |    |        |   |
        *...S1-------S0...* ---
        |    :        :   |  | ----> shrink_ratio
        +----*--------*---+ ---      
       P1             ^   P0
                      |
                      +------------- node
     
      Cut edges to ratio at nodes, connect nodes, take intersections.
      See DooSabin_face_shrink(pts,ratio=0.15) in scadx_edgeface.scad 
      
  (2) ClockEFs approach:
  
     Use ClockEFs data structure to get subfaces, and 
     the following approach (centering_ratio) to get subPts:
     

            P2               P3
        +---------------+ ---
        |'.          .' |   | ----> shrink_ratio
        |  S2      S3--------
        |    '.  .'     |
        |      'C.      |
        |    .'   '.    |
        |  S1      S0-------
        |.'          '. |  | ----> shrink_ratio
        +---------------+---      
       P1              P0
                     
     Take center pt C, cut C-pt to ratio at S. 
     See get_subFacePts() in scadx_subdiv.scad.                

  II. 
  
  Observations:
  
  --- (2) is far faster than (1) (by a factor of at least 5)
  --- (2) produces more UNEVEN faces, making it ungly and impractical.

*/

module 184p_compare_ClockedEFS(shrink_ratio=0.2)
{
    echom(_span("Centering-ratio for pts, ClockedEFS for faces --- un-planar faces, much faster"
               ,s="color:red;font-size:20px;font-weight:900px;background:lightgreen"));
   
    //=======================================================
    function get_tip_subfaces( aclockedEFs )=
    (
      //echo("-------------- get_tip_subfaces( aclockedEFs )" )
      [ for( cf= aclockedEFs)
         [ for( h=vals( cf ) ) 
           hash(h, "subface")[0]
         ]      
      ]
    );
    //=======================================================
    function get_edge_subfaces( aclockedEFs, edges, faces )=
    (
      /// edges can be given, or calc if faces is given 
  
      //echo("-------------- get_edge_subfaces()")
      let( edges = edges==undef? get_edges(faces): edges )
      [ for( pi = range(aclockedEFs))
         each
         [ for( kv=hashkvs( aclockedEFs[pi] ) ) 
            if( has(edges, [pi, kv[0]]) )
             hash(kv[1], "subface") 
         ]   
      ]
    );
    //=======================================================
    function get_subfaces( aclockedEFs, faces )=
    (
      //echo("-------------- get_subfaces( aclockedEFs, faces )" )
      concat( get_core_subfaces( faces )
            , get_tip_subfaces( aclockedEFs )
            , get_edge_subfaces( aclockedEFs, faces=faces )
            )
    );
    //=======================================================
    module 184o_Smooth_obj( pts,faces, cycle=1
                     , original_frame=[]
                     , is_label = false
                     , is_echo = true  
                     , _i=0)
    {
       echom("184o_Smooth_obj()");
   
       //echo(cycle=cycle, original_frame=original_frame, _i=_i);
       if( _i==0 && original_frame )
       { 
         //echo("drawing frame");
         for( f = faces ) Line( get(pts,f), closed=true, opt=original_frame );
       }

       subPts = get_subPts( pts= pts, faces=faces, shrink_ratio=shrink_ratio );
       clockedEFs= get_clocked_EFs( pts, faces ); ///<======
       subfaces= get_subfaces( clockedEFs, faces);
       echo( clockedEFs = clockedEFs);
       	
        core_subfaces = get_core_subfaces( faces );
        tip_subfaces = get_tip_subfaces( clockedEFs );
        edge_subfaces = get_edge_subfaces( clockedEFs, faces=faces );
       
       	
       if(cycle>_i+1)
       {
         184o_Smooth_obj(pts=subPts
                   , faces=subfaces
                   , cycle=cycle
                   , is_label = is_label
                   , is_echo=is_echo
                   ,_i=_i+1);
       }
       
       else  // Final cycle
       {
 
        if(is_echo)
        {
          //echo( cycle=cycle );
          //echo( len_subPts = len(subPts), len_subfaces = len(subfaces) );
          echo(_s("<b>ClockedEFS</b> w/ shrink_ratio=<b>{_}</b>, Got <b>{_}</b> pts and <b>{_}</b> faces after <b>{_}</b> cycles"
                 , [shrink_ratio, len(subPts), len(subfaces), cycle ]
                 )
              );   
          if(is_echo==2)
          { echo("");
            echo( subPts = subPts );
            echo("");
            echo( subfaces = subfaces );
          }  
        }
        
        if(is_label)
        {
          
           for( fi = range(subfaces) ) 
            {
              f = subfaces[fi];
              MarkPt( sum( get(subPts, f) ) / len(f)
                    , Ball=false
                    , Label=["text",fi, "indent",0, "color","black", "scale",0.5] 
                    ); 
              if( fi==32 || fi == 33 )      
                Line( get(subPts,f), closed=false, color="red" );
            }
        
           //echo( core_subfaces = core_subfaces );   
           //echo( tip_subfaces = tip_subfaces );   
           //echo( edge_subfaces = edge_subfaces ); 
           echo( _b("subPts= "), subPts );         
           echo( _b("subfaces= "), subfaces );
       
           for( f = core_subfaces )
           { Plane( get(subPts,f), color=["gold",0.5] );}   
           for( f = tip_subfaces )
           { Plane( get(subPts,f), color=["green",0.5] );}   
           for( f = edge_subfaces )
           { Plane( get(subPts,f),  color=["blue",0.5] );}
           //color("gold", 0.7)
           //polyhedron( subPts, subfaces );
       
        }
        else
        
          polyhedron( subPts, subfaces );
       }
       //MarkPts(pts, Line=["closed",true]);
    }

    
  //=======================================================
  //=======================================================
  //=======================================================

   pf = hash( POLY_SAMPLES, "cone4" );
   
   //color("teal")
   184o_Smooth_obj( pts = hash( pf, "pts" )
             , faces= hash( pf, "faces" )
             , original_frame=["r",0.01, "color","black"]
             , is_label=true
             , is_echo=true
             , shrink_ratio=shrink_ratio
             , cycle=2
             
             );

}

//184p_compare_ClockedEFS();

module 184p_compare_edgefaces(shrink_ratio=0.25)
{
    echom(_span("Edge-ratio for pts + Edgefaces for faces --- more planar faces, slower"
         ,s="color:blue;font-size:20px;font-weight:900px;background:lightgreen"));
    
    /*
  
       functions/modules are copied from scadx_edgiface.scad
     
       Except from dev_edgeface.scad: 
            
         1848_get_edge_subface() 
         1848_get_subpi_of_pi_on_fi()
         1848_get_smooth_PtsFaces()
  
    */
  
    function get_edgefi(faces,_hash=[],_i=0)=
    (  // An edgefi is:
       // [ [2,3], ["L",3,"R",6,"F",[4], "B",[0,1]] 
       // , ...
       // ]
       // Means, for edge [2,3], 
       //   it's left face is faces[3]
       //   it's right face is faces[3]
       //   it's forward faces are faces[4]
       //   it's backward faces are faces[0,1]
       // 
        _i<len(faces)? 
        let( face = faces[_i]   // a face is an array of pt-indices
             , edges= get2(face) 
             , _new = [ for(eg=edges) 
                        each [eg, [ haskey(_hash, [eg[1],eg[0]])?"L":"R"
                                  , _i ] 
                             ]
                      ]              
             )
        ////get_edgeface(faces, _hash= update(_hash,_hash_new), _i=_i+1 )
        get_edgefi(faces, _hash= edgefi_update(_hash,_new), _i=_i+1 )
    
        :_hash
    
        /*
        :// We have done adding the R/L faces
         // Now we add fowward and backward faces 
         let( pt_fis= get_a_fis_on_pts(faces) 
                     // pt_fis: indices of faces connect to a pt
                     // [ [2,0,1], [5,4,2] ...] = pts[0] has faces 2,0,1  
              )
           [ for( eg=keys(_hash))
             let( h_LRfis = hash(_hash, eg) // ["L",4,"R",5]
                , LRfis = vals(h_LRfis) // [4,5]
                , p1fis = pt_fis[ eg[1] ]
                , Ffis  = dels( p1fis, LRfis )
                , p0fis = pt_fis[ eg[0] ]
                , Bfis  = dels( p0fis, LRfis )
                )
             each [ eg, concat( h_LRfis
                              , ["F", Ffis,"B", Bfis]
                              )
                  ] 
           ]
        */   
    );
  
    //====================================================
    function edgefi_update(ef1, ef2, _i=0)=
    (
       _i> len(ef2)-2? ef1
       : let( eg2= ef2[_i]   // current edge by index, like [2,3]
            , f2s= ef2[_i+1] // current face hash, like 
                             //  [ "L",3, "R", 6 ]
                             //  3: left face index (in faces) = faces[3]
                             // An ef is:
                             // [ [2,3], ["L",3,"R",6] 
                             // , ...
                             // ]
            , eg2rev = [ eg2[1], eg2[0] ]                 
            , ef1= haskey(ef1, eg2) // eg2 found in ef1, so we need to replace
                                    // [ "L",3, "R",6 ] of ef1.eg2
                                    // with f2s (= ef2.eg2)
                   ? update( ef1
                           , [ eg2 
                             , update( hash(ef1, eg2), f2s )
                             ]
                           )
                   :haskey(ef1, eg2rev )?
                     update( ef1
                           , [ eg2rev
                             , update( hash(ef1, eg2rev), f2s )
                             ]
                           )
                   : concat( ef1, [eg2, f2s] )
            )
          edgefi_update( ef1, ef2, _i=_i+2)
    );
    //====================================================
  
    function re_align_a_pis(faces,_rtn=[],_L=0, _i=0)=
    (  
       // rodfaces(3):
       // [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
       // Re-align the indices to:  
       // [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
       _i>=len(faces)?_rtn
       : re_align_a_pis( faces=faces
                           , _rtn= concat(_rtn, [range(_L, _L+len(faces[_i]))] )
                           , _L=_L+len(faces[_i])
                           , _i=_i+1
                           )
   
    );
    //====================================================
    function shrinkPts(pts, ratio=0.15)=
    (
       /*
            .---------.
            | .-----.  \
            | |      \  \
            | |       \  \
            | |        \  \
            | '--._ _-'  _- 
            '--._  `  _-'  
                 `'-.' 
           P2               P3
            +----------------+ ---
            |                |  | ----> ratio/2
            |  S2-------S3   | ---
            |   |        |   |
            |   |        |   |
            |   |        |   |
            |  S1-------S0   |---
            |                | | ----> ratio/2
            +---*--------*---+---      
           P1            ^   P0
                         |
                         +------------- node
                          
       --- Following DooSabin approach for subdivision:                       
           Take edge P0-P1, divide it according to ratio
       --- Pts don't need to be co-planar
       */      
       DooSabin_face_shrink(pts,ratio)
    );
    //====================================================
    function get_subpis_on_pi(a_pis, pi, subfi=undef)=
    (
       subfi==undef ?
       let( //fisubpis_by_pis= [ for(pis=a_pis) get_fisubpis_by_pis(a_pis, pis) ]
            // subpis_by_pis =
            //   [ [0,[0,1,2]], [1,[3,4,5]], [2,[6,7,8,9]]
            //   , [3,[10,11,12,13]], [4,[14,15,16,17]] ]
            a_subpis = re_align_a_pis(a_pis)
          , cw_fis_on_pi = get_a_fis_on_pts(a_pis)[pi] // [2,0,1] ( already clock-wized)
          )
        //cw_fis_on_pi
        [ for(fi=cw_fis_on_pi) a_subpis[fi][ idx( a_pis[fi], pi) ] ] 
    
       :let( a_subpis = re_align_a_pis(a_pis)
             // a_pis   = [[2,1,0],[3,4,5],[0,1,4,3],[ 1, 2, 5, 4],[ 2, 0, 3, 5]] 
             // a_subpis= [[0,1,2],[3,4,5],[6,7,8,9],[10,11,12,13],[14,15,16,17]]
          , fi = subfi 
          , pis = a_pis[ fi ]
          , pi_th = idx( pis, pi )
          )
       a_subpis[ subfi ][ pi_th ]
    );
    //====================================================
    function get_a_fis_on_pts(faces)=
    (  
      let( pts_i_max = max( flatten(faces) ) // pt index max
         , frng = [1:len(faces)-1]
         , _rtn = [ for(pti=[0:pts_i_max])
                    [ for(fi=[0:len(faces)-1])
                      if(has(faces[fi],pti)) fi 
                    ]
                  ]
                 // Pts[0] neighbors 3 faces: faces[0], faces[2], faces[4]
                 // [[0, 2, 4], [0, 2, 3], [0, 3, 4], [1, 2, 4], [1, 2, 3], [1, 3, 4]]
             
         , rtn = [ for(pi=range(_rtn))
                  clock_wize_fis_on_pi ( faces,pi,_rtn[pi] )
                  // This makes them clock-wize. 
                  // i.e., faces[0]-faces[2]-faces[4] is clock-wize for pts[0]
                  // i.e., faces[0]-faces[3]-faces[2] is clock-wize for pts[1]
                  // [[0, 2, 4], [0, 3, 2], [0, 4, 3], [1, 4, 2], [1, 2, 3], [1, 3, 4]] 
                 ]        
         )
         rtn
    );
    //====================================================
    function _re_arrange_a_edges_on_pi_rev_last(a_edges_on_pi_rev_last,_rtn=[],_i=0)=
    (
        // Internal func for clock_wize_fis_on_pi()
    
        // a_edges_on_pi_rev_last=
        //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
        //  , [0, [1,2],[2,4],[4,5],[1,5] ]
        //  , [1, [1,0],[0,3],[3,2],[1,2] ]
        //  ]  
        // is re-arranged to:      
        //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
        //  , [1 [1,0],[0,3],[3,2],[1,2] ]
        //  , [0 [1,2],[2,4],[4,5],[1,5] ]
        //  ]  
        // such that they are in order in terms of edge continuatility :
          
        //  [ [2, [1,5],...,[1,0] ]
        //  , [1, [1,0],...,[1,2] ]
        //  , [0 [1,2],...,[1,5] ]
        //  ]
     
        //echo(_i=_i)
        //echo(a_edges_on_pi_rev_last=a_edges_on_pi_rev_last)  
       _i>=len(a_edges_on_pi_rev_last)-1? _rtn
       :let( _rtn=_i==0?[a_edges_on_pi_rev_last[0]]:_rtn
           // _rtn=_i==0?[a_edges_on_pi_rev_last[0]]:_rtn
           , prevlast= last(last(_rtn)) 
            , _rtn2= concat( _rtn
                           , [ for(f=a_edges_on_pi_rev_last)
                               //echo(f=f)
                               if(f[1]==prevlast)f 
                               ]
                           )      
            )
        //echo(_rtn=_rtn)     
        //echo(_rtn2=_rtn2)     
        //echo(prevlast=prevlast)    
        _re_arrange_a_edges_on_pi_rev_last(a_edges_on_pi_rev_last
                                           ,_rtn=_rtn2,_i=_i+1)           
          
    );      
    //====================================================
    function clock_wize_fis_on_pi( faces      // = a_fis
                               , pi         // i of one pt 
                               , fis_on_pi  // [2,0,1]
                               )=
    (
        /*
                      4 
                  _.-'`'-._ 
             _.-'`         '-._
           2'._       fi=0     '- 5
            |  `-._          _.-'\   
            |       '-._   _-'    \
            |   fi=1    1'         \ 
           3-._         |   fi=2   _\            
               `-._     |      _.-'  6
                   `-._ | _.-'  
                       0 `
    
        Let fis_of_pi = [0,1,2]  (it could be any order)
    
        clock_wize_fis_on_pi( faces, 1, [0,1,2] ) => [0,2,1]
     
        For fi_0 to be on the next of fi_1:
          [2,1] of fi_1 = reversed edge [1,2] of fi_0 
      
        edges:
          fi=0: [ [1,2],[2,4],[4,5],[5,1] ]
          fi=1: [ [1,0],[0,3],[3,2],[2,1] ]
          fi=2: [ [1,5],[5,6],[6,0],[0,1] ]
          
       */ 
       let( firng= range(fis_on_pi)
          //, fis_on_pi = 
          , a_pis_on_pi= [ for(fi=fis_on_pi) faces[fi] ] 
              // fis_on_pi= [2,0,1]
              // a_pis_on_pi = [ [6,0,1,5],[1,2,4,5],[3,2,1,0]]
            //----------------------------------------                                                                                           
          , a_edges_on_pi = [ for(pis=a_pis_on_pi) get2(pis) ]
              // a_edges_on_pi=
              //  [ [ [6,0],[0,1],[1,5],[5,6] ]
              //  , [ [1,2],[2,4],[4,5],[5,1] ]
              //  , [ [3,2],[2,1],[1,0],[0,3] ]
              //  ]
            //----------------------------------------                                                                                           
          , a_edges_on_pi_rolled = 
              [ for(egs=a_edges_on_pi)
                 let(i= idx( [for(eg=egs)eg[0]], pi)) // i= index of edge where
                    i>0? roll(egs, -i): egs            // the pi is the first i
              ]                                        // like [2,3] for pi=2
                                                      // [5,4] for pi=5
              // a_edges_on_pi_rolled=
              //  [ [ [1,5],[5,6],[6,0],[0,1] ]
              //  , [ [1,2],[2,4],[4,5],[5,1] ]
              //  , [ [1,0],[0,3],[3,2],[2,1] ]
              //  ]     
            //----------------------------------------                                                                                           
          , a_edges_on_pi_rev_last= 
             [ for(fi=firng)
                let( egs=a_edges_on_pi_rolled[fi])
               [ fis_on_pi[fi]
               , for(eg=egs)
                 eg==last(egs)? [eg[1],eg[0]]: eg 
               ]
            ]  
              // a_edges_on_pi_rev_last=
              //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
              //  , [0, [1,2],[2,4],[4,5],[1,5] ]
              //  , [1, [1,0],[0,3],[3,2],[1,2] ]
              //  ]   
              // The 2,0,1 above is the fi
            //----------------------------------------                                                                                           
           , re_arranged = 
              _re_arrange_a_edges_on_pi_rev_last(a_edges_on_pi_rev_last)
              // re_arranged=
              //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
              //  , [1, [1,0],[0,3],[3,2],[1,2] ]
              //  , [0, [1,2],[2,4],[4,5],[1,5] ]
              //  ]   
            //----------------------------------------                                                                                           
           )
        //echo(faces=faces)   
        //echo(pi=pi)
        //echo(fis_on_pi=fis_on_pi)
        //echo(a_pis_on_pi=a_pis_on_pi)   
        //echo(a_edges_on_pi=a_edges_on_pi)   
        //echo(a_edges_on_pi_rolled=a_edges_on_pi_rolled)   
        //echo(a_edges_on_pi_rev_last=a_edges_on_pi_rev_last)   
        //echo("-------") 
        [for(x=re_arranged)x[0]]
   
    );
    //====================================================
    function DooSabin_face_shrink(pts,ratio=0.15)=
    ( /* Check out dev_edgeface.scad to see how this 
          formula is derived
       */
       let( rng=[0:len(pts)-1]
          , p2 = get2(pts)
          , nodepairs=[ for(i=rng) 
                        onlinePts( p2[i], ratios=[ratio, 1-ratio])
                        // This works, too, but hard to read
                        // [ (1-ratio)*p2[i][0]+ratio*p2[i][1]
                        //, ratio*p2[i][0]+(1-ratio)*p2[i][1]
                        //]
                      ]
          )
          [ for(i=rng)
             midPt( lineBridge( [nodepairs[i][0], get(nodepairs,i-2)[1]]
                              , [get(nodepairs,i-1)[1], get(nodepairs,i+1)[0]]
                              )
                  ) 
          ]
    );
    //====================================================
    function 1848_get_edge_subface(faces, inherited_subfaces, edgefi, edge)=
    (
        /*
          -------4-------
          ====7  |  3====
              |  |  |  
              |  |  |  
          ====6  |  2====
          -------3--------
            
          For edge [3,4], return its edge_subface [6,7,3,2]
          get_edge_subface(faces, inherited_subfaces, edgefi, [3,4])= [6,7,3,2]       

        */
   
        let( cur_ef= hash(edgefi, edge) // ["R",3,"L",2,"F",[4,5],"B",[0,1]]
           , L_fi= hash(cur_ef,"L") 
           , R_fi= hash(cur_ef,"R")
           )
        [ 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[0],L_fi)
        , 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[1],L_fi)
        , 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[1],R_fi)
        , 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, edge[0],R_fi)
        ]
    );
    //====================================================
    function 1848_get_subpi_of_pi_on_fi(faces, inherited_subfaces, pi,fi)=
    (  
       /* Internal function for 1848_get_edge_subface()
   
          faces[2]    faces[4]
         ----------4-----------
           9====7  |  3====2
           |    |  |  |    |
           |    |  |  |    |
          10====6  |  2====1
         ----------3-----------
       
         get_subpi_of_pi_on_fi(faces, inherited_subfaces, 4,2) = 3
         get_subpi_of_pi_on_fi(faces, inherited_subfaces, 3,2) = 2
     
     
        
       */
        let( i_on_face= idx(faces[fi],pi ) ) 
        inherited_subfaces[fi][i_on_face]   
    );
    //====================================================
  
    function 1848_get_smooth_PtsFaces(pts, faces, shrink_ratio=shrink_ratio, depth=1
                                     , ptface 
                                     , _i=0)=
    (
        // return [subPts, subfaces ]
        //_i>=depth? ["pts", pts, "faces", faces ]
        _i>=depth? [pts, faces ] // Change final rtn to array 
                                 // for easier access later
                                 // 2018.4.25
        :let( 
        , pts = _i==0 && ptface? hash(ptface, "pts"): pts
        , faces = _i==0 && ptface ? hash(ptface, "faces"):faces
        , prng= [0:len(pts)-1]
        , frng= [0:len(faces)-1]
        , edgefi= get_edgefi(faces)
         // edgefi = 
         // [ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
         // , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
         // , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
         // , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
         // , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
         // , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
         // , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
         // , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
         // , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
         // ]
       
       , edges = keys(edgefi)
         //----------------------------------------------
         //  subfaces has 3 parts:
         //  
         //   inherited_subfaces
         //   tip_subfaces
         //   edge_subfaces
   
         //------------------------------------------------------
         //
         //  Get inherited_subfaces
         //
         //------------------------------------------------------
       
       , inherited_subfaces = re_align_a_pis(faces)
         //  inherited_subfaces= 
         //  [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
         
       , a_subfacePts= [ for(face=faces)
                         shrinkPts( get(pts, face), ratio = shrink_ratio ) ]
         //echo(a_subfacePts=a_subfacePts);
    
       , subPts = [for(subfacePts=a_subfacePts) each subfacePts] 
 
         //------------------------------------------------------
         //
         //  Get tip_subfaces
         //
         //------------------------------------------------------
       
       , tip_subfaces = [ for(pi= prng) get_subpis_on_pi(faces, pi) ]
         // tip_subfaces= 
         //  [[2, 6, 15], [1, 10, 7], [0, 14, 11], [3, 16, 9], [4, 8, 13], [5, 12, 17]]
 
         //------------------------------------------------------
         //
         //  Get edge_subfaces
         //
         //------------------------------------------------------
        
       , edge_subfaces= [ 
            for(edge=edges) 
            1848_get_edge_subface(faces, inherited_subfaces, edgefi, edge)
            ]
          
         //------------------------------------------------------
          
         //echo(edge_subfaces=edge_subfaces);
         // edge_subfaces= 
         // [ [11, 10, 1, 0], [7, 6, 2, 1], [15, 14, 0, 2], [9, 8, 4, 3]
         // , [13, 12, 5, 4], [17, 16, 3, 5], [10, 13, 8, 7], [16, 15, 6, 9]
         // , [14, 17, 12, 11]]
       , all_subfaces = concat( inherited_subfaces
                              , tip_subfaces
                              , edge_subfaces )
       //, _ptsfaces= [subPts,all_subfaces]
                         
       )
   
       1848_get_smooth_PtsFaces( pts = subPts
                               , faces = all_subfaces
                               , shrink_ratio=shrink_ratio, depth=depth
                               , ptface = ptface
                               , _i=_i+1)
       //echo(inherited_subfaces=inherited_subfaces)
       //echo(tip_subfaces=tip_subfaces)
       //echo(edge_subfaces=edge_subfaces)
    );   

    module testing_1848_get_smooth_PtsFaces()
    {
      echo("testing_1848_get_smooth_PtsFaces()");
      ptface = hash(POLY_SAMPLES,"cone4");
      pts = transPts(hash( ptface, "pts"), ["x", 6]);
      faces = hash( ptface, "faces");
      
      for( f = faces ) 
        Line( get(pts,f), closed=true, opt=["r",0.01, "color","black"]);
      
      depth = 2;
      aPoly= 1848_get_smooth_PtsFaces(     
                               pts = pts //hash(ptface,"pts") //cubePts(3)
                              , faces = faces //hash(ptface, "faces") // rodfaces(4)
                              //, ptface = ptface
                              , shrink_ratio=shrink_ratio
                              , depth=depth
                              ); 

      subPts = aPoly[0];
      subfaces = aPoly[1];
      echo(_s("EdgeFace w/ shrink_ratio= <b>{_}</b>: Got <b>{_}</b> pts and <b>{_}</b> faces after depth <b>{_}</b>"
                   , [shrink_ratio, len(subPts), len(subfaces), depth ]
                   )
                );                         
      echo( _b("subPts= "), subPts );         
      echo( _b("subfaces= "), subfaces );
      //
      for( fi = range(subfaces) ) 
      {
        f = subfaces[fi];
        fpts = get(subPts, f);
        MarkPt( sum( fpts ) / len(f)
              , Ball=false
              , Label=["text",fi, "indent",0, "color","black", "scale",0.5] 
              ); 
        if( fi==30 || fi == 61 )      
          Line( fpts, closed=false, color="red" );
      }
      color("gold", 0.8)
      polyhedron( subPts, subfaces );                        
    }
    testing_1848_get_smooth_PtsFaces();

}

184p_compare_ClockedEFS(shrink_ratio=0.25);

184p_compare_edgefaces(shrink_ratio=0.2);

