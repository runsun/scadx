include <../scadx.scad>

/*
   scadx_edgeface.scad (2018.3.25)
   
   Implant an edge-face data structure. Instead of just pts, faces for polyhedra,
   we use pts and edgeface, where edgeface is:
   
   [ edge1, [face1.1, face1.2], edge2, [face2.1, face2.2], ...]


   This data structure is invented here, don't know yet if it works 
   a long road ahead. 
*/
//Cylinder($fn=6, MarkPts=true);

//echo(rodfaces(4));
//Cube(MarkPts=true);
//Cylinder($fn=6, MarkPts=true);

//pts =[ [2,0,0], ORIGIN, [2,2,0], [2,0,3], [0,0,3], [2,2,3] ];
//polyhedron( points = pts, faces = rodfaces(3));
//MarkPts(pts);
//Frame(pts);

//function get_edgefaceHash(faces, _hash=[], _i=0)=
//(
//    _i>len(faces)-1? _hash
//    : let( face = faces[_i]
//         , edges= get_face_edges(face)
//         , _hash_new= [ for(eg=edges) 
//                        each haskey(_hash, [eg[1],eg[0]])? 
//                             [[eg[1],eg[0]], [hash(_hash,[eg[1],eg[0]]),face]]
//                            :[eg, face]
//                      ]
//         )
//    get_edgefaceHash(faces, _hash= update(_hash, _hash_new), _i=_i+1 )
//);
//
//    get_edgefaceHash=[ "get_edgefaceHash", "face", "array", "Face",
//     "Return the edges defined in a face
//     
//     get_face_edges(rodfaces(6)[2])= [[0, 1], [1, 7], [7, 6], [6, 0]]
//     "
//    ,""
//    ];
//
//    function get_edgefaceHash_test( mode=MODE, opt=[] )=
//    (   
//        let( rodfaces= rodfaces(3) )
//        doctest( get_edgefaceHash,
//        [ 
//          "var rodfaces"  //[[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
//        , [get_edgefaceHash(rodfaces)
//          , [ [2, 1], [[2, 1, 0], [1, 2, 5, 4]]
//            , [1, 0], [[2, 1, 0], [0, 1, 4, 3]]
//            , [0, 2], [[2, 1, 0], [2, 0, 3, 5]]
//            , [3, 4], [[3, 4, 5], [0, 1, 4, 3]]
//            , [4, 5], [[3, 4, 5], [1, 2, 5, 4]]
//            , [5, 3], [[3, 4, 5], [2, 0, 3, 5]]
//            , [1, 4], [[0, 1, 4, 3], [1, 2, 5, 4]]
//            , [3, 0], [[0, 1, 4, 3], [2, 0, 3, 5]]
//            , [2, 5], [[1, 2, 5, 4], [2, 0, 3, 5]]
//          ], "rodfaces[3]"]
//        //, [get_face_edges(rodfaces[0]), [[5,4],[4,3],[3,2],[2,1],[1,0],[0,5]], "rodfaces[0]"]
//        
//        ]
//        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces]
//        )
//    );

function re_align_a_pis(faces,_rtn=[],_L=0, _i=0)=
(  
   // rodfaces(3):
   // [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
   // Re-align the indices to:  
   // [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
   _i>=len(faces)?_rtn
   : re_align_a_pis( faces=faces
                       , _rtn= concat(_rtn, [range(_L, _L+len(faces[_i]))] )
                       , _L=_L+len(faces[_i])
                       , _i=_i+1
                       )
   
);

    re_align_a_pis=[ "re_align_a_pis", "faces", "array", "Face",
     "Re-arrange the first item of faces so its first index is 0
      For example, 
      
      faces= [[2, 1, 0], [3, 4, 5], ... ]
      re_align_a_pis(faces)
           = [[0, 2, 1], [3, 4, 5], ... ]
     "
    ,""
    ];

    function re_align_a_pis_test( mode=MODE, opt=[] )=
    (   
        let( faces= rodfaces(3)
           ) 
        doctest( re_align_a_pis,
        [ 
          "var faces" 
        , [ re_align_a_pis( faces)
          , [ [0, 1, 2], [3, 4, 5]
            , [6, 7, 8, 9], [10, 11, 12, 13], [14, 15, 16, 17]]
          , "faces", ["nl",true] 
          ]
          
        , str("rodfaces(4)=")
        , str("&nbsp;&nbsp;&nbsp;", rodfaces(4))  
        , [ re_align_a_pis( rodfaces(4))
          , [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11]
            , [12, 13, 14, 15], [16, 17, 18, 19], [20, 21, 22, 23]]
          
          , "rodfaces(4)", ["nl",true] 
          ]
        ]
        ,mode=mode, opt=opt, scope=["faces",faces]
        )
    );
    
    



function get_edges_from_faces(faces)=
(
  let( all_edges= [ for(f=faces) each get2(f) ]
     , uniq_edges = [ for(i=range(all_edges)) 
                      let(eg = all_edges[i]
                         ,ei = idx( all_edges, [eg[1],eg[0]] )
                         )
                      if(!(ei>=0 && ei<i)) eg 
                     ]
     )
     uniq_edges
);

    get_edges_from_faces=[ "get_edges_from_faces", "faces", "array", "Face",
     "Return the edges defined in faces, excluding reversed repeat. 
      For example, [1,2] and [2,1], only [1,2] (the first one) is included.
     "
    ,""
    ];

    function get_edges_from_faces_test( mode=MODE, opt=[] )=
    (   
        let( faces= rodfaces(3)
           , alledges= [for(f=faces) each get2(f)]) 
        doctest( get_edges_from_faces,
        [ 
          "var faces" 
        , "//All edges including reversed repeats:"
        , str(alledges)
        , "// Excluding reversed repeats:"
        , [ get_edges_from_faces( faces)
          ,  [[2, 1], [1, 0], [0, 2], [3, 4], [4, 5], [5, 3], [1, 4]
             , [3, 0], [2, 5]]
          , "faces", ["nl",true] 
          ]
        ]
        ,mode=mode, opt=opt, scope=["faces",faces, "alledges",alledges]
        )
    );
    

//function get_edgefi(faces,_hash=[],_i=0)=
//(  // An edgefi is:
//   // [ [2,3], ["L",3,"R",6] 
//   // , ...
//   // ]
//    _i>=len(faces)? _hash
//    : let( face = faces[_i]   // a face is an array of pt-indices
//         , edges= get2(face) 
//         , _new = [ for(eg=edges) 
//                    each [eg, [ haskey(_hash, [eg[1],eg[0]])?"L":"R"
//                              , _i] 
//                         ]
//                  ]              
//         )
//    ////get_edgeface(faces, _hash= update(_hash,_hash_new), _i=_i+1 )
//    get_edgefi(faces, _hash= edgefi_update(_hash,_new), _i=_i+1 )
//);
function get_edgefi(faces,_hash=[],_fi=0)=
(  // An edgefi is:
   // [ [2,3], ["L",3,"R",6,"F",[4], "B",[0,1]] 
   // , ...
   // ]
   // Means, for edge [2,3], 
   //   it's left face is faces[3]
   //   it's right face is faces[3]
   //   it's forward faces are faces[4]
   //   it's backward faces are faces[0,1]
   // 
    _fi<len(faces)? 
    let( face = faces[_fi]   // a face is an array of pt-indices
         , edges= get2(face) 
         , _new = [ for(eg=edges) 
                    each [eg, [ haskey(_hash, [eg[1],eg[0]])?"L":"R"
                              , _fi ] 
                         ]
                  ]              
         )
    ////get_edgeface(faces, _hash= update(_hash,_hash_new), _i=_i+1 )
    get_edgefi(faces, _hash= edgefi_update(_hash,_new), _fi=_fi+1 )
    
   // :_hash
    
    
    :// We have done adding the R/L faces
     // Now we add fowward and backward faces 
     let( pt_fis= get_a_fis_on_pts(faces) 
                 // pt_fis: indices of faces connect to a pt
                 // [ [2,0,1], [5,4,2] ...] = pts[0] has faces 2,0,1  
          )
       echo( "... get_edgefi: ", pt_fis=pt_fis )   
       [ each for( eg=keys(_hash))
         let( h_LRfis = hash(_hash, eg) // ["L",4,"R",5]
            , LRfis = [hash( h_LRfis,"L"), hash( h_LRfis,"R")] //vals(h_LRfis) // [4,5]
            , p1fis = pt_fis[ eg[1] ]
            , Ffis  = dels( p1fis, LRfis )
            , p0fis = pt_fis[ eg[0] ]
            , Bfis  = dels( p0fis, LRfis )
            )
         echo( "... get_edgefi: ", eg=eg, h_LRfis=h_LRfis, LRfis=LRfis, p1fis=p1fis, Ffis=Ffis, Bfis=Bfis )   
          [ eg, concat( h_LRfis
                          , ["F", Ffis,"B", Bfis]
                          )
              ] 
       ]
      
);

    get_edgefi=[ "get_edgefi", "faces", "array", "Face",
     "Return the edgefi data defined by faces.
     
      An edgefi is a hash defined as:
      
        {edge:{'R':Rfi, 'L':Lfi, 'F':[Ffi], 'B':[Bfi]}}
        
      where fi = index of face (in reference to faces)
            L,R,F,B = Left, Right, Forward and Backward, resp. 
      
      Like:
	  [ [2, 1], ['R', 0, 'L', 3, 'F', [2], 'B', [4]]
          , [1, 0], ['R', 0, 'L', 2, 'F', [4], 'B', [3]]
          , ...
          ]       
     "
    ,""
    ];
    function get_edgefi_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3)
           , conefaces = faces( shape= "cone", nseg=1) )
        doctest( get_edgeface,
        [ 
          "var rodfaces"  //[[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , [get_edgefi(rodfaces)
          ,[ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
           , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
           , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
           , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
           , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
           , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
           , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
           , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
           , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
           ]
            
          , "rodfaces", ["nl", true]
          ] 
        , ""
        , "var conefaces"
        , [get_edgefi(conefaces)
          ,[ [3, 2], ["R", 0, "L", 3, "F", [2], "B", [4]] 
           , [2, 1], ["R", 0, "L", 2, "F", [1], "B", [3]]
           , [1, 0], ["R", 0, "L", 1, "F", [4], "B", [2]]
           , [0, 3], ["R", 0, "L", 4, "F", [3], "B", [1]]
           , [1, 4], ["R", 1, "L", 2, "F", [3, 4], "B", [0]]
           , [4, 0], ["R", 1, "L", 4, "F", [0], "B", [2, 3]]
           , [2, 4], ["R", 2, "L", 3, "F", [1, 4], "B", [0]]
           , [3, 4], ["R", 3, "L", 4, "F", [1, 2], "B", [0]]
           ]
          , "conefaces", ["nl", true]
          ] 
            
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces, "conefaces", conefaces]
        )
    );


function get_edgeface(faces, _hash=[], _i=0)=
(
    _i>=len(faces)? _hash
    : let( face = faces[_i]   // a face is an array of pt-indices
         , edges= get2(face) 
         
         //// This works, too. But we decide to use the edgeface_update.
         //// Still keep this for future use for debug.
         ////, _hash_new= [ for(eg=edges)
                        //let(egrev = [eg[1],eg[0]]
                           //) 
                        //each haskey(_hash, egrev)? 
                             //[egrev, update(hash(_hash, egrev), ["L",[_i,face]]) ]
                            //:[eg, ["R",[_i,face]] ]
                      //]
         , _new = [ for(eg=edges) 
                    each [eg, [ haskey(_hash, [eg[1],eg[0]])?"L":"R"
                              , [_i, face]] 
                         ]
                  ]              
         )
    ////get_edgeface(faces, _hash= update(_hash,_hash_new), _i=_i+1 )
    get_edgeface(faces, _hash= edgeface_update(_hash,_new), _i=_i+1 )
);

    get_edgeface=[ "get_edgeface", "faces", "array", "Face",
     "Return the edgeface data defined by faces.
     
      An edgeface is a hash defined as:
      
        {edge:{'R'/'L': [fi,fpi]}}
        
      where fi = index of face (in reference to faces)
            fpi= indices of pts defining the face  
      
      Like:
	  [ [2, 1], ['R', [0, [2, 1, 0]], 'L', [3, [1, 2, 5, 4]]]
          , [1, 0], ['R', [0, [2, 1, 0]], 'L', [2, [0, 1, 4, 3]]]
          ...
          ]       
     "
    ,""
    ];
    function get_edgeface_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3) )
        doctest( get_edgeface,
        [ 
          "var rodfaces"  //[[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , [get_edgeface(rodfaces)
          , [ [2, 1], ["R", [0, [2, 1, 0]], "L", [3, [1, 2, 5, 4]]]
            , [1, 0], ["R", [0, [2, 1, 0]], "L", [2, [0, 1, 4, 3]]]
            , [0, 2], ["R", [0, [2, 1, 0]], "L", [4, [2, 0, 3, 5]]]
            , [3, 4], ["R", [1, [3, 4, 5]], "L", [2, [0, 1, 4, 3]]]
            , [4, 5], ["R", [1, [3, 4, 5]], "L", [3, [1, 2, 5, 4]]]
            , [5, 3], ["R", [1, [3, 4, 5]], "L", [4, [2, 0, 3, 5]]]
            , [1, 4], ["R", [2, [0, 1, 4, 3]], "L", [3, [1, 2, 5, 4]]]
            , [3, 0], ["R", [2, [0, 1, 4, 3]], "L", [4, [2, 0, 3, 5]]]
            , [2, 5], ["R", [3, [1, 2, 5, 4]], "L", [4, [2, 0, 3, 5]]]
            ]
            
          , "rodfaces[3]"
          ]           
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces]
        )
    );

function get_core_edgeface_for_shrunkPts(faces)=
(
   let( accum_i= accum( [ for(f=faces) len(f)] )
      ) 
   //accum_i   
   [ for(fi= [0:len(faces)-1])
      [for(i= [0:len(faces[fi])-1]) faces[fi][i]+ accum_i[fi]-len(faces[0]) ]
   ]
);
//echo(rodfaces3=rodfaces(4));
//echo(rodfaces3sh= get_core_edgeface_for_shrunkPts(rodfaces(4)));


function _re_arrange_a_edges_on_pi_rev_last(a_edges_on_pi_rev_last,_rtn=[],_i=0)=
(
    // Internal func for clock_wize_fis_on_pi()
    
    // a_edges_on_pi_rev_last=
    //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
    //  , [0, [1,2],[2,4],[4,5],[1,5] ]
    //  , [1, [1,0],[0,3],[3,2],[1,2] ]
    //  ]  
    // is re-arranged to:      
    //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
    //  , [1 [1,0],[0,3],[3,2],[1,2] ]
    //  , [0 [1,2],[2,4],[4,5],[1,5] ]
    //  ]  
    // such that they are in order in terms of edge continuatility :
          
    //  [ [2, [1,5],...,[1,0] ]
    //  , [1, [1,0],...,[1,2] ]
    //  , [0 [1,2],...,[1,5] ]
    //  ]
     
    //echo(_i=_i)
    //echo(a_edges_on_pi_rev_last=a_edges_on_pi_rev_last)  
   _i>=len(a_edges_on_pi_rev_last)-1? _rtn
   :let( _rtn=_i==0?[a_edges_on_pi_rev_last[0]]:_rtn
       // _rtn=_i==0?[a_edges_on_pi_rev_last[0]]:_rtn
       , prevlast= last(last(_rtn)) 
        , _rtn2= concat( _rtn
                       , [ for(f=a_edges_on_pi_rev_last)
                           //echo(f=f)
                           if(f[1]==prevlast)f 
                           ]
                       )      
        )
    //echo(_rtn=_rtn)     
    //echo(_rtn2=_rtn2)     
    //echo(prevlast=prevlast)    
    _re_arrange_a_edges_on_pi_rev_last(a_edges_on_pi_rev_last
                                       ,_rtn=_rtn2,_i=_i+1)           
          
);          
          

function clock_wize_fis_on_pi( faces      // = a_fis
                             , pi         // i of one pt 
                             , fis_on_pi  // [2,0,1]
                             )=
(
    /*
                  4 
              _.-'`'-._ 
         _.-'`         '-._
       2'._       fi=0     '- 5
        |  `-._          _.-'\   
        |       '-._   _-'    \
        |   fi=1    1'         \ 
       3-._         |   fi=2   _\            
           `-._     |      _.-'  6
               `-._ | _.-'  
                   0 `
    
    Let fis_of_pi = [0,1,2]  (it could be any order)
    
    clock_wize_fis_on_pi( faces, 1, [0,1,2] ) => [0,2,1]
     
    For fi_0 to be on the next of fi_1:
      [2,1] of fi_1 = reversed edge [1,2] of fi_0 
      
    edges:
      fi=0: [ [1,2],[2,4],[4,5],[5,1] ]
      fi=1: [ [1,0],[0,3],[3,2],[2,1] ]
      fi=2: [ [1,5],[5,6],[6,0],[0,1] ]
          
   */ 
   let( firng= range(fis_on_pi)
      //, fis_on_pi = 
      , a_pis_on_pi= [ for(fi=fis_on_pi) faces[fi] ] 
          // fis_on_pi= [2,0,1]
          // a_pis_on_pi = [ [6,0,1,5],[1,2,4,5],[3,2,1,0]]
        //----------------------------------------                                                                                           
      , a_edges_on_pi = [ for(pis=a_pis_on_pi) get2(pis) ]
          // a_edges_on_pi=
          //  [ [ [6,0],[0,1],[1,5],[5,6] ]
          //  , [ [1,2],[2,4],[4,5],[5,1] ]
          //  , [ [3,2],[2,1],[1,0],[0,3] ]
          //  ]
        //----------------------------------------                                                                                           
      , a_edges_on_pi_rolled = 
          [ for(egs=a_edges_on_pi)
             let(i= idx( [for(eg=egs)eg[0]], pi)) // i= index of edge where
                i>0? roll(egs, -i): egs            // the pi is the first i
          ]                                        // like [2,3] for pi=2
                                                  // [5,4] for pi=5
          // a_edges_on_pi_rolled=
          //  [ [ [1,5],[5,6],[6,0],[0,1] ]
          //  , [ [1,2],[2,4],[4,5],[5,1] ]
          //  , [ [1,0],[0,3],[3,2],[2,1] ]
          //  ]     
        //----------------------------------------                                                                                           
      , a_edges_on_pi_rev_last= 
         [ for(fi=firng)
            let( egs=a_edges_on_pi_rolled[fi])
           [ fis_on_pi[fi]
           , for(eg=egs)
             eg==last(egs)? [eg[1],eg[0]]: eg 
           ]
        ]  
          // a_edges_on_pi_rev_last=
          //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
          //  , [0, [1,2],[2,4],[4,5],[1,5] ]
          //  , [1, [1,0],[0,3],[3,2],[1,2] ]
          //  ]   
          // The 2,0,1 above is the fi
        //----------------------------------------                                                                                           
       , re_arranged = 
          _re_arrange_a_edges_on_pi_rev_last(a_edges_on_pi_rev_last)
          // re_arranged=
          //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
          //  , [1, [1,0],[0,3],[3,2],[1,2] ]
          //  , [0, [1,2],[2,4],[4,5],[1,5] ]
          //  ]   
        //----------------------------------------                                                                                           
       )
    //echo(faces=faces)   
    //echo(pi=pi)
    //echo(fis_on_pi=fis_on_pi)
    //echo(a_pis_on_pi=a_pis_on_pi)   
    //echo(a_edges_on_pi=a_edges_on_pi)   
    //echo(a_edges_on_pi_rolled=a_edges_on_pi_rolled)   
    //echo(a_edges_on_pi_rev_last=a_edges_on_pi_rev_last)   
    //echo("-------") 
    [for(x=re_arranged)x[0]]
   
);
     clock_wize_fis_on_pi=[ "clock_wize_fis_on_pi"
                           , "faces,pi,fis_of_pi", "array", "Face",
     "
      fis_of_pi: indices of faces surrounding pi, like [2,0,3]
     
      This func makes sure that the order of those faces are clock-wize
      
      "
    ,""
    ];
    function clock_wize_fis_on_pi_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3)
           , fis_on_1 = get_a_fis_on_pts(rodfaces)[1]
           , cw_fon1= clock_wize_fis_on_pi( rodfaces,1, fis_on_1 )
            )
        doctest( clock_wize_fis_on_pi,
        [ 
          "var rodfaces"  
          /// [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , "pi=1"
        , "var fis_on_1"  /// [0, 2, 3]
        , str( "edges_on_1= ")
          // [ [[2, 1], [1, 0], [0, 2]]
          // , [[0, 1], [1, 4], [4, 3], [3, 0]]
          // , [[1, 2], [2, 5], [5, 4], [4, 1]]
          // ]
        , str( [for(fi=fis_on_1) get2(rodfaces[fi])] )
        , ""
        
        , [ cw_fon1, [0, 3, 2], "rodfaces,1, fis_on_1"] 
        , str( "clock-wized edges_on_1=")
        , str( [for(fi=cw_fon1) get2(rodfaces[fi])] )
          // [ [[2, 1], [1, 0], [0, 2]]
          // , [[1, 2], [2, 5], [5, 4], [4, 1]]
          // , [[0, 1], [1, 4], [4, 3], [3, 0]]
          // ]  
        , "// Note that the 1st and 2nd face (faces[0],faces[3]) share same edge [1,2]"
        , "// Note that the 2nd and 3rd face (faces[3],faces[2]) share same edge [1,4]"
        , "// Note that the 3rd and 1st face (faces[2],faces[0]) share same edge [1,0]"
           
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces, "fis_on_1", fis_on_1]
        )
    ); 


function get_a_fis_on_pts(faces)=
(  
  let( pts_i_max = max( flatten(faces) ) // pt index max
     , frng = [1:len(faces)-1]
     , _rtn = [ for(pti=[0:pts_i_max])
                [ for(fi=[0:len(faces)-1])
                  if(has(faces[fi],pti)) fi 
                ]
              ]
             // Pts[0] neighbors 3 faces: faces[0], faces[2], faces[4]
             // [[0, 2, 4], [0, 2, 3], [0, 3, 4], [1, 2, 4], [1, 2, 3], [1, 3, 4]]
             
     , rtn = [ for(pi=range(_rtn))
              clock_wize_fis_on_pi ( faces,pi,_rtn[pi] )
              // This makes them clock-wize. 
              // i.e., faces[0]-faces[2]-faces[4] is clock-wize for pts[0]
              // i.e., faces[0]-faces[3]-faces[2] is clock-wize for pts[1]
              // [[0, 2, 4], [0, 3, 2], [0, 4, 3], [1, 4, 2], [1, 2, 3], [1, 3, 4]] 
             ]        
     )
     rtn
);
    get_a_fis_on_pts=[ "get_a_fis_on_pts", "faces", "array", "Face",
     "Return an array of pt's face indices
      --- faces that connect to a pt.
     
      [ [2,0,1], [5,4,2] ...]
      
      means: 
          faces 2,0,1 are connected to pts[0] (3 faces)
          faces 5,4,2 are connected to pts[1] (3 faces)
     "
    ,""
    ];
    function get_a_fis_on_pts_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3) )
        doctest( get_a_fis_on_pts,
        [ 
          "var rodfaces"  
          /// [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , [ get_a_fis_on_pts( rodfaces)
        
          //,[ [0, 2, 4], [0, 2, 3], [0, 3, 4]
          // , [1, 2, 4], [1, 2, 3], [1, 3, 4]]
          , [ [0, 2, 4], [0, 3, 2], [0, 4, 3]
            , [1, 4, 2], [1, 2, 3], [1, 3, 4]]   
     
          , "rodfaces"
          ]           
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces]
        )
    ); 

function get_h_fipis_all_faces(a_pis)=
(
   [ for(fi=[0:len(a_pis)-1]) [fi, a_pis[fi] ] ]
     
   // a_pis   = [[2,1,0],[3,4,5],[0,1,4,3],[ 1, 2, 5, 4],[ 2, 0, 3, 5]] 
   // a_subpis= [[0,1,2],[3,4,5],[6,7,8,9],[10,11,12,13],[14,15,16,17]]
   
   // Get h_fipis for all faces: 
   
   // get_h_fipis_all_faces( a_pis ) =
   //   [ [0,[2,1,0]], [1,[3,4,5]], [2,[0,1,4,3]]
   //   , [3,[1,2,5,4]], [4,[2,0,3,5]] ]
);

function get_h_fisubpis_all_faces(a_pis)=
(
   let( a_subpis= re_align_a_pis(a_pis) )
   [ for(fi=[0:len(a_pis)-1]) [fi, a_subpis[fi] ] ]
     
   // a_pis   = [[2,1,0],[3,4,5],[0,1,4,3],[ 1, 2, 5, 4],[ 2, 0, 3, 5]] 
   // a_subpis= [[0,1,2],[3,4,5],[6,7,8,9],[10,11,12,13],[14,15,16,17]]
   
   // Get h_fisubpis for all faces: 
   
   // get_h_fisubpis_all_faces( a_pis ) =
   //   [ [0,[0,1,2]], [1,[3,4,5]], [2,[6,7,8,9]]
   //   , [3,[10,11,12,13]], [4,[14,15,16,17]] ]
);

function get_h_fisubpis_by_pis(a_pis, pis)=
(
   get_h_fisubpis_all_faces(a_pis)[ idx( a_pis, pis) ]
   //let(fi=idx(a_pis, pis) )
   //[ fi, re_align_a_pis(a_pis)[ fi ] ] 
   // a_pis   = [[2,1,0],[3,4,5],[0,1,4,3],[ 1, 2, 5, 4],[ 2, 0, 3, 5]] 
   // a_subpis= [[0,1,2],[3,4,5],[6,7,8,9],[10,11,12,13],[14,15,16,17]]
   
   // Given a face(=pis), return its corresponding subface (=subpis)
   // in a hash {fi:pis}
   
   // get_h_fisubpis_by_pis( a_pis, [0,1,4,3] ) = [ 2, [6,7,8,9] ]    
);

function get_a_h_fisubpis_on_pi(a_pis, pi)=
(
   let( fis_w_pi= [for(pis=a_pis) if(has(pis,pi)) pis ] )
   get( get_a_fisubpis(a_pis), fis_w_pi)
   //let(fi=idx(a_pis, pis) )
   //[ fi, re_align_a_pis(a_pis)[ fi ] ] 
   
   // a_pis   = [[2,1,0],[3,4,5],[0,1,4,3],[ 1, 2, 5, 4],[ 2, 0, 3, 5]] 
   // a_subpis= [[0,1,2],[3,4,5],[6,7,8,9],[10,11,12,13],[14,15,16,17]]
   // get_h_fisubpis_all_faces( a_pis ) = 
   //   [ [0,[0,1,2]], [1,[3,4,5]], [2,[6,7,8,9]]
   //   , [3,[10,11,12,13]], [4,[14,15,16,17]] ]
   
   // Given a face(=pis), and pt indix (pi), return pi's corresponding 
   // subfaces (=a_subpis)
   // in an array of hashes {fi:pis}
   
   // get_a_h_fisubpis_on_pi( a_pis, 2 ) =
   //     [ [0,[0,1,2]], [3,[10,11,12,13]], [4,[14,15,16,17]] ]  
   //  
);



function get_subpis_on_pi(a_pis, pi, subfi=undef)=
(
   subfi==undef ?
   let( //fisubpis_by_pis= [ for(pis=a_pis) get_fisubpis_by_pis(a_pis, pis) ]
        // subpis_by_pis =
        //   [ [0,[0,1,2]], [1,[3,4,5]], [2,[6,7,8,9]]
        //   , [3,[10,11,12,13]], [4,[14,15,16,17]] ]
        a_subpis = re_align_a_pis(a_pis)
      , cw_fis_on_pi = get_a_fis_on_pts(a_pis)[pi] // [2,0,1] ( already clock-wized)
      )
    //cw_fis_on_pi
    [ for(fi=cw_fis_on_pi) a_subpis[fi][ idx( a_pis[fi], pi) ] ] 
    
   :let( a_subpis = re_align_a_pis(a_pis)
         // a_pis   = [[2,1,0],[3,4,5],[0,1,4,3],[ 1, 2, 5, 4],[ 2, 0, 3, 5]] 
         // a_subpis= [[0,1,2],[3,4,5],[6,7,8,9],[10,11,12,13],[14,15,16,17]]
      , fi = subfi 
      , pis = a_pis[ fi ]
      , pi_th = idx( pis, pi )
      )
   a_subpis[ subfi ][ pi_th ]
);

    /*                i6 
                  _.-'`'-._ C
             _.-'` _.-9-._ '-._
           i7._  8:_  c   '-._ '-._ 
            :  `-._ `-._     10  _.:\i5   
           |  13._ '-._ '7-'` _-'    \
           :  :   `-._ `-._.-'    .5  \ 
        D |  |  d   11  /i1    _-'  \  \  B           
          :  :     /   /   \  4      \  \
         |  |    /   /  2   \  \   b  \  \
         :  :  /   /   / \   \  \    _-6  \
        |  | /   /   /    \   \  \-'     _-'i4
        : 12   /   /   a   \   \ 3   _-'
       |     /   /    __.--0    \_-'   
       :   /   1'--''`  __...--'i0    
      |  /    ____...-''
      i3.--''`     A     
      
      a_pis    = [ [i0,i3,i1], [i0,i1,i5,i4], [i1,i7,i6,i5], [i1,i3,i7] ]
      a_subpis = [a,b,c,d]
               = [ [0,1,2],    [3,4,5,6],     [7,8,9,10],    [11,12,13] ]
                
      Let subfis = [0,1,2,3] = subfaces a,b,c,d
      
      get_subpis_on_pi(a_pis, pi(a_pis, i1, 1) = 4
      get_subpis_on_pi(a_pis, pi(a_pis, i1, 2) = 7
      get_subpis_on_pi(a_pis, pi(a_pis, i1, 3) = 11
      get_subpis_on_pi(a_pis, pi(a_pis, i1, 0) = 2
      get_subpis_on_pi(a_pis, pi(a_pis, i1) = [2,4,7,11]
      
      
      
      
    */
    get_subpis_on_pi=[ "get_subpis_on_pi(a_pis, pi", "a_pis,pi,[subfi]", "array", "Face",
     "
      Given pi, return the corresponding subpi on subfaces[subfi]
      on a data of a_pis
      
      a_pis: array of face indices (= commonly called faces in OpenSCAD)  
        
      pis: old faces indices
        
      [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
      
      subfis: the new pis after DooSabin subdivision process that 
              separates all the faces so there's no co-edge. They can
              be obtained: re_align_a_pis( pis): 
      
      [ [0, 1, 2], [3, 4, 5], [6, 7, 8, 9], [10, 11, 12, 13]
      , [14, 15, 16, 17]]
       
      Note: pis and subpis have one-to-one relationship AND
         the pis on both faces are arrange in the same order.
      
      So if we take a pt, pts[3], the pis containing pi=3 are:
      [3,4,5], [0,1,4,3] and [2,0,3,5], which is fi= 1,2,4 
      
      Which means, subfaces 1,2,4 containing the subpts of pt:
      
       [3, 4, 5], [6, 7, 8, 9], [14, 15, 16, 17]
      
      They are on the same i_th of pt on their respective faces
      as the i_th the pt is on face 1,2,4.
      
         on the a_fis[1]: subpts[3]
         on the a_fis[2]: subpts[9]
         on the a_fis[4]: subpts[16]
     "
    ,""
    ];
    function get_subpis_on_pi_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3) )
        doctest( get_subpis_on_pi,
        [ 
          "var rodfaces"  
          // rodfaces= a_pis= [[2,1,0],[3,4,5],[0,1,4,3],[ 1, 2, 5, 4],[ 2, 0, 3, 5]] 
          // a_subpis       = [[0,1,2],[3,4,5],[6,7,8,9],[10,11,12,13],[14,15,16,17]]
        , [ get_subpis_on_pi( rodfaces,3 ), [3, 16, 9], "rodfaces,3"]           
        , [ get_subpis_on_pi( rodfaces,2 ), [0, 14, 11], "rodfaces,2"]           
        , [ get_subpis_on_pi( rodfaces,3,2 ), 9, "rodfaces,3,2"]           
        , [ get_subpis_on_pi( rodfaces,2,4 ), 14, "rodfaces,2,4"]           
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces]
        )
    );   
    
    

function ef2faces(ef)=
(
   let(fif = [ for(rl = vals(ef)) each each vals(rl) ] //// [2, [2, 1, 0],1, [3, 4, 5], ...] 
      ,fis = sort(uniq(keys(fif)))
      //,iface=  hashex(fif, keys=fis)
      // iface: [0, [2, 1, 0], 1, [3, 4, 5], 2, [0, 1, 4, 3], 3, [1, 2, 5, 4], 4, [2, 0, 3, 5]]
      )
   [ for(fi=fis) hash(fif,fi) ]   
   //fis
   //fif
);    
 
    ef2faces=[ "ef2faces", "ef", "array", "Face",
     "Return faces from ef (the edgeface data
     "
    ,""
    ];
    function ef2faces_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3) )
        doctest( ef2faces,
        [ 
          "var rodfaces"  
          /// [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , [ ef2faces( get_edgeface(rodfaces) )
          , [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]            
          , "get_edgeface(rodfaces)"
          ]           
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces]
        )
    );    
    
function edgefi_update(ef1, ef2, _i=0)=
(
   _i> len(ef2)-2? ef1
   : let( eg2= ef2[_i]   // current edge by index, like [2,3]
        , f2s= ef2[_i+1] // current face hash, like 
                         //  [ "L",3, "R", 6 ]
                         //  3: left face index (in faces) = faces[3]
                         // An ef is:
                         // [ [2,3], ["L",3,"R",6] 
                         // , ...
                         // ]
        , eg2rev = [ eg2[1], eg2[0] ]                 
        , ef1= haskey(ef1, eg2) // eg2 found in ef1, so we need to replace
                                // [ "L",3, "R",6 ] of ef1.eg2
                                // with f2s (= ef2.eg2)
               ? update( ef1
                       , [ eg2 
                         , update( hash(ef1, eg2), f2s )
                         ]
                       )
               :haskey(ef1, eg2rev )?
                 update( ef1
                       , [ eg2rev
                         , update( hash(ef1, eg2rev), f2s )
                         ]
                       )
               : concat( ef1, [eg2, f2s] )
        )
      edgefi_update( ef1, ef2, _i=_i+2)
);    

function edgeface_update(ef1, ef2, _i=0)=
(
   _i> len(ef2)-2? ef1
   : let( eg2= ef2[_i]   // current edge by index, like [2,3]
        , f2s= ef2[_i+1] // current face hash, like 
                         //  [ "L",[3, [5,4,3]], "R", [6, [3,4,7]] ]
                         //  3: left face index (in faces) = faces[3]
                         //  [5,4,3]: left face containing pts indices:[ pts[5], pts[4], pts[3] ]
                         // An ef is:
                         // [ [2,3]
                         // , [ "L",[3, [5,4,3]], "R", [6, [3,4,7]] ] 
                         // , ...
                         // ]
        , eg2rev = [ eg2[1], eg2[0] ]                 
        , ef1= haskey(ef1, eg2) // eg2 found in ef1, so we need to replace
                                // [ "L",[3, [5,4,3]], "R", [6, [3,4,7]] ] of ef1.eg2
                                // with f2s (= ef2.eg2)
               ? update( ef1
                       , [ eg2 
                         , update( hash(ef1, eg2), f2s )
                         ]
                       )
               :haskey(ef1, eg2rev )?
                 update( ef1
                       , [ eg2rev
                         , update( hash(ef1, eg2rev), f2s )
                         ]
                       )
               : concat( ef1, [eg2, f2s] )
        )
      edgeface_update( ef1, ef2, _i=_i+2)
);    

    edgeface_update=[ "edgeface_update", "face", "array", "Face",
     "Return the edges defined in a face
     
     "
    ,""
    ];

    function edgeface_update_test( mode=MODE, opt=[] )=
    (   
        let( ef= [ [0,1],["L",[2, [0,1,2]], "R",[5, [2,1,0,5]]] ] 
           , ef2=[ [0,1],["L",[2, [0,1,2]]] ]
           )
        doctest( edgeface_update,
        [ 
          "var ef" 
        , [ edgeface_update( ef, [[1,2],["L",[5,[1,2,6,7]],"R",[8, [5,3,2,1]]]])
          ,[ [0, 1], ["L", [2, [0, 1, 2]],    "R", [5, [2, 1, 0, 5]]]
           , [1, 2], ["L", [5, [1, 2, 6, 7]], "R", [8, [5, 3, 2, 1]]]
           ]
          , "ef, [[1,2],['L',[5,[1,2,6,7]],'R',[8, [5,3,2,1]]]]" 
          ]
         
        , ""  
        , "var ef2"   
        , [ edgeface_update( ef2, [[1,0],[ "L", [2, [2,1,0,5]] ]])
           , [ [0, 1], ["L", [2, [2, 1, 0, 5]]] ]
           , "ef, [[1,0],[ 'L', [2, [2,1,0,5]] ]]" ]
           
        , [ edgeface_update( ef2, [[1,0],["R",[3, [2,1,0,5]]]])
           , [[0, 1], ["L", [2, [0, 1, 2]], "R", [3, [2, 1, 0, 5]]]]
           , "ef, [[1,0],['R',[3, [2,1,0,5]]]]" ]
        ]
        ,mode=mode, opt=opt, scope=["ef", ef, "ef2", ef2]
        )
    );


function relist_face_indices(faces, _rtn=[], _previ=0, _f=0)=
(
    _f>=len(faces)? _rtn
    : relist_face_indices( faces 
                  , concat(_rtn,[[for(x=[_previ: _previ+len(faces[_f])-1])x ]])
                  , _previ+ len(faces[_f])
                  , _f+1
                  )
);

     relist_face_indices=[ "relist_face_indices", "faces", "array", "Face",
     " Used in shrinking faces / subdividing surfaces.
       
       When a face went through the first step of DooSabin subdiv,
       it generate a smaller face that is on the plane of the original
       face. 
       
       We put together all the new smaller faces and regenerate faces list.
       
       It's called 'core-faces' cos it only represents the faces coming
       directly from the old faces, but not ALL new faces
     
     "
    ,""
    ];

    function relist_face_indices_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3) )
        doctest( relist_face_indices,
        [ 
          "var rodfaces"  //[[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , [ relist_face_indices(rodfaces)
           , [[0, 1, 2], [3, 4, 5], [6, 7, 8, 9], [10, 11, 12, 13], [14, 15, 16, 17]]
          , "rodfaces[3]"]
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces]
        )
    );   


function get_ptFaceList(faces, rtn=[], _max=0, _i=0)=
(
  let( _max = _i==0? max( [for(f=faces) each f] )
                   : _max
     , rtn = concat( rtn, [ [for(f=faces) if(has(f,_i)) f] ]) 
     )
     _i==_max? rtn
             : get_ptFaceList(faces, rtn, _max, _i+1)
);   

     get_ptFaceList=[ "get_ptFaceList", "faces", "array", "Face",
     "Return a list containing faces neighboring to a pt indexed 
       by the index of this list
     
      For example, ptFaceList = [ [ [0,1,2], [0,3,4] ]
                                , [ [1,2,5,7], [4,1,6,8] ]
                                , ...
                                ]
                                
       means: pts[0] has faces [ [0,1,2], [0,3,4] ]
              pts[1] has faces [ [1,2,5,7], [4,1,6,8] ]
                                
     "
    ,""
    ];

    function get_ptFaceList_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3) )
        doctest( get_ptFaceList,
        [ 
          "var rodfaces"  //[[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , [get_ptFaceList(rodfaces)
          , [ [[2, 1, 0], [0, 1, 4, 3], [2, 0, 3, 5]]
            , [[2, 1, 0], [0, 1, 4, 3], [1, 2, 5, 4]]
            , [[2, 1, 0], [1, 2, 5, 4], [2, 0, 3, 5]]
            , [[3, 4, 5], [0, 1, 4, 3], [2, 0, 3, 5]]
            , [[3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4]]
            , [[3, 4, 5], [1, 2, 5, 4], [2, 0, 3, 5]] ]
          , "rodfaces[3]"]
        //, [get_face_edges(rodfaces[0]), [[5,4],[4,3],[3,2],[2,1],[1,0],[0,5]], "rodfaces[0]"]
        
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces]
        )
    );
    
    

 function shrinkPts(pts, ratio=0.15)=
(
   /*
        .---------.
        | .-----.  \
        | |      \  \
        | |       \  \
        | |        \  \
        | '--._ _-'  _- 
        '--._  `  _-'  
             `'-.' 
       P2               P3
        +----------------+ ---
        |                |  | ----> ratio/2
        |  S2-------S3   | ---
        |   |        |   |
        |   |        |   |
        |   |        |   |
        |  S1-------S0   |---
        |                | | ----> ratio/2
        +---*--------*---+---      
       P1            ^   P0
                     |
                     +------------- node
                          
   --- Following DooSabin approach for subdivision:                       
       Take edge P0-P1, divide it according to ratio
   --- Pts don't need to be co-planar
   */      
   DooSabin_face_shrink(pts,ratio)
);
   
function get_DooSabin_surface_divide_Pts(pts, edgefaces, cycle=1)=
( /*
    
      For each face, we want to "shrink the points", then connect
      them with the "shrunk points" of the next face.   
     
                _.-'`'-._
           _.-'` _.-'-._ '-._
          |._  ':_      '-._ '-._ 
          :  `-._ `-._     _'  _.:\P2   
         |  |-._ '-._ 'T1'` _-'    \
         :  :   `-._ `-._.-'    Q2  \ 
        |  |       S1   P1   _-'  \  \           
        :  :     /   /   \  \Q1    \  \
       |  |    /   /  R2  \  \      \  \
       :  :  /   /   / \   \  \    _Q3  \
      |  | /   /   /    \   \  \-'    _--'P3
      :  /   /   /       \   \  Q0 _-'
     |     /   /    __.--R0   \_-'    F0
     :   /  R1'--''`  __...--'P0    
    |  /    ____...-''
    :/.--''`     F1
     
     
    For example, we want to form new faces like [Q0,Q1,Q2,Q3] and [R0,R1,R2],
    and form new edges, Q0-R0 and Q1-R2, to create new face(s) [Q0,R0,R2,Q1]
    
    Note that:
    
    1) Each edge is splitted into two: P0-P1=> Q0-Q1 and R0-R2
    2) each new edge has one of the shrunken face + one of the new faces
       Q0-Q1 => [Q0,Q1,Q2,Q3] and [Q0,R0,R2,Q1] 
     
     
 */
  3

);

function DooSabin_face_shrink(pts,ratio=0.15)=
( /* Check out dev_edgeface.scad to see how this 
      formula is derived
   */
   let( rng=[0:len(pts)-1]
      , p2 = get2(pts)
      , nodepairs=[ for(i=rng) 
                    onlinePts( p2[i], ratios=[ratio, 1-ratio])
                    // This works, too, but hard to read
                    // [ (1-ratio)*p2[i][0]+ratio*p2[i][1]
                    //, ratio*p2[i][0]+(1-ratio)*p2[i][1]
                    //]
                  ]
      )
      [ for(i=rng)
         midPt( lineBridge( [nodepairs[i][0], get(nodepairs,i-2)[1]]
                          , [get(nodepairs,i-1)[1], get(nodepairs,i+1)[0]]
                          )
              ) 
      ]
);

