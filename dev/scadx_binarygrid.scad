include <scadx.scad>
include <doctest.scad>

/*
 bingrid
 
 bingrid is a data type attempting to represent a binary grid structure
 (each cell contains either 1 or 0).

 A bg below:
 
       1 0 1
       0 1 0
       1 0 0
       
 is turned into [5,2,4] :
 
       1 0 1  = 2^2+1 = 5
       0 1 0  = 2^1   = 2
       1 0 0  = 2^2   = 4


 Definition:
 
  source: [5,2,4]
  bgrid :  [ 1 0 1
           ,   1 0
           , 1 0 0
           ]
  sbgrid : [ "1 0 1"
           , "0 1 0"
           , "1 0 0"
           ]
  
  site :  [x,y]
  
  block:  [site, size] = [[x,y],[m,n]]
  
  
    
*/


function bgrid(src=[],r/*row*/)= /// [5,3] => [101,11]
(
   src==[]?
      //echo("in bgrid", rtn=repeat( [0], r==undef?3:r))   
      repeat( [0], r==undef?2:r) 
   :let( _bg= [for(n=src) tobin(n) ] /// [5,10,2]
      , _c = max( [for(n=src) log(2,n)] )
      , _r = len(src)
      , rtn = _r<r ? concat(_bg, repeat([0],r-_r)): _bg
      ) 
   //echo("in bgrid", rtn=rtn)   
   rtn
);

   bgrid=["bgrid","src,r", "arr", "Bgrid"
   ,"Convert *src* to bgrid where *src* = int arr like [5,4,1]
   ;; and the bgrid is its binary form: [101,100,1]
   ;;
   ;; src: [5,4,1]
   ;; r : min row count, default=2
   ;;
   ",""];
   function bgrid_test(mode=MODE,opt=[])=
   (
      doctest( bgrid,
      [
        [ bgrid(),[0,0],"" ]
      , [ bgrid(r=4),[0,0,0,0],"r=4" ]
      , [ bgrid([2,3]),[10,11],[2,3] ]
      , [ bgrid([2,3],r=3),[10,11,0],"[2,3],r=3" ]
      , [ bgrid([5,4,1,3]),[101, 100, 1, 11],[5,4,1,3] ]
      ]
      ,mode=mode
      ,opt= opt 
      ,scope=[]
      )
   );
   //echo(bgrid_test(mode=12)[1]);



function sbgrid(src=[],r,c)=
(
   let( bg = [ for(x=bgrid(src=src,c=c,r=r)) str(x) ]
      , w = max( max( [for(x=bg) len(x) ] ), c==undef?0:c)
      , ww= _s("w={_};p=0",w)
      , rtn= [ for(b=bg) trim(replaces(_f( b, ww),["1","X ","0","0 "])) ]
      ) 
   //echo(src=src, bg=bg, w=w, ww=ww, rtn=rtn)   
   rtn 
);

   sbgrid=["sbgrid","src,r,c", "arr", "Bgrid"
   ,"Convert *src* to sbgrid where *src* = int arr like [5,4,1]
   ;; and the bgrid is its binary form: [101,100,1]
   ;;
   ;; src: [5,4,1]
   ;; c : min column count, default=3
   ;; r : min row count, default=undef
   ;;
   ",""];
   function sbgrid_test(mode=MODE,opt=[])=
   (
      doctest( sbgrid,
      [
        [ sbgrid(),["0", "0"],"" ]
      , [ sbgrid(r=4),["0", "0", "0", "0"],"r=4" ]
      , [ sbgrid([2,3]),["X 0", "X X"],[2,3] ]
      , [ sbgrid([2,3],r=3), ["X 0", "X X", "0 0"],"[2,3],r=3" ]
      , [ sbgrid([2,3],c=3), ["0 X 0", "0 X X"],"[2,3],c=3" ]
      , [ sbgrid([5,4,1,3]),["X 0 X", "X 0 0", "0 0 X", "0 X X"],[5,4,1,3] ]      ]
      ,mode=mode
      ,opt= opt 
      ,scope=[]
      )
   );
   //echo(sbgrid_test(mode=12)[1]);

//echo(_formula([_vector(sbgrid([5,4,1,3] ))]));

function bg_getbysite(sbgrid, site)=  // site: [x,y], return: "X" or "0"
(
   let( h= len(sbgrid)
      , w= len(sbgrid[0])
      , rtn = sbgrid[h-site.y-1][site.x*2][0]
      )
   //echo(h=h,w=w, h-site.y, sbgrid, sbgrid[h-site.y])   
   rtn
);
   bg_getbysite=["bg_getbysite","src,r,c", "arr", "Bgrid"
   ,"Convert *src* to sbgrid where *src* = int arr like [5,4,1]
   ;; and the bgrid is its binary form: [101,100,1]
   ;;
   ;; src: [5,4,1]
   ;; c : min column count, default=3
   ;; r : min row count, default=undef
   ;;
   ",""];
   function bg_getbysite_test(mode=MODE,opt=[])=
   (
      //echo(_formula([_vector(sbgrid([5,4,1,3] ))]))
      doctest( bg_getbysite,
      [
        [ sbgrid([5,4,1,3]),["X 0 X", "X 0 0", "0 0 X", "0 X X"],[5,4,1,3] ]
         /*
            X 0 X  
            X 0 0  
            0 0 X  
            0 X X  
        */
      , [ bg_getbysite(sbgrid([5,4,1,3]),[1,0]),"X","sbgrid([5,4,1,3]),[1,0]" ]
      , [ bg_getbysite(sbgrid([5,4,1,3]),[0,2]),"X","sbgrid([5,4,1,3]),[0,2]" ]
      , [ bg_getbysite(sbgrid([5,4,1,3]),[2,1]),"X","sbgrid([5,4,1,3]),[2,1]" ]
      , [ bg_getbysite(sbgrid([5,4,1,3]),[2,2]),"0","sbgrid([5,4,1,3]),[2,2]" ]
      ]
      ,mode=mode
      ,opt= opt 
      ,scope=[]
      )
   );
   echo(bg_getbysite_test(mode=12)[1]);





module _bgrid(src=[], c,r)
{
  //echo( str(_BR, _vector( bgrid(arr,w)) ));
  sbg = sbgrid(src,c,r);
  //echo(sbg=sbg);
  w = max( [ for(r=sbg) len(r)] );
  opt= _s("w={_}",w+1);
  _formula([_BR, _vector( sbgrid(src,c,r), opt)]);
}   


function bgrid_from_block( bgridsize // [x,y]
                         , block     // [site, size] = [[x,y],[m,n]]
                         )=
(
   3

);
//echo(log(2,8));
//echo( repeat("0 ",5) );

//_bgrid([5,10,16]);
//_bgrid([0,0, 24,24,24]);
//_bgrid([28,28, 24,24,24]);
//_bgrid([0,0, 24,28,28]);
//_bgrid([16,16,16,16,16]);
//_bgrid([5,8],5);
//_bgrid([5,8],5,5);
//_bgrid(c=3);

//_bgrid( [0,0,4] );
//_bgrid( [0,0,7] );
// _formula([ _vector( sbgrid([0,0,4]),"w=7")
//          , "+" 
//          , _vector( sbgrid([0,0,3],c=3),"w=7")
//          , "=" 
//          , _vector( sbgrid([0,0,7]),"w=7")
//          ]);
//          
// _formula([ _vector( sbgrid(c=5),"w=7")
//          , "+" 
//          , _vector( sbgrid([0,6,6],c=5),"w=7")
//          , "=" 
//          , _vector( sbgrid([0,6,6],c=5),"w=7")
//          ]);
//   
//_bgrid( [1,2,3]+[1,2,1] );   
