include <../scadx.scad>

/*
   2018.4.15
   
   Based on recent development in scadx_edgeface.scad and scadx_subdiv.scad,
   attempt to reconstruct scadx_subdiv for easier debugging. 
   
   The main idea is to seperate the funcs needed to generate 
    
    core_subfaces => rename to core_subfaces here
    tip_subfaces
    edge_subfaces
    
    so they can be individually called to verify the outcome.
   
*/

//. subpts

function get_subPts( fpts
                   , shrink_ratio=0.25
                   , borders// an arr of ei. For example, [0,1] means edges 0 and 1 are border
                   , border_shrink_ratio // 0 ~ 1   
                   , opt
                   )=
(
   /*   
        Shrink pts on an internal face (i.e., not on the border of a mesh)
        
        fpts: fpts of a SINGLE face  

        .---------.
        | .-----.  \
        | |      \  \
        | |       \  \
        | |        \  \
        | '--._ _-'  _- 
        '--._  `  _-'  
             `'-.' 
       P2               P3
        +----------------+ ---
        |                |  | ----> shrink_ratio
        |  S2-------S3   | ---
        |   |        |   |
        |  S1-------S0   | ---
        |                | | ----> shrink_ratio
        +---*--------*---+ ---      
       P1            ^   P0
                     |
                     +------------- node
                          
   --- Pts don't need to be co-planar  
    
   //--------------------------------------------------------------
     border_shrink_ratio = undef: 
   
     
         |          |          |
         8----------9----------10-----
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6-----
         |  +====+  |  +====+  |
         |  | 0  |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2-----
   
     Setting it to a number will extend the side of subfaces to the border:
     
     border_shrink_ratio = 0.25 
   
         |          |          |
         8----------9----------10-----
         +=======+  |  +====+  |
         |   3   |  |  | 4  |  |
         +=======+  |  +====+  |
         4----------5----------6-----
         +=======+  |  +====+  |
         |   0   |  |  | 1  |  |
         |       |  |  |    |  |
         0=======+--1--+====+--2-----
   
     border_shrink_ratio =0  --- this keeps the border
         unchanged for future merging with other meshes    
     
         |          |           |
         8.---------9-----------10-----
         | ``--.._  |   +====+  |
         |   3   |  |   | 4  |  |
         + __..--+  |   +====+  |
         4'---------5-----------6-----
         | ``--.._  |   +====+  |
         |       \  |  /  1  \  |
         |   0    \ | /       \ |  
         |         \|/         \|  
         0==========1===========2----
         
   //-------------------------------------------------------------
   
     Old dev:
   
      Similar to shrinkInternalFacePts but can also handle a face
      that is in the border of a patch. Note that two extra arguments
      (pis and edgefi) are needed.
       
      * This func is nenamed from shrink_for_patch() inside of  
        180410_dev_DooSabin_face_shrink_for_patch() in the dev_edgeface.scad
       
      ### Issue of border faces  

      When a face is on the border, or edge, of a poly mesh: 
       
         |          |          |
         8----------9----------10--
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6--
         |  +====+  |  +====+  |
         |  |  0 |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2--

      Its faces =  
       [ [4,5,1,0], [5,6,2,1], [6,7,3,2], [8,9,5,4], [9,10,6,5],.... ]
         
      Its subfaces -- if obtained in the same way as that used in a 
      solid (as shown above) --- reduces the patch size. 
        
      We need to make subfaces like this:
          
         |          |          |
         |          |          |
         8----------9----------10-----
         +=======+  |  +====+  |
         |   3   |  |  | 4  |  |
         +=======+  |  +====+  |
         4----------5----------6-----
         +=======+  |  +====+  |
         |   0   |  |  | 1  |  |
         |       |  |  |    |  |
         0=======+--1--+====+--2-----
        
      That is, special treatment needed for the displayed 4 border edges:
       
         [2,1], [1,0], [0,4], [4,8]
         
       faces[4] shrinks as usual -- for each edge calc nodepairs(@)
       that are determined by the edges before and after it. Then we
       locate the intersections (+). 
        
           4---@----@---5
           |   :    :   |
           @...+====+...@ 
           |   |  0 |   |  
           @...+====+...@
           |   :    :   |
           0---@----@---1
        
         
       For the rest, for each edge, check if the edge[0] (first pi) of 
       it, AND those BEFORE and AFTER it, shows up in the border_pis list.
         
       For this purpose, we make a array Bs (means: "is border?" for each edge).
       B=0 for [4,5], B=1 for [1,0] and [0,4], etc.
               
         face  pis        Bs
         0    [4,5,1,0]   [0,0,1,1] 
         3    [8,9,5,4]   [0,0,0,1]
         4    [9,10,6,5]  [0,0,0,0]
         1    [5,6,2,1]   [0,0,1,0]
           
       
       ### Shrink a border face 
         
       We use faces[0] as example:
       
         shrinkBorderFacePts( fpts    // pts on the target face
                            , pis     // = the target face
                            , edgefi  // = get_edgefi( faces )
                            , ratio=0.15
                            )                   
                           
       edges: [4,5], [5,1], [1,0], [0,4]
         
       With Bs= [0,0,1,1], it means,
         
       Edges [4,5] and [5,1] are NOT on border;
       Edges [1,0] and [1,4] ARE on border;
                            
       We make a triplet B3:
         
         bi =   0, 1, 2, 3    // index for border 
         Bs = [ 0, 0, 1, 1 ] 	
           
         bi   B3= [ Bs[bi-1], Bs[bi], Bs[bi+1] ]  
         0   	[0,0,1]
         1   	[0,0,1]
         2   	[0,1,1]
         3  	[1,1,0]    
            
       B3s= get3m( Bs )= [ [0,0,1],[0,0,1],[0,1,1],[1,1,0] ]                  
                            
       We then loop through B3s to determine subPts of each face   
   */ 
   let( center= sum(fpts)/len(fpts) 
      , _shrink_ratio= (shrink_ratio>0.49?0.49 ///- Make no sense to thrink >50% 
                       :shrink_ratio)*2        /// 'cos it'd become negative 
                                               ///- x2 'cos shrinking actually applied to
                                               ///  a line [pt,center] but not the edge 
      , borders = arg(borders, opt, "borders", []) 
      )
     //echo(shrink_ratio= shrink_ratio, _shrink_ratio= _shrink_ratio)
     //echo(borders=borders
     //    , _b_sh_ratio=_b_sh_ratio, b_sh_ratio=b_sh_ratio)
         
     borders && 
     border_shrink_ratio!=undef? /// If border_shrink_ratio undef, proceed as no border
      
       let( _b_sh_ratio= arg( border_shrink_ratio, opt
                            , "border_shrink_ratio", shrink_ratio)   
          , b_sh_ratio= _b_sh_ratio>0.25?0.25:_b_sh_ratio     
                        /// b_sh_ratio is limited to 0~0.25
                        /// 'cos >.25 would make the new face
                        /// border edge larger than border edge
                        /// of remaining face  
          , Bs = [ for(pi=range(fpts)) has(borders,pi)?1:0 ]  
                 /// [1,0,0,1 ...] indicating border
          , B3s= get3m(Bs) 
                 /// triplet of Bs = [ [?,1,0], [1,0,0], [0,0,1] ...]
          )  
         [ for(pi=range(fpts))
            let( prevpi = pi==0?(len(fpts)-1):(pi-1) )
            has(borders,pi)? /// If current edge ( [pi, pi+1] ) is border 
              (
                has(borders,prevpi)? /// If previous edge [pi-1,pi] is border, too
                 fpts[pi]  
                : onlinePt( get2(fpts,pi)
                          , ratio=b_sh_ratio ) 
              ) 
            : has(borders,prevpi)?  /// If previous is border (but not current one)
               onlinePt( get2(fpts,pi-1)
                       , ratio=1-b_sh_ratio )  
               
            : onlinePt( [fpts[pi], center]        /// No border. This is for the internal
                      , ratio=_shrink_ratio*1.3 ) /// -facing edges. Increase the ratio 
                                                  /// a little to prevent the border 
                                                  /// subfaces getting too large
         ]
     : [for(pt=fpts) onlinePt( [pt,center], ratio = _shrink_ratio )]
);



//. subfaces


function get_core_subfaces(faces,_rtn=[],_L=0, _i=0)=
(  
   // rodfaces(3):
   // [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
   // Re-align the indices to:  
   // [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
   _i>=len(faces)?_rtn
   : get_core_subfaces( faces=faces
                       , _rtn= concat(_rtn, [range(_L, _L+len(faces[_i]))] )
                       , _L=_L+len(faces[_i])
                       , _i=_i+1
                       )
   
);

    get_core_subfaces=[ "get_core_subfaces", "faces", "array", "Face",
     "Re-arrange the first item of faces so its first index is 0
      For example, 
      
      faces= [[2, 1, 0], [3, 4, 5], ... ]
      get_core_subfaces(faces)
           = [[0, 2, 1], [3, 4, 5], ... ]
     "
    ,""
    ];

    function get_core_subfaces_test( mode=MODE, opt=[] )=
    (   
        let( faces= rodfaces(3)
           ) 
        doctest( get_core_subfaces,
        [ 
          "var faces" 
        , [ get_core_subfaces( faces)
          , [ [0, 1, 2], [3, 4, 5]
            , [6, 7, 8, 9], [10, 11, 12, 13], [14, 15, 16, 17]]
          , "faces", ["nl",true] 
          ]
          
        , str("rodfaces(4)=")
        , str("&nbsp;&nbsp;&nbsp;", rodfaces(4))  
        , [ get_core_subfaces( rodfaces(4))
          , [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11]
            , [12, 13, 14, 15], [16, 17, 18, 19], [20, 21, 22, 23]]
          
          , "rodfaces(4)", ["nl",true] 
          ]
        ]
        ,mode=mode, opt=opt, scope=["faces",faces]
        )
    );

function get_h_pi_subpi(faces, core_subfaces)=
(
  [ for(i=range(faces)) each zip(faces[i],core_subfaces[i]) ]
  //zip( [ for(i=range(faces), f=faces[i])f ]
  //   , [ for(i=range(faces), f=core_subfaces[i]) f ]
  //   )
);

//======================================================

function get_edgefi(faces,_hash=[],_i=0)=
(  // An edgefi is:
   // [ [2,3], ["L",3,"R",6,"F",[4], "B",[0,1]] 
   // , ...
   // ]
   // Means, for edge [2,3], 
   //   it's left face is faces[3]
   //   it's right face is faces[3]
   //   it's forward faces are faces[4]
   //   it's backward faces are faces[0,1]
   // 
    _i<len(faces)? 
    let( face = faces[_i]   // a face is an array of pt-indices
         , edges= get2(face) 
         , _new = [ for(eg=edges) 
                    each [eg, [ haskey(_hash, [eg[1],eg[0]])?"L":"R"
                              , _i ] 
                         ]
                  ]              
         )
    ////get_edgeface(faces, _hash= update(_hash,_hash_new), _i=_i+1 )
    get_edgefi(faces, _hash= update_edgefi(_hash,_new), _i=_i+1 )
    
    :_hash
    
    /*
    :// We have done adding the R/L faces
     // Now we add fowward and backward faces 
     let( pt_fis= get_aFis_on_pts(faces) 
                 // pt_fis: indices of faces connect to a pt
                 // [ [2,0,1], [5,4,2] ...] = pts[0] has faces 2,0,1  
          )
       [ for( eg=keys(_hash))
         let( h_LRfis = hash(_hash, eg) // ["L",4,"R",5]
            , LRfis = vals(h_LRfis) // [4,5]
            , p1fis = pt_fis[ eg[1] ]
            , Ffis  = dels( p1fis, LRfis )
            , p0fis = pt_fis[ eg[0] ]
            , Bfis  = dels( p0fis, LRfis )
            )
         each [ eg, concat( h_LRfis
                          , ["F", Ffis,"B", Bfis]
                          )
              ] 
       ]
    */   
);

    get_edgefi=[ "get_edgefi", "faces", "array", "Face",
     "Return the edgefi data defined by faces.
     
      An edgefi is a hash defined as:
      
        {edge:{'R':Rfi, 'L':Lfi, 'F':[Ffi], 'B':[Bfi]}}
        
      where fi = index of face (in reference to faces)
            L,R,F,B = Left, Right, Forward and Backward, resp. 
      
      Like:
	  [ [2, 1], ['R', 0, 'L', 3, 'F', [2], 'B', [4]]
          , [1, 0], ['R', 0, 'L', 2, 'F', [4], 'B', [3]]
          , ...
          ]       
     "
    ,""
    ];
    function get_edgefi_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3)
           , conefaces = faces( shape= "cone", nseg=1) )
        doctest( get_edgeface,
        [ 
          "var rodfaces"  //[[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , [get_edgefi(rodfaces)
          ,[ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
           , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
           , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
           , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
           , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
           , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
           , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
           , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
           , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
           ]
            
          , "rodfaces", ["nl", true]
          ] 
        , ""
        , "var conefaces"
        , [get_edgefi(conefaces)
          ,[ [3, 2], ["R", 0, "L", 3, "F", [2], "B", [4]] 
           , [2, 1], ["R", 0, "L", 2, "F", [1], "B", [3]]
           , [1, 0], ["R", 0, "L", 1, "F", [4], "B", [2]]
           , [0, 3], ["R", 0, "L", 4, "F", [3], "B", [1]]
           , [1, 4], ["R", 1, "L", 2, "F", [3, 4], "B", [0]]
           , [4, 0], ["R", 1, "L", 4, "F", [0], "B", [2, 3]]
           , [2, 4], ["R", 2, "L", 3, "F", [1, 4], "B", [0]]
           , [3, 4], ["R", 3, "L", 4, "F", [1, 2], "B", [0]]
           ]
          , "conefaces", ["nl", true]
          ] 
            
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces, "conefaces", conefaces]
        )
    );
    
function update_edgefi(ef1, ef2, _i=0)=
(
   _i> len(ef2)-2? ef1
   : let( eg2= ef2[_i]   // current edge by index, like [2,3]
        , f2s= ef2[_i+1] // current face hash, like 
                         //  [ "L",3, "R", 6 ]
                         //  3: left face index (in faces) = faces[3]
                         // An ef is:
                         // [ [2,3], ["L",3,"R",6] 
                         // , ...
                         // ]
        , eg2rev = [ eg2[1], eg2[0] ]                 
        , ef1= haskey(ef1, eg2) // eg2 found in ef1, so we need to replace
                                // [ "L",3, "R",6 ] of ef1.eg2
                                // with f2s (= ef2.eg2)
               ? update( ef1
                       , [ eg2 
                         , update( hash(ef1, eg2), f2s )
                         ]
                       )
               :haskey(ef1, eg2rev )?
                 update( ef1
                       , [ eg2rev
                         , update( hash(ef1, eg2rev), f2s )
                         ]
                       )
               : concat( ef1, [eg2, f2s] )
        )
      update_edgefi( ef1, ef2, _i=_i+2)
);    
  
    edgeface_update=[ "edgeface_update", "face", "array", "Face",
     "Return the edges defined in a face
     
     "
    ,""
    ];

    function edgeface_update_test( mode=MODE, opt=[] )=
    (   
        let( ef= [ [0,1],["L",[2, [0,1,2]], "R",[5, [2,1,0,5]]] ] 
           , ef2=[ [0,1],["L",[2, [0,1,2]]] ]
           )
        doctest( edgeface_update,
        [ 
          "var ef" 
        , [ edgeface_update( ef, [[1,2],["L",[5,[1,2,6,7]],"R",[8, [5,3,2,1]]]])
          ,[ [0, 1], ["L", [2, [0, 1, 2]],    "R", [5, [2, 1, 0, 5]]]
           , [1, 2], ["L", [5, [1, 2, 6, 7]], "R", [8, [5, 3, 2, 1]]]
           ]
          , "ef, [[1,2],['L',[5,[1,2,6,7]],'R',[8, [5,3,2,1]]]]" 
          ]
         
        , ""  
        , "var ef2"   
        , [ edgeface_update( ef2, [[1,0],[ "L", [2, [2,1,0,5]] ]])
           , [ [0, 1], ["L", [2, [2, 1, 0, 5]]] ]
           , "ef, [[1,0],[ 'L', [2, [2,1,0,5]] ]]" ]
           
        , [ edgeface_update( ef2, [[1,0],["R",[3, [2,1,0,5]]]])
           , [[0, 1], ["L", [2, [0, 1, 2]], "R", [3, [2, 1, 0, 5]]]]
           , "ef, [[1,0],['R',[3, [2,1,0,5]]]]" ]
        ]
        ,mode=mode, opt=opt, scope=["ef", ef, "ef2", ef2]
        )
    );




function get_subpi_of_pi_on_fi( pi,fi, faces, core_subfaces)=
(  
   /* 
     Get the subpi (subpt index) on fi (face index) corresponding to 
     the pi (pt index). 
     
     core_subfaces: subfaces like [ [10,9,7,6], [2,3,0,1] ] below.
                         Optional. If not given, will get it from faces  
       
     Unage: Internal function for get_edge_subface()
   
      faces[2]    faces[4]
     ----------4-----------
       9====7  |  3====0
       |    |  |  |    |
       |    |  |  |    |
      10====6  |  2====1
     ----------3-----------
       
     get_subpi_of_pi_on_fi(4,2, faces, core_subfaces) = 3
     get_subpi_of_pi_on_fi(3,2, faces) = 2
        
   */
    //echo("----get_subpi_of_pi_on_fi()----")
    let( i_on_face= idx(faces[fi],pi ) 
       , core_subfaces = core_subfaces==undef?
                               get_core_subfaces(faces)
                              :core_subfaces
       , rtn= core_subfaces[fi][i_on_face]   
       )
    //echo(i_on_face=i_on_face, rtn=rtn)
    //echo("----get_subpi_of_pi_on_fi()---- done")
    rtn    
);
   
   
function get_edge_subface(edge, edgefi, faces, core_subfaces)=
(
    /*
      --------4-------
      8====7  |  3====o
      |    |  |  |    |
      |    |  |  |    |
      9====6  |  2====1
      --------3--------
            
      For edge [3,4], return its edge_subface [6,7,3,2]
      get_edge_subface(faces, core_subfaces, edgefi, [3,4])= [6,7,3,2]       

      core_subfaces: subfaces like [9,8,7,6], [0,1,2,3] above
      
      If edge is like [6,9] or [1,2] below, means it's on the border
      of a patch. In those cases, they have now L-face

      ----------4---------
        8====7  |  3====o
        |    |  |  |    |
        |    |  |  |    |
      --9====6--3--2====1--
      
    */
   
    let( cur_ef= hash(edgefi, edge) // ["R",3,"L",2,"F",[4,5],"B",[0,1]]
       , L_fi= hash(cur_ef,"L") 
       , R_fi= hash(cur_ef,"R")
       , core_subfaces = core_subfaces==undef?
                               get_core_subfaces(faces)
                              :core_subfaces
       )
    (L_fi==undef)? undef  // means, this edge is on the border of a patch   
    : [ get_subpi_of_pi_on_fi( edge[0], L_fi, faces, core_subfaces )
      , get_subpi_of_pi_on_fi( edge[1], L_fi, faces, core_subfaces )
      , get_subpi_of_pi_on_fi( edge[1], R_fi, faces, core_subfaces )
      , get_subpi_of_pi_on_fi( edge[0], R_fi, faces, core_subfaces )
      ]
);    

function get_tip_subfaces(pts, faces, isPatch) = 
(
  isPatch?
  [ for(pi= range(pts)) 
      let( subpis_on_pi = get_subpis_on_pi(faces, pi) )
      if(len(subpis_on_pi)>2) subpis_on_pi 
  ]
  : [ for(pi= range(pts)) get_subpis_on_pi(faces, pi) ]
);                    

function get_smooth_PtsFaces( pts, faces, shrink_ratio=0.15, depth=1
                             , ptsfaces 
                             , _isPatch  // if this is for a poly patch (not a solid obj)
                             , _i=0)=
(
    //
    // return [subPts, subfaces ]
    //
    echo("============= get_smooth_PtsFaces()===========")
    _i>=depth? ["pts", pts, "faces", faces ]
    :let( 
    , pts = _i==0 && ptsfaces? hash(ptsfaces, "pts"): pts
    , faces = _i==0 && ptsfaces ? hash(ptsfaces, "faces"):faces
    , prng= [0:len(pts)-1]
    , frng= [0:len(faces)-1]
    , edgefi= get_edgefi(faces)
     // edgefi = 
     // [ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
     // , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
     // , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
     // , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
     // , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
     // , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
     // , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
     // , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
     // , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
     // ]
     
   , _isPatch = _isPatch==undef? 
                  any( [ for(h=vals(edgefi)) !haskey(h,"L")] )
                  : _isPatch
        
   , edges = keys(edgefi)
     //----------------------------------------------
     //  subfaces has 3 parts:
     //  
     //   inherited_subfaces
     //   tip_subfaces
     //   edge_subfaces
   
     //------------------------------------------------------
     //
     //  Get inherited_subfaces
     //
     //------------------------------------------------------
       
   , inherited_subfaces = re_align_a_pis(faces)
     //  inherited_subfaces= 
     //  [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
         
   , aSubfacePts= _isPatch?
                    [ for(face=faces)
                     shrinkFacePts( get(pts, face)
                                  , pis=face
                                  , edgefi=edgefi
                                  , ratio = shrink_ratio ) ]
                   :[ for(face=faces)
                     shrinkFacePts( get(pts, face)
                     , ratio = shrink_ratio ) ]
     //echo(aSubfacePts=aSubfacePts);
   
                      
   , subPts = [for(subfacePts=aSubfacePts) each subfacePts] 
 
     //------------------------------------------------------
     //
     //  Get tip_subfaces
     //
     //------------------------------------------------------
       
   , tip_subfaces = _isPatch?
                      [ for(pi= prng) 
                          let( subpis_on_pi = get_subpis_on_pi(faces, pi) )
                          if(len(subpis_on_pi)>2) subpis_on_pi 
                      ]
                    : [ for(pi= prng) get_subpis_on_pi(faces, pi) ]
     // tip_subfaces= 
     //  [[2, 6, 15], [1, 10, 7], [0, 14, 11], [3, 16, 9], [4, 8, 13], [5, 12, 17]]
 
     //------------------------------------------------------
     //
     //  Get edge_subfaces
     //
     //------------------------------------------------------
        
   , edge_subfaces= [ 
        for(edge=edges) 
        let( egf= get_edge_subface(edge, edgefi, faces, inherited_subfaces))
        if(egf) egf
        ]
          
     //------------------------------------------------------
          
     //echo(edge_subfaces=edge_subfaces);
     // edge_subfaces= 
     // [ [11, 10, 1, 0], [7, 6, 2, 1], [15, 14, 0, 2], [9, 8, 4, 3]
     // , [13, 12, 5, 4], [17, 16, 3, 5], [10, 13, 8, 7], [16, 15, 6, 9]
     // , [14, 17, 12, 11]]
   , all_subfaces = concat( inherited_subfaces
                          , tip_subfaces
                          , edge_subfaces )
   //, _ptsfaces= [subPts,all_subfaces]
                         
   )
   //echo(pts=pts)
   //echo(_isPatch = _isPatch)
   //echo(inherited_subfaces= inherited_subfaces)
   echo(tip_subfaces= tip_subfaces)
   echo(edge_subfaces= edge_subfaces)
   echo(len_tip_subfaces= len(tip_subfaces))
   echo(len_edge_subfaces= len(edge_subfaces))
   echo(len_all_subfaces= len(all_subfaces))
   get_smooth_PtsFaces( pts = subPts
                       , faces = all_subfaces
                       , shrink_ratio=shrink_ratio, depth=depth
                       , ptsfaces = ptsfaces
                       , _isPatch = _isPatch
                       , _i=_i+1)
   //echo(inherited_subfaces=inherited_subfaces)
   //echo(tip_subfaces=tip_subfaces)
   //echo(edge_subfaces=edge_subfaces)
);   


module demo_get_subfaces()
{
  echom("demo_get_subfaces()");
  
  pf= hash( POLY_SAMPLES, "cube");
  pts = hash( pf, "pts");
  faces=hash( pf, "faces");
  echo(faces=faces);
  a_fpts= [for(f=faces) get(pts,f)];
  
  a_subfpts = [ for(fpts=a_fpts) get_subPts(fpts) ];
  subfpts = [for(subfpts=a_subfpts) each subfpts ];
  
  core_subfaces = get_core_subfaces(faces);
  echo(core_subfaces = core_subfaces);

  //hPiSubPi= get_h_pi_subpi(faces, core_subfaces);  
  //echo(hPiSubPi=hPiSubPi);
  //echo( pi_1= hash(hPiSubPi,1));
  
  //----------------------------------
  MarkPts(pts, Line=false);
  MarkPts(subfpts, Label=["text", range(subfpts), "color","green"]
                 , Line=false);
  
  for( i=range(faces) )
  {
    pis = faces[i];
    fpts = get(pts, pis);
    MarkPt( sum(fpts)/len(fpts), Ball=false
          , Label=["text",i, "indent",0, "color","red"]);
    Line( fpts, closed=true);
  }
  
  for( pis = core_subfaces ) 
  {
    Line(get(subfpts, pis), closed=true, color="green");
    //Plane(get(subfpts, pis), mode=1);
  }
  echom("Leaving demo_get_subfaces()");
  
}
//demo_get_subfaces();

function 184h_get_clock_faces( faces, pi
                          , _nPts
                          , _frange
                          , _fipis_near
                          , _nNearFaces
                          , _rtn
                          , _i=0
                          )=//, _rtn=[], _pi=0)=
(
   /*
   
    Note: This ver of clock faces does everything right but stuck with 
    the edge_subfaces. We will need to revise it to include the Left- and
    right-faces 
   
    clockfaces is a new data structure defined as:
    
      hash{fi:face}
      
      like  cf = [0,[0,3,2,1], 2,[0,1,5,4], 5,[0,4,7,3]]
      
      where the keys (0,2,5) are face indices; the values are pis (pt indices)
      representing the face indexed by its key. 
      
      The pis of each face are re-arranged such that they start with given pi.
      Therefore, the edges connected to this pi can be obtained as:
      
      edges = [ for(v=vals(cf)) [v[0],v[1]] ]
      
      For example, the clockfaces for pts[5] below is:
         
         cf_5 = [ 0,[5,1,0,4]
                , 3,[5,4,8,9]
                , 4,[5,9,10,6]
                , 1,[5,6,2,1] 
                ]
      
         |          |          |
         8----------9----------10---------11
         +=======+  |  +====+  |  +====+  |
         |   3   |  |  | 4  |  |  | 5  |  |
         +=======+  |  +====+  |  +====+  |
         4----------5----------6----------7
         +=======+  |  +====+  |  +====+  |
         |   0   |  |  | 1  |  |  |  2 |  |
         |       |  |  |    |  |  |    |  |
         0=======+--1--+====+--2--+====+--3---
 
      For pts[2] that is on the border of a mesh patch, we insert a undef to
      indicate the discontinuiaty :
      
        cf_2 = [ 1,[2,1,5,6], 2, [2,6,7,3], undef]
        
      For pts[0]: 
      
        cf_0 = [0, [0,44,5,1], undef]
        
      This can be verified with: 
      
        len(cf_2) %2 ==1
       
     ---------------------------------------------------
     
     faces = [[3,2,1,0], [4,5,6,7], [0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
     
     cf_pi = 184h_get_clock_faces( faces, pi )  
             ///= [0,[0,3,2,1], 2,[0,1,5,4], 5,[0,4,7,3]]
     
     Derived info:
     
       isPatchBorder_at_pi = len(cf_pi) %2 ==1
     
       nearfis_at_pi = keys( cf_pi )   ///= [0,2,5]  clock-wise 
       nearfaces_at_pi = vals( cf_pi )  ///= [ [0,3,2,1],[0,1,5,4],[0,4,7,3]] 
     
       edges_at_pi = [ for(v=vals(cf_pi)) [v[0],v[1]] ]   ///= [[0,1],[0,4],[0,3]] clock-wise
        
       tip_subface_at_pi = aSubpis                     ///= [3,8,21]   
                         = [ for( fi = nearfis_at_pi )
                              core_subfaces[fi][ idx( faces[fi], pi) ]
                           ]
                           
      edge_subfaces_at_pi = aEdgeSubFaces
                        for edge [pi, pi2]
                         right_face_i = [ for(i=range(nearfaces_at_pi))
                                          if( nearfaces_at_pi[i][1]==pi2)
                                          i
                                        ][0]
                         right face fi = nearfis_at_pi[ right_face_i ]
                                        
                         left_face=  
                        = [
                            
                            core_subfaces[fi][ idx( faces[fi], pi) ]
                            
                        
                        
                        
                          ]
      
      
      
                         = [ for( i = range( nearfis_at_pi ) )
                              let( prevfi= get( nearfis_at_pi, i-1)
                                 , fi= nearfis_at_pi[i] 
                                 )
                              each
                              [ get_subpi_of_pi_on_fi( pi,fi, faces, core_subfaces)
                              , get_subpi_of_pi_on_fi( pi,fi, faces, core_subfaces)
                              ]   
                           ]                     
        
        
   // faces:     
   // [[2,1,0], [3,4,5], [0,1,4,3], [1,2,5,4], [2,0,3,5]]
   // Re-align the indices to:  
   // [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
          
   // get_subpi_of_pi_on_fi( pi,fi, faces, core_subfaces)
          
   */
   echo( "---------184h_get_clock_faces()------------", _i = _i )
   _i==0? 
   let( _nPts= _nPts==undef? (max( [for(f=faces)each f])+1): _nPts
      , _frange = [0:len(faces)-1]
      , _fipis_near= [ for(fi=_frange)
                         let( i_for_pi_in_f= idx(faces[fi], pi) 
                            )
                         if( i_for_pi_in_f>=0) 
                           [ fi, roll( faces[fi], -i_for_pi_in_f) ]       
                       ]
      , _nNearFaces = len(_fipis_near) 
      , _rtn = _fipis_near[0] 
      )
   echo( " _i=0, setting up ...." )
   echo( _nPts = _nPts )
   echo( _frange = _frange )
   echo( _fipis_near = _fipis_near )
   echo( _nNearFaces = _nNearFaces ) 
   echo( _rtn = _rtn )  
   184h_get_clock_faces( faces, pi
                      , _nPts
                      , _frange
                      , _fipis_near
                      , _nNearFaces
                      , _rtn
                      , _i=_i+1
                      )   
  : _i> _nNearFaces-1? 
    echo(final_rtn = _rtn)
    (len(_rtn)%2==1? concat(_rtn,[undef]): _rtn )
    
  : let(
         rtn = [ for( j = [1:_nNearFaces-1] )
                 
                 //echo( ___j = j)
                 //echo( ___check = get(_fipis_near[_i][1],-1) )
                 //echo( ___to_match= _fipis_near[j][1][1] )
                 //echo( ___is_found= get(_fipis_near[_i][1],-1)
                          //== _fipis_near[j][1][1]?j:undef
                     //) 
                 if( get(_fipis_near[_i-1][1],-1)
                          == _fipis_near[j][1][1] 
                   ) each _fipis_near[j]
               ]
  
       )    
   echo( _nPts = _nPts )
   echo( _frange = _frange )
   echo( _fipis_near = _fipis_near )
   echo( _nNearFaces = _nNearFaces )
   echo( rtn_at_this_i = rtn )                 
   184h_get_clock_faces( faces, pi
                      , _nPts
                      , _frange
                      , _fipis_near
                      , _nNearFaces
                      , _rtn = concat(_rtn, rtn)
                      , _i=_i+1
                      )
);
function 184k_get_clock_faces( faces, pi
                          , _nPts
                          , _frange
                          , _fipis_near
                          , _nNearFaces
                          , _core_subfaces // needs this to calc subface of each edge
                          , _rtn
                          , _i=0
                          )=//, _rtn=[], _pi=0)=
(
   /*
   
    Note: This ver of clock faces does everything right but stuck with 
    the edge_subfaces. We will need to revise it to include the Left- and
    right-faces 
   
    clockfaces is a new data structure defined as:
    
      hash{fi:face}
                    4
                   .+_
                 .' | '--_               
               .'   |     '--_               
             .'     |         '--_                 
           .'       |   [5]       '-_  7  
       5 .'   [2]   |                |
         |          |                |
         |          |                |
         |        .'0--_             |
         |      .'      '--_         |
         |    .'            '--_     |
         |  .'     [0]          '--_ |
         |.'                        '+ 3
      1  ._                        .'
           '--_                  .'  
               '--_            .'
                   '--_      .'
                       '--_-' 
                          2 
                     
    // cf_at_pi= 
    //  [ 3,["L",[5,[0,4,7,3]],"R",[0,[0,3,2,1]],"subface",[21,20,0,3]]
    //  , 1,["L",[0,[0,3,2,1]],"R",[2,[0,1,5,4]],"subface",[3,2,9,8]]
    //  , 4,["L",[2,[0,1,5,4]],"R",[5,[0,4,7,3]],"subface",[8,11,22,21]]
    //  ]   
          
      where the keys (0,2,5) are face indices; the values are pis (pt indices)
      representing the face indexed by its key. 
      
      The pis of each face are re-arranged such that they start with given pi.
      Therefore, the edges connected to this pi can be obtained as:
      
      edges = [ for(v=vals(cf)) [v[0],v[1]] ]
      
      For example, the clockfaces for pts[5] below is:
         
         cf_5 = [ 0,[5,1,0,4]
                , 3,[5,4,8,9]
                , 4,[5,9,10,6]
                , 1,[5,6,2,1] 
                ]
      
         |          |          |
         8----------9----------10---------11
         +=======+  |  +====+  |  +====+  |
         |   3   |  |  | 4  |  |  | 5  |  |
         +=======+  |  +====+  |  +====+  |
         4----------5----------6----------7
         +=======+  |  +====+  |  +====+  |
         |   0   |  |  | 1  |  |  |  2 |  |
         |       |  |  |    |  |  |    |  |
         0=======+--1--+====+--2--+====+--3---
 
      For pts[2] that is on the border of a mesh patch, we insert a undef to
      indicate the discontinuiaty :
      
        cf_2 = [ 1,[2,1,5,6], 2, [2,6,7,3], undef]
        
      For pts[0]: 
      
        cf_0 = [0, [0,44,5,1], undef]
        
      This can be verified with: 
      
        len(cf_2) %2 ==1
       
     ---------------------------------------------------
     
     faces = [[3,2,1,0], [4,5,6,7], [0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
     
     cf_pi = 184h_get_clock_faces( faces, pi )  
             ///= [0,[0,3,2,1], 2,[0,1,5,4], 5,[0,4,7,3]]
     
     Derived info:
     
       isPatchBorder_at_pi = len(cf_pi) %2 ==1
     
       nearfis_at_pi = keys( cf_pi )   ///= [0,2,5]  clock-wise 
       nearfaces_at_pi = vals( cf_pi )  ///= [ [0,3,2,1],[0,1,5,4],[0,4,7,3]] 
     
       edges_at_pi = [ for(v=vals(cf_pi)) [v[0],v[1]] ]   ///= [[0,1],[0,4],[0,3]] clock-wise
        
       tip_subface_at_pi = aSubpis                     ///= [3,8,21]   
                         = [ for( fi = nearfis_at_pi )
                              core_subfaces[fi][ idx( faces[fi], pi) ]
                           ]
                           
      edge_subfaces_at_pi = aEdgeSubFaces
                        for edge [pi, pi2]
                         right_face_i = [ for(i=range(nearfaces_at_pi))
                                          if( nearfaces_at_pi[i][1]==pi2)
                                          i
                                        ][0]
                         right face fi = nearfis_at_pi[ right_face_i ]
                                        
                         left_face=  
                        = [
                            
                            core_subfaces[fi][ idx( faces[fi], pi) ]
                            
                        
                        
                        
                          ]
      
      
      
                         = [ for( i = range( nearfis_at_pi ) )
                              let( prevfi= get( nearfis_at_pi, i-1)
                                 , fi= nearfis_at_pi[i] 
                                 )
                              each
                              [ get_subpi_of_pi_on_fi( pi,fi, faces, core_subfaces)
                              , get_subpi_of_pi_on_fi( pi,fi, faces, core_subfaces)
                              ]   
                           ]                     
        
        
   // faces:     
   // [[2,1,0], [3,4,5], [0,1,4,3], [1,2,5,4], [2,0,3,5]]
   // Re-align the indices to:  
   // [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
          
   // get_subpi_of_pi_on_fi( pi,fi, faces, core_subfaces)
          
   */
   
    // cf_at_pi= 
    //  [ 3,["L",[5,[0,4,7,3]],"R",[0,[0,3,2,1]],"subface",[21,20,0,3]]
    //  , 1,["L",[0,[0,3,2,1]],"R",[2,[0,1,5,4]],"subface",[3,2,9,8]]
    //  , 4,["L",[2,[0,1,5,4]],"R",[5,[0,4,7,3]],"subface",[8,11,22,21]]
    //  ]    
            
   echo( "---------184k_get_clock_faces()------------", _i = _i )
   _i==0? 
   let( _nPts= _nPts==undef? (max( [for(f=faces)each f])+1): _nPts
      , _frange = [0:len(faces)-1]
      , _fipis_near= [ for(fi=_frange)
                         let( i_for_pi_in_f= idx(faces[fi], pi) 
                            )
                         if( i_for_pi_in_f>=0) 
                           [ fi, roll( faces[fi], -i_for_pi_in_f) ]       
                       ]
         ///  _fipis_near: (un-ordered)
         /// [[0,[0,3,2,1]],[2,[0,1,5,4]],[5,[0,4,7,3]]] 
                       
      , _nNearFaces = len(_fipis_near) 
      , _core_subfaces= get_core_subfaces(faces) 
      , _rtn = [ for( _fi = [1:_nNearFaces-1] )
                 
                 //echo( ___j = j)
                 //echo( ___check = get(_fipis_near[_i][1],-1) )
                 //echo( ___to_match= _fipis_near[j][1][1] )
                 //echo( ___is_found= get(_fipis_near[_i][1],-1)
                          //== _fipis_near[j][1][1]?j:undef
                     //)
                  
                 // This is for the first face [0,[0,3,2,1]]
                 // edge = [0,3]
                 //   right face is itself [0,[0,3,2,1]]
                 //   left face is one ends up with _fipis_near[0][1]= 3 
                 if( last(_fipis_near[_fi][1])
                          == _fipis_near[0][1][1] 
                   ) let(Lfi = _fipis_near[_fi][0]
                        ,Rfi = _fipis_near[0][0]
                        )
                     each [ _fipis_near[0][1][1]
                          , ["L", _fipis_near[_fi], "R", _fipis_near[0] 
                            ,"subface", [ 
                                 _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                               , _core_subfaces[Lfi][ idx( faces[Lfi], get(_fipis_near[_fi][1],-1)) ]
                               , _core_subfaces[Rfi][ idx( faces[Rfi], _fipis_near[0][1][1]) ]
                               , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                               ] 
                            ] 
                          ]
                          
               ]
      )
   echo( " _i=0, setting up ...." )
   echo( _nPts = _nPts )
   echo( _frange = _frange )
   echo( _fipis_near = _fipis_near )
   echo( _nNearFaces = _nNearFaces ) 
   echo( _rtn = _rtn )  
   184k_get_clock_faces( faces, pi
                      , _nPts
                      , _frange
                      , _fipis_near
                      , _nNearFaces
                      , _core_subfaces
                      , _rtn
                      , _i=1
                      )   
  : _i> _nNearFaces-1? 
  
      echo(_red( str("final_rtn = cf_pi= ",_rtn)))
      //  final _rtn= cf= 
      //     [ 3, [ "L", [5,[0,4,7,3]], "R", [0,[0,3,2,1]] ]
      //     , 1, [ "L", [0,[0,3,2,1]], "R", [2,[0,1,5,4]] ]
      //     , 4, [ "L", [2,[0,1,5,4]], "R", [5,[0,4,7,3]] ]  
      //     ]
      // final _rtn = cf_pi= 
      //  [ 3,["L",[5,[0,4,7,3]],"R",[0,[0,3,2,1]],"subface",[21,20,0,3]]
      //  , 1,["L",[0,[0,3,2,1]],"R",[2,[0,1,5,4]],"subface",[3,2,9,8]]
      //  , 4,["L",[2,[0,1,5,4]],"R",[5,[0,4,7,3]],"subface",[8,11,22,21]]
      //  ]   
      
      /*
      isPatchBorder_at_pi = len(cf_pi) %2 ==1
      */
     echo( _red("==== Checking derivable info: ==== BEG"))
     let(  nearpis_at_pi = keys(_rtn)
         , nearfis_at_pi = [for(v=vals(_rtn)) hash(v,"R")[0]]
         , nearfaces_at_pi = [for(v=vals(_rtn)) hash(v,"R")[1]]
         , edges_at_pi = [for(_pi=nearpis_at_pi) [0,_pi]]
         , tip_subface_at_pi = [ for( fi = nearfis_at_pi )
                                  _core_subfaces[fi][ idx( faces[fi], pi) ]
                                ]
         , edge_subfaces_at_pi= [
              for( kv = hashkvs(_rtn) ) hash( kv[1], "subface")
            ]                       
         )
      echo( nearpis_at_pi = nearpis_at_pi) 
      echo( nearfis_at_pi = nearfis_at_pi) 
      echo( nearfaces_at_pi = [for(fi=nearfis_at_pi)faces[fi]])
      echo( nearfaces_at_pi_rolled = nearfaces_at_pi)
      echo( edges_at_pi = edges_at_pi)
      echo( tip_subface_at_pi = tip_subface_at_pi )
      echo( edge_subfaces_at_pi = edge_subfaces_at_pi )
      echo( _red("==== Checking derivable info ==== END"))
                      
      (len(_rtn)%2==1? concat(_rtn,[undef]): _rtn )
    
  : let(
         rtn = [ for( _fi = [0:_nNearFaces-1] )
                 
                 //echo( ___j = j)
                 //echo( ___check = get(_fipis_near[_i][1],-1) )
                 //echo( ___to_match= _fipis_near[j][1][1] )
                 //echo( ___is_found= get(_fipis_near[_i][1],-1)
                          //== _fipis_near[j][1][1]?j:undef
                     //) 
                 //if( get(_fipis_near[_i-1][1],-1)
                 //         == _fipis_near[j][1][1] 
                 //  ) each _fipis_near[j]
                 if( last(_fipis_near[_fi][1])
                          == _fipis_near[_i][1][1] 
                   ) 
                   let(Lfi = _fipis_near[_fi][0]
                        ,Rfi = _fipis_near[_i][0]
                        )
                   each [ _fipis_near[_i][1][1]
                        , [ "L", _fipis_near[_fi], "R", _fipis_near[_i] 
                          ,"subface"
                          , [ 
                             _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                           , _core_subfaces[Lfi][ idx( faces[Lfi], get(_fipis_near[_fi][1],-1)) ]
                           , _core_subfaces[Rfi][ idx( faces[Rfi], _fipis_near[_i][1][1]) ]
                           , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                           ] 
                          ]
                        ]  
               ]
  
       )    
   echo( _nPts = _nPts )
   echo( _frange = _frange )
   echo( _fipis_near = _fipis_near )
   echo( _nNearFaces = _nNearFaces )
   echo( rtn_at_this_i = rtn )
   184k_get_clock_faces( faces, pi
                      , _nPts
                      , _frange
                      , _fipis_near
                      , _nNearFaces
                      , _core_subfaces
                      , _rtn = concat(_rtn, rtn)
                      , _i=_i+1
                      )
);
function 184L_get_clock_faces( faces, pi
                          , _nPts
                          , _frange
                          , _fipis_near
                          , _nNearFaces
                          , _core_subfaces // needs this to calc subface of each edge
                          , _rtn
                          , _i=0
                          )=//, _rtn=[], _pi=0)=
(
   // Check previous version (184k_get_clock_faces) for doc
   
    // cf_at_0= 
    //  [ 3,["L",[5,[0,4,7,3]],"R",[0,[0,3,2,1]],"subface",[21,20,0,3]]
    //  , 1,["L",[0,[0,3,2,1]],"R",[2,[0,1,5,4]],"subface",[3,2,9,8]]
    //  , 4,["L",[2,[0,1,5,4]],"R",[5,[0,4,7,3]],"subface",[8,11,22,21]]
    //  ]    
            
   echo( _green("########## 184L_get_clock_faces() ##########"), pi=pi, _i = _i )
   _i==0? 
   echo(faces = faces)
   echo( _b("_i=0, setting up"))
   
   let( _nPts= _nPts==undef? (max( [for(f=faces)each f])+1): _nPts
      , _frange = [0:len(faces)-1]
      , _fipis_near= [ for(fi=_frange)
                         let( i_for_pi_in_f= idx(faces[fi], pi) 
                            )
                         if( i_for_pi_in_f>=0) 
                           [ fi, roll( faces[fi], -i_for_pi_in_f) ]       
                       ]
         ///  _fipis_near: (un-ordered)
         /// [[0,[0,3,2,1]],[2,[0,1,5,4]],[5,[0,4,7,3]]] 
                       
      , _nNearFaces = len(_fipis_near) 
      , _core_subfaces= get_core_subfaces(faces) 
      , _rtn = [ for( _fi = [1:_nNearFaces-1] )
                 
                 //echo( ___j = j)
                 //echo( ___check = get(_fipis_near[_i][1],-1) )
                 //echo( ___to_match= _fipis_near[j][1][1] )
                 //echo( ___is_found= get(_fipis_near[_i][1],-1)
                          //== _fipis_near[j][1][1]?j:undef
                     //)
                  
                 // This is for the first face [0,[0,3,2,1]]
                 // edge = [0,3]
                 //   right face is itself [0,[0,3,2,1]]
                 //   left face is one ends up with _fipis_near[0][1]= 3 
                 if( last(_fipis_near[_fi][1])
                          == _fipis_near[0][1][1] 
                   ) let(Lfi = _fipis_near[_fi][0]
                        ,Rfi = _fipis_near[0][0]
                        )
                     each [ _fipis_near[0][1][1]
                          , ["L", _fipis_near[_fi], "R", _fipis_near[0] 
                            ,"subface", [ 
                                 _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                               , _core_subfaces[Lfi][ idx( faces[Lfi], get(_fipis_near[_fi][1],-1)) ]
                               , _core_subfaces[Rfi][ idx( faces[Rfi], _fipis_near[0][1][1]) ]
                               , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                               ] 
                            ] 
                          ]
                          
               ]
      )
  
   echo( _s("_fipis_near pts[pi={_}] = {_}", [pi, _fipis_near]))
   echo( _s("_fi_near pts[pi={_}] = {_}", [pi, [for(x=_fipis_near) x[0]]] ))
   echo(_rtn=_rtn)
   echo( _b("Done setup, go next to _i=0"))
   184L_get_clock_faces( faces, pi
                      , _nPts
                      , _frange
                      , _fipis_near
                      , _nNearFaces
                      , _core_subfaces
                      , _rtn
                      , _i=_i+1
                      )   
  
  //=====================================================
  
  : _i> _nNearFaces-1? 
  
      echo(_green(_s("####################  END of pi=[{_}], preparing final _rtn", pi )))
      let( 
           final_for_this_pi = len(_rtn)%2==1? concat(_rtn,[undef]): _rtn    
         , clocked_faces = [ for(v=vals(final_for_this_pi))
                             hash(v,"R")[0]
                           ] 
         )
      echo(clocked_faces=clocked_faces)   
      echo(final_for_this_pi = final_for_this_pi)
      echo("")
                      
      clocked_faces
    
  //=====================================================
    
  : 
  
    echo("================", _______i=_i )
    echo( _s("_fipis_near pts[pi={_}][_i={_}] = {_}", [pi, _i,_fipis_near[1]]))
    //echo(_rtn=_rtn)
    //echo( _fipis_near__i = _fipis_near[_i])
    echo( "Setting _rtn" )
    let(
         prev_R_fipis =  hash(last(_rtn),"R") //_fipis_near[_i-1]
       , next_pi = last(prev_R_fipis[1])
       , next_fipis = [ for(_fi=[0:len(_fipis_near)-1] )
                     if( _fipis_near[_fi][1][1]== next_pi 
                       )
                   let( Lfi = _fipis_near[_fi][0]
                      , Rfi = prev_fipis[0]
                      )                          
                     each 
                        [ next_pi
                        , [ "L", _fipis_near[_fi], "R", _fipis_near[_i] 
                          ,"subface"
                          , [ 
                             _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                           , _core_subfaces[Lfi][ idx( faces[Lfi], get(_fipis_near[_fi][1],-1)) ]
                           , _core_subfaces[Rfi][ idx( faces[Rfi], _fipis_near[_i][1][1]) ]
                           , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                           ] 
                          ]
                        ]  
                   ]       
                       
//       , rtn = [ for( _fi = [0:_nNearFaces-1] )
//                 
//                 //echo( ___fi= _fi)
//                 //echo( ___fipis_near__fi = _fipis_near[_fi])
//                 //echo( ___check = get(_fipis_near[_fi][1],-1) )
//                 //echo( ___to_match= _fipis_near[_fi][1][1] )
//                 //echo( ___is_found= get(_fipis_near[_i][1],-1)
//                          //== _fipis_near[_fi][1][1]?_fi:undef
//                     //) 
//                 //last(_fipis_near[_fi][1])
//                //          == _fipis_near[0][1][1]
//                              
//                 //if( get(_fipis_near[_i-1][1],-1)
//                 //         == _fipis_near[_fi][1][1] 
//                 //  ) each _fipis_near[_fi]
//                 
//                 if( last(_fipis_near[_fi][1])== prev_fipis[1][1] 
//                   ) 
//                   let( Lfi = _fipis_near[_fi][0]
//                      , Rfi = prev_fipis[0]
//                      )
//                   each 
//                   [ _fipis_near[_fi][1][1]
//                        , [ "L", _fipis_near[_fi], "R", _fipis_near[_i] 
//                          ,"subface"
//                          , [ 
//                             _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
//                           , _core_subfaces[Lfi][ idx( faces[Lfi], get(_fipis_near[_fi][1],-1)) ]
//                           , _core_subfaces[Rfi][ idx( faces[Rfi], _fipis_near[_i][1][1]) ]
//                           , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
//                           ] 
//                          ]
//                        ]  
//               ]
  
       )   
    
    
   echo( prev_R_fipis = prev_R_fipis ) ///=  [0, [0, 3, 2, 1]]
   echo( next_pi = next_pi )
   echo( _s( "The face prior to _i=[{_}] is fi={_}, pis={_}"
            , [_i, _fipis_near[_i-1][0],_fipis_near[_i-1][1]] 
            ))
          
   //echo( _nPts = _nPts )
   //echo( _frange = _frange )
   //echo( _fipis_near = _fipis_near )
   //echo( _nNearFaces = _nNearFaces )
   echo( rtn_at_this_i = [next_fipis] )
   184L_get_clock_faces( faces, pi
                      , _nPts
                      , _frange
                      , _fipis_near
                      , _nNearFaces
                      , _core_subfaces
                      , _rtn = concat(_rtn, [next_fipis])
                      , _i=_i+1
                      )
);
function 184m_get_clock_faces( faces, pi
                          , _nPts
                          , _near_fis
                          , _core_subfaces // needs this to calc subface of each edge
                          , _next_edge_end
                          , _rtn
                          , _i=0
                          )=//, _rtn=[], _pi=0)=
(
   // Check previous version (184L_get_clock_faces) for doc
   
    // Simplify the previous data structure: 
    // cf_at_0= 
    //  [ 3,["L",[5,[0,4,7,3]],"R",[0,[0,3,2,1]],"subface",[21,20,0,3]]
    //  , 1,["L",[0,[0,3,2,1]],"R",[2,[0,1,5,4]],"subface",[3,2,9,8]]
    //  , 4,["L",[2,[0,1,5,4]],"R",[5,[0,4,7,3]],"subface",[8,11,22,21]]
    //  ]    
    // to:
    //  [ 3,["L",5, "R",0, "subface",[21,20,0,3]]
    //  , 1,["L",0, "R",2, "subface",[3,2,9,8]]
    //  , 4,["L",2, "R",5, "subface",[8,11,22,21]]
    //  ]   
    //
    // i.e., no longer roll the pis of a face so there's no need to 
    // include it. Just use faces[fi] to get the pis
   
            
   echo( _blue("########## 184m_get_clock_faces() ##########"), pi=pi, _i = _i )
   _i==0?
   /*
    We start with the first fi in the unclocked _near_fis: faces[0]
    
    	faces[0] = [0,3,2,1]
    	
    We get:
     
     	edge_end (for edge [0,3]) = 3
    	Rfi = 0
    	Lfi = need to search for this from _near_fis
    	
    	
    from _near_fis, search face:
         
        prev( faces[fi], pi) == edge_end = 3
    
    is the Lfi 
    
        Lfi = 5
        Lface = faces[ Lfi ] = [0,1,5,4]
    
    We then use Lfi (5) and Rfi (0) to search for subface. 
    
    So we get a cf [ edge_end, ["L", Lfi, "R", Rfi, "subface", [...] ]
                   
        cf = [3, ["L",5, "R",0, "subface",[...] ]
               
    We also set next_edge_end: 
    
       next_edge_end = prev( faces[Rfi], pi ) =1
   
   */ 
   echo(faces = faces)
   echo( _b("_i=-1, setting up"))
   
   let( _nPts= _nPts==undef? (max( [for(f=faces)each f])+1): _nPts
      , _core_subfaces= get_core_subfaces(faces)
      , _near_fis = [ for(fi= range(faces) )          // unclocked fis around
                     if( has(faces[fi], pi) ) fi
                   ]
      
      // For _i=0 only: 
      , Rfi = _near_fis[0]
      , edge_end = next( faces[Rfi], pi) // index of pts next to pi
                                         // = tail pi of the edge 
      , _next_edge_end = prev( faces[Rfi], pi )              
      , Lfi = [ for( i= [1:len(_near_fis)-1] )
                 let( fi = _near_fis[i] ) 
                 if( prev( faces[fi], pi) == edge_end ) 
                 fi
              ][0]
      , _rtn= [ edge_end 
              , [ "L", Lfi
                , "R", Rfi
                , "surface"
                , [ _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                  , _core_subfaces[Lfi][ idx( faces[Lfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                  ]   
                ] 
             ]
      )
  
   echo( str(_s("_near_fis of pts[{_}] = {_}", [pi, _near_fis])
         , _green("// unclocked nearby fis"))
       )
   echo( str(_s("Start with the 1st face, fi=_near_fis[0] = {_}", Rfi)
         , _green("// This fi = Rfi --- index of the right-side face" )
         )
       )
   echo( edge_end= edge_end 
         , _green(_s("// For edge [{_},{_}]", [pi, edge_end]))
       )  
   echo( "Search to find the Lfi: ", Lfi=Lfi) 
   echo( _next_edge_end = _next_edge_end )     
   echo(_rtn=_rtn)
   echo( _b("Done setup, go next to _i=0"))
   184m_get_clock_faces( faces, pi
                      , _nPts
                      , _near_fis
                      , _core_subfaces
                      , _next_edge_end
                      , _rtn=_rtn
                      , _i=_i+1
                      )   
  
  //=====================================================
  
  : _i> len(_near_fis)-1? 
  //:_i>0?
      echo(_blue(_s("####################  END of pi=[{_}], preparing final _rtn", pi )))
      let( 
           final_for_this_pi = len(_rtn)%2==1? concat(_rtn,[undef]): _rtn    
         , clocked_faces = [ for(v=vals(final_for_this_pi))
                             hash(v,"R")
                           ] 
         )
      echo(clocked_faces=clocked_faces)   
      echo(final_for_this_pi = final_for_this_pi)
      echo("")
                      
      final_for_this_pi
    
  //=====================================================
    
  : 
  
    //echo("================", _______i=_i )
    echo( _s("near_fis of pts[{_}][{_}] = {_}", [pi, _i, _near_fis[1]]))
    echo( previous_rtn = _rtn ) ///=>  like: [3, ["L",5, "R",0, "surface",[21,20,0,3]]]
    echo( previous_next_edge_end= _next_edge_end
        , _green(_s("// This will be the edge_end of current edge [{_},{_}]"
                   , [pi,_next_edge_end] ))
        )
    echo( "Setting _rtn" )
    /*
                4
               .+_
             .' | '--_               
           .'   |     '-_ 7  
       5 .' [2] |  [5]  |
         |    .'0-_     |
         |  .'     '--_ |
         |.'   [0]     '+ 3
      1  ._           .'
           '--_     .'  
               --_-' 
                 2    
     
    faces = [[3, 2, 1, 0], [4, 5, 6, 7], [0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]]             
    _near_fis = [0,2,5] (unclocked)
    
    If we flatten it out, and plot the subface in each:
        
       6----------7
       |  6====7  | 
       |  |  1 |  | 
       |  5====4  | 
       5----------4----------7----------6----------5
       | 10====11 | 22====23 | 18====19 | 14====15 |
       |  |  2 |  |  | 5  |  |  | 4  |  |  | 3  |  |
       |  9====8  | 21====20 | 17====16 | 13====12 |
       1----------0----------3----------2----------1
       |  2====3  | 
       |  |  0 |  | 
       |  1====0  | 
       2----------3
                  
    This gives clock-faces at 0 = 
    
      [ 3,["L",5, "R",0, "subface",[21,20,0,3]]
      , 1,["L",0, "R",2, "subface",[3,2,9,8]]
      , 4,["L",2, "R",5, "subface",[8,11,22,21]]
      ]
      
    I. 
    
    We start with the first fi in the unclocked _near_fis: faces[0]
    
    	faces[0] = [0,3,2,1]
    	
    We get:
     
     	edge_end (for edge [0,3]) = 3
    	Rfi = 0
    	Lfi = need to search for this from _near_fis
    	
    	
    from _near_fis, search face:
         
        prev( faces[fi], pi) == edge_end = 3
    
    is the Lfi 
    
        Lfi = 5
        Lface = faces[ Lfi ] = [0,1,5,4]
    
    We then use Lfi (5) and Rfi (0) to search for subface. 
    
    So we get a cf [ edge_end, ["L", Lfi, "R", Rfi, "subface", [...] ]
                   
        cf = [3, ["L",5, "R",0, "subface",[...] ]
               
    We also set next_edge_end: 
    
       next_edge_end = prev( faces[Rfi], pi ) =1
                  
    II.
    
    After the faces[0], we add next edge_end/faces 
        
        
       prev_cf = [3, ["L",5, "R",0, "subface",[...] ] 
       Lfi = prev_Rfi = hash( prev_cf[1], "R")
       edge_end = prev next_edge_end = 1
       Rfi = search:
       
             next(faces[fi],pi)== edge_end 
             
    Note in I. we search for Lfi, thereafter we search for Rfi            
        
    */
    
    let( edge_end = _next_edge_end
       , Lfi = hash( last(_rtn), "R") 
       , Rfi = [ for( i= [1:len(_near_fis)-1] )
                 let( fj = _near_fis[i] ) 
                 if (next(faces[fj],pi)== edge_end)  fj
               ][0]
       , _next_edge_end = prev( faces[Rfi], pi )              
                 
       , cf = [ edge_end 
              , [ "L", Lfi
                , "R", Rfi
                , "surface"
                , [ _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                  , _core_subfaces[Lfi][ idx( faces[Lfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                  ]   
                ] 
             ]       
       )   
   echo(_green(_s("Checking the Lfi and Rfi of edge [{_},{_}]",[pi,edge_end]))) 
   echo( "... ",edge_end = edge_end )
   echo( "... ",Lfi = Lfi, _green("// Lfi = the Rfi of previous _i") )
   echo( "... ",Rfi = Rfi, _green("// Rfi is searched") )
   echo( "... ",cf = cf )    
   echo( "... ",_rtn= concat(_rtn,cf) ) 
   184m_get_clock_faces( faces, pi
                      , _nPts
                      , _near_fis
                      , _core_subfaces
                      , _next_edge_end
                      , _rtn = concat(_rtn, cf)
                      , _i=_i+1
                      )
);

//. Clocked-EFs

function 184n_get_Clocked_EFs( faces, pi
                          , _nPts
                          , _near_fis
                          , _core_subfaces // needs this to calc subface of each edge
                          , _next_edge_end
                          , _rtn
                          , _i=0
                          )=//, _rtn=[], _pi=0)=
(
   // Check previous version (184L_get_clock_faces) for doc
   
    // Simplify the previous data structure: 
    // cf_at_0= 
    //  [ 3,["L",[5,[0,4,7,3]],"R",[0,[0,3,2,1]],"subface",[21,20,0,3]]
    //  , 1,["L",[0,[0,3,2,1]],"R",[2,[0,1,5,4]],"subface",[3,2,9,8]]
    //  , 4,["L",[2,[0,1,5,4]],"R",[5,[0,4,7,3]],"subface",[8,11,22,21]]
    //  ]    
    // to:
    //  [ 3,["L",5, "R",0, "subface",[21,20,0,3]]
    //  , 1,["L",0, "R",2, "subface",[3,2,9,8]]
    //  , 4,["L",2, "R",5, "subface",[8,11,22,21]]
    //  ]   
    //
    // i.e., no longer roll the pis of a face so there's no need to 
    // include it. Just use faces[fi] to get the pis
   
            
   echo( _blue("########## 184n_get_Clocked_EFs() ##########"), pi=pi, _i = _i )
   _i==0?
   /*
    We start with the first fi in the unclocked _near_fis: faces[0]
    
    	faces[0] = [0,3,2,1]
    	
    We get:
     
     	edge_end (for edge [0,3]) = 3
    	Rfi = 0
    	Lfi = need to search for this from _near_fis
    	
    	
    from _near_fis, search face:
         
        prev( faces[fi], pi) == edge_end = 3
    
    is the Lfi 
    
        Lfi = 5
        Lface = faces[ Lfi ] = [0,1,5,4]
    
    We then use Lfi (5) and Rfi (0) to search for subface. 
    
    So we get a cf [ edge_end, ["L", Lfi, "R", Rfi, "subface", [...] ]
                   
        cf = [3, ["L",5, "R",0, "subface",[...] ]
               
    We also set next_edge_end: 
    
       next_edge_end = prev( faces[Rfi], pi ) =1
   
   */ 
   echo(faces = faces)
   echo( _b("_i=-1, setting up"))
   
   let( _nPts= _nPts==undef? (max( [for(f=faces)each f])+1): _nPts
      , _core_subfaces= get_core_subfaces(faces)
      , _near_fis = [ for(fi= range(faces) )          // unclocked fis around
                     if( has(faces[fi], pi) ) fi
                   ]
      
      // For _i=0 only: 
      , Rfi = _near_fis[0]
      , edge_end = next( faces[Rfi], pi) // index of pts next to pi
                                         // = tail pi of the edge 
      , _next_edge_end = prev( faces[Rfi], pi )              
      , Lfi = [ for( i= [1:len(_near_fis)-1] )
                 let( fi = _near_fis[i] ) 
                 if( prev( faces[fi], pi) == edge_end ) 
                 fi
              ][0]
      , _rtn= [ edge_end 
              , [ "L", Lfi
                , "R", Rfi
                , "subface"
                , [ _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                  , _core_subfaces[Lfi][ idx( faces[Lfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                  ]   
                ] 
             ]
      )
  
   echo( str(_s("_near_fis of pts[{_}] = {_}", [pi, _near_fis])
         , _green("// unclocked nearby fis"))
       )
   echo( str(_s("Start with the 1st face, fi=_near_fis[0] = {_}", Rfi)
         , _green("// This fi = Rfi --- index of the right-side face" )
         )
       )
   echo( edge_end= edge_end 
         , _green(_s("// For edge [{_},{_}]", [pi, edge_end]))
       )  
   echo( "Search to find the Lfi: ", Lfi=Lfi) 
   echo( _next_edge_end = _next_edge_end )     
   echo(_rtn=_rtn)
   echo( _b("Done setup, go next to _i=0"))
   184n_get_Clocked_EFs( faces, pi
                      , _nPts
                      , _near_fis
                      , _core_subfaces
                      , _next_edge_end
                      , _rtn=_rtn
                      , _i=_i+1
                      )   
  
  //=====================================================
  
  : _i> len(_near_fis)-1? 
  //:_i>0?
      echo(_blue(_s("####################  END of pi=[{_}], preparing final _rtn", pi )))
      let( 
           final_for_this_pi = len(_rtn)%2==1? concat(_rtn,[undef]): _rtn    
         , clocked_faces = [ for(v=vals(final_for_this_pi))
                             hash(v,"R")
                           ] 
         )
      echo(clocked_faces=clocked_faces)   
      echo(final_for_this_pi = final_for_this_pi)
      echo("")
                      
      final_for_this_pi
    
  //=====================================================
    
  : 
  
    //echo("================", _______i=_i )
    echo( _s("near_fis of pts[{_}][{_}] = {_}", [pi, _i, _near_fis[1]]))
    echo( previous_rtn = _rtn ) ///=>  like: [3, ["L",5, "R",0, "surface",[21,20,0,3]]]
    echo( previous_next_edge_end= _next_edge_end
        , _green(_s("// This will be the edge_end of current edge [{_},{_}]"
                   , [pi,_next_edge_end] ))
        )
    echo( "Setting _rtn" )
    /*
                4
               .+_
             .' | '--_               
           .'   |     '-_ 7  
       5 .' [2] |  [5]  |
         |    .'0-_     |
         |  .'     '--_ |
         |.'   [0]     '+ 3
      1  ._           .'
           '--_     .'  
               --_-' 
                 2    
     
    faces = [[3, 2, 1, 0], [4, 5, 6, 7], [0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]]             
    _near_fis = [0,2,5] (unclocked)
    
    If we flatten it out, and plot the subface in each:
        
       6----------7
       |  6====7  | 
       |  |  1 |  | 
       |  5====4  | 
       5----------4----------7----------6----------5
       | 10====11 | 22====23 | 18====19 | 14====15 |
       |  |  2 |  |  | 5  |  |  | 4  |  |  | 3  |  |
       |  9====8  | 21====20 | 17====16 | 13====12 |
       1----------0----------3----------2----------1
       |  2====3  | 
       |  |  0 |  | 
       |  1====0  | 
       2----------3
                  
    This gives clock-faces at 0 = 
    
      [ 3,["L",5, "R",0, "subface",[21,20,0,3]]
      , 1,["L",0, "R",2, "subface",[3,2,9,8]]
      , 4,["L",2, "R",5, "subface",[8,11,22,21]]
      ]
      
    I. 
    
    We start with the first fi in the unclocked _near_fis: faces[0]
    
    	faces[0] = [0,3,2,1]
    	
    We get:
     
     	edge_end (for edge [0,3]) = 3
    	Rfi = 0
    	Lfi = need to search for this from _near_fis
    	
    	
    from _near_fis, search face:
         
        prev( faces[fi], pi) == edge_end = 3
    
    is the Lfi 
    
        Lfi = 5
        Lface = faces[ Lfi ] = [0,1,5,4]
    
    We then use Lfi (5) and Rfi (0) to search for subface. 
    
    So we get a cf [ edge_end, ["L", Lfi, "R", Rfi, "subface", [...] ]
                   
        cf = [3, ["L",5, "R",0, "subface",[...] ]
               
    We also set next_edge_end: 
    
       next_edge_end = prev( faces[Rfi], pi ) =1
                  
    II.
    
    After the faces[0], we add next edge_end/faces 
        
        
       prev_cf = [3, ["L",5, "R",0, "subface",[...] ] 
       Lfi = prev_Rfi = hash( prev_cf[1], "R")
       edge_end = prev next_edge_end = 1
       Rfi = search:
       
             next(faces[fi],pi)== edge_end 
             
    Note in I. we search for Lfi, thereafter we search for Rfi            
        
    */
    
    let( edge_end = _next_edge_end
       , Lfi = hash( last(_rtn), "R") 
       , Rfi = [ for( i= [1:len(_near_fis)-1] )
                 let( fj = _near_fis[i] ) 
                 if (next(faces[fj],pi)== edge_end)  fj
               ][0]
       , _next_edge_end = prev( faces[Rfi], pi )              
                 
       , cf = [ edge_end 
              , [ "L", Lfi
                , "R", Rfi
                , "subface"
                , [ _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                  , _core_subfaces[Lfi][ idx( faces[Lfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                  ]   
                ] 
             ]       
       )   
   echo(_green(_s("Checking the Lfi and Rfi of edge [{_},{_}]",[pi,edge_end]))) 
   echo( "... ",edge_end = edge_end )
   echo( "... ",Lfi = Lfi, _green("// Lfi = the Rfi of previous _i") )
   echo( "... ",Rfi = Rfi, _green("// Rfi is searched") )
   echo( "... ",cf = cf )    
   echo( "... ",_rtn= concat(_rtn,cf) ) 
   184n_get_Clocked_EFs( faces, pi
                      , _nPts
                      , _near_fis
                      , _core_subfaces
                      , _next_edge_end
                      , _rtn = concat(_rtn, cf)
                      , _i=_i+1
                      )
);


module 184n_Apply_clocked_EFS()
{
    demo_get_subfaces();
	
    pts= [ for(pt=cubePts()) pt*3] ;
    faces = rodfaces(4);
    subPts = [ for(face=faces) 
                 each get_subFacePts( fpts = get(pts, face)
                           , shrink_ratio = 0.25
                           )
             ];
                                     
    echo( subPts = subPts );
    edges = get_edges(faces) ;
    echo( edges = edges );

    //clockfaces = 184m_get_clock_faces(faces, 0, len(pts));
    //echo(clockfaces=clockfaces);
    aClockedEFs = [ for(pi=range(pts)) 
                     echo(_red(_b(_s("@@@@@@@@@@@@@@@@@@@@@@@@@ pi = {_}",pi))))
                     184n_get_Clocked_EFs(faces, pi) ];
    echo(aClockedEFs=aClockedEFs);

    core_subfaces = get_core_subfaces(faces);
    tip_subfaces = [ for( cf= aClockedEFs)
                      //echo(pi=pi)
                      //echo(cf = aClockedEFs[pi] ) 
                      //echo(cfv = vals(cf) ) 
                      [ for( h=vals( cf ) ) 
                         //echo("---", h=h)
                         //echo("---", sf = hash(h,"subface"))
                         hash(h, "subface")[0]
                      ]   
                   ];
    echo(tip_subfaces=tip_subfaces);


    edge_subfaces = [ for( pi = range(aClockedEFs))
                      //echo(pi=pi)
                      //echo(cf = aClockedEFs[pi] ) 
                      //echo(cfv = vals(aClockedEFs[pi]) ) 
                      each
                      [ for( kv=hashkvs( aClockedEFs[pi] ) ) 
                         //echo("...", h=h)
                         //echo("...", sf = hash(h,"subface"))
                          if( has(edges, [pi, kv[0]]) )
                          //each [ [pi, kv[0]], hash(kv[1], "subface") ]
                          hash(kv[1], "subface") 
                      ]   
                   ];
    echo(edge_subfaces= edge_subfaces);


    /// draw core_subfaces
    ///
    for( corefi = range(core_subfaces) )
    {
       //echo(corefi= corefi);
       coresubface = core_subfaces[corefi] ;
       //echo( coresubface = coresubface );
       coresubpts = get( subPts, coresubface );
       //echo( coresubpts = coresubpts );
       Plane( coresubpts, mode=1, color="red" );
    }

    /// draw tip_subfaces
    for( f = tip_subfaces ) Plane( get( subPts, f), mode=1, color="green");

    /// draw edge_subfaces
    for( f = edge_subfaces ) Plane( get( subPts, f), mode=1, color="blue");


    subfaces = concat( core_subfaces, tip_subfaces, edge_subfaces );
    echo(subfaces=subfaces);
    polyhedron( points=subPts, faces= subfaces );

}
//184n_Apply_clocked_EFS();

module 184r_clockedEFs_for_mesh()
{
  echom("184r_clockedEFs_for_mesh()");
  /*
       5----------4----------7
       | 10====11 | 22====23 | 
       |  |  2 |  |  | 5  |  | 
       |  9====8  | 21====20 | 
       1----------0----------6
       |  2====3  | 
       |  |  0 |  | 
       |  1====0  | 
       2----------3

       faces = [ [0,3,2,1], [0,1,5,4], [0,4,7,6] ]
       
       The clockedEFs for 
       
       pi=0: [ 3,["L",undef, "R",0 ]
             , 1,["L",0, "R",2, "subface",[3,2,9,8]]
             , 4,["L",2, "R",5, "subface",[8,11,22,21]]
             , 6,["L",5, "R",undef]
             ] 
       pi=2: [ 1, ["L",undef, "R",0]
             , 3, ["L",0,"R", undef]
             ]
       pi=1: [ 5, ["L", undef, "R", 2]
             , 0, ["L", 2, "R", 0, "subface", [9,8,3,2] ]      
             , 2, ["L", 0, "R", undef]
             ]
             
CEF0 = [3, ["L", 2, "R", 0, "subface", [8, 11, 1, 0]]
      , 1, ["L", 0, "R", 1, "subface", [0, 3, 5, 4]]
      , 4, ["L", 1, "R", 2, "subface", [4, 7, 9, 8]]
      ]
CEF1 = [0, ["L", 1, "R", 0, "subface", [5, 4, 0, 3]]
      , 2, ["L", 0, "R", undef, "subface", [3, 2, undef, undef]]]
CEF2 = [1, ["L", undef, "R", 0, "subface", [undef, undef, 3, 2]]]             
        
CEF0 = [3, ["L", undef, "R", 0, "subface", [undef, undef, 1, 0]]
       , 1, ["L", 0, "R", 1, "subface", [0, 3, 5, 4]]
       , 4, ["L", 1, "R", 2, "subface", [4, 7, 9, 8]]
       ]
CEF1 = [0, ["L", 1, "R", 0, "subface", [5, 4, 0, 3]]
      , 2, ["L", 0, "R", undef, "subface", [3, 2, undef, undef]]]
CEF2 = [1, ["L", undef, "R", 0, "subface", [undef, undef, 3, 2]]]             
  */

   faces = [ [0,3,2,1], [0,1,5,4], [0,4,7,6] ];
   edges = get_edges(faces);
   echo(edges=edges);
   
   //CEF0 = get_clocked_EFs_at_pi( faces, pi=0 ); 
   CEF1 = get_clocked_EFs_at_pi( faces, pi=1 ); 
   //CEF2 = get_clocked_EFs_at_pi( faces, pi=2 ); 
   
   //echo(CEF0=CEF0);
   echo(pi=1,CEF1=CEF1);
  //echo(CEF2=CEF2);



}
184r_clockedEFs_for_mesh();