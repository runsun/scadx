include <../scadx.scad>

//1hole();
module 1hole()
{
 pts = [[0, -0.5, 3], [0, 0.5, 3], [0, 0.5, 0]
        , [0, -0.5, 0], [3, -0.5, 3], [3, 0.5, 3]
        
        , [0.5, -0.5, 2.8], [0.5, 0.5, 2.8], [0.5, 0.5, 1.8]
        , [0.5, -0.5, 1.8], [1.5, -0.5, 2.8], [1.5, 0.5, 2.8]
        ];
echo(pts=pts);           
//Red()MarkPts(pts);           

faces = [
  [0,3,2,1], [0,1,5,4],   [2,3,4,5],    // outside
  [6,7,8,9], [7,6,10,11], [11,10,9,8],  // inside
  
  [0,4,3,0,6,9,10,6]                   // front
  ,[1,2,5,1,7,11,8,7]                    // back

];

Gold(0.7)polyhedron(pts,faces);
}



//2holes();
module 2holes()
{
 pts = [[0, -0.5, 3], [0, 0.5, 3], [0, 0.5, 0]
        , [0, -0.5, 0], [3, -0.5, 3], [3, 0.5, 3]
        
        , [0.5, -0.5, 2.8], [0.5, 0.5, 2.8], [0.5, 0.5, 1.8]
        , [0.5, -0.5, 1.8], [1.5, -0.5, 2.8], [1.5, 0.5, 2.8]
        
        , [0.35, -0.5, 1.5], [0.35, 0.5, 1.5], [0.45, 0.5, 0.75]
        , [0.45, -0.5, 0.75], [1.25, -0.5, 1.5], [1.25, 0.5, 1.5]
        ];
Black()MarkPts(addy(pts,-0.2), Label=["text", range(pts), "scale", 0.75], Line=false, Ball=false);           

faces = [
  [0,3,2,1], [0,1,5,4],   [2,3,4,5],    // outside
  [6,7,8,9], [7,6,10,11], [11,10,9,8],  // inside
  [12,13,14,15],[13,12,16,17],[15,14,17,16],
  
//  [0,4,3,0, 9,10,6,9,  0,12,  15,16,12],                   // front
//  [1,2,5,1, 8,7,11,8,  1,13,  17,14,13]                    // back
 //------  xxx  -------  xxxx   -----------    
//  [0,4,3,0, 0, 9,10,6,9,  0,  15,16,12,15],                   // front
//  [1,2,5,1, 1, 8,7,11,8,  1,  17,14,13,17]                    // back

  [0,4,3,0,  0,  9,10,6,9,  0,  15,16,12,15],                   // front
  [1,2,5,1,  1,  11,8,7,11, 1,  17,14,13,17]                    // back
  //------   x   ---------  x   -----------    
  // outer   ^    inner.1          inner.2
  
    

];

Red() Arrow2( get(pts, [0,4,3,0]) );
_red( [0,4,3,0] );


Green()  Arrow( addz(get(pts, [0,9]),-0.05) );
_green( [0,9] );  

Blue() Arrow2( get(pts,[9,10,6,9])); 
_blue( [9,10,6,9] );

Purple() Arrow( addz(get(pts,[9,0]),0.065));
_purple([9,0]);

Olive() Arrow( addz(get(pts,[0,12]),-0.075));
_olive([0,12]);

Teal() Arrow2( get(pts,[12,16,15,12])); 
_teal([12,16,15,12]);

Gold()polyhedron(pts,faces);
}

function dev_tubefaces(nO=4, nI=[3,4])= // 2018.12.8
(
  /* faces are to be arranged like:
     [ top_face
     , bottom_face
     , each out_faces
     , each in_faces_0 
     , each in_faces_1
     , ...
     ]

  Points are in the following order:
        
                      .7-----8._
                  .-' |  17 |  '-._
               .-'16+----+  |      '-9
             6'     |\   |  |       /|
             |'-.   |.\__|--3._    / |   
             |   '-.|15  |14   '-_/  |
             |   .' '-.  |       / '.|
             |.'  12+--'-. 13   /   /4
            1'-.     \   |'-.5 /   /
                '-. 11\__|   |    /
                   '-.   10  |   /
                      '-.    |  / 
                         '-. | /
                            '0/
 */

 let( nI=nI==undef? [nO]: isint( nI )? [nI]:nI
     , L = nI+nO
     , top_out = range(nO,2*nO)
     , top_ins= [ for (i=range(len(nI)))
                  let( base = 2*nO+ 2*sum( slice( nI, 0,i) )+nI[i] )
                  //echo(i=i, base=base)
                  each  concat( [ nO ]
                          , [ for (j=range(base+nI[i]-1, base-1 ) ) j ]
                          , [ base+nI[i]-1 ]
                          )
                ]
     , top = concat( top_out, top_ins )
                
     , bot_out = range(nO-1,-1)
     , bot_ins= [ for (i= range(len(nI)))
                  let( base = 2*nO+ 2*sum( slice( nI, 0,i) ) )
                  //echo(i=i, base=base)
                  each concat( [ nO-1 ]
                         , [ for (j=range(base, base+nI[i]) ) j ]
                         , [ base ]
                         )
                ]
     , bot = concat( bot_out, bot_ins )
                
     , out= [for(i=range(nO))
              [ i, i==nO-1?0:i+1, (i==nO-1?0:i+1)+nO, i+nO]
            ] 
     , in = [for(i=range(nI))
              let( base = 2*nO+ 2*sum( slice( nI, 0,i) ) )
              //echo("for in", i=i, base=base)
              each [for(j=range(base, base+nI[i]))
                     [ j==base+nI[i]-1?base:j+1
                     , j
                     , j+nI[i]
                     , (j==base+nI[i]-1?base:j+1)+nI[i] ]
                   ]  
            ] 
     )
     //echo(nO=nO, nI=nI)
     //echo(top_out=top_out)
     //echo(top_ins=top_ins)
     //echo(top=top)
     //echo("---")
     //echo(bot_out=bot_out)
     //echo(bot_ins=bot_ins)
     //echo(bot=bot)
     //echo("---")
     //echo(out=out)
     //echo(in=in)
  [top, bot, each out, each in]
);



//multiholes2();
module multiholes2()
{
  echom("multiholes2()");
   h=1;
  pts_b= [ O, 4*Y, 3*X+5*Y, 2*X ];
  pts_t= addz(pts_b, h);
  pts_hole2= [ X+2.5*Y, X/2+3.5*Y, 2*X+3.5*Y];
  
  pts_hole= [ X+.5*Y, X/2+1.5*Y, 2*X+1.5*Y];
  
  pts = concat(pts_b, pts_t
              , pts_hole
              , addz(pts_hole,h)
              
              , pts_hole2
              , addz(pts_hole2,h)
              
              );
           
  //Red()MarkPts(pts);           
  
  faces = dev_tubefaces( nO=len(pts_b)
                       , nI= [len(pts_hole), len(pts_hole)]
                       , isflat=false);
  echo(faces=faces);
  
  
  Gold()polyhedron(pts,faces);
}



multiholes3();
module multiholes3()
{
  echom("multiholes3()");
   h=1;
  pts_b= [ O, 4*Y, 3*X+5*Y, 2*X ];
  pts_t= addz(pts_b, h);
  
  pts_hole= [ X+.5*Y, 0.4*X+1.2*Y, 2*X+1.5*Y];
  pts_hole2= [ .5*X+1.8*Y, .5*X+2.1*Y, 1.8*X+2.2*Y, 1.5*X+2*Y];
  
  pts_hole3= [ X+2.75*Y, X/2+3.5*Y, 2*X+3.5*Y];
  
  pts = concat(pts_b, pts_t
              , pts_hole
              , addz(pts_hole,h)
              
              , pts_hole2
              , addz(pts_hole2,h)
              
              , pts_hole3
              , addz(pts_hole3,h)
              
              );
           
  //Red()MarkPts(slice(pts, 0,14));           
  //Green()MarkPts(slice(pts, 14,22), Label=["text", range(14,22)] );           
  //Blue()MarkPts(slice(pts, 22), Label=["text", range(22, 30)] );           
  
  faces = dev_tubefaces( nO=len(pts_b)
                       , nI= [len(pts_hole), len(pts_hole2), len(pts_hole3)]
                       , isflat=false);
  echo(faces=faces);
  
  
  Gold()polyhedron(pts,faces);
}

