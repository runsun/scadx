//include <../scadx.scad>
//include <../woodworking/scadx_woodworking.scad>
//include <../../doctest/doctest.scad>
include <scadx.scad>
include <woodworking/scadx_woodworking.scad>
include <doctest.scad>

ISLOG=1;

//cutlist2d_for_1_mat_dev_1881();
module cutlist2d_for_1_mat_dev_1881()
{
   echom("cutlist_for_1_mat_dev_1881()");
   parts= [
            "A", [ "mat" , "P1x4x8"
                 , "size", [3,4] 
                 , "count", 2                        
                 , "color", "teal"
                 ]
          , "B", [ "mat" , "P1x4x8"
                 , "size",  [2,10]
                 , "count", 4
                 , "color", "purple"
                 ] 
          , "C", [ "mat" , "P1x4x8"
                 , "size",  [12,24]
                 , "count", 1
                 , "color", "blue"
                 ] 
          , "D", [ "mat", "P2"
                 , "size", [6,8] 
                 , "count", 2 
                 , "color", "olive"
                 ]
          , "E", [ "mat", "P1x4x8"
                 , "size", [6,6] 
                 , "count", 2 
                 , "color", "olive"
                 ]
          , "F", [ "mat", "P1x4x8"
                 , "size", [36,2] 
                 , "count", 3 
                 , "color", "olive"
                 ]
          ];
   mat= "P1x4x8";
   matsize= reverse( h(WOODS, mat) );
   //echo(matsize = matsize);
   matpts = cubePts(matsize);
   Line( get(matpts,[0,1,2,3]), r=0.2, color="red", closed=true );
   //color("gold",0.2) cube( matsize);
   //DrawEdges( pts=cubePts(size=matsize), faces=CUBEFACES, r=0.1 );
   
   //===============================================
   
   log_b("Parts prep");
   log_( str(_b("Extract parts: "), "---> [partname, size, count]" ) ,1);
   
   parts2= [ for( pn = keys(parts) ) 
             let( part = hash(parts, pn) 
                )
             if( (h( part,"mat")==mat) ) 
              [ pn, hash(part,"size"), hash(part,"count", 1)
            //, hash(part,"matsize")  
            ]
            //=> [ <part_name>, <part_size>, <part_count>, <part_mat_size> ]
          ];  
   //log_( ["parts2", parts2] ,1);
   
   
   //log_(layer=1);
   //log_( str(_b("Lay out all copies: "), "---> [partname, size]" ) ,1);
   parts_copies = [ for( p = parts2 ) 
              each [for (i=range( p[2]) ) [p[0],p[1]] ]
            ];
   //log_( ["parts_copies", parts_copies] ,1);
   
   
   
   //log_(layer=1);
   //log_( str(_b("Add id for each copy:"), "---> [id, partname, size]" ) ,1);
   parts_id = [ for( i= range(parts_copies) )    
              concat( [i], parts_copies[i] )
            ];
   //log_( ["parts_id", parts_id] ,1);
   
   
   //log_(layer=1);
   //log_( str(_b("Duplicate each if the x!=y in size:")
   //     , "---> [id, partname, size[0], size[1]], [id, partname, size[1],size[0]]" ) ,1);
   parts_rot = [ for( p = parts_id ) 
                 let( size = p[2]
                    , n = size[0]==size[1]?1:2
                    )
                 each [ for(i=range(n)) i==0? 
                          [p[0],p[1], p[2][0], p[2][1]]
                        : [p[0],p[1], p[2][1], p[2][0]]
                      ]    
                      //    : [p[0],p[1], reverse(p[2])] ] 
               ];
   //log_( ["parts_rot", parts_rot] ,1);

   //log_(layer=1);
   //log_( str(_b("Sort by first size:")
   //      , "---> [id, partname, size[0], size[1]], [id, partname, size[1],size[0]]" )
   //      ,1);
   parts_sort = reverse( sortArrs( parts_rot, by=2) );
   log_( ["parts_sort", parts_sort] ,1);
   
   log_e("End parts prep");
         
   echo();
   echo( str(_b("fit:")
             , "---> " ));
   function fit( parts = parts_sort, mat=[ [0,FT4], [0,0], [FT8,0] ]
               , _rtn=[], _used_parts=[], _i=-1 )=
   (
     _i==-1? 
     echo( log_b("fit init, _i=-1", 2) )
     let( p    = parts[0]
        , _used_parts = [parts[0]]
        , _parts = [for(pp=parts)
                     if(pp[0]!=p[0]) pp ]
        , _mat = [ mat[0], [0,p[3]], [p[2],p[3]], [p[2],0], mat[2] ] 
        , _rtn = [ [p[1], [p[2],p[3]], [0,0]] ] 
        )
        echo( log_(["matsize",  matsize],3) )
        echo( log_(["mat",  mat],3) )
        echo( log_(["_mat",  _mat],3) )
        echo( log_(["parts",  parts],3) )
        echo( log_(["p",  p],3) )
        echo( log_(["_used_parts", _used_parts],3) )
        echo( log_(["_parts",  _parts],3) )
        echo( log_(["_rtn",_rtn],3))
        echo(log_("Done init",2))   
        fit( parts = _parts, mat=_mat
               , _rtn=_rtn, _used_parts=_used_parts, _i=0 )
     
     
     : parts?
     echo(log_b(_red(str("_i= ", _i)), 3))
     let( maty = mat[0].y- mat[1].y 
        , matx = mat[2].x- mat[1].x
        , at = mat[1]
        , ps = [ for(p=parts) if( p[2]<=matx && p[3]<=maty) p ]
        , p = ps[0]
        , px = p[2]
        , py = p[3]
        , _used_parts = concat( _used_parts, [p] )
        , _parts = [for(pp=parts)
                     if(pp[0]!=p[0]) pp ]
        //, _mat = [ mat[0], [0,p[3]], [p[2],p[3]], [p[2],0], mat[2] ] 
        , _mat = [ mat[0]
                 , each [ [at[0],at[1]+py], [px, at[1]+py], [px, at[1]] ] 
                 , each [ for(i=[2:len(mat)-1]) mat[i] ]
                 ]
        , _rtn = concat(_rtn, [ [p[1], [p[2],p[3]], mat[1]] ] ) 
        )
        
     echo(log_(["parts",  parts],3) )
     echo(log_(["ps",  ps],3) )
     echo(log_(["p",  p],3) )
     echo(log_(["at",  at],3) )
     echo(log_(["mat",  mat],3) )
     echo(log_(["maty", maty, "matx", matx], 3))
     echo(log_(["_mat",  _mat],3) )
     echo( log_(["_used_parts", _used_parts],3) )
     echo( log_(["_parts",  _parts],3) )
     echo( log_(["_rtn",_rtn],3))
     echo(log_e("", 3))
     fit( parts = _parts, mat=_mat
               , _rtn=_rtn, _used_parts=_used_parts, _i=_i+1 )             
     
     : // exiting 
       echo( log_b( str(_red("Existing, _i="), _i),3))
       echo( log_(["mat",  mat],3) )
       echo( log_(["parts",  parts],3) )
       echo( log_(["_used_parts", _used_parts],3) )
       echo( log_(["_rtn",_rtn],3))
       echo( log_e(layer=3) )
       echo( log_e("Leaving fit()", layer=2) )
       _rtn
     
   );      
   
   cutlist = fit();
   echo( cutlist = cutlist );
   
   log_b(_b("Drawing"));
   for( p = cutlist )
   {
      log_( ["p",p] );
      name = p[0];
      xyz = concat(p[1],[1.05]);
      act = p[2];
      pts = cubePts( xyz, actions=["t", act] );
      color= [hashs(parts, [p[0],"color"] ), 0.3];
      //color= hashs(parts, [p[0],"color"] );
   
      MarkCenter( get(pts,[4,5,6,7]), Label=["text", p[0], "scale",5] );      
      translate(act)
      Cube( size=xyz, color= color);
      //Line(pts, r=0.2, color=color);
   }  
   log_e();
   
}


function get_matsize(mat, len)=
(  // Return mat size. mat is either a mat name (one of keys of WOODS) or [x,y,z]
   // Return [Len, width, height] which is [x,y,z]
   // 
   //  get_matsize("B2x4x8")     => [96, LEN4, LEN2]
   //  get_matsize("B2x4x8", 60) => [60, LEN4, LEN2]
   //  get_matsize([70,LEN4,LEN2])  => [70, LEN4, LEN2]
   //  get_matsize("B2x4x8", 80)    => [70, LEN4, LEN2]
   //
   let( _matsize = hash(WOODS,mat)
      , rtn= _matsize? reverse( slice(_matsize,0,3) ): mat
      )
   //echo("in get_matsize()", _matsize = _matsize)
   //echo("in get_matsize()", rtn = rtn)
   len? [len, rtn[1],rtn[2]]: rtn    
);

//===========================================================


//. 18cL


function cl2d_makecut_18cL( stocks // [ [0,2], [4,1] 
                              // , [1,0], [3,3] 
                              // ] 
                     , cutpoint  // [1,0]  
                     , cutsize   // [2,1]
                     )=
( // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]
  // 
  //   +------------------+     
  //   |    :        :    |    
  //   +----+ - -  - - - -+     
  // [0,2]  |        :    |     
  //        +--------+ - -|  
  //      [1,1]      |    | 
  //                 +----+  
  //               [3,0]
     
      
  // Example: makecut(stocks, [1,0], [2,1])
  //          is to convert a block [1,0],[3,3]
  //          to [1,1],[3,1], [3,0],[1,3]
  // The cutpoint [1,0] is splitted into [1,1] and [3,1]
  //          [1,0]+ [cutsize[0],0] = [3,1]
  //          [1,0]+ [0,cutsize[1]] = [1,1]
  // newstocks: [ [0, 2], [4, 1]
  //            , [3, 0], [1, 3]
  //            , [1, 1], [3, 2]]
      
  //echo(mats=mats, cutpoint=cutpoint, cutsize=cutsize)
  let( bx = stocks[0][0]+ stocks[1][0]
     , by = stocks[0][1]+ stocks[1][1]
     , _rtn=[ for(cp= keys(stocks))
             //echo(cp=cp)
             each cp==cutpoint?
                      let( cp1= cutpoint+ [ cutsize[0], 0 ]
                         , cp2= cutpoint+ [ 0, cutsize[1] ]
                         )
                      [ cp1, [bx-cp1.x, by-cp1.y]      
                      , cp2, [bx-cp2.x, by-cp2.y]      
                      ]
                          
                  : [ cp, h(stocks,cp) ]
           ]
     , rtn = [ for(k=keys(_rtn))
               if ( h(_rtn,k)[0]!=0 && h(_rtn,k)[1]!=0) // remove cases like 
                                                        // [ [8,0], [0,4] ]
               each [k, h(_rtn,k) ]
             ]      
     )
  rtn
 );

  // Testing makecut():
  // 
     //stocks= [ [0,2], [4,1]
            //, [1,0], [3,3] 
            //];
     //newstocks = makecut(stocks, [1,0], [2,1] );
     //echo(newstocks = newstocks); // [[0, 2], [4, 1], [3, 0], [1, 3], [1, 1], [3, 2]]
     //echo( newstocks== [[0, 2], [4, 1], [3, 0], [1, 3], [1, 1], [3, 2]]);         
     
   cl2d_makecut_18cL=["cl2d_makecut_18cL", "stocks,cutpoint,cutsize", "hash","cutlist"
   ,
   "// Make a cut (of size cutsize) at one of the stocks at cutpoint 
    // Return a new stocks
    //
    // stocks: hash{ cutpoint: cutsize }
    //       like: [ [0,2], [4,1] 
    //             , [1,0], [3,3] 
    //             ]      
    //     +-------------------+     
    //     |    :              |  1  
    //     +----+ - - - - - - -+     
    //   [0,2]  |              |     
    //          |              |  
    //          |              | 
    //          +--------------+  
    //        [1,0]      3
      
    // cutpoint: like [1,0]
    // cutsize : like [2,1]
    // 
    //   +------------------+     
    //   |    :        :    |    
    //   +----+ - -  - - - -+     
    // [0,2]  |        :    |     
    //        +--------+ - -|  
    //      [1,1]      |    | 
    //                 +----+  
    //               [3,0]
   "];

   function cl2d_makecut_18cL_test( mode, opt=[])=
   (
      doctest( cl2d_makecut_18cL, 
      [   
        [ cl2d_makecut_18cL( [[0,2],[4,1],[1,0],[3,3]], [1,0], [2,1] )
        , [[0,2],[4,1], [3,0],[1,3], [1,1],[3,2]]
        , "[[0,2],[4,1],[1,0],[3,3]], [1,0], [2,1]"
        ]
      ]	
      ,mode=mode, opt= opt 
      ,scope=[]
      )
   );   
   //echo( cl2d_makecut_18cL_test(mode=12)[1] );
   
   
   
function cl2d_fitness_18cL( stocksize // [x,y] 
                     , cutsize   // [x,y]       
                     )=
(/* Check how well a cut of cutsize fits to the stocksize 
  Return an array [a,b]
  
  where a,b are fitness for :
    (a) cut piece is as it is or 
    (b) is rotated by 90 degree
    
  The larger a or b is, the better fit.   
     
           sy +---------------+
        a     |               |
           cy +---------+     |
              |         |     |  
        b     |         |     |
              |         |     |
              +---------+-----+
                        cx    sx
                   c       d
                                   
   The init fitness f0 is defined as:
          
      f0= 1-((sx-cx)/sx) * ((sy-cy)/sy)
        = 1- a*d/(a+b)/(c+d)
        
   The larger the f0, the better fit of cut 
       
   if oversized ( cx>sx or cy>sy ): undef

   If f0=1, means one of the following:
   
      +--+------+   +----------+ 
      |  |      |   |          |
      |  |      |   |          |
      +--+------+   +----------+ 
                    |          |
                    +----------+
   
   In these cases, additional value is added to f0 to distinguish:
   
    //         A            B   
    // cy= sy +--+-----+   +--+------------+
    //        |  |     |   |  |            |
    //        |  |     |   |  |            |
    //        +--+-----+   +--+------------+
    //          cx    sx      cx           sx     
   
   to make f_A > f_B
   
 */
 let( sx = stocksize.x
    , sy = stocksize.y
    , cx = cutsize.x 
    , cy = cutsize.y 
    
    // The following criteria puts case A ahead of B, which is not what we want:
    //
    // , f0= (1-(sx-cx)/sx) + (1-(sy-cy)/sy)
    // , f1= (1-(sx-cy)/sx) + (1-(sy-cx)/sy)
    //
    // or
    //
    // , f0= (1-(sx-cx)/sx)*(1-(sy-cy)/sy)
    // , f1= (1-(sx-cy)/sx)*(1-(sy-cx)/sy)
    //
    // or
    //
    // , _f0= (cx/sx) * cy/sy 
    // , _f1= (cy/sx) * cx/sy 
    //
    //        A             B   
    //   +-----+    
    //   +--+  |    +--+-------------+    
    //   |  |  |    |  |             |    
    //   |  |  |    |  |             |    
    //   +--+--+    +--+-------------+   
    
    
    , _f0= 1-((sx-cx)/sx) * ((sy-cy)/sy) 
    , _f1= 1-((sx-cy)/sx) * ((sy-cx)/sy) 
    
    
    // For cases like:
    //         A            B   
    // cy= sy +--+-----+   +--+------------+
    //        |  |     |   |  |            |
    //        |  |     |   |  |            |
    //        +--+-----+   +--+------------+
    //          cx    sx      cx           sx
    //            
    //   They both have 1-rx*ry = 1, but obviously we want A to have higher fit
    //   (fill and use up the smaller stock first)
    //   
    //   So if f=1, we do: 
    //   
    //     f = 1+ cx/sx
    //
    , factor=.5
    , f0= _f0==1? (1+ ( cx==sx?cy/sy:factor*cx/sx )):_f0
    , f1= _f1==1? (1+ ( cy==sx?cx/sy:factor*cy/sx )):_f1
    )
 //echo(s=s,c=c,sx=sx,sy=sy)
 [ sx<cx || sy<cy ? undef: f0  
 , sx<cy || sy<cx ? undef: f1  
 ] 
);
   
   cl2d_fitness_18cL=["cl2d_fitness_18cL", "stocksize,cutsize", "array","cutlist"
   ,
   "Check how well a cut of cutsize fits to the stocksize 
  Return an array [a,b]
  
  where a,b are fitness for :
    (a) cut piece is as it is or 
    (b) is rotated by 90 degree
    
  The larger a or b is, the better fit.   
     
           sy +---------------+
        a     |               |
           cy +---------+     |
              |         |     |  
        b     |         |     |
              |         |     |
              +---------+-----+
                        cx    sx
                   c       d
                                   
   The init fitness f0 is defined as:
          
      f0= 1-((sx-cx)/sx) * ((sy-cy)/sy)
        = 1- a*d/(a+b)/(c+d)
        
   The larger the f0, the better fit of cut 
       
   if oversized ( cx>sx or cy>sy ): undef

   //--------------------
   
   If f0=1, means one of the following:
   
      +--+------+   +----------+ 
      |  |      |   |          |
      |  |      |   |          |
      +--+------+   +----------+ 
                    |          |
                    +----------+
   
   In cases A and B:
   
            A            B   
    cy= sy +--.-----+   +--.------------+
           |  :     |   |  :            |
           |  :     |   |  :            |
           +--'-----+   +--'------------+
             cx    sx      cx           sx     
            a    b       a       b 
                             
   Both have f0= 1, but obviously we want A to have higher fit
   (fill and use up the smaller stock first)
   
   In these cases, additional value is added to f0 to make f_A > f_B:
   
     f = f0+ a/(a+b) = 1+ a/(a+b)
   
   "];

   function cl2d_fitness_18cL_test( mode, opt=[])=
   (
      doctest( cl2d_fitness_18cL, 
      [ "Return [a,b] representing the fit scale (smaller =better) of 'as it is' or 'rot90'"
      , [ cl2d_fitness_18cL( [8,4],[4,2] ),[0.75,1.25],"[8,4],[4,2]", ["//","2nd>1st item, means rot90 (2nd item) makes better fit"]]
      , [ cl2d_fitness_18cL( [8,4],[2,4] ),[1.25,0.75],"[8,4],[2,4]"]
      , [ cl2d_fitness_18cL( [8,4],[8,1] ),[1.25,undef],"[8,4],[8,1]", ["//","undef=oversize"]]
      , [ cl2d_fitness_18cL( [8,4],[1,8] ),[undef,1.25],"[8,4],[1,8]"]
      ]	
      ,mode=mode, opt= opt 
      ,scope=[]
      )
   );
   //echo( cl2d_fitness_18cL_test(mode=12)[1] );
   
   
   
   
function cl2d_sorted_fitnesses_18cL( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y ]
                       )=
( /*
    Decide which stock is the best fit for the cutsize.
    Return a sorted array [ ["fitness",f, "stock",i, "rot",j ]
                          , ["fitness",f, "stock",i, "rot",j ]
                          , ...]
   (sorted based on f, from small to large)                       

   For special cases where f=1 like case A and B below:

    A             B   
   +--.-----+    +--.-------------+
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   +--.-----+    +--.-------------+
    
   f= 1 for both cases. But obviously we want A to have the priority
   (fill and used up the smaller stock first)
   
   So f=1 needs special treatment. 
    
    
 */
  let(  
       fits= [ for(i = range(stocksizes) )
                 let(fs = cl2d_fitness_18cL( stocksize = stocksizes[i], cutsize = cutsize ))
                 // fs = [ [a,b], [c,undef], [d,e] ...]
                 each [ for(f=range(fs))
                         if( fs[f]!=undef ) [fs[f],[i,f]]
                      ]    
              ]
             // array [ ["fitness",f, "stock",i, "rot",j ]
             //       , ["fitness",f, "stock",i, "rot",j ]
             //       , ...] 
     , sfits = quicksort(fits)
     
     , rtn = [ for(x=sfits)
               ["fitness", x[0], "stock", x[1][0], "rot", x[1][1] ]
             ]          
     )
   rtn
);

   cl2d_sorted_fitnesses_18cL=["cl2d_sorted_fitnesses_18cL", "stocksizes,cutsize", "array","cutlist"
   ,
   " Decide which stock is the best fit for the cutsize.
    
     Return array of fitness hashes, sorted by the fitness (from small to large):
     
       [ ['fitness', 0.75, 'stock', 1, 'rot', 0] 
       , ...
       ]
    
    The last item is the best fit.   
    "];

   function cl2d_sorted_fitnesses_18cL_test( mode, opt=[])=
   (
      doctest( cl2d_sorted_fitnesses_18cL, 
      [ "Decide which stock is the best fit for the cutsize"
      , "<u>['fitness',0.9, 'stock',2, 'rot',1]</u> means:"
      , "When fitted to stocks[2], a rotated( rot=1, means by 90d) cutsize has fitness=0.9", 
      , [ cl2d_sorted_fitnesses_18cL( [[4,3],[6,4]], [3,2])
        , [ ["fitness", 0.75, "stock", 1, "rot", 0]
          , ["fitness", 0.833333, "stock", 1, "rot", 1]
          , ["fitness", 0.916667, "stock", 0, "rot", 0]
          , ["fitness", 1.5, "stock", 0, "rot", 1]
          ]
        , "[[4,3],[6,4]], [3,2]"
        , ["asstr",true, "nl",1]
        ]                   
      ]	
      ,mode=mode, opt= opt 
      ,scope=[]
      )
   );
   //echo( cl2d_sorted_fitnesses_18cL_test(mode=12)[1] );
   

//cl2d_Draw_sorted_fitnesses([[5,4],[8,4],[5,2]],[3,2]);
//cl2d_Draw_sorted_fitnesses([[5,4],[8,4],[5,2]],[4,2]);
//cl2d_Draw_sorted_fitnesses([[5,4],[8,4],[5,2]],[3,3]);
//cl2d_Draw_sorted_fitnesses([[5,4],[8,4],[5,2]],[3.8,1]);
//cl2d_Draw_sorted_fitnesses([[5,4],[8,4],[5,2]],[4,1]);
module cl2d_Draw_sorted_fitnesses( stocksizes, cutsize )
{
  //
  // Display stocksizes, cutsize to debug cl2d_sorted_fitnesses_18cL()
  //
  echom("cl2d_Draw_sorted_fitnesses()");
  translate(-Y) Black()
  Text( _s("Fitness of a block {_} on different stocks {_}", [cutsize, stocksizes] ) 
      , scale=0.5
      //, site= [ X-Y, -Y, 0]
      );
  
  // x0s: where block starts 
  x0s = [for(i=range(stocksizes))
        i==0?0:(sum( [for(j=[0:i-1])stocksizes[j][0] ] )+1*i)
       ];
  echo(x0s = x0s);
       

  fs = cl2d_sorted_fitnesses_18cL( stocksizes, cutsize);
  echo(fs = fs );
  
  for(i = range(fs))  // f= [["fitness", 0, "stock", 0, "rot", 1]
  {
     f=fs[i];
     _color(str("f=",f), COLORS[i+1]);
     x0 = x0s[ h(f,"stock") ];
     x = cutsize[h(f,"rot")? 1:0];
     y = cutsize[h(f,"rot")? 0:1];
     pts = [ x0*X, x0*X+y*Y, (x0+x)*X+y*Y, (x0+x)*X ];
     color( COLORS[i+1]) {
       MarkPts( pts, Label=false, Line=["closed",1] );
       MarkPt( (x0+x)*X+y*Y, h(f,"fitness") );
     }
  }
  
  for(i=range(stocksizes))
  {
     st= stocksizes[i];
     x= x0s[i];
     pl= [ [x,0,0], [x,st[1],0], [x+st[0],st[1],0], [x+st[0],0,0] ];
     echo(x=x, pl=pl);
     //translate([x,0,0]) 
     //MarkPts(pl, Label=false, Ball=false, Line=["closed",1], r=0.1); 
     color(COLORS[i+1], 0.1) Plane( pl );
  }
  
}
  
  
function cl2d_18cL( stocksize // [x,y] 
                  , parts // [ [x,y], [x,y] .... ]
                  , _stocks // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _cuts   // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]          
                  , _i=-1
                  )=
(
  _i==-1? // init 
   echo(log_b(_ss("<b>cl2d_18cL</b>(stocksize={_:b},parts={_:b}, _stocks={_:c=red}, _cuts={_:c=red}): init", [stocksize,parts,str(_stocks),str(_cuts)]),0))  
   let( //_stock = stock.x<stock.y? // Make sure landscape view
        //         [stock.y,stock.x]:stock
        _stocks = [ [0,0], stocksize ]
      , _parts = reverse(
                   quicksort([  // Convert parts to sorted 
                              //   [ [[x,y],3], [[x,y],1], [[x,y],2] .... ]
                              // where 3,1,2 are the original index   
                    for( i=range(parts)) 
                        [ parts[i].x<parts[i].y  // flip x,y if y > x, for sort
                            ? [parts[i].y,parts[i].x]
                            : parts[i]
                        , i
                        ]  
                  ])
                )  
      )
      cl2d_18cL( stocksize=stocksize
                , parts=_parts
                , _stocks= _stocks // [ [0,0], [x,y] ]
                , _cuts= [] 
                , _i=0 )
          
 :_i<len(parts)?
   echo(log_b(_ss("<b>cl2d_18cL</b>(_stocks={_:c=red}, _cuts={_:c=red}, _i={_:b;c=red} ): init", [_stocks,_cuts, str(_i)]),1))  
        
   let( part= parts[_i]  // [part_size([x,y]), part_index]
      , stocksizes = vals( _stocks )
      , cutsize = part[0]
      , fitnesses = cl2d_sorted_fitnesses_18cL( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       )
                    // fitnesses: arr of ["fitness", 0.833333, "stock", 1, "rot", 1]
                    
      , target = last( fitnesses ) // The chosen stock with the largest fitness
      
      , stock_i = h(target, "stock")
      , cutpoint = keys( _stocks )[stock_i]
      , _cutsize= h(target, "rot")? reverse(cutsize): cutsize               
                       
      , __stocks = cl2d_makecut_18cL( _stocks // [ [0,2], [4,1] 
                              // , [1,0], [3,3] 
                              // ] 
                     , cutpoint  // [1,0]  
                     , _cutsize   // [2,1]
                     )                 
      , __cuts = concat(_cuts, [cutpoint, _cutsize ] )                     
      )
      echo(log_(["parts", parts],1 ))
      echo(log_(["cutpoint", cutpoint, "cutsize", cutsize],1 ))
      echo(log_(["__stocks", __stocks],1))
      echo(log_(["__cuts", __cuts],1))
      echo(log_e(str("End of _i=",_i),1)) 
      cl2d_18cL( stocksize=stocksize
                , parts=parts
                , _stocks= __stocks // [ [0,0], [x,y] ]
                , _cuts= __cuts 
                , _i=_i+1 )

   
  : 
  echo( log_(["_stocks", _stocks, "_cuts", _cuts],0))
  echo(log_e("Leaving cl2d_18cL()",0))
  [_stocks,_cuts]
);  



//cl2d_18cL_test();
module cl2d_18cL_test()
{

   echom("cl2d_18cL_test()");
   
   stocksize = [8,4];
   //parts = [ [2,3], [5,4], [1,2] ]; // nice
   
   
   //=========================================
   parts = [ [2,3], [2,3], [1,2] ]; // last cutpoint error
   /*
      parts = [ [2,3], [2,3], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [2, 3], [2, 0], [2, 3], [2, 3], [2, 1]]
      _stocks=[[4, 0], [4, 4], [4, 3], [4, 1], [0, 3], [8, 1]]
      
         Error _stocks [[4, 3], [4, 1]] causing the [2,1] cut 
         up in the wrong place
      
      .---.---+=======+---.---.---.---.
      |   :   |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |      -|---.---.---.---.
      |       |       |   :   :   :   :
      |       |       |---.---.---.---.
      |       |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      
   */
   
   //=========================================
   //parts = [ [2,3], [5,1], [1,2] ];  
   /* parts = [ [2,3], [5,1], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
          f0= _f0==1? (1+ ( cx==sx?cy/sy:cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:cy/sx )):_f1
          
          
      cuts = [[0, 0], [5, 1], [5, 0], [3, 2], [5, 2], [1, 2]]
      stocks = [[6, 2], [2, 2], [0, 1], [8, 3]]  <=== wrong !!!
               should have been:
               [[6, 2], [2, 2], [0, 1], [5, 3]]
               
      .---.---.---.---.---+---+---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---|   |---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---+===+=======+
      |   :   :   :   :   |           |
      +===================+           |
      |                   |           |
      +===================+===========+
                                      
      Consider fix: when placing [3,2] on either [8,3] or [3,5]:
   
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      +===================+---.---.---.
      |                   |   :   :   :
      +===================+---.---.---.
     
      [8,3] is filled first. 
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [5, 1], [0, 1], [2, 3], [5, 0], [2, 1]]    
      _stocks=[[7, 0], [1, 4], [5, 1], [3, 3], [2, 1], [6, 3]]
      
      _stocks should have been:
                [[7, 0], [1, 4], [2, 1], [6, 3]]
          
      +=======+---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      +=======+===========+=======+---.
      |                   |       |   :
      +===================+=======+---.

   
   
   */
   
   //stocksize = [12,4];
   //parts = [ [2,3], [5,1], [1,2] ]; 
   
   //parts=[ [2,4], [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ]; // Error: cutpoint overlap
   //parts=[ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ];
   /*
      +=======+---.---.---+=======+===+
      |       |   :   :   |       |   |
      |       |---.---.---|       |   |
      |       |   :   :   |       |   |
      |       |---.---.---+=======+===+
      |       |   :   :   |       |   |
      +=======+===========+       |   |
      |                   |       |   |
      +===================+=======+===+
   
   Note: this doesn't look a good arrangement. HOWEVER, 
      it is better than what is thought to be a good one:
      
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |=======+=======+---.---.
      |       |       |       |   :   :
      |       |       |      -|---.---.
      |       |       |       |   :   :
      +=======+=======+===+===+===+---.
      |                   |       |   :
      +===================+=======+---.
   
   because it leaves out a square block. 
   
   */
   
   //parts= repeat( [ [1,3] ], 8 ); // ERROR cutpoints overlap !!! 
   //parts= concat( repeat( [ [1,2] ], 5 )
   //             , repeat( [ [1,3] ], 4 ) ); // ERROR cutpoints overlap !!! 
   
   cl2d= cl2d_18cL( stocksize // [x,y] 
                  , parts 
                  );
                  
   cuts= cl2d[1];
   
   x=stocksize[0];
   y=stocksize[1];
   for(r= range(y+1) ) Line( [O+r*Y, x*X+r*Y]  );
   for(c= range(x+1) ) Line( [O+c*X, c*X+y*Y]  );
   
   for( i = range(keys(cuts))) //x0= keys(cuts) )
   {
      x0= cuts[2*i];
      _x0 = addz(x0,0);
      size = h(cuts, x0); 
      x=size.x;
      y=size.y;
      pts= addz( [ _x0, _x0+y*Y, _x0+x*X+y*Y, _x0+x*X], i*0.2) ;
      echo(x0=x0, size=size, pts=pts);
      color(COLORS[i+1])Plane(pts);
      Black() MarkCenter( addz(pts,0.2), Label=["text",str(x,",",y),"scale",1.5]);
   }
}


//. 18cp

function cl2d_makecut_18cp( stocks // [ [0,2], [4,1] 
                                   // , [1,0], [3,3] 
                                   // ] 
                     , cut  // [ [1,0], [2,1] ]  = [cutpt, cutsize]
                     )=
( /*
  ====================================================                 
     This version takes care of the following 2 types of overlapping blockings:
     (Use the makecut_tester() to test)

       +------------------------+   [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]  
       |    :         :         |        left          self         right
       |    +=========+[3,4]    |    [0,3],[1,2]                [3,0],[2,5] 
       |    |         |         |    [0,4],[5,1]
       +----+         | - - - - |     
     [0,3]  |         |         |     
            |         |         |                 
            |         |         |    
            +=========+ - - - - |                   
          [1,1]       |         |                           
                      +---------+                      
                   [3,0] 
                   
       +------------------------+  [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]   
       |    :         :         |    
       |    :         :         |    [0,3],[5,2]
       |    :         :         |                              [3,0],[2,1]
       +----+==============+ - -|                              [4,0],[1,5] 
     [0,3]  |              |    |     
            |              |    |     
            |              |    |     
            +==============+- - |  
          [1,1]       |         | 
                      +---------+                      
                   [3,0]
    
  ====================================================                 
  */
  
  
  // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]
  // 
  //   +------------------+     
  //   |    :        :    |    
  //   +----+ - -  - - - -+     
  // [0,2]  |        :    |     
  //        +--------+ - -|  
  //      [1,1]      |    | 
  //                 +----+  
  //               [3,0]
     
      
  // Example: makecut(stocks, [1,0], [2,1])
  //          is to convert a block [1,0],[3,3]
  //          to [1,1],[3,1], [3,0],[1,3]
  // The cutpoint [1,0] is splitted into [1,1] and [3,1]
  //          [1,0]+ [cutsize[0],0] = [3,1]
  //          [1,0]+ [0,cutsize[1]] = [1,1]
  // newstocks: [ [0, 2], [4, 1]
  //            , [3, 0], [1, 3]
  //            , [1, 1], [3, 2]]
      
  /* ==================================================
     This version (18cp) hopes to solve hard cases like below :
  
    (A) [ [0,2],[4,1], [1,0],[3,3] ] 
       => [ [0,2],[4,1]
          , [2,0],[2,3] ] 
        (but not [ [0,2],[4,1]
                 , [1,2], [3,1]
                 , [2,0],[3,3] ] )
    
       +-------------------+     
       |                   |     
       +----+====+- - - - -+     
     [0,2]  |    |         |     
            |    |         |  
            |    |         | 
            +----+---------+  
          [1,0]       
  
    (B) [ [0,2],[4,2], [1,0],[3,4] ] 
        => [ [2,3],[2,1]
           , [1,0],[3,3] <== exception to the full-size rule ([4,3] instead of [4,4])
           , [2,0],[2,4]  
           ] 
        (but not [ [0,2],[4,1]
                 , [1,2], [3,1]
                 , [2,0],[3,3] ] )
    
       +=========+---------+     
       |         |         |     
       +=========+- - - - -+     
     [0,3]  |              |     
            |              |  
            |              |  
            |              |  
            |              | 
            +----+---------+  
          [1,0]      
    
    
     (C) [ [0,2],[4,1], [1,1],[3,2], [3,0],[1,3] ] 
        => [ [2,3],[2,1]
           , [1,1],[2,2] <== exception to the full-size rule ([3,3] instead of [4,4])
           ] 
        (but not [ [0,2], [4,1]
                 , [1,1], [3,2]
                 , [2,0], [1,1] 
                 ] ) 
    
         +------------------+     
         |    :        :    |    
         +----+ - -  - - - -+     
       [0,2]  |        :    |     
              +--------+ - -|  
            [1,1]      |    | 
                       +----+  
                     [3,0]  
                     
         +------------------+     
         |    :        :    |    
         +----+ - -  - +====+     
       [0,2]  |        |    |     
              +--------+    |  
            [1,1]      |    | 
                       +====+  
                     [3,0]  
  
  
  
  */
 
    /*
      2018.12.28: new approach
      Block of 5x5
                                     origin: [0,0],[5,5] 
       +------------------------+     
       |    :         :         |    cut: [0,0],[1,3]
       |    :         :         |    new: [ [0,3], [ [1,2]  --- min block
       |    :         :         |                  , [5,2]  --- max block
       + - -+ - - - - - - - - - |                  ] 
       |    :         :         |                  
       |    :         :         |         , [1,0], [ [4,3]
       |    :         :         |                  , [4,5]
       |    :         :         |                  ]
       |    :         :         |         ]          
       +----+---------+---------+                      
     [0,0]          
     
       [5,0],[0,0],[0,5],[5,5]
                               
                                     origin: [0,0],[5,5] 
       +------------------------+    old: [ [0,3],[5,2], [1,0],[4,5] ] 
       |    :         :         |    cut: [0,0],[1,3]
       |    :         :         |    new: [ [0,3], [ [1,2]  --- min block
       |    :[1,3]    :         |                  , [5,2]  --- max block
       +----+ - - - - - - - - - |                  ] 
     [0,3]  |         :         |                  
            |         :         |         , [1,0], [ [4,3]
            |         :         |                  , [4,5]
            |         :         |                  ]
            |         :         |         ]          
            +---------+---------+                      
          [1,0]                          
                                         
       [5,0],  [1,0],[1,3],[0,3],  [0,5],[5,5]
               ^^^^^^^^^^^^^^^^^
               
               
       +------------------------+     
       |    :         :         |    
       |    :         :         |    
       |    :         :         |    
       +----+ - - - - - - - - - |     
     [0,3]  |         :         |     
            |         :         |     
            |         :         |     
            +---------+ - - - - |  
          [1,1]       |         | 
                      +---------+                      
                   [3,0]               
               

       [5,0],  [1,0],[1,3],[0,3],  [0,5],[5,5]
               ^^^^^^^^^^^^^^^^^

       [5,0],        [1,3],[0,3],  [0,5],[5,5]
                     ^^^^^^^^^^^
           [3,0],[1,1]    

               
  */     
     
  /*
      Block of 5x5
  
       +------------------------+    [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ] 
       |    :         :         |    
       |    :         :         |    
       |    :         :         |    
       +----+ - - - - - - - - - |     
     [0,3]  |         :         |     
            |         :         |     
            |         :         |     
            +---------+ - - - - |  
          [1,1]       |         | 
                      +---------+                      
                    [3,0]
                   
                   
                                     
       +------------------------+    [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ] 
       |    :         :         |    cut = [1,1],[1,1]
       |    :         :         |    
       |    :         :         |    [ [0,3],[5,2],              [3,0],[2,5] ]
       +----+ - - - - - - - - - |     
     [0,3]  |         :         |         , [2,1],[3,4],  [1,2],[4,3] 
       [1,2]+====+ - -: - - - - |     
            |    |    :         |    This is solved in 18cL. 
            +====+----+ - - - - |  
               [2,1]  |         | 
                      +---------+                      
                    [3,0]
                   
       +=========+--------------+   [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]  
       |         |    :         |   cut = [ [0,3],[2,2] ] 
       |         |    :         |                               [3,0],[2,5]
       |         |    :         |     [2,3],[3,2]
       +=========+- - - - - - - |                 [1,1],[4,2]
     [0,3]  |         :         |                 [2,1],[3,4] 
            |         :         |     
            |         :         |     
            +---------+- - - - -|  
          [1,1]       |         | 
                      +---------+                      
                   [3,0]
                   
                   
       +------------------------+   [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]  
       |    :         :         |    
       |    :         :         |     [0,3],[5,2]
       |    :         :         |                 [1,1],[2,4]
       +----- - - - - - - - - - |                 [1,2],[2,3] 
     [0,3]  |         :         |                              [3,2],[2,3]
            |         +=========+     
            |         |         |     
            +---------|         |  
          [1,1]       |         | 
                      +=========+                      
                   [3,0]
                   
       +------------------------+   [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]  
       |    :         :         |      acp         ^^^^^^^^^^^   pcp  
       |    :         :         |                   cp     
       |    :         :         |    cut = [1,1],[2,2]  ==> tr= topright= [3,3]
       +----+=========+ - - - - |     
     [0,3]  |         |         |    [0,3],[5,2]                [3,0],[2,5] 
            |         |         |     
            |         |         |    tr.y = acp.y  => pre ([0,3],[5,2]) unchanged
            +=========+ - - - - |    tr.x = pcp.x  => next ([3,0],[2,5]) unchanged 
          [1,1]       |         | 
                      +---------+    self(=[1,1],[1,4]) disappears                   
                   [3,0]
                   
       +------------------------+   [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]  
       |    :         :         |        left          self         right
       |    +=========+[3,4]    |    [0,3],[1,2]                [3,0],[2,5] 
       |    |         |         |    [0,4],[5,1]
       +----+         | - - - - |     
     [0,3]  |         |         |     
            |         |         |                 
            |         |         |    
            +=========+ - - - - |                   
          [1,1]       |         |                           
                      +---------+                      
                   [3,0]                                             
      
      [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]     [1,1],[2,3]
           left          self         right            cut
     site   [0,3]       [1,1]         [3,0]            [1,1]
     size   [5,2]       [4,4]         [2,5]            [2,3]
     tr     [5,5]       [5,5]         [5,5]            [3,4]
           
     cut.tr.x(3) = right.site.x ==> right unchanged
     cut.tr.y(4) > left.site.y(3)                     
                   ==> left splits to [0,3],[1,2]   A   tr=[1,5]
                                      [0,4],[5,1]   B   tr=[5,5]    
                   A.site([0,3]) = left.site
                   A.size.x(1) = self.site.x
                   A.size.y(2) = left.size.y
                   
                   B.site.x(0)= left.site.x
                   B.site.y(4)= self.tr.y
                   B.size.x(5)= left.size.x
                   B.size.y(1)= left.tr.y(5)- self.tr.y(4) 
                          
     cut.tr.x(3) = r.x ==> right unchanged
     cut.tr.y(4) > l.y(3)                     
                   ==> left splits to [0,3],[1,2]   A   tr=[1,5]
                                      [0,4],[5,1]   B   tr=[5,5]    
                   A.site([0,3]) = l.xy
                   A.sx(1) = s.x
                   A.sy(2) = l.sy
                   
                   B.site.x(0)= left.site.x
                   B.site.y(4)= self.tr.y
                   B.size.x(5)= left.size.x
                   B.size.y(1)= left.tr.y(5)- self.tr.y(4)        
                   
                   
                   
       +------------------------+  [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]   
       |    :         :         |    
       |    :         :         |    [0,3],[5,2]
       |    :         :         |                              [3,0],[2,1]
       +----+==============+ - -|                              [4,0],[1,5] 
     [0,3]  |              |    |     
            |              |    |     
            |              |    |     
            +==============+- - |  
          [1,1]       |         | 
                      +---------+                      
                   [3,0]

      [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]     [1,1],[32]
           left          self         right            cut
     site   [0,3]       [1,1]         [3,0]            [1,1]
     size   [5,2]       [4,4]         [2,5]            [3,2]
     tr     [5,5]       [5,5]         [5,5]            [4,3]
           
     cut.tr.x(4) > right.site.x(3)                     
                   ==> right splits to [3,0],[2,1]   A   tr=[5,1]
                                       [4,0],[1,5]   B   tr=[5,5]    
                   A.site([3,0]) = right.site
                   A.size.x(2) = right.size.x
                   A.size.y(1) = self.site.y
                   
                   B.site.x(4)= right.tr.x(5)- self.tr.x(1)
                   B.site.y(0)= right.site.y
                   B.size.x(5)= right.tr.x(5)-
                   B.size.y(1)= left.tr.y(5)- self.tr.y(4)
                   
                   
                   
                   
                           
     cut.tr.x(3) = right.site.x ==> right unchanged
                                      
                   
                   
                   
       +------------------------+     
       |    :         :         |    
       |    :         :         |    
       |    :         :         |    
       +----+ - - - - - - - - - |     
     [0,3]  |         :         |     
            |         :         |     
            |         :         |     
            +---------+ - - - - |  
          [1,1]       |         | 
                      +---------+                      
                   [3,0]
                   
       +------------------------+     
       |    :         :         |    
       |    :         :         |    
       |    :         :         |    
       +----+ - - - - - - - - - |     
     [0,3]  |         :         |     
            |         :         |     
            |         :         |     
            +---------+ - - - - |  
          [1,1]       |         | 
                      +---------+                      
                   [3,0]
                   
       +------------------------+     
       |    :         :         |    
       |    :         :         |    
       |    :         :         |    
       +----+ - - - - - - - - - |     
     [0,3]  |         :         |     
            |         :         |     
            |         :         |     
            +---------+ - - - - |  
          [1,1]       |         | 
                      +---------+                      
                   [3,0]
  
       +------------------+     
       |    :        :    |    
       +----+ - -  - - - -+     
     [0,2]  |        :    |     
            +--------+ - -|  
          [1,1]      |    | 
                     +----+  
                   [3,0]
       
  
       +=========+--------+     
       |         |   :    |    
       +=========+ - - - -+     
     [0,3]  |        :    |
            |        +====+
            +--------|    |  
          [1,1]      |    | 
                     +====+  
                   [3,0]
         
  
       +------------------+     
       |    :        :    |    
       +----+========+ - -+     
     [0,2]  |        |    |     
            +========+ - -|  
          [1,1]      |    | 
                     +----+  
                   [3,0]  
  
       +-----------------------+     
       |    :        :         |    
       |    :        :         |    
       |    :        :         |    
       +----+ - -  - - - - - - +     
     [0,3]  |        :         |     
            |        :         |     
            |        :         |     
            +--------+ - - - - |  
          [1,1]      |         | 
                     +-- ------+  
                   [3,0]
  
  */   
      
  //echo(mats=mats, cutpoint=cutpoint, cutsize=cutsize)
  
  /*
    /*
      2018.12.26: maybe new data type
      Block of 5x5
                                     origin: [0,0],[5,5] 
       +------------------------+    old: [ [0,3],[5,2], [1,0],[4,5] ] 
       |    :         :         |    cut: [0,0],[1,3]
       |    :         :         |    new: [ [0,3], [ [1,2]  --- min block
       |    :         :         |                  , [5,2]  --- max block
       +----+ - - - - - - - - - |                  ] 
     [0,3]  |         :         |                  
            |         :         |         , [1,0], [ [4,3]
            |         :         |                  , [4,5]
            |         :         |                  ]
            |         :         |         ]          
            +---------+---------+                      
          [1,0]                          
                                         
  
  
       +------------------------+    old: [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ] 
       |    :         :         |    
       |    :         :         |    new: [ [0,3], [ [1,2]  --- min block
       |    :         :         |                  , [3,2]
       +----+ - - - - - - - - - |                  , [5,2]  --- max block 
     [0,3]  |         :         |                  ]
            |         :         |         , [1,1], [ [2,2]
            |         :         |                  , [2,4]
            +---------+ - - - - |                  , [4,2]
          [1,1]       |         |                  , [4,4]
                      +---------+                  ]    
                    [3,0]                 , [3,0], [ [2,1]
                                                   , [2,3]
                                                   , [2,5]
                                                   ]
                                          ]          
  
  */
   /*
   
       +------------------------+   [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]  
       |    :         :         |        left          self         right
       |    +=========+[3,4]    |    [0,3],[1,2]                [3,0],[2,5] 
       |    |         |         |    [0,4],[5,1]
       +----+         | - - - - |     
     [0,3]  |         |         |     
            |         |         |                 
            |         |         |    
            +=========+ - - - - |                   
          [1,1]       |         |                           
                      +---------+                      
                   [3,0]                                             
      
      [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]     [1,1],[2,3]
           left          self         right            cut
     site   [0,3]       [1,1]         [3,0]            [1,1]
     size   [5,2]       [4,4]         [2,5]            [2,3]
     tr     [5,5]       [5,5]         [5,5]            [3,4]
           
     cut.tr.x(3) = right.site.x ==> right unchanged
     cut.tr.y(4) > left.site.y(3)                     
                   ==> left splits to [0,3],[1,2]   A   tr=[1,5]
                                      [0,4],[5,1]   B   tr=[5,5]    
                   A.site([0,3]) = left.site
                   A.size.x(1) = self.site.x
                   A.size.y(2) = left.size.y
                   
                   B.site.x(0)= left.site.x
                   B.site.y(4)= self.tr.y
                   B.size.x(5)= left.size.x
                   B.size.y(1)= left.tr.y(5)- self.tr.y(4) 
   
     */
     echo(str("markcut_18cp(",_s("stocks={_}, cut={_})",[ str(stocks),str(cut)]))) 
     let( _stocks = sortByCutptX( stocks )
        , cps= keys(_stocks)
        , cp= cut[0]
        , cutsize = cut[1] 
        , cut_tr= sum(cut)  // top-right corner
        
        , cut_i = idx( cps, cp )
        
        , L_cp=  _stocks[2*(cut_i-1)]
        , L_size= _stocks[2*(cut_i-1)+1] 
        , L_tr = L_cp + L_size
        
        , R_cp= _stocks[2*(cut_i+1)]
        , R_size= _stocks[2*(cut_i+1)+1] 
        , R_tr = R_cp +R_size
        
        , isOverSizeX = cut_tr.x> R_cp.x //right.x
        , isOverSizeY = cut_tr.y> L_cp.y //left[0].y
        
        , isMatchSizeX = cut_tr.x== R_cp.x
        , isMatchSizeY = cut_tr.y== L_cp.y
        
        , _rtn= isOverSizeY? // we assume that only one to the left of cp
                             // is oversized. In reality there maybe more
                             // but we settle with just one for now
               let(tmp= [
                         for(i=range(cps))
                           echo( _b("Loop_cps"), i=i, cp=cps[i] )
                   
                           i==cut_i? 
                             echo("self, discarded")
                             []  // cp will disappear
                   
                           : i== cut_i-1? // the block left[-1], will split to 2 
                             echo("Left")
                             let( blocks= [ L_cp // left cutpoint unchanged
                                          , [ cp.x-L_cp.x, L_size.y ]
                                  
                                          , [ L_cp.x, cut_tr.y ]
                                          , [ L_size.x, L_tr.y-cut_tr.y ]  
                                          ]
                                )
                                echo( "  blocks from left = ", blocks )
                                blocks             
                   
                           : echo("unchanged: ", [ cps[i], h(_stocks,cps[i]) ] )
                             [ cps[i], h(_stocks,cps[i]) ]
                       ]
                  )
                  [for(x=tmp)each x]        
        
              : isOverSizeX?
 
               let(tmp= [
                         for(i=range(cps))
                           echo( _b("Loop_cps"), i=i, cp=cps[i] )
                   
                           i==cut_i? 
                             echo("self, discarded")
                             []  // cp will disappear
                   
                           : i== cut_i+1? // the block right, will split to 2 
                             echo("Left")
                             let( blocks= [ R_cp // right.site unchanged
                                          , [ R_size.x, cp.y-R_cp.y ]
                                  
                                          , [ cut_tr.x, R_cp.y ]
                                          , [ R_tr.x-cut_tr.x, R_size.y ]  
                                          ]
                                )
                                echo( "  blocks from left = ", blocks )
                                blocks             
                   
                           : echo("unchanged: ", [ cps[i], h(_stocks,cps[i]) ] )
                             [ cps[i], h(_stocks,cps[i]) ]
                       ]
                  )
                  [for(x=tmp)each x] 
                                
              : [ for(_cp= keys(_stocks))
                 //echo(cp=cp)
                 each _cp==cp?
                          let( cp1= cp+ [ cutsize[0], 0 ]
                             , cp2= cp+ [ 0, cutsize[1] ]
                             )
                          [ cp1, [bx-cp1.x, by-cp1.y]      
                          , cp2, [bx-cp2.x, by-cp2.y]      
                          ]                          
                      : [ _cp, h(stocks,_cp) ]
               ]
        , rtn = [ for(k=keys(_rtn))
               if ( h(_rtn,k)[0]!=0 && h(_rtn,k)[1]!=0) // remove cases like 
                                                        // [ [8,0], [0,4] ]
               each [k, h(_rtn,k) ]
             ]      
        )
        echo(stocks=stocks)
        echo(_stocks=_stocks)
        echo(_rtn=_rtn)
        echo(rtn=rtn)
        echo( cps = cps )
        echo( cut_i = cut_i )
        echo(isOverSizeY=isOverSizeY, cut_tr=cut_tr)
   rtn
   );    

  // Testing makecut():
  // 
     //stocks= [ [0,2], [4,1]
            //, [1,0], [3,3] 
            //];
     //newstocks = makecut(stocks, [1,0], [2,1] );
     //echo(newstocks = newstocks); // [[0, 2], [4, 1], [3, 0], [1, 3], [1, 1], [3, 2]]
     //echo( newstocks== [[0, 2], [4, 1], [3, 0], [1, 3], [1, 1], [3, 2]]);         
   

//. 18cu



function cl2d_makecut_18cu( stocks // [ [0,2], [4,1] 
                                   // , [1,0], [3,3] 
                                   // ] 
                     , cut  // [ [1,0], [2,1] ]  = [cutpt, cutsize]
                     )=
( /*
  ====================================================                 
     This version takes care of the following types of overlapping blockings:
     (Use the makecut_tester() to test)

       +------------------------+   [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]  
       |    :         :         |        left          self         right
       |    +=========+[3,4]    |    [0,3],[1,2]                [3,0],[2,5] 
       |    |         |         |    [0,4],[5,1]
       +----+         | - - - - |     
     [0,3]  |         |         |     
            |         |         |                 
            |         |         |    
            +=========+ - - - - |                   
          [1,1]       |         |                           
                      +---------+                      
                   [3,0] 
                   
       +------------------------+  [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]   
       |    :         :         |    
       |    :         :         |    [0,3],[5,2]
       |    :         :         |                              [3,0],[2,1]
       +----+==============+ - -|                              [4,0],[1,5] 
     [0,3]  |              |    |     
            |              |    |     
            |              |    |     
            +==============+- - |  
          [1,1]       |         | 
                      +---------+                      
                   [3,0]
                   
                   
       +------------------------+  [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ]   
       |    :         :         |    
       |    :         :         |    
       |    :         :         |                              
       +----+=========+ - - - - |                               
     [0,3]  |         |         |     
            |         |         |     
            |         |         |     
            +=========+- - - - -|  
          [1,1]       |         | 
                      +---------+                      
                   [3,0]
                   
  But fail at:
  
                   
       +------------------------+  [ [0,3],[5,2], [1,0],[4,5] ]   
       |    :         :         |    
       |    :         :         |    
       |    :         :         |                              
       +----+ - - - - + - - - - |                               
     [0,3]  |         |         |     
            +=========+         |     
            |         |         |     
            |         |         |     
            |         |         |
            +=========+---------+                      
         [1,0]
         
         
       +------------------------+  [ [0,3],[5,2], [1,0],[4,5], [3,0],[2,5] ]   
       |    :                   |    
       +====+ - - - - - - - - - +    
       |    |                   |                              
       +====+ - - - - - - - - - +                               
     [0,3]  |                   |     
            |                   |     
            |                   |     
            |                   |     
            |                   |
            +-------------------+                      
         [1,0]
                   
                   
                                      
    
  ====================================================                 
  */
  
  
  // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]

     echo(str("markcut_18cu(",_s("stocks={_}, cut={_})",[ str(stocks),str(cut)]))) 
     let( _stocks = sortByCutptX( stocks )
        , stocksize = getStockSize( stocks )
        
        , cps= keys(_stocks)
        , cp= cut[0]
        , cutsize = cut[1] 
        , cut_tr= sum(cut)  // top-right corner
        
        , cut_i = idx( cps, cp )
        
        , L_cp=  _stocks[2*(cut_i-1)]
        , L_size= _stocks[2*(cut_i-1)+1] 
        , L_tr = L_cp + L_size
        
        , R_cp= _stocks[2*(cut_i+1)]
        , R_size= _stocks[2*(cut_i+1)+1] 
        , R_tr = R_cp +R_size
        
        , isOverSizeX = cut_tr.x> R_cp.x //right.x
        , isOverSizeY = cut_tr.y> L_cp.y //left[0].y
        
        , isUnderSizeX = cut_tr.x< R_cp.x //right.x
        , isUnderSizeY = cut_tr.y< L_cp.y //left[0].y
        
        , isMatchSizeX = cut_tr.x== R_cp.x
        , isMatchSizeY = cut_tr.y== L_cp.y
        
        
        
        // 18cu:
        // We assume that overstocking (overlapping stocking) happens, if any,  
        // only on left stock and/or right stock (i.e., the cut_i-1 and/or 
        // cut_i+1 stocks), but not go futher to cut_i-2, cut_i-3, cut_i+2, 
        // cut_i+3, etc. This might cause issues in extreme cases. But we
        // leave it for now  
        
        , _rtn= 
           let(tmp=[ //------------------------ stocks 0 ~ i-2 
                     cut_i>=1?
                        echo( _u(_b("stocks 0 ~ i")))
                        let(early= [ for(i= range(cut_i-1))   
                                    [ cps[i], h(_stocks,cps[i]) ] 
                                  ]
                        ) echo(early=early)
                        early
                        :[]   
               
                   , //------------------------ stocks L (left) 
                     echo(_u(_b("Left"))) 
                     echo(isOverSizeY=isOverSizeY)  
                      isOverSizeY?
                      let( blocks= [ L_cp // left cutpoint unchanged
                                    , [ cp.x-L_cp.x, L_size.y ]
                                  
                                    , [ L_cp.x, cut_tr.y ]
                                    , [ L_size.x, L_tr.y-cut_tr.y ]  
                                    ]
                          )
                          echo( "  blocks from left = ", blocks )
                          blocks 
                     : let( left= [L_cp, L_size])
                       echo(left=left)
                       left
                                      
                                
                   , //------------------------ stocks self (= cut) 
                     echo(_u(_b("self(=cut)")))
                     echo( both_underSize= isUnderSizeX && isUnderSizeY) 
                      isUnderSizeX && isUnderSizeY?
                      echo( "   both x y undersized" ) 
                      let( cp1= cp+ [ 0, cutsize.y ]
                         , cp2= cp+ [ cutsize.x, 0 ]
                         //[ cp1, [bx-cp1.x, by-cp1.y]      
                         //, cp2, [bx-cp2.x, by-cp2.y]      
                         , self_splits= [ 
                                  cp1, [stocksize.x-cp1.x, stocksize.y-cp1.y]      
                                , cp2, [stocksize.x-cp2.x, stocksize.y-cp2.y]      
                                ]
                         )
                         echo( self_splits = self_splits )
                         self_splits       
                      :
                       echo(self=[])
                       []
                
                   , //------------------------ stocks R (right) 
                     echo(_u(_b("Right"))) 
                     echo(isOverSizeX=isOverSizeX)  
                     isOverSizeX?
             
                      	let( blocks= [ R_cp // right.site unchanged
                                  , [ R_size.x, cp.y-R_cp.y ]
                                  
                                  , [ cut_tr.x, R_cp.y ]
                                  , [ R_tr.x-cut_tr.x, R_size.y ]  
                                  ]
                        )
                        echo( "  blocks from left = ", blocks )
                        blocks  
                     : let( right= [R_cp, R_size])
                       echo(right=right)
                       right 
                     , //------------------------ stocks > i-1 
                      echo(_u(_b("stocks > i + 1"))) 
                      let(late = [ for(i= range(cut_i+2, len(cps) ) )   
                                      [ cps[i], h(_stocks,cps[i]) ] 
                                 ]
                         )
                      echo(late=late)
                      late                
                   ]
           )
           [ for(x=tmp) each x ]               
                          
                          
        , rtn = [ for(k=keys(_rtn))
               if ( h(_rtn,k)[0]!=0 && h(_rtn,k)[1]!=0) // remove cases like 
                                                        // [ [8,0], [0,4] ]
               each [k, h(_rtn,k) ]
             ]      
        )
        echo(stocks=stocks)
        echo(_stocks=_stocks)
        echo(stocksize=stocksize)
        echo(_rtn=_rtn)
        echo(rtn=rtn)
        echo( cps = cps )
        echo( cut_i = cut_i )
        echo(isOverSizeY=isOverSizeY, cut_tr=cut_tr)
   rtn
   );    



//. 18cv

function cl2d_makecut_18cv( stocks // [ [0,2], [4,1] 
                                   // , [1,0], [3,3] 
                                   // ] 
                     , cut  // [ [1,0], [2,1] ]  = [cutpt, cutsize]
                     )=
( /*
  ====================================================                 
     This version (18cv) pretty much takes care of thinkable cases.
  ====================================================                 
  */
  
  
  // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]

     echo(log_b(str("markcut_18cv(",_ss("stocks={_:b;bc=yellow}, cut={_:b;bc=yellow})"
                                      ,[ str(stocks),str(cut)])), 2)
         ) 
     let( _stocks = sortByCutptX( stocks )
        , stocksize = getStockSize( stocks )
        
        , cps= keys(_stocks)
        , cp= cut[0]
        , cutsize = cut[1] 
        , cut_tr= sum(cut)  // top-right corner
        
        , cut_i = idx( cps, cp )
        
        , L_cp=  _stocks[2*(cut_i-1)]
        , L_size= _stocks[2*(cut_i-1)+1] 
        , L_tr = L_cp + L_size
        
        , R_cp= _stocks[2*(cut_i+1)]
        , R_size= _stocks[2*(cut_i+1)+1] 
        , R_tr = R_cp +R_size
        
        , isOverSizeX = cut_tr.x> R_cp.x 
        , isOverSizeY = cut_tr.y> L_cp.y 
        
        , isUnderSizeX = cut_tr.x< R_cp.x 
        , isUnderSizeY = cut_tr.y< L_cp.y 
        
        //, isMatchSizeX = cut_tr.x== R_cp.x
        //, isMatchSizeY = cut_tr.y== L_cp.y
        
        
        
        // 18cu, 18cv:
        // We assume that overstocking (overlapping stocking) happens, if any,  
        // only on left stock and/or right stock (i.e., the cut_i-1 and/or 
        // cut_i+1 stocks), but not go futher to cut_i-2, cut_i-3, cut_i+2, 
        // cut_i+3, etc. This might cause issues in extreme cases. But we
        // leave it for now  
        
        , _rtn= 
           let(tmp=[ //------------------------ stocks 0 ~ i-2
                     // stocks between 0 and cut_i-2 remain unchanged 
                     echo( log_b(_u(_b("Early stocks 0 ~ i-2")),3))
                     let( _tmp= cp.x==0?
                                 echo(log_( "cutpoint.x=0 => []", 3))
                                 []
                               :let( tmp= cut_i>=1?
                                          let(early= [ for(i= range(cut_i-1))   
                                                      [ cps[i], h(_stocks,cps[i]) ] 
                                                    ]
                                          ) 
                                          early
                                          :[]
                                   )
                                   tmp             
                         )
                         echo( log_e(["Early stocks",_tmp],3))
                         _tmp
                         
                   , //------------------------ stocks L (left) 
                   
                     // Left stock. Needs to split into two when
                     // isOverSizeY:
                     //  B--C------G  stock1= ABCD 
                     //  |         |  stock2= BGFE  
                     //  E  +==+   F  <--- cut_tr.y   
                     //  A--D  |   |  <--- L_cp.y
                     //     |  |   |  
                     //     |  |   | 
                     //     +==+-+ |
                     //          +-+
                     echo( log_b([_u(_b("Left")),[L_cp,L_size]],3)) 
                     
                     let(tmp = L_cp==undef?[]
                                :cp.x==0?
                                echo(log_( "cutpoint.x=0 => []", 3))
                                []
                               :echo( log_(["isOverSizeY",isOverSizeY],3))  
                                isOverSizeY?
                                let( blocks= [ L_cp // left cutpoint unchanged
                                              , [ cp.x-L_cp.x, L_size.y ]
                                  
                                              , [ L_cp.x, cut_tr.y ]
                                              , [ L_size.x, L_tr.y-cut_tr.y ]  
                                              ]
                                    )
                                    echo( log_(["blocks from left", blocks],3) )
                                    blocks 
                               : let( left= [L_cp, L_size])
                                 echo(log_(["left",left],3))
                                 left
                         )
                         echo( log_e( ["Left", tmp], 3))
                         tmp             
                                
                   , //------------------------ stocks self (= cut) 
                     echo( log_b(_u(_b("self(=cut)")),3))
                     echo( log_(["both_underSize",isUnderSizeX && isUnderSizeY],3)) 
                     let(tmp= 
                        isUnderSizeX && isUnderSizeY || cp.y==0 || cp.x==0?
                        echo( log_("both x y undersized",3) ) 
                        let( cp1= cp+ [ 0, cutsize.y ]
                           , cp2= cp+ [ cutsize.x, 0 ]
                           , stock1=  L_cp.y <= cut_tr.y? 
                                      echo( log_(["stock1",[]],3) )
                                       [] //  +--------+  +--------+ 
                                          //  |        |  |  +==+  |
                                          //  +--+==+  |  +--|  |  |
                                          //     |  |  |     |  |  |
                                          //     +==+--+     +==+--+
                                     
                                     : [cp1, [stocksize.x-cp1.x, stocksize.y-cp1.y]]
                                       //  +-B------C    
                                       //  |        |  stock1= ABCD   
                                       //  +-+      |  <--- L_cp.y
                                       //    A=+    D  <--- cut_tr.y
                                       //    | |    | 
                                       //    +=+-+  |
                                       //        +--+
                                    
                           , stock2= R_cp.x<= cut_tr.x? 
                                     echo( log_(["stock2",[]],3) )
                                     [] //  +==+-----+  +=====+--+ 
                                        //  |  |     |  |     |  |
                                        //  +==+     |  +=====+  |
                                        //     |     |     |     |
                                        //     +-----+     +-----+
                                    : [cp2, [stocksize.x-cp2.x, stocksize.y-cp2.y]]
                                       //  +---B----C     
                                       //  |        |    
                                       //  +-+      |   stock2= ABCD
                                       //    +=+    |  
                                       //    | |    | 
                                       //    +=A-+  D  
                                       //        |  |
                                       //        +--+
                                    
                           , self_splits= concat( stock1, stock2 )
                           )
                           echo(log_(["cp_x",cp.x, "cp_y",cp.y],3))
                           //echo( log_(["self_splits", self_splits],3) )
                           //echo(log_e("",3))
                           self_splits       
       
                        : echo(log_("self set to []",3))
                         []
                       )
                       echo(log_e(["self splits to ", tmp],3))
                       tmp
                       
                   , //------------------------ stocks R (Right) 
                     // Right stock. Needs to split into two when
                     // isOverSizeX:
                     //  +--------F--G  stock1= ABCD 
                     //  |           |  stock2= DEFG  
                     //  +--+=====+  |  
                     //     |     |  | 
                     //     +==B==+  C
                     //        |     |
                     //        A--E--D 
                     echo( log_b([_u(_b("Right")),[R_cp,R_size]],3)) 
                     
                     let(tmp= R_cp==undef?[]
                             : cp.y==0?   
                               echo(log_( "cutpoint.y=0 => []", 3))
                               [] 
                             : echo(log_(["isOverSizeX",isOverSizeX],3)) 
                               isOverSizeX?
             
                              	let( blocks= [ R_cp // right.site unchanged
                                          , [ R_size.x, cp.y-R_cp.y ]
                                  
                                          , [ cut_tr.x, R_cp.y ]
                                          , [ R_tr.x-cut_tr.x, R_size.y ]  
                                          ]
                                )
                                echo( log_(["blocks from left", blocks],3) )
                                blocks  
                             : let( right= [R_cp, R_size])
                               //echo( log_(["right",right],3))
                               right 
                         )
                         echo(log_e(["Right", tmp],3))
                         tmp
                         
                     , //------------------------ stocks > i-1 
                        // stocks >i-1 remain unchanged 
                     echo( log_b(_u(_b("Late (stocks > i + 1)")),3)) 
                     let(tmp = 
                          cp.y==0?
                          echo(log_( "cutpoint.y=0 => []", 3))
                          [] 
                         :let(late = [ for(i= range(cut_i+2, len(cps) ) )   
                                          [ cps[i], h(_stocks,cps[i]) ] 
                                     ]
                             )
                          late 
                        )
                        echo( log_e(["Late", tmp],3))
                        tmp                    
                   ]
           )
           [ for(x=tmp) each x ]               
                          
                          
        , rtn = [ for(k=keys(_rtn))
               if ( h(_rtn,k)[0]!=0 && h(_rtn,k)[1]!=0) // remove cases like 
                                                        // [ [8,0], [0,4] ]
               each [k, h(_rtn,k) ]
             ]      
        )
        echo(log_(["stocks",stocks],2))
        echo(log_(["_stocks",_stocks],2))
        echo(log_(["stocksize",stocksize],2))
        echo(log_(["_rtn",_rtn],2))
        echo(log_(["rtn",rtn],2))
        echo(log_([" cps", cps],2) )
        echo(log_([" cut_i", cut_i],2) )
        echo(log_(["isOverSizeY",isOverSizeY, "cut_tr",cut_tr],2))
        echo(log_e(["markcut_18cv() end, stocks= ",rtn],2))
   rtn
   );    


function cl2d_18cv( stocksize // [x,y] 
                  , parts // [ [x,y], [x,y] .... ]
                  , _stocks // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _cuts   // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]          
                  , _i=-1
                  )=
(
  _i==-1? // init 
   echo(log_b(_ss("<b>cl2d_18cv</b>(stocksize={_:b},parts={_:b}, _stocks={_:c=red}, _cuts={_:c=red}): init (_i=-1)", [stocksize,parts,str(_stocks),str(_cuts)]),0))  
   let( //_stock = stock.x<stock.y? // Make sure landscape view
        //         [stock.y,stock.x]:stock
        _stocks = [ [0,0], stocksize ]
      , _parts = reverse(
                   quicksort([  // Convert parts to sorted 
                              //   [ [[x,y],3], [[x,y],1], [[x,y],2] .... ]
                              // where 3,1,2 are the original index   
                    for( i=range(parts)) 
                        [ parts[i].x<parts[i].y  // flip x,y if y > x, for sort
                            ? [parts[i].y,parts[i].x]
                            : parts[i]
                        , i
                        ]  
                  ])
                )  
      )
      echo(log_(["_parts(sorted parts)", _bcolor(_b(_parts),"yellow")],0))
      cl2d_18cv( stocksize=stocksize
                , parts=_parts
                , _stocks= _stocks // [ [0,0], [x,y] ]
                , _cuts= [] 
                , _i=0 )
          
 :_i<len(parts)?
   echo(log_b(_ss("<b>cl2d_18cv</b>(_stocks={_:c=red}, _cuts={_:c=red}, _i={_:b;c=red} ): init", [_stocks,_cuts, str(_i)]),1))  
   echo(log_(["part = ", _bcolor(_b(parts[_i]),"yellow")],1))     
   let( part= parts[_i]  // [part_size([x,y]), part_index]
      , stocksizes = vals( _stocks )
      , cutsize = part[0]
      , fitnesses = cl2d_sorted_fitnesses_18cL( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       )
                    // fitnesses: arr of ["fitness", 0.833333, "stock", 1, "rot", 1]
                    
      , target = last( fitnesses ) // The chosen stock with the largest fitness
      
      , stock_i = h(target, "stock")
      , cutpoint = keys( _stocks )[stock_i]
      , _cutsize= h(target, "rot")? reverse(cutsize): cutsize               
                       
      , __stocks = cl2d_makecut_18cv( _stocks // [ [0,2], [4,1] 
                              // , [1,0], [3,3] 
                              // ] 
                     , [cutpoint, _cutsize]   // [[1,0],[2,1]]  
                     )                 
      , __cuts = concat(_cuts, [cutpoint, _cutsize ] )                     
      )
      echo(log_(["parts", parts],1 ))
      echo(log_(["cutpoint", cutpoint, "cutsize", cutsize],1 ))
      echo(log_(["__stocks", __stocks],1))
      echo(log_(["__cuts", __cuts],1))
      echo(log_e(str("End of _i=",_i),1)) 
      cl2d_18cv( stocksize=stocksize
                , parts=parts
                , _stocks= __stocks // [ [0,0], [x,y] ]
                , _cuts= __cuts 
                , _i=_i+1 )

   
  : 
  echo( log_(["_stocks", _stocks, "_cuts", _cuts],0))
  echo(log_e("Leaving cl2d_18cv()",0))
  [_stocks,_cuts]
);  

//stocksize= [8,4]; 
//parts= [ [2,3], [5,4], [1,2] ];
//parts= [ [2,3], [2,3], [1,2] ];
//parts= [ [2,3], [5,1], [1,2] ];
//parts= [ [2,4], [1,3], [1,3], [2,1],[2,1] ];
//parts= [ [1,3], [1,3], [2,1],[2,1] ]; // error: overlap
//parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ]; // error: overlap and failed
//parts= [ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ];
//parts= [ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ]; // error: failed

//cl2d_18cv_test( stocksize, parts );
module cl2d_18cv_test( stocksize, parts)
{

   echom("cl2d_18cv_test()");
   
   //stocksize = [8,4];
   //parts = [ [2,3], [5,4], [1,2] ]; // nice
   
   
   //=========================================
   //parts = [ [2,3], [2,3], [1,2] ]; // last cutpoint error
   /*
      parts = [ [2,3], [2,3], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [2, 3], [2, 0], [2, 3], [2, 3], [2, 1]]
      _stocks=[[4, 0], [4, 4], [4, 3], [4, 1], [0, 3], [8, 1]]
      
         Error _stocks [[4, 3], [4, 1]] causing the [2,1] cut 
         up in the wrong place
      
      .---.---+=======+---.---.---.---.
      |   :   |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |      -|---.---.---.---.
      |       |       |   :   :   :   :
      |       |       |---.---.---.---.
      |       |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      
   */
   
   //=========================================
   //parts = [ [2,3], [5,1], [1,2] ];  
   /* parts = [ [2,3], [5,1], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
          f0= _f0==1? (1+ ( cx==sx?cy/sy:cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:cy/sx )):_f1
          
          
      cuts = [[0, 0], [5, 1], [5, 0], [3, 2], [5, 2], [1, 2]]
      stocks = [[6, 2], [2, 2], [0, 1], [8, 3]]  <=== wrong !!!
               should have been:
               [[6, 2], [2, 2], [0, 1], [5, 3]]
               
      .---.---.---.---.---+---+---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---|   |---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---+===+=======+
      |   :   :   :   :   |           |
      +===================+           |
      |                   |           |
      +===================+===========+
                                      
      Consider fix: when placing [3,2] on either [8,3] or [3,5]:
   
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      +===================+---.---.---.
      |                   |   :   :   :
      +===================+---.---.---.
     
      [8,3] is filled first. 
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [5, 1], [0, 1], [2, 3], [5, 0], [2, 1]]    
      _stocks=[[7, 0], [1, 4], [5, 1], [3, 3], [2, 1], [6, 3]]
      
      _stocks should have been:
                [[7, 0], [1, 4], [2, 1], [6, 3]]
          
      +=======+---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      +=======+===========+=======+---.
      |                   |       |   :
      +===================+=======+---.

   
   
   */
   
   //stocksize = [12,4];
   //parts = [ [2,3], [5,1], [1,2] ]; 
   
   //parts=[ [2,4], [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ]; // Error: cutpoint overlap
   //parts=[ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ];
   /*
      +=======+---.---.---+=======+===+
      |       |   :   :   |       |   |
      |       |---.---.---|       |   |
      |       |   :   :   |       |   |
      |       |---.---.---+=======+===+
      |       |   :   :   |       |   |
      +=======+===========+       |   |
      |                   |       |   |
      +===================+=======+===+
   
   Note: this doesn't look a good arrangement. HOWEVER, 
      it is better than what is thought to be a good one:
      
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |=======+=======+---.---.
      |       |       |       |   :   :
      |       |       |      -|---.---.
      |       |       |       |   :   :
      +=======+=======+===+===+===+---.
      |                   |       |   :
      +===================+=======+---.
   
   because it leaves out a square block. 
   
   */
   
   //parts= repeat( [ [1,3] ], 8 ); // ERROR cutpoints overlap !!! 
   //parts= concat( repeat( [ [1,2] ], 5 )
   //             , repeat( [ [1,3] ], 4 ) ); // ERROR cutpoints overlap !!! 
   
   cl2d= cl2d_18cv( stocksize // [x,y] 
                  , parts 
                  );
                  
   cuts= cl2d[1];
   
   x=stocksize[0];
   y=stocksize[1];
   for(r= range(y+1) ) Line( [O+r*Y, x*X+r*Y]  );
   for(c= range(x+1) ) Line( [O+c*X, c*X+y*Y]  );
   
   for( i = range(keys(cuts))) //x0= keys(cuts) )
   {
      x0= cuts[2*i];
      _x0 = addz(x0,0);
      size = h(cuts, x0); 
      x=size.x;
      y=size.y;
      pts= addz( [ _x0, _x0+y*Y, _x0+x*X+y*Y, _x0+x*X], i*0.2) ;
      echo(x0=x0, size=size, pts=pts);
      color(COLORS[i+1])Plane(pts);
      Black() 
         MarkCenter( addz(pts,0.2)
                   , Label=["text",str(i,":[",x,",",y,"]"),"scale",1.5]);
   }
}



//. 18cv2

function cl2d_makecut_18cv2( stocks // [ [0,2], [4,1] 
                                   // , [1,0], [3,3] 
                                   // ] 
                     , cut  // [ [1,0], [2,1] ]  = [cutpt, cutsize]
                     )=
( /*
  ====================================================                 
     This version (18cv2) will try to solve the stocks w/ "collapsed roof':
  ==================================================== 
         [2,3]      [4,3] 
           +---------+  
 [0,2]     |         |
   +-------+         |
   |                 |
   |                 |
   |                 |
   +-----------------+
 [0,0]              [4,0]
   
  This could occur frequently in the middle of stockings. 
  If we do a cut of [ [0,0],[1,1] ]

         [2,3]      [4,3] 
           +---------+  
 [0,2]     |         |
   +-------+         |
   |                 |
   +===+             |
   |   |             |
   +---+-------------+
 [0,0]              [4,0]

   Ordinally this would split [ [0,0],[4,3]] into 2 stocks:  
     [0,1],[4,2]  and [1,0],[3,3]
     
   but we need:
     [0,1],[4,1]  and [1,0],[3,2]
   
   ==> need to adjust the size.y
     
  */
  
  
  // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]

     echo(log_b(str("markcut_18cv2(",_ss("stocks={_:b;bc=yellow}, cut={_:b;bc=yellow})"
                                      ,[ str(stocks),str(cut)])), 2)
         ) 
     let( _stocks = sortByCutptX( stocks )
        , stocksize = getStockSize( stocks )
        
        , cps= keys(_stocks)
        , cp= cut[0]
        , cutsize = cut[1] 
        , cut_tr= sum(cut)  // top-right corner
        
        , cut_i = idx( cps, cp )
        
        , L_cp=  _stocks[2*(cut_i-1)]
        , L_size= _stocks[2*(cut_i-1)+1] 
        , L_tr = L_cp + L_size
        
        , R_cp= _stocks[2*(cut_i+1)]
        , R_size= _stocks[2*(cut_i+1)+1] 
        , R_tr = R_cp +R_size
        
        , isOverSizeX = cut_tr.x> R_cp.x 
        , isOverSizeY = cut_tr.y> L_cp.y 
        
        , isUnderSizeX = cut_tr.x< R_cp.x 
        , isUnderSizeY = cut_tr.y< L_cp.y 
        
        //, isMatchSizeX = cut_tr.x== R_cp.x
        //, isMatchSizeY = cut_tr.y== L_cp.y
        
        //----------------------------------
        // for stocks w/ collapsed roof (18cv2)
        //            +--+
        //        +---+  |  <--- cutSize_roof
        //   +----+      |  <--- cutSite_roof 
        //   |           |
        //   +-+         |
        //     +====+    |
        //     | cut|    | 
        //     +====+----+ 
           
        , stocksTopPts = getStocksTop(stocks) // 18cv2
                      
        , cutSite_roof = echo( log_(["stocksTopPts",stocksTopPts],2))
                         [ for( i= [0:len(stocksTopPts)] )
                           if(cp.x < stocksTopPts[i+1].x) 
                              [i, stocksTopPts[i+1].y]
                           //[ cp.x , stocksTopPts[i+1].x, stocksTopPts[i+1].y ]
                         ][0]    
        , cutSize_roof = [ for( i= [0:len(stocksTopPts)] )
                           //echo(i=i, cut_tr_x=cut_tr.x, stocksTopPts_i_y=stocksTopPts[i].y)
                           if(cut_tr.x < stocksTopPts[i+1].x) 
                              [i, stocksTopPts[i+1].y]
                           //[cut_tr.x, stocksTopPts[i].x, stocksTopPts[i].y]
                         ][0]    
        //--------------------------------------                       
        
        // 18cu, 18cv2:
        // We assume that overstocking (overlapping stocking) happens, if any,  
        // only on left stock and/or right stock (i.e., the cut_i-1 and/or 
        // cut_i+1 stocks), but not go futher to cut_i-2, cut_i-3, cut_i+2, 
        // cut_i+3, etc. This might cause issues in extreme cases. But we
        // leave it for now  
        
        , _rtn= 
           let(tmp=[ //------------------------ stocks 0 ~ i-2
                     // stocks between 0 and cut_i-2 remain unchanged 
                     echo( log_b(_u(_b("Early stocks 0 ~ i-2")),3))
                     let( _tmp= cp.x==0?
                                 echo(log_( "cutpoint.x=0 => []", 3))
                                 []
                               : echo(log_(["range",range(cut_i-1)],3))
                                 let( tmp= cut_i>=1?
                                          let(early= [ for(i= range(cut_i-1))   
                                                      [ cps[i], h(_stocks,cps[i]) ] 
                                                    ]
                                          ) 
                                          early
                                          :[]
                                   )
                                   tmp             
                         )
                         echo( log_e(["Early stocks",_tmp],3))
                         _tmp
                         
                   , //------------------------ stocks L (left) 
                   
                     // Left stock. Needs to split into two when
                     // isOverSizeY:
                     //  B--C------G  stock1= ABCD 
                     //  |         |  stock2= BGFE  
                     //  E  +==+   F  <--- cut_tr.y   
                     //  A--D  |   |  <--- L_cp.y
                     //     |  |   |  
                     //     |  |   | 
                     //     +==+-+ |
                     //          +-+
                     echo( log_b([_u(_b("Left")),[L_cp,L_size]],3)) 
                     
                     let(tmp = L_cp==undef?[]
                                :cp.x==0?
                                echo(log_( "cutpoint.x=0 => []", 3))
                                []
                               :echo( log_(["isOverSizeY",isOverSizeY],3))  
                                isOverSizeY?
                                let( blocks= [ L_cp // left cutpoint unchanged
                                              , [ cp.x-L_cp.x, L_size.y ]
                                  
                                              , [ L_cp.x, cut_tr.y ]
                                              , [ L_size.x, L_tr.y-cut_tr.y ]  
                                              ]
                                    )
                                    echo( log_(["blocks from left", blocks],3) )
                                    blocks 
                               : let( left= [L_cp, L_size])
                                 echo(log_(["left",left],3))
                                 left
                         )
                         echo( log_e( ["Left", tmp], 3))
                         tmp             
                                
                   , //------------------------ stocks self (= cut) 
                     //: self(=cut)
                     echo( log_b(_u(_b("self(=cut)")),3))
                     echo( log_(["both_underSize",isUnderSizeX && isUnderSizeY],3)) 
                     echo( log_(["cp",cp,"cut_tr", cut_tr],3))
                     echo( log_(["stocksTopPts",stocksTopPts],3))
                     echo( log_(["cutSite_roof",cutSite_roof,"cutSize_roof",cutSize_roof],3))
                     let(tmp= 
                        isUnderSizeX && isUnderSizeY || cp.y==0 || cp.x==0?
                        echo( log_("both x y undersized",3) ) 
                        let( cp1= cp+ [ 0, cutsize.y ]
                           , cp2= cp+ [ cutsize.x, 0 ]
                           , stock1=  L_cp.y <= cut_tr.y? 
                                      echo( log_(["stock1",[]],3) )
                                       [] //  +--------+  +--------+ 
                                          //  |        |  |  +==+  |
                                          //  +--+==+  |  +--|  |  |
                                          //     |  |  |     |  |  |
                                          //     +==+--+     +==+--+
                                     
                                     : let( tmp= [ cp1, [ stocksize.x-cp1.x
                                                        , und(cutSite_roof[1],stocksize.y)-cp1.y
                                                        ]
                                                 ]
                                          )
                                          echo( log_(["stock1",tmp],3) )
                                          tmp       
                                      // , [stocksize.x-cp1.x, stocksize.y-cp1.y]]
                                       //  +-B------C    
                                       //  |        |  stock1= ABCD   
                                       //  +-+      |  <--- L_cp.y
                                       //    A=+    D  <--- cut_tr.y
                                       //    | |    | 
                                       //    +=+-+  |
                                       //        +--+
                                    
                           , stock2= let( tmp= 
                                       cutSite_roof[0]<cutSize_roof[0]
                                       // stock2 when there's a collapsed roof, AND 
                                       // cut_tr.x is on different roof than cut.cp.x
                                       //
                                        // for stocks w/ collapsed roof (18cv2)
                                        //   
                                        //        F------G  <--- cutSize_roof
                                        //        |      |
                                        //   B----+      C  <--- cutSite_roof 
                                        //   |           |
                                        //   A====E==+   D   stock1= ABCD
                                        //   |  cut  |   |   stock2= EFGD 
                                        //   +=======+---+
                                       && cut_tr.x!= stocksTopPts[cutSize_roof[0]].x
                                       //  This is to avoid ABCD
                                       //         B-------C
                                       //         |       |
                                       //    +----+       |
                                       //    |            |
                                       //    +====A       D 
                                       //    |    |       |
                                       //    +====+-------+                                       
                                       && cutSite_roof[1] != cutSize_roof[1]
                                       //  This is to avoid ABCD
                                       //    +---B--------C
                                       //    |            |
                                       //    +===A====+   D
                                       //    |        |   |
                                       //    +========+   | 
                                       //        |        |
                                       //        +--------+
                                       ?
                                                                               
                                       echo(log_(_s("cut over collapsed roof @ y= {_} & {_}"
                                                   , [cutSite_roof[1],cutSize_roof[1]]),3))
                                       let(x= stocksTopPts[cutSize_roof[0]].x) 
                                       [ [x, cut_tr.y]
                                       , [ stocksize.x-x
                                         , cutSize_roof[1]-cut_tr.y
                                         ] 
                                       ]
                                      
                                     :R_cp.x<= cut_tr.x? 
                                     echo( log_(["stock2",[]],3) )
                                     [] //  +==+-----+  +=====+--+ 
                                        //  |  |     |  |     |  |
                                        //  +==+     |  +=====+  |
                                        //     |     |     |     |
                                        //     +-----+     +-----+
                                    : [cp2, [ stocksize.x-cp2.x
                                            , und(cutSize_roof[1],stocksize.y)-cp2.y
                                            ]
                                      ]
                                    // : [cp2, [stocksize.x-cp2.x, stocksize.y-cp2.y]]
                                       //  +---B----C     
                                       //  |        |    
                                       //  +-+      |   stock2= ABCD
                                       //    +=+    |  
                                       //    | |    | 
                                       //    +=A-+  D  
                                       //        |  |
                                       //        +--+
                                    )
                                    echo( log_(["stock2",tmp],3) )
                                    tmp  
                           , self_splits= concat( stock1,  stock2 )
                           )
                           echo(log_(["cp_x",cp.x, "cp_y",cp.y],3))
                           //echo( log_(["self_splits", self_splits],3) )
                           //echo(log_e("",3))
                           self_splits       
       
                        : echo(log_("self set to []",3))
                         []
                       )
                       echo(log_e(["self splits to ", tmp],3))
                       tmp
                       
                   , //------------------------ stocks R (Right) 
                     // Right stock. Needs to split into two when
                     // isOverSizeX:
                     //  +--------F--G  stock1= ABCD 
                     //  |           |  stock2= DEFG  
                     //  +--+=====+  |  
                     //     |     |  | 
                     //     +==B==+  C
                     //        |     |
                     //        A--E--D 
                     echo( log_b([_u(_b("Right")),[R_cp,R_size]],3)) 
                     
                     let(tmp= R_cp==undef?[]
                             //: cp.y==0?   
                               //echo(log_( "cutpoint.y=0 => []", 3))
                               //[] 
                             : echo(log_(["isOverSizeX",isOverSizeX],3)) 
                               isOverSizeX?
             
                              	let( blocks= [ R_cp // right.site unchanged
                                          , [ R_size.x, cp.y-R_cp.y ]
                                  
                                          , [ cut_tr.x, R_cp.y ]
                                          , [ R_tr.x-cut_tr.x, R_size.y ]  
                                          ]
                                )
                                echo( log_(["blocks from left", blocks],3) )
                                blocks  
                             : let( right= [R_cp, R_size])
                               //echo( log_(["right",right],3))
                               right 
                         )
                         echo(log_e(["Right", tmp],3))
                         tmp
                         
                     , //------------------------ stocks > i-1 
                        // stocks >i-1 remain unchanged 
                     echo( log_b(_u(_b("Late (stocks > i + 1)")),3)) 
                     let(tmp = 
                          //cp.y==0?
                          //echo(log_( "cutpoint.y=0 => []", 3))
                          //[] 
                         //:
                         echo( log_(["range",range(cut_i+2, len(cps) )],3))
                          let(late = [ for(i= range(cut_i+2, len(cps) ) )   
                                          [ cps[i], h(_stocks,cps[i]) ] 
                                     ]
                             )
                          late 
                        )
                        echo( log_e(["Late", tmp],3))
                        tmp                    
                   ]
           )
           [ for(x=tmp) each x ]               
                          
                          
        , rtn = [ for(k=keys(_rtn))
               if ( h(_rtn,k)[0]!=0 && h(_rtn,k)[1]!=0) // remove cases like 
                                                        // [ [8,0], [0,4] ]
               each [k, h(_rtn,k) ]
             ]      
        )
        echo(log_(["stocks",stocks],2))
        echo(log_(["_stocks",_stocks],2))
        echo(log_(["stocksize",stocksize],2))
        echo(log_(["_rtn",_rtn],2))
        echo(log_(["rtn",rtn],2))
        echo(log_([" cps", cps],2) )
        echo(log_([" cut_i", cut_i],2) )
        echo(log_(["isOverSizeY",isOverSizeY, "cut_tr",cut_tr],2))
        echo(log_e(["markcut_18cv2() end, stocks= ",rtn],2))
   rtn
   );    


function cl2d_18cv2( stocksize // [x,y] 
                  , parts // [ [x,y], [x,y] .... ]
                  , _stocks // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _cuts   // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]          
                  , _i=-1
                  )=
(
  _i==-1? // init 
   echo(log_b(_ss("<b>cl2d_18cv2</b>(stocksize={_:b},parts={_:b}, _stocks={_:c=red}, _cuts={_:c=red}): init (_i=-1)", [stocksize,parts,str(_stocks),str(_cuts)]),0))  
   let( //_stock = stock.x<stock.y? // Make sure landscape view
        //         [stock.y,stock.x]:stock
        _stocks = [ [0,0], stocksize ]
      , _parts = reverse(
                   quicksort([  // Convert parts to sorted 
                              //   [ [[x,y],3], [[x,y],1], [[x,y],2] .... ]
                              // where 3,1,2 are the original index   
                    for( i=range(parts)) 
                        [ parts[i].x<parts[i].y  // flip x,y if y > x, for sort
                            ? [parts[i].y,parts[i].x]
                            : parts[i]
                        , i
                        ]  
                  ])
                )  
      )
      echo(log_(["_parts(sorted parts)", _bcolor(_b(_parts),"yellow")],0))
      cl2d_18cv2( stocksize=stocksize
                , parts=_parts
                , _stocks= _stocks // [ [0,0], [x,y] ]
                , _cuts= [] 
                , _i=0 )
          
 :_i<len(parts)?
   echo(log_b(_ss("<b>cl2d_18cv2</b>(_stocks={_:c=red}, _cuts={_:c=red}, _i={_:b;c=red} ): init", [_stocks,_cuts, str(_i)]),1))  
   echo(log_(["part = ", _bcolor(_b(parts[_i]),"yellow")],1))     
   let( part= parts[_i]  // [part_size([x,y]), part_index]
      , stocksizes = vals( _stocks )
      , cutsize = part[0]
      , fitnesses = cl2d_sorted_fitnesses_18cL( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       )
                    // fitnesses: arr of ["fitness", 0.833333, "stock", 1, "rot", 1]
                    
      , target = last( fitnesses ) // The chosen stock with the largest fitness
      
      , stock_i = h(target, "stock")
      , cutpoint = keys( _stocks )[stock_i]
      , _cutsize= h(target, "rot")? reverse(cutsize): cutsize               
                       
      , __stocks = cl2d_makecut_18cv2( _stocks // [ [0,2], [4,1] 
                              // , [1,0], [3,3] 
                              // ] 
                     , [cutpoint, _cutsize]   // [[1,0],[2,1]]  
                     )                 
      , __cuts = concat(_cuts, [cutpoint, _cutsize ] )                     
      )
      echo(log_(["parts", parts],1 ))
      echo(log_(["cutpoint", cutpoint, "cutsize", cutsize],1 ))
      echo(log_(["__stocks", __stocks],1))
      echo(log_(["__cuts", __cuts],1))
      echo(log_e(str("End of _i=",_i),1)) 
      cl2d_18cv2( stocksize=stocksize
                , parts=parts
                , _stocks= __stocks // [ [0,0], [x,y] ]
                , _cuts= __cuts 
                , _i=_i+1 )

   
  : 
  echo( log_(["_stocks", _stocks, "_cuts", _cuts],0))
  echo(log_e("Leaving cl2d_18cv2()",0))
  [_stocks,_cuts]
);  

stocksize= [8,4]; 
parts= [ [2,3], [5,4], [1,2] ];
parts= [ [2,3], [2,3], [1,2] ];
parts= [ [2,3], [5,1], [1,2] ]; // need align-right
parts= [ [2,4], [1,3], [1,3], [2,1],[2,1] ]; // need align-top
parts= [ [1,3], [1,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1],[3,1] ]; 
parts= [ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ];
parts= [ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ]; // error: invalid type ... need align right
/* 
markcut_18cv2(stocks=[[0, 1], [5, 3], [0, 2], [8, 2]], cut=[[0, 2], [2, 2]])
ECHO: "       self(=cut)"
ECHO: "         both_underSize=false"
ECHO: "         cp=[0, 2], cut_tr=[2, 4]"
ECHO: "         stocksTopPts=[[0, 4, 0], [8, 4, 0]]"
ECHO: "         cutSite_roof=[0, 4], cutSize_roof=[0, 4]"
ECHO: "         both x y undersized"
ECHO: "         stock1=[]"
ECHO: "         stock2=[[2, 2], [6, 2]]"
ECHO: "         cp_x=0, cp_y=2"
ECHO: "       self splits to =[[2, 2], [6, 2]]"

markcut_18cv2(stocks=[[2, 2], [6, 2]], cut=[[2, 2], [2, 2]])
ECHO: "       self(=cut)"
ECHO: "         both_underSize=false"
ECHO: "         cp=[2, 2], cut_tr=[4, 4]"
ECHO: "         stocksTopPts=[[[2, 4, 0], [8, 4, 0]]]"
ECHO: "         cutSite_roof=undef, cutSize_roof=undef"
ECHO: "         self set to []"
ECHO: "       self splits to =[]"
*/

//parts= [ [3,4],[3,4] ];
/*
ECHO: "       self(=cut)"
ECHO: "         both_underSize=false"
ECHO: "         cp=[3, 0], cut_tr=[6, 4]"
ECHO: "         stocksTopPts=[[[3, 4, 0], [8, 4, 0]]]"
ECHO: "         cutSite_roof=undef, cutSize_roof=undef"
ECHO: "         both x y undersized"
ECHO: "         stock1=[[3, 4], [5, 0]]"
ECHO: "         stock2=[[6, 0], [2, 4]]"
ECHO: "         cp_x=3, cp_y=0"
ECHO: "       self splits to =[[3, 4], [5, 0], [6, 0], [2, 4]]"
*/

//cl2d_18cv2_test( stocksize, parts );
module cl2d_18cv2_test( stocksize, parts)
{

   echom("cl2d_18cv2_test()");
   
   //stocksize = [8,4];
   //parts = [ [2,3], [5,4], [1,2] ]; // nice
   
   
   //=========================================
   //parts = [ [2,3], [2,3], [1,2] ]; // last cutpoint error
   /*
      parts = [ [2,3], [2,3], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [2, 3], [2, 0], [2, 3], [2, 3], [2, 1]]
      _stocks=[[4, 0], [4, 4], [4, 3], [4, 1], [0, 3], [8, 1]]
      
         Error _stocks [[4, 3], [4, 1]] causing the [2,1] cut 
         up in the wrong place
      
      .---.---+=======+---.---.---.---.
      |   :   |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |      -|---.---.---.---.
      |       |       |   :   :   :   :
      |       |       |---.---.---.---.
      |       |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      
   */
   
   //=========================================
   //parts = [ [2,3], [5,1], [1,2] ];  
   /* parts = [ [2,3], [5,1], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
          f0= _f0==1? (1+ ( cx==sx?cy/sy:cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:cy/sx )):_f1
          
          
      cuts = [[0, 0], [5, 1], [5, 0], [3, 2], [5, 2], [1, 2]]
      stocks = [[6, 2], [2, 2], [0, 1], [8, 3]]  <=== wrong !!!
               should have been:
               [[6, 2], [2, 2], [0, 1], [5, 3]]
               
      .---.---.---.---.---+---+---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---|   |---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---+===+=======+
      |   :   :   :   :   |           |
      +===================+           |
      |                   |           |
      +===================+===========+
                                      
      Consider fix: when placing [3,2] on either [8,3] or [3,5]:
   
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      +===================+---.---.---.
      |                   |   :   :   :
      +===================+---.---.---.
     
      [8,3] is filled first. 
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [5, 1], [0, 1], [2, 3], [5, 0], [2, 1]]    
      _stocks=[[7, 0], [1, 4], [5, 1], [3, 3], [2, 1], [6, 3]]
      
      _stocks should have been:
                [[7, 0], [1, 4], [2, 1], [6, 3]]
          
      +=======+---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      +=======+===========+=======+---.
      |                   |       |   :
      +===================+=======+---.

   
   
   */
   
   //stocksize = [12,4];
   //parts = [ [2,3], [5,1], [1,2] ]; 
   
   //parts=[ [2,4], [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ]; // Error: cutpoint overlap
   //parts=[ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ];
   /*
      +=======+---.---.---+=======+===+
      |       |   :   :   |       |   |
      |       |---.---.---|       |   |
      |       |   :   :   |       |   |
      |       |---.---.---+=======+===+
      |       |   :   :   |       |   |
      +=======+===========+       |   |
      |                   |       |   |
      +===================+=======+===+
   
   Note: this doesn't look a good arrangement. HOWEVER, 
      it is better than what is thought to be a good one:
      
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |=======+=======+---.---.
      |       |       |       |   :   :
      |       |       |      -|---.---.
      |       |       |       |   :   :
      +=======+=======+===+===+===+---.
      |                   |       |   :
      +===================+=======+---.
   
   because it leaves out a square block. 
   
   */
   
   //parts= repeat( [ [1,3] ], 8 ); // ERROR cutpoints overlap !!! 
   //parts= concat( repeat( [ [1,2] ], 5 )
   //             , repeat( [ [1,3] ], 4 ) ); // ERROR cutpoints overlap !!! 
   
   cl2d= cl2d_18cv2( stocksize // [x,y] 
                  , parts 
                  );
                  
   cuts= cl2d[1];
   
   x=stocksize[0];
   y=stocksize[1];
   for(r= range(y+1) ) Line( [O+r*Y, x*X+r*Y]  );
   for(c= range(x+1) ) Line( [O+c*X, c*X+y*Y]  );
   
   for( i = range(keys(cuts))) //x0= keys(cuts) )
   {
      x0= cuts[2*i];
      _x0 = addz(x0,0);
      size = h(cuts, x0); 
      x=size.x;
      y=size.y;
      pts= addz( [ _x0, _x0+y*Y, _x0+x*X+y*Y, _x0+x*X], i*0.2) ;
      echo(x0=x0, size=size, pts=pts);
      color(COLORS[i+1])Plane(pts);
      Black() 
         MarkCenter( addz(pts,0.2)
                   , Label=["text",str(i,":[",x,",",y,"]"),"scale",1.5]);
   }
}


//. 1913

function cl2d_makecut_1913( stocks // [ [0,2], [4,1] 
                                   // , [1,0], [3,3] 
                                   // ] 
                     , cut  // [ [1,0], [2,1] ]  = [cutpt, cutsize]
                     , _i
                     )=
( 
  // Use new restock/restocks(). Great success !!
  // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]

     echo(log_b(str("markcut_1913(",_ss("stocks={_:b;bc=yellow}, cut={_:c=red}: {_:b;bc=yellow})"
                                      ,[ str(stocks),_i==undef?"":_i, str(cut)])), 2)
         ) 
     let( _stocks = sortByCutptX( stocks )
        , newstocks = restocks( stocks, cut )
        )
     echo(log_e( ["newstocks", newstocks], 2))         
   newstocks
   );    



function cl2d_fitness_1913( stocksize // [x,y] 
                     , cutsize   // [x,y]       
                     )=
(/* Check how well a cut of cutsize fits to the stocksize 
  Return an array [a,b]
  
  where a,b are fitness for :
    (a) cut piece is as it is (=f0) or 
    (b) is rotated by 90 degree (=fr)
    
  The larger a or b is, the better fit.   
     
           sy +---------------+
        a     |               |
           cy +---------+     |
              |         |     |  
        b     |         |     |
              |         |     |
              +---------+-----+
                        cx    sx
                   c       d
                                   
   The init fitness f0 is defined as:
          
      f0= 1-((sx-cx)/sx) * ((sy-cy)/sy)
        = 1- a*d/(a+b)/(c+d)
        
   The larger the f0, the better fit of cut 
       
   if oversized ( cx>sx or cy>sy ): undef
  
 */
 echo(log_b( str( "cl2d_fitness_1913"
                , _s(" ( stocksiz={_},cutsize={_} )",[stocksize, cutsize])
                ), 4) )
 let( sx = stocksize.x
    , sy = stocksize.y
    , cx = cutsize.x 
    , cy = cutsize.y 
    
    // The following criteria puts case A ahead of B, which is not what we want:
    //
    // , f0= (1-(sx-cx)/sx) + (1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx) + (1-(sy-cx)/sy)
    //
    // or
    //
    // , f0= (1-(sx-cx)/sx)*(1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx)*(1-(sy-cx)/sy)
    //
    // or
    //
    // , _f0= (cx/sx) * cy/sy 
    // , _fr= (cy/sx) * cx/sy 
    //
    //        A             B   
    //   +-----+    
    //   +--+  |    +--+-------------+    
    //   |  |  |    |  |             |    
    //   |  |  |    |  |             |    
    //   +--+--+    +--+-------------+   
    
    , _f0 = 1-((sx-cx)/sx) * ((sy-cy)/sy)  
    , _fr = 1-((sx-cy)/sx) * ((sy-cx)/sy) 
    
    // 1914: To make a long piece be filled first, 
    //       introduce a "landscape ratio" for the stock:
    //
    //    +-----------------+
    //  w |                 |
    //    +-----------------+
    //             l
    //  L = l/w
    //
    // THIS IS VERY CRITICAL.
    //
    , L = stocksize.x/stocksize.y 
    , f0=_f0*L
    , fr=_fr*L
    
    
    // Exclude undersized stocks --- there shouldn't have any fitness
    // if the stock is smaller than cut.
    //  
    // To avoid rounding error, need to use isequal() but not just:
    //    , rtn =  [ sx<cx || sy<cy ? undef: f0*L  
    //             , sx<cy || sy<cx ? undef: fr*L  
    //             ] 
    , rtn =  [ (isequal(sx,cx)||sx>cx) && (isequal(sy,cy)||sy>cy)? f0:undef  
             , (isequal(sx,cy)||sx>cy) && (isequal(sy,cx)||sy>cx)? fr:undef  
             ] 
    )
 //echo( log_(["L (stock landscape ratio)",L],4) )
 //echo( log_(["_f0",_f0, "_fr",_fr, "f0",f0, "fr", fr],4) )
 //echo( log_(["sx",sx,"sy",sy, "cx",cx,"cy",cy],4))
 //echo( log_(["sx&lt;cx",sx<cx,"sy&lt;cy",isequal(sy,cy), "sx&lt;cy",sx<cy,"sy&lt;cx",sy<cx],4))
 echo( log_e(["fitness", rtn],4))
 rtn
);
 


function cl2d_sorted_fitnesses_1913( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y ]
                       )=
( /*
    Decide which stock is the best fit for the cutsize.
    Return a sorted array [ ["fitness",f, "stock",i, "rot",j ]
                          , ["fitness",f, "stock",i, "rot",j ]
                          , ...]
   (sorted based on f, from small to large)                       

   For special cases where f=1 like case A and B below:

    A             B   
   +--.-----+    +--.-------------+
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   +--.-----+    +--.-------------+
    
   f= 1 for both cases. But obviously we want A to have the priority
   (fill and used up the smaller stock first)
   
   So f=1 needs special treatment. 
    
    
 */
  echo(log_b( str( "cl2d_sorted_fitnesses_1913"
                , _s(" ( stocksizs={_},cutsize={_} )",[stocksizes, cutsize])
                ), 3) )
 
  let(  
       fits= [ for(i = range(stocksizes) )
                 let(fs = cl2d_fitness_1913( stocksize = stocksizes[i], cutsize = cutsize ))
                 // fs = [ [a,b], [c,undef], [d,e] ...]
                 each [ for(f=range(fs))
                         if( fs[f]!=undef ) [fs[f],[i,f]]
                      ]    
              ]
             // array [ ["fitness",f, "stock",i, "rot",j ]
             //       , ["fitness",f, "stock",i, "rot",j ]
             //       , ...] 
     , sfits = sortArrs(fits,0)
     
     , rtn = [ for(x=sfits)
               ["fitness", x[0], "stock", x[1][0], "rot", x[1][1] ]
             ]          
     )
   echo( log_(["fits", fits],3))
   echo( log_e(["sorted fitness", rtn],3))
   rtn
);


function cl2d_1913( stocksize // [x,y] 
                  , parts // [ [x,y], [x,y] .... ]
                  , stop_i // Set to an int to stop the iteration midway for debug
                  , _stocks // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _cuts   // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]          
                  , _i=-1
                  
                  )=
(
   //echo(stop_i=stop_i, (stop_i==undef?true: (_i<stop_i)))
   _i==-1? // init 
   echo(log_b(_ss("<b>cl2d_1913</b>(stocksize={_:b},parts={_:b}, _stocks={_:c=red}, _cuts={_:c=red}): init (_i=-1)", [stocksize,parts,str(_stocks),str(_cuts)]),1))  
   let( //_stock = stock.x<stock.y? // Make sure landscape view
        //         [stock.y,stock.x]:stock
        _stocks = [ [0,0], stocksize ]
      , _parts = reverse(
                   quicksort([  // Convert parts to sorted 
                              //   [ [[x,y],3], [[x,y],1], [[x,y],2] .... ]
                              // where 3,1,2 are the original index   
                    for( i=range(parts)) 
                        [ parts[i].x<parts[i].y  // flip x,y if y > x, for sort
                            ? [parts[i].y,parts[i].x]
                            : parts[i]
                        , i
                        ]  
                  ])
                )  
      )
      echo(log_e(["_parts(sorted parts)", _bcolor(_b(_parts),"yellow")],1))
      cl2d_1913( stocksize=stocksize
                , parts=_parts
                , stop_i= stop_i
                , _stocks= _stocks // [ [0,0], [x,y] ]
                , _cuts= [] 
                , _i=0 )
          
 :_i<len(parts) && (stop_i==undef?true: (_i<stop_i))?
   echo(log_b(_ss("<b>cl2d_1913</b>(_stocks={_:c=red}, _cuts={_:c=red}, _i={_:b;c=red} ): init", [_stocks,_cuts, str(_i)]),1))  
   echo(log_(["part = ", _bcolor(_b(parts[_i]),"yellow")],1))     
   let( part= parts[_i]  // [part_size([x,y]), part_index]
      , stocksizes = vals( _stocks )
      , cutsize = part[0]
      , fitnesses = cl2d_sorted_fitnesses_1913( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       )
                    // fitnesses: arr of ["fitness", 0.833333, "stock", 1, "rot", 1]
                    
      , target = last( fitnesses ) // The chosen stock with the largest fitness
      
      , stock_i = h(target, "stock")
      , cutpoint = keys( _stocks )[stock_i]
      , _cutsize= h(target, "rot")? reverse(cutsize): cutsize               
                       
      , __stocks = cl2d_makecut_1913( _stocks // [ [0,2], [4,1] 
                              // , [1,0], [3,3] 
                              // ] 
                     , [cutpoint, _cutsize]   // [[1,0],[2,1]]  
                     , _i
                     )                 
      , __cuts = concat(_cuts, [cutpoint, _cutsize ] )                     
      )
      echo(log_(["parts", parts],1 ))
      echo(log_(["cutpoint", cutpoint, "cutsize", cutsize],1 ))
      echo(log_(["__stocks", __stocks],1))
      echo(log_(["__cuts", __cuts],1))
      echo(log_e(str("End of _i=",_i),1)) 
      cl2d_1913( stocksize=stocksize
                , parts=parts
                , stop_i= stop_i
                , _stocks= __stocks // [ [0,0], [x,y] ]
                , _cuts= __cuts 
                , _i=_i+1 )

   
  : 
  echo( log_(["_stocks", _stocks, "_cuts", _cuts],0))
  echo(log_e("Leaving cl2d_1913()",0))
  [_stocks,_cuts]
);  


stocksize= [8,4]; 
parts= [ [2,3], [5,4], [1,2] ];
parts= [ [2,3], [2,3], [1,2] ];
parts= [ [2,3], [5,1], [1,2] ]; 
parts= [ [2,4], [1,3], [1,3], [2,1],[2,1] ]; 
parts= [ [1,3], [1,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1],[3,1] ]; 
parts= [ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ];
parts= [ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ]; 

parts=[ [1,0.5,4], [2,3,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1],  [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1,2], [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1], [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1,3], [1,2,3], [0.2,2,5] ];

//cl2d_1913_test( stocksize, parts );

module cl2d_1913_test( stocksize, parts
                     , show_stocks=false
                     //, stop_i=5 // <=== for debug
                     )
{

   echom("cl2d_1913_test()");
   
   //stocksize = [8,4];
   //parts = [ [2,3], [5,4], [1,2] ]; // nice
   
   
   //=========================================
   //parts = [ [2,3], [2,3], [1,2] ]; // last cutpoint error
   /*
      parts = [ [2,3], [2,3], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [2, 3], [2, 0], [2, 3], [2, 3], [2, 1]]
      _stocks=[[4, 0], [4, 4], [4, 3], [4, 1], [0, 3], [8, 1]]
      
         Error _stocks [[4, 3], [4, 1]] causing the [2,1] cut 
         up in the wrong place
      
      .---.---+=======+---.---.---.---.
      |   :   |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |      -|---.---.---.---.
      |       |       |   :   :   :   :
      |       |       |---.---.---.---.
      |       |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      
   */
   
   //=========================================
   //parts = [ [2,3], [5,1], [1,2] ];  
   /* parts = [ [2,3], [5,1], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
          f0= _f0==1? (1+ ( cx==sx?cy/sy:cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:cy/sx )):_f1
          
          
      cuts = [[0, 0], [5, 1], [5, 0], [3, 2], [5, 2], [1, 2]]
      stocks = [[6, 2], [2, 2], [0, 1], [8, 3]]  <=== wrong !!!
               should have been:
               [[6, 2], [2, 2], [0, 1], [5, 3]]
               
      .---.---.---.---.---+---+---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---|   |---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---+===+=======+
      |   :   :   :   :   |           |
      +===================+           |
      |                   |           |
      +===================+===========+
                                      
      Consider fix: when placing [3,2] on either [8,3] or [3,5]:
   
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      +===================+---.---.---.
      |                   |   :   :   :
      +===================+---.---.---.
     
      [8,3] is filled first. 
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [5, 1], [0, 1], [2, 3], [5, 0], [2, 1]]    
      _stocks=[[7, 0], [1, 4], [5, 1], [3, 3], [2, 1], [6, 3]]
      
      _stocks should have been:
                [[7, 0], [1, 4], [2, 1], [6, 3]]
          
      +=======+---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      +=======+===========+=======+---.
      |                   |       |   :
      +===================+=======+---.

   
   
   */
   
   //stocksize = [12,4];
   //parts = [ [2,3], [5,1], [1,2] ]; 
   
   //parts=[ [2,4], [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ]; // Error: cutpoint overlap
   //parts=[ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ];
   /*
      +=======+---.---.---+=======+===+
      |       |   :   :   |       |   |
      |       |---.---.---|       |   |
      |       |   :   :   |       |   |
      |       |---.---.---+=======+===+
      |       |   :   :   |       |   |
      +=======+===========+       |   |
      |                   |       |   |
      +===================+=======+===+
   
   Note: this doesn't look a good arrangement. HOWEVER, 
      it is better than what is thought to be a good one:
      
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |=======+=======+---.---.
      |       |       |       |   :   :
      |       |       |      -|---.---.
      |       |       |       |   :   :
      +=======+=======+===+===+===+---.
      |                   |       |   :
      +===================+=======+---.
   
   because it leaves out a square block. 
   
   */
   
   //parts= repeat( [ [1,3] ], 8 ); // ERROR cutpoints overlap !!! 
   //parts= concat( repeat( [ [1,2] ], 5 )
   //             , repeat( [ [1,3] ], 4 ) ); // ERROR cutpoints overlap !!! 
   
   
   _parts = [ for(p=parts) 
               each len(p)==3? [ for(i=range(p[2])) repeat( [p[0],p[1]] ) ]
                        : [ p ]
            ];
   echo(_parts=_parts);
                
   cl2d= cl2d_1913( stocksize= stocksize // [x,y] 
                  , parts=_parts 
                  , stop_i= stop_i
                  
                  );
                  
   cuts= cl2d[1];
   stocks_left = cl2d[0];
   //echo(stocks_left= stocks_left);
   if(show_stocks)
   for(i = range( len(stocks_left)/2 ))
   {
      pts= get_block_pts( stocks_left[2*i], stocks_left[2*i+1]);
      //echo("stocks_left: ", i=i, stocks_left[2*i], pts=pts);
      Colors(i+1) Line( addz(pts,-0.5)
                      , closed=1);
      Colors(i+1,0.1) Plane( addz(pts,-0.5) );                 
   }
   
   x=stocksize[0];
   y=stocksize[1];
   for(r= range(y+1) ) Line( [O+r*Y, x*X+r*Y]  );
   for(c= range(x+1) ) Line( [O+c*X, c*X+y*Y]  );
   
   for( i = range(keys(cuts))) //x0= keys(cuts) )
   {
      x0= cuts[2*i];
      _x0 = addz(x0,0);
      size = h(cuts, x0); 
      x=size.x;
      y=size.y;
      pts= addz( [ _x0, _x0+y*Y, _x0+x*X+y*Y, _x0+x*X], i*0.2) ;
      //echo(x0=x0, size=size, pts=pts);
      Colors(i+1,0.5)Plane(pts);
      Black() 
         MarkCenter( addz(pts,0.2)
                   , Label=["text",str(i,":[",x,",",y,"]"),"scale",.75]);
   }
}

//. 1914

function cl2d_makecut_1914( stocks // [ [0,2], [4,1] 
                                   // , [1,0], [3,3] 
                                   // ] 
                     , cut  // [ [1,0], [2,1] ]  = [cutpt, cutsize]
                     , _i
                     )=
( 
  // Use new restock/restocks(). Great success !!
  // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]

     echo(log_b(str("cl2d_markcut_1914(",_ss("stocks={_:b;bc=yellow}, cut={_:c=red}: {_:b;bc=yellow})"
                                      ,[ str(stocks),_i==undef?"":_i, str(cut)])), 2)
         ) 
     let( _stocks = sortByCutptX( stocks )
        , newstocks = restocks( stocks, cut )
        )
     echo(log_e( ["newstocks", newstocks], 2))         
   newstocks
   );    



function cl2d_fitness_1914( stocksize // [x,y] 
                     , cutsize   // [x,y]       
                     )=
(/* Check how well a cut of cutsize fits to the stocksize 
  Return an array [a,b]
  
  where a,b are fitness for :
    (a) cut piece is as it is (=f0) or 
    (b) is rotated by 90 degree (=fr)
    
  The larger a or b is, the better fit.   
     
           sy +---------------+
        a     |               |
           cy +---------+     |
              |         |     |  
        b     |         |     |
              |         |     |
              +---------+-----+
                        cx    sx
                   c       d
                                   
   The init fitness f0 is defined as:
          
      f0= 1-((sx-cx)/sx) * ((sy-cy)/sy)
        = 1- a*d/(a+b)/(c+d)
        
   The larger the f0, the better fit of cut 
       
   if oversized ( cx>sx or cy>sy ): undef
  
 */
 echo(log_b( str( "cl2d_fitness_1914"
                , _s(" ( stocksiz={_},cutsize={_} )",[stocksize, cutsize])
                ), 4) )
 let( sx = stocksize.x
    , sy = stocksize.y
    , cx = cutsize.x 
    , cy = cutsize.y 
    
    // The following criteria puts case A ahead of B, which is not what we want:
    //
    // , f0= (1-(sx-cx)/sx) + (1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx) + (1-(sy-cx)/sy)
    //
    // or
    //
    // , f0= (1-(sx-cx)/sx)*(1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx)*(1-(sy-cx)/sy)
    //
    // or
    //
    // , _f0= (cx/sx) * cy/sy 
    // , _fr= (cy/sx) * cx/sy 
    //
    //        A             B   
    //   +-----+    
    //   +--+  |    +--+-------------+    
    //   |  |  |    |  |             |    
    //   |  |  |    |  |             |    
    //   +--+--+    +--+-------------+   
    
    , _f0 = 1-((sx-cx)/sx) * ((sy-cy)/sy)  
    , _fr = 1-((sx-cy)/sx) * ((sy-cx)/sy) 
    
    // 1914: To make a long piece be filled first, 
    //       introduce a "landscape ratio" for the stock:
    //
    //    +-----------------+
    //  w |                 |
    //    +-----------------+
    //             l
    //  L = l/w
    //
    // THIS IS VERY CRITICAL.
    //
    , L = stocksize.x/stocksize.y 
    , f0=_f0*L
    , fr=_fr*L
    
    
    // Exclude undersized stocks --- there shouldn't have any fitness
    // if the stock is smaller than cut.
    //  
    // To avoid rounding error, need to use isequal() but not just:
    //    , rtn =  [ sx<cx || sy<cy ? undef: f0*L  
    //             , sx<cy || sy<cx ? undef: fr*L  
    //             ] 
    , rtn =  [ (isequal(sx,cx)||sx>cx) && (isequal(sy,cy)||sy>cy)? f0:undef  
             , (isequal(sx,cy)||sx>cy) && (isequal(sy,cx)||sy>cx)? fr:undef  
             ] 
    )
 //echo( log_(["L (stock landscape ratio)",L],4) )
 //echo( log_(["_f0",_f0, "_fr",_fr, "f0",f0, "fr", fr],4) )
 //echo( log_(["sx",sx,"sy",sy, "cx",cx,"cy",cy],4))
 //echo( log_(["sx&lt;cx",sx<cx,"sy&lt;cy",isequal(sy,cy), "sx&lt;cy",sx<cy,"sy&lt;cx",sy<cx],4))
 echo( log_e(["stocksize", stocksize, "fitness [as is, rotated]", rtn
             ],4))
 rtn
);
 


function cl2d_sorted_fitnesses_1914( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       , stockWeights // 1914: optional, h{stock_i:weight}
                                       //         weight: number, default=1
                       )=
( /*
    Decide which stock is the best fit for the cutsize.
    Return a sorted array [ ["fitness",f, "stock",i, "rot",j ]
                          , ["fitness",f, "stock",i, "rot",j ]
                          , ...]
   (sorted based on f, from small to large)                       

   For special cases where f=1 like case A and B below:

    A             B   
   +--.-----+    +--.-------------+
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   +--.-----+    +--.-------------+
    
   f= 1 for both cases. But obviously we want A to have the priority
   (fill and used up the smaller stock first)
   
   So f=1 needs special treatment. 
    
    
 */
  echo(log_b( str( "cl2d_sorted_fitnesses_1914"
                , _s(" ( stocksizs={_},cutsize={_} )",[stocksizes, cutsize])
                ), 3) )
 
  let(  
       _fits= [ for(i = range(stocksizes) )
                 let(fs = echo(log_(_s("Calc fitness of cut {_} to stock {_}", [cutsize,stocksizes[i]]),3))
                          cl2d_fitness_1914( stocksize = stocksizes[i], cutsize = cutsize ))
                 // fs = [ [a,b], [c,undef], [d,e] ...]
                 each [ for(f=range(fs))
                         if( fs[f]!=undef ) [fs[f],[i,f]]
                      ]    
              ]
      // fits = arr[fit, [stock_i, is_rot]]
      //      =[[2.56055, [1, 0]], [0.5, [2, 1]], ...]
      
//     , fits= [ for( fir= _fits )
//                let( stock_i= fir[1][0] 
//                   , weight= h(stockWeights, stock_i,1)
//                   )
//                 [fir[0]*weight, fir[1] ]  
//             ] 
     , sfits = sortArrs(_fits,0)
     
     , rtn = [ for(x=sfits)
               ["fitness", x[0], "stock", x[1][0], "rot", x[1][1] ]
             ]          
     )
   echo( log_(["fits (a[fitness,[stock_i, is_rot]])", _fits ],3))
   echo( log_e(["sorted fitness", rtn],3))
   rtn
);


function cl2d_1914( stocks // h{site:size} = [[x,y],[a,b],[x,y],[a,b]...] 
                  , parts // [ [x,y], [x,y] .... ]
                  //, stocks // if given, stocksize is ignored 
                  
                  , stop_i // Set to an int to stop the iteration midway for debug
                  , _stocks // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _cuts   // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]          
                  , _i=-1
                  
                  )=
(
   echo(log_b(
       _i==-1?
         _ss("<b>cl2d_1914</b>(stocks={_:b},parts={_:b}: init(_i=<b>-1</b>)", [stocks,parts])  
        :_ss("<b>cl2d_1914</b>(_stocks={_:c=red}, _cuts={_:c=red}, _i={_:b;c=red} )", [_stocks,_cuts, _span(_i,"font-size:14px")])
       , 1))  
   _i==-1? // init 
   let(             
       // Make sure stocks is in landscape mode:
      , _stocks = [ for( i = [0:len(stocks)/2-1] )
                    let( site = stocks[i*2]
                       , size = stocks[i*2+1]
                       )
                    each [ site, size.x<size.y?[size.y,size.x]:size ]
                  ]              

      , _parts = reverse(
                   quicksort([  // Convert parts to sorted 
                              //   [ [[x,y],3], [[x,y],1], [[x,y],2] .... ]
                              // where 3,1,2 are the original index   
                    for( i=range(parts)) 
                        [ parts[i].x<parts[i].y  // flip x,y if y > x, for sort
                            ? [parts[i].y,parts[i].x]
                            : parts[i]
                        , i
                        ]  
                  ])
                )  
      )
      
      echo(log_(["_stocks", _bcolor(_b(_stocks),"yellow")],1))
      //echo(log_(["_parts(sorted parts)", _bcolor(_b(_parts),"yellow")],1))
      echo(log_(["_parts(parts sorted by long side => a[part_size, part_i])", _bcolor(_b(_parts),"yellow")],1))
      echo(log_e("Ready to cut parts one by one",1)) 
      cl2d_1914( stocks=stocks
                , parts=_parts
                //, stocks = _stocks 
                , stop_i= stop_i
                , _stocks= _stocks // [ [0,0], [x,y] ]
                , _cuts= [] 
                , _i=0 )
          
 :_i<len(parts) && (stop_i==undef?true: (_i<stop_i))?
   
   echo(log_(["part", parts[_i], "=> cut", _bcolor(_b(parts[_i][0]),"yellow")],1))     
   let( cut= parts[_i]  // [part_size([x,y]), part_index]
      , stocksizes = vals( _stocks )
      , cutsize = cut[0]
      , fitnesses = echo(log_("Calling cl2d_sorted_fitnesses_1914()",1))
                    cl2d_sorted_fitnesses_1914( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       )
                    // fitnesses: arr of ["fitness", 0.833333, "stock", 1, "rot", 1]
                    
      , target = last( fitnesses ) // The chosen stock with the largest fitness
      
      , stock_i = h(target, "stock")
      , cutpoint = keys( _stocks )[stock_i]
      , _cutsize= h(target, "rot")? reverse(cutsize): cutsize               
                       
      , __stocks = cl2d_makecut_1914( _stocks // [ [0,2], [4,1] 
                              // , [1,0], [3,3] 
                              // ] 
                     , [cutpoint, _cutsize]   // [[1,0],[2,1]]  
                     , _i
                     )                 
      , __cuts = concat(_cuts, [cutpoint, _cutsize ] )                     
      )
      //echo(log_(["parts", parts],1 ))
      //echo(log_(["cutpoint", cutpoint, "cutsize", cutsize],1 ))
      //echo(log_(["__stocks", __stocks],1))
      //echo(log_(["__cuts", __cuts],1))
      echo(log_e(str("End of _i=",_i),1)) 
      cl2d_1914( stocks=stocks
                , parts=parts
                //, stocks = _stocks 
                , stop_i= stop_i
                , _stocks= __stocks // [ [0,0], [x,y] ]
                , _cuts= __cuts 
                , _i=_i+1 )

   
  : 
  echo( log_(["_stocks", _stocks, "_cuts", _cuts],1))
  echo(log_e("Leaving cl2d_1914()",1))
  [_stocks,_cuts]
);  


stocksize= [8,4]; 
parts= [ [2,3], [5,4], [1,2] ];
parts= [ [2,3], [2,3], [1,2] ];
parts= [ [2,3], [5,1], [1,2] ]; 
parts= [ [2,4], [1,3], [1,3], [2,1],[2,1] ]; 
parts= [ [1,3], [1,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1],[3,1] ]; 
parts= [ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ];
parts= [ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ]; 
//
// Multiple copies
parts=[ [1,0.5,4], [2,3,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1],  [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3], [8,0.1,2], [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1], [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1,3], [1,2,3], [0.2,2,5] ];

stocksize=[ [8,2], [2,4] ];
//stocks=undef;
//cl2d_1914_test( stocksize=stocksize
              //, stocks=stocks 
//              , parts=parts 
//              );

module cl2d_1914_test( stocksize, parts
                     //, stocks // if given, ignore stocksize 
                     , show_stocks=false
                     , stop_i
                     //, stop_i=5 // <=== for debug
                     )
{

   echom("cl2d_1914_test()");
   
   //stocksize = [8,4];
   //parts = [ [2,3], [5,4], [1,2] ]; // nice
   
   
   //=========================================
   //parts = [ [2,3], [2,3], [1,2] ]; // last cutpoint error
   /*
      parts = [ [2,3], [2,3], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [2, 3], [2, 0], [2, 3], [2, 3], [2, 1]]
      _stocks=[[4, 0], [4, 4], [4, 3], [4, 1], [0, 3], [8, 1]]
      
         Error _stocks [[4, 3], [4, 1]] causing the [2,1] cut 
         up in the wrong place
      
      .---.---+=======+---.---.---.---.
      |   :   |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |      -|---.---.---.---.
      |       |       |   :   :   :   :
      |       |       |---.---.---.---.
      |       |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      
   */
   
   //=========================================
   //parts = [ [2,3], [5,1], [1,2] ];  
   /* parts = [ [2,3], [5,1], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
          f0= _f0==1? (1+ ( cx==sx?cy/sy:cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:cy/sx )):_f1
          
          
      cuts = [[0, 0], [5, 1], [5, 0], [3, 2], [5, 2], [1, 2]]
      stocks = [[6, 2], [2, 2], [0, 1], [8, 3]]  <=== wrong !!!
               should have been:
               [[6, 2], [2, 2], [0, 1], [5, 3]]
               
      .---.---.---.---.---+---+---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---|   |---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---+===+=======+
      |   :   :   :   :   |           |
      +===================+           |
      |                   |           |
      +===================+===========+
                                      
      Consider fix: when placing [3,2] on either [8,3] or [3,5]:
   
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      +===================+---.---.---.
      |                   |   :   :   :
      +===================+---.---.---.
     
      [8,3] is filled first. 
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [5, 1], [0, 1], [2, 3], [5, 0], [2, 1]]    
      _stocks=[[7, 0], [1, 4], [5, 1], [3, 3], [2, 1], [6, 3]]
      
      _stocks should have been:
                [[7, 0], [1, 4], [2, 1], [6, 3]]
          
      +=======+---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      +=======+===========+=======+---.
      |                   |       |   :
      +===================+=======+---.

   
   
   */
   
   //stocksize = [12,4];
   //parts = [ [2,3], [5,1], [1,2] ]; 
   
   //parts=[ [2,4], [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ]; // Error: cutpoint overlap
   //parts=[ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ];
   /*
      +=======+---.---.---+=======+===+
      |       |   :   :   |       |   |
      |       |---.---.---|       |   |
      |       |   :   :   |       |   |
      |       |---.---.---+=======+===+
      |       |   :   :   |       |   |
      +=======+===========+       |   |
      |                   |       |   |
      +===================+=======+===+
   
   Note: this doesn't look a good arrangement. HOWEVER, 
      it is better than what is thought to be a good one:
      
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |=======+=======+---.---.
      |       |       |       |   :   :
      |       |       |      -|---.---.
      |       |       |       |   :   :
      +=======+=======+===+===+===+---.
      |                   |       |   :
      +===================+=======+---.
   
   because it leaves out a square block. 
   
   */
   
   //parts= repeat( [ [1,3] ], 8 ); // ERROR cutpoints overlap !!! 
   //parts= concat( repeat( [ [1,2] ], 5 )
   //             , repeat( [ [1,3] ], 4 ) ); // ERROR cutpoints overlap !!! 
   //echo( log_b("cl2d_1914_test()") );
   echo(log_b(_ss("<b>cl2d_1914_test</b>(stocksize={_:b},parts={_:b}", [stocksize,parts]))) ; 
  
   _stocksize = isnum(stocksize[0])?[stocksize]:stocksize;
   
   __stocks = [ for( i=range( _stocksize) )  
                      let( sizexy = _stocksize[i] 
                         , base_y= i==0?0
                                   :sum( [for(j=[0:i-1]) _stocksize[j].y ])+ 0.2
                         )
                         each [ [0,base_y], sizexy ]
                    ]; 
   echo(log_(["stocksize", stocksize]));
   echo(log_(["_stocksize", _stocksize]));
   echo(log_(["__stocks", __stocks]));
                          
   // Make sure stocks is in landscape mode:
   _stocks = [ for(i = [0:len(__stocks)/2-1] )
                    let( site = __stocks[i*2]
                       , size = __stocks[i*2+1]
                       )
                    each [ site, size.x<size.y?[size.y,size.x]:size ]
                  ];              
   
   echo(log_(["_stocks (assure landscape stocks)", _stocks]));  
   
   _parts = [ each for(p=parts) 
               len(p)==3? repeat( [[p[0],p[1]]], p[2] ) 
                          : [[p[0],p[1]]] 
            ];
   echo(log_(["parts", parts]));
   echo(log_(["_parts (repeats expanded)", _parts]));
   
   //--------------------------------------------------
   echo(log_("Calling cl2d= cl2d_1914( stocks=_stocks, parts=_parts)",1));
   cl2d= cl2d_1914( stocks= _stocks //stocksize // [x,y] 
                  , parts=_parts 
                  , stop_i= stop_i
                  );
   echo(log_(["cl2d (=[stocks,cuts]) ", cl2d],1));
   echo(log_(["=> cl2d[0](remaining stocks)",cl2d[0], "cl2d[1](cuts)", cl2d[1]],1));
     
   //--------------------------------------------------               
   cuts= cl2d[1];
   stocks_left = cl2d[0];
   //echo(stocks_left= stocks_left);
   if(show_stocks)
   for(i = range( len(stocks_left)/2 ))
   {
      pts= get_block_pts( stocks_left[2*i], stocks_left[2*i+1]);
      //echo("stocks_left: ", i=i, stocks_left[2*i], pts=pts);
      Colors(i+1) Line( addz(pts,-0.5)
                      , closed=1);
      Colors(i+1,0.1) Plane( addz(pts,-0.5) );                 
   }
   
   
   //---------------------------------
   // Draw multiple original _stocks:                 
   for(sto=hashkvs(_stocks))
   {
     //echo(sto = sto);
     x=sto[0].x;
     y=sto[0].y;
     a=sto[1].x;
     b=sto[1].y;
     for(r= [y:b+y]) //x:x+a] ) 
     {  p2=[r*Y+x*X, (x+a)*X+r*Y];
        //echo(r=r, p2=p2);
        Line( p2 );
     }
     for(c= [x:a+x]) //x:x+a] ) 
     {  p2=[y*Y+c*X, (y+b)*Y+c*X];
        //echo(c=c, p2=p2);
        Line( p2 );
     }
   }
   //---------------------------------
   
      
     //x=stocksize[0];
     //y=stocksize[1];
     //for(r= range(y+1) ) Line( [O+r*Y, x*X+r*Y]  );
     //for(c= range(x+1) ) Line( [O+c*X, c*X+y*Y]  );
      
                    
                    
   
   for( i = range(keys(cuts))) //x0= keys(cuts) )
   {
      x0= cuts[2*i];
      _x0 = addz(x0,0);
      size = h(cuts, x0); 
      x=size.x;
      y=size.y;
      pts= addz( [ _x0, _x0+y*Y, _x0+x*X+y*Y, _x0+x*X], i*0.2) ;
      //echo(x0=x0, size=size, pts=pts);
      Colors(i+1,0.5)Plane(pts);
      Black() 
         MarkCenter( addz(pts,0.2)
                   , Label=["text",str(i,":[",x,",",y,"]"),"scale",.75]);
   }
   
   echo(log_e("End of cl2d_1914_test()"));
}


//. 19142

function cl2d_makecut_19142( stocks // [ [0,2], [4,1] 
                                   // , [1,0], [3,3] 
                                   // ] 
                     , cut  // [ [1,0], [2,1] ]  = [cutpt, cutsize]
                     , _i
                     )=
( 
  // Use new restock/restocks(). Great success !!
  // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]

     echo(log_b(str("cl2d_markcut_19142(",_ss("stocks={_:b;bc=yellow}, cut={_:c=red}: {_:b;bc=yellow})"
                                      ,[ str(stocks),_i==undef?"":_i, str(cut)])), 2)
         ) 
     let( _stocks = sortByCutptX( stocks )
        , newstocks = restocks( stocks, cut )
        )
     echo(log_e( ["newstocks", newstocks], 2))         
   newstocks
   );    



function cl2d_fitness_19142( stocksize // [x,y] 
                     , cutsize   // [x,y]       
                     )=
(/* Check how well a cut of cutsize fits to the stocksize 
  Return an array [a,b]
  
  where a,b are fitness for :
    (a) cut piece is as it is (=f0) or 
    (b) is rotated by 90 degree (=fr)
    
  The larger a or b is, the better fit.   
     
           sy +---------------+
        a     |               |
           cy +---------+     |
              |         |     |  
        b     |         |     |
              |         |     |
              +---------+-----+
                        cx    sx
                   c       d
                                   
   The init fitness f0 is defined as:
          
      f0= 1-((sx-cx)/sx) * ((sy-cy)/sy)
        = 1- a*d/(a+b)/(c+d)
        
   The larger the f0, the better fit of cut 
       
   if oversized ( cx>sx or cy>sy ): undef
  
 */
 echo(log_b( str( "cl2d_fitness_19142"
                , _ss(" ( cutsize={_:b}, stocksiz={_:b;c=blue} )",[cutsize, stocksize])
                ), 4) )
 let( sx = stocksize.x
    , sy = stocksize.y
    , cx = cutsize.x 
    , cy = cutsize.y 
    
    // The following criteria puts case A ahead of B, which is not what we want:
    //
    // , f0= (1-(sx-cx)/sx) + (1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx) + (1-(sy-cx)/sy)
    //
    // or
    //
    // , f0= (1-(sx-cx)/sx)*(1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx)*(1-(sy-cx)/sy)
    //
    // or
    //
    // , _f0= (cx/sx) * cy/sy 
    // , _fr= (cy/sx) * cx/sy 
    //
    //        A             B   
    //   +-----+    
    //   +--+  |    +--+-------------+    
    //   |  |  |    |  |             |    
    //   |  |  |    |  |             |    
    //   +--+--+    +--+-------------+   
    
    , _f0 = 1-((sx-cx)/sx) * ((sy-cy)/sy)  
    , _fr = 1-((sx-cy)/sx) * ((sy-cx)/sy) 
    
    // 1914: To make a long piece be filled first, 
    //       introduce a "landscape ratio" for the stock:
    //
    //    +-----------------+
    //  w |                 |
    //    +-----------------+
    //             l
    //  L = l/w
    //
    // THIS IS VERY CRITICAL.
    //
    , L = stocksize.x/stocksize.y 
    
    // 19142: srcStockWeight:
    //        if stocksize is [x,y,n] but not [x,y], n is the weighting 
    //        factor coming from source stock (the very original one(s))
    //        applied to it's fitness. This allows specific piece of source
    //        stock be filled first.  
    , srcWeight= und(stocksize[2],1) 
    , f0=_f0*L * srcWeight
    , fr=_fr*L * srcWeight 
    
    
    // Exclude undersized stocks --- there shouldn't have any fitness
    // if the stock is smaller than cut.
    //  
    // To avoid rounding error, need to use isequal() but not just:
    //    , rtn =  [ sx<cx || sy<cy ? undef: f0*L  
    //             , sx<cy || sy<cx ? undef: fr*L  
    //             ] 
    , rtn =  [ (isequal(sx,cx)||sx>cx) && (isequal(sy,cy)||sy>cy)? f0:undef  
             , (isequal(sx,cy)||sx>cy) && (isequal(sy,cx)||sy>cx)? fr:undef  
             ] 
    )
 //echo( log_(["L (stock landscape ratio)",L],4) )
 //echo( log_(["_f0",_f0, "_fr",_fr, "f0",f0, "fr", fr],4) )
 //echo( log_(["sx",sx,"sy",sy, "cx",cx,"cy",cy],4))
 //echo( log_(["sx&lt;cx",sx<cx,"sy&lt;cy",isequal(sy,cy), "sx&lt;cy",sx<cy,"sy&lt;cx",sy<cx],4))
 echo( log_e(["stocksize", stocksize, "srcWeight"
             , srcWeight, "fitness [as is, rotated]", _b(rtn)
             ],4))
 rtn
);
 


function cl2d_sorted_fitnesses_19142( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       , stockWeights // 19142: optional, h{stock_i:weight}
                                       //         weight: number, default=1
                       )=
( /*
    Decide which stock is the best fit for the cutsize.
    Return a sorted array [ ["fitness",f, "stock",i, "rot",j ]
                          , ["fitness",f, "stock",i, "rot",j ]
                          , ...]
   (sorted based on f, from small to large)                       

   For special cases where f=1 like case A and B below:

    A             B   
   +--.-----+    +--.-------------+
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   +--.-----+    +--.-------------+
    
   f= 1 for both cases. But obviously we want A to have the priority
   (fill and used up the smaller stock first)
   
   So f=1 needs special treatment. 
    
    
 */
  echo(log_b( str( "cl2d_sorted_fitnesses_19142"
                , _s(" ( stocksizs={_},cutsize={_} )",[stocksizes, cutsize])
                ), 3) )
 
  let(  
       _fits= [ for(i = range(stocksizes) )
                 let(fs = echo(log_(_s("Calc fitness of cut {_} to stock {_}", [cutsize,stocksizes[i]]),3))
                          cl2d_fitness_19142( stocksize = stocksizes[i], cutsize = cutsize ))
                 // fs = [ [a,b], [c,undef], [d,e] ...]
                 each [ for(f=range(fs))
                         if( fs[f]!=undef ) [fs[f],[i,f]]
                      ]    
              ]
      // fits = arr[fit, [stock_i, is_rot]]
      //      =[[2.56055, [1, 0]], [0.5, [2, 1]], ...]
      
//     , fits= [ for( fir= _fits )
//                let( stock_i= fir[1][0] 
//                   , weight= h(stockWeights, stock_i,1)
//                   )
//                 [fir[0]*weight, fir[1] ]  
//             ] 
     , sfits = sortArrs(_fits,0)
     
     , rtn = [ for(x=sfits)
               ["fitness", x[0], "stock", x[1][0], "rot", x[1][1] ]
             ]          
     )
   echo( log_(["fits (a[fitness,[stock_i, is_rot]])", _fits ],3))
   echo( log_e(["sorted fitness", rtn],3))
   rtn
);


function cl2d_19142( stocks // h{site:size} = [[x,y],[a,b],[x,y],[a,b]...] 
                  , parts // [ [x,y], [x,y] .... ]
                  //, stocks // if given, stocksize is ignored 
                  
                  , stop_i // Set to an int to stop the iteration midway for debug
                  , _stocks // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _cuts   // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _unfits // 19142: parts that are not fitted or run-outta stocks 
                            // [ [sx,sy],[sx,sy]...]                    
                  , _i=-1
                  
                  )=
(
   echo(log_b(
       _i==-1?
         _ss("<b>cl2d_19142</b>(stocks={_:b},parts={_:b}: init(_i=<b>-1</b>)", [stocks,parts])  
        :_ss("<b>cl2d_19142</b>(_stocks={_:c=red}, already _cuts={_:c=red}, _i={_:b;c=red} )", [_stocks,_cuts, _span(_i,"font-size:14px")])
       , 1))  
   _i==-1? // init 
   let(             
       // Make sure stocks is in landscape mode:
      , _stocks = [ for( i = [0:len(stocks)/2-1] )
                    let( site = stocks[i*2]
                       , size = stocks[i*2+1]
                       )
                    each [ site, size.x<size.y?[size.y,size.x]:size ]
                  ]              

      , _parts = reverse(
                   quicksort([  // Convert parts to sorted 
                              //   [ [[x,y],3], [[x,y],1], [[x,y],2] .... ]
                              // where 3,1,2 are the original index   
                    for( i=range(parts)) 
                        [ parts[i].x<parts[i].y  // flip x,y if y > x, for sort
                            ? [parts[i].y,parts[i].x]
                            : parts[i]
                        , i
                        ]  
                  ])
                )  
      )
      
      echo(log_(["_stocks", _bcolor(_b(_stocks),"yellow")],1))
      //echo(log_(["_parts(sorted parts)", _bcolor(_b(_parts),"yellow")],1))
      echo(log_(["_parts(parts sorted by long side => a[part_size, part_i])", _bcolor(_b(_parts),"yellow")],1))
      echo(log_e("Ready to cut parts one by one",1)) 
      cl2d_19142( stocks=stocks
                , parts=_parts
                //, stocks = _stocks 
                , stop_i= stop_i
                , _stocks= _stocks // [ [0,0], [x,y] ]
                , _cuts= []
                , _unfits=[] 
                , _i=0 )
          
 :_i<len(parts) && (stop_i==undef?true: (_i<stop_i))?
   
   echo(log_(["part", parts[_i], "=> cut", _bcolor(_b(parts[_i][0]),"yellow")],1))     
   let( cut= parts[_i]  // [part_size([x,y]), part_index]
      , stocksizes = vals( _stocks )
      , cutsize = cut[0]
      , fitnesses = echo(log_("Calling cl2d_sorted_fitnesses_19142()",1))
                    cl2d_sorted_fitnesses_19142( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       )
                    // fitnesses: arr of ["fitness", 0.833333, "stock", 1, "rot", 1]
      )
      ( fitnesses==[]? 
          let( __unfits= concat( _unfits, [parts[_i][0]] )
             )
          echo(log_(["fitnesses", fitnesses],1 ))
          echo(log_(str("===> fitnesses=[] => "
                       , _red("can't find any stock for cut ")
                       , _b(_red( cut[0]))
                       ) ,1 ))
          echo(log_(["__unfits", __unfits],1 ))
          echo(log_e(str("End of _i=",_i),1)) 
          cl2d_19142( stocks=stocks
                    , parts=parts
                    //, stocks = _stocks 
                    , stop_i= stop_i
                    , _stocks= _stocks // [ [0,0], [x,y] ]
                    , _cuts= _cuts 
                    , _unfits = __unfits
                    , _i=_i+1 )
      
                    
      :let(
           target = last( fitnesses ) // The chosen stock with the largest fitness
      
          , stock_i = h(target, "stock")
          , cutpoint = keys( _stocks )[stock_i]
          , _cutsize= h(target, "rot")? reverse(cutsize): cutsize               
                       
          , __stocks = cl2d_makecut_19142( _stocks // [ [0,2], [4,1] 
                                  // , [1,0], [3,3] 
                                  // ] 
                         , [cutpoint, _cutsize]   // [[1,0],[2,1]]  
                         , _i
                         )                 
          , __cuts = concat(_cuts, [cutpoint, _cutsize ] )                     
          )
          echo(log_(["fitnesses", fitnesses, "_unfits", _unfits],1 ))
          echo(log_e(str("End of _i=",_i),1)) 
          cl2d_19142( stocks=stocks
                    , parts=parts
                    //, stocks = _stocks 
                    , stop_i= stop_i
                    , _stocks= __stocks // [ [0,0], [x,y] ]
                    , _cuts= __cuts
                    , _unfits = _unfits 
                    , _i=_i+1 )
       ) 
   
  : 
  echo( log_(["_stocks", _stocks, "_cuts", _cuts],1))
  echo(log_e("Leaving cl2d_19142()",1))
  [_stocks,_cuts, _unfits]
);  


stocksize= [8,4]; 
parts= [ [2,3], [5,4], [1,2] ];
parts= [ [2,3], [2,3], [1,2] ];
parts= [ [2,3], [5,1], [1,2] ]; 
parts= [ [2,4], [1,3], [1,3], [2,1],[2,1] ]; 
parts= [ [1,3], [1,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1],[3,1] ]; 
parts= [ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ];
parts= [ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ]; 
//
// Multiple copies
parts=[ [1,0.5,4], [2,3,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1],  [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3], [8,0.1,2], [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1], [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1,3], [1,2,3], [0.2,2,5] ];

stocksize=[ [8,2], [2,4] ];
stocksize=[ [8,2], [2,4,10] ];
stocksize=[ [4,2], [2,4], [5,5] ];
stocksize=[ [5,5], [4,2,10], [2,4] ];
stocksize=[ [5,5], [4,3,10], [2,4] ];
stocksize=[ [5,5], [4,3], [2,4,10] ];
stocksize=[ [5,5,10], [4,3], [2,4] ];
stocksize=[ [5,5,10], [4,3,10], [2,4] ];
stocksize=[ [5,5,10], [4,3,20], [2,4] ];
//stocksize=[ [5,5,10], [4,3], [2,4,10] ];
//stocksize=[ [5,5,10], [4,3], [2,4,20] ];

//stocks=undef;

stocksize=[ [10,10] ];
parts=[ [4,8],[7,3],[3,3,2]];
parts=[ [7,6], [4,3,2], [4,2] ];
parts=[ [7,6], [4,3,2], [4,2,2] ];

//cl2d_19142_test( stocksize=stocksize
//              , parts=parts 
//              );

module cl2d_19142_test( stocksize, parts
                     //, stocks // if given, ignore stocksize 
                     , show_stocks=false
                     , stop_i
                     //, stop_i=5 // <=== for debug
                     )
{

   echom("cl2d_19142_test()");
   
   //stocksize = [8,4];
   //parts = [ [2,3], [5,4], [1,2] ]; // nice
   
   
   //=========================================
   //parts = [ [2,3], [2,3], [1,2] ]; // last cutpoint error
   /*
      parts = [ [2,3], [2,3], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [2, 3], [2, 0], [2, 3], [2, 3], [2, 1]]
      _stocks=[[4, 0], [4, 4], [4, 3], [4, 1], [0, 3], [8, 1]]
      
         Error _stocks [[4, 3], [4, 1]] causing the [2,1] cut 
         up in the wrong place
      
      .---.---+=======+---.---.---.---.
      |   :   |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |      -|---.---.---.---.
      |       |       |   :   :   :   :
      |       |       |---.---.---.---.
      |       |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      
   */
   
   //=========================================
   //parts = [ [2,3], [5,1], [1,2] ];  
   /* parts = [ [2,3], [5,1], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
          f0= _f0==1? (1+ ( cx==sx?cy/sy:cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:cy/sx )):_f1
          
          
      cuts = [[0, 0], [5, 1], [5, 0], [3, 2], [5, 2], [1, 2]]
      stocks = [[6, 2], [2, 2], [0, 1], [8, 3]]  <=== wrong !!!
               should have been:
               [[6, 2], [2, 2], [0, 1], [5, 3]]
               
      .---.---.---.---.---+---+---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---|   |---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---+===+=======+
      |   :   :   :   :   |           |
      +===================+           |
      |                   |           |
      +===================+===========+
                                      
      Consider fix: when placing [3,2] on either [8,3] or [3,5]:
   
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      +===================+---.---.---.
      |                   |   :   :   :
      +===================+---.---.---.
     
      [8,3] is filled first. 
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [5, 1], [0, 1], [2, 3], [5, 0], [2, 1]]    
      _stocks=[[7, 0], [1, 4], [5, 1], [3, 3], [2, 1], [6, 3]]
      
      _stocks should have been:
                [[7, 0], [1, 4], [2, 1], [6, 3]]
          
      +=======+---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      +=======+===========+=======+---.
      |                   |       |   :
      +===================+=======+---.

   
   
   */
   
   //stocksize = [12,4];
   //parts = [ [2,3], [5,1], [1,2] ]; 
   
   //parts=[ [2,4], [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ]; // Error: cutpoint overlap
   //parts=[ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ];
   /*
      +=======+---.---.---+=======+===+
      |       |   :   :   |       |   |
      |       |---.---.---|       |   |
      |       |   :   :   |       |   |
      |       |---.---.---+=======+===+
      |       |   :   :   |       |   |
      +=======+===========+       |   |
      |                   |       |   |
      +===================+=======+===+
   
   Note: this doesn't look a good arrangement. HOWEVER, 
      it is better than what is thought to be a good one:
      
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |=======+=======+---.---.
      |       |       |       |   :   :
      |       |       |      -|---.---.
      |       |       |       |   :   :
      +=======+=======+===+===+===+---.
      |                   |       |   :
      +===================+=======+---.
   
   because it leaves out a square block. 
   
   */
   
   //parts= repeat( [ [1,3] ], 8 ); // ERROR cutpoints overlap !!! 
   //parts= concat( repeat( [ [1,2] ], 5 )
   //             , repeat( [ [1,3] ], 4 ) ); // ERROR cutpoints overlap !!! 
   //echo( log_b("cl2d_19142_test()") );
   echo(log_b(_ss("<b>cl2d_19142_test</b>(stocksize={_:b},parts={_:b}", [stocksize,parts]))) ; 
  
   __stocksize = isnum(stocksize[0])?[stocksize]:stocksize;
   
   _stocksize = [ for(xy=__stocksize)
                    xy.x>xy.y? [xy[0],xy[1],und(xy[2],1) ] 
                             : [xy[1],xy[0],und(xy[2],1) ] 
   
                ];
                
   __stocks = [ for( i=range( _stocksize) )  
                      let( sizexy = _stocksize[i] 
                         , base_y= i==0?0
                                   :sum( [for(j=[0:i-1]) _stocksize[j].y ])+ 0.2*i
                         )
                         each [ [0,base_y], sizexy ]
                    ]; 
   echo(log_(["stocksize", stocksize]));
   echo(log_(["_stocksize", _stocksize]));
   echo(log_(["__stocks", __stocks]));
                          
   // Make sure stocks is in landscape mode:
//   _stocks = [ for(i = [0:len(__stocks)/2-1] )
//                    let( site = __stocks[i*2]
//                       , size = __stocks[i*2+1]
//                       )
//                    each [ site, size.x<size.y?[size.y,size.x, size[2]]:size ]
//                  ];              
   
   _stocks = __stocks;
   
   echo(log_(["_stocks (assure landscape stocks)", _stocks]));  
   
   _parts = [ each for(p=parts) 
               len(p)==3? repeat( [[p[0],p[1]]], p[2] ) 
                          : [[p[0],p[1]]] 
            ];
   echo(log_(["parts", parts]));
   echo(log_(["_parts (repeats expanded)", _parts]));
   
   //--------------------------------------------------
   echo(log_("Calling cl2d= cl2d_19142( stocks=_stocks, parts=_parts)",1));
   cl2d= cl2d_19142( stocks= _stocks //stocksize // [x,y] 
                  , parts=_parts 
                  , stop_i= stop_i
                  );
   echo(log_(["cl2d (=[stocks,cuts]) ", cl2d],1));
   echo(log_(["=> cl2d[0](remaining stocks)",cl2d[0]],1));
   echo(log_(["=> cl2d[1](cuts)",cl2d[1]],1));
   echo(log_(["=> cl2d[2](unfits)",cl2d[2]],1));
     
   //--------------------------------------------------               
   cuts= cl2d[1];
   stocks_left = cl2d[0];
   //echo(stocks_left= stocks_left);
   if(show_stocks)
   for(i = range( len(stocks_left)/2 ))
   {
      pts= get_block_pts( stocks_left[2*i], stocks_left[2*i+1]);
      //echo("stocks_left: ", i=i, stocks_left[2*i], pts=pts);
      Colors(i+1) Line( addz(pts,-0.5)
                      , closed=1);
      Colors(i+1,0.1) Plane( addz(pts,-0.5) );                 
   }
   
   
   //---------------------------------
   // Draw multiple original _stocks:                 
   for(sto=hashkvs(_stocks))
   {
     //echo(sto = sto);
     x=sto[0].x;
     y=sto[0].y;
     a=sto[1].x; //max([sto[1][0], sto[1][1]]);
     b=sto[1].y; //min([sto[1][0], sto[1][1]]);
     for(r= [y:b+y]) //x:x+a] ) 
     {  p2=[r*Y+x*X, (x+a)*X+r*Y];
        //echo(r=r, p2=p2);
        Line( p2 );
     }
     for(c= [x:a+x]) //x:x+a] ) 
     {  p2=[y*Y+c*X, (y+b)*Y+c*X];
        //echo(c=c, p2=p2);
        Line( p2 );
     }
   }
   //---------------------------------
   
      
     //x=stocksize[0];
     //y=stocksize[1];
     //for(r= range(y+1) ) Line( [O+r*Y, x*X+r*Y]  );
     //for(c= range(x+1) ) Line( [O+c*X, c*X+y*Y]  );
      
                    
                    
   
   for( i = range(keys(cuts))) //x0= keys(cuts) )
   {
      x0= cuts[2*i];
      _x0 = addz(x0,0);
      size = h(cuts, x0); 
      x=size.x;
      y=size.y;
      pts= addz( [ _x0, _x0+y*Y, _x0+x*X+y*Y, _x0+x*X], i*0.2) ;
      //echo(x0=x0, size=size, pts=pts);
      Colors(i+1,0.5)Plane(pts);
      Black() 
         MarkCenter( addz(pts,0.2)
                   , Label=["text",str(i,":[",x,",",y,"]"),"scale",.75]);
   }
   
   echo(log_e("End of cl2d_19142_test()"));
}



//. 1917 -- LF (landscape factor); re-define fitness and data type for them; 

function cl2d_makecut_1917( stocks // [ [0,2], [4,1] 
                                   // , [1,0], [3,3] 
                                   // ] 
                     , cut  // [ [1,0], [2,1] ]  = [cutpt, cutsize]
                     , _i
                     )=
( 
  // Use new restock/restocks(). Great success !!
  // Make a cut (of size cutsize) at one of the stocks at cutpoint 
  // Return a new stocks
  //
  // stocks: hash{ cutpoint: cutsize }
  //       like: [ [0,2], [4,1] 
  //             , [1,0], [3,3] 
  //             ]      
  //     +-------------------+     
  //     |    :              |  1  
  //     +----+ - - - - - - -+     
  //   [0,2]  |              |     
  //          |              |  
  //          |              | 
  //          +--------------+  
  //        [1,0]      3
      
  // cutpoint: like [1,0]
  // cutsize : like [2,1]

     echo(log_b(str("cl2d_markcut_1917(",_ss("stocks={_:b;bc=lightgreen}, cut={_:c=red}: {_:b;bc=lightgreen})"
                                      ,[ str(stocks),_i==undef?"":_i, str(cut)])), 2)
         ) 
     let( _stocks = sortByCutptX( stocks )
        , newstocks = restocks( stocks, cut )
        )
     echo(log_e( ["newstocks", newstocks], 2))         
   newstocks
   );    



function cl2d_fitness_1917( stocksize // [x,y] 
                     , cutsize   // [x,y]  
                     , LF // 1917: Landscape Factor, default = len/width     
                     )=
(/* Check how well a cut of cutsize fits to the stocksize 
  Return an array [a,b]
  
  where a,b are fitness for :
    (a) cut piece is as it is (=f0) or 
    (b) is rotated by 90 degree (=fr)
    
  The larger a or b is, the better fit.   
     
           sy +---------------+
        a     |               |
           cy +---------+     |
              |         |     |  
        b     |         |     |
              |         |     |
              +---------+-----+
                        cx    sx
                   c       d
                                   
   The init fitness f0 is defined as:
          
      f0= 1-((sx-cx)/sx) * ((sy-cy)/sy)
        = 1- a*d/(a+b)/(c+d)
        
   The larger the f0, the better fit of cut 
       
   if oversized ( cx>sx or cy>sy ): undef
  
 */
 echo(log_b( str( "cl2d_fitness_1917"
                , _ss(" ( cutsize={_:b}, LF={_:b}, stocksiz={_:b;c=blue} )"
                     ,[cutsize, LF, stocksize])
                ), 4) )
 let( sx = stocksize.x
    , sy = stocksize.y
    , cx = cutsize.x 
    , cy = cutsize.y 
    
    // Exclude undersized stocks --- there shouldn't have any fitness
    // if the stock is smaller than cut.
    //  
    // To avoid rounding error, need to use isequal() but not just:
    //    , rtn =  [ sx<cx || sy<cy ? undef: f0*L  
    //             , sx<cy || sy<cx ? undef: fr*L  
    //             ] 
    , size_fits=[ (isequal(sx,cx)||sx>cx) && (isequal(sy,cy)||sy>cy)
                , (isequal(sx,cy)||sx>cy) && (isequal(sy,cx)||sy>cx)
                ]    
    
    // The following criteria puts case A ahead of B, which is not what we want:
    //
    // , f0= (1-(sx-cx)/sx) + (1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx) + (1-(sy-cx)/sy)
    //
    // or
    //
    // , f0= (1-(sx-cx)/sx)*(1-(sy-cy)/sy)
    // , fr= (1-(sx-cy)/sx)*(1-(sy-cx)/sy)
    //
    // or
    //
    // , _f0= (cx/sx) * cy/sy 
    // , _fr= (cy/sx) * cx/sy 
    //
    //        A             B   
    //   +-----+    
    //   +--+  |    +--+-------------+    
    //   |  |  |    |  |             |    
    //   |  |  |    |  |             |    
    //   +--+--+    +--+-------------+   
    
    , _f0 = 1-((sx-cx)/sx) * ((sy-cy)/sy)  
    , _fr = 1-((sx-cy)/sx) * ((sy-cx)/sy) 
    
    // 1914: To make a long piece be filled first, 
    //       introduce a "landscape ratio" for the stock:
    //
    //    +-----------------+
    //  w |                 |
    //    +-----------------+
    //             l
    //  L = l/w
    //
    // THIS IS VERY CRITICAL.
    //
    , L = stocksize.x/stocksize.y 
        
    // 1917: srcStockWeight:
    //        if stocksize is [x,y,n] but not [x,y], n is the weighting 
    //        factor coming from source stock (the very original one(s))
    //        applied to it's fitness. This allows specific piece of source
    //        stock be filled first.  
    , srcWeight= und(stocksize[2],1) 
    
    //, f0=_f0*LF * srcWeight
    //, fr=_fr*LF * srcWeight 
    
    , f0= (_f0 * srcWeight + L*und(LF,1))//: undef
    , fr= (_fr * srcWeight + L*und(LF,1))//: undef
    
    
    //    , rtn =  [ (isequal(sx,cx)||sx>cx) && (isequal(sy,cy)||sy>cy)? f0:undef  
//             , (isequal(sx,cy)||sx>cy) && (isequal(sy,cx)||sy>cx)? fr:undef  
//             , LF
//             ]

    // 1917: we replace the original rtn, which is [f0,f1] as the fitness for
    //       "asis" and "rotated", respectively, to [f,r,LF] where f is the 
    //       max between f0 and fr and r is 0 or 1 for "asis" or "rotated".
    //           
    //, size_fits=[ (isequal(sx,cx)||sx>cx) && (isequal(sy,cy)||sy>cy)
    //            , (isequal(sx,cy)||sx>cy) && (isequal(sy,cx)||sy>cx)
    //            ] 
    , rtn =  size_fits==[true,false]? [f0,stocksize,cutsize,LF]
            :size_fits==[false,true]? [fr,stocksize,,[cy,cx],LF]
            :size_fits==[false,false]? []
            :f0>fr? [f0,stocksize,cutsize,LF]:[fr,stocksize,[cy,cx],LF]
             
    )
 //echo( log_(["L (stock landscape ratio)",L],4) )
 //echo( log_(["_f0",_f0, "_fr",_fr, "f0",f0, "fr", fr],4) )
 //echo( log_(["sx",sx,"sy",sy, "cx",cx,"cy",cy],4))
 //echo( log_(["sx&lt;cx",sx<cx,"sy&lt;cy",isequal(sy,cy), "sx&lt;cy",sx<cy,"sy&lt;cx",sy<cx],4))
 echo( log_( [ "srcWeight", srcWeight, "L",L, "LF", LF, "size_fits", size_fits], 4))
 echo( log_( [ _s("_f0 (cut={_})", str(cutsize)), _f0               
             , _s("f0 (cut={_}){_}",[str(cutsize), size_fits[0]?"":_orange(" oversized! ")]), f0
             ], 4))
 echo( log_( [ _s("_fr (cut={_})", str([cy,cx])), _fr
             , _s("fr (cut={_}){_}",[str([cy,cx]), size_fits[1]?"":_orange(" oversized! ")]), fr
             ], 4))
 //echo( log_( [ _s("f0 (cut={_})", str(cutsize)), f0, _s("fr (cut={_})",str([cy,cx])), fr], 4))
 echo( log_e(["stocksize", stocksize 
             , "fitness [f, stocksize, cutsize,LF]", _b(_blue(rtn))
             ],4))
 rtn
);
 


function cl2d_best_fit_1917( stocksizes // [ [x,y], [x,y]...]
                       , cutsize    // [x,y]
                       , stockWeights // 19142: optional, h{stock_i:weight}
                                       //         weight: number, default=1
                       , LF // 1917: Landscape Factor (optional)
                       )=
( /*
    Decide which stock is the best fit for the cutsize.
    Return a sorted array [ ["fitness",f, "stocksize", stocksize, "stock",i, "rot",j ]
                          , ["fitness",f, "stocksize", stocksize, "stock",i, "rot",j ]
                          , ...]
   (sorted based on f, from small to large)  
   Like:
   
   	[ ["fitness", 0.25, "stocksize", [2, 8, 1], "stock_i", 1, "rot", 1]
   	, ["fitness", 0.72, "stocksize", [4, 5, 1], "stock_i", 0, "rot", 1]
   	, ["fitness", 0.8, "stocksize", [4, 5, 1], "stock_i", 0, "rot", 0]
   	]                      
   Which means: stock [4,5] w/o cut rotation is the best fit. 

   For special cases where f=1 like case A and B below:

    A             B   
   +--.-----+    +--.-------------+
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   |  :     |    |  :             |    
   +--.-----+    +--.-------------+
    
   f= 1 for both cases. But obviously we want A to have the priority
   (fill and used up the smaller stock first)
   
   So f=1 needs special treatment. 
    
    
 */
  echo(log_b( str( "cl2d_best_fit_1917"
                , _s(" ( stocksizs={_},cutsize={_}, LF={_} )",[stocksizes, cutsize,LF])
                ), 3) )
 
  let(  
       _fits= [ for(i = range(stocksizes) )
                 let(f = echo(log_(_s("Calc fitness of cut {_} to stock {_}, LF={_}", [cutsize,stocksizes[i], LF]),3))
                          cl2d_fitness_1917( stocksize = stocksizes[i]
                                           , cutsize = cutsize
                                           , LF = LF ))
                          // fs => a[[f,is_rot,LF]]
                  if(f)
                  [ f[0]  // fitness
                  , i     // stock_i
                  , f[1]  // stocksize
                  , f[2]  // cutsize
                  , f[3]  // LF
                  ]        
                 //each [ for(fi=range(fs)) // 
                 //        if( fs[fi]!=undef ) [fs[fi],[stocksizes[i], i,fi]]
                 //     ]    
              ]
       
      // fits = arr[fit, [stock_i, is_rot]]
      //      =[[2.56055, [1, 0]], [0.5, [2, 1]], ...]
      
//     , fits= [ for( fir= _fits )
//                let( stock_i= fir[1][0] 
//                   , weight= h(stockWeights, stock_i,1)
//                   )
//                 [fir[0]*weight, fir[1] ]  
//             ] 
     , sfits = len(_fits)>1?sortArrs(_fits,0):_fits
     , f = last(sfits)
     , rtn = f?[ "fitness", f[0], "stock_i", f[1]
             , "stocksize", f[2], "cutsize", f[3] 
             , "LF", f[4]
             ] :[]         
     )
   echo( log_(["_fits (a[fitness,stock_i, stocksize, cutsize, LF]])", _fits ],3))
   echo( log_(["sfits (a[fitness,stock_i, stocksize, cutsize, LF]])", sfits ],3))
   //echo( log_(["sorted fitness", rtn],3))
   echo( log_e(["best fit", rtn],3))
   rtn
);


function cl2d_1917( stocks // h{site:size} = [[x,y],[a,b],[x,y],[a,b]...] 
                  , parts // [ [x,y], [x,y] .... ]
                  //, stocks // if given, stocksize is ignored 
                  , LF  // Landscape Factor (1917)
                       
                  , stop_i // Set to an int to stop the iteration midway for debug
                  , _stocks // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _cuts   // [ [sx,sy], [cx,cy] 
                            // , [sx,sy], [cx,cy]
                            // , ... ]
                  , _unfits // 1917: parts that are not fitted or run-outta stocks 
                            // [ [sx,sy],[sx,sy]...]                    
                  , _i=-1
                  
                  )=
(
   echo(log_b(
       _i==-1?
         _ss("<b>cl2d_1917</b>(stocks={_:b},parts={_:b}, LF={_:b}): init(_i=<b>-1</b>)", [stocks,parts, LF])  
        :_ss("<b>cl2d_1917</b>(_stocks={_:c=red}, already _cuts={_:c=red}, _i={_:b;c=red} )", [_stocks,_cuts, _span(_i,"font-size:14px")])
       , 1))  
   _i==-1? // init 
   let(             
       // Make sure stocks is in landscape mode:
      , _stocks = [ for( i = [0:len(stocks)/2-1] )
                    let( site = stocks[i*2]
                       , size = stocks[i*2+1]
                       )
                    each [ site, size.x<size.y?[size.y,size.x]:size ]
                  ]              

      , _parts = reverse(
                   quicksort([  // Convert parts to sorted 
                              //   [ [[x,y],3], [[x,y],1], [[x,y],2] .... ]
                              // where 3,1,2 are the original index   
                    for( i=range(parts)) 
                        [ parts[i].x<parts[i].y  // flip x,y if y > x, for sort
                            ? [parts[i].y,parts[i].x]
                            : parts[i]
                        , i
                        ]  
                  ])
                )  
      )
      
      echo(log_(["_stocks", _bcolor(_b(_stocks),"lightgreen")],1))
      //echo(log_(["_parts(sorted parts)", _bcolor(_b(_parts),"yellow")],1))
      echo(log_(["_parts(parts sorted by long side => a[part_size, part_i])", _bcolor(_b(_parts),"lightgreen")],1))
      echo(log_e("Ready to cut parts one by one",1)) 
      cl2d_1917( stocks=stocks
                , parts=_parts
                , LF = LF  // Landscape Factor (1917)
                , stop_i= stop_i
                , _stocks= _stocks // [ [0,0], [x,y] ]
                , _cuts= []
                , _unfits=[] 
                , _i=0 )
          
 :_i<len(parts) && (stop_i==undef?true: (_i<stop_i))?
   
   echo(log_(["part", parts[_i], ", LF", LF, "=> cut", _bcolor(_b(parts[_i][0]),"lightgreen")],1))     
   let( cut= parts[_i]  // [part_size([x,y]), part_index]
      , stocksizes = vals( _stocks )
      , cutsize = cut[0]
      , bestfit = echo(log_( str("Calling cl2d_best_fit_1917(...LF=",LF,")"),1))
                    cl2d_best_fit_1917( stocksizes=stocksizes // [ [x,y], [x,y]...]
                       , cutsize=cutsize    // [x,y]
                       , LF=LF  // Landscape Factor (1917)
                       )
                    // =>    
                    // bestfit: arr of ["fitness", 0.833333, "stock", 1, "rot", 1]
      )
      ( bestfit==[]||_stocks==[]? 
          let( __unfits= concat( _unfits, [parts[_i][0]] )
             )
          echo(log_(["bestfit", bestfit],1 ))
          echo(log_(str("===> bestfit=[] => "
                       , _red("can't find any stock for cut ")
                       , _b(_red( cut[0]))
                       ) ,1 ))
          echo(log_(["__unfits", __unfits],1 ))
          echo(log_e(str("End of _i=",_i),1)) 
          cl2d_1917( stocks=stocks
                    , parts=parts
                    , LF = LF  // Landscape Factor (1917)
                    , stop_i= stop_i
                    , _stocks= _stocks // [ [0,0], [x,y] ]
                    , _cuts= _cuts 
                    , _unfits = __unfits
                    , _i=_i+1 )
      
                    
      :let(
           stock_i = h(bestfit, "stock_i")
          , stocksize= h(bestfit, "stocksize")
          , cutpoint = keys( _stocks )[stock_i]
          , _cutsize= h(bestfit, "cutsize") //h(bestfit, "rot")? reverse(cutsize): cutsize               
                       
          , __stocks = cl2d_makecut_1917( _stocks // [ [0,2], [4,1] 
                                  // , [1,0], [3,3] 
                                  // ] 
                         , [cutpoint, _cutsize]   // [[1,0],[2,1]]  
                         , _i
                         )                 
          , __cuts = concat(_cuts, [cutpoint, _cutsize ] )                     
          )
          echo(log_(["bestfit", bestfit, "_unfits", _unfits],1 ))
          echo(log_e(str("End of _i=",_i),1)) 
          cl2d_1917( stocks=stocks
                    , parts=parts
                    , LF = LF  // Landscape Factor (1917)
                    , stop_i= stop_i
                    , _stocks= __stocks // [ [0,0], [x,y] ]
                    , _cuts= __cuts
                    , _unfits = _unfits 
                    , _i=_i+1 )
       ) 
   
  : 
  echo( log_(["_stocks", _stocks, "_cuts", _cuts],1))
  echo(log_e("Leaving cl2d_1917()",1))
  [_stocks,_cuts, _unfits]
);  

 //: cl2d_1917_test
stocksize= [8,4]; 
parts= [ [2,3], [5,4], [1,2] ];
parts= [ [2,3], [2,3], [1,2] ];
parts= [ [2,3], [5,1], [1,2] ]; 
parts= [ [2,4], [1,3], [1,3], [2,1],[2,1] ]; 
parts= [ [1,3], [1,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ]; 
parts= [ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1],[3,1] ]; 
parts= [ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ];
parts= [ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ]; 
////
//// Multiple copies
parts=[ [1,0.5,4], [2,3,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1],  [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3], [8,0.1,2], [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1], [1,2,3], [0.2,2,5] ];
parts=[ [1,0.5,4], [2,3,1], [8,0.1,3], [1,2,3], [0.2,2,5] ];
//

// The following stocks are to demo the srcWeight in multi-stock conditions:
stocksize=[ [8,2], [2,4] ];
stocksize=[ [8,2], [2,4,10] ];
stocksize=[ [5,3], [4,2], [2,3] ];
stocksize=[ [5,3], [4,2], [2,3,10] ];
stocksize=[ [5,3], [4,2,10], [2,3,20] ];
stocksize=[ [5,3], [4,2,20], [2,3,10] ];
stocksize=[ [5,3], [4,2,10], [2,3] ];
stocksize=[ [5,3,10], [4,2], [2,3] ];
//stocksize=[ [5,5], [4,3,10], [2,4] ];
//stocksize=[ [5,5], [4,3], [2,4,10] ];
//stocksize=[ [5,5,10], [4,3], [2,4] ];
//stocksize=[ [5,5,10], [4,3,10], [2,4] ];
//stocksize=[ [5,5,10], [4,3,20], [2,4] ];
////stocksize=[ [5,5,10], [4,3], [2,4,10] ];
//stocksize=[ [5,5,10], [4,3], [2,4,20] ];
//stocksize=[ [5,5], [4,3], [2,4] ];
//stocksize=[ [5,5,20], [4,3], [2,4] ];
//
////stocks=undef;
//
//stocksize=[ [10,10] ];
//parts=[ [4,8],[7,3],[3,3,2]];
//parts=[ [7,6], [4,3,2], [4,2] ];
//parts=[ [7,6], [4,3,2], [4,2,2] ]; // This example shows that "fully-left-pack" is not always good
   // 1918: test this by changing LF   
   //
   // fitness for cutsize [4,3] at i=1 ([4,3],[3,4]):
   //
   //    LF    stock[10,3], stock[4,10]   best
   //  -----------------------------------------------
   //  undef   (4.3,os)  (1.4,1.25)    [10,3]/[4,3]
   //    0     (1,os)    (1,0.85)         "
   //   0.5    (2.67,os) (1.2,1.05)       " 
   //    1     (4.33,os) (1.4,1.25)       "
   //    2    (7.66,os)  (1.8, 1.65)      "  
   //   -1    (-2.33,os) (0.6,0.45)   [4,10]/[4,3] 
   //   -2    (-5.66,os) (0.2,0.05)       "
   //
   // The lower the LF, the less effect of "Lanscape Factor" ( = L = 10/3 )
   // LF < 0 ===> negative effect of Landscape Factor, making [4,10] more likely.
   
cl2d_1917_test( stocksize=stocksize
              , parts=parts
              //, LF = -2//0.8 // 0.1 
              );

module cl2d_1917_test( stocksize, parts
                     //, stocks // if given, ignore stocksize 
                     , show_stocks=false
                     , LF  // Landscape Factor (1917)
                     , stop_i
                     //, stop_i=5 // <=== for debug
                     )
{

   echom("cl2d_1917_test()");
   
   //stocksize = [8,4];
   //parts = [ [2,3], [5,4], [1,2] ]; // nice
   
   
   //=========================================
   //parts = [ [2,3], [2,3], [1,2] ]; // last cutpoint error
   /*
      parts = [ [2,3], [2,3], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [2, 3], [2, 0], [2, 3], [2, 3], [2, 1]]
      _stocks=[[4, 0], [4, 4], [4, 3], [4, 1], [0, 3], [8, 1]]
      
         Error _stocks [[4, 3], [4, 1]] causing the [2,1] cut 
         up in the wrong place
      
      .---.---+=======+---.---.---.---.
      |   :   |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |      -|---.---.---.---.
      |       |       |   :   :   :   :
      |       |       |---.---.---.---.
      |       |       |   :   :   :   :
      +=======+=======+---.---.---.---.
      
   */
   
   //=========================================
   //parts = [ [2,3], [5,1], [1,2] ];  
   /* parts = [ [2,3], [5,1], [1,2] ]
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
          f0= _f0==1? (1+ ( cx==sx?cy/sy:cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:cy/sx )):_f1
          
          
      cuts = [[0, 0], [5, 1], [5, 0], [3, 2], [5, 2], [1, 2]]
      stocks = [[6, 2], [2, 2], [0, 1], [8, 3]]  <=== wrong !!!
               should have been:
               [[6, 2], [2, 2], [0, 1], [5, 3]]
               
      .---.---.---.---.---+---+---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---|   |---.---.
      |   :   :   :   :   |   |   :   |
      .---.---.---.---.---+===+=======+
      |   :   :   :   :   |           |
      +===================+           |
      |                   |           |
      +===================+===========+
                                      
      Consider fix: when placing [3,2] on either [8,3] or [3,5]:
   
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      .---.---.---.---.---.---.---.---.
      |   :   :   :   :   :   :   :   :
      +===================+---.---.---.
      |                   |   :   :   :
      +===================+---.---.---.
     
      [8,3] is filled first. 
   
      when f0,f1 are assigned as below in cl2d_fitness_18cL(): 
      
          f0= _f0==1? (1+ ( cx==sx?cy/sy:2*cx/sx )):_f0
          f1= _f1==1? (1+ ( cy==sx?cx/sy:2*cy/sx )):_f1
          
      i.e., x2 for cases like [8,3] above (longer x lengh):
      
      _cuts=[[0, 0], [5, 1], [0, 1], [2, 3], [5, 0], [2, 1]]    
      _stocks=[[7, 0], [1, 4], [5, 1], [3, 3], [2, 1], [6, 3]]
      
      _stocks should have been:
                [[7, 0], [1, 4], [2, 1], [6, 3]]
          
      +=======+---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      |       |---.---.---.---.---.---.
      |       |   :   :   :   :   :   :
      +=======+===========+=======+---.
      |                   |       |   :
      +===================+=======+---.

   
   
   */
   
   //stocksize = [12,4];
   //parts = [ [2,3], [5,1], [1,2] ]; 
   
   //parts=[ [2,4], [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [1,3], [1,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [1,3], [1,4], [2,3], [2,1],[2,1] ];
   //parts=[ [2,4], [1,3], [2,3], [1,4], [2,3], [2,1],[2,1] ]; // Error: cutpoint overlap
   //parts=[ [1,2],[1,2], [1,5], [2,2], [2,2], [2,3] ];
   /*
      +=======+---.---.---+=======+===+
      |       |   :   :   |       |   |
      |       |---.---.---|       |   |
      |       |   :   :   |       |   |
      |       |---.---.---+=======+===+
      |       |   :   :   |       |   |
      +=======+===========+       |   |
      |                   |       |   |
      +===================+=======+===+
   
   Note: this doesn't look a good arrangement. HOWEVER, 
      it is better than what is thought to be a good one:
      
      +=======+=======+---.---.---.---.
      |       |       |   :   :   :   :
      |       |=======+=======+---.---.
      |       |       |       |   :   :
      |       |       |      -|---.---.
      |       |       |       |   :   :
      +=======+=======+===+===+===+---.
      |                   |       |   :
      +===================+=======+---.
   
   because it leaves out a square block. 
   
   */
   
   //parts= repeat( [ [1,3] ], 8 ); // ERROR cutpoints overlap !!! 
   //parts= concat( repeat( [ [1,2] ], 5 )
   //             , repeat( [ [1,3] ], 4 ) ); // ERROR cutpoints overlap !!! 
   //echo( log_b("cl2d_1917_test()") );
   echo(log_b(str("<b>cl2d_1917_test</b>("
                 , _ss( "stocksize={_:b},parts={_:b}, LF={_:b})"
                      , [stocksize,parts,LF])
                 ))) ; 
  
   __stocksize = isnum(stocksize[0])?[stocksize]:stocksize;
   
   _stocksize = [ for(xy=__stocksize)
                    xy.x>xy.y? [xy[0],xy[1],und(xy[2],1) ] 
                             : [xy[1],xy[0],und(xy[2],1) ] 
   
                ];
                
   __stocks = [ for( i=range( _stocksize) )  
                      let( sizexy = _stocksize[i] 
                         , base_y= i==0?0
                                   :sum( [for(j=[0:i-1]) _stocksize[j].y ])+ 0.2*i
                         )
                         each [ [0,base_y], sizexy ]
                    ]; 
   echo(log_(["stocksize", stocksize]));
   echo(log_(["_stocksize", _stocksize]));
   echo(log_(["__stocks", __stocks]));
                          
   // Make sure stocks is in landscape mode:
//   _stocks = [ for(i = [0:len(__stocks)/2-1] )
//                    let( site = __stocks[i*2]
//                       , size = __stocks[i*2+1]
//                       )
//                    each [ site, size.x<size.y?[size.y,size.x, size[2]]:size ]
//                  ];              
   
   _stocks = __stocks;
   
   echo(log_(["_stocks (assure landscape stocks)", _stocks]));  
   
   _parts = [ each for(p=parts) 
               len(p)==3? repeat( [[p[0],p[1]]], p[2] ) 
                          : [[p[0],p[1]]] 
            ];
   echo(log_(["parts", parts]));
   echo(log_(["_parts (repeats expanded)", _parts]));
   
   //--------------------------------------------------
   echo(log_(str("Calling cl2d= cl2d_1917( "
                , _ss("stocks={_:b}, parts={_:b}, LF={_:b})"
                     , [_stocks, _parts, LF])
                ) ,1));
   cl2d= cl2d_1917( stocks= _stocks //stocksize // [x,y] 
                  , parts=_parts 
                  , LF = LF  // Landscape Factor (1917)
                , stop_i= stop_i
                  );
   echo(log_(["cl2d (=[stocks,cuts]) ", cl2d],1));
   echo(log_(["=> cl2d[0](remaining stocks)",cl2d[0]],1));
   echo(log_(["=> cl2d[1](cuts)",cl2d[1]],1));
   if(cl2d[2])
   echo(log_(_span(str("=> cl2d[2](unfits) = ",_b(cl2d[2])),"color:red;background:yellow;font-size:12px"),1));
     
   //--------------------------------------------------               
   cuts= cl2d[1];
   stocks_left = cl2d[0];
   //echo(stocks_left= stocks_left);
   if(show_stocks)
   for(i = range( len(stocks_left)/2 ))
   {
      pts= get_block_pts( stocks_left[2*i], stocks_left[2*i+1]);
      //echo("stocks_left: ", i=i, stocks_left[2*i], pts=pts);
      Colors(i+1) Line( addz(pts,-0.5)
                      , closed=1);
      Colors(i+1,0.1) Plane( addz(pts,-0.5) );                 
   }
   
   
   //---------------------------------
   // Draw multiple original _stocks:                 
   for(sto=hashkvs(_stocks))
   {
     //echo(sto = sto);
     x=sto[0].x;
     y=sto[0].y;
     a=sto[1].x; //max([sto[1][0], sto[1][1]]);
     b=sto[1].y; //min([sto[1][0], sto[1][1]]);
     for(r= [y:b+y]) //x:x+a] ) 
     {  p2=[r*Y+x*X, (x+a)*X+r*Y];
        //echo(r=r, p2=p2);
        Line( p2 );
     }
     for(c= [x:a+x]) //x:x+a] ) 
     {  p2=[y*Y+c*X, (y+b)*Y+c*X];
        //echo(c=c, p2=p2);
        Line( p2 );
     }
   }
   //---------------------------------
   
      
     //x=stocksize[0];
     //y=stocksize[1];
     //for(r= range(y+1) ) Line( [O+r*Y, x*X+r*Y]  );
     //for(c= range(x+1) ) Line( [O+c*X, c*X+y*Y]  );
      
                    
                    
   
   for( i = range(keys(cuts))) //x0= keys(cuts) )
   {
      x0= cuts[2*i];
      _x0 = addz(x0,0);
      size = h(cuts, x0); 
      x=size.x;
      y=size.y;
      pts= addz( [ _x0, _x0+y*Y, _x0+x*X+y*Y, _x0+x*X], i*0.2) ;
      //echo(x0=x0, size=size, pts=pts);
      Colors(i+1,0.5)Plane(pts);
      Black() 
         MarkCenter( addz(pts,0.2)
                   , Label=["text",str(i,":[",x,",",y,"]"),"scale",.75]);
   }
   
   echo(log_e("End of cl2d_1917_test()"));
}



//. tools


//makecut_tester( [ [0,0],[5,5] ], [[0,0],[5,5]], [[0,5],[5,5],[5,0],O] ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
//makecut_tester( [ [0,0],[5,5] ], [[0,0],[5,3]], [[0,5],[5,5],[5,0],O] ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
//makecut_tester( [ [0,0],[5,5] ], [[0,0],[3,5]], [[0,5],[5,5],[5,0],O] ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
//makecut_tester( [ [0,0],[5,5] ], [[0,0],[1,3]], [[0,5],[5,5],[5,0],O] ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
//makecut_tester( [[3, 0], [2, 5], [1,1],[4,4], [0,4],[5,1]], [[1,1],[1,1]], [[0,5],[5,5],[5,0],[3,0],[3,1],[1,1],[1,4],[0,4]] ); // [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]

//----------------------------------
// cl2d_makecut_18cp-specific test (overlapping blocking):

//makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[1,2]], stockingMode="18cu" ); // 
//makecut_tester( [[2, 0], [3, 5], [1, 1], [4, 4], [0, 2], [5, 3]], [[1,1],[1,2]], [[0,5],[5,5],[5,0],[2,0],[2,1],[1,1],[1,2],[0,2]] ); // 
//makecut_tester( [[2, 0], [3, 5], [1, 1], [4, 4], [0, 2], [5, 3]], [[1,1],[1,3]], [[0,5],[5,5],[5,0],[2,0],[2,1],[1,1],[1,2],[0,2]] ); // 
//makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[2,1]] ); // 
//makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[3,1]] ); // 

//----------------------------------
// cl2d_makecut_18cu-specific test (overlapping blocking on both x and y):

//makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[1,1]] ); // 
//makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[3,2]] ); // 
//makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[4,2]] ); // 
//makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[3,3]] ); // 
//makecut_tester( [[2, 0], [3, 5], [1, 2], [4, 3], [0, 3], [5, 2]], [[1,2],[4,3]] ); // 

//----------------------------------
// cl2d_makecut_18cv-specific test (overlapping blocking):

//makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[1,2]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[1,3]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[4,3]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[1,4]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[1,5]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 
//makecut_tester( [[1, 0], [4, 5], [0, 3], [5, 2]], [[1,0],[4,5]], [[0,5],[5,5],[5,0],[1,0],[1,3],[0,3]] ); 

//makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[1,1]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[2,1]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[3,1]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[2,2]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[5,1]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 
//makecut_tester( [[2, 0], [3, 5], [0, 3], [5, 2]], [[0,3],[5,2]], [[0,5],[5,5],[5,0],[2,0],[2,3],[0,3]] ); 

//----------------------------------
// cl2d_makecut_18cv debugging:

//makecut_tester( [[5, 0], [3, 3], [6, 0], [2, 4]], [[5, 0], [1, 3]], [[5,3],[6,3],[6,4],[8,4],[8,0],[5,0] ] ); 
//makecut_tester( [[5, 0], [3, 3], [6, 0], [2, 4]], [[5, 0], [1, 2]], [[5,3],[6,3],[6,4],[8,4],[8,0],[5,0] ] ); 
//makecut_tester( [[1, 0], [7, 3], [3, 0], [5, 4]], [[1, 0], [1, 2]], [[1,0],[1,3],[3,3],[3,4],[8,4],[8,0] ] ); 
//makecut_tester( [[1, 0], [7, 3], [3, 0], [5, 4]], [[1, 0], [2, 2]], [[1,0],[1,3],[3,3],[3,4],[8,4],[8,0] ] ); 
//makecut_tester( [[1, 0], [7, 3], [3, 0], [5, 4]], [[1, 0], [3, 2]], [[1,0],[1,3],[3,3],[3,4],[8,4],[8,0] ] ); 


module makecut_tester( stocks
                     , cut
                     , framePts // optional
                     , stockingMode="1913"  // "18cL", "18cp", "18cu", "18cv" 
                     )
{
  /* visual demo for new makecut routines */
  
  /*
       +------------------------+    [ [0,3],[5,2], [1,1],[4,4], [3,0],[2,5] ] 
       |    :         :         |    
       |    :         :         |    
       |    :         :         |    
       +----+ - - - - - - - - - |     
     [0,3]  |         :         |     
            |         :         |     
            |         :         |     
            +---------+ - - - - |  
          [1,1]       |         | 
                      +---------+                      
                    [3,0]

   */
   echom("makecut_tester()");
   //_stocks = makecut( stocks, cut ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
   
   st = addz(getStocksTop(stocks));
   echo(stockstop = st);
   Line(st, r=0.1);
   
//   st2 = addz(getStocksRight(stocks));
//   echo(stocksright = st2);
//   Line(st2, r=0.1);
   
   stocks2 = stockingMode=="18cL"?
                cl2d_makecut_18cL( stocks, cut[0], cut[1] ) // = [[1, 0], [4, 5], [0, 3], [5, 2]]
             : stockingMode=="18cu"?
                cl2d_makecut_18cu( stocks, cut ) // = [[1, 0], [4, 5], [0, 3], [5, 2]]
             : stockingMode=="18cv"?
                cl2d_makecut_18cv( stocks, cut ) // = [[1, 0], [4, 5], [0, 3], [5, 2]]
             : stockingMode=="18cv2"?
                cl2d_makecut_18cv2( stocks, cut ) // = [[1, 0], [4, 5], [0, 3], [5, 2]]
             : stockingMode=="1913"?
                cl2d_makecut_1913( stocks, cut ) // = [[1, 0], [4, 5], [0, 3], [5, 2]]
             : cl2d_makecut_18cp( stocks, cut ); // = [[1, 0], [4, 5], [0, 3], [5, 2]]
                  
   echo(stocks = stocks);
   echo(stocks2 = stocks2);
   
   if(framePts) Line( addz(framePts,0), r=0.025, closed=1, color="black");
   else 
   Draw_frames([5,5], [[0,3],[1,2],[2,0]] );
   
   Draw_blocks(stocks2, raise=0.1);
   
   Purple(0.5) Line( get_block_pts( cut[0], cut[1] ), r=0.075, closed=1 );
   Black(0.1)Draw_block(cut);
   
}

 
  

function restock( stock   // [site, size] = [ [x,y],[a,b]]
                , cut     // [site, size] = [ [x,y],[a,b]]
                )=
(
  /* A stock = a square given as [site, size]
  
    stock [[x,y],[a,b]] 
        +-------------+
      b |             |
        |             |
        +-------------+
      [x,y]    a 

    is subjected to a "restock" when a part of it is cut away by
    a *cut* (also given as [site,size]):
              a
        C------D------E
        |             |
      b B      +======F==+
        |      |         |
        A------G         |
      [x,y]    +=========+
      
    restock() example above returns a hash h{site:size} containing 2 stocks:   
      
        C-------------E
        |             |
        B-------------F
      
        C------D
        |      |
        |      |
        |      |
        A------G
       
            
  
  */
   let( st_cp= stock[0] 
      , st_size= stock[1]
      , st_tr= sum( stock )  // top_right corner
      , st_weight = stock[2]==undef?1:stock[2] 
      
      , cut_cp= cut[0]
      , cut_size= cut[1]
      , cut_tr= sum(cut)
      
      , sw = und(stock[1][2],1) // 19142: Source Stock Weight --- 
          // 19142: srcWeight:
          //        if stocksize is [x,y,n] but not [x,y], n is the weighting 
          //        factor coming from source stock (the very original one(s))
          //        applied to it's fitness. This allows specific piece of source
          //        stock be filled first.  
    
   , rtn=   
   cut_tr.x<=st_cp.x || 
   cut_tr.y<=st_cp.y ||
   cut_cp.x>=st_tr.x ||
   cut_cp.y>=st_tr.y ? [stock]
   :[ 
    //    B--G---C
    //    |      |  
    // +==A==+   D
    // |     F---E
    // +=====+
     st_cp.y <= cut_tr.y && cut_tr.y< st_tr.y ?
        // ABCD
        [ [ st_cp.x, cut_tr.y], [st_size.x, st_tr.y-cut_tr.y, sw] ] : []
   , st_cp.x <= cut_tr.x && cut_tr.x < st_tr.x ?
        // FGCE
        [ [ cut_tr.x, st_cp.y], [st_tr.x-cut_tr.x, st_size.y, sw] ] : []     

    //      +==+==+   
    //  B---C     |
    //  E   +==F==+
    //  |      |
    //  A---D--G
   , st_cp.x < cut_cp.x && cut_cp.x <= st_tr.x ?
        // ABCD
        [ st_cp, [cut_cp.x-st_cp.x, st_size.y, sw] ]:[]
   , st_cp.y < cut_cp.y && cut_cp.y<= st_tr.y ?   
        // AEFG
        [ st_cp, [st_size.x, cut_cp.y-st_cp.y, sw] ]:[]   
   ]   
   )
   [ for(x=rtn) each x ]   
//   :  3 
);

//echo(test__ = sum( [[2,3], [2,3,4]] ));
//restock_tests();
module restock_tests()
{
   echom("restock_tests()");
   stock = [ [2,1], [5,3] ];
   
   cut = [ [1,0], [1,1] ];
   cut = [ [1,0], [3,3] ];
   cut = [ [1,0], [3,4] ];
   cut = [ [1,0], [6,3] ];
   cut = [ [1,0], [6,4] ];
   //---------------------
   cut = [ [1,1], [1,1] ];
   cut = [ [1,1], [3,2] ];
   cut = [ [1,1], [3,3] ];
   cut = [ [1,1], [6,2] ];
   cut = [ [1,1], [6,3] ];
   //---------------------
   cut = [ [2,1], [1,1] ];
   cut = [ [2,1], [3,2] ];
   cut = [ [2,1], [3,3] ];
   cut = [ [2,1], [6,2] ];
   cut = [ [2,1], [5,3] ];
   //---------------------
   cut = [ [3,1], [1,1] ];
   cut = [ [3,1], [3,2] ];
   cut = [ [3,1], [3,3] ];
   cut = [ [3,1], [6,2] ];
   cut = [ [3,1], [5,3] ];
   //---------------------
   cut = [ [2,2], [1,1] ];
   cut = [ [2,2], [3,2] ];
   cut = [ [2,2], [3,3] ];
   cut = [ [2,2], [6,2] ];
   cut = [ [2,2], [5,3] ];
   //---------------------
   cut = [ [3,2], [1,1] ];
//   cut = [ [3,2], [3,2] ];
//   cut = [ [3,2], [3,3] ];
//   cut = [ [3,2], [6,2] ];
//   cut = [ [3,2], [5,3] ];
//   //---------------------
//   cut = [ [7,1], [1,1] ];
//   cut = [ [7,1], [3,2] ];
//   cut = [ [7,1], [3,3] ];
//   cut = [ [7,1], [6,2] ];
//   cut = [ [7,1], [5,3] ];
//   //---------------------
//   cut = [ [7,2], [1,1] ];
//   cut = [ [7,2], [3,2] ];
//   cut = [ [7,2], [3,3] ];
//   cut = [ [7,2], [6,2] ];
//   cut = [ [7,2], [5,3] ];
   
   
   newstocks = restock( stock, cut );
   
   ss = [ each cut, each newstocks ];
   echo(newstocks = newstocks);
   Draw_blocks( ss, raise=0.25 );
   Line( get_block_pts( stock[0], stock[1]), r=0.05, closed=1 );
}


function restocks( stocks, cut )=
(
  echo( log_("Cut and re-arranging stocks: <b>restocks</b>(stocks,cut)",3) ) 
  let( _rtn=[ for( stock= hashkvs(stocks) )
            //echo( stock = stock )
            let( new = restock( stock, cut ) )
            //echo( new= new )
            each new
  	    ]
     )
  deInnerStocks( _rtn)   	    
);    
  


//restocks_tests();
module restocks_tests()
{
   echom("restocks_tests()");
   stocks =  [[2, 2], [5, 2], [4, 1], [3, 3]];
   
   cut = [ [1,0], [1,1] ];
   cut = [ [1,0], [3,3] ];
   cut = [ [1,0], [3,4] ];
   cut = [ [1,0], [6,3] ];
   cut = [ [1,0], [6,4] ];
   //---------------------
   cut = [ [1,1], [1,1] ];
   cut = [ [3,0], [3,3] ];
//   cut = [ [1,1], [3,3] ];
//   cut = [ [1,1], [6,2] ];
//   cut = [ [1,1], [6,3] ];
//   //---------------------
//   cut = [ [2,1], [1,1] ];
//   cut = [ [2,1], [3,2] ];
//   cut = [ [2,1], [3,3] ];
//   cut = [ [2,1], [6,2] ];
//   cut = [ [2,1], [5,3] ];
//   //---------------------
//   cut = [ [3,1], [1,1] ];
//   cut = [ [3,1], [3,2] ];
//   cut = [ [3,1], [3,3] ];
//   cut = [ [3,1], [6,2] ];
//   cut = [ [3,1], [5,3] ];
//   //---------------------
//   cut = [ [2,2], [1,1] ];
//   cut = [ [2,2], [3,2] ];
//   cut = [ [2,2], [3,3] ];
//   cut = [ [2,2], [6,2] ];
//   cut = [ [2,2], [5,3] ];
//   //---------------------
//   cut = [ [3,2], [1,1] ];
//   cut = [ [3,2], [3,2] ];
//   cut = [ [3,2], [3,3] ];
//   cut = [ [3,2], [6,2] ];
//   cut = [ [3,2], [5,3] ];
//   //---------------------
//   cut = [ [7,1], [1,1] ];
//   cut = [ [7,1], [3,2] ];
//   cut = [ [7,1], [3,3] ];
//   cut = [ [7,1], [6,2] ];
//   cut = [ [7,1], [5,3] ];
//   //---------------------
//   cut = [ [7,2], [1,1] ];
//   cut = [ [7,2], [3,2] ];
//   cut = [ [7,2], [3,3] ];
//   cut = [ [7,2], [6,2] ];
//   cut = [ [7,2], [5,3] ];
   
   
   _newstocks = restocks( stocks, cut );
   newstocks = deInnerStocks( _newstocks ); //restocks( _newstocks, cut ) );
   
   ss = [ each cut, each newstocks ];
   echo( stocks = stocks , cut=cut);
   echo( _newstocks = _newstocks);
   echo( newstocks = newstocks);
   //echo( newstocks2 = newstocks2);
   
   Draw_blocks( stocks, raise=0 );
   Draw_blocks( newstocks, raise=0.5 );
   //Line( get_block_pts( stock[0], stock[1]), r=0.05, closed=1 );
   Draw_frames( [7,4] );
   Black() Draw_blocks(cut, raise=-0.15);
   
}


function get_block_pts( site, size, z=0 )= // site=[x,y], size=[a,b] ==> 4 pts
(
  addz( [ site
        , [ site.x, site.y+size.y ]
        , [ site.x+size.x, site.y+size.y] 
        , [ site.x+size.x, site.y]
        ], z)
);


module Draw_block( block,z=0 )  // cut = [ site, size ]
{
   Plane( get_block_pts( block[0],  block[1], z ) );
}

function isInnerStock( small,big )=  // big, small: [site, size]
(
   // Check if small is an inner stock of big.
   let( b_tr= sum(big)
      , s_tr= sum(small)
      , chk = [ b_tr.x>=s_tr.x ?1:0
              , b_tr.y>=s_tr.y ?1:0
              , big[0].x <= small[0].x?1:0
              , big[0].y <= small[0].y ?1:0
              ]
      , isIn= sum(chk)==4
   )
   //echo( _b(big), big_cp= big[0], b_tr=b_tr )
   //echo( "isInnerStock() ", big=big, small=_color(small, isIn?"red":"green")
   //    , s_tr= _color(s_tr, isIn?"red":"green"), chk=chk, isIn = isIn
   //    , isIn? _b(str(small, " is in ", big)):"")
   isIn?1:0
);

function deInnerStocks(stocks)=  // stocks: h{site:size}
(                                // Remove all inner stocks 
   let( _stocks = quicksort( hashkvs( stocks )) 
      , rtn = [ for( small = _stocks )
                let(chk= sum( [ for( big = _stocks )
                                isInnerStock( small, big )
                              ]
                            )
                    )
                 if(chk<=1)
                  //echo(small=_bcolor(_b(small),"yellow"), chk=chk)   
                  small 
              ] 
      )
      //echo( "In deInnerStocks, ", stocks=stocks)
      //echo( "In deInnerStocks, ", _stocks=_stocks)
      [ for(ss=rtn) each ss ]
);


//Draw_frames(size=[5,5]);
//Draw_frames(size=[5,5], cutpts=[[1,3],[2,1]]);
//Draw_frames(size=[5,5], cutpts=[[0,3],[1,1], [2,0]]);
module Draw_frames( size=[5,3]  // optional total size [x,y] 
                  , cutpts 
                  )  
{
   /*
       +-----------------------+     
       |    :        :         |    
       |    :        :         |    
       |    :        :         |    
       +----+ - -  - - - - - - +     
     [0,3]  |        :         |     
            |        :         |     
            |        :         |     
            +--------+ - - - - |  
          [1,1]      |         | 
                     +-- ------+  
                   [3,0]
                   
    cutpts = [ [0,3],[1,1],[3,0]]              
   */                
   //echom("Draw_frames()");
   w=size.x; h=size.y;
   
   cpts = cutpts?
          [ for(i=range(cutpts))
            each [ cutpts[i]
                 , [ i==len(cutpts)-1? size.x:cutpts[i+1].x
                   , cutpts[i].y] ]
          , size, [ cutpts[0].x, size.y]
          ]
          : [ [size.x,0], [0,0], [0,size.y], size ] ;  
   
   //echo( cpts=addz(cpts,0) );
   Black() Line( addz(cpts,0), closed=true, r=0.025 );
   
   for(r= range(h+1) ) Line( [O+r*Y, w*X+r*Y]  );
   for(c= range(w+1) ) Line( [O+c*X, c*X+h*Y]  );   
}

//Draw_frames(size=[5,3], cutpts=[[0,2],[1,0]]);
//Draw_blocks([ [0,2], [4,1], [1,0], [2,2]]);
//Draw_frames([5,4]);
module Draw_blocks( blocks  // h{site:size}
                  , raise=0.01 // raise up block to check overlap
                  , markCenterOpt=[]
                  )  
{
   //echom("Draw_blocks()");
   for( i = range(keys(blocks))) 
   {
      blockpts= get_block_pts( site=blocks[2*i], size=blocks[2*i+1] , z=(i+1)*raise);
      //echo("In Draw_blocks: ", blockpts=blockpts);
      color(COLORS[i+1],0.4)Plane(blockpts);
      
      Black() MarkCenter( addz(blockpts,raise+0.1)
                        , Label=[ "text", str(i,":[",join(blocks[2*i+1],","),"]")
                                , "scale",1.5]
                        , opt=markCenterOpt);
                        
   }
   
}


function getStockSize( stocks )= // stocks: h{site:size} rtn: [sx,sy]
(
   let( xys = [ for(kv=hashkvs(stocks)) sum(kv) ] // [ top_right_of_blocks_[sx,sy] ]
      , x = max( [for(xy=xys) xy[0] ] )
      , y = max( [for(xy=xys) xy[1] ] )
      )
   [x,y]   
);



function getStocksTop(stocks)= // stocks: h{site:size}
(
   /* Stocks top is needed to fix errors caused by stocks w/ collapsed roof:
   
        D----E    stocks = [ [0,0], [2,1]   // ABFG
        |    |             , [1,0], [1,2] ] // HDEG
   B----C    F    blockpts= [ ABFG, HDEG ]
   |         |    blocktops=  [ BF, DE ]
   A----H----G    _blocktops= [ BC, DE ]
                  stockstop = BCDE
   */           
   let( blockpts = quicksort( [ for( i=[0:len(stocks)/2-1] )
                     get_block_pts( stocks[2*i], stocks[2*i+1] )
                   ])
      , blocktops= [ for(pts=blockpts) 
                      let(y=maxy(pts))
                      [ [minx(pts),y], [maxx(pts),y] ]
                   ] // = [ [[0,1],[2,1]], [[1,2],[2,2]] ]
      , _blocktops= len(blocktops)==1? blocktops
                    : [ for(i= [0:len(blocktops)-2] ) // blocktops[i]= [[1,2],[2,2]] 
                       each [ blocktops[i][0] 
                             , //echo(len(blocktops)-1, i=i,blocktops[i][0])
                               [ blocktops[i+1][0].x, blocktops[i][1].y ]
                             ]
                    , each last(blocktops)   
                    ]       
      )
      //echo("In getStocksTop, blockpts=", blockpts)
      //echo("In getStocksTop, blocktops=", blocktops)
      //echo("In getStocksTop, _blocktops=", _blocktops)
      addz( uniq(_blocktops) )
);

//function getStocksRight(stocks)= // stocks: h{site:size}
//(
//   /* 
//        D----E    stocks = [ [0,0], [2,1]   // ABFG
//        |    |             , [1,0], [1,2] ] // HDEG
//   B----C    F    blockpts= [ ABFG, HDEG ]
//   |         |    blockrights=  [ FG, EG ]
//   A----H----G    _blockrights= [ FG, EG ]
//                  stockstop = [ FG, EG ]
//   */           
//   let( blockpts = reverse(
//                     sortArrs( [ for( i=[0:len(stocks)/2-1] )
//                     get_block_pts( stocks[2*i], stocks[2*i+1] )
//                   ], by=1)
//                   )
//      , blockrights= [ for(pts=blockpts) 
//                      let(x=maxx(pts))
//                      [ [x, maxy(pts)], [x, miny(pts)] ]
//                   ] // = [ [[0,1],[2,1]], [[1,2],[2,2]] ]
//      , _blockrights= [ for(i= [0:len(blockrights)-2] ) // blocktops[i]= [[1,2],[2,2]] 
//                       each [ blockrights[i][0] 
//                             , echo(len(blockrights)-1, i=i,blockrights[i][0])
//                               [ blockrights[i+1][0].x, blockrights[i][1].y ]
//                             ]
//                    , each last(blockrights)   
//                    ]       
//      )
//      echo("In getStocksRight, blockpts=", blockpts)
//      echo("In getStocksRight, blockrights=", blockrights)
//      echo("In getStocksRight, _blockrights=", _blockrights)
//      addz( uniq(_blockrights) )
//);



function sortByCutptX( stocks )=
(  
   echo(log_("Sort stocks by site.x: <b>sortByCutptX</b>(stocks)", 3) )
   let( _stocks= [ for( i= range(keys(stocks)) )
                    [ stocks[2*i].x, [ stocks[2*i], stocks[2*i+1]] ]  
                 ] 
      , rtn = [ for( x= quicksort(_stocks) ) each x[1] ]
      )
   rtn   
);
   
   
   

//. logging 
//
// Set logging layer, used for the arg layer: log_(s, layer, ...)
// 
LOG_DPS = 1;  // DrawParts
LOG_DPS2= 2;  // DrawParts
LOG_DP  = 4;  // DrawPart

LOG_CL1 = 5;  // cutlist_for_1_mat
LOG_CL  = 4;  // cutlist
LOG_DCL = 3;  // DrawCutlist

LOG_PP  = 5;  // processParts

// cutlist
function log_cl_b(mode, add_ind, prompt, debug, hParams)=
   log_b( "cutlist()", LOG_CL, add_ind, prompt, debug, hParams); 
function log_cl(ss, mode, add_ind, prompt, debug, hParams)=
   log_(ss, LOG_CL, add_ind, prompt, debug, hParams); 
function log_cl_e(ss, mode, add_ind, prompt, debug, hParams)=
   log_e(ss?ss:"", LOG_CL, add_ind, prompt, debug, hParams); 
   
// cutlist_for_1_mat
function log_cl1_b(mode, add_ind, prompt, debug, hParams)=
   log_b( "cutlist_for_1_mat()", LOG_CL, add_ind, prompt, debug, hParams); 
function log_cl1(ss, mode, add_ind, prompt, debug, hParams)=
   log_(ss, LOG_CL, add_ind, prompt, debug, hParams); 
function log_cl1_e(ss, mode, add_ind, prompt, debug, hParams)=
   log_e(ss?ss:"", LOG_CL, add_ind, prompt, debug, hParams); 

// DrawCutlist
module log_dcl_b(mode, add_ind, prompt, debug, hParams){
   log_b("DrawCutlist", LOG_DCL, add_ind, prompt, debug, hParams); }
module log_dcl(ss, mode, add_ind, prompt, debug, hParams){
   log_(ss, LOG_DCL, add_ind, prompt, debug, hParams); }
module log_dcl_e(mode, add_ind, prompt, debug, hParams){
   log_e("DrawCutlist", LOG_DCL, add_ind, prompt, debug, hParams); }

// DrawPart
module log_dp_b(partname, mode, add_ind, prompt, debug, hParams){
   log_b(str("DrawPart(",_b(partname),")"), LOG_DP, add_ind, prompt, debug, hParams); }
module log_dp(ss, mode, add_ind, prompt, debug, hParams){
   log_(ss, LOG_DP, add_ind, prompt, debug, hParams); }
module log_dp_e(ss, mode, add_ind, prompt, debug, hParams){
   log_e( ss?ss:"", LOG_DP, add_ind, prompt, debug, hParams); }

// DrawParts
module log_dps_b(mode, add_ind, prompt, debug, hParams){
   log_b("DrawParts", LOG_DPS, add_ind, prompt, debug, hParams); }
module log_dps(ss, mode, add_ind, prompt, debug, hParams){
   log_(ss, LOG_DPS, add_ind, prompt, debug, hParams); }
module log_dps_e(ss, mode, add_ind, prompt, debug, hParams){
   log_e("DrawParts", LOG_DPS, add_ind, prompt, debug, hParams); }
   
module log_dps2_b(ss, mode, add_ind, prompt, debug, hParams){
   log_b( str("DrawParts", ss?" -- ":"", ss?_b(ss):"")
        , LOG_DPS2, add_ind, prompt, debug, hParams); }
module log_dps2(ss, mode, add_ind, prompt, debug, hParams){
   log_(ss, LOG_DPS2, add_ind, prompt, debug, hParams); }
module log_dps2_e(ss, mode, add_ind, prompt, debug, hParams){
   log_e( str("DrawParts", ss?" -- ":"", ss?_b(ss):"")
        , LOG_DPS2, add_ind, prompt, debug, hParams); }
// processParts
function log_pp_b(partname, mode, add_ind, prompt, debug, hParams)=
   log_b(str("processParts() -- ", _b(partname)), LOG_PP, add_ind, prompt, debug, hParams); 
function log_pp(ss, mode, add_ind, prompt, debug, hParams)=
   log_(ss, LOG_PP, add_ind, prompt, debug, hParams); 
function log_pp_e(ss,mode, add_ind, prompt, debug, hParams)=
   log_e(ss?ss:"", LOG_PP, add_ind, prompt, debug, hParams); 


//. ref
/*

* Branch and Bound algorithm
  https://en.wikipedia.org/wiki/Branch_and_bound


* PATTERN GENERATION FOR TWODIMENSIONAL CUTTING STOCK PROBLEM WITH LOCATION (2012)
  W. N. P Rodrigo1, W. B Daundasekera2 and A. A. I Perera3
  https://pdfs.semanticscholar.org/e670/93eb987900009d2e64a24fdc684706787eb4.pdf  
  http://www.ijmttjournal.org/Volume-3/issue-2/IJMTT-V3I2P504.pdf  
 
* Invited Review
  Cutting stock problems and solution procedures (1991)
  https://pdfs.semanticscholar.org/baa1/7869fc41e62bbf36776fc650f0179e8f43fc.pdf  
  Robert W. Haessler and Paul E. Sweeney

* Modeling Two-Dimensional Guillotine Cutting Problems via Integer Programming
  Fabio Furini, Enrico Malaguti, Dimitri Thomopulos, 2016
  http://www.optimization-online.org/DB_FILE/2014/11/4663.pdf
  https://pubsonline.informs.org/doi/10.1287/ijoc.2016.0710
  
* An Exact Method for the 2D Guillotine Strip Packing Problem (2009)
  Abdelghani Bekrar1 and Imed Kacem2
  http://www.kurims.kyoto-u.ac.jp/EMIS/journals/HOA/AOR/Volume2009/732010.pdf
  
  
* Algorithms for Two-Dimensional Bin Packing and Assignment Problems (2000 )
  Andrea Lodi Il Coordinatore I Tutori Prof. Giovanni Marro Prof. Silvano Martello
  https://pdfs.semanticscholar.org/c8fc/4b62b6fc4d9b5184377b0cc7a91a9cb72052.pdf  
  ## Looks like my algorithm
  

*/ 
/* Glossary

Column Generation
Branch and Bound
Strip packing problem
guillotine
dichotomic method vs brand-and-bound method 
    
*/  