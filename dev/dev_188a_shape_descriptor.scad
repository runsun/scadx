include <../scadx.scad>

///// 2D shape descriptor

ISLOG=1;

module 1_1_find_pulling_angle(steps=[1])
{
   echom("1_1_find_pulling_angle()");
   
   echo(_b("Step-1: Objective"));
   L=4;
   A=L*Y;
   oa = [O, A];
   MarkPts( oa, Label=["text","OA","color","red"], Line=["r",0.05] );   

   // let B be a pt on the 1st quadrant:
   B=[2,3,0];
   //MarkPts( [O,B], Label=["text","OB","color","blue"], Line=["r",0.05] );   
   MarkPt( B, Label=["text","B","color","red"],r=0.1);   

   _red( " How does the introduction of B affect the orientation of OA ? " );
   _red( " I.e., how much does B pull A away from Y axis ? " );
   
   //------------------------------------------------------
    C225 = anglePt( [A,O,B], a=22.5, len=3);
    C45  = anglePt( [A,O,B], a=45, len=3);
    C675 = anglePt( [A,O,B], a=67.5, len=3);
      
    J225 = projPt( C225, oa );
    J45  = projPt( C45,  oa );
    J675 = projPt( C675, oa );


   if( has(steps,2) )
   {
      echo(_b("Step-2: 3 scenarios -- C225,C45,C675"));
      
      MarkPts( [O,C225], Label=["text",["","C225"],"color","green"]
             , Line=["r",0.02] );   
      MarkPts( [O,C45], Label=["text",["","C45"],"color","green"]
             , Line=["r",0.02] );   
      MarkPts( [O,C675], Label=["text",["","C675"],"color","green"]
             , Line=["r",0.02] );   
   
      Arrow( [C225, J225] ); 
      Arrow( [C45, J45] ); 
      Arrow( [C675, J675] ); 
      Dim( [O,J675,C675], Label="h");
      Dim( [O,A,C225], Label="L", spacer=0.5);
      
     _green("Given 3 pts at 22.5, 45 and 67.5 degree (C225, C45, C675) :");
     _green("By observations:");
     _green("-- longer h ==> larger pull force");
     _green("-- smaller angle ==> larger pull force");
     _green("-- Pulling Force ~ prop_to ~ h*a ");
     _green("-- Normalized: ==>  <b>h/L*a</b> ");
   }
   if( has(steps,3) )
   {
      echo(_b("Step-3: 3 scenarios -- C225,C45,C675 (cont)"));
      //_blue( )
      
      D= anglePt([A,O,C225], len=4);
      Blue()
      MarkPts([O,D],Label=["text",["","D"], "color","blue"]);
      _blue("OD has same len as OA => h/L = 1");
      _blue(" h/L*a = a  ... we can't move OA by a (all the way to D), but only max at a/2");
      _blue(" So OA should be moved by <b>ah/2L</b> === the formula of this approach");
      
   }
   if( has(steps,4) )
   {
   
      echo(_b("Step-4: Apply"));
      da225= 22.5* dist(J225,O) / L / 2;
      da45 = 45  * dist(J45,O)  / L / 2;
      da675= 67.5* dist(J675,O) / L / 2;
      
      _purple( _s("d225={_}, d45={_}, d675={_}",[dist(J225,O),dist(J45,O),dist(J675,O)]));
      _purple( _s("da225={_}, da45={_}, da675={_}",[da225,da45,da675]));
      _purple( "Largest pulling force (=da) @ a=45 " );
      echo("");
      _purple( "Imperfection: when the pulling force is the point D, it is" );
      _purple( "  supposed to pull it to a/2= 22.5/2, but since we use " );
      _purple( "  the D projection on OA, this makes it less than 22.5/2. " );
      echo("");
      _purple( "We will leave it as it is and see how it performs in real cases." );
      
      E225= anglePt([A,O,C225], a= da225, len=L );
      Purple() MarkPts([O,E225],Label=["text",["","1"] ]);
      
      E45= anglePt([A,O,C225], a= da45, len=L );
      Teal() MarkPts([O,E45],Label=["text",["","2"]]);
      
      E675= anglePt([A,O,C225], a= da675, len=L );
      Olive() MarkPts([O,E675],Label=["text",["","3"]]);

   }
  
}
//1_1_find_pulling_angle([2,3,4]);

module 1_2_find_long_axis( )
{
   echom("1_2_find_long_axis()");

   echo( _red(" Find long axis of pts based on the approach obtained in find_pulling_angle()) " ));
   
   
   echo(_b("Step-1: prep pts"));
   
   pts= pts( nseg=10, m=5, noise=1, len=4);
   //_pts = randConvexPlane(15);
   //pts = [for(p=_pts) p-centroidPt(_pts)];
   
   echo( pts=pts );
   gs = groupByQuadrant(pts);
   qcs = quadCentroidPts( pts );

   MarkPt( centroidPt( pts ), "C", Ball=["r",0.08] );
   
   for( i=[0:3] )
     color(COLORS[i+1]) 
     {  
        MarkPt( qcs[i], r=0.08);
        if(isdevmode)
        MarkPts( gs[i], Label=false, Ball=["dec_r",false], r=0.05, Line=0);
     }
   
   //if(!isdevmode)
   //  Gold() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
     
   ax02= [ qcs[0],qcs[2] ];
   ax13= [ qcs[1],qcs[3] ];
   
   axi = dist(ax02)>dist(ax13)? 0:1;
   P_long = qcs[axi]; // pt that starts the long axis 
   
   //Red() Line( ax02, r= axi==0?0.05:0.02  );
   //Green() Line( ax13, r= axi==1?0.05:0.02 );

   Red() MarkPts( ax02, Line=["r", axi==0?0.05:0.02]  );
   Green() MarkPts( ax13, Line=["r", axi==1?0.05:0.02] );
   
   // angle adjustment (following the result of find_pulling_angle()) :
   
   xp = intsec( ax02, ax13 );
   MarkPt( xp, r=0.08, color="black");
   
   a01= angle( [qcs[0], xp,qcs[1]] );
   echo(a01=a01);
   
   /*
      If ax02 is the long axis, we could have 3 conditions:
      
         Y                Y
          | /             |     0  
         \|/  0       -._ |  _.-'` 
     -----+-----X    ----`+'`----X
         /|\         _.-'`|`-. 
     2  / |        2`     | 
 
      a01< 90           a01>90        a01 = 90
     closer_i=1     closer_i=3      closer_i=undef
       
    */
    
   
   if( axi==0 ) // "/"
   {
     if( a01<90 )
     {
       P_short= qcs[1]; 
       small_ang = a01;
       J = projPt( P_short, [xp, P_long] );
       Mark90( [P_short, J, P_long] );
       distL = dist(xp,P_long);
       distJ = dist(xp,J);
       delta_a = (distJ/distL)*small_ang/2;
       newp= anglePt( [P_long, xp, P_short], a= delta_a, len=distJ+distL );  
       Arrow( [P_short,J] );
       Black() MarkPts( [xp, newp], Label=["text",["","newp"]], r=0.02);
       Black() MarkPt(J, "J");
       echo( _s("P_short={_}, small_ang={_}, J={_}, delta_a={_}"
               , [P_short, small_ang, J, delta_a] ) );
               
     }
     else
     {
       P_short= qcs[3]; 
       small_ang = 180-a01;
       J = projPt( P_short, [xp, P_long] );
       Mark90( [P_short, J, P_long] );
       
       distL = dist(xp,P_long);
       distJ = dist(xp,J);
       delta_a = (distJ/distL)*small_ang/2;
       newp= anglePt( [P_long, xp, P_short], a= delta_a, len=distJ+distL );  
       Arrow( [P_short,J] );
       Black() MarkPts( [xp, newp], Label=["text",["","newp"]], r=0.02);
       Black() MarkPt(J, "J");
       echo( _s("P_short={_}, small_ang={_}, J={_}, delta_a={_}"
               , [P_short, small_ang, J, delta_a] ) );
     }
   }  
   else   // "\ "
   {
     if( a01<90 )
     {
       P_short= qcs[0]; 
       small_ang = a01;
       J = projPt( P_short, [xp, P_long] );
       Mark90( [P_short, J, P_long] );
       distL = dist(xp,P_long);
       distJ = dist(xp,J);
       delta_a = (distJ/distL)*small_ang/2;
       newp= anglePt( pqr=[P_long, xp, P_short], a= delta_a, len=distJ+distL );  
       
       Arrow( [P_short,J] );
       Black() MarkPts( [xp, newp], Label=["text",["","newp"]], r=0.02);
       Black() MarkPt(J, "J");
       echo( _s("P_short={_}, small_ang={_}, J={_}, delta_a={_}"
               , [P_short, small_ang, J, delta_a] ) );
               
     }
     else
     {
       P_short= qcs[2]; 
       small_ang = 180-a01;
       J = projPt( P_short, [xp, P_long] );
       Mark90( [P_short, J, P_long] );
       distL = dist(xp,P_long);
       distJ = dist(xp,J);
       delta_a = (distJ/distL)*small_ang/2;
       newp= anglePt( pqr=[P_long, xp, P_short], a= delta_a , len=distJ+distL );  
       Arrow( [P_short,J] );
       Black() MarkPts( [xp, newp], Label=["text",["","newp"]], r=0.02);
       Black() MarkPt(J, "J");
       echo( _s("P_short={_}, small_ang={_}, J={_}, delta_a={_}"
               , [P_short, small_ang, J, delta_a] ) );
     }
   }  
    
}
//1_2_find_long_axis( isdevmode=1);

module 1_3_find_long_axis_concise( )
{
   echom("1_3_find_long_axis_concise()");

   echo( _red("Consolidate some duplicate codes in previous 1_2_find_long_axis() " ));
   
   
   echo(_b("Step-1: prep pts"));
   
   pts= pts( nseg=10, m=5, noise=1, len=4);
   //_pts = randConvexPlane(15);
   //pts = [for(p=_pts) p-centroidPt(_pts)];
   
   echo( pts=pts );
   gs = groupByQuadrant(pts);
   qcs = quadCentroidPts( pts );

   MarkPt( centroidPt( pts ), "C", Ball=["r",0.08] );
   
   for( i=[0:3] )
     color(COLORS[i+1]) 
     {  
        MarkPt( qcs[i], r=0.08);
        if(isdevmode)
        MarkPts( gs[i], Label=false, Ball=["dec_r",false], r=0.05, Line=0);
     }
   
   if(!isdevmode)
     Gold() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
     
   ax02= [ qcs[0],qcs[2] ];
   ax13= [ qcs[1],qcs[3] ];
   
   axi = dist(ax02)>dist(ax13)? 0:1;
   P_long = qcs[axi]; // pt that starts the long axis 
   
   Red() Line( ax02, r= axi==0?0.05:0.02  );
   Green() Line( ax13, r= axi==1?0.05:0.02 );
   
   // angle adjustment (following the result of find_pulling_angle()) :
   
   xp = intsec( ax02, ax13 );
   MarkPt( xp, r=0.08, color="black");
   
   a01= angle( [qcs[0], xp,qcs[1]] );
   echo(a01=a01);
   
   /*
      If ax02 is the long axis, we could have 3 conditions:
      
         Y                Y
          | /             |     0  
         \|/  0       -._ |  _.-'` 
     -----+-----X    ----`+'`----X
         /|\         _.-'`|`-. 
     2  / |        2`     | 
 
      a01< 90           a01>90        a01 = 90
     closer_i=1     closer_i=3      closer_i=undef
       
    */
    
   small_ang= a01<90? a01:(180-a01);
   P_short = axi==0? 
               (a01<90? qcs[1]: qcs[3] )
             : (a01<90? qcs[0]: qcs[2] );
             
   J = projPt( P_short, [xp, P_long] );
   distL = dist(xp,P_long);
   distJ = dist(xp,J);
   delta_a = (distJ/distL)*small_ang/2;
   newp= anglePt( [P_long, xp, P_short], a= delta_a, len=distJ+distL );  
   Arrow( [P_short,J] );
   MarkPts( [xp, newp], Label=["text",["","newp"]], r=0.02);
   Black() MarkPt(J, "J");
   echo( _s("P_short={_}, small_ang={_}, J={_}, delta_a={_}"
           , [P_short, small_ang, J, delta_a] ) );
}
//1_3_find_long_axis_concise( isdevmode=0);

function 1_4_longAxis(pts)=
(
   echo(_b("1_4_longAxis() function:") )   
   echo(_red("Get longAxes on ONE side (quadrant 1&2) of the logitude") )   
   let( qcs = quadCentroidPts( pts )
      , ax02= [ qcs[0],qcs[2] ]
      , ax13= [ qcs[1],qcs[3] ]
      , axi = dist( ax02 )>
              dist( ax13 )? 0:1
      , P_long = qcs[axi] // pt that starts the long axis 
      , xp = intsec( ax02, ax13 )
      , a01= angle( [qcs[0], xp,qcs[1]] )
       /*
          If ax02 is the long axis, we could have 3 conditions:
      
             Y                Y
              | /             |     0  
             \|/  0       -._ |  _.-'` 
         -----+-----X    ----`+'`----X
             /|\         _.-'`|`-. 
         2  / |        2`     | 
 
          a01< 90           a01>90        a01 = 90
         closer_i=1     closer_i=3      closer_i=undef
       
        */
    
       , small_ang= a01<90? a01:(180-a01)
       , P_short = axi==0? 
                   (a01<90? qcs[1]: qcs[3] )
                 : (a01<90? qcs[0]: qcs[2] )
       , J = projPt( P_short, [xp, P_long] )
       , distL = dist(xp,P_long)
       , distJ = dist(xp,J)
       , delta_a = (distJ/distL)*small_ang/2
       , newP= anglePt( [P_long, xp, P_short], a= delta_a, len=distJ+distL )  
       )
    echo(qcs = qcs)
    echo(P_long= P_long, P_short=P_short)  
    echo( axi=axi, a01=a01, delta_a = delta_a)   
    [xp, newP]
);

module 1_5_demo_longAxis()
{
   echom("1_5_demo_longAxis()");

   echo( _red(" Find long axis of pts using longAxis() function " ));
   
   
   //--------------------------------------------
   // data-1: rand pts
   pts= pts( nseg=10, m=5, noise=.5, len=4);
   
   //--------------------------------------------
   // data-2: convex plane
   //_pts = randConvexPlane(4);
   //pts = [for(p=_pts) p-centroidPt(_pts)];
   
   //--------------------------------------------
   // data-3: square
   //x=5; y=3;
   //m=10;
   //noise=1;
   //_pts = [ for(p=[ x*X,O,y*Y, x*X+y*Y ]) p-[x/2,y/2,0] ];
   //pts = [ for(i=range(_pts)) 
                 //each [for(j=[0:m-1]) 
                        //_pts[i]+ randPt(d=noise, z=0)
                      //]
            //];
   //--------------------------------------------

   
   C= centroidPt(pts);
   N = N([pts[0],C,pts[1]]);
   MarkPts( [C,N], "CN" );
   //Mark90( [N,C,pts[0]]);
   //echo( pts=pts );
   axis= 1_4_longAxis(pts);
   
   MarkPts( axis, Label=false );
   Gold() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   
}
//1_5_demo_longAxis( );

function 1_6_longAxis_q34(pts)=
(
   echo(_b("1_6_longAxis_q34() function:") )   
   echo(_red("Get longAxes on THE OTHER side (quadrant 3&4) of the logitude") )   
   let( qcs = quadCentroidPts( pts )
      , ax02= [ qcs[0],qcs[2] ]
      , ax13= [ qcs[1],qcs[3] ]
      , axi = dist( ax02 )>
              dist( ax13 )? 2:3 // 0:1 for quadrant-1&2, 2:3 for quadrant-3&4
      , P_long = qcs[axi] // pt that starts the long axis 
      , xp = intsec( ax02, ax13 )
      , a01= angle( [qcs[0], xp,qcs[1]] ) // this is the same as a23
       /*
          If ax02 is the long axis, we could have 3 conditions:
      
             Y                Y
              | /             |     0  
             \|/  0       -._ |  _.-'` 
         -----+-----X    ----`+'`----X
             /|\         _.-'`|`-. 
         2  / |        2`     | 
 
          a01< 90           a01>90        a01 = 90
         closer_i=1     closer_i=3      closer_i=undef
       
        */
    
       , small_ang= a01<90? a01:(180-a01)
       , P_short = axi==2? 
                   (a01<90? qcs[1]: qcs[3] )
                 : (a01<90? qcs[0]: qcs[2] )
       , J = projPt( P_short, [xp, P_long] )
       , distL = dist(xp,P_long)
       , distJ = dist(xp,J)
       , delta_a = -(distJ/distL)*small_ang/2
       , newP= anglePt( [P_long, xp, P_short], a= delta_a, len=distJ+distL )  
       )
     echo(qcs = qcs)
     echo(P_long= P_long, P_short=P_short)  
     echo( axi=axi, a01=a01, delta_a = delta_a)   
     [xp, newP]
);

module 1_7_demo_longAxes( )
{
   echom("1_7_demo_longAxes()");

   echo( _red(" Find long axis of pts using 1_4_longAxis() & 1_6_longAxis_q34()) " ));
   
   
   //--------------------------------------------
   // data-1: rand pts
   pts= pts( nseg=10, m=5, noise=.5, len=4);
   
   //--------------------------------------------
   // data-2: convex plane
   //_pts = randConvexPlane(4);
   //pts = [for(p=_pts) p-centroidPt(_pts)];
   
   //--------------------------------------------
   // data-3: square
   //x=5; y=3;
   //m=10;
   //noise=1;
   //_pts = [ for(p=[ x*X,O,y*Y, x*X+y*Y ]) p-[x/2,y/2,0] ];
   //pts = [ for(i=range(_pts)) 
                 //each [for(j=[0:m-1]) 
                        //_pts[i]+ randPt(d=noise, z=0)
                      //]
            //];
   //--------------------------------------------

   
   C= centroidPt(pts);
   N = N([pts[0],C,pts[1]]);
   MarkPts( [C,N], "CN" );
   //Mark90( [N,C,pts[0]]);
   //echo( pts=pts );
   axis_q12= 1_4_longAxis(pts);
   axis_q34= 1_6_longAxis_q34(pts);
   
   Red() MarkPts( axis_q12, Label=false );
   Green() MarkPts( axis_q34, Label=false );
   
   Gold() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   
}
//1_7_demo_longAxes( );


module 1_8_evaluation( )
{
   echom("1_8_evaluation()");

   echo( _red("We use averaged ssd (sum squared distances) to evaluate the results" ));
   echo( _red("of both line segments (quadrant 1&2 side and quadrant 3&4 side)" ));
   echo( _red("and compare them to the a-ssd of the original line (pq) that" ));
   echo( _red("is used to generate the pts" ));
   
   
   //--------------------------------------------
   // data-1: rand pts
   nseg=10; 
   m=5;
   noise=.5;
   len=4; 
   pq= randPts(2);
   pq = [O, 4*X];
   _pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noise, len=len);
   pts = h(_pts_pq, "pts");
   _pq = h(_pts_pq, "translated_pq");
   
   //--------------------------------------------
   // data-2: convex plane
   //_pts = randConvexPlane(4);
   //pts = [for(p=_pts) p-centroidPt(_pts)];
   
   //--------------------------------------------
   // data-3: square
   //x=5; y=3;
   //m=10;
   //noise=1;
   //_pts = [ for(p=[ x*X,O,y*Y, x*X+y*Y ]) p-[x/2,y/2,0] ];
   //pts = [ for(i=range(_pts)) 
                 //each [for(j=[0:m-1]) 
                        //_pts[i]+ randPt(d=noise, z=0)
                      //]
            //];
   //--------------------------------------------

   
   C= centroidPt(pts);
   N = N([pts[0],C,pts[1]]);
   MarkPts( [C,N], "CN" );
   //Mark90( [N,C,pts[0]]);
   //echo( pts=pts );
   axis_q12= 1_4_longAxis(pts);
   axis_q34= 1_6_longAxis_q34(pts);   
   
   Red() MarkPts( axis_q12, Label=false );
   Green() MarkPts( axis_q34, Label=false );
  
   //==============================
   // we will connect the two ends of the two line segments 
   // (to form a straight line) to compare with the ssd of 
   // two separate segments.
    
   qcs= quadCentroidPts( pts );
   MarkPts(qcs,"1234");
   ax02= [ qcs[0],qcs[2] ];
   ax13= [ qcs[1],qcs[3] ];
   Black() Line( ax02 );
   Black() Line( ax13 );
   is02 = dist( ax02 )>dist( ax13 );
   axis_straight = is02? ax02:ax13;
   ssd_straight= avg_SSD(pts,axis_straight);
   Blue() Line( axis_straight );
   //==============================
   
   // This measure is to take the mid-pt of ax02 and ax13 to form a line
   ax_mid= [ sum(axis_q12)/2, sum(axis_q34)/2 ];
   ssd_mid= avg_SSD(pts,ax_mid) ;

   ssd0= avg_SSD(pts,_pq) ;
   ssd12= avg_SSD(pts,axis_q12);
   ssd34= avg_SSD(pts,axis_q34) ;
   echo("");
//   echo( _s("ssd0= {_}, ssd12= {_}, ssd34= {_}, ssd_straight={_}, ssd_mid={_}"
//         , [ _b(_brown( floor(ssd0*1000)/1000 ))
//           , _b(_red( floor(ssd12*1000)/1000 ))
//           , _b(_green( floor(ssd34*1000)/1000 ))
//           , _b(_blue( floor(ssd_straight*1000)/1000 ))
//           , _b(_purple( floor(ssd_mid*1000)/1000 ))
//           ] ));
   
   echo( "n:2N/L\tssd0 \tssd12(%)   \t ssd34(%) \t ssd_str(%) \t ssd_mid(%)" );
   echo( _s("{_} : {_} \t{_}\t{_}({_})\t{_}({_})\t{_}({_})\t{_}({_})"
           ,[ nseg*m, 2*noise/len
            //, ssd0, ssd12, ssd34, ssd_straight, ssd_mid
            , round(ssd0*100)/100 
            , round(ssd12*100)/100 , round((ssd12-ssd0)/ssd0*1000)/10
            , round(ssd34*100)/100 , round((ssd34-ssd0)/ssd0*1000)/10
            , round(ssd_straight*100)/100 , round((ssd_straight-ssd0)/ssd0*1000)/10
            , round(ssd_mid*100)/100 ,round((ssd_mid-ssd0)/ssd0*1000)/10
           
            ] ) );
   
   
   Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   Gold(0.2) Line( _pq, r=0.5 );
   
   /* Results
   
   ECHO: "n:2N/L	ssd0 	ssd12(%)   	 ssd34(%) 	 ssd_str(%) 	 ssd_mid(%)"
   ECHO: "50 : 1 	1.44	1.32(-8.7)	1.3(-9.7)	1.35(-6.4)	1.31(-9.3)"
   ECHO: "50 : 1 	1.36	1.29(-4.6)	1.29(-4.8)	1.32(-2.8)	1.29(-5)"
   ECHO: "50 : 1 	1.36	1.29(-4.6)	1.29(-4.8)	1.32(-2.8)	1.29(-5)"
   ECHO: "50 : 1 	1.3	1.33(2.4)	1.32(1.9)	1.39(7.4)	1.32(2)"
   ECHO: "50 : 1 	1.52	1.5(-1.5)	1.5(-1.4)	1.56(2.7)	1.5(-1.5)
   ECHO: "50 : 1 	1.54	inf(-1.7%)	1.53(-0.7%)	1.57(2.2%)	1.52(-1.3%)"
   ECHO: "50 : 1 	1.025	0.992(-3.3%)	0.994(-3%)	1.066(3.9%)	0.992(-3.2%)"
   
   ECHO: "50 : 0.75 	0.62	0.62(0.2)	0.62(0.2)	0.62(0.4)	0.62(0.2)"
   ECHO: "50 : 0.75 	0.93	0.97(4.9)	0.99(6.6)	1.06(14.2)	0.98(5.5)"
   ECHO: "50 : 0.75 	0.76	0.75(-1)	0.75(-0.8)	0.87(14)	0.75(-0.9)"
   ECHO: "50 : 0.75 	0.7	0.67(-5.3)	0.66(-5.5)	0.72(1.9)	0.66(-5.4)"
   ECHO: "50 : 0.75 	0.94	0.91(-3.9)	0.91(-3.8)	0.92(-2.9)	0.91(-3.9)"
   
   ECHO: "50 : 0.5 	0.27	0.28(2.2)	0.28(1.9)	0.31(14.8)	0.28(1.9)"
   ECHO: "50 : 0.5 	0.48	0.45(-5.5)	0.48(0.1)	0.57(17.9)	0.47(-2.2)"
   ECHO: "50 : 0.5 	0.3	0.3(-2.4)	0.29(-2.7)	0.29(-2.8)	0.3(-2.6)"
   ECHO: "50 : 0.5 	0.31	0.3(-1)		0.31(0.3)	0.44(44.5)	0.31(-0.2)"
   ECHO: "50 : 0.5 	0.24	0.24(-1.4)	0.24(-1.2)	0.28(16.2)	0.24(-1)"
   
   ECHO: "50 : 0.25 	0.09	0.09(6.9)	0.08(-4.3)	0.08(-7.9)	0.09(-0.9)"
   ECHO: "50 : 0.25 	0.07	0.07(2.4)	0.08(4.1)	0.07(2)		0.07(3)"
   ECHO: "50 : 0.25 	0.07	0.07(3.3)	0.07(4.5)	0.07(-1)	0.07(4)"
   ECHO: "50 : 0.25 	0.07	0.06(-13.6)	0.06(-13.7)	0.06(-14.3)	0.06(-13.6)"      
   ECHO: "50 : 0.25 	0.09	0.09(-4.3)	0.08(-9.7)	0.08(-8.4)	0.08(-7.1)"

   
   
   Observation:
   
   -- ssd_straight is always the worst one, so it can be eliminated in the future
   -- ssd_mid doesn't improve the result
   -- in almost all cases, ssd12 and ssd34 are similar and generally provide
      better result than ssd0 (the source). 
      
   Suggestion:
   
   	pick the smaller one between ssd12 and ssd34. 
   
   */      
   
}
//1_8_evaluation( );

function 1_9_longAxis(pts)=
(
   echo(_b("1_9_longAxis() function:") )   
   echo(_red("Get longAxes by picking from 1st and 2nd V that has the smaller aSSD ") )
   let( qcs = quadCentroidPts( pts )
      , ax02= [ qcs[0],qcs[2] ]
      , ax13= [ qcs[1],qcs[3] ]
      , d02 = dist( ax02 )
      , d13 = dist( ax13 )
      , xp = intsec( ax02, ax13 )
      , a01= angle( [qcs[0], xp,qcs[1]] )
      , small_a= a01<90? a01:(180-a01)
      , axlonger = d02>d13? ax02:ax13
      
      /* 
        pts 0~3 below are centroidPt of pts in 4 quadrant 
        
        4 conditions: d02>d13 ?  a01<90 ?
        In each conditions, we use 2 V-shape parts: v1 and v2   
            
        d02 > d13:                  
                     0                
              1     /                      0  
               \   /                     .'
                \ /            1-.._   .'
            -----xp-----X     ------`xp----- X
                / \                .'  `'-3
               /   \             .'         
             2/     3           2      
             
             a01 < 90             a01 > 90              
             v1: 0-xp-1           v1: 0-xp-3
             v2: 2-xp-3           v2: 2-xp-1

        d02 < d13: 
                         
             1                       
              \     0                        
               \   /          1          0
                \ /           `'-.._   .'
            -----xp-----X     ------`xp----- X
                / \                .'  `'-_
              2/   \              2        `'-3 
                    3                 
             
             a01 < 90             a01 > 90              
             v1: 1-xp-0           v1: 1-xp-2
             v2: 3-xp-2           v2: 3-xp-0
              
        For a V-shape 3 pointers  A-xp-B:
        
        - A, B is long, short arm, respectively;
        - Since A is the long arm, it indicates the orientation
        - Calc the extence B pulls the orientation away from A
          to get a new A, call it v1_newP ( or v2_newP in case of v2)
        - Calc the avg_SSD of pts on [xp, v1_newP] and on [xp, v2_newP]
        - Pick either v1_newP or v2_newP depending on which has less avg_SSD
        
        ==> [ xp, newP ] is our long axis 
        */   
         
      //======================================
      // The first V (V1):
      // 
      //    if axi1=0: either 0-xp-1 or 0-xp-3
      //    if axi1=1: either 1-xp-0 or 1-xp-2 
      //
      , v1i = d02 > d13 ? 0:1
      , v1j = v1i==0? ( a01<90?1:3 ): ( a01<90?0:2 ) 
      , v1P = qcs[v1i]   // pt on the longer arm, either qcs[0] or qcs[1]
      , v1Q = qcs[v1j]   // pt on the short arm, could be any in qcs 
      , v1J = projPt( v1Q, axlonger)
      , v1distL= dist( xp, v1P )  
      , v1distJ= dist( xp, v1J )  
      , v1_da = (v1distJ/v1distL)* small_a / 2
      , v1_newP= anglePt( [v1P, xp, v1Q], a= v1_da, len=v1distJ+v1distL ) 
      , v1_ssd = avg_SSD( pts, [xp, v1_newP] ) 
    
      //======================================
      // The 2nd V (v2):
      // 
      //    if axi1=0: either 0-xp-1 or 0-xp-3
      //    if axi1=1: either 1-xp-0 or 1-xp-2 
      //
      , v2i = d02 > d13 ? 2:3
      , v2j = v2i==2? ( a01<90?3:1 ): ( a01<90?2:0 ) 
      , v2P = qcs[v2i]   // pt on the longer arm, either qcs[2] or qcs[3]
      , v2Q = qcs[v2j]   // pt on the short arm, could be any in qcs 
      , v2J = projPt( v2Q, axlonger)
      , v2distL= dist( xp, v2P )  
      , v2distJ= dist( xp, v2J )  
      , v2_da = (v2distJ/v2distL)* small_a / 2
      , v2_newP= anglePt( [v2P, xp, v2Q], a= v2_da, len=v2distJ+v2distL ) 
      , v2_ssd = avg_SSD( pts, [xp, v2_newP] ) 
      
      //======================================
      
      , newP = v1_ssd < v2_ssd ? v1_newP: v2_newP
      )
      echo( v1_ssd = v1_ssd )   
      echo( v2_ssd = v2_ssd )   
     [xp, newP]   
     
);


module 1_10_evaluation( )
{
   echom("1_10_evaluation()");

   echo( _red("Evaluate the new func 1_9_longAxis that picks smaller avg_SSD from the first and 2nd V" ));
   
   
   //--------------------------------------------
   // data-1: rand pts
   nseg=10; 
   m=5;
   noise=1;
   len=4; 
   pq= randPts(2);
   //pq = [O, 4*X];
   _pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noise, len=len);
   pts = h(_pts_pq, "pts");
   _pq = h(_pts_pq, "translated_pq");
   ssd0 = avg_SSD(pts,_pq);
   echo( pq=pq );
   echo( pts=pts );

   C= centroidPt(pts);
   //N = N([pts[0],C,pts[1]]);
   //MarkPts( [C,N], "CN" );
   //Mark90( [N,C,pts[0]]);
   
   qcs = quadCentroidPts( pts );
   //MarkPts(qcs,"PQRS");
   dist02= dist( qcs[0], qcs[2] );
   dist13= dist( qcs[1], qcs[3] );
   MarkPts( [qcs[0], qcs[2]], "02", Line=["r", dist02>dist13?0.05:0.02] );
   MarkPts( [qcs[1], qcs[3]], "13", Line=["r", dist02<dist13?0.05:0.02] );
   
   longaxis = 1_9_longAxis(pts);
   aSSD = avg_SSD( pts, longaxis);
   
   Red() MarkPts( longaxis, Label=false );
   
   
   //==============================
   axis_q12= 1_4_longAxis(pts);
   axis_q34= 1_6_longAxis_q34(pts);   
   ssd12= avg_SSD(pts,axis_q12);
   ssd34= avg_SSD(pts,axis_q34) ;
   
   Blue() Line( axis_q12 );
   Purple() Line( axis_q34 );
   
   echo( "n\t2N/L\tssd0\tssd(%)\tssd12(%)\tssd34(%)" );
   echo( _s("{_}\t{_}\t{_}\t{_}({_})\t{_}({_})\t{_}({_})"
           ,[ nseg*m, 2*noise/len
            , round(ssd0*100)/100 
            , round(aSSD*100)/100 , round((aSSD-ssd0)/ssd0*1000)/10
            , round(ssd12*100)/100 , round((ssd12-ssd0)/ssd0*1000)/10
            , round(ssd34*100)/100 , round((ssd34-ssd0)/ssd0*1000)/10
            ] ) );
   
   
   Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   Gold(0.2) Line( _pq, r=0.5 );
  
   
}
//1_10_evaluation( );

function 1_11_longAxis(pts, use_centroid_as_pivot)=
(
   //echo(_b("1_11_longAxis() function:") )   
   //echo(_red("Add a new arg 'use_centroidPt_as_pivot' to 1_9_longAxis() to allow testing if centroidPt(pts) is better than xp") )
   //echo( use_centroid_as_pivot = use_centroid_as_pivot )
   let( qcs = quadCentroidPts( pts )
      , ax02= [ qcs[0],qcs[2] ]
      , ax13= [ qcs[1],qcs[3] ]
      , d02 = dist( ax02 )
      , d13 = dist( ax13 )
      , xp = intsec( ax02, ax13 )
      , a01= angle( [qcs[0], xp,qcs[1]] )
      , small_a= a01<90? a01:(180-a01)
      , axlonger = d02>d13? ax02:ax13
      
      , Rf = use_centroid_as_pivot? centroidPt(pts): xp 
      
      /* 
        pts 0~3 below are centroidPt of pts in 4 quadrant 
        
        4 conditions: d02>d13 ?  a01<90 ?
        In each conditions, we use 2 V-shape parts: v1 and v2   
            
        d02 > d13:                  
                     0                
              1     /                      0  
               \   /                     .'
                \ /            1-.._   .'
            -----xp-----X     ------`xp----- X
                / \                .'  `'-3
               /   \             .'         
             2/     3           2      
             
             a01 < 90             a01 > 90              
             v1: 0-xp-1           v1: 0-xp-3
             v2: 2-xp-3           v2: 2-xp-1

        d02 < d13: 
                         
             1                       
              \     0                        
               \   /          1          0
                \ /           `'-.._   .'
            -----xp-----X     ------`xp----- X
                / \                .'  `'-_
              2/   \              2        `'-3 
                    3                 
             
             a01 < 90             a01 > 90              
             v1: 1-xp-0           v1: 1-xp-2
             v2: 3-xp-2           v2: 3-xp-0
              
        For a V-shape 3 pointers  A-xp-B:
        
        - A, B is long, short arm, respectively;
        - Since A is the long arm, it indicates the orientation
        - Calc the extence B pulls the orientation away from A
          to get a new A, call it v1_newP ( or v2_newP in case of v2)
        - Calc the avg_SSD of pts on [xp, v1_newP] and on [xp, v2_newP]
        - Pick either v1_newP or v2_newP depending on which has less avg_SSD
        
        ==> [ xp, newP ] is our long axis 
        */   
         
      //======================================
      // The first V (V1):
      // 
      //    if axi1=0: either 0-xp-1 or 0-xp-3
      //    if axi1=1: either 1-xp-0 or 1-xp-2 
      //
      , v1i = d02 > d13 ? 0:1
      , v1j = v1i==0? ( a01<90?1:3 ): ( a01<90?0:2 ) 
      , v1P = qcs[v1i]   // pt on the longer arm, either qcs[0] or qcs[1]
      , v1Q = qcs[v1j]   // pt on the short arm, could be any in qcs 
      , v1J = projPt( v1Q, axlonger)
      , v1distL= dist( xp, v1P )  
      , v1distJ= dist( xp, v1J )  
      , v1_da = (v1distJ/v1distL)* small_a / 2
      , v1_newP= anglePt( [v1P, Rf, v1Q], a= v1_da, len=v1distJ+v1distL ) 
      
      , v1_ssd = avg_SSD( pts, [Rf, v1_newP] ) 
    
      //======================================
      // The 2nd V (v2):
      // 
      //    if axi1=0: either 0-xp-1 or 0-xp-3
      //    if axi1=1: either 1-xp-0 or 1-xp-2 
      //
      , v2i = d02 > d13 ? 2:3
      , v2j = v2i==2? ( a01<90?3:1 ): ( a01<90?2:0 ) 
      , v2P = qcs[v2i]   // pt on the longer arm, either qcs[2] or qcs[3]
      , v2Q = qcs[v2j]   // pt on the short arm, could be any in qcs 
      , v2J = projPt( v2Q, axlonger)
      , v2distL= dist( xp, v2P )  
      , v2distJ= dist( xp, v2J )  
      , v2_da = (v2distJ/v2distL)* small_a / 2
      , v2_newP= anglePt( [v2P, Rf, v2Q], a= v2_da, len=v2distJ+v2distL ) 
      
      , v2_ssd = avg_SSD( pts, [Rf, v2_newP] ) 
      
      //======================================
      
      , newP = v1_ssd < v2_ssd ? v1_newP: v2_newP
      )
      //echo( newP = newP )
      //echo( v1_ssd = v1_ssd )   
      //echo( v2_ssd = v2_ssd )   
     [xp, newP]   
     
);


module 1_12_evaluation_use_centroid_as_pivot( )
{
   echom("1_12_evaluation_use_centroid_as_pivot()");

   echo( _red("Evaluate use_centroid_as_pivot=true/false in 1_11_longAxis()" ));
   echo( _red("Result: setting use_centroid_as_pivot=true doesn't seem to help much (1%)" ));
   
   
   //--------------------------------------------
   // data-1: rand pts
   nseg=10; 
   m=5;
   noise=1.5;
   len=4; 
   pq= randPts(2);
   //pq = [O, 4*X];
   _pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noise, len=len);
   pts = h(_pts_pq, "pts");
   _pq = h(_pts_pq, "translated_pq");
   ssd0 = avg_SSD(pts,_pq);
   echo( pq=pq );
   echo( translated_pq= _pq );
   echo( pts=pts );

   C= centroidPt(pts);
   //N = N([pts[0],C,pts[1]]);
   //MarkPts( [C,N], "CN" );
   //Mark90( [N,C,pts[0]]);
   
   qcs = quadCentroidPts( pts );
   //MarkPts(qcs,"PQRS");
   dist02= dist( qcs[0], qcs[2] );
   dist13= dist( qcs[1], qcs[3] );
   //MarkPts( [qcs[0], qcs[2]], "02", Line=["r", dist02>dist13?0.05:0.02] );
   //MarkPts( [qcs[1], qcs[3]], "13", Line=["r", dist02<dist13?0.05:0.02] );
   
   longaxis = 1_11_longAxis(pts);
   aSSD = avg_SSD( pts, longaxis);
   Red() MarkPts( longaxis, Label=false );
   
   //==============================
   longaxis_C = 1_11_longAxis(pts, use_centroid_as_pivot=true);
   aSSD_C = avg_SSD( pts, longaxis_C);
   Blue() MarkPts( longaxis_C, Label=false, Ball=["r",0.1, "color",["blue",0.3]] );
   
   //Blue() Line( axis_q12 );
   //Purple() Line( axis_q34 );
   
   echo( "n\t2N/L\tssd0\tssd(%)\tssd_useC(%)" );
   echo( _s("{_}\t{_}\t{_} \t {_}({_}) \t {_}({_})"
           ,[ nseg*m, 2*noise/len
            , round(ssd0*1000)/1000 
//            , _color( round(aSSD*1000)/1000, aSSD<aSSD_C?"green":"red") 
//               , round((aSSD-ssd0)/ssd0*1000)/10
//            , _color( round(aSSD_C*1000)/1000 , aSSD<aSSD_C?"red":"green")
//               , round((aSSD_C-ssd0)/ssd0*1000)/10
            , round(aSSD*1000)/1000 
               , round((aSSD-ssd0)/ssd0*1000)/10
            , round(aSSD_C*1000)/1000
               , round((aSSD_C-ssd0)/ssd0*1000)/10
            ] ) );
   
   
   Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   Gold(0.2) Line( _pq, r=0.5 );
   
   /* Results
  
   
   Observation:
   
      
   Suggestion:
   
   
   */      
   
}
//1_12_evaluation_use_centroid_as_pivot( );

module tester_longAxis_longAxisC( 
               len=4
             , nseg=10
             , m=5
             , noises= [.5,1,1.5,2]
             , nRepeat= 3
             , _data=[], _inoise=0, _irep=0
             , _i=0
             )
{
    //echom( _s("tester, _inoise={_}, __inoise={_}, _irep={_}, __irep={_}, _i={_}"
     //        , [_inoise, __inoise, _irep, __irep, _i]));
    /*
    
    , i  n ns/L ssd0 aSSD (%)    aSSD_C (%)    angle
    , 0 50 0.125 0.051 0.06(17.3%)   0.06 (17.3%)  a=6.72734
    , 1 50 0.125 0.084 0.092(9.6%)   0.09 (7.1%)   a=4.67607
    , 2 50 0.125 0.106 0.105(-0.9%)  0.103 (-2.3%) a=0.604173
    , 3 50 0.125 0.078 0.078(0.4%)   0.078 (0.6%)  a=0.615792
    , 4 50 0.125 0.063 0.062(-1.3%)  0.062 (-1.4%) a=1.04253

    , 5 50 0.250 0.38 0.378(-0.4%)   0.379 (-0.2%) a=0.321871
    , 6 50 0.250 0.368 0.376(2.3%)   0.378 (2.7%)  a=6.58955
    , 7 50 0.250 0.339 0.317(-6.7%)  0.317 (-6.5%) a=176.3  <=== something wrong
    , 8 50 0.250 0.282 0.213(-24.4%) 0.213 (-24.6%)a=2.88523
    , 9 50 0.250 0.332 0.323(-2.7%)  0.322 (-2.8%) a=2.54723

    , 10 50 0.375 0.722 0.681(-5.7%) 0.68 (-5.9%)  a=1.92003
    , 11 50 0.375 0.987 0.97(-1.8%)  0.971 (-1.7%) a=12.8501
    , 12 50 0.375 0.714 0.715(0.1%)  0.715 (0.1%)  a=3.92006
    , 13 50 0.375 0.808 0.775(-4.1%) 0.775 (-4.1%) a=12.132
    , 14 50 0.375 0.73 0.857(17.4%)  0.84 (15.1%)  a=13.7692

    , 15 50 0.500 0.996 1.132(13.7%) 1.18 (18.5%)  a=26.5899
    , 16 50 0.500 1.605 1.621(1%)    1.661 (3.5%)  a=1.9088
    , 17 50 0.500 1.167 1.155(-1.1%) 1.178 (0.9%)  a=8.77681
    , 18 50 0.500 1.267 1.261(-0.4%) 1.263 (-0.3%) a=4.57279
    , 19 50 0.500 1.307 1.277(-2.3%) 1.277 (-2.3%) a=2.09415
    ]"
    ECHO: "   Displaying the worst data: data #="14", aSSD(%)="0.857(17.4%)"
            , aSSD_C(%)="0.84(15.1%)", angle="13.77""
    ECHO: "   Drawing the worst data set..."
    ECHO: "   Calc sd of aSSD%, aSSD_C% and angle"
    ECHO: "     [ []
    ,  i  n ns/L  aSSD%(+-sd)  aSSD_C%(+-sd) angle(+-sd)
    ,  0 50 0.125 5.02(8.19)   4.26(8.16)    2.73(2.81)
    ,  5 50 0.250 -6.38(10.6) -6.28(10.79)  37.73(77.5) <=== something wrong
    , 10 50 0.375 1.18(9.33)   0.70(8.37)    8.92(5.55)
    , 15 50 0.500 2.18(6.55)   4.06(8.34)    8.79(10.33)
    
    
    Observation (18.8.16):
    
    -- Can't tell which of aSSD and aSSD_C is better;
    -- angle variations (sd) is usually less than 15. 
    
    Next:
    
      Calc aSSD_C and Use the method described in:
      https://stackoverflow.com/questions/12934213/how-to-find-out-geometric-median
      
      to approach a better answer, starting angle jump
      can set at +-15.  
    
    */ 
     
     
       function deci(v,pos=1000)= round(v*pos)/pos;
       function perc_d(v0=ssd0,v)= round((v-v0)/v0*1000)/10;
    if(_irep== 0) 
    {
       echo("");
       echo("---------------, _inoise= ", _inoise);
    }
  
    if ( (_i< len(noises)*nRepeat) )
    {
      log_b(_s("_i={_}",_i));
       pq= randPts(2);
   //pq = [O, 4*X];
   _pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noises[_inoise], len=len);
   pts = h(_pts_pq, "pts");
   _pq = h(_pts_pq, "translated_pq");
   ssd0 = avg_SSD(pts,_pq);
   
   C= centroidPt(pts);
   
   longaxis = 1_11_longAxis(pts);
   aSSD = avg_SSD( pts, longaxis);
   //==============================
   longaxis_C = 1_11_longAxis(pts, use_centroid_as_pivot=true);
   aSSD_C = avg_SSD( pts, longaxis_C);
   
   
   // calc the angle
   P = longaxis[1];
   dP= [ for(p=_pq) [p, dist(p,P)]];
   Q = sortArrs(dP, by=1)[0][0];
   I = intsec( longaxis, _pq );
   angle = angle([P,I,Q] );
   log_(["_i",_i, "longaxis", longaxis, "_pq", _pq
        , "intsec", intsec(longaxis,_pq)] );
   log_(["P",P, "Q",Q, "I",I, "angle", angle]);     
   //log_(["angle45", angle([X,O, X+Y]) ]);
   //log_(["angle45", angle([newz(X,0),O, newz(X+Y,0)])] );
   //angle = angle( [ longaxis[0]
                 //, intsec(longaxis,_pq)
                 //, _pq ] );
               //
          
       ssds = [ ssd0, aSSD, aSSD_C ];
       data = [ deci(aSSD), perc_d(v0=ssd0, v=aSSD)
              , deci(aSSD_C), perc_d(v0=ssd0, v=aSSD_C) ];
               
       __data = concat( _data
                     , [concat( [ nseg*m
                                , numstr(noises[_inoise]/len,2)
                                , numstr(ssd0,3) //deci(ssd0) 
                                ] 
                        , data )]
                     );
       
       __data= concat( _data
                     , [[ _i
                        , nseg*m
                        , numstr(noises[_inoise]/len,3)
                        , numstr(ssd0,3) //deci(ssd0) 
                        , deci(aSSD)
                        , perc_d(v0=ssd0, v=aSSD)
                        , deci(aSSD_C)
                        , perc_d(v0=ssd0, v=aSSD_C)
                        //, angle
                        //, angle_C
                        
                        , pts
                        , longaxis
                        , longaxis_C
                        , _pq
                        , angle
                        ]
                       ]   
                     );
       
       __inoise = _inoise+ (_irep==nRepeat-1?1:0); 
       __irep = // +1 except: _inoise is still running AND _irep running out
               _irep>=nRepeat-1? 0:(_irep+1);
       log_( _s("tester_longAxis_longAxisC, nRepeat={_}, _inoise={_}, __inoise={_}, _irep={_}, __irep={_}, _i={_}"
             , [nRepeat, _inoise, __inoise, _irep, __irep, _i]));
       //log_( ["pq",pq,"_pq",_pq] );
       //log_( ["C",C] );
       //log_( ["longaxis",longaxis] );
       //log_( ["longaxis_C",longaxis_C] );
       //log_( ["ssd0", ssd0] );
       //log_( ["aSSD", aSSD] );
       //log_( ["aSSD_C", aSSD_C] );
       //log_(["ssds", ssds]);
       //log_(["data", data]);
       //log_(["__data", __data]);
       log_e();       
       tester_longAxis_longAxisC( 
               len=len
             , nseg=nseg
             , m=m
             , noises= noises 
             , nRepeat= nRepeat
             , _data=   __data
             , _inoise= __inoise
             , _irep  = __irep
             , _i=_i+1
             );              
    }
    else
    {
       log_b("tester looping completed...");
       sorted_data = sortArrs( _data, by= 5); 
       worst_data = last(sorted_data);
       worst_i = worst_data[0];
       
       log_( arrprn(concat([[]]
               , [_s( "{_}\t{_}\t {_}\t {_}\t <u>{_}</u> ({_})\t <u>{_}</u>\t ({_})\t{_}",
                    ["i", "&nbsp;n", "ns/L", "ssd0", "aSSD", "%", "aSSD_C", "%", "angle"] )
                 ]   
               , [for(i= range(_data)) //d=_data)
                    _color( 
                            _s( "{_}\t{_}\t {_}\t {_}\t <u>{_}</u>({_}%)\t <u>{_}</u>\t ({_}%)\ta={_}"
                             , concat( get(_data[i], range(8)), [last(_data[i])] )
                             )
                          , i==worst_i?"red":"black"  
                          ) 
                 ]
               )
           , dep=1 ) );
       log_( ["Displaying the worst data: data #", _red(worst_data[0]) 
             , "aSSD(%)", str( worst_data[4], "(", _red(worst_data[5]), "%)") 
             , "aSSD_C(%)", str( worst_data[6], "(", _red(worst_data[7]), "%)")
             , "angle", numstr(last(worst_data),2) 
             ] );
             
       //log_( ["longaxis", worst_data[9], "longaxis_C", worst_data[10]
       //      , "_pq", worst_data[11]] );
       
         log_("Drawing the worst data set...");
         
         pts=worst_data[8];
         longaxis=worst_data[9];
         longaxis_C=worst_data[10];
         _pq=worst_data[11];
         
         Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
         Red() MarkPts( longaxis, Label=false );
         Blue() MarkPts( longaxis_C, Label=false, Ball=["r",0.1, "color",["blue",0.3]] );
         Gold(0.2) Line( _pq, r=0.5 );
         
         //log_( ["pts resulting in the worst data:", pts] );
        
       //
       // calc sd of aSSD%, aSSD_C% and angle
       //
       log_b("Calc sd of aSSD%, aSSD_C% and angle",1);
       // First, group data by noise levels:
       
       data2= subArrs( _data, size=nRepeat);
//       sds = [ for(g=data2) 
//                [ for(d=g)
//                   get(d, [0,1,2,5,7,-1]) // i, nseg*m, str(noise/L), aSSD%, aSSD_C%, angle
//                ]   
//             ];
       sds = [ for(i= range(data2)) 
                let( grp= data2[i] 
                   , avg_aSSD_percent = numstr(avg( [ for(d=grp) d[5] ] ),2) 
                   , avg_aSSD_C_percent = numstr(avg( [ for(d=grp) d[7] ] ),2) 
                   , avg_angle = numstr(avg( [ for(d=grp) last(d) ] ),2) 
                   , sd_aSSD_percent = numstr(sd( [ for(d=grp) d[5] ] ),2) 
                   , sd_aSSD_C_percent = numstr(sd( [ for(d=grp) d[7] ] ),2) 
                   , sd_angle = numstr(sd( [ for(d=grp) last(d) ] ),2) 
                   )
                //echo( grp)   
                [ _f( grp[0][0], "|w2ar")
                , grp[0][1], grp[0][2]
                , avg_aSSD_percent, sd_aSSD_percent
                , avg_aSSD_C_percent, sd_aSSD_C_percent
                , avg_angle, sd_angle 
                  // i, nseg*m, str(noise/L), aSSD%, aSSD_C%, angle
                ]   
             ];
       //log_( str("sds= ",sds), 1 );   
       //log_( ["lens ", [for(d=data2) len(d)]], 1 );      
      
       log_( arrprn(concat([[]]
               , [_s( "<u>{_}</u>\t<u>{_}</u>\t <u>{_}</u>\t <u>{_}(+-{_})</u> <u>{_}(+-{_})</u>\t<u>{_}(+-{_})</u>"
                    , ["i","&nbsp;n","ns/L", "aSSD%","sd", "aSSD_C%","sd", "angle","sd"] 
                    )
                 ]   
               , [for(sd=sds) 
                    _s( "{_}\t{_}\t {_}\t <u>{_}({_})</u>\t <u>{_}({_})</u>\t <u>{_}({_})</u>", sd )
                 ]
               )
           , dep=1 ), 1 );
       log_e(layer=1);
       
       
          
       log_e();
       
       
    }

     
}
//tester_longAxis_longAxisC();

module tester_longAxis_longAxisC_use_tablelist( 
               len=4
             , nseg=10
             , m=5
             , noises= [.5, 1, 1.5, 2]
             , nRepeat= 5
             , _data=[], _inoise=0, _irep=0
             , _i=0
             )
{
   
    //echom( _s("tester, _inoise={_}, __inoise={_}, _irep={_}, __irep={_}, _i={_}"
     //        , [_inoise, __inoise, _irep, __irep, _i]));
    /*
ECHO: "   The worst data: data #="10", worst data at i=10, aSSD(%)="1.26399(36.5%)", aSSD_C(%)="1.26937(37.1%)", angle="40.41""
ECHO: "   Result table:"
ECHO: "   ssd="Sum of Squared Dist. The smaller ssd, the better result it is.""
ECHO: "   ssd0="Avg SSD of the pts against source line. Unknown in real case.""
ECHO: "   aSSD="Avg SSD of the pts against the 1onger arm of quadCentroidPts""
ECHO: "   aSSD_C="Avg SSD of the pts against the 1onger arm of quadCentroidPts connected to overall centroidPt""
ECHO: "   %="Diff % of either aSSD or aSSD_C against ssd0""
ECHO: "   angle="Angle between the source line and the long arm of quadCentroidPts. Unknown in real case""
ECHO: "   
 i  n  ns/L  ssd0   aSSD   aSSD%   aSSD_C  aSSD_C%  angle 
 0 50 0.125  0.08    0.08   2.80      0.08    2.90    1.12
 1 50 0.125  0.09    0.08  -10.00     0.08   -8.40    4.28
 2 50 0.125  0.09    0.09   5.50      0.09    5.30    3.18
 3 50 0.125  0.09    0.09  -6.50      0.09   -6.80    3.61
 4 50 0.125  0.08    0.09  14.50      0.08    8.60    4.08

 5 50 0.250  0.33    0.32  -2.60      0.32   -1.60     5.7
 6 50 0.250  0.32    0.33   2.60      0.33    3.30    4.92
 7 50 0.250  0.29    0.29  -0.40      0.29   -0.60    8.17
 8 50 0.250   0.4    0.39  -2.80      0.39   -2.80    2.83
 9 50 0.250  0.25    0.24  -3.70      0.24   -3.80    7.84

10 50 0.375  0.93    1.26  36.50      1.27   37.10   40.41
11 50 0.375   0.6    0.62   3.50      0.62    2.90    5.49
12 50 0.375  0.66    0.73  10.50      0.72    9.00   10.62
13 50 0.375  0.76     0.7  -7.70       0.7   -8.70    3.11
14 50 0.375  0.78    0.69  -11.60      0.7  -10.70   11.16

15 50 0.500  1.17    1.03  -12.20     1.03  -12.20   12.21
16 50 0.500  1.35    1.34  -0.80      1.35   -0.70   43.29
17 50 0.500  1.71    1.88   9.70      1.88    9.60   62.11
18 50 0.500  1.66    1.76   6.30      1.77    6.40   22.96
19 50 0.500  1.49    1.56   4.20      1.57    5.10   12.46
"
ECHO: "   The worst data set..."
ECHO: "   Calc sd of aSSD%, aSSD_C% and angle"
ECHO: "   
 i  n  ns/L  ssd0   (+-0)   aSSD   (+-1)  aSSD%   (+-2)  aSSD_C   (+-3)  aSSD_C%  (+-4)  angle   (+-5)
 0 50 0.125  0.09  0.01     0.09  0.01     1.26  9.78     0.09   0.01     0.32   7.53     3.25  1.27  
 5 50 0.250  0.32  0.06     0.31  0.06    -1.38  2.53     0.31   0.06     -1.10  2.74     5.89  2.2   
10 50 0.375  0.75  0.12     0.8   0.26     6.24  19.07     0.8   0.26     5.92   19.24   14.16  15.06 
15 50 0.500  1.48  0.22     1.51  0.34     1.44  8.52     1.52   0.34     1.64   8.59    30.61  21.67   
    
    Observation (18.8.16):
    
    -- Can't tell which of aSSD and aSSD_C is better;
    -- angle variations (sd) is usually less than 15. 
    -- ssd is roughly proportational to ns/L --- larger ns/L, more like a circle a shape is.  
    
    Next:
    
      Calc aSSD_C and Use the method described in:
      https://stackoverflow.com/questions/12934213/how-to-find-out-geometric-median
      
      to approach a better answer, starting angle jump
      can set at +-15.  
    
    */ 
     
     
       function deci(v,pos=1000)= round(v*pos)/pos;
       function perc_d(v0=ssd0,v)= round((v-v0)/v0*1000)/10;
    if(_irep== 0) 
    {
       echo("");
       echo("---------------, _inoise= ", _inoise);
    }
  
    if ( (_i< len(noises)*nRepeat) )
    {
      log_b(_s("_i={_}",_i));
       pq= randPts(2);
   //pq = [O, 4*X];
   _pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noises[_inoise], len=len);
   pts = h(_pts_pq, "pts");
   _pq = h(_pts_pq, "translated_pq");
   ssd0 = avg_SSD(pts,_pq);
   
   C= centroidPt(pts);
   
   longaxis = 1_11_longAxis(pts);
   aSSD = avg_SSD( pts, longaxis);
   //==============================
   longaxis_C = 1_11_longAxis(pts, use_centroid_as_pivot=true);
   aSSD_C = avg_SSD( pts, longaxis_C);
   
   
   // calc the angle
   P = longaxis[1];
   dP= [ for(p=_pq) [p, dist(p,P)]];
   Q = sortArrs(dP, by=1)[0][0];
   I = intsec( longaxis, _pq );
   _angle = angle([P,I,Q] );
   angle= _angle>90? (180-_angle):_angle;
   //log_(["_i",_i, "longaxis", longaxis, "_pq", _pq
   //     , "intsec", intsec(longaxis,_pq)] );
   //log_(["P",P, "Q",Q, "I",I, "angle", angle]);     
   //log_(["angle45", angle([X,O, X+Y]) ]);
   //log_(["angle45", angle([newz(X,0),O, newz(X+Y,0)])] );
   //angle = angle( [ longaxis[0]
                 //, intsec(longaxis,_pq)
                 //, _pq ] );
               //
          
       ssds = [ ssd0, aSSD, aSSD_C ];
       data = [ deci(aSSD), perc_d(v0=ssd0, v=aSSD)
              , deci(aSSD_C), perc_d(v0=ssd0, v=aSSD_C) ];
               
       __data = concat( _data
                     , [concat( [ nseg*m
                                , numstr(noises[_inoise]/len,2)
                                , numstr(ssd0,3) //deci(ssd0) 
                                ] 
                        , data )]
                     );
       
       __data= concat( _data
                     , [[ _i
                        , nseg*m
                        //, numstr(noises[_inoise]/len,3)
                        //, numstr(ssd0,3) //deci(ssd0) 
                        , noises[_inoise]/len
                        , ssd0 //deci(ssd0) 
                        , aSSD
                        , perc_d(v0=ssd0, v=aSSD)
                        , aSSD_C
                        , perc_d(v0=ssd0, v=aSSD_C)
                        //, angle
                        //, angle_C
                        
                        , pts
                        , longaxis
                        , longaxis_C
                        , _pq
                        , angle
                        ]
                       ]   
                     );
       
       __inoise = _inoise+ (_irep==nRepeat-1?1:0); 
       __irep = // +1 except: _inoise is still running AND _irep running out
               _irep>=nRepeat-1? 0:(_irep+1);
       //log_( _s("tester_longAxis_longAxisC, nRepeat={_}, _inoise={_}, __inoise={_}, _irep={_}, __irep={_}, _i={_}"
       //      , [nRepeat, _inoise, __inoise, _irep, __irep, _i]));
       //log_( ["pq",pq,"_pq",_pq] );
       //log_( ["C",C] );
       //log_( ["longaxis",longaxis] );
       //log_( ["longaxis_C",longaxis_C] );
       //log_( ["ssd0", ssd0] );
       //log_( ["aSSD", aSSD] );
       //log_( ["aSSD_C", aSSD_C] );
       //log_(["ssds", ssds]);
       //log_(["data", data]);
       //log_(["__data", __data]);
       log_e();       
       tester_longAxis_longAxisC_use_tablelist( 
               len=len
             , nseg=nseg
             , m=m
             , noises= noises 
             , nRepeat= nRepeat
             , _data=   __data
             , _inoise= __inoise
             , _irep  = __irep
             , _i=_i+1
             );              
    }
    else
    {
       log_b("tester looping completed...");
       sorted_data = sortArrs( _data, by= 5); 
       worst_data = last(sorted_data);
       worst_i = worst_data[0];
       
       log_( ["The worst data: data #", _red(worst_data[0]) 
             , "worst data at i", worst_i
             , "aSSD(%)", str( worst_data[4], "(", _red(worst_data[5]), "%)") 
             , "aSSD_C(%)", str( worst_data[6], "(", _red(worst_data[7]), "%)")
             , "angle", numstr(last(worst_data),2) 
             ] );
          
       prn_data= [for(i= range(_data)) //d=_data)
                   concat( get(_data[i], range(8)), [last(_data[i])] )
                 ];
       log_( _b("Result table:") );
       log_( ["ssd", "Sum of Squared Dist. The smaller ssd, the better result it is."] );
       log_( ["ssd0", "Avg SSD of the pts against source line. Unknown in real case."]);
       log_( ["aSSD", "Avg SSD of the pts against the 1onger arm of quadCentroidPts"]);
       log_( ["aSSD_C", "Avg SSD of the pts against the 1onger arm of quadCentroidPts connected to overall centroidPt"]);
       log_( ["%","Diff % of either aSSD or aSSD_C against ssd0"] );
       log_( ["angle", "Angle between the source line and the long arm of quadCentroidPts. Unknown in real case"]);
       log_( _table( header=["i","|w2ac","n","|w2ac","ns/L","|w5d3"
                      , "ssd0", "|w6d2ac"
                      , "aSSD",  "|w6d2", "aSSD%","|w7acd2"
                      , "aSSD_C","|w8d2", "aSSD_C%","|w7ard2"
                      , "angle",  "|w7d2"//, "(+-sd3)","|w7ald2"  
                      ]
                   , data= prn_data 
                   , opt=["sep_is",[m:m:nseg*m] 
                         , "rowstyles", [[worst_i], "color:red"]
                         ]
                   )
           );        
                   
       //log_( ["longaxis", worst_data[9], "longaxis_C", worst_data[10]
       //      , "_pq", worst_data[11]] );
       
         log_("The worst data set...");
         
         pts=worst_data[8];
         longaxis=worst_data[9];
         longaxis_C=worst_data[10];
         _pq=worst_data[11];
         
         Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
         Red() MarkPts( longaxis, Label=false );
         Blue() MarkPts( longaxis_C, Label=false, Ball=["r",0.1, "color",["blue",0.3]] );
         Gold(0.2) Line( _pq, r=0.5 );
         
         //log_( ["pts resulting in the worst data:", pts] );
        
       //
       // calc sd of aSSD%, aSSD_C% and angle
       //
       log_b("Calc sd of aSSD%, aSSD_C% and angle",1);
       // First, group data by noise levels:
       
       data2= subArrs( _data, size=nRepeat);
//       sds = [ for(g=data2) 
//                [ for(d=g)
//                   get(d, [0,1,2,5,7,-1]) // i, nseg*m, str(noise/L), aSSD%, aSSD_C%, angle
//                ]   
//             ];
       sds = [ for(i= range(data2)) 
                let( grp= data2[i] 
                   
                   //, avg_aSSD_percent = numstr(avg( [ for(d=grp) d[5] ] ),2) 
                   //, sd_aSSD_percent = numstr(sd( [ for(d=grp) d[5] ] ),2) 
                   
                   , avg_aSSD_percent = _f(avg( [ for(d=grp) d[5] ] ),"|w6d2arp ") 
                   , sd_aSSD_percent = _f(sd( [ for(d=grp) d[5] ] ),"|d2w6ar") 
                   
                   , avg_aSSD_C_percent = _f(avg( [ for(d=grp) d[7] ] ),"|d2w6ar") 
                   , sd_aSSD_C_percent = _f(sd( [ for(d=grp) d[7] ] ),"|d2w6ar") 
                   
                   , avg_angle = _f(avg( [ for(d=grp) last(d) ] ),"|d2w6ar") 
                   , sd_angle = _f(sd( [ for(d=grp) last(d) ] ),"|d2w6ar") 
                   )
                //echo( grp)   
                [ _f( grp[0][0], "|w2ar")
                , grp[0][1] // n
                , grp[0][2] // noise/L
                //, grp[0][3] // noise/L
                , avg_aSSD_percent, sd_aSSD_percent
                , avg_aSSD_C_percent, sd_aSSD_C_percent
                , avg_angle, sd_angle 
                  // i, nseg*m, str(noise/L), aSSD%, aSSD_C%, angle
                ]   
             ];
       sds2 = [ for(i= range(data2)) 
                let( grp= data2[i] 
                   
                   
                   , avg_ssd0_percent = numstr(avg( [ for(d=grp) d[3] ] ),2) 
                   , sd_ssd0_percent = numstr(sd( [ for(d=grp) d[3] ] ),2) 
                   
                   , avg_aSSD = avg( [ for(d=grp) d[4] ] )
                   , sd_aSSD = sd( [ for(d=grp) d[4] ] )
                   
                   , avg_aSSD_percent = avg( [ for(d=grp) d[5] ] ) 
                   , sd_aSSD_percent = sd( [ for(d=grp) d[5] ] )
                   
                   , avg_aSSD_C = avg( [ for(d=grp) d[6] ] )
                   , sd_aSSD_C = sd( [ for(d=grp) d[6] ] ) 
                   
                   , avg_aSSD_C_percent = avg( [ for(d=grp) d[7] ] ) 
                   , sd_aSSD_C_percent = sd( [ for(d=grp) d[7] ] ) 
                   
                   , avg_angle = avg( [ for(d=grp) last(d) ] ) 
                   , sd_angle = sd( [ for(d=grp) last(d) ] ) 
                   )
                //echo( grp)   
                [ grp[0][0]
                , grp[0][1] // n
                , grp[0][2] // noise/L
                
                , avg_ssd0_percent, sd_ssd0_percent
                , avg_aSSD, sd_aSSD
                , avg_aSSD_percent, sd_aSSD_percent
                , avg_aSSD_C, sd_aSSD_C
                , avg_aSSD_C_percent, sd_aSSD_C_percent
                , avg_angle, sd_angle 
                  // i, nseg*m, str(noise/L), aSSD%, aSSD_C%, angle
                ]   
             ];
             
       //log_( str("sds= ",sds), 1 );  
       //log_( ["lens ", [for(d=data2) len(d)]], 1 );      
       log_( _table( 
               header=["i","|w2ac","n","|w2ac","ns/L","|w5d3"
                      , "ssd0",  "|w6d2ac", "(+-0)","|w6ald2"
                      , "aSSD",  "|w7d2ac", "(+-1)","|w6ald2"
                      , "aSSD%",  "|w7d2ac", "(+-2)","|w6ald2"
                      , "aSSD_C","|w8d2ac", "(+-3)","|w6ald2"
                      , "aSSD_C%","|w8d2ac", "(+-4)","|w6ald2"
                      , "angle",  "|w7d2ac", "(+-5)","|w6ald2"  
                      ]
               ,data=sds2       
                    )
            );        
       log_e(layer=1);
          
       log_e();
       
    }     
}
//tester_longAxis_longAxisC_use_tablelist();

// Geometrical Median. 
   // https://stackoverflow.com/questions/12934213/how-to-find-out-geometric-median
   // Note: a gmedian tends to be close to where pts cluster
  

function 2_1_longAxis_chk_ang_adj
                     ( pts
                     , dAs= [0, 10,20,30,45]  // search better fit [0, 10,20,30,45] 
                     , nMaxStep=0 // max # of try
                     , accept_ssd = 2 
                     , _c // centroidPt
                     , _longax
                     , _ssd // avg sum of squard differnce 
                      , _i=-1)= 
(  
   //_i==-1?
     //echo(log_b("2_1_longAxis_chk_ang_adj()"))
     let( _C= centroidPt(pts)
        , qcs =quadCentroidPts(pts)
        
        // Find the long ax --- is it 0~2 or 1~3 where 0 1 2 3 are qudrants 
        , ax02 = [qcs[0],qcs[2]]
        , ax13 = [qcs[1],qcs[3]]
        ,_longax= dist(ax02)> dist(ax13)? ax02:ax13
        
        // Find the end pts after rotational adjustment of some angle
        , P = _longax[0]
        , N = N( [pts[0], _C, pts[1] ] ) // this decides angle direction ( + or -) 
        , as = concat( -1* reverse(dAs), slice(dAs,1) )
        , endpts = [ for(a= as)
                        anglePt( pqr=[P,_C, pts[0]], a=a, Rf=N ) 
                   ]         
        , lines = [ for(p=endpts) [_C,p] ] 
        , assds = [ for(line = lines) 
                     avg_SSD(pts, line) ] 
        
        //, lineQ= [_C, Q]
        //, P= anglePt( [Q,O,X], a=dA )
        //, R= anglePt( [Q,O,X], a=-dA )
        //
        //, ssd_pa = avg_SSD( pts, [P,_C] )
        //, ssd_0 = avg_SSD( pts, [Q,_C] )
        //, ssd_na = avg_SSD( pts, [R,_C] )
        )
     //echo( log_(["dAs", dAs]) )
     //echo( log_(["splitas", slice(dAs,1)]) )
     //echo( log_(["as",as]) )
     //echo( log_( ["endpts", endpts]) )
     //echo( log_(["lines",lines]))
     //echo( log_(["assds",assds]))
     //echo(log_( ["ssd_0", ssd_0, "ssd_pa", ssd_pa, "ssd_na", ssd_na]) )   
     //echo( log_(["pts", pts] ))
     //echo( log_(["sum_pts",sum(pts)] ))
     //echo( log_(["sum_pts/n",sum(pts)/len(pts)] ))
     //echo( log_(["_pt",_pt] ))
     //echo( log_(["_sod",_sod] ))
     //echo( log_e() )
     ["ines", lines, "angles", as, "assds", assds]
);     
   


module 2_1_plot_ssd_vs_angle(              
             //Data src
               len=4
             , nseg=10
             , m=5
             , noises= [.5, 1, 2, 3, 4 ]
             //, nRepeat= 5
             //Angle to check  
             , dAs = range(0,22.5, 91) //range(0,5,66)
             //===============
             //, _data=[], _inoise=0, _irep=0
             //, _i=0
             )
{
   
    echom( "2_1_plot_ssd_vs_angle" );
    
   module run(noise, ni, isdoc, ismark)
   { 
   
    if(isdoc)
    {
     log_(_red(_b("Find the longer quadLine, adj it's angle to get a smaller assd")));
    log_(_red("This module find how far the longarm rotates to get the minimum Sum of Squared Dist"  ));
    log_(_red(" --- Given a list of angles, calc new ssd after the long quadline rotates by that angle"));
    log_(_red( _s(" --- Angles given: {_}", str(as) )));
    log_(_red( " --- Resulting ssds(Y) are plotted against angles(X) at different Noise/len ratios"));
    log_(_red( " --- The angles are shrunken to 1/20 for easy display"));
    
    
    log_(_blue(_b("Observation")));
    log_(_blue("- The ssd-vs-angle follows a <b>reversed cosine wave</b> -- " ));
    log_(_blue("- Theoretically one of the two lowest ssds occurrs in <b>ang=0</b>"));
    log_(_blue("- There will be 2 valleys( local mininums), both give correct minimum ssd"));
    log_(_blue("- but we would prefer getting the one with smallest angle."));
    log_(_blue("- We only need to set the first step with a small angle like 45"));
    }

    /*
    ECHO: "   noise/len=0.12, min assd=0.087 at angle -180"
    ECHO: "   noise/len=0.12, min assd=0.087 at angle -180"
    ECHO: "   noise/len=0.25, min assd=0.363 at angle -20"
    ECHO: "   noise/len=0.25, min assd=0.379 at angle -180"
    ECHO: "   noise/len= 0.5, min assd=1.246 at angle -180"
    ECHO: "   noise/len= 0.5, min assd=1.226 at angle -160"
    ECHO: "   noise/len=0.75, min assd=3.238 at angle -180"
    ECHO: "   noise/len=0.75, min assd=3.579 at angle -120"
    ECHO: "   noise/len=   1, min assd=5.214 at angle -40"
    ECHO: "   noise/len=   1, min assd=5.672 at angle -140"
    */

       _pq = randPts(2,z=0);
       src= pts2( _pq, nseg=nseg, m=m, noise=noise, len=len);
       //["pts", pts, "translated_pq",translated_pq ] 
       pts = h(src, "pts");
       pq = h(src, "translated_pq");
       //echo(src_assd= avg_SSD( pts, pq), ismark=ismark);
       //Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
       //Gold(0.2) Line( pq, r=0.5 );
   
       _data= 2_1_longAxis_chk_ang_adj(pts=pts, dAs = dAs);
       //==> [ "Lines",lines, "angles",as, "assds",assds]
       as = h(_data, "angles");
       assds = h(_data, "assds");
       
       tobesorted = [ for(ii=range(as)) [assds[ii], ii, as[ii] ] ];
       sorted = sortArrs( tobesorted, by=0);
       
       //log_( ["is0(sorted[0][0]-sorted[1][0])", is0(sorted[0][0]-sorted[1][0])]  );
       mini = is0(sorted[0][0]-sorted[1][0])
               ? //echo(log_( ["sorted_0_1", _color( [sorted[0],sorted[1]], "black")
                 //           , "i0", sorted[0][1],"i1", sorted[1][1]
                 //           , "absa0", abs(sorted[0][2]),"absa1", abs(sorted[1][2])
                 //           ] ))
                 //min ( [abs(sorted[0][2]),abs(sorted[1][2])])
                  sorted[ abs(sorted[0][2])>abs(sorted[1][2])?1:0][1] 
                 
               : sorted[0][1];
       mina = as[mini];
       minassd= assds[mini];
       lowestPt= [mina, minassd,0];


//       mina = is0(sorted[0][0]-sorted[1][0])
//               ? //echo(log_( ["sorted_0_1", _color( [sorted[0],sorted[1]], "black"), "i0", sorted[0][1],"i1", sorted[1][1]] ))
//       
//                 min ( [abs(sorted[0][2]),abs(sorted[1][2])])
//               : sorted[0][2];
//       mini = [for(j=[0:1]) if(sorted[j][2]==mina) sorted[j][1]][0]; 
//      minassd= assds[mini];
      
      
       //log_( ["len_tobesorted", len(tobesorted), "len_sorted", len(sorted)] );
       //log_( ["tobesorted", _green(tobesorted)] );
       //log_( ["sorted_0_1", _color( [sorted[0],sorted[1]], ni==0?"olive":COLORS[ni])] );
       //log_( ["sorted", _color(sorted, ni==0?"olive":COLORS[ni])] );
       //log_( ["mini", mini] );
       log_(_mono( _color(_s( "noise/len={_}, mini={_}, min_assd={_} at angle {_} (lowestPt={_})"
                      , [ _f( noise/len, "|w4", pad="&nbsp;")
                        , _f( mini, "|w4")
                        , _f( minassd, "|w5d3", pad="&nbsp;")
                        , _f( mina,    "|5d0", pad="&nbsp;")
                        , lowestPt
                        ] )
                  , ni==0?"olive":COLORS[ni])
            )      
          );
       
        
       
       //echo( log_(["as", as]) );
       //echo(log_(["assds", assds]) );
   
       ssd_by_a = [ for(i=range(as))[ as[i]/20, assds[i], 0] ];
       color(COLORS[ni]) 
       {
         //log_( ["lowestPt", lowestPt] );
         MarkPts(pts=ssd_by_a, Label=false); 
         MarkPt( pt= newx(lowestPt, lowestPt.x/20), Ball=0.15 );
         
         //log_( ["[mina, minassd,0]",[mina, minassd,0]] );
         //MarkPt( [mina, minassd,0], Ball=0.15 );
         if(ismark) MarkPt( last(ssd_by_a), str("Noise/L = ", noise/len) );
       }  
         
   }
    
   for(i=range(noises))
    for(j=range(2)) 
      run(noises[i],i, isdoc=i==0&&j==0, ismark=j==0);
    
   log_e();        
   
}
//2_1_plot_ssd_vs_angle();


//========================================================
function 2_2_longAxis_by_angle_adj( pts
                , prec= 3 // minimum angle jump
                , nMaxSteps = 20 // <== just for safeguard
                , da= 45  // Intv for each search. Given line [I,B] where I 
                          // is the intsec of two quadlines, make [I,A] and
                          // [I,C] where angle([B,I,A])= -da and angle([B,I,C])=da.
                          // Calc the aSSD (avg sum of squared dist) on IA,IB,IC.   
                          // Find one that has the smallest aSSD. If the IB (the original
                          // one) has the smallest, half the da and repeat. If the picked
                          // line is IC, then pick IC and repeat with +da. If IA, 
                          // pick IA and repeat with -da 
                          // Search stops when da < prec or nSteps=nMaxSteps 
                , _longax 
                , _longarm 
                //, _A
                //, _C
                , _starting_aSSD
                , _aSSD
                , _dir // -1|0|1 for -da, da/2, and da, respectively
                       // _dir=0 means need to check both ends with +-da/2
                       // _dir=-1 means, 
                       /*
                          _dir=-1: Use A as next longarm and check only -da
                                  
                                  C  
                                 /
                                B                               
                               / 
                              A

                          _dir=0: Keep B as next longarm, check +-da/2
                          
                              A.       C
                                `.   .`
                                  `B'

                          _dir=1: Use C as next longarm and check only +da
                          
                              A.
                                `B.
                                   `C

                       */
                        
                , _i=-1)=
(  // Geometrical Median. 
   // https://stackoverflow.com/questions/12934213/how-to-find-out-geometric-median
   // Note: a gmedian tends to be close to where pts cluster
   let(lv=1)
   _i==-1?
     echo(log_b(["2_2_longAxis_by_angle_adj() init: _i",_i]))
     let( Cent= centroidPt(pts) 
        , _pts= [for(p=pts)p-Cent] 
        , qcs = quadCentroidPts(_pts)
        , ax02= [qcs[0], qcs[2]]   
        , ax13= [qcs[1], qcs[3]]
        , _longax= dist(ax02)>dist(ax13)? ax02:ax13
        , B = _longax[0]
        , I = intsec(ax02,ax13)
        , N = N( [B,I,_pts[0]] ) 
        , _starting_aSSD= avg_SSD(_pts, _longax)  
        )
     //echo( log_(["pts", pts] ))
     echo( log_(["Cent", Cent] ))
     echo( log_(["I", I, "B",B] ))
     //echo( log_(["sum_pts",sum(pts)] ))
     //echo( log_(["sum_pts/n",sum(pts)/len(pts)] ))
     //echo( log_(["_pts",_pts] ))
     echo( log_(["_starting_aSSD",_starting_aSSD] ))
     echo( log_e() )
     2_2_longAxis_by_angle_adj( 
                  pts=_pts
                , prec= prec 
                , nMaxSteps = nMaxSteps 
                , da= da 
                , _ax02= ax02
                , _ax13= ax13
                , _longax = _longax
                , _longarm = [I,B] 
                , _starting_aSSD=_starting_aSSD
                , _aSSD=_starting_aSSD
                , _dir=0
                , _i=_i+1)   
   
  : !( _i>nMaxSteps || abs(da)< prec/2 )? 
     
    let( B= _longarm[1]
       , I= _longarm[0]
       , A= _dir==1? undef:anglePt( [B,I,Y], a=-da, Rf=Z ) // _dir=1, forward, don't need A 
       , C= _dir==-1? undef:anglePt( [B,I,Y], a=da, Rf=Z ) // _dir=-1, backward,don't need C 
       , aSSD_A= _dir==1? _aSSD*2  // _dir=1, forward, don't need A, *2 to make it larger
                        : avg_SSD( pts, [I,A])
       , aSSD_B= _aSSD 
       , aSSD_C= _dir==-1? _aSSD*2  // _dir=-1, backward, don't need C, *2 to make it larger
                         : avg_SSD( pts, [I,C]) 
       , sorted_aSSDs = sortArrs([ [ aSSD_A, -1] 
                          , [ aSSD_B, 0] 
                          , [ aSSD_C, 1] 
                          ], by=0)
       , nextdir= sorted_aSSDs[0][1] //[-1,0,1][ smallest ]                   
       , _aSSD= sorted_aSSDs[0][0]                   
       )
     echo( log_b( _s("_i={_}, nMaxSteps={_}, prec={_}, da={_}"
                 ,[_i,nMaxSteps,prec,da]),lv ))
     //echo( log_( ["pts", pts]))
     echo( log_( ["_longarm", _longarm]))
     echo( log_( ["IA", [I,A]]))
     echo( log_( ["IB", [I,B]]))
     echo( log_( ["IC", [I,C]]))
     echo( log_( ["aSSD_A", aSSD_A]))
     echo( log_( ["aSSD_B", aSSD_B]))
     echo( log_( ["aSSD_C", aSSD_C]))
     echo( log_( ["aSSDs", [[aSSD_A,-1],[aSSD_B,0],[aSSD_C,1]]]))
     echo( log_( ["sorted_aSSDs", sorted_aSSDs]))
     //echo( log_( ["i of lowest aSSDs:", aSSDs[0][1]] ))
     echo( log_( ["_dir", _dir]))
     echo( log_( ["next_dir", nextdir]))
     echo( log_( ["_aSSD", _red(_aSSD)]))
     echo( log_e("", lv))
     2_2_longAxis_by_angle_adj( 
                  pts=pts
                , prec= prec 
                , nMaxSteps = nMaxSteps  
                , da= nextdir==-1 ?-da // The one with -da has the smallest aSSD
                        :nextdir==0 ? da/2
                        : da 
                , _ax02= _ax02
                , _ax13= _ax13
                , _longax = _longax
                , _longarm = nextdir==-1?[I,A]
                             :nextdir==0 ?[I,B]:[I,C]
                , _starting_aSSD= _starting_aSSD
                , _aSSD= _aSSD
                , _dir = nextdir
                , _i=_i+1) 
                
   : //----------------- exiting ------------------     
     echo( log_b( _s("Existing 2_2_longAxis_by_angle_adj, _i={_}, prec={_}, da={_}"       
                 , [_i,prec,da] )))
     //echo( log_(["I",I]))
     //echo( log_(["_pts",_pts]))
     let( len_2ndArm= dist(_longax)-dist(_longarm)
        )
     echo( log_e())
     ["longarm",_longarm
     , "longax", [ _longarm[1], onlinePt(_longarm, len=dist(_longarm)- dist(_longax))] 
     , "longax2", [ onlinePt(_longarm, ratio=1.5)
                 , onlinePt(_longarm, len=-len_2ndArm*1.5)
                 ] 
     , "aSSD",_aSSD
     , "ax02", _ax02, "ax13", _ax13
     , "center", _longarm[0]
     ]
);

module 2_3_demo_longAxis_by_angle_adj( )
{
   echom("2_3_demo_longAxis_by_angle_adj()");
   log_b("2_3_demo_longAxis_by_angle_adj()");
   //echo( _red("Evaluate use_centroid_as_pivot=true/false in 1_11_longAxis()" ));
   //echo( _red("Result: setting use_centroid_as_pivot=true doesn't seem to help much (1%)" ));
    
   //--------------------------------------------
   // data-1: rand pts
   nseg=10; 
   m=5;
   noise=1.5;
   len=4; 
   pq= randPts(2);
   //pq = [O, 4*X];
   _pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noise, len=len);
   pts = h(_pts_pq, "pts");
   _pq = h(_pts_pq, "translated_pq");
   //log_(["pts", pts]);
   log_(["_pq", _pq]);
   
   data= 2_2_longAxis_by_angle_adj( pts=pts
                , prec= 3 // minimum angle jump
                , nMaxSteps = 20 // <== just for safeguard
                , da= 45 );
   log_(["data",data]);
      
   Blue() MarkPt( h(data, "center"), r=0.1);
   MarkPts( h(data, "longax"), r=0.03 );             
   MarkPts( h(data, "longax2") );  
   Red(0.2)
   {  Line( h(data, "ax02"),r=0.1);
      Line( h(data, "ax13"),r=0.1 );
   }           
   
   Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   //Gold(0.2) Line( _pq, r=0.5 );     
   
   log_e();
}
//2_3_demo_longAxis_by_angle_adj( );

//========================================================
//function 2_4_shape2d_info( 
//      pts
//    , prec= 3 // minimum angle jump
//    , nMaxSteps = 20 // <== just for safeguard
//    , da= 45  // Intv for each search. Given line [I,B] where I 
//              // is the intsec of two quadlines, make [I,A] and
//              // [I,C] where angle([B,I,A])= -da and angle([B,I,C])=da.
//              // Calc the aSSD (avg sum of squared dist) on IA,IB,IC.   
//              // Find one that has the smallest aSSD. If the IB (the original
//              // one) has the smallest, half the da and repeat. If the picked
//              // line is IC, then pick IC and repeat with +da. If IA, 
//              // pick IA and repeat with -da 
//              // Search stops when da < prec or nSteps=nMaxSteps 
//    , _longax 
//    , _longarm 
//    //, _A
//    //, _C
//    , _starting_aSSD
//    , _aSSD
//    , _dir // -1|0|1 for -da, da/2, and da, respectively
//           // _dir=0 means need to check both ends with +-da/2
//           // _dir=-1 means, 
//           /*
//              _dir=-1: Use A as next longarm and check only -da
//                                  
//                      C  
//                     /
//                    B                               
//                   / 
//                  A
//
//              _dir=0: Keep B as next longarm, check +-da/2
//                          
//                  A.       C
//                    `.   .`
//                      `B'
//
//              _dir=1: Use C as next longarm and check only +da
//                          
//                  A.
//                    `B.
//                       `C
//
//           */
//                        
//    , _i=-1)=
//(  
//   let(lv=1)
//   _i==-1?
//     echo(log_b(["2_4_shape2d_info() init: _i",_i], lv))
//     let( Cent= centroidPt(pts) 
//        , _pts= [for(p=pts)p-Cent] 
//        , qcs = quadCentroidPts(_pts)
//        , ax02= [qcs[0], qcs[2]]   
//        , ax13= [qcs[1], qcs[3]]
//        , _longax= dist(ax02)>dist(ax13)? ax02:ax13
//        , B = _longax[0]
//        , I = intsec(ax02,ax13)
//        , N = N( [B,I,_pts[0]] ) 
//        , _starting_aSSD= avg_SSD(_pts, _longax)  
//        )
//     //echo( log_(["pts", pts] ))
//     echo( log_(["Cent", Cent] , lv))
//     echo( log_(["I", I, "B",B], lv ))
//     //echo( log_(["sum_pts",sum(pts)] , lv))
//     //echo( log_(["sum_pts/n",sum(pts)/len(pts)] , lv))
//     //echo( log_(["_pts",_pts], lv ))
//     echo( log_(["_starting_aSSD",_starting_aSSD] , lv))
//     echo( log_e("", lv) )
//     2_4_shape2d_info( 
//                  pts=_pts
//                , prec= prec 
//                , nMaxSteps = nMaxSteps 
//                , da= da 
//                , _ax02= ax02
//                , _ax13= ax13
//                , _longax = _longax
//                , _longarm = [I,B] 
//                , _starting_aSSD=_starting_aSSD
//                , _aSSD=_starting_aSSD
//                , _dir=0
//                , _i=_i+1)   
//   
//  : !( _i>nMaxSteps || abs(da)< prec/2 )? 
//     
//    let( B= _longarm[1]
//       , I= _longarm[0]
//       , A= _dir==1? undef:anglePt( [B,I,Y], a=-da, Rf=Z ) // _dir=1, forward, don't need A 
//       , C= _dir==-1? undef:anglePt( [B,I,Y], a=da, Rf=Z ) // _dir=-1, backward,don't need C 
//       , aSSD_A= _dir==1? _aSSD*2  // _dir=1, forward, don't need A, *2 to make it larger
//                        : avg_SSD( pts, [I,A])
//       , aSSD_B= _aSSD 
//       , aSSD_C= _dir==-1? _aSSD*2  // _dir=-1, backward, don't need C, *2 to make it larger
//                         : avg_SSD( pts, [I,C]) 
//       , sorted_aSSDs = sortArrs([ [ aSSD_A, -1] 
//                          , [ aSSD_B, 0] 
//                          , [ aSSD_C, 1] 
//                          ], by=0)
//       , nextdir= sorted_aSSDs[0][1] //[-1,0,1][ smallest ]                   
//       , _aSSD= sorted_aSSDs[0][0]                   
//       )
//     echo( log_b( _s("_i={_}, nMaxSteps={_}, prec={_}, da={_}"
//                 ,[_i,nMaxSteps,prec,da]),lv ))
//     //echo( log_( ["pts", pts]))
//     echo( log_( ["_longarm", _longarm], lv))
//     echo( log_( ["IA", [I,A]], lv))
//     echo( log_( ["IB", [I,B]], lv))
//     echo( log_( ["IC", [I,C]], lv))
//     echo( log_( ["aSSD_A", aSSD_A], lv))
//     echo( log_( ["aSSD_B", aSSD_B], lv))
//     echo( log_( ["aSSD_C", aSSD_C], lv))
//     echo( log_( ["aSSDs", [[aSSD_A,-1],[aSSD_B,0],[aSSD_C,1]]], lv))
//     echo( log_( ["sorted_aSSDs", sorted_aSSDs], lv))
//     //echo( log_( ["i of lowest aSSDs:", aSSDs[0][1]] , lv))
//     echo( log_( ["_dir", _dir], lv))
//     echo( log_( ["next_dir", nextdir], lv))
//     echo( log_( ["_aSSD", _red(_aSSD)], lv))
//     echo( log_e("", lv))
//     2_4_shape2d_info( 
//                  pts=pts
//                , prec= prec 
//                , nMaxSteps = nMaxSteps  
//                , da= nextdir==-1 ?-da // The one with -da has the smallest aSSD
//                        :nextdir==0 ? da/2
//                        : da 
//                , _ax02= _ax02
//                , _ax13= _ax13
//                , _longax = _longax
//                , _longarm = nextdir==-1?[I,A]
//                             :nextdir==0 ?[I,B]:[I,C]
//                , _starting_aSSD= _starting_aSSD
//                , _aSSD= _aSSD
//                , _dir = nextdir
//                , _i=_i+1) 
//                
//   : //----------------- exiting ------------------ 
//       
//     echo( log_b( _s("Existing 2_4_shape2d_info, _i={_}, prec={_}, da={_}"       
//                 , [_i,prec,da] ), lv))
//     //echo( log_(["I",I]))
//     //echo( log_(["_pts",_pts]))
//     let( len_2ndArm= dist(_longax)-dist(_longarm)
//        , longaxis=[ _longarm[1], onlinePt(_longarm, len=dist(_longarm)- dist(_longax))] 
//        , longaxis2= [ onlinePt(_longarm, ratio=1.5)
//                 , onlinePt(_longarm, len=-len_2ndArm*1.5)
//                 ] 
//     , 
//        , half_width= sqrt(_aSSD,2)
//                      // Suppose there's a rectangler frame boxing all pts, 
//                      // and all pts are on the two frame sides parallel to
//                      // the long axis, then they will give a _aSSD the same
//                      // as calculated _aSSD. So we got the calc. _aSSD, reverse
//                      // it back to "avg distance" a pt is to the longax to get
//                      // the half_width
//        , shortaxis= [anglePt( [_longarm[1], _longarm[0], X], a=90, len= half_width )
//                   , anglePt( [_longarm[1], _longarm[0], X], a=-90, len= half_width )
//                   ]
//        , shortLongRatio= dist(shortaxis)/dist(longaxis2) 
//        //-------------
//        , longEndPtsInfo= endPtsInfoAlongLine(pts,longaxis)
//                         ///: [ [ i0, pt0, dist0], [i1, pt1, dist1] ]
//        , longAx_Js = [projPt(longEndPts[0],longaxis), projPt(longEndPts[1],longaxis)] 
//        
//        , sortByDist_along_shortAx = sortByDist( pts, longaxis)
//                         ///: [ [ i0, pt0, dist0], [i1, pt1, dist1], ... ]
//        
//        , shortEndPtsInfo= [ sortByDist_along_shortAx[0]
//                           , last(sortByDist_along_shortAx)
//                           ]
//                         ///: [ [ i0, pt0, dist0], [i1, pt1, dist1] ]
//        
//        , shortaxismax= [ projPt(shortaxisEndPts[0],shortaxis)   
//                        , projPt(shortaxisEndPts[1],shortaxis)
//                        ]  
//        , boxPts = [ longaxismax[0]+ 
//                   , shortaxismax[0]+sum(longaxismax)/2                                    
//        )
//     /*   
//     function SSD(pts, target)= // Sum of Squared Dist. Target could be pt, line or plane. 2018.8.12
//(
//  sum( [for(p=pts) pow( dist(p,target),2) ] ) 
//);
//
//function avg_SSD(pts, target)= // averaged Sum of Squared Dist. 2018.8.12
//(
//  sum( [for(p=pts) pow( dist(p,target),2) ] ) / len(pts) 
//);
//
//     */   
//        
//     echo( log_e("", lv))
//     ["longarm",_longarm
////     , "longaxis", [ _longarm[1], onlinePt(_longarm, len=dist(_longarm)- dist(_longax))] 
////     , "longaxis2", [ onlinePt(_longarm, ratio=1.5)
////                 , onlinePt(_longarm, len=-len_2ndArm*1.5)
////                 ] 
//     , "longaxis", longaxis
//     , "longaxis2", longaxis2
//     , "longaxismax", longaxismax
//     , "shortaxismax", shortaxismax
//     , "half_width", half_width            
//     , "shortaxis", shortaxis
//     , "shortlongratio", shortLongRatio            
//     , "aSSD",_aSSD
//     , "quadaxis02", _ax02
//     , "quadaxis13", _ax13
//     , "center", _longarm[0]
//     , "Note", "longaxis2= longaxis extended on each side by 50%; quadaxis02=[quadCentroidPts[0],quadCentroidPts[1]]; center=longarm[0]= intsec of quadaxes" 
//     ]
//);

function 2_4_shape2d_info( 
      pts
    , prec= 3 // minimum angle jump
    , nMaxSteps = 20 // <== just for safeguard
    , da= 45  // Intv for each search. Given line [I,B] where I 
              // is the intsec of two quadlines, make [I,A] and
              // [I,C] where angle([B,I,A])= -da and angle([B,I,C])=da.
              // Calc the aSSD (avg sum of squared dist) on IA,IB,IC.   
              // Find one that has the smallest aSSD. If the IB (the original
              // one) has the smallest, half the da and repeat. If the picked
              // line is IC, then pick IC and repeat with +da. If IA, 
              // pick IA and repeat with -da 
              // Search stops when da < prec or nSteps=nMaxSteps 
    , _longax 
    , _longarm
    //, _A
    //, _C
    , _starting_aSSD
    , _aSSD
    , _dir // -1|0|1 for -da, da/2, and da, respectively
           // _dir=0 means need to check both ends with +-da/2
           // _dir=-1 means, 
           /*
              _dir=-1: Use A as next longarm and check only -da
                                  
                      C  
                     /
                    B                               
                   / 
                  A

              _dir=0: Keep B as next longarm, check +-da/2
                          
                  A.       C
                    `.   .`
                      `B'

              _dir=1: Use C as next longarm and check only +da
                          
                  A.
                    `B.
                       `C

           */
                        
    , _i=-1)=
(  
   let(lv=1)
   _i==-1?
     echo(log_b(["2_4_shape2d_info() init: _i",_i], lv))
     let( Cent= centroidPt(pts) 
        , _pts= pts //[for(p=pts)p-Cent] 
        , qcs = quadCentroidPts(_pts, Rf=Cent)
        , ax02= [qcs[0], qcs[2]]   
        , ax13= [qcs[1], qcs[3]]
        , _longax= dist(ax02)>dist(ax13)
                    || ax13[0]==undef || ax13[1]==undef? ax02:ax13
        , B = _longax[0]
        , I = Cent //intsec(ax02,ax13)
        , N = N( [B,I,_pts[0]] ) 
        , _starting_aSSD= avg_SSD(_pts, _longax)  
        )
     //echo( log_(["pts", pts] ))
     echo( log_(["Cent", Cent] , lv))
     echo( log_(["qcs", qcs], lv ))
     echo( log_(["ax02", ax02], lv ))
     echo( log_(["ax13", ax13], lv ))
     echo( log_(["_longax", _longax], lv ))
     echo( log_(["I", I, "B",B], lv ))
     //echo( log_(["sum_pts",sum(pts)] , lv))
     //echo( log_(["sum_pts/n",sum(pts)/len(pts)] , lv))
     //echo( log_(["_pts",_pts], lv ))
     echo( log_(["_starting_aSSD",_starting_aSSD] , lv))
     echo( log_e("", lv) )
     2_4_shape2d_info( 
                  pts=_pts
                , prec= prec 
                , nMaxSteps = nMaxSteps 
                , da= da 
                , _ax02= ax02
                , _ax13= ax13
                , _longax = _longax
                , _longarm = [I,B] 
                , _starting_aSSD=_starting_aSSD
                , _aSSD=_starting_aSSD
                , _dir=0
                , _i=_i+1)   
   
  : !( _i>nMaxSteps || abs(da)< prec/2 )? 
     
    let( B= _longarm[1]
       , I= _longarm[0]
       , A= _dir==1? undef:anglePt( [B,I,Y], a=-da, Rf=Z ) // _dir=1, forward, don't need A 
       , C= _dir==-1? undef:anglePt( [B,I,Y], a=da, Rf=Z ) // _dir=-1, backward,don't need C 
       , aSSD_A= _dir==1? _aSSD*2  // _dir=1, forward, don't need A, *2 to make it larger
                        : avg_SSD( pts, [I,A])
       , aSSD_B= _aSSD 
       , aSSD_C= _dir==-1? _aSSD*2  // _dir=-1, backward, don't need C, *2 to make it larger
                         : avg_SSD( pts, [I,C]) 
       , sorted_aSSDs = sortArrs([ [ aSSD_A, -1] 
                          , [ aSSD_B, 0] 
                          , [ aSSD_C, 1] 
                          ], by=0)
       , nextdir= sorted_aSSDs[0][1] //[-1,0,1][ smallest ]                   
       , _aSSD= sorted_aSSDs[0][0]                   
       )
     echo( log_b( _s("_i={_}, nMaxSteps={_}, prec={_}, da={_}"
                 ,[_i,nMaxSteps,prec,da]),lv ))
     //echo( log_( ["pts", pts]))
     echo( log_( ["_longarm", _longarm], lv))
     echo( log_( ["IA", [I,A]], lv))
     echo( log_( ["IB", [I,B]], lv))
     echo( log_( ["IC", [I,C]], lv))
     echo( log_( ["aSSD_A", aSSD_A], lv))
     echo( log_( ["aSSD_B", aSSD_B], lv))
     echo( log_( ["aSSD_C", aSSD_C], lv))
     echo( log_( ["aSSDs", [[aSSD_A,-1],[aSSD_B,0],[aSSD_C,1]]], lv))
     echo( log_( ["sorted_aSSDs", sorted_aSSDs], lv))
     //echo( log_( ["i of lowest aSSDs:", aSSDs[0][1]] , lv))
     echo( log_( ["_dir", _dir], lv))
     echo( log_( ["next_dir", nextdir], lv))
     echo( log_( ["_aSSD", _red(_aSSD)], lv))
     echo( log_e("", lv))
     2_4_shape2d_info( 
                  pts=pts
                , prec= prec 
                , nMaxSteps = nMaxSteps  
                , da= nextdir==-1 ?-da // The one with -da has the smallest aSSD
                        :nextdir==0 ? da/2
                        : da 
                , _ax02= _ax02
                , _ax13= _ax13
                , _longax = _longax
                , _longarm = nextdir==-1?[I,A]
                             :nextdir==0 ?[I,B]:[I,C]
                , _starting_aSSD= _starting_aSSD
                , _aSSD= _aSSD
                , _dir = nextdir
                , _i=_i+1) 
                
   : //----------------- exiting ------------------ 
       
     echo( log_b( _s("Existing 2_4_shape2d_info, _i={_}, prec={_}, da={_}"       
                 , [_i,prec,da] ), lv))
     //echo( log_(["I",I]))
     //echo( log_(["_pts",_pts]))
     let( len_2ndArm= dist(_longax)-dist(_longarm)
        , longAx=[ _longarm[1], onlinePt(_longarm, len=dist(_longarm)- dist(_longax))] 
        , longAx2= [ onlinePt(_longarm, ratio=1.5)
                 , onlinePt(_longarm, len=-len_2ndArm*1.5)
                 ] 
     , 
        , half_width= sqrt(_aSSD,2)
                      // Suppose there's a rectangler frame boxing all pts, 
                      // and all pts are on the two frame sides parallel to
                      // the long Ax, then they will give a _aSSD the same
                      // as calculated _aSSD. So we got the calc. _aSSD, reverse
                      // it back to "avg distance" a pt is to the longax to get
                      // the half_width
        , shortAx= [anglePt( [_longarm[1], _longarm[0], X], a=90, len= half_width )
                   , anglePt( [_longarm[1], _longarm[0], X], a=-90, len= half_width )
                   ]
        , shortLongRatio= dist(shortAx)/dist(longAx) 
        //-------------
        , longEndPtsInfo= endPtsInfoAlongLine(pts,longAx)
                         ///: [ [ i0, pt0, dist0], [i1, pt1, dist1] ]
                         
        , longAx_Js = [ projPt(longEndPtsInfo[0][1],longAx)
                      , projPt(longEndPtsInfo[1][1],longAx)]
                      // 2 end pts on the longAx 
        , longAx_midPt = sum(longAx_Js)/2
        , longAx_bysect_line = [ longAx_midPt
                               , anglePt( [longAx_Js[0], longAx_midPt, X]
                                        , a=90, len= half_width 
                                        ) 
                               ]
        , sortByDist_against_longAx = sortByDist( pts
                                       , [longAx[0], longAx[1]
                                         , N([longAx[0], longAx_midPt,shortAx[0]])
                                         ] // Use plane 'cos we need negative dist
                                       , isAbs=0)
                         ///: [ [ i0, pt0, dist0], [i1, pt1, dist1], ... ]
        
        , shortAx_Js = [ projPt(sortByDist_against_longAx[0][1],longAx_bysect_line) 
                       , projPt(last(sortByDist_against_longAx)[1],longAx_bysect_line)
                       ]
                       
        , boxPts = [ longAx_Js[0]+ shortAx_Js[0]-longAx_midPt
                   , longAx_Js[0]+ shortAx_Js[1]-longAx_midPt
                   , longAx_Js[1]+ shortAx_Js[1]-longAx_midPt
                   , longAx_Js[1]+ shortAx_Js[0]-longAx_midPt
                   ]  
        , roundedness= dist(shortAx_Js)/dist(longAx_Js)                                           
        )
     /*   
     function SSD(pts, target)= // Sum of Squared Dist. Target could be pt, line or plane. 2018.8.12
(
  sum( [for(p=pts) pow( dist(p,target),2) ] ) 
);

function avg_SSD(pts, target)= // averaged Sum of Squared Dist. 2018.8.12
(
  sum( [for(p=pts) pow( dist(p,target),2) ] ) / len(pts) 
);

     */   
        
     echo( log_e("", lv))
     ["longarm",_longarm
//     , "longAx", [ _longarm[1], onlinePt(_longarm, len=dist(_longarm)- dist(_longax))] 
//     , "longAx2", [ onlinePt(_longarm, ratio=1.5)
//                 , onlinePt(_longarm, len=-len_2ndArm*1.5)
//                 ] 
     , "longAx", longAx
     , "longAx_Js", longAx_Js
     , "shortAx_Js", shortAx_Js
     , "boxPts", boxPts
     , "roundedness", roundedness
     //, "half_width", half_width            
     , "shortAx", shortAx
     , "shortlongratio", shortLongRatio            
     , "aSSD",_aSSD
     , "quadAx02", _ax02
     , "quadAx13", _ax13
     , "center", _longarm[0]
     , "Note", "longAx2= longAx extended on each side by 50%; quadAx02=[quadCentroidPts[0],quadCentroidPts[1]]; center=longarm[0]= intsec of quadaxes" 
     ]
);

module 2_5_demo_longAx_by_angle_adj( )
{
   echom("2_5_demo_longAx_by_angle_adj()");
   log_b("2_5_demo_longAx_by_angle_adj()");
   //echo( _red("Evaluate use_centroid_as_pivot=true/false in 1_11_longAx()" ));
   //echo( _red("Result: setting use_centroid_as_pivot=true doesn't seem to help much (1%)" ));
    
   //--------------------------------------------
   // data-1: rand pts
   nseg=10; 
   m=5;
   noise=1.5;
   len=4; 
   pq= randPts(2);
   //pq = [O, 4*X];
   _pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noise, len=len);
   pts = h(_pts_pq, "pts");
   _pq = h(_pts_pq, "translated_pq");
   //log_(["pts", pts]);
   log_(["_pq", _pq]);
   
   data= 2_4_shape2d_info( pts=pts
                , prec= 3 // minimum angle jump
                , nMaxSteps = 20 // <== just for safeguard
                , da= 45 );
   log_(["data", _fmth(data) ]);
      
   Blue() MarkPt( h(data, "center"), r=0.1);
   MarkPts( h(data, "longAx"), r=0.03 );             
  // MarkPts( h(data, "longAx2") );  
   //MarkPts( h(data, "shortAx") );  
   Red(0.2)
   {  Line( h(data, "quadAx02"),r=0.1);
      Line( h(data, "quadAx13"),r=0.1 );
   }           
   
   //Line( h(data, "shortAx_Js"), r=0.2 );
   Line( h(data, "boxPts"), closed=true );
   
   Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   //Gold(0.2) Line( _pq, r=0.5 );     
   //------------------------------------------------
   
   log_e();
}
//2_5_demo_longAx_by_angle_adj( );

module 2_6_demo_longAx_by_angle_adj_application( )
{
   echom("2_6_demo_longAx_by_angle_adj_application()");
   log_b("2_6_demo_longAx_by_angle_adj_application()");
   //echo( _red("Evaluate use_centroid_as_pivot=true/false in 1_11_longAx()" ));
   //echo( _red("Result: setting use_centroid_as_pivot=true doesn't seem to help much (1%)" ));
    
   //--------------------------------------------
   // data-1: rand pts
   //nseg=10; 
   //m=5;
   //noise=1.5;
   //len=4; 
   //pq= randPts(2);
   //pq = [O, 4*X];
   //_pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noise, len=len);
   //pts = h(_pts_pq, "pts");
   //_pq = h(_pts_pq, "translated_pq");
   
   //log_(["pts", pts]);
   //log_(["_pq", _pq]);
      
   function rand_rectangular_4pts(oundedness=0.3)=
   (
     let( rand_a = rands(0,359,1)[0] 
        ,_pts = [ 2*X-2*Y*roundedness
                ,-2*X-2*Y*roundedness
                ,-2*X+2*Y*roundedness
                , 2*X+2*Y*roundedness
                ]
        , C= centroidPt(_pts)
        , pts= rotPts(_pts, axis=[O,Z], a=rand_a)
        )
    echo(rand_a=rand_a)
    pts    
   
   );
   pts = rand_rectangular_4pts(roundedness=.2);
         
   //Red() Plane(pts);      
   //echo(pts=pts);
   
   data= 2_4_shape2d_info( pts=pts
                , prec= 3 // minimum angle jump
                , nMaxSteps = 20 // <== just for safeguard
                , da= 45 );
   log_(["data", _fmth(data) ]);
      
   Blue() MarkPt( h(data, "center"), r=0.1);
   MarkPts( h(data, "longAx"), r=0.03 );             
  // MarkPts( h(data, "longAx2") );  
   //MarkPts( h(data, "shortAx") );  
   Red(0.2)
   {  Line( h(data, "quadAx02"),r=0.1);
      Line( h(data, "quadAx13"),r=0.1 );
   }           
   
   //Line( h(data, "shortAx_Js"), r=0.2 );
   Line( h(data, "boxPts"), closed=true );
   
   Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   //Gold(0.2) Line( _pq, r=0.5 );     
   //------------------------------------------------
   
   log_e();
}
//2_6_demo_longAx_by_angle_adj_application( );

module 2_7_demo_longAx_by_angle_adj_application2( )
{
   echom("2_7_demo_longAx_by_angle_adj_application2()");
   log_b("2_7_demo_longAx_by_angle_adj_application2()");
   //echo( _red("Evaluate use_centroid_as_pivot=true/false in 1_11_longAx()" ));
   //echo( _red("Result: setting use_centroid_as_pivot=true doesn't seem to help much (1%)" ));
    
   //--------------------------------------------
   // data-1: rand pts
   //nseg=10; 
   //m=5;
   //noise=1.5;
   //len=4; 
   //pq= randPts(2);
   //pq = [O, 4*X];
   //_pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noise, len=len);
   //pts = h(_pts_pq, "pts");
   //_pq = h(_pts_pq, "translated_pq");
   
   //log_(["pts", pts]);
   //log_(["_pq", _pq]);
      
   function rand_rectangular_4pts(oundedness=0.5)=
   (
     let( rand_a = rands(0,359,1)[0] 
        ,_pts = [ 2*X-2*Y*roundedness
                ,-2*X-2*Y*roundedness
                ,-2*X+2*Y*roundedness
                , 1.8*X+2*Y*roundedness
                , 1.8*X+0.5*Y*roundedness
                , 2*X+0.5*Y*roundedness
                //, 2*X+2*Y*roundedness
                ]
        , C= centroidPt(_pts)
        , Rf= randPt(z=0)
        , pts= [for( P=rotPts(_pts, axis=[O,Z], a=rand_a)) P+Rf]
        )
    echo(rand_a=rand_a)
    pts    
   
   );
   pts = rand_rectangular_4pts(roundedness=.5);
         
   //Red() Plane(pts);      
   //echo(pts=pts);
   
   data= 2_4_shape2d_info( pts=pts
                , prec= 3 // minimum angle jump
                , nMaxSteps = 20 // <== just for safeguard
                , da= 45 
                );
   log_(["data", _fmth(data) ]);
      
   Blue() MarkPt( h(data, "center"), r=0.1);
   MarkPts( h(data, "longAx"), r=0.03 );             
  // MarkPts( h(data, "longAx2") );  
   //MarkPts( h(data, "shortAx") );  
   Red(0.2)
   {  Line( h(data, "quadAx02"),r=0.1);
      Line( h(data, "quadAx13"),r=0.1 );
   }           
   
   //Line( h(data, "shortAx_Js"), r=0.2 );
   Line( h(data, "boxPts"), closed=true );
   
   Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   //Gold(0.2) Line( _pq, r=0.5 );     
   //------------------------------------------------
   
   log_e();
}
//2_7_demo_longAx_by_angle_adj_application2( );


/*
2_8_shape2d_info()

  Given a set of random pts on a plane (the pts themselves are still in 
  3d like [x,y,z]), return a hash representing the shape info
  
  (Note: it works, somehow, on 3d pts, but not yet able to reach the optimal result)
  
  2-stage approach:  I. longer oppo_quadrants; II. Simulated annealing (SA) 
  
  First, group pts into 4 quadrants, 0~3. Find the centroidPt in each quadrant,
  from them we get 2 lines corresponding to 2 oppoquadrants: 0-2, 1-3. The longer
  line would be the rough direction of the long axis of pts
    
  I. Longer oppo_quadrants:
            
     C: centroidPt; a,b,c,d: pts grouped into quadrants       
            
      bb b  | 
        b   |  a
     b     b| a
   ---------C---------
         c  | d  d
         c c| dd  d
            |    d
            
     Find centroidPts in each quadrant, and connect one with that in
     the opposite quadrant to form 2 lines: 0-2 and 1-3
        
             
        '.  |   
          '.|.'
     -------C-----  
          .'| `.
            |   `.
                 
     In this case, line of 1-3 is the rough long axis of pts
     
  II. Simulated annealing (SA)
  
    Simulated annealing (SA) 
    https://en.wikipedia.org/wiki/Simulated_annealing
    as examplified in    
    https://stackoverflow.com/questions/12934213/how-to-find-out-geometric-median
    which is applied to gmedian() in scadx_geometry.scad
    
    SA is carried out through steps by rotating the longax (obtained in II)
    about C (centroid) by an angle in each step. 

    The result of each step is evaluated based on the aSSD (avg Sum of Standard dev) 
    in each step. The goal is to get the longax of minimal aSSD 
    
    Let [P,Q] = longax and C the centroidPt(pts) :  
  
               P           
                '.  |   
                  '.|.'
             -------C-----  
                  .'| `.
                    |   `Q
  
    Note that line pq doesn't necessarily pass through C, but always very close to it. 
    An alternative is to make C the interset between PQ and the shortax. But that
    will require extra steps when taking care of situations of empty quadrant (i.e.,
    a quadrant w/o pts). 
    
    First the longax is turned by and angle *da* (the starting angle) and by *-da*. 
     
               G(-da)
            P.  \ 
          F _ '. \   |
        (da) '-_'.\  |
                '-'\ |
                   '\|.'
              -------C-----  
                   .'| `.
                     |   ` 
    
    This gives 3 long arms: CG(=rotating -da), CP and CF(=rotating da), to which we
    assign dir=-1,0,1, respectively.
    
    They give us 3 aSSD's: aSSD(-1), aSSD(0), aSSD(1).
    
    If aSSD(-1) is the smallest (meaning the answer is on the -1 side), the dir is 
    set to -1. Use CF as the next longarm and check only the -da rotation: 
                     
                    .G
                  P'                               
                .' 
               F
     
     dir=0: Keep CP as next longarm, and scale down the da by half: new da = da/2
                          
                F.   .G
                  `P'  
     
     dir=1: next longarm = CG and check only the +da rotation;  
                
                F.
                  `P.
                     `G    
     
                 
*/


function 2_8_shape2d_info( // Based on 2_4_shape2d_info()
                           // --- wrapping up
                           
                            
      pts
    , prec= 3 // minimum angle jump
    , nMaxSteps = 20 // <== just for safeguard
    , da= 45  // Intv for each search. Given line [I,B] where I 
              // is the intsec of two quadlines, make [I,A] and
              // [I,C] where angle([B,I,A])= -da and angle([B,I,C])=da.
              // Calc the aSSD (avg sum of squared dist) on IA,IB,IC.   
              // Find one that has the smallest aSSD. If the IB (the original
              // one) has the smallest, half the da and repeat. If the picked
              // line is IC, then pick IC and repeat with +da. If IA, 
              // pick IA and repeat with -da 
              // Search stops when da < prec or nSteps=nMaxSteps 
    , _longax 
    , _longarm
    , _starting_aSSD
    , _aSSD
    , _dir // -1|0|1 for -da, da/2, and da, respectively
           // _dir=0 means need to check both ends with +-da/2
           // _dir=-1 means, 
           /*
              _dir=-1: Use A as next longarm and check only -da
                                  
                      C  
                     /
                    B                               
                   / 
                  A

              _dir=0: Keep B as next longarm, check +-da/2
                          
                  A.       C
                    `.   .`
                      `B'

              _dir=1: Use C as next longarm and check only +da
                          
                  A.
                    `B.
                       `C

           */
                        
    , _i=-1)=
(  
   let(lv=1)
   _i==-1?
     echo(log_b(["2_8_shape2d_info() init: _i",_i], lv))
     let( C= centroidPt(pts) 
        //, _pts= pts //[for(p=pts)p-C] 
        , qcs = quadCentroidPts(pts, Rf=C)
        , ax02= [qcs[0], qcs[2]]   
        , ax13= [qcs[1], qcs[3]]
        , _longax= dist(ax02)>dist(ax13)
                    || ax13[0]==undef    // take care of quadrants w/o any pt
                    || ax13[1]==undef? 
                    ax02:ax13
        //, B = _longax[0]
        //, I = Cent //intsec(ax02,ax13)
        //, N = N( [B,I,_pts[0]] ) 
        , _starting_aSSD= avg_SSD(pts, _longax)  
        )
     //echo( log_(["pts", pts] ))
     echo( log_(["C", C] , lv))
     echo( log_(["qcs", qcs], lv ))
     echo( log_(["ax02", ax02], lv ))
     echo( log_(["ax13", ax13], lv ))
     echo( log_(["_longax", _longax], lv ))
     //echo( log_(["sum_pts",sum(pts)] , lv))
     //echo( log_(["sum_pts/n",sum(pts)/len(pts)] , lv))
     //echo( log_(["_pts",_pts], lv ))
     echo( log_(["_starting_aSSD",_starting_aSSD] , lv))
     echo( log_e("", lv) )
     2_4_shape2d_info( 
                  pts=pts
                , prec= prec 
                , nMaxSteps = nMaxSteps 
                , da= da 
                , _ax02= ax02
                , _ax13= ax13
                , _longax = _longax
                , _longarm = [C, _longax[0]] //[I,B] 
                , _starting_aSSD=_starting_aSSD
                , _aSSD=_starting_aSSD
                , _dir=0
                , _i=_i+1)   
   
  : !( _i>nMaxSteps || abs(da)< prec/2 )? 
     
    let(   
       B= _longarm[1]
       , I= _longarm[0]
       , 
       A= _dir==1? undef:anglePt( [B,I,Y], a=-da, Rf=Z ) // _dir=1, forward, don't need A 
       , C= _dir==-1? undef:anglePt( [B,I,Y], a=da, Rf=Z ) // _dir=-1, backward,don't need C 
       , aSSD_A= _dir==1? _aSSD*2  // _dir=1, forward, don't need A, *2 to make it larger
                        : avg_SSD( pts, [I,A])
       , aSSD_B= _aSSD 
       , aSSD_C= _dir==-1? _aSSD*2  // _dir=-1, backward, don't need C, *2 to make it larger
                         : avg_SSD( pts, [I,C]) 
       , sorted_aSSDs = sortArrs([ [ aSSD_A, -1] 
                          , [ aSSD_B, 0] 
                          , [ aSSD_C, 1] 
                          ], by=0)
       , nextdir= sorted_aSSDs[0][1] //[-1,0,1][ smallest ]                   
       , _aSSD= sorted_aSSDs[0][0]                   
       )
     echo( log_b( _s("_i={_}, nMaxSteps={_}, prec={_}, da={_}"
                 ,[_i,nMaxSteps,prec,da]),lv ))
     //echo( log_( ["pts", pts]))
     echo( log_( ["_longarm", _longarm], lv))
     echo( log_( ["IA", [I,A]], lv))
     echo( log_( ["IB", [I,B]], lv))
     echo( log_( ["IC", [I,C]], lv))
     echo( log_( ["aSSD_A", aSSD_A], lv))
     echo( log_( ["aSSD_B", aSSD_B], lv))
     echo( log_( ["aSSD_C", aSSD_C], lv))
     echo( log_( ["aSSDs", [[aSSD_A,-1],[aSSD_B,0],[aSSD_C,1]]], lv))
     echo( log_( ["sorted_aSSDs", sorted_aSSDs], lv))
     //echo( log_( ["i of lowest aSSDs:", aSSDs[0][1]] , lv))
     echo( log_( ["_dir", _dir], lv))
     echo( log_( ["next_dir", nextdir], lv))
     echo( log_( ["_aSSD", _red(_aSSD)], lv))
     echo( log_e("", lv))
     2_4_shape2d_info( 
                  pts=pts
                , prec= prec 
                , nMaxSteps = nMaxSteps  
                , da= nextdir==-1 ?-da // The one with -da has the smallest aSSD
                        :nextdir==0 ? da/2
                        : da 
                , _ax02= _ax02
                , _ax13= _ax13
                , _longax = _longax
                , _longarm = nextdir==-1?[I,A]
                             :nextdir==0 ?[I,B]:[I,C]
                , _starting_aSSD= _starting_aSSD
                , _aSSD= _aSSD
                , _dir = nextdir
                , _i=_i+1) 
                
   : //----------------- exiting ------------------ 
       
     echo( log_b( _s("Existing 2_4_shape2d_info, _i={_}, prec={_}, da={_}"       
                 , [_i,prec,da] ), lv))
     //echo( log_(["I",I]))
     //echo( log_(["_pts",_pts]))
     let( len_2ndArm= dist(_longax)-dist(_longarm)
        , longAx=[ _longarm[1], onlinePt(_longarm, len=dist(_longarm)- dist(_longax))] 
        , longAx2= [ onlinePt(_longarm, ratio=1.5)
                 , onlinePt(_longarm, len=-len_2ndArm*1.5)
                 ] 
     , 
        , half_width= sqrt(_aSSD,2)
                      // Suppose there's a rectangler frame boxing all pts, 
                      // and all pts are on the two frame sides parallel to
                      // the long Ax, then they will give a _aSSD the same
                      // as calculated _aSSD. So we got the calc. _aSSD, reverse
                      // it back to "avg distance" a pt is to the longax to get
                      // the half_width
        , shortAx= [anglePt( [_longarm[1], _longarm[0], X], a=90, len= half_width )
                   , anglePt( [_longarm[1], _longarm[0], X], a=-90, len= half_width )
                   ]
        , shortLongRatio= dist(shortAx)/dist(longAx) 
        //-------------
        , longEndPtsInfo= endPtsInfoAlongLine(pts,longAx)
                         ///: [ [ i0, pt0, dist0], [i1, pt1, dist1] ]
                         
        , longAx_Js = [ projPt(longEndPtsInfo[0][1],longAx)
                      , projPt(longEndPtsInfo[1][1],longAx)]
                      // 2 end pts on the longAx 
        , longAx_midPt = sum(longAx_Js)/2
        , longAx_bysect_line = [ longAx_midPt
                               , anglePt( [longAx_Js[0], longAx_midPt, X]
                                        , a=90, len= half_width 
                                        ) 
                               ]
        , sortByDist_against_longAx = sortByDist( pts
                                       , [longAx[0], longAx[1]
                                         , N([longAx[0], longAx_midPt,shortAx[0]])
                                         ] // Use plane 'cos we need negative dist
                                       , isAbs=0)
                         ///: [ [ i0, pt0, dist0], [i1, pt1, dist1], ... ]
        
        , shortAx_Js = [ projPt(sortByDist_against_longAx[0][1],longAx_bysect_line) 
                       , projPt(last(sortByDist_against_longAx)[1],longAx_bysect_line)
                       ]
                       
        , boxPts = [ longAx_Js[0]+ shortAx_Js[0]-longAx_midPt
                   , longAx_Js[0]+ shortAx_Js[1]-longAx_midPt
                   , longAx_Js[1]+ shortAx_Js[1]-longAx_midPt
                   , longAx_Js[1]+ shortAx_Js[0]-longAx_midPt
                   ]  
        , roundedness= dist(shortAx_Js)/dist(longAx_Js)                                           
        )
 
        
     echo( log_e("", lv))
     ["longarm",_longarm
//     , "longAx", [ _longarm[1], onlinePt(_longarm, len=dist(_longarm)- dist(_longax))] 
//     , "longAx2", [ onlinePt(_longarm, ratio=1.5)
//                 , onlinePt(_longarm, len=-len_2ndArm*1.5)
//                 ] 
     , "longAx", longAx
     , "longAx_Js", longAx_Js
     , "shortAx_Js", shortAx_Js
     , "boxPts", boxPts
     , "roundedness", roundedness
     //, "half_width", half_width            
     , "shortAx", shortAx
     , "shortlongratio", shortLongRatio            
     , "aSSD",_aSSD
     , "quadAx02", _ax02
     , "quadAx13", _ax13
     , "center", _longarm[0]
     , "Note", "longAx2= longAx extended on each side by 50%; quadAx02=[quadCentroidPts[0],quadCentroidPts[1]]; center=longarm[0]= intsec of quadaxes" 
     ]
);

module 2_9_demo_longAx_by_angle_adj_application( )
{
   echom("2_9_demo_longAx_by_angle_adj_application()");
   log_b("2_9_demo_longAx_by_angle_adj_application()");
   //echo( _red("Evaluate use_centroid_as_pivot=true/false in 1_11_longAx()" ));
   //echo( _red("Result: setting use_centroid_as_pivot=true doesn't seem to help much (1%)" ));
    
   //--------------------------------------------
   // data-1: rand pts
   //nseg=10; 
   //m=5;
   //noise=1.5;
   //len=4; 
   //pq= randPts(2);
   //pq = [O, 4*X];
   //_pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noise, len=len);
   //pts = h(_pts_pq, "pts");
   //_pq = h(_pts_pq, "translated_pq");
   
   //log_(["pts", pts]);
   //log_(["_pq", _pq]);
      
   
      
   function rand_rectangular_4pts(oundedness=0.3)=
   (
     let( rand_a = rands(0,359,1)[0] 
        ,_pts = [ 2*X-2*Y*roundedness
                ,-2*X-2*Y*roundedness
                ,-2*X+2*Y*roundedness
                , 2*X+2*Y*roundedness
                ]
        , C= centroidPt(_pts)
        , pts= rotPts(_pts, axis=[O,Z], a=rand_a)
        )
    echo(rand_a=rand_a)
    pts    
   
   );
   //pts = rand_rectangular_4pts(roundedness=.2);
   
   rndPt = randPt();
   baseline_len = 4;
   noise_ratio = .5;
   pq = [rndPt, rndPt+[-1,-1,1]*baseline_len];
   pts = randPtsAlongLine(pq, count=100, d= baseline_len*noise_ratio );
         
   //Red() Plane(pts);      
   //echo(pts=pts);
   
   data= 2_8_shape2d_info( pts=pts
                , prec= 3 // minimum angle jump
                , nMaxSteps = 20 // <== just for safeguard
                , da= 45 );
   log_(["data", _fmth(data) ]);
      
   Blue() MarkPt( h(data, "center"), r=0.1);
   MarkPts( h(data, "longAx"), r=0.03 );             
  // MarkPts( h(data, "longAx2") );  
   //MarkPts( h(data, "shortAx") );  
   Red(0.2)
   {  Line( h(data, "quadAx02"),r=0.1);
      Line( h(data, "quadAx13"),r=0.1 );
   }           
   
   //Line( h(data, "shortAx_Js"), r=0.2 );
   Line( h(data, "boxPts"), closed=true );
   
   Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
   //Gold(0.2) Line( _pq, r=0.5 );     
   //------------------------------------------------
   
   log_e();
}
2_9_demo_longAx_by_angle_adj_application( );


//module 2_4_tester_longAxis_by_angle_adj( 
//               len=4
//             , nseg=10
//             , m=5
//             , noises= [.5, 1, 1.5, 2]
//             , nRepeat= 5
//             , _data=[], _inoise=0, _irep=0
//             , _i=0
//             )
//{
//   
//    //echom( _s("tester, _inoise={_}, __inoise={_}, _irep={_}, __irep={_}, _i={_}"
//     //        , [_inoise, __inoise, _irep, __irep, _i]));
//    /*
//ECHO: "   The worst data: data #="10", worst data at i=10, aSSD(%)="1.26399(36.5%)", aSSD_C(%)="1.26937(37.1%)", angle="40.41""
//ECHO: "   Result table:"
//ECHO: "   ssd="Sum of Squared Dist. The smaller ssd, the better result it is.""
//ECHO: "   ssd0="Avg SSD of the pts against source line. Unknown in real case.""
//ECHO: "   aSSD="Avg SSD of the pts against the 1onger arm of quadCentroidPts""
//ECHO: "   aSSD_C="Avg SSD of the pts against the 1onger arm of quadCentroidPts connected to overall centroidPt""
//ECHO: "   %="Diff % of either aSSD or aSSD_C against ssd0""
//ECHO: "   angle="Angle between the source line and the long arm of quadCentroidPts. Unknown in real case""
//ECHO: "   
// i  n  ns/L  ssd0   aSSD   aSSD%   aSSD_C  aSSD_C%  angle 
// 0 50 0.125  0.08    0.08   2.80      0.08    2.90    1.12
// 1 50 0.125  0.09    0.08  -10.00     0.08   -8.40    4.28
// 2 50 0.125  0.09    0.09   5.50      0.09    5.30    3.18
// 3 50 0.125  0.09    0.09  -6.50      0.09   -6.80    3.61
// 4 50 0.125  0.08    0.09  14.50      0.08    8.60    4.08
//
// 5 50 0.250  0.33    0.32  -2.60      0.32   -1.60     5.7
// 6 50 0.250  0.32    0.33   2.60      0.33    3.30    4.92
// 7 50 0.250  0.29    0.29  -0.40      0.29   -0.60    8.17
// 8 50 0.250   0.4    0.39  -2.80      0.39   -2.80    2.83
// 9 50 0.250  0.25    0.24  -3.70      0.24   -3.80    7.84
//
//10 50 0.375  0.93    1.26  36.50      1.27   37.10   40.41
//11 50 0.375   0.6    0.62   3.50      0.62    2.90    5.49
//12 50 0.375  0.66    0.73  10.50      0.72    9.00   10.62
//13 50 0.375  0.76     0.7  -7.70       0.7   -8.70    3.11
//14 50 0.375  0.78    0.69  -11.60      0.7  -10.70   11.16
//
//15 50 0.500  1.17    1.03  -12.20     1.03  -12.20   12.21
//16 50 0.500  1.35    1.34  -0.80      1.35   -0.70   43.29
//17 50 0.500  1.71    1.88   9.70      1.88    9.60   62.11
//18 50 0.500  1.66    1.76   6.30      1.77    6.40   22.96
//19 50 0.500  1.49    1.56   4.20      1.57    5.10   12.46
//"
//ECHO: "   The worst data set..."
//ECHO: "   Calc sd of aSSD%, aSSD_C% and angle"
//ECHO: "   
// i  n  ns/L  ssd0   (+-0)   aSSD   (+-1)  aSSD%   (+-2)  aSSD_C   (+-3)  aSSD_C%  (+-4)  angle   (+-5)
// 0 50 0.125  0.09  0.01     0.09  0.01     1.26  9.78     0.09   0.01     0.32   7.53     3.25  1.27  
// 5 50 0.250  0.32  0.06     0.31  0.06    -1.38  2.53     0.31   0.06     -1.10  2.74     5.89  2.2   
//10 50 0.375  0.75  0.12     0.8   0.26     6.24  19.07     0.8   0.26     5.92   19.24   14.16  15.06 
//15 50 0.500  1.48  0.22     1.51  0.34     1.44  8.52     1.52   0.34     1.64   8.59    30.61  21.67   
//    
//    Observation (18.8.16):
//    
//    -- Can't tell which of aSSD and aSSD_C is better;
//    -- angle variations (sd) is usually less than 15. 
//    -- ssd is roughly proportational to ns/L --- larger ns/L, more like a circle a shape is.  
//    
//    Next:
//    
//      Calc aSSD_C and Use the method described in:
//      https://stackoverflow.com/questions/12934213/how-to-find-out-geometric-median
//      
//      to approach a better answer, starting angle jump
//      can set at +-15.  
//    
//    */ 
//     
//     
//    if(_irep== 0) 
//    {
//       echo("");
//       echo("---------------, _inoise= ", _inoise);
//    }
//  
//    if ( (_i< len(noises)*nRepeat) )
//    {
//      log_b(_s("_i={_}",_i));
//       pq= randPts(2);
//   //pq = [O, 4*X];
//   _pts_pq= pts2( pq=pq, nseg=nseg, m=m, noise=noises[_inoise], len=len);
//   pts = h(_pts_pq, "pts");
//   _pq = h(_pts_pq, "translated_pq");
//   ssd0 = avg_SSD(pts,_pq);
//   
//   C= centroidPt(pts);
//   
//   q_Axis = 1_11_longAxis(pts); // long axis retrieved from quadCentroidPts line
//   aSSD = avg_SSD( pts, longaxis);
//   //==============================
//   a_data = 2_2_longAxis_by_angle_adj(pts);// shape data retrieved from quadCentroidPts line + angle adjustment
//   a_Axis = h(a_data, "longax2");// long axis retrieved from quadCentroidPts line + angle adjustment
//   a_aSSD = h(a_data, "aSSD");
//   
//   
//   // calc the angle
//   //P = qAxis[1];
//   //dP= [ for(p=_pq) [p, dist(p,P)]];
//   //Q = sortArrs(dP, by=1)[0][0];
//   //I = intsec( qAxis, _pq );
//   //_angle = angle([P,I,Q] );
//   //angle= _angle>90? (180-_angle):_angle;
//   
//   //log_(["_i",_i, "longaxis", longaxis, "_pq", _pq
//   //     , "intsec", intsec(longaxis,_pq)] );
//   //log_(["P",P, "Q",Q, "I",I, "angle", angle]);     
//   //log_(["angle45", angle([X,O, X+Y]) ]);
//   //log_(["angle45", angle([newz(X,0),O, newz(X+Y,0)])] );
//   //angle = angle( [ longaxis[0]
//                 //, intsec(longaxis,_pq)
//                 //, _pq ] );
//               //
//          
//       ssds = [ ssd0, aSSD, a_aSSD ];
//       data = [ aSSD, perc_d(v0=ssd0, v=aSSD)
//              , aSSD_C, perc_d(v0=ssd0, v=aSSD_C) ];
//               
//       __data = concat( _data
//                     , [concat( [ nseg*m
//                                , numstr(noises[_inoise]/len,2)
//                                , numstr(ssd0,3) //deci(ssd0) 
//                                ] 
//                        , data )]
//                     );
//       
//       __data= concat( _data
//                     , [[ _i
//                        , nseg*m
//                        //, numstr(noises[_inoise]/len,3)
//                        //, numstr(ssd0,3) //deci(ssd0) 
//                        , noises[_inoise]/len
//                        , ssd0 //deci(ssd0) 
//                        , aSSD
//                        , perc_d(v0=ssd0, v=aSSD)
//                        , aSSD_C
//                        , perc_d(v0=ssd0, v=aSSD_C)
//                        //, angle
//                        //, angle_C
//                        
//                        , pts
//                        , longaxis
//                        , longaxis_C
//                        , _pq
//                        , angle
//                        ]
//                       ]   
//                     );
//       
//       __inoise = _inoise+ (_irep==nRepeat-1?1:0); 
//       __irep = // +1 except: _inoise is still running AND _irep running out
//               _irep>=nRepeat-1? 0:(_irep+1);
//       //log_( _s("tester_longAxis_longAxisC, nRepeat={_}, _inoise={_}, __inoise={_}, _irep={_}, __irep={_}, _i={_}"
//       //      , [nRepeat, _inoise, __inoise, _irep, __irep, _i]));
//       //log_( ["pq",pq,"_pq",_pq] );
//       //log_( ["C",C] );
//       //log_( ["longaxis",longaxis] );
//       //log_( ["longaxis_C",longaxis_C] );
//       //log_( ["ssd0", ssd0] );
//       //log_( ["aSSD", aSSD] );
//       //log_( ["aSSD_C", aSSD_C] );
//       //log_(["ssds", ssds]);
//       //log_(["data", data]);
//       //log_(["__data", __data]);
//       log_e();       
//       tester_longAxis_longAxisC_use_tablelist( 
//               len=len
//             , nseg=nseg
//             , m=m
//             , noises= noises 
//             , nRepeat= nRepeat
//             , _data=   __data
//             , _inoise= __inoise
//             , _irep  = __irep
//             , _i=_i+1
//             );              
//    }
//    else
//    {
//       log_b("tester looping completed...");
//       sorted_data = sortArrs( _data, by= 5); 
//       worst_data = last(sorted_data);
//       worst_i = worst_data[0];
//       
//       log_( ["The worst data: data #", _red(worst_data[0]) 
//             , "worst data at i", worst_i
//             , "aSSD(%)", str( worst_data[4], "(", _red(worst_data[5]), "%)") 
//             , "aSSD_C(%)", str( worst_data[6], "(", _red(worst_data[7]), "%)")
//             , "angle", numstr(last(worst_data),2) 
//             ] );
//          
//       prn_data= [for(i= range(_data)) //d=_data)
//                   concat( get(_data[i], range(8)), [last(_data[i])] )
//                 ];
//       log_( _b("Result table:") );
//       log_( ["ssd", "Sum of Squared Dist. The smaller ssd, the better result it is."] );
//       log_( ["ssd0", "Avg SSD of the pts against source line. Unknown in real case."]);
//       log_( ["aSSD", "Avg SSD of the pts against the 1onger arm of quadCentroidPts"]);
//       log_( ["aSSD_C", "Avg SSD of the pts against the 1onger arm of quadCentroidPts connected to overall centroidPt"]);
//       log_( ["%","Diff % of either aSSD or aSSD_C against ssd0"] );
//       log_( ["angle", "Angle between the source line and the long arm of quadCentroidPts. Unknown in real case"]);
//       log_( _table( header=["i","|w2ac","n","|w2ac","ns/L","|w5d3"
//                      , "ssd0", "|w6d2ac"
//                      , "aSSD",  "|w6d2", "aSSD%","|w7acd2"
//                      , "aSSD_C","|w8d2", "aSSD_C%","|w7ard2"
//                      , "angle",  "|w7d2"//, "(+-sd3)","|w7ald2"  
//                      ]
//                   , data= prn_data 
//                   , opt=["sep_is",[m:m:nseg*m] 
//                         , "rowstyles", [[worst_i], "color:red"]
//                         ]
//                   )
//           );        
//                   
//       //log_( ["longaxis", worst_data[9], "longaxis_C", worst_data[10]
//       //      , "_pq", worst_data[11]] );
//       
//         log_("The worst data set...");
//         
//         pts=worst_data[8];
//         longaxis=worst_data[9];
//         longaxis_C=worst_data[10];
//         _pq=worst_data[11];
//         
//         Black() MarkPts( pts, Label=false, Ball=["dec_r",false], Line=0, r=0.05 );
//         Red() MarkPts( longaxis, Label=false );
//         Blue() MarkPts( longaxis_C, Label=false, Ball=["r",0.1, "color",["blue",0.3]] );
//         Gold(0.2) Line( _pq, r=0.5 );
//         
//         //log_( ["pts resulting in the worst data:", pts] );
//        
//       //
//       // calc sd of aSSD%, aSSD_C% and angle
//       //
//       log_b("Calc sd of aSSD%, aSSD_C% and angle",1);
//       // First, group data by noise levels:
//       
//       data2= subArrs( _data, size=nRepeat);
////       sds = [ for(g=data2) 
////                [ for(d=g)
////                   get(d, [0,1,2,5,7,-1]) // i, nseg*m, str(noise/L), aSSD%, aSSD_C%, angle
////                ]   
////             ];
//       sds = [ for(i= range(data2)) 
//                let( grp= data2[i] 
//                   
//                   //, avg_aSSD_percent = numstr(avg( [ for(d=grp) d[5] ] ),2) 
//                   //, sd_aSSD_percent = numstr(sd( [ for(d=grp) d[5] ] ),2) 
//                   
//                   , avg_aSSD_percent = _f(avg( [ for(d=grp) d[5] ] ),"|w6d2arp ") 
//                   , sd_aSSD_percent = _f(sd( [ for(d=grp) d[5] ] ),"|d2w6ar") 
//                   
//                   , avg_aSSD_C_percent = _f(avg( [ for(d=grp) d[7] ] ),"|d2w6ar") 
//                   , sd_aSSD_C_percent = _f(sd( [ for(d=grp) d[7] ] ),"|d2w6ar") 
//                   
//                   , avg_angle = _f(avg( [ for(d=grp) last(d) ] ),"|d2w6ar") 
//                   , sd_angle = _f(sd( [ for(d=grp) last(d) ] ),"|d2w6ar") 
//                   )
//                //echo( grp)   
//                [ _f( grp[0][0], "|w2ar")
//                , grp[0][1] // n
//                , grp[0][2] // noise/L
//                //, grp[0][3] // noise/L
//                , avg_aSSD_percent, sd_aSSD_percent
//                , avg_aSSD_C_percent, sd_aSSD_C_percent
//                , avg_angle, sd_angle 
//                  // i, nseg*m, str(noise/L), aSSD%, aSSD_C%, angle
//                ]   
//             ];
//       sds2 = [ for(i= range(data2)) 
//                let( grp= data2[i] 
//                   
//                   
//                   , avg_ssd0_percent = numstr(avg( [ for(d=grp) d[3] ] ),2) 
//                   , sd_ssd0_percent = numstr(sd( [ for(d=grp) d[3] ] ),2) 
//                   
//                   , avg_aSSD = avg( [ for(d=grp) d[4] ] )
//                   , sd_aSSD = sd( [ for(d=grp) d[4] ] )
//                   
//                   , avg_aSSD_percent = avg( [ for(d=grp) d[5] ] ) 
//                   , sd_aSSD_percent = sd( [ for(d=grp) d[5] ] )
//                   
//                   , avg_aSSD_C = avg( [ for(d=grp) d[6] ] )
//                   , sd_aSSD_C = sd( [ for(d=grp) d[6] ] ) 
//                   
//                   , avg_aSSD_C_percent = avg( [ for(d=grp) d[7] ] ) 
//                   , sd_aSSD_C_percent = sd( [ for(d=grp) d[7] ] ) 
//                   
//                   , avg_angle = avg( [ for(d=grp) last(d) ] ) 
//                   , sd_angle = sd( [ for(d=grp) last(d) ] ) 
//                   )
//                //echo( grp)   
//                [ grp[0][0]
//                , grp[0][1] // n
//                , grp[0][2] // noise/L
//                
//                , avg_ssd0_percent, sd_ssd0_percent
//                , avg_aSSD, sd_aSSD
//                , avg_aSSD_percent, sd_aSSD_percent
//                , avg_aSSD_C, sd_aSSD_C
//                , avg_aSSD_C_percent, sd_aSSD_C_percent
//                , avg_angle, sd_angle 
//                  // i, nseg*m, str(noise/L), aSSD%, aSSD_C%, angle
//                ]   
//             ];
//             
//       //log_( str("sds= ",sds), 1 );  
//       //log_( ["lens ", [for(d=data2) len(d)]], 1 );      
//       log_( _table( 
//               header=["i","|w2ac","n","|w2ac","ns/L","|w5d3"
//                      , "ssd0",  "|w6d2ac", "(+-0)","|w6ald2"
//                      , "aSSD",  "|w7d2ac", "(+-1)","|w6ald2"
//                      , "aSSD%",  "|w7d2ac", "(+-2)","|w6ald2"
//                      , "aSSD_C","|w8d2ac", "(+-3)","|w6ald2"
//                      , "aSSD_C%","|w8d2ac", "(+-4)","|w6ald2"
//                      , "angle",  "|w7d2ac", "(+-5)","|w6ald2"  
//                      ]
//               ,data=sds2       
//                    )
//            );        
//       log_e(layer=1);
//          
//       log_e();
//       
//    }     
//}
////tester_longAxis_longAxisC_use_tablelist();


//===================================================
//===================================================
//===================================================
//===================================================
//===================================================
//===================================================
//===================================================
//===================================================
//===================================================
//===================================================
//===================================================
//===================================================
//===================================================


function pts( pq, nseg=10, m=5, noise=1, len=4)=
(  
   // make pts. Each segment has m pts. Total pt count = nseg*m
    
   let( _pq = pq? [newz(pq[0],0), newz(pq[1],0) ]
              :randPts(2, d=3, z=0)         // 2 random pts
      , pq = [_pq[0], onlinePt(_pq,len)]   // set len
      , _pts = [ for(i=range(nseg)) 
                 each [for(j=[0:m-1]) 
                        onlinePt( pq, len= dist(pq)/nseg*i) + randPt(d=noise, z=0)
                      ]
            ]
      )      
   
   //echo( _s( "<b>pts</b>( pq, nseg={_}, m={_}, noise={_}, len={_}) => {_} pts"
   //    ,[nseg,m,noise,len, _b(len(_pts))]))
       
   // Translate the whole thing such that its centroidPt is on O:
   transPts( _pts, actions=["t",-1*centroidPt(_pts)] )
);

function pts2( pq, nseg=10, m=5, noise=1, len=4)=
(  
   // make pts like pts() but return both pts and translated_pq
    
   let( _pq = pq? [newz(pq[0],0), newz(pq[1],0) ]
              :randPts(2, d=3, z=0)         // 2 random pts
      , pq = [_pq[0], onlinePt(_pq,len)]   // set len
      , _pts = [ for(i=range(nseg)) 
                 each [for(j=[0:m-1]) 
                        onlinePt( pq, len= dist(pq)/nseg*i) + randPt(d=noise, z=0)
                      ]
            ]
      , pts=    transPts( _pts, actions=["t",-1*centroidPt(_pts)] )
      , translated_pq = transPts( pq, actions=["t",-1*centroidPt(_pts)] )
      )      
   
   //echo( _s( str("<b>pts2</b>( pq, nseg={_}, m={_}, noise={_}, len={_}) => {_} pts "
                //, "(translated_pq={_})") 
       //,[nseg,m,noise,len, _b(len(_pts)),translated_pq]))
     ["pts", pts, "translated_pq",translated_pq ]  
);
