//include <../doctest/doctest2_dev.scad>
//use <scadx_code.scad>
//include <../doctest/doctest.scad>
//MODE=112;
    
//=========================================================

function accum(arr)=
(
    [for (i=range(arr)) sum( slice(arr, 0,i+1))]
);    

    accum=[ "accum", "arr", "array", "Array" 
    ,"Given [a,b,c...], return [a,a+b,a+b+c...]
    "
    //,"sum, sumto"
    ];

    function accum_test( mode=MODE, opt=[] )=
    (
        doctest( accum,
        [ 
           [ accum([3,4,2,5]), [3,7,9,14], [3,4,2,5] ] 
        ]
        , mode=mode, opt=opt 
        )
    );
    
//================================================================
function all(arr,isdefined=false,item=undef)=
(
    isdefined
    ?(len([ for(x=arr) if(x==item) 1])==len(arr))
    :(len([ for(x=arr) if(x) 1])==len(arr))
);
    
    all=["all", "arr,isdefined=false,item=undef)","T|F","Core, Inspect",
    "Return true if all items in *arr* is:
    ;;  (1) ==item (when isdefined=true), or
    ;;  (2) treated as true (when isdefined=false)"
    ,"index"
    ];

    function all_test( mode=112,opt=[])=
    (
      doctest( all,
      [
        [ all( [true,1,"2",-1] ), true, [true,1,"2",-1] ]
      , [ all( [true,1,"2",0] ), false, [true,1,"2",0] ]
      , [ all( [true,1,"2",undef] ), false, [true,1,"2",undef] ]
      , ""
      , "// Use isdefined:"
      , ""
      , [ all( [true,1,"2",-1],true,true ), false, "[true,1,'2',-1],true,true" ]
      , [ all( [0,0,0]),false, [0,0,0] ]
      , [ all( [0,0,0],true,0 ), true, "[0,0,0],true,0" ]
      , [ all( [undef,undef] ), false, [undef,undef] ]
      , [ all( [undef,undef],true,undef ), true
            , "[undef,undef],true,undef"]
      ]
      , mode=mode, opt=opt
      )
    );    
    
//==========================================================

function any(arr)=
(
    len([for( x=arr ) if(x)1])>0
);   
   
  any=["any", "arr", "true|false", "Array",
  "Return true if any item in arr is evaluated as true "
  ];    
  function any_test( mode=MODE, opt=[] ) =
  (  
      doctest( any,
      [ 
         [ any( [0, undef] ),  false, "[0, undef]" ]
      ,  [ any( [0, 1, undef] ),  true, "[0, 1, undef]" ]
      ], mode=mode )
  );
//==========================================================

function app(arr,x)= concat(arr,[x]);

    app=["app", "arr,x", "arr", "Array"
    ,"Append x to arr"
    ,"appi"
    ];

    function app_test( mode=MODE, opt=[] ) =
    (  
        doctest( app,
        [ 
           [ app( [1,2,3],4 ),  [1,2,3,4], "[1,2,3], 4" ]
        ], mode=mode )
    );


//==========================================================
function appi(arr, i, x, conv=false)=
    let( xi = get(arr,i)
       , xx= isarr(xi)?xi:[xi] 
       , nx= concat( xx, [x] )
       )
    (   
      !conv && !isarr(xi)
       ? typeErr( [ "vname", _s("arr[{_}]",[i])
                  , "fname", "appi"
                  , "tname", "arr"
                  , "vtype", type(xi)
                  , "val"  , xi ] )            
      : [ for(_i=range(arr)) 
            _i==fidx(arr,i) ? nx : arr[_i] ]
    );

    appi=["appi", "arr,i,x,conv=false", "arr", "Array"
    , "Append x to arr[i]. If conv=false, the arr[i] MUST be an array, or will return an error msg. 
    ;; 
    ;; If conv and it's not an array, it will be converted to array, 
    ;; then appended with x. Arg i could be negative. 
    ;; 
    ;;   appi([1,2,[3,33],4], 2,9)= [1,2,[3,33,9],4] 
    ;;   appi([1,2,[3,33],4],-2,9)= [1,2,[3,33,9],4] 
    ;; 
    ;; When arr[i] not an array:
    ;; 
    ;;   appi([1,2,[3,33],4],1,9) =   [typeError] : 
    ;;       arr[1] in appi() should be arr. A int given (2) 
    ;; 
    ;; But setting conv=true:
    ;; 
    ;;   appi([1,2,[3,33],4],1,9,true)= [1,[2,9],[3,33],4]"
    ,"app"
    ];

    function appi_test( mode=MODE, opt=[] )=
    (
        let( a=[1,2,[3,33],4] )
        
        doctest( appi,
        [
          [ appi(a, 2,9), [1,2,[3,33,9],4],"[1,2,[3,33],4], 2,9" ]
        //, [ appi(a, 2,9,true), [1,2,[3,33,9],4],"[1,2,[3,33],4], 2,9" ]
        , [ appi(a,-2,9), [1,2,[3,33,9],4], "[1,2,[3,33],4],-2,9" ]
          
        ,""
        ,"// If item-i not an array:"
        ,""
        ,[appi(a,1,9), "", "[1,2,[3,33],4],1,9)", ["notest",true] ]
          
        ,""
        ,"// Set conv=true to turn it into an array:"
        ,""
        , [ appi(a,1,9,true), [1,[2,9],[3,33],4], "[1,2,[3,33],4],1,9,true"]
        
        ], mode=mode, opt=opt 
        )
    );
   
//=====================================================

function arrblock(arr, indent="  "
					, v="| "
					, t="-"
					, b="-"
					, lt="."
					, lb="'"
					, lnum=undef
					, lnumdot=". "					 
)=
(
  concat( [ str( indent, lt,repeat(t, 45) ) ]
		, isint(lnum)
		  ? [ for( i=range(arr) ) str( indent, v, i+lnum, lnumdot, arr[i]) ]
		  : [ for( i=range(arr) ) str( indent, v,                , arr[i]) ]
		, [ str( indent, lb,repeat(b, 45) ) ]
  ) 
);
     
    arrblock=[ "arrblock","arr,indent='  ',v='| ', t='-',b='-',lt='.', lb='\"'","array", "Doc"
    , "Given an array of strings, reformat each line
    ;; for echoing a string block . Arguments:
    ;; 
    ;;  indent: prefix each line; 
    ;;  v : vertical left edge, default '| '
    ;;  t : horizontal line on top, default '-'
    ;;  b : horizontal line on bottom, default '-'
    ;;  lt: left-top symbol, default '.'
    ;;  lb: left-bottom symbol, default `
    ;;  lnum: undef. Set to int to start line number
    ;;  lnumdot: '. '
    ;; 
    ;; join(arrblock( ['abc','def', 'ghi'], lnum=1 ), &lt;br/&gt;)= ''
    ;;  .---------------------------------------------
    ;;  | 1. abc
    ;;  | 2. def
    ;;  | 3. ghi
    ;;  `---------------------------------------------
    "];

    function arrblock_test( mode=MODE, opt=[] )=
    (
        let( br="<br/>" )
        doctest
        (
            arrblock
            ,[
                [ join( arrblock(["abc","def"]), br)
                , join( [ "  .---------------------------------------------"
                        , "  | abc"
                        , "  | def"
                        , "  '---------------------------------------------"
                        ], br)
                , ["abc","def"]
                , ["nl",true, "myfunc", "join( arrblock(['abc','def']), br) "] 
               ]  
        
            ,	[ join(arrblock(["abc","def","ghi"], lnum=1),br)
                , join([ "  .---------------------------------------------"
                        , "  | 1. abc"
                        , "  | 2. def"
                        , "  | 3. ghi"
                        , "  '---------------------------------------------"
                        ], br ) 
                , "['abc','def', 'ghi'], lnum=1"
                , ["nl",true
                  , "myfunc"
                     , ["join", "arrblock( ['abc','def','ghi'], lnum=1), br"]
                  ] 
                ]
             ]
            ,mode=mode, scope=["br","&lt;br/>"]
        )
    );          

//========================================================
          
function arrprn( // 2015.4.16
       arr
     , dep=2
     , nl=false //true // changed to true 2015.8.2
     , br="<br/>"
     , ind=["&nbsp;", "&nbsp;"] 
     , commafirst=true // "," is placed before next item, in vertical align w/ "["
                       //  [ [2, 2]
                       //  , [2, 3]
                       //  ]
     , compact = true // First line is packed:
                       //   [[[3, 2]
                       //    ,[[4, 2]
                       //     ,[2, 3]
                       //     ]
                       //    ]
                       //   ]
     , _d=0)=
(
   let( _ind = join(ind,"")
      , hasarr= len( [for(a=arr) if(isarr(a)) 1] ) 
      , ind1 = repeat(_ind,_d)
      , ind2 = repeat(_ind,_d+1) 
      , sp = commafirst? join( [for(i=[0:len(ind)-1]) i==0?",":ind[i]], ""): ind2
      , arr2= [ for(a=arr) 
            isarr(a)?
            arrprn( a,dep=dep,br=br,ind=ind
                  , commafirst=commafirst
                  , compact=compact, _d=_d+1) 
            : a
         ] 
       )//let
   str( nl?"<br/>":""    
   , hasarr && _d<dep?
     str( "<code>[", //_green(str(repeat(SP,10), "(_d=",_d, ")"))
        , compact? join( slice(ind,1),""):str(br,ind2) 
        , join( arr2, commafirst?
                         str(br, str(ind1,sp))
                        :str(",",br,ind2) )
        , br, repeat(_ind,_d), "]</code>"
        )
     :arr2  
  ) 
); 
 
    arrprn=["arrprn","arr,dep,br,ind,commafirst,compact","str","format"
    ,"Format arr to a tree-like structure.
    ;; 
    ;; dep=1: depth of expension
    ;; br='&lt;br/&gt;': the html line break
    ;; ind= [ '&nbsp;', '&nbsp;']: joined to be the indent 
    ;; commafirst=true: ',' is placed before data, vertically align w/ [, ]
    ;; compact=true: 
    ;;    Pack multiple '['s if one next to each other."
    ,"fmt"
    ];

    function arrprn_test( mode=MODE, opt=[] )=
    (   
        let( arr= [ [[2,2],[2,3]], [[2,2],[2,3],[2,4],[2,5]], [[[3,2],[[4,2],[2,3]]]] ]
        )
        
        doctest( arrprn,
        [

        "<b>Set dep (depth)</b>" 

        , [ arrprn(arr), 
    "[ [ [2, 2]
      , [2, 3]
      ]
    , [ [2, 2]
      , [2, 3]
      , [2, 4]
      , [2, 5]
      ]
    , [ [\"[3, 2]\", \"[\"[4, 2]\", \"[2, 3]\"]\"]
      ]
    ]", "arr,dep=1", ["nl",true, "notest",true, "//","default"] ]

        , [ arrprn(arr, dep=2),"[ [ [2, 2],
        [2, 3]
      ],
      [ [2, 2],
        [2, 3],
        [2, 4],
        [2, 5]
      ],
      [ [\"[3, 2]\", \"[\"[4, 2]\", \"[2, 3]\"]\"]
      ]
    ]",
    "arr,dep=2", ["nl",true, "notest",true, "//", "(Noew: use a large dep for full depth)"] ]
        
        , "<b>Default commafirst = true: </b>" 
        
        , [ arrprn(arr, dep=2, commafirst=false), "[
      [
        [2, 2]
      , [2, 3]
      ]
    , [
        [2, 2]
      , [2, 3]
      , [2, 4]
      , [2, 5]
      ]
    , [
        [\"[3, 2]\", \"[\"[4, 2]\", \"[2, 3]\"]\"]
      ]
    ]"
                ,"arr,dep=2,commafirst=false", ["nl",true, "notest",true] ]
                
        , "<b>Default compact = true: </b>" 
        
        , [ arrprn(arr, dep=2, compact=false),"[
      [
        [2, 2]
      , [2, 3]
      ]
    , [
        [2, 2]
      , [2, 3]
      , [2, 4]
      , [2, 5]
      ]
    , [
        [\"[3, 2]\", \"[\"[4, 2]\", \"[2, 3]\"]\"]
      ]
    ]"
                ,"arr,dep=2,compact=false", ["nl",true, "notest",true] ]
                
        , "<b>Default ind= [ '&nbsp;', '&nbsp;']: </b>" 
        
        , [ arrprn(arr, dep=2, ind=["#", "..."]), "[...[...[2, 2]
    #...,...[2, 3]
    #...]
    ,...[...[2, 2]
    #...,...[2, 3]
    #...,...[2, 4]
    #...,...[2, 5]
    #...]
    ,...[...[\"[3, 2]\", \"[\"[4, 2]\", \"[2, 3]\"]\"]
    #...]
    ]"
                ,"arr,dep=2,ind=['#','...']", ["nl",true, "notest",true] ]
                
        ], mode=mode, opt=opt, scope=["arr",arr]
        )
    );    
 
 
function arr_diff(a1,a2)=
(
  [ for(x=a1) if(!has(a2,x))x 
  , for(x=a2) if(!has(a1,x))x
  ]
);

    arr_diff=["arr_diff", "a1,a2", "array", "Array",
     " Given arrays a1, a2, return the difference between
      the two --- means, items not in either a1 or a2.
      This is the same as dels(a1, dels(a2,a1))
    ;;
    ;;   dels( [2,4,6,8], [4,6] )= [2, 8]
    ;;
    ;; Ref: arrItcp
    "];

    function arr_diff_test( mode=MODE, opt=[] )=
    (
        doctest( arr_diff,
        [ 
          [ arr_diff( [2,4,8], [4,5,6]), [2,8,5,6], "[2,4,8], [4,5,6]" ]
        ]
        , mode=mode, opt=opt
        )
    );
          
//========================================================
function avg(arr) = sum(arr)/len(arr);

    avg=["avg", "arr", "n", "Array",
     " Return average of arr
     "
    ];
      
    function avg_test( mode=MODE, opt=[] )=
    (
        doctest( avg,
        [ 
          [ avg([2,3,4]),3, "[2,3,4]" ]
        , [ avg([2,3,4,5]),3.5, "[2,3,4,5]" ]
        ]
        , mode=mode, opt=opt
        )
    );  
    

//==================================================

function del(arr,x, byindex=false,count=1)=
(
    let( x = byindex? fidx(arr, x):x )
	len(arr)==0?[]
	: byindex
	  ? [for(i=range(arr)) if (i<x || i>=x+count) arr[i] ]
	  : [for(k=arr) if (k!=x) k] 
);

    del=["del", "arr,x,byindex=false,count=1", "array", "Array",
     " Given an array arr, an x and optional byindex, delete all
    ;; occurances of x from the array, or the item at index x when
    ;; byindex is true. 
    ;;
    ;;   del( [2,4,6,8], 2 )= [4, 6, 8]
    ;;   del( [3,undef, 4, undef], undef )= [3, 4]
    ;;   del( [2,4,6,8], 2, byindex=true )= [2, 4, 8] 
    ;;                      
    ;; New 2014.8.7: arg *count*: when byindex=true, count sets 
    ;;  how many items starting from x (an index) to del.
    ;;
    ;;   del( [2,4,6,8], 1, true,2 )= [2, 8] // del 2 tiems starting from 1                      
    "];

    function del_test( mode=MODE, opt=[] )=
    (
        doctest( del,
        [ 
          [ del( [2,4,4,8], 4), [2,8], "[2,4,4,8], 4" ]
        , [ del( [2,4,6,8], 6), [2,4,8], "[2,4,6,8], 6" ]
        , [ del( [2,4,6,8], 2), [4,6,8], "[2,4,6,8], 2" ]
        , [ del( [3,undef,4,undef],undef),[3,4],"[3,undef,4,undef],undef" ]
        , "// byindex = true"
        , [ del( [2,4,6,8], 0, true), [4,6,8], "[2,4,6,8], 0, true" ]
        , [ del( [2,4,6,8], 3, true), [2,4,6], "[2,4,6,8], 3, true" ]
        , [ del( [2,4,6,8], 2, true), [2,4,8], "[2,4,6,8], 2, true" ]
        , "//"
        , "// New 2014.8.7: arg *count* when byindex=true"
        , "//"
        , [ del( [2,4,6,8], 1, true), [2,6,8], "[2,4,6,8], 1, true" ]
        , [ del( [2,4,6,8], 1, true,2), [2,8], "[2,4,6,8], 1, true,2"
                   ,["//","del 2 tiems"]]
        , [ del( [2,4,6,8], 2, true,2), [2,4], "[2,4,6,8], 2, true,2"
            , ["//","del only 1 'cos it's in the end."] ]
        , [ del( [], 1, true), [], "[], 1, true" ]
        ]
        , mode=mode, opt=opt
        )
    );       

//========================================================

function dels(a1,a2)=
(
    let( a2=isarr(a2)?a2:[a2] )
	[for(x=a1) if (index(a2,x)==undef) x] 
);

    dels=["dels", "a1,a2", "array", "Array",
     " Given arrays a1, a2 delete all items in a2 from a1.
    ;;
    ;;   dels( [2,4,6,8], [4,6] )= [2, 8]
    ;;
    ;; Ref: arrItcp
    "];

    function dels_test( mode=MODE, opt=[] )=
    (
        doctest( dels,
        [ 
          [ dels( [2,4,4,8], 4), [2,8], "[2,4,4,8], 4" ]
        , [ dels( [2,4,6,8], [4,6]), [2,8], "[2,4,6,8], [4,6]" ]
        ]
        , mode=mode, opt=opt
        )
    );

//==================================================================

function deeprep(arr, indices, new, _debug=[])=
    let( i = indices[0] 
//       ,_debug= _fmth(concat( _debug,
//                [ "<br/>",""
//                , "arr",arr
//                , "indices",indices
//                , "i",i
//                ]
//             )) 
    )         
    ( 
     indices==[]? arr
     :len(indices)>1
     //? _debug 
     ? replace( arr, i, deeprep( get(arr,i), slice(indices,1), new, _debug ) )
     : len(indices)==1
     //? _debug
     ? replace( arr, i, new )     
     : new
    );     
     
    deeprep=["deeprep","arr,indices,new", "arr","Array"
    , "Given arr, and list of indices ([i,j,k...], and a value, new, replace 
    ;; the item arr[i][j][k]... with new. Items in indices could be negative. 
    ;; 
    ;; arr=
    ;;  [0,1,                      ,3]
    ;;        [20,            ,22]
    ;;             [40,41,42]
    ;;
    ;;  deeprep( arr, [2,1,-1], 99 ):
    ;; 
    ;;  [0,1,                      ,3]
    ;;        [20,            ,22]
    ;;             [40,41,99]
    ;;
    ;;   deeprep( [0,1,[20,21,22],3],[-2,1],99 )= [0, 1, [20, 99, 22], 3]
    ;;"
    ];
         
    function deeprep_test( mode=MODE, opt=[] )=
    (     
        doctest( deeprep, 
        [
          [ deeprep([0,1,2,3],indices=[2], new=99 )
                , [0,1,99,3]
                ,"[0,1,2,3],[2],99"
          ]
        , [ deeprep([0,1,2,3],indices=[-1], new=99 )
                , [0,1,2, 99]
                ,"[0,1,2,3],[-1],99"
          ]
        , [ deeprep([0,1,[20,21,22],3],indices=[-2,1], new=99 )
                , [0,1,[20,99,22],3]
                ,"[0,1,[20,21,22],3],[-2,1],99"
          ]
        , [ deeprep([0,1,[20,[40,41,42],22],3]
                                ,indices=[2,1,-1], new=99 
                         )
                , [0,1,[20,[40,41,99],22],3 ]
                ,"[0,1,[20,[40,41,42],22],3], [2,1,-1],99"
                , ["nl",true]
          ]
        , [ deeprep([0,1,[20,[40,41,42],22],3]
                                ,indices=[2,-1], new=99 
                         )
                , [0,1,[20,[40,41,42],99],3]
                ,"[0,1,[20,[40,41,42],22],3], [2,-1],99"
                , ["nl",true]
          ]
        , [ deeprep([0,1,2,3],indices=[], new=99)
                , [0,1,2,3]
                ,"[0,1,2,3],[],99"
          ]        
        ]
        , mode=mode, opt=opt )
    );   

//========================================================
//arr= [[[1,2],[2,3]], [[1,2],[2,3] ]];
//echo(flatten_arr = [for(i=range(arr),x=arr[i])each x] );

function flatten(a, d=undef)=
( 
  d==undef || d>0?
  [for(x=a) 
      for( b= (str(x)==x) || len(x)==undef? x:flatten(x, d-1)  ) 
      b ]
  :a 
);

    flatten=["flatten", "a,d=undef", "arr", "Core, array",
    ,"Flatten array a. d: depth. If d=undef (default), 
    ;; flatten all; if d=0, return a.
    ;;
    ;; Note: other approaches:
    ;;
    ;; [ for(i=range(a),x=a[i]) x] 
    
    "];

    function flatten_test( mode=MODE, opt=[] )=
    (
        let( a=[ [[1,2],[2,3]], [[[1,2],[2,3]]] ]
           , b=[ ["c",[2,3]], [["cc",[2,3]]] ]
           , c=[ ["abc", ["def","g"]],"h"]
           )
        
        doctest( flatten,
        [
          "var a",""
        , [ flatten(a), [1, 2, 2, 3, 1, 2, 2, 3], "a" ]
        , [ flatten(a,0), [ [[1,2],[2,3]], [[[1,2],[2,3]]] ] , "a,d=0"]
        , [ flatten(a,1), [[1, 2], [2, 3], [[1, 2], [2, 3]]] , "a,d=1"]
        , [ flatten(a,2), [1, 2, 2, 3, [1, 2], [2, 3]] , "a,d=2"]
        , "","var b",""
        , [ flatten(b,1), ["c", [2, 3], [ "cc", [2, 3]]] , "b,d=1"]
        , [ flatten(b,2), ["c", 2, 3, "cc", [2, 3]] , "b,d=2"]
        , "", "var c",""
        , [ flatten(c), [ "abc", "def","g","h"] , "c"]
        , "// Note that flatten( [a,b,c],d=1 ) = concat( a,b,c )"
        , "// when all a,b,c are arrays."
        ]
        ,mode=mode, opt=opt, scope=["a",a,"b",b,"c",c]
        )    
    );
          
//========================================================
function getcol(mm,i=0)=
(
	[ for(row=mm) get(row,i) ]
);

    getcol=[ "getcol","mm,i=0", "array", "Array, Index",
     " Given a matrix and index i, return a 1-d array containing
    ;; item i of each matrix row (i.e., the column i of the multi
    ;; matrix. The index i could be negative for reversed indexing.
    "];

    function getcol_test( mode=MODE, opt=[] )=
    (
        let( mm= [[1,2,3],[3,4,5]]
           )
           
        doctest( getcol,
        [
          "var mm"
        , [ getcol(mm), [1,3],"mm" ]
        , [ getcol(mm,2), [3,5], "mm,2" ]
        , [ getcol(mm,-2), [2,4], "mm,-2" ]
        ]
        , mode=mode, opt=opt, scope=["mm", mm ]
        )
    );
    
//========================================================

    
function insert(o,x,i)=
(
    isarr(o)?
    ( i==undef||i>(len(o)-1)? concat(o,[x])
      :let(i=fidx(o,i))
        joinarr( [ for(j=range(o)) i==j?[ x, o[j]]:[o[j]]
               ])
    ):( i==undef||i>(len(o)-1)? str(o,x)
      :let(i=fidx(o,i))
       join( [ for(j=range(o)) i==j? str(x, o[j]):o[j]
               ])
    )    
);

//function insert(arr,x,i)=
//(
//    i==undef||i>(len(arr)-1)? concat(arr,[x])
//    :let(i=fidx(arr,i))
//      joinarr( [ for(j=range(arr)) i==j?[ x, arr[j]]:[arr[j]]
//             ])
//);
     
    insert= ["insert", "o,x,i", "arr", "Array/String",
      "Insert x to the position i. i could be negative.
    ;; If i larger than the last index, or i is undef,
    ;; append x to o. o could be array or string."
    ];
           
    function insert_test( mode=MODE, opt=[] )=(
        doctest( insert, 
        [ [ insert([2,3,4],5,1), [2,5,3,4] , "[2,3,4],5,1"]
        , [ insert([2,3,4],5,-1), [2,3,5,4], "[2,3,4],5,-1"]
        , [ insert([2,3,4],5,0), [5,2,3,4], "[2,3,4],5,0"]
        , [ insert([2,3,4],5,9), [2,3,4,5], "[2,3,4],5,9"]
        , [ insert([2,3,4],5), [2,3,4,5], "[2,3,4],5"]
        , "","When str (2015.11.13):",""
        , [ insert( "0123","xx",2), "01xx23", "'0123','xx',2"]
        , [ insert( "0123","xx",0), "xx0123", "'0123','xx',0"]
        , [ insert( "0123","xx"), "0123xx", "'0123','xx'"]
        , [ insert( "0123","xx",-3), "0xx123", "'0123','xx',-3"]
        ],mode=mode, opt=opt
        )
    );
    //echo( insert_test( 112 )[1] );
    
    
//========================================================
    
function join( arr, sp="", _ret="", _i=0)=
(
    _i<len(arr)? 
        join( arr, sp=sp
            , _ret= _i==0?arr[0]:str(_ret,sp,arr[_i]) //str(_ret,_i==0?"":sp,arr[_i])
            , _i=_i+1 )
    :_ret
);
    
//function join(arr, sp="", _i_=0)=
////let(L=len(arr)) 
//(
//	_i_<len(arr)?
//	str(
//		arr[ _i_ ]
//		, _i_<len(arr)-1?sp:""
//		,join(arr, sp, _i_+1)
//	):""
//
////	L==0?"":str( arr[0], L==1?"":sp, join(slice(arr,1),sp=sp) )
//
//); 

    join=[ "join", "arr, sp=\"\"" , "string","Array",
    " Join items of arr into a string, separated by sp.
    "];

    function join_test( mode=MODE, opt=[] )=(
        doctest( join, 
        [ [ join([2,3,4]),      "234" , "[2,3,4]"]
        , [ join([2,3,4], "|"), "2|3|4" , "[2,3,4], '|'"]
        , [ join([2,[5,1],"a"], "/"), "2/[5, 1]/a" , "[2,[5,1],'a'], '/'"]
        , [ join([], "|"), "" , "[], '|'"]
        ],mode=mode, opt=opt
        )
    );
    //echo( join_test(112)[1] );

//========================================================

function joinarr(arr, _rtn=[], _i=0)= 
(
	isarr(arr)?
    (_i>len(arr)-1? _rtn
	: joinarr( arr, concat( _rtn, arr[_i]), _i+1)   
    )
    :undef
); 

    joinarr=[ "joinarr", "arr" , "array","Array",
    " Given an array(arr), concat items in arr. See the difference below:
    ;; 
    ;;  concat( [1,2], [3,4] ) => [1,2,3,4] 
    ;;  joinarr( [ [1,2],[3,4] ] ) => [1,2,3,4] 
    ;;  join( [ [1,2],[3,4] ] ) => '[1,2],[3,4]' 
    "];

    function joinarr_test( mode=MODE, opt=[] )=(

        doctest( joinarr, 
        [ 
          [ joinarr([[2,3],[4,5]]), [2,3,4,5], "[[2,3],[4,5]]" ]
        , ""
        , "// 2015.2: you can also use the new run(): "
        , ""
        , [ run("concat",[[2,3],[4,5]]), [2,3,4,5],[[2,3],[4,5]],
                ["myfunc", "run('concat',[[2,3],[4,5]])" ] ]
        , ""
        , "// 2015.4: or the new flatten(a,d=1): "
        , ""
        , [ flatten([[2,3],[4,5]]), [2,3,4,5], [[2,3],[4,5]],
                ["myfunc", "flatten{_}" ] ]    
        ]
        , mode=mode, opt=opt
        )
    );

//echo( joinarr_test(mode=12)[1]);    


//========================================================
function last(o, i=1)= o[len(o)-i]; 
 
     last=[ "last", "o", "item", "Index, String, Array" ,
    " Given a str or arr (o), return the last item of o.
    "];
    function last_test( mode=MODE, opt=[] )=(//==================
        doctest( last, 
         [ 
           [ last([20,30,40,50]), 50, [20,30,40,50] ]
         , [ last([20,30,40,50],2), 40, "[20,30,40,50],2" ]
         , [ last([]), undef,[] ]
         , [ last("abcdef"), "f", "'abcdef'" ]
         , [ last(""), undef,"''" ]
         ]
         , mode=mode, opt=opt
        )
    );

function next(o, x, d=1)= //echo(_blue(next([1,2,3],2))) = 3
                            //echo(_blue(next([1,2,3],3))) = 1
(
   o[( idx(o,x) +d)%len(o) ] 
   
   //let( i = idx(arr,x) )
   //i>=0? arr[(i+d)%len(arr) ] 
   // : undef
); 

   next=[ "next", "o, x,d=1", "item", "Array",
    " Return the item next to x by distance 1. 
    "];

    function next_test( mode=MODE, opt=[] )=
    ( 
        let( arr=[1,2,3] )
        doctest( next,
        [
          "var arr"
        , [next(arr,1), 2, "arr,1" ]
        , [next(arr,3), 1, "arr,3" ]
        , [next(arr,3,2), 2, "arr,3,2" ]
        , [next(arr,3,5), 2, "arr,3,5" ]
        , [next("abcde","c"), "d", "'abcde','c'"]    
        ],mode=mode, opt=opt, scope=["arr",arr]
        )
    );
//echo(13%5);
//echo(_blue(prev([1,2,3],1)), "want 3");  // 3
//echo(_blue(prev([1,2,3],1,4)), "want 3");  // 3
//echo(_blue(prev([1,2,3],2)), "want 1");  // 1
//echo(_blue(prev([1,2,3],3)), "want 2");  // 2
//echo(_blue(prev([1,2,3],3,2)), "want 1"); // 1
//echo(_blue(prev([1,2,3],3,5)), "want 1"); // 1
//echo("---");
//echo(_blue(next([1,2,3],1)), "want 2"); //2
//echo(_blue(next([1,2,3],2)), "want 3"); //3 
//echo(_blue(next([1,2,3],3)), "want 1"); //1
//echo(_blue(next([1,2,3],3,2)), "want 2"); //2
//echo(_blue(next([1,2,3],3,5)), "want 2"); //2

//========================================================

function combine(a,b, _rtn=[],_i=0)=  // combine array a,b 2015.4.25
(  
  let( _rtn= !a?b: !b?a
             : _rtn==[]?a
             : len( [ for(x=a) if(isequal(b[_i],x)) 1] )==0 // if b[_i] not in a
               ? concat( _rtn, [b[_i]] )  
               : _rtn
     )         
  _i>=len(b)-1 ? _rtn
  : combine(a,b,_rtn,_i+1)
);

    combine=["combine", "a,b", "arr","Array"
    ,"Combine arrays a,b and no repeat in the result"
    ,"join,slice,split"
    ];

    function combine_test( mode=MODE, opt=[] )=
    (
        doctest( combine,
        [
          [ combine( [2,3,4],[3,5,7] ), [2,3,4,5,7], "[2,3,4],[3,5,7]" ]
        , [ combine( [],[3,5,7] ), [3,5,7], "[],[3,5,7]" ]
        , [ combine( [2,3,4],[] ), [2,3,4], "[2,3,4],[]" ]
        ]
        , mode=mode, opt=opt
        )
    );
    //echo(combine_test(112)[1]);

//function combine(a,b, _rtn=[],_i=0)=  // combine array a,b 2015.4.25
//(  
//  let( _rtn= !a?b: !b?a
//             : _rtn==[]?a
//             : len( [ for(x=a) if(isequal(b[_i],x)) 1] )==0 // if b[_i] not in a
//               ? concat( _rtn, [b[_i]] )  
//               : _rtn
//     )         
//  _i>=len(b)-1 ? _rtn
//  : combine(a,b,_rtn,_i+1)
//);
//
//    combine=["combine", "a,b", "arr","Array"
//    ,"Combine arrays a,b and no repeat in the result"
//    ,"join,slice,split"
//    ];
//
//    function combine_test( mode=MODE, opt=[] )=
//    (
//        doctest( combine,
//        [
//          [ combine( [2,3,4],[3,5,7] ), [2,3,4,5,7], "[2,3,4],[3,5,7]" ]
//        , [ combine( [],[3,5,7] ), [3,5,7], "[],[3,5,7]" ]
//        , [ combine( [2,3,4],[] ), [2,3,4], "[2,3,4],[]" ]
//        ]
//        , mode=mode, opt=opt
//        )
//    );
//    //echo(combine_test(112)[1]); 
  
//========================================================
function permute( arr ) = //, n, _i, _debug="" )= 
(   // Using the "each" feature of 16.10.4 snapshot, much easier and faster
    // The following works for n=len(arr) or n is undef 
    // but we are trying to make n work. 16bd <========= TODO
    len(arr)>2
    ? [ for( i= range(arr) )         
         let ( tmps= permute( del(arr,i, byindex=true) ))
          each [ for (a=tmps) concat([arr[i]], a)]
      ] 
    : [ arr, [arr[1],arr[0]] ]        
    
);    

//echo( permute( [3,4] ) );    

/*function permute( arr, want, _rtn=[], _debug="" )=
(    
    let( want=want==undef?len(arr):want
        ,_rtn= [ for(x = _rtn) concat([arr[0]],x)]
        , arr=slice(arr,1)
       , _debug= str( _debug
                    , "<hr/><b>arr= </b>", arr
                    , "<br/><b>want= </b>", want
                    , "<br/><b>_rtn= </b>", _rtn
                    )
       )
    //want==1? arr
    //:want==2? permute2(arr)
    //: want>len(arr)? _debug //_rtn
    len(arr)==3? 
        let( base= permute2(arr) )
        [ for(x=base) concat( _rtn, x) ]
    : permute( arr, want, _rtn, _debug )
);           
*/
    permute=["permute","arr","array","Array, Math",
     " Given an array, return its permutation.
    ;;
    ;; [2,4,6] => [[2,4,6],[2,6,4],[4,2,6],[4,6,2],[6,2,4],[6,4,2]]
    "];

    function permute_test( mode=MODE, opt=[] )=
    (
        doctest( permute
        ,[
          [permute(range(2))
            , [[0,1],[1,0]]
            ,"range(2)", ["ml",3]
          ]
          
        , [permute([2,4,6])
            , [[2,4,6],[2,6,4],[4,2,6],[4,6,2],[6,2,4],[6,4,2]]
            ,[2,4,6], ["ml",3]
          ]
          
        ,  [ permute(range(3)),[[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]], "range(3)", ["ml",1]]
      
        , [permute([1,2,3,4]),
            [ [1, 2, 3, 4], [1, 2, 4, 3], [1, 3, 2, 4]
            , [1, 3, 4, 2], [1, 4, 2, 3], [1, 4, 3, 2]
            , [2, 1, 3, 4], [2, 1, 4, 3], [2, 3, 1, 4]
            , [2, 3, 4, 1], [2, 4, 1, 3], [2, 4, 3, 1]
            , [3, 1, 2, 4], [3, 1, 4, 2], [3, 2, 1, 4]
            , [3, 2, 4, 1], [3, 4, 1, 2], [3, 4, 2, 1]
            , [4, 1, 2, 3], [4, 1, 3, 2], [4, 2, 1, 3]
            , [4, 2, 3, 1], [4, 3, 1, 2], [4, 3, 2, 1]
            ]
            ,[1,2,3,4] 
            , ["ml",1]
           ]                    
        /*, [permute([1,2,3],2),
            [[2, 3], [3, 2], [1, 3], [3, 1], [1, 2], [2, 1]]
            ,"[1,2,3],2"
            , ["ml",1]
           ]
//        , [permute(range(5),2),
//            [[2, 3], [3, 2], [1, 3], [3, 1], [1, 2], [2, 1]]
//            ,"range(5),2"
//            , ["ml",1]
//           ]
        , [permute([1,2,3,4],2),
            [ ]
            ,"[1,2,3,4],2"
            , ["ml",1]
           ]
        , [permute([1,2,3,4],3),
            [ [1, 3, 4], [1, 4, 3], [1, 2, 4], [1, 4, 2], [1, 2, 3], [1, 3, 2]
            , [2, 3, 4], [2, 4, 3], [2, 1, 4], [2, 4, 1], [2, 1, 3], [2, 3, 1]
            , [3, 2, 4], [3, 4, 2], [3, 1, 4], [3, 4, 1], [3, 1, 2], [3, 2, 1]
            , [4, 2, 3], [4, 3, 2], [4, 1, 3], [4, 3, 1], [4, 1, 2], [4, 2, 1]
            ]
            ,"[1,2,3,4],3"
            , ["ml",1]
           ]           
           */
           ], mode=mode, opt=opt
        )
    );
  
function permute2( arr )=
(
  [for( i=[0:len(arr)-2], j=i+1 ) 
     for(k=[j:len(arr)-1]) 
        [arr[i],arr[k]]
  ]
//  [for( i=[0:len(arr)-1]
//      , x=slice(arr,i+1)
//      ) 
//    if(arr[i]!=x)[arr[i],x]
//  ]  
);

    permute2=["permute2","arr","any",  "Array",
    "
     Given an array, return its 'pair' permutation --- i.e., only 2 numbers
    ;; each item: [[1,2],[1,3]...]
    "  
    ];

    function permute2_test( mode=MODE, opt=[] )=
    (
        doctest( permute2, 
        [
          [permute2([1,2,3]), [[1,2],[1,3],[2,3]], "[1,2,3]"]
        , [permute2([2,3,4,5]), [[2,3],[2,4],[2,5],[3,4],[3,5],[4,5]], "[2,3,4,5]"]
        ], mode=mode, opt=opt
        )
    );
      
function permutes(arrs, _i, _lv=-1)=
( /*
    permutes( [[1,2,3],5,[0,4]] )
    => [ [1,5,0],[1,5,4], [2,5,0],[2,5,4], [3,5,0],[3,5,4] ]
    size: 3*2=6
    
  */
  
  let(lv=_lv)
  //echo( log_b( str("permutes(), arrs=", arrs), lv))   
  _i==undef?
    ( len(arrs)==1? [for(a=arrs[0])[a]]
    : let( arrs= [for(a=arrs) len(a)==undef?[a]:a]
         )
       //echo( log_( _s("init, _arrs={_}, _i={_}", [arrs,_i] )))
       permutes( arrs=arrs, _i=0, _lv=lv+1)
    ) 
    
  : len(arrs)==2? /// [[1,2],[3,4]] ==> [ [1,3],[1,4],[2,3],[2,4] ]
    [ for( a=arrs[0]) for (b=arrs[1]) [a,b] ] 
                                      
  : [ for(a=arrs[0])
       let(bs= permutes( arrs=slice(arrs,1)
                   , _i=0, _lv=lv+1)
           )
       each [for(b=bs) concat( [a], b)]       
    ]                   

);

    permutes=["permutes", "arrs", "arr", "Array",
    " permutes( [[1,2,3],5,[0,4]] )
    ;; => [ [1,5,0],[1,5,4], [2,5,0],[2,5,4], [3,5,0],[3,5,4] ]
    ;; size: 3*1*2=6
    ;;
    ;; date:  2018.10.7
    ;; Note.1: Non-array is allowed in the *arrs*: [[2,3,4],5]
    ;; Note.2: permute([1,2]) != permutes([[1,2],[1,2]) (see usage)
    ;; Note.3: both permute and permutes are NOT tail-recursive code
    ;; 
    ;; Usage: eval.scad/func() when scope has multiple arrays:['x',[1,2],'y',[3,4]]
    "];

    function permutes_test( mode=MODE, opt=[] )=
    (
        doctest( permutes, 
        [
          [ permutes( [[1,2,3],[5,7]]) 
          , [ [1,5],[1,7],[2,5],[2,7],[3,5],[3,7]]  
          , "[[1,2,3],[5,7]]"
          , ["nl",1]
          ]
        ,  [ permutes( [[1,2,3],[5,7],[6,9]] )
          , [ [1,5,6],[1,5,9],[1,7,6],[1,7,9]
            , [2,5,6],[2,5,9],[2,7,6],[2,7,9]
            , [3,5,6],[3,5,9],[3,7,6],[3,7,9]
            ]
          , "[[1,2,3],[5,7],[6,9]]"
          , ["ml",6]
          ]          
        , ""
        , "// Non-array is allowed in the arg:"
        , ""  
        , [ permutes( [[1,2,3],4,[5,7]]) //,[6,9]] )
          , [ [1,4,5],[1,4,7],[2,4,5],[2,4,7],[3,4,5],[3,4,7]]  
          , "[[1,2,3],4,[5,7]]"
          , ["nl",1]
          ]
        , ""
        , [ permutes( [[1,2,3]]) //,[6,9]] )
          , [ [1],[2],[3]]  
          , "[[1,2,3]]"
          ]
        , [ permutes( [5]) //,[6,9]] )
          , [[5]]  
          , "[5]"
          ]
        , ""
        , _red("Note:")
        
        ///>>> permute([1,2])= [[1, 2], [2, 1]]
        ///>>> permutes([[1,2],[1,2]])= [[1, 1], [1, 2], [2, 1], [2, 2]]			
        
        ,""
        , str( ">>> <b>permute</b>(<u>[1,2]</u>)= ", _fmt(permute([1,2])) )
        , str( ">>> <b>permutes</b>(<u>[[1,2],[1,2]]</u>)= ", _fmt(permutes([[1,2],[1,2]])) )
        
          
        ], mode=mode, opt=opt
        )
    );
                 

function ppEach( arr, x, _I_=0)=
(
	[ for(a=arr) concat([x],a) ]
);

    ppEach=[ "ppEach", "arr,x", "arr", "Array",
     " Given an array of arrays (arr) and an item (x), prepend x to 
    ;; each array in arr.
    ;;
    ;; 20150206:Put back  (Usage: permute)
    "];

    function ppEach_test( mode=MODE, opt=[] )=
    ( 
        let( arr=[[1,2],[3,4],[5,6]] )
        doctest( ppEach,
        [
            [ppEach(arr,9), [[9,1,2],[9,3,4],[9,5,6]], "arr,9" ]
            ,[ppEach([[1,2],3],9), [[9,1,2],[9,3]], "[[1,2],3],9" ]
        ],mode=mode, opt=opt, scope=["arr",arr]
        )
    );
  
function prev(arr, x, d=1)= // echo(_blue(next([1,2,3],2))) = 1                
(                           // echo(_blue(next([1,2,3],1))) = 3
   //o[ (len(o)+ (x%len(o))%len(o)]
   //let( i = idx(arr,x) )
   //i>=0? arr[ (len(arr)+ i -d%len(arr))%len(arr) ]: undef
   arr[ (len(arr)+ idx(arr,x) -d%len(arr))%len(arr) ]
);  
   prev=[ "prev", "o, x,d=1", "item", "Array",
    " Return the item prior to x by distance 1. 
    "];

    function prev_test( mode=MODE, opt=[] )=
    ( 
        let( arr=[1,2,3] )
        doctest( prev,
        [
          "var arr"
        , [prev(arr,1), 3, "arr,1" ]
        , [prev(arr,1,4), 3, "arr,1,4" ]
        , [prev(arr,3,2), 1, "arr,3,2" ]
        , [prev(arr,3), 2, "arr,3" ]
        , [prev(arr,3,2), 1, "arr,3," ]
        , [prev(arr,3,5), 1, "arr,3,5" ]
        , [prev("abcde","c"), "b", "'abcde','c'"]    
            
        ],mode=mode, opt=opt, scope=["arr",arr]
        )
    );

//========================================================
   
function quicksort(arr, _i=0) = 
(   //let( arr = [ for(x=arr) str(x) ] )
    !arr ? [] 
           : let(   pivot   = arr[floor(len(arr)/2)],
                    lesser  = [ for (y = arr) if (scomp(y,"<",pivot)) y ],
                    equal   = [ for (y = arr) if (isequal(y,pivot)) y ],
                    //equal   = [ for (y = arr) if (scomp(y,"=",pivot)) y ],
                    greater = [ for (y = arr) if (scomp(y,">",pivot)) y ]
                ) concat(
                      quicksort(lesser, lesser==[]?_i:_i+1)
                    , equal
                    , quicksort(greater, greater==[]?_i:_i+1)
                )  
); 
   //[[1, "b"], [2, "d"], [4, "c"], [6, "D"]] got: 
   //[[2, "d"], [6, "D"], [1, "b"], [4, "c"], [2, "d"], [2, "d"], [4, "c"], [2, "d"], [6, "D"], [4, "c"]]
                    
    quicksort=["quicksort","arr","arr","Math",
    "Sort the arr. Each arr item is converted to str before comparison,
    ;; allowing for sorting of array of arrays, which is useful when
    ;; sorting a series of pts on their x values.
    "];
    // https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/List_Comprehensions#Quicksort    
                           
    function quicksort_test( mode=MODE, opt=[] )=
    (
        doctest( quicksort,
        [
          [ quicksort([6,1,8,9,3,2]),[1,2,3,6,8,9], [6,1,8,9,3,2] ]
        , [ quicksort(["d","D","b","c"]),["D", "b", "c", "d"],["d","D","b","c"] ]
        , [ quicksort([[2,"d"],[6,"D"],[1,"b"],[4,"c"]]),[[1,"b"],[2,"d"],[4,"c"],[6,"D"]],[[2,"d"],[6,"D"],[1,"b"],[4,"c"]] ]
        ]
        , mode=mode, opt=opt
        )
    );    

//echo( quicksort_test( 112 )[1] );
                    

//========================================================
function reverse(o)=
(
	isarr(o)
	? [ for(i=range(o)) o[len(o)-1-i] ]
	: join( [ for(i=range(o)) o[len(o)-1-i] ], "" )
);

    reverse=[ "reverse", "o", "str|arr", "Array, String",
    "
      Reverse a string or array.
    "];

    function reverse_test( mode=MODE, opt=[] )=
    (
       doctest( reverse,
       [ [ reverse([2,3,4]), [4,3,2], [2,3,4] ]
       , [ reverse([2]), [2], [2] ]
       , [ reverse( "234" ), "432", "'234'"  ]
       , [ reverse( "2" ), "2", "'2'" ]
       , [ reverse( "" ), "", "'"  ]
       ], mode=mode, opt=opt
       )
    );
//reverse_test( ["mode",22] );
//doc(reverse);                


//========================================================

function roll(arr,count=1)=
 let(L= len(arr)
	,count= count<0?L+count:count)
(
 	count==0 || count>L-1? arr
	:concat( [for(i= [L-count:L-1]) arr[i]]
		   , [for(i= [0:L-count-1]) arr[i]]
		   )
); 
    
    roll=["roll", "arr,count=1", "arr", "Array"
    ," Given an array (arr) and an int (count), roll arr items
    ;; as shown below:
    ;;
    ;; roll( [0,1,2,3] )   => [0,1,2],[3] => [3],[0,1,2] => [3,0,1,2]
    ;; roll( [0,1,2,3],-1) => [0],[1,2,3] => [1,2,3],[0] => [1,2,3,0] 
    ;; roll( [0,1,2,3], 2) => [0,1],[2,3] => [2,3],[0,1] => [2,3,0,1]
    ;;
    ;; Compare range w/cycle, which (1) returns indices (2) doesn't 
    ;; maintain array size:
    ;;
    ;; |> obj = [10, 11, 12, 13]
    ;; |> range( obj,cycle=1 )= [0, 1, 2, 3, 0]
    ;; |> range( obj,cycle=2 )= [0, 1, 2, 3, 0, 1]
    ;; |> range( obj,cycle=-1 )= [3, 0, 1, 2, 3]
    ;; |> range( obj,cycle=-2 )= [2, 3, 0, 1, 2, 3]

    "];
               
    function roll_test( mode=MODE, opt=[] )=
    (
        doctest( roll,
        [
          [ roll([0,1,2,3]), [3,0,1,2] , "[0,1,2,3]"]
        , [ roll([0,1,2,3],-1), [1,2,3,0] , "[0,1,2,3],-1"]
        , [ roll([0,1,2,3], 2), [2,3,0,1] , "[0,1,2,3], 2"]
        , [ roll([0,1,2,3,4,5], 2), [4,5,0,1,2,3] , "[0,1,2,3,4,5], 2"]
        , [ roll([0,1,2,3],0), [0,1,2,3] , "[0,1,2,3],0"]
        ], mode=mode, opt=opt
        )
    );

function sd(arr)= // sample standard deviation
(
  sqrt(sum([for(x=arr) pow(x-avg(arr),2)/(len(arr)-1) ])) 
);

//arr=[727.7, 1086.5,1091.0,1361.3,1490.5,1956.1];
//echo(sd = sd(arr)); // = 420.96 //https://en.wikipedia.org/wiki/Standard_deviation

    sd= [ "sd", "arr", "n", "Array",
    "
     Sample Standard Deviation
    "
    //https://en.wikipedia.org/wiki/Standard_deviation
    ];

    function sd_test( mode=MODE, opt=[] )=
    (
        doctest( sd, 
        [
          [ sd([727.7, 1086.5,1091.0,1361.3,1490.5,1956.1]), 420.962
            , "[727.7, 1086.5,1091.0,1361.3,1490.5,1956.1]"
            , ["asstr",1] 
          ]
        ], mode=mode, opt=opt
        )
    );


function share(a,b)= //2018.5.23
(
   [for(x=a) if(idx(b,x)!=undef) x]  
);           
    share= [ "share", "a,b", "arr", "Array",
    "
     Return an intersection of array a and b
    "
    ];

    function share_test( mode=MODE, opt=[] )=
    (
        doctest( share, 
        [
          [ share([2,3,4],[3,5,6]), [3], "[2,3,4],[3,5,6]" ]
        , [ share(["2","3","4"],["3","2","6"]), ["2","3"], "['2','3','4'],['3','2','6']" ]
        ], mode=mode, opt=opt
        )
    );
    //shrink_test( ["mode",22] );


//========================================================

function shrink( o )= slice(o,1);

    shrink= [ "shrink", "o,i=1", "str|arr", "String, Array",
    "
     Return a str|arr short (on the left side) than o by one.
    "
    ];

    function shrink_test( mode=MODE, opt=[] )=
    (
        doctest( shrink, 
        [
          [ shrink([2,3,4]), [3,4], [2,3,4] ]
        , [ shrink([2]), [], [2] ]
        , [ shrink( "234" ), "34",  "'234'" ]
        , [ shrink( "2" ), "", "'2'" ]
        , [ shrink( "" ), "", "'" ]
        ], mode=mode, opt=opt
        )
    );
    //shrink_test( ["mode",22] );
//========================================================
function shuffle(arr, _i=rand(1))=
(
	len(arr)>0
    ? concat( 
         [ pick(arr, floor( len(arr)*_i))[0] ]
       , shuffle( pick(arr, floor( len(arr)*_i))[1], _i=rand(1) )
       )
    :[]
);    


    shuffle=["shuffle", "arr", "array", "Array",
    " Given an array (arr), return a new one with all items randomly shuffled.
    "];
    function shuffle_test( mode=MODE, opt=[] )=
    (
        let( arr = range(6)
           , arr2= [ [-1,-2], [2,3],[5,7], [9,10]] 
           )
           
        doctest( shuffle,
        [
            str( "shuffle(arr)= ", shuffle(arr) )
            ,str( "shuffle(arr)= ", shuffle(arr) )
            ,str( "shuffle(arr)= ", shuffle(arr) )
            ,str( "shuffle(arr)= ", shuffle(arr) )
            ,str( "shuffle(arr2)= ", shuffle(arr2) )
            ,str( "shuffle(arr2)= ", shuffle(arr2) )
            ,str( "shuffle(arr2)= ", shuffle(arr2) )
        ]
        , mode=mode, opt=opt, scope=["arr", arr, "arr2", arr2]
        )
    );
    //shuffle_test(); 


////========================================================
//sortwithidx=["sortwithidx","arr of arr", "arr", "Array",
//"

//========================================================
function sort( arr, i, j )=
(
    let(_i = countArr(arr)>0 && i==undef? 0:i 
       , i= fidx(arr,_i) 
       ) 
    !arr ? []
    : len(arr)==1? arr 
    : i==undef?
        let( pivot   = arr[floor(len(arr)/2)]
           , lesser  = [ for (y = arr) if(y<pivot) y ]
           , equal   = [ for (y = arr) if(isequal(y,pivot)) y ]
           , greater = [ for (y = arr) if(y>pivot) y ]
           ) concat(
                sort(lesser), equal, sort(greater)
            )
   : j==undef?
        let(
          pivot   = arr[floor(len(arr)/2)][i]
        , lesser  = [ for (y = arr) if(y[i]<pivot) y ]
        , equal   = [ for (y = arr) if(y[i]==pivot)y ] 
                   // Avoid using: 
                   //   if(isequal(y[i],pivot)) y ]
                   // It somehow creates repeated items in 
                   // getPairingData_sorted_afips_demo() 
        , greater = [ for (y = arr) if(y[i]>pivot) y ]
        ) concat(
            sort(lesser,i), equal, sort(greater,i)
        )  
  
     /* In case of getPairingData_demo(), This sort(arr,i,j) 
        sometime works, sometime not: 
        
            sort( arr, 0, 1)==>
                [ [0, 1, 0, [0.623846, -4.78822, 0.725188]]
                , [0, 2, 0, [1.80406, -2.92427, -0.450729]]
           ==>  , [120, 2, 1, [4.00388, -1.68256, -1.05812]]
           ==>  , [120, 1, 1, [6.49004, -1.47699, -0.894529]]
                , [240, 1, 2, [0.422682, 0.847507, -3.2995]]
                , [240, 2, 2, [1.72862, -0.810872, -1.95999]]
                ]
     */   
     :let( j = fidx( arr, j)
        , sorti = sort(arr,i)
        , packsame = [ for(k=range(sorti))
                         let( key = sorti[k][i] )
                           [ for(x= sorti) if(x[i]==key) x]
                     ]          
        )
        //sorti //packsame
        joinarr( [ for(k= range(packsame) )
                    let( x = packsame[k] )
                    if( x!=packsame[k-1]) sort(x,j)
                 ] )                   
        
);

    sort=["sort","arr,/i,j/","Array","Array"
    ,"Sort an array of arrays based on indices i and then j"
    ];
                        
    function sort_test( mode=MODE, opt=[] )=
    (
        let( arr=[[0, 1, 0]
                , [120, 1, 1]
                , [240, 1, 2]
                , [0, 2, 0]
                , [60, 2, 1]
                , [120, 2, 2]
                , [180, 2, 3]
                , [240, 2, 4]
                ] 
           )
                        
        doctest( sort,
        [
          "arr ="
        , arrprn(arr)
        , [ sort([6,1,8,9,3,2]),[1,2,3,6,8,9], [6,1,8,9,3,2] ]
        , [ sort(["d","D","b","c"]),["D", "b", "c", "d"],["d","D","b","c"] ]
        , [ sort([[2,"d"],[6,"D"],[1,"b"],[4,"c"]])
                ,[[1,"b"],[2,"d"],[4,"c"],[6,"D"]]
                ,[[2,"d"],[6,"D"],[1,"b"],[4,"c"]], ["nl",true] ]
        , [ sort([[2,"d"],[6,"D"],[1,"b"],[4,"c"]],1)
                ,[[6, "D"], [1, "b"], [4, "c"], [2, "d"]]
                ,"[[2,'d'],[6,'D'],[1,'b'],[4,'c']],1", ["nl",true] ]
        , ""
        , "// If contains arr, set i=0 and sort by the first item:"
        , ""                
        , [ sort([[6,"D"],[1,"c"],[1,"b"],[4,"c"]])
                ,[[1, "c"], [1, "b"], [4, "c"], [6, "D"]]
                ,[[6,"D"],[1,"c"],[1,"b"],[4,"c"]], ["nl",true] ]
        , ""
        , "// Sort by the first item (i=0), then by the 2nd (j=1):"
        , ""                
        , [ sort([[6,"D"],[1,"c"],[1,"b"],[4,"c"]],0,1)
                ,[[1, "b"], [1, "c"], [4, "c"], [6, "D"]]
                ,"[[6,'D'],[1,'c'],[1,'b'],[4,'c']],0,1", ["nl",true] ]
        ,""
        ,str( "arr = ", arr)
        ,""
        , [ sort(arr,0,1)
                ,[[0, 1, 0], [0, 2, 0], [60, 2, 1], [120, 1, 1], [120, 2, 2], [180, 2, 3], [240, 1, 2], [240, 2, 4]]
                ,"arr,0,1", ["nl",true] ]
        

        
        ]
        , mode=mode, opt=opt, scope=["arr",arr] 
        )
    );    

//echo( sort_test( 112 )[1] );

//========================================================
{ // sortArrs
function sortArrs(arrs, by=0)=
(
  !(len(arrs)>0) ? [] : 
    let( pivot   = arrs[floor(len(arrs)/2)][by],
           lesser  = [ for (y = arrs) if ( y[by]  < pivot ) y ],                                        ,
           equal   = [ for (y = arrs) if ( y[by] == pivot ) y ] ,
           greater = [ for (y = arrs) if ( y[by]  > pivot ) y ]
       )
      concat( sortArrs(lesser, by), 
              equal, 
              sortArrs(greater, by) 
            )
);


    sortArrs=["sortArrs", "arr", "number", "Math, Array",
    "Return the sum of arr's items, which could be pts.  
    "
    ];

    function sortArrs_test( mode=MODE, opt=[] )=
    (
        let( pts =[[2.9, 1.9, -2.8], [-2.5, -1.5, -2.9]
             , [-0.1, 2.8, 2.4], [0.9, 1.8, 0.8], [-1, -1.3, -0.8]]
           )  
        
        doctest( sortArrs, 
        [
         "var pts"
        , [ sortArrs(pts),[[-2.5, -1.5, -2.9], [-1, -1.3, -0.8], [-0.1, 2.8, 2.4], [0.9, 1.8, 0.8], [2.9, 1.9, -2.8]],"pts"]
        , [ sortArrs(pts,1),[[-2.5, -1.5, -2.9], [-1, -1.3, -0.8], [0.9, 1.8, 0.8], [2.9, 1.9, -2.8], [-0.1, 2.8, 2.4]],"pts,1"]
        , [ sortArrs(pts,2),[[-2.5, -1.5, -2.9], [2.9, 1.9, -2.8], [-1, -1.3, -0.8], [0.9, 1.8, 0.8], [-0.1, 2.8, 2.4]],"pts,2"]
        ]
        , mode=mode, opt=opt, scope=["pts",pts] )

        // [[-2.5, -1.5, -2.9]
        //, [-1, -1.3, -0.8]
        //, [-0.1, 2.8, 2.4]
        //, [0.9, 1.8, 0.8]
        //, [2.9, 1.9, -2.8]]
        //
        // [[-2.5, -1.5, -2.9]
        //, [-1, -1.3, -0.8]
        //, [0.9, 1.8, 0.8]
        //, [2.9, 1.9, -2.8]
        //, [-0.1, 2.8, 2.4]
        //]
        //
        // [[-2.5, -1.5, -2.9]
        //, [2.9, 1.9, -2.8]
        //, [-1, -1.3, -0.8]
        //, [0.9, 1.8, 0.8]
        //, [-0.1, 2.8, 2.4]]
    );
    
}


function subArrs(arr,size, keep=true)=
(
   concat(
   [ for( i = [size:size:len(arr)] )
     [ for(j = [i-size:i-1] ) arr[j] ]
   ]
   , keep && (len(arr)%size)?
      [[ for(i=[(len(arr)-len(arr)%size):len(arr)-1])
        arr[i]
      ]]:[]
   )     
);
    
    subArrs=["subArrs", "arr,size,keep", "arr", "Array, String",
    "Split the given arr into size. For the residual tail items,
    ;;  keep them if keep=true (default), otherwise ignored. 
    ;;  
    ;;  * If the intention is to return indices, use:
    ;;  
    ;;    subArrs( range(arr), size, keep )
    ;;  
    ;; * Works for string, too.   
    "
    ];

    function subArrs_test( mode=MODE, opt=[] )=
    (
        let( arr= [3,2,4,51,21,67,45,7,9,8])
        doctest( subArrs, 
        [
          [ subArrs(arr,2), [[3,2],[4,51],[21,67],[45,7],[9,8]], "arr,2"]
        , [ subArrs(arr,3), [[3, 2, 4], [51, 21, 67], [45, 7, 9], [8]],"arr,3"]
        , [ subArrs(arr,3,false), [[3,2,4],[51, 21, 67], [45, 7, 9]], "arr,3,false"]
        , [ subArrs(arr,4),[[3, 2, 4, 51], [21, 67, 45, 7], [9, 8]],"arr,4"]
        , [ subArrs(arr,4,false),[[3, 2, 4, 51], [21, 67, 45, 7]],"arr,4,false"]
        , [ subArrs(range(arr),2),[[0, 1], [2, 3], [4, 5], [6, 7], [8, 9]], "range(arr)"]
        , [ subArrs(range(10),2),[[0, 1], [2, 3], [4, 5], [6, 7], [8, 9]], "range(10)"]
        , [ subArrs("ABCDEFGHIJ",2), [["A", "B"], ["C", "D"], ["E", "F"], ["G", "H"], ["I", "J"]], "'ABCDEFGHIJ',2"]
        , [ subArrs("ABCDEFGHIJ",3),[["A", "B", "C"], ["D", "E", "F"], ["G", "H", "I"],["J"]], "'ABCDEFGHIJ',3"]
        , [ subArrs("ABCDEFGHIJ",3,false),[["A", "B", "C"], ["D", "E", "F"], ["G", "H", "I"]], "'ABCDEFGHIJ',3,false"]
        ]
        , mode=mode, opt=opt )
    );
    


function sum(arr, _rtn=0, _i=0)=
(  
    let( _rtn= _i==0 && isarr(arr[0])
               ?repeat([0],len(arr[0])):_rtn )
    _i<len(arr)? 
        sum( arr, _rtn+arr[_i], _i+1 )
    : _rtn
);  

    sum=["sum", "arr", "number", "Math, Array",
    "Return the sum of arr's items, which could be pts.  
    "
    ];

    function sum_test( mode=MODE, opt=[] )=
    (
        doctest( sum, 
        [
          [ sum([2,3,4,5]),14, [2,3,4,5]]
        , [ sum([[1,2,3],[4,5,6]]),[5,7,9], [[1,2,3],[4,5,6]]]
        , [ sum([[1,2,3],[4,5,6],[1,1,1]]),[6,8,10], [[1,2,3],[4,5,6],[1,1,1]]]
        ], mode=mode, opt=opt )
    );

    //echo( sum_test(mode=12)[1] );


//========================================================

function sumto(n)= n+ (n>1?sumto(n-1):0);

    sumto =[  "sumto", "n", "int", "Math" ,
    "
     Given n, return 1+2+...+n 
    "
    ];
    function sumto_test( mode=MODE, opt=[] )=
    (
        doctest( sumto, 
        [
          [ sumto(5), 15, "5" ]
        , [ sumto(6), 21, "6" ]
        ], mode=mode, opt=opt )
    );
    
    
//========================================================

function switch(arr,i,j)=
 let( L=len(arr)
    , i=fidx(arr,i)
	, j=fidx(arr,j)
    , errdata= ["vname","arr", "len", len(arr), "fname", "switch"]
    )
(
    [ for (k=range(arr)) k==i? arr[j]: k==j?arr[i]:arr[k] ] 
);
    
    switch=["switch","arr,i,j","array", "Array",
     " Given an array and two indices (i,j), return a new array
    ;; with items i and j switched. i,j could be negative. If either
    ;; one out of range, return undef.
    "
    ];
        
    function switch_test( mode=MODE, opt=[] )=
    (
      let( arr=[2,3,4,5,6] )
        doctest( switch,
        [
          "var arr"		
        , [ switch(arr,1,2), [2,4,3,5,6] , "arr,1,2"]
        , [ switch(arr,0,2), [4,3,2,5,6] , "arr,0,2"]
        , [ switch(arr,0,-1), [6,3,4,5,2] , "arr,0,-1"]
        , [ switch(arr,-1,-5), [6,3,4,5,2] , "arr,-1,-5"]
        , [ switch(arr,-1,4), [2,3,4,5,6], "arr,-1,4", ["//","No switch"] ]

        ]
        ,mode=mode, opt=opt, ["arr", arr]

        )
    );
    
    
//========================================================

function transpose(mm)=
(
    mm? [ for(c=range(mm[0])) [for(r=range(mm)) mm[r][c]] ]
      : mm 
);
    
    transpose= [ "transpose", "mm", "mm",  "Array" ,
    "Given a multmatrix, return its transpose.
    "
    ];
        
    function transpose_test( mode=MODE, opt=[] )=
    (
        let( mm = [[1,2,3],[4,5,6]]
           , mm2= [[1,4],[2,5],[3,6]]
           )
           
        doctest( transpose,
        [
          [  transpose(mm), mm2, mm ]
        , [  transpose(mm2), mm, mm2 ]
        ]
        , mode=mode, opt=opt
        )
    );
    //transpose_test( ["mode",22] );


//========================================================

function uniq(arr, _rtn=[], _i=0)= // NOTE: doesn't seem to deal correctly with 
(                                  // array of arrays ( [ [3,4], [7,8]...])
   let( x= arr[_i]
      , _rtn = has(_rtn,x)?_rtn:app(_rtn,x)
      ) 
   _i<len(arr)-1?
      uniq( arr, _rtn= _rtn, _i=_i+1 )
      :_rtn
);    

    uniq=["uniq","arr","arr","Array",
    "Remove the duplicates in arr."
    ];

    function uniq_test( mode=MODE, opt=[] )=
    (
        doctest( uniq,
        [
          [  uniq( [1,1,2,2,2,1,3,3]),[1,2,3],[1,1,2,2,2,1,3,3] ]
        , [ uniq( [ [2,3],[4,5],[2,3]] ),[[2,3],[4,5]], [ [2,3],[4,5],[2,3]] ]   
        ]
        , mode=mode, opt=opt
        )
    );
    //echo( uniq_test( mode=112)[1] );


function zip(a1,a2, isflat=0)=  // 2018.4.16 for subdiv
(
  isflat?
  [for(i=[0:max(len(a1),len(a2))-1])
    each [a1[i], a2[i]] 
  ]
  :[for(i=[0:max(len(a1),len(a2))-1])
    [a1[i], a2[i]] 
   ]
);

  zip=[ "zip", "a1,a2,isflat=0", "arr", "Array",
    " Zip a1 and a2 together. 
    "];

    function zip_test( mode=MODE, opt=[] )=
    ( 
        doctest( zip,
        [
          [zip([1,2,3],[4,5,6],isflat=1), [1,4,2,5,3,6], "[1,2,3],[4,5,6],isflat=1" ]
        , [zip([1,2,3],[4,5,6,7],isflat=1), [1,4,2,5,3,6,undef,7], "[1,2,3],[4,5,6,7],isflat=1" ]
        , [zip([1,2,3,4],[5,6,7],isflat=1), [1,5,2,6,3,7,4,undef], "[1,2,3,4],[5,6,7],isflat=1" ]
        , [zip([1,2,3],[4,5,6]), [[1,4],[2,5],[3,6]], "[1,2,3],[4,5,6]" ]
        , [zip([1,2,3],[4,5,6,7]), [[1,4],[2,5],[3,6],[undef,7]], "[1,2,3],[4,5,6,7]" ]
        , [zip([1,2,3,4],[5,6,7]), [[1,5],[2,6],[3,7],[4,undef]], "[1,2,3,4],[5,6,7]" ]
        ],mode=mode, opt=opt
        )
    );    
    
function zipx(a,b)= // zipx( [2,3],[4,5] ) => [8,15]
(
   [ for(i=range(min(len(a),len(b))))
     a[i]*b[i] 
   ]
);

    zipx=[ "zipx", "a,b", "arr", "Array",
    " Zip a1 and a2 together by a0xb0, a1xb1, ... 
    "];

    function zipx_test( mode=MODE, opt=[] )=
    ( 
        doctest( zipx,
        [
          [zipx([1,2,3],[4,5,6]), [4,10,18], "[1,2,3],[4,5,6]" ]
        , [zipx([1,2,3],[4,5,6,7]), [4,10,18], "[1,2,3],[4,5,6,7]" ]
        , [zipx([1,2,3,4],[4,5,6]), [4,10,18], "[1,2,3,4],[4,5,6]" ]
        ],mode=mode, opt=opt
        )
    );    
    


/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_array_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_array.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_array_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_array",  mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      accum_test( mode, opt )
    , all_test( mode, opt )
    , any_test( mode, opt )
    , app_test( mode, opt )
    , appi_test( mode, opt )
    , arr_diff_test( mode, opt )
    , arrblock_test( mode, opt )
    , arrprn_test( mode, opt )
    , avg_test( mode, opt ) 
    , combine_test( mode, opt )
    , deeprep_test( mode, opt )
    , del_test( mode, opt )
    , dels_test( mode, opt )
    , flatten_test( mode, opt )
    , getcol_test( mode, opt )
    , insert_test( mode, opt )
    , join_test( mode, opt )
    , joinarr_test( mode, opt )
    , last_test( mode, opt )
    , next_test( mode, opt )
    , permute_test( mode, opt )
    , permute2_test( mode, opt )
    , permutes_test( mode, opt )
    , ppEach_test( mode, opt )
    , prev_test( mode, opt )
    , quicksort_test( mode, opt )
    , reverse_test( mode, opt )
    , roll_test( mode, opt )
    , sd_test( mode, opt ) 
    , share_test( mode, opt )
    , shrink_test( mode, opt )
    , shuffle_test( mode, opt )
    , sort_test( mode, opt )
    , sortArrs_test( mode, opt )
    , subArrs_test( mode, opt )
    , sum_test( mode, opt )
    , sumto_test( mode, opt )
    , switch_test( mode, opt )
    , transpose_test( mode, opt )
    , uniq_test( mode, opt )
    , zip_test( mode, opt )
    , zipx_test( mode, opt ) 
    ]
);

   