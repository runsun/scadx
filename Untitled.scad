//include <scadx.scad>

module Line( pts, r=0.05, closed=false, color=undef, transp=1, fn=4 )
{
  if(len(pts)>1 && len(pts[0])>1 && r>0) 
  { 
    // Convert to 3d if 2d; Attach 1st pt if closed
    pts  = concat( [for(p=pts) [ p[0],p[1],p[2]==undef?0:p[2] ]],  
                  closed? [[ pts[0][0], pts[0][1], pts[0][2]==undef?0:pts[0][2] ]] : []
    ); 
    color(color,transp)
    for(i=[0:len(pts)-2]) 
    { 
       v = pts[i+1]-pts[i]; 
       L = norm(v); 
       if( L > 1e-12) 
         translate(pts[i]) 
         rotate([0, acos(v[2]/L), atan2(v[1],v[0])]) 
         rotate(45) cylinder(h=L, r=r, $fn=fn); 
    } 
  } 
}


//2000_pts();
module 2000_pts()
{
   pts= randPts(2000);
   Line(pts);
}

//color("red") SineLine(n=5);
//color("blue") SineLine( [36:3600], h=3,n=1,base=2, r=0.1);
function sinePts(xs=[0:3600],xd=100, n=5, h=5, base=0, z=0)=
(
   [ for(x=xs) [(x-xs[0])/xd,h*sin(x/n)+base,z] ]
);

module SineLine( xs=[0:3600],n=5, h=5, base=0, r=0.2 )
{ Line( sinePts( xs=xs, n=n, h=n, base=base ), r=r); 
}


//SineEdgePlate();
module SineEdgePlate()
{
   function to2d(pts)=[for(p=pts)[p[0],p[1]]];
   pts= concat( [ [36,0], [0,0]]
                , to2d(sinePts( h=0.5, n=0.5, base=10 ))
                 ); 
   polygon( pts );
}

SineEdgeBlock();
module SineEdgeBlock()
{
   z=20;
   //x= 
   sinepts= sinePts( xs=[-5:90], xd=5, h=1, n=0.1, base=10 );
   Line(sinepts);
   x= sinepts[ len(sinepts)-1 ].x;
   pts= concat( [[x,0,0],[0,0,0]] 
              , sinepts   
              , [[x,0,z],[0,0,z]]
              , [ for(p=sinepts) [p[0],p[1],z] ]
              ); 
   faces=  [[97, 96, 95, 94, 93, 92, 91, 90, 89, 88, 87, 86, 85, 84, 83, 82, 81, 80, 79, 78, 77, 76, 75, 74, 73, 72, 71, 70, 69, 68, 67, 66, 65, 64, 63, 62, 61, 60, 59, 58, 57, 56, 55, 54, 53, 52, 51, 50, 49, 48, 47, 46, 45, 44, 43, 42, 41, 40, 39, 38, 37, 36, 35, 34, 33, 32, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0], [98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195], [0, 1, 99, 98], [1, 2, 100, 99], [2, 3, 101, 100], [3, 4, 102, 101], [4, 5, 103, 102], [5, 6, 104, 103], [6, 7, 105, 104], [7, 8, 106, 105], [8, 9, 107, 106], [9, 10, 108, 107], [10, 11, 109, 108], [11, 12, 110, 109], [12, 13, 111, 110], [13, 14, 112, 111], [14, 15, 113, 112], [15, 16, 114, 113], [16, 17, 115, 114], [17, 18, 116, 115], [18, 19, 117, 116], [19, 20, 118, 117], [20, 21, 119, 118], [21, 22, 120, 119], [22, 23, 121, 120], [23, 24, 122, 121], [24, 25, 123, 122], [25, 26, 124, 123], [26, 27, 125, 124], [27, 28, 126, 125], [28, 29, 127, 126], [29, 30, 128, 127], [30, 31, 129, 128], [31, 32, 130, 129], [32, 33, 131, 130], [33, 34, 132, 131], [34, 35, 133, 132], [35, 36, 134, 133], [36, 37, 135, 134], [37, 38, 136, 135], [38, 39, 137, 136], [39, 40, 138, 137], [40, 41, 139, 138], [41, 42, 140, 139], [42, 43, 141, 140], [43, 44, 142, 141], [44, 45, 143, 142], [45, 46, 144, 143], [46, 47, 145, 144], [47, 48, 146, 145], [48, 49, 147, 146], [49, 50, 148, 147], [50, 51, 149, 148], [51, 52, 150, 149], [52, 53, 151, 150], [53, 54, 152, 151], [54, 55, 153, 152], [55, 56, 154, 153], [56, 57, 155, 154], [57, 58, 156, 155], [58, 59, 157, 156], [59, 60, 158, 157], [60, 61, 159, 158], [61, 62, 160, 159], [62, 63, 161, 160], [63, 64, 162, 161], [64, 65, 163, 162], [65, 66, 164, 163], [66, 67, 165, 164], [67, 68, 166, 165], [68, 69, 167, 166], [69, 70, 168, 167], [70, 71, 169, 168], [71, 72, 170, 169], [72, 73, 171, 170], [73, 74, 172, 171], [74, 75, 173, 172], [75, 76, 174, 173], [76, 77, 175, 174], [77, 78, 176, 175], [78, 79, 177, 176], [79, 80, 178, 177], [80, 81, 179, 178], [81, 82, 180, 179], [82, 83, 181, 180], [83, 84, 182, 181], [84, 85, 183, 182], [85, 86, 184, 183], [86, 87, 185, 184], [87, 88, 186, 185], [88, 89, 187, 186], [89, 90, 188, 187], [90, 91, 189, 188], [91, 92, 190, 189], [92, 93, 191, 190], [93, 94, 192, 191], [94, 95, 193, 192], [95, 96, 194, 193], [96, 97, 195, 194], [97, 0, 98, 195]];
   echo(len(faces));
   polyhedron ( pts,faces);
}







/*
* I know OpenSCAD is primarily for 3D printing, but it's also very useful 
for laser cutting. When cutting, enclosed objects are not always the norm, 
sometimes you just need to cut a line. 

* It would be a useful tool in creting some complex geometries. Say, for 
example, that you want the area enclosed by two slightly phased sine waves 
as an object, something that gives me a math headache. If you had lines, you 
could first draw the sines as lines, then have some operation where you do a 
2D "flood fill" from a specified point to get the enclosed area. 

* Linear reference systems. I know most people in the CAD/GIS world love XY. 
XY everywhere you look. Talk about anything but XY, and they think you are 
insane. But, in many other applications, linear reference systems are used, 
and they are extremely useful. Instead of XY, you reference a point as "this 
distance from the start of this line". Very often used in infrastructure 
databases (road, rail, cables), for example, but also useful if you want to 
do stuff like "place objects at equal distance along this non-straight 
line". The math to do that is very straightforward in a typical langage, 
but, alas, not on OpenSCAD, where it is downright painful. Linear reference 
systems, once you realize how magnificient they are, are insanely useful. 

* Cutting 2D objects. For example, my laser cutter doesn't have an infinit 
working table size. I sometimes need to divide stuff into smaller parts. A 
line could, with difference(), cut a 2D part with a zero kerf (cutting 
width) cut. As the laser have a certain kerf, you don't want to lose 
precision by adding more. 

* Laser cutters also engrave. It's nice to be able to include marks on a 
design, for example an + where to drill or a line for where to attach some 
other part. These are often lines (sometimes unfilled circles). 

I'm sure there are many more useful use cases. 

Is there any library for this, or anything like it in some plans for the 
future? 
*/