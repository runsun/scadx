include <scadx.scad>

module get_animate_visibility_demo()
{
   // set FPS=10, steps=10 for animation
  
   showtimes = [ for(t=[0:9]) [ t/10, t/10+0.09] ];
   //showtimes = [ for(t=[0:9]) undef ];  // <--- this will turn all on
   for( x=[0:9] )
   {
     st = showtimes[x];
     v =  get_animate_visibility( st ); 
     echo( $t=$t, st = st, v=v  );
     if(v)
        translate( [x,0,0] )
        cube( [2, 2, 2 ] );
   }  
}


get_animate_visibility_demo();  // set FPS=10, steps=10 for animation
