
//========================================================
{ // pqr 
function pqr(d=2, a=undef, p=undef,r=undef)=
(   let(pqr=randPts(3,d=d)
       , P0=pqr[0], Q=pqr[1], R0=pqr[2]
       , dp= or(p, dist(Q,P0))
       , dr= or(r, dist(Q,R0))
       , P = onlinePt( [Q,P0], len=dp)
       )
    isnum(a)? [P, Q, anglePt( pqr, a, len=dr)]
    : [ P, Q, onlinePt( [Q,R0], len=dr) ]  
);  
    pqr=["pqr","a,p,r","pts","Geometry"
    ,""];
    function pqr_test( mode=MODE, opt=[] )=(

        doctest( pqr, [], mode=mode, opt=opt )

    ); // ???        
//echo( pqr(p=1,r=2,a=90));
} // pqr 

//========================================================
{ // pqr90    
function pqr90(pqr, d=3,x,y,z,seed)= 
    let( pqr=pqr==undef?randPts(3,d,x,y,z,seed):pqr)   
(    
    [ pqr[0], pqr[1], anglePt(pqr,90) ]
);
    pqr90=["pqr90","pqr,d=3,x,y,z,seed","pts","Geometry",
    " Return a pqr that with anelg(pqr)=90. If pqr not given, make one randomly
    ;;
    ;; Refer to randPt() for other argument definitions;
    ;; Use randRightAnglePts() if need to specify leg lengths"
    ];

    function pqr90_test( mode=MODE, opt=[] )=(

        doctest( pqr90, [], mode=mode, opt=opt )

    ); // ???
} // pqr90    


//========================================================
rand=["rand", "m=1, n=undef, seed=undef", "Any", "Random, Number, String",
" Given a number (m), return a random number between 0~m.
;; If another number (n) is given, return a random number between m~n.
;; If m is an array or string, return one of its member randomly.
;;
;;   arr= ['a', 'b', 'c']; 
;;
;;   rand(5)=3.63411
;;   rand(5,8)=6.4521
;;   rand(arr)='b'
;;
;; 2015.2.26: add arg 'seed'
;;
;;   rand( 5,seed=3 )= 2.75399
;;   rand( 5,8, seed=3 )= 6.65239
;;   rand( arr, seed=3 )= ''b''
"
];

function rand(m=1, n=undef, seed=undef)=
(
	isarr( m )||isstr(m) ? m[ floor(rand(len(m),seed=seed)) ]
	: (isnum(n)
		? (seed==undef? rands( min(m,n), max(m,n), 1)
                     : rands( min(m,n), max(m,n), 1, seed) )
		: (seed==undef? rands( 0, m, 1)
                     : rands( 0, m, 1, seed) )
	  )[0]
);



    function rand_test( mode=MODE, opt=[] )=
    (
        let( arr=["a","b","c"] )
        //echo("rand_test, arr=", arr);
        doctest( rand,
        [
            "// rand(a)"
            ,str(">> rand(5):", rand(5))
            ,str(">> rand(5):", rand(5))
            ,str(">> rand(5):", rand(5))
            ,str(">> rand(5):", rand(5))
            ,str(">> rand(5):", rand(5))
            ,"//rand(a,b)"
            ,str(">> rand(5,8):", rand(5,8))
            ,str(">> rand(5,8):", rand(5,8))
            ,str(">> rand(5,8):", rand(5,8))
            ,str(">> rand(5,8):", rand(5,8))
            ,str(">> rand(5,8):", rand(5,8))
            ,"//rand(arr)"
            ,str(">> rand(arr):", rand(arr))
            ,str(">> rand(arr):", rand(arr))
            ,str(">> rand(arr):", rand(arr))
            ,str(">> rand(arr):", rand(arr))
            ,str(">> rand(arr):", rand(arr))
            ,"// rand(len(arr))=> float"
            ,"// rand(range(arr))=> integer = floor(rand(len(arr)))"
            ,str(">> rand(range(arr)):", rand(range(arr)))
            ,str(">> rand(range(arr)):", rand(range(arr)))
            ,str(">> rand(range(arr)):", rand(range(arr)))
            ,"// Add arg seed 2015.2.26, making it repeatable"
            ,[ rand(5,seed=3),2.75399,"5,seed=3", ["asstr",true]]
            ,[ rand(5,8, seed=3),6.65239,"5,8, seed=3", ["asstr",true]]
            ,[ rand(arr, seed=3),"b","arr, seed=3"]
        
        ], mode=mode, opt=opt, scope=["arr",arr]
        )
    );
    
//========================================================
function randc( r=[0,1],g=[0,1],b=[0,1] )=
(
    [ rands(r[0],r[1],1)[0]
	, rands(g[0],g[1],1)[0]
	, rands(b[0],b[1],1)[0] ]	
);
    randc=["randc", "r=[0,1],g=[0,1],b=[0,1]", "arr", "Random,Color" ,
    "
     Given r,g,b (all have default [0,1], which is the max range),
    ;; return an array with random values for color. Examples:
    ;;
    ;; >> color( randc() )  sphere(r=1)
    ;; >> darker = [0, .4]  // smaller number
    ;; >> color( randc( r=darker, g=darker ) )  sphere(r=1)
    "];

    function randc_test( mode=MODE, opt=[] )=
    (
        doctest(randc, [], mode=mode, opt=opt )
    );

//========================================================



//========================================================
{ // randchain 
function randchain(
       n =3, 
     , a //=[-360,360]
     , a2 //=[-360,360]
     , seglen //= [0.5,3] // rand len btw two values
     , lens //=undef
     , seed //=undef // base pqr
     , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
                     //   , "closed",false,details=false]
//     , d //=3
//     , x //=undef
//     , y //=undef
//     , z // =undef
     , opt=[]
     , _rtn=[] // for noUturn
     , _smsegs=[] // segments of smoothed pts
     ,_i=0 // for noUturn
     ,_debug
    )
= // ["pts", [...], "unsmoothed", [...], "smooth", undef, "seed", p3, "lens", undef
  //, "seglen", undef, "L", 2.46809, "a", [170, 190], "a2", [-45, 45], "debug", undef]
    
(
//  !n? _red(str( "ERROR: the n(# of pts) given to randchain() is invalid: ",n ))
//  :(
    //echo("randchain()")
    _i==0? //####################################### i==0
    
    let( 
        //=========[ initiation ] =======

          opt = popt(opt) // handle sopt
        //, n = hash(opt,"n", n)
        , n = n>1?n: hash(opt,"n",2)
        , a = und(a, hash(opt,"a", [170,190])) // = aPQR
        , a2= und(a2,hash(opt,"a2",[-45,45])) // = aRQN
        , seglen= or(seglen, hash(opt, "seglen"))
        , _lens = or(lens, hash(opt, "lens"))
        , seed = or(seed, hash(opt, "seed", randPts(3)))
        , smooth= und(smooth, hash(opt, "smooth"))

        //==============================

        ,lens = // If lens < n-1, repeat lens        
           _lens && isarr(_lens) && len(_lens)< n-1?
             repeat(_lens, ceil((n-1)/len(_lens)))
            : _lens
            
//        , L = isarr(lens)? lens[_i-1]
//            : isarr(seglen)? rand(seglen[0],seglen[1]) 
//            : isnum(seglen)? seglen
//            : rand(0.5,3)
        , pt= seed? seed[0]:randPt() //d,x,y,z)
        , newrtn = [pt]  
              
     )//let @_i==0
     
     randchain(n=n,a=a,a2=a2,seglen=seglen, lens=lens
              , seed = seed
              //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1
              , smooth=smooth, _debug=_debug)   
              
   :let(  // ###################################### i > 0
   
     isend= _i==n-1
     ,_a = isarr(a)? rand(a[0],a[1]):a
     ,_a2= isarr(a2)? rand(a2[0],a2[1]):a2
     ,L = isarr(lens)? lens[_i-1]
          : isarr(seglen)? rand(seglen[0],seglen[1]) 
          : isnum(seglen)? seglen
          : rand(0.5,3)
     ,pt= _i==1? (seed? onlinePt( p01(seed), len=L)
                      :onlinePt( [last(_rtn)
                               , randPt()],len=L)
                  )    
          :  let( P = get(_rtn,-2)
                  , Q = get(_rtn,-1)
                  , Rf = _i==2 && seed? seed[2] //onlinePt( p12(seed), len=L)
                         : iscoline( get(_rtn, [-3,-2,-1]))
                            ?randOfflinePt( get(_rtn,[-2,-1]))
                           : get(_rtn,-3)
                  )
               anglePt( [P,Q,Rf], a=_a, a2=_a2, len=L)   
                       
     ,newrtn = app(_rtn, pt ) 
     , smpts = isend?
              ( smooth==true? smoothPts(newrtn)
                :smooth? smoothPts(newrtn, opt=smooth) 
//                   smoothPts( newrtn, n=hash(smooth, "n")
//                            , aH=hash(smooth,"aH")
//                            , dH=hash(smooth,"dH")
//                            , closed= hash(smooth, "closed")
//                            , details= hash(smooth, "details")
//                       )
                       
                   //n=10, aH=0.5, dH=0.3, closed=false, details=false
                  :newrtn
              ):[]
          
//     ,_debug= str(_debug, "<br/>"
//                 ,"<br/>, <b>_i</b>= ", _i
//                 ,"<br/>, <b>len</b>= ", len
//                 ,"<br/>, <b>L</b>= ", L
//                 ,"<br/>, <b>_rtn</b>= ", _rtn
//                 ,"<br/>, <b>newrtn</b>= ", newrtn
//                 ,"<br/>, <b>actual len</b>= ", dist( get(newrtn,[-1,-2]))
//                 ,"<br/>, <b>pt</b>= ", pt
//                 )
     )//let

  //isend? (smooth? [newrtn,smpts]: newrtn ) 
  isend? 
    [ "pts", smpts
    , "unsmoothed", newrtn
    , "smooth", smooth
    , "seed", seed
    , "lens",lens
    , "seglen", seglen
    , "L",L
    , "a",a
    , "a2", a2
    , "debug",_debug
    ]
    // (smooth? [newrtn,smpts]: newrtn ) 
  
  : randchain(n=n,a=a,a2=a2,seglen=seglen, lens=lens, seed = seed
             //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1, smooth=smooth, _debug=_debug)
  //)// if_n
);

} // randchain
     
      
    randchain=["randchain", " n,a,a2,seglen,lens,seed,smooth,opt", "hash","Rand"
    , "Generate a hash for random chain, containing the following keys:
    ;; 
    ;;  'pts'
    ;;  'unsmoothed'
    ;;  'smooth'
    ;;  'seed'
    ;;  'lens'
    ;;  'seglen'
    ;;  'L'  // like 2.46809
    ;;   'a' // like [170, 190]
    ;;   'a2' // like [-45, 45]
    ;;   'debug'
    ;;
    "];

    function randchain_test( mode=MODE, opt=[] )=
    (
        doctest(randchain
        , [
            [randchain(),"","randchain()", ["notest",1] ]
          ]
        , mode=mode, opt=opt 
        )
    );
         
     
function randConvexPlane(n=5, d=3,x,y,z, site=pqr(), _sum_a=0, _rtn=[], _i=2)=
(
  let( base_r = dist(ORIGIN, randPt(d,x,y,z)) 
     , init= [ for(i=[1:n])
               precPts( anglePt( site 
                        , a = 360/n * i + rands(-360/n/2,360/n/2,1)[0]
                        , len = base_r
                        ))
             ]
     )
  init       
);
    randConvexPlane=["randConvexPlane","n=5, d=3,x,y,z, site", "pts","Random,Point"
    , "Return pts representing a random random convex plane."
    ];
    function randConvexPlane_test( mode=MODE, opt=[] )=
    (
        doctest( randConvexPlane,
        [
        ]
        ,
        mode=mode, opt=opt
        )
    );

        
//========================================================
function randCoordPts(len=5, d=3,x,y,z,seed)=
(
   let(pqr = pqr90(d=d,x=x,y=y,z=z,seed=seed) )
   [ normalPt(pqr, len=len), pqr[0], pqr[1], pqr[2]]
);

         
//========================================================
{ // randOfflinePt
function randOfflinePt(pq,d=3,x,y,z)=
(
    let(pt= randPt(d=d,x=x,y=y,z=z))
    iscoline( pq, pt )? randOfflinePt(pq,d=3,x,y,z)
                    : pt
);
//echo( randOfflinePt( randPts(2) ) );
} // randOfflinePt


//========================================================
{ // randInPlanePt
function randInPlanePt(pqr)=
(
  let( r3= rands(0,1,3) ) 
  (r3/sum(r3))*pqr  
//   let( ps= pick(pqr, rand([0,1,2]) ))
//   //echo( pick_test = rand([0,1,2]))
//   //echo(pqr=pqr)//, ps=ps)
//   //echo(ps0=ps[0])//, ps1=ps[1], rand=rand())
//   onlinePt( [ ps[0], onlinePt( ps[1], ratio=rand() )], ratio=rand() )
);

    randInPlanePt=["randInPlanePt","pqr", "point","Random,Point"
    , "Return a random pt inside the triangle pqr."
    ];
    //randInPlanePt_demo();
    function randInPlanePt_test( mode=MODE, opt=[] )=
    (
        doctest( randInPlanePt,
        [
        ]
        ,
        mode=mode, opt=opt
        )
    );
} // randInPlanePt
/*
module dev_inTrianglePt2()
{
   echom("dev_inTrianglePt2");
   n = 100;
   P=randPt(); Q=randPt(); R=randPt();
   P= 0.5*X+1*Y;
   Q= 5*X+0.5*Y;
   R= 1.5*X+4*Y;
   
   echo(pqr=[P,Q,R]);
   //pqr = pqr();
   MarkPts([P,Q,R], "PQR", Line=["closed",true] );
      r3 = rands(0,1,3);
      sum = sum(r3);
   echo(r3=r3, r3/sum, sum(r3/sum));
   echo( [P,Q,R]*(r3), (r3[0]*P+r3[1]*Q+r3[2]*R)); 
   for(i=[0:n-1])
   {
      r3 = rands(0,1,3);
      sum = sum(r3);
      S=(r3/sum)*[P,Q,R];
      MarkPt( S, Label=false );
   }
}

*/
//========================================================
{ // randInPlanePts
function randInPlanePts(pqr, count=2)=
(
   count>0? concat( [randInPlanePt( pqr )]	
                  , randInPlanePts( pqr, count-1)
                  ) :[]
);
    randInPlanePts=["randInPlanePts","pqr,count=2", "point","Random,Point"
    , "Return random points inside triangle pqr"
    ];
    function randInPlanePts_test( mode=MODE, opt=[] )=
    (
        doctest( randInPlanePts,
        [
        ]
        ,
        mode=mode, opt=opt
        )
    );

} // randInPlanePt

//========================================================
{ // randOnPlanePt
//function randOnPlanePt(pqr=randPts(3), r=undef, a=360)=
//(
////  onlinePt( [pqr[floor(rand(3))], randInPlanePt(pqr)], ratio= rand(3) )
//   anglePt( pqr
////		 [  pqr[0], incenterPt(pqr)
////		   ,pqr[1]
////		   ]
//		  , a = isnum(a)?(rand(1)*a)
//					: rands( a[0], a[1],1)[0]
//			,r = r==undef? max( d01(pqr), d02(pqr), d12(pqr) ) * rand(1)
//				:isarr(r)? rands( r[0], r[1],1)[0]
//				:r
//		)	
//);
function randOnPlanePt(pqr=randPts(3), r=undef, a=360)=
(
//  onlinePt( [pqr[floor(rand(3))], randInPlanePt(pqr)], ratio= rand(3) )
   anglePt( pqr
//		 [  pqr[0], incenterPt(pqr)
//		   ,pqr[1]
//		   ]
		  , a = isnum(a)?(rand(1)*a)
					: rands( a[0], a[1],1)[0]
			,r = r==undef? max( d01(pqr), d02(pqr), d12(pqr) ) * rand(1)
				:isarr(r)? rands( r[0], r[1],1)[0]
				:r
		)	
);

    randOnPlanePt=["randOnPlanePt","pqr,r=undef,a=360", "Point", "Random, Point"
    ,"Return random a point on plane pqr"];
    //
    // optional r : distance from pqr's incenterPt. Can be: a number, an array [a,b] for range
    // optional a : random range of aXQP (X = returned Pt). Default 360. Can be [a1,a2] for range] 
    function randOnPlanePt_test( mode=MODE, opt=[] )=
    (
        doctest( randOnPlanePt,
        [
        ]
        ,
        mode=mode, opt=opt
        )
);

} // randOnPlanePt

//========================================================
{ // randOnPlanePts
function randOnPlanePts(pqr=randPts(3), count=2, r=undef, a=360 )=
(
   count>0? concat( [randOnPlanePt( r=r, pqr, a=a )]	
                  , randOnPlanePts( count-1, pqr, r=r,a=a )
                  ) :[]
);
    randOnPlanePts=["randOnPlanePts","count=2,pqr=randPts(3),r=undef,a=360", "Points", "Random, Point"
    ,"Return random points on plane pqr"];

    function randOnPlanePts_test( mode=MODE, opt=[] )=
    (
        doctest( randOnPlanePts,
        [
        ]
        ,
        mode=mode, opt=opt
        )
    );
} // randOnPlanePts


function randPlane(d=3,x,y,z)=
(
  let(pq = randPts(2))
  [pq[0], pq[1], randOfflinePt(pq,d=d,x=x,y=y,z=z)]
);

//========================================================
{ // randRightAnglePts
function randRightAnglePts( 
    r=rand(5), 
    dist=randPt(), 
    r2=-1
    )=
( 
    RMs( [ rand(360), rand(360), rand(360)] 
	   , //shuffle(
  	[
  	 [      r,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
  	,[      0,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
  	,[      0,r2>0?r2:rand(r), 0]+(isnum(dist)?[dist,dist,dist]:dist) 
  	]
		//)
	   ) 
); 
    randRightAnglePts=["randRightAnglePts","r=rand(5),dist=randPt(),r2=-1","Points","Point,Random",
    " r: first arm length (default: rand(5) )
    ;; dist: dist from ORIGIN. It could be a point (indicating 
    ;; translocation) or integer (default: rand(3)). If it is 
    ;; an integer, will draw at range inside [dist,dist,dist] 
    ;; location, and the Q will always be the one closest to 
    ;; ORIGIN.  
    ;; r2: 2nd arm length. If not given (default), auto pick a 
    ;; random value	between 0~r
    ;; 
    ;; randRightAnglePts( r=3, r2=3) gives points of a *special 
    ;; right triangle* in which 3 angles are 90, 45, 45.
    ;;
    ;; ref: pqr90() for a quick right triangle with that the arm 
    ;; lengths can't be specified.
    "];

     function randRightAnglePts_test( mode=MODE, opt=[] )=
    (
        doctest( randRightAnglePts,
        [
        ]
        ,
        mode=mode, opt=opt
        )
    );
} //  randRightAnglePts

//========================================================
randConvexPts=[[]];
/*
convex polygons - those with the property that 
given any two points inside the polygon, the entire line segment connecting those two 
points must lie inside the polygon. Such polygons are the intersection of half-planes, 
which means that if you extend the boundary edges into infinite lines the polygon will 
always lie completely on one side of each edge line. If a point is on the same side of each 
edge as the polygon, then it must lie inside the polygon.

http://www.cs.oberlin.edu/~bob/cs357.08/VectorGeometry/VectorGeometry.pdf

function randOnPlaneConvexPts( count=4, pqr=randPts(3)  )=
(
3
 // THIS IS HARD	

);
*/


//========================================================
{ // randPt
function randPt(d, x,y,z, opt=[], seed)=  // 2018.4.15: add opt
                                          // 2018.5.17: decimal =2    
(                                         
   let( _d= arg(d, opt, "d", 30*SCALE) 
      , d=  isnum(_d)?[-_d,_d]:_d 
      , x0= arg(x, opt, "x", d) 
      , y0= arg(y, opt, "y", d) 
      , z0= arg(z, opt, "z", d) 
      , _x = isnum(x0)?[-x0,x0]:x0
      , _y = isnum(y0)?[-y0,y0]:y0
      , _z = isnum(z0)?[-z0,z0]:z0
      )
       mm*( seed? 
          [ x==0?x: round(rands( _x[0], _x[1],1, seed )[0]*100)/100
          , y==0?y: round(rands( _y[0], _y[1],1, seed )[0]*100)/100
          , z==0?z: round(rands( _z[0], _z[1],1, seed )[0]*100)/100
          ]
          :[ x==0?x: round(rands( _x[0], _x[1],1 )[0]*100)/100
           , y==0?y: round(rands( _y[0], _y[1],1 )[0]*100)/100
           , z==0?z: round(rands( _z[0], _z[1],1 )[0]*100)/100
          ]
          )
    //echo(opt=opt, _d=_d, d=d, x0=x0,y0=y0,z0=z0)  
    
);
    randPt=["randPt", "d=3,x,y,z,seed", "pt", "Random,Point",  
    "
     Get a random pt, covering the range defined by [-d, d]. Or d (distance 
    ;; to the ORIGIN) can be given as [-1,3], [0,3], etc.  Or, ranges along 
    ;; the x,y,z directions can be individually specified as x=3, (same
    ;; as x=[-3,3]), y=[-1,4], etc. Set seed to make it repeatable.
    ;; 
    ;;   randPt()          -3,+3 around ORIGIN
    ;;   randPt(1)         -1,+1 around ORIGIN
    ;;   randPt([4,5])      between 4, 5 of all axes 
    ;;   randPt([2,3],z=0)  between 2,3 on x and y on xy plane
    ;;   randPt(0, y=[2,4]) between y=2 and 4 along y axis
    ;;
    ;; Refer to randPts() that gives multiple random points; 
    ;; pqr() to get randPts(3), and pqr90() to get a pts of a random 
    ;; right triangle
    "];

    function randPt_test( mode=MODE, opt=[] )=
    (
        doctest( randPt, mode=mode, opt=opt )
    );
} // randPt

////========================================================
{ // randPts
function randPts(count=3, d=3, x,y,z, opt, seed)=
(
  [ for(i=range(count)) randPt(d,x,y,z,opt, seed) ]
);

 } // randPts
 
function randPtsAlongLine(pts, count, d, closed, fixed_ends, keep_pts, opt)=
(
  // Generate pts along the poly line with noise defined by opt
  // opt = opt for randPt()
  //
  // fixed_ends: if NOT closed, include both beg and end pts
  //                 if closed, include only the beg pt 
  let( lv=1
     , _closed= arg(closed, opt, "closed", false)
     , _fixed_ends= _closed?false: arg(fixed_ends, opt, "fixed_ends",1)
     , _count = arg(count, opt, "count", 10)
                -(_fixed_ends?(_closed?1:2):0)
     , _keep_pts= arg(keep_pts, opt, "keep_pts", true)           
     , _d= arg(d, opt, "d", 0.2)
     , pts_rtn= [for(pt= randPtsOnline( pts, count=_count, closed=closed
                                      , keep_pts=_keep_pts, opt=opt))
                 pt+ randPt( opt= ["d",_d] )]
     )
  //echo( log_b("randPtsAlongLine", lv))
  //echo( log_(["count",count,"_count",_count], lv))
  //echo( log_(["d",d,"_d",_d], lv))
  //echo( log_(["closed",closed,"_closed",_closed], lv))
  //echo( log_(["_fixed_ends",_fixed_ends], lv))
  //echo( log_(["_keep_pts",_keep_pts], lv))
  //echo( log_(["len(pts_rtn)", len(pts_rtn)], lv))
  //echo( log_e("",lv ))   
  concat( 
      _fixed_ends? [pts[0]]:[]
    , pts_rtn
    //, _fixed_ends? [_closed?pts[0]:last(pts)]:[]
    , _fixed_ends&&!_closed? [last(pts)]:[]
  )   
);

function randPtsOnline(pts, count, closed, keep_pts, opt)=
(
  /*
    Generate random pts ON the polyline. Pts are roughly 
    evenly distributed onto each section of the polyline
    i.e., sections with same length have roughly equal
    # of pts.    
    
    keep_pts: the original pts are incorporated into the return.
  */ 
  let( lv=2
     , _closed= len(pts)<=2?false: arg(closed, opt, "closed", false)
     , nsegs = len(pts)-(_closed?0:1)
     , pts = _closed?concat(pts,[pts[0]]):pts
     , _keep_pts= arg(keep_pts, opt, "include_ts",1)
     , _count= arg(count, opt, "count",10)
               - (_keep_pts? (len(pts)-(_closed?1:0)):0)
     )
  _count<=0 ? pts
  : let( dists= [for(i=[0:len(pts)-2]) dist( pts[i], pts[i+1])]
       , distRatios= [for(d=dists) d/sum(dists) ] 
       
       , npts_p_seg= [ for(r=distRatios) floor(r*_count) ]
                     // Set count of pts in each section depending
                     // on the len=> [2,5,3] with sum <= _count
                       
       , residue_pts = _count-sum(npts_p_seg)
       , ratios = [ for( is = range(nsegs) )
                      concat( 
                        _keep_pts? [0]:[]
                        , quicksort(
                              [ for(i=range( npts_p_seg[is] 
                                           + (is<residue_pts?1:0) 
                                           )) 
                                rands(0,1,1)[0] 
                              ]
                              )
                      , is==nsegs-1 && _keep_pts && !_closed? [1]:[]  
                      )  
                   ]
       , rtn= [ for(si= range(nsegs)) 
                 each [ for(rs=ratios[si] ) 
                         onlinePt( [ pts[si], pts[si+1]], ratio= rs )
                      ]
              ]            
       )
      //echo( log_b("randPtsOnline", lv))
      //echo( log_(["count",count,"_count",_count] , lv))
      //echo( log_(["closed",closed,"_closed",_closed] , lv))
      //echo( log_(["len(pts)",len(pts), "nsegs",nsegs] , lv))
      //echo( log_(["_keep_pts",_keep_pts] , lv))
      //echo( log_(["dists",dists], lv))
      //echo( log_(["distRatios", distRatios], lv))
      //echo( log_(["sum_distRatios(=1)", sum(distRatios)], lv))
      //echo( log_(["npts_p_seg", npts_p_seg], lv))
      //echo( log_(["_sum_npts_p_seg (&lt;=_count)", sum(npts_p_seg)] , lv))
      //echo( log_(["residue_pts", residue_pts] , lv))
      //echo( log_(["ratios (arr of ratios)", ratios ], lv)) 
      //echo( log_(["len(rtn)", len(rtn) ], lv)) 
      //echo( log_e("", lv))  
      rtn
);
 
 
//function randVibratePt(pt,d=0.2, x,y,z, opt)=
//(  pt+ randPt(d,x,y,z, opt)
//);
//
//    randVibratePt=["randVibratePt", "pt,d,x,y,z", "pts", "Random, Point",
//    "
//     Given a pt, randomly move it in the range within    
//      the range specified (see randPt)
//   
//     This is used to generate noise.
//    "];
//     
//    function randVibratePt_test( mode=MODE, opt=[] )=
//    (
//        doctest( randVibratePt, mode=mode, opt=opt )
//    );
//
//function randVibratePts(pts,d, x,y,z, opt)=
//(  [ for(pt=pts) randVibratePt(pt,d,x,y,z,opt) ]
//);

function get_scadx_rand_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_rand.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_rand_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_rand", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      pqr_test( mode, opt )
    , pqr90_test( mode, opt )
    , rand_test( mode, opt )
    //, randConvexPlane_test( mode, opt ) // to-be-coded
    //, randCoordPts_test( mode, opt ) // to-be-coded
    , randInPlanePt_test( mode, opt )
    , randInPlanePts_test( mode, opt )
    //, randOfflinePt_test( mode, opt ) // to-be-coded
    //, randOnPlaneConvexPts_test( mode, opt ) // to-be-coded
    , randOnPlanePt_test( mode, opt )
    , randOnPlanePts_test( mode, opt )
    //, randPlane_test( mode, opt ) // to-be-coded
    , randPt_test( mode, opt )
    //, randPts_test( mode, opt ) // to-be-coded
    //, randPtsAlongLine_test( mode, opt ) // to-be-coded
    //, randPtsOnline_test( mode, opt ) // to-be-coded
    , randRightAnglePts_test( mode, opt )
    , randc_test( mode, opt )
    //, randchain_test( mode, opt ) // to-be-coded
    ]
);
