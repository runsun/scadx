



//========================================================

module echo_(s, arr=undef)
{
	if(arr==undef){ echo(s); }
	else if (isarr(arr)){
		echo( _s(s, arr));
	}else { echo( str(s, arr) ); }
}



//========================================================

module echoblock(arr, indent="  ", v="| ",t="－",b="－", lt=".", lb="'")
{
	lines = arrblock(arr, indent=indent, v=v,t=t,b=b, lt=lt, lb=lb);

	_Div( join(lines,"<br/>")
		, "font-family:Droid Sans Mono"
		);  
}
 

//========================================================

module echoh(s, h, showkey) 
{  echo( _h(s, h, showkey=showkey));
    
    
}

//========================================================

module echofh(h, pairbreak=" , "
			  , keywrap="<b>,</b>"
			  , valwrap="<i>,</i>"
              , eq = "="
              , iskeyfmt= false
              ) 
{  echo( _fmth(h, pairbreak=pairbreak
			  , keywrap=keywrap
			  , valwrap=valwrap
              , eq = eq
              , iskeyfmt= iskeyfmt
    ));
}

//========================================================

module echom(mname, mti)
{
    indent = isint(mti)?repeat("&nbsp;",2*mti):"";
    
    echo( str( "<span style='color:navy'><code>"
             , indent //mti==undef? "": repeat("&nbsp;",2*mti)
             //, "", mname, " ---------------------</code></span>"
             , "&lt;", mname, "&gt;:"
             , isint(mti)?mti:"", "---------------------</code></span>"
             ) ); 
                    
    
//	echo_( _code(_red(
//    "--------------&lt;  {_}  &gt;--------------"))
//         , [ mname ] );
}


//echom_demo();
//accum_test(["mode",2]);




//=========================================
function toHtmlColor(colorArray)= // Convert return of randc 
( 
  let( ca = [ for(c=colorArray) int(c*255) ] )                          
  _s("rgb({_},{_},{_})",ca)
);
    
      
//========================================================

function _fmth(h
//              , wrap="[ , ]"
//              , pairbreak=" , "
//			  , keywrap="<b>,</b>"
//			  , valwrap="<i>,</i>"
//              , pairwrap=","
//              , eq = "="
//              , iskeyfmt= false
//              // New 2015.3.16: if key is sectionhead or comment, convert pair
//              // to section head and comment, respectively
//              , sectionhead= "###"
//              , comment= "///"
              , _i=0
              , _rtn=""
              
			  )=
(  
    let( 
         newrtn = str( _rtn, "<b>", h[_i],"</b>="
                           , _fmt( h[_i+1]), _i<len(h)-2?", ":"")
       , newi = _i+2                    
       )
    _i>len(h)-2? str("[", _rtn,"]")
    : _fmth( h, _i=newi, _rtn=newrtn)    

);     
  
//========================================================
function _f(x,fmt, sp="|", pad=" ")=  
    //:fmt: "bui^v%a/w?a?p?d?/w={,}/x=in:ftin/w=/a=/n=/s=/c=/bc=/aj=, "
(  
    let( fs= split(fmt,sp)
       , f1= fs[0]  // single char setting
       , f2= fs[1]  // 2-char setting 
       , f3= joinarr( [for( kp= slice(fs,2) )  // complex setting
                              split(kp,"=") ] )
       
       // Set %
       , pctg = has(f1,"%")
       , sn = num(x) * ( pctg?100:1 )
       , isnum= isnum(sn)
       , conv = hash(f3,"x")
       
       // Set d (decimal point) and unit conversion
       , _s = isnum? 
                //str( 
                    numstr( sn
                       , d= num( hash( f2, "d") )
                       , conv= pctg?undef:conv // disable uconv
                       )                       // if pctg
                  // , pctg?"%":""
                  // )
              : x
       
       // width
       , _w = hash( f2, "w" )
       , _wn = num(_w)
       , w= _w? (isnum(_wn)? _wn
                            : asc(_w)-87)
              : undef
       , al = hash( f2, "a", "r" ) // align
       , _pad= hash( f2, "p", pad ) // padding
       , dw = w-len(_s)-(pctg?1:0)
       , spad= dw<0? str(slice( _s,0,w-(pctg?1:0)), pctg?"%":"")
            : dw>0? 
               str( al=="r"? repeat( _pad, dw)
                    :al=="c"? repeat( _pad, dw/2):""         
                  , _s
                  , pctg?"%":""
                  ,  al=="l"? repeat( _pad, dw)
                    :al=="c"? repeat( _pad, dw/2-(dw%2?1:0)):""              )
            : str( _s, pctg?"%":"")
       //, spctg= isnum && pctg? str(sp,"%"):sp
       , sb= has(f1,"b")?_b(spad):spad//ctg
       , su= has(f1,"u")?_u(sb):sb
       , si= has(f1,"i")?_i(su):su
       , s_sup= has(f1,"^")? str("<sup>",si,"</sup>")
                :has(f1,"v")? str("<sub>",si,"</sub>"):si
                
       , wrap= hash( f3, "wr")
       , sw= wrap? replace( wrap,",",s_sup):s_sup
       
       , color= hash( f3, "c")
       , sc= color? _color(sw,color): sw 
       
       , bc= hash( f3, "bc")
       , sbc= bc? _bcolor(sc,bc): sc 
       
       // Don't know why fs and sty doesn't work. Leave it for now
       //, fs = hash( f3, "fs")
       //, sf = fs? _span( sbc, s=str("font-size:",fs,"px;") ): sbc
       //, sty = hash( f3, "sty")
       //, ss  = sty? _span( sbc, s=sty): sbc
        
       , srtn= sbc //ss //bc //sf
       , stya= hash( f3, "a")
       , styn= hash( f3, "n")
       , stys= hash( f3, "s")
       , rtn = styn && isnum? replace(styn,",",srtn)
               : stys && isstr(x) ?replace(stys,",",srtn)
               : stya && isarr(x) ?replace(stya,",",srtn):srtn
       )
   
   
   isarr(x)&& has(f1,"a")?
   str("["
      , join([ for(item=x)
                _f( item,fmt= str( replace(f1,"a","")
                            , sp, join( slice( fs, 1), sp )
                            )  
             , sp=sp, pad=pad) ]
             ,hash( f3,"aj",", ")
             )
      ,"]")
   :rtn 
   //[fs,sf, rtn]
 );

//========================================================

function _fmt(x, s=_span("&quot;,&quot;", "color:purple") 
			, n=_span(",", "color:brown") //magenta")//blueviolet")//brown")
			, a=_span(",", "color:navy")//midnightblue")//cadetblue")//mediumblue")//steelblue") //#993300") //#cc0060")  
			, ud=_span(",", "color:brown") //orange") 
			//, level=2
			, fixedspace= true
		   )=
(
	x==undef? replace(ud, ",", x) 
	:isarr(x)? replace(a, ",", replace(str(x),"\"","&quot;")) 
	: isstr(x)? replace(s,",",fixedspace?replace(str(x)," ", "&nbsp;"):str(x)) 
		:isnum(x)||x==true||x==false?replace(n,",",str(x))
				:x
);

//========================================================

function _h(s,h, showkey=false, wrap="{,}",isfmt=true, _i_=0)= 
( 
	len(h)<2?s
	:_i_>len(h)?s
	  :_h(
		  replace(s, replace(wrap,",",h[_i_])
				, str( showkey?h[_i_]:"", showkey?"=":""
					, h[_i_+1] //isfmt?_fmt(h[_i_+1]):h[_i_+1] 
					)
				)
		 , h
		 , wrap= wrap
		 , showkey= showkey
		 , _i_=_i_+2
		 )
);

//========================================================

module _mdoc(mname, s){ 
	echo(_pre(_code(
			str("<br/><b>"
			, mname
			, "()</b><br/><br/>"
			, parsedoc(s)
			)
			,s="color:blue"
		)));
}				  


//========================================================
parsedoc=["parsedoc","string","string", "Doc",
 " Given a string for documentation, return formatted string.
"];

function parsedoc( s
			, prefix ="|"
			, indent =""
			, replace=[]
			, linenum
			)=
 let( s   = split(s, ";;")	)
(
	
  join(
	[for( i=range(s) )
		 str( indent
			, prefix
			, isnum(linenum)?str((linenum+i),". "):"" 
			, replace
			  (
				replace
				( 
				  replace( s[i], "''", "&quot;" )
				, "<", "&lt;"
				)
			  , ">", "&gt;"
			  )
			)
	],"<br/>")									
);


//========================================================
function _s(s,arr, sp="{_}", _rtn="", _i=0, _j=0)= 
( 
    let( arr= !isarr(arr)?[str(arr)]:arr
       , m= match_at( s, _i, ["{",str(A_zu,0_9d),"}"] )  // false | [2,4,"{_}"]
       , add= m? (_j<len(arr)?arr[_j]:sp):s[_i]
       , _rtn = str( _rtn, add)
       , _i = _i+ (m? len(sp):1)
       , _j = _j+ (m? 1:0)
       )
    !s?s
    :_i>=len(s)? _rtn
    : _s( s, arr, sp, _rtn, _i,_j )
       
//	index(s,sp)>=0 && len(arr)>_i ?
//		str( slice(s,0, index( s,sp))
//			, arr[_i] 
//			,  index(s,sp)+len(sp)==len(s)?
//				"":(_s( slice( s, index(s,sp)+len(sp)), arr, sp, _i+1))
//		   ): s
);

function _s2( s
    , data= []
    , beg="{"
    , df_fmtchars= str(A_zu,0_9d,"-*`|=%/,:")
    , morefmtchars=""
    , end="}"
    , show=undef
    
//    , _debug=[]
    )=
(
    let( data= isarr(data)?data:[str(data)]
       , ps= [ beg
             , str(df_fmtchars, morefmtchars)
             , end
             ]
       , m = match(s,ps=ps) 
             //: [ [2,4,  "{2}", ["{","2","}"]]
             //  , [9,11, "{5}", ["{","5","}"]] ]
       , ss= m?
             join( 
               [for( mi = range(m) )
                   
                let( mx= m[mi] //:[2,4,"{2}",["{","2","}"]]
                   , i = mx[0] 
                   , j = mx[1]
                   , ms= mx[2] //: ms:entire matched string "{2}"
                   , mcs= split(mx[3][1],"`") 
                               //: mcs:content between { and }
                               
                   , mc= mcs[0] //: _, *, int, name
                   , fmt= mcs[1]
                   , nmc= num(mc)
                   , x = isnum(nmc)?
                            (inrange(data, nmc)? 
                                get(data, nmc): ms)
                         : mc=="_"? or(data[mi], ms)
                         : mc=="*"? data
                         : hash(data, mc,ms)
                   )
                str( slice( s,mi==0?0:(m[mi-1][1]+1),i )
                   //, "["
                   , fmt ? _f(x,fmt):x
                  // , "]"
                   , mi==len(m)-1? slice(s,j+1):""
                   )
               ]//_for
             )//_join
             :s
       )//_let
    show? hash( [ "df_fmtchars", df_fmtchars
                , "patterns", ps
                , "matched", m
                ], show )
    :ss
);  


  