include <scadx_core.scad>
include <../doctest/doctest.scad>

//include <../doctest/doctest2_dev.scad>
//
// The funcs here are usually called by scadx_doctesting_dev.scad.
// To test them directly in this file, un-mark the following line:
//include <../doctest/doctest_dev.scad>
//
FORHTML=false;
MODE=11;

/* 
=================================================================
                    scadx_core_test.scad
=================================================================
*/
/*
      fname = doct[ DT_iFNAME ]  // function name like "sum"
	, args  = doct[ DT_iARGS ]   // argument string like "x,y"
  //, rtntype = doct[ DT_iTYPE ] // return type
    , tags  = doct[ DT_iTAGS ]   // like, "Array, Inspect"
  //, doc   = doct[ DT_iDOC ]    // documentation (str)
    , rels  = doct[ DT_iREL ]
*/

//=====================================================================
lpCrossPt=["lpCrossPt", "pq,tuv", "point", "Point,Geometry",
 " Given a 2-pointer for a line (pq) and a 3-pointer for a plane (tuv),
;; return the point of intercept between pq and tuv. 
;;
    m=dist(pq)

    P   m
    |'-._	  
    |    'Q._  n
    |dp   |  '-._
    |     |dq    '-.
   -+-----+----------X-

    (m+n)/m = dp/(dp-dq) 

    onlinePt( pq, ratio = dp/(dp-dq) )

"];
function lpCrossPt_test( mode=MODE, opt=[] )=
(
	doctest( lpCrossPt, mode=mode, opt=opt )

);


////==================================================
//minPt=[ "minPts", "pts", "array", "Point, Math",
//"Given an array of points (pts), return a pt that
//;; has the min x then min y then min z.
//"];
// 
//function minPt_test( mode=MODE, opt=[] )=
//(	
//    let( pts1= [[2,1,3],[0,2,1] ]
//	   , pts2= [[2,1,3],[0,3,1],[0,2,0], [0,2,1]]
//       )
//	
//	doctest( minPt,
//	[
//      "var pts1", "var pts2"
//    , [ minPts( pts1 ), [0,2,1], "pts1" ]
//    , [ minPts( pts2 ), [0,2,0], "pts2" ]
//    ]
//    , mode=mode, opt=opt, scope=["pts1", pts1, "pts2", pts2]
//	)
//);
//
////==================================================
//function minxPts_test( mode=MODE, opt=[] )=(//======================
//	
//    let( pts1= [[2,1,3],[0,2,1] ]
//	   , pts2= [[2,1,3],[0,3,1],[0,2,0]]
//       )
//	
//	doctest( minxPts,
//	[
//      "var pts1", "var pts2"
//    , [ minxPts( pts1 ), [[0,2,1]], "pts1" ]
//    , [ minxPts( pts2 ), [[0,3,1],[0,2,0]], "pts2" ]
//    ]
//    , mode=mode, opt=opt, scope= ["pts1", pts1, "pts2", pts2]
//	)
//);
//
////==================================================
//function minyPts_test( mode=MODE, opt=[] )=(//==================
//	
//    let( pts1= [[2,1,3],[0,2,1],[3,3,0]]
//	   , pts2= [[2,1,3],[0,3,1],[3,1,0]]
//	   )
//       
//	doctest( minyPts, 
//    [
//      "var pts1", "var pts2"
//    , [ minyPts( pts1 ), [[2,1,3]], "pts1" ]
//    , [ minyPts( pts2 ), [[2,1,3],[3,1,0]], "pts2" ]
//    ]
//    ,mode=mode, opt=opt, scope=["pts1", pts1, "pts2", pts2]
//	)
//);
//
////==================================================
//function minzPts_test( mode=MODE, opt=[] )=(//==================
//	
//    let( pts1= [[2,1,3],[0,2,1]]
//	   , pts2= [[2,1,3],[0,3,1],[0,2,1]]
//	   )
//	doctest( minzPts,
//    [
//      "var pts1", "var pts2"
//    , [ minzPts( pts1 ), [[0,2,1]], "pts1"  ]
//    , [ minzPts( pts2 ), [[0,3,1],[0,2,1]], "pts2" ]
//    ]
//    , mode=mode, opt=opt, scope=["pts1", pts1, "pts2", pts2]
//	)
//);

//==================================================
		

//module normalPt_demo()
//{
//	echom("normalPt");
//	pqr = randPts(3);
//	P = pqr[0];  Q=pqr[1]; R=pqr[2];
//	Chain( pqr, ["closed", false] );
//	N = normalPt( pqr );
//	echo("N",N);
//	MarkPts( concat( pqr,[N]) , ["labels","PQRN"] );
//	//echo( [Q, N] );
//	Mark90( [N,Q,P] );
//	Mark90( [N,Q,R] );
//	Dim( [N, Q, P] );
//	Line0( [Q, N], ["r",0.02,"color","red"] );
//	Nr = normalPt( pqr, len=1, reversed=true);
//	echo("Nr",Nr);
//	Mark90( [Nr,Q,P] );
//	Mark90( [Nr,Q,R] );
//	Dim( [Nr, Q, P] );
//	Line0( [Q, Nr], ["r",0.02,"color","green"] );
//
//}
//normalPt_demo();
//Dim_test( );


//========================================================
//packuni=["packuni","uni, prefix=''#/''","arr", "Array"
//,"Given an arr with a data structure called *uni*, an arr of one-dimensional
//;; arrs, each for a block:
//;;
//;;   [[''#/1'', ''*'', ''#/2''], [''x'', ''+'', 1], [''y'', ''+'', 2]]
//;;
//;; which is a return of getuni( ''(x+1)*(y+2)'' ), pack and stack those 
//;; blocks into an array it represents:  
//;;
//;;   [ [''x'', ''+'', 1], ''*'', [''y'', ''+'', 2] ]
//"];
//
//function packuni_test( mode=MODE, opt=[] )=
//(
//   doctest( packuni
//   ,[ 
//    
//     [ packuni([["a","#/1"], ["b","#/2"], ["c","d"]])
//      , ["a",["b",["c","d"]]]
//      , [ ["a","#/1"], ["b","#/2"], ["c","d"]]
//     ]
//
//   , [ packuni( [["#/1"], ["a","x","#/2"], ["b","#/3"], ["c","d"]])
//     , [["a", "x", ["b", ["c", "d"]]]]
//     , [ ["#/1"], ["a","x","#/2"], ["b","#/3"], ["c","d"]]
//     ]
//
//   , [ packuni( [ ["#/1","*","#/2"], ["x","+","1"], ["y","+","2"]] )
//     , [["x", "+", "1"], "*", ["y", "+", "2"]]
//     , [["#/1","*","#/2"], ["x","+","1"], ["y","+","2"]]
//     ]
//
//    ,[ packuni( [["#/1","*","#/2"], ["x","+","1"], ["sin","y","+","2"]] )
//     , [["x", "+", "1"], "*", ["sin", "y", "+", "2"]]
//     , [["#/1","*","#/2"], ["x","+","1"], ["sin","y","+","2"]]
//     ]
//         
//    ,[ packuni( [["#/1", "*", "#/2"], ["x", "+", 1], ["y", "+", 2]] )
//     , [["x", "+", 1], "*", ["y", "+", 2]]
//     , [["#/1", "*", "#/2"], ["x", "+", 1], ["y", "+", 2]]
//     ]
//     
////     ,[  [["#/1", "^"], ["left_arm", "+", 1]]
////     , packuni( [["#/1", "^"], ["left_arm", "+", 1]] )
////     , []
////     ]
////     
////     ,[  [["#/1", "+", 2], ["left_arm", "+", 1]]
////     , packuni( [["#/1", "+", 2], ["left_arm", "+", 1]] )
////     , []
////     ]
//     
////     ,[  [["#/1", "^", 2], ["left_arm", "+", 1]]
////     , packuni( [["#/1", "^", 2], ["left_arm", "+", 1]] )
////     , []
////     ]
//     
// 
//    ], mode=mode, opt=opt )
//);

//========================================================
//function randc_demo()
//{
//	light = [0.8,1];
//	dark  = [0,0.4];
//	translate(randPt()) color(randc(r=dark,g=dark,b=dark)) sphere(r=0.5);
//	translate(randPt()) color(randc()) sphere(r=1);
//	translate(randPt()) color(randc(r=light, g=light,b=light)) sphere(r=1.5);
//}

//========================================================


//========================================================
ribbonPts=["ribbonPts","pts,len=1,paired=true,closed=false", "Points", "Point"
, ""];
function ribbonPts_test( mode=MODE, opt=[] )=
(
    doctest( ribbonPts,
	[
    ]
    ,
    mode=mode, opt=opt
	)
);


////========================================================
//roll=["roll", "arr,count=1", "arr", "Array"
//," Given an array (arr) and an int (count), roll arr items
//;; as shown below:
//;;
//;; roll( [0,1,2,3] )   => [0,1,2],[3] => [3],[0,1,2] => [3,0,1,2]
//;; roll( [0,1,2,3],-1) => [0],[1,2,3] => [1,2,3],[0] => [1,2,3,0] 
//;; roll( [0,1,2,3], 2) => [0,1],[2,3] => [2,3],[0,1] => [2,3,0,1]
//;;
//;; Compare range w/cycle, which (1) returns indices (2) doesn't 
//;; maintain array size:
//;;
//;; |> obj = [10, 11, 12, 13]
//;; |> range( obj,cycle=1 )= [0, 1, 2, 3, 0]
//;; |> range( obj,cycle=2 )= [0, 1, 2, 3, 0, 1]
//;; |> range( obj,cycle=-1 )= [3, 0, 1, 2, 3]
//;; |> range( obj,cycle=-2 )= [2, 3, 0, 1, 2, 3]
//
//"];
//
//function roll_test( mode=MODE, opt=[] )=
//(
//	doctest( roll,
//	[
//      [ roll([0,1,2,3]), [3,0,1,2] , "[0,1,2,3]"]
//    , [ roll([0,1,2,3],-1), [1,2,3,0] , "[0,1,2,3],-1"]
//    , [ roll([0,1,2,3], 2), [2,3,0,1] , "[0,1,2,3], 2"]
//    , [ roll([0,1,2,3,4,5], 2), [4,5,0,1,2,3] , "[0,1,2,3,4,5], 2"]
//    , [ roll([0,1,2,3],0), [0,1,2,3] , "[0,1,2,3],0"]
//    ], mode=mode, opt=opt
//	)
//);

////========================================================
//shift= [ "shift", "o,i=1", "str|arr", "String,Array" ,
//" Return a str|arr short than o by one on the right side
//;; = slice(o,0,-1).
//" 
//];
//   
//function shift_test( mode=MODE, opt=[] )=
//(
//	doctest( shift ,
//    [
//      [ shift([2,3,4]), [2,3] , "[2,3,4]"]
//    , [ shift([2]), [] , "[2]" ]
//    , [ shift([]), [] , "[]" ]
//    , [ shift( "234" ), "23"  , "'234'"]
//    , [ shift( "2" ), ""  , "'2'"]
//    , [ shift( "" ), ""  , "'"]
//	],mode=mode, opt=opt
//
//	)
//);
////shift_test( ["mode",22] );


////========================================================
//shrink= [ "shrink", "o,i=1", "str|arr", "String, Array",
//"
// Return a str|arr short (on the left side) than o by one.\\
//"
//];
//
//function shrink_test( mode=MODE, opt=[] )=
//(
//	doctest( shrink, 
//	[
//      [ shrink([2,3,4]), [3,4], [2,3,4] ]
//    , [ shrink([2]), [], [2] ]
//    , [ shrink( "234" ), "34",  "'234'" ]
//    , [ shrink( "2" ), "", "'2'" ]
//    , [ shrink( "" ), "", "'" ]
//    ], mode=mode, opt=opt
//	)
//);
////shrink_test( ["mode",22] );

////========================================================
//shuffle=["shuffle", "arr", "array", "Array",
//" Given an array (arr), return a new one with all items randomly shuffled.
//"];
//
//function shuffle_test( mode=MODE, opt=[] )=
//(
//	let( arr = range(6)
//	   , arr2= [ [-1,-2], [2,3],[5,7], [9,10]] 
//       )
//       
//	doctest( shuffle,
//	[
//		str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr2)= ", shuffle(arr2) )
//		,str( "shuffle(arr2)= ", shuffle(arr2) )
//		,str( "shuffle(arr2)= ", shuffle(arr2) )
//	]
//    , mode=mode, opt=opt, scope=["arr", arr, "arr2", arr2]
//	)
//);
////shuffle_test();

//========================================================

//function subops_test( opt=[] )=
//(
//    /*
//       Let an obj has 2 sub unit groups: arms and legs, each has L and R
//       Obj: arms{armL,armR}
//          : legs{legL,legR}
//       
//       com_keys for arms: ["color","r"]
//       com_keys for legs: ["color","len"]
//       
//       
//    
//    
//    */
//    //=========================================
//    
//    function tester( 
//    // The following are to set defaults on the fly
//    //   for checking how different default settings work.
//    //   I.e., in real use, don't do it this way.
//    // Usage: as a test rec: 
//    //   [
//              u_ops
//              , df_color = undef
//              , df_r     = undef
//              , df_len   = undef
//              , df_show_arms = undef 
//              , df_show_armL = undef 
//              , df_show_armR = undef
//              , df_show_legs = undef
//              , df_show_legL = undef
//              , df_show_legR = undef 
//              
//              , df_arms = undef 
//              , df_armL = undef 
//              , df_armR = undef
//              , df_legs = undef
//              , df_legL = undef
//              , df_legR = undef 
//              
//              , com_keys_arms= ["color","r"]
//              , com_keys_legs= ["color","len"]
//    ){
//        df_ops= updates(
//            [
//              df_color==undef? []:["color", df_color]
//            , df_r    ==undef? []:["r",     df_r]
//            , df_len  ==undef? []:["len",   df_len]
//        
//            , df_show_arms ==undef?[]:["df_arms", df_show_arms]
//            , df_show_armL ==undef?[]:["df_armL", df_show_armL]
//            , df_show_armR ==undef?[]:["df_armR", df_show_armR]
//            , df_show_legs ==undef?[]:["df_legs", df_show_legs]
//            , df_show_legL ==undef?[]:["df_legL", df_show_legL]
//            , df_show_legR ==undef?[]:["df_legR", df_show_legR]
//            ]
//        );
//        
//        function get_ops( df_subs, com_keys )=
//            subops( u_ops
//                  , df_ops = df_ops
//                  , df_subs= df_subs
//                  , com_keys=com_keys
//                  );
//        function get_arm_ops( armname, df)=
//            get_ops( df_subs= [ "arms", df_arms, armname,df]
//                   , com_keys= com_keys_arms
//                   );
//        function get_leg_ops( legname, df)=
//            get_ops( df_subs= [ "legs", df_legs, legname,df]
//                   , com_keys= com_keys_legs
//                   );
//                   
//        // We need to set 4 ops:
//        
//        ops_armL = get_arm_ops( "armL", df_armL );
//        ops_armR = get_arm_ops( "armR", df_armR );
//        ops_legL = get_leg_ops( "legL", df_legL );
//        ops_legR = get_leg_ops( "legR", df_legR );
//            
//        
//    }
////    
////    opt=[];
////    df_arms=["len",3,"r",1];
////    df_armL=["fn",5];
////    df_armR=[];
////    df_ops= [ "len",2
////            , "arms", true
////            , "armL", true
////            , "armR", true
////            ];
////    com_keys= ["fn"];
////    
////    /*
////        setops: To simulate obj( ops=[...] )
////        args  : ops  
////                // In real use, following args are set inside obj()
////                df_ops
////                df_subops // like ["arms",[...], "armL",[...]] 
////                com_keys 
////                objname   // like "obj","armL","armR"          
////                propname  // like "len"
////    */
////    function setops(   
////            ops=opt
////            , df_ops=df_ops
////            , df_subops=[ "arms",df_arms, "armL",df_armL,"armR",df_armR]
////            , com_keys = com_keys
////            , objname ="obj"
////            , propname=undef
////            )=
////        let(
////             _ops= update( df_ops, ops )
////           , df_arms= hash(df_subops, "arms")  
////           , df_armL= hash(df_subops, "armL")  
////           , df_armR= hash(df_subops, "armR")  
////           , ops_armL= subops( _ops, df_ops = df_ops
////                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]                    
////                    , com_keys= com_keys
////                    )
////            , ops_armR= subops( _ops, df_ops = df_ops
////                        , sub_dfs= ["arms", df_arms, "armR", df_armR ]
////                        , com_keys= com_keys
////                        )
////            , ops= objname=="obj"?_ops
////                    :objname=="df_ops"?df_ops
////                    :objname=="armL"?ops_armL
////                    :ops_armR
////            )
////        // ["ops",_ops,"df_arms", df_arms, "ops_armL",ops_armL, "ops_armR",ops_armR];
////        propname==undef? ops:hash(ops, propname);
////      
////     /**
////        Debugging Arrow2(), in which a getsubops call results in the warning:
////        DEPRECATED: Using ranges of the form [begin:end] with begin value
////        greater than the end value is deprecated.
////            
////     */       
////     function debug_Arrow2(ops=["r", 0.1, "heads", false, "color","red"])=
////        let (
////        
////           df_ops=[ "r",0.02
////               , "fn", $fn
////               , "heads",true
////               , "headP",true
////               , "headQ",true
////               , "debug",false
////               , "twist", 0
////               ]
////          ,_ops= update(df_ops,ops) 
////          ,df_heads= ["r", 1, "len", 2, "fn",$fn]    
////          , ops_headP= subops( _ops, df_ops=df_ops
////                , com_keys= [ "color","transp","fn"]
////                , sub_dfs= ["heads", df_heads, "headP", [] ]
////                ) 
////        )
////        ops_headP;
////        
////     doctest(subops
////        , [ ""
////            ,"In above settings, ops is the user input, the rest are"
////            ,"set at design time. With these:"
////            ,""
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops"
////                 , setops()
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops_armL"
////                 , setops(objname="armL")
////                 , ["len", 3, "r", 1, "fn", 5]]
////            ,["ops_armR"
////                 , setops(objname="armR")
////                 , [ "len", 3, "r", 1]]
////                            
////            ,""
////            ,"Set r=4 at the obj level. Since r doesn't show up in com_keys, "
////            ,"it only applies to obj, but not either arm. "
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true],]
////            ,["ops"
////                 , setops(ops=["r",4])
////                 , ["len",2,"arms",true,"armL", true, "armR", true,"r",4],]
////            ,["ops_armL"
////                 , setops(ops=["r",4],objname="armL")
////                 , ["len", 3, "r", 1,"fn", 5]]
////            ,["ops_armR"
////                 , setops(ops=["r",4],objname="armR")
////                 , [ "len", 3, "r", 1]]
////                            
////            ,""
////            ,"Set fn=10 at the obj level. Since fn IS in com_keys, "
////            ,"it applies to obj and both arms. "
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true],]
////            ,["ops"
////                 , setops(ops=["fn",10])
////                 , ["len",2,"arms",true,"armL", true, "armR", true, "fn",10],]
////            ,["ops_armL"
////                 , setops(ops=["fn",10],objname="armL")
////                 , ["len", 3, "r", 1, "fn", 10]]
////            ,["ops_armR"
////                 , setops(ops=["fn",10],objname="armR")
////                 , ["len", 3, "r", 1, "fn", 10]]  
////                 
////        
////            ,""
////            ,"Disable armL:"
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops"
////                 , setops(ops=["armL",false])
////                 , ["len",2,"arms",true,"armL", false, "armR", true]]
////            ,["ops_armL"
////                 , setops(ops=["armL",false],objname="armL")
////                 , false]
////            ,["ops_armR"
////                 , setops(ops=["armL",false],objname="armR")
////                 , ["len", 3, "r", 1]]
////
////
////            ,""
////            ,"Disable both arms:"
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops"
////                 , setops(ops=["arms",false])
////                 , ["len",2,"arms",false,"armL", true, "armR", true]]
////            ,["ops_armL"
////                 , setops(ops=["arms",false],objname="armL")
////                 , false]
////            ,["ops_armR"
////                 , setops(ops=["arms",false],objname="armR")
////                 , false]
////        
////        
//////            ,""
//////            ,"Debugging Arrow2(), in which a getsubops call results in the warning:"
//////            ,"DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated."
//////            ,""
//////            
//////            ,["debug_Arrow2(ops=['r', 0.1, 'heads', false, 'color','red']"
//////             , debug_Arrow2(ops=["r", 0.1, "heads", false, "color","red"]),[]
//////             ]
////                            
////          ]
////          
////        , ops, [ "df_ops",df_ops, 
////               , "df_arms", df_arms
////               , "df_armL", df_armL
////               , "df_armR", df_armR
////               , "com_keys", com_keys
////               , "ops",opt
////               ]
////    )
//);

////========================================================
//sum=["sum", "arr", "number", "Math, Array",
//"Return the sum of arr's items, which could be pts.  
//
//"
//];
//function sum_test( mode=MODE, opt=[] )=
//(
//    doctest( sum, 
//    [
//      [ sum([2,3,4,5]),14, [2,3,4,5]]
//    , [ sum([[1,2,3],[4,5,6]]),[5,7,9], [[1,2,3],[4,5,6]]]
//    ], mode=mode, opt=opt )
//);
//
////echo( sum_test(mode=12)[1] );
//
////========================================================
//sumto =[  "sumto", "n", "int", "Math" ,
//"
// Given n, return 1+2+...+n 
//"
//];
//
//function sumto_test( mode=MODE, opt=[] )=
//(
//    doctest( sumto, 
//    [
//      [ sumto(5), 15, "5" ]
//    , [ sumto(6), 21, "6" ]
//    ], mode=mode, opt=opt )
//);


////========================================================
//switch=["switch","arr,i,j","array", "Array",
// " Given an array and two indices (i,j), return a new array
//;; with items i and j switched. i,j could be negative. If either
//;; one out of range, return undef.
//"];
//
//function switch_test( mode=MODE, opt=[] )=
//(
//	let( arr=[2,3,4,5,6] )
//    
//	doctest( switch,
//	[		
//      [ switch(arr,1,2), [2,4,3,5,6] , "arr,1,2"]
//    , [ switch(arr,0,2), [4,3,2,5,6] , "arr,0,2"]
//    , [ switch(arr,0,-1), [6,3,4,5,2] , "arr,0,-1"]
//    , [ switch(arr,-1,-5), [6,3,4,5,2] , "arr,-1,-5"]
//    , [ switch(arr,-1,4), [2,3,4,5,6], "arr,-1,4", ["//","No switch"] ]
//
//	],mode=mode, opt=opt, ["arr", arr]
//
//	)
//);


////========================================================
//function subarr_test(mode=112)
//{
//	arr=[10,11,12,13];
//	doctest( subarr,
//	[
//		 ["arr", subarr( arr ), [[13,10,11],[10, 11,12],[11,12,13],[12,13,10]] ]
//		,["arr,cycle=false", subarr( arr,cycle=false ), [[10,11],[10, 11,12],[11,12,13],[12,13]] ]
//
//		,["arr,cover=[0,1]", subarr( arr,cover=[0,1] ), [[10,11],[11,12],[12,13],[13,10]] ]
//		,["arr,cover=[2,0]", subarr( arr,cover=[2,0] ), [[12,13,10],[13,10,11],[10,11,12],[11,12,13]] ]
//	],mode=mode, ["arr",arr]
//	);
//}


//========================================================


////========================================================
//transpose= [ "transpose", "mm", "mm",  "Array" ,
//"Given a multmatrix, return its transpose.
//"
//];
//
//function transpose_test( mode=MODE, opt=[] )=
//(
//	let( mm = [[1,2,3],[4,5,6]]
//	   , mm2= [[1,4],[2,5],[3,6]]
//       )
//       
//	doctest( transpose,
//    [
//      [  transpose(mm), mm2, mm ]
//    , [  transpose(mm2), mm, mm2 ]
//    ]
//    , mode=mode, opt=opt
//	)
//);
////transpose_test( ["mode",22] );

//========================================================
//uopt=["uopt","opt1,opt2,df=[]","opt","String,Hash,Core,Inspect",
//  "Merge sopt and opt allowing module to use both.
//;;
//;; See sopt() for definition of sopt.
//;;
//;;  MarkPts( pts, 'l,c,color=red', ['grid',true] )
//;;  MarkPts( pts, ['color','red'], 'l,c,color=red' )
//;;      
//;;  The shortcut definition, df=['l','label','c','chain']
//;;  is defined inside MarkPts()
//;; 
//;;  <b>Internal sopt</b>:
//;; 
//;;  The 1st 2 items are internal sopt:
//;;
//;;  opt = [<b>'sopt','l,c,r=3'</b>, 'color', 'red']
//;;
//"
//,"sopt,subopt1"
//];
//
//function uopt_test( mode=MODE, opt=[] )=
//(
//    doctest( uopt, 
//    [
//      [uopt("c,d"), ["c",true,"d",true], "'c,d'"]
//    , [uopt(["a",5]), ["a",5], ["a",5]]
//    , [uopt("c,r=3",["a",5])
//            , ["c",true,"r",3,"a",5]
//            , "'c,r=3',['a',5]"
//            , ["nl",false]]
//    , [uopt(["a",5],"c,r=3")
//            , ["a",5,"c",true,"r",3]
//            , "['a',5], 'c,r=3'"
//            , ["nl",false]]
//    , ""
//    , "// Define shortcuts using df: r represents rad"
//    , "// Note that 'a' is not changed to 'age' `cos "
//    , "// it`s on an hash but not a sopt."
//    , ""
//    , [uopt(["a",5],"c,r=3",df=["a","age","r","rad","c","chain"])
//            , ["a",5,"chain",true,"rad",3]
//            , "['a',5],'c,r=3',df=['a','age','r','rad','c','chain']'"
//            , ["nl",true]]
//
//            
//    ,""
//    ,"<b>Using internal sopt </b>"
//    ," " 
//    ,"The following case has opt1=hash, opt2=sopt. However,"
//    ,"opt1 also contains an <b>internal sopt</b>:"
//    ,""
//    ,"  ['a',5,<b>'sopt','d,e=1'</b>]"
//    ,""
//    ,"This is for cases of subobj opt:"
//    ,""
//    ,"  Rod(.... opt=['markpts', ['sopt','g,c','label'...]] )"
//    ,""    
//    ,"Let <b>df</b>=['d','date','r','rad','c','chain']"
//    ,""
//    , [uopt( ["a",5,"sopt","d,e=1"],"c,r=3"
//           , df=["d","date","r","rad","c","chain"]
//           )
//            , ["a",5,"date",true, "e",1, "chain",true,"rad",3]
//            , "['a',5,'sopt','d,e=1'],'c,r=3',df"
//            , ["nl",true]
//      ]
//    ]
//    , mode=mode, opt=opt )
//);
//echo( uopt_test( mode=112 )[1] );


//========================================================


//function vlinePt_test( mode=MODE, opt=[] )=
//(
//	doctest( vlinePt, mode=mode, opt=opt )
//);


//======================================
lineRotation=[ "lineRotation", "pq,p2", "?", "Angle"
, " The rotation needed for a line
;; on X-axis to align with line-p1-p2
"];

// The rotation needed for a line
// on X-axis to align with line-p1-p2
// 
function lineRotation(p1,p2)=
(
 [ 0,-sign(p2[2]-p1[2])*atan( abs((p2[2]-p1[2])
		                    / slopelen((p2[0]-p1[0]),(p2[1]-p1[1]))))
    , p2[0]==p1[0]?0
	  :sign(p2[1]-p1[1])*atan( abs((p2[1]-p1[1])/(p2[0]-p1[0])))]
);
  

//
// The funcs here are usually called by scadx_doctesting_dev.scad.
// To test them directly in this file, un-mark the following line:
//include <../doctest/doctest_dev.scad>
//
//FORHTML=false;
//MODE=112;

/* 
=================================================================
                    scadx_core_test.scad
=================================================================
*/
/*
      fname = doct[ iFNAME ]  // function name like "sum"
	, args  = doct[ DT_iARGS ]   // argument string like "x,y"
  //, rtntype = doct[ DT_iTYPE ] // return type
    , tags  = doct[ DT_iTAGS ]   // like, "Array, Inspect"
  //, doc   = doct[ DT_iDOC ]    // documentation (str)
    , rels  = doct[ DT_iREL ]
*/


//===================================================
//===================================================
//===================================================

module do_tests( title="Doctests for scadx_core"
                , mode=MODE, opt=["showmode",false] ){
    
    results= [
        accum_test( mode, opt ) //[math]
      /* ,addx_test( mode, opt ) // [geo]
       ,addy_test( mode, opt ) 
       ,addz_test( mode, opt )
       ,all_test( mode, opt ) //[core]
       ,angle_test( mode, opt ) //[geo]
      
       /////,angle_bylen( mode, opt )  //[geo] ///// we can use angle( [ line1,line2] )
     //  ,angleBisectPt_test( mode, opt ) //[geo]
     //  , anglePt_test( mode, opt ) //[geo] 
       //, any_test( mode, opt ) // [arr] //// needed             
       ////, anyAnglePt_test( mode, opt ) // to be replaced by anglePt. 2015.5.7             
     //  , app_test( mode, opt ) //[core]            
     //  , appi_test( mode, opt ) //[core]            
     //  , arcPts_test( mode, opt )  //[geo]           
       , arrblock_test( mode, opt ) //[arr]
       , arrprn_test( mode, opt )   //[arr]
      
                   
       , assert_test( mode, opt ) //[error]          
       
       , begmatch_test( mode, opt ) //[match]            
       /*, begwith_test( mode, opt )  //[insp]           
       */
       //, begword_test( mode, opt )  //[insp]           
       //, between_test( mode, opt )  //[range]           
       //, boxPts_test( mode, opt )      
       /*
       , calc_shrink_test( mode, opt )  //[math]     
       , calc_flat_test( mode, opt )    
       , calc_func_test( mode, opt ) 
       , centroidPt_test( mode, opt ) //[geo]
       
        , cirfrags_test( mode, opt ) //[geo]
       , _colorPts_test( mode, opt ) //[echo] ////
       , combine_test( mode, opt ) //[core]
       , coordPts_test( mode, opt ) //[geo]
       , 
       cornerPt_test( mode, opt ) //[geo]
       , countArr_test( mode, opt ) //[core]  
       , countInt_test( mode, opt ) //[core]  
       , countNum_test( mode, opt ) //[core]  
       , countStr_test( mode, opt ) //[core]  
       , countType_test( mode, opt ) //[core]
       , cubePts_test( mode, opt ) //[geo]
       , deeprep_test( mode, opt ) //[core] 
       , del_test( mode, opt )  //[core]
       , delkey_test( mode, opt )  //[core]
       , dels_test( mode, opt )  //[core]
       , det_test( mode, opt )  //[math]
       , dist_test( mode, opt ) //[geo] 
       , distPtPl_test( mode, opt )  //[geo]
       

       //, dor_test( mode, opt ) /////
       , dot_test( mode, opt ) //[math]
       , echoblock_test( mode, opt ) //[echo]
       , echo__test( mode, opt ) //[echo]
       , echofh_test( mode, opt )//[echo]
       , echoh_test( mode, opt )//[echo]
       , endwith_test( mode, opt ) //[insp]
       ////, endword_test( mode, opt ) //[insp] // We could use match
       , eval_test( mode, opt ) //[eval]
       , evalarr_test( mode, opt ) // [eval] //most complicated recur
       , expandPts_test( mode, opt ) //[geo]
       , fac_test( mode, opt ) //[match]
       , faces_test( mode, opt ) //[geo]
       
       , fidx_test( mode, opt )  //[rng]
        , flatten_test( mode, opt ) //[core]
        , _f_test( mode, opt ) // [echo]
        , _fmt_test( mode, opt ) // [echo]
        , _fmth_test( mode, opt ) // [echo]
        , ftin2in_test( mode, opt ) // [geo]
        , get_test( mode, opt ) //[rng]
        , getdeci_test( mode, opt ) //[core]
        , getSideFaces_test( mode, opt ) //[geo]
        
        , getv_test( mode, opt ) //[hash] //// needed
        
        , gtype_test( mode, opt )// [geo] /////
        ,_h_test( mode, opt ) //[echo]
        , has_test( mode, opt ) //[insp]
        , hash_test( mode, opt ) //[core]
        , hashex_test( mode, opt )//[core]
        , hashkvs_test( mode, opt )//[core]
        , haskey_test( mode, opt )//[core]
        , incenterPt_test( mode, opt ) //[geo]
        , idx_test( mode, opt )  //[rng]           
        ////, index_test( mode, opt )  //[rng]  //////// to be replaced by idx()          
        , inrange_test( mode, opt ) //[rng]
        , int_test( mode, opt )  //[insp]
        //, intsec( mode, opt ) //[geo] ////
        , is0_test( mode, opt )             
        , is90_test( mode, opt )            
        , isarr_test( mode, opt )            
        ////, isat_test( mode, opt ) ///// doesn't seem to be used anywhere 
        */
        //, isball_test( mode, opt )///// Need this func
        , isbool_test( mode, opt ) 
        //, iscir_test( mode, opt ) ///// Need this
        , iscoord_test( mode, opt)  //// need this
        , isequal_test( mode, opt )  
        , isfloat_test( mode, opt )  
        , ishash_test( mode, opt ) 
        //, isinline_test( mode, opt ) ///// NEED THIS 
        , isint_test( mode, opt )  
        , isln_test( mode, opt ) 
        //, isnan_test( mode, opt ) //// NEED THIS 
        , isnum_test( mode, opt )  
        , isonln_test( mode, opt ) //// NEED THIS
        , isOnPlane_test( mode, opt ) 
        , isparal_test( mode, opt )  
        , ispl_test( mode, opt )  
        , ispt_test( mode, opt )  
        /*
        //, ispts_test( mode, opt )  ///// NEEDED
        //, isSamePt_test( mode, opt ) //// NEEDED
        , isSameSide_test( mode, opt )  
        , isstr_test( mode, opt )  
        , istype_test( mode, opt )  
        , join_test( mode, opt )  
        , joinarr_test( mode, opt )  
        , keys_test( mode, opt )  //[core]
        , kidx_test( mode, opt )  //[core]
        , last_test( mode, opt )  // [rng]
        , lcase_test( mode, opt ) //[core]
        //, lineCrossPt_test( mode, opt )
        //, lineCrossPts_test( mode, opt )
        , linePts_test( mode, opt ) //[geo]
        , longside_test( mode, opt ) //[geo]
        
        //, lpCrossPt_test( mode ,opt ) // Do we wanna keep this ?
        
       , match_test( mode, opt ) //[match]            
       , match_at_test( mode, opt ) //[match]             
       , match_nps_at_test( mode, opt ) //[match]  
       , match_ps_at_test( mode, opt ) //[match]            
       , match_rp_test( mode, opt ) //[match]  
       , _mdoc_test( mode, opt ) //[echo]
       , midPt_test( mode, opt ) //[geo]
       , mod_test( mode, opt )  //[math]
       , normal_test( mode, opt )  //[geo]           
       , normalLn_test( mode, opt ) //[geo]            
       , normalPt_test( mode, opt ) //[geo]            
       , normalPts_test( mode, opt ) //[geo]
       I/
       //, notall_test( mode, opt )///// needed 
       /*, num_test( mode, opt )  //[core]
       , numstr_test( mode, opt ) //[core]             
       , onlinePt_test( mode, opt )  //[geo]           
       , onlinePts_test( mode, opt )  //[geo]           
       , or_test( mode, opt )  //[core]           
       , othoPt_test( mode, opt ) //[geo]
      //// , packuni_test( mode, opt ) 
       , parsedoc_test( mode, opt )             
       , permute_test( mode, opt )             
       , pick_test( mode, opt )   
       ///, planeAt_test( mode, opt ) ////
       , planecoefs_test( mode, opt )             
      // , planePts_test( mode, opt )////
      /// , plat_test( mode, opt ) //////
       , ppEach_test( mode, opt )  //[core]
      // , popt_test( mode, opt ) ////
       , pqr90_test( mode, opt )  //[geo]           
       , prod_test( mode, opt )             
       , projPt_test( mode, opt ) //[geo]            
       , quadrant_test( mode, opt ) //[geo]            
       , quicksort_test( mode, opt ) //[core]            
       , rand_test( mode, opt )   //[core]          
       , randc_test( mode, opt )  //[core]
       //, randchain_test( mode, opt )
       , randPt_test( mode, opt ) //[geo]            
       , randPts_test( mode, opt ) //[geo]            
       , randInPlanePt_test( mode, opt ) //[geo]
       , randInPlanePts_test( mode, opt ) //[geo]
       , randOnPlanePt_test( mode, opt ) //[geo]
       , randOnPlanePts_test( mode, opt )   //[geo]         
       , randRightAnglePts_test( mode, opt ) //[geo]            
       
       , range_test( mode, opt )             
       , ranges_test( mode, opt )             
       , repeat_test( mode, opt )  //[core]           
       , replace_test( mode, opt ) //[core]            
       , reverse_test( mode, opt ) //[core]            
       , rodfaces_test( mode, opt )             
       , rodPts_test( mode, opt )        
       , rodsidefaces_test( mode, opt ) 
       
       , roll_test( mode, opt )   //[core]  
       //, rotpqr_test( mode, opt )       
       , rotPt_test( mode, opt )    //[geo]         
       , rotPts_test( mode, opt )  //[geo]           
       , run_test( mode, opt )  //[core]

       , _s_test( mode, opt )             
       , _s2_test( mode, opt ) 
       //,scomp( mode, opt )  //// needed      
       , sel_test( mode, opt )             
       , shift_test( mode, opt )   //[core]          
       , shortside_test( mode, opt )             
       , shrink_test( mode, opt )  //[core]           
       , shuffle_test( mode, opt ) //[core]            
       , sinpqr_test( mode, opt )             
       , slice_test( mode, opt )   //[core]          
       , slope_test( mode, opt )
       */
       //, sopt_test( mode, opt )  //[hash]   
       /*, split_test( mode, opt )  //[core]           
       , splits_test( mode, opt )  //[core]           
       , squarePts_test( mode, opt ) //[geo]            
       , sum_test( mode, opt )             
       , sumto_test( mode, opt )             
       , switch_test( mode, opt )  //[core]           
       , symmedPt_test( mode, opt )  //[geo]           
       //, tanglnPt_test( mode, opt )
      // , toHtmlColor_test( mode, opt ) /////
       , transpose_test( mode, opt )   //[core]          
       , trim_test( mode, opt )    //[core]         
       , trfPts_test( mode, opt )  
       //, trslN_test( mode, opt )       
       , twistangle_test( mode, opt )             
       , type_test( mode, opt )             
       //, typeplp_test( mode, opt )  /////////           
       , ucase_test( mode, opt )   //[core]          
       , uconv_test( mode, opt )             
       , update_test( mode, opt )  //[core]           
       , updates_test( mode, opt ) //[core]
       */
       ////, uopt_test( mode, opt ) // where is this defined ? Can't find it       
       /*, uv_test( mode, opt )        
       //, uvN_test( mode, opt )     
       , vals_test( mode, opt )    //[core] 
      */       
//    
    ];    
    
    doctests( title=title
            , subtitle= str("scadx_core version: ", 0) //SCADX_CORE_VERSION)
            , mode=MODE
            , results=results
            , htmlstyle=htmlstyle );
}
    
//do_tests( title="Doctests for scadx_core"
/////   , mode=MODE );
//   , mode=112);

//scadx_array_test( mode= MODE, opt=[] );
//scadx_hash_test( mode= MODE, opt=[] );
//scadx_inspect_test( mode= MODE, opt=[] );
//include <../Make/Make.scad>
//echo( split("abcdefcdxy","cd"));
//echo( split_test( mode= 112 )[1]);
//echo( splits_test( mode= 112 )[1]);

 

module do_tests2( title="Doctests for scadx_core"
                , mode=MODE, opt=["showmode",false] )
{
   results= concat
   (    
     //[digest_test( mode=mode, opt=opt )]
//    get_scadx_array_test_results( title="", mode=mode, opt=opt )
//   ,get_scadx_core_test_results( title="", mode=mode, opt=opt ) 
//   ,get_scadx_num_test_results( title="", mode=mode, opt=opt )     
//   ,get_scadx_string_test_results( title="", mode=mode, opt=opt ) 
//   ,get_scadx_hash_test_results( title="", mode=mode, opt=opt )      
//   ,get_scadx_range_test_results( title="", mode=mode, opt=opt ) 
//   ,get_scadx_inspect_test_results( title="", mode=mode, opt=opt )      
//   ,get_scadx_match_test_results( title="", mode=mode, opt=opt ) 
   //,get_scadx_eval_test_results( title="", mode=mode, opt=opt ) 
  
   , get_scadx_geometry_test_results( title="", mode=mode, opt=opt ) 
   // get_scadx_error_test_results( title="", mode=mode, opt=opt ) 
  // , 
   );
    
   doctests( title=title
        , subtitle= str("scadx_core version: ", 0) //SCADX_CORE_VERSION)
        , mode=MODE
        , results=results
        , htmlstyle=htmlstyle ); 
}            
do_tests2();


//echo( _f(5) );
//echo( _f_test(mode=112)[1] );

//echo( replace_test(mode=112)[1] );

//echo( split= split("abcdef","b"));
//echo(get_scadx_range_test_results( title="", mode=mode, opt=opt ) );

//===========================================
//
//           Version / History
//
//===========================================

scadx_core_test_ver=[
["20150207-1", "Separated from original scadx_core.scad"]
,["20150420-1", "Revised all to fit new doctest.scad. Time to run all tests at mode 112: 3 min"]
,["20150516-1", "Revised all to fit new doctest()"]
,["20150524-1", "Detailed doc and tests for _f and _s2"]
,["20150524-2", "Minor bugfixes. Took 40sec with mode=11: 174 funcs, 1006 tests and 2 fail(s)."]
,["20150601-1", "evalarr_test; fix _f % error"]

];

//SCADX_CORE_TEST_VERSION = last(scadx_core_test_ver)[0];

//echo_( "<code>||.........  <b>scadx core test version: {_}</b>  .........||</code>",[SCADX_CORE_TEST_VERSION]);
//echo_( "<code>||Last update: {_} ||</code>",[join(shrink(scadx_core_test_ver[0])," | ")]);    


    
    