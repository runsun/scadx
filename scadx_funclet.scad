  
// Convert a 2-pointer pq to a vector
function p2v(pq)= pq[1]-pq[0];  

function pw(x)= pow(x,2);
       
function dx(pq)= pq[1].x - pq[0].x; //= dist(pq,"x")
function dy(pq)= pq[1].y - pq[0].y;
function dz(pq)= pq[1].z - pq[0].z;

function addx(pts,x)= [ for(p=pts) p+[x,0,0] ];
function addy(pts,y)= [ for(p=pts) p+[0,y,0] ];
function addz(pts,z)= [ for(p=pts) p+[0,0,z] ];

function d01(pqr)= norm(pqr[1]-pqr[0]); // dist btw p and q 
function d12(pqr)= norm(pqr[2]-pqr[1]); 
function d02(pqr)= norm(pqr[2]-pqr[0]);

function minx(pts)= min( [for(p=pts) p.x] ); // smallest x in pts 
function miny(pts)= min( [for(p=pts) p.y] );
function minz(pts)= min( [for(p=pts) p.z] );
function maxx(pts)= max( [for(p=pts) p.x] ); // largest x in pts
function maxy(pts)= max( [for(p=pts) p.y] );
function maxz(pts)= max( [for(p=pts) p.z] );

function px00(p)=[ p[0],0,0];  // for pt [x,y,z] => [x,0,0]: proj to x-axis
function p0y0(p)=[ 0, p[1],0 ];
function p00z(p)=[ 0,0, p[2]];
function pxy0(p)=[ p[0],p[1],0]; // for pt [x,y,z] => [x,y,0]
function p0yz(p)=[ 0, p[1],p[2] ];
function px0z(p)=[ p[0],0, p[2]];

function p01(pts,ps)= [pts[0], pts[1]];  
function p10(pts,ps)= [pts[1], pts[0]];
function p02(pts,ps)= [pts[0], pts[2]];
function p20(pts,ps)= [pts[2], pts[0]];
function p03(pts,ps)= [pts[0], pts[3]];
function p30(pts,ps)= [pts[3], pts[0]];
function p12(pts,ps)= [pts[1], pts[2]];
function p21(pts,ps)= [pts[2], pts[1]];
function p13(pts,ps)= [pts[1], pts[3]];
function p31(pts,ps)= [pts[3], pts[1]];
function p23(pts,ps)= [pts[2], pts[3]]; 
function p32(pts,ps)= [pts[3], pts[2]];

function p012(pts)= [pts[0],pts[1],pts[2]]; 
function p013(pts)= [pts[0],pts[1],pts[3]];
function p021(pts)= [pts[0],pts[2],pts[1]];
function p023(pts)= [pts[0],pts[2],pts[3]];
function p031(pts)= [pts[0],pts[3],pts[1]];
function p032(pts)= [pts[0],pts[3],pts[2]];

function p102(pts)= [pts[1],pts[0],pts[2]];
function p103(pts)= [pts[1],pts[0],pts[3]];
function p120(pts)= [pts[1],pts[2],pts[0]];
function p123(pts)= [pts[1],pts[2],pts[3]];
function p130(pts)= [pts[1],pts[3],pts[0]];
function p132(pts)= [pts[1],pts[3],pts[2]];

function p201(pts)= [pts[2],pts[0],pts[1]];
function p203(pts)= [pts[2],pts[0],pts[3]];
function p210(pts)= [pts[2],pts[1],pts[0]];
function p213(pts)= [pts[2],pts[1],pts[3]];
function p230(pts)= [pts[2],pts[3],pts[0]];
function p231(pts)= [pts[2],pts[3],pts[1]];

function p301(pts)= [pts[3],pts[0],pts[1]];
function p302(pts)= [pts[3],pts[0],pts[2]];
function p310(pts)= [pts[3],pts[1],pts[0]];
function p312(pts)= [pts[3],pts[1],pts[2]];
function p320(pts)= [pts[3],pts[2],pts[0]];
function p321(pts)= [pts[3],pts[2],pts[1]];

function newx(pt,x)= [x, pt.y, pt.z];
function newy(pt,y)= [pt.x, y, pt.z];
function newz(pt,z)= [pt.x, pt.y, z];

function minx(pts)= min( [ for(p=pts) p.x] );
function maxx(pts)= max( [ for(p=pts) p.x] );
function miny(pts)= min( [ for(p=pts) p.y] );
function maxy(pts)= max( [ for(p=pts) p.y] );
function minz(pts)= min( [ for(p=pts) p.z] );
function maxz(pts)= max( [ for(p=pts) p.z] );

function to3d(pts,z=0)= len(pts[0])==3? pts
                        : [ for(p=pts) [p[0],p[1],z] ];

// product of difference, used in lineCrossPt
function podPt(P,Q,R,S)=[ (Q-P).x * (S-R).x   
                        , (Q-P).y * (S-R).y
                        , (Q-P).z * (S-R).z ];
function P(pqr) = pqr[0];
function Q(pqr) = pqr[1];
function R(pqr) = pqr[2];

function dist_2(p,q)= pow( dist(p,q),2);

function N(pqr,len=1,reverse=false) = 
          //let(pqr=[pqr[0],pqr[1],pqr[2]])
           iscoline(pqr)?undef
           :normalPt( pqr,len=len,reverse=reverse);
function N2(pqr,len=1,reverse=false) = 
        iscoline(pqr)?undef
        :normalPt( [N(pqr), Q(pqr), P(pqr) ],len=len,reverse=reverse);

// Binormal Pt
function B(pqr,len=1,reverse=false) = 
        let(pqr=[pqr[0],pqr[1],pqr[2]])
        iscoline(pqr)?undef
        :normalPt( [N(pqr), Q(pqr), P(pqr) ],len=len,reverse=reverse);

// With the following, we can do:
//   PQ(pqr,S) => PQS // could replace this with get(pqr,[0,1],ap=S)
//   PR(pqr,S,1) => PSR 
function PQ(pts,pt,i)= let(pts=[pts[0],pts[1]])pt?insert(pts,pt,i):pts;  
function PR(pts,pt,i)= let(pts=[pts[0],pts[2]])pt?insert(pts,pt,i):pts;
function QP(pts,pt,i)= let(pts=[pts[1],pts[0]])pt?insert(pts,pt,i):pts;
function QR(pts,pt,i)= let(pts=[pts[1],pts[2]])pt?insert(pts,pt,i):pts;
function RP(pts,pt,i)= let(pts=[pts[2],pts[0]])pt?insert(pts,pt,i):pts;
function RQ(pts,pt,i)= let(pts=[pts[2],pts[1]])pt?insert(pts,pt,i):pts;

//PQ()

function PQN(pts)= [pts[0],pts[1],N(pts)];  
function PRN(pts)= [pts[0],pts[2],N(pts)];
function QPN(pts)= [pts[1],pts[0],N(pts)];
function QPN(pts)= [pts[1],pts[2],N(pts)];
function RPN(pts)= [pts[2],pts[0],N(pts)];
function RQN(pts)= [pts[2],pts[1],N(pts)];

function PQB(pts)= [pts[0],pts[1],B(pts)];  
function PRB(pts)= [pts[0],pts[2],B(pts)];
function QPB(pts)= [pts[1],pts[0],B(pts)];
function QPB(pts)= [pts[1],pts[2],B(pts)];
function RPB(pts)= [pts[2],pts[0],B(pts)];
function RQB(pts)= [pts[2],pts[1],B(pts)];

//module mv(x,y,z) {translate([und(x,0),und(y,0),und(z,0)]) children();}
//module mvx(d) {translate([d,0,0]) children();}
//module mvy(d) {translate([0,d,0]) children();}
//module mvz(d) {translate([0,0,d]) children();}
//module mvxy(x,y) {translate([x,y,0]) children();}
//module mvxz(x,z) {translate([x,0,z]) children();}
//module mvyz(y,z) {translate([0,y,z]) children();}
module Mv(x,y,z) {translate([und(x,0),und(y,0),und(z,0)]) children();}
module Mvx(d) {translate([d,0,0]) children();}
module Mvy(d) {translate([0,d,0]) children();}
module Mvz(d) {translate([0,0,d]) children();}
module Mvxy(x,y) {translate([x,y,0]) children();}
module Mvxz(x,z) {translate([x,0,z]) children();}
module Mvyz(y,z) {translate([0,y,z]) children();}

function mvPts(pts,v)=[ for(p=pts)p+v]; 

//Note: rotx(a) is a module, rotx(pt,a) is a function
module Rotx(a){ rotate( [a,0,0] ) children(); }
module Roty(a){ rotate( [0,a,0] ) children(); }
module Rotz(a){ rotate( [0,0,a] ) children(); }
//module rotx(a){ rotate( [a,0,0] ) children(); }
//module roty(a){ rotate( [0,a,0] ) children(); }
//module rotz(a){ rotate( [0,0,a] ) children(); }

// 2020.7.12: The following funclets will replace many above:
function x(pts,x)= isnum(pts[0]) ? [pts[0]+x,pts[1], pts[2] ]
                   : [ for ( p = pts ) [p[0]+x, p[1], p[2] ] ] ;
function y(pts,y)= isnum(pts[0]) ? [pts[0],pts[1]+y, pts[2] ]
                   : [ for ( p = pts ) [p[0], p[1]+y, p[2] ] ] ;
function z(pts,z)= isnum(pts[0]) ? [pts[0],pts[1], pts[2]+z ]
                   : [ for ( p = pts ) [p[0], p[1], p[2]+z ] ] ;
function t(pts,xyz)= isnum(pts[0]) ? [pts[0]+xyz.x,pts[1]+xyz.y, pts[2]+xyz.z ]
                   : [ for ( p = pts ) [p[0]+xyz.x, p[1]+xyz.y, p[2]+xyz.z ] ] ;

module X(n){ translate( [n,0,0] ) children(); }
module Y(n){ translate( [0,n,0] ) children(); }
module Z(n){ translate( [0,0,n] ) children(); }
module T(a){ translate( a ) children(); }