include <scadx_core_test2.scad>
include <../doctest/doctest2_dev.scad>
//
// The funcs here are usually called by scadx_doctesting_dev.scad.
// To test them directly in this file, un-mark the following line:
//include <../doctest/doctest_dev.scad>
//
//FORHTML=false;
//MODE=112;

/* 
=================================================================
                    scadx_core_test.scad
=================================================================
*/
/*
      fname = doct[ DT_iFNAME ]  // function name like "sum"
	, args  = doct[ DT_iARGS ]   // argument string like "x,y"
  //, rtntype = doct[ DT_iTYPE ] // return type
    , tags  = doct[ DT_iTAGS ]   // like, "Array, Inspect"
  //, doc   = doct[ DT_iDOC ]    // documentation (str)
    , rels  = doct[ DT_iREL ]
*/


//===================================================
//===================================================
//===================================================

module do_tests( title="Doctests for scadx_core"
                , mode=MODE, opt=["showmode",false] ){
    
    results= [
        accumulate_test( mode, opt )
       ,addx_test( mode, opt )
       ,addy_test( mode, opt )
       ,addz_test( mode, opt )
       ,all_test( mode, opt )
       ,angle_test( mode, opt )    
       ,angleBisectPt_test( mode, opt )
       , anglePt_test( mode, opt )             
       ////, anyAnglePt_test( mode, opt ) // to be replaced by anglePt. 2015.5.7             
       , app_test( mode, opt )             
       , appi_test( mode, opt )             
       , arcPts_test( mode, opt )             
       , arrblock_test( mode, opt )
       , begmatch_test( mode, opt )             
       , begwith_test( mode, opt )             
       , begword_test( mode, opt )             
       , between_test( mode, opt )             
       , boxPts_test( mode, opt )      
       , calc_shrink_test( mode, opt )       
       , calc_flat_test( mode, opt ) 
       , calc_func_test( mode, opt ) 
       , centroidPt_test( mode, opt )
       , cirfrags_test( mode, opt )
       , combine_test( mode, opt )
       , coordPts_test( mode, opt )
       ////, iscoord_test( mode, opt )
       ////, tocoord_test( mode, opt )
       , cornerPt_test( mode, opt )
       , countArr_test( mode, opt )   
       , countInt_test( mode, opt )   
       , countNum_test( mode, opt )   
       , countStr_test( mode, opt )   
       , countType_test( mode, opt ) 
       , cubePts_test( mode, opt )
       , deeprep_test( mode, opt )  
       , del_test( mode, opt )  
       , delkey_test( mode, opt )  
       , dels_test( mode, opt )  
       , det_test( mode, opt )  
       , dist_test( mode, opt )  
       , distPtPl_test( mode, opt )  
       , distx_test( mode, opt )  
       , disty_test( mode, opt )  
       , distz_test( mode, opt )
       , dot_test( mode, opt ) 
       , echoblock_test( mode, opt )
       , echo__test( mode, opt )
       , echofh_test( mode, opt )
       , echoh_test( mode, opt )
       , endwith_test( mode, opt )
       , endword_test( mode, opt )
       , expandPts_test( mode, opt )
       , fac_test( mode, opt )
       , faces_test( mode, opt )
       , fidx_test( mode, opt ) 
        , flatten_test( mode, opt )
        , _fmt_test( mode, opt )
        , _fmth_test( mode, opt )
        , ftin2in_test( mode, opt )
        , get_test( mode, opt )
        , getdeci_test( mode, opt )
        , getSideFaces_test( mode, opt )
        ////, getuni_test( mode, opt ) // SLOW ...
        ,_h_test( mode, opt )
        , has_test( mode, opt )
        , hash_test( mode, opt )
        , hashex_test( mode, opt )
        , hashkvs_test( mode, opt )
        , haskey_test( mode, opt )
        , incenterPt_test( mode, opt )
        , idx_test( mode, opt )             
        , index_test( mode, opt )            
        , inrange_test( mode, opt ) 
        , int_test( mode, opt )
        , intersectPt_test( mode, opt )
        , is0_test( mode, opt )             
        , is90_test( mode, opt )            
        , isarr_test( mode, opt )            
        , isat_test( mode, opt )  
        , isbool_test( mode, opt )  
        , isequal_test( mode, opt )  
        , isfloat_test( mode, opt )  
        , ishash_test( mode, opt )  
        , isint_test( mode, opt )  
        , isln_test( mode, opt )  
        , isnum_test( mode, opt )  
        , isOnPlane_test( mode, opt )  
        , ispl_test( mode, opt )  
        , ispt_test( mode, opt )  
        , isSameSide_test( mode, opt )  
        , isstr_test( mode, opt )  
        , istype_test( mode, opt )  
        , join_test( mode, opt )  
        , joinarr_test( mode, opt )  
        , keys_test( mode, opt )  
        , kidx_test( mode, opt )  
        , last_test( mode, opt )
        , lcase_test( mode, opt )
        , lineCrossPt_test( mode, opt )
        , lineCrossPts_test( mode, opt )
        , linePts_test( mode, opt )
        , longside_test( mode, opt )
        , lpCrossPt_test( mode ,opt )
        
       , match_test( mode, opt )             
       , match_at_test( mode, opt )             
       , match_ps_at_test( mode, opt )             
       , match_nps_at_test( mode, opt )   
       , _mdoc_test( mode, opt )
       , midPt_test( mode, opt )
       , mod_test( mode, opt )
       , normal_test( mode, opt )             
       , normalLn_test( mode, opt )             
       , normalPt_test( mode, opt )             
       , normalPts_test( mode, opt )
       , num_test( mode, opt ) 
       , numstr_test( mode, opt )             
       , onlinePt_test( mode, opt )             
       , onlinePts_test( mode, opt )             
       , or_test( mode, opt )             
       , othoPt_test( mode, opt ) 
      //// , packuni_test( mode, opt ) 
       , parsedoc_test( mode, opt )             
       , permute_test( mode, opt )             
       , pick_test( mode, opt )             
       , planecoefs_test( mode, opt )             
       , ppEach_test( mode, opt )             
       , pqr90_test( mode, opt )             
       , prod_test( mode, opt )             
       , projPt_test( mode, opt )             
       , quadrant_test( mode, opt )             
       , quicksort_test( mode, opt )             
       , rand_test( mode, opt )             
       , randc_test( mode, opt )             
       , randPt_test( mode, opt )             
       , randPts_test( mode, opt )             
       , randInPlanePt_test( mode, opt )
       , randInPlanePts_test( mode, opt ) 
       , randOnPlanePt_test( mode, opt )
       , randOnPlanePts_test( mode, opt )            
       , randRightAnglePts_test( mode, opt )             
       , range_test( mode, opt )             
       , ranges_test( mode, opt )             
       , repeat_test( mode, opt )             
       , replace_test( mode, opt )             
       , reverse_test( mode, opt )             
       , rodfaces_test( mode, opt )             
       , rodPts_test( mode, opt )        
       , rodsidefaces_test( mode, opt ) 
       
       , roll_test( mode, opt )             
       , rotPt_test( mode, opt )             
       , rotPts_test( mode, opt )             
       , run_test( mode, opt ) 

       , _s_test( mode, opt )             
       , sel_test( mode, opt )             
       , shift_test( mode, opt )             
       , shortside_test( mode, opt )             
       , shrink_test( mode, opt )             
       , shuffle_test( mode, opt )             
       , sinpqr_test( mode, opt )             
       , slice_test( mode, opt )             
       , slope_test( mode, opt )             
       , split_test( mode, opt )             
       , splits_test( mode, opt )             
       , squarePts_test( mode, opt )             
       , sum_test( mode, opt )             
       , sumto_test( mode, opt )             
       , switch_test( mode, opt )             
       , symmedPt_test( mode, opt )             
       ,transpose_test( mode, opt )             
       , trim_test( mode, opt )             
       , trfPts_test( mode, opt )             
       , twistangle_test( mode, opt )             
       , type_test( mode, opt )             
       , typeplp_test( mode, opt )             
       , ucase_test( mode, opt )             
       , uconv_test( mode, opt )             
       , update_test( mode, opt )             
       , updates_test( mode, opt )             
       , uv_test( mode, opt )             
       , vals_test( mode, opt )             
//    
    ];    
    
    doctests( title=title
            , subtitle= str("scadx_core version: ", SCADX_CORE_VERSION)
            , mode=MODE
            , results=results
            , htmlstyle=htmlstyle );
}


    
do_tests( title="Doctests for scadx_core"
   , mode=11);
  // , mode=MODE );


//===========================================
//
//           Version / History
//
//===========================================

scadx_core_test_ver=[
["20150207-1", "Separated from original scadx_core.scad"]
,["20150420-1", "Revised all to fit new doctest.scad. Time to run all tests at mode 112: 3 min"]
,["20150516-1", "Revised all to fit new doctest2()"]
];

SCADX_CORE_TEST_VERSION = last(scadx_core_test_ver)[0];

//echo_( "<code>||.........  <b>scadx core test version: {_}</b>  .........||</code>",[SCADX_CORE_TEST_VERSION]);
//echo_( "<code>||Last update: {_} ||</code>",[join(shrink(scadx_core_test_ver[0])," | ")]);    


    
    