include <scadx_core_dev.scad>
//include <scadx_obj_adv_dev.scad>

//========================================================
Arc=[[],
"

 angle not given:  draw arc:RS

                     _R 
                   _:  :
                  :     : 
                 -       :
                :         : 
               -           : 
       p ------S-----------'Q


 angle given: draw arc:DS

                     _R 
                   _:  :
                D :     : 
                 -'-_    :
                :    '-_  : 
               -     a  '-_: 
       p ------S-----------'Q


	 
"];


module Arc(pqr,ops=[]){

    //_mdoc("Arc","");
                     
	ops= concat( ops
    , [ "rad", d01(pqr)  // rad of the curve this obj follows. 
      , "r", 0.2         // Set rad of crossection on P and R ends at once
      , "rP", undef      // rad of the circle at P
	  , "rR", undef      // rad of the circle at R  
      , "a", angle(pqr)  // angle determining cone length
                         // if not given, use a_pqr
      , "slice", $fn==0?6:$fn //# of slices 
      , "fn", $fn==0?6:$fn    //# of points along bottom circle
      , "debug", false   // set to true to show pt indices
      ], OPS);

    function ops(k,df)=hash(ops,k,df);

    rad=ops("rad");
    r = ops("r");
    rP= ops("rP", r);
    rR= ops("rR", r);
    sl= ops("slice");
    fn=ops("fn");
    //echo("rP,rR", rP, rR);
    
    a = ops("a");
    P = pqr[0]; Q=pqr[1]; 
    
    R= anglePt(pqr, a, len=r);  // The end pt of arc

    pqr = [P,Q,R];
	
    function getPtsBySlice(i)= 
    (  let( 
             X = anglePt( pqr   // X is a point along the P->R curve
                  , a= i*a/sl
                  , len=rad )
           , X2= anglePt( pqr   // X2 is a little bit further down the arc
                  , a= i*a/sl+1
                  , len=rad )
           ) 
        arcPts( [Q,X, N([Q,X,X2])]
                    , pl="xy"
                    , a=360
                    , count= fn
                    , r= rP + i*(rR-rP) /fn
                    )
    );
    
    // pts: [ [p10,p11,p12...p16]  <== layer 1
    //      , [p20,p21,p22...p26]
    //      ...
    //      , [pi0,pi1,pi2...pi6] ] <==layer i    
    pts_layers = [ for( i = range(sl+1) )
                   getPtsBySlice(i) ];
    
    pts = [ for(a=pts_layers, b=a) b] ; 

    // This for loop is for debug only. It displays
    // lines thru circling pts of each layer, and also
    // label pts
    if( ops("debug")){
        MarkPts( pts, ["samesize",true, "r", 0.01] );
        LabelPts( pts );    
    }
    
    faces = faces("chain", sides=fn, count= sl);      

    color( ops("color"), ops("transp") )    
    polyhedron( points= pts
              , faces = faces
              );              
}    

module Arc_demo_1()
{ _mdoc("Arc_demo_1","");
    
	pqr = randPts(3);
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
	Chainf( pqr );
	LabelPt( midPt( p01(pqr)), " Arc(pqr)" ); 
    Arc(pqr); //, ["angle", 70]);
	
    pqr2= randPts(3); 
    Chainf(pqr2, ops=["color","red"] );
    Arc(pqr2, ["rad",3,"color","red"] );
    Dim2( [Q(pqr2), onlinePt( p10(pqr2), len=3), R(pqr2)]
        , ["color","red"] );
    LabelPt( midPt( p02(pqr2) )
    , "Arc(pqr,[\"rad\",3,\"color\",\"red\"])", ["color","red"]);    
}

module Arc_demo_2()
{ _mdoc("Arc","");
    
	pqr = randPts(3);
    echo("pqr", pqr);
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
	Chainf( pqr ); 
        
    Arc(pqr);
	
    Arc(pqr, ["r",0.1, "rad",3, "slice",4, "color","red"]); 
    R2 = onlinePt( [Q,P], len=3);
    Dim2( [ Q, R2, R ],["color","red"] );
    LabelPt( angleBisectPt(pqr, len=1.5) , 
    " [\"r\",0.1,\"rad\",3,\"slice\",4,\"color\",\"red\"]"
    ,["color","red"] ); 
    
	Arc(pqr, ["rP",0.1, "rR", 0.8, "rad",6, "color","blue"]); 
    LabelPt( angleBisectPt(pqr, len=7) , 
    " [\"rP\",0.1,\"rR\",0.8,\"rad\",6,\"color\",\"blue\"]"
    ,["color","blue"] ); 
 	

}

module Arc_demo_3_debug()
{ _mdoc("Arc_demo_3_debug","");
    
	pqr = randPts(3);
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
	Chainf( pqr );
//	LabelPt( Q,
//        , "Arc(pqr,[\"debug\",true,\"slice\",4,\"fn\",6, \"rad\",2,\"r\",1])" ); 
    Arc(pqr, ["debug",true, "a",90, "slice",4
    ,"rad",5,"fn",6,"r",0.7, "transp", 0.7]); 
}

module Arc_demo_4_angle()
{ _mdoc("Arc_demo_4_angle","");
    
	pqr = randPts(3);
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
	Chainf( pqr );

    M = angleBisectPt( pqr, len = 4.5 );
    Line0( [Q,M], ["r", 0.01]);
    
    Arc(pqr, ["rad",2]);
   
    Arc(pqr, ["a",30, "rad",2.5, "color","red" ]);
    Arc(pqr, ["a",60, "rad",3, "color","green" ]);
    Arc(pqr, ["a",90, "rad",3.5, "color","blue" ]);
    
   LabelPt(Q, "   a=30(red),60(green),90(blue)");
    
}
//Arc_demo_1();
//Arc_demo_2();
//Arc_demo_3_debug();
//Arc_demo_4_angle();

//angle_test(["mode",13]);






//========================================================
Arrow=["Arrow","pq, ops", "n/a", "geometry"
, "



"];

module Arrow(pq, ops=[])
{
    //_mdoc("Arrow","");
    
    df_fn = $fn==0?6:$fn;    
    com_keys= [ "color","transp","fn"];
    
    df_ops=[ "r",0.02
           , "fn", df_fn
           , "heads",true
           , "headP",true
           , "headQ",true
           , "debug",false
           , "twist", 0
           ];
    
    _ops= update( df_ops, ops );    
       
    function ops(k)= hash(_ops,k);
    fn = ops("fn");
    r  = ops("r");
    L  = d01(pq);
    
    //======================================== arrow
    // Decides arrow r and len automatically (based on line r)
    // when they are not set by user:
    arr_l = min(10*exp(-0.5*r)*r, 2*L/5);    
                                // 6*r, but can't be too large 
    arr_r = (3*exp(-0.8*r)) *r; // Must be >r, but with certain ratio
                                // that is large ( 3x ) @ small r but
                                // smaller @ large r. So we use a 
                                // exponential function 
    df_heads= ["r", arr_r, "len", arr_l, "fn", fn];
    df_headP= [];
    df_headQ= [];
    
    /*
        _-'|          |'-_ 
      P----A----------B----Q
        '-_|          |_-'  
           '          '
    */
    
    // ops_arrow 
    
    u_hs = ops("heads");  // user input
    u_hp = ops("headP");  // user input
    u_hq = ops("headQ");  // user input
    
//    fmt = [ "color", ops("color")    // The 
//          , "transp", ops("transp") 
//          , "fn", ops("fn")];
//    ops_arrowP =  concat( isarr(ap)?ap:[], isarr(arw)?arw:[], fmt, df_arrowP);
//    ops_arrowQ =  concat( isarr(aq)?aq:[], isarr(arw)?arw:[], fmt, df_arrowQ);
//
    ops_headP= getsubops( ops, df_ops=df_ops
                , com_keys= com_keys
                , sub_dfs= ["heads", df_heads, "headP", df_headP ]
                );
                
    ops_headQ= getsubops( ops, df_ops=df_ops
                , com_keys= com_keys
                , sub_dfs= ["heads", df_heads, "headQ", df_headQ ]
                );
//
//    //echo( "ops_headP", ops_headP);
//    //echo( "ops_headQ", ops_headQ);
//    
    function ops_headP(k) = hash( ops_headP,k );
    function ops_headQ(k) = hash( ops_headQ,k );
    
    headpq = linePts( pq 
                     , len=[ u_hs && u_hp?-ops_headP("len"):0 
                           , u_hs && u_hq?-ops_headQ("len"):0]
                     );
    //echo("_ops",_ops);
    
    linepq = linePts( headpq, len= [u_hp?GAPFILLER:0
                                   , u_hq?GAPFILLER:0 ] );    
    
    R= len(pq)>2? pq[2]:randPt();
    //echo("arrow, concat(linepq,[R])",concat(linepq,[R]));
    
    //====================================== line 
    Line( linepq, _ops);
    //Line( concat(linepq,[R]), _ops);
    //======================================
    
    
    if(u_hs && u_hp) Cone( [ headpq[0], pq[0], R ], ops_headP );
    if(u_hs && u_hq) Cone( [ headpq[1], pq[1], R ], ops_headQ );
    
}    

module Arrow_demo_1()
{
 _mdoc("Arrow_demo_1"
, "Yellow: Arrow( randPts(2) )
");   
    pq = randPts(2);
    //MarkPts( pq, ["labels", "PQ", "colors",["red"]] );
    //Chain( pqr, ["closed",false]);
    Arrow( pq );
    
    pq2 = randPts(2);
    //MarkPts( pq2, ["labels", "PQ", "colors",["green"]] );
    Arrow( pq2, [ "color","green"
                , "fn", 6, "r",0.3
                ]);
    
    LabelPt( pq[0], " Arrow(pq)");
    LabelPt( pq2[0], 
    " Arrow(pq,[\"fn\",6,\"r\",0.3,\"color\",\"green\"])"
    ,["color","green"]);
//    
//    
}    
module Arrow_demo_2_defaultArrowSettings()
{
 _mdoc("Arrow_demo_2_autoArrowRad"
, "The arrow rad and len are auto set, if not specified. 
;; The following are Arrow( pq, [''r'',r]) 
;; at  r= 0.01, 0.025, 0.05, 0.1, 0.2, 0.3, 0.5
;; This could be used as a guide line to set arrow rad/len.
");   

    rs= [0.01, 0.025, 0.05, 0.1, 0.2, 0.3, 0.5];
    
    LabelPt( [2,-1,4],
      "Arrows with different r:[0.01,0.025,0.05,0.1,0.2,0.3,0.5]"
    );
    
    for( i = range(rs) )
    {  pq = randPts(2);
       r = rs[i];
       L = d01(pq); 
       echo( "r", rs[i]
           , "arr_r", (3*exp(-0.8*r)) *r
           , "arr_l", min(10*exp(-0.5*r)*r, 2*L/5) 
           );   
       Arrow( pq, ["r", rs[i]]);
    }
  
}     

module Arrow_demo_3_lineProp()
{
 _mdoc("Arrow_demo_3_lineProp"
," Yellow: Arrow( pq, [''r'', 0.1, ''fn'',8, ''transp'', 0.8 ])
;; Red: Arrow( pq, [''r'', 0.1, ''heads'', false, ''color'',''red''] )
;; Green: Arrow( pq, [''r'', 0.1, ''headP'', false, ''color'',''green''] )
;; Blue: Arrow( pq, [''r'', 0.1, ''headQ'', false, ''color'',''blue''] )
"); 
//    Arrow(randPts(2));
//    Arrow( randPts(2)
//         , ["r", 0.1, "fn",8, "transp", 0.8 ]);   
    
    pq0= randPts(2);
    //MarkPts(pq0);
    //LabelPt( pq0[0], "  heads=false", ["color","red"]);
    Arrow( pq0        , ["r", 0.1, "heads", false, "color","red"] );

    pq= randPts(2);
    MarkPts(pq);
    LabelPt( pq[0], "  P (headP=false)", ["color","green"]);
    Arrow( pq
        , ["r", 0.1, "headP", false, "color","green"] );

    pq2= randPts(2);
    MarkPts(pq2);
    LabelPt( pq2[1], "  Q (headQ=false)", ["color","blue"]);
    Arrow( pq2
        , ["r", 0.1, "headQ", false, "color","blue"] );
 
}    
module Arrow_demo_4_arrowProp()
{
 _mdoc("Arrow_demo_4_arrowProp"
," Yellow: Arrow( pq, [''r'', 0.1,  ''headP'', [''color'',''red'']])
;; Red: Arrow( pq, [''r'', 0.1, ''color'',''blue''
;;                 , ''headQ'', [''color'',''green''
;;                               ,''r'', 0.5
;;                               ,''len'', 1.5
;;                               ,''fn'', 4
;;                               ]] )
"); 

    pq= randPts(2);
    MarkPts(pq);
    LabelPts( pq, [" P", " Q"] );
    Arrow( pq
         , ["r", 0.1,  "headP", ["color","red"]] );
    LabelPt( pq[0], "   Arrow(pq,[\"r\",0.1,\"headP\",[\"color\",\"red\"]])");
    
    pq2= randPts(2);
    MarkPts(pq2);
    LabelPts( pq2, [" P", " Q"] );
    
    Arrow( pq2
        , ["r", 0.1, "color","blue"
          , "headQ", ["color","green"
                      ,"r", 0.5
                      ,"len", 1.5
                      , "fn", 4
    ]] );
    
    color("blue")
    {LabelPt( pq2[0], "   Arrow( pq2, [\"r\", 0.1, \"color\",\"blue\" ");
    LabelPt( addy(pq2[0],-1.5), "   , \"headQ\", [\"color\",\"green\",\"r\",0.5,\"len\",1.5,\"fn\",4]])");
    }                  
 
}   
//Arrow_demo_1();    
//Arrow_demo_2_defaultArrowSettings();    
//Arrow_demo_3_lineProp();     
//Arrow_demo_4_arrowProp();    
    
    

//========================================================
Block=[ "Block","pq,ops", "n/a", "3D, shape",
 " Given a 2-pointer pq, make a block that has its one edge lies on
;; or parallel to the pq line. The block size is determined by pq,
;; width and depth. It can rotate by an angle defined by ''rotate''
;; about the pq-line. Default ops:
;;
;;  [ ''color'' , ''yellow''
;;  , ''transp'', 1
;;  , ''rotate'', 0           // rotate angle alout the line
;;  , ''shift'' , ''corner''  //corner|corner2|mid1|mid2|center
;;  , ''width'' , 1
;;  , ''depth'' , 6
;;  ]
;;
;; The shift is a translation decides where on the block it attaches
;; itself to pq. It can be any [x,y,z], or one of the following strings:
;;
;;    corner       mid1          mid2         center
;;    q-----+      +-----+      +-----+      +-----+
;;   / :     :    p :     :    / q     :    / :     :
;;  p   +-----+  +   +-----+  +   +-----+  + p +-----+
;;   : /     /    : q     /    p /     /    : /     /
;;    +-----+      +-----+      +-----+      +-----+
;;
;;    corner2
;;    +-----+
;;   / :     :
;;  +   q-----+
;;   : /     /
;;    p-----+
"];

module Block(pq,ops){
	Line( pq, update(ops, ["style", "block"]) );
}

module Block_test(ops){ doctest(Block, ops=ops);}

module Block_demo_1()
{
	echom("Block_demo_1");
	pq= randPts(2);
	//pq = [ [2,1,3], [4,3,2]];
	MarkPts(pq);
	Block(pq);
}

module Block_demo_2()
{
	echom("Block_demo_2");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]], ["width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], update(ops, ["color","red"]));//
	Block([[x-w,0,0], [x-w,0,z]], update(ops, ["color","green"]) );

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Block( [ [x, 0, z], [0, 0, z+1]], update(ops, ["color","blue", "rotate",360*$t] ) );
	
}


module Block_demo_3()
{
	echom("Block_demo_3");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]]
					   , [ "width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift","corner2" // <====
					  ]) );


}

module Block_demo_4()
{
	echom("Block_demo_4");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]]
					   ,[ "width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift","mid1" // <====
					  ]) );

}

module Block_demo_5()
{
	echom("Block_demo_5");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]]
					   , ["width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift","mid2" // <====
					  ]) );
}


module Block_demo_6()
{
	echom("Block_demo_6");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]],[ "width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift","center" // <====
					  ]) );
}


module Block_demo_7()
{
	echom("Block_demo_7");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]]
					   , ["width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift",[2*w, 0,w] // <====
					  ]) );
}


module Block_demo()
{
	//Block_demo_1();
	//Block_demo_2();
	Block_demo_3();
	//Block_demo_4();
	//Block_demo_5();
	//Block_demo_6();
	//Block_demo_7();
}
//Block_test( ["mode",22] );
//Block_demo();
//doc(Block);






//// shape ========================================================
Chain=["Chain","pts,ops", "n/a", "3D, Shape",
 " Given array of pts (pts) and ops, make a chain of line
;; segments to connect points. ops:
;;
;;  [ ''r''     , 0.05
;;  , ''closed'',true
;;  , ''color'' ,false
;;  , ''transp'',false
;;  , ''usespherenode'',true
;;  ]
"];	

module Chain(pts, ops=[])
{
    //echo("in Chain ops",ops);
    
	ops= concat(ops,
		 ["r", 0.05
		 ,"r1", false  // <=== what is this ?
		 ,"closed",true
		 ,"color",false
		 ,"transp",false
		 ,"usespherenode",true
		]);
    
//	ops= update(
//		 ["r", 0.05
//		 ,"r1", false  // <=== what is this ?
//		 ,"closed",false
//		 ,"color",false
//		 ,"transp",false
//		 ,"usespherenode",true
//		], ops);
//    
    //echo("in Chain ops",ops);
    
	function ops(k)= hash(ops,k);
	r = ops("r");

	path = concat( pts, ops("closed")?[pts[0]]:[] );
	//echo( "paths in Chain:", paths);
	//echo("ops in Chain: ", ops );
	//echo("r in Chain: ", r);
	//union(){
	for(i=[0:len(path)-2]){
		//echo( i, paths[i]);
		color(ops("color"), ops("transp"))
		if(i>0){translate(path[i])
			if(!ops("usespherenode")){
				//rotate( nor
				cylinder( r=r, h=r*5.0, center=true );
			} else {
				//sphere( r=r+0.005, h=r, center=true );
				sphere( r=r, h=r, center=true );
			}
		}		
		Line0( [pts[i], path[i+1]], ops );
	  }
	//}
}
module Chain_test(ops){ doctest(Chain,ops=ops);}

module Chain_demo_1()
{
	//paths= [ [0,5,0], [ 4,5,0], [4,3,0], [1,3,0], [0,0,0], [4,0,0] ];
	path= [ [0,5,1]
			,[3,5,0],[3.7,4.7,0], [4,4.3,1] //, [ 4,5,0]
			, [4,3,-1], [1,3,2], [0,0,3], [4,0,-1] ];
	//color("silver", 0.9)
	Chain(path);
	
	Chain( randPts(5), ["r",0.2, "closed",true
					  ,"transp",0.3,"color","green"]);

	Chain( boxPts( randPts(2) ),["color","red"]);
}

module Chain_demo_2() // testing chains on axis planes
{
	ops = [ "transp", 0.4, "closed", true ];
	pathxy = randPts(3, z=0);
	Chain( pathxy, update( ops, ["color","red"]) );
	//echo(pathxy);
	
	pathyz = randPts(4, x=0);
	Chain( pathyz, update( ops, ["color","green"]) );
	
	pathxz = randPts(5, y=0);
	Chain( pathxz, update( ops, ["color","blue"]) );
	
}

module Chain_demo_3()
{
	pathxy = randPts(8);
	Chain( pathxy, ["color","red", "transp", 0.4, "usespherenode", false]) ;
	//echo(pathxy);
	
	pathxy2 = randPts(5);
	Chain( pathxy2, ["r",0.2, "color","green", "transp", 0.4, "usespherenode", false]) ;
	
	//pathyz = randPts(4, x=0);
	//Chain( pathyz, update( ops, ["color","green"]) );
	
	//pathxz = randPts(5, y=0);
	//Chain( pathxz, update( ops, ["color","blue"]) );
	
}

module Chain_demo()
{
	Chain_demo_1();
	//Chain_demo_2();
	//Chain_demo_3();
}
//Chain_demo();
//doc(Chain);

module Chainf(pts, ops=[]){  // formatted Chain
    ops = concat( ops, ["labelbeg", "P","r",0.01, "closed",false]);
    labelbeg= hash(ops,"labelbeg");
    //echo(" in chainf, ops",ops);
    Chain( pts, ops=ops );
    LabelPts( pts, labelbeg=="P"?"PQRSTUVWXYZABCDEF":
                    labelbeg==0?range(pts):labelbeg
                , ["scale", hash(ops,"scale", 0.03)] 
                 );
    MarkPts( pts, ["r",0.06] );
}    




//========================================================
ColorAxes=["ColorAxes", "ops", "n/a", "Shape",
 " Paint colors on axes. 
;;  [
;;   ''r'',0.012
;;  ,''len'', 4.5
;;  ,''xr'',-1
;;  ,''yr'',-1
;;  ,''zr'',-1
;;  ,''xops'', [''color'',''red''  , ''transp'',0.3]
;;  ,''yops'', [''color'',''green'', ''transp'',0.3]
;;  ,''zops'', [''color'',''blue'' , ''transp'',0.3]
;;  ]
"];

module ColorAxes( ops=[] )
{
	ops = concat( ops,
	[
		"r",0.012
		,"len", 4.5
		,"xr",-1
		,"yr",-1
		,"zr",-1
		,"fontscale", 1
		,"xops", ["color","red", "transp",0.3]
		,"yops", ["color","green", "transp",0.3]
		,"zops", ["color","blue", "transp",0.3]
	]);
	function ops(k)= hash(ops,k);
	r = ops("r");
	l = ops("len");

	fscale= ops("fontscale");
	//echo( "fscale" , fscale);
	//include <../others/TextGenerator.scad>
	
	xr= hash( ops, "xr", if_v=-1, then_v=r) ;
	yr= hash( ops, "yr", r, if_v=-1, then_v=r);
	zr= hash( ops, "zr", r, if_v=-1, then_v=r);
	
	echo( "r",r, "xr", xr);

	Line0( [[ l,0,0],[-l,0,0]], concat( ["r", xr], ops("xops") ));
	Line0( [[ 0, l,0],[0, -l,0]], concat( ["r", yr], ops("yops") ));
	Line0( [[ 0,0,l],[0,0,-l]], concat( ["r", zr], ops("zops") ));

	xc = hash(ops("xops"), "color");
	yc = hash(ops("yops"), "color");
	zc = hash(ops("zops"), "color");
	translate( [l+0.2, -6*r,0]) 
		rotate($vpr) scale( fscale/15 ) 
		color(xc) text( "x" );
	translate( [6*r, l+0.2, 0]) 
		rotate($vpr) scale( fscale/15 ) 
		color(yc) text( "y" );
	translate( [0, 0, l+0.2]) 
		rotate($vpr) scale( fscale/15 ) 
		color(zc) text( "z" );
} 
module ColorAxes_test(ops){ doctest( ColorAxes ,ops=ops);}


//========================================================
Cone=["Cone","pq, ops=[]", "n/a", "Geometry"
, "Given a 2 or 3-pointer pq and ops, make a cone along the PQ
;; line. If pq a 3-pointer, the points forming the cone bottom
;; will start from plane of the 3 points and circling around 
;; axis PQ in clockwise manner. Default ops:
;;
;; [ ''r'' , 3  // rad of the cone bottom
;; , ''len'', 0.3  // height of the cone
;; , ''fn'', $fn==0?6:$fn // # of segments along the cone curve,
;; ]
;;
;; New feature: You can mark points @ run-time:
;; 
;; Cone(pq, [''markpts'', true] )
;; Cone(pq, [''markpts'', [''labels'',''ABCDEF''] )
;; 
"];
module Cone_test(ops=[]){
    doctest( Cone, [], ops); }
    
module Cone( pq, ops=[] ){
    //_mdoc("Cone","");
                     
	ops= concat( ops
    , [ "r" , 1            // rad of the curve this obj follows
	  , "len", undef       // default to len_PQ. Could be negative
      , "fn", $fn==0?6:$fn // # of segments along the cone curve,
                           // = # of slices or segments
      , "markpts", false   // true or hash
      , "twist", 0
      ], OPS);

    /*      Q
           _|_ 
      _--'' | ''--_
    -{------P------}----- 
      ''-.__|__.-''
            |      | 
            |<---->|
                r    
    */
    
    function ops(k,df)=hash(ops,k,df);
    
    //echo("ops= ", ops);
    
    r = ops("r");
    fn= ops("fn");
    
    P = pq[0]; Q=pq[1]; 
    R= len(pq)==3? pq[2]: anglePt( [Q,P, randPt()], a=90, r=r);
    
    pqr = [P,Q,R];
    
    //     0
    //     | 
    //  _4-|-3_
    // 5_  +  _2
    //   -6-1-
    //
    // faces= [ [0,2,1]
    //        , [0,3,2] 
    //        , ... 
    //        , [0,6,5]
    //        , [0,1,6]
    //        ]
    
    faces= concat(
            [ for( i =range(fn) ) [0, i>fn-2?1:i+2, i+1] ]
            , [range(1, fn+1)]    
      );  

    //echo("faces", faces);
       
    arcpts = arcPts( [Q,P,R]
                    , pl="yz"
                    , a=360
                    , count=fn
                    , r=r
                    , twist= ops("twist")
                    ); 
//    echo( "arcpts", arcpts);   
            
    df_markpts= ["samesize",true, "labels", range(1, fn+1)];       
    m = ops("markpts");
	if(m) { MarkPts(arcpts , concat( isarr(m)?m:[], df_markpts));}
    
    color( ops("color"), ops("transp"))        
    polyhedron( points= concat( [Q], arcpts )
              , faces = faces );               
}

module Cone_demo_1(){
_mdoc("Cone_demo_1"
," yellow: Cone( randPts(2) )
;; green: Cone( randPts(3) , [ ''fn'',6
;;                , ''color'',''green''
;;                , ''transp'',0.5
;;                , ''markpts'',true  // <=== new feature in scadex
;;                ] 
 ");    

    pq = randPts(2);
    MarkPts( pq, ["labels","PQ"]);
    Cone( pq );
    LabelPt( pq[1], "  Cone(pq)");

    pqr = randPts(3);
    Plane(pqr, ["mode",3]);
    MarkPts( pqr, ["labels","PQR"]);
    Cone( pqr , [ "fn",6
                , "color","green"
                , "transp",0.5
                , "markpts",true  // <=== new feature in scadex
                ]);
    LabelPt( pqr[1], "  Cone(pqr,[\"fn\",6,\"markpts\",true...])"
            , ["color","green"]);
                            
    pqr2 = randPts(3);
    Plane(pqr2, ["mode",3]);
    MarkPts( pqr2, ["labels","PQR"]);
    Cone( pqr2 , [ "fn",6
                , "color","blue"
                , "transp",0.5
                , "markpts",true  // <=== new feature in scadex
                , "twist", 30 // <=== new 2015.1.25
                ]);
     LabelPt( pqr2[1]
     , "  Cone(pqr,[\"fn\",6,\"markpts\",true,\"twist\",30...])"
            , ["color","blue"]);
            
}    

//Cone_demo_1();



//========================================================
module Cube(pqr, p=undef, r=undef, h=1)
{
	echom("Cube");
	cpts =cubePts(pqr,p=p,r=r,h=h);
//	echo("pqr:", pqr);
//	echo("cpts:", cpts);
	polyhedron( points= cpts, faces = CUBEFACES );
}

module Cube_demo()
{
	Cube(randPts(3));
	Cube(randPts(3),p=0.2,r=0.4,h=5);
	Cube(randPts(3),p=5,r=5,h=0.2);
}

//Cube_demo();




//========================================================
CurvedCone=["CurvedCone","pqr, ops=[]"
   , "n/a", "Geometry"
, "Given a 3-pointer pqr and ops, make a curved 
;; cone, where ops is:
;;
;; [ ''r'' , 3  // rad of the curve
;; , ''r2'', 0.3  // rad of the bottom of the cone
;; , ''a'', undef // angle determining cone length
;;              // if not given, use a_pqr
;; , ''fn'', $fn==0?6:$fn // # of segments along the cone curve,
;; , ''fn2'', $fn==0?6:$fn // # of points along bottom circle 
;; ]
"];
module CurvedCone_test(ops=[]){
    doctest( CurvedCone, [], ops); }
    
module CurvedCone( pqr, ops=[] )
{
/*          	. R  						 	       _
             _-' -      
          _-'     :
       _-'         :     
    _-'            : 
  Q+---------------+ P



                R  |N (N is on pl_pqr, and NP perp to PQ )
             _-' : | 
          _-'     || 
       _-'        _|_ 
    _-'      _--'' | ''--_
  Q+--------{------P------}----- 
             ''-.__|__.-''
                   |      | 
   |<------------->|<---->|
           r           r2 
    
*/
    //_mdoc("CurvedCone","");
                     
	ops= concat( ops
    , [ "r" , undef  // rad of the curve this obj follows
	  , "r2", 0.5  // rad of the bottom of the obj 
      , "a", undef // angle determining cone length
                   // if not given, use a_pqr
      , "fn", $fn==0?6:$fn // # of segments along the cone curve,
                    // = # of slices or segments
      , "fn2", $fn==0?6:$fn // # of points along bottom circle
      ], OPS);

    function ops(k,df)=hash(ops,k,df);
    

    r= ops("r")==undef?d01(pqr):ops("r"); 
    echo("r", r, "d01(pqr)", d01(pqr));
    
    a = ops("a")? ops("a"): angle(pqr);
    P = pqr[0]; Q=pqr[1]; R= anglePt(pqr, a, len=r);
    
    r2= ops("r2");
    fn= ops("fn");
    fn2=ops("fn2");
    
    pqr = [P,Q,R];
   
    function getPtsByLayer(i)=
    (   let( // X is a point along the R->P curve
             X = anglePt( pqr
                  , a=(fn-i-1)*a/(fn)
                  , len=r )
           , N = N([Q,X,R])
           ) 
        arcPts( [Q,X,N]
                    , pl="xy"
                    , a=360
                    , count=fn2
                    , r=r2 /fn*(i+1)
                    )
    );

    // This for loop is for debug only. It displays
    // lines thru circling pts of each layer, and also
    // label pts
    //    for( i = range(fn) ){
    //       pts = getPtsByLayer(i); 
    //       Chain( pts, ["r",0.01]);
    //       MarkPts( pts, ["samesized",true, "r", 0.02] );
    //       LabelPts( pts, range( i*fn2+1, (i+1)*fn2+1 )
    //                , ["scale",0.02] ); 
    //    }    
    
    
    // pts: [ [p10,p11,p12...p16]  <== layer 1
    //      , [p20,p21,p22...p26]
    //      ...
    //      , [pi0,pi1,pi2...pi6] ] <==layer i    
    pts_layers = [ for( i = range(fn) )
                   getPtsByLayer(i) ];
    //echo("");
    //echo("pts_layers: ", pts_layers);
    //echo( "pts_layers = ", pts_layers );
        
    pts = [ for(a=pts_layers, b=a) b] ; 
       
    //echo("");
    //echo("pts: ", pts);
    //echo("pts = ", pts); 
        
    ptsAll = concat( [R], pts );
    //echo("");
    //echo("with R: ptsAll: ", ptsAll);
    
    /*
    faces of a raised pantagon :
    
    faces = [ [1,0,5,6]
            , [2,1,6,7]
            , [3,2,7,8]
            , [4,3,8,9]
            , [0,4,9,5]
            , [0,1,2,3,4]
            , [5,6,7,8,9]
            ];
            
    fn = 4
    fn2= 6
    layer i = range( fn)
    faces= 
    [ [1,6,12,7]    // i = 1  
   , [2,1, 7,8]                    
   , [3,2, 8,9]
   , [4,3, 9,10]
   , [5,4,10,11]
   , [6,5,11,12]
   
   , [7,12,18,13]  // i = 2, j= range(fn2)  
          [(i-1)*fn2+1, fn2*2, fn2*3, i*fn2+1) 
   , [8,7, 13,14] 
          [(i-1)*fn2+j+1, (i-1)*fn2+j, i*fn2+j, i*fn2+j+1) 
   , [9,8,14,15]     
   , [10,9,15,16]
   , [11,10,16,17]
   , [12,11,17,18]
   
   , [13,18,24,19] // layer = 3
   , [14,13,19,20]
   , [15,14,20,21]
   , [16,15,21,22]
   , [17,16,22,23]
   , [18,17,23,24] 
   ]
            
    Last 2 are easy to make. Prior to that,         
    we make a "preface" first then transpose it:
       
       [ [1,2,3,4,0]  : concat( range(1,5), [0])
       , [0,1,2,3,4]  : range( 0, 5 )
       , [5,6,7,8,9]  : range( 5, 5+5 )
       , [6,7,8,9,5]  : concat( range(6,5+5), [5]) 
       ]
    ---------------------   
    If there are more than one layer, the above
    is the layer 0. Each layer will inc by 5:
    
    i=1:
       [ [5,6,7,8,9]  : range( 5, 5+5 )
       , [10,11,12,13,14]: range( 5+5, 10+5 )
       , [11,12,13,14,10]: concat( range(5+6,10+5), [10]) 
       , [6,7,8,9,5]  : concat( range(5+1,10), [5]) 
       ]
    
    layer i, fn:
    
        [ concat( range( i*fn+1, (i+1)*fn ), [fn] )
        , range( i*fn, (i+1)*fn )
        , range( (i+1)*fn, (i+2)*fn )
        , concat( range((i+1)*fn+1,(i+2)*fn),[i*fn] )
        ]
    */    
    
    // Faces w/o the first layer (which contains
    // triangles) and the bottom layer
//    function getSideFacesByLayer(i, fn, hasnext=false)=
//      transpose(
//        [ concat( range( i*fn+1, (i+1)*fn ), [fn] )
//        , range( i*fn, (i+1)*fn )
//        , range( (i+1)*fn, (i+2)*fn )
//        , hasnext? range((i+1)*fn+1,(i+2)*fn+1) 
//          : concat( range((i+1)*fn+1,(i+2)*fn),[i*fn] )
//        ]
//      )
//      ;
    
    // Layer0 contains triangles
    // 
    //     0
    //     | 
    //  _4-|-3_
    // 5_  +  _2
    //   -6-1-
    //
    // faces= [ [0,2,1]
    //        , [0,3,2] 
    //        , ... 
    //        , [0,6,5]
    //        , [0,1,6]
    //        ]
    
    function getLayer0Faces(fn)=
      [ for( i =range(fn-1) )
        [ 0, i>fn-3?1:i+2, i+1]
      ];  

    
    sidefaces = getTubeSideFaces
      ( [ for (i=range(1, fn+1))
          [ for (j=range(fn2)) (i-1)*fn2+j+1 ]
        ]      
      , isclockwise=true 
      );
    //echo("");      
    //echo("\nsidefaces = ",sidefaces);
                
    faces = mergeA( 
            [getLayer0Faces(fn2+1)
            , mergeA( sidefaces )  
            ,  [range((fn-1)*fn2+1, fn*fn2+1)] 
            ]);                
    //echo("");            
    //echo( "faces: ", faces);
    //echo("");
       
    //color("green", 0.8)            
    polyhedron( points= ptsAll
              , faces = faces );               
}

module CurvedCone_demo_1(){
_mdoc("CurvedCone_demo_1"
," yellow: CurvedCone( randPts(3) )
;; red  : a=60, r=8, r2=1,  fn= 4, fn2=6 
;; green: a=30, r=8, r2=0.5, fn=10, fn2=20 
;; blue : a=120,r=3, r2=1  , fn= 9, fn2=4 
 ");    

    pqr = randPts(3);
    MarkPts( pqr, ["labels","PQR"] ); Chain(pqr, ["closed",false]); 
    
    CurvedCone( pqr );
    LabelPt( midPt(p12(pqr)), "CurvedCone(pqr)");
    
    color("red", 0.6)
    CurvedCone( randPts(3), ["a",60
                     , "r", 8
                     , "r2", 1
                     , "fn",4 
                     , "fn2",6 
                     ] );
                     
    color("green", 0.6)
    CurvedCone( randPts(3), ["a",30
                     , "r", 8
                     , "r2", 0.5
                     , "fn",10
                     , "fn2",20
                     ] );
    
    color("blue", 0.6)
    CurvedCone( randPts(3), ["a",120
                     , "r", 3
                     , "r2", 1
                     , "fn",9
                     , "fn2",4
                     ] );    
}    

//CurvedCone_demo_1();


//========================================================
//cycleRange=["cycleRange","pts/i, 



//========================================================
DashBox=[ "DashBox", "pq,ops", "n/a", "3D, Shape", 
 " Given a 2-pointer (pq) and ops, make a box with dash lines parallel
;; to axes. The default ops is:
;;
;;   [ ''r'',0.005
;;   , ''dash'', 0.05
;;   , ''space'', 0.05
;;   , ''bySize'', true
;;   ]
;;
;; where r= radius of dash line, dash= the length of dash, space= space
;; between dashes. The p,q are the opposite corners of the box if bySize=
;; false. If bySize=true, p is a corner, and q contains size of box on
;; x,y,z direction.
"];  

module DashBox(pq, ops)
{
	ops=update(
		[ "r",0.005
		, "dash", 0.1
		//, "space", 0.05
		, "bySize", true
		], ops );
	function ops(k)= hash(ops,k);

	echo( ops("bySize"));

	pts = boxPts( pq, bySize= ops("bySize") );

	r= ops("r");

	color("blue"){
		Line( [ pts[0], pts[4] ], ops);
		DashLine( [ pts[1], pts[5] ], ops );
		DashLine( [ pts[2], pts[6] ], ops );
		DashLine( [ pts[3], pts[7] ], ops );
	}
	color("red"){
		DashLine( [ pts[0], pts[1] ], ops);
		DashLine( [ pts[2], pts[3] ], ops );
		DashLine( [ pts[4], pts[5] ], ops );
		DashLine( [ pts[6], pts[7] ], ops );
	}
	color("lightgreen"){
		DashLine( [ pts[0], pts[3] ], ops);
		DashLine( [ pts[1], pts[2] ], ops );
		DashLine( [ pts[4], pts[7] ], ops );
		DashLine( [ pts[5], pts[6] ], ops );
	}
}


module DashBox_test( ops ) {	doctest( DashBox, ops=ops); }

module DashBox_demo(pq, bySize=true)
{
	pq = [ [2,1,-1],[3,2,1]];
	DashBox( pq , [["bySize", bySize]]);

	//boxPts_demo( pq, bySize );
	MarkPts( boxPts( pq ) );

}
//DashBox_demo(bySize=true);
//doc(DashBox);


//========================================================
DashLine=["DashLine","pq,ops", "n/a", "3D, Shape", 
 " Given two-pointers(pq) and ops, draw a dash-line between pq.
;; Default ops:
;;
;;   [ ''r''     , 0.02     // radius
;;   , ''dash''  , 0.5      // dash length
;;   , ''space'' , undef    // space between dashes, default=dash/3
;;   , ''color'' , false
;;   , ''transp'', false
;;   , ''fn''    , $fn
;;   ]
"];

module DashLine(pts, ops=[])
{
	//echo("DashLine:");
	pts=countArr(pts)==0?
		[[0,0,0], pts] : pts;

	ops= update(["r",0.02
				,"dash",0.5
				,"space",undef
				,"color", false
				,"transp", false
				,"fn",$fn
				], ops);
	function ops(k)= hash(ops,k);

	dash = ops("dash");
	space= hash(ops, "space", dash/3); 
	s= dash+space;
	L = norm(pts[1]-pts[0]);
	count = L/s;
	if( L<=dash )
	{
		Line0( pts, ops );	
	}else if (count<1)
	{
		Line0( [pts[0], onlinePt(pts, ratio=dash/L)], ops );
	}else{
		for (i=[0:count])
		{
			if((i*s+dash)>L)
			{
				Line0( [ onlinePt(pts, ratio=i*s/L)
			  		  , pts[1] ], ops);

			}else{
				Line0( [ onlinePt(pts, ratio=i*s/L)
				  , onlinePt(pts, ratio=(i*s+dash)/L)]
				  ,ops);
			}
		}
	}
}


module DashLine_test(ops){ doctest(DashLine,ops=ops); }

module DashLine_demo(){

	pts= randPts(2);
	DashLine( pts); 
	MarkPts(pts);
	
	pts2 = randPts(2);
	DashLine( pts2, ops=["dash",0.02, "space",0.06] );
	MarkPts(pts2);
}
//DashLine_demo();
//DashLine_test();
//doc(DashLine);



//========================================================
LabelPt=["LabelPt", "p,label,ops=[]", "n/a", "Geometry, Doc",
 " Given a point (p), a string (label) and option (ops), write the
;; label next to the point. The label is written according to the 
;; viewpoint ($vpr) such that it is always facing the viewer. Other
;; properties of label are decided by ops. 
"];

module LabelPt_test( ops=["mode",13] ) { doctest( LabelPt, ops=ops ); }

// Require TextGenerator 
module LabelPt(p,label, ops=[]) 
{
	//include <../others/TextGenerator.scad>
	ops = concat( ops,
	[ "shift", false  // could be: 2.5, [3,1,0] 
	, "scale", 0.03
	], OPS);
	scale = hash(ops,"scale");
	_shift = hash(ops, "shift"
				, if_v=false
				, then_v=[1,1,1]*scale*3 );
				//, else_v= [0,0,0] ) ;
	shift = isarr(_shift)?_shift:_shift*[1,1,1] ;
	//echo("shift", shift, "scale", scale);
	//echo("p",p,"label: ", label);
	//echo("LabelPt ops", ops);
	translate( p+ shift ) 
		scale( scale ) 
		rotate($vpr) 
		color( hash(ops,"color"), hash(ops,"transp") ) 
		text( str(label), font="Liberation Mono" );
		//drawtext( str(label) );
		
}

module LabelPt_demo()
{
	pqr = randPts(3);
	MarkPts( pqr, ["r", 0.05] );

	LabelPt( pqr[0], "P" );
	LabelPt( pqr[1], "Q" );
	LabelPt( pqr[2], "R" );

	pt= randPt();
	MarkPts([pt]);
	LabelPt ( pt, "Scale=0.05", ["scale",0.05] ); 
	 
}
//LabelPt_demo();




//========================================================
LabelPts=["LabelPts", "pts,labels,ops=[]", "n/a", "Geometry, Doc",
 " Given points (pts), labels (array of strings or a string) and option
;; (ops), write the
;; label next to the point. The label is written according to the 
;; viewpoint ($vpr) such that it is always facing the viewer. Other
;; properties of label are decided by ops. 
;;
;; New args 2014.8.16: 
;;
;;   prefix, suffix, begnum, usexyz
;;
"];

module LabelPts_test( ops=["mode",13] ) { doctest( LabelPts, ops=ops ); }


module LabelPts( pts, labels="", ops=[] )
{
	//echom("LabelPts");
	ops = concat(ops
	, [ "color", [0.4,0.4,0.4]
	  , "colors", COLORS
	  , "prefix", "" //labels?"":""
	  , "suffix", ""
	  , "begnum", 0
	  , "usexyz",false
      ], OPS );
	function ops(k)= hash(ops, k);
	begnum = ops("begnum");
	prefix = ops("prefix");
	suffix = ops("suffix");
	usexyz = ops("usexyz");
    colors = hash( ops, "colors"); //, false)
    
	function getlabel(i)= 
	(
		usexyz
		? pts[i]
		:str( prefix, labels?labels[i]:(begnum+i), suffix )
	);
    
	//echo("ops",ops);
	circle =concat([get(pts,-1)], pts, [pts[0]]);
	//echo("circle: ",circle);
	if(len(pts)<3){

//		for (i=[0:len(pts)-1])	assign( 
//			colors = hash( ops, "colors", false)
//		){ 
        for (i=[0:len(pts)-1])	{
            
			LabelPt( pts[i]
				, getlabel(i) //labels?labels[i]: getlabel(i)
				, concat( ["color", hash(ops,"color", colors[i])]
						, ops ) ); 
	
		}
	
	}else{

		for (i=[0:len(pts)-1])
		{
            abp = angleBisectPt( 
                   [ circle[i], circle[i+1], circle[i+2] ] 
            );			   
//		for (i=[0:len(pts)-1])
//		assign( colors = hash( ops, "colors", false)
//			 , abp = angleBisectPt( [ circle[i], circle[i+1], circle[i+2] ] )
//			 ) 
//		{ 
			//echo("in LabelPts, i = ", i);
			LabelPt( onlinePt( [pts[i],abp], len=-0.2)
				, getlabel(i) //labels?labels[i]:getlabel(i)
				, concat( ["color", hash(ops,"color", colors[i])]
						, ops ) ); 
	
		}
	}

}


module LabelPts_demo_1()
{
	pqr = randPts(3);
	MarkPts( pqr, ["r", 0.05] );
	LabelPts( pqr, "PQR" );
	Plane( pqr, ["mode",3] );

	lmn = randPts(3);
	MarkPts( lmn, ["r",0.05] );
	Plane( lmn, ["mode",3] );
	LabelPts( lmn, ops=["color","red"]);

	fghi = randPts(4);
	MarkPts( fghi, ["r",0.05] );
	Plane( fghi, ["mode",3] );
	LabelPts( fghi, "FGHI", ["color","green"] );

//	A = randPts(1);
//	MarkPts( A, ["r",0.05, "grid", true] );
//	LabelPts( A, ["Single A"], ["color","blue"] );

	pts = randPts(5);
	MarkPts( pts, ["r",0.05, "samesize",true, "sametransp",true] );
	LabelPts( pts, range(pts), ["color","purple"] );
	Chain(pts, ["r", 0.02 ] );
}
module LabelPts_demo_2_scale()
{
	pqr = randPts(3);
	MarkPts( pqr, ["r", 0.05] );
	LabelPts( pqr, "PQR" );
	Chain( pqr, ["r",0.02, "closed", false, "color","red"] ); 
	LabelPt( pqr[0], "scale=0.03 (default)",["color","red"] );

	lmn = randPts(3);
	MarkPts( lmn, ["r",0.05] );
	Chain( lmn, ["r",0.02, "closed", false, "color","green"] ); 
	LabelPts( lmn, "lmn", ops=["scale",0.06]);
	LabelPt( lmn[0], "scale=0.06", ["color","green"] );

	fghi = randPts(4);
	MarkPts( fghi, ["r",0.05] );
	Chain( fghi, ["r",0.02, "closed", false, "color","blue"] ); 
	LabelPts( fghi, "FGHI", ["scale",0.02,"color","red"] );
	LabelPt( fghi[0], "scale=0.02", ["color","blue"] );
}

module LabelPts_demo_3_prefix_begnum_etc()
{
	// New 2014.8.16:
	// prefix, suffix, begnum, usexyz

	// Testing prefix, suffix with given labels 
	pqr = randPts(3);
	MarkPts( pqr, ["r", 0.05] );
	LabelPts( pqr, range(pqr),["color","red", "prefix","p[", "suffix","]"] );
	Chain( pqr, ["r",0.02, "closed", false] ); 


	// Testing begnum=10 
	lmn = randPts(3);
	MarkPts( lmn, ["r",0.05] );
	Chain( lmn, ["r",0.02, "closed", false, "color","green"] ); 
	LabelPts( lmn,ops=["scale",0.06, "begnum",10]);


	fghi = randPts(4);
	MarkPts( fghi, ["r",0.05] );
	Chain( fghi, ["r",0.02, "closed", false, "color","blue"] ); 
	LabelPts( fghi, fghi, ops=["scale",0.02,"color","blue"] );
	//LabelPt( fghi[0], "scale=0.02", ["color","blue"] );
}
module LabelPts_demo()
{
	//LabelPts_demo_1();
	//LabelPts_demo_2_scale();
	LabelPts_demo_3_prefix_begnum_etc();
}
//LabelPts_demo();





////========================================================
//Line =["Line", "pq,ops", "n/a", "3D, Shape",
//"
// Given a 2-pointer pq and ops, draw a line between p,q in pq. The
//;; default ops:

module Line(pq, ops=[])
{
    //_mdoc("Line","");
    //P= pq[0]; Q=pq[1];
    pqr= len(pq)==3?pq:concat(pq, [randPt()]); 
    
    df_ops=[ "r", 0.01     // radius
        , "rP", undef   // radius at the P end
        , "rQ", undef   // radius at the Q end
    
        , "len", d01(pq) 
        , "ext", 0      // extend outward. A # for len or "r#" for ratio
        , "extP",undef  // extend outward from P pt
        , "extQ",undef  // extend outward from Q pt
    
        , "fn", $fn
        , "markpts", false // Mark pts. false:disable; true:default
                           // hash to customize
        , "dim", false // true or hash, to show dimension. 
    
        , "a", 0    
        , "aP", undef
        , "aQ", undef
    
        , "twist", 0   // twist the line pts counter-clockwise away
                       // from the pqr plane when pq is a 3-pointer
                       // (= pqr). If pq is a 2-pointer, a random
                       // R will be assigned. 
        , "shift", undef
        ]; 
     
    _ops= update( df_ops, ops);
     //echo(" line,_ops", _ops);
    function ops(k,df)=hash(_ops,k,df);
       
    len= ops("len");
    ext= ops("ext");
    // if "r0.4", treat it as a ratio and convert to len
        _extP=ops("extP", ext);
        _extQ=ops("extQ", ext);
        extP = isstr(_extP)&&index(_extP,"r")==0
                ?num(slice(_extP,1))*len: _extP ;
        extQ = isstr(_extQ)&&index(_extQ,"r")==0
                ?num(slice(_extQ,1))*len: _extQ ;

    r = ops("r");
    rP= ops("rP",r);
    rQ= ops("rQ",r);
    
    fn= ops("fn");
    tw= ops("twist");
    sh= ops("shift");
    _markpts = ops("markpts"); 
    markpts = _markpts==true?[]:_markpts;
    
    //echo("ops", ops);

    newpq= linePts( pqr, len= [extP,extQ] );
    
    P= newpq[0]; 
    Q= newpq[1]; 
    R= pqr[2];
    
    //pq = linePts;
    //echo(_s( "P={_}, Q={_}",[P,Q]));
    pqr2= [P,Q,R];

    dim=ops("dim");
    if(dim) Dim2( pqr2, ops=ishash(dim)?dim:[]); //
    //echo("=> pqr2", pqr2 );
    // S is to make same clockwise-ness on P and Q
    //
    //  o-----o================o
    //  S     P                Q
    S = onlinePt(pqr2, len=-1);   
    spr = [S,P,R]; 
    ptsP = arcPts( pqr2, a=360, r=rP, count=fn, pl="yz" );
    ptsQ = arcPts( spr, a=360, r=rQ, count=fn, pl="yz" );
    
    pts = concat( ptsQ, ptsP );
    
    //echo("markpts", markpts);
    
    if(ishash(markpts))
        Chainf(pts, concat(markpts,["labelbeg",0]) );
    
    faces = faces("rod", sides=fn); 
    //echo("faces", faces);
    
    color(ops("color"), ops("transp"))
    polyhedron( points = pts, faces= faces );
}

module Line_demo_1_basic() 
{_mdoc("Line_demo_1_basic","");
 
    pq = randPts(2);
    MarkPts( pq, ["labels","PQ"] );
    Line( pq );
    LabelPt( pq[0], "Line(pq)");
    
    pq2= randPts(2);
    Line(pq2, ["dim",true]);
    LabelPt( pq2[0], "Line(pq, [\"dim\",true])", ["color","blue"]);
    
    pqr= randPts(3);
    MarkPts( pqr, ["labels", "PQR"] );
    Line( pqr, ["dim",true] );
    LabelPt( pqr[1], "Line(pqr, [\"dim\",true])", ["color","green"]);
}    

module Line_demo_2()
{_mdoc("Line_demo_2","");
    
    pqr = randPts(3);
    //MarkPts( p01(pqr), ["labels","PQ"] );
    Line( pqr, ["r",0.2, "len", 3, "dim",true] );
    //Dim2( pqr );
    
    pq2= randPts(2);
    Line(pq2);
    LabelPt( pq2[0], "Line(pq)");
}    
//Line_demo_1_basic();
//Line_demo_2();


module Line_test( ops ){doctest (Line, ops=ops); }


module Line_demo_1_default()
{
	echom("Line_demo_1_default");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true]);
}

module Line_demo_1a_extension()
{
	ops=["transp",0.3,  "markpts",true, "transp",0.4] ;

	echom("Line_demo_1a_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, update(ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line(pq2, update(ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	Line(pq3, update(ops, ["color","blue","extension0", 1,"extension1", 1]));

	pq4= randPts(2);
	//MarkPts(pq3);
	Line(pq4, update(ops, ["color","purple","extension0", -0.5,"extension1", -0.5]));

}

module Line_demo_2_sizeColor()
{
	echom("Line_demo_2_sizeColor");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, [ "r", 0.5
			 , "color","green"
			 , "transp",0.3
			 , "fn", 6
			 , "markpts",true
			 ]);
}

module Line_demo_3a_head_cone()
{
	echom("Line_demo_3a_head_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true, "head"
			  ,["style","cone"]
			 ]);
}

module Line_demo_3b_head_cone_settings()
{
	echom("Line_demo_3b_head_cone_settings");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true,"head"
			  , [ "style","cone"
				,"color","blue"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5
			    ]
			 ]);
}

module Line_demo_4a_tail_cone()
{
	echom("Line_demo_4a_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				, "color","red"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5		    
				]
			 ]);
}

module Line_demo_4b_tail_cone()
{
	echom("Line_demo_4b_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				,"r", 0.2
				, "r0",0.6
				]
			 ]);
}

module Line_demo_5a_headtail_cone()
{
	echom("Line_demo_5a_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq,["markpts",true
			  ,"head"
			  , [ "style","cone"
			    ]
			  
			, "tail"
			  , [ "style","cone"
			 	]
			  
			 ]);
}

module Line_demo_5b_headtail_cone()
{
	echom("Line_demo_5b_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"head"
			  , [ "style","cone"
				, "color","blue"
				, "r", 0.4
				, "len",1
			 	, "transp", 0.6
				, "fn", 5
			    ]
			, "tail"
			  , [ "style","cone"
				, "r", 0.2
				, "r0",0.6
			 	]
			 ]);
}

module Line_demo_6a_headtail_sphere()
{
	echom("Line_demo_6a_headtail_sphere");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"head"
			  , [ "style","sphere"
				, "color", "yellow"
				, "r",.5
				, "transp", 0.4
			    ]
			  
			, "tail"
			  , [ "style","sphere"
				,"color", "red"
				, "r", .4
				, "transp", 0.4
				]
			  
			 ]);
}


module Line_demo_7_headtail_fn()
{
	echom("Line_demo_7_headtail_fn");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				]
			 ]);
}


module Line_demo_8_rotate()
{
	echom("Line_demo_8_rotate");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				, "rotate", 45  //<====
				]
			 ]);
}


module Line_demo_9a_block()
{
	echom("Line_demo_9a_block");
	pq= randPts(2);
	//pq = [ [2,1,3], [4,3,2]];
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"style","block"]);
}


module Line_demo_9a2_block_extension()
{
	ops=["transp",0.3,  "markpts",true,"style","block"] ;

	echom("Line_demo_9a2_block_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, update( ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line(pq2, update( ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	//Line(pq3, update( ops, [["color","blue"],["extension0", 1],["extension1", 1]]));

	pq4= randPts(2);
	//MarkPts(pq3);
	//Line(pq4, update( ops, [["color","purple"],["extension0", -0.5],["extension1", -0.5]]));

}

module Line_demo_9b_block()
{
	echom("Line_demo_9b_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 //, "markpts",true
		];

	Line([O, [0,0,z+1]], [ "style","block"
					   //, ["markpts",true]
			  		   , "width",0.5
					   , "depth",4
					  ]);

	Line([[0,0,w], [x,0,w]], update(ops, ["color","red"]));
	Line([[x-w,0,0], [x-w,0,z]], update(ops, ["color","green"]) );

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true]
		 ) );
	
}


module Line_demo_9c_block()
{
	echom("Line_demo_9c_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","corner2"] // <====
					  ) );


}

module Line_demo_9d_block()
{
	echom("Line_demo_9d_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //["markpts",true]
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid1"] // <====
					  ) );

}

module Line_demo_9e_block()
{
	echom("Line_demo_9e_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [//"markpts",true
			  			 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid2"] // <====
					  ) );

}


module Line_demo_9f_block()
{
	echom("Line_demo_9f_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [//"markpts",true
		 				 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","center"] // <====
					  ) );

}

module Line_demo_9g_block()
{
	echom("Line_demo_9g_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], ["style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift",[2*w, 0,w] ]// <====
					  ) );

}

module Line_demo_10_shift()
{
	echom("Line_demo_10_shift");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
		 	 , "r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "green"
				, "transp",0.5
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "transp",0.5
			    ]
			,"shift", "corner2"
			 ]);
}

module Line_demo2()
{
	pts = [ [-1,0,-1], [2,3,4]  ];
	MarkPts( pts );
	pts = randPts(2);
	//Line2( pts,[["r",0.5]] );
	//MarkPts(pts, [["transp",0.3],["r",0.25]] );
	Line( pts //, [["style","block"]
		, ["markpts",true
		 , "style","block", "transp",0.5
		, "rotate", $t*60
		, "shift", "corner"
		, "head", ["style","cone"]
		, "tail", ["style","cone"]
			]
			);
}

module Line_demo_11_block_touch()
{
	echom("Line_demo_11_block_touch");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "markpts",true
		 , "style","block"
		 , "width",w
		 , "depth",y
		 , "transp",0.5
		 ];

	p_touch = [x/3,y/3,z/3];
	MarkPts([ p_touch ] , ["r",0.3,"transp",0.3]);

	//Line([O, [0,0,z+1]], [ ["style","block"]
	//				   , ["width",w]
	//				   , ["depth",y]
	//				  ]);
	//Line([[0,0,w], [x,0,w]], ops);
	//Line([[x-w,0,0], [x-w,0,z]], ops);

	pts = [[x, 0, z], [0, 0, z+1]];
	//pts=randPts(2);
	Line( pts );
	//MarkPts( pts );
	
	Line( pts
		, update(ops, ["color","blue"//, "shift","corner2"
					  ,"touch", p_touch]  // <====
					  ) );
}

module Line_demo_12_verify_2D()
{
	//pq= [ [ 1.5, 1, 0 ], [ 1, 3, 0 ] ];
	pq_= randPts(2);
	echo(pq_);
	
	pq= [ [ pq_[0].x, pq_[0].y,0]
		, [ pq_[1].x, pq_[1].y,0] ];
	echo(pq);
	MarkPts( pq, ["grid",true] );
	Line( pq, ["markpts", true] );

	/*
	pq2= [ [ 1.5, 0,1 ], [ 1, 0,3 ] ];
	MarkPts( pq2, ["grid",true] );
	Line( pq2, ["markpts", true] );

	pq3= [ [ 0,1.5, 1 ], [ 0, 1, 3 ] ];
	MarkPts( pq3, ["grid",true] );
	Line( pq3, ["markpts", true] );
	*/
}

module Line_demo()
{
	//Line_demo_1_default();
	//Line_demo_1a_extension(); // 2014.5.18
	//Line_demo_2_sizeColor();
	//Line_demo_3a_head_cone();
	//Line_demo_3b_head_cone_settings();
	//Line_demo_4a_tail_cone();
	//Line_demo_4b_tail_cone();
	//Line_demo_5a_headtail_cone();
	//Line_demo_5b_headtail_cone();
	//Line_demo_6a_headtail_sphere();
	//Line_demo_7_headtail_fn();
	//Line_demo_8_rotate();
	//Line_demo_9a_block();
	//Line_demo_9a2_block_extension();
	//Line_demo_9b_block();
	//Line_demo_9c_block();
	//Line_demo_9d_block();
	//Line_demo_9e_block();
	//Line_demo_9f_block();
	//Line_demo_9g_block();
	//Line_demo_10_shift();
	//Line_demo_11_block_touch();
	//Line_demo_12_verify_2D();
}
//Line_test( ["mode",22] );
Line_demo();



//========================================================
Line2 =[["Line2", "pq,ops", "n/a", "3D, Shape"]
	  ,["Given a 2-pointer pq and ops, draw a line between p,q in pq."
	   ,"The default ops:"
		,"                                           " 
		," [[''r'', 0.05]                   // radius of the line    "
		," ,[''style'',''solid'']       // solid | dash | block      "
		," ,[''color'', ''yellow'']                                  "
		," ,[''transp'', 1]                       // transparancy    "
		," ,[''fn'', $fn]                                            "
		," ,[''rotate'', 0]             // rotate alout the line     "
		,", [''markpts'', false]      // Run MarkPts if true"
	    ," ,[''shift'', ''corner''] //corner|corner2|mid1|mid2|center   "
		,"  //................. style-block settings                 "
		," ,[''width'', 1]                                           "
		," ,[''depth'', 6]                                           "
		," ,[''touch'', false ] // a pt where the block will rotate  "
		,"                      // until the block touches the pt.   "
		,"  //................. style-dash settings                  "
		," ,[''dash'', 0.4]     // len of dash                       "
		," ,[''space'', false]  // space between dashes              "
		,"  //................. head/tail                            "
		," ,[''head'',head_ops ]                                     "
		," ,[''tail'',tail_ops ]                                     "
		," ]                                                         "
		,"where head_ops and tail_ops both are:                      "
		,"    [[''style'',false]   // false|cone|sphere              "
		,"    ,[''len'',line_r*6]       // length                    "
		,"    ,[''r'',line_r*3]         // radius                    "    
		,"    ,[''r0'',0]          // smaller radius                 "
		,"    ,[''fn'',ops(''fn'')]                                  "
		,"    ,[''rotate'',0]      // rotate about the line          "  
		,"    ,[''color'', ops(''color'')]                           " 
		,"    ,[''transp'',ops(''transp'')]                          "
		,"    ]                                                      "
		]
		];

module Line2( pq, ops){

	
	//echom("Line");
	//echo("In Line, original pq: ", pq);

	ops= updates( OPS,
		[ "r", 0.05			// radius of the line
		, "style","solid"   // solid | dash | block
		, "markpts", false
		, "shift", "corner" //corner|corner2|mid1|mid2|center
		//................. style-block settings	
		, "w", 1  //,"width", 1
		, "depth", 6
		, "touch", false  // a pt where the block will rotate 
					    // until the block touches the pt.  
		//................. style-dash settings	
		, "dash", 0.4     // len of dash
		, "space", false  // space between dashes
		//................. head and tail extension 2014.5.18
		, "extension0", 0  
		, "extension1", 0  
		//....................
		, "head", false
		, "tail", false

		], ops); function ops(k)= hash(ops,k);
	
	if(ops("markpts")) MarkPts(pq);	

	style = ops("style");
	r 	  = ops("r");
	w     = ops("w");
	headops  = ops("head");
	tailops  = ops("tail");
	depth = ops("depth");
	extension0 = ops("extension0");
	extension1 = ops("extension1");

	//echo("style",style, "rotate", ops("rotate"));

	// Re-sort p,q to make p has the min xyz. This is to 
	// simplify the calc of rotation later.
	p = minyPts(minxPts(minzPts(pq)))[0];
	switched = p!=pq[0];
	//echo("switched? ", switched);
	q = switched?pq[0]:pq[1]; //:pq[0]; 
	L = norm(p-q);
	
	x = q[0]-p[0];
	y = q[1]-p[1];
	z = q[2]-p[2];

	rot = rotangles_z2p( q-p ); 
	//echo_("rot={_}",[rot]);

	/*  
	KEEP THIS SECTION for debugging style=block
	//============================
	pz = [0,0,L];
	pm = rmx( rot[0] )* pz;
	Line0( [O, pz]);
	Line0( [O, pm] );
	
	pn = rmy( rot[1] ) * pm + p;
	Line0( [O+p, pn], r=0.2, color="blue", transp=0.3);
	MarkPts( [pz,pm,pn], [["transp",0.3],["r",0.3]] );
	
	//============================
	*/


	// The shift is a translation [x,y,z] determines how (or, where 
	// on the line/block) the line/block attaches itself to pq. It
	// can be any 3-pointer. If style=block, one of the following 
	// preset values also works:
	//
	//    corner			mid1			mid2			center 
	//    q-----+      +-----+      +-----+      +-----+                      
	//   / \     \    p \     \    / q     \    / \     \     
	//  p   +-----+  +   +-----+  +   +-----+  + p +-----+
	//   \ /     /    \ q     /    p /     /    \ /     / 
 	//    +-----+      +-----+      +-----+      +-----+  

	//    corner2
	//    +-----+   
	//   / \     \  
	//  +   q-----+ 
	//   \ /     /  
 	//    p-----+   
	//

	shift_ = ops("shift");
	shift =  isarr(shift_)? shift_  // if shift_ is something like [2,-1,1]
	:hash(                          // if shift_ is one of the following  
		[ "corner",  [0,0,0] 
		, "corner2", [ -w, 0, 0] 
		, "mid1",    [ -w/2, 0 ,0] 
		, "mid2",    [ 0, -depth/2,0] 
		, "center" , [ -w/2, -depth/2, 0 ] 
		], shift_
	);
    		

	// head/tail settings:
	//
	// Each has a "len" for the length and a "r" for the radius. 
	// Also a r0 if needed depending on head/tail style (for
	// example, cone). Possible head/tail styles: 
	// 	
	// | sphere 
	// | cone : this is in fact a cylinder. In this case, the 
	// |        r and r0 values are assigned to the r1, r2 
	// |        arguments of the cylinder. It's default setting
	// |		   is to make an arrow.

	head0= updates( ops, 
			[ "style",false // false|cone|sphere
			, "len",r*6
			, "r",r*3
			, "r0",0
			], isarr( headops )?headops:[] );

	tail0= updates( ops,
			[ "style",false
			, "len",r*6
			, "r",r*3
			, "r0",0
			], isarr( tailops )?tailops:[] );

	// We need to chk if head/tail should be reversed
	head = switched?tail0:head0;
	tail = switched?head0:tail0;	

	function head(k)= hash(head,k);
	function tail(k)= hash(tail,k);
	
	hstyle = head("style");
	tstyle = tail("style");

	hl 	   = hstyle?head("len"):0;
	tl 	   = tstyle?tail("len"):0;


	// Rotation of head/tail around the line. 
	// If not defined, follow the line rotation
	//hrot   = hasht(head, "rotate", ops("rotate"));
	//trot   = hasht(tail, "rotate", ops("rotate"));	
	hrot   = hash(head, "rotate", ops("rotate"));
	trot   = hash(tail, "rotate", ops("rotate"));
	
	// headcut/tailcut: the length that is required to cut away
	// from the line to accomodate the head/tail. This determines
	// where exactly the head/tail is gonna be placed. For example, 
	// a cone (or arrow) starts from the exact p or q location
	// and extends its length inward toward the center of line.
	// A sphere however is placed at exactly the q or q, so half
	// of it will be extended outside of the p-q line. 
	headcut = hash( [ "cone", hl-0.0001 
					,"sphere", 0
					,false, 0
					], hstyle );
	tailcut = hash( [ "cone", tl-0.0001 
					,"sphere", 0
					,false, 0
					], tstyle );	


	// LL is the actual length that is shorter or equal to L
	// depending on the styles of head and tail. 
	//LL = L- headcut - tailcut;
	LL = L- headcut - tailcut + extension0 + extension1;

	
	// The point to start/end the line taking into consideration 
	// of headcut/tailcut. onlinePt is a function returning the
	// xyz of a pt lies along a line specified by pq.
	ph= head("style")? onlinePt([p,q], ratio = (L-headcut)/L):q; 
	pt= tail("style")? onlinePt([p,q], ratio = tailcut/L):p; 
	

	/*	
	------------ Calc the angle to "rotate to touch pt" in style_block
 
	Let: pqr the 3-pointer on the plane of block surface
		 pt : the touch pt

	        Pt
    		    /|
    		d1 /	 | 			
    		  /  | d2
    		 /a  |
		p----+------q-----r

	If we have r (a 3rd pt on block surface other than pq), we can calc:

		d1 = norm( pt-pq[0])
		d2 = distPtPl( pt, pqr )
	
	BUT, how do we find that 3rd pt r ?

	We know that the block is first placed on yz-plane when created.
	So any pt on yz-plane, like [0,1,1] serves the purpose. 

	We know that the transformation of style-block is:

		translate( tail("style")?pt:p )	 
		rotate(rot)
		rotate( [0, 0, ops("rotate")])
		translate( shift )
		cube( ... )

	By applying "this process" to [0,1,1] (skipping the last 3 lines),
	we should find the 3rd r pt we want. 

	So, how to come up with the forumla for "this process" ?

	We should already have all tools for this (see the following), 
	but for some reason it hasn't worked out yet. Has to come back.

	// First a unit pt on yz surface:
	puz = [0,1,1];

	// This pt is shifted following the shift of the block:
	puz_shift = puz + shift ;

	// It then rotates about z-axis following the block's ops("rotate"):
	puz_opsrot = rmz( ops("rotate"))* puz_shift ;

	// Then the real rotation, first about x, then y:
	puz_rx = rmx( rot.x )*puz_opsrot;
	puz_ry = rmy(rot.y)*puz_rx;

	// It then needs to translocate with the block:
	p3rd = puz_ry +(tail("style")?pt:p);	

	This p3rd is on the surface of the block.

	echo("ops[rotate]", ops("rotate"));
	echo("shift ", shift);
	echo("puz_opsrot ", puz_opsrot);
	echo("puz_rx ", puz_rx);
	echo("puz_ry ", puz_ry);
	echo("p3rd ", p3rd );
	
	//MarkPts([ puz, puz_opsrot, puz_rx, puz_ry, p3rd]
	//	,[["r",0.2],["transp",0.4]] );

	MarkPts([puz, p3rd],[["r",0.2],["transp",0.4]]);

	
	touch = ops("touch");
	//echo("touch ", touch);
	p3rd= rmy(rot.y)*
			rmx(rot.x)*
				rmz( ops("rotate"))* ([0,1,1]+shift)
			 + (tail("style")?pt:p);
	p3rd= rmy(rot.y)*
			rmx(rot.x)*
				rmz( ops("rotate"))* ([0,1,1]+shift)
			 + (tail("style")?pt:p);
	//MarkPts( [ p3rd ], [["r",0.2],["colors",["blue"]],["transp",0.4]] );
	d1 = norm( touch-p);
	d2 = distPtPl( touch, [p,q,p3rd] );
	rotang = -asin(d2/d1);
	//MarkPts([p3rd],[["r",0.2],["transp",0.4]]);
	echo("d1 = ", d1);
	echo("d2 = ", d2 );
	echo("rotang ", rotang);
	
*/
	touch = ops("touch");
	prerot =touch? rotang: ops("rotate");
	//echo("prerot ", prerot);
			

	//echo("Before drawing line, rot = ", rot);

	// ------------------------------------           The line itself :	
	if(style== "dash"){

		color(ops("color"), ops("transp"))
		DashLine([pt,ph], ops);

	} else if (style=="block") {    // block

		translate( tail("style")?pt:p )	 
		rotate(rot)
		rotate( [0,0, prerot]) //ops("rotate")])
		color(ops("color"), ops("transp"))
		translate( shift+ [0,0,switched?-extension1:-extension0] )
    		cube( [ops("width"), ops("depth"), LL ]
			, $fn=ops("fn"));

	} else {                       // solid

		translate( tail("style")?pt:p) 
		rotate(rot)
		rotate( [0,0, ops("rotate")])
    		color(ops("color"), ops("transp"))
		translate( shift+ [0,0,switched?-extension1:-extension0] )
    		cylinder( h=LL, r=r , $fn=ops("fn"));
	} 
 


	// ---------------------------------- The tail :
	//		
	if(tstyle=="cone")
	{
		translate( p )  
		rotate( rot)
		rotate( [0,0,trot] )
		color(tail("color"), tail("transp"))
		translate( shift )
    		cylinder( h=tl, r1=tail("r0"), r2=tail("r"), $fn=tail("fn"));

	} else 

	if (tstyle=="sphere")
	{
		translate( switched?pt:p ) 
		rotate( rot)
		rotate( [0,0,trot] )
		color(tail("color"), tail("transp"), $fn=tail("fn"))
		translate( shift )
    		sphere( r=tail("r"));
	} 

	// ----------------------------------- The head :
	// 
	if(hstyle=="cone")
	{
		translate( ph )  
		rotate( rot)
		rotate( [0,0,hrot] )
		color(head("color"), head("transp"))
		translate( shift )
    		cylinder( h=hl, r2=head("r0"), r1=head("r"), $fn=head("fn"));

	} else

	if (hstyle=="sphere")
	{
		translate( q ) 
		rotate( rot)
		rotate( [0,0,hrot] )
		color( head("color"), head("transp"), $fn=head("fn"))
		translate( shift )
    		sphere( r=head("r"));
	} 
	
} // Line2 

module Line2_test( ops ){doctest (Line, ops=ops); }


module Line2_demo_1_default()
{
	echom("Line2_demo_1_default");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true]);
}

module Line2_demo_1a_extension()
{
	ops=["transp",0.3,  "markpts",true, "transp",0.4] ;

	echom("Line2_demo_1a_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, update(ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line2(pq2, update(ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	Line2(pq3, update(ops, ["color","blue","extension0", 1,"extension1", 1]));

	pq4= randPts(2);
	//MarkPts(pq3);
	Line2(pq4, update(ops, ["color","purple","extension0", -0.5,"extension1", -0.5]));

}

module Line2_demo_2_sizeColor()
{
	echom("Line2_demo_2_sizeColor");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, [ "r", 0.5
			 , "color","green"
			 , "transp",0.3
			 , "fn", 6
			 , "markpts",true
			 ]);
}

module Line2_demo_3a_head_cone()
{
	echom("Line2_demo_3a_head_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true, "head"
			  ,["style","cone"]
			 ]);
}

module Line2_demo_3b_head_cone_settings()
{
	echom("Line2_demo_3b_head_cone_settings");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true,"head"
			  , [ "style","cone"
				,"color","blue"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5
			    ]
			 ]);
}

module Line2_demo_4a_tail_cone()
{
	echom("Line2_demo_4a_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				, "color","red"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5		    
				]
			 ]);
}

module Line2_demo_4b_tail_cone()
{
	echom("Line2_demo_4b_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				,"r", 0.2
				, "r0",0.6
				]
			 ]);
}

module Line2_demo_5a_headtail_cone()
{
	echom("Line2_demo_5a_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq,["markpts",true
			  ,"head"
			  , [ "style","cone"
			    ]
			  
			, "tail"
			  , [ "style","cone"
			 	]
			  
			 ]);
}

module Line2_demo_5b_headtail_cone()
{
	echom("Line2_demo_5b_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"head"
			  , [ "style","cone"
				, "color","blue"
				, "r", 0.4
				, "len",1
			 	, "transp", 0.6
				, "fn", 5
			    ]
			, "tail"
			  , [ "style","cone"
				, "r", 0.2
				, "r0",0.6
			 	]
			 ]);
}

module Line2_demo_6a_headtail_sphere()
{
	echom("Line2_demo_6a_headtail_sphere");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"head"
			  , [ "style","sphere"
				, "color", "yellow"
				, "r",.5
				, "transp", 0.4
			    ]
			  
			, "tail"
			  , [ "style","sphere"
				,"color", "red"
				, "r", .4
				, "transp", 0.4
				]
			  
			 ]);
}


module Line2_demo_7_headtail_fn()
{
	echom("Line2_demo_7_headtail_fn");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				]
			 ]);
}


module Line2_demo_8_rotate()
{
	echom("Line2_demo_8_rotate");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				, "rotate", 45  //<====
				]
			 ]);
}


module Line2_demo_9a_block()
{
	echom("Line2_demo_9a_block");
	pq= randPts(2);
	//pq = [ [2,1,3], [4,3,2]];
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"style","block"]);
}


module Line2_demo_9a2_block_extension()
{
	ops=["transp",0.3,  "markpts",true,"style","block"] ;

	echom("Line2_demo_9a2_block_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, update( ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line2(pq2, update( ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	//Line2(pq3, update( ops, [["color","blue"],["extension0", 1],["extension1", 1]]));

	pq4= randPts(2);
	//MarkPts(pq3);
	//Line2(pq4, update( ops, [["color","purple"],["extension0", -0.5],["extension1", -0.5]]));

}

module Line2_demo_9b_block()
{
	echom("Line2_demo_9b_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 //, "markpts",true
		];

	Line2([O, [0,0,z+1]], [ "style","block"
					   //, ["markpts",true]
			  		   , "width",0.5
					   , "depth",4
					  ]);

	Line2([[0,0,w], [x,0,w]], update(ops, ["color","red"]));
	Line2([[x-w,0,0], [x-w,0,z]], update(ops, ["color","green"]) );

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true]
		 ) );
	
}


module Line2_demo_9c_block()
{
	echom("Line2_demo_9c_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","corner2"] // <====
					  ) );


}

module Line2_demo_9d_block()
{
	echom("Line2_demo_9d_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //["markpts",true]
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid1"] // <====
					  ) );

}

module Line2_demo_9e_block()
{
	echom("Line2_demo_9e_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [//"markpts",true
			  			 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid2"] // <====
					  ) );

}


module Line2_demo_9f_block()
{
	echom("Line2_demo_9f_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [//"markpts",true
		 				 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","center"] // <====
					  ) );

}

module Line2_demo_9g_block()
{
	echom("Line2_demo_9g_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], ["style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift",[2*w, 0,w] ]// <====
					  ) );

}

module Line2_demo_10_shift()
{
	echom("Line2_demo_10_shift");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
		 	 , "r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "green"
				, "transp",0.5
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "transp",0.5
			    ]
			,"shift", "corner2"
			 ]);
}

module Line2_demo2()
{
	pts = [ [-1,0,-1], [2,3,4]  ];
	MarkPts( pts );
	pts = randPts(2);
	//Line2( pts,[["r",0.5]] );
	//MarkPts(pts, [["transp",0.3],["r",0.25]] );
	Line2( pts //, [["style","block"]
		, ["markpts",true
		 , "style","block", "transp",0.5
		, "rotate", $t*60
		, "shift", "corner"
		, "head", ["style","cone"]
		, "tail", ["style","cone"]
			]
			);
}

module Line2_demo_11_block_touch()
{
	echom("Line2_demo_11_block_touch");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "markpts",true
		 , "style","block"
		 , "width",w
		 , "depth",y
		 , "transp",0.5
		 ];

	p_touch = [x/3,y/3,z/3];
	MarkPts([ p_touch ] , ["r",0.3,"transp",0.3]);

	//Line2([O, [0,0,z+1]], [ ["style","block"]
	//				   , ["width",w]
	//				   , ["depth",y]
	//				  ]);
	//Line2([[0,0,w], [x,0,w]], ops);
	//Line2([[x-w,0,0], [x-w,0,z]], ops);

	pts = [[x, 0, z], [0, 0, z+1]];
	//pts=randPts(2);
	Line2( pts );
	//MarkPts( pts );
	
	Line2( pts
		, update(ops, ["color","blue"//, "shift","corner2"
					  ,"touch", p_touch]  // <====
					  ) );
}

module Line2_demo_12_verify_2D()
{
	//pq= [ [ 1.5, 1, 0 ], [ 1, 3, 0 ] ];
	pq_= randPts(2);
	echo(pq_);
	
	pq= [ [ pq_[0].x, pq_[0].y,0]
		, [ pq_[1].x, pq_[1].y,0] ];
	echo(pq);
	MarkPts( pq, ["grid",true] );
	Line2( pq, ["markpts", true] );

	/*
	pq2= [ [ 1.5, 0,1 ], [ 1, 0,3 ] ];
	MarkPts( pq2, ["grid",true] );
	Line2( pq2, ["markpts", true] );

	pq3= [ [ 0,1.5, 1 ], [ 0, 1, 3 ] ];
	MarkPts( pq3, ["grid",true] );
	Line2( pq3, ["markpts", true] );
	*/
}

module Line2_demo()
{
	//Line2_demo_1_default();
	//Line2_demo_1a_extension(); // 2014.5.18
	//Line2_demo_2_sizeColor();
	//Line2_demo_3a_head_cone();
	//Line2_demo_3b_head_cone_settings();
	//Line2_demo_4a_tail_cone();
	//Line2_demo_4b_tail_cone();
	//Line2_demo_5a_headtail_cone();
	//Line2_demo_5b_headtail_cone();
	//Line2_demo_6a_headtail_sphere();
	//Line2_demo_7_headtail_fn();
	//Line2_demo_8_rotate();
	Line2_demo_9a_block();
	//Line2_demo_9a2_block_extension();
	//Line2_demo_9b_block();
	//Line2_demo_9c_block();
	//Line2_demo_9d_block();
	//Line2_demo_9e_block();
	//Line2_demo_9f_block();
	//Line2_demo_9g_block();
	//Line2_demo_10_shift();
	//Line2_demo_11_block_touch();
	//Line2_demo_12_verify_2D();

	

}
//Line2_test( ["mode",22] );
//Line2_demo();

//========================================================
LineByHull=["LineByHull", "pq,r=0.05,fn=15", "n/a", "3D,Shape",
"
 Draw a line as thick as 2*r between the two points in pq using
;; hull(). This approach doesn't need to calc rotation/translation,
;; so the faithfulness of outcome is guaranteed. But is much slower
;; so not good for routine use.
"];

module LineByHull( pq,r=0.05, fn=15 )
{
	hull(){
		translate(pq[0])
		sphere(r=r, $fn=fn);
		translate(pq[1])
		sphere(r=r, $fn=fn);
	}
}

module LineByHull_test( ops ){ doctest(LineByHull, ops=ops); }

module LineByHull_demo()
{
	LineByHull( randPts(2) );
}
//LineByHull_demo();
//doc(LineByHull);




//========================================================
Mark90=["Mark90", "pqr,ops", "n/a", "Grid, Math, Geometry, 3D, Shape",
" Given a 3-pointer pqr and an ops, make a L-shape at the middle
;; angle if it is 90-degree. The default ops, other than usual
;; r, color and transp, has ''len''= 0.4
;;
;; ops: [ ''len'', 0.4
;;      , ''color'', false
;;      , ''transp'', 0.5
;;      , ''r'', 0.01
;;      ]
"];
	
module Mark90(pqr, ops)
{
	ops=update(   
		[ "len", 0.4
		, "color",false
		, "transp", 0.5
		, "r", 0.01
		], ops );
	function ops(k) = hash(ops,k);

	p = pqr[0];
	q = pqr[1];
	r = pqr[2];
	m = min( 0.7*norm(p-q), 0.7*norm(r-q), ops("len"));
	
	pq1 = onlinePt( [q,p], len=m); //ratio= ops("len")/ norm(p-q) );
	pq3 = onlinePt( [q,r], len=m); //ratio= ops("len")/ norm(r-q) );
 	sqpts = squarePts( [pq1, q, pq3] );
		
	if (is90( [p,q], [q,r] ) ){
		Line0( [sqpts[2],sqpts[3]],ops );
		Line0( [sqpts[0],sqpts[3]],ops );
	}
}

module Mark90_demo()
{
	pqr = randPts(3);
	MarkPts( pqr );
	sqs = squarePts( pqr );
	Plane( sqs , ["mode",4]);
	Mark90(sqs, ["len", 0.4, "color", "black"] );
}

module Mark90_test(ops){ doctest( Mark90, ops=ops); }
//Mark90_demo();
//Mark90_test();
//doc(Mark90);


//========================================================
MarkPts=["MarkPts", "pts,ops", "n/a", "3D, Shape, Point",
"
 Given a series of points (pts) and ops, make a sphere at each pt.
;; Default ops:
;;
;;   [ ''r'',0.1
;;   , ''colors'',[''red'',''green'',''blue'',''purple'',''khaki'']
;;   , ''transp'',1
;;   , ''grid'',false
;;   , ''echo'',false
;;   ]
;;
;; The colors follow what's in colors. If len(pts)>len(colors),
;; colors recycles from the the 1st one and the transparancy and
;; radius reduced by ratio accordingly by a ratio each cycle. If
;; grid is true, draw PtGrid for each point; if echo is true,
;; echo each point.
"];

module MarkPts(pts, ops) 
{
	ops= update( ["r",0.1,"transp",1
				 ,"grid",false
				 ,"echopts",false
				 ,"colors", ["red","green","blue","purple"]
				 ,"labels",false
				 ,"samesize", false
				 ,"sametransp", false
				 ], ops);
	function ops(k)= hash(ops,k);
	colors = ops("colors");
	transp = ops("transp");
	samesize = ops("samesize");
	sametransp=ops("sametransp");
	labels = ops("labels");
     cL = len(colors);
	//echo("<br/>ops = ", ops);

	// color cycle. The more ccycle, the smaller the sphere
	// and the more transparent they are.  
	function ccycle(i) = floor(i/cL); 

	for (i=[0:len(pts)-1]){
		translate(pts[i])
		color( ops("colors")[i-cL*ccycle(i)]   // cycling thru colors
			, transp* ( sametransp?1:(1-0.3*ccycle(i))) // transp level
			) 
		sphere(r=ops("r")*(samesize?1:(1-0.15*ccycle(i)))
			  ); // radius

		if(ops("grid")){ PtGrid(pts[i], ops=["mode", ops("grid")]); }
		if(ops("echopts")){ echo_("MarkPts #{_}={_}",[i, pts[i]]);}
	}
	if( labels ){

		LabelPts( pts, labels=labels==true?"":labels
				, ops=["shift", ops("r")*1.3 ]
					 
					);
	}
}

module MarkPts_test( ops ){ doctest(MarkPts,ops=ops);}
module MarkPts_demo_1()
{
	ops= ["r",0.1,  "echopts",true, "grid",true] ;
	MarkPts( randPts(3), ops=ops);
	MarkPts( randPts(12));
}

module MarkPts_demo_2_more_points()
{
	MarkPts( randPts(20));
}

module MarkPts_demo_2_more_points_2()
{
	MarkPts( randPts(20), ["samesize",true]);
}

module MarkPts_demo_2_more_points_3()
{
	MarkPts( randPts(30), ["samesize",true, "colors",["red"]]);
}

module MarkPts_demo_2_more_points_4()
{
	MarkPts( randPts(30), ["samesize",true, "sametransp",true, "colors",["red"]]);
}

module MarkPts_demo_3_label()
{
	MarkPts( randPts(3), ["r",0.15,  "labels",true, "colors",["red"],"samesize",true, "sametransp",true]); // labels = P1, P2 ... 

	MarkPts( randPts(3), ["r",0.15,  "labels","ABC", "colors",["green"],"samesize",true, "sametransp",true]); // labels = P1, P2 ... 

	pts = randPts(10);
	Chain( pts, ["r",0.02,["closed",false]]);
	MarkPts( pts, ["labels",increase(range(pts)), "colors",["blue"],"samesize",true, "sametransp",true]); // labels = P1, P2 ... 

}

module MarkPts_demo_4_grid()
{
	function getops(grid, color, label=false)=
	  ["grid", grid, "colors",[color],"samesize",true, "sametransp",true
	   ,"labels", [label] ];

	MarkPts(randPts(1), getops(true, "red", "grid:true") );
	MarkPts(randPts(1), getops(1, "green", "grid:1") );
	MarkPts(randPts(1), getops(2, "blue", "grid:2") );
	MarkPts(randPts(1), getops(3, "purple", "grid:3") );

}
module MarkPts_demo()
{
	//MarkPts_demo_1();
	//MarkPts_demo_2_more_points();
	//MarkPts_demo_2_more_points_2();
	//MarkPts_demo_2_more_points_3();
	//MarkPts_demo_2_more_points_4();
	MarkPts_demo_3_label();
	//MarkPts_demo_4_grid();

}
//MarkPts_test( ["mode",22]);
//MarkPts_demo();
//doc(MarkPts);



//========================================================
Normal=["Normal", "pqr,ops","n/a","3D, Line, Geometry",
 " Given a 3-pointer (pqr) and ops, draw the normal of plane made up by
;; pqr. The normal will be made passed through Q (pqr[1]). Its length
;; is determined by L/3 where L the shortest length of 3 sides. The
;; default ops:
;;
;; [ ''r''      , 0.03
;; , ''transp'' , 0.5
;; , ''mark90p'', true
;; , ''mark90r'', true
;; , ''len''    , L/3
;; ]
"];
 
module Normal_test( ops=["mode",13] ){ doctest( Normal, ops=ops); }

module Normal( pqr, ops )
{
	L = min( d01(pqr), d02(pqr), d12(pqr));
	ops = update( [
			  "r"     , 0.03
			  ,"len"  , L/3
			, "transp", 1
			, "mark90p", false
			, "mark90r", true
			, "reverse", false
			, "label", false
			], ops);
	function ops(k)= hash(ops,k);
	echo(ops);
	mark90p = ops("mark90p");
	mark90r = ops("mark90r");

	Nrm = (ops("reverse")?-1:1)*normal( pqr )/3;
	_N = Nrm + pqr[1];
	N = onlinePt( [ORIGIN+ pqr[1],_N]
				, len=ops("len") );

	MarkPts([N]);

	if( mark90p ) { Mark90( [ N,pqr[1],pqr[0] ], mark90p ); } 
	if( mark90r ) { Mark90( [ N,pqr[1],pqr[2] ], mark90r ); } 

	Line([ ORIGIN+ pqr[1], N ], concat( ["head",["style","cone"]], ops) );
	
}

module Normal_demo_1()
{
	pqr = randPts(3);
	MarkPts( pqr, ["grid",["mode",2]] );
	Chain( pqr );
	Normal( pqr );

	color("red") Normal(pqr, ["reverse",true]);
}

module Normal_demo()
{
	Normal_demo_1();
}
//Normal_demo();



//========================================================
Pie=[[]];

module Pie( angle, r, ops=[] )
{
	
	ops = concat( ops
		// ,["h",1
		//  ]
		 ,OPS);
	
	function ops(k) = hash( ops, k);
	
	arcpts = concat( [[0,0]], arcPts( a=angle, r = r, count = angle) );
	//echo( "arcpts: ", arcpts);

	paths = [range(len(arcpts))];

	color(ops("color"), ops("transp"))
	polygon ( points=arcpts, paths=paths); 



}

module Pie_demo()
{

	Pie(angle=60, r=3);

	ColorAxes();
	
	translate( [0,0,-1])
	Pie( angle=80, r=4 );

	scale([1,1,0.5])
	translate( [0,0,-2])
	Pie( angle=280, r=4 );
	
	translate( [0,0,-3])
	Pie( angle=360, r=5 ); //<==== note this

}
//Pie_demo();



//========================================================
Plane=[ "Plane", "pqr,ops", "n/a", "2D, Geometry, Shape", 
" 
 Given a 3-pointer pqr and ops, draw a plane in space based on
;; settings in ops:
;;
;;  [ ''mode'', 1 // mode = 1~4
;;  , ''color'', ''brown''
;;  , ''transp'', .4
;;  , ''frame'', false
;;  , ''r'', 0.005
;;  , ''th'', 0 // thickness
;;  ]
;;
;; # mode=1: rect parallel to axes planes
;;
;;  The 1st pt is the anchor(starting) pt
;;  The 2nd pt is treated like sizes:
;;     [ x,y,* ] = parallel to xy-plane (* = any)
;;     [ x,0,z ] = parallel to xz-plane
;;     [ 0,y,z ] = parallel to yz-plane
;;  The 3rd pt, r, is ignored.
;;  Plane( [ [1,2,3],[4,-5,6]], [[''mode'',1]] )
;;     draw a plane of size 4 on x, 5 on -y
;;     starting from [1,2,3]
;;
;; # mode=2 rectangle
;;
;;   line p1-p2 is a side of rectangle.
;;   p3 is on the side line parallel to line p1~p2
;;
;; # mode=3 triangle
;;
;; # mode=4 parallelogram
;;
;; # frame: default= false
;;
;;   Draw a frame around using Chain if frame is true or is
;;    a hash as the ops option for Chain. For example
;;
;;    Plane( pqr, [[''frame'', [''color'', ''blue''] ]] )
;;
;;    draw a blue frame.
;;
;; NEW 2014.6.19: len(pqr) can be >3. But all faces connect to pqr[0].
"];

//doc(Plane);


module Plane(pqr, ops=[])
{
	//echom("Plane");
	//echo("pqr=", pqr);
	ops = concat(ops, [
				"mode", 1 // 1: rect parallel 2 xy-, xz- or yz- plan
							// 2: rect
							// 3: triangle
							// 4: parallelogram
				,"color","brown"
				,"transp", .4
				,"frame", false
				,"r",0.005
				,"th",0 // thickness
				 ]);
	function ops(k)=hash(ops,k);
	c = ops("color");
	t = ops("transp");
	m = ops("mode");
	th= ops("th");
	p1= pqr[0];
	p2= pqr[1];
	p3= pqr[2];
	p4 = m<4?undef
		:cornerPt(pqr);
	frame = ops("frame");

	//echo("ops = ",ops);

	// mode=1: axis-plane parallel rectangle 
	// 	
	// The 1st pt is the anchor(starting) pt
	// The 2nd pt is treated like sizes:
	//   [ x,y,* ] = parallel to xy-plane (* = any)
	//   [ x,0,z ] = parallel to xz-plane
	//   [ 0,y,z ] = parallel to yz-plane 
	//	The 3rd pt, r, is ignored.
	// Plane( [ [1,2,3],[4,-5,6]], [["mode",1]] )
	//   draw a plane of size 4 on x, 5 on -y
	//   starting from [1,2,3]
	m1p2 = p2.x==0?(p1+p0y0(p2)):p2.y==0?(p1+px00(p2)):(p1+px00(p2));
	m1p3 = p2.x==0?(p1+p0yz(p2)):p2.y==0?(p1+px0z(p2)):(p1+pxy0(p2));
	m1p4 = p2.x==0?(p1+p00z(p2)):p2.y==0?(p1+p00z(p2)):(p1+p0y0(p2));

	// mode=2 rectangle
	//
	// p1, p2 are 2 corners and form a line
	// p3 is on the line parallel to line p1~p2

	// all points
	ps= m==1?[p1,m1p2, m1p3, m1p4]
		:m==2?squarePts(pqr)
		:m==3?pqr
		:concat(pqr,[p4]);
		  
	/* 
		Trying to add thickness (th). The th is the dist between 
		the plane PL1 and its clone PL2 that can be obtained by 
		translation from PL1 by a scaled copy of a vector V that 
		is the normal of any 3 pts. This translation will move 
		PL1 through a line L that passes through P1 (any point on
		PL1) to Q1 (any pt on PL2).
		
		We can get this V easily (= normal(pqr)), but first need
		to figure out the scale factor k that makes the dist of 
		translation equal to th. For this we need to figure out
		what Q is. 

			vect = normal(pqr);
		 
		The th is:

			sqrt( k^2*x^2+ k^2*y^2 + k^2*z^2 )= th

			k^2*x^2+ k^2*y^2 + k^2*z^2 )= th^2

			k^2 = th^2 / ( x^2+ y^2 + z^2)

			k = th/ sqrt(x^2+ y^2 + z^2)) = th / norm([x,y,z])

	*/

	function getfaces(pts, _i_=0)=
	(	                            // new 2014.6.19
		_i_<len(pts)-1?
		concat( [ [0, _i_+1, _i_+2]]
			 , getfaces( pts,_i_+1)
			 )
		:[]
	);

	//echo("In Plane, pqr = ", pqr, ", faces = ", getfaces(pqr) );

	if(len(pqr)>3){ // new 2014.6.19
		//echo( "pqr > 3" );
		color("blue",t)
		polyhedron( points=pqr, faces=getfaces(pqr) );

		if(frame){
			if(isarr(frame)){
				//Chain( pqr, update(ops, update(["closed",true], frame)) );
				Chain( pqr, concat(frame, ["closed",true], ops) );
			}else{			
				Chain( pqr, concat(["closed",true],ops) );
			}
		}
	
	}else{

		if(m==3){
			color( c, t )
			polyhedron( points=pqr, faces=[[0,1,2]] );
			if( th>0){
//				echo_("normal(pqr)={_} ",[normal(pqr)]);
//				echo_("norm(pqr)={_} ",[norm(pqr[0])]);
//				echo_("normal(pqr)*th/norm(pqr)={_} "
//					,[normal(pqr)*th/norm(pqr[0])]);
				translate( normal(pqr)*th/norm(pqr[0]) )	
				polyhedron( points=pqr, faces=[[0,1,2]] );
			}
		}else{
			color( c, t )
			polyhedron( points=ps, faces=[[0,1,2],[2,3,0]] );
		}
		if(frame){
			if(isarr(frame)){
				//Chain( ps, update(ops, update(["closed",true], frame)) );
				Chain( ps, concat(frame, ["closed",true], ops) );
			}else{			
				Chain( ps, update(ops, ["closed",true]) );
			}
		}
	}
}

module Plane_demo_1()
{

	// mode=1
	Plane( randPts(3,r=false, x=[-1,-2],y=[-1,-2]), ["color","red","transp",1] );
	//Plane( [ randPts(1,r=1)[0], px0z(randPts(1,r=2)[0])]
	//					, ["color","lightgreen","transp",1] );
	//Plane( [ randPts(1,r=1)[0], p0yz(randPts(1,r=2)[0])]
	//					, ["color","blue","transp",1] );

	Plane( [ randPt(r=false, x=[-1,-2],y=[-1,-2]), px0z(randPts(1,r=2)[0])]
						, ["color","lightgreen","transp",1] );
	Plane( [ randPt(r=false, x=[-1,-2],y=[-1,-2]), p0yz(randPts(1,r=2)[0])]
						, ["color","blue","transp",1] );

	// mode=2
	pqr2 = randPts(3,r=false, x=[2,3],y=[2,3]);
	MarkPts( pqr2 );
	Plane( pqr2, ["mode",2,"color","red"]  );


	// mode=3
	pqr3 = randPts(3, ,r=false, x=[0,3],y=[-1,-3]);
	Plane( pqr3, ["mode",3,"color","green","th",0.5]  );
	
	// mode=4
	pqr4 = randPts(3, r=3);
	//MarkPts( pqr4 );
	Plane( pqr4, ["mode",4,"color","blue"]  );

	pqr5 = randPts(3, r=false, x=[0,2],y=[0,2],z=[-1,-3]);;
	Plane( pqr5, [ "mode",2, "color","khaki"
				, "frame",["r",0.08
						   ,"color","blue"
						   ,"transp",.8
						   ]
                   
				]);

}

module Plane_demo_2() // 2014.6.19
{
	pts = randPts(5);
	//echo( "pts: ", pts);
	MarkPts(pts);
	Plane( pts, ["color","red", "frame",true] );
	//echo( "Leaving demo 2");
}

module Plane_demo()
{
	//Plane_demo_1();
	Plane_demo_2();
}

//Plane_demo();

module Plane_test(ops){ doctest(Plane,ops=ops); }

//Plane_test( ["mode",22] );
//Plane_demo();


	 
//========================================================

PtGrid=["PtGrid", "pt,ops", "n/a","3D, Point, Line, Geometry" ,
"
 Given a pt and ops, draw thin cylinders from pt to axes or axis planes
;; for the purpose of displaying the pt location in 3D. Default ops:
;;
;;  [ ''highlightaxes'', true
;;  , ''r'',0.007    // line radius
;;  , ''projection'', false, // draw projection on axis planes.
;;  ,                      // ''xy''|''yz''|''xy,yz''|...
;;  , ''mark90'', ''auto'' // draw a L-shape to mark the 90-degree angle
;;  , ''mode'',2 // 0: none
;;             // 1: 2lines: pt~p_xy, p@xy~Origin
;;             // 2: plane and line: pt~p@xy, p@xy~x, p@xy~y
;;             // 3: block
;;]
"];
/*
  , ''scale'', 1    // scale for internal grids \\
  , ''othoToAxis'', false // if true, draw from pt to axes \\
  , ''faces'',''all0'' // a str setting what face(s) \\
  ,    has the face grids. A box formed by the pt and the \\
  ,    origin has 6 faces, 3 touching the origin, 3 touching \\
  ,    the pt, represented by 0 and 1, respectively. A *faces* \\
  ,    ''??0'' means the face(s) that cover the origin will \\
  ,    have grids. For example, ''xy0, yz1''= both the face on \\
  ,    xy plane touching [0,0,0], and the face on yz1 plane \\
  ,    not touching [0,0,0] have the grids. To show grids on \\
  ,    all faces, use ''all0, all1''. \\ 
"];
*/

module PtGrid(pt, ops)
{
	L= norm(pt);
	//echo_("in PtGrid, pt={_}, L={_}, L/5={_}",[pt, L,(L/5)]);
	ops=updates(OPS,
		[[ "r",0.006
		, "transp", 0.8
		//, "color", "red"
		//, "style", "line" // line|dashline
		//, "dash", (L/5) //0.05]
		//, "space", 0.05
		//, "scale", 0
		//, "scalefaces", "all1" 
		//, "othoToAxis", false
		, "projection", false //"xy"|"yz"|"xy,yz"|"xy,yz,xz"| ...
		//, "quickgrid", true
		, "mark90", "auto"
		, "highlightaxes", true
		, "mode", 2   // 0: none
					// 1: line // pt:p_xy, p_xy:Origin
					// 2: plane and line //pt:p_xy, p_xy:x, p_xy:y
					// 3: block
			 
		], ops] );
	function ops(k)= hash(ops,k);
	//echo(ops);
	r= ops("r");
	mode = ops("mode")==true?2: ops("mode");
	style= ops("style");
	mark90= ops("mark90");
	
	xcolor = "red";
	ycolor = "lightgreen";
	zcolor = "blue";
		
	p_xy = [pt.x, pt.y, 0   ]; //pxy0(pt);
	p_yz = [   0, pt.y, pt.z]; //p0yz(pt);
	p_xz = [pt.x,    0, pt.z]; //px0z(pt);
	p_x  = [pt.x,    0, 0   ]; //px00(pt);
	p_y  = [   0, pt.y, 0   ]; //p0y0(pt);
	p_z  = [   0,    0, pt.z]; //p00z(pt);

	//module line ( pq, lineops=false ) { 
	//	Line0( pq, update( ops, lineops) );
	//}

	module chain ( pts, chainops=false) { 
		Chain( pts, update(ops,chainops) );
	}
	
	module projline (face="xy",pt2plane=true, projops=false){ 
		// Draw a L-shape line
		//echo("in projline, face= ", face);
		pts=[pt, face=="xy"?p_xy:face=="xz"?p_xz:p_yz, ORIGIN];
		if(pt2plane)
		{	chain( pts, update(ops, ["closed",false], projops) ); }
		else 
		{	line( pts, update(ops, projops) ); }
		
		if( mark90 || mark90=="auto")
		Mark90( pts, updates(ops, projops));
		
	}

	//
	// mark90
	//
	if( mark90=="auto" ){
		if( mode==1 ) Mark90( [ORIGIN, p_xy, pt ], ops );
	}
	else if( mark90 ){
		//Mark90( [ORIGIN, p_xy, pt ], mark90); //update(ops, mark90) );
		Mark90( [ORIGIN, p_xy, pt ], update(ops, mark90) );
	}
	
	// 
	// grid mode
	//
	// echo("in PtGrid, mode = ", mode );

	if(mode==1) { projline("xy"); //chain( [pt, p_xy, ORIGIN] );	

	} else 

	if(mode==2) { chain( [pt, p_xy, p_x] ,["closed",false]);
				line( [p_xy, p_y] );			
	} else
	
	if(mode==3) {
				chain([ p_yz, p_z, p_xz, p_x, p_xy
					  , p_y, p_yz, pt, p_xy ],["closed",false]);
				line( [pt, p_xz]);
	}

	//
	// highlight axes
	//
	hlops = ops("highlightaxes"); 
	hlops = hash( ops, "highlightaxes"
			   ,	if_v  =true
			   , then_v=["r", r]  // <=== default axes setting
			   );	
	if( hlops )
	//  assign( axesops= update( ops, hlops ))
	{
        axesops= update( ops, hlops );
		//echo("hlops", hlops);
		//echo("axesops", axesops);
		color( hash(hlops, "color", xcolor), hash(hlops, "transp"))
		Line( [ORIGIN, p_x], axesops );
		color( hash(hlops, "color", ycolor), hash(hlops, "transp"))
		Line( [ORIGIN, p_y], axesops );
		color( hash(hlops, "color", zcolor), hash(hlops, "transp"))
		Line( [ORIGIN, p_z], axesops );
	}

	//
	// projection = "xy"|"yz"|"xy,yz"|"xy,yz,xz"| ... 
	//
	proj  = ops("projection");
	//echo(proj);
	if(proj)
//	assign( faces = proj==true?["xy"]    // if ==true, set to "xy"
//				:proj?split(proj,",") // else, split them
//					:false
//	)
    {  
    faces = proj==true?["xy"] // if ==true, set to "xy"
			:proj?split(proj,",") // else, split them
					:false;
        
	//echo(faces);
	for( face=faces ){ 
		//echo( "-- face = ", face);
		if(!(mode==1 && face=="xy")) // projline drawn automatically
			projline(face); }       // when (mode==1 && face=="xy")
	}
	//if (proj=="xy" && mode!=1) { projline("xy");} 
	//else if (proj=="yz") {Line0( [ORIGIN, p_yz], ops );}
	//else if (proj=="xz") {Line0( [ORIGIN, p_xz], ops );}
	


	/*
	if ( ops("quickgrid")) {
	
		color(ops("color")){
			DashLine( [ pt, p_xy ], ops );
			//DashLine( [ p_x, p_xy], ops );
			//DashLine( [ p_y, p_xy], ops );
			//DashLine( [ pt, p_z  ], ops );
			DashLine( [ p_xy, ORIGIN  ], ops );
		}	
	}
	else assign(

		scale     = ops("scale")
		, scalefaces= ops("scalefaces")
		, xscount = abs(pt.x<0?ceil( pt.x/ops("scale") ):floor( pt.x/ops("scale") ))
		, yscount = abs(pt.y<0?ceil( pt.y/ops("scale") ):floor( pt.y/ops("scale") ))
		, zscount = abs(pt.z<0?ceil( pt.z/ops("scale") ):floor( pt.z/ops("scale") ))

	){ 
	echo(" axes_ops = ", axes_ops );
	// Lines parallel to X
	color( xcolor ){
		DashLine( [[pt[0],pt[1],0 ],[0,pt[1],0 ]], ops );	// xy0, 0y0
		DashLine( [[pt[0],0,pt[2] ], [0,0,pt[2]]], ops ); // x0z, 00z
		DashLine( [pt, [0, pt[1],pt[2]]], ops); 			// xyz, 0yz
		}

	// Lines parallel to Y
	color( ycolor ){
		DashLine( [[pt[0],pt[1],0 ],[pt[0],0,0 ]], ops ); // xy0, x00
		DashLine( [pt, [pt[0],0,pt[2] ]], ops); 			// xyz, x0z
		DashLine( [[0, pt[1],pt[2]], [0,0,pt[2]]],ops );	// 0yz, 00z
		}

	// Lines parallel to Z
	color( zcolor ){	
		DashLine( [pt, [pt[0],pt[1],0 ]], ops);            // xy0, xyz
		DashLine( [[pt[0],0,pt[2] ], [pt[0],0,0] ], ops ); // x0z, x00
		DashLine( [[0, pt[1],pt[2]], [0,pt[1],0]], ops );  // 0yz, 0y0
		}
	}	
	*/
	
	//*
	//Lines along axes
	// axes_ops  

	
	 /*
	[ "r",    4*r
				, "dash",  3*ops("dash")
				, "space", 2*ops("space")
				];
	*/

	/*

	color( xcolor )
	if(pt[0]>0){
		Line0( [ ORIGIN,[pt[0],0, 0] ], update(axes_ops,["r",4*r]) );
	}else{
		DashLine( [ ORIGIN,[pt[0],0, 0] ], axes_ops);
	}

	color( ycolor )
	if(pt[1]>0){
		//MarkPts([[0,pt[1],0]]);
		Line0( [ ORIGIN,[0,pt[1],0] ], update(axes_ops,["r",4*r]) );
	}else{
		DashLine( [ ORIGIN,[0,pt[1],0] ], axes_ops);
	}

	color( zcolor )
	if(pt[2]>0){
		Line0( [ ORIGIN,[0,0,pt[2]] ], update(axes_ops,["r",4*r]) );
	}else{
		DashLine( [ ORIGIN,[0,0,pt[2]] ], axes_ops);
	}

	//Lines JL to axes 
	if(ops("othoToAxis")){
		DashLine( [pt, [pt[0],0,0 ]], ops );
		DashLine( [pt, [0, pt[1],0]], ops );
		DashLine( [pt, [0,0,pt[2] ]], ops );
	}

	//Projection lines onto surfaces
	if(ops("projection")){
	  color("gray"){
	  	DashLine( [ ORIGIN, [pt[0],pt[1],0 ]], ops );
	  	DashLine( [ ORIGIN, [pt[0],0,pt[2] ]], ops );
	  	DashLine( [ ORIGIN, [0,pt[1],pt[2] ]], ops );
		}	
	} 

	// Internal Grid lines

	scale = ops("scale");
	scalefaces = ops("scalefaces");
	
	xscount = abs(pt.x<0?ceil( pt.x/scale ):floor( pt.x/scale ));
	yscount = abs(pt.y<0?ceil( pt.y/scale ):floor( pt.y/scale ));
	zscount = abs(pt.z<0?ceil( pt.z/scale ):floor( pt.z/scale ));

	if( scale && hasAny(scalefaces, ["xy0","all0"])){
	  if(xscount>0){
		for( i = [1: xscount]){
			color( ycolor)
			DashLine([[ i*scale*sign(pt.x), pt.y, 0 ]
					   ,[ i*scale*sign(pt.x),    0, 0 ]],ops );
		}
	  }
	  if(yscount>0){
		for( i = [1: yscount]){
			color( xcolor )
			DashLine( [[pt.x, i*scale*sign(pt.y), 0 ]
					  ,  [   0, i*scale*sign(pt.y), 0 ]],ops );			
		}	
	  }
	}

	if( scale && hasAny(scalefaces, ["xy1", "all1"]) ){
	  if(xscount>0){
		for( i = [1: xscount]){
			color( ycolor)
			DashLine([[ i*scale*sign(pt.x), pt.y, pt.z ]
					   ,[ i*scale*sign(pt.x),    0, pt.z ]],ops );
		}
	  }
	  if(yscount>0){
		for( i = [1: yscount]){
			color( xcolor )
			DashLine( [[pt.x, i*scale*sign(pt.y), pt.z ]
					  ,  [   0, i*scale*sign(pt.y), pt.z ]],ops );			
		}	
	  }
	}	

	if( scale && hasAny(scalefaces, ["xz0", "all0"])){
	  if(xscount>0){
		for( i = [1: xscount]){
			color( zcolor)
			DashLine([[ i*scale*sign(pt.x), 0, pt.z ]
					   ,[ i*scale*sign(pt.x),    0, 0 ]],ops );
		}
	  }
	  if(zscount>0){
		for( i = [1: zscount]){
			color( xcolor )
			DashLine( [[pt.x, 0,i*scale*sign(pt.z) ]
					  ,[   0, 0, i*scale*sign(pt.z) ]],ops );			
		}	
	  }
	}


	if( scale && hasAny( scalefaces, ["xz1", "all1"] )){
	  if(xscount>0){
		for( i = [1: xscount]){
			color( zcolor)
			DashLine([[ i*scale*sign(pt.x), pt.y, pt.z ]
					   ,[ i*scale*sign(pt.x), pt.y, 0 ]],ops );
		}
	  }
	  if(zscount>0){
		for( i = [1: zscount]){
			color( xcolor )
			DashLine( [[pt.x, pt.y, i*scale*sign(pt.z) ]
					  ,[   0, pt.y, i*scale*sign(pt.z) ]],ops );			
		}	
	  }
	}	


	if( scale && hasAny(scalefaces, ["yz0", "all0"])){
	  if(yscount>0){
		for( i = [1: yscount]){
			color( zcolor)
			DashLine([[ 0, i*scale*sign(pt.y),  pt.z ]
					 ,[ 0, i*scale*sign(pt.y),     0 ]],ops );
		}
	  }
	  if(zscount>0){
		for( i = [1: zscount]){
			color( ycolor )
			DashLine( [[0, pt.y, i*scale*sign(pt.z) ]
					  ,[   0, 0, i*scale*sign(pt.z) ]],ops );			
		}	
	  }
	}


	if( scale && hasAny(scalefaces, ["yz1", "all1"] )){
	  if(yscount>0){
		for( i = [1: yscount]){
			color( zcolor)
			DashLine([[ pt.x, i*scale*sign(pt.y), 0 ]
					 ,[ pt.x, i*scale*sign(pt.y), pt.z ]],ops );
		}
	  }
	  if(zscount>0){
		for( i = [1: zscount]){
			color( ycolor )
			DashLine( [[pt.x, pt.y, i*scale*sign(pt.z) ]
					  ,[pt.x, 0, i*scale*sign(pt.z) ]],ops );			
		}	
	  }
	}	
	*/
}


module PtGrid_demo_1_mode()
{
	pts = randPts(3);
	MarkPts( pts );
	PtGrid( pts[0], ["mode",1] );
	PtGrid( pts[1], ["mode",2] );
	PtGrid( pts[2], ["mode",3] ); // Note that mark90 is disabled automatically
}

module PtGrid_demo_2_mark90()
{
	pts = randPts(3);
	MarkPts( pts );
	PtGrid( pts[0] );
	PtGrid( pts[1], ["mark90",true] );
	PtGrid( pts[2], ["mark90",["color","red", "r",0.05] ] );
}


module PtGrid_demo_3_projection()
{
	pts = randPts(4);
	PtGrid( pts[0], ["mode",1] );
	PtGrid( pts[1], ["mode",1, "projection","xz"] );
	PtGrid( pts[2], ["mode",1, "projection","xz,yz"] );
	PtGrid( pts[3], ["mode",1, "projection","xz,yz", "mark90", false] );
	MarkPts( pts, ["transp",0.5] );
}

module PtGrid_demo_4_highlightaxes()
{
	pts = randPts(3);
	PtGrid( pts[0]);
	PtGrid( pts[1], ["highlightaxes",false] );
	PtGrid( pts[2], ["highlightaxes", ["r",0.08, "transp", 0.1, "color", "purple" ]] );
	MarkPts( pts, ["transp",0.5] );
}


module PtGrid_demo()
{
	//PtGrid_demo_1_mode();
	//PtGrid_demo_2_mark90();
	//PtGrid_demo_3_projection();
	PtGrid_demo_4_highlightaxes();
}
module PtGrid_test( ops ){ doctest(PtGrid,ops=ops);}

//PtGrid_demo();
//PtGrid_test();
//doc(PtGrid);



//========================================================
PtGrids=["PtGrids", "pts,ops", "n/a", "3D, Point, Line, Geometry",
"
 Given a series of points, draw grid based on ops.
;; Refer to PtGrid (for a single pt) for ops definition.
"];

module PtGrids(pts, ops)
{
	for(i=[0:len(pts)-1]){
		//echo_("In PtGrids, i={_}, pts[i]={_}",[i,pts[i]] );
		PtGrid( pts[i], ops );
	}
}

module PtGrids_demo()
{
	pts = randPts(3);
	PtGrids(pts, ["mode",3]);
	MarkPts(pts, ["transp",0.3]);
}

module PtGrids_test( ops ) { doctest( PtGrids, ops=ops ); }
//PtGrids_demo();
//PtGrids_test();
//doc(PtGrids);



//========================================================
Ring=["Ring", "ops", "n/a", "3D, Shape",
"
;; Given the ops, draw a ring on a plane that has its normal defined;;
;; in ops. Default ops:
;;
;;  [ ''ro'',3
;;  , ''ri'',2
;;  , ''r2o'',0
;;  , ''r2i'',0
;;  , ''target'', ORIGIN
;;  , ''normal'', [0,0,1]
;;  , ''rotate'', 0
;;  , ''color'',  false
;;  , ''transp'', 1
;;  , ''fn'', $fn
;;  , ''h'', 1
;;  ]
"];

module Ring( ops )
{
	ops=update( [
		 "ro",3
		,"ri",2
		,"r2o",0
		,"r2i",0
		,"target", ORIGIN 
		,"normal", [0,0,1]
		,"rotate", 0      
		,"color",  false  
		,"transp", 1      
		,"fn", $fn        
		,"h",      1      
		//,"inrod", false
		//,"outrod", false
		], ops ); 
	function ops(k)= hash(ops,k);

	//echo("ops(''outrod'' = ", ops("outrod"));

	h  = ops("h"  );
	rc = ops("rc" );
	fn = ops("fn" );
	ro = ops("ro" );
	ri = ops("ri" );
	r2o= ops("r2o");
	r2i= ops("r2i");
	color = ops("color");
	transp= ops("transp");
	rotang= ops("rotate");

	inrod_ = ops("inrod");
	outrod_ = ops("outrod");
	//inbar_ = ops("inbar");
	//outbar_ = ops("outbar");

	outrod = !outrod_?false
			  : update([ "style", "rod" // rod|block
					   , "len", 1 
					   , "count",  12  
					   ,"color",  color  
					   ,"transp", transp      
					   ,"fn", fn        
					   , "r",  0.4      // for style = rod
					   ,"width",0.4   // for style = bar
					   ,"h", h    // for style = bar
					   ,"markcolor",false // [[1,"red"],[4,"blue"]]
					   ], outrod_ );
	function outrod(k)= hash(outrod, k);

	inrod = !inrod_?false
			  : update([ "style", "rod" // rod|block
					   , "len", ri-r2o 
					   , "count",  6  
					   ,"color",  color  
					   ,"transp", transp      
					   ,"fn", fn        
					   , "r",  0.4      // for style = rod
					   ,"width",0.4   // for style = bar
					   ,"h", h    // for style = bar
					   ,"markcolor",false // [1,"red",4,"blue"]
					   ], inrod_ );	
	function inrod(k)= hash(inrod, k);

	inrodmarkcolor= inrod("markcolor");
	outrodmarkcolor= outrod("markcolor");	
	//echo("outrodmarkcolor = ", outrodmarkcolor);

	outrodPts= arcPts( r=ro, a=360, count=outrod("count") );
	inrodPts= arcPts( r=ri, a=360, count=inrod("count") );

	//outrodstyle = trueor(outrod("style","bar")!="bar","block");

	//echo(ORIGIN);
	//echo(hash(ops,"h"));

	translate( ops("target") )
	rotate( rotangles_z2p( ops("normal") ) )
	rotate( [0,0, rotang ] ){

	  difference(){
		color( color, transp )
		cylinder(r=ro, h=h,   center=true, $fn=fn );
		cylinder(r=ri, h=h+1, center=true, $fn=fn );
	  }
	
	  if( r2o>0 ){ 
		difference(){
		color( color, transp )
			cylinder(r=r2o, h=h, center=true, $fn=fn );
			cylinder(r=r2i, h=h+1, center=true, $fn=fn );
		}
	  }
	  //------------------------------------------------
	  if( outrod ){
		if( outrod("style")=="block"){
			for( i= [0:len(outrodPts)-1] ){
                p= outrodPts[i];
				L=len(outrodPts);
				FL=floor(len(outrodPts)/4);
				rodcolor= haskey(outrodmarkcolor,i)
								?hash(outrodmarkcolor,i)
								:outrod("color"); 
					
//			  assign( p= outrodPts[i]
//					, L=len(outrodPts)
//					, FL=floor(len(outrodPts)/4)
//					, rodcolor= haskey(outrodmarkcolor,i)
//								?hash(outrodmarkcolor,i)
//								:outrod("color") 
//					)
                //{
				//echo("rodcolor = ", rodcolor );
                //echo("outrodmarkcolor = ", outrodmarkcolor );    
			   Line( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= -outrod("len") )
					] 
			     ,  update( outrod, ["shift","center"
								 ,"color",rodcolor
								  ,"depth",outrod("h") 
								  ,"extension0",
								     // Extends to cover line-curve gap: 
									hash(outrod, "extension0",
									 ro-shortside(ro, outrod("width") )
									+ GAPFILLER )
								, "rotate", outrod("rotate")>0?
										    ((i>=FL)
											&&( i< L-(L/4==FL?FL: FL+1)				  
											)?-1:1) 
                                                  *outrod("rotate")
											:0]	
				//,["color", i==0?"red": i==1?"green":outrod("color") ]

								)
                   );	
			}
		} else if (outrod("style")=="rod"){
			//for( p= outrodPts ){
			for( i= [0:len(outrodPts)-1] ){
              	p= outrodPts[i];
				rodcolor= haskey(outrodmarkcolor,i)
								?hash(outrodmarkcolor,i)
								:outrod("color"); 
					 
//			  assign( p= outrodPts[i]
//					, rodcolor= haskey(outrodmarkcolor,i)
//								?hash(outrodmarkcolor,i)
//								:outrod("color") 
//					)
                
			   Line0( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= -outrod("len") 
							)
					] 
			     , update( outrod, ["color", rodcolor] )
					
                   );	
			}
		}
	
	  } // if 
	  //------------------------------------------------
	  if( inrod ){
		if( inrod("style")=="block"){
			for( i= [0:len(inrodPts)-1] ){
			    p= inrodPts[i];
				L=len(inrodPts);
				FL=floor(len(inrodPts)/4);
				rodcolor= haskey(inrodmarkcolor,i)
								?hash(inrodmarkcolor,i)
								:inrod("color") ;					
//			  assign( p= inrodPts[i]
//					, L=len(inrodPts)
//					, FL=floor(len(inrodPts)/4)
//					, rodcolor= haskey(inrodmarkcolor,i)
//								?hash(inrodmarkcolor,i)
//								:inrod("color") 
//					)
                //{
			  //echo_("L={_}, FL={_}, i={_}, i>=FL({_}), i-FL={_}, L-i={_}, i/4={_}"
				//	,[L,FL,i,i>=FL,i-FL,L-i, i/4] );	
			   Line( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= inrod("len") )
					] 
			     ,  update( inrod, [ "shift","center"
								, "color",rodcolor
								, "depth",inrod("h") 
								, "extension1",
								     // Extends to cover line-curve gap: 
									hash(inrod, "extension1", 
									 ro-shortside(ro, inrod("width") )
									+ GAPFILLER 
									)

								  
				//,["color", i==0?"red": i==1?"green":inrod("color") ]

								// If there's block rotation, need to change the
								// sign of rotation based on i  
/*
    L              FL    i_change  L-i_change
	4: +--+        1          3    1
	5: +--++                  3    2
	6: +---++                 4    2
	7: +----++                5    2 

	8: ++----++    2          6    2
	9: ++----+++              6    3
	10:++-----+++             7    3
	11:++------+++             8   3

	12:+++------+++     3     9    3
	13:+++------++++          9    4
	14:+++-------++++         10   4 
	15:+++--------++++        11   4

	16:++++--------++++   4   12   4 

*/								, "rotate", inrod("rotate")>0?
										    ((i>=FL)
											&&( i< L-(L/4==FL?FL: FL+1)				  
											)?-1:1) 
                                                  *inrod("rotate")
											:0]		
								)
                   );	
			  //}// assign
			}// for
		} else if (inrod("style")=="rod"){
			for( i= [0:len(inrodPts)-1] ){
                p= inrodPts[i];
				rodcolor= haskey(inrodmarkcolor,i)
								?hash(inrodmarkcolor,i)
								:inrod("color"); 
					
//			  assign( p= inrodPts[i]
//					, rodcolor= haskey(inrodmarkcolor,i)
//								?hash(inrodmarkcolor,i)
//								:inrod("color") 
//					)
                
			Line0( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= inrod("len")
									// Extends to cover line-curve gap: 
									+ r2o-shortside(r2o, inrod("r") )
									+ GAPFILLER 
							)
					] 
			     , update( inrod, ["color", rodcolor] )
                   );	
			}
	  } // if 
	  
	  }
	 }//

} // Ring



module Ring_demo_1()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0] 
			  ,"r2o", 1
		 	  ,"r2i", 0.8
			  ,"rotate", 720*$t
			  ,"color", "lightgreen"
			  ,"outrod", ["count",6,"width",1, "style","block","h",0.5]
			  ,"inrod", ["count",4]
			  ] 
		);
	Line( pq, ["r",0.5, "extension0", 2] );
	
}	

module Ring_demo_2()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
			  ,"r2o", 1
		 	  ,"r2i", 0.8
			  ,"rotate", 720*$t
			  ,"color", "lightgreen"
			  ,"outrod", ["count",12,"width",1, "h",0.5,"color","khaki"]
			  ,"inrod", ["count",24
						,"style","block"
						,"h", 0.6
						,"width",0.1
						,"color","blue"
					    ]
			   
			  ] 
		);
	//}
	Line( pq, ["r",0.5, "extension0", 2] );
	
}

module Ring_demo_3()
{
	
	//pq = randPts(2);
	pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
			  //,"h", 5
			  ,"ro", 3.2
		 	  ,"ri", 2.8
			  ,"r2o", 1
		 	  ,"r2i", .7
			  ,"rotate", 720*$t
			  ,"color", "lightgreen" 
			  //,"outrod", ["count",12,"width",1, "h",0.5,"color","khaki"] 
			  ,"inrod",   ["count",15
						,"style","block"
						,"h", 1
						,"width",0.1
						,"color","blue"
						,"rotate", 30
						,"extension1", 0.25 // overwrite default exteionsion1
										    // to cover the gaps between rods
											// and the out curve of ring2 
					    ]
			    
			  ] 
		);


	Line( pq, [["r",0.5], ["extension0", 2]] );
	
}

module Ring_demo_4()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0] 
			  //,"h", 5
			  ,"ro", 3.2
		 	  ,"ri", 2.8
			  ,"r2o", 1
		 	  ,"r2i", .7
			  ,"rotate", 720*$t
			  ,"color", "lightgreen"
			  ,"inrod", ["count",6,"width",1, "h",0.5]
			  ,"outrod", ["count",12
						,"style","block"
						,"h", 1.6
						,"width",0.2
						,"color","blue"
						,"rotate", 60
						,"extension0", .35 // overwrite default exteionsion1
										    // to cover the gaps between rods
											// and the out curve of ring2 
					    ]
			    
			  ] 
		);


	Line( pq, ["r",0.5, "extension0", 2] );
	
}

module Ring_demo_5_markrod()
{
	
	pq = randPts(2);
	pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	pq = [ORIGIN, [0,0,3]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
			 // ,"inrod", ["count",6,"width",1, "h",0.5] 
			  ,"outrod", ["count",12
						,"style","block"
						,"color","blue"
						,"transp",0.4
						,"markcolor", [0,"red",5,"green"] 
						,"markpts", true
						]
			   	
			  ,"inrod", ["count",6
						//,"style","block"
						//,"color","blue"
						,"transp",0.4
                        ,"r",0.3
						,"markcolor", [0,"purple"] 
					    ]
			    ] 
			   
		);


	Line( pq, ["r",1.5, "extension0", 2] );
	
}

module Ring_demo_6()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
              , "ri",0
               , "h",0.3
               
			 // ,"inrod", ["count",6,"width",1, "h",0.5] 
			  ,"outrod", ["count",24, "len", 0.5
						,"style","block"
						,"markcolor", [0,"blue"] 
					    //,"color","blue"
						]
			   	
			  /*,"inrod", ["count",6
						//,"style","block"
						//,"color","blue"
						,"transp",0.4
                        ,"r",0.3
						,"markcolor", [0,"purple"] 
					    ] */
			    ] 		   
		);

	Line( pq, ["r",.5, "extension0", 0] );
}

module Ring_demo()
{
	Ring_demo_1();
	//Ring_demo_2();
	//Ring_demo_3();
	//Ring_demo_4();
	//Ring_demo_5_markrod();
    //Ring_demo_6();
	//
}
module Ring_test( ops ){ doctest(Ring,ops=ops);}

//Ring_demo();




//========================================================
Ring0=[["Ring0", "ops", "n/a", "3D, Shape"],
"
 Given an ops, draw a simple ring on a plane that has its normal defined\\
 in ops. Default ops:\\
 \\
  [ ''normal'', [0,0,1] \\
  , ''target'', ORIGIN  // where to translate to\\
  , ''ro'',3 // outer radius\\
  , ''ri'',2 // inner radius\\
  ]\\
"];

module Ring0( ops )
{
	echom("Ring0");

		ops= updates( OPS
			,[[ "normal", [0,0,1]
			 , "target", ORIGIN 
			 , "ro",3
			 , "ri",2
			 ], ops] );

		function ops(k)=hash(ops,k);

		h  = ops("h" );
		fn = ops("fn");

		//echo(ops);

		translate( ops("target") )
		rotate( rotangles_z2p( ops("normal") ) )
	  	difference(){
			color( ops("color"), ops("transp") )
			cylinder(r=ops("ro"), h=h,   center=true, $fn=fn );
			cylinder(r=ops("ri"), h=h+1, center=true, $fn=fn );
	  	}
}

module Ring0_demo_1()
{
	pq = randPts(2);
	Ring0( [ "normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   ] );
	//c = randcolor();
	//echo(c);
	Line(pq, ["r",rands( 1,20,1)[0]/10,"extension0",2, "color", randc()] );
}

module Ring0_demo_2()
{
	pq = randPts(2);
	MarkPts(pq, ["r",0.5]);
	Line(pq, ["r",0.6, "color", randc()] );
	Ring0( [ "normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "h", 10
		   , "transp", 0.3
		,"ro", 0.8, "ri", 0.7	
		   ] );
}

module Ring0_demo_3_tube()
{
	pts = randPts(9);
	pq=randPts(2);
	MarkPts(pq, ["r",0.5]);
	Line(pq, ["r",0.6, "color", randc()] );
	L = norm( pq[1]-pq[0] );
	Ring0( [ "normal", pq[1]-pq[0]
		   , "target", midPt(pq) //pq[1] 
		   , "color", randc()
		   , "h", L
		   , "transp", 0.3
		,"ro", 0.8, "ri", 0.7	
		   ] );
}

module Ring0_demo_4_tubes()
{
	pts = randPts(6,r=2);
	MarkPts(pts, ["r",.12, "colors",["brown"]]);

    c = randc();
 
	for (i=[0:len(pts)-2])
//	assign(  L= norm(pts[i+1]-pts[i])
//		  )
	{
     L= norm(pts[i+1]-pts[i]);   
	 Ring0( [ "normal", pts[i+1]-pts[i]
		   , "target", midPt([pts[i],pts[i+1]]) 
		   , "color", "khaki" //c //randc()
		   , "h", L
		   , "transp", 1//0.7
		   , "ro", .18, "ri", .16	
		   ] );
	}
	
}

module Ring0_test( ops ){ doctest(Ring0,ops=ops);}

module Ring0_demo()
{
	//Ring0_demo_1();
	//Ring0_demo_2();
	//Ring0_demo_3_tube();
	Ring0_demo_4_tubes();
}	

//Ring0_demo();
//doc(Ring0);




//========================================================
Rod=["Rod", "pqr,ops", "n/a", "Geometry,Line",
 " Given a 2- or 3-pointer (pqr), draw a line (cylinder) from point P
;; to Q using polyhedron. The 3rd point, R, is to make a plane (of pqr), 
;; from that the angle of drawn points start in a anti-clock-wise manner 
;; (when looking from P-to-Q direction). So R is to determine where the
;; points start. If it is not important, use 2-pointer for pqr and R will
;; be randomly set. 
;;
;; Imagine pqr as a new coordinate system where P is on the x axis, Q is 
;; the ORIGIN and R is on xy plane: 
;; 
;;       N (z)
;;       |
;;       |________
;;      /|'-_     |
;;     / |   '-_  |
;;    |  |      : |
;;    |  Q------|---- M (y)
;;    | / '-._/ |
;;    |/_____/'.|
;;    /          '-R
;;   /
;;  P (x)
;;
;; In the graph below, P,Q,R and 0,4 are co-planar. 
;;
;;       _7-_
;;    _-' |  '-_
;; 6 :_   |Q.   '-4
;;   | '-_|  '_-'| 
;;   |    '-5' '-|
;;   |    | |    '-_
;;   |  p_-_|    |  'R
;;   |_-' 3 |'-_ | 
;;  2-_     |   '-0
;;     '-_  | _-'
;;        '-.'
;;          1
;;
;; headangle/tailangle are 90, but customizable.
;;
;;                         R
;;                        :
;; |       .--------.    :  |
;; |     .'          '. :   |  
;; |   P'--------------Q.   | 
;; | .' pa            qa '. |
;; |------------------------|
"];

//========================================================

module Rod( pqr=randPts(3), ops=[]) //r=0.6, count=6, showindex=true)
{
	// A Rod making use of rodPts

	//echom("Rod");

	//------------------------------------------
	ops = concat
	( ops,
	, [ 
	  //------ To be sent to rodPts()
		"r", 0.05
	  , "sides",     6
	  , "label", "" 
	  , "p_pts",undef  // pts on the P end
	  , "q_pts",undef  // pts on the q end
	  , "p_angle",90   // cutting angle on p end
	  , "q_angle",90   // cutting angle on q end
	  //, "twist_angle", 0 // see twistangle()
	  , "rotate",0  	  // rotate about the PQ line, away from xz plane
	  , "cutAngleAfterRotate",true // to determine if cutting first, or
								  // rotate first. 
	  //------ The above are to sent to rodPts()
	
	  , "has_p_faces", true
	  , "has_q_faces", true
	  , "addfaces",[]
	  , "addpts",[]

	  , "echodata",false
	  , "markrange",false // Set to true or some range to mark points
						 //if true, full range
	  , "markOps",  []    // [markpt setting, label setting] 
	  , "labelrange",false
	  , "labelOps", []
	  ]
	, OPS
	);
	function ops(k)=hash(ops,k);
	sides = ops("sides");

	rodpts = rodPts( pqr, ops);

	p_pts = rodpts[0];
	q_pts = rodpts[1];

	// Check if angles are correct:
	//	echo( "angle p:", angle( [Q(pqr),P(pqr), p_pts[0]] ));
	//	echo( "angle q:", angle( [P(pqr),Q(pqr), q_pts[0]] ));

	allpts = concat( p_pts, q_pts, ops("addpts") );
	//echo("Rod.allpts: ", allpts);

	_label = ops("label");
	label= _label==true?"PQ":_label;

	if(label) LabelPts( p01(pqr), label ) ;

//	p_faces = reverse( range(sides) );
//	// faces need to be clock-wise (when viewing at the object) to avoid 
//	// the error surfaces when viewing "Thrown together". See
//	// http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
//	q_faces = range(sides, sides*2); 
//	faces = concat( ops("has_p_faces")?[p_faces]:[]
//				 , ops("has_q_faces")?[q_faces]:[]
//				 , rodsidefaces(sides)
//				 , ops("addfaces")
//				 );
//	faces = rodfaces(sides);
	faces = faces("rod",sides);
	
	echo("Rod.faces:", faces);

	//=========== marker and label =============
	allrange= range(allpts);
	markrange = hash(ops, "markrange" , if_v=true, then_v=allrange);
	labelrange= hash(ops, "labelrange", if_v=true, then_v=allrange);
	markops = or(hash(ops, "markops"),[]); 
	labelops= or(hash(ops, "labelops"),["labels",""]); 
	pts_to_mark = [ for(i=markrange)	allpts[i] ];
	pts_to_label= [ for(i=labelrange)	allpts[i] ];

	// We make them BEFORE the polyhedron so they are always visible
	if(markrange){ MarkPts( pts_to_mark, markops); }
	if(labelrange){ LabelPts( pts_to_label, hash(labelops, "labels"), labelops); }
	// =========================================

	color( ops("color"), ops("transp") )
	polyhedron( points = allpts 
			  , faces = faces
			 );

	//==========================================
	if( ops("echodata"))
	{
		echo(
			 "<b>Rod()</b>:<br/><b>ops</b> = ", ops
			,join([""
			,str("<b>q_pts</b> = ", q_pts )
			,str("<b>p_pts</b> = ", p_pts )
			,str("<b>faces</b> = ", faces )
			],";<br/>"));
	}
}
//========================================================

module _Rod_dev( pqr=randPts(3), ops=[]) //r=0.6, count=6, showindex=true)
{
	// This is the original version before rodPts() was built. After we 
	// have rodPts, the rod point calc was taken out so the current Rod()  
	// simply makes use of rodPts();

	//echom("Rod");

	//------------------------------------------
	ops = concat
	( ops,
	, [ "r", 0.05
	  , "sides",     6
	  , "label", "" 
	  , "p_pts",undef  // pts on the P end
	  , "q_pts",undef  // pts on the q end
	  , "p_angle",90   // cutting angle on p end
	  , "q_angle",90   // cutting angle on q end
	  , "rotate",0  // rotate about the PQ line, away from xz plane
	  , "cutAngleAfterRotate",true // to determine if cutting first, or
						  // rotate first. 
	  , "echodata",false

	  , "markrange",false // Set to true or some range to mark points
						 //if true, full range
	  , "markOps",  []    // [markpt setting, label setting] 
	  , "labelrange",false
	  , "labelOps", []
	  ]
	, OPS
	);

	function ops(k) = hash( ops, k);

	//echo("Rod.ops:",ops);
	//echo("Rod, p-, q-angle: ", [p_angle, q_angle] );

	r = ops("r");
	rot = ops("rotate");
	sides = ops("sides");
	p_angle = ops("p_angle");
	q_angle = ops("q_angle");
	cutAngleAfterRotate= ops("cutAngleAfterRotate");

	//echo("sides", sides);
	//------------------------------------------
	// pqr0: original pqr before rotate
	//
	pqr0= pqr==undef
		 ? randPts(3)
		 : len(pqr)==2
		   ? concat( pqr, [randPt()] )
		   : pqr ;

	//pq = p01(pqr0);
	P = pqr0[0];
	Q = pqr0[1];
	R1= pqr0[2];
	pq= [P,Q];
	N = N(pqr0);
	M = N2(pqr0);

	// Before rotation: coplanar: P Q A B 0 4 R1
	//
	//                          _-5
	//                       _-'.' '.
	//                    _-' .'     '.
	//                 _-'  .'         '.
	//              _-'   _6    _-Q----_-4--------------A 
	//            1'   _-'  '_-'   '_-'.'            _-'
	//          .' '.-'   _-' '. _-' .'           _-'
	//        .' _-' '. -'    _-'. .' '.       _-'
	//      .'_-'   _-''.  _-'   _-7    R1  _-'
	//	  2-'     P-----0-'----------------B     
	//      '.         .'  _-'
	//        '.     .' _-'
	//          '. .'_-'
	//            3-'
	//                     
	// After rotation by 45 degree: coplanar: P Q 4 0 R2 (R2 not shown)  
	//                 
	//	      5-------4
	//	     /|      /|
	//	    / |  Q'-/----------A
	//	   /  |  /\/  |       /
	//	  /   6-/-/---7      /
	//   /   / / /  \/      /
	//	1-------0   /\  	  /
	//	|  / /  |  /  R1  /
	//	| / P.--|-/------B
	//	|/    '.|/          
	//  2-------3          
	//           
	// The pl_PQ40 rotates (swings like a door) to make 0-4 move away 
	// from pl_PQAB. So [P,Q,R2] will be the new pqr to work with.

	// So, if rotation, we make a R2 that is on the pl_PQ40 to make a new 
	// pqr: [P,Q,R2] 

	// We use the othoPt T as the rotate center to carry the rotation:
	//
	//	       N (z)
	//	       |
	//	       |
	//	      /|
	//	     / |       R2  
	//	    |  |     .' 
	//	    |  Q---_------- M (y)
	//	    | / '-:_
	//	    |/_-'   '._
	//	  T /----------'-R1
	//	   /
	//	  P (x)
	//   /
	//  /
	// P2

	// Calc R2 = post-rotation R using T

	T = othoPt( [ P,R1,Q ] );

	// Since we need to get R2 by rotating an angle on the PTR1 plane, 
	// of which its normal is decided by the order of P,T, R1, the location 
	// of T is critical that it could get the rotation to the wrong side.
	// 
	// (1)
	//      R1_             PQ > TQ
	//      |  '-._
	//	p---T------'Q
	//
	// (2)           R1     PQ > TQ     
	//              /|
	//     P-------Q T
	//   
	// (3)   R1_            PQ < TQ
	//       |  '-._
	//       |       '-._
	//       T  p-------'Q
	// (4)
	//              R1      PQ < TQ
	//            .'|
	//          .'  |
	//        .'    |
	//   P---Q      T
	//   
	// Shown above, case (3) will cause PTR1 to go to wrong direction. 
	// So we make a new p1_for_rot as P to ensure that it is always on 
	// the far left:

	p1_for_rot= angle(pqr0)>=90
				? P
				: onlinePt([norm(T-Q)>=norm(P-Q)? T:P,Q], len=-1 );

	R2 = anyAnglePt( [ p1_for_rot, T, R1]     
                   , a= rot 
                   , pl="yz"
                   , len = dist([T,R1]) 
				  );

	P2 = onlinePt( [P,Q], len=-1);  // Extended P: P2----P----Q

	norot_pqr_for_p = [P2,P, R1];
	norot_pqr_for_q = [P, Q, R1];
	rot_pqr_for_p   = [P2,P, R2];
	rot_pqr_for_q   = [P, Q, R2];

	pqr_for_p = cutAngleAfterRotate&&rot>0
				? rot_pqr_for_p
				: norot_pqr_for_p;
	pqr_for_q = cutAngleAfterRotate&&rot>0
				? rot_pqr_for_q
				: norot_pqr_for_q;		

	// p_pts90 = [p0,p1,p2,p3]
	// q_pts90 = [p5,p6,p7,p8]
	// 
	// If p_angle=0, and p_pts not given, p_pts90 will
	// be the final points on p end for polyhedron 

	// p_pts90 and q_pts90 decides the points of side lines. 
	// It doesn't matter if cutAngleAfterRotate is true, 'cos 
	// the position of side lines is independent of cut-angle.
	// So we use rot_pqr_for_p. It will be = rot_pqr_for_p
	// If rotate=0 (then R2=R1).
	// 
	// Later we will project these lines to p_plane and q_plane,
	// Those planes are what decides the final sideline pts. 
	p_pts90= arcPts( rot_pqr_for_p
				  , r=r, a=360, pl="yz", count=sides );

	q_pts90= arcPts( rot_pqr_for_q
				   , r=r, a=360, pl="yz", count=sides );

	//Line0( [P, p_pts90[0] ],["r",0.05] );
	//MarkPts( [P2],["colors",["purple"]] );
	//MarkPts( p_pts90 );
	//Chain( p_pts90 );

	//-------------------------------------------------
	// Cut an angle from P end. 
	//
	// If cutAngleAfterRotate, we want to use the pqr 
	// BEFORE rotate.
	Ap = anyAnglePt( cutAngleAfterRotate
					? [P2,P,R1]
					: [P2,P,R2]
					//rot_pqr_for_p 
					// cutAngleAfterRotate
					//? norot_pqr_for_p
					//: rot_pqr_for_p 
				  , a=p_angle+(90-p_angle)*2
				  , pl="xy"); 
	Np = N([ Ap, P, Q ]);  // Normal point at P
	//echo("Nh= ", Nh);
	//MarkPts( [Ah], ["labels",["Ah"]] );
	p_plane = [Ap, Np, P ];  // The head surface 
	//Plane( headplane, ["mode",4] );
	p_pts=or( ops("p_pts")
			  , p_angle==0
			   ? p_pts90
			   : 	[ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], p_plane ) ]
			  );

	//-------------------------------------------------
	// Cut an angle from Q end. 
	Aq = anyAnglePt( cutAngleAfterRotate
					? [P,Q,R1]
					: [P,Q,R2]
					//rot_pqr_for_q
					// cutAngleAfterRotate
					//? rot_pqr_for_q
					//: norot_pqr_for_q 
				  , a=q_angle //90-q_angle
				  , pl="xy"); // Angle point for tail
	Nq = N( cutAngleAfterRotate
					? norot_pqr_for_q
					: rot_pqr_for_q );  // Normal point at Q
	//echo("Nt= ", Nt);
	//MarkPts( [At], ["labels",["At"]] );

	// q_plane is the surface of the final pts landed. Final pts
	// are determined by extending the side lines to meet the 
	// q_plane where the intersections are those final pts are. 
	//
	// These side lines will be the same no matter it's rotate-cut_angle
	// or cut_angle-then-rotate. 
	//
	// The q_plane, however, will be different (cut_angle before or
	// after rotate). 

	q_plane = [Aq, Nq, Q ];//:pqr0;  // The head surface 
	//Plane( headplane, ["mode",4] );
	q_pts=or( ops("q_pts")
			  , q_angle==0
			    ? q_pts90
			    : [ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], q_plane ) ]
			  );


//	echo("rot = ", rot);
//	echo("angle( [ p_pts[0], P, Q] )= ", angle( [ p_pts[0], P, Q] ));
//	echo("angle( [ q_pts[0], Q, P] )= ", angle( [ q_pts[0], Q, P] ));

//	if( tailangle==90 ){ tailpts = tailpts90
//	}

	allpts = concat( p_pts, q_pts );

	_label = ops("label");
	label= _label==true?"PQ":_label;

	//if(ops("showsideindex")) LabelPts( allpts, range(allpts) ) ;
	if(label) LabelPts( pq, label ) ;

	p_faces =  range(sides); 
	q_faces = range(sides, sides*2); 
	faces = concat( [p_faces], [q_faces], rodsidefaces(sides) );
	//echo("faces: ", faces);


	//=========== marker and label =============
	allrange= range(allpts);
	markrange = hash(ops, "markrange" , if_v=true, then_v=allrange);
	labelrange= hash(ops, "labelrange", if_v=true, then_v=allrange);
	markops = or(hash(ops, "markops"),[]); 
	labelops= or(hash(ops, "labelops"),["labels",""]); 
	pts_to_mark = [ for(i=markrange)	allpts[i] ];
	pts_to_label= [ for(i=labelrange)	allpts[i] ];

	// We make them BEFORE the polyhedron so they are always visible
	if(markrange){ MarkPts( pts_to_mark, markops); }
	if(labelrange){ LabelPts( pts_to_label, hash(labelops, "labels"), labelops); }
	// =========================================

	color( ops("color"), ops("transp") )
	polyhedron( points = allpts 
			  , faces = faces
			 );

	// We make marker AFTER the polyhedron so they are hidden behind
	// the object
	
//	echo_( "markrange={_}, markops={_}",[markrange,markops]);
//	echo_( "labelrange={_}, labelops={_}",[labelrange,labelops]);

	//
	// To check if the p-, q-angle work:
	//
//	echo_("Rod: cutAngleAfterRotate:{_}, p_angle={_}, angle( [Q, P, allpts[0]] )={_} "
//		, [cutAngleAfterRotate, p_angle, angle( [Q, P, allpts[0]] )] 
//		);
//	echo_("Rod: cutAngleAfterRotate:{_}, q_angle={_}, angle( [P, Q, allpts[{_}]] )={_} "
//		, [cutAngleAfterRotate, q_angle, sides, angle( [P,Q, allpts[sides]] ) ]
//		);
  


	//==========================================
	if( ops("echodata"))
	{
		echo(
			 "<b>Rod()</b>:<br/><b>ops</b> = ", ops
			,join([""
			,str("<b>q_pts</b> = ", q_pts )
			,str("<b>p_pts</b> = ", p_pts )
			,str("<b>faces</b> = ", faces )
			],";<br/>"));
	}
}

module Rod_demo_0()
{ _mdoc("Rod_demo_0"
,
 " Basic Rod() operation with the following code:
;;
;;  Rod( pqr, [''r'',1, ''sides'',5, ''labelrange'',true,''transp'',0.4] ); 
;;
;; The surface of pqr and side-indices are shown. Note how the point
;; indices are arranged. "
);	

	pqr = randPts(3);
	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",1, "sides",5, "labelrange",true,"transp",0.4] );
}
//Rod_demo_0();



module Rod_demo_1()
{ _mdoc("Rod_demo_1"
,
 " More demos showing varying sides. Default: [''sides'',6]. "
);

	Rod();

	Rod(randPts(2), ["color","red", "r",0.3, "echodata",true] );

	Rod( ops=["color","green", "r",0.5,"sides",4] );

	Rod( ops=["color","blue", "r",0.4,"sides",20] );
}
//Rod_demo_1();



module Rod_demo_2_labels()
{
_mdoc("Rod_demo_2_labels",
" Demos showing marker and label. Check out MarkPts() and LabelPts() 
;; for detailed options. 
;; 
;; dfcolor: [''label'',true] => show PQ
;; dfcolor: [''label'',''AB''] => show A,B
;; red    : [''labelrange'', true] => show all indices
;; green  : [''markrange'', true] => mark all pts
;; blue   : [''markrange'', [0,2,4,6,8,10]] => mark selected pt
;; purple : [''labelrange'',[0,6]
;;          ,''labelops'',[''labels'',[''1st'',''2nd'']]
;;          ,''markrange'',[0,6]
;;          ]
;; gray   : [''labelrange'',true
;;          , ''labelops'',[''color'',''red'',''scale'',0.02]
;;          , ''markrange'',[0,6]
;;          ]
");

	Rod(ops=["transp",0.5, "r",0.3,"label",true]);
	Rod(ops=["transp",0.5, "r",0.3,"label","AB"]);
	
	Rod(ops=["color","red","transp",0.3, "r",0.6
			, "labelrange", true
			]);

	Rod(ops=["color","green","transp",0.3, "r",0.3
			, "markrange",true]);

	Rod(ops=["color","blue","transp",0.3, "r",0.6
			, "markrange",[0,2,4,6,8,10]]);

	Rod(ops=["color","brown","transp",0.3, "r",0.6
			, "labelrange",[0,6]
			, "labelops",["labels",["1st","2nd"]]
			, "markrange",[0,6]
			]
		);

	Rod(ops=["color","gray","transp",0.3, "r",0.6
			, "labelrange",true
			, "labelops",["color","red","scale",0.02]
			, "markrange",[0,6]
			]
		);

}
//Rod_demo_2_labels();



module Rod_demo_3_angles()
{
_mdoc("Rod_demo_3_angles",
" p_angle, q_angle
;;
;; yellow one: 60, 45
;; red one: 30, 60
;;
;; default (when not given) = 90
");

	pqr = randPts(3);

	Chain(pqr, ["r",0.05]);

	MarkPts( pqr, ["labels", ["P(60)","Q(45)","R"]] );

	Rod(pqr, ops=["transp",0.5, "r",1, "showsideindex",true
			, "p_angle",60
			, "q_angle",45
			, "echodata", true
			]);


	pqr2 = randPts(3);

	Chain(pqr2, ["r",0.05]);

	MarkPts( pqr2, ["labels", ["P(30)","Q(60)","R"]] );

	Rod(pqr2, ops=["transp",0.5, "r",1, "showsideindex",true
			, "p_angle",30
			, "q_angle",60
			, "echodata", true
			, "color","red"
			]);
}
//Rod_demo_3_angles();



module Rod_demo_4_angles()
{
_mdoc("Rod_demo_4_angles",
" Show how can the p_angle, q_angle be applied to join rods. 
;;
;; Note that this is just for demo. The joints in this demo
;; actually have two exact same faces overlapping. 
");

	pqr = randPts(3);
	rqp = p210(pqr);

	Chain(pqr);

	M = angleBisectPt( pqr );
	angle = angle(pqr)/2;
	MarkPts(pqr, ["labels","PQR"] );
	Rod(pqr, ops=["transp",0.5, "r",0.4
				//,"label",true, "showsideindex",true
				, "q_angle", angle //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp, ops=["transp",0.5, "r",0.4
				//,"label",true, "showsideindex",true
				, "q_angle", angle // angle( [ P(pqr), R(pqr), M] )
				]);
	
	//==========================
	pqr2 = randPts(3);
	rqp2 = p210(pqr2);

	//Chain(pqr);

	M2 = angleBisectPt( pqr2 );
	angle2 = angle(pqr2)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("red",0.7){
	Rod(pqr2, ops=["transp",0.2, "r",0.3, "sides",3
				//,"label",true, "showsideindex",true
				, "q_angle", angle2 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp2, ops=["transp",0.2, "r",0.3, "sides",3
				//,"label",true, "showsideindex",true
				, "q_angle", angle2 // angle( [ P(pqr), R(pqr), M] )
				]);
	}

	//==========================
	pqr3 = randPts(3);
	rqp3 = p210(pqr3);

	//Chain(pqr);

	M3 = angleBisectPt( pqr3 );
	angle3 = angle(pqr3)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("green", 0.7){
	Rod(pqr3, ops=["transp",0.2, "r",0.3, "sides",4
				//,"label",true, "showsideindex",true
				, "q_angle", angle3 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp3, ops=["transp",0.2, "r",0.3, "sides",4
				//,"label",true, "showsideindex",true
				, "q_angle", angle3 // angle( [ P(pqr), R(pqr), M] )
				]);
	}

	//==========================
	pqr4 = randPts(4);
	rqp4 = p210(pqr4);

	//Chain(pqr);

	M4 = angleBisectPt( pqr4 );
	angle4 = angle(pqr4)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("blue", 0.7){
	Rod(pqr4, ops=["transp",0.2, "r",0.3, "sides",18
				//,"label",true, "showsideindex",true
				, "q_angle", angle4 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp4, ops=["transp",0.2, "r",0.3, "sides",18
				//,"label",true, "showsideindex",true
				, "q_angle", angle4 // angle( [ P(pqr), R(pqr), M] )
				]);
	}
}
//Rod_demo_4_angles();



module Rod_demo_5_user_q_pts()
{
_mdoc("Rod_demo_5_user_q_pts",
" This demo shows how to use customized q_pts
;; with expanded q_pts.
;;
;; The pts when angle=90
;;
;;   q_pts90 = arcPts( pqr, r=r, a=360, pl=''yz'', count=sides ); 
;;
;; Expand them to twice the r
;;
;;   ex_q_pts90= expandPts( q_pts90, dist=2*r ); 
;;
;;   Rod( pqr, [''q_pts'', ex_q_pts90, ...] ); 
");

	pqr = randPts(3);
	
	//P2 = onlinePt( p01(pqr), len=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);

	sides = 6;
	r = 0.5;
	
	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=sides );

	// Expand them to twice the r
	ex_q_pts90= expandPts( q_pts90, dist=2*r );

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",r, "q_pts", ex_q_pts90
			 , "sides",sides
			 , "showsideindex",true,"transp",0.4] );
}
//Rod_demo_5_user_q_pts();



module Rod_demo_6_user_q_pts()
{
_mdoc("Rod_demo_6_user_q_pts",
" Another demo shows how to use customized q_pts.
;;
;;   sides=24; 
;;   spark_ratio=3; 
;;
;; The pts when angle=90
;;
;;   q_pts90 = arcPts( pqr, r=r, a=360, pl=''yz'', count=sides ); 
;;
;; Construct the points:
;;
;;   pts= [for(i=range(sides))
;;          mod(i,3)==0? onlinePt( [Q, q_pts90[i]], ratio=spark_ratio)
;;                     : q_pts90[i]
;;        ];
;;
;;   Rod( pqr, [''q_pts'', pts, ...] ); 
");

	// This demo shows how to use customized headpts

	pqr = randPts(3);
	
	//P2 = onlinePt( p01(pqr), len=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);

	sides = 24;
	r = 0.5;
	spark_ratio = 3;

	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=sides );

	pts= [for(i=range(sides))
			mod(i,3)==0? onlinePt( [Q, q_pts90[i]], ratio=spark_ratio)
						: q_pts90[i]
		];

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",r, "q_pts", pts
			 , "sides",sides
			 , "showsideindex",false
			,"transp",1] );
}
//Rod_demo_6_user_q_pts();



module Rod_demo_7_gear()
{
_mdoc("Rod_demo_7_gear",
 " This demo shows how to use Rod() to make a simple gear-like
;; structure using customized p_pts/q_pts
;;
;; Assuming each tooth has 6 pts (defined as pt_per_tooth):
;;      __
;;     /  ^      23 
;;     |  |    1    4
;;  ---+  +    0    5
;;
;; We extend the point certain distance outward according to 
;; mod(i, pt_per_tooth). 
");     
	

	_pqr = randPts(3);
	
	pqr = newx( _pqr, onlinePt( p10(_pqr), len=0.5) );
	//P2 = onlinePt( p01(pqr), dist=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);
	P2=onlinePt( [Q,P], len=1);

	teeth = 18;
	pt_per_tooth = 6;
	sides = teeth* pt_per_tooth;
	r = 1; //0.5;
	tooth_height=0.2;

	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=sides );
	
	//tailpts90 = arcPts( [P2,P,R], r=r, a=360, pl="yz", count=sides );

	gear_pts_q= [for(i=range(sides))
			 mod(i,pt_per_tooth)==1||mod(i,pt_per_tooth)==4
			  ? onlinePt( [q_pts90[i],Q], len=-tooth_height*1/2)
			  : mod(i,pt_per_tooth)==2||mod(i,pt_per_tooth)==3
			    ? onlinePt( [q_pts90[i],Q], len=-tooth_height)
			//:mod(i,pt_per_tooth)==0|| mod(i,pt_per_tooth)==5
				: q_pts90[i]
		];

	dpq = P-Q;
	gear_pts_p = [for(p=gear_pts_q) p+dpq];

	//=========================

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",5, "len",0.5
			, "q_pts", gear_pts_q
			, "p_pts", gear_pts_p
			 , "sides",sides
			 , "showsideindex",false
			,"transp",0.8] );
}
//Rod_demo_7_gear();



module Rod_demo_8_rotate()
{
sides=6;
_mdoc("Rod_demo_8_rotate",
_s(" red: rotate {_} degree
;; green: rotate {_} degree
;; 
;; Note how the rotation goes (counter clock-wise)
", [360/sides/2, 360/sides])
);
	
	pqr = randPts(3);
	pqr=[[-2.73324, -2.05788, -2.44777], [-0.285864, -1.8622, -1.77504], [2.84959, 2.00984, -2.64569]];
	P=P(pqr); Q=Q(pqr); R=R(pqr);

	MarkPts( pqr, ["labels","PQR"] );
	Plane( [ onlinePt( [Q,P], ratio=2)
		   , Q(pqr)
		   ,onlinePt( [Q,R], ratio=2)
		   ] ,  , ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",1, "sides",sides, "markrange",[0],"labelrange",[0],"transp",0.9] );
//
	// 2014.8/21: 
	// Found that sometimes the rotation goes to the opposite direction
	// 
	//	It seems that it occurs in (2) case:
	//
	//	(1)
	//    
	//        R._
	//           '-._
	//	p---T------'Q
	//
	//
	//	(2)    
	//        R._
	//           '-._
	//               '-._
	//	    T  p-------'Q
	//
	// Fixed in 20140821-3

	//echo("pqr: ", pqr);
	T = othoPt( [P,R,Q] );
	echo( "Is TQ > PQ ? ", norm(T-Q)> norm(P-Q) );


	P=P(pqr); Q=Q(pqr); R=R(pqr);

	Rod( [ onlinePt( [P,Q], len=0.3)
		 , onlinePt( [Q,P], len=0.3)
		 , R ]
		, ["r",2, "sides",sides,"markrange",[0],"labelrange",[0],"color","red", "transp",0.6
			  , "rotate", 360/sides/2] );

	Rod( [ onlinePt( [P,Q], len=0.5)
		 , onlinePt( [Q,P], len=0.5)
		 , R ]
		, ["r",3, "sides",sides,"markrange",[0],"labelrange",[0],"color","green", "transp",0.3
			  , "rotate", 360/sides] );
}
//Rod_demo_8_rotate();




module Rod_demo_8_1_q_rotate() // give up
{
_mdoc("Rod_demo_8_1_q_rotate",
" We give up this appoach of making chain using Rod with both 
twistangle and p- q-angle specified. 'cos it's too difficult 
to get both of twistangle and p- q-angle right. 2014.8.29

");

	pqr = randPts(3);
	pqrs1 = concat( pqr, [randPt()] );

	shift = normalPt(pqr,len=1.5)-pqrs1[1];

	pqrs2= [for(p=pqrs1) p+shift];
	pqrs3 = [for(p=pqrs2) p+shift];

pqrs1= [[2.26761, 1.21599, 0.51161], [0.447378, -2.10168, 0.613161], [-0.582338, -1.77329, 1.33888], [-2.13944, 0.683927, -2.8544]];
pqrs2= [[3.02213, 0.840008, 1.75231], [1.20189, -2.47767, 1.85386], [0.172176, -2.14928, 2.57958], [-1.38492, 0.307941, -1.6137]];
pqrs3=  [[3.77664, 0.464021, 2.99301], [1.95641, -2.85365, 3.09457], [0.92669, -2.52527, 3.82029], [-0.630407, -0.0680459, -0.372994]];

	aPQR = angle( pqr ) /2;
	aQRS = angle( p123(pqrs1) )/2;
	taPQRS= twistangle( pqrs1 ); 

	echo("aPQR=", aPQR);
	echo("aQRS=", aQRS);
	echo("taPQRS=", taPQRS);
	 
	echo( "shift=", shift);
	echo( "pqrs1=", pqrs1);
	echo( "pqrs2=", pqrs2);
	echo( "pqrs3=", pqrs3);
	
	MarkPts( pqrs1, ["labels", "PQRS"] );

	ops= ["r",0.4, "transp",0.9];

	Chain( pqrs1 );
	Rod( p012(pqrs1) , concat(["q_angle",aPQR],ops) );
	Rod( p123(pqrs1) , concat(["p_angle",aPQR, "q_angle",aQRS
						, "rotate", -taPQRS  // Rod_QR is calc based on QRS, so 
												// need to make a twiste
						, "cutAngleAfterRotate",false // aPQR is calc based on PQR,                                                         
													 // that is BEFORE rotate
						//, "_rotate", taPQRS  // need to rotate q-end back to QRS 
							],ops) );
//	Rod( p123(pqrs1) , concat(["p_angle",aPQR, "q_angle",aQRS, "rotate", taPQRS, "cutAngleAfterRotate",true],ops) );
	Rod( p231(pqrs1) , concat(["p_angle",aPQR],ops) );

//====================================
	
//	opqr1 = randPts(4);
//	shift = normalPt(p012(opqr1),len=1.5)-opqr1[1];
//	//Normal(p012(opqr1),["color","red","len",3]);
//	opqr2 = [for(p=opqr1) p+shift];
//	opqr3 = [for(p=opqr2) p+shift];
//	//pts = [pqr, pqr2, pqr3];
//	
//	//ap = angle( opqr )/2 ;
//	aOPQ = angle( opqr1 )/2;
//	aPQR = angle( p123(opqr1) )/ 2 ;
//
//	taOPQ= twistangle( reverse(opqr1) );
//
//	echo("aPQR=", aPQR);
//	echo("aOPQ=", aOPQ);
//	echo("taOPQ=", taOPQ);
//	 
//	echo( "shift=", shift);
//	echo( "opqr1=", opqr1);
//	echo( "opqr2=", opqr2);
//	echo( "opqr3=", opqr3);
//	echo( "p123(opqr1) = ", p123(opqr1));
//	echo( "slice(opqr1,1)",slice(opqr1,1));
//
//	
//	MarkPts( opqr1, ["labels", "OPQR"] );
//
//	ops= ["r",0.4, "transp",0.9];
//
//	Chain( opqr1 );
//	Rod( p012(opqr1) , concat(["q_angle",aOPQ],ops) );
//	Rod( p123(opqr1) , concat(["p_angle",aOPQ, "q_angle",aPQR, "rotate", taOPQ, "cutAngleAfterRotate",true],ops) );
//	Rod( p231(opqr1) , concat(["p_angle",aPQR],ops) );
//	
//	Chain( opqr2, ["color","red"]);
//	Rod( p123(opqr1) , concat(["q_angle",aPQR],ops) );
//	Rod( p321(opqr1) , concat(["q_angle",aPQR],ops) );
////
////	Chain( opqr3, ["color","green"] );
////	Rod( p123(opqr3) , ops );
////	Rod( reverse(p123(opqr3)) , concat([ ], ops) );


}

//Rod_demo_8_1_q_rotate();



module Rod_demo_9_rotate_and_angles()
{
_mdoc("Rod_demo_9_rotate_and_angles"
," This demo shows cases when both rotate and p_angle/q_angle are set.
;; It is tricky 'cos the order makes difference. 
;;
;; First we make two copies of a same Rod side by side (yellow), then 
;; add a larger one to each. Make both p_angle and q_angle = 45. 
;; 
;; red: cutAngleAfterRotate= true // default :  rotate then cut
;; green: cutAngleAfterRotate= false // cut then rotate
;;
;; The blue dots on them show how the first point rotates by 30 degree.
");

	sides=6;

	pqr = randPts(3);
	pqr2 = [for(p=pqr) p+ normalPt(pqr,len=4)-Q(pqr)];
	
	for(pts=[pqr,pqr2])
	{
		Chain( pts ,["r",0.02, "color","blue"]);
		
		MarkPts( pts, ["labels","PQR"] );
		
		Plane(pts, ["mode",3]);
		
		Rod( pts, ["r",1.7, "sides",sides, "transp",1
				  ,"markrange",[0]
				  ,"labelrange",[0]
				] 
				);
	}	

	xpqr = concat( linePts( p01(pqr), dist=[0.1,0.1]), [R(pqr)]);
	xpqr2= [for(p=xpqr) p+ normalPt(xpqr,len=4)-Q(xpqr)];

	echo( "xpqr: ", xpqr );
	echo( "xpqr2:", xpqr2);

	ops=[ "r",2
		, "sides",sides
		, "labelrange",true
		, "transp",0.7, 
		, "markrange",[0]
		, "markops",["colors",["blue"]]
		, "p_angle",45
		, "q_angle",45
		, "rotate", 360/sides/2
		];

	Rod( xpqr, concat( ["color","red"  , "cutAngleAfterRotate", true],ops ));
	Rod( xpqr2,concat( ["color","green", "cutAngleAfterRotate", false],ops) );


}
//Rod_demo_9_rotate_and_angles();


//==========================
module Rod_demo_10_join_rotate()
{
_mdoc( "Rod_demo_10_chain_rotate"
, " Show the role of cutAngleAfterRotate in making Rod joint when
;; rotation is needed. 
;;
;; dfcolor: no rotate 
;; red    : rotate. cutAngleAfterRotate= true (default)
;; green  : rotate, cutAngleAfterRotate= false
;;
");
	pqr = randPts(3);
	pqr2 = [for(p=pqr) p+ normalPt(pqr,len=1.5)-Q(pqr)];
	pqr3 = [for(p=pqr) p+ normalPt(pqr,len=3)-Q(pqr)];
	pts = [pqr, pqr2, pqr3];

	colors=[ undef, "red", "green" ];
	rotates=[ 0, 45, 45 ];
	caar  =[ undef, true, false ];

	for(i= range(3))
	{
		//Chain( pts[i] ); // ,["r",0.02, "color","blue"]);

		JRod( pts[i]
			, rotate = rotates[i]
			, ops = [ "cutAngleAfterRotate", caar[i]
					, "color",colors[i]
					, "transp", 0.8
					, "r", 0.5
					, "sides", 4
					, "label",true
					]
			);

	}	
	jr_ops= [ "transp", 0.7
			, "r", 0.5
			, "sides", 4
			, "label",true
			];

	module JRod(pqr=randPts(3), rotate=0, ops=[] )
	{
		Chainf(pqr, ["closed",false]);

		P=P(pqr); Q=Q(pqr); R=R(pqr); 
		echo("Rod_demo_10_chain_rotate.pqr=", pqr);
		angle = angle(pqr)/2; 
		//echo("angle= ", angle);
		rqp= [R,Q,P];
		Rod( pqr, concat(["rotate", rotate,"q_angle",angle],ops,jr_ops) );
		Rod( rqp, concat(["rotate",-rotate,"q_angle",angle],ops,jr_ops) );
	}		
}

//==========================
module Rod_demo_11_chain()
{
_mdoc( "Rod_demo_11_chain"
, " Show the role of cutAngleAfterRotate in making Rod joint when
;; rotation is needed. 
;;
;; dfcolor: no rotate 
;; red    : rotate. cutAngleAfterRotate= true (default)
;; green  : rotate, cutAngleAfterRotate= false
;;
");
	sides = 36;
	count = 4;
	
	linepts= randPts(count+1);

	//Chain( linepts );
	//MarkPts( linepts, ["labels",true] );

	_allrps = [for(i=range(count-1))
				let( pqr = [ linepts[i], linepts[i+1], linepts[i+2] ]
				   , opqr= [ linepts[i-1],  linepts[i], linepts[i+1], linepts[i+2] ]
				   , ang = angle(pqr)/2
				)
				rodPts( pqr
					  , ["r",0.5, "sides", sides
						,"q_angle", i>=count-2?90:ang
						,"q_rotate", i==0?0:twistangle(opqr)
						]
					  )
			 ];

//	allrps = concat( _allrps[0][0], [ for(rp=_allrps) rp[1] ]);
	allrps = concat( _allrps[0][0], joinarr( [ for(rp=_allrps) rp[1] ] ));
	//LabelPts( allrps, labels="");
	//Chain( allrps );					

//	p00  = onlinePt( p01(linepts), len=-1 );
//	plast= onlinePt( [get(linepts,-1), get(linepts,-2)] , len=-1 );
//
//	linepts2= concat( [p00], linepts, [plast] );
//
//	Chain( linepts2, ["transp",0.5, "r",0.3, "color","red"] );
//
//	//rp = rodPts( linepts,["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	//Chain( rp );
//
//	rp1 = rodPts( slice(linepts, 0,3),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp1 );
//
//	rp2 = rodPts( slice(linepts, 1,4),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp2 );
		
//	pts= joinarr( 
//			[for(c=range(1,count+1))
//				let( pqr=[ linepts2[c-1]
//						, linepts2[c]
//						, linepts2[c+1]
//						]
//				   , ang = angle(pqr)/2
//					)
//				rodPts(pqr,["sides",sides,"q_angle", ang] )
//			]
//		);

//[ "r", 0.05
//	  , "sides",     6
//	  , "label", "" 
//	  , "p_pts",undef  // pts on the P end
//	  , "q_pts",undef  // pts on the q end
//	  , "p_angle",90   // cutting angle on p end
//	  , "q_angle",90   // cutting angle on q end
//	  , "rotate",0  // rotate about the PQ line, away from xz plane
//	  , "cutAngleAfterRotate",true // to determine if cutting first, or
//						  // rotate first. 
//	  ]
	faces = faces("chain", sides=sides, count=count);

//	echo("faces_demo_5_chain.N=", N);
	echo("<br/>faces_demo_5_chain._allrps=", _allrps);
	echo("<br/>faces_demo_5_chain.allrps=", allrps);
	//echo("<br/>faces_demo_5_chain.pts=", pts);
	echo("<br/>faces_demo_5_chain.faces=", faces);
	//color(false, 0.8)
	polyhedron( points = allrps
			 , faces = faces
			 );

}

//Rod_demo_0();
//Rod_demo_1();
//Rod_demo_2_labels();
//Rod_demo_3_angles();
//Rod_demo_4_angles();
//Rod_demo_5_user_q_pts();
//Rod_demo_6_user_q_pts();
//Rod_demo_7_gear();
//Rod_demo_8_rotate();
//Rod_demo_8_1_q_rotate(); // given-up appoach, wait to be removed. 2014.8.29
//Rod_demo_9_rotate_and_angles();
//Rod_demo_10_join_rotate();
//Rod_demo_11_chain();
//Rod_demo_12_addfaces();

module Rod_demo_12_addfaces()
{
	echom("Rod_demo_11_addfaces");
	r = 1.5;
	sides = 6;
	pqr = randPts(3);

	Chain(pqr);
	rodpts = rodPts( pqr, ["r",1.5, "sides", sides] );
//	
//	echo( "rodpts: ", rodpts);
//	echo( "joinarr(rodpts): ", joinarr(rodpts) );
//	LabelPts( join(rodpts),true );

	MarkPts( joinarr(rodpts), ["labels",true] );
//	inner_p_pts = reverse( arcPts( p102(pqr), r=r*2/3, a=360, pl="yz", count=sides));    
//	inner_q_pts =  arcPts( pqr, r=r*2/3, a=360, pl="yz", count=sides);    

	inner_p_pts = expandPts( rodpts[0], dist=-0.5 );
	inner_q_pts = expandPts( rodpts[1], dist=-0.5 );

	echo(inner_p_pts, inner_q_pts );
	Chain(inner_p_pts);
	Chain(inner_q_pts);

	faces = rodsidefaces(sides);

	Rod( pqr,  ["r",1.5, "sides", sides, "label",true, "transp", 0.7

		,"has_p_faces",false
		,"has_q_faces",false
		,"addpts", concat( inner_p_pts, inner_q_pts)
		,"addfaces", concat( [for(f=faces) f+[sides*2,sides*2,sides*2,sides*2] ] )
	]);
}

module Rod_demo_12_hole()
{
_mdoc( "Rod_demo_12_hole"
,"
");
	sides=4;
	pqr = randPts(3);
	Chain(pqr);
	//MarkPts( pqr, ["labels","PQR"] );
	Rod( pqr, ["r",.7, "transp",0.8, "sides",sides
			  ,"label",true
				, "labelrange",true
				, "labelops", ["begnum",sides*2, "color","red" ] 
			  ] );  

	Rod( pqr, ["r",2, "transp",0.2, "sides",sides
			  ,"label",true
				, "labelrange",true] );  

//    p_faces=	 [ [0,3,11, 8]
//			 , [1,0, 8, 9]
//			 , [2,1, 9,10]
//			 , [3,2,10,11] ];
//
//	q_faces=	 [ [4,5,13,12]
//			 , [5,6,14,13]
//			 , [6,7,15,14]
//			 , [7,4,12,15] ]

}

//Rod_demo_12_hole();
module Rod_bugfixing()
{
	pqr=[[-2.73324, -2.05788, -2.44777], [-0.285864, -1.8622, -1.77504], [2.84959, 2.00984, -2.64569]];
	rqp= [R(pqr),Q(pqr),P(pqr)];
	Rod( pqr, ["r", 0.8, "transp", 0.7, "label",true
				, "labelrange",true, "labelops",["begnum",0,"prefix",""]]);
	Chain(pqr);
	MarkPts(pqr);

	//Rod( rqp, ["r", 0.8, "transp", 0.7, "label",true
	//			, "labelrange",true, "labelops",["begnum",0,"prefix",""]]);

}
//Rod_bugfixing();



//========================================================
Text=["subarr","arr, cover=[1,1], cycle=true", "array", "Array",
 " Given an array (arr) and range of cover (cover), which is in the form
;; of [m,n], i.e., [1,1] means items i-1,i,i+1, return an array of items.
;;
;; Set cycle=true (default) to go around the beginning or end and resume 
;; from the other side. 
"];

module Text(pqr, txt){
    
  	module transform(followVP=ops("vp"))
	{
		translate( midPt( [L0,L1] ) )
	     color( ops("color"), ops("transp") )
		if(followVP){ rotate( $vpr ) children(); }
		else { rotate( rotangles_z2p( Q-P ) )
			  rotate( [ 90, -90, 0] )
			  children();
			}
	}
	conv = ops("conv");
	//echo("conv",conv, "L", L, "numstr(L, uconv=uconv)", numstr(L, conv=conv));
	transform()
	scale(0.04) text(str(ops("prefix")
						  //, L, "="
						  , numstr(L
								, conv=ops("conv")
								, d=ops("decimal")
								, maxd=ops("maxdecimal")
								)
						  , ops("suffix") ));
    
  
}

module Text_dev(){

  module AxisLines( oxyz
      , lnops=[]
      , plops=[]
    )
  {
      ln_ops= concat( lnops, ["r",0.03, "transp", 0.3]);
      pl_ops= concat( lnops, ["color","blue", "mode",3, "transp",0.2]); 
      
      Line0( p01( oxyz), concat(["color", "red"], ln_ops) );
      Line0( p02( oxyz), concat(["color", "green"], ln_ops) );
      Line0( p03( oxyz), concat(["color", "blue"], ln_ops) );
      
      color( "green", hash(pl_ops,"transp") )
      { Plane( p123( oxyz), pl_ops );
        
      }    
      
//      color( "green", hash(pl_ops,"transp") )
//      { Plane( p012( oxyz), pl_ops );
//        Plane( p023( oxyz), pl_ops );
//        Plane( p013( oxyz), pl_ops );
//      }    
  }    
  
  pqr = randPts(3);
  //pqr = [[-1.8992, -2.12299, -1.62216], [1.25559, -0.48364, -1.36525], [-1.10772, -1.4157, 0.591315]];
//  pqr = [[-2.84926, -0.433825, 1.42911], [0.0140438, 1.59454, 1.47263], [0.313024, 0.256869, 0.98651]]; 
  // ERROR:
  // pqr= [[0.982085, -0.443367, -2.85914], [2.66226, 2.26935, 0.904825], [2.38218, 2.66048, 0.311629]]
  echo("pqr", pqr);
  
  P= pqr[0]; 
  Q= pqr[1];
  R= pqr[2];
  N= onlinePt( [Q, N(pqr)]  , len= 3 );
  M= onlinePt( [Q, N([N,Q,P])], len= 3 );

 
  O = Q;
  X = P;
  Y = M;
  Z = N;
  // New coordinate: 0:Q, x:P, y:M, z:N
  MarkPts( [O,X,Y,Z], ["labels","OPQR"] );
  
  
  // ==============================
    
  xyzo_0 = [O,X,Y,Z];

  //AxisLines( xyzo_0 );

  // ==============================

  xyzo_1 = [ for(p=xyzo_0) p+(ORIGIN-O)];
      
  AxisLines( xyzo_1,plops=["color","green"]);
  
  MarkPts( xyzo_1, ["labels","OXYZ"] );
  
  Arrow( [ O, xyzo_1[0] ]
       , ["color","red", "arrow",["r",0.15,"len",0.4], "arrowP",false ] );
  
  //========================    

  A = [ norm(xyzo_1[1]), 0, 0];
  MarkPts([A], ["labels","A"] );
  
  apts = arcPts( [A,ORIGIN, xyzo_1[1]],r=A.x, count=20 ); 
  Line0( [ORIGIN,A] , ["r",0.005]);
  Chain( apts, ["closed",false, "r",0.01] );
    
  
  D = N( [A, ORIGIN, xyzo_1[1]], len=3);  
  YY = xyzo_1[2];
  ZZ = xyzo_1[3];
  
  T = othoPt( [ORIGIN, YY, D]);
  Tz= othoPt ( [ORIGIN, ZZ, D]);
  
  Line0( [YY,T], ["r",0.03] );
  Mark90( [YY,T,ORIGIN] ); 
  MarkPts( [D,T], ["labels", "DT"] );
  Line0( linePts( [ORIGIN, D], len=[5,5]), ["r", 0.03,"color","red"]); 
  
  //---------------------------------------
  a_xoa = angle([ xyzo_1[1], ORIGIN, A]);
  
  B= anyAnglePt( [ORIGIN, T, YY]
                , a=a_xoa, len= norm(T-YY), pl="yz"
                ); 
  MarkPts( [B], ["labels","B"] );              
  Chain( [ORIGIN, B,T], ["closed",false, "r",0.03] );              

  Line0( [ORIGIN,B] , ["r",0.01]);
  aptsY = arcPts( [B,T, YY],r= norm(B-T) ); 
  echo("aptsY", aptsY);
  Chain( aptsY, ["closed",false,"r",0.01] );
  
//---------------------------------------
  a_xoa = angle([ xyzo_1[1], ORIGIN, A]);
  
  C= anyAnglePt( [ORIGIN, Tz, ZZ]
                , a=-a_xoa, len= norm(Tz-ZZ), pl="yz"
                ); 
  MarkPts( [C], ["labels","C"] );              
  Chain( [ORIGIN, C,Tz], ["closed",false, "r",0.03] );              

  AxisLines( [ORIGIN, A,B,C] );
}
//Text_dev();





