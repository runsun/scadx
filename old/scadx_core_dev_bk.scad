
/* 
=================================================================
                    scadx -- an OpenScad lib.
=================================================================

-- scadx_core.scad
-- Function doc and Test Tools 
III. Shape

Naming convention: 

    arr : array
    str : string
    int : integer

    pq  : [P,Q]
    pqr : [P,Q,R]

    v,pt: a vector, a point 
    pl  : a plane
    pts : many points 

    2-pointer: pq, st
    3-pointer: pqr, stu
    4-pointer: pqrs

    Single capital letters: points/vector (P=[2,5,0]);

    aPQR: angle of P-Q-R
   
    function: starts with lower case (angle(), hash(...)) 
    module for object: starts with capitals (Line, MarkPts)

=================================================================
*/ 

GAPFILLER = 0.0001; // used to extend len to cover gap
ORIGIN = [0,0,0];
$fn    = 12; 
ZERO   = 1e-13;     // used to chk a value that's supposed to be 0: abs(v)<zero
IM     = [[1,0,0],[0,1,0],[0,0,1]]; //Identity matrix (3x3)
PI     = 3.14159265358979323846264338327950;
COLORS = ["red","green","blue","purple","brown","khaki"];   // default colors

A_Z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
a_z = "abcdefghijklmnopqrstuvwxyz";
A_z = str(A_Z,a_z);
A_zu = str(A_z,"_");
0_9 = "0123456789";
0_9d= str(0_9,".");


CUBEFACES=[ [0,1,2,3], [4,5,6,7]
		  , [0,1,5,4], [1,2,6,5]
		  , [2,3,7,6], [3,0,4,7]  
		  ];  
          
/*
   Functions available to calc(). Note: to add functions, add name here,
   and add it's action inside calc().
*/
CALC_FUNCS= [
    "abs", "sign", "sin", "cos", "tan", "acos", "asin", "atan", "atan2", "floor", "round", "ceil", "ln"
    //, "len", "let"
    , "log", "pow", "sqrt", "exp", "rands"
    , "min", "max"
    ];

CALC_OPS= ["^","*","/","+","-"];

CALC_BLOCKS = joinarr(concat( [["(",")"]]
                    ,  [for(f=CALC_FUNCS) [ str(f,"("), ")"] ] ));
    

// A common option for shapes
OPS=[ "r" , 0.1
	, "h", 1
	, "w", 1
	, "len",1 
	, "color", undef 
	, "transp", 1
	, "fn", $fn
	, "rotate", 0
	, "rotates", [0,0,0]
	, "translate", [0,0,0]	 
	];
 
scadx_categories=
[
	  "Array"
	, "Doctest"
	, "Geometry"
	, "Hash"
	, "Index"
	, "Inspect"
	, "Math"
	, "Number"
	, "Point"
	, "2D_Object"
	, "3D_Object"
	, "String"
	, "Type"
];

/*
=================================================================
               toolets
=================================================================
*/

    // Convert a 2-pointer pq to a vector
    function p2v(pq)= pq[1]-pq[0];  

    function pw(x)= pow(x,2);
    function pqr()= randPts(3);
                 
    function dx(pq)= pq[1].x - pq[0].x;  
    function dy(pq)= pq[1].y - pq[0].y;
    function dz(pq)= pq[1].z - pq[0].z;

    function d01(pqr)= norm(pqr[1]-pqr[0]); // dist btw p and q 
    function d12(pqr)= norm(pqr[2]-pqr[1]); 
    function d02(pqr)= norm(pqr[2]-pqr[0]);

    // square of dist btw p and q 
    function d01pow2(pqr)= pow(norm(pqr[1]-pqr[0]),2);  
    function d12pow2(pqr)= pow(norm(pqr[2]-pqr[1]),2);
    function d02pow2(pqr)= pow(norm(pqr[2]-pqr[0]),2);

    function minx(pq)= min(pq[0][0],pq[1][0]); // smallest x btw p and q 
    function miny(pq)= min(pq[0][1],pq[1][1]);
    function minz(pq)= min(pq[0][2],pq[1][2]);
    function maxx(pq)= max(pq[0][0],pq[1][0]); // largest x btw p and q
    function maxy(pq)= max(pq[0][1],pq[1][1]);
    function maxz(pq)= max(pq[0][2],pq[1][2]);

    function px00(p)=[ p[0],0,0];  // for pt [x,y,z] => [x,0,0]
    function p0y0(p)=[ 0, p[1],0 ];
    function p00z(p)=[ 0,0, p[2]];
    function pxy0(p)=[ p[0],p[1],0]; // for pt [x,y,z] => [x,y,0]
    function p0yz(p)=[ 0, p[1],p[2] ];
    function px0z(p)=[ p[0],0, p[2]];

    function p01(pts)= [pts[0], pts[1]];  // pick pt-0, pt-1 from pts 
    function p10(pts)= [pts[1], pts[0]];
    function p02(pts)= [pts[0], pts[2]];
    function p20(pts)= [pts[2], pts[0]];
    function p03(pts)= [pts[0], pts[3]];
    function p30(pts)= [pts[3], pts[0]];
    function p12(pts)= [pts[1], pts[2]];
    function p21(pts)= [pts[2], pts[1]];
    function p13(pts)= [pts[1], pts[3]];
    function p31(pts)= [pts[3], pts[1]];
    function p23(pts)= [pts[2], pts[3]];
    function p32(pts)= [pts[3], pts[2]];

    function p012(pts)= [pts[0],pts[1],pts[2]];
    function p013(pts)= [pts[0],pts[1],pts[3]];
    function p021(pts)= [pts[0],pts[2],pts[1]];
    function p023(pts)= [pts[0],pts[2],pts[3]];
    function p031(pts)= [pts[0],pts[3],pts[1]];
    function p032(pts)= [pts[0],pts[3],pts[2]];

    function p102(pts)= [pts[1],pts[0],pts[2]];
    function p103(pts)= [pts[1],pts[0],pts[3]];
    function p120(pts)= [pts[1],pts[2],pts[0]];
    function p123(pts)= [pts[1],pts[2],pts[3]];
    function p130(pts)= [pts[1],pts[3],pts[0]];
    function p132(pts)= [pts[1],pts[3],pts[2]];

    function p201(pts)= [pts[2],pts[0],pts[1]];
    function p203(pts)= [pts[2],pts[0],pts[3]];
    function p210(pts)= [pts[2],pts[1],pts[0]];
    function p213(pts)= [pts[2],pts[1],pts[3]];
    function p230(pts)= [pts[2],pts[3],pts[0]];
    function p231(pts)= [pts[2],pts[3],pts[1]];

    function p301(pts)= [pts[3],pts[0],pts[1]];
    function p302(pts)= [pts[3],pts[0],pts[2]];
    function p310(pts)= [pts[3],pts[1],pts[0]];
    function p312(pts)= [pts[3],pts[1],pts[2]];
    function p320(pts)= [pts[3],pts[2],pts[0]];
    function p321(pts)= [pts[3],pts[2],pts[1]];


    function pp2(pts,i,j)= [pts[i], pts[j]];    // pick 2 points 
    function pp3(pts,i,j,k)= [pts[i], pts[j], pts[k]];
    function pp4(pts,i,j,k,l)= [pts[i], pts[j], pts[k], pts[l]];

    function newx(pt,x)= [x, pt.y, pt.z];
    function newy(pt,y)= [pt.x, y, pt.z];
    function newz(pt,z)= [pt.x, pt.y, z];

    // product of difference, used in lineCrossPt
    function podPt(P,Q,R,S)=[ (Q-P).x * (S-R).x   
                            , (Q-P).y * (S-R).y
                            , (Q-P).z * (S-R).z ];
                            
    function P(pqr) = pqr[0];
    function Q(pqr) = pqr[1];
    function R(pqr) = pqr[2];
    function N(pqr,len=1,reverse=false) = normalPt(pqr,len=len,reverse=reverse);
    function N2(pqr,len=1,reverse=false) = 
            normalPt( [N(pqr), Q(pqr), P(pqr) ],len=len,reverse=reverse);

/*
=================================================================
               Error Handling
=================================================================
*/

ERRMSG = [
  "rangeError"
    , "<br/>|  Index {iname}={index} out of range of {vname} (len={len})"
 ,"typeError"
    , "<br/>|  {vname} should be {tname}. A {vtype} given ({val}) "
 ,"arrTypeError"
    , "<br/>|  Array {vname} does NOT allow item of type {tname}. ({vname}: {val})"
];
function errmsg(errname,data)=
let ( _v= str(hash(data,"val"))
    , v = len(_v)>30? str( slice(_v,0, 30),"..."): _v
    , _data = update( data , ["val", v] )
    , data = [ for(i=range(_data))
                isint(i/2)? _data[i]: str("<u>", _data[i], "</u>") ] 
    )
(
  _red(
         str( 
              "  [<u>", errname, _h("</u>] in {fname}(): ",data)
            , _h( hash(ERRMSG, errname), data)
            )
      )
);
    
function typeErr(data)= errmsg("typeError", data);      
function rangeErr(data)= errmsg("typeError", data);      
function arrTypeErr(data)= errmsg("arrTypeError", data);      

/*
=================================================================
               Argument design pattern
=================================================================

1. Undef arg with default val

    module obj( ops=[] )
    {
        ops= update( [ "r", 3 ], ops );
    }

2. Two or more related SIMPLE args, set it at once or seperate

    module obj( ops=[] )
    {
        ops = update( ["r", 3], ops );
        function ops(k,df) = hash(ops, k, df);
        r = ops("r");
        rP= ops("rP", r);  // if rP undef, use r
        rQ= ops("rQ", r);  // if rQ undef, use r
        ...
    }
    
3. ONE complex arg

    module obj( ops=[] )
    {
        u_ops  = ops;
        df_ops = [...];
        df_head = ["r",2];
        
        ops = update( df_ops, u_ops );
        function ops(k,df) = hash(ops, k, df);

        ops_head= update( df_head, ops("head",[]) ); 
        ...
    }    
               
4. two or more complex args 
   (example, heads in Arrow. Or Dim2() ):

    module obj( ops=[] )
    {
        u_ops = ops;
        df_ops= [ "r",0.02
                , "heads",true
                , "headP",true
                , "headQ",true
                ];
        df_heads= ["r", 0.5, "len", 1];  
        df_headP= [];
        df_headQ= [];
                
        ops = update( df_ops, u_ops );
        function ops(k,df)= hash(ops,k,df);
        
        com_keys = [ "color","transp","fn"];
        
        ops_headP= getsubops( ops=u_ops, df_ops=df_ops
                    , com_keys= com_keys;
                    , sub_dfs= ["heads", df_heads, "headP", df_headP ]
                    );
        ops_headQ= getsubops( ops=u_ops, df_ops=df_ops
                    , com_keys=com_keys;
                    , sub_dfs= ["heads", df_heads, "headQ", df_headQ ]
                    );
            
        function ops_headP(k) = hash( ops_headP,k );
        function ops_headQ(k) = hash( ops_headQ,k ); 
    }
    
    NOTE:
        
        For multiple layer sub-units, like:
        
        body - arm - finger
        
        An each approach is to allow setting, for example, fingers like:
        
        obj( ["arms", ["finger", ["count",3]]] ) 
        
=================================================================

*/

module ArgumentPatternDemo_simple()
{
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= concat( ops
            , [ "color", "blue" ]  // <== set default here
            , OPS );
        function ops(k,df) = hash(ops,k,df);
        color = ops("color");
        len   = ops("len", 2); // <== or here. Note that len doesn't
                               //     need to be defined in ops
        translate(transl) color( ops("color"), 0.4) 
        cube(x=0.5,y=0.5,z=1 );
    }  
    Obj();    
    Obj( ["color","red"], [0,1,0] );
}    

module ArgumentPatternDemo_multiple_simples()
{
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= concat( ops
            , [ "limlen", 2
              ], OPS );
        function ops(k,df) = hash(ops,k,df);
        lim = ops("limlen");
        arm = ops("armlen",lim);
        leg = ops("leglen",lim); 
        
        //echo("ll, arm,leg", ll, arm, leg);
        translate(transl)
        color( ops("color"), 0.4) { 
            cube( size=[0.5,1,2] );
            translate( [0,1, 1]) cube( size=[0.5,arm,0.5] );
            translate( [0,0.5, -leg]) cube( size=[0.5,0.5,leg] );
        }    
    }  
    Obj();    
    Obj( ["limlen",1, "color","red"], [2,0,0] );
    Obj( ["armlen",0.5, "color","blue"], [4,0,0] );
    LabelPt( [0, 0, 2], "Obj()=> both lims= default 2" );
    LabelPt( [2, 1, 1.5], "Obj([\"limlen\",1])=> both lims=1",["color","red"] );
    LabelPt( [4, 1, 1.5], "Obj([\"armlen\",0.5])=> arm len=0.5", ["color","blue"] ); 
}    

module ArgumentPatternDemo_complex()
{_mdoc("ArgumentPatternDemo_complex","");
  
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= update( ["arm", true]
                    ,ops);
        function ops(k,df)= hash(ops,k,df);

        df_arm=["len",3];
        
        ops_arm= getsubops( ops
                    , com_keys= ["transp"]
                    , sub_dfs= ["arm", df_arm ]
                    );
        function ops_arm(k,df) = hash(ops_arm,k,df);
        
        echo("ops_arm",ops_arm);
        
        translate(transl) color(ops("color"), ops("transp"))
        cube( size=[0.5,1,2] );
        if ( ops_arm )
        {   translate( transl+[0,1, 1]) 
            color(ops_arm("color"), ops_arm("transp"))
            cube( size=[0.5,ops_arm("len"),0.5] );
        }   
    }  
    Obj();    
    Dim( [ [0,4,1.5], [0,1,1.5], [2,1,1.5]] );
    Obj( ["arm",false], [1.5,0,0] );
    Obj( ["arm",["len",2]], [3,0,0] );
    Obj( ["arm",["color","red"]], [4.5,0,0] );
    Obj( ["color", "green"], [6,0,0] );
    Obj( ["transp", 0.5], [7.5,0,0] );
    
    LabelPt( [0.5,4.2,1.5], "1.Obj()");
    LabelPt( [1.8,3,1.2], "2.Obj([\"arm\",false])");
    LabelPt( [3.6,3.5,1.5], "3.Obj([\"arm\",[\"len\",2]])");
    LabelPt( [4.8,4.2,1.5], "4.Obj([\"arm\",[\"color\",\"red\"]])",["color","red"]);
    LabelPt( [6.5,4.2,1.5], "5.Obj([\"color\",\"green\"])",["color","green"]);
    
    LabelPt( [8,4.2,1.5], "6.Obj([\"transp\",0.5])");
    
    LabelPt( [10, 0, 0], "Note #5,#6: #5 color set body color but not"
    ,["color","blue"]);
    LabelPt( [10, 0, -0.6], "arm color, but #6 set transp of both, "
    ,["color","blue"]);
    LabelPt( [10, 0, -1.2], "because transp is in com_keys "
    ,["color","blue"]);
    
}  


module ArgumentPatternDemo_multiple_complex()
{
  _mdoc("ArgumentPatternDemo_complice"
," 0: armL has transp=0.6 'cos  df_armL=[''transp'',0.6]
;; 1,2,4: no armL 'cos armL= false 
;; 2    : no arms 'cos arms= false
;; 3~7  : both len = 2 'cos arms=[''len'',2]
;; 5,6  : armL ops placed inside arms [] have no effect
;; 7    : armL ops should have one [] by its own 
;; 8    : set color on obj-level doesn't apply to arms but transp,
;;        does 'cos transp is in com_keys but color is not.
;; 9    : transp set on obj-level can be overwritten by that 
;;      : set in armL [].
");
    
    module Obj( ops=[], transl=[0,0,0] )
    {
        df_ops= ["arms", true
               , "armL", true
               , "armR", true
               ];
        com_keys = ["transp","color"];
        df_arms=["len",3];
        df_armL=["transp",0.6];
        df_armR=[];
        
        u_ops = ops;
        _ops= update(df_ops, u_ops); // Needed only if func ops() is used 
        function ops(k,df)= hash( _ops,k,df);
        
        function subops(sub_dfs)=
               getsubops( 
                      u_ops= u_ops  
                    , df_ops = df_ops
                    , com_keys= com_keys
                    , sub_dfs= sub_dfs
                    );
        ops_armL = subops(["arms", df_arms, "armL", df_armL ]);
        ops_armR = subops(["arms", df_arms, "armR", df_armR ] );
        
        function ops_armL(k,df) = hash(ops_armL,k,df);
        function ops_armR(k,df) = hash(ops_armR,k,df);

        //echo("");
        echo(args= ops );
        echo("ops", ops);
        echo("df_ops", df_ops);
        echo("ops_armL", ops_armL);
        echo("ops_armR", ops_armR);
        echo( ops_armL= getsubops_debug(ops  
                    , df_ops = df_ops
                    , com_keys= ["transp","color"]
                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]
                    )); 
        
        translate(transl) color(ops("color"), ops("transp"))
        cube( size=[0.5,1,2] );
        if ( ops_armR )
        {   
            translate( transl+[0,1, 1]) 
            color(ops_armR("color"), ops_armR("transp"))
            cube( size=[0.5,ops_armR("len"),0.5] );
        }    
        if ( ops_armL )
        {    translate( transl+[0,-ops_armL("len"), 1]) 
            color(ops_armL("color"), ops_armL("transp"))
            cube( size=[0.5,ops_armL("len"),0.5] );
            
        }   
    }  
    Obj();    
    //Dim( [ [0,4,1.5], [0,1,1.5], [2,1,1.5]] );
    
    all_ops= [
        ["arms",false]  // 1
        ,["armL",false]  // 2
        ,["arms",["len",2]] // 3
        ,["arms",["len",2], "armL", false] // 4
        ,["arms",["len",2, "armL", false]] // 5
        ,["arms",["len",2, "armL", ["color","red"]]] // 6
        ,["arms",["len",2], "armL", ["color","red", "len",1]] // 7
        ,["color","red", "transp", 0.3] // 8
        ,["transp", 0.5, "armL", ["transp", 1] ] // 9  
        ,["armR",["color", "blue"]]
        ,["armR",["len",1]]
    ];
    for(i=range(all_ops)){
        echo(i= str("<br/>===================== #",i+1, "========"));
        Obj( all_ops[i], [1.5*(i+1), 0, 0] );
        LabelPt( [(i+1)*1.5+0.3, 4.3, 1.5], str(i+1, " ops=", all_ops[i]) 
               , ["color", "blue"] );
    }
 
}  
//ArgumentPatternDemo_simple();
//ArgumentPatternDemo_multiple_simples();
//ArgumentPatternDemo_complex();
//ArgumentPatternDemo_multiple_complex();



//=========================================================
accumulate=[ "accumulate", "arr", "array", "Math" ,
" Given a numeric array [n0,n1,n2 ...], return an array 
;; [ n0, n0+n1, n0+n1+n2 ... ]
;;
;; ref: sum(arr) 
"];

function accumulate(arr)=
(
    [for (i=range(arr)) sum( slice(arr, 0,i+1))]
);


    
//========================================================
addx=[ "addx", "pt,x=0", "array", "Point"
, "Given a point or points (pt) and a x value, add x to pt.x.
;; When pt is a collection of points, add x to each pt if x is
;; a number, or add x[i] to pt[i].x if x is an array. That is, 
;; by setting x as array, we can add different values to each 
;; pt[i].x. 
;;
;;   addx([2,3,4],x=1) => [3,3,4]
;;   addx([[2,3],[5,6]],x=1) => [[3,3],[6,6]]
;;   addx([[2,3],[5,6]],x=[8,9]) => [[10,3],[14,6]]
;;
;; Note that addz() - unlike addx, addy - expands a 2D pt to 3D.
;;
;; ref: addy, addz, app(), appi()
"];

function addx(pt,x=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addx(pt[i], isarr(x)?x[i]:x) ]
	: len(pt)==3
       ? [ pt.x+x,pt.y,pt.z ] 
	   : [ pt.x+x,pt.y]  
);
    
//========================================================
addy=[ "addy", "pt,y=0", "array", "Point"
, "Add y to pt. See doc in addx
;;
;; ref: addx, addz, app(), appi()
"
];
    
function addy(pt,y=0,_I_=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addy(pt[i], isarr(y)?y[i]:y) ]
	: len(pt)==3
       ? [ pt.x,pt.y+y,pt.z ] 
	   : [ pt.x,pt.y+y] 
);

    
//========================================================
addz=[ "addz", "pt,z=0", "array", "Point"
, "Add z to pt. See doc in addx.
;;
;; ref: addx, addy, app(), appi()
"
];
    
function addz(pt,z=0,_I_=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addz(pt[i], isarr(z)?z[i]:z) ]
	: len(pt)==3
       ? [ pt.x,pt.y,pt.z+z ] 
	   : [ pt.x,pt.y,z] 
);    

    
//================================================================
all=["all", "arr,isdefined=false,item=undef)","T|F","Core, Inspect",
"Return true if all items in *arr* is:
;;  (1) ==item (when isdefined=true), or
;;  (2) treated as true (when isdefined=false)
"];
function all(arr,isdefined=false,item=undef)=
(
    isdefined
    ?(len([ for(x=arr) if(x==item) 1])==len(arr))
    :(len([ for(x=arr) if(x) 1])==len(arr))
);

//========================================================
angle=["angle","pqr,refpt=undef, lineHasToMeet=true","number","Angle",
 " Given a 3-pointer (pqr), return the angle of P-Q-R. 
;;    
;; 2015.3.3: new arg *refpt* : a point given to decide the sign of 
;;  angle. If the refpt is on the same side of normalPt, the angle
;;  is positive. Otherwise, negative. 
;;     
;; 2015.3.8: 
;;    
;;  Allows for angle( [line1,line2]). If lineHasToMeet(default), 2 lines 
;;  must have a lineCrossPt. If lineHasOtMeet=false , then the angle of 
;;  both lines' unit vectors is returned. 
;;
;; ref: angle, angleBisectPt, anglePt, anyAnglePt, incenterPt, intersectPt, is90, Mark90
"
];


function angle(pqr, refPt=undef, lineHasToMeet=false)=   
 let(    
    arelines = len(pqr)==2
    ,itcp = arelines? lineCrossPt( [pqr[0], pqr[1]] ): undef
    ,
     pqr = arelines? // two lines
           ( lineHasToMeet? 
             [ itcp== pqr[0][0]? pqr[0][1]:pqr[0][0]
              , itcp
              , itcp==pqr[1][1]? pqr[1][0]:pqr[1][1] 
             ]
            : [ uv(pqr[0]), ORIGIN, uv(pqr[1])] // conv to unit vectors.
           ):pqr
    , P=pqr[0], Q=pqr[1], R=pqr[2] 
    , a= acos( (( R-Q)*( P- Q) ) / d01(pqr) / d12(pqr) )
    , np = normalPt( pqr )
    , sign= refPt==undef?1: isSameSide( [np, refPt], pqr)?1:-1
    )
(
	sign*a
);


// Use atan instead of acos --- need to test if this is better 2015.3.5
function angle_atan(pqr, refPt=undef)=
 let( P=pqr[0], Q=pqr[1], R=pqr[2]
    , D = othoPt(sel(pqr,[0,2,1]))
    , PD = norm(P-D)
    , RD = norm(R-D)
    , QD = norm(Q-D)
    , _a= atan( RD/QD ) //acos( (( R-Q)*( P- Q) ) / d01(pqr) / d12(pqr) )
    , a = PD>QD && PD> norm(P-Q)? (180-_a):_a
    , np = normalPt( pqr )
    , sign= refPt==undef?1: isSameSide( [np, refPt], pqr)?1:-1
    )
(
	sign* a
);

//function angle_lnpl( [pq, pqr], refPt=undef)=
//
//function ang(pts,refPt=undef)=
// let( 
//      
// )
//(
//    countArr(pts)==0? angle(pts)
//    
//);

//function angle2( 

//==================================================
angle_series=["angle_series","pqr, prec=8","n", "Angle, Math",
" angle of pqr calculated with the approaching series algorithm 
;; (that is, no triagonometry; or, triagonometry in disguise) that 
;; can specify precision. The arg *prec* determines the number of
;; items in the K series below. The larger the angle is, the 
;; larger prec is needed. 
;; http://mathforum.org/library/drmath/view/55341.htm
;; 
;; Calc angle A, 3 sides = a,b,c
;;  
;;   x = (b^2+c^2-a^2)/2bc
;;   K = (1/2)x^3/3 + (1/2)(3/4)x^5/5 + (1/2)(3/4)(5/6)x^7/7 ...  
;; 
;;       1 x^3   1 3 x^5    1 3 5 x^7
;;     = ----- + -------- + --------- + ....
;;       2  3    2 4  5     2 4 6  7 
;;        
;;                 g x^( 2(i+1)+1 )
;;    each item = -----------------
;;                 h ( 2(i+1)+1 )
;;        
;;       g = range( 1,2, 2(i+1)+1 )
;;       h = range( 2,2, 2(i+1)+1 )
;;      
;;    ==> j = 2(i+1)+1
;;  
;;                   prod( range(1,2,j) ) x^j
;;    K = series of -------------------------
;;                   prod( range(2,2,j) )  j
;;        
;;    A = ( Pi/2 - x - sum(K) ) * 180/PI         
"];

/* calc angle w/o trigonometry

  http://mathforum.org/library/drmath/view/55341.htm

  Calc angle A, 3 sides = a,b,c

  x = (b^2+c^2-a^2)/2bc
  K = (1/2)x^3/3 + (1/2)(3/4)x^5/5 + (1/2)(3/4)(5/6)x^7/7 ...  

         1  x^3      1 3 x^5      1 3 5 x^7
    =   --------  +  --------  +  ----------- + ....
         2   3       2 4 5        2 4 6  7 
       
                       g x^( 2(i+1)+1 )
   =  (sign(-i+0.5))  -----------------
                       h ( 2(i+1)+1 )
       
      g = range( 1,2, 2(i+1)+1 )
      h = range( 2,2, 2(i+1)+1 )
     
   ==> j = 2(i+1)+1
 
                  prod( range(1,2,j) ) x^j
   K = series of -------------------------
                  prod( range(2,2,j) )  j
       
  A = Pi/2 - x - K
      
       
  A is a radian. Multiply to degrees by x180/PI
*/ 

function angle_series(pqr, prec=6)=   // prec = len(K)
    let ( a= d02(pqr) // PR
        , b= d01(pqr) // PQ
        , c= d12(pqr) // QR
        , x= (pow(b,2)+pow(c,2)-pow(a,2))/ (2*b*c)
        , K = [ for(i=range(prec))
                  let( j = 2*i+3     
                     , g = prod( range(1,2,j) )
                     , h = prod( range(2,2,j) )
                     )
                   g * pow(x,j) / h / j
              ] 
        )          
(
   (PI/2-x-sum(K))*180/PI
);
        
//module angle_series_test(ops=["mode",13])
//{
//   _pqr= randPts(3);
//   echo( _pqr=_pqr );
//   P=P(_pqr); Q=Q(_pqr);
//    
//   //angs = [10,30,45,60,90,120,150, 170];
//   _angs= [for(i=range(10)) rand( 1,180 )];
//   angs = quicksort(_angs);
//   
//   Rs = [ for(a=angs) anglePt(_pqr, a=a) ]; 
//   precs= [5,10,20,40,60,"-"]; 
//   recs = [ for( i=range(Rs),p=precs )
//            let( a=angs[i], R=Rs[i], pqr=[P,Q,R] 
//               , ang= p!="-"?angle(pqr):0
//               , angcor = p!="-"?angle_cordic(pqr, prec=p):0
//               ) 
//            p=="-"? "//-----------------------"
//            :_s("a={_},p={_}, [angle,angle_cordic, diff] = [{_}, {_}, {_}]"
//              , [a,p,ang,angcor, ang-angcor]
//              )
//          ];   
//   doctest(angle_series,
//    recs, ops );
//}
/*
|a=10,p=5, [angle_acos,angle_cordic] = [10, 15.5757]
|a=10,p=10, [angle_acos,angle_cordic] = [10, 12.848]
|a=10,p=20, [angle_acos,angle_cordic] = [10, 11.1381]
|a=10,p=40, [angle_acos,angle_cordic] = [10, 10.3057]
|a=10,p=60, [angle_acos,angle_cordic] = [10, 10.1055]
|//-----------------------
|a=30,p=5, [angle_acos,angle_cordic] = [30, 30.4097]
|a=30,p=10, [angle_acos,angle_cordic] = [30, 30.0467]
|a=30,p=20, [angle_acos,angle_cordic] = [30, 30.0011]
|a=30,p=40, [angle_acos,angle_cordic] = [30, 30]
|a=30,p=60, [angle_acos,angle_cordic] = [30, 30]
|//-----------------------
|a=45,p=5, [angle_acos,angle_cordic] = [45, 45.0186]
|a=45,p=10, [angle_acos,angle_cordic] = [45, 45.0003]
|a=45,p=20, [angle_acos,angle_cordic] = [45, 45]
|a=45,p=40, [angle_acos,angle_cordic] = [45, 45]
|a=45,p=60, [angle_acos,angle_cordic] = [45, 45]
|//-----------------------
|a=60,p=5, [angle_acos,angle_cordic] = [60, 60.0002]
|a=60,p=10, [angle_acos,angle_cordic] = [60, 60]
|a=60,p=20, [angle_acos,angle_cordic] = [60, 60]
|a=60,p=40, [angle_acos,angle_cordic] = [60, 60]
|a=60,p=60, [angle_acos,angle_cordic] = [60, 60]
|//-----------------------
|a=90,p=5, [angle_acos,angle_cordic] = [90, 90]
|a=90,p=10, [angle_acos,angle_cordic] = [90, 90]
|a=90,p=20, [angle_acos,angle_cordic] = [90, 90]
|a=90,p=40, [angle_acos,angle_cordic] = [90, 90]
|a=90,p=60, [angle_acos,angle_cordic] = [90, 90]
|//-----------------------
|a=120,p=5, [angle_acos,angle_cordic] = [120, 120]
|a=120,p=10, [angle_acos,angle_cordic] = [120, 120]
|a=120,p=20, [angle_acos,angle_cordic] = [120, 120]
|a=120,p=40, [angle_acos,angle_cordic] = [120, 120]
|a=120,p=60, [angle_acos,angle_cordic] = [120, 120]
|//-----------------------
|a=150,p=5, [angle_acos,angle_cordic] = [150, 149.59]
|a=150,p=10, [angle_acos,angle_cordic] = [150, 149.953]
|a=150,p=20, [angle_acos,angle_cordic] = [150, 149.999]
|a=150,p=40, [angle_acos,angle_cordic] = [150, 150]
|a=150,p=60, [angle_acos,angle_cordic] = [150, 150]
|//-----------------------
|a=170,p=5, [angle_acos,angle_cordic] = [170, 164.424]
|a=170,p=10, [angle_acos,angle_cordic] = [170, 167.152]
|a=170,p=20, [angle_acos,angle_cordic] = [170, 168.862]
|a=170,p=40, [angle_acos,angle_cordic] = [170, 169.694]
|a=170,p=60, [angle_acos,angle_cordic] = [170, 169.895]
*/

//========================================================

function angleBisectPt( pqr, len=undef, ratio=undef )= 
(
    let( Q= pqr[1]
        ,D= onlinePt( p02(pqr) //: ln_PR
            , ratio= norm(pqr[0]-pqr[1]) //: d_PR
                    /(norm(pqr[0]-pqr[1]) //: d_PR + d_RQ
                     +norm(pqr[1]-pqr[2])) 
                   )
     )
    len!=undef? onlinePt( [Q,D], len=len )
        :ratio!=undef? onlinePt( [Q,D], ratio=ratio)
        :D
);

function anglePt( pqr, a=undef, an=0, len=undef, ratio=1)=
(    
/*     2015.5.7: new arg: an (angle to the N (normalPt))
       
       B-----------_R-----A 
       |         _:  :    |
       |  T._---:-----:---+ S 
       |  |  '-:_      :  | 
       |  |   :  '-._   : |
       |  |  :     a '-._:| 
       p--+--+-----------'Q
          U  D    


       B----_R-----A--._ 
       |      :    |    ''-_
       |       : S +--------T 
       |        :  |      -'|
       |         : |   _-'  |
       |          :|_-'     |
       p----------'Q--------U

            a = a_PQT
*/
  let( a= a==undef? angle(pqr):a
     , L= ratio* (len?len:dist( p01(pqr)) )
     , Q= Q(pqr)
     , QA= [ Q, squarePts( pqr )[2] ]
     , QP= p10(pqr)
     , pt= abs(a)==90? // if a=90, return S
            onlinePt( QA, len= sign(a)*L )
          :abs(a)==270? // if a=270
            onlinePt( QA, len= -sign(a)*L )
          :squarePts(
            [ onlinePt( QP, len= L*cos(a) ) //: U
            , Q
            , onlinePt( QA, len= L*sin(a) ) //: S	
            ])[3]
     )
  a2? anglePt( [pt, Q, N(pqr)],a=a2,len=L):pt
);


    
//==============================================

//function anglePt( pqr, a, len=1)=
//(
///*
//       B-----------_R-----A 
//       |         _:  :    |
//       |  T._---:-----:---+ S 
//       |  |  '-:_      :  | 
//       |  |   :  '-._   : |
//       |  |  :     a '-._:| 
//       p--+--+-----------'Q
//          U  D    
//
//
//       B----_R-----A--._ 
//       |      :    |    ''-_
//       |       : S +--------T 
//       |        :  |      -'|
//       |         : |   _-'  |
//       |          :|_-'     |
//       p----------'Q--------U
//
//            a = a_PQT
//*/
//	abs(a)==90? // if a=90, return S
//		onlinePt( [ Q(pqr), squarePts( pqr )[2] ]  // [Q,A]
//			    , len= sign(a)*len
//				)
//	:abs(a)==270? // if a=270
//		onlinePt( [ Q(pqr), squarePts( pqr )[2] ]  // [Q,A]
//			    , len= -sign(a)*len
//				)
//	:squarePts(
//	[ 
//		// U
//		onlinePt( p10(pqr) // [Q,P] // squarePts( pqr )[2] ]  // [Q,A]
//		    , len= len*cos(a)
//			)
//		// Q	
//		, Q(pqr)
//
//		// S
//		,onlinePt( [ Q(pqr), squarePts( pqr )[2] ]  // [Q,A]
//		    , len= len*sin(a)
//			)	
//	])[3]
//
//);



//==============================================

function anyAnglePt( pqr, a, len=1, pl=undef)=  // pqn, pqr, qnn, qrn
(
	anglePt
	( 
	   pl=="yz"||pl=="mn"?          [ N2(pqr,len=-1), Q(pqr), N(pqr) ]
	 : pl=="xy"||pl=="pm"||pl=="pr"? pqr //[ P(pqr), Q(pqr), N2(pqr,reverse=true) ]
	 : pl=="xz"||pl=="pn"?          [ P(pqr), Q(pqr), N(pqr) ]
	 :                              [ R(pqr), Q(pqr), N(pqr) ]
	, a=a, len=len
	)
);


//==========================================================

function app(arr,x)= concat(arr,[x]);

//==========================================================

function appi(arr, i, x, conv=false)=
    let( xi = get(arr,i)
       , xx= isarr(xi)?xi:[xi] 
       , nx= concat( xx, [x] )
       )
    (   
      !conv && !isarr(xi)
       ? typeErr( [ "vname", _s("arr[{_}]",[i])
                  , "fname", "appi"
                  , "tname", "arr"
                  , "vtype", type(xi)
                  , "val"  , xi ] )            
      : [ for(_i=range(arr)) 
            _i==fidx(arr,i) ? nx : arr[_i] ]
    );

// =========================================================

function arcPts( 
    pqr=randPts(3)
    , a=undef
    , r=1
    , count=6
    , pl="xy"
    , cycle=undef
    , twist = 0 
    , pitch=0)=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, _a= a==undef?angle(pqr):a
	, a = cycle?cycle*360:_a 
	, pl= pitch?"yz":pl 
	)
(
	[ for(i=range(count)) 
		anyAnglePt( [ P
					, onlinePt([Q,P],len= i*pitch/count)
					, R
				 	] , a= twist+ i*a/(count-(a==360?0:1))
				  , len=r, pl=pl) ]
);

//=====================================================

function arrblock(arr, indent="  "
					, v="| "
					, t="-"
					, b="-"
					, lt="."
					, lb="'"
					, lnum=undef
					, lnumdot=". "					 
)=
(
  concat( [ str( indent, lt,repeat(t, 45) ) ]
		, isint(lnum)
		  ? [ for( i=range(arr) ) str( indent, v, i+lnum, lnumdot, arr[i]) ]
		  : [ for( i=range(arr) ) str( indent, v,                , arr[i]) ]
		, [ str( indent, lb,repeat(b, 45) ) ]
  ) 
);

//========================================================
          
function begmatch( s, ptns )= // ptn = [ [name, w0,w] ... ]
(
    let( rtn=[ for(p=ptns) 
               let( m= begword(s, w0=p[1], w=p[2], we=p[3]))
               if(m) [p[0],m]
             ]
       )
   rtn?rtn[0]:undef
);
               

//========================================================

function begwith(o,x)= 
(
	x==""||x==[]
	? undef
	: isarr(x)
	  ? [ for(i=range(x)) if(index(o,x[i])==0) x[i] ] [0]
	  : index(o,x)==0
);



// ========================================================

function begword_(
      s
    , isnum = false
    , wb= "" //str(a_z, A_Z, "_") // df letter for s[0]
    , w = "" //str(a_z, A_Z, "_", "1234567890") // df letters
    , we= "" 
    , dwb= str(a_z, A_Z, "_")
    , dw= str(a_z, A_Z, "_", "1234567890")  
    ) =
   let( _w0= isstr(wb)
      , _w = w==undef?str(a_z, A_Z, "_", "1234567890"):w // df chars
      //, _we= we //we==undef?str(a_z, A_Z, "_", "1234567890"):we // df chars
      , w0 = isnum? "1234567890.": _w0 
      , w  = isnum? "1234567890.": _w 
      //, w  = isnum? "": _w 
      , s0 = has(w0, s[0])?s[0]:false
      , _i = [for(i=range(s)) // index of the 1st unmatch
                let( m= has( i==0?w0:w, s[i]) )
                if((!m) && ( we==undef? true
                             : has(we,s[i])
                           )
                  ) i ][0] 
      ,rtn = _i==0?"": !_i? s
             :( !s0?"" 
                : _i==undef? s0
                : slice( s, 0, we&&_i?(_i+1):_i) 
              )
//      ,_debug= _fmth([ "s=",s
//                , "_i=",_i
//                , "w0",w0
//                , "w", w
//                , "we",we
//                , "rtn=", rtn ])     
      )
(
   //_debug //join(_debug ,"<br/>")
   rtn 
);
       
//=============================================================
    
function begword(
      s
    , isnum = false
    , w0= undef //str(a_z, A_Z, "_") // df letter for s[0]
    , w = undef //str(a_z, A_Z, "_", "1234567890") // df letters
    , we= undef 
    ) =
   let( _w0= w0==undef?str(a_z, A_Z, "_"):w0 // df char for s[0]
      , _w = w==undef?str(a_z, A_Z, "_", "1234567890"):w // df chars
      //, _we= we //we==undef?str(a_z, A_Z, "_", "1234567890"):we // df chars
      , w0 = isnum? "1234567890.": _w0 
      , w  = isnum? "1234567890.": _w 
      //, w  = isnum? "": _w 
      , s0 = has(w0, s[0])?s[0]:false
      , _i = [for(i=range(s)) // index of the 1st unmatch
                let( m= has( i==0?w0:w, s[i]) )
                if((!m) && ( we==undef? true
                             : has(we,s[i])
                           )
                  ) i ][0] 
      ,rtn = _i==0?"": !_i? s
             :( !s0?"" 
                : _i==undef? s0
                : slice( s, 0, we&&_i?(_i+1):_i) 
              )
//      ,_debug= _fmth([ "s=",s
//                , "_i=",_i
//                , "w0",w0
//                , "w", w
//                , "we",we
//                , "rtn=", rtn ])     
      )
(
   //_debug //join(_debug ,"<br/>")
   rtn 
);
                
//echo( has("}","}"));
//echo( index("}","}"));
//echo( begword( "{abc}+123",w0="{" ) );               
//echo( begword( "{abc}+123",w0="{",we="}" ) );               
                
//function begword(
//      s
//    , isnum = false
//    , w0= undef //str(a_z, A_Z, "_") // df letter for s[0]
//    , w = undef //str(a_z, A_Z, "_", "1234567890") // df letters
//    ) =
//   let( _w0= w0==undef?str(a_z, A_Z, "_"):w0 // df char for s[0]
//      , _w = w==undef?str(a_z, A_Z, "_", "1234567890"):w // df chars
//      , w0 = isnum? "1234567890.": _w0 
//      , w  = isnum? "1234567890.": _w 
//      , s0 = has(w0, s[0])?s[0]:false
//      , _i = [for(i=range(s)) // index of the 1st unmatch
//                let(c = s[i]
//                   , m= index( i==0?w0:w, c)>=0 )
//                if(!m) i ][0] 
//      ,rtn = _i==0?"": !_i? s
//             :( !s0?"" : _i==undef? s0: slice( s, 0, _i) )
////      ,_debug= concat(  
////               "<br/>///"
////                , "s=",s
////                , "_i=",_i
////                , "rtn=", rtn )     
//      )
//(
//   //join(_debug ,"<br/>")
//   rtn 
//);
   



// ========================================================

function between( low, n, high, include=0 )=
(
  (
	(include==1 || include[0]) ? 
		( low<=n) : (low<n )
  )&&
  (
	(include==1 || include[1]) ? 
		( n<=high) : (n<high )
  )
);




//======================================================
 
function boxPts(pq, bySize=true)=
(
 	bySize?  
	  // q is edge lengths. Just replace pq below with [pq[0], pq[0]+pq[1]]
    [ [ minx([pq[0], pq[0]+pq[1]])
	  , miny([pq[0], pq[0]+pq[1]])
	  , minz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , miny([pq[0], pq[0]+pq[1]])
      , minz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , minz([pq[0], pq[0]+pq[1]]) ]
	, [ minx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , minz([pq[0], pq[0]+pq[1]]) ]
	, [ minx([pq[0], pq[0]+pq[1]])
      , miny([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , miny([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	, [ minx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	]
	: // we need min*, max* to "sort" the point coordinates
	[ [ minx(pq), miny(pq), minz(pq) ]
	, [ maxx(pq), miny(pq), minz(pq) ]
	, [ maxx(pq), maxy(pq), minz(pq) ]
	, [ minx(pq), maxy(pq), minz(pq) ]
	, [ minx(pq), miny(pq), maxz(pq) ]
	, [ maxx(pq), miny(pq), maxz(pq) ]
	, [ maxx(pq), maxy(pq), maxz(pq) ]
	, [ minx(pq), maxy(pq), maxz(pq) ]
	]
);

module boxPts_test( ops )
{
	scope=["pq",[[1,2,-1],[2,1,1]]];
	pq = hash(scope,"pq");

	doctest
	(
		boxPts
		,[
		  [	"pq", boxPts(pq), [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
							, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0]]
		  ]
		 ,[ "pq,true",boxPts(pq,true)
			, [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
			, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0]]      
		  ]
		]
		,ops, scope
	);
}


//boxPts_test();
//

//========================================================
calc= ["calc", "fml,scope", "val", "eval"
,"Calculate the value of the formula (fml) defined as a string. The
;; scope is the data given in hash format
;;
;;   calc( 'x+1',['x',3] )= 4
;;   calc( 'x*(y+2)',['x',3,'y',1] )= 9
;;
;; calc is done left to right, no matter what operator is:
;;
;;   calc( 'x*(y+2)^2',['x',3,'y',1] )= 81
;;   calc( 'x*((y+2)^2)',['x',3,'y',1] )= 27
;; 
;; 3x is interpretated as 3 * x
;;
;;   calc( '2x+1.5y',['x',3,'y',4] )= 12
;;
;; Net yet ready for 2(x+1).  
;;
;; Note: this is done by turning fml into an array w/ packuni(getuni(fml))
;; and is a relatively slow process, and using calc_basic() in process.
;; Most built-in functions are available to use. This is defined in 
;; calc_basic() and its calc is carried out by run(). 
;;
;;   calc( 'sin(x)^2+(cos(x)^2)',['x',30] )= 1
"];


function calc( fml, scope, isdebug=false, _debug=[] )=
    let( 
        _fml = isarr(fml)? fml
                 : packuni( 
                    getuni( fml, blk=CALC_BLOCKS, isdebug=isdebug ) 
                 )
        ,_debug= _fmth(
                concat( _debug
                      ,[ "<br/>calc",""
                       , "fml", fml
                       , "_fml", _fml
                       ]
                      ) ) 
       )
(
   countArr(_fml)==0
   ? calc_basic( _fml, scope=scope )
   : calc_basic( 
     [ for (a=_fml)
        isarr( a )?
            calc( a , scope=scope, isdebug=isdebug, _debug=_debug )
            : ( isdebug?_debug: a )
     ], scope=scope 
     )
);



//========================================================
calc_basic= ["calc_basic", "arr,scope", "val", "eval"
,"This is mainly used for calc(). Given arr representing an one-dimension, 
;; calculable array (containing no array), and scope given as a hash, calc 
;; the result.
;;
;; calc_basic( ['x','+',1], ['x',3] ) => 4 
;; calc_basic( ['x','^',2], ['x',3] ) => 9 
;;
;; Note that calc is done left to right, no matter what operator is:
;;
;; calc_basic( [2,'+','x','^',2], ['x',3] ) => 25 
;;
"];

function calc_basic( arr // arr is an array containing no arrs:
                         // ["x","+","2"], ["x"], ["2"] 
            , scope = ["x",2,"y",3] // variables must be single digits
            , isdebug=false
            , _opbuf= undef
            , _rtn  = undef
            , _sign = 1
            , _debug= []
            )=
    let( 
        ops=["*","/", "+","-","^"]   // Single digits
        ,funcs= CALC_FUNCS //["sin","ceil"] 
       , v0 = arr[0]  
     
       //--------------------------------
       // Set value. Anything other than num or var will 
       // set val = undef. But it's ok, 'cos we will set
       // conditions later to avoid using this undef
       , val= isnum(num(v0)) // If it's a num, convert to num
              ?_sign*num(v0) 
              :hash(scope,v0) // If it's var defined in scope, 
                                 // convert to value it defines.
                                 //// If not def in scope, return itself.
       , nx = slice(arr,1) // Next s.
    
       , isNegative = (v0=="-"&&_opbuf)  // When "-", and it's in op mode (means
                                         // it is behind an operator), like x*-2, 
                                         // set to turn it's next num to negative. 
//                                         
//       //--------------------------------
//       // Actual calc happens here:
//       // Hint: this would be a way to design customized 
//       //       operator, if so desired. 
       , n_rtn= isNegative? _rtn             // When neg sign, do nothing to _rtn 
                :_opbuf=="^" ? pow(_rtn,val) 
                :_opbuf=="*"? _rtn*val
                :_opbuf=="/"? _rtn/val
                :_opbuf=="+"? _rtn+val
                :_opbuf=="-"? _rtn-val
//                                             // in case of 2(x+1) when x is like 3
//                                             // it'd be 2,3 w/o operator in between.
//                                             // We put a * here. ==> need more work
//                :(_rtn!=undef && _opbuf==undef) ? _rtn*val
                :_rtn 
//                
       ,n_sign= isNegative?-1:1  // Sign carried over to next cycle       
       ,n_opbuf= isNegative      // Set the op state. 
                  ? _opbuf       // If isNegative, do nothing
                  : has(ops, v0)?v0:undef  // If v0 is any operator, store it
                  
       ,_debug= _fmth(
                concat( _debug
                      ,[ "<br/>",""
                      // , "fml", fml
                      // , "_fml", _fml
                       ]
                      ) )           
       )         
(   
    
    !isarr(arr)?
      errmsg("typeError", ["vname","arr"
                          ,"fname","calc_basic"
                          ,"tname","array"
                          ,"vtype",type(arr)
                          ,"val", arr ] )
   :countArr(arr)>0?     
       errmsg("arrTypeError", ["vname","arr"
                      ,"fname","calc_basic"
                      ,"tname","array"
                      ,"vtype",type(arr)
                      ,"val", arr ] )   
   :(
      has(funcs, arr[0])    // v0 is a func name, proceed with the
                            // rest by setting _func= func name
      ? run( arr[0], calc_basic( nx, scope=scope, isdebug=isdebug, _debug=_debug) ) 
      : ( _rtn==undef   // start up, _rtn is undef
          ? (
              v0=="-" 
              ? calc_basic(  nx, scope=scope, _opbuf=undef  ,_rtn= undef, _sign=-1, isdebug=isdebug, _debug=_debug )
              : calc_basic(  nx, scope=scope, _opbuf=n_opbuf,_rtn= val  , _sign=n_sign, isdebug=isdebug, _debug=_debug )
            )
          : arr 
             ? calc_basic(nx, scope=scope, _opbuf=n_opbuf,_rtn=n_rtn, _sign=n_sign, isdebug=isdebug, _debug=_debug )
             : (isdebug?_debug:_rtn)  // When running out of upks, return _rtn
       )    
   )   
);      

function calc_axb(a,op,b)=
(
   op=="+"? a+b
   :op=="-"? a-b  
   :op=="*"? a*b  
   :op=="/"? a/b  
   :op=="<"? a<b?1:0
   :op==">"? a>b?1:0
   :op=="="? a==b?1:0
   :op=="&"? a&&b?1:0
   :op=="|"? a||b?1:0
   :op=="%"? a%b  
   :op=="^"? pow(a,b)  
   :undef 
);

//echo( 3_4 = calc_axb( 3,"<",4 ) );
//echo( 3_4 = calc_axb( 3,">",4 ) );
//echo( 3_4 = calc_axb( 5,"<",4 ) );
//echo( 3_4 = calc_axb( 5,">",4 ) );
//echo( 3_4 = calc_axb( 3,"=",4 ) );
//echo( 3_4 = calc_axb( 3,"=",3 ) );





function calc_shrink( data
                    , ops=["%", "^","*/","+-","><=&|"]
                    , _i=1
                    ,  _debug=[] 
                    )=
(
    // data: [5, "+", 4, "*", 3, "+", 2]
    // ops: ["^","*/","+-"] : sets of operators. Each set is a str    //                        containing one or more single-char 
    //                        operators, treated equally. 
    let(
         opset = ops[0]
         
       , iod= // Find the first item in data matching any of opr in
              //  this opr set. For example, if opset="*/", then 
              //  get the index of first found * or / in data 
              //
              //  old code (working):
              // [ for(i=range(data))
              //        if( index(opset, data[i])>-1) i
              //    ][0] // <=== pick the first one
              //
              // new code. match return like [[2,2, ["/"]],[9,9, ["*"]]]
              // or false. Note: false[0]=> undef
              match( data, ps=[opset] )[0][0]
                  
       , op = data[iod]  // what the found operator is
                  
       , newops= iod==undef? // If this opset fail, go next set 
                  slice( ops, 1) : ops
                  
       , newdata= iod==undef? data 
              :[ for( i=range(data) ) //if (data[i]!=op) data[i] 
                  if( i<iod || i>iod+1 ) // Say data= [ .... x,"+",y ....]
                                         // Item before x and after y are unchanged 
                                         // When reach x, calc x+y, and continue
                                         // to the item after y 
                     i==iod-1? calc_axb( data[i], op, data[i+2] )
                     : data[i]     
                ]   
       , newi = iod==undef
                  ? 0        // opset has no match in data, reset _i to 0 for next opset 
                  : (_i+1)   // opset has match in data, continue to see if more match
                             //  like: [5, "*", 4, "+", 3, "*", 2] has 2 *   
//      ,_debug=  concat(_debug,
//                [_fmth([ "data", data
//                       , "ops", ops
//                       , "opset", opset
//                      , "_di", _di
//                      , "iod", iod
//                      //, "_oi", _oi
//                      , "op", op
//                      , "di", di
//                      , "newops", newops
//                      , "newdata", newdata
//                      ]
//                     ) 
//                ])           
       )  
    // NOTE: len(data)>3 is enough. We throw in "ops" to prevent 
    //       anything goes wrong resulting in endless run
    len(data)>3 && ops 
       ? calc_shrink( newdata, newops, _di=newi, _debug=_debug)
    : calc_axb( data[0], data[1], data[2] )
    //: str( "<br/>",join(_debug, "<br/>"))
                  
//    _oi<len(ops)? calc_shrink( new, ops, _oi=_oi+1)
//    : new[0]
); 

        
//function calc_blk(
//          s           // s is a simply formula w/o (), like, "a+2bb/4-6" 
//        , scope = []  // like: ["x",3,"age",20]
//        , _pis= undef // indices of "(", like:  [0,9]
//        , _debug= ""
//        )=
//(
//   let( isbeg= _pis==undef   // Is this the first run?
//      , blkm = isbeg?        // Match blocks in the first run    
//                 match(s,ps=[["(",1]])  //:[[0,0,["("]],[9,9,["("]]]
//                 :undef
//      ,_pis = blkm ? [ for(m=blkm)m[0] ]  //:[0,9]
//                 : _pis
//      
//      , blk = _pis?         // Extract content between "(" and ")"
//                match_ps_at( s
//                           , i= last(_pis)   // Starting backward
//                           , ps=[["("],str(A_zu,0_9d,"^*/+-"),[")"]]
//                           )    //: [3, 7, ["(", "y+2", ")"]]           
//               :undef     
//      
//      , blkval= blk?        // Convert extracted content: "y+2"=> 3 
//                calc_flat( blk[2][1], scope) 
//                : undef
//         
//      , newpis = _pis? slice(_pis,0,-1): undef
//      
//      , news = _pis?     //: blk= [3, 7, ["(", "y+2", ")"]]     
//               str( slice(s,0, blk[0])
//                  , blkval
//                  , slice(s, blk[1]+1)
//                  ):undef                
//      
////      , _debug=str( _debug,"<br/>"
////                  , ", _pis= ", _pis
////                  , ", blkm= ", blkm
////                  , ", blk= ", blk
////                  , ", blkval= ", blkval
////                  , ", newpis= ", newpis
////                  , ", news= ", news
////                  ) 
//      )
//      
////   isbeg && !blkm ? _debug //calc_flat( s,scope)
////   :!newpis? _debug //calc_flat( news,scope)
////   : calc_blk(news, scope, _pis=newpis, _debug=_debug)
//
//   isbeg && !blkm ? calc_flat( s,scope)
//   :!newpis? calc_flat( news,scope)
//   : calc_blk(news, scope, _pis=newpis, _debug=_debug)
//
//);

function calc_flat(   // calc flat formula (no function name, no ( ) )
            s
          , scope= []
          , _i =0
          , _buf=[]
          , _debug=""
          )=
(
    let( ops = "%^*/+-<>=&|" 
         // Match named patterns to current i
         // Returned m => ["word",2,4,["abc"] ]
       , ptns=[                  
                ["op",[[ops,1]], -1]
              , ["word",[ A_zu], -1]  
              , ["num", [0_9d],  -1]
              , ["sp", [" "],    -1]  // space to skip
              ]
      // , cond = split(s, "?")
      // , cond_tf= len(cond)>1? split(cond[1],":"):undef
      // , m = len(cond)==1? match_nps_at( s, _i, ptns):undef
       , 
       , m = match_nps_at( s, _i, ptns)
       , name = m[0]
       , v    = m[3][0]
       , _val = name=="num"? num(v)
                : name=="word"? hash(scope, v)
                : undef
       //----------------------------------------------
       // Handle 2 conditions:
       // (1) 3x => add additional "*" between 3 and x
       // (2) 5+-6 => when we at 6, the previous 2 items
       //              in _buf are an op and a "-"
       //          => make 6=-6, and take "-" away from _buf
       , last_is_op = has( ops, get(_buf,-1))
       , isneg  = _val!=undef 
                  && last(_buf)=="-" 
                  && ( _i==1 || has( ops, get(_buf,-2)))
       , val = isneg? -_val:_val
               
       , newi = _i +( m ? len(v):1) 
       , _buf = val && !last_is_op&&_buf
                 ? app( _buf, "*" )     //... condition (1)
                :isneg ? slice(_buf,0,-1) //... condition (2)
                :_buf
                
       , newbuf= concat( _buf, val!=undef?[val]:name=="op"?[v]:[]
                       )  
//       , _debug= str( _debug,"<br/>"
//                    ,"{ s[_i=",_i,"]= ", s[_i] 
//                    ,", m= ", m
//                    ,", name= ", name
//                    ,", v= ", v
//                    ,", isneg= ", isneg
//                    ,", val= ", val 
//                    ,", newi= ", newi
//                    ,", newbuf= ", newbuf
//                    ,"}"
//                    )
       )
   //_i>=len(s)-1? _debug
   //cond_tf? (
   _i>=len(s)-1? ( len(newbuf)==1?newbuf[0]
                   : calc_shrink( newbuf ) )
   : calc_flat( s, scope, newi, newbuf, _debug)   

);

module calc_flat_test0(){
    echo( module_name = parent_module(0) );
    scope= ["a",3,"b",4];
//echo( calc_flat( "a/bb*3",scope ) ); 
//
//echo( "2.25? ",calc_flat( "a/bb*3",scope )==2.25 ); 
//echo( "2.25? ",calc_flat( "a*3/bb",scope )==2.25 ); 
//echo( "19 ? ", calc_flat( "a+bb^2", scope)==19 ); 
//echo( "11 ? ", calc_flat("a*bb/2+5",scope)==11);
//echo( "10 ? ", calc_flat("a+bb/2+5",scope)==10);
//echo( calc_flat("  a+ bb /2+ 5",scope));
//echo( "10 ? ", calc_flat("  a+ bb /2+ 5",scope)==10 );
//echo( "24 ? ", calc_flat("a+4bb+5",scope)==24 ); 
//echo( "24 ? ", calc_flat("1+a*2bb^2*2",scope)==193 ); 
//echo( "305 ? ", calc_flat("a+bb/2+300",scope)==305 ); 
//echo( "305.15 ? ", calc_flat("a+bb/2+300.15",scope)==305.15 );
//echo( "305 ? ", calc_flat("a+300+bb/2",scope)==305 );    
//echo( "305.16 ? ", calc_flat("a+300.16+bb/2",scope)==305.16 ); 
//echo( "2.25 ? ", calc_flat("a/bb*3",scope)==2.25 ); 
//echo( repeat("-",20) );
//echo( calc_flat( "5") );
//echo( calc_flat( "-5") );
//echo( calc_flat( "3*-5") );
echo( calc_flat( "3*5%2+1") );
echo( "calc_flat('a<b',scope)= ", calc_flat("a<b",["a",3,"b",4]));
echo( "calc_flat('a<b',scope)= ", calc_flat("a<b",scope=scope));
echo( "calc_flat('a<b',scope)= ", calc_flat("a<b",["a",3,"b",4]));
//echo( calc_flat( "3-5") );
    } 

//calc_flat_test0();
  
    
function calc_func(
          s           // s is a simply formula w/o (), like, "a+2bb/4-6" 
        , scope = []  // like: ["x",3,"age",20]
        , _pis= undef // indices of "(", like:  [0,9]
        , _debug= ""
        )=
(
   let( ops= "%^*/+-<>=&|"
      //---------------------------------------------------------
      , scp_vals= vals(scope) //: ["x",[2,3,4],"y",5]=> [ [2,3,4], 5 ]
      , scp_arr_is = [ for(i=range(scp_vals))   
                      if( isarr( scp_vals[i])) i] // indices of arr in scope
      , skip_by_scp = scp_arr_is  
                      
      //---------------------------------------------------------
      // Chk if s contains aaa?bb:cc struct. If yes, all 
      // critical assignments inside this let() will be skipped
      // and it will be resolved in the func body first. 
      , cond = split(s,"?")
      , skip_by_cond= skip_by_scp || (len(cond)!=1)                                             
      , cond_tf= !skip_by_cond? split(cond[1],":"):undef
                      
      //---------------------------------------------------------
      // Find the indices of "(". This is done only on the
      // first run. The generated pis (paranthesis indices)
      // will be carried over down the calc. 
      //
      , isbeg= _pis==undef   // Is this the first run?
      , blkm = !skip_by_cond &&isbeg?   // Match blocks in the first run    
                 match(s,ps=[["(",1]])  //:[[0,0,["("]],[9,9,["("]]]
                 :undef
      ,_pis = blkm ? [ for(m=blkm)m[0] ]  //:[0,9]
                 : _pis
      , skip_by_blkm = blkm==undef                 
      
      //---------------------------------------------------------
      // Matching block contents, starting backward from the last item
      // of _pis. blk: [3, 7, ["(", "y+2", ")"]]           
      , blk = !skip_by_blkm && _pis?         
                match_ps_at( s
                           , i= last(_pis)   // Starting backward
                           , ps=[["("],str(A_zu,0_9d,ops),[")"]]
                           )    
               :undef  
      , _blkval= blk?        // Convert extracted content: "y+2"=> 3 
                calc_flat( blk[2][1], scope) 
                : undef
      , skip_by_blk = !blk   
      
      //---------------------------------------------------------
      // Check if there's a func name before the found "("
      // If yes, do a calc with it
      , prestr = !skip_by_blk?slice(s,0, blk[0]):undef// substr before "("
      , _fname = !skip_by_blk?
                  matchr_at( prestr,-1,CALC_FUNCS ) //:[0,4,"round"]
                  : undef
      , fname = _fname[2]
      , blkval = fname?
                 run( fname, _blkval):_blkval
      
      //---------------------------------------------------------
      // Adjust the ending index of str on the left
      , prestr_lasti = blk[0]-1-(fname?len(fname):0) //
      
      //---------------------------------------------------------
      // Prepare for the next run
      , newpis = !skip_by_cond && _pis? slice(_pis,0,-1): undef
      , news = !skip_by_cond&&_pis?     //: blk= [3, 7, ["(", "y+2", ")"]]     
               str( slice(s,0, prestr_lasti+1) //blk[0]-(fname?len(fname):0))
                  , //prestr_lasti>0
                    s[ prestr_lasti ] //: not the first item
                    && 
                    !has( str("(",ops), s[prestr_lasti])
                      ? "*":""
                  , blkval
                  , slice(s, blk[1]+1)
                  ):undef                
      
//      , _debug=str( _debug,"<br/>"
//                  , ", _pis= ", _pis
//                  , ", blkm= ", blkm
//                  , ", blk= ", blk
//                  , ", prestr= ", prestr
//                  , ", fname= ", fname
//                  , ", blkval= ", blkval
//                  , ", newpis= ", newpis
//                  , ", news= ", news
//                  ) 
      )
//   isbeg && !blkm ? _debug //calc_flat( s,scope)
//   :!newpis? _debug //calc_flat( news,scope)
//   : calc_blk(news, scope, _pis=newpis, _debug=_debug)

//   3>4?(  
//              calc_func( cond[0], scope )
//             ?calc_func( cond_tf[0], scope )
//             :calc_func( cond_tf[1], scope )
//           )
//   :
   isbeg && !blkm ? calc_flat( s,scope)
   :!newpis? calc_flat( news,scope)
   : calc_func(news, scope, _pis=newpis, _debug=_debug)

);  

module calc_func_test(){
   echo( module_name = parent_module(0) );
    
   scope= ["a", 3, "b", 4];
    
   echo( "calc_flat('a<b',scope)= ", calc_flat("a<b",scope) );
   echo( "calc_flat('a<b',scope)= ", calc_flat("a<b",["a", 3, "b", 4]) );
   echo( calc_func("a<b",scope) );
    
}

//calc_func_test();


   


    
//function calc_flat(
//          s          // s is a simply formula w/o (), like, "a+2bb/4-6" 
//        , scope = [] // like: ["x",3,"age",20]
//        , _isneg = false
//        , _buf  = []
//        , _debug= ""
//)=(
//   let( ops= "+-*/^" 
//      //, oplvs= ["+",0,"-",0,"*",1,"/",1,"^",2] // decide operator priority
//      , ptns= [                  // matching paterns
//                ["op",ops,""]
//              , ["word"]         // use default begword settings
//              , ["num", ".0123456789",".0123456789"]
//              , ["sp", " "," "]  // space to skip
//              ]
//      , m  = begmatch( str(s), ptns ) 
//      , _val= m[0]=="num"? num(m[1]):hash( scope, m[1]) 
//      , val = _isneg && _val!=undef ? -_val:_val
//      // return like ["op","+"],["num","3.5"],["word","age"], ["sp","   "]
//      ,last_is_op = index( ops, last(_buf))>-1
//      , _isneg= s[0]=="-" && last_is_op  
//      //,thisnotop = index( ops, m[1])==-1
//      //,_buf0= _buf && val && !last_is_op  ? app( _buf,  "*" ): _buf
//      ,_buf0= _buf && val && !last_is_op  ? app( _buf,  "*" )
//              : _isneg? slice(_buf,-1) // For cases like [ 3, "*", "-",5]
//                                       // We already moved "-" to 5: [3,"*",-5]
//              : _buf
//      ,_buf= concat( _buf0
//                   , m && (m[0]=="op" || m[0]=="word" || m[0]=="num")
//                      ? (val!=undef?val:m[1])
//                      : [] 
//                   )
//      , newbuf= _buf //_isneg? [ -_buf[1]]:_buf
//      //, newbuf= len(_buf)==2 &&_buf[0]=="-"? [ -_buf[1]]:_buf
//      , mlen= _isneg?1:len( m?m[1]:s[0] )  // len of the matched, if any 
//      , nexts= m?slice( s, mlen):s  // Shorten s 
////      ,_debug=  str(_debug,"<br/>"
////                      , "{ s= ", s
////                      , ", m= ", m
////                      , ", _isneg= ", _isneg
////                      , ", val= ", val
////                      , ", _buf0= ", str(_buf0)
////                      , ", _buf= ", str(_buf)
////                      , ", newbuf= ", str(newbuf)
////                      , ", nexts= ", nexts
////                      , "}<BR/>"
////                     ) 
//                
//      )
//   
//   !isstr(s) || ( (m[1]==s || str(val)==s) && len(_buf)==1)? val
//   :len(s)<2? newbuf //calc_shrink(newbuf) 
//   : calc_flat( nexts, scope, _isneg, newbuf) //newbuf)      
//
////This is for debug:
//
////   len(s)<2? str( "<br/>",_debug)   
////   : calc_flat( nexts, scope, _isneg, newbuf ,_debug)      
//
//);
//calc_test();


//function calc_flat(
//          s          // s is a simply formula w/o (), like, "a+2bb/4-6" 
//        , scope = [] // like: ["x",3,"age",20]
//        , _buf  = []
//        //, _debug= []
//)=(
//   let( ops= "+-*/^" 
//      , oplvs= ["+",0,"-",0,"*",1,"/",1,"^",2] // decide operator priority
//      , ptns= [                  // matching paterns
//                ["op",ops,""]
//              , ["word"]         // use default begword settings
//              , ["num", ".0123456789",".0123456789"]
//              , ["sp", " "," "]  // space to skip
//              ]
//      , m  = begmatch( s, ptns )  
//             // return like ["op","+"],["num","3.5"],["word","age"], ["sp","   "]
//                                 
//      , val= m[0]=="num"? num(m[1]):hash( scope, m[1])
//             // We have a value only when matching num and word
//                                 
//      , _buf0= m[0]=="op"? appi(_buf,len(_buf)-1,m[1])
//                           // Example: s= "a+bb/2+5"  (a=3,b=4)
//                           // _buf:[[3]] ==> _buf0=[[3, "+"]]
//                           // _buf:[[3,"+"],[4]] ==>  _buf0=[[3,"+"],[4,"/"]]
//                           // _buf:[[3,"+"],[4,"/"],[2]] ==>_buf0=[[3,"+"],[4,"/"],[2,"+"]]
//                           
//              :val!=undef? // We have a value
//               ( len(_buf)==0          // start up, _buf:[]==> _buf0=[[val]] 
//                 || len(last(_buf))==2 // _buf:[[3,"+"]] ==>  _buf0=[[3,"+"], [4]]
//                 ? app( _buf,[val])
//                 : replace( _buf, -1, [last(_buf)[0]*val] )
//                           // When a val matched, and the last item has no op:
//                           //  _buf=[[3,"+"], [5]] 
//                           // it means we are encountering cases like 5bb below:
//                           //   s="a+5bb+3"  (a=3,b=4)
//                           //   _buf:[[3,"+"],[5]]==> _buf0=[[3,"+"],[20]]             
//               )
//              : _buf  // not an op and no value: no change   
//              
//      // Check operator priority and shrink (collapse) _buf0 if needed:
//      
//      , oplv =  hash( oplvs, m[1]) // Larger means topper priority
//      
//      , isop = m[0]=="op"
//      
//      , newbuf=  isop && oplv==hash( oplvs, get(_buf0,-2)[1]) ? 
//                                   // Current and prev ops are same level, like
//                                   // [[3, "/"], [4, "*"]]  
//                                      calc_shrink(_buf0, len(_buf0)-2)
//                 :isop && oplv< len(_buf0) ?
//                     calc_shrink(_buf0, oplv) 
//                 :len(s)==len(m[1])? // the last match, _buf0 will look like:
//                                     //  [ ..... [5]]
//                                     // We want to shrink it to two items:
//                                     //  [ [n,op],[5] ] to prepare it for 
//                                     // calc_axb(...) later
//                    calc_shrink(_buf0, 1)
//                 :_buf0
//                 
////      , newbuf=  len(last(_buf0))==2 && oplv == hash( oplvs, get(_buf0,-2)[1]) ?
////                                      calc_shrink(_buf0, len(_buf0)-2)
////                 :len(last(_buf0))==2 && oplv< len(_buf0) ?
////                     calc_shrink(_buf0, oplv) 
////                 :len(s)==len(m[1])? // the last match, _buf0 will look like:
////                                     //  [ ..... [5]]
////                                     // We want to shrink it to two items:
////                                     //  [ [n,op],[5] ] to prepare it for 
////                                     // calc_axb(...) later
////                    calc_shrink(_buf0, 1)
////                 :_buf0
//
//      , mlen= len( m?m[1]:s[0] )  // len of the matched, if any 
//      , nexts= m?slice( s, mlen):s  // Shorten s 
////      ,_debug=  concat(_debug,
////                [_fmth([ "s", s
////                      , "m", m
////                      , "val", val
////                      , "_buf", _buf
////                      , "_buf0", _buf0
////                      , "oplv", oplv
////                      , "newbuf", newbuf
////                      , "nexts", nexts
////                      ]
////                     ) 
////                ])
//      )
//
//   len(s)<2? calc_axb( newbuf[0][0], newbuf[0][1], newbuf[1][0] ) //_buf
//   : calc_flat( nexts, scope, newbuf)      
//
//// This is for debug:
////   len(s)<2? str( "<br/>",join(_debug, "<br/>"))   
////   : calc_flat( nexts, scope, newbuf,_debug)      
//
//);


////========================================================
//calcs= ["calcs", "arr,scope", "val", "eval"
//,"Given arr representing an one-dimension, calculable array (containing no
//;; array), and scope given as a hash, calc the result.
//;;
//;; calc( [''x'',''+'',1], [''x'',3] ) => 4 
//;; calc( [''x'',''^'',2], [''x'',3] ) => 9 
//;;
//;; Note that calc is done left to right, no matter what operator is:
//;;
//;; calc( [2,''+'',''x'',''^'',2], [''x'',3] ) => 25 
//;;
//"];
//
//
//function calcs( fml, scope, isdebug=false, _debug=[] )=
//    let( 


//========================================================
centroidPt=["centroidPt","pqr","Point","Point"
, "Return the centroid pt of pqr, where lines of pt-to-opposite-midPt
;; intersect.
"];
//http://en.wikipedia.org/wiki/Centroid

function centroidPt(pqr)=
   let( P=pqr[0], Q=pqr[1], R=pqr[2] )
   lineCrossPt( P, midPt(Q,R), Q, midPt(P,R) );  



//========================================================
chkbugs=["chkbugs","","","",
,"                     
;;  function abc( ... _debug=[]) // <--- define _debug
;;     let( a=...                // <--- define vars
;;        , b=...
;;        , _debug = chkbugs( _debug         // store it
;;                          , [''a'',a,''b'',b] 
;;                          )                      
;;       )
;;     ...
;;     //: return   <----- comment out your return
;;     : _debug                  
"];
                      
function chkbugs(_debug, h)=
   _fmth( concat( _debug, ["<br/>", ""], h ) );                       
         
         
         
//========================================================
cirfrags=[ "cirfrags", "r,fn,fs=0.01,fa=12", "number", "Object, Geometry",
 " Given a radius (r) and optional fn, fs, fa, return the number 
;; of fragments (NoF) in a circle. Similar to $fn,$fs,$fa but
;; this function (1) doesn't put a limit to r and (2) could return
;; a float. In reality divising a circle shouldn't have given partial
;; fragments (like 5.6 NoF/cicle). But we might need to calc the 
;; frags for an arc (part of a circle) so it'd better not to round it
;; off at circle level. 
;; fa: min angle for a fragment. Default 12 (30 NoF/cycle). 
;; fs: min frag size. W/ this, small circles have smaller 
;; NoFs than specified using fa. Default=0.01 (Note: built-in $fs 
;; has default=2). 
;; fn: set NoF directly. Setting it disables fs & fa. 
;;
;; When using fs/fa for a circle, min NoF = 5. 
;;
;; See: http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn

"];
 
function cirfrags(r, fn, fs=0.01, fa=12)=
( // from: get_fragments_from_r
  // http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn
    fn? max(fn,3)
	: max(min(360 / fa, r*2*PI / fs), 5)
);


//=========================================
function combine(a,b, _rtn=[],_i=0)=  // combine array a,b 2015.4.25
(
  let( _rtn= _rtn==[]?a
             : len( [ for(x=a) if(b[_i]==x) 1] )==0 // if b[_i] not in a
               ? concat( _rtn, [b[_i]] )  
               : _rtn
     )         
  _i>=len(b)-1 ? _rtn
  : combine(a,b,_rtn,_i+1)
);
                 
                 
//=========================================
function coordPts(pqr, len,x,y,z)= 
    let( P=pqr[0], Q=pqr[1], R=pqr[2]
       , len= len==undef?dist([P,Q]):len
       , x = x==undef?len:x
       , y = y==undef?len:y
       , z = z==undef?len:z
       , N=N(pqr,len=z), NN=N( [N, Q(pqr),P(pqr)] ,len=y) 
       )
(  [ onlinePt( [Q,P], len=x )
   , Q, NN, N ]
);

//=========================================
function tocoord(pts, coord)= 
    let( O = coord[1]
       , X = coord[0]
       , Y = coord[2]
       , Z = coord[3]
       )
(  [for(p=pts) [ dist( [O, othoPt([ O,p,X])] )
               , dist( [O, othoPt([ O,p,Y])] )
               , dist( [O, othoPt([ O,p,Z])] )
               ]] 
);



//=========================================
iscoord=["iscoord","pts","T/F", "Geometry",
" Return true if pts is a 4-pointer representing a coordinate system,
;; in which pts[1] is the origin."
];

function iscoord(pts)=
   len(pts)==4 && is90( sel(pts,[1,0]))
               && is90( sel(pts,[1,2]))
               && is90( sel(pts,[1,3]));
               
module iscoord_test(ops=["mode",13])
{
   doctest( iscoord,
   [
   ], ops );    
}    



//========================================================
cornerPt=[ "cornerPt", "pqr", "pt", "Point",
"
 Given a 3-pointer (pqr), return a pt that's on the opposite side
;; of Q across PR line.
;;
;;        Q-----R  Q--------R
;;       /   _//    ^.      |^.
;;      /  _/ /       ^.    |  ^.
;;     /_/   /          ^.  |    ^.
;;    //    /             ^.|      ^. 
;;   P     pt               P        pt
"];
 

function cornerPt(pqr)=
(
	onlinePt( [pqr[1],midPt	([pqr[0],pqr[2]])], ratio=2)
);



//========================================================
countArr= ["countArr", "arr", "int", "Array, Inspect, Type",
" Given array (arr), count the number of arrays in arr.
"];

function countArr(arr)= countType(arr, "arr");

module countArr_test( ops )
{
	doctest
	( 
	  countArr
	, [
	   [ "[''x'',2,[1,2],5.1]", countArr(["x",2,[1,2],5.1]), 1 ]
	  ]
	, ops
	);

} 
//doc(countArr);



//========================================================
countInt=["countInt", "arr", "int", "Array, Inspect, Type",
" Given array (arr), count the number of integers in arr.
"];

function countInt(arr)= countType(arr, "int");



//========================================================
countNum= ["countNum", "arr", "int", "Array, Inspect, Type",
" Given array (arr), count the number of numbers in arr.
"];

function countNum(arr)= countType(arr, "num");



//========================================================
countStr =  ["countStr", "arr", "int", "Array, Inspect, Type",
" Given array (arr), count the number of strings in arr.
"];

function countStr(arr)= countType(arr, "str") ;



//========================================================
countType= ["countType", "arr,typ", "int", "Array, Inspect, Type",
" Given array (arr) and type name (typ), count the items of type typ in arr.\\
"];

function countType(arr, typ, _i_=0)=
(	
    typ=="num"
    ?len( [ for(x=arr) let(t=type(x)) if( t=="float"||t=="int") 1] )
    :len( [ for(x=arr) let(t=type(x)) if( t==typ) 1] )
);



//========================================================
cubePts=["cubePts", "pqr,p=undef,r=undef,h=1", "points", "Point",
" Like boxPts but is not limited to axes-parallel boxes. That is, 
;; it produces 8 points for a cube of any size at any location along 
;; any direction. 
;; 
;; The input pqr = [P,Q,R0] is used to make the square PQRS that is
;; coplanar with R0. It is then added the h to make TUVW by finding 
;; the normal of pqr. Optional p,r,h: numbers indicating lengths as 
;; shown belo.
;;
;;     U------------------V_ 
;;     |'-_     R0        | '-_
;;     |   '-_ -''-_      |    '-_
;;     |   _. '-T---'-------------W
;;     |_-'     |      '-_|       |
;;     Q_-------|---------R_      | h
;;       '-_    |    r      '-_   |
;;       p  '-_ |              '-_|
;;             'P-----------------S
;;                        r
;; Return a 8-pointer: [P,Q,R,S,T,U,V,W] with order:
;;
;;     5----------6_ 
;;     | '-_      | '-_
;;     1_---'-_---2_   '-_
;;       '-_   '4---------7
;;          '-_ |      '-_|
;;             '0---------3
;;
;; Use rodfaces(4) to generate the faces needed for polyhedron. 
;; 
;; New 20141003: shift 
;;
;;     U------------------V_ 
;;     |'-_               | '-_
;;     |   '-_            |    '-_
;;    Q|------'-T-----------------W
;;     | '-_    |  r      |       |
;;     |_---'-_-|----------_      | 
;;       '-_   'P           '-_   | ---
;;          '-_ |              '-_|   -shift
;;             'X------------------ ---
;;
;; New 2015.3.7: pqr is optional. If not given, make it with randPts(3)
"];

function cubePts( pqr, p=undef, r=undef, h=1,shift)=
 let( _pqr= or(pqr,randPts(3))
    , Q = Q(_pqr)
	, h_vector = normalPt(_pqr, len=h)-Q
	, shiftvec = onlinePt( [N(_pqr), Q], len=-shift )-Q
	, pqr = isnum(shift)
			? [ for (p=_pqr) p+shiftvec ] 
			:_pqr 
    )	
(	concat( squarePts( pqr, p=p,r=r )
		  , squarePts( //addv( pqr, normalPt(pqr, len=h)-Q(pqr))
					  [for (pt=pqr) sign(h)>0?(pt-h_vector):(pt+h_vector) ]
                      ,p=p,r=r )
	)		
);

                      
                                   
                      
//========================================================
del=["del", "arr,x,byindex=false,count=1", "array", "Array",
 " Given an array arr, an x and optional byindex, delete all
;; occurances of x from the array, or the item at index x when
;; byindex is true. 
;;
;;   del( [2,4,6,8], 2 )= [4, 6, 8]
;;   del( [3,undef, 4, undef], undef )= [3, 4]
;;   del( [2,4,6,8], 2, byindex=true )= [2, 4, 8] 
;;                      
;; New 2014.8.7: arg *count*: when byindex=true, count sets 
;;  how many items starting from x (an index) to del.
;;
;;   del( [2,4,6,8], 1, true,2 )= [2, 8] // del 2 tiems starting from 1                      
"];

function del(arr,x, byindex=false,count=1)=
let( x = byindex? fidx(arr, x):x )
(
	(len(arr)==0?[]
	: byindex
	  ? [for(i=range(arr)) if (i<x || i>=x+count) arr[i] ]
	  : [for(k=arr) if (k!=x) k] 
//	  : concat( arr[0]==x? []:[arr[0]]
//				, del(shrink(arr),x, byindex=false)
//				)
	)
);
      


//========================================================
delkey=[ "delkey", "h,k", "hash",  "Hash",
" Given a hash (h) and a key (k), delete the [key,val] pair from h. 
;;
;;   delkey( [''a'',1,''b'',2], ''b'' )= [''a'', 1]      
"];
function delkey(h,key, _i_=0)=
(
     _i_>=len(h)? []
	 : concat( h[_i_]==key? []:[h[_i_], h[_i_+1]]
	         , delkey( h, key, _i_+2) 
			 )
);


//========================================================
dels=["dels", "a1,a2", "array", "Array",
 " Given arrays a1, a2 delete all items in a2 from a1.
;;
;;   dels( [2,4,6,8], [4,6] )= [2, 8]
;;
;; Ref: arrItcp
"];

function dels(a1,a2)=
let( a2=isarr(a2)?a2:[a2] )
(
	[for(x=a1) if (index(a2,x)==-1) x] 
);


//========================================================
det=["det","pq","array","Array,Math",
" Given a 2-pointer (pq), return the determinant. "
];

function det(pq)=
(
	dot( pq[0], pq[0] ) * dot( pq[1],pq[1]) - pow( dot(pq[0],pq[1]),2)
//( N1 . N1 ) ( N2 . N2 ) - ( N1 . N2 )2
);



//==================================================================
deeprep=["deeprep","arr,indices,new", "arr","Array"
, "Given arr, and list of indices ([i,j,k...], and a value, new, replace 
;; the item arr[i][j][k]... with new. Items in indices could be negative. 
;; 
;; arr=
;;  [0,1,                      ,3]
;;        [20,            ,22]
;;             [40,41,42]
;;
;;  deeprep( arr, [2,1,-1], 99 ):
;; 
;;  [0,1,                      ,3]
;;        [20,            ,22]
;;             [40,41,99]
;;
;;   deeprep( [0,1,[20,21,22],3],[-2,1],99 )= [0, 1, [20, 99, 22], 3]
;;"
];

function deeprep(arr, indices, new, _debug=[])=
    let( i = indices[0] 
//       ,_debug= _fmth(concat( _debug,
//                [ "<br/>",""
//                , "arr",arr
//                , "indices",indices
//                , "i",i
//                ]
//             )) 
    )         
    ( 
     indices==[]? arr
     :len(indices)>1
     //? _debug 
     ? replace( arr, i, deeprep( get(arr,i), slice(indices,1), new, _debug ) )
     : len(indices)==1
     //? _debug
     ? replace( arr, i, new )     
     : new
    );     
        

   
//========================================================
// Note 2015.4.11:
// dist could be made available for dist of dist(a,b)
//                a       b   
//   pt-pt      P       Q
//   pt-ln      P       [Q,R] 
//   ln-pt      [Q,R]   P
//   ln-ln      [P,Q]   [R,S]       
//   pt-pl      P       [R,S,T]
//   pl-pt      [R,S,T] P
//   ln-pl      [P,Q]   [R,S,T]
//   pl,ln      [R,S,T] [P,Q]
//   pl,pl      [P,Q,R] [S,T,W]




//========================================================
dist=["dist","[a,b];a,b","number","len",
"The distance between a,b, both could be pt, line or plane."
];
////function dist(pq) = norm(pq[1]-pq[0]);  // Get distance between a 2-pointer
function dist(a,b)=
(
    let( c= b==undef?a[0]:a
       , d= b==undef?a[1]:b
       , t= str(typeplp(c),typeplp(d))
       )
     t=="ptpt"? norm(c-d)
    :t=="ptln"? norm(c- othoPt( [d[0],c,d[1]] ) )
    :t=="lnpt"? norm(d- othoPt( [c[0],d,c[1]] ) )
    :t=="ptpl"? distPtPl( c, d )
    :t=="plpt"? distPtPl( d, c )
    :t=="lnln"? dist( lineCrossPts( a, b ) ) // need to fix this 
    :t=="lnpl"? undef // need this
    :t=="plpl"? undef // need this
    :undef
);

//========================================================
distPtPl=["distPtPl", "pt,pqr", "number", "len" ,
 " Given a pt and a 3-pointer (pqr) , return the signed distance between
;; the plane formed by pqr and the point.
"];
	
/*
	Pt = [x0, y0, z0 ]
	Pl = Ax + By + Cz + D = 0;

 	      Ax0 + By0 + Cz0 + D
	d= --------------------------
		sqrt( A^2 + B^2 + C^2 ) = norm([A,B,C])

	pc = planecoefs( pqr ) = [A,B,C,D]

	Ax0 + By0 + Cz0 + D=  [pc[0],pc[1],pc[2]] * Pt  + pc[3] = 

*/
function distPtPl(pt,pqr)=
(
	( planecoefs(pqr, has_k=false)* pt  + planecoefs(pqr)[3]) 
	/ norm(	planecoefs(pqr, has_k=false) )
);



//========================================================
distx = ["distx", "pq", "number", "len",
" Given a 2-pointer pq, return difference of x between pq. i.e., 
;;
;; distx(pq) = pq[1][0]-pq[0][0]
"];

function distx(pq)= pq[1][0]-pq[0][0];


//========================================================
disty= ["disty", "pq", "number", "len",
" Given a 2-pointer pq, return difference of y between pq. i.e.,
;;
;; disty(pq) = pq[1][1]-pq[0][1]
"];

function disty(pq)= pq[1][1]-pq[0][1];





//========================================================
distz= ["distz", "pq", "number", "len",
" Given a 2-pointer pq, return difference of z between pq. i.e.,
;;
;; distz(pq) = pq[1][2]-pq[0][2]
"];

function distz(pq)= pq[1][2]-pq[0][2];



//========================================================
dot=["dot", "P,Q", "num", "Geometry, Math"
, "dot product of P,Q, same as P*Q . P*Q is generally easier than dot(P,Q). 
;; But if P and Q are complex, then dot() might be easier. For example
;; dot(P-Q,R-S) * dot(P-R,Q-S)  would be easier and more descriptive than 
;; ((P-Q)*(R-S))*((P-R)*(Q-S))
;;
;;   P= [2,3,4];  Q= [-1,0,2]; 
;;   dot( P,Q )= 6
;;   dot( [P,Q] )= 6
;;
;; EX: See it in lineCrossPt()
"];

function dot(P,Q)= 
    let( pq= Q==undef? P:[P,Q] )
(   pq[0]*pq[1]
);


//========================================================
echoblock=[ "echoblock", "arr", "n/a", "Doc, Console" , 
" Given an array of strings, echo them as a block. Arguments:
;;
;;   indent: prefix each line
;;   v : vertical left edge, default ''| ''
;;   h : horizontal line on top and bottom, default ''-''
;;   lt: left-top symbol, default ''.''
;;   lb: left-bottom symbol, default \"'\"
;;
;; See arrblock()
"];
module echoblock(arr, indent="  ", v="| ",t="－",b="－", lt=".", lb="'")
{
	lines = arrblock(arr, indent=indent, v=v,t=t,b=b, lt=lt, lb=lb);

	_Div( join(lines,"<br/>")
		, "font-family:Droid Sans Mono"
		);  
}
 


//========================================================
echo_=["echo_", "s,arr=undef", "n/a", "Console",
" Given a string containing one or more ''{_}'' as blanks to fill,
;; and an array (arr) as the data to fill, echo a new s with all
;; blanks replaced by arr items one by one. If the arr is given as 
;; a string, it is the same as echo( str(s,arr) ).
;;
;; Example usage:
;;
;; echo_( ''{_}.scad, version={_}'', [''scadex'', ''20140531-1''] );
"];


module echo__test( ops ){ doctest( echo_, ops=ops); }

module echo_(s, arr=undef)
{
	if(arr==undef){ echo(s); }
	else if (isarr(arr)){
		echo( _s(s, arr));
	}else { echo( str(s, arr) ); }
}




//========================================================
echoh=["echoh","s,h,showkey","n/a", "Hash, Console",
" Given a string template (s) containing ''{key}'' where key is a key of
;; a hash, and a hash (h), return a new string in which all keys in {??}
;; are replaced by their corresponding values in h. If showkey is true, 
;; replaced by key=val.
"]; 

module echoh(s, h, showkey) 
{  echo( _h(s, h, showkey=showkey));
}

//========================================================
echofh=["echofh","h","n/a", "Hash, Console",
" Given a string template (s) containing ''{key}'' where key is a key of
;; a hash, and a hash (h), return a new string in which all keys in {??}
;; are replaced by their corresponding values in h. If showkey is true, 
;; replaced by key=val.
"]; 

module echofh(h, pairbreak=" , "
			  , keywrap="<b>,</b>"
			  , valwrap="<i>,</i>"
              , eq = "="
              , iskeyfmt= false
              ) 
{  echo( _fmth(h, pairbreak=pairbreak
			  , keywrap=keywrap
			  , valwrap=valwrap
              , eq = eq
              , iskeyfmt= iskeyfmt
    ));
}

//========================================================

module echom(mname)
{
	echo_( _code(_red(
    "--------------&lt;  {_}  &gt;--------------"))
         , [ mname ] );
}


//echom_demo();
//accumulate_test(["mode",2]);


//========================================================
endwith=[ "endwith", "o,x", "T/F",  "String, Array, Inspect",
"
 Given a str|arr (o) and x, return true if o ends with x.
;;
;; New 2014.8.11:
;;
;;  x can be an array, return true if o ends with any item in x.
;;
;; New 2015.2.17
;;
;;  If x is array, instead of returning T/F, return the m or undef
;;  where m is the item in x that matches.
;;
;; Ref: begwith()
"];

function endwith(o,x)= 
( 
    isarr(o)?
        isstr(x)
            ? last(o)==x
            :[for(y=x) if(last(o)==y) y][0]
    : isstr(x)
            ? slice(o,-len(x))==x
            :[for(y=x) if(slice(o,-len(y))==y) y][0] 
);


// ========================================================

function endword(
      s
    //, w=""
    //, w0=""
    , isnum = false
    , w0= str(a_z, A_Z, "_") // df letter for s[0]
    , w = str(a_z, A_Z, "_", "1234567890") // df letters
    ) =
   let( w0 = isnum? "1234567890.": w0 //str(w0,dfw0) 
      , w  = isnum? "1234567890.": w //str(w,dfw)  
      , s0 = has(w0, s[0])?s[0]:false
      , _i = [for(i=[len(s)-1:-1:0]) // index of the last match
                let(c = s[i]
                   , m= index( i==0?w0:w, c)>=0 )
                if(m) i ][0] 
      ,rtn = _i==len(s)-1?"": !_i? s
             :( !s0?"" : _i==undef? s0: slice( s, 0, _i) )
//      ,_debug= concat(  
//               "<br/>///"
//                , "s=",s
//                , "_i=",_i
//                , "rtn=", rtn )     
      )
(
   //join(_debug ,"<br/>")
   rtn 
);

//========================================================
expandPts=["expandPts", "pts,dist=0.5,cycle=true", "points", "Point",
" Given an array of points (pts), return an array of points that *expand* every 
;; point outward as indicated by Q ==> Q2 in the graph below:
;;
;; d = dist
;;
;; ---------------------Q2
;;                     /
;;                    /
;; P----------Q_     /
;;           /  '-_ /
;;          /   d  :
;;         /      /
;;        /      /
;;       R      /
"
];


/*
 	d = dist
 
   ---------------------Q2
                       /        
                      /        
   P----------Q_     /
             /  '-_ / 
            /   d  : 
           /      / 
          /      / 
         R      /     
*/

function expandPts(pts,dist=0.5, cycle=true, _i_=0)=
 let( p3s = subarr( pts,cycle=cycle )
	)
//	, p3s = [for(ijk=i3s) pts[
(	// 
	[ for(i=range(pts))
		onlinePt( [pts[i], angleBisectPt( p3s[i] )]
				, len=-dist/sin(angle(p3s[i])/2)
			    )
	]
);

	

//========================================================
fac=[ "fac", "n", "int",  "Math",
"
 Given n, return the factorial of n (= 1*2*...*n) 
"];

function fac(n=5)= n*(n>2?fac(n-1):1);



//========================================
faces=["faces","shape,sides,count", "array", "Faces,Geometry",
 " Given a shape (''rod'', ''cubesides'', ''tube'', ''chain''), an int
;; (sides) and an optional int (count), return an array for faces of
;; a polyhedron for that shape.
"];


function faces(shape="rod", sides=6, count=3)= // count is used for shape that has
 let( s = sides                                // segments unknown until run-time
	, s2= 2*sides                              // For example, chain
	, s3= 3*sides
)(
	// cube, cubesides
	//       _6-_
	//    _-' |  '-_
	// 5 '_   |     '-7
	//   | '-_|   _-'| 
	//   |    |-4'   |
	//   |   _-_|    |
	//   |_-' 2 |'-_ | 
	//  1-_     |   '-3
	//     '-_  | _-'
	//        '-.'
	//          0
	shape=="cubesides"
	? [ for(i=range(s))
		  [ i
			, i==s-1?0:i+1
			, (i==s-1?0:i+1)+s
			, i+s
		  ]
	  ]
 	: shape=="rod"
	? concat( [ reverse(range(s))], [range(s,s2) ],
			  faces("cubesides", s) ) 
	// tube
	//             _6-_
	//          _-' |  '-_
	//       _-'   14-_   '-_
	//    _-'    _-'| _-15   '-_
	//   5_  13-'  _-'  |      .7
	//   | '-_ |'12_2-_ |   _-' | 
	//   |    '-_'|    '|_-'    |
	//   |   _-| '-_10_-|'-_    |
	//   |_-'  |_-| 4' _-11 '-_ | 
	//   1_   9-  | |-'       _-3
	//     '-_  '-8'|      _-'    
	//        '-_   |   _-'
	//           '-_|_-'
    //              0
	: shape=="tube"
	? concat( faces( "cubesides", s )  // outside sides
			, [ for(f=faces( "cubesides", s ))    // inner sides
					reverse( [ for(i=f) i+s2] )
			  ]
			, [ for(i=range(s))        // bottom faces
				  [i, mod(i+s-1,s), mod(i+s-1,s)+s2, i+s2 ] 
			  ]
			, [ for(i=range(s,s2))    // top faces
				  [i, i==s2-1?s:i+1, i==s2-1?s3:i+s2+1, i+s2 ]
			  ]
			)	  
	
			//  bottom=	 [ [0,3,11, 8]
			//			 , [1,0, 8, 9]
			//			 , [2,1, 9,10]
			//			 , [3,2,10,11] ];
			//
			//	top	=	 [ [4,5,13,12]
			//			 , [5,6,14,13]
			//			 , [6,7,15,14]
			//			 , [7,4,12,15] ]

	//                 
	//       _6-_-------10------14
	//    _-' |  '-_    | '-_   | '-_
	//  2'_   |     '-5-----'9-------13
	//   | '-_|   _-'|  |    |  |    |
	//   |    |-1'   |  |    |  |    |
	//   |   _-_|----|-11_---|--15   |
	//   |_-' 7 |'-_ |    '-_|    '-_| 
	//  3-_     |   '-4------8-------12
	//     '-_  | _-'
	//        '-.'
	//          0
	//
	:shape=="chain"
	? concat( [ reverse(range(s))]            // starting face
			, [ range( count*s,(count+1)*s) ] // ending face
			, joinarr( [ for(c=range(count))
				 [ for(f=faces("cubesides", sides))
					[ for(i=f) i+c*sides]
				 ]
			  ])
			)  
	:[]
);



//========================================================
fidx=["fidx", "o,i,cycle=false,fitlen=false", "int|undef", "Index, Array, String, Inspect",
" Try to fit index i to range of o (arr or str). If i is negative, convert it 
;; to *legal* index value (means, positive int). For o of len L, this would mean 
j;; -L &lt;= i &lt; L. That is, i should be in range *(-L,L]*. 
;; 
;; New 2014.8.17: o could be an integer for range 0 ~ o
;;
;; If i out of this range, return undef (when cycle=false) or:
;; -- set fitlen=true to return the last index
;; -- set cycle=true to loop from the other end of o. So if cycle=true, a given i 
;;     will never go undef, no matter how large or small it is.  
;;
;; ref:
;;
;; get(arr,i)
;; range(arr)
"];

function fidx(o,i,cycle=false, fitlen=false)=
let( L=isint(o)?o:len(o), j = cycle? mod(i,L):i )
(
	/*
	len: 1 2 3 4 5
		 0 1 2 3 4   i>=0
   		-5-4-3-2-1   i <0    

  				   0 1 2 3 4   : legal index
  		-5-4-3-2-1 0 1 2 3 4   : proper index

	When out of range: 

		if cycle=false: return false
		if cycle=true: 
			cycle through array as many as needed. So if cycle is set
					to true, it never returns false.  

	*/

	-L<=j&&j<L 
	? ( (j<0? L:0)+j )
	:fitlen==true?(L-1):undef
);


//========================================================
flatten=["flatten", "a,d=undef", "arr", "Core, array",
,"Flatten array a. d: depth. If d=undef (default), 
;; flatten all; if d=0, return a.
"];

function flatten(a, d=undef)=
( 
  d==undef || d>0?
  [for(x=a) 
      for( b= (str(x)==x) || len(x)==undef? x:flatten(x, d-1)  ) 
      b ]
  :a 
);

      
//========================================================
_fmth=["_fmth" ,"h,pairbreak='' , '',keywrap=''<b>,</b>'',valwrap=''<u>,</u>'', pairwrap='','',eq=''='',iskeyfmt=false,sectionhead=''####'',comment=''////''" 
	   ,"string","String, Doc"
,"Return a formatted hash. Usually used with echo().
;;
;;  keywrap,valwrap,pairwrap: strs wrap around each key,val or pair,
;;     respectively.
;;  eq: symbol between key and val
;;  iskeyfmt: format key if true
;;  sectionhead, comment: if key is a sectionhead or comment, convert
;;     the pair to section head or comment, respectively
;;
;; The return is a single line of string. Setting pairbreak to &lt;br/&gt;
;; will break the line to lines of k-v pairs. 
;;
"];

function _fmth(h
//              , wrap="[ , ]"
//              , pairbreak=" , "
//			  , keywrap="<b>,</b>"
//			  , valwrap="<i>,</i>"
//              , pairwrap=","
//              , eq = "="
//              , iskeyfmt= false
//              // New 2015.3.16: if key is sectionhead or comment, convert pair
//              // to section head and comment, respectively
//              , sectionhead= "###"
//              , comment= "///"
              , _i=0
              , _rtn=""
              
			  )=
(  
    let( 
         newrtn = str( _rtn, "<b>", h[_i],"</b>="
                           , _fmt( h[_i+1]), _i<len(h)-2?", ":"")
       , newi = _i+2                    
       )
    _i>len(h)-2? str("[", _rtn,"]")
    : _fmth( h, _i=newi, _rtn=newrtn)    

);     
     
//echo( _fmth( ["num",23,"str","test", "arr", [2,"a"]] ) );

       
//function _fmth(h, wrap="[ , ]"
//              , pairbreak=" , "
//			  , keywrap="<b>,</b>"
//			  , valwrap="<i>,</i>"
//              , pairwrap=","
//              , eq = "="
//              , iskeyfmt= false
//              // New 2015.3.16: if key is sectionhead or comment, convert pair
//              // to section head and comment, respectively
//              , sectionhead= "###"
//              , comment= "///" 
//			  )=
//(  let( _pre= split(wrap,",")
//      , post= _pre[1]
//      , pre = _pre[0]
//      )
//   str( pre,   
//   join(
//       [ for( ki= [0:2:len(h)-1] ) 
//             let( _k = h[ki]
//                , _v = h[ki+1]
//                , k = replace(keywrap,",", iskeyfmt?_fmt(_k):_k)
//                , v = replace(valwrap,",", _fmt(_v)) 
//                , eq= _green(eq)
//                , pair= _k==sectionhead? _u(_b(str("   [ ",_v," ]   ")))
//                       : _k==comment? 
//                              _span(str("| ", replace(_v," ","&nbsp;"))
//                                   , s="color:#222;font-size:10px;font-family:courier new")
//                       : replace( pairwrap, ",", str(k,eq,v) )
//                )
//       // [_k,_v,k,v,eq]
//             pair
//       ]
//   , sp = pairbreak
//   )
//   ,post)
//);     
//     



//========================================================
_fmt=["_fmt", "x,[s,n,a,ud,fixedspace]", "string", "String, Doc, Console" 
," Given an variable (x), and format settings for string (s), number (n),
;; array (a) or undef (ud), return a formatted string of x. If fixedspace
;; is set to true, replace spaces with html space. The formatting is
;; entered in ''(prefix),(suffix)'' pattern. For example, a ''{,}'' converts
;; x to ''{x}''.
"
];


function _fmt(x, s=_span("&quot;,&quot;", "color:purple") 
			, n=_span(",", "color:brown") //magenta")//blueviolet")//brown")
			, a=_span(",", "color:navy")//midnightblue")//cadetblue")//mediumblue")//steelblue") //#993300") //#cc0060")  
			, ud=_span(",", "color:brown") //orange") 
			//, level=2
			, fixedspace= true
		   )=
(
	x==undef? replace(ud, ",", x) 
	:isarr(x)? replace(a, ",", replace(str(x),"\"","&quot;")) 
	: isstr(x)? replace(s,",",fixedspace?replace(str(x)," ", "&nbsp;"):str(x)) 
		:isnum(x)||x==true||x==false?replace(n,",",str(x))
				:x
);



//_fmt_demo();
//doc(_fmt);
//accumulate_test();




//========================================================
ftin2in=["ftin2in","ftin","number","Unit, len",
"Given a ftin ( could be a string like 3`6``, or an array like [3,6], 
;; or a number 3.5, all for 3 feet 6 in ), convert to inches.
;;
;;   ftin2in( 3.5 )= 42
;;   ftin2in( [3,6] )= 42
;;   ftin2in( 3`6`` )= 42
;;   ftin2in( 8` )= 96
;;   ftin2in( 6`` )= 6
;;
"];

function ftin2in(ftin)=
(
	isnum(ftin)
	? ftin*12
	: isarr(ftin)
	  ? ftin[0]*12+ftin[1]
	  : endwith(ftin,"'")
		? num( replace(ftin,"'",""))*12

	    : index(ftin,"'")>0
		   ? num( split(ftin,"'")[0] )*12 
			+ num( replace(split(ftin,"'")[1],"\"",""))
		   : num( replace(ftin,"\"",""))
);


//========================================================
get=["get", "o,i,cycle=false", "item", "Index, Inspect, Array, String" ,
 " Given a str or arr (o), an index (i), get the i-th item. 
;; i could be negative.
;;
;; New argument *cycle*: cycling from the other end if out of range. This is
;; useful when looping through the boundary points of a shape.
;;
;;   arr= [20, 30, 40, 50]; 
;;   get( arr, 1 )= 30
;;   get( arr, -2 )= 40
;;   get( ''abcdef'', -2 )= ''e''
;;   //New 2014.6.27: cycle
;;   get( arr, 4 )= undef
;;   get( arr, 4, cycle=true )= 20
;;   get( arr, 10 )= undef
;;   get( arr, 10, cycle=true )= 40
;;   get( arr, -5 )= undef
;;   get( arr, -5, cycle=true )= 50
"];

function get(o,i, cycle=false)= 
( 
	o[ fidx(o,i,cycle=cycle) ]	
);


//========================================================
getcol=[ "getcol","mm,i=0", "array", "Array, Index",
 " Given a matrix and index i, return a 1-d array containing
;; item i of each matrix row (i.e., the column i of the multi
;; matrix. The index i could be negative for reversed indexing.
"];
				
function getcol(mm,i=0)=
(
	[ for(row=mm) get(row,i) ]
);


//========================================================
function _getdeci(n)= index(str(n),".")<0?0:len(split(str(n),".")[1]);


//========================================================
getSideFaces=["getSideFaces","topIndices, botIndices, isclockwise=true","array", "Faces"
, "Given topIndices and botIndices (both are array
;; of indices), return an array of indices for the 
;; side faces arguments for the polyhedron() function
;; to create a column object (note that the top
;; and bottom faces are not included).
;; isclockwise: true if looking down from top to 
;; bottom, the order of indices is clockwise. Default
;; is true.
;; 
;;  clockwise (default)
;;       _2-_
;;    _-' |  '-_
;;   1_   |    '-3
;;   | '-_|   _-'| 
;;   |    '-0'   |
;;   |   _6_|    |
;;   |_-'   |'-_ | 
;;   5 _    |    7
;;     '-_  | _-'
;;        '-4' 
;;
;;  => [ [0,3,7,4], [1,0,4,5], [2,1,5,6],[3,2,6,7]]
;;
;;  clockwise = false
;;       _2-_
;;    _-' |  '-_
;;   3_   |    '-1
;;   | '-_|   _-'| 
;;   |    '-0'   |
;;   |   _6_|    |
;;   |_-'   |'-_ | 
;;   7 _    |   '5
;;     '-_  | _-'
;;        '-4' 
;;
;;  => [ [3,0,4,7], [2,3,7,6], [1,2,5,6],[0,1,5,4] ]
;; [[0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]] 
"];
/* //"
//*/
function getSideFaces(topIndices
                    , botIndices
                    , clockwise=true)=
let( ts= topIndices
   , bs= botIndices
   , tlast = last(ts)
   , blast = last(bs)
   )
(
  clockwise
  ? [for (i=range(ts))
      i==0
      ? [ ts[0], tlast, blast, bs[0] ]
      : [ ts[i], ts[i-1], bs[i-1], bs[i] ]
     ]
  : [for (i=range(ts))
      i<len(ts)-1
      ? [ ts[i], ts[i+1], bs[i+1], bs[i] ]
      : [ tlast, ts[0], bs[0], blast ]
      ]
);



//========================================================
getsubops= ["getsubops","ops, com_keys, sub_dfs","hash or false", "args"
 ," Define the options of multiple sub objs for situations like :
;;
;;   obj.arms.armL
;;   obj.arms.armR
;;
;; in which you want a module Obj that allows for:
;;
;;  Obj([''arms'',false]) ==> disable arms
;;  Obj([''armL'',true) ==> enable both with default arm properties.
;;  Obj([''arms'',[''r'',1]) ==> set both arm properties at once
;;  Obj([''armL'',[''r'',2]) ==> set armL properties
;;  Obj([''arms'',[''r'',1], ''armL'',false) ==> set all arms, but disable armL
;;  Obj([''arms'',[''r'',1], ''armL'',true) ==> set all arms, but armL uses default
;;  Obj([''arms'',[''r'',1], ''armL'',[''r'',2]) ==> set all arms, but override armL
;;
;;    module Obj( ops=[] )
;;    {
;;        //---------------------- default
;;        df_arms=[''len'',3];
;;        df_armL=[''transp'',0.6];
;;        df_armR=[];
;;        df_ops= [''arms'', true
;;               , ''armL'', true
;;               , ''armR'', true
;;               ];
;;        com_keys= [''transp'',''r'']j;     
;;        //----------------------- user
;;        u_ops = ops;   
;;        //_ops= update(df_ops, ops);
;;        //function ops(k,df)= hash(_ops,k,df);
;;        //----------------------- compose func
;;        function subops(sub_dfs)=
;;           getsubops( u_ops  
;;                    , df_ops = df_ops
;;                    , com_keys= com_keys
;;                    , sub_dfs= sub_dfs
;;                    );
;;        ops_armL = subops([''arms'', df_arms, ''armL'', df_armL ]);
;;        ops_armR = subops([''arms'', df_arms, ''armR'', df_armR ] );
;;        function ops_armL(k,df) = hash(ops_armL,k,df);
;;        function ops_armR(k,df) = hash(ops_armR,k,df);
;;        //----------------------- use func
;;        if(ops_armR){ //use ops_armR(''color''), ops_armR(''len'') )...}
;;        if(ops_armL){ //use ops_armL(''color''), ops_armL(''len'') )...}
;;    }  
"
  //," Define the options of multiple sub objs. The scenario is that an obj O 
//;; can have n common subobjs, such as left-arm, right-arm. They belong to
//;; ''arms''. We want :
//;;
//;; 1. Both arms can be turned off by O[''arms'']=false
//;; 2. Common arm props can be set on the obj level by
//;;    O[''r'',1] that applies to entire obj including other parts
//;; 3. Common arm props can also be set on the obj level by
//;;    by O[''arms'']=[''r'',2] that will only apply to arms. 
//;; 4. Each arm can be turned off by O[''leftarm'']=false
//;; 5. Each arm can be set by O[''leftarm'']= [''r'',1]
//;;
//;; ops: obj-level ops 
//;; subs: a hash of 2 k-v pairs: [''arms'', df_arms, ''armL'', df_armL]
//;;       ''arms'': name used in ops as ops=[ ... ''arms'', true ...]
//;;       df_arms : hash for default arms 
//;;       ''armL'': name  used in ops as ops=[ ... ''armL'', true ...]
//;;       df_armL : hash for default left arm
//;; com_keys: like ''len'',''color''. This is the common keys that
//;;           will set on entire obj and also carries over to all arms
//;;
//;; NOTE: com_keys SHOULD shows up in ops but NEVER in df_arms or df_armL
//"
];

function getsubops( 
      u_ops
    , df_ops=[] // default ops given at the design time
    , sub_dfs   // a hash showing layers of sub op defaults
                // ["arms", df_arms, "armL", df_armL]
                // The 1st k-v pair, ("arms",df_arms) is the group [name
                // , default], to which the next ("armL",df_armL), belongs.
    , com_keys  // names of sub ops, which you want them to be also be set 
                // from the obj level. Ex,["r", "color"]
    )=
(  let(subnames = keys( sub_dfs ) // name of subunit   ["arms", "armL"]
      ,df_subs  = vals( sub_dfs ) // df opt of subunit 
                                  // [df_arms, df_armL, df_armR]
      ,df_com    = com_keys?[ for(i=range(df_ops)) // part of user ops that have 
                                           // keys showing up in com_keys
                      let( key= df_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) df_ops[i]
                  ]:[] // ["r",1] 
      ,u_com    = com_keys?[ for(i=range(u_ops)) // part of user ops that have keys 
                                      // showing up in com_keys
                      let( key= u_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) u_ops[i]
                  ] // ["r",1]
                  :[]    
      ,u_subops = [ for(sn= subnames)  // Extract subops settings from 
                    hash(u_ops,sn) ]     // user input ops ops, like 
                                       // ["arms",t
//      ,u_subops = [ for(sn= subnames)  // Extract subops settings from 
//                    hash(ops,sn) ]     // user input ops ops, like 
//                                       // ["arms",true, "armL",["r",1]]
      // order of reversed priority:
      // [  df_ops, df_arms, df_armL, u_com,u_arms, u_armL ] 
      ,subops = updates(
          df_com
        , [ df_subs[0]
          , df_subs[1]
          , u_com
          , u_subops[0]==true? []:ishash(u_subops[0])?u_subops[0]:false
          , u_subops[1]==true? []:ishash(u_subops[1])?u_subops[1]:false
          ])

       ,show= sum( [ for(k=subnames) hash(u_ops,k)==false?1:0])==0
       //,show = [ for(k=subnames) if(hash(u_ops,k)) 1]
       )
       //_//df_ops
       //[ for(s=u_subops) s==false?1:0] //u_subops
    show? subops 
        : false
);
    
       
function getsubops_debug( 
      u_ops
    , df_ops=[] // default ops given at the design time
    , com_keys  // names of sub ops, which you want them to be also be set 
                // from the obj level. Ex,["r", "color"]
    , sub_dfs   // a hash showing layers of sub op defaults
                // ["arms", df_arms, "armL", df_armL]
                // The 1st k-v pair, ("arms",df_arms) is the group [name
                // , default], to which the next ("armL",df_armL), belongs.
    )=
  let(subnames = keys( sub_dfs ) // name of subunit   ["arms", "armL"]
      ,df_subs  = vals( sub_dfs ) // df opt of subunit 
                                  // [df_arms, df_armL, df_armR]
      ,df_com    = [ for(i=range(df_ops)) // part of user ops that have 
                                           // keys showing up in com_keys
                      let( key= df_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) df_ops[i]
                  ] // ["r",1] 
      ,u_com    = com_keys?[ for(i=range(u_ops)) // part of user ops that have keys 
                                      // showing up in com_keys
                      let( key= u_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) u_ops[i]
                  ] // ["r",1]
                  :[]    
      ,u_subops = [ for(sn= subnames)  // Extract subops settings from 
                    hash(u_ops,sn) ]     // user input ops ops, like 
                                       // ["arms",true, "armL",["r",1]]
      // order of reversed priority:
      // [  df_ops, df_arms, df_armL, u_com,u_arms, u_armL ] 
      ,subops = updates(
          df_com
        , [df_subs[0]
          , df_subs[1]
          , u_com
          , u_subops[0]==true? []:ishash(u_subops[0])?u_subops[0]:false
          , u_subops[1]==true? []:ishash(u_subops[1])?u_subops[1]:false
          ])

       ,show= sum( [ for(k=subnames) hash(u_ops,k)==false?1:0])==0
       //,show = len([ for(k=subnames) if(hash(u_ops,k)!=false) 1])>0
       )
(  _fmth(
     [
       "u_ops", u_ops
      ,"df_ops",df_ops
      ,"com_keys", com_keys
      ,"sub_dfs", sub_dfs
       
      ,"subnames", subnames // name of subunit   ["arms", "armL"]
      ,"df_subs",  df_subs // df opt of subunit 
                           // [df_arms, df_armL, df_armR]
      ,"df_com",  df_com 
      ,"u_com",  u_com    
      ,"u_subops", u_subops  // Extract subops settings from 
                             // user input ops ops, like 
                                       // ["arms",true, "armL",["r",1]]
      // order of reversed priority:
      // [  df_ops, df_arms, df_armL, u_com,u_arms, u_armL ] 
      ,"subops",  subops
      ,"show",  show
     ] 
     , pairbreak="<br/>,"  
    )   
);    


//========================================================
getTubeSideFaces=["getTubeSideFaces","indexSlices, clockwise=true","array", "faces"
, "Given an array of indexSlices (an indexSlice
;; is a list of indices representing the order 
;; of points for a circle), return an array of 
;; faces for running polyhedon(). Typically
;; usage is in a column or tube of multiple
;; segments. For example, CurvedCone.
;;
;; See getSideFaces that is for tube of only
;; one segment. 
"];  
function getTubeSideFaces(
         indexSlices, clockwise=true)=  
  let( ii = indexSlices )
( 
    //mergeA( 
      [ for (i=range(len(ii)-1))
        getSideFaces( topIndices= ii[i]
                    , botIndices= ii[i+1]   
                    , clockwise=clockwise
                    )
      ]
    //)
);




//========================================================
getuni= ["getuni","s,blk=['(',')'],keep=1,sp=',',prefix='#/'", "array", "Array"
, " Given a str representing a nested, multiple-block array, parse it 
;; to a data structure called *uni*, an arr of one-dimensional arrs:
;;
;;  getuni( '(x+1)*(y+2)' )
;;  = [['#/1', '*', '#/2'], ['x', '+', 1], ['y', '+', 2]]
;; 
;; blk: define block head and tail, default=['(,')']
;;      It could also be: ['(,')', '[,']']
;;      or with func name: ['(,')', 'sin(,')']
;; keep: usually block head/tails are not shown. But in cases like 
;;   ['sin(,')'], the name 'sin' must be reserved. 
;;   //  keep=0: [ [#/1,+,1], [x]     ]  
;;   //  keep=1: [ [#/1,+,1], [sin,x] ] 
;; sp: ',' 
;; prefix: '#/', block label prefix 
;;
;; Useby: packuni() 
"];

//========================================================


/*
"(x+1)*((y+2)*3)" 

Critical indices: 
    bi : current block is added to block i
    xi : current x is added to block i
    
"_c,c=",["(","("],"b,e=",["("," "],bi=1,"(_ly,nly)=[0, 1]","_rtn=",[[]],"
"_c,c=",["x","x"],"b,e=",[" "," "],xi=1,"(_ly,nly)=[1, 1]","_rtn=",[["#/1"], []],"
"_c,c=",["+","+"],"b,e=",[" "," "],xi=1,"(_ly,nly)=[1, 1]","_rtn=",[["#/1"], ["x"]],"
"_c,c=",["1", 1], "b,e=",[" "," "],xi=1,"(_ly,nly)=[1, 1]","_rtn=",[["#/1"], ["x","+"]],"
"_c,c=",[")",")"],"b,e=",[" ",")"],---- "(_ly,nly)=[1, 0]","_rtn=",[["#/1"], ["x","+", 1]],"
"_c,c=",["*","*"],"b,e=",[" "," "],xi=0,"(_ly,nly)=[0, 0]","_rtn=",[["#/1"], ["x","+", 1]],"
"_c,c=",["(","("],"b,e=",["("," "],bi=2,"(_ly,nly)=[0, 1]","_rtn=",[["#/1","*"], ["x","+", 1]],"
"_c,c=",["(","("],"b,e=",["("," "],bi=3,"(_ly,nly)=[1, 2]","_rtn=",[["#/1","*","#/2"], ["x","+", 1], []],"
"_c,c=",["y","y"],"b,e=",[" "," "],xi=3,"(_ly,nly)=[2, 2]","_rtn=",[["#/1","*","#/2"], ["x","+", 1, "#/3"], [], []],"
"_c,c=",["+","+"],"b,e=",[" "," "],xi=3,"(_ly,nly)=[2, 2]","_rtn=",[["#/1","*","#/2"], ["x","+", 1, "#/3"], [], ["y"]],"
"_c,c=",["2", 2], "b,e=",[" "," "],xi=3,"(_ly,nly)=[2, 2]","_rtn=",[["#/1","*","#/2"], ["x","+", 1, "#/3"], [], ["y","+"]],"
"_c,c=",[")",")"],"b,e=",[" ",")"],---- "(_ly,nly)=[2, 1]","_rtn=",[["#/1","*","#/2"], ["x","+", 1, "#/3"], [], ["y","+", 2]],"
"_c,c=",["*","*"],"b,e=",[" "," "],xi=2"(_ly,nly)=[1, 1]","_rtn=",[["#/1","*","#/2"], ["x","+", 1, "#/3"], [], ["y","+", 2]],"
"_c,c=",["3", 3], "b,e=",[" "," "],xi=2"(_ly,nly)=[1, 1]","_rtn=",[["#/1","*","#/2"], ["x","+", 1, "#/3"], [], ["y","+", 2, "*"]],"
"_c,c=",[")",")"],"b,e=",[" ",")"],----"(_ly,nly)=[1, 0]","_rtn=",[["#/1","*","#/2"], ["x","+", 1, "#/3"], [], ["y","+", 2, "*", 3]],"
"_c,c=",[undef, undef],"b,e=",[" "," "],"(_ly,nly)=[0, 0]","_rtn=",[["#/1","*","#/2"], ["x","+", 1, "#/3"], [], ["y","+", 2, "*", 3]]] 
*/

/*

"a,(b,(c,d))" ===> [["a", "#/1"], ["b", "#/2"], ["c", "d"]] ] 

    a, ( b, ( c, d ) )
L     #1   #2               "#/"+len(_rtn)
bi     0    1 
xi  0    1    2  2 - -     the i-th block the content app to
Ly  0  + 1  + 2    - -  0  = Ly + (b?1:e?-1:0)

    0  0 0  0 0    
       1 1  1 1
            2 2

    bi_#1 =0 // always 0
    
    
"(a,x,(b, (c,d)))" => [["#/1"], ["a", "x", "#/2"], ["b", "#/3"], ["c", "d"]] ] 

    ( a, x, ( b, ( c,d ) ) )
Li #1      #2   #3  
bi  0       1    2
xi    1  1    2    3 3 - - - 
Ly  + 1  !  + 2  + 3 3 - - -  0      

    1 1  1  1 1  1  
            2 2  2
                 3  



"(x+1)*(y+2)" => [["#/1", "*", "#/2"], ["x", "+", 1], ["y", "+", 2]] ]

    ( x + 1 ) * ( y + 2 )
Li #1          #2 
bi  0           0
xi    1 1 1   0   2 2 2 
Ly  + 1 1 1 - 0 + 1 2 2 - 0

    1 1 1 1 1 0 2 2 2 2 2
    


"(x+1)*((y+2)*3)" => [ [#/1, *, #/2], [x,+,1], [ #/3, *, 3 ], [y,+,2] ]

   "( x + 1 ) * (  (  y + 2 ) * 3 )" 
Li #1          #2 #3                  Li: index of block where new block label append to 
bi  0           0  2                  bi: index of block where new block to be created (append [] to)
xi    1 1 1   0       3 3 3   2 2     xi: index of block where new content append to
Ly  + 1 1 1 - 0 + 1+  2 2 2 - 1 1 - 0

    1 1 1 1 1 0 2  2  2 2 2 2 2 2 2
                      3 3 3 3  

"((y+2)*3)*(x+1)" => [["#/1", "*", "#/2"], ["#/3","*",3], ["y", "+", 2], , ["x", "+", 1] ] ]

    ( ( y + 2 ) * 3 ) * ( x + 1 )
Li #1 #2               #3
bi  0 1                 0
xi      2 2 2   1 1   0   3 3 3 
Ly  + + 2 2 2 - 1 1 - 0 + 1 3 3 - 0

    1 1 1 1 1 1 1 1 1 0 3 3 3 3 3  
      2 2 2 2 2        

"(x+1)*((y+2)*3)" => [["#/1", "*", "#/2"], ["x", "+", 1], ["#/3","*",3],["y", "+", 2]] ]

    ( x + 1 ) * ( ( y + 2 ) * 3 )
Li #1          #2#3  
bi  0           0 2
xi    1 1 1   0     3 3 3   2 2 
Ly  + 1 1 1 - 0 + + 2 3 3 - 1 1 - 0 
 
    1 1 1 1 1 0 2 2 2 2 2 2 2 2 2
                  3 3 3 3 3 

 
"a+(b*( 2- (c+d)^2) ) * (x/(y+1))+ 5" 
                 
    a + ( b * ( 2 - ( c + d ) ^ 2 ) ) * ( x / ( y + 1 ) ) + 5
Li     #1    #2    #3                  #4    #5
bi      0     1     2                   0     4
xi  0 0   1 1   2 2   3 3 3   2 2     0   4 4   5 5 5     0 0
Ly  0 0 + 1 1 + 2 2 + 3 3 3 - 2 2 - - 0 + 1 1 + 2 2 2 - - 0 0
    
    1 2 3 4 5 6 7 8 9 011121314151617181920212223242526272829 
    0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
        1 1 1 1 1 1 1 1 1 1 1 1 1 1 1   4 4 4 4 4 4 4 4 4 
              2 2 2 2 2 2 2 2 2 2 2           5 5 5 5 5 
                    3 3 3 3 3  
                 
0 [0]
1 [0]
2 [0]
3 [0,1]

19: [0,4]
22  [0,4,5]

                 
                 
Ly = Ly + (b?1:e?-1:0)

                 
L = str(prefix, len(_rtn))
bi= 




*/
function getuni( 
     s
   , blk = ["(",")"]
   , keep=true // true: keep func name "sin(x)+1"
               //       [ [#/1,+,1], [x]     ]  keep=0 
               //       [ [#/1,+,1], [sin,x] ]  keep=1
   , sp  = "," // separator.: (a,x,(b, (c,d))) => 
               // => [["#1"], ["a", "x", "#2"], ["b", "#3"], ["c", "d"]] ]
   , prefix="#/"  
   
   , _rtn= [[]] // init return, containing the root block. Each item is an array representing a block
   , _lys= [0]  // indeice of Layer of nested blocks. 
                //  [0,4,5] means, current block is the 5th blk inside the 4th one on the root level
                 
   , _debug=[] 
   
   )= let ( 
          bs= keys( blk )      // ["(", "[", "sin(" ...]
        , es= vals( blk )      // [")", "]", ")" ...]
        , b = begwith( s, bs)  // beg ("(", ...) | undef
        , e = begwith( s, es)  // end (")", ...) | undef
        
         // The w and n make sure var names like a_top (more than one letter), x0,y12 
         //  (starting with alphabet, ends with number) ... can be detected and parsed.
                                                   
        , w = begword( s )     // beginning word | "". 
        , n = begword( s, isnum=true)  // beginning digits | ""
        
        , nx= begword( slice( s, len(n) ))  // For the 3x in 3x + 2
                                            // => [ "3x","+",2]
        
        , _c = b?b:e?e:nx?str(n,nx):w?w:n?n:s[0]     // current item
        , ns= slice(s,len(_c))  // next s
        
        , nc = num(_c)   // Is this _c a number
        , c =  nc?nc:_c   // convert if it is
        
        , nlys = b? concat( _lys, len(_rtn))   // Add new block index
                 :e? slice( _lys,0, -1)        // Reduce block indices
                 : _lys                        
        
        , nrtn= e?_rtn   // no change to _rtn if a block end
                :b ? concat(
                             // Append new block label (#/?) to the block 
                             // indexed by the last in _lys
                             appi( _rtn, last(_lys), str(prefix, len(_rtn)) )
                             // Append new block to rtn. New block [] is ALWAYS appended 
                             // to the last. If function name in the block head, like 
                             // "sin(" and keep=true, store "sin" to the new block
                           , [ keep&& len(b)>1         
                                 ? [ slice(b,0,-1)]      
                                 : []
                             ] 
                    )
                :  ( c==sp||c==" "? _rtn                 // skip sp and space
                            : appi(_rtn, last(_lys), c)  // append c to rtn
                   ) 
        
        , _debug= concat(_debug
              , ["<br>///", 
                //  , "ns=", ns
                // , "n=", n
                , "_c,c=",[_c,c]
                
                ,"b,e=", [b?b:" ", e?e:" "]
                ,,str("(_lys,nlys)=", [_lys,nlys])
                ,"_rtn=", _rtn
                // ,"nrtn=", nrtn
                ]
            )
        )
(
    len(s)>0 ?
         getuni( ns
               , blk=blk
               , keep=keep
               , sp = sp
               , prefix=prefix
               , _lys=nlys
               , _rtn=nrtn 
               , _debug=_debug
               )
    : _rtn
    //: _debug                      
);  
      
//========================================================
_h= [ "_h", "s,h, showkey=false, wrap='{,}'", "string", "Hash, Doc",
"
 Given a string (s), a hash (h) and a wrapper string like {,},
;; replace the {key} found in s with the val where [key,val] is
;; in h. If showkey=true, replace {key} with key=val. This
;; is useful when echoing a hash:
;;
;; _h( '{w},{h}', ['h',2,'w',3])=> '3,2'
;; _h( '{w},{h}', ['h',2,'w',3], true)=> 'w=3,h=2'
"];

function _h(s,h, showkey=false, wrap="{,}",isfmt=true, _i_=0)= 
( 
	len(h)<2?s
	:_i_>len(h)?s
	  :_h(
		  replace(s, replace(wrap,",",h[_i_])
				, str( showkey?h[_i_]:"", showkey?"=":""
					, h[_i_+1] //isfmt?_fmt(h[_i_+1]):h[_i_+1] 
					)
				)
		 , h
		 , wrap= wrap
		 , showkey= showkey
		 , _i_=_i_+2
		 )
);


//========================================================
has=["has","o,x","T|F","String, Array, Inspect",
" Given an array or string (o) and x, return true if o 
;; contains x. 
;;
;;   has( 'abcdef','d' )= true
;;   has( 'abcdef','m' )= false
;;   has( [2,3,4,5],3 )= true
;;   has( 'abcdef',['d','y'] )= true 
;;
;; 2015.3.11: non-arr and non-str gets false
;;
;;   has(false,...) = false   
;;   has(3,...) = false   
"];
 
function has(o,x)= 
    !(isarr(o)||isstr(o))?false // new 2015.3.11
    :isarr(x)? len([ for(a=x) if (index(o,a)>-1) 1])>0
    :index(o,x)>-1;



////========================================================
//hasAny=["hasAny", "o1,o2", "T|F", "Inspect, Array, String",
//" Given two arrays/string, o, o2, return true if and only
//;; if any of items in one appears in another. Same as
//;; len(arrItcp(o1,o2))>0, but arrItcp is for arr only.
//"];
//
//function hasAny(o1,o2)= 
//(
//	len( [for(i=range(o2)) if(index(o1,o2[i])>-1) o2[i] ] )>0
//);




//========================================================
hash=[ "hash", "h,k, notfound=undef", "Any","Hash",
" Given an array (h, arranged like h=[k,v,k2,v2 ...] to serve
;; as a hash) and a key (k), return a v from h: hash(h,k)=v.
;; If key not found, or value not found (means k is the last item
;; in h), return notfound.
;;
;; New 2015.4.20: use list comprehension (instead of recursion)
"];

function hash(h,k, notfound=undef)= 
(
  let( rtn= [ for(i=[0:2:len(h)-2])
             if( h[i]==k ) h[i+1] ]
     )
     len(rtn)?rtn[0]:notfound
);

//========================================================
//hash=[ "hash", "h,k, notfound=undef, if_v=undef, then_v=undef, else_v=undef", "Any","Hash",
//" Given an array (h, arranged like h=[k,v,k2,v2 ...] to serve
//;; as a hash) and a key (k), return a v from h: hash(h,k)=v.
//;; If key not found, or value not found (means k is the last item
//;; in h), return notfound. When if_v is defined, check if v=if_v.
//;; If yes, return then_v. If not, return else_v if defined, or v
//;; if not.
//"];             
//function hash(h,k, notfound=undef, if_v=undef, then_v=undef, else_v=undef, _i_=0)= 
//(
//	!(isarr(h)||isstr(h))? notfound      // not a hash
//	:_i_>len(h)-1? notfound  // key not found
//		: h[_i_]==k?        // key found
//			 (_i_==len(h)-1?notfound  // value not found
//				: (if_v==undef    
//					? (h[_i_+1]==undef?notfound:h[_i_+1])     
//                            // if v=undef, return notfound; else v 
//					: h[_i_+1]==if_v  // else, ask if v==if_v
//						? then_v  	  // if yes, return if_v  
//						:( else_v==undef    // else, ask if else_v defined
//								?h[_i_+1]  // if no, return v
//								:else_v)   // if yes, return else_v 
//				  )	 
//			)
//			: hash( h=h, k=k, notfound=notfound, if_v=if_v, then_v=then_v
//				  , else_v=else_v, _i_=_i_+2)           
//);

//doc(hash );

//========================================================
hashex= ["hashex", "h,keys", "hash", "Hash",
"Extract a sub hash from a hash based on given keys"
];

function hashex(h, keys)=
    (joinarr( [for(k=keys) 
                let( v=hash(h,k) )
                if(v) [k,v] ] )
    );
////  let( hh = [for(k=keys) [k, hash(h,k)] ] )
////  [ for(kv=hh, x=kv) x ]   ;
      
  
//========================================================
hashkvs=["hashkvs", "h", "array of arrays", "Hash",
"
 Given a hash (h), return [[k1,v1], [k2,v2] ...].
"];

function hashkvs(h)=
(
	[for(i=[0:2:len(h)-2]) [h[i],h[i+1]]]
);




//========================================================
haskey=[ "haskey", "h,k", "T|F", "Hash, Inspect",
"
 Check if the hash (h) has a key (k)
"];

function haskey(h,k)=
(
	index(keys(h),k)>=0
);



//========================================================
incenterPt=["incenterPt", "pqr", "point", "Angle, Point"
,
 " Given a 3-pointer (pqr), return the incenter point of the triangle, which
;; is where all 3 angle bisector lines meet."
];

function incenterPt(pqr)=
(
	lineCrossPt( pqr[0], angleBisectPt( rearr(pqr, [1,0,2]) )
			  , pqr[1], angleBisectPt( pqr )
			  )
);

        
          
//========================================================
idx=["idx", "o,x", "int", "Index,String",
"Return the index of x in o (str|arr), or -1 if not found.
;; 
;; Compare to index:
;; 
;;  Unlike index, idx does`t use slice() and join() for each 
;;  possible char check in s. Should be faster than index when 
;;  x is a word but not a single char.
"
,"index"
];           
function idx( o,x )=
(    
   let( isstr= isstr(o)
      , rtn = isstr && len(x)>1  // string and x is word
            ? [ for (i=range(o))
                if( (o[i]==x[0]) &&          // Match the first char 
                    (![ for(xi=[1:len(x)-1]) // Match the following 
                         if( o[i+xi]!=x[xi] )1  // len(x)-1 char in x
                      ]) 
                  ) i // Return i only when both match        
              ][0]
            : [ for(i=range(o)) if(o[i]==x) i ][0]        
      )
   rtn==undef?-1:rtn
);           
 
//========================================================
index=["index", "o,x", "int", "Index,Array,String",
" Return the index of x in o (a str|arr), or -1 if not found.
;;
;;   arr= ['xy', 'xz', 'yz']; 
;;   index( arr, 'yz' )= 2
;;   index( arr, 'xx' )= -1
;;
;;   index( 'I_am_the_findw', 'am' )= 2
","idx"
];	 

function index(o,x)=  
(
  let(rtn= x==""? undef
           : isarr(o)? [ for(i=range(o)) if(o[i]==x)i ][0]
           : isstr(o)? [ for(i=range(o)) if( slice(o,i,len(x)+i)==x) i ][0]
           : undef )
   rtn==undef?-1:rtn
);            
////========================================================
//index=["index", "o,x, wrap=undef", "int", "Index,Array,String",
//" Return the index of x in o (a str|arr), or -1 if not found.
//;;
//;;   arr= ['xy', 'xz', 'yz']; 
//;;   index( arr, 'yz' )= 2
//;;   index( arr, 'xx' )= -1
//;;
//;;   index( 'I_am_the_findw', 'am' )= 2
//;;
//;; Use wrap: 
//;;
//;;   index( 'a{b}c','b', '{,}' )= 1
//;;   index( 'a{_b_}{_c_}','c', '{_,_}' )= 6
//"];	 
//
//function index(o,x, wrap=undef, _i_=0)=  
// let( x  = wrap?replace(wrap,",",x):x
//	, oi = isstr(o)
//		   ? slice(o, _i_, _i_+len(x))
//		   : o[_i_]
//    , end= _i_==len(o)
//	, found= isnum(x)? isequal(oi,x): oi== x 
//    )
//(
//    x=="" || end ?-1
//	: found 
//	  ? _i_
//	  : index(o,x,undef,_i_+1)
//);



//========================================================
inrange=[ "inrange", "o,i", "T|F", "Index, Array, String, Inspect",
" Check if index i is in range of o (a str|arr). i could be negative.
;;
;; ## string ##
;;   inrange( ''789'',2 )= true
;;   inrange( ''789'',-2 )= true
;;   inrange( ''789'',-4 )= false
;; ## array ##
;;   inrange( [7,8,9],2 )= true
;;   inrange( [7,8,9],-2 )= true
;;   inrange( [7,8,9],-4 )= false
"];

function inrange(o,i)=
(
	/*
	len: 1 2 3 4 5
		 0 1 2 3 4   i>=0
   		-5-4-3-2-1   i <0    
	*/
	
	isint(i)? ( (-len(o)<=i) && ( i<len(o)) )
            : false

);



//========================================================
int=[ "int", "x", "int", "Type, Number",
" Given x, convert x to integer. 
;;
;; int(''-3.3'') => -3
"];

function int(x,_i_=0)=
(
	isint(x)? x
	:isfloat(x)? sign(x)*round(abs(x))
	 :isarr(x)? undef
	  :x=="" || x[0]=="."? 0 //index(x,".")==0? 0
		: ( _i_==0 && x[0]=="-"? 
			-int(x,_i_+1)
			:(_i_< min( len(x),
					  index(x,".")<0?
					  len(x):index(x,".")
					)?
			(pow(10, (min( len(x),
							index(x,".")<0?
							len(x):index(x,".")
						 )-_i_-1
					 )
				)
			* hash(  ["0",0, "1",1, "2",2
					,"3",3, "4",4, "5",5
					,"6",6, "7",7, "8",8
					,"9",9], x[_i_] )
			
			+ int(x, _i_+1)
			 ):0
			)
		  )
);


//========================================================
intersectPt=["intersectPt", "pq, target", "point", "Point"
, " 

"];



function intersectPt( pq, target) = 
(
	len(target)>2
	?  // target =[r,s,t ...]: line pq intersets with plane rst
//	  pq[0] + dot(normal(rst)+target[1], target[1]-pq[0]) 
//		 / dot(normal(rst)+target[1], pq[1]    -pq[0])
//		 * (pq[1]-pq[0])
	  onlinePt( pq, len= dist([pq[0],projPt( pq[0], target )])
			/cos( angle([projPt( pq[0], target ),pq[0],pq[1]])) )	
	: //target =[r,s]: line pq intersets with line rs
	lineCrossPt( pq[0], pq[1], mn[0],mn[1] )
);





//========================================================
is90=["is90", "pq1,pq2", "T|F", "Inspect",
"
 Given 2 2-pointers pq1, pq2 (each represents a line), return
;; true if the angle between two lines is 90 degree.
;;
;; New 20140613: can take a single 3-pointer, pqr:
;;
;;   is90( pqr );
;;
;; Same as angle( pqr )==90. Refer to : othoPt_test() for testing.
"];

function is90(pq1, pq2)=
( 
	pq2==undef                   // When pq1 is a 3-pointer and pq2 not given; is90(pqr)
	? abs((pq1[1]-pq1[0]) * (pq1[2]-pq1[1]))<ZERO  
	: abs(p2v(pq1)*p2v(pq2))< ZERO    // is90( [p1,q1],[p2,q2] )
);


//========================================================
isarr=[ "isarr","x", "T|F", "Type, Inspect, Array",
"
 Given x, return true if x is an array.
"];

function isarr(x)= type(x)=="arr";


//========================================================

//
//function isat(o,i,x)=
//(
//   isstr(o) && len(x)>1 ?
//   ![ for( j=range(x) )
//     if( x[j]!= o[i+j] ) 1]
//   : o[i]==x
//); 

function isat(s,i,x)=
(
   isarr(x)? [ for(w=x) if(isat(s,i,w)) w ][0]
   : len(x)>1 ?
     ![ for( j=range(x) )
      if( x[j]!= s[i+j] ) 1]
     : s[i]==x
); 
     
//========================================================
isbool=[ "isbool","x", "T|F", "Type, Inspect",
"
 Given x, return true if x is a boolean (true|false).
"];

function isbool(x)= x==true || x==false; 



//========================================================
isequal=["isequal", "a,b", "T|F", "Inspect",
_s(" Given 2 numbers a and b, return true if they are determined to
;; be the same value. This is needed because sometimes a calculation
;; that should return 0 ends up with something like 0.0000000000102.
;; So it fails to compare with 0.0000000000104 (another shoulda-been
;; 0) correctly. This function checks if a-b is smaller than the
;; predifined ZERO constant ({_}) and return true if it is.
", [ZERO])
];

function isequal(a,b)=
(
  	isnum(a)&&isnum(b)
	? (abs(a-b)< ZERO)
	: a==b
);





//========================================================
isfloat=[ "isfloat","x", "T|F","Type, Inspect, Number",
"
 Check if x is a float.
"];

function isfloat(x)= type(x)=="float";




//========================================================
ishash = [ "ishash","x", "T|F", "hash,type, Inspect",
"
 Check if x **could** be a hash, i.e., an array with even number length.
"];

function ishash(x)=  (type(x)=="arr") && ( len(x)/2 == floor(len(x)/2));



//========================================================
isint = [ "isint","x", "T|F", "Type,Inspect, Number",
"
 Check if x is an integer.
"];

function isint(x)=   type(x)=="int";



//================================================================
isln=["isln","x,dim=3","T|F","Geo, Inspect"
,"Return true if x is a line. Default is3d=true"
];

function isln(x,dim=3)=
( 
    len(x)==2?
    ( x[0]!=x[1] && all( [for(y=x) ispt(y,dim)] ) )
    :false       
);
    

//========================================================
isnum =[ "isnum","x", "T|F", "Type,Inspect,Number",
"
 Check if x is a number.
"];

function isnum(x)=   type(x)=="int" || type(x)=="float";



//========================================================
isodd=["isodd", "n", "T/F", "Math"
 ," Return true if n is odd number."
];
function isodd(n) = round(i/2)!=i/2;
function iseven(n)= round(i/2)==i/2;
 
 
//========================================================
isOnPlane=["isOnPlane", "pqr,pt", "T|F", "Inspect, Plane",
"
 Given a 3-pointer (pqr) and a point (pt) ( to be converted to planecoefs,
;; = [a,b,c,k] or [a,b,c])), check if the pt is on
;; the plane:
;;
;;   a*pt.x + b*pt.y + c*pt.z + k == ZERO
"];

function isOnPlane( pqr, pt )=
 let( pco = isarr(pqr[0])?planecoefs(pqr):pqr  
	, got =  pco[0]*pt.x
	   + pco[1]*pt.y
	   + pco[2]*pt.z
	   + (len(pco)==3?0:pco[3])
	)
(
	is0(got)
);

//================================================================
ispl=["ispl","x,dim=3","T|F","Geo, Inspect"
,"Return true if x is a plane. Default is3d=true"
];

function ispl(x,dim=3)=
( 
    len(x)==3 ?
    (  (x[0]!=x[1]) && (x[0]!=x[2]) && (x[1]!=x[2]) 
       && all( [for(y=x) ispt(y,dim)] ) 
    )
    :false    
);
    

//====================
ispt = ["ispt","x,dim=3","T|F","Geo, Inspect"
,"Return true if x is a point. Default is3d=true"
];
function ispt(x,dim=3)= 
(   len(x)==dim&& countNum(x)==dim
);


//========================================================
isSameSide=["isSameSide", "pts,pqr", "T/F", "Geometry",
" Return true if all pts is on the side of plane pqr."];

function isSameSide( pts, pqr )=
   let( ss = sum( [ for(p=pts) sign( distPtPl( p,pqr) )] )
      )
(  abs(ss)== len(pts)
);      
   
module isSameSide_test(ops=["mode",13])
{
   doctest( isSameSide,
   [
      
   ], ops );
}
     

//========================================================
isstr= [ "isstr","x", "T|F", "Type,String,Inspect",
"
 Check if x is a string.
"];

function isstr(x)=   type(x)=="str";

//
////========================================================
is0=["is0","n","T|F","Inspect",
str( " Given a number, check if it is 'equivalent' to zero. The 
;; decision is made by comparing it to the constant, ZERO=", ZERO )
,"isequal"
];

function is0(n) = abs(n)<ZERO;



//========================================================
istype=["istype","o,t","T|F", "type, inspect",
"
  Given a variable (o) and a typename (t), return true if 
;; o is type t. It can also check if o is one of multiple types
;; given, in which case, t could be entered as:
;;
;; istype(o, [''arr'',''str''] )
;; istype(o, ''arr,str'')
;; istype(o, ''arr str'')
"];

function istype(o,t)=
let( t = index(t,",")>=0
		? split(t,",")
		:index(t," ")>=0
		? split(t," "): t )
(	
//	[t, [for(x=t) type(o)==x?1:0 ] , sum([for(x=t) type(o)==x?1:0 ])]

	isstr(t)
	? type(o)==t
	: isarr(t)
	  ? has(t, type(o)) 
	  : false
);


//========================================================
join=[ "join", "arr, sp=\",\"" , "string","Array",
" Join items of arr into a string, separated by sp.
"];

function join(arr, sp="", _i_=0)=
//let(L=len(arr)) 
(
	_i_<len(arr)?
	str(
		arr[ _i_ ]
		, _i_<len(arr)-1?sp:""
		,join(arr, sp, _i_+1)
	):""

//	L==0?"":str( arr[0], L==1?"":sp, join(slice(arr,1),sp=sp) )

); 



//========================================================
joinarr=[ "joinarr", "arr" , "array","Array",
" Given an array(arr), concat items in arr. See the difference below:
;; 
;;  concat( [1,2], [3,4] ) => [1,2,3,4] 
;;  joinarr( [ [1,2],[3,4] ] ) => [1,2,3,4] 
;;  join( [ [1,2],[3,4] ] ) => ''[1,2],[3,4]'' 
"];

function joinarr(arr,_I_=0)= 
(
	_I_>len(arr)-1? []
	: concat( arr[_I_], joinarr( arr, _I_+1) )  
); 




//========================================================
keys=["keys", "h", "array", "Hash",
"
 Given a hash (h), return keys of h as an array.
"];

function keys(h)=
(
	[for(i=[0:2:len(h)-2]) h[i]]
);


//========================================================
kidx=["kidx", "h,k", "hash", "Array, Hash"
, "Return index of key k in hash h. It's the order of k
;; in keys(h).
;; 2015.1.30"
];
function kidx(h,k)=
    [for(i=[0:2:len(h)-2]) if(h[i]==k)i/2][0];



//========================================================
lcase=["lcase", "s","str","String",
 " Convert all characters in the given string (s) to lower case. "
];
_alphabetHash= split("A,a,B,b,C,c,D,d,E,e,F,f,G,g,H,h,I,i,J,j,K,k,L,l,M,m,N,n,O,o,P,p,Q,q,
R,r,S,s,T,t,U,u,V,v,W,w,X,x,Y,y,Z,z",",");
function lcase(s, _I_=0)=
(
  	_I_<len(s)
	? str( hash( _alphabetHash, s[_I_], notfound=s[_I_] )
		, lcase(s,_I_+1)
		)
	:""
);
    
//========================================================
last=[ "last", "o", "item", "Index, String, Array" ,
"
 Given a str or arr (o), return the last item of o.
"];

function last(o)= o[len(o)-1]; 






//========================================================
lineCrossPt=["lineCrossPt", "P,Q,M,N","point", "Point",
"
 Given 4 points (P,Q,M,N), return the point (R below) where extended
;; lines of PQ and MN intesects.
;;
;;     M        
;;      :       Q
;;       :    -'
;;        R-'
;;      -' :
;;    P'    N
;;
;;   M
;;    : 
;;     :
;;      N      Q 
;;           -'
;;        R-'
;;      -'
;;    P'
;;
;; 2015.2.6: All the following 3 work:
;;
;;     lineCrossPt( P,Q,M,N )
;;     lineCrossPt( pq, mn )
;;     lineCrossPt( [pq,mn] )
;;     lineCrossPt( pqmn )
"];

function lineCrossPt( P,Q,M,N )=
    let( pqmn = N? [P,Q,M,N]      // lineCrossPt( P,Q,M,N )
                :Q? concat( P,Q ) // lineCrossPt( pq, mn )
                :len(P)==2?[ P[0][0],P[0][1],P[1][0],P[1][1] ]
                :P               // lineCrossPt( pqmn )
       , P = pqmn[0]
       , Q = pqmn[1]
       , M = pqmn[2]
       , N = pqmn[3]
       , err= !isOnPlane( [P,Q,M],N) 
       )
(
// Ref: Paul Burke tutorial 
//   http://paulbourke.net/geometry/pointlineplane/
// and his code in c :
//   http://paulbourke.net/geometry/pointlineplane/lineline.c
                   
//	P + (Q-P) * ( ((M-P)*(M-N))* ((M-N)*(P-Q)) - ((M-P)*(P-Q))*((M-N)*(M-N)))
// 	 		 / ( ((P-Q)*(P-Q))* ((M-N)*(M-N)) - ((M-N)*(P-Q))*((M-N)*(P-Q)))
               
	err?_red(str( "Error in lineCrossPt: 2 lines are not on same plane. line1: "
           , [P,Q], ", line2: ", [M,N] ))
    :P + (Q-P) * ( dot(M-P,M-N)* dot(M-N,P-Q) - dot(M-P,P-Q)* dot(M-N,M-N) )
 	 		 / ( dot(P-Q,P-Q)* dot(M-N,M-N) - dot(M-N,P-Q)* dot(M-N,P-Q) )
);


//========================================================
lineCrossPts=["lineCrossPts","pq,mn","2-pointer", "Geo",
"Return 2-pointer, [U,V], for the shortest line segment between
;; any two pts from line pq and mn. 
;;
;; The dist([U,V]) is the distance between lines pq,mn. 
;; If U=V then pq,rs crosses;
;;
;; Ref: http://geomalgorithms.com/a07-_distance.html
"];
function lineCrossPts( pq,rs )=
(
    let( pqmn = N? [P,Q,M,N]      // lineCrossPts( P,Q,M,N )
                :Q? concat( P,Q ) // lineCrossPts( pq, mn )
                :len(P)==2?[ P[0][0],P[0][1],P[1][0],P[1][1] ]
                :P               // lineCrossPts( pqmn )
       , P = pqmn[0]
       , Q = pqmn[1]
       , M = pqmn[2]
       , N = pqmn[3]
       // Below follows: http://geomalgorithms.com/a07-_distance.html
       , u = Q-P
       , v = N-M
       , w0= P-M
       , a = u*u
       , b = u*v
       , c = v*v
       , d = v*w0
       , denom = a*c-b*b
       )
    [ (b*e-c*d)/denom, (a*e-b*d)/denom ]
);

module lineCrossPts_test( mode=112 ){ //===============
    doctest( lineCrossPts,
    [
    
    
    ]
    , mode=mode
    );
}


//========================================================
linePts=["linePts","pq, len, ratio", "pq", "Point, Line",
 " Extend/Shrink line. Given a 2-pointer (pq) and len or ratio , return 
;; a new 2-pointer for a line with extended/shortened lengths. 
;;
;; Both len and ratio can be positive|negative. Positive= extend, negative=shrink.
;; 
;; They can be either 2-item array (like [1,0]) or a number. If a number, it's the
;; same as [number,number].
;;
;; If array [a,b], a,b for p, q side, respectively. For example, 
;;
;;   linePts( pq, len=-1): both sides shrink by 1
;;   linePts( pq, len=[1,1]): both sides extends by 1

;; Compared to onlinePt, this function:
;; (1) is able to extend both ends. 
;; (2) return 2-pointers.
;;
;; linePts(pq, len=1) = [ onlinePt( pq, len=-1)
;;                      , onlinePt( p10(pq), len=-1 ]
"];


function linePts(pq,len=undef, ratio=[1,1])=
 let( len=isnum(len)?[len,len]:len
	, ratio=isnum(ratio)?[ratio,ratio]:ratio
	)
( 
   [ onlinePt( pq,  len= -len[0], ratio=-ratio[0])
   , onlinePt( p10(pq), len= -len[1], ratio=-ratio[1])
   ]
);		




//========================================================
longside=["longside","a,b","number","Triangle",
 " Given two numbers (a,b) as the lengths of two short sides of a 
;; right triangle, return the long side c ( c^2= a^2 + b^2 )."
,"shortside"
];
function longside(a,b)= sqrt(pow(a,2)+pow(b,2));



lpCrossPt=["lpCrossPt", "pq,tuv", "point", "Point,Geometry",
 " Given a 2-pointer for a line (pq) and a 3-pointer for a plane (tuv),
;; return the point of intercept between pq and tuv. 
;;
    m=dist(pq)

    P   m
    |'-._	  
    |    'Q._  n
    |dp   |  '-._
    |     |dq    '-.
   -+-----+----------X-

    (m+n)/m = dp/(dp-dq) 

    onlinePt( pq, ratio = dp/(dp-dq) )

"];

function lpCrossPt(pq, tuv)=
 let( P=pq[0]
	, Q=pq[1]
	, L= dist(pq)
	, dp = distPtPl(P, tuv)
	, dq = distPtPl(Q, tuv)
	, sameside = sign(dp)==sign(dq)
	, ratio = sameside ?  dp/(dp-dq)
					 :abs(dp)/(abs(dp)+abs(dq))
	)
(
	onlinePt( pq, ratio = ratio)
);

//=============================================================

function match(s
    , ps           // list of patterns, each p could be string or array,
                   // match 
    , want=undef
    , _i=0           
    , _rtn=[]            
    , _debug=[]
    )=
(
   let( 
        m= match_ps_at(s,_i,ps,want) //: [3,7,["cos","(","2",")"]] or false
      ,newi= m? m[1]+1: _i+1
      ,newrtn= m? app(_rtn,m):_rtn
//      , _debug= concat(_debug,
//         str(
//          [ "_i", _i
//          , "s[_i]", s[_i]
////          , "newi", newi
////          , "_rtn", _rtn
////          , "newrtn", newrtn
//          ]
//         ))
      )
    _i<len(s)-len(ps)+1?  
      match( s,ps,want, _i=newi, _rtn=newrtn, _debug=_debug )
    //: join( _debug, "<br/>") 
      : _rtn
);
    
//=============================================================

function match_pn(s
    , ps           // hash of patterns: ["name1",p1,"name2",p2 ...]
                   // Each p is: str, [str,n], or array of words
    , want=undef
    , _pnames= []
    , _ps = []
    , _i=0           
    , _rtn=[]            
    , _debug=[]
    )=
(
   let( _pnames= keys( ps )
      , _ps = vals( ps )
      , m= match_ps_at(s,_i,_ps,want) //: [3,7,["cos","(","2",")"]] or false
      ,newi= m? m[1]+1: _i+1
      ,newrtn= m? app(_rtn,m):_rtn
//      , _debug= concat(_debug,
//         str(
//          [ "_i", _i
//          , "s[_i]", s[_i]
////          , "newi", newi
////          , "_rtn", _rtn
////          , "newrtn", newrtn
//          ]
//         ))
      )
    _i<len(s)-len(ps)+1?  
      match( s,ps,want, _i=newi, _rtn=newrtn, _debug=_debug )
    //: join( _debug, "<br/>") 
      : _rtn
);

//=============================================================

function match_at(s,i,p,n)=
(
   let( 
   isarrp= isarr(p)
   , endi = isarrp? undef   // endi is the first unmatched
            :isint(n)?               // limit match count
             ( [ for(j=[i:i+n-1])        // found an unmatched i 
                 if(!has(p,s[j])) false  // within i~i+n-1 
               ]? undef                  // Means match fails
                : i+n                    // Success 
             )
             : [ for(j=[i:len(s)])  // Note: run to the i behind last  
                   if(!has(p,s[j])) j    // index of s to pick up the
               ][0]                      // last char of s 
   
   , rtn=   isarrp?                // p is an array: for each item, px,
              [ for( px=p )                 // check if each char in px
                  if(![for( j= range(px) )  // is the same as s[i+?]   
                    if( s[i+j]!=px[j]) false]) px 
              ][0]
              : endi!=undef && endi!=i? 
                 (endi==len(s)? slice(s,i): slice( s,i,endi ))
                 :undef
  )
    rtn? [i, i+len(rtn)-1, rtn]: false        
);               

                    
function matchr_at(s,i,p,n)=  // for i<0, match from the end
(
   let( 
   isarrp= isarr(p)
   , _i = i<0?(len(s)+i):i // real i
   , endi = isarrp? undef   // endi is the first unmatched
            :isint(n)?               // limit match count
             ( [ for(j=range(_i,_i-n-1)) // found an unmatched i 
                 if(!has(p,s[j])) false  // within _i ~ _i-n-1 
               ]? undef                  // Means match fails
                : _i-n                    // Success 
             )
             : [ for(j=range( _i, -1 )
                    )  // Note: run to the i behind 0
                     
                   if(!has(p,s[j])) j    // to pick up the
               ][0]                      // first char of s                  
   
   , rtn=   isarrp?                // p is an array: for each item, px,
              [ for( px=p )                 // check if each char in px
                  if(![for( j= range(px) )  // is the same as s[_i-?]   
                    if( s[_i-len(px)+j+1]!=px[j]) false]) px 
              ][0]
              : endi==undef? slice(s,0,_i+1)
              : endi!=undef && endi!=_i? 
                 //(endi==len(s)? slice(s,i): slice( s,i,endi ))
                  slice(s,endi+1,_i+1)   
                 :undef
  )
    //rtn? [i, i+len(rtn)-1, rtn]: false        
    rtn?  [isarrp?(_i-len(rtn)+1)
                 :endi==undef?0:(endi+1), _i, rtn]: false        
); 
                    
//=============================================================
// ps = [p1,p2,p3 ...]
function match_ps_at( s, i, ps, want, _i0, _rtn=[], _pi=0, _debug=[])=
(
   let( _i0= _i0==undef? i: _i0 // keep track of starting i
      , want= want==undef? range(ps):isint(want)?[want]:want
      , _p = ps[_pi]
      , p = len(_p)==2 && isint(_p[1])? _p : [_p,undef]  
      , _m = match_at( s, i, p=p[0], n=p[1] ) // _m:[i,j,word]
      , m = _m? _m[2]: undef                  // m: word 
      , allm = m && _pi == len(ps)-1          // all ps matched
        
      , newi = m? _m[1]+1:undef
      , newpi= m && !allm ? _pi+1:undef  // next p only when m and !allm
      , _rtn = has(want, _pi)? app( _rtn, m ):_rtn
//      , _debug= concat( _debug,
//           str( [ str("s[",i,"]"), s[i]
//           // , "ps", ps
//           // , "_pi", _pi
//           // , "ps[_pi]", ps[_pi]
//            , "p", p
//            , "m", m
//            , "mm", match_at( s, i, p=p[0], n=p[1] )
//            , "_rtn",_rtn
//            ])
//            )
      )    
//   debug:
//    i< len(s) - len(ps) && _pi<len(ps)? 
//     match_ps_at( s, newi, ps, _i0, _rtn, newpi, _debug)         
//     : str("<br/>", join(_debug, "<br/>"),"<br/>")
     
   !m? false // quit as soon as any p not match
   :allm? app( [_i0, _m[1] ], _rtn )
    : match_ps_at( s, newi, ps, want,_i0, _rtn, newpi)         
             
);        

//=============================================================
/* 
   np: [ "name1",  ps=[p,p...], want  ] : a named pattern
                       each p could be p or [p,n]
   nps: [ np1,np2 ... ]
   
   return: ["func", 2, 4, ["cos"]] or false
   
   Note:
   
   match_ps_at(s,i,ps,want) checks if ps matches s[i+]
   match_nps_at(s,i,nps=[ ["name", ps, want]   checks WHICH ps matches s[i+]
                          , ["name", ps, want]
                          , ...] )//
*/
function match_nps_at( s, i, nps, _npi=0, _debug=[])=
(
   let( np = nps[_npi]           //_pn: ["name", [ [p,p..], n] ]
      , name= np[0]
      , ps =  np[1]
      , _want= np[2]
      //, ps = _ps //len(_ps)==2 && isint(_ps[1])? _ps : [_ps,undef] 
      , want= _want==-1 || !_want?range(ps[0]):_want
      
      , m = match_ps_at( s, i, ps=ps, want=want ) // _m:[i,j,word]
      
//      , _debug= concat( _debug,
//           str( [// "nps",nps
//                 "_npi", _npi
//                , "np", np
//                , "ps", ps
//                , "name",name
//                , "want",want
//                , "m", m
//                ] )
//            )
      )    
//   debug:
//    _npi<len(nps)?
//     match_nps_at( s, i, nps, _npi=_npi+1, _debug=_debug ) 
//     : str("<br/>", join(_debug, "<br/><br/>"),"<br/>")
   m? concat( [name], m )
   : _npi< len( nps) ? match_nps_at( s, i, nps, _npi=_npi+1 ) 
   : false
    
        
             
);       
                    
//========================================================
parsedoc=["parsedoc","string","string", "Doc",
 " Given a string for documentation, return formatted string.
"];

function parsedoc( s
			, prefix ="|"
			, indent =""
			, replace=[]
			, linenum
			)=
 let( s   = split(s, ";;")	)
(
	
  join(
	[for( i=range(s) )
		 str( indent
			, prefix
			, isnum(linenum)?str((linenum+i),". "):"" 
			, replace
			  (
				replace
				( 
				  replace( s[i], "''", "&quot;" )
				, "<", "&lt;"
				)
			  , ">", "&gt;"
			  )
			)
	],"<br/>")									
);

//========================================================
_mdoc=["_mdoc", "mname,s", "n/a", "Doc",
 " Given a module name (mname) and a doc string (s), parse s (using 
;; parsedoc()) and echo to the console.
"];

module _mdoc(mname, s){ 
	echo(_pre(_code(
			str("<br/><b>"
			, mname
			, "()</b><br/><br/>"
			, parsedoc(s)
			)
			,s="color:blue"
		)));
}				  


//========================================================
midPt=[ "midPt", "PQ", "pt", "Point" ,
"
 Given a 2-pointer PQ, return the pt that's in the middle of P and Q
"];

function midPt(PQ)= (PQ[1]-PQ[0])/2 + PQ[0];


//========================================================
minPt=[ "minPts", "pts", "array", "Point, Math",
"
 Given an array of points (pts), return a pt that
;; has the min x then min y then min z.
"];
 
function minPts(pts)=
(
	minzPts( minyPts( minxPts( pts ) ) )[0]
);



//========================================================
minxPts=[ "minxPts", "pts", "array", "Point"	,
"
 Return an array containing the pt(s) that has the min x.
"];
 
function minxPts(pts, _i_=0)=
(
    /* minx = min(getcol(pts,0)); */
	_i_<len(pts)?
	 concat( pts[_i_][0]==min(getcol(pts,0))? [pts[_i_]]:[]
			, minxPts( pts, _i_+1) 
			)
	 :[]
);



//========================================================
minyPts=[ "minyPts", "pts", "array", "Point",
"
 Return an array containing the pt(s) that has the min y.
"];
function minyPts(pts, _i_=0)=
(
    /* minx = min(getcol(pts,0)); */
	_i_<len(pts)?
	 concat( pts[_i_][1]==min(getcol(pts,1))? [pts[_i_]]:[]
			, minyPts( pts, _i_+1) 
			)
	 :[]
);




//========================================================
minzPts=[ "minzPts", "pts", "array", "Point",
"
 Return an array containing the pt(s) that has the min y.
"];

function minzPts(pts, _i_=0)=
(
    /* minx = min(getcol(pts,0)); */
	_i_<len(pts)?
	 concat( pts[_i_][2]==min(getcol(pts,2))? [pts[_i_]]:[]
			, minzPts( pts, _i_+1) 
			)
	 :[]
);


//========================================================
mod=["mod", "a,b", "number", "Number, Math",
" Given two number, a,b, return the remaining part of division a/b.
;;
;; Note: OpenScad already has an operator for this, it is the percentage
;; symbol. This is here because this code file might be parsed by python
;; that reads the percentage symbol as a string formatting site and causes
;; errors.
"];

function mod(a,b) = sign(a)*(abs(a) - floor(abs(a)/abs(b))*abs(b) );



//========================================================
normal= ["normal","pqr","pt","math",
"
 Given a 3-pointer pqr, return the normal (a vector) to the plane
;; formed by pqr.
"];

function normal(pqr, len=undef)=
(
	isnum(len)? onlinePt( [ORIGIN, cross( pqr[0]-pqr[1], pqr[2]-pqr[1] )], len=len)
	:cross( pqr[0]-pqr[1], pqr[2]-pqr[1] )
);

//========================================================
normalLn=["normalLn","pqr,len=undef, sign=1", "2-pointer", "Geometry",
" Return a 2-pointer, [Q,N], where Q is the mid pt of pqr and N is 
;; the normal pt. Set len to determine the length, and sign for 
;; direction."
];
function normalLn( pqr, len=undef, sign=1)=
  let( Q=pqr[1], N=N(pqr) 
     )
(
   sign==1 && len==undef? [Q,N] : onlinePt( [Q,N], len=sign*(len?len:norm(N-Q)) ) 
);   

//========================================================
normalPt=["normalPt", "pqr,len,reversed", "point", "Point",
 " Given a 3-pointer (pqr), optional len and reversed, return a 
;; point representing the normal of pqr."
];
function normalPt(pqr, len=undef, reversed=false)=
(
	len==undef? (reversed?-1:1)*normal(pqr)+pqr[1]
			  :onlinePt( [ pqr[1], (reversed?-1:1)*normal(pqr)+pqr[1]], len= len )
);

		


//========================================================
normalPts=[[]];
function normalPts(pts, len=1)=
 let(tripts= subarr(pts,  cycle=true)
	)
( 
	[for(pqr=tripts) normalPt(pqr,len=len)]
);



//========================================================
num=[ "num", "x", "number", "Type, Number",
" Given x, convert x to number. x could be a number, a number string,
; or a string for feet-inches like 3'4'' that will be converted to number
; in inches.
;
; num(''-3.3'') => -39  
; num(''2'6'''') => 18   
"];

function num(x,_i_=0)=
(
	isnum(x)
	 ? x
	 : isstr(x)
	   ?(
		 x==""
		 ? undef
		 : endwith(x,"'")||endwith(x,"\"")
		 ? ftin2in(x)//12
		 : _i_<len(x)
		   ?(
			index(x,".")==0
		  	? num( str("0",x), _i_=0)	
		 	: _i_==0 && x[0]=="-"
		   		?-num(x,_i_=1)
		   		: (_i_== index(x,".") //x[_i_]=="."
				  ? 0
					// 345.6      3456
					// 210 -1     3210  <== pow(10, ?)
					// 01234      0123  <== _i_
					//    3         -1  <== index(x,".")
					
				  : pow(10, (index(x,".")<0
							?(len(x)-_i_-1)
							: ( index(x,".")-_i_ )
							  -( index(x,".")>_i_?1:0)	
						   )
					  )
				    * hash( ["0",0, "1",1, "2",2
				 		,"3",3, "4",4, "5",5
						,"6",6, "7",7, "8",8
						,"9",9], x[_i_] )

				  )+ num(x, _i_+1)
		    )
		   :0 // out of bound
		)
		:undef // everything other than string and num
);





//========================================================
numstr=["numstr","n,d=undef,dmax=undef,convert=undef", "string", "Number, String",
 " Convert a number to string. d = decimal
;; 
;;   numstr(3.8, d=3) = ''3.800''   ... (a)
;;   numstr(3.8765, d=3) = ''3.877'' ... (b)
;; 
;; If we want to limit decimal points (b) but don't want to add
;; extra 0 (a), then we can use dmax:
;;
;;   numstr(3.8, dmax=3) = ''3.8''
;;   numstr(3.8765, dmax=3) = ''3.877''
;;
;;   numstr( 3.839, d=1 )= ''3.8''
;;   numstr( 3.839, d=2 )= ''3.84''
;;   numstr( 3.839, d=3 )= ''3.839''
;;   numstr( 3.839, d=4 )= ''3.8390''
;;   numstr( 3.839, d=1, dmax=2 )= ''3.8''
;;   numstr( 3.839, d=2, dmax=2 )= ''3.84''
;;   numstr( 3.839, d=3, dmax=2 )= ''3.84''
;;   numstr( 3.839, d=4, dmax=2 )= ''3.84''
;;
;;   numstr( 0.5,conv=''ft:ftin'' )= ''6''''
;;   numstr( 96,conv=''in:ftin'' )= ''8'''
"];



function numstr(n,d=undef, dmax=undef, conv=undef)=
(
	conv=="ft:ftin"
	?(n<1
	   ? str( numstr(n*12, d, dmax) , "\"")
	   : mod(n,1)==0
		? str(n, "'")
		: str( floor(n), "'"
			, numstr( mod(n,1)*12,d,dmax), "\"")	
	 )
	:conv=="in:ftin"
	? numstr( ( isstr(n)&&endwith(n,"\"")?num(shift(n,"\"","")):n )
			 /12, d=d, dmax=dmax, conv="ft:ftin")
	:conv
	? numstr( uconv(n, from=split(conv,":")[0], to=split(conv,":")[1] )
			, d=d, dmax=dmax )
//	: conv=="in:cm"
//	? numstr( n*2.54, d=d, dmax=dmax)
//	: conv=="ft:cm"
//	? numstr( n*12*2.54, d=d, dmax=dmax)

//	: conv=="ftin:cm" && isstr(n)
//	? numstr( endwith(n,"'")
//			 ? num(shift(n)*12 
//			index(n, "'")>0? // 3'2", 3'
//			( num(split(n,"'")[0])*12
//			 )+(len(split(n,"'"))>1?
//			  split(n,"'")[0]*12:0
//			 ) n*12*2.54, d=d
//			)

	// Finish conv part above. Start processing decimal 

	: isint(d)&&d>-1
	  ?(
		isint(dmax)  // when both defined
		? _getdeci(n)== min(d,dmax)
           ? str(n)
		  : _getdeci(n)< min(d,dmax)
		    ? str( n, repeat("0", min(d,dmax)-_getdeci(n)))
			: str( round(n*pow(10,min(d,dmax)))
				        /pow(10,min(d,dmax))
				)	
		: // d given, dmax not
		  _getdeci(n)== d
           ? str(n)
		  : _getdeci(n)< d
		    ? str( n, isint(n)?".":"", repeat("0", d-_getdeci(n)))
			: str( round(n*pow(10, d))
				        /pow(10,d)
				)
	   )
	  :(
		isint(dmax)
		?_getdeci(n)<= dmax
           ? str(n)
		  : str( round(n*pow(10, dmax))
				        /pow(10, dmax)
			  )			 
		:str(n)
	   )
);




//========================================================
onlinePt=[ "onlinePt", "PQ,len=false,ratio=0.5", "pt", "Point",
"
 Given a 2-pointer (PQ), return a point (pt) that lies along the
;; line between P and Q, with its distance away from P defined by 
;; *len* or by *ratio* if len is not given. When ratio=1, return Q.
;; Both *len* and *ratio* can be negative.
"];

function onlinePt(pq, len=undef, ratio=0.5)=
(
	[ (isnum(len)?len/dist(pq):ratio)*distx(pq)+pq[0].x
	, (isnum(len)?len/dist(pq):ratio)*disty(pq)+pq[0].y 
	, (isnum(len)?len/dist(pq):ratio)*distz(pq)+pq[0].z ] 
	
);


//========================================================
onlinePts=["onlinePts","PQ,lens=false,ratios=false","path","Point",
"
 Given a 2-pointer (PQ) and a numeric array assigned to either lens
;; or ratios, return an array containing pts along the PQ line with 
;; distances (from P to each pt) defined in the given array. Refer to
;; onlinePt().
;; This is good for getting multiple points on pq line. For example, to
;; get 4 points that divide pq line evenly into 5 parts:
;;
;;   onlinePts( pq, ratios=[ 0.2, 0.4, 0.6, 0.8 ] );
;;
;; This ratios can be produced this way: range(0.2,0.2,1)
"];

function onlinePts(pq, lens=false, ratios=false)=
(
	ratios==false?
	( len(lens)==0?[]
	  :	concat( [onlinePt( pq, len=lens[0])]
		  , onlinePts( pq, lens=shrink(lens) )
		  )
	):
	( len(ratios)==0?[]
	  :	concat( [onlinePt( pq, ratio=ratios[0])]
		  , onlinePts( pq, ratios= shrink(ratios) )
		  )
	)
);



//========================================================
or=["or","x,dval","any|dval",  "Core",
"
 Given x,dval, if x is evaluated as true, return x. Else return
;; dval. Same as x?x:dval but x only appears once.
"  
];

function or(x,dval)=x?x:dval;




//========================================================
othoPt= [ "othoPt", "pqr", "pt", "Point",
"
 Given a 3-pointer (pqr), return the coordinate of the point D
;; such that line QD is at 90 degree with line PR.
;;
;;         Q
;;         :.
;;        /: :
;;       / :  :
;;      /  :   :
;;     /   :_   :
;;    /____:_|___:
;;   P     D      R
"];

function othoPt( pqr )= 
(
/*  Return [x,y] of D
           Q          ___
           ^\          :
          /: \         :   
       a / :  \_  b    : h
        /  :    \_     :
       /   :-+    \    :    
     P-----D-!------R ---

	|------ c ------|  
        
	a^2 = h^2+ PD^2
	b^2 = h^2+ DR^2  
	a^2- PD^2 = b^2-(c-PD)^2
			  = b^2-( c^2- 2cPD+PD^2 )
			  = b^2- c^2 + 2cPD -PD^2
	a^2 = b^2-c^2+ 2cPD
	PD = (a^2-b^2+c^2)/2c
	ratio = PD/c = (a^2-b^2+c^2)/2c^2    
*/
	onlinePt( [pqr[0],pqr[2]]
	        , ratio=( d01pow2(pqr) - d12pow2(pqr) + d02pow2(pqr))/2/d02pow2(pqr) 
	)
);





//=============================================
packuni=["packuni","uni, prefix=''#/''","arr", "Array"
,"Given an arr with a data structure called *uni*, an arr of one-dimensional
;; arrs, each for a block:
;;
;;   [[''#/1'', ''*'', ''#/2''], [''x'', ''+'', 1], [''y'', ''+'', 2]]
;;
;; which is a return of getuni( ''(x+1)*(y+2)'' ), pack and stack those 
;; blocks into an array it represents:  
;;
;;   [ [''x'', ''+'', 1], ''*'', [''y'', ''+'', 2] ]
"];

function packuni( uni
        , prefix="#/"
        , _lastbi=undef
        , _init= false
        , _debug=[]
        )=
    let( uni = _init? uni
              : [ for( arr=uni )
                    joinarr( 
                        [ for( c=arr )
                            let( ns= begword( c, isnum=true ) // ex: "2" in "2x"
                               )
                                ns && (len(ns) < len(c)) ?  
                                    [[ num(ns), "*", slice(c,len(ns)) ]]
                                    :[c]
                        ]
//                        [ for( i=range(arr) )
//                            let( c=arr[i]
//                               , ns= begword( c, isnum=true ) // ex: "2" in "2x"
//                               , n = num(ns)                  // ex: 2 in "2x"
//                               )
//                               ( ns && (len(ns) < len(c)) )
//                               || (isnum(n) &&begwith(arr[i+1],prefix)) ?   
//                                    [[ n, "*", slice(c,len(ns)) ]]
//                                    :[c]
//                        ]
                   )
                ]        
       , _lastbi= _init? _lastbi  // Set _lastbi to be the LAST item of uni 
                                   //  containing block(s) info. For example, the
                                   //  index of [...,#/3,...] below:
                                   //  [ ...[...,#/3,...],[x,+,3] ]
                  :last( [ for (i=range(uni)) 
                        let( hasblock = [for(x=uni[i]) 
                                            if(isstr(x)&&begwith(x,prefix) ) 1] )
                        if( hasblock ) i 
                    ] 
                  )
      , arr= uni[_lastbi] // The last item of uni that has "#/": ["a", "#/4" ], [3,5] 
                          // It will be replaced with  newarr:   ["a", [3,5]], [3,5] 
                          // ==> arr=["a","#/4"], newarr=["a",[3,5]]   
      ,newarr=   [ for(x=arr)
                     isstr(x) && begwith(x,prefix) ?
                        uni[ int(slice(x,len(prefix))) ]
                        : x
                 ]  
      , _debug= concat( 
            _debug
            , ["<br/>///"
              , "_lastbi= ", _lastbi
              , "arr= ", arr
              , "newarr= ", newarr
              , "uni[0]= ", uni[0]
              ])
       )
(
    _lastbi>=0
    ? packuni(
              replace( uni, _lastbi, newarr)
              , _lastbi=_lastbi-1
              , _init = true
              , _debug=_debug
             )
    //: _debug 
    :uni[0]             
);
    


//========================================================
permute=["permute","arr","array","Array, Math",
 " Given an array, return its permutation.
;;
;; [2,4,6] => [[2,4,6],[2,6,4],[4,2,6],[4,6,2],[6,2,4],[6,4,2]]
"];

function permute(arr, _I_=0)=
(
	len(arr)==2
	? [arr,[arr[1],arr[0]]]
	:len(arr)==3
	? [arr, p021(arr), p102(arr), p120(arr), p201(arr), p210(arr) ]  
	:_I_<len(arr)
	? concat(
			ppEach( permute( del(arr,_I_, byindex=true),_I_+1)
						, arr[_I_]
						)
			, permute(arr,_I_+1) )
	: []
);



//========================================================
pick=["pick", "o,i", "pair", "Array",  
"
 Given an array or a string (o) and index (i), return a two-member array :
;;
;; [ o[i], the rest of o ] // when o is array
;; [ o[i], [ pre_i string, post_i_string] ] //when o is a string
;;
;; Note that index could be negative.
"];
function pick(o,i) = 
(
  	concat( [get(o,i)]
		  , isarr(o)
			? [ del(o,i, byindex=true) ] 
			: [[ slice(o,0,i), slice(o,i+1) ]]
		  )
  
);




//========================================================
planecoefs=["planecoefs", "pqr, has_k=true", "arr", "Geometry" ,
"
 Given 3-pointer pqr, return [a,b,c,k] for the plane ax+by+cz+k=0.
"];

function planecoefs(pqr, has_k=true)=
(
	/*
	https://answers.yahoo.com/question/index?qid=20110121141727AAncAu4

	Using normal vector <5, 6, -3> and point (2, 4, -3), we get equation of plane: 
	5 (x-2) + 6 (y-4) - 3 (z+3) = 0 
	5x - 10 + 6y - 24 - 3z - 9 = 0 
	5x + 6y - 3z = 43

	normal = [a,b,c]
	k = -(ax+by+c)

	*/	
	concat( normal(pqr) 
		  , has_k?[-( normal(pqr)[0]* pqr[0][0]
			 + normal(pqr)[1]* pqr[0][1]
			 + normal(pqr)[2]* pqr[0][2]
             )]:[]			 
		)	
);

////========================================================
//plplint=["plplint", "pqr1,pqr2", "pq", "Geometry",
//" Return a [P,Q] as the line that plane pqr1 and pqr2 intercept.
//;;
//;; http://mathworld.wolfram.com/Plane-PlaneIntersection.html
//;; 
//;; The line of intersection, pq, is 90 to normals of both planes, n1,n2.
//;; So pq is parallel to the normal of n1 and n2. 
//
//
//
//"
//];
//
//function plplint( pqr1,pqr2 )=
//   let( f1 = planecoefs(pqr1) // [a1,b1,c1,k1]
//      , f2 = planecoefs(pqr2) // [a2,b2,c2,k2]
//      , a1 = f1[0], b1=f1[1], c1=f1[2], k1=f1[3]
//      , a2 = f2[0], b2=f2[1], c2=f2[2], k2=f2[3]
//      )
//   /*  a1x + b1y + c1z + k1 = 0                     [1]
//       a2x + b2y + c2z + k2 = 0                     [2]
//       (c1/c2)a2x + (c1/c2)b2y + c1z + (c1/c2)k2 = 0   [3]
//       (a1- c1a2/c2)x + (b1-c1b2/c2)y + (k1-c1k2/c2) = 0   [4]
//       (a1- c1a2/c2)x = - (b1-c1b2/c2)y - (k1-c1k2/c2)   [5]
//       x = ( - (b1-c1b2/c2)y - (k1-c1k2/c2) ) /(a1- c1a2/c2)   [5]
//   */
//(
//     
//
//);

//========================================================
ppEach=[ "ppEach", "arr,x", "arr", "Array",
 " Given an array of arrays (arr) and an item (x), prepend x to 
;; each array in arr.
;;
;; 20150206:Put back  (Usage: permute)
"];

function ppEach( arr, x, _I_=0)=
(
	[ for(a=arr) concat([x],a) ]
);


//========================================================
pqr90=["pqr90","pqr,d=3,x,y,z,seed","pts","Geometry",
" Return a pqr that with anelg(pqr)=90. If pqr not given, make one randomly
;;
;; Refer to randPt() for other argument definitions;
;; Use randRightAnglePts() if need to specify leg lengths"
];
    
function pqr90(pqr, d=3,x,y,z,seed)= 
    let( pqr=pqr==undef?randPts(3,d,x,y,z,seed):pqr)   
(    
    [ pqr[0], pqr[1], anglePt(pqr,90) ]
);
    
    
    
//========================================================
projPt=["projPt", "pt,pqr", "point", "Point",
"  Given a point (pt) and a 3-pointer (pqr), return the projection of pt
;; onto the pqr plane.
"];

function projPt( pt, pqr )=
(
	/*
http://stackoverflow.com/questions/8942950/how-do-i-find-the-orthogonal-projection-of-a-point-onto-a-plane

		The projection of a point q = (x, y, z) onto a plane 
given by a point p = (a, b, c) and a normal n = (d, e, f) is

q_proj = q - dot(q - p, n) * n

	n = (normal(pqr)/norm(normal(pqr)))
	*/
	pt - dot(pt-pqr[1], (normal(pqr)/norm(normal(pqr))))*(normal(pqr)/norm(normal(pqr))) 
);


//========================================================
prod=["prod", "arr","n","Math",
" prod( [n1,n2,n3...] ) = n1*n2*n3*...
"];

function prod(arr)=
   arr? arr[0]* prod(slice(arr,1)) :1;
   

//========================================================
quadrant=["quadrant", "pqr,s", "int", "Geometry",
 " Given a 3-pointer (pqr) and a point, return the quadrant of S on the 
;; yz plane in a coordinate system where P is on the positive X and Q 
;; is the origin. 
;;
;;       
;;       6
;;  +----N-----+
;;  : 2  |  1  :
;;  :    |     :
;; 7-----P-----R- 5
;;  :    :     :
;;  : 3  :  4  :
;;  +----:-----+ 
;;       8                 R
;;                        /|  
;;                       / |
;;                      /  |
;;                     /   |
;;                    /    |
;;        S.---------/--T  |
;;        |:        /   |  |   _.'
;;        | :      /    |  :.-'
;;        U.-:----/-----+-'
;;          '.:  /  _.-'
;;            ':/.-'
;;   N---------Q---------
;;       _.-' 
;;    P-'
;;
;; quadrant(pqr,S) => 1
;;
"];

function quadrant(pqr,s)=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, N = N(pqr)
	, dST = distPtPl( s, pqr )
	, dSU = distPtPl( s, [N,Q,P] )
	, signs = [ -sign( dST ), sign( dSU )]
	)
(
	 hash( [[1,1],1
			,[-1,1],2
			,[-1,-1],3
			,[1,-1],4
			,[0,0],0
			,[0,1], 6
			,[-1,0], 7
			,[0,-1], 8
			,[1,0],  5
			], signs )
);
 


//========================================================
function quicksort(arr) = !(len(arr)>0) ? [] : let(
    pivot   = arr[floor(len(arr)/2)],
    lesser  = [ for (y = arr) if (y  < pivot) y ],
    equal   = [ for (y = arr) if (y == pivot) y ],
    greater = [ for (y = arr) if (y  > pivot) y ]
) concat(
    quicksort(lesser), equal, quicksort(greater)
);
    
//========================================================
rand=["rand", "m=1, n=undef, seed=undef", "Any", "Random, Number, String",
" Given a number (m), return a random number between 0~m.
;; If another number (n) is given, return a random number between m~n.
;; If m is an array or string, return one of its member randomly.
;;
;;   arr= [''a'', ''b'', ''c'']; 
;;
;;   rand(5)=3.63411
;;   rand(5,8)=6.4521
;;   rand(arr)=''b''
;;
;; 2015.2.26: add arg 'seed'
;;
;;   rand( 5,seed=3 )= 2.75399
;;   rand( 5,8, seed=3 )= 6.65239
;;   rand( arr, seed=3 )= ''b''
"
];
function rand(m=1, n=undef, seed=undef)=
(
	isarr( m )||isstr(m) ? m[ floor(rand(len(m),seed=seed)) ]
	: (isnum(n)
		? (seed==undef? rands( min(m,n), max(m,n), 1)
                     : rands( min(m,n), max(m,n), 1, seed) )
		: (seed==undef? rands( 0, m, 1)
                     : rands( 0, m, 1, seed) )
	  )[0]
);



//========================================================
randc=["randc", "r=[0,1],g=[0,1],b=[0,1]", "arr", "Random,Color" ,
"
 Given r,g,b (all have default [0,1], which is the max range),
;; return an array with random values for color. Examples:
;;
;; >> color( randc() )  sphere(r=1)
;; >> darker = [0, .4]  // smaller number
;; >> color( randc( r=darker, g=darker ) )  sphere(r=1)
"];
 
function randc( r=[0,1],g=[0,1],b=[0,1] )=
(
    [ rands(r[0],r[1],1)[0]
	, rands(g[0],g[1],1)[0]
	, rands(b[0],b[1],1)[0] ]	
);




//========================================================
randConvexPts=[[]];
/*
convex polygons – those with the property that 
given any two points inside the polygon, the entire line segment connecting those two 
points must lie inside the polygon. Such polygons are the intersection of half-planes, 
which means that if you extend the boundary edges into infinite lines the polygon will 
always lie completely on one side of each edge line. If a point is on the same side of each 
edge as the polygon, then it must lie inside the polygon.

http://www.cs.oberlin.edu/~bob/cs357.08/VectorGeometry/VectorGeometry.pdf
*/
function randOnPlaneConvexPts( count=4, pqr=randPts(3)  )=
(
3
 // THIS IS HARD	

);




//========================================================
randPt=["randPt", "d=3,x,y,z,seed", "pt", "Random,Point",  
"
 Get a random pt, covering the range defined by [-d, d]. Or d (distance 
;; to the ORIGIN) can be given as [-1,3], [0,3], etc.  Or, ranges along 
;; the x,y,z directions can be individually specified as x=3, (same
;; as x=[-3,3]), y=[-1,4], etc. Set seed to make it repeatable.
;; 
;;   randPt()          -3,+3 around ORIGIN
;;   randPt(1)         -1,+1 around ORIGIN
;;   randPt([4,5])      between 4, 5 of all axes 
;;   randPt([2,3],z=0)  between 2,3 on x and y on xy plane
;;   randPt(0, y=[2,4]) between y=2 and 4 along y axis
;;
;; Refer to randPts() that gives multiple random points; 
;; pqr() to get randPts(3), and pqr90() to get a pts of a random 
;; right triangle
"];

function randPt(d=3, x,y,z, seed)=
   let( d= isnum(d)?[-d,d]:d
      , _x= x==undef?d: isnum(x)?[-x,x]:x
      , _y= y==undef?d: isnum(y)?[-y,y]:y
      , _z= z==undef?d: isnum(z)?[-z,z]:z
      )
(   seed? 
    [ x==0?x: rands( _x[0], _x[1],1, seed )[0]
    , y==0?y: rands( _y[0], _y[1],1, seed )[0]
    , z==0?z: rands( _z[0], _z[1],1, seed )[0]
    ]
    :[ x==0?x: rands( _x[0], _x[1],1 )[0]
    , y==0?y: rands( _y[0], _y[1],1 )[0]
    , z==0?z: rands( _z[0], _z[1],1 )[0]
    ]
);


//========================================================
randPts=["randPts", "count,d,x,y,z", "pts", "Random, Point",
"
 Return random pts. See randPt() for arguments.
"];

function randPts(count=3, d=3, x,y,z, seed)=
(
  count==0? []: [for(i=range(count)) randPt(d,x,y,z,seed) ]
);


//========================================================
randInPlanePt=["randInPlanePt","pqr", "point","Random,Point"
, ""
];
function randInPlanePt(pqr)=
(
	[onlinePt( 
			[pqr[0], onlinePt([pqr[1],pqr[2]], ratio=rand())]
		   , ratio=rand()
		)
	,onlinePt( 
			[pqr[1], onlinePt([pqr[2],pqr[0]], ratio=rand())]
		   , ratio=rand()
		)
	,onlinePt( 
			[pqr[2], onlinePt([pqr[0],pqr[1]], ratio=rand())]
		   , ratio=rand()
		)
	][ floor(rand(3))]
);



//========================================================
randInPlanePts=["randInPlanePts","pqr,count=2", "point","Random,Point"
, ""
];
function randInPlanePts(pqr, count=2)=
(
   count>0? concat( [randInPlanePt( pqr )]	
                  , randInPlanePts( pqr, count-1)
                  ) :[]
);

//========================================================
randOnPlanePt=["randOnPlanePt","pqr,r=undef,a=360", "Point", "Random, Point"
,""];
//
// optional r : distance from pqr's incenterPt. Can be: a number, an array [a,b] for range
// optional a : random range of aXQP (X = returned Pt). Default 360. Can be [a1,a2] for range]  
//
function randOnPlanePt(pqr=randPts(3), r=undef, a=360)=
(
//  onlinePt( [pqr[floor(rand(3))], randInPlanePt(pqr)], ratio= rand(3) )
   anglePt( pqr
//		 [  pqr[0], incenterPt(pqr)
//		   ,pqr[1]
//		   ]
		  , a = isnum(a)?(rand(1)*a)
					: rands( a[0], a[1],1)[0]
			,r = r==undef? max( d01(pqr), d02(pqr), d12(pqr) ) * rand(1)
				:isarr(r)? rands( r[0], r[1],1)[0]
				:r
		)	
);



//========================================================
randOnPlanePts=["randOnPlanePts","count=2,pqr=randPts(3),r=undef,a=360", "Points", "Random, Point"
,""];
function randOnPlanePts(count=2, pqr=randPts(3), r=undef, a=360 )=
(
   count>0? concat( [randOnPlanePt( r=r, pqr, a=a )]	
                  , randOnPlanePts( count-1, pqr, r=r,a=a )
                  ) :[]
);



//========================================================
randRightAnglePts=["randRightAnglePts","r=rand(5),dist=randPt(),r2=-1","Points","Point,Random"
,"
 r: first arm length (default: rand(5) )
 dist: dist from ORIGIN. It could be a point (indicating translocation) or integer (default: rand(3))
	 If it is an integer, will draw at range inside [dist,dist,dist] location, and the Q will always be the one
	 closest to ORIGIN.	
 r2: 2nd arm length. If not given (default), auto pick a random value
	between 0~r

 randRightAnglePts( r=3, r2=3) gives points of a *special right triangle*
 in which 3 angles are 90, 45, 45.

 ref: pqr90() for a quick right triangle with that the arm lengths can't
      be specified.
"];

function randRightAnglePts( 
    r=rand(5), 
    dist=randPt(), 
    r2=-1
    )=
( 
    RMs( [ rand(360), rand(360), rand(360)] 
	   , //shuffle(
			[
			 [      r,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
			,[      0,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
			,[      0,r2>0?r2:rand(r), 0]+(isnum(dist)?[dist,dist,dist]:dist) 
		 	]
		//)
	   ) 
); 



//========================================================
range=["range", "i,j,k,cycle", "array", "index",

 " Given i, optional j,k, return a range or ranges of ordered numbers 
;; without the end point. 
;;
;; (1) basic
;;
;;   range(3)    => [0,1,2] // from 0 to 3, inc by 1 (= for(n=[0:i-1] )
;;   range(3,6)  => [3,4,5] // inc by 1  (= for(n=[i:j-1] )
;;   range(3,2,6)=> [3,5]   // inc by 2  (= for(n=[i:j:k-1] )
;;   range(6,3)  => [6,5,4]
;;   range(6,2,3)=> [6,4]
;;
;; (2) i= array or string
;;
;;   for(x=arr)        ... the items
;;   for(i=range(arr)) ... the indices 
;;
;;   range([3,4,5]) => [0,1,2]
;;   range( ''345'' )= [0, 1, 2]
;;
;; (3) cycle= i,-i, true: cycling (New 2014.9.7)
;;
;;   range( ... cycle= i|-i}true...)
;;
;; cycle= -1/+i: extend the index on the left/right side by 1 count
;; cycle= true: same as cycle=1 
;;
;;   range( 4,cycle=1 )   = [0, 1, 2, 3, 0] // Add first to the right
;;   range( 4,cycle=-1 )  = [3, 0, 1, 2, 3] // Add last to the left
;;   range( 2,5,cycle=2 ) = [2, 3, 4, 2, 3] // Add 1st,2nd to the right
;;   range( 2,5,cycle=-1 )= [4, 2, 3, 4]    // Add last to the left
;;   range( 2,1.5,8,cycle=2 )= [2, 3.5, 5, 6.5, 2, 3.5]
;;
;; range(obj,cycle=i,-1,true...)
;;
;;   range( [11, 12, 13],cycle=2 )= [0, 1, 2, 0,1]
"];

function range(i,j,k, cycle)=
let ( 
	 obj = isarr(i)||isstr(i)? i :undef
	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
	,beg= inum==1 
            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
    ,end= inum==1 
		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
		   : inum==2?j:k
		   
	,cycle=cycle==true?1:cycle

	,intv = sign(end-beg)        // when inum=3, the middle one is
			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	

	,residual= or( mod(abs(end-beg), abs(intv)), intv)

	,extraidx= abs(intv)>abs(beg-end) // End points to be skipped, 
	           ? 1                    // depending on the intv. If
	           : residual             // intv too large, set to 1
	,objrange= obj
			   ? range ( len(obj)
						, cycle=cycle ) 
			   : []
											 
)(
//	[beg, intv, end ]
//    [ for(i=[beg:intv:end-extraidx]) i	]

	( intv==0 || beg==end || (beg==0&&end==undef) || obj==[] || obj=="" )
	? []
	: obj  // Allowing range(arr) or range(str)
	  ? objrange

	  // Non obj : =================

	  : cycle>0
		  ? concat( 
				[ for(i=[beg:intv:end-extraidx]) i ]
				,[ for(i=[beg:intv:beg+(cycle-1)*intv]) i]
				)
		  : cycle<0
			?  concat( 
				[ for(i=[end+cycle*intv:intv:end-intv]) i]
				,[ for(i=[beg:intv:end-extraidx]) i ]
				)
		 	:[ for(i=[beg:intv:end-extraidx]) i	]
		  
);


//========================================================
ranges=["ranges", "i,j,k,cover=1,cycle=true", "array", "index",
 " Similar to range(), but instead of returning an arr of indices,
;; this func returns an ''array of *array of indices*'':
;;
;;   range(3)   = [0,1,2]
;;   ranges(3)  = [[0,1], [1,2], [2,0]]  // cover= 1 or [0,1]
;;   ranges(3..)= [[2,0], [0,1], [1,2]]  // cover= -1 or [1,0]
;;   ranges(3..)= [[0,1,2], [1,2,0], [2,0,1]] //cover= 2 or [0,2]
;;
;; This is achieved by setting the *cover* : 
;;
;;   cover=+n: cover n more indices on the right side 
;;   cover=-n: cover n more indices on the left side
;;   cover=[i,j]: set both sides together
;;
;;   obj = [10, 11, 12, 13]
;;
;;   ranges( obj )= [[0, 1], [1, 2], [2, 3], [3, 0]]
;;   ranges( 0,2,6,cover=[0,2] )= [[0, 2, 4], [2, 4, 0], [4, 0, 2]]
;;
;; Usage of the rest of arguments, i,j,k cycle, are the same as in range()
"];


function ranges(i,j,k, cover=1, cycle=true, _debug=[])=
let ( 
     cover= cover>0?[0,cover]:cover<0?[-cover,0]:cover // allow +n, -m, or [m,n]
	,obj = isarr(i)||isstr(i)? i :undef
	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
	,beg= inum==1 
            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
    ,end= inum==1 
		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
		   : inum==2?j:k
		   
	,intv = sign(end-beg)        // when inum=3, the middle one is
			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	

    ,sign = sign(end-beg)
    ,baserange= obj?range(obj):range(beg,intv,end) 
    ,left = cover[0]
    ,right= len(baserange)>1?cover[1]:0    
    
    ,newrange = concat( 
                 cycle?range( end-cover[0]*intv, sign*intv, end ):[]
                ,baserange
                ,cycle?range( beg, sign*intv, beg+intv*cover[1] ):[]
                )
//	,_debug= chkbugs( _debug,
//             ["cover",cover
//             ,"left",left
//             ,"right",right
//             , "baserange", baserange
//             , "newrange", newrange
//             ,"beg",beg
//             ,"end",end
//             ,"intv",intv
//             ]
//             )
        
)(
	( intv==0 || beg==end || (beg==0&&end==undef) || obj==[] || obj=="" )
	? []
	: obj  // Allowing range(arr) or range(str)
	  ? ranges ( len(obj)
				, cover=cover
				, cycle=cycle
				)

	  // Non obj : =================

	  : cover
        //? _debug
	  ?//[ _debug,   // uncomment to debug
           [ for( i=range(left,1, left+len(baserange)) )
               //range( i-left, i+right+1) //
               let( islast = i==left+len(baserange)-1
                  , cycledleft = (i==left&&!cycle?0:left) // When cycle, adj the
                  , cycledright= (islast&&!cycle?0:right) // 1st and last 
                  )
               [for( ii=range( i - cycledleft
                             , i + cycledright + 1 
                             ) ) newrange[ii] 
               ] 
           ]
         // ]      // uncomment to debug

	   // no cover  ---------------
       : cycle
		  ? concat( [for(i=[beg:intv:end])[i] ], [[beg]] )
		  :[ for(i=[beg:intv:end]) [i]	]
	  
);



//========================================================
repeat=["repeat", "x,n=2", "str|arr", "Array,String" ,
"
 Given x and n, duplicate x (a str|int|arr) n times.
"];

function repeat(x,n=2)=
(
	isarr(x)
    ?
	 ( n>0? concat(x, n>1?repeat(x,n-1):[]): [] )
	:
     ( n>0? str(x, n>1?repeat(x,n-1):""): "") 
);



//========================================================
replace =[ "replace", "o,x,new=\"\"", "str|arr", "String, Array",
"
 Given a str|arr (o), x and optional new. Replace x in o with new when
;; o is a str; replace the item x (an index that could be negitave) 
;; with new when o is an array.
" ];

function replace(o,x,new="", _i=0)=
    let( isa = isarr(o)
       , x= isa?fidx(o,x):x)
( 
  isa
  ? (inrange(o,x)
	? concat( slice(o, 0, x), [new], x==len(o)-1?[]:slice(o,x+1)): o 
	)
  : (index(o,x)<0? 
	  o :str( slice(o,0, index(o,x))
			, new 
			, (index(o,x)==len(o)-len(x)?""
				:replace( slice(o, index(o,x)+len(x)), x, new) )
		   )
    )
);

//function replace(o,x,new="",_i=0)=
//(  let( is_a = isarr(o)
//      , is_s = isstr(o)
//      , is_sa= is_a || is_s
//      , L = len(x)
//      , now = isa? o[_i]
//              : len(x)==1? o[_i]
//                :join( [ for( j=[_i:_i+L-1]) o[j]],"")
//      , _new = now==x?new:now
//      )               
//   isa
//   ?
//      ( _i>=len(o) ?[]
//       :concat( [_new], replace( o,x,new,_i+1)) 
//      )
//   :        
//      ( _i>len(o)-L ?""
//       :str( _new, replace( o,x,new,_i+L) )
//      )
//);   
                


//========================================================
reverse=[ "reverse", "o", "str|arr", "Array, String",
"
 Reverse a string or array.\\
"];

function reverse(o)=
(
	isarr(o)
	? [ for(i=range(o)) o[len(o)-1-i] ]
	: join( [ for(i=range(o)) o[len(o)-1-i] ], "" )
);

module reverse_test( ops ){

	doctest(
	reverse
	,[
		[  [2,3,4], reverse([2,3,4]), [4,3,2] ]
	   ,[  [2], reverse([2]), [2] ]
	   ,[ "''234''",   reverse( "234" ), "432"  ]
	   ,[ "''2''",   reverse( "2" ), "2"  ]
	   ,[ "''",   reverse( "" ), ""  ]
	 ], ops
	);
}



//========================================================
ribbonPts=["ribbonPts","pts,len=1,paired=true,closed=false", "Points", "Point"
, ""];
/*


	pts = [ p0,p1,p2 ...]

	ribbonPts( pts ) = [ pts1, pts2 ] 
		where pts1 = [N1,N2,N3 ...]
			 pts2 = [S1,S2,S3 ...]

	ribbonPts( pts, paired=true ) = [ pair_1, pair_2 ... ] 
		where pair_i = [Ni,Si]

	Note: 

	let 
		pairs = ribbonPts( pts, paired=true );
	then
		ribbonPts( pts, paired=false ) = [ getcol( pairs,0), getcol(pairs,1) ]
 
	or even easier:

		ribbonPts( pts, paired=false ) = transpose( pairs )


 // neighbors(arr, i, left=1, right=1, cycle=false)
 // neighborsAll(arr, left=1, right=1, cycle=false)

*/
function ribbonPts( pts, len=1, paired=true, closed=false,_I_=0 )=
(
	closed?[]
	:(
	_I_< len(pts)?
	concat( 	[ [normalPt( neighbors( pts, _I_), len=len ) 
			  ,normalPt( neighbors( pts, _I_), len=-len ) 
			  ]
			 ]
		 , ribbonPts( pts, len=len, paired=paired, closed=closed,_I_=_I_+1 )
		):[]
	)
  
);

//========================================================
RM=[ "RM","a3","matrix", "Geometry",
"
;; Given a rotation array (a3=[ax,ay,az] containing rotation angles about
;; x,y,z axes), return a rotation matrix. Example:
;;
;;  p1= [2,3,6]; 
;;  a3=[90,45,30]; 
;;  p2= RM(a3) * p1;  // p2= coordinate after rotation
;;
;; In OpenScad the same rotation is carried out this way:
;;
;;   rotate( a3 )
;;   translate( p1 )
;;   sphere(r=1); 
;;
;; This rotates a point marked by a sphere from p1 to p2
;; but the info of p2 is lost. RM(a3) fills this hole, thus
;; provides a possibility of retrieving coordinates of
;; rotated shapes.
;;
;; The reversed rotation from p2 to p1 can be achieved by simply applying
;; a transpose :
;;
;;  p3 = transpose(RM(a3)) * p2 = p1
"];


function RM(a3)=
(
	a3.z==0?
	IM*RMy(a3.y) * RMx(a3.x) 
	:RMz(a3.z) * RMy(a3.y) * RMx(a3.x) 

//  No idea why the following doesn't work with [0,y,0]:
//  Note: Run demo_reverse( "On y", p0y0(randPt()), "lightgreen");
//        in rotangles_p2z_demo();
//	(a3.z==0?IM:RMz(a3.z)) * 
//	(a3.y==0?IM:RMy(a3.y)) *
//	(a3.x==0?IM:RMy(a3.x)) 
	
);

module RM_test(ops){ doctest(RM,
[],ops); }

//RM_test( [["mode",22]] );
//doc(RM);

module RM_demo()
{
	a3 = randPt(r=1)*360;
	echo("in RM, a3= ",a3);
	rm = RM(a3);
	echo(" rm = ", rm);
}
//RM_demo();

function rotMx(a)=[ [1,0,0], 
                    [0,cos(a),-sin(a)], 
                    [0,sin(a),cos(a)]
                  ]; 

function rotMy(a)=[ [cos(a),0,sin(a)], 
                    [0,1,0], 
                    [-sin(a),0,cos(a)]
                  ]; 

function rotMz(a)=[ [cos(a),-sin(a),0], 
                    [sin(a),cos(a),0],
                    [0,0,1] 
                  ]; 


//========================================================
rotM= ["rotM","axis,a", "matrix", "Geometry",
" Rotation matrix for rotating angle a about line axis. 
;;
;; To rotate 30 degreen about x-axis:
;;
;;    x_axis= [ [1,0,0], [0,0,0] ]
;;    m = rotM( x_axis, 30 ) 
;;    pt2 = m*(pt- x_axis[1]) + x_axis[1]  
;;
;; Mainly used in in rotPt() and rotPts(). Most likely the rotPts()
;; is the one to go for rotation, in which the translation ( +/-
;; x_axis[1]) part is taken care of. So the above example usage can be:
;;
;;    pt2= rotPt( pt, x_axis, 30 )
;;    
;; Check out rotPt_demo() (in file scadx_demo.scad)
;; 
;; To apply this to OpenScad object (non-polyh):
;;
;;    translate( axis[1] ) 
;;    multmatrix(m= rotM( axis,a) ) 
;;    {
;;       translate( loc-axis[1] ) // loc: where obj is in xyz coord
;;       cube( [3,2,1] );
;;    }
;;        
;; Note: rotation of pts about any axis can also be achieved through
;; anyAnglePt(). However, anyAnglePt() can't be used on the OpenScad
;; made, non-polyhedron objects for such a task. In that case, 
;; rotM is needed. 
;; http://forum.openscad.org/Transformation-matrix-function-library-td5154.html
"]; 

function rotM(axis,a) = 
   /* 
       Rotate about any arbitrary axis pq.
       
       http://www.fastgraph.com/makegames/3drotation/
   */   
   let( 
//       qp = axis //reverse(axis)
//        /* if we move pq by moving p to ORIGIN, p=>q will be looking
//           outward from the ORIGIN. This will result in wrong direction
//           of rotation. So we need to reverse it.
//        */
//      //, a=-a
      , uv= onlinePt( reverse(axis) ,len=1)- axis[1]   // unit vector
      , x=uv[0]
      , y=uv[1]
      , z=uv[2]
      , c=cos(a)
      , s=sin(a)
      , t=1-c
      , m= [ [ t*(x*x)+c,   t*(x*y)-s*z, t*(x*z)+s*y ]
           , [ t*(x*y)+s*z, t*(y*y)+c,   t*(y*z)-s*x ]
           , [ t*(x*z)-s*y, t*(y*z)+s*x, t*(z*z)+c   ]
           ]     
//       , _d= chkbugs([],
//                [  "pq", pq
//               , "x", x  
//               , "y", y 
//               , "z", z  
//               , "c",c
//               , "s",s
//               , "t",t
//               , "m",m
//               ]
//               )
       ) 
( // _d               
   m
);

//========================================================
rotObj= ["rotObj", "axis,a,center=[0,0,0]", "n/a","Geometry",
" Module to rotate obj @center about axis for angle a.
;;
;; center is the translation when the obj is created.
;;
;;    rotObj( axis, a, center=loc )
;;        color(clr, 0.4) 
;;        cube( lens ); 
"
];

module rotObj( axis
             , a 
             , center=[0,0,0] // = translation of obj when it is built.
             )
{
    translate( axis[1] ) 
    multmatrix(m= rotM( axis,a) ) 
    {
       translate( center-axis[1] ) // center: where obj is in xyz coord
       children();
    }           
}    

//========================================================
rotPt=["rotPt","pt,axis, a","pt", "Geometry",
" Rotate pt for angle a about line pq.
"
];

function rotPt(pt,axis,a)=
   let( m=rotM(axis,a)
      )
(
   m*(pt-axis[0]) + axis[0] 
);


//========================================================
function rotPts(pts,axis,a)=
   let( m=rotM(axis,a)
      )
(
   [for(pt=pts) m*(pt-axis[0]) + axis[0] ] 
);

//========================================================
trfPts=["trfPts", "pqr,pqr2", "n/a", "Geometry",
" A module to transform an obj from a coordnate (defined by pqr)
;; to another (pqr2). Args pqr, pqr2 are any 3-pointer.
;; 
"];   
function trfPts( pts, pqr ) // Transform pts to align with pqr
= let(
   
    pqr0  = len(pts)==3? pts: slice( pts,0,3 ) // pick 1st 3 pts as pqr0
   , a_pt2 = angle( pqr0 )  
  , QR    = d12(pts)  
  , pqrc  =[ onlinePt( p10(pqr), len=d01(pqr0) )// Make a copy of pqr0
           , pqr[1]                              // on the pqr plane 
           , onlinePt( p12(pqr), len=QR )] // Used as the template
  
  // Translate to attach Qs 
  , Q = Q(pqr) // This Q remains unchanged till the end   
  , pts_t= [for(p=pts) p + Q-Q(pts) ] 
  
  // 1st rotation to align PQs of pqrtrl and pqrc
  , pqr_v = [ P(pts_t), Q, P(pqr) ]  // pqr to produce axis
  , a1= angle( pqr_v )
  , axis1 = reverse(normalLn( pqr_v) )         
  , m     = rotM( axis1, a1)  
  , pts_r1= rotPts( pts_t              // pqr after 1st rotation
                  , axis= axis1
                  , a = a1
                  )
  , D = othoPt( p021( pts_r1 ) )
     // Find a pt on pqr where R(pqr_r1) corresponds 
  , R2 = onlinePt( [Q, anglePt( pqr,  a_pt2) ]
                   ,len= QR )
     
    , pqr_v2 = [ R(pts_r1), D, R2 ] 
     
     ,axis2 = p01(pts_r1) 

     ,a2= angle( pqr_v2, refPt= axis2[0])
     
     
     ,m2 = rotM( axis2, a2)
     ,pts_r2 = rotPts( pts_r1
                  , axis= axis2
                 ,a = a2
               )
   )
(
    pts_r2
);
 
    

//========================================================
rotMx=["rotMx", "a", "matrix", "Math",
,""
];

function rotMx(a)=[[1,0,0,0], 
                     [0,cos(-a),-sin(-a),0], 
                     [0,sin(-a),cos(-a),0], 
                     [0,0,0,1]]; 

module rotMx_test(ops=["mode",13])
{
    doctest( rotMx,
    [
      [ 90, slice(rotMx(90)*[2,1,-2,1],0,-1)
            , [2, 2,1]
            , ["funcwrap", "slice({_}*[2,1,-2,1])"] 
      ]
    
    ]
    ,ops);
}
//angle=90;
//M= [ [cos(angle), -sin(angle), 0, 0],
//                [sin(angle), cos(angle), 0, 0],
//                [0, 0, 1, 1],
//                [0, 0, 0,  1]
//              ];
//echo( Mpt= M*[0,2,1,1] );

//========================================================
rotx=["rotx", "pt,a", "matrix", "Math",
,""
];

function rotx(pt,a)=
    let( pt = rotMx(a)* [ pt.x,pt.y,pt.z,1] )
    [pt.x,pt.y,pt.x]; 

module rotx_test(ops=["mode",13])
{
    doctest( rotx,
    [
      [ "[2,1,-1], 90", rotx([2,1,-1], 90)
            , [2, 1,1]
      ]
      , [ "[1,2,3], 90", rotx([1,2,3], 90)
            , [1,-3,2]
      ]
      , [ "[2,1,-1], 90", RMx(90)*([2,1,-1])
            , [2, 1,1]
      ]
      , [ "[1,2,3], 90", RMx(90)*([1,2,3])
            , [1,-3,2]
      ]
    ]
    ,ops);
}


//========================================================
RMx=["RMx","ax","matrix", "Geometry",
"
 Given an angle (ax), return the rotation matrix for
;; rotating a pt about x-axis by ax. Example:
;;
;;  pt = [2,3,4]; 
;;  pt2= RMx(30)*pt;  
;;
;; In openscad, you can rotate the pt to pt2 by thiss:
;;
;;  rotate( [30,0,0 ])
;;  translate( pt )
;;  sphere(r=1); 
;;
;; However,  the coordinate info of pt2 is lost. RMx
;; (and RMy, RMz) provides a way to retrieve it. This
;; is a work around solution to an often asked question:
;; how to obtain the coordinates of a shape.
"];	

function RMx(ax)= [
	[1,0,0]
	,[0, cos(-ax), sin(-ax) ]
	,[0,-sin(-ax), cos(-ax) ]
];

module RMx_test(ops)
{
	doctest( RMx
	,[
	
	],ops );
}
//RMx_test( ["mode",22] );
//doc(RMx);




////========================================================
RMy=["RMy","ay","matrix", "Geometry",
"
 Given an angle (ay), return the rotation matrix for
;; rotating a pt about y-axis by ay. See RMx for details.
"];	
function RMy(ay)=[
	 [cos(-ay), 0, -sin(-ay) ]
	,[0,        1,        0  ]
	,[sin(-ay), 0,  cos(-ay) ]
];
module RMy_test(ops)
{
	doctest( RMy
	,[
	
	],ops );
}
//RMy_test( ["mode",22] );



//========================================================
RMz=["RMz","az","matrix", "Geometry",
"
 Given an angle (az), return the rotation matrix for
;; rotating a pt about z-axis by az. See RMx for details.
"];

function RMz(az)=[
	 [ cos(-az), sin(-az), 0 ]
	,[-sin(-az), cos(-az), 0 ]
	,[       0,         0, 1 ]
];
module RMz_test(ops)
{
	doctest( RMz
	,[
	
	],ops );
}
//RMz_test( ["mode",22] );
//doc(RMz);


//========================================================
RMs=[[]];
function RMs( angles, pts, _i_=0 )=
(
	_i_<len(pts)? concat( [ RM(angles)*pts[_i_] ]
                         , RMs( angles, pts, _i_+1)
					  ):[]  
);




//========================================================
RM2z=["RM2z","pt,keepPositive=true", "matrix", "Geometry", 
"
 Given a pt, return the rotation matrix that rotates
;; the pt to z-axis. If keepPositive is true, the matrix will
;; always rotate pt to positive z. The xyz of the new pt on z-axis, ;;
;; pt2, is thus:
;;
;;  pt2 = RM2z(pt)*pt
;;
;; The reversed rotation, i.e., rotate a point on z back to pt,
;; can be achieved as:
;;
;;  pt3 = transpose( RM2z(pt) )*pt2  = pt
"];

function RM2z(pt,keepPositive=true)=
(
	/*pt.z==0
	?RM( rotangles_p2z(pt,keepPositive=true) ) 
	:RMy((keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/noRM(pt)))
	*RMx( sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z)) )
	*/
	RM( rotangles_p2z(pt,keepPositive=true) )
);

//========================================================
RMz2p=[ "RMz2p","pt","array", "Geometry",
"
 Given a pt, return the rotation matrix that rotates the pt to
;; z-axis. The xyz of the new pt, pn, is:
;;
;;  pn = RM2z(pt)*pt
"];

function RMz2p(pt)=
(
3
//ax = -sign(pt.z)*asin( pt.y/L );  // rotate@y to xz-plane
//	ay = pt.z==0?90:atan(pt.x/pt.z); 	// rotate from xz-plane to target 	
	
);

//echo("a<span style='font-size:20px'><i>this is a&nbsp;test</i></span>b");


//========================================================
rodfaces=["rodfaces","sides=6, hashole","array", "Faces",
 " Given count of sides (sides) of a rod( column, cylinder), return the 
;; faces required for the polyhedron() function to render the sides of
;; that rod. For a rod with sides=4 below: 
"];

function rodfaces(sides=6,hashole=false)=
 let( sidefaces = rodsidefaces(sides)
	, holefaces = hashole? 
				  [ for(f=sidefaces) 
					 reverse( [for(i=f) [i+sides*2] ])
				  ]:[]
	)
(
	hashole
	? concat( range(sides-1, -1)
			, range(sides, sides*2)
			, sidefaces
			) 	
	: concat( [ range(sides-1, -1) ]
			, [ range(sides, sides*2) ]
			, sidefaces
			)
);

//	p_faces = reverse( range(sides) );
//	// faces need to be clock-wise (when viewing at the object) to avoid 
//	// the error surfaces when viewing "Thrown together". See
//	// http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
//	q_faces = range(sides, sides*2); 
//concat( ops("has_p_faces")?[p_faces]:[]
//				 , ops("has_q_faces")?[q_faces]:[]
//				 , rodsidefaces(sides)
//				 , ops("addfaces")
//				 );




//========================================================
rodPts=["rodPts","","pts","Point",
 " Given 
;;
"];

function rodPts( pqr=randPts(3), ops=[] )=
 let( _ops = [ "r", 0.05
	  , "sides",     6
	  , "label", "" 
	  , "p_pts",undef  // pts on the P end
	  , "q_pts",undef  // pts on the q end
	  , "p_angle",90   // cutting angle on p end
	  , "q_angle",90   // cutting angle on q end
	  , "rotate",0  // rotate about the PQ line, away from xz plane
	  , "q_rotate", 0 // see twistangle()
	  , "cutAngleAfterRotate",true // to determine if cutting first, or
						  // rotate first. 
	  ]
	,ops = concat( ops, _ops )
	,r       = hash(ops, "r")
	,rot     = hash(ops,"rotate")
	,sides   = hash(ops,"sides")
	,p_angle = hash(ops,"p_angle")
	,q_angle = hash(ops,"q_angle")
	,q_rotate = hash(ops,"q_rotate")
	,qidx_rotate= round(q_rotate/360)*sides
	,cutAngleAfterRotate= hash(ops,"cutAngleAfterRotate")

	,pqr0= pqr==undef
			? randPts(3)
		 	: len(pqr)==2
		   	  ? concat( pqr, [randPt()] )
		   	  : pqr 
	,P = pqr0[0]
	,Q = pqr0[1]
	,R1= pqr0[2]
	,pq= [P,Q]
	,N = N(pqr0)
	,M = N2(pqr0)
	,T = othoPt( [ P,R1,Q ] )
	,p1_for_rot= angle(pqr0)>=90
				? P
				: onlinePt([norm(T-Q)>=norm(P-Q)? T:P,Q], len=-1 )
	,R2 = anyAnglePt( [ p1_for_rot, T, R1]     
                   , a= rot 
                   , pl="yz"
                   , len = dist([T,R1]) 
				  )
	,P2 = onlinePt( [P,Q], len=-1)

	,norot_pqr_for_p = [P2,P, R1]
	,norot_pqr_for_q = [P, Q, R1]
	,rot_pqr_for_p   = [P2,P, R2]
	,rot_pqr_for_q   = [P, Q, R2]

//	pqr_for_p = cutAngleAfterRotate&&rot>0
//				? rot_pqr_for_p
//				: norot_pqr_for_p;
//	pqr_for_q = cutAngleAfterRotate&&rot>0
//				? rot_pqr_for_q
//				: norot_pqr_for_q;		

	,p_pts90= arcPts( rot_pqr_for_p
				  , r=r, a=360, pl="yz", count=sides )

	,_q_pts90= arcPts( rot_pqr_for_q
				   , r=r, a=360, pl="yz", count=sides )
	, q_pts90= qidx_rotate
			   ? concat( slice(_q_pts90, qidx_rotate), slice(_q_pts90, 0, qidx_rotate)) 
			   : _q_pts90

	//-------------------------------------------------
	// Cut an angle from P end. 
	,Ap = anyAnglePt( cutAngleAfterRotate
					? [P2,P,R1]
					: [P2,P,R2]
				  , a=p_angle+(90-p_angle)*2
				  , pl="xy")
	,Np = N([ Ap, P, Q ])  // Normal point at P
	,p_plane = [Ap, Np, P ]  // The head surface 
	,p_pts=or( hash(ops,"p_pts")
			  , p_angle==0
			   ? p_pts90
			   : 	[ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], p_plane ) ]
			  )
	//-------------------------------------------------
	// Cut an angle from Q end. 
	,Aq = anyAnglePt( cutAngleAfterRotate
					? [P,Q,R1]
					: [P,Q,R2]					
				  , a=q_angle //90-q_angle
				  , pl="xy") // Angle point for tail
	,Nq = N( cutAngleAfterRotate
					? norot_pqr_for_q
					: rot_pqr_for_q )  // Normal point at Q
	,q_plane = [Aq, Nq, Q ]//:pqr0;  // The head surface 
	,q_pts =or( hash(ops,"q_pts")
			  , q_angle==0
			    ? q_pts90
			    : [ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], q_plane ) ]
			  )
//	,q_pts= qidx_rotate>0
//			? concat( slice( _q_pts, sides-qidx_rotate)
//		 			, slice( _q_pts, 0, sides-qidx_rotate)
//					)
//			: qidx_rotate<0
//	  			? concat( slice(_q_pts, qidx_rotate)
//						, slice(_q_pts, 0+qidx_rotate)
//		 				)
//				: _q_pts
	
)( 
	[p_pts,q_pts]
);




//========================================================
rodsidefaces=["rodsidefaces","sides=6,twist=0","array", "Faces",
 " Given count of sides (sides) of a rod( column, cylinder), return the 
;; faces required for the polyhedron() function to render the sides of
;; that rod. For a rod with sides=4 below: 
;;         
;;       _6-_
;;    _-' |  '-_
;; 5 '_   |     '-7
;;   | '-_|   _-'| 
;;   |    '(4)   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;; 
;; twist=0
;;
;;   [0,1,5,4]
;; , [1,2,6,5]
;; , [2,3,7,6]
;; , [3,0,4,7]
;;
;; twist = 1:
;;
;;       _5-_
;;    _-' |  '-_
;;(4)'_   |     '-6
;;   | '-_|   _-'| 
;;   |    '-7'   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;;
;;   [0,1,4,7]
;; , [1,2,5,4]
;; , [2,3,6,5]
;; , [3,0,7,6]
;;
;;
;; twist = 2:
;;       (4)_
;;    _-' |  '-_
;; 7 '_   |     '-5
;;   | '-_|   _-'| 
;;   |    '-6'   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;;
;;   [0,1,7,6]
;; , [1,2,4,7]
;; , [2,3,5,4]
;; , [3,0,6,5]
;;
;; Note that the top and bottom faces are NOT included. 
"];

function rodsidefaces( sides=6, twist=0) =
 let( top= twist
		   ? roll( range(sides,2*sides), count=twist) 
		   : range(sides,2*sides)
	)
(
	[ for(i = range(sides) )
	  [ i
	  , i==sides-1?0:i+1
	  , i==sides-1? top[0]: top[i+1]
	  , top[i]
//	  , i + 1 + (i- twist==sides-1?0:sides) -twist
//	  , i+sides - twist +( i==0&&twist>0?sides:0)
	  ]
	]
);




//========================================================
roll=["roll", "arr,count=1", "arr", "Array"
," Given an array (arr) and an int (count), roll arr items
;; as shown below:
;;
;; roll( [0,1,2,3] )   => [0,1,2],[3] => [3],[0,1,2] => [3,0,1,2]
;; roll( [0,1,2,3],-1) => [0],[1,2,3] => [1,2,3],[0] => [1,2,3,0] 
;; roll( [0,1,2,3], 2) => [0,1],[2,3] => [2,3],[0,1] => [2,3,0,1]
;;
;; Compare range w/cycle, which (1) returns indices (2) doesn't 
;; maintain array size:
;;
;; |> obj = [10, 11, 12, 13]
;; |> range( obj,cycle=1 )= [0, 1, 2, 3, 0]
;; |> range( obj,cycle=2 )= [0, 1, 2, 3, 0, 1]
;; |> range( obj,cycle=-1 )= [3, 0, 1, 2, 3]
;; |> range( obj,cycle=-2 )= [2, 3, 0, 1, 2, 3]

"];

function roll(arr,count=1)=
 let(L= len(arr)
	,count= count<0?L+count:count)
(
 	count==0 || count>L-1? arr
	:concat( [for(i= [L-count:L-1]) arr[i]]
		   , [for(i= [0:L-count-1]) arr[i]]
		   )
); 



//========================================================
rotangles_p2z=["rotangles_p2z","pt,keepPositive=true","Array","Geometry",
"
 Given a 3D pt, return the rotation array that
;; would rotate pt to a point on z-axis, ptz= [0,0,L],
;; where L is the len of pt to the ORIGIN.
;; Two ways to rotate pt to ptz using the rotation
;; array (RA) is obtained:
;;
;; 1. Scadex way: (has the target coordinate)
;;
;;    RA = rotangles_p2z( pt ); 
;;    ptz = RM( RA )*pt; 
;;    translate( ptz )
;;    sphere9 r=1 ); 
;;
;; 2. OpenScad way: (lost the target coordinate)
;;
;;    RA = rotangles_p2z( pt ); 
;;    rotate( RA )
;;    translate( pt )
;;    sphere( r=1 ); 
"
];
        
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);





//========================================================
rotangles_z2p=["rotangles_z2p","pt","Array","Geometry",
"
 Given a 3D pt, return the rotation array that
;; would rotate a pt on z, ptz (=[0,0,L] where L is
;; the distance between pt and the origin), to pt
;; Usage:
;;
;;    RA= rotangles_z2p( pt ); 
;;    rotate( RA )
;;    translate( ptz )
;;    sphere( r=1 ); 
"
];

/*
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);
*/
function rotangles_z2p(pt)=
(
	//// 2014.5.20: add 
	////   (pt.x<0?-1:1)
	//// to cover the case of : [x,0,0] when x<0
	////
	(pt.z==0&&pt.y!=0)
	?[ -sign(pt.y)*90, 0, -sign(pt.x)*sign(pt.y)*abs(atan(pt.x/pt.y))]  
	:[-sign(pt.z)*asin( pt.y/ norm(pt) )  // rotate@y to xz-plane
	 , pt.z==0?((pt.x<0?-1:1)*90):atan(pt.x/pt.z) 	      // rotate from xz-plane to target 	
	 , 0]

);	

module rotangles_z2p_test(ops)
{ doctest( rotangles_z2p, ops=ops ); }

//rotangles_z2p_test( ["mode",22] );
//rotangles_z2p_demo();


//========================================================
run=["run", "f,d", "val", "Core"
," 
"];

function run(f, d)=
    let( v= isarr(d)?d[0]:d )
(
    isarr(f)? run( f[0], slice(f,1))
    : f=="min"? min(d)
    : f=="max"? max(d)
    : f=="abs"? abs(v)
    : f=="sign"? sign(v)
    : f=="sin"? sin(v)
    : f=="cos"? cos(v)
    : f=="tan"? tan(v)
    : f=="acos"? acos(v)
    : f=="asin"? asin(v)
    : f=="atan"? atan(v)
    : f=="atan2"? atan2(v)
    : f=="concat"? concat( v, len(d)<=1?[]:run("concat", slice(d,1)))
    : f=="floor"? floor(v)
    : f=="round"? round(v)
    : f=="ceil"? ceil(v)
    : f=="ln"? ln(v)
    : f=="len"? len(v)
    : f=="log"? log(v)
    : f=="pow"? pow(v, len(d)==1||len(d)==undef?2:d[1])
    : f=="sqrt"? sqrt(v)
    : f=="exp"? exp(v)
    : f=="rands"? len(d)==3? rands(d[0],d[1],d[2])
                           : rands(d[0],d[1],d[2],d[3])                
    : undef
);     


//========================================================
_s= [ "_s", "s,arr,sp=''{_}''" , "str", "String",
"
 Given a string (s) and an array (arr), replace the sp in s with
;; items of arr one by one. This serves as a string template.
"
] ;

function _s(s,arr, sp="{_}", _i_=0)= 
( 
	index(s,sp)>=0 && len(arr)>_i_ ?
		str( slice(s,0, index( s,sp))
			, arr[_i_] 
			,  index(s,sp)+len(sp)==len(s)?
				"":(_s( slice( s, index(s,sp)+len(sp)), arr, sp, _i_+1))
		   ): s
);
	



//========================================================
scaleM=["scaleM", "v", "matrix", "Math",
"scale matrix
;;  ???
"
];

function scaleM(v)=[[v[0],0,0,0], 
                   [0,v[1],0,0], 
                   [0,0,v[2],0], 
                   [0,0,0,1]]; 

module scaleM_test(ops=["mode",13])
{
    doctest( scaleM,
    [ 
        [ [2,3,1], slice(scaleM([2,1,1])*[2,1,-2,0],0,-1)
            , [], ["funcwrap", "slice({_}*[2,1,-2,0])"] ]
    
    ]
    ,ops);
}

//========================================================
sel = ["sel", "arr, indices", "arr", "Array",
"Select items according to indices."
];
function sel(arr,indices)=[for(i=indices) arr[i]];

//========================================================
shift= [ "shift", "o,i=1", "str|arr", "String,Array" ,
" Return a str|arr short than o by one on the right side
;; = slice(o,0,-1).
" 
];
   
function shift( o )=
( 
	slice(o,0,-1)
);




//========================================================
shortside=["shortside","b,c","number","Geometry",
 " Given two numbers (b,c) as the lengths of the longest side and one of 
;; the shorter sides a right triangle, return the the other short side a.
;; ( c^2= a^2 + b^2 )."
,"longside"
];
function shortside(b,c)= sqrt(abs(pow(c,2)-pow(b,2)));



//========================================================
shrink= [ "shrink", "o,i=1", "str|arr", "String, Array",
"
 Return a str|arr short (on the left side) than o by one.\\
"
];

function shrink( o )= slice(o,1);


//========================================================
shuffle=["shuffle", "arr", "array", "Array",
" Given an array (arr), return a new one with all items randomly shuffled.
"];
function shuffle(arr, _i_=rand(1))=
(
	len(arr)>0? concat( [pick(arr, floor( len(arr)*_i_))[0] ]
					, shuffle( pick(arr, floor( len(arr)*_i_))[1], _i_=rand(1) )
					):[]
);

//module shuffle_test( ops=["mode",13] )
//{
//	arr = range(6);
//	arr2= [ [-1,-2], [2,3],[5,7], [9,10]] ;
//	doctest( shuffle,
//	[
//		str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr2)= ", shuffle(arr2) )
//		,str( "shuffle(arr2)= ", shuffle(arr2) )
//		,str( "shuffle(arr2)= ", shuffle(arr2) )
//	], ops, ["arr", arr, "arr2", arr2]
//	);
//}
////shuffle_test();



//========================================================
sinpqr= ["sinpqr","pqr","number","Math,Geometry",
 " Given a 3-pointer (pqr), return the len= sin( RT/QR ) where T is 
;; the R's projection on PQ line (T=othoPt([P,R,Q])).
;;
;;           R
;;          /| 
;;         / |.'
;;        / .'T
;;       /.'
;;  ----Q------
;;    .'
;;   P
"]; 

function sinpqr( pqr )=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, T= othoPt( [P,R,Q] )
	, RT= dist([R,T])
	, RQ= dist([R,Q])
	, onQside = norm(T-P) > norm(Q-P)?1:-1
	)
(
	sign(onQside)* sin( angle([R,Q,T]) )
);



//========================================================
slice=[ "slice", "s,i,j", "str|arr", "String,Array" ,
 " Slice s (str|arr) from i to j-1. 
;;
;; - If j not given or too large, slice from i to the end. 
;; - Both could be negative/positive.
;; - If i >j, do a reversed slicing
;;
;;   slice( ''012345'',2 )= ''2345''
;;   slice( ''012345'',2,4 )= ''23''
;;   slice( ''012345'',-2 )= ''45''
;;   slice( [10,11,12,13,14,15],2,20 )= [12, 13, 14, 15] // When out of range
;;
;; Reversed slicing:  
;;
;;   slice( [10,11,12,13,14,15], 5,2 )= [15, 14, 13]
;;   slice( ''012345'', 5,2 )= ''543''
;;   slice( ''012345'', -3,-5 )= ''32''
;;
;; Note that this func doesn't do cycling (i.e., when it is
;; out of range, it doesn't continue from the other end). 
;; Use *range()* for that purpose.
"
];


function slice(o,i,j)= 
 let( i= i<-(len(o))?0:fidx(o,i)
    , j=j==undef||j>len(o)-1? len(o): fidx(o, j)
	, L=len(o)
	, ret= i<j? [for(k=[i:j-1])o[k]]
			:i>j? [for(k=[L-i:L-j-1])o[L-k]]
		    :[]
	)
(
	isarr(o)? ret: join(ret,"")
);



//========================================================
slope= [ "slope", "p,q=undef", "num",  "Math,Geometry" ,
" Return slope. slope(p,q) where p,q are 2-D points or
;; slope(p) where p = [p1,p2]\\
"
];

function slope(p,q=undef)= q!=undef?
						((q[1]-p[1])/(q[0]-p[0]))
						:((p[1][1]-p[0][1])/(p[1][0]-p[0][0]));




//========================================================
split= [ "split", "s, sp=\" \", keep=false, keepempty=false", "arr", "String" ,
"
 Split a string into an array. 
;; 
;; New 2015.2.11: (1) arg keep, set true to keep the sp. Default false: 
;; 
;; split(''abc'' ''b'',false)=> [''a'',''c'']  // keep = false
;; split(''abc'',''b'',true) => [''a'',''b'',''c''] // keep = true
;;
;; New 2015.3.2: new arg: keepempty=false
;; 
;;  split( '''','','' )= []
;;  split( '''','','', keepempty=true )= ['''']
;;  split( '',t'','','' )= [''t'']
;;  split( '',t'','','', keepempty=true )= ['''', ''t'']
;;  split( ''ttt'' )= []
;;  split( ''ttt'', keepempty=true )= ['''', '''']
"
];


function split(s,sp=" ", keep=false,keepempty=false)=
 let(i = index(s,sp) 
    ,sp2= keep?[sp]:[]
    ,empty= keepempty?[""]:[]
	)	
(
	s==""? empty
    : s==sp?(keep?[s]:[])
    :i==0? concat( empty, sp2, split( slice(s,1), sp, keep, keepempty)  )
    :i==len(s)-1?  concat( [slice(s,0,-1)],sp2, empty)
    :i<0 ? [s]
	: concat(
		 [ slice(s, 0, i) ]
         , sp2
		 , split( slice(s, i+len(sp)), sp, keep, keepempty ) 
    )
);




//========================================================
splits= [ "splits", "s, sp=[], keep=false", "arr", "String",
 " Split a string into an array based on the item in array sp . 
;;
;; splits(''abcdef'',[''b'',''e'']) => [''a'',''cd'',''f'']
;;
;; Developer note: 
;;
;; Most other recursions in this file go through items in a list
;; with a pattern:
;;
;;   _I_&lt;len(arr)
;;   ? ...
;;   : []           // Note this
;; 
;; This code, however, demos a rare case of recursion into layers 
;; of a tree:
;;
;;   _I_&lt;len(arr)
;;   ? ...
;;   : arr          // major difference
;;
;; 2015.2.11: New arg: keep=false
"];

function splits(s,sp=[], keep=false, keepempty=false, _i=0)=
let(s = isarr(s)?s:[s] )
(
//    joinarr([ for(x=s) split(x, sp=sp[_i], keep=keep) ])
	_i<len(sp)
	?  splits( joinarr(
                [ for(x=s) split(x, sp=sp[_i], keep=keep, keepempty=keepempty) ])
             , sp=sp, keep=keep, keepempty=keepempty, _i=_i+1)
	:s	
);




//========================================================
squarePts=["squarePts", "pqr,p=undef,r=undef", "pqrs", "Point",
" Given a 3-pointer pqr, return a 4-pointer that has p,q of the
;; given pqr plus two pts, together forms a square passing through
;; r or has r lies along one side. This is useful to find the
;; square defined by p,q,r.
;;
;; Given [p,q,r], return [P,Q,N,M]
;;   P-------Q
;;   |      /|
;;   |     / |
;;   |    /  |
;;   M---R---N
;;
;;   P------Q
;;   |'-_   |:
;;   |   '-_| :
;;   |      '-_:
;;   |      |  ':
;;   M------N    R
;;
;; New 2014.7.15: new OPTIONAL arg: p, r to set size. 
;;
;;          p  
;;   P---|-----Q
;;   |   |    /| 
;;   |   |   / | r
;;   |   +--/---
;;   |     /   |
;;   '----R----'
"
];

//\\
// If size is given:\\
//\\
//         size
//   p---------------n
//   |'-           -'|
//   |  '-       -'  |
//   |    '-   -'    |
//   |      'x'      |
//   |        '-     |
//   |          '-   |
//   |            '- |
//   m---------------q

    
function squarePts(pqr, p=undef, r=undef)=
(
	p
	?( r
	   ? squarePts( [ anyAnglePt(pqr,a=90, len=r,pl="xy")
				  , Q(pqr)
				  , onlinePt(p10(pqr),len=r) 
				  ] )
	   : squarePts( [ onlinePt(p10(pqr),len=p), Q(pqr), R(pqr) ] )	 
	 ) 
	: r
	  ? squarePts( [ P(pqr)
				  , Q(pqr)
				  , anyAnglePt(pqr,a=90, len=r,pl="xy")	
				  //, onlinePt(p12(pqr),dist=r) 
				  ] )
	  : [  pqr[0]
	 	, pqr[1]
	 	, cornerPt( [pqr[2], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[1]]) 
	 	, cornerPt( [pqr[0], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[2]])
		]
);


//
////=============================================================
//subops=["subops", "parentops=false, subopsname='''',default=[]", "hash", "Args",
//"
// Set the ops of a sub-component of a parent (like, leg features of a
//;; robot). Given a parent ops (for robot properties), a subopsname (''leg''),
//;; and a default hash (like [''count'',4]), return:
//;;
//;;   [],      if parentops[subopsname] is false; 
//;;   default, if parentops[subopsname] is true; 
//;; else:
//;;   update( default, parentops[subopsname])
//;;
//;; A case study: F below has a subunit called Arm that is not added by
//;; default. Users can do F( [''arm'',true] ) to show it in predefined
//;; default, Arm ops, or give runtime ops to customize it.
//;;
//;; module F(ops){
//;;   ops= update( [ ( F's default ops here )
//;;                , ''arm'',false              // disabled arm
//;;                ], ops );                   // updated with runtime ops
//;;   armops= subops( ops, ''arm'', [''len'',5]); // set armops default and allows
//;;                                           // it be updated at runtime
//;;   if(armops) Arm( armops ); // armops has one of the following values:
//;;                             // false, [''len'',5], or updated version like [''len'',7]
//;; } 
//"
//];
//
//function subops(parentops=false, subopsname="",default=[])=
//(
//	hash(parentops, subopsname)==false?[]
//	:hash(parentops, subopsname)==true? default	
//		:update( default, hash(parentops, subopsname) )
//);
//
//module subops_test(ops)
//{
//	scope=[ "tire_dft", ["a",10]
//		 , "car1", ["seat",2, "tire",false ]
//		 , "car2", ["seat",2,"tire",true]
//		 , "car3", ["seat",2,"tire",["a",8, "d",15] ]
//		  ];
//	car1= hash(scope, "car1");
//	car2= hash(scope, "car2");
//	car3= hash(scope, "car3");
//	dft = hash(scope, "tire_dft");
//
//	doctest( subops,
//		[	
//		 "// When tire=false :"
//		, ["parentops=car1, subopsname=''tire'', default= tire_dft"
//			, subops( parentops=car1, subopsname="tire", default=dft)
//			,[]
//			]
//		,"// When tire is set to true at runtime :"
//		,["parentops=car2, subopsname=''tire'', default= tire_dft"
//			, subops( parentops=car2, subopsname="tire", default=dft)
//			,["a",10]
//			]
//		,"// When tire is given a value at runtime :"
//		,["parentops=car3, subopsname=''tire'', default= tire_dft"
//			, subops( parentops=car3, subopsname="tire", default=dft)
//			,["a", 8, "d", 15]
//			]
//		,"// When parentops not given:"
//		,["default=tire_dft"
//			, subops( default=dft)
//			,["a", 10]
//			]
//		], ops,["scope",scope]
//		);
//}
////doc(subops);
////subops_test();		




//========================================================
sumto =[  "sumto", "n", "int", "Math" ,
"
 Given n, return 1+2+...+n 
"
];

function sumto(n)= n+ (n>1?sumto(n-1):0);



//========================================================
sum=["sum", "arr", "number", "Math, Array",
"
 Given an array, return the sum of its items.
"
];

// New code (2015.4.11), not reply on other func
function sum(arr, _rtn=0, _i=0)=
(
    _i<len(arr)? 
        sum( arr, _rtn+arr[_i], _i+1 )
    : _rtn
);  
// NOTE: This ver is the more efficient "tail recursion"
// The old version is not: 
//  function sum(arr)=
//  (
//	  len(arr)==0?0
//	  :( arr[0] + sum(shrink(arr)) )
//  );





//========================================================
switch=["switch","arr,i,j","array", "Array",
 " Given an array and two indices (i,j), return a new array
;; with items i and j switched. i,j could be negative. If either
;; one out of range, return undef.
"];

function switch(arr,i,j)=
 let( L=len(arr)
    , i=fidx(arr,i)
	, j=fidx(arr,j)
    , errdata= ["vname","arr", "len", len(arr), "fname", "switch"]
    )
(
//    (i>= L || i<-L)? 
//        rangeErr( concat( ["iname","i","index",i], errdata) )
//    :(j>= L || j<-L)? 
//        rangeErr( concat( ["iname","j","index",j], errdata) )
//    :
    [ for (k=range(arr)) k==i? arr[j]: k==j?arr[i]:arr[k] ] 
);
//
//
////========================================================
//subarr=["subarr","arr, cover=[1,1], cycle=true", "array", "Array",
// " Given an array (arr) and range of cover (cover), which is in the form
//;; of [m,n], i.e., [1,1] means items i-1,i,i+1, return an array of items.
//;;
//;; Set cycle=true (default) to go around the beginning or end and resume 
//;; from the other side. 
//;;
//;; Note 20150206: slice ?
//"];
//
//function subarr(arr, cover=[1,1],cycle=true)=
// let( rngs = ranges( arr, cycle=cycle, cover=cover) )
//(
// [ for(a=rngs) [for(i=a) arr[i]]]
//);
//
//module subarr_test(ops=["mode",13])
//{
//	arr=[10,11,12,13];
//	doctest( subarr,
//	[
//		 ["arr", subarr( arr ), [[13,10,11],[10, 11,12],[11,12,13],[12,13,10]] ]
//		,["arr,cycle=false", subarr( arr,cycle=false ), [[10,11],[10, 11,12],[11,12,13],[12,13]] ]
//
//		,["arr,cover=[0,1]", subarr( arr,cover=[0,1] ), [[10,11],[11,12],[12,13],[13,10]] ]
//		,["arr,cover=[2,0]", subarr( arr,cover=[2,0] ), [[12,13,10],[13,10,11],[10,11,12],[11,12,13]] ]
//	],ops, ["arr",arr]
//	);
//}


//========================================================
symmedianPt=["symmedianPt","pqr","Point","Point"
, "Return the symedian pt of pqr, which is the S below:
;; 
;;    C---A---S
;;
;; where C = centroid pt, A= angleBisectPt of pqr.
"];

function symedianPt(pqr)=
   let( P=pqr[0], Q=pqr[1], R=pqr[2] )
(
    onlinePt( [centroidPt(pqr), angleBisectPt(pqr)], len=2)
);    

//========================================================
transpose= [ "transpose", "mm", "mm",  "Array" ,
"
 Given a multmatrix, return its transpose.
"
];

function transpose(mm)=
(
	[ for(c=range(mm[0])) [for(r=range(mm)) mm[r][c]] ]
);



//========================================================
trim=["trim", "o, l='' '', r='' ''", "arr/str", "Array, String",
, "Given o as a str/arry, trim the string or strings in arr on either
;; or both left and right. The characters to be trimmed away are 
;; defined in l or r. If defined as l=''abc'', all the chars a,b,c
;; on the far left will be trimmed: 
;;
;; l = ''abc''
;; ''abcxyz'' => ''xyz''
;; ''bcaxyz'' => ''xyz''
;; ''cbaxyz'' => ''xyz''

"];

function trim(o, l=" ", r=" ")=
(
    isarr(o)? [for(x=o) trim(x, l=l,r=r) ]
    : isstr(o)?
        index(l, o[0])>=0? trim( slice(o,1), l=l,r=r)
        : index(r, last(o))>=0? trim( slice(o, 0,-1), l=l, r=r )
        : o
    :o
);    

    
   
//========================================================
twistangle=["twistangle","pqrs", "number", "Angle",
 " Given a 4-pointer (pqrs), looking down from Q to R (using QR
;; as the axis), return the angle representing how much the line_RS
;; twists away from line_PQ. 
;;
;; The following graphs looks down from Q to R: 

;;  +-----:-----+   a: 0~90
;;  :     :  S  :  
;;  :     : /   : 
;;  :     :R    :
;;  ------Q-----P 

;;  +-----:-----+   a:90~180
;;  : S   :     :   
;;  :  '. :     : 
;;  :    R:     :
;;  ------Q-----P 

;;  ------Q-----P  a: 0~ -90
;;  :     :R    :
;;  :     : '.  :
;;  :     :   S :
;;  +-----:-----+ 

;;  ------Q-----P  a: -90~ -180
;;  :    R:     :
;;  :   / :     :
;;  :  S  :     :
;;  +-----:-----+ 
;;
;;  a_PAB + a_DCS      D   S
;;                     :  /|
;;                     : / |
;;                F    :/  |   
;;                :    /   |  .'
;;           E    :   /:  .|'
;;           :    :  / :.' |
;;  P    B   :    : / .C---:
;;  |'.  |   :    :/.'_.-''
;;  |  '.| --:----R--'------
;;  |    |.  :  .'
;;  |    | '.:.'
;; -|----|-_.Q -------    
;;  |.'_.|.'
;;  '----A'
;;     .'

"];
function twistangle( pqrs )=
 let( pqr= p012(pqrs), P=P(pqr), Q=Q(pqr), R=R(pqr), S= pqrs[3]
	, C= othoPt( [Q,S,R] )
	, D= projPt( S, pqr )
	, a= angle([D,C,S])	
	, quad = quadrant( [Q,R,P],S)
	, ks= [0,0,1,a,2,a,3,180-a,4,-a, 5,0,6,90,7,180,8,-90 ]
	)
( 
	 hash( ks, quad )
);






//========================================================
type=[ "type", "x", "str", "Type, Inspect" ,
"
 Given x, return type of x as a string.
"
];

function type(x)= 
(
	x==undef?undef:
	( abs(x)+1>abs(x)?
	  floor(x)==x?"int":"float"
	  : str(x)==x?"str"
		: str(x)=="false"||str(x)=="true"?"bool"
			:(x[0]==x[0])? "arr":"unknown"
	)
);

//========================================================
typeplp=["typeplp","x,dim=3","pt|ln|pl|false", "Type,Inspect",
"Check if x is one of ('pt','ln','pl',false), indicating that
;; x is a point, line, plane, or none of them, respectively"
];

function typeplp(x,dim=3)=
(    ispt(x,dim)?"pt"
    :isln(x,dim)?"ln"
    :ispl(x,dim)?"pl":false
);
    

//========================================================
ucase=["ucase", "s","str","String",
 " Convert all characters in the given string (s) to upper case. "
];
_alphabetHash2= split("a,A,b,B,c,C,d,D,e,E,f,F,g,G,h,H,i,I,j,J,k,K,
l,L,m,M,n,N,o,O,p,P,q,Q,r,R,s,S,t,T,u,U,v,V,w,W,x,X,y,Y,z,Z",",");

function ucase(s, _I_=0)=
(
  	_I_<len(s)
	? str( hash( _alphabetHash2, s[_I_], notfound=s[_I_] )
		, ucase(s,_I_+1)
		)
	:""
);


//========================================================
uconv=["uconv", "n, units=''in,cm'', utable=UCONV_TABLE)", "number", "Unit",
" Convert n's unit based on the *units*, which has default ''in,cm''. 
;; The conversion factor is stored in a constant UCONV_TABLE. To use
;; something not specified in UCONV_TABLE, do this:
;;
;;   uconv(n, ''u1,u2'', factor )
;;
;; Or you can define own table:
;;
;;   table=[''u1,u2'',factor]
;;   uconv( n, ''u1,u2'', table )
;;
;; n can be a string like \"2'8''\". If so the arg *from* is ignored.
;;
;; Related:
;;
;; num(''3'6\"'')= 3.5  // in feet
;; numstr( 3.5, conv=''ft:ftin'') = ''3'6\"'' // return str
;; ftin2in(''3'6\"'') = 42 
"];

UCONV_TABLE = [ "in,cm", 2.54
			  , "cm,in", 1/2.54
			  , "ft,cm", 2.54*12
			  , "cm,ft", 1/(2.54*12)
			  , "ft,in", 12
			  , "in,ft", 1/12	
			  ];
function uconv(n,units="in,cm", utable=UCONV_TABLE)=
let( 
	  frto = split(units,",")   
    //, isone = len(frto)==1    // units = "cm", "in", ... without ","
    , isftin= endwith(n, ["'","\""] ) 
	, m    = or(num(n), n) 
    , from = (isftin||len(frto)==1) ? "in":frto[0]
	, to   = or( frto[1] , frto[0] ) 
	, units= str(from, ",", to) 
    
	)
(
//    [ from, to ]
	(from==to) ? m
	: isnum(utable)? m* utable
    : m * hash( utable
			  , units )
);



//========================================================
update= ["update", "h,h1", "hash", "Hash" ,
"
 Given two hashes (h,h1) update h with items in h1.
"
];

function update(h,g)=
  let( h= isarr(h)?h:[]
     , g= isarr(g)?g:[] )
  g==[]? h //<=============== this line added 2015/3/17
  :[ for(i2=[0:len(h)+len(g)-1])      // Run thru entire range of h + g
      let(is_key= round(i2/2)==i2/2  // i2 is even= this position is a key
         ,is_h = i2<len(h)           // Is i2 currently in the range of h ?
         ,cur_h = is_h?h:g           // Is current hash the h or g?
         ,cur_i = is_h?i2:(i2-len(h)) // Convert i2 to h or g index
         ,k= cur_h[ is_key?cur_i:cur_i-1 ]  // Current key
         ,overlap = kidx((is_h?g:h),k)>=0   // Is cur k-v overlap btw h & g?
      )
      if( !(overlap&&!is_h)) // Skip if overlap and current hash is g
         is_key? k
               : overlap?hash(g,k):cur_h[cur_i]    
];






//========================================================
updates= ["updates", "h,hs", "hash", "Hash" ,
"
 Given a hash (h) and an array of multiple hashes (hs),
;; update h with hashes one by one.
;; 
;; New 2015.2.1: no hs, enter h as a one-layer array: updates(hs).
;; That is, updates the hs[0] with the rest in hs
"
];

function updates(h,hs)=
    let ( _h= hs==undef? h[0]:h
        , hs= hs==undef? shrink(h): hs 
        )
(	len(hs)==0? _h
	: updates( update(_h,hs[0]), shrink(hs) )
);


//========================================================
uv=["uv", "pq", "pt", "Geometry",
" Return the unit vector (len = 1 ) of pq "
];
function uv(pq)= onlinePt( pq ,len=1)- pq[0]; 

//========================================================
vals=[  "vals", "h", "arr",  "Hash",
"
 Return the vals of a hash as an array.
"
];

function vals(h)=
(
	[ for(i=[0:2:len(h)-2]) h[i+1] ]
);




//======================================
vlinePt=["vlinePt", "pqr,len=1", "points", "Point",
 " Given a 3-pointer pqr and a len(default 1), return a point T such that
;; 
;; (1) TQ is perpendicular to PQ 
;; (2) T is on the plane of PQR
;; (3) T is on the R side of PQ line
;; 
;;           T   R
;;        len|  / 
;;        ---| /  
;;        |  |/
;;   P-------Q
"];

module vlinePt_test( ops=["mode",13] )
{
	doctest( vlinePt, ops=ops );
}

function vlinePt(pqr,len=1)=
 let(Q=Q(pqr)
	,N=N(pqr)
	,T=N([N,Q,P(pqr)])
	)
(
	onlinePt( [Q,T], len=len )
);

module vlinePt_demo_0()
{_mdoc("vlinePt_demo_0",
"");

	pqr= randPts(3);
	P=P(pqr); Q= Q(pqr); R=R(pqr);


	T = vlinePt(pqr);
	echo("T=",T);
	//Line0( [Q,T] );
	pqt = [P,Q,T];
	Mark90( pqt, ["r",0.05, "color","red"] );
	Plane( pqt, ["mode",3] );
	MarkPts( [P,Q,R,T], ["labels","PQRT"] );
	Chain(pqt, ["r",0.05, "transp",1,"color","red"]); 
	Chain(pqr, ["r",0.15, "transp",0.5]); 
	
}
//vlinePt_demo_0();


lineRotation=[ "lineRotation", "pq,p2", "?", "Angle"
, " The rotation needed for a line
;; on X-axis to align with line-p1-p2
"];

////======================================
//// The rotation needed for a line
//// on X-axis to align with line-p1-p2
//// 
//function lineRotation(p1,p2)=
//(
// [ 0,-sign(p2[2]-p1[2])*atan( abs((p2[2]-p1[2])
//		                    /slopelen((p2[0]-p1[0]),(p2[1]-p1[1]))))
//    , p2[0]==p1[0]?0
//	  :sign(p2[1]-p1[1])*atan( abs((p2[1]-p1[1])/(p2[0]-p1[0])))]
//);

//===========================================
//
// Console html tools 
//
//===========================================

_SP  = "&nbsp;";
_SP2 = "　";  // a 2-byte space 
_BR  = "<br/>"; 

function _tag(tagname, t="", s="", a="")=
(
	str( "<", tagname, a?" ":"", a
		, s==""?"":str(" style='", s, "'")
		, ">", t, "</", tagname, ">")
);

function _div (t, s="", a="")=_tag("div", t, s, a);
function _span(t, s="", a="")=_tag("span", t, s, a);
function _table(t, s="", a="")=_tag("table", t, s, a);
function _pre(t, s="font-family:ubuntu mono;margin-top:5px;margin-bottom:0px", a="")=_tag("pre", t, s, a);
function _code (t, s="", a="")=_tag("code", t, s, a);
function _tr   (t, s="", a="")=_tag("tr", t, s, a);
function _td   (t, s="", a="")=_tag("td", t, s, a);
function _b   (t, s="", a="")=_tag("b", t, s, a);
function _i   (t, s="", a="")=_tag("i", t, s, a);
function _u   (t, s="", a="")=_tag("u", t, s, a);
function _h1  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h1", t, s, a); 
function _h2  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h2", t, s, a); 
function _h3  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h3", t, s, a); 
function _h4  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h4", t, s, a);  

function _color(t,c) = _span(t, s=str("color:",c));
function _red  (t) = _span(t, s="color:red");
function _green(t) = _span(t, s="color:green");
function _blue (t) = _span(t, s="color:blue");
function _gray (t) = _span(t, s="color:gray");

function _tab(t,spaces=4)= dt_replace(t, dt_multi("&nbsp;",spaces)); // replace tab with spaces
function _cmt(s)= _span(s, "color:darkcyan");   // for comment

// Format the function/module argument
function _arg(s)= _span( s?str("<i>"
					,dt_replace(dt_replace(s,"''","&quot;"),"\"","&quot;")//"“"),"\"","“")
					,"</i>"):""
					, "color:#444444;font-family:ubuntu mono");


//===========================================
//
//       Old History 
//
//===========================================

old_scadex_ver=[
["20150202_1", "getsubops: fixed warning (beg shouldn't be larger 
than end) by inserting let(h=isarr(h)?h:[],g=isarr(g)?g:[]) to update()"
] 
,["20150201_1", "updates() now can take one argument (instead of 2); Still debugging getsubops about beg shouldn't be larger than end warning."] 
,["20150131_1", "getsubops revised; tested"] 
,["20150130_1", "kidx(); Updated update();"] 
,["20150125_5", "Cone() new arg: twist."] 
,["20150125_4", "arcPts() new arg: twist."] 
,["20150125_3", "Arc() done."] 
,["20150125_2", "angleBisecPt: new arg len, ratio; hash:let notfound be shown when value is set to undef; Chainf(): fix bug."] 
,["20150125_1", "Chainf(): formatted Chain; Arc in progress"] 
,["20150124_1", "Change Arrow args from arrow to head. Text() in progress"] 
,["20150123_1", "Dim2's guides and arrows seem to be done. "] 
,["20150122_1", "hashex done. getsubops seems tested ok, checking if can use in Dim2 "] 
,["20150121_2", "getsubops in progress. hashex in progress "] 
,["20150121_1", "Dim2 in progress. "] 
,["20150120_1", "Arrow doc and demos. "] 
,["20150119_2", "Arrow (the line and arrow faces not yet aligned. "] 
,["20150119_1", "Cone(), w/new feature: set markpts on the fly"] 
,["20150117_2", "CurvedCone_demo_1() cleanup"]
,["20150117_1", "getSideFaces(), tested"
              , "getTubeSideFaces(), tested"
              , "mergeA(), tested"
              , "CurvedCone, CurvedCone_demo_1()"
               ]
,[ "20150116-1", "CurvedCone first step done. Check CurvedCone_demo_1()", 
    "Change some argument name r to len" 
]
,[ "20140921-1","Change args of isOnPlane()" 
]
,[ "20140918-1","New:roll(arr,count)" 
]
,[ "20140916-1","join(): change default sp from , to empty str."
			,"numstr(): change arg maxd back to dmax." 
]
,[ "20140914-1","New ranges(): similar to range, but return array of arrays."
			, "Both range() and ranges() now return only indices. No more return obj content. Both need docs." 
]
,[ "20140912-1","range(): Revert to entire 0909-1. Re-design range to have simpler args: range(i,j,k, cycle)." 
]
,[ "20140910-2_segmentation_fault","range(): revert this func back to 0909-1, but it causes segmentation fault. Next version: copy entire 0909-1" 
]
,[ "20140910-2","range(): revert it back to 0909-1." 
]
,[ "20140910-1","Re-design range() for partial obj ranging: not yet succeed but already very very slow (too much condition checks in each call). Will revert it back to 0909-1 in next ver." 
]
,[ "20140909-1","Re-design range(): remove args: maxidx, obj; allow cycle= i,-i" 
]
,[ "20140901-1","New:vlinePt()." 
]
,[ "20140831-2","onlinePt: change arg name dist to len." 
]
,[ "20140831-1","Misc improvements. (mainly Rod())" 
]
,[ "20140825-1","New: twistangle()" 
]
,[ "20140823-1","New arg s for angle().","New quadrant()" 
]
,[ "20140822-3","faces() demo of shape ''tube''. Pretty cool." 
]
,[ "20140822-2","New: rodPts(). Makes Rod() uses it."
			, "New: rodfaces(), faces()" 
]
,[ "20140822-1","Rod(): Fix another rotation bug. Doc done, too."
			, "LabelPts: default prefix changed from P to '', begnum from 1 to 0" 
]
,[ "20140821-3","Rod(): Fix rotation bug found earlier today"
]
,[ "20140821-2","Rod(): New args markrange, markOps, labelrange, labelOps; remove showsideindex",
	"Rename mdoc() to parsedoc()"
]
,[ "20140821-1","Bug in Rod : sometime rotate to wrong side. See Rod_demo_8_rotate()"
]
, [ "20140820-1","New mdoc(), _mdoc()"
			  , "Fix major Rod bugs (guess so). More demo with _mdoc"
]
,[ "20140819-1","New Rod arg: rotate; more Rod demos (including gears)"
]
,[ "20140818-3","New: subarr()"
]
,[ "20140818-2","Revised range(): add new args: obj and isitem"
]
,[ "20140818-1", "Revised fidx(o,i): o can be an integer"
			 ,"Revised range(): remove arg obj, add new arg cover and maxidx"
]
,[ "20140817-2", "New: lpCrossPt()"]
,[ "20140817-1", "Rename: rodfaces() to rodsidefaces()"
			 , "New: Rod()"]
,[ "20140816-3", "New: rodfaces()"]
,[ "20140816-2", "New arcPts, good implementation. "]
,[ "20140816-1", "New for LabelPts: args: prefix, suffix, begnum, usexyz; Use new builtin text()"]
,["20140815-1","Done with the migration to OpenSCAD 2014.05.31 snapshot in terms
			of doctest behaviors. Need to fix shape making. Somehow it seems that
			the rotation of points goes wrong."]
,["20140814-1","New:dels()"
	, "Improved: isequal(), now can take var of non-number. tests done"
	, "is0: doc and tests done."]
,["20140812-1","Fix bug: endwith(), istype(). Doc and tests done."]
,["20140811-2","New: begwith and endwith can check if beg/end with any of [a,b,...] ", "Working on: new istype(), improving uconv()"]
,["20140811-1","New: splits()", "Rename: trueor()=> or()"]
,["20140810-1","Disable neighbors(). The new range() can cover it."
			, "consider to disable rearr(). The new list comp can handle it.==> maybe not"
			, "Make range() accept str as the 1st arg as well, so range(''abc'')=[0,1,2]. This is used in reversed()"
			, "New arg for fidx: fitlen"
			, "Simplify slice code, allowing reversed slicing" 
			, "New: joinarr()"]
,["20140809-1","Rename all *iscycle* to *cycle*"
			, "Del range() and copy range2() code to range()"
			, "range2(): new arg: obj,cycle. Doc and test done."
]
,["20140808-3","Remove fitrange(). It can be replaced by fidx()"
			,"New: range2(), much elegantly written than range()."
			,"Re-evaluating the need of neighbors ... it might be able to use range2() instead"
]
,["20140808-2","New function: fidx(). This should be very powerful. "
]
,["20140808-1","Disabled for future removal: mina, maxa, inc, hashi, hashkv"
			,"Renamed: multi()=>repeat(), midCornerPt()=>cornerPt()"
]
,["20140807_1", "START CODING WITH 2014.05.31/Linux 64bit"
			, "Remove addv (too easy to code with list comp so no need a func)"
			, "New arg for del(): *count*."
			, "New func: fitrange() (consider to replace inrange in the future )" 
			, "Re-chk code for use of OpenScad_DocTest for doc testing --- down to hash()"
		    
]
,["20140730_1", "New: prependEach(), permute()"
]
,["20140729_1", "New:switch()", "New: ucase(), lcase()"
			,"New: Error message reporting in switch(). Consider apply it to other functions in the future"
]
,["20140715_5", "New:cubePts(), Cube() done (Cube() needs doc)"]
,["20140715_4", "squarePts(): fix runtime error. "]
,["20140715_3", "Dim(): Fixed bug (S= squarePt(pqr,[''mode'',4]): remove mode. "]
,["20140715_2", "New: addv()"]
,["20140715_1", "New optional args p,q for squarePts() to set sizes." 
             , "_TODO_: to change boxPts from anti-clockwise to clockwise for pqr polyhedron geometry making. --- may not need it after squarePts() has p,r "]
,["20140714_1", "New: p012(pts) ...p320(pts) 24 funclets"
			, "New: p2(), p3(), p4() 3 funclets" ]
,["20140713_2", "numstr(): change arg dmax to maxd; Dim(): new arg maxdecimal." ]
,["20140713_1", "New args for Dim(): conv, decimal." ]
,["20140712_1", "numstr() done." ]
,["20140711_4", "New: uconv() --- need more tests." ]
,["20140711_3", "num() now can convert f'i'' to feet." ]
,["20140711_2", "New: ftin2in()" ]
,["20140711_1", "New: num(numstr)=>num" ]
,["20140710_1", "Working on: new: numstr() for Dim() to use" ]
,["20140709_2", "rand() can take an array or string, return one item randomly." ]
,["20140709_1", "Introducing groupdoc feature and apply it to addx/addy/addz" ]
,["20140708_3", "addx, addy, addz improved, plus their docs and tests. Remove add_z_to_pts" ]
,["20140708_2", "rename doctest to doctest_old, doctest2 to doctest; MOVE this new doctest to a new file scadex_doctest.scad" ]
,["20140708_1", "doctest2: simplify doc from fname=[[fname,arg,retn,tags],doclines] to [fname,arg,retn,tags,doclines]; also using ; instead of \\ for linebreak in doclines." ]
,["20140707_2", "More demos for anyAnglePt." 
]
,["20140707_1", "anglePt bug fixed: failed when a=90 (detected by anyAnglePt_demo_4();" 
]
,["20140703_1", "neighbors(), neighborsAll(): change default of cycle to true"
		   , "normalPts(): fix bug (arg *len* was written as dist in the code)"
		   , "normalPt: change default:len=undef, new: reversed=false" 
]
,["20140701_1", "angle(): use simpler formula.","anyangle()", "P(),Q(),R(),N(),M()"
			 ]
,["20140630_1", "randOnPlanePt: new arg a (for angle); and change the center of pts from incenterPt to Q" ]
,["20140629_1", "anglePt(): change angle to aPQR (originally aQPR) and reaname arg from (pqr, ang, len) to (pqr,a,r).", "Fix randOnPlanePts (it uses anglePt) accordingly" ]
,["20140628_2", "normalPt(), normalPts()"]
,["20140628_1", "expandPts(). cycle=false needs fix."]
,["20140627_4", "neighbors(), neighborsAll()"]
,["20140627_3", "mod()", "remove increase(). Has an inc() already."]
,["20140627_2", "get() got a new arg cycle", "In work: intersectPt(), expand()"]
,["20140627_1", "Output scadex version info when loaded."]
,["20140626_5", "LabelPts_demo_2_scale()"]
,["20140626_4", "increase(pts,n=1)", "LabelPts and MarkPts_demo() can set labels=range(pts) instead of range(len(pts))"]
,["20140626_3", "LabelPts Bug fixed.(demo with LabelPts_demo() and MarkPts_demo())"]
,["20140626_2", "MarkPts: Now can set labels", "LabelPts Bug found: fail to output label when pts contains only one item."]
,["20140626_1", "ishash()"
			, "Improved LabelPts arguments (shift)"
			, "range can do range(arr)"]
,["20140625_5", "Pie() 2D"]
,["20140625_4", "projPt() code, doc and test done."]
,["20140625_3", "Normal() new argument:reverse; Also show arrow but not just line."]
,["20140625_2", "Revising Arc()=> failed. But made a good 2D pie."]
,["20140625_1", "between() code, doc and test done."]
,["20140624_7", "randRightAnglePts improved (allows for dist= a point or an integer."]
,["20140624_6", "Bugfix for pick() and shuffle()."]
,["20140624_5", "shuffle() code, doc and test done."]
,["20140624_4", "pick() code, doc and test done."]
,["20140624_3", "range() code, doc and test done. "]
,["20140624_2", "randRightAnglePts() improved to allow fixed sizes right tria. "]
,["20140624_1", "randRightAnglePts() done. "]
,["20140623_4", "det(), RMs()", "Working on randRightAnglePts()"]
,["20140623_3", "Fix randOnPlanePt(), randOnPlanePts()"]
,["20140623_2", "Bug fix for anglePt()"]
,["20140623_1", "p021, p120, p102, p201, p210", "rearr()", "incenterPt()", "Bug in doctest when mode= 2"]
,["20140622_3", "randInPlanePts(), randOnPlanePt, randOnPlanePts():this one needs fixes"
]
,["20140622_2", "rand()", "p01() ... p03() ... p30()", "randInPlanePt()"
]
,["20140622_1", "lineCrossPt(), crossPt()", "dot()", "is0()", "prodPt()", "addx, addy, addz, dx, dy, dz"
]
,["20140620_1", "Default *closed* param in Chain set to true"
 , "Mark90 seems to have false-positive error"
 , "making anglePt() ==> doesn't work very well. Need more work"
 , "include PGreenland's TextGenerator."
 , "COLORS for default colors. Set LabelPts default colors to COLORS"
]
,["20140619_3", "Plane improved: len(pqr) can be >3. But all faces connect to pqr[0]"]
,["20140619-2", "LabelPt(), LabelPts(). Both need doc. "
  ,"randPt, randPts x,y,z range doesn't seem to work", "LabelPts()" ]
,["20140619-1", "ColorAxes()"]
,["20140618-1", "Dim(), needs fixes on the angle when vp=false"]
,["20140613-5", "Rename: dx=>distx, dy=>disty, dz=>distz."]
,["20140613-4", "Done rewriting doc and test to fit new doctest."]
,["20140613-3", "new func: select()"
		,"Rewriting doc and test to fit new doctest ... down to split()"]
,["20140613-2", "newx(), newy(), newz()", "tests for isOnlinePt, planecoefs"
		,"Rewriting doc and test to fit new doctest ... down to planecoefs()"]
,["20140613-1", "New: isOnPlane()"
		,"Rewriting doc and test to fit new doctest ... down to Plane()"]
,["20140612-2", "onlinePt takes only 3d pts.", "Random tests for onlinePt()"
		,"Rewriting doc and test to fit new doctest ... down to onlinePts()"]
,["20140612-1", "Done migration from index(x,o) to index(o,x)."]
,["20140611-3", "About to make a big change to change index(x,o) to index(o,x) to be 
consistent with other functions."]
,["20140611-2", "doctest: change arg from (doc,recs=[], ops=[], ops2=[]) 
to (doc,recs=[], ops=[], scope=[]). So we can do:
 doctest(doc, recs, ops, [''i'',3])
but not 
 doctest(doc, recs, ops, [''scope'',[''i'',3]] )
"]
,["20140611-1", "doctest: for functions difficult to test (like, _fmt), testcount is 0, 
so do not show 'Tests:', show 'Usage:' instead."
]
,["20140610-5", "scadex_doctest: some improvements."
			, "Rewriting doc and test to fit new doctest ... down to echoblock()" ]
,["20140610-4", "scadex_doctest: some improvement on its header" ]
,["20140610-3", "doctest(): header adds some highlighting; need re-factoring but we'll leave it for now. " ]
,["20140610-2", "doctest() done. " ]
,["20140610-1", "Rename scadex_test to scadex_doctest and improve header, footer" ]
,["20140609-3", "Fine-tuning doctest()"
		, "On the way to change doctest mode 0 to list all func names" ]
,["20140609-2", "DT_MODES; New doctest mostly done. Needs only minor adjustment. " ]
,["20140609-1", "Combining doc() and test() into doctest() about complete. " ]
,["20140608-1", "Combine doc() and test() into doctest(). They were previously
together but separated around 5/29. Put them back to doctest(), and aiming to
have doc() and test() call doctest() with specific parameters." ]
,["20140607-1", "Using concat (instead of update) to setup ops in test(). Will gradually adapt this pattern for all modules." ]
,["20140606-3", "New module Normal(pqr)", "Fix bug in angle() for angle >90"
			, "MarkPts: Fix err that [''grid'',[ops]] doesn't have effect" ]
,["20140606-2", "New ops *funcwrap* for test. Chk angleBisectPt_test() for example"]
,["20140606-1", "New func angle(), _fmth() ", "test() uses isequal() for number comparison" ]
,["20140605-4","Rename h_() to _h(), s_() to _s()"]
,["20140605-3","test() refined."]
,["20140605-2","New func subops(); test() now can show doc (when [''showdoc'',true])."]
,["20140605-1","New function _getDocLines(), so doc can be called to either doc() or test()."]
,["20140604-1","doc for test() finished."]
,["20140603-1","Rename midAnglePt to angleBisectPt"
	,"Rename pts_2d_2_3d to add_z_to_pts"
	,"Done revising doc of each funcmod for new doc() from 
      top down"]
,
["20140601-2","New: isbool, isequal", "Remove ishash, LineNew"
	,"Disable midAngleCrossLinePts()"
	,"Revising doc of each funcmod for new doc() from 
      top down ... to midAnglePt() "]
, 
["20140601-1","Revising doc of each funcmod for new doc() from 
              \\ top down ... to isarr() "]
,["20140531-1","Remove fontPts(), hasht()", "Rename hashitems to hashhkvs"
		, "When showing doc header, replace _ with ＿ (_ -> ＿) to make it higher" 
		, "Revising doc of each funcmod for new doc() from top down ... to hashkvs "]
,["20140530-7","Remove Box()"
		 , "Revising doc of each funcmod for new doc() from top down ... to echoblock."]
,["20140530-6","Revising doc of each funcmod for new doc() from top down ... to Block."]
,["20140530-5","doc(): better algorithm to speed up. Also allows for correct \\
	display of mono-spacing text. We finally have a system that displays code and \\
	arranged ascii graph correctly in both the code editor and console. "]
,["20140530-4","doc(): new doc format that uses double back-slash \\
	to indicate line-breaking", "_tab(): convert tab to html spaces"]
,["20140530-3","test() done, including scoping for test records "]
,["20140530-2","test() mostly done. "]
,["20140530-1","test() basic done. Still buggy."]
,["20140529-3","Start to seperate doctest into doc() and test(). doc() done."]
,["20140529-2","Basic feature of doctests works fine."]
,["20140529-1","Rename fmt to _fmt. Failed to make deep fmt into array."]
,["20140528-4","console html tools", "Working on doctest2", "fmt()"]
,["20140528-3","onlinePts()"]
,["20140528-2","PtGrids for multiple points. Note that the 'scale' feature in PtGrid is disabled now. "]
,["20140528-1","Disgard hashif; add new arg to hash: if_v, then_v, else_v"]
,["20140527-3","hash: fix default argument recursion error", "working on hashif()"]
,["20140527-2","Mark90 auto adjusts the size of L to fit small pt xyz"]
,["20140527-1","Remove PtFrame; Remove the DashLine style op in PtGrid"]
,["20140526-1","Rename PtGrids to PtGrid and significantly simplies it to reduce process time, including change the use of DashLine to Line"]
,["20140525-1","accumulate()", "onlinePts()"]
,["20140524-1","randPts() allows setting x,y,z = 0 to put pt on plane or axis."]
,["20140522-6","Rename Line0 to Line_by_x_align and LineNew to Line0"]
,["20140522-5", "Dashline: Fix bug of over-draw when dash > line length  " ]
,["20140522-4", "LineNew()." ]
,["20140522-3", "RodCircle: done. Seems rodrotate in all conditions are good." ]
,["20140522-2", "RodCircle:Fix problems with the rodrotate --- almost" ]
,["20140522-1", "RodCircle, almost done", "inc()", "pts_2d_2_3d()", "Fixed arcPts: at a=360, should remove the last one, but not the first" ]
,["20140521-3", "randc() for random color", "OPS as a common ops for all shapes" ]
,["20140521-2", "New func: updates(h,hs) for multiple updates."
			, "Remove the feature of multiple hashes in update." ]
,["20140521-1", "Improved update: can update up to 3 hashes at once" ]
,["20140520-4", "Working on : More Rings; improving update" ]
,["20140520-3", "One more demo for Ring" ]
,["20140520_2-2", "Done docs for new hash design. Ready to merge back to scadex" ]
,["20140520_2-1", "Found that all files before this have wrong year tag (2013 should have been 20140. Embarrassing.", "Finish the doc for rotangles_p2z and rotangles_z2p" ]
,["20130518-3", "New: hash2 using [k,v,k,v ...] but not [[k,v],[k,v]...] as data. " ]
,["20130518-2", "Ring: add feature to mark rod colors with ''markrod''", "Ring_demo() --- weird circle line around the line through. " ]
,["20130518-1", "Line: new arg extension0/extension1 for line extension", "arcPts: add ''a==360&&_i_==0?[]:'' to skip _i_=0 when a=360"
			,"shortside(),longside()" ]
,["20130517-4", "New module Ring()"]
,["20130517-3", "New function dist(pq) (same as norm(p-q))", "onlinePt got new argument *dist*"]
,["20130517-2", "Rename all rm (rotation matrix) to RM."]
,["20130517-1", "Let rm2z uses rotangles_p2z"]
,["20130516-3", "Revising doc down to reverse()", "replace(): add replacing array item"]
,["20130516-2", "Mark90()"]
,["20130516-1", "is90()"]
,["20130515-1", "Found Line's bug source in rotangles_z2p( Not in rotangles_p2z). Fixed"]
,["20130514-5", "Fix bug (unable to rotate [x,y,0] to z) in rotangles_p2z. The bug in Line still there"]
,["20130514-4", "The bug in Line could be due to the new-found bug in rotangles_p2z -- unable to rotate [x,y,0] to z-axis. "]
,["20130514-3", "Revising function docs to use new doctest"
  , "Found bug in Line (but not in Line0): unable to draw lines on xy-plane (like [[1,1,0],[-1,2,0]]. "]
,["20130514-2", "Revising function docs to use new doctest"
  , "Add *markpts* argument to Line0 and Line"]
,["20130514-1", "Revising function docs to use new doctest", "Rename getd() to trueor()", "Make hash return [v1,v2...] when item = [k,v1,v2...]", "Rename the argument *strick* in ishash to *relax"]
,["20130513-6", "Revising function docs to use new doctest", "Remove h_ feature from echo_ and create a new func echoh"]
,["20130513-5", "Revising function docs to use new doctest", "Func del allows byindex" ]
,["20130513-4", "Revising function docs to use new doctest", "Rename countType??? to count???" ]
,["20130513-3", "doctest: fine tuning; Revising function docs to use new doctest" ]
,["20130513-2", "doctest: fine tuning; Revising function docs to use new doctest" ]
,["20130513-1", "doctest: fine tuning, looks good" ]
,["20130512-4", "doctest: fine tuning, new arg: mode" ]
,["20130512-3", "doctest done, remove DT_old and doctest3" ]
,["20130512-2", "doctest3 clean up", "rename: doctest=>DT_old, doctest3=>doctest" ]
,["20130512-1", "doctest3, mainly based on doctest, works fine." ]
,["20130509-2", "sum(arr)","Trying doctests, good idea, almost done, but extremely slow, maybe more than 100x slower than doctest, has to give up" ]
,["20130509-1", "arrblock()" ]
,["20130507-3", "Remove Arrow()", "Adding thickness for plane(working)" ]
,["20130507-2", "Plane: allows for changing frame settings " ]
,["20130507-1", "randPt() (only one pt)" ]
,["20130506-1", "Block " ]
,["20130505-4", "Plane: done mode 1 ~ 4 " ]
,["20130505-3", "squarePts()" ]
,["20130505-2", "Plane: mode 1 done" ]
,["20130505-1", "Chain: change argument pattern to ops" ]
,["20130504-2", "Plane: allows 4-side plane", "midPt()", "midCornerPt()" ]
,["20130504-1", "Line: failed to make touch work","Rename Surface to Plane" ]
,["20130503-4", "Line: found mechanism to find the p3rd -- the pt on surface. Still under construction." ]
,["20130503-3", "Line: let shift apply not only to block but to line, too", 
				"Remove Line2. In Line, rename blockAnchor to shift" ]
,["20130503-1", "Line style block: allows any shift" ]
,["20130502-4", "echom(modulename).", "Detailed Line_demo_???"
			, "Line:Fix rotate in block style" ]
,["20130502-3", "Del Line, rename Line3 to Line, style=block to be fixed."]
,["20130502-2", "module Line0, to replace Line as the simple line module."]
,["20130502-1", "Fix Line3 (same problems also in Line2) head/tail at wrong side when switched"]
,["20130501-2", "Working on Line3 head/tail cone"]
,["20130501-1", "rotangles_z2p() made with RMz2p_demo(). Leave RMz2p_demo for future references", "Rename rot2z_angles to rotangles_p2z"]
,["20130428-1", "rmx(), rmy(), rmz(), rm2z(), rot2z_angles()"]
,["20130425-3", "Complete PtGrid new scale feature and doc"]
,["20130425-2", "hasAny()", "arrItcp()"]
,["20130425-1", "has()", "Add 'scale' to PtGrid()"]
,["20130424-1", "Remove dot(pq) ==> simply p*q; PtGrid takes new arg 'scale'"]
,["20130423-4", "Surface()","distPtPl()", "dot(pq)"]
,["20130423-3", "Line2: Improved. New shift:corner2."]
,["20130423-2", "Line2: Improve doc."]
,["20130423-1", "Line2: Rename style panel to block."]
,["20130422-6", "Add new style panel to Line2"
		     , "randPts() take new argument r."]
,["20130422-5", "Fix Line2 head/tail color and transp issues."]
,["20130422-4", "Re-organize Line2 examples."]
,["20130422-3", "Bugfix Line2."]
,["20130422-2", "Add fn arg to DashLine, Line"
			,"Fix arrow position problem in Line2" ]
,["20130422-1", "Rename markPts to MarkPts. Improve MarkPts()"]
,["20140421-1", "markPts(), randPts()"]
,["20140420-3", "Line2(): new head/teail style: cylinder"]
,["20140420-2", "Improved Line2()"]
,["20140420-1", "del()", "Raname: arrmax=>maxa, arrmin=>mina", "Line2()"]
,["20140419-3", "minPt()"]
,["20140419-2", "minxPts(), minyPts(), minzPts()"]
,["20140419-1", "mina(),maxa()"]
,["20140418-5", "Improved Box()"]
,["20140418-4", "Add a strick flag to ishash()"]
,["20140418-3", "Improve update: item order of h2 doesn't matter."]
,["20140418-2", "hashi()", "hashkv()"]
,["20140418-1", "Rename hashd to hasht", "New hashd()"]
,["20140417-3","Box()."]
,["20140417-2","DashBox()."]
, ["20140417-1","boxPts() improved."]
,["20140416-3","boxPts()"]
,["20140416-2","getd()", "hashd()"]
,["20140416-1","Remove DottedLine()", "Modify PtGrid to take hash argument", "PtGrid_test()"]
,["20140415-4","Modify DashLine to take hash argument"]
,["20140415-3", "echo_()"]
,["20140415-2","Rename rep() to s_()", "h_()"]
,["20140415-1", "normal_demo()","Allow wrap in index()"]
,["20140413-1", "PtGrid()"]
,["20140412-1","Rename Lines to Chain", "scadex_demo()", "DashLine()","DottedLine()"]
,["20140409-2","normal()", "planecoefs()"]
,["20140409-1","getcol()", "transpose()","Rename semiAnglePt to midAnglePt"
			, "Rename crossLinePts to othoCrossLinePts"
			, "Rename crossPts to begCrossLinePts" ]
,["20140406-5", "othoCrossLinePts()", "semiAnglePt()"]
,["20140406-4","Line_test() and _demo()"
			, "Use convert_scadex.py to create scadex.scad and scadex_test.scad" ]
,["20140406-3", "Fix othoPt", "Line() module"]
,["20140406-2","Restructure the code layout a bit to make them consistent"]
,["20140406-1","disable dist()", "Define doc of each function with name of it"]
,["20140405-1", "Change [_] in rep() to {_}", "add echo_level arg to doctest"
	, "dx(),dy(),dz()","linecoef", "slope()", "othoPt()", "begCrossLinePts()"
	, "Rename arcPoints to arcPts"]
,["20140402-1", "Rename curvePoints() to arcPoints"]
,["20140401-2", "reverse();"]
,["20140401-1", "Hey it's April Fool!; curvePoints();In shift(), change arg i to _i_;"]
,["20140326-2", "Remove test_run() (use doctest instead); Update doc for doctest(); scad_ver" ]
,["20140326-1", "doctest(); Rename items() to vals(); Remove stepdown()" ]
,["20140324-1", "multi_test(); echoblock()" ]
,["20140321-2", "shrink()" ] 
,["20140321-1", "stepdown()" ]
,["20140320-3", "Trying to add doc feature in test_run(), under construction." ]
,["20140320-2", "Allows update to add single [k,v] pair more easily
			get(o,i) now get(o,i,[j]) that can retrieve multiarray data." ]
,["20140320-1", "more examples for update()." ]
,["20140319-6", "update(). Surprisingly easy." ] 
,["20140319-5", "ishash();delkey()" ]
,["20140319-4", "bugfix: test_run(): make sure no * is shown when the test-specific 
			asstr is set to false in cases where function-wise asstr is true;
			Setup norm_test() to demo the use of test_run() (asstr, rem, etc);
			Add indent to output when test fails in test_run()" ]

,["20140319-3", "countType(); Rename count???Items to count???Type, and use countType( except countNum )" ]
,["20140319-2", "Only int allowed in i in get(o,i) now;
			 slice(s,i,j)=undef when i,j order wrong" ]
,["20140319-1", "inrange_test()" ]
,["20140318-1", "keys()" ]
,["20140315-1", "Start using 2013.03.11 with concat feature turned on; 
           bugfix: previous ver doesn't show * for test-specific asstr;
           slice works on array; 
           split() " ]
,["20140313-1", "int(),countArr(),countInt(), countNum(); Add "*" for asstr" ] 
,["20140312-2", "Re-doc for test_run(); minor bug fixes." ]
,["20140312-1", "1.Restructure again for easier test cases writing for user." ]
,["20140311-1", "1.Restructure test arguments to allow for 'asstr' and 'rem' options
		    2. One test_echo() for all (including those with optional args)" ]
,["20140309-3", "rename from tools to scadex" ]
,["20140309-2", "More doc for the functions. " ]
,["20140309-1", "Remove findw (use index instead)" ] 
,["20140308-3", "more thorough doc." ]
,["20140308-2", "doc for test tools" ]
,["20140308-1", "test features done; code structured" ]
,["20140307-4", "test_echo1(), test_echo2()" ]
,["20140307-3", "rep()" ]
,["20140307-2", "make index() take string. deprecricating findw()." ]
,["20140307-1", "fix bugs in findw." ]
];
            
            
//===========================================
//
//           Theories
//
//===========================================

/*


Intersection of line and plan:
http://paulbourke.net/geometry/pointlineplane/

The equation of a plane (points P are on the plane with normal N and point P3 on the plane) can be written as
N dot (P - P3) = 0
The equation of the line (points P on the line passing through points P1 and P2) can be written as
P = P1 + u (P2 - P1)
The intersection of these two occurs when
N dot (P1 + u (P2 - P1)) = N dot P3
Solving for u gives

u = (N dot (P3-P1) ) / (N dot (P2-P1))

P = P1 +   (N dot (P3-P1) ) / (N dot (P2-P1)) * (P2 - P1)


[P1,P2] => pq
[P3, P4, P5] = target
N = normal(rst)

P = pq[0] + dot(normal(rst), target[0]-pq[0]) 
		 / dot(normal(rst), pq[1]    -pq[0])
		 * (pq[1]-pq[0])

         
*/         

/*
	P1 = Q + t * (P-Q)
	P2 = N + s * (M-N)

	when P1=P2

	Q.x + t(P-Q).x = N.x + t(M-N).x


	t = 

	let:	A = a point on both pq and mn
		c = PA / PQ
 		
	=> A = [ c * dx(pq), c * dy(pq), c * dz(pq) ] 
		= c * [ dx(pq), dy(pq), dz(pq) ] 
		= c* (Q-P)
		= PA (Q-P) / PQ

	let d = MA/MN
	=> A = MA (N-M ) / MN

	PA(Q-P)/PQ = MA(N-M)/MN

	 PA							   MA
	---- [ dx(pq), dy(pq), dz(pq) ] = ---- [ dx(mn), dy(mn), dz(mn) ] 
	 PQ							   MN	

	let a = PA/PQ, b = MA/MN

	a [ dx(pq), dy(pq), dz(pq) ] = b [ dx(mn), dy(mn), dz(mn) ] 
	
	a * dx(pq) = b * dx(mn)
	a * dy(pq) = b * dy(mn)
	a * dz(pq) = b * dz(mn)

	a =  b * dx(mn) / dx(pq)
	    (a * dy(pq) / dy(mn)) * dx(mn) / dx(pq)

			(N-M) PQ
	PA = MA ---------
			(Q-P) MN

	//-----------------------------------------------

      P3 
	   &'-_
	   |&  '-._     
	   | &     '-._
	   |  &        '-_ 
	   |   &         _'P2
	   |  b &      -' /
	   |     &  _-'  /	
	   |      X'    /
	   |  a -' &   /
	   |  -'    & /
	   |-'_______&
     P1            P4


http://paulbourke.net/geometry/pointlineplane/

Line a = P:Q
Line b = M:N

This note describes the technique and algorithm for determining the intersection point of two lines (or line segments) in 2 dimensions.

The equations of the lines are

Pa = P + ua ( Q - P )
Pb = M + ub ( N - M )

Solving for the point where Pa = Pb gives the following two equations in two unknowns (ua and ub)

P.x + ua (Q.x - P.x) = M.x + ub (N.x - M.x)

and 

P.y + ua (Q.y - P.y) = M.y + ub (N.y - M.y)

Solving gives the following expressions for ua and ub

k = [ (N.y-M.y)(Q.x-P.x) - (N.x-M.x)(Q.y-P.y) ]

ua = [ (N.x-M.x)(P.y-M.y) - (N.y-M.y)(P.x-M.x) ] / k

ub = [ (Q.x-P.x)(P.y-M.y) - (Q.y-P.y)(P.x-M.x) ] / k




Line a = P1:P2
Line b = P3:P4

This note describes the technique and algorithm for determining the intersection point of two lines (or line segments) in 2 dimensions.

The equations of the lines are

Pa = P1 + ua ( P2 - P1 )
Pb = P3 + ub ( P4 - P3 )

Solving for the point where Pa = Pb gives the following two equations in two unknowns (ua and ub)

x1 + ua (x2 - x1) = x3 + ub (x4 - x3)

and 

y1 + ua (y2 - y1) = y3 + ub (y4 - y3)

Solving gives the following expressions for ua and ub

k = [ (y4-y3)(x2-x1) - (x4-x3)(y2-y1) ]

ua = [ (x4-x3)(y1-y3) - (y4-y3)(x1-x3) ] / k

ub = [ (x2-x1)(y1-y3) - (y2-y1)(x1-x3) ] / k

*/

//);

/*
// OpenSCAD transformation matrix function library. 
// Henry Baker, Santa Barbara, CA, 7/2013. 

function scale(v)=[[v[0],0,0,0], 
                   [0,v[1],0,0], 
                   [0,0,v[2],0], 
                   [0,0,0,1]]; 

function rotatex(a)=[[1,0,0,0], 
                     [0,cos(a),-sin(a),0], 
                     [0,sin(a),cos(a),0], 
                     [0,0,0,1]]; 

function rotatey(a)=[[cos(a),0,sin(a),0], 
                     [0,1,0,0], 
                     [-sin(a),0,cos(a),0], 
                     [0,0,0,1]]; 

function rotatez(a)=[[cos(a),-sin(a),0,0], 
                     [sin(a),cos(a),0,0], 
                     [0,0,1,0], 
                     [0,0,0,1]]; 

// From Wikipedia. 
function rotatea(c,s,l,m,n)=[[l*l*(1-c)+c,m*l*(1-c)-n*s,n*l*(1-c)+m*s,0], 
                             [l*m*(1-c)+n*s,m*m*(1-c)+c,n*m*(1-c)-l*s,0], 
                             [l*n*(1-c)-m*s,m*n*(1-c)+l*s,n*n*(1-c)+c,0], 
                             [0,0,0,1]]; 

function rotateanv(a,nv)=rotatea(cos(a),sin(a),nv[0],nv[1],nv[2]); 

function rotate(a,v)=(v==undef)?rotatez(a[2])*rotatey(a[1])*rotatex(a[0]): 
                     rotateanv(a,v/sqrt(v*v)); 

function translate(v)=[[1,0,0,v[0]], 
                       [0,1,0,v[1]], 
                       [0,0,1,v[2]], 
                       [0,0,0,1]]; 

// From Wikipedia. 
function mirrorabc(a,b,c)=[[1-2*a*a,-2*a*b,-2*a*c,0], 
                           [-2*a*b,1-2*b*b,-2*b*c,0], 
                           [-2*a*c,-2*b*c,1-2*c*c,0], 
                           [0,0,0,1]]; 

function mirrornv(nv)=mirrorabc(nv[0],nv[1],nv[2]); 

function mirror(v)=mirrornv(v/sqrt(v*v)); 

// Example of the use of these functions. 
// The following two commands both do the same thing. 
// Note that the order of transformations in both versions is the same! 

multmatrix(m=translate([20,0,0])*rotate([0,10,0])*translate([10,0,0])) cube(size=[1,2,3],center=false); 

translate([20,0,0]) rotate([0,10,0]) translate([10,0,0]) cube(size=[1,2,3],center=false); 

*/

            
//===========================================
//
//           Version / History
//
//===========================================

            
scadx_core_history=[
["20150204-1", "Migrated from scadex."]
,["20150206-1", "Remove:subops; Add: centroidPt, symediamPt; Add back: prependEach; update: lineCrossPt accepts different arguments "]
,["20150210-1", "dig(), digrep(), repeat(x,n) now return [] or '' if n not valid"]
,["20150211-1", "preparing for fml_parse."
    ,"split(),splits(): new arg, keep, and new behavior: no more empty str \"\" in return"
    , "New: trim(), blockidx(), splitb()"]
,["20150215-1", "Improved blockidx: now 1. takes multiple sets of block beg/end; 2. each beg/end could be more than 1 letter; 3. ret [ [bi,b,ei,e],[...],...]"]
,["20150218-1", "unify(); app(), appi(), Improved begwith/endwith(o,x): return val (instead of T/F) when x is an array"]
,["20150219-1", "Rename unify to getuni(); packuni()"]
,["20150219-2", "begword(); Imp getuni: cut by word, num converted"]
,["20150220-1", "Serious bugfix and imp for begword() and getuni()"]
,["20150220-2", "getuni & packuni: customizable prefix; bugfix: previous can't handle (ab+1)*20-1.5"]
,["20150221-1", "New: run(); calc_basic(); Fix _fmth a bit. "]
,["20150222-1", "getuni() bug found: (x+1)*(y+2) is correct, (x+1)*((y+2)*3) should be (str quotes removed for clearity): [[#/1,*,#/2],[x,+,1],[#/3,*,3],[y,+,2]] but got: [[#/1,*,#/2],[x,+,1,#/3],[],[y,+,2,*,3]] "]
,["20150223-1", "fix getuni() bug"]
,["20150223-2", "calc() bug-free now"]
,["20150223-3", "Imp: packuni() to split '2x' to 2,*,x. Not yet able for 2(x+1)."]
,["20150224-1", " Fix uconv bug; Comment out to be removed: blockidx(), dig();"]
,["20150225-1", "Move all demos to scadx_demo.scad; Rename: prepandEach=> ppEach; Disable mergeA()"]
,["20150226-1", "rand(): new arg seed"]
,["20150302-1", "Fixed some bugs in ranges() "]
,["20150302-2", "split(), splits(): new arg keepempty; Comment subarr for removal. Done code cleaning for scadx_core."]
,["20150305-1", "New:isSameSide(), demo tested. Imp: angle: new arg refPt"
 ]
,["20150306-1", "New: angle_series(), prod(), sel()"
 ]
,["20150306-2", "New: pw(), pqr(), pqr90, coord(); Recode: randPt, randPts"]
,["20150306-3", "randPt/randPts: Rename arg len to d (=distance to ORIGIN)"
 ]
,["20150306-4", "rotM, rotPt,rotPts (untested)"]
,["20150307-1", "Cool demo of rotPts_demo(); Imp: cubePts(pqr): pqr will be given if not; bug found: Mark90 makrs when a< 0"]
,["20150307-2", "Fix bug in is90() that causes Mark90 to goes wrong in some cases."]
,["20150308-1", "rotObj() to rotate obj (not polyh). Tested: rotObj_demo()."]
,["20150308-2", "New: uv(); Imp: angle: now allow([pq1,pq2]); New: normalLn; Imp: modify error msg text; new: Chainc: closed chain; Get transf_dev() in rot_test.scad to work"]
,["20150309-1", "Attempt trfPts_demo_polyh_cube() but seems not perfect"]
,["20150310-1", "coordPts, iscoord(), Coord(); Coord_demo() works great; N2(): change direction. This might have impact on other funcs."]
,["20150311-1", "Imp has(): non-arr and non-str gets false"]
,["20150312-1", "Tidy up getsubops and give more detailed doc. New mod echofh()"]
,["20150317-1", "bugfix: update(h,g) error when g=[]; Imp: rewrite _fmth and add new args (sectionhead/comment); Working on new subops() in arg_pat.scad"]
,["20150317-2", "New subops() and subops_demo() in arg_pat.scad. Seems to ready for transfering to scadx_core_dev.scad. Need a test."]
,["20150328-1", "Imp lineCrossPt(): report err when pts not on plane. New arg in angle(): lineHasToMeet."]
,["20150412-1", "Recode:sum(); Rename:digrep=>deeprep; New: all(), ispt(), isln(),ispl(),typeplp(), lineCrossPts()(needs tests), flatten()"]
,["20150417-?", "flatten() has new arg:d"]
,["20150420-1", "Recode hash usning list comp. Also simplify hash args to h,k,notfound; Recode index using list comp. Also remove wrap from its args."
] 
,["20150425-1", "New:combine()." ]
,["20150507-1", "New arg for anglePt: a2,ratio. To replace anyAnglePt later" ]
,["20150507-2", "calc00() and calc0() for simple calc, hope to get it quicker than calc(). " ]
,["20150508-1", "New: begmatch(). Tested" ]
,["20150508-2", "New: calc_shrink(). Tested" ]
,["20150508-3", "New: calc_base(), handles operator priority and 2a style calc. Tested. Excellent. Rename: calc00=>calc_axb() " ]
,["20150508-4", "Rename: calc_base => calc_flat; Fixed bug: calc_flat fails to handle [[3,'/'], [4,'*']]" ]
,["20150508-5", "Recode calc_flat, calc_shrink to take care of 2b^2, which is done, but fail to treat * and / (or + & -) equally (a/b*3 wrongly treated as a/(b*3)" ]
,["20150508-6", "New calc_flat and calc_shrink, together take care of anything flat." ]
,["20150509-1", "calc_flat(s) bug fixed: unable to calc when s= 5 or '5'; begword new arg: we " ]
,["20150509-2", "working on new func: match() " ]

,["20150510-1", "match() done. Works pretty well. " ]
,["20150510-2", "New idx(), same as index(), better code" ]
,["20150510-3", "match2() done. Another excellent code. " ]
,["20150511-1", "New: match3(), isat()" ]
,["20150511-2", "New: match_at();Improve: isat(): chk multiple x's. String only. " ]
,["20150511-3", "New: match4(),Excellent regex-like matcher" ]
,["20150511-4", "Improve: match4() new arg 'want' to return selected matches" ]
,["20150512-1", "New: matches_at(). Tested. Bugfix: match_at() and some changes" ]
,["20150512-2", "New: match5(). We reached the goal of a matching func. " ]
,["20150512-3", "Remove old match?() and rename match5 to match. Imp: matches_at: want can be an int." ]
,["20150512-4", "New: match_nps_at(); Rename matches_at() to match_ps_at()" ]
,["20150513-1", "New: calc_blk()" ]
,["20150513-1", "New: marchr_at(): reversed match from tail of s" ]
,["20150513-2", "New: calc_func(): much better features and much faster than calc @20150223-2." ]
,["20150513-3", "Recode calc_flat to make use of match(). Much easier to debug." ]

,["20150514-1", "calc_axb,_flat,_func: new ops: %><=&|, and new conditional a?b:c construct. " ]


];

/* Funcs that take a varying len of args:


dist(pq), dist(P,Q)

*/

SCADX_CORE_VERSION = last(scadx_core_history)[0];

//echo_( "<code>||.........  <b>scadx_core version: {_}</b>  .........||</code>",[SCADX_CORE_VERSION]);
//echo_( "<code>||Last update: {_} ||</code>",[join(shrink(last(scadx_core_history))," | ")]); 

// projPt
// pick , select, pp1,pp2,pp3, subarr, between
// rodfaces, rodsideface, 

// Arg design pattern: variable len of arguments: check range()
// debug: chkbugs() 