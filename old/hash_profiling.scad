include </home/vincent/Documents/prog/openscad/doctest/doctest_dev.scad>
/*
 Change the DATA_SIZE, RUN_COUNT, then select hash version. Run and write down the time it takes:
 
 Note: Recursive#2 is hash_recurve taken away the type check:
      !dt_isarr(o) ... 
      Its speed is much faster than that w/ type-check, but
      run into "recursive calling len" error when running the
      test codes at the bottom of this file

================================
DATA   RUN       time (seconds)  
size  Count  Recursive list Recursive#2
================================
100    100     6~7       0        
100    1000    63,65     0   
120    1000    123,128   0      
200    1000    hanging   0      1
500    1000     --       1      3
1000   1000     --       2      6
1000   2000     --       4      13
================================
*/

DATA_SIZE=1000;
RUN_COUNT =2000;

RECURSIVE=0;
LIST = 1;
SEARCH=2;

hash_choice= RECURSIVE;
//hash_choice= LIST;
//hash_choice= SEARCH;


function makekeys( count=DATA_SIZE )=
(
   [ for(i=[0:count-1]) 
         dt_join( [for(x=rands(65,65+52,4)) chr(x) ], "" )
    ]
);
        
// randomly make DATA_SIZE keys
keys = makekeys( DATA_SIZE ); 
         
// make keys into [key,i] pairs         
pairs= [ for(i=[0:len(keys)-1])[ keys[i],i ] ]; 
   

data = [ for( i=[0:len(keys)*2-1] )
         i%2==0? keys[i/2]: (i-1)/2 ];
//echo(keys);
//echo(data);

// keys to use in profiling run (total of RUN_COUNT). They could have
// repeats.
keys_to_use = [ for (i= rands(0, len(keys)-1, RUN_COUNT)) keys[i] ];


function hash_recursive(h,k,notfound=undef,_i=0, debug)=
(
	//!dt_isarr(h)? notfound      // not a dt_hash
	//:
    _i>len(h)-1? notfound  // key not found
		: h[_i]==k?        // key found
			 (_i==len(h)-1?notfound  // value not found
                : h[_i+1] 
			 )
			: hash_recursive( h=h, k=k, notfound=notfound, _i=_i+2) 
);

/*
(
	!dt_isarr(h)? notfound      // not a dt_hash
	:_i>len(h)-1? notfound  // key not found
		: h[_i]==k?        // key found
			 (_i==len(h)-1?notfound  // value not found
                : h[_i+1] 
			 )
			: dt_hash( h=h, k=k, notfound=notfound, _i=_i+2)           
);
*/
function hash_list(h,k,notfound=undef)=
(
  let( rtn= [ for(i=[0:2:len(h)-2])
             if( h[i]==k ) h[i+1] ]
     )
     len(rtn)?rtn[0]:notfound
);
 
 
function hash_search(h,k,notfound=undef)=
(
   let( rtn= search([k],h) )
   rtn==[[]]?notfound:h[rtn[0]+1]
);          

function hash(h,k,notfound=undef)=
(         
    hash_choice== RECURSIVE
    ? hash_recursive(h,k,notfound)
    :hash_choice== LIST
    ? hash_list(h,k,notfound)
    :hash_search(h,k,notfound)
);           
//
module speedtest_recursive()
{
    echo("speedtest_recursive, DATA_SIZE=", DATA_SIZE, ", RUN_COUNT= ", RUN_COUNT);
    rtn = 
     [ for(k=keys_to_use) hash_recursive( data, k )]; 
    echo("speedtest_recursive --- done");
}

module speedtest_list()
{
    echo("speedtest_list, DATA_SIZE=", DATA_SIZE, ", RUN_COUNT= ", RUN_COUNT);
    rtn = 
     [ for(k=keys_to_use) hash_list( data, k )] ;
    echo("speedtest_list --- done");
         
}

if( hash_choice==RECURSIVE )
    speedtest_recursive();
if( hash_choice==LIST )
    speedtest_list();

//=============================
hash = ["hash", "h,k,notfound"];
module hash_test( mode=112 ){ //====================
	h= 	[ -50, 20 
			, "xy", [0,1] 
			, 2.3, 5 
			,[0,1],10
			, true, 1 
			, 1, false
			,-5
			];

	doctest( hash,  
    [ str("> h= ", h) //_fmth(h, eq=", ", iskeyfmt=true))
    , ""
    , [ hash(h, -50), 20, "h, -50" ]
    , [ hash(h, "xy"), [0,1], "h,'xy'"  ]
    , [ hash(h, 2.3), 5, "h,2.3" ]
    , [ hash(h, [0,1]), 10, "h,[0,1]" ]
    , [ hash(h, true), 1, "h,true" ]
    , [ hash(h, 1), false, "h,1" ]
    , [ hash(h, "xx"), undef, "h,'xx'" ]

    , "## key found but it's in the last item without value:"
    , [ hash(h, -5), undef, "h, -5" ]
    , "## notfound is given: "
    , [ hash(h, -5,"df"), "df", "h,-5,'df'", ["//", "value not found"] ]
    , [ hash(h, -50,"df"), 20, "h,-50,'df'" ]
    , [ hash(h, -100,"df"), "df", "h,-100,'df'" ]
    , "## Other cases:"
    , [ hash([], -100), undef, "[],-100" ]
    , [ hash(true, -100), undef, "true,-100" ]
    , [ hash([ [undef,10]], undef), undef, "[[undef,10]],undef" ]
    , " "
    , "## Note below: you can ask for a default upon a non-hash !!"
    , [ hash(true, -100, "df"), "df", "true,-100,'df'"]
    , [ hash(true, -100), undef, "true,-100 "]
    , [ hash(3, -100, "df"), "df", "3,-100,'df'"]
    , [ hash("test", -100, "df"), "df", "'test',-100,'df'"]
    , [ hash(false, 5, "df"), "df", "false,5,'df'"]
    , [ hash(0, 5, "df"), "df", "0,5,'df'" ]
    , [ hash(undef, 5, "df"), "df", "undef,5,'df'" ]
    , "  "
    , "## New 20140528: if_v, then_v, else_v "
    , ""
    , str("> h= ", h) //_fmth(h, eq=", ", iskeyfmt=true))
    , ""
//    , [ hash(h, 2.3, if_v=5, then_v="got_5"), "got_5", "h,2.3,if_v=5, then_v='got_5'" ]
//    , [ hash(h, 2.3, if_v=6, then_v="got_5"), 5, "h,2.3,if_v=6, then_v='got_5'" ]
//    , [ hash(h, 2.3, if_v=6, then_v="got_5", else_v="not_5"), "not_5", "h, 2.3, if_v=6, then_v='got_5', else_v='not_5'"
//             ]
//    ,"  "
//    ,"## New 20140730: You can use string for single-letter hash:"
//    ,[hash("a1b2c3","b"), "2", "'a1b2c3','b'" ]
    //           ," "
    //           ,"## 20150108: test concat "
    //           ,["['a',3,'b',4,'a',5]", hash(["a",3,"b",4,"a",5],"a"), 3] 
    ], mode=mode //,["h",h]
    );
}    
//hash_test();  