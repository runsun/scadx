include <scadx_core_test.scad>
include <arg_pat.scad>
include <../app/openscad_doctest/openscad_doctest.scad>

module scadex_test(mode=13)
{
	//======================================
	//
	// Header (not needed for actual tests)
	//
	//====================================== 

	function hashkvs(h, _i_=0)=
	(
		!dt_isarr(h)? undef        // not a hash
		:_i_>len(h)-1? []       // end of search
			: concat( 
					 [[h[_i_],h[_i_+1]]]
				    , hashkvs(h, _i_=_i_+2)
					)
	);

	function _getModeStrings(mode_kvs, mode, _i_=0)=
	(
		_i_<len(mode_kvs)?
			concat( [mode==mode_kvs[_i_][0]?
						(str(" @ ", mode_kvs[_i_][0]
							,": ", mode_kvs[_i_][1] ))
						:_gray(str("   ", mode_kvs[_i_][0]
							,  ": ", mode_kvs[_i_][1]  ))
					]
				 , _getModeStrings(mode_kvs, mode, _i_=_i_+1) 
                   )
			:[]
	);

	echo(_pre(str(
	_BR,  _color(dt_multi("#",78),"brown")
	,_h3(dt_s("scadex_test( mode={_} ) starts",[ mode ]), "color:brown;margin-top:0px;margin-bottom:0px")
	//,_s("mode={_}: {_}",[ mode, hash(DT_MODES,mode) ])
	,dt_multi("-",78)
	,"<br/>"
	,dt_join( _getModeStrings( hashkvs(DT_MODES), mode=mode), "<br/>")
	,_BR,  _color(dt_multi("-",78),"brown")
	)));

	ops=["mode",mode];

	//======================================
	//
	// tests of multiple functions
	//
	//======================================

//------------------------------------

//	accumulate_test(ops);
//	addx_test(ops);
//	addy_test(ops);
//	addz_test(ops);
//	angle_test(ops);
//    angle_series_test(ops); // THIS IS SLOW ....
//	angleBisectPt_test( ops ); // test with other function
//	anglePt_test(ops);
//	anyAnglePt_test(ops);
//    app_test( ops );
//    appi_test( ops );
//	arcPts_test(ops); // NEED FIX (2014.08.07)
//	arrblock_test(ops);
//	
////	begCrossLinePts_test(ops);
	begwith_test(ops);
//    begword_test( ops );
//	between_test(ops);
//    //// blockidx_test(ops);  2015.2.23: to be removed
//	boxPts_test(ops);
//    calc_test( ops );
//    calc_basic_test( ops );
//  centroidPt_test( ops );
//	cirfrags_test(ops);
//	cornerPt_test(ops);
//	countArr_test(ops);
//	countInt_test(ops);
//	countNum_test(ops);
//	countStr_test(ops);
//	countType_test(ops);
//	cubePts_test(ops);	
//
//	del_test(ops);
//	dels_test(ops);
//	delkey_test(ops);
//	det_test(ops);  // Need re-investigation
//  digrep_test(ops);     
//	dist_test(ops);
//	distPtPl_test(ops);
//	distx_test(ops);
//	disty_test(ops);
//	distz_test(ops);
//    dot_test(ops);
//	echoblock_test(ops);
//	echo__test(ops);
//	echoh_test(ops);
////     echom_test(ops);
//	endwith_test(ops);
//	expandPts_test(ops);
//	fac_test(ops);
//  faces_test(ops);
//	fidx_test(ops); // New2014.8.8
//	_fmth_test(ops); //==> Recursion detected calling function 'index' ==> 2015.2.25 don't see that problem
//	_fmt_test(ops);
    _fmth_test(ops);
//	ftin2in_test(ops);	
//    //_getdeci_test( ops ); // need this
//	get_test(ops); // <=== start using simplified scoping (6/11/2014)
//	getcol_test(ops);
//    getSideFaces_test(ops);
    getsubops_test(ops);
//    getTubeSideFaces_test(ops);
//    getuni_test(ops);
//	_h_test(ops);
//	has_test(ops);
////	hasAny_test(ops); // 15.2.25: disabled to be removed (use has() )
//	hash_test(ops);
//    hashex_test(ops);
//	hashkvs_test(ops);
//	haskey_test(ops);
//	incenterPt_test(ops);
//	index_test(ops);
//	inrange_test(ops);
//	int_test(ops);  // in test cases, only string needs to be quoted
//
//	intersectPt_test(ops); // 14.8.8: WE NEED THIS
//
//	is90_test(ops); // randomly generated
//	isarr_test(ops);
//	isbool_test(ops);
//	isequal_test(ops);  // need test cases
//	isfloat_test(ops);
//	ishash_test(ops);
//	isint_test(ops);
//	isnum_test(ops);
//	is0_test(ops); // need add this 
	is90_test(ops);  
	isOnPlane_test(ops);
    isSameSide_test(ops);
//	isstr_test(ops);
//	istype_test(ops);  
//	join_test(ops);
//	joinarr_test(ops);
//	keys_test(ops);
//  kidx_test(ops); // need this
//	last_test(ops);
//	lcase_test(ops); // need this
//////  linecoef_test(ops); // probably don't need this 
//	lineCrossPt_test(ops); // 2014.8.8: need test cases
//    longside_test(ops);
//	linePts_test(ops);
//	longside_test(ops);
//  lpCrossPt_test( ops ) // need this
//
//	_mdoc_test(ops);
//	midPt_test(ops);
////	midCornerPt_test(ops);// 2014.8.8: rename to cornerPt
////	mina_test(ops); // 2014.8.8: disabled to be removed --- new version covers this feature 
//	minPt_test(ops);
//	minxPts_test(ops);
//	minyPts_test(ops);
//	minzPts_test(ops);
//	mod_test(ops);
//////	multi_test(ops); // 2014.8.8: rename to repeat()
//
//// 	neighbors_test(ops); // 2014.8.9: disabled to be removed --- new range() can take care of this.
//
////  norm_test(ops);   //builtin
//	normal_test(ops);
//	normalPt_test(ops); // <=== needed 
//    normalPts_test(ops); // need this
//	num_test(ops);
//	numstr_test(ops);
//	onlinePt_test(ops); // test with other function
//	onlinePts_test(ops); 
//	or_test(ops);
//	othoPt_test(ops);  
//    packuni_test( ops );
//	parsedoc_test(ops);
//	permute_test(ops);
//	pick_test(ops);
//	planecoefs_test(ops); // test with other function
//	ppEach_test(ops); // Removed on 2015.2.4. too easy and too less use to warran a function // Put back on 2015.2.6: easier to code in complex codes
    pqr90_test(ops);
//    prod_test(ops);
//  projPt_test(ops); // need this
//  quadrant_test( ops ) // need this
//	rand_test(ops);     // random, no test
//	randc_test(ops);
//  randInPlanePt_test(ops); // need this
//  randInPlanePts_test(ops); // need this
//  randOnPlanePt_test(ops); // need this
//  randOnPlanePts_test(ops); // need this
	randPt_test(ops);
//	randPts_test(ops);
//  randRightAnglePts_test(ops); // need this
//	range_test(ops);
//	ranges_test(ops);
////	rearr_test(ops); // moved to "discard list"
//	repeat_test(ops);
	replace_test(ops);	
//	reverse_test(ops);
//	ribbonPts_test(ops); // we need this
//// 	Ring0_test(ops);
//	RM_test(ops);
//	RMx_test(ops);
//	RMy_test(ops);
//	RMz_test(ops);
//  RMs_test(ops); // we need this ?
//	RM2z_test(ops);  // need this
////	RMz2p_test(ops);
//
//	rodfaces_test(ops); // need this
//	rodPts_test(ops);
//	rodsidefaces_test(ops);
//	roll_test(ops);
//	rotangles_p2z_test(ops);    
//	rotangles_z2p_test(ops);  
    rotMx_test(ops);
    rotx_test(ops);
//  run_test( ops );
//	_s_test(ops);
//// 	select_test(ops); // 14.8.9: removed. Many other ways already: rearr() or list comp 
    scaleM_test(ops);
//	shift_test(ops);
//    shortside_test(ops); // We need this
//	shrink_test(ops);
//	shuffle_test(ops);
//    sinpqr_test(ops); 
//	slice_test(ops);
//	slope_test(ops);
//	split_test(ops);
//    splits_test(ops);
//	squarePts_test(ops); // test with other function
    subops_test(ops);
//	sumto_test(ops);
//	sum_test(ops);
//	switch_test( ops );
//  symmedianPt_test( ops );
//	transpose_test(ops);
//   trim_test(ops);
//	twistangle_test(ops);
//	type_test(ops);
//	ucase_test( ops );
//	uconv_test(ops);
//	update_test(ops);
//	updates_test(ops);
//	vals_test(ops);
//	vlinePt_test(ops); 


//======================================
//    CurvedCone_test(ops);
//	Block_test(ops);
//	Chain_test(ops);
//	ColorAxes_test(ops);
//	DashBox_test(ops);
//	DashLine_test(ops);
//	Dim_test(ops); // Need fix: doc too long
//	LabelPt_test(ops); // need add this
//	LabelPts_test(ops); // need add this
//	Line0_test(ops);
//	Line_by_x_align_test(ops);
//	Line_test(ops);
//	LineByHull_test(ops);//	Mark90_test(ops);
//	MarkPts_test(ops);
//	Normal_test(ops);
//	Plane_test(ops);
//	PtGrid_test(ops);
//	PtGrids_test(ops);
//	Ring_test(ops);
//	Rod_test(ops);
//	RodCircle_test(ops);

	//======================================
	//
	// footer (not needed for actual tests)
	//
	//======================================
	echo(_pre(str(
		 _color(dt_multi("-",78),"brown")
		,_h3(dt_s("scadex_test( mode={_} ) ends",[mode])
				, "color:brown;margin-top:0px;margin-bottom:0px")
		,  _color(dt_multi("#",78),"brown")
		)));
}

scadex_test( 13 );

scadex_doctesting_ver=[
["20140726_1", "Copied content from openscad_doctest.scad." ]
];


SCADEX_DOCTEST_VERSION = scadex_doctesting_ver[0][0];

echo( _color(str("<code>||.........  <b>scadex_doctesting version: "
	, SCADEX_DOCTEST_VERSION
	, "</b>  .........||</code>"
	),"brown"));

