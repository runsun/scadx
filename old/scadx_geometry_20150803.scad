
//========================================================
function addx(pt,x=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addx(pt[i], isarr(x)?x[i]:x) ]
	: len(pt)==3
       ? [ pt.x+x,pt.y,pt.z ] 
	   : [ pt.x+x,pt.y]  
);
    
//========================================================
function addy(pt,y=0,_I_=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addy(pt[i], isarr(y)?y[i]:y) ]
	: len(pt)==3
       ? [ pt.x,pt.y+y,pt.z ] 
	   : [ pt.x,pt.y+y] 
);
    
//========================================================
function addz(pt,z=0,_I_=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addz(pt[i], isarr(z)?z[i]:z) ]
	: len(pt)==3
       ? [ pt.x,pt.y,pt.z+z ] 
	   : [ pt.x,pt.y,z] 
);    
    

//========================================================
angle=["angle","pqr,Rf=undef, lineHasToMeet=true","number","Angle",
 " Given a 3-pointer (pqr), return the angle of P-Q-R. 
;;    
;; 2015.3.3: new arg *Rf* : a point given to decide the sign of 
;;  angle. If the Rf is on the same side of normalPt, the angle
;;  is positive. Otherwise, negative. 
;;     
;; 2015.3.8: 
;;    
;;  Allows for angle( [line1,line2]). If lineHasToMeet(default), 2 lines 
;;  must have a lineCrossPt. If lineHasOtMeet=false , then the angle of 
;;  both lines' unit vectors is returned. 
;;
;; ref: angle, angleBisectPt, anglePt, anyAnglePt, incenterPt,  is90, Mark90
"
];


//========================================================
function angle(pqr, Rf=undef, lineHasToMeet=false)= 
  let( P=pqr[0], Q=pqr[1], R=pqr[2] )
  isSamePt(P,Q)||isSamePt(Q,R)? undef
  : isSamePt(P,R)? 0
  : let(    
        arelines = len(pqr)==2
        //,itcp = arelines? lineCrossPt( [pqr[0], pqr[1]] ): undef
        ,itcp = arelines? intsec( pqr[0], pqr[1] ): undef
        ,
         pqr = arelines? // two lines
               ( lineHasToMeet? 
                 [ itcp== pqr[0][0]? pqr[0][1]:pqr[0][0]
                  , itcp
                  , itcp==pqr[1][1]? pqr[1][0]:pqr[1][1] 
                 ]
                : [ uv(pqr[0]), ORIGIN, uv(pqr[1])] // conv to unit vectors.
               ):pqr
        , a= acos( (( R-Q)*( P- Q) ) / d01(pqr) / d12(pqr) )
    //    , J = projPt(P, [Q,R])
    //    , a = atan2( dist(R,J), dist(Q,J))
        , np = normalPt( pqr )
        , sign= Rf==undef?1: isSameSide( [np, Rf], pqr)?1:-1
        
        , dpq=dist(P,Q), dpr=dist(P,R), dqr=dist(Q,R)
        )
    (
        iscoln(pqr)? ( dpq>=dpr? 0
                     : dqr>dpr? 0: 180
                     )
        :sign*a
       
    );
   
//
////========================================================
//function angle(pqr, Rf=undef, lineHasToMeet=false)=   
// let(    
//    arelines = len(pqr)==2
//    ,itcp = arelines? lineCrossPt( [pqr[0], pqr[1]] ): undef
//    //,itcp = arelines? intsec( pqr[0], pqr[1] ): undef
//    ,
//     pqr = arelines? // two lines
//           ( lineHasToMeet? 
//             [ itcp== pqr[0][0]? pqr[0][1]:pqr[0][0]
//              , itcp
//              , itcp==pqr[1][1]? pqr[1][0]:pqr[1][1] 
//             ]
//            : [ uv(pqr[0]), ORIGIN, uv(pqr[1])] // conv to unit vectors.
//           ):pqr
//    , P=pqr[0], Q=pqr[1], R=pqr[2] 
//    , a= acos( (( R-Q)*( P- Q) ) / d01(pqr) / d12(pqr) )
////    , J = projPt(P, [Q,R])
////    , a = atan2( dist(R,J), dist(Q,J))
//    , np = normalPt( pqr )
//    , sign= Rf==undef?1: isSameSide( [np, Rf], pqr)?1:-1
//    
//    , dpq=dist(P,Q), dpr=dist(P,R), dqr=dist(Q,R)
//    )
//(
//	iscoln(pqr)? ( dpq>=dpr? 0
//                 : dqr>dpr? 0: 180
//                 )
//    :sign*a
//   
//);

//========================================================
function angle_bylen(a,b,c)=
(
    let( abc = b==undef || c==undef?
               a: [a,b,c]
       , a = abc[0]
       , b = abc[1]
       , c = abc[2]
       , L = a+b+c
       , Lx = L*(L-2*a)*(L-2*b)*(L-2*c)
       , sinB= sqrt( Lx ) / (2*a*c)
       )
    asin(sinB)
);
//echo( a= angle_bylen( 1,sqrt(2),1));
//echo( a= angle_bylen( 2,sqrt(3),1));

//========================================================
function angleAt(pts,i)=
(
   len(i)==3? angle( sel( pts, i ) )
   : angle( sel( pts, [ i-1,i,i+1] ) )
   
//   i==0 || i>len(pts)-2 ? undef
//   : len(i)==3? angle( sel( pts, i ) )
//   : angle( sel( pts, [i-1,i,i+1] ) )
);
//pqr=pqr();
//echo(pqr);
//echo(angleAt(pqr(),1));

// Use atan instead of acos --- need to test if this is better 2015.3.5
function angle_atan(pqr, Rf=undef)=
 let( P=pqr[0], Q=pqr[1], R=pqr[2]
    , D = othoPt(sel(pqr,[0,2,1]))
    , PD = norm(P-D)
    , RD = norm(R-D)
    , QD = norm(Q-D)
    , _a= atan( RD/QD ) //acos( (( R-Q)*( P- Q) ) / d01(pqr) / d12(pqr) )
    , a = PD>QD && PD> norm(P-Q)? (180-_a):_a
    , np = normalPt( pqr )
    , sign= Rf==undef?1: isSameSide( [np, Rf], pqr)?1:-1
    )
(
	sign* a
);

//========================================================

function angleBisectPt( pqr, len=undef, ratio=undef,Rf=undef )= 
(
    iscoln(pqr)?
      anglePt( [pqr[0],pqr[1], Rf?Rf:randPt()], a=90)
    :let( Q= pqr[1]
        ,D= onlinePt( p02(pqr) //: ln_PR
            , ratio= norm(pqr[0]-pqr[1]) //: d_PR
                    /(norm(pqr[0]-pqr[1]) //: d_PR + d_RQ
                     +norm(pqr[1]-pqr[2])) 
                   )
     )
    len!=undef? onlinePt( [Q,D], len=len )
        :ratio!=undef? onlinePt( [Q,D], ratio=ratio)
        :D
);

//========================================================
function anglePt( pqr, a=undef, a2=0, len=undef, ratio=1
                , Rf=undef  // Rf could be used when pqr is coln
                )=
(    
/*     2015.5.7: new arg: an (angle to the N (normalPt))
       
       B-----------_R-----A 
       |         _:  :    |
       |  T._---:-----:---+ S 
       |  |  '-:_      :  | 
       |  |   :  '-._   : |
       |  |  :     a '-._:| 
       p--+--+-----------'Q
          U  D    


       B----_R-----A--._ 
       |      :    |    ''-_
       |       : S +--------T 
       |        :  |      -'|
       |         : |   _-'  |
       |          :|_-'     |
       p----------'Q--------U

            a = a_PQT
     _R    B-----------A--._ 
       `-_ |      :    |    ''-_
          `|_      : S +--------T 
           | `-_    :  |      -'|
           |    `-_  : |   _-'  |
           |       `-_:|_-'     |
           p----------'Q--------U
*/
  let( a0 = angle(pqr)
     , a= a==undef? a0:a
     , Q= Q(pqr)
     , QP= p10(pqr)
     , dPQ= dist( QP )
     // Introducing R to deal with colinear pqr
     , R = is0(a0-180)
            ? (ispt(Rf)?Rf: randofflnPt( p01(pqr)))
            : pqr[2]
     ,_pqr= [pqr[0],pqr[1],R]
     , L= ratio* (len?len:dPQ )
     , QA= [ Q, squarePts( _pqr )[2] ]
    
     , pt= abs(a)==90? // if a=90, return S
            onlinePt( QA, len= sign(a)*L )
          :abs(a)==270? // if a=270
            onlinePt( QA, len= -sign(a)*L )
          :squarePts(
            [ onlinePt( QP, len= L*cos(a) ) //: U
            , Q
            , onlinePt( QA, len= L*sin(a) ) //: S	
            ])[3]
            
//     , pt= abs(a)==90? // if a=90, return S
//            onlinePt( QA, len= sign(a)*L )
//          :abs(a)==270? // if a=270
//            onlinePt( QA, len= -sign(a)*L )
//          :squarePts(
//            [ onlinePt( QP, len= L*cos(a) ) //: U
//            , Q
//            , onlinePt( QA, len= L*sin(a) ) //: S	
//            ])[3]
     )
     
  a2? anglePt( [pt
               ,Q
               , N(_pqr)
               ],a=a2,len=L):pt
);

////========================================================
//function anglePt( pqr, a=undef, a2=0, len=undef, ratio=1)=
//(    
///*     2015.5.7: new arg: an (angle to the N (normalPt))
//       
//       B-----------_R-----A 
//       |         _:  :    |
//       |  T._---:-----:---+ S 
//       |  |  '-:_      :  | 
//       |  |   :  '-._   : |
//       |  |  :     a '-._:| 
//       p--+--+-----------'Q
//          U  D    
//
//
//       B----_R-----A--._ 
//       |      :    |    ''-_
//       |       : S +--------T 
//       |        :  |      -'|
//       |         : |   _-'  |
//       |          :|_-'     |
//       p----------'Q--------U
//
//            a = a_PQT
//
//
//     _R    B-----------A--._ 
//       `-_ |      :    |    ''-_
//          `|_      : S +--------T 
//           | `-_    :  |      -'|
//           |    `-_  : |   _-'  |
//           |       `-_:|_-'     |
//           p----------'Q--------U
//
//	// (1)
//	//      R1_             PQ > TQ
//	//      |  '-._
//	//	P---T------'Q
//	//
//	// (2)           R1     PQ > TQ     
//	//              /|
//	//     P-------Q T
//	//   
//	// (3)   R1_            PQ < TQ
//	//       |  '-._
//	//       |       '-._
//	//       T  P-------'Q
//
//   if (3), then a2 will turn to wrong
//   direction. To solve this, we make a P: 
//   
//	// (3)   R1_            PQ < TQ
//	//       |  '-._
//	//       |       '-._
//	//  Px---T--P-------'Q
//    
//     
//*/
//  let( a= a==undef? angle(pqr):a
//     , L= ratio* (len?len:dist( p01(pqr)) )
//     , Q= Q(pqr)
//     , QA= [ Q, squarePts( pqr )[2] ]
//     , QP= p10(pqr)
//     //, Px = a2? onlinePt([P,Q], len= 2* 
//     
//     , pt= abs(a)==90? // if a=90, return S
//            onlinePt( QA, len= sign(a)*L )
//          :abs(a)==270? // if a=270
//            onlinePt( QA, len= -sign(a)*L )
//          :squarePts(
//            [ onlinePt( QP, len= L*cos(a) ) //: U
//            , Q
//            , onlinePt( QA, len= L*sin(a) ) //: S	
//            ])[3]
//     )
//  a2? anglePt( [pt, Q, N(pqr)],a=a2,len=L):pt
//);


    
//==============================================
//function anglePts( pqr, a, len=1)=




//==============================================

//function anyAnglePt( pqr, a, len=1, pl=undef)=  // pqn, pqr, qnn, qrn
//(
//	anglePt
//	( 
//	   pl=="yz"||pl=="mn"?          [ N2(pqr,len=-1), Q(pqr), N(pqr) ]
//	 : pl=="xy"||pl=="pm"||pl=="pr"? pqr //[ P(pqr), Q(pqr), N2(pqr,reverse=true) ]
//	 : pl=="xz"||pl=="pn"?          [ P(pqr), Q(pqr), N(pqr) ]
//	 :                              [ R(pqr), Q(pqr), N(pqr) ]
//	, a=a, len=len
//	)
//);

//==============================================
function arcPts(  // If set a2, arc will span on plRQN
                  //   and a will be autoset to aPQR
    pqr=randPts(3)
    , rad= undef
    , ratio= 1
    , a = undef
    , a2 = undef
    , n=6
    )=
(    
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, a= a==undef?angle(pqr):a
    , rad= or(rad, d01(pqr))*ratio
	)
    a2==undef? // a apply to aPQR 
    [ for(i=[0:n-1]) anglePt( pqr, a=i*a/(n-1), a2=0, len=rad ) ]
    : [ for(i=[0:n-1]) anglePt( pqr, a=a, a2=i*a2/(n-1), len=rad ) ]   
);
    

function chainPts0( 

           pts
         , r 
         , nside
         , closed 
         , opt=[] 
         , _xsecs=[]
         , _Rf0=undef
         , _i=0
         , _debug=[]
         )=
(
   _i==0?
   
   let(//###################################### i = 0 
   
      //------------------------------
      //    param initiation

        opt = popt(opt) // handle sopt
        
      , r   = und(r, hash(opt,"r", 0.008))      
      , nside= or( nside, hash(opt,"nside",4))
      , closed= und(closed, hash(opt,"closed",false))      
       
      , preP = onlinePt( p01(pts), -1)
      , Rf0  = len(pts)==2? randofflnPt( p01(pts) ) 
               : !iscoln( p012(pts))? pts[2]
               : randofflnPt( p01(pts) )  
                         
      , pl90 = arcPts( [preP, pts[0], Rf0]
                     , a=90
                     , a2= 360*(nside-1)/nside
                     , n=nside, rad= r
                     )                   
            
      , _xsecs= [ pl90 ]
      , _debug= str( _red("<br/><br/><b>chainPts0 debug:</b>")
        , "<br/><b>_i</b> = ", _i
        , "<br/><b>Rf0</b> = ", Rf0
        , "<br/><b>pl90</b> = ", pl90
        , "<br/><b>_xsecs</b> = ", _xsecs
        )     
      )// let @ i=0
      
      chainPts0( pts, r=r
              , nside = nside
              , closed= closed
              , _xsecs= _xsecs
              ,_Rf0=Rf0, _i=_i+1
              , _debug=_debug
              )
       
   //####################################### 0 < i < last 
   
   : 0<_i && _i< len(pts)-1 ?  
   
     let( _Rf= last(_xsecs)[0]
        , Rf = iscoln( get( pts, [_i-1,_i]), _Rf)?
                randofflnPt(get( pts, [_i-1,_i])): _Rf
        , pl90_i = arcPts( [pts[_i-1], pts[_i], Rf]
                 , a=90
                 , a2= 360*(nside-1)/nside
                 , n=nside, rad= r
                 )  
         , patrtn= planeDataAt( pts,_i ) 
         , pl_i = hash( patrtn, "pl")
         , xsec_i = projPts( pl90_i //last(_xsecs)
                           , pl_i
                           , along=get(pts,[_i-1,_i] )
                           )
         , _xsecs = app(_xsecs, xsec_i) 
      , _debug= str( _debug
        , "<br/><br/><b>_i</b> = ", _i
        , "<br/><b>_Rf</b> = ", _Rf
        , "<br/><b>Rf</b> = ", Rf
        , "<br/><b>pl_i</b> = ", pl_i
        , "<br/><b>pl90_i</b> = ", pl90_i
        , "<br/><b>pts</b> = ", pts
        , "<br/><b>_xsecs</b> = ", _xsecs
        )           
         )// let @ i>0

       chainPts0( pts, r=r
               , nside =nside
               , closed=closed
               , _xsecs=_xsecs
               ,_Rf0=_Rf0,_i=_i+1
              , _debug=_debug
               )

   //#################################### i = last
   
   : //_i==len(pts)-1?

     let( pl_i= len(pts)==2?undef
                  : hash(
                         planeDataAt( pts,_i,
                         , Rf= last(_xsecs)[0]
                         , closed= closed
                         ), "pl")
                  
//        //, pl_i = patrtn //hash( patrtn, "pl")
//        , xsec_i =  len(pts)==2?
//                    [ for(pt= last(_xsecs)) pt+ pts[1]-pts[0] ]
//                    : projPts( last(_xsecs)
//                           , pl_i
//                           , along= get( pts,[_i-1,_i] )
//                           )
//                  
//                  
//        , _xsecs = app(_xsecs, xsec_i)
//      , _debug= str( _debug
//        , "<br/><br/><b>_i</b> = ", _i
//       , "<br/><b>pl_i</b> = ", pl_i
//        , "<br/><b>_xsecs</b> = ", _xsecs
//        , _red("<br/><b>chainPts0 debug END</b><br/>")
                   
//        )   
        )
      //_debug
        [ "xsecs", _xsecs
        , "bone", pts
        , "closed", closed
        , "Rf0", _Rf0
        , "debug", _debug 
        
        ]
); 
    


//========================================================
function borderPts(pts, border="min", xyz="x")=
(
    /* Return an array containing pts that have the
       min or max value (determined by *border*) of
       a group of pts (*pts*) along the x,y,z direction
       
       borderPts( pts, "min","x" ) 
         = those in pts having the min x 
    */
    let( xyz= xyz=="x"?0:xyz=="y"?1:2
       , d = [ for(p=pts) p[ xyz ] ]
       , b = border=="min"? min(d):max(d)
       , pts= [ for(p=pts) if(p[xyz]==b) p]
       )
    pts   
);
       
       
circlePts=[];
function circlePts( pqr, rad, n)=
(
   arcPts( pqr?pqr:pqr(), rad=rad, a=90, a2= 360/n*(n-1), n=n)
);
       
//========================================================
centerPt= ["centerPt", "pqr", "pt", "Geometry",
" Given pqr, return a point that is same distance from pts in pqr"
];
function centerPt(pqr)=
(
    iscoln( pqr )? undef
    : let( m1 = midPt( p01(pqr) )
         , m2 = midPt( p12(pqr) )
         , c1= anglePt( [ pqr[0], m1, pqr[2] ], a=90 )
         , c2= anglePt( [ pqr[2], m2, pqr[0] ], a=90 )
         )
    intsec( [m1,c1], [m2,c2] )
);
       
//========================================================
pairPts= ["pairPts","pts1,pts2,c1,c2,rot","pts1,pts2","Geometry",
 "Given two co-center pts, pts1 and pts2 (having center C),  
;; on the the same plane, scanning through pts by angle, add 
;; pts to pts1 if pts2[a] is found, or to pts2 if pts1[a] is found:       
;;        
;;      N-_
;;         `-_
;;    ---B    M
;;        `.   :
;;    C-----A---L---P0   
;;        pts1  pts2
;;       
;;       N B2
;;         `-_
;;    ---B    M
;;        `M1  :
;;    C-----A---L'---P0   
;;        pts1  pts2
;;
;; The rot is the offset given to all pts of pts2
;;
;; Return new pts1 and pts2, such that when scanning angle 
;; from 0 to 360, pts in both appear in pairs.
;;
;; This is useful for some situations:
;; 1) Lofting
;; 2) Connecting two chains
;; 3) Punching a hole
"];
       
function getPairingData_sorted_afips(pts1, pts2,C1,C2)=
(   
    /* 2015.7.23
    
    This is to be called inside getPairingData()

    afips: [angle, face, i, pt]
        where face = 1 or 2 for pts1, pts2, resp.

    Usage:

    pqr =  [[2.02391, 0.977828, 2.76615], [2.51219, -1.8059, -1.15628], [1.7283, -2.1596, 0.479486]];
    pl1= arcPts( pqr, rad=3, a=90, a2= 360/4*3, n=4);
    pl2= arcPts( pqr, rad=1.5, a=90, a2= 360/5*4, n=5);
    afips = pairPts_sorted_afips( pl1,pl2,C1=pqr[1] );
    for(x=afips[0]) echo(x); 

    ECHO: [0, 1, 0, [1.09593, -4.04264, 0.254821]]
    ECHO: [0, 2, 0, [1.80406, -2.92427, -0.450729]]
    ECHO: [72, 2, 1, [3.54269, -2.63013, -0.443046]]
    ECHO: [90, 1, 1, [5.13943, -2.81244, -0.114897]]
    ECHO: [144, 2, 2, [3.85721, -1.19693, -1.42103]]
    ECHO: [180, 1, 2, [3.92845, 0.43084, -2.56738]]
    ECHO: [216, 2, 3, [2.31295, -0.605306, -2.03314]]
    ECHO: [270, 1, 3, [-0.115054, -0.799365, -2.19766]]
    ECHO: [288, 2, 4, [1.04404, -1.67286, -1.43346]]
    */
     let( C1=C1?C1:centerPt(pts1)
        , C2= C2?C2:centerPt(pts2)
        , _a = 0, _f=1, _i=2, _p = 3

        , tmpfactor= 1 //10e2
        , rot=0
        
        , Rf1 = N( [pts1[0], C1, pts1[1]] )
        , Rf2 = N( [pts2[0], C2, pts2[1]] )
        //
        // afip: [ang,face,i,p] where face = 1 or 2
        // NOTE: When getting angle, we have to typecast it to str,
        //       then back to num, 'cos sometimes it's tricky that
        //       that two angles that both should be 120, and they
        //       do display as 120, but internally one could be
        //       120.0000001. Note: OpenSCAD displays number this way:
        //
        //         Internally    displayed 
        //         120.00004  :  120.00004
        //         120.000004 :  120
        //         120.00005  :  120.00005
        //         120.000005 :  120.00001
        //       deci:  123456 
        //        
        //       For 120.0000001, if we str(a) it, it will be "120", it 
        //       will then have actual value as 120 when num(str(a))
        
        , afips1= [ for(i=range(pts1)) let(p=pts1[i])
                    let( _aa= angle( [pts1[0], C1, p], Rf=Rf1 ) 
                       , a= round(100*(is0(_aa)?0:_aa))/100
                       )
                    [ num(str((i==0?0:((a<0?360:0)+a))*tmpfactor)), 1, i,p ]
                  //  [ num(str((i==0?0:((a<0?360:0)+a))*tmpfactor)), 1, i,p ]
                  ]
       , afips2= [ for(i=range(pts2)) let(p=pts2[i])
                   let( _aa= angle( [pts2[0], C2, p], Rf=Rf2 ) 
                      , a= round(100*( is0(_aa)?0:_aa))/100
                      )
                  [ (i==0?0:((a<0?360:0)+a))+rot, 2, i,p ]
//                  [ num(str(((i==0?0:((a<0?360:0)+a))+rot)*tmpfactor)), 2, i,p ]
                 ]         
       //
       // sort first by angle (at i=0) then by face( at i=1):
       // 
       , afips= sort( concat( afips1, afips2 ),0,1)
//       , afips = [ for(x=_afips) app(x, type(x[0]))]
//       , _afips= sort( concat( afips1, afips2 ),0)
//       , angs= uniq([ for(x=_afips) x[_a] ])
//       , anghash = [ for(a=angs) [a,[]] ]
//       , _afips2= [ for(ah = anghash) ah
////                      for( afip= _afips)
////                         if( afip[_a]==ah[0]) 
////                            update( anghash, [ah[0], app( anghash[1], afip)])
//                  ]
//       , afips = angs //_afips2 //last( _afips2)                  
        )                     
    //[afips, afips1, afips2, C1, C2] 
    ["afips", afips
    ,"afips1", afips1
    ,"afips2", afips2
    ,"angles", uniq( [for(afip=afips) afip[0]] )
    ,"pts1",pts1
    ,"pts2",pts2
    ,"C1", C1
    ,"C2", C2
    ]   
);
       
function getPairingData_ang_seg( pts, C, _N, _P0, _rtn=[], _i=0,_debug=[] )=
(
    // 2015.7.24 for use in getPairingData   
    //           Return [ [ [aP,aQ],[P,Q]], [ [aP2,aQ2],[P2,Q2]] ... ]
    //                  for each seg
    // c: center (optional)   
    let ( C= C==undef?centerPt(pts):C
        , P = pts[_i]
        , Q = get(pts, _i+1, cycle=true)
        , _N= _N==undef?N([pts[0],C, pts[1]]):_N
        , _P0= _P0!=undef?_P0: pts[0]
                  //rot? rotPt( pts[0], [_N,C], rot)
                  //: pts[0]
        ,_aP = num(str(angle( [ _P0,C,P], Rf= _N ) ))
        ,_aQ = num(str(angle( [ _P0,C,Q], Rf= _N ) ))
        , aP = _i==0? _aP
              : (_aP< last(_rtn)[0][0])? (_aP+360):_aP
        , aQ = _i==0? _aQ
              : (_aQ< last(_rtn)[0][1])? (_aQ+360):_aQ
        ,rtn = app( _rtn, [ [ round(100*aP)/100, round(100*aQ)/100 ],[P,Q] ] ) 
//        ,_debug= str( _debug
//                 , "<br/><b style='color:red'>"
//                 ,   "getPairingData_ang_seg()</b>, _i= </br>", _i
//                 , "<br/><b>pts</b>= ", pts
//                 , "<br/><b>P</b>= ", P
//                 , "<br/><b>Q</b>= ", Q
//                 , "<br/><b>C</b>= ", C
//                 , "<br/><b>_N</b>= ", _N
//                 , "<br/><b>_P0</b>= ", _P0
//                 , "<br/><b>a</b>= ", a
//                 , "<br/><b>rtn</b>= ", _colorPts(rtn)
//                 )
        )
    //_i==len(pts)-1? _debug  
    _i==len(pts)-1? rtn
    : getPairingData_ang_seg( pts=pts, C=C, _N=_N
                        , _P0=_P0, _rtn=rtn, _i=_i+1, _debug=_debug )    
);       
       
       
//a= ["a","b","c"];
//echo( get= get(a, 3, cycle=true));
       
function getPairingData(pts1, pts2,C1,C2, rot=0)=
(
    /* FOR: punch hole, grow something, connect 
    */
    
     let(C1=C1?C1:centerPt(pts1)
       , C2= C2?C2:centerPt(pts2)
       //, 
       ,_a = 0, _f=1, _i=2, _p = 3

       // afip: [ang,face,i,p] where face = 1 or 2       
       , afipsdata= getPairingData_sorted_afips(
                   pts1, pts2,C1,C2)
       , afips = hash( afipsdata, "afips" )
       , afips1 = hash( afipsdata, "afips1" ) // [ [a,face,i,p], ... ]
       , afips2 = hash( afipsdata, "afips2" )
       //, afips = sort( concat( afips1, afips2),0,1)
       , asegs1 = getPairingData_ang_seg(pts1) //[ [a0,[P0,P1]], [a1,[P1,P2]]..
       , asegs2 = getPairingData_ang_seg(pts2)
              
       , lasti = len( afips )-1
            
       , pts1new= [ 
       
           for(i=[0:lasti] ) 
            /* 
                Looping through these:
           
                [[0, 1, 0, [2.4716, -5.72261, -1.19268]]
                ,[0, 2, 0, [0.804891, -3.30039, 0.705988]]
                ,[60, 2, 1, [-0.626286, -2.96622, 0.405886]]
                ,[120, 1, 1, [-4.68, -1.1484, 0.519362]]
                ,[120, 2, 2, [-1.34059, -1.92813, 1.2196]]
                ,[180, 2, 3, [-0.623719, -1.22421, 2.33342]]
                ,[240, 1, 2, [2.48016, 0.0840993, 5.23243]]
                ,[240, 2, 4, [0.807458, -1.55838, 2.63352]]
                ,[300, 2, 5, [1.52176, -2.59647, 1.81981]]
                ]
           */
            // Skip items that have angle already appeared :
            if(  str(afips[i][_a])!= str(afips[i-1][_a])
              )
                
            let( 
                 afip = afips[i]    // [ [a,face,i,p], ... ]
                , a  = afip[_a] 
                , p  = afip[_p]
                , pt = afip[_f]==1? // afip[_f]=1: this afip
                       p            // is from afips1 so 
                                    // we give its p back
                  : // afip[_f]=2, like [97, 2, 2, [3.94,-2.06,-0.79]]:
                    // The point, afip[_p], is from pts2, means we need 
                    // to create a new pt on afips1 using this afip[_p]
                    [for(xi=range(asegs1))
                        let(aseg = asegs1[xi] // [[90,180],[p,q]]
                           , as= aseg[0]     // [90,180]
                           //, pq= aseg[1]
                           )
                         if(as[0]< a && a < as[1]) 
                             let( ang = a-as[0] 
                                , pg = aseg[1]
                                , p3 = [ pts1[xi], C1
                                       , get(pts1,xi+1, cycle=true)
                                         //   , pts1[fidx(pts1,xi+1)]
                                        ]
                                , p_at_ang = anglePt(p3, a=ang) 
                                )
                         //[p3, p_at_ang ]
                         projPt( C1, pg, [C1,p_at_ang] )
                     ][0]  
                )
//                    let( pts1seg=
//                         [for(xi=range(asegs1))
//                            let(aseg = asegs1[xi] // [[90,180],[p,q]]
//                               , as= aseg[0]     // [90,180]
//                               , pq= aseg[1]
//                               )
//                         if(as[0]< a && a < as[1]) 
//                             //[as[0], a, as[1], pq]
//                             pq
//                        ][0] 
//                       , ang=  angle( [ pts2[0],C2, p]
//                                      , Rf = N( [pts2[0],C2,pts2[1]]) )
//                       , p_on_this_angle= 
//                            anglePt( [ pts1[0], C1, pts1[1]], a=ang) 
//                       )
//
//                    projPt( C1, pts1seg, [C1,p_on_this_angle] ) 
//               )

               pt                

           ]                 

       , pts2new= [ 
       
           for(i=[0:lasti] ) 
            // Skip items that have same angle as their next one:
            if(  str(afips[i][_a])!= str(afips[i+1][_a]) )
              let( 
                 afip = afips[i]    // [ [a,face,i,p], ... ]
                , a  = afip[_a] 
                , p  = afip[_p]
                , pt = afip[_f]==2? // afip[_f]=1: this afip
                       p            // is from afips1 so 
                                    // we give its p back
                  : // afip[_f]=1, like [97, 1, 2, [3.94,-2.06,-0.79]]:
                    // The point, afip[_p], is from pts1, means we need 
                    // to create a new pt on afips2 using this afip[_p]
                    [for(xi=range(asegs2))
                        let(aseg = asegs2[xi] // [[90,180],[p,q]]
                           , as= aseg[0]     // [90,180]
                           //, pq= aseg[1]
                           )
                         if(as[0]< a && a < as[1]) 
                             let( ang = a-as[0] 
                                , pg = aseg[1]
                                , p3 = [ pts2[xi], C2
                                            , get(pts2,xi+1,cycle=true)]
                                , p_at_ang = anglePt( p3, a=ang) 
                                )
                         projPt( C2, pg, [C2,p_at_ang] )
                    ][0]  
               )         
                             
           pt                

           ]
       , n= len(pts1new)
       , 2n = n*2
           
        )// let                     
      [ "pts", concat( pts1new, pts2new )
      , "pts1", pts1new
      , "pts2", pts2new
      , "pts1origin", pts1
      , "pts2origin", pts2    
      , "rdiffs", [ for(i=range(pts1new))
                      let(r1= dist( pts1new[i],C1)
                         ,r2= dist( pts2new[i],C2)
                         , d= r2-r1 )
                      [ is0(d)?0:d, r1, r2]
                  ]     
      , "afips", afips
      , "afips1", afips1
      , "afips2", afips2
      , "angles", hash(afipsdata,"angles")
      , "C1", C1
      , "C2", C2
      , "asegs1", asegs1
      , "asegs2", asegs2
      , "faces", [ for(i= [0:n-1])
                      [ i
                      , n+i
                      , i==n-1?n:i+1+n
                      , i==n-1?0:i+1 
                      ]
                 ]     
      ] 
);


       

//        //-----------------------------------------------              
//        // 2015.7.23: loft                      
//        , isloft = isnum(nsidelast) || ispts(cseclast)         
//        , csecs= !isloft? newcsecs
//          
//            : // processing loft
//                      
//            let(
//                  loft_ht_ang_pt_pairs =  // arr of [angle, "h"|"t", pt ]
//                      let( hpt0 = newcsecs[0][0]
//                         , tpt0 = last(newcsecs)[0]
//                         , rot  = isnum(rot)?rot:0
//                         )     
//                      concat(
//                          [ for(p=newcsecs[0]) 
//                              [ angle( [hpt0, pts[0], p] ), "h", p ]
//                          ]
//                        , [ for(p=last(newcsecs)) 
//                              [ angle( [hpt0, last(pts), p] ) + rot, "t", p ]
//                          ]
//                      )
//             , loft_ht_ang_pt_pairs_sorted = quicksort(
//                     , loft_ht_ang_pt_pairs)
//                     /*
//                     loft_ht_ang_pt_pairs_sorted could be:
//                        
//                     1) [ [a0,"h",hp0],[b0,"t",tp0], ...]
//                       
//                       In this case, b0-a0 = twist
//                       a0 is, in fact, always 0 
//                        
//                     2) [ [a0,"h",hp0],[a1,"h",hp1], [b0,"t",tp0], ...]
//                        
//                     3) etc   
//                        
//                     */   
//             , loft_h_t_pairs = // [ [h0,t0], [h1,t1] ... ]
//                  let( afps = loft_ht_ang_pt_pairs_sorted
//                              // afp: [ang,face,pt] where face= "h" or "t"
//                     , ang = 0, face=1, pt=2
//                     )   
//                  [ for(i = range( afps ) )
//                      let( afp = afps[i]
//                         , L = len(afps)-1
//                         , prei= i==0? L-1: i-1
//                         , nexti= i==L?0:i+1
//                         , preafp= afps[ prei ]  
//                         , nextafp= afps[ nexti ]  
//                         , nexta= afp2[ang]
//                         , ish = afp[face]=="h"
//                         , p= afp[pt] 
//                         )
//                      i==0? [ p, afp[ang]==nexta? afp2[pt]
//                                   : intsec( [ pts[0], p], [
                    

       
//========================================================
chainPts=["chainPts","pts,nr","pts", "Geom",
"Return arr of slices in which a slice is an arr of pts.
;;
;; nr: sides of each crosec.
;;
"
];    
    
//######################################### 
/*
  chainPts(pts, r=1, nside=6 )
  chainPts(pts, opt="r=1;nside=6" )
  chainPts(pts, opt=["r",1,"nside",6] )
  chainPts(pts, opt=["r=1","nside",6] )
  

Ref:

1. This article describes sweeping pts across space curve, 
in a way that is not as easy as chainPts() uses:
http://webhome.cs.uvic.ca/~blob/courses/305/notes/pdf/ref-frames.pdf

2. trigs
http://www.ibiblio.org/e-notes/Splines/tree/maple.htm

3. Frenet-Serret_formulas
https://en.wikipedia.org/wiki/Frenet%E2%80%93Serret_formulas



*/

function chaindata( 
        
    pts
    , nside 
    , nsidelast // 2015.7.23: Different nside than the 
                // beginning face, allowing for loft.
           /*  The loft can be done in :
           
               1) Set the 1st csec with either nside or csec0
               2) Set the last csec w/ either nsidelast or cseclast
               
               Note that in these conditions :
               -- twist will NOT twist the csecs, but will rotate 
                    the cseclast for loft
               
                  .------.
                .'        `.
              .`     .      `.
               `.          .'
                 `.______-`

           */ 
    , loftpower // Power of lofting. The real r at pt i, ri, changes
                // its value from r_head to r_tail over i's. The larger 
                // the loftpower is, the faster the ri approaches r_tail. 
                // If =0, the change of r follows a straight line. If 
                // between -1~0, r will remains unchanged until the end 
                // of pts before it starts lofting. If <-1, r could shrink 
                // first before start lofting. 
                 
    , r    // radius at the head (first csec)
    , rlast   // radius at the tail (last csec)
    , rs     // array of r to be added on top of r 
             //   If rs < pts, repeat rs  
             // Note that this array could contain arrays:
             //   rs = [ 3, [3,3.5], 2,2 ] 
             // indicating multiple r's at that pt
    , closed // If the chain is meant to be closed.
             // The starting and ending crosecs will
             // be modified. 
    //-------------------------------------------------
    , twist // Angle of twist of the last crosec 
            // comparing to the first crosec
    , rot   // The entire chain rotate about its bone
            // away from the seed plane, which
            // is [ p0, p1, Rf ]


    //-------------------------------------------------
    // The following are used to generate data using 
    // randchain( data,n,a,a2... ) when data is a pts.
    // They are ignored if data is (2) or (3) 

    //         , n      //=3, 
    //         , a      //=[-360,360]
    //         , a2     //=[-360,360]
    //         , seglen //= [0.5,3] // rand len btw two values
    //         , lens   //=undef
    //         , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
    //                         //   , "clos
    , seed   //=undef // base pqr
    , Rf // Reference pt to define the starting
         // seed plane from where the crosec 
         // starts 1st pt. Seed Plane=[p0,p1,Rf]
            
    //-------------------------------------------------
    , csec0 // Define cross-section. This would become
             // the first cross-section (i.e., chain head)
             // When defined, the r,rlast, and rs are ignored
    , cseclast // Define cross-section. This would become
             // the last cross-section (i.e., chain tail)
             // When defined, the r,rlast, and rs are ignored
             // Define both csec0 and cseclast to loft.
             
    
    , bevel0

    , cut    
    , cutlast   /* [x,a]
                 cut angle at first face( cut) or last face
                 
                 x is a number btw 0~360, representing
                 pos on a clock face. Line XO will be the
                 axis about which the pl_PQRS will rot
                 and tilt by angle a to create the cut.
                 
                            /  
                           / Z
                       /  / /    /
                      / _Q_/    /
                     _-`  /`-_ /
                    R    O----P
                     `-_   _-`
                        `S`
                        
                 Let Z the last pt of bone. 
                 
                 Default x is 0, which means the cut is
                 to tilt toward the first pt of xsec. i.e,
                 aPOZ = a. 
                 
                 If x is 90, toward the Q-direction on
                 the above graph, i.e., aQOZ=a 
                 
                 hcut could be a number a, in that case
                 default OQ is the axis. i.e., x=0       
              */

    //===============================================
    , _pl90at0 // The pl90 ( pts on the first 90-deg plane.
               // It is kept over the entire length of chain
               // till the last for the possible need of loft
    , opt=[]             
    , _csecs=[]
    ,_i=0
    , _debug)=
(   
   _i==0?
   
   let(//###################################### i = 0 
   
      //------------------------------
      //    param initiation
        
        opt = popt(opt) // handle sopt
      , pts = ispt(pts[0])? pts 
              : haskey( pts, "pts")? hash(pts, "pts")
              : or(seed, hash(opt,"seed"))? seed
              : pqr()
      
      , r   = und(r, hash(opt,"r", 0.5))
      , rlast  = und(rlast,hash(opt,"rlast",r))
      , _rs  = or(rs, hash(opt,"rs",[]))
      
      , rot= und(rot, hash(opt,"rot",0 )) 

      //, xpts= or(xpts, hash(opt,"xpts",undef))
      , csec0= or(csec0, hash(opt,"csec0", undef))
      
      , Rf0= or(Rf, hash(opt,"Rf", undef))       
      , twist= und(twist, hash(opt,"twist",0))
      , closed= len(pts)==2?false
                : und(closed, hash(opt,"closed",false))
                
      //, _hbevel= or( hbevel, hash(opt,"hbevel"))
      //, hbevel= _hbevel==true? ["dir",-1, "r",0.5, "n", 
      
      , _cut= or( cut, hash(opt,"cut"))
      , _cutlast= or( cutlast, hash(opt,"cutlast"))
      , cut = isnum(_cut)?[0,_cut]:_cut
      , cutlast = isnum(_cutlast)?[0,_cutlast]:_cutlast
      
      , _nside= or( nside, hash(opt,"nside",4))
      , nside = csec0? len(csec0):_nside
      , nsidelast= und( nsidelast, hash(opt,"nsidelast"))
      , loftpower= und( loftpower,hash(opt,"loftpower",0))
                  
      //=========================================
      
      , coln_101= iscoln( p_101(pts))   
      , coln012 = iscoln( p012(pts))   
      , coln123 = iscoln( p123(pts))   
      , _Rf = Rf0?Rf0
             : len(pts)==2? randofflnPt( pts )
             : closed ? 
                ( coln012 ?  //   x . . .0---1
                              //  / 
                  ( coln123?      //   x . .0---1---2
                                  //  /
                    randofflnPt(p01(pts))
                    :pts[3]       //    x . . 0---1
                  )               //   /          | 
                : pts[2]     //   x . . 0
                ) 
             : 
               ( coln012?     //   0---1---2
                  ( coln123 || len(pts)==3?     //   0--1--2--3
                    randofflnPt(p01(pts))
                    : pts[3]     //   0--1--2
                  )              //        /   
               : pts[2]       //   0--1 
               )              //     /
               
      , Rf = rot? rotPt( _Rf, p01(pts), rot):_Rf     
      
      ,rs = // If rs < pts, repeat rs        
           _rs && isarr(_rs) && len(_rs)< len(pts)?
             repeat(_rs, ceil(len(pts)/len(_rs)))
            : _rs

      , _newr = isnum(rlast)? (_i*(rlast-r)/(len(pts)-1)+r)
                  : r
         
      , newr = rs ? 
               (  // rs[_i] could be array !!
                 rs[0] && isarr( rs[0] ) ? 
                   [ for(_r=rs[0]) _r+_newr]
                 :isnum(rs[0])? (_newr + rs[0] )
                 :_newr
              )     
              : _newr
       
      //, preP = closed? last(pts): onlinePt( p01(pts), -1)
      , preP = onlinePt( p01(pts), -1)
      
      //====================================================             
      // Data pts are created on pl90, then projected to pl
      // to form csec:
                   
      // pl90 is the actual data pts created on a plane that is
      // 90d to the line p01. They will soon be projected to pl
      //             
      , pl90= ispts(csec0) || ispts( csec0,2) ? 
               let( // Translate pts in given csec0 to the pl90
                    //preP= onlinePt( p01(pts), -1)
                   R90 = anglePt( [ preP, pts[0], Rf], a=90 )
                  , uvX = uv( pts[0], R90 )
                  , uvY = uv( pts[0], N( [preP,pts[0],R90] ) )        
                  ) 
                  [ for(P=csec0) pts[0]+ uvX*P.x + uvY*P.y ] 
             :        
              arcPts( [preP, pts[0], Rf]
                      , a=90
                      , a2= 360*(nside-1)/nside
                      ,n=nside, rad= isarr(newr)?newr[0]:newr
                      )
                  
      , pl90_xtra= isarr(newr)?              
                 // Create xtra pl on the same _i 
                 arcPts( [preP, pts[0], Rf]  
                      , a=90                
                      , a2= 360*(nside-1)/nside
                      ,n=nside, rad= newr[1]
                      )
                 :undef
      
      // pl is the actual plane onto which the pts placed 
      // on pl90 will be projected to. Since it is just a
      // pl to hold data, number and order of pts that form
      // this pl are not important. 
      , pl = closed?
               ( let( pldata = planeDataAt(pts,0
                                      ,closed=true) )
                  hash( pldata,"pl")
               )
              :cut[1]?
               ( let( pldata = planeDataAt(pts,0, Rf=Rf
                                      , rot= cut? cut[0]:undef
                                      , a=cut? cut[1]:undef
                                      ,closed=false) )
                  hash( pldata,"pl")
               ) 
               : pl90  
               
      // csec contains the actual data pts (on the plane pl)         
      , csec= closed|| cut[1] ? 
                projPts( pl90, pl, along=p01(pts) ) 
                : pl90
      , csec_xtra= isarr(newr)?
                  (closed || cut[1]? 
                    projPts( pl90_xtra, pl, along=p01(pts) ) 
                    : pl90_xtra        
                  ):undef  
      
      // _csecs will be carried to next pt 
      , _csecs= isarr(newr)? [ csec, csec_xtra]: [csec]
    
       , _debug=str(_debug, "<hr/>"
          ,_red("<br/>, <b>chaindata debug, _i</b>= "), _i 
          ,"<br/>, <b>rs</b>= ", rs
          ,"<br/>, <b>pts</b>= ", pts
          ,"<br/>, <b>nside</b>= ", nside
          ,"<br/>, <b>closed</b>= ", closed
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>preP</b>= ", preP
          ,"<br/>, <b>pl90</b>= ", pl90
          ,"<br/>, <b>pl90_xtra</b>= ", pl90_xtra
          
          ,"<br/>, <b>pl</b>= ", pl
          ,"<br/>, <b>Rf</b>= ", Rf
          ,"<br/>, <b>csec0</b>= ", arrprn(csec0, dep=2)
          ,"<br/>, <b>csec</b>= ", csec
          ,"<br/>, <b>pts_i</b>= ", arrprn(pts[_i],dep=2)
          ,"<br/>, <b>, cut</b>= ", cut 
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>_csecs</b>= ", _csecs
//          ,"<br/>, <b>xpts_i</b>= ", arrprn(xpts_i, dep=2)
          //,"<br/>, <b>newrtn</b>= ", arrprn(newrtn, dep=2)
        )//_debug
        
      )// let @ i=0
      //_debug
      chaindata( pts
               , nside=nside
               , nsidelast=nsidelast
               , loftpower=loftpower
               , r=r, rlast=rlast, rs=rs
               , closed=closed
               , twist=twist, rot=rot
               , Rf= csec0[0]
               , cseclast= cseclast
               , cut=cut,cutlast=cutlast               
               , _csecs=_csecs
               , _pl90at0 = pl90
               ,_i=_i+1
               ,_debug=_debug
               )                 
 
   : 0<_i && _i <len(pts)-1?
   
    let( //#################################### 0 < i < last 
   
        dtwi = twist? twist/(len(pts)-1) :0
        //, pldata=planeDataAtQ( pts, get3(pts,_i)) 
        , pldata=planeDataAt( pts,_i) 
        
        // The pl where previous data will project to
        , pl = hash( pldata, "pl")
        
        //, p_xsec = last(_xsecs)
         
        , _newr = isnum(rlast)? (_i*(rlast-r)/(len(pts)-1)+r): r
        //, newr = rs&&isnum(rs[_i])?( rs[_i]+_newr):_newr 
        , newr = rs ? (
                 rs[_i] && isarr( rs[_i] ) ? // rs[_i] could be array !!
                   [ for(_r=rs[_i]) _newr+_r]
                 :isnum(rs[_i])? (_newr + rs[_i] )
                 :_newr
                 )
                 :_newr       
                   
        , _pre_csec = twist? 
                   rotPts( last(_csecs), get(pts,[_i-1,_i]), dtwi) 
                   : last(_csecs)
                   
        , pre_csec = (isnum(rlast)&&rlast!=r)||rs? [ for(p= _pre_csec)
                        onlinePt( [ pts[_i-1], p], isarr(newr)?newr[0]:newr)] 
                   : _pre_csec
                  
        , csec_r =  projPts( pre_csec, pl
                           , along=get(pts,[_i-1,_i] )
                           )
        , csec= csec_r               
//        , csec = (isnum(rlast)&&rlast!=r)||rs? [ for(p= csec_r)
//                        onlinePt( [ pts[_i], p], isarr(newr)?newr[0]:newr)] 
//                   : csec_r 
        , csec_xtra = isarr(newr)?
              ( isnum(rlast) || rs? [ for(p= csec_r)
                onlinePt( [ pts[_i], p], newr[1])]
               : csec_r 
              ):undef   
        
        ,newcsecs= isarr(newr)? concat( _csecs,[csec,csec_xtra])
                              : app(_csecs,csec)                   
        //,newxsecs= app(_xsecs,xsec)
        
//       , _debug=str(_debug, "<hr/>"
//          ,"<br/>, <b>_i</b>= ", _i 
//          ,"<br/>, <b>nside</b>= ", nside
//          ,"<br/>, <b>closed</b>= ", closed
//          ,"<br/>, <b>pl</b>= ", pl
//         ,"<br/>, <b>newr</b>= ", newr
//          // ,"<br/>, <b>pts_i</b>= ", arrprn(pts[_i],dep=2)
//         // ,"<br/>, <b>, cut</b>= ", cut 
//         // ,"<br/>, <b>_csec0</b>= ", arrprn(_csec0, dep=2)
//         // ,"<br/>, <b>last_xpts</b>= ", arrprn(last(_xpts),2)
//          //,"<br/>, <b>pl</b>= ", arrprn(pl)
//         // ,"<br/>, <b>xpts_i</b>= ", arrprn(xpts_i, dep=2)
//          ,"<br/>, <b>newcsecs</b>= ", arrprn(newcsecs, dep=2)
//          )//_debug
   
       , _debug=str(_debug, "<hr/>"
          ,_red("<br/>, <b>chaindata debug, _i</b>= "), _i 
          ,"<br/>, <b>rs</b>= ", rs
          ,"<br/>, <b>pts</b>= ", pts
          ,"<br/>, <b>nside</b>= ", nside
          ,"<br/>, <b>closed</b>= ", closed
          ,"<br/>, <b>newr</b>= ", newr
          
          ,"<br/>, <b>pl</b>= ", pl
          ,"<br/>, <b>Rf</b>= ", Rf
          ,"<br/>, <b>pre_csec</b>= ", pre_csec
          ,"<br/>, <b>csec_r</b>= ", csec_r
          ,"<br/>, <b>csec</b>= ", csec
          ,"<br/>, <b>csec_xtra</b>= ", csec_xtra
          ,"<br/>, <b>pts_i</b>= ", arrprn(pts[_i],dep=2)
          ,"<br/>, <b>, cut</b>= ", cut 
          ,"<br/>, <b>csec0</b>= ", arrprn(csec0, dep=2)
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>_csecs</b>= ", _csecs
          ,"<br/>, <b>newcsecs</b>= ", newcsecs
          )
        )// let @ i>0

      //_i==len(pts)-2? _debug:  
      chaindata( pts
               , nside=nside
               , nsidelast=nsidelast
               , loftpower=loftpower
               , r=r, rlast=rlast, rs=rs
               , closed=closed
               , twist=twist, rot=rot
               , Rf= csec0[0]
               , cseclast= cseclast
               , cutlast=cutlast               
               , _csecs= newcsecs
               , _pl90at0 = _pl90at0
               ,_i=_i+1
               ,_debug=_debug
               )  
               
//       chaindata( pts, r=r, rlast=rlast, rs=rs
//                , nside=nside, rot=rot, Rf=xsec[0]
//                , cut=cut,cutlast=cutlast, twist=twist
//                , closed=closed, _xsecs=newxsecs
//                ,_i=_i+1,_debug=_debug)
                
                
      : //_i==len(pts)-1? 
   
     let( //#################################### i = last
           dtwi = twist? twist/(len(pts)-1) :0
 
        , pldata= cutlast && cutlast[1]?
                   planeDataAt( pts,_i,
                        , rot= cutlast? cutlast[0]:undef
                        , a=cutlast? cutlast[1]:undef
                        , closed=closed) 
                   :planeDataAt( pts,_i,
                        //, Rf=Rf
                        , closed=closed
                   ) 
        , pl = hash( pldata, "pl")
        
        , pre_csec = twist? 
                   rotPts( last(_csecs), get(pts,[_i-1,_i]), dtwi) 
                   : last(_csecs)
 
         , _newr = isnum(rlast)? (_i*(rlast-r)/(len(pts)-1)+r)
                  : r

       , newr = rs ? (
                 rs[_i] && isarr( rs[_i] ) ? // rs[_i] could be array !!
                   [ for(_r=rs[_i]) _r+_newr]
                 :isnum(rs[_i])? (_newr + rs[_i] )
                 :_newr
                 )
                 :_newr
                   
        , csec_r =  projPts( pre_csec, pl
                           , along=get(pts,[_i-1,_i] )
                           )
        ,_csec = isnum(rlast)&&rlast!=r || rs? [ for(p= csec_r)
                        onlinePt( [ pts[_i], p], isarr(newr)?newr[0]:newr)]
                   : csec_r 
        
        , csec = newr==0 && !csec0?  // cone
                 [_csec[0]]:_csec
        
        , csec_xtra = isarr(newr)?
                      ( isnum(rlast) || rs? [ for(p= csec_r)
                        onlinePt( [ pts[_i], p], newr[1])]
                       : csec_r 
                      ):undef   
        
        ,_newcsecs= isarr(newr)? concat( _csecs,[csec,csec_xtra])
                              : app(_csecs,csec) 
        , newcsecs=is0(r + (rs?rs[0]:0))? // for conedown
                    replace( _newcsecs,0,[pts[0]]) 
                   : _newcsecs 
        /*
          lofting
            
          When nsidelast is set, Lofting is handled AFTER all other
          processes are done. This is not the opitimized way,
          'cos it will do many steps that are not needed for the 
          lofting, so it's a waste of time. However, this is a 
          complicated process and could mess up other process easily, 
          so mixing them up is not a good idea at this moment. 2015.7.25

          Lofting is done by first calc the "paring data" using 
          the first and last crossections(taken from newcsecs) with
          getParingData(), converting to two xsecs (called lofthead, 
          lofttail) that have same # of pts.
          
          Then loop thru pts, in each step i, 
          (1) translocate the lofthead to the pl that's 90d to the 
          bone at pts[i] (call it pl90). Lets call it loft_i; 
          (2) Loop thru loft_i, in each step xi, adjust each r (dist 
          between loft_i[xi] and bone) according to rdiffs (r 
          differences) obtained w/ getParingData(). Call the result 
          loft_i_r 
          (3) Project loft_i_r onto the pl at i (pl_i).   
          
        */    
        , csecs = nsidelast>2? // lofting
                  let( 
                       pts1 = newcsecs[0] 
                     , lastcsec= last(newcsecs)
                     , pts2 = circlePts( 
                                pqr= [ get(pts,-2)
                                     , get(pts,-1)
                                     , lastcsec[0]] 
                                , rad= dist( lastcsec[0], last(pts))
                                , n = nsidelast
                             )
                             
                     , pairdata =  getPairingData(
                                       pts1= pts1
                                      , pts2= pts2
                                      )
                     , lofthead = hash( pairdata, "pts1") // head face
                     , lofttail = hash( pairdata, "pts2") // tail face
                     , angles = // Angles of each pt (in lofthead or 
                                // lofttail) spanning from pt [0]. These
                                // angles are the same for lofthead/lofttail)  
                                hash(pairdata, "angles")
                     , rdiffs = // radius and their differences:
                                // [ [rdiff0,r_head0,r_tail0]
                                // , [rdiff1,r_head1,r_tail1]
                                // ]
                                hash( pairdata, "rdiffs" ) 

                     , lenratio = // For [p0,p1,p2...pn] of pts (the bone),
                                  // ratio of di/d where di= dist(0,i),
                                  // d= dist(0,n), eg: [ 0, 0.3, 0.45,0.78,1]
                            let( lens= [ for(i=[1:len(pts)-1])
                                        dist( get(pts, [i-1,i])) ]
                               , lensum= sum(lens)
                               , accumlens= accum(lens)
                               )
                            concat([0],[ for( d=accumlens) d/lensum ])
                     )
                            
                 [ for( i=range(pts) )
                      i==0 ? lofthead
                      : i== len(pts)-1? lofttail
                      : let( pts_unloft = newcsecs[i]
                           , lofthead_at90i = 
                                // this is lofthead relocated to pl 90d to (i-1,i)
                                [for(xi= range(lofthead))
                                    
                                  anglePt( [ pts[i-1], pts[i], pts_unloft[0] ]
                                         , a=90, a2=angles[xi]
                                         , len= rdiffs[xi][1]
                                         )    
                               ]
                            , lofttail_at90i = 
                                // this is lofttail relocated to pl 90d to (i-1,i)
                                [for(xi= range(lofttail))
                                   
                                  anglePt( [ pts[i-1], pts[i], pts_unloft[0] ]
                                         , a=90, a2= angles[xi]
                                         , len= rdiffs[xi][2]
                                         )      
                                ] 
                             , projected_loft_i = 
                                [for(xi= range(lofttail))
                                  let( phead= lofthead_at90i[xi]
                                     , ptail= lofttail_at90i[xi]
                                     , dr = rdiffs[xi][0] // diff of r
                                     , dri_ratioed= // dist of phead~ptail when 
                                                // lofthead and lofttail are
                                                // placed on the same plane.
                                                // It could be <0
                                                dr* lenratio[i] 
                                    , dri_factored =
                                            loftpower>=0?  
                                            ( dri_ratioed
                                             +(dr-dri_ratioed)
                                              *(1-exp(-loftpower*lenratio[i]))
                                            ):                                
                                            ( dri_ratioed
                                              -(dr-dri_ratioed)
                                              *(1-exp(loftpower*lenratio[i]))
                                            )   

                                     , p90=  onlinePt( [phead,pts[i]]
                                                     , len= -dri_factored)
                                     )
                                projPt( p90, p012( pts_unloft ),get(pts,[i-1,i]) )
                                ]
                             ) 
                      projected_loft_i 
                ]
                : newcsecs      
                      
                      
//        , newxsecs = rlast==0 && !csec0?  // cone
//                     [_newxsecs[0]]
//                     : _newxsecs  
                    
       , _debug=str(_debug, "<hr/>"
          ,"<b>_i</b>= ", _i 
          ,"<br/>, <b>nside</b>= ", nside
          ,"<br/>, <b>csec</b>= ", csec
          ,"<br/>, <b>closed</b>= ", closed
          ,"<br/>, <b>pl_i</b>= ", pl
          ,"<br/>, <b>pldata</b>= ", pldata
          ,"<br/>, <b>cutlast</b>= ", cutlast 
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>twist</b>= ", twist
          ,"<br/>, <b>dtwi</b>= ", dtwi
         /// ,"<br/>, <b>last_xpts</b>= ", arrprn(last(_xpts),2)
          ,"<br/>, <b>pts</b>= ", pts
          ,"<br/>, <b>rs</b>= ", rs
          ,"<br/>, <b>csec_xtra</b>= ", csec_xtra
          ,"<br/>, <b>newcsecs</b>= ", arrprn(newcsecs, dep=2)
          )//_debug      

        )
      //_debug
      ["csecs", csecs
     // ,"csecs_", csecs_
        // ,"xpts0", xpts
         //, "seed", app( p01(pts), _Rf)
         ,"bone", pts
      ,"newcsecs",newcsecs
      ,"cut", cut //[cut,cutax]
         ,"cutlast", cutlast //[cutlast,cutlastax]
         ,"rs", rs
         ,"closed", closed
         ,"Rf", Rf
         ,"debug", _debug
         ]          
                
);  


//========================================================
 
function cirfrags(r, fn, fs=0.01, fa=12)=
( // from: get_fragments_from_r
  // http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn
    fn? max(fn,3)
	: max(min(360 / fa, r*2*PI / fs), 5)
);

            
//=========================================
convPolyPts=["convPolyPts","arr,to","arr","Geom",
"For tube-like shapes: convert pts between out-in order and xsec order.
;;
;; Let tube T with 5 sides going from X (a pt on x-axis) 
;; to O (Origin).
;;             
;; Looking at T from X (to O), an outin-style pt order is 
;; (all pts going counter-clock):
;;             
;; [ [ P,Q,R,S,T ]  // outer pts on X-end
;; , [ A,B,C,D,E ]  //           on O-end
;; , [ U,V,W,X,Y ]  // inner pts on X-end
;; , [ F,G,H,I,J ]  //           on O-end
;; ]             
;;
;; Now, see T as a ring-like structure, and read its 
;; cross-sections along the ring circle, an xsec-style is:
;;
;; [ [P,A,F,U] // xsec 0 (again, counter-clockwise)
;; , [Q,B,G,V] // xsec 1
;; , [R,C,H,W]
;; , [S,D,I,X]
;; , [T,E,J,Y]
;; ] 
;;
;; By converting xsec to outin, we can use the faces
;; that is generated by faces() function:
;;
;;  faces=faces('tube', sides=5); // this uses outin style
;;  polyhedron( points = joinarr(pts_xsec_converted_to_outin)
;;             , faces=faces ); 
"
];
function convPolyPts( arr, to="outin")=
(
  to=="outin"?
    switch( transpose( arr ), 2,3)
  : transpose( switch(arr,2,3))
);

//=========================================
function coordPts(pqr, len,x,y,z)= 
    let( P=pqr[0], Q=pqr[1], R=pqr[2]
       , len= len==undef?dist([P,Q]):len
       , x = x==undef?len:x
       , y = y==undef?len:y
       , z = z==undef?len:z
       , N=N(pqr,len=z), NN=N( [N, Q(pqr),P(pqr)] ,len=y) 
       )
(  [ onlinePt( [Q,P], len=x )
   , Q, NN, N ]
);

//=========================================
function tocoord(pts, coord)= 
    let( O = coord[1]
       , X = coord[0]
       , Y = coord[2]
       , Z = coord[3]
       )
(  [for(p=pts) [ dist( [O, othoPt([ O,p,X])] )
               , dist( [O, othoPt([ O,p,Y])] )
               , dist( [O, othoPt([ O,p,Z])] )
               ]] 
);


//========================================================

function centroidPt(pqr)=
   let( P=pqr[0], Q=pqr[1], R=pqr[2] )
   intsec( [P, midPt(Q,R)], [Q, midPt(P,R)] );  

//   lineCrossPt( P, midPt(Q,R), Q, midPt(P,R) );  


//========================================================
function chainbone(
       n //=3, 
     , a //=[-360,360]
     , a2 //=[-360,360]
     , seglen //= [0.5,3] // rand len btw two values
     , lens //=undef
     , seed //=undef // base pqr
     , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
                     //   , "closed",false,details=false]
     , opt=[]
     , _rtn=[] // for noUturn
     , _smsegs=[] // segments of smoothed pts
     ,_i=0 // for noUturn
     ,_debug
    )
=(
//  !n? _red(str( "ERROR: the n(# of pts) given to randchain() is invalid: ",n ))
//  :(
    _i==0? //####################################### i==0
    
    let( 
        //=========[ initiation ] =======

          opt = popt(opt) // handle sopt
        , n = n>1?n: hash(opt,"n",2)
        , a = und(a, hash(opt,"a", [170,190])) // = aPQR
        , a2= und(a2,hash(opt,"a2",[-45,45])) // = aRQN
        , seglen= or(seglen, hash(opt, "seglen"))
        , _lens = or(lens, hash(opt, "lens"))
        , seed = or(seed, hash(opt, "seed", randPts(3)))
        , smooth= und(smooth, hash(opt, "smooth"))

        //==============================

        ,lens = // If lens < n-1, repeat lens        
           _lens && isarr(_lens) && len(_lens)< n-1?
             repeat(_lens, ceil((n-1)/len(_lens)))
            : _lens
            
//        , L = isarr(lens)? lens[_i-1]
//            : isarr(seglen)? rand(seglen[0],seglen[1]) 
//            : isnum(seglen)? seglen
//            : rand(0.5,3)
        , pt= seed? seed[0]:randPt() //d,x,y,z)
        , newrtn = [pt]  
              
     )//let @_i==0
     
     chainbone(n=n,a=a,a2=a2,seglen=seglen, lens=lens
              , seed = seed
              //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1
              , smooth=smooth, _debug=_debug)   
              
   :let(  // ###################################### i > 0
   
     isend= _i==n-1
     ,_a = isarr(a)? rand(a[0],a[1]):a
     ,_a2= isarr(a2)? rand(a2[0],a2[1]):a2
     ,L = isarr(lens)? lens[_i-1]
          : isarr(seglen)? rand(seglen[0],seglen[1]) 
          : isnum(seglen)? seglen
          : rand(0.5,3)
     ,pt= _i==1? (seed? onlinePt( p01(seed), len=L)
                      :onlinePt( [last(_rtn)
                               , randPt()],len=L)
                  )    
          :  let( P = get(_rtn,-2)
                  , Q = get(_rtn,-1)
                  , Rf = _i==2 && seed? seed[2] //onlinePt( p12(seed), len=L)
                         : iscoln( sel(_rtn, [-3,-2,-1]))
                            ?randofflnPt( get(_rtn,[-2,-1]))
                           : sel(_rtn,-3)
                  )
               anglePt( [P,Q,Rf], a=_a, a2=_a2, len=L)   
                       
     ,newrtn = app(_rtn, pt ) 
     , smpts = isend?
              ( smooth==true? smoothPts(newrtn)
                :smooth? smoothPts(newrtn, opt=smooth) 
//                   smoothPts( newrtn, n=hash(smooth, "n")
//                            , aH=hash(smooth,"aH")
//                            , dH=hash(smooth,"dH")
//                            , closed= hash(smooth, "closed")
//                            , details= hash(smooth, "details")
//                       )
                       
                   //n=10, aH=0.5, dH=0.3, closed=false, details=false
                  :newrtn
              ):[]
          
//     ,_debug= str(_debug, "<br/>"
//                 ,"<br/>, <b>_i</b>= ", _i
//                 ,"<br/>, <b>len</b>= ", len
//                 ,"<br/>, <b>L</b>= ", L
//                 ,"<br/>, <b>_rtn</b>= ", _rtn
//                 ,"<br/>, <b>newrtn</b>= ", newrtn
//                 ,"<br/>, <b>actual len</b>= ", dist( sel(newrtn,[-1,-2]))
//                 ,"<br/>, <b>pt</b>= ", pt
//                 )
     )//let

  //isend? (smooth? [newrtn,smpts]: newrtn ) 
  isend? 
    [ "pts", smpts
    , "unsmoothed", newrtn
    , "smooth", smooth
    , "seed", seed
    , "lens",lens
    , "seglen", seglen
    , "L",L
    , "a",a
    , "a2", a2
    , "debug",_debug
    ]
    // (smooth? [newrtn,smpts]: newrtn ) 
  
  : chainbone(n=n,a=a,a2=a2,seglen=seglen, lens=lens, seed = seed
             //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1, smooth=smooth, _debug=_debug)
  //)// if_n
);

//========================================================
function cornerPt(pqr)=
(
	onlinePt( [pqr[1],midPt	([pqr[0],pqr[2]])], ratio=2)
);

//========================================================

function cubePts( pqr, p=undef, r=undef, h=1,shift)=
 let( _pqr= or(pqr,randPts(3))
    , Q = Q(_pqr)
	, h_vector = normalPt(_pqr, len=h)-Q
	, shiftvec = onlinePt( [N(_pqr), Q], len=-shift )-Q
	, pqr = isnum(shift)
			? [ for (p=_pqr) p+shiftvec ] 
			:_pqr 
    )	
(	concat( squarePts( pqr, p=p,r=r )
		  , squarePts( //addv( pqr, normalPt(pqr, len=h)-Q(pqr))
					  [for (pt=pqr) sign(h)>0?(pt-h_vector):(pt+h_vector) ]
                      ,p=p,r=r )
	)		
);


//========================================================
// Note 2015.4.11:
// dist could be made available for dist of dist(a,b)
//                a       b   
//   pt-pt      P       Q
//   pt-ln      P       [Q,R] 
//   ln-pt      [Q,R]   P
//   ln-ln      [P,Q]   [R,S]       
//   pt-pl      P       [R,S,T]
//   pl-pt      [R,S,T] P
//   ln-pl      [P,Q]   [R,S,T]
//   pl,ln      [R,S,T] [P,Q]
//   pl,pl      [P,Q,R] [S,T,W]


                      
//========================================================

////function dist(pq) = norm(pq[1]-pq[0]);  // Get distance between a 2-pointer
function dist(a,b,ij=[])=
(
    // http://math.harvard.edu/~ytzeng/worksheet/distance.pdf
    let( c= b==undef?(ij?a:a[0]):a
       , d= b==undef?a[1]:b
       , t= str(gtype(c), gtype(d)) 
       )
     ij? dist( get(a,ij) )  
    :d=="x"? c[1][0]-c[0][0]
    :d=="y"? c[1][1]-c[0][1]
    :d=="z"? c[1][2]-c[0][2]
    :t=="ptpt"? norm(c-d)
     
    :t=="ptln"? norm(c- othoPt( [d[0],c,d[1]] ) )
    :t=="lnpt"? norm(d- othoPt( [c[0],d,c[1]] ) )
    
    :t=="lnln"? uv(c)==uv(d)? // parallel
                  norm( othoPt( [c[0],d[0],c[1]]) - d[0])
                // skew lines, use unit vector of c to make a pl
                // with d (pl=[ d[0],d[1], d[0]+uv_vc, which is
                // parallel to c, then calc dist btw c and pl  
                : abs(distPtPl( c[1], [d[0],d[1], d[1]+ uv(c)] ))
                            
    :t=="ptpl"? distPtPl( c, d )
    :t=="plpt"? distPtPl( d, c )
    :t=="lnpl"? distPtPl( c[0], d)
    :t=="plln"? distPtPl( d[0], c)
    :t=="plpl"? undef // need this
    :undef
);

//========================================================

// UNDER CONSTRUCT
//
//function distPts( ln1, ln2 )= 
//(
//    // Given 2 lines, return 2 pts,A,B, one on each line, 
//    // that the dist_AB is the dist between the 2 lines
//    // (shortest distance)
//    // NOTE: have to watch if any func used in term requires this
//    //       func, which creates endless loop. Funcs we use all 
//    //       trace back to intsec(ln,ln), which is fine.
//   let( R = ln2[0]
//      , S = ln2[1]
//      , Jr = projPt( R, ln1)
//      , Js = projPt( S, ln1)
//      , dr = dist( Jr,R )
//      , ds = dist( Js,S )
//      , B = onlinePt( [R,S], ratio = dr/(dr+ds) )
//      , A = projPt( B, ln1 )
//      //, A = onlinePt( [Jr,Js], ratio=dr/(dr+ds) ) 
//      //, B = projPt( A, ln2) //onlinePt( [R,S], ratio=dr/(dr+ds) ) 
//      ) [A,B, Jr, Js]
//      
//    // Approach:
//    //   let ln1= [P,Q], ln2= [R,S]
//    //   pl = [P,Q,R]
//    //   J  = projPt( S,pl)
//    //   A = intsec( [P,Q], [R,J] )
//    //   B = onlinePt( [R,S], ratio= dist( R,A) / dist(R,J) )
//    // 
////   let( R = ln2[0]
////      , S = ln2[1]
////      , pl= [ ln1[0],ln1[1],R] 
////      , J = projPt( S, pl )
////      , A = intsec( ln1, [R,J] )
////      , B = projPt( A, ln2) //onlinePt( [R,S], ratio= dist( R,A) / dist(R,J) )    
////      ) [A,B, J]
//);


//========================================================

function distPtPl(pt,pqr)=
(
	( planecoefs(pqr, has_k=false)* pt  + planecoefs(pqr)[3]) 
	/ norm(	planecoefs(pqr, has_k=false) )
);


//========================================================
function expandPts(pts,dist=0.5, cycle=true, _i_=0)=
 let( p3s = subarr( pts,cycle=cycle )
	)
//	, p3s = [for(ijk=i3s) pts[
(	// 
	[ for(i=range(pts))
		onlinePt( [pts[i], angleBisectPt( p3s[i] )]
				, len=-dist/sin(angle(p3s[i])/2)
			    )
	]
);

	




//========================================
faces=["faces","shape,nside,nseg", "array", "Faces,Geometry",
 " Given a shape (''rod'', ''cubesides'', ''tube'', ''chain''), an int
;; (sides) and an optional int (count), return an array for faces of
;; a polyhedron for that shape.
"];


function faces( 

        shape="rod"
      , nside=4
      , nseg=3 // for chain,ring
      , tailroll=0
        // nseg is used for shape that has
        // segments unknown until run-time
        // For example, chain
        /* tailroll: when closed, you might want to 
        //      rotate the tail face to align with
        //      the head face so the closed link
        //      from head to tail won't look twisted
        // tailroll is an int as "how many pts to roll"
         
             tailroll=0     = 1       = 2
             45----46    46----38  38----37
             |      |    |      |  |      |
             |      |    |      |  |      |
             37----38    45----37  46----45
        */ 
     , nside_in   // for tubeEnd
     , idx_in   // for tubeEnd
     , idx_out  // for tubeEnd
     /* 
        shape= "tubeEnd" is for tubes that has different
        nsides in and out
        
        Set either te_nside_in and te_nside_out, or 
        te_idx_in and te_idx_out. 
        
        If setting te_nside_in/te_nside_out, the indices
        will start from 0:
        
        te_nside_in= 3
        te_nside=out=5
        ==> indices: in:[0,1,2], out:[3,4,5,6,7]

     */
      
)=    
(let( s = nside                                
	, s2= 2*nside                              
	, s3= 3*nside
    , shape = isarr(shape)? // If an arr is given, try to get shape
                            // This arr should be:[ pts, pts, pts...]
              ( len(shape[0])==1 && len(last(shape))==1? "spindle"
                : len(shape[0])==1 ? "conedown"
                : len(last(shape))==1? "cone"
                : "chain"
              ):shape
                
    )
	//==================================================
	// cubesides
	//       _6-_
	//    _-' |  '-_
	// 5 '_   |     '-7
	//   | '-_|   _-'| 
	//   |    |-4'   |
	//   |   _-_|    |
	//   |_-' 2 |'-_ | 
	//  1-_     |   '-3
	//     '-_  | _-'
	//        '-.'
	//          0
	shape=="cubesides"
	? [ for(i=range(s))
		  [ i
			, i==s-1?0:i+1
			, (i==s-1?0:i+1)+s
			, i+s
		  ]
	  ]

    : shape=="rod"
	? concat( [ reverse(range(s))], [range(s,s2) ],
			  faces("cubesides", s) ) 

	//==================================================
	// cone
    /*
              8
	//       /:`   
	//      / |: -
	//     /  | : `_ 
	//    /   |  :  '_
	//   /_.-'6-._:   `_
	//  5-._  |    :'-._`_  
	//  |   `'-._   :   `7
	//  |     |  `'-.4-'`|
	//  | _.-'2-._   |   |
	//  1-._       `'|._ |  
	//      `'-._    |   3
	//           `'-.:-'`
	//               0    
     */
    : shape=="cone"
    ? concat( [ reverse(range(s))]        // starting face
    
			, joinarr( [ for(c=range(nseg-1)) // nseg-1 because
				 [ for(f=faces("cubesides", nside)) // last one
					[ for(i=f) i+c*nside]           // is cone
				 ]
                      ])
                    
             // The last seg is the cone
            , [ for(i=range( (nseg-1)*nside // = 4 above 
                           , nseg*nside ))  // = 8 above 
                [  i, i==nseg*nside-1?(nseg-1)*nside:i+1
                , nseg*nside]
              ]            
			)   
 
	//==================================================
	// conedown
    /*
              0
	//       /:`   
	//      / |: -
	//     /  | : `_ 
	//    /   |  :  '_
	//   /_.-'3-._:   `_
	//  4-._  |    :'-._`_  
	//  |   `'-._   :   `2
	//  |     |  `'-.1-'`|
	//  | _.-'7-._   |   |
	//  8-._       `'|._ |  
	//      `'-._    |   6
	//           `'-.:-'`
	//               5    
     */
    : shape=="conedown"
    ? concat( 
                 // The 1st seg is the cone
                [ for(i=range( 1 , nside+1 ))  
                  [ i, i==1?nside:i-1, 0 ]
                ] 
    
             , joinarr( [ for(c=[0:nseg-2]) // 
                     [ for(f=faces("cubesides", nside)) 
                         [ for(i=f) i+c*nside+1]           
                     ]
                  ])
                        
             , [ range( (nseg-1)*s+1,(nseg)*s+1) ] // ending face
             
			)   

	//==================================================
	// spindle
    /*
              0
	//       /:`   
	//      / |: -
	//     /  | : `_ 
	//    /   |  :  '_
	//   /_.-'3-._:   `_
	//  4-._  |    :'-._`_  
	//  |   `'-._   :   `2
	//  |     |  `'-.1-'`|
	//  | _.-'7-._   |   |
	//  8-._       `'|._ |  
	//      `'-._    |   6
	//           `'-.:-'`
	//               5    
     */
    : shape=="spindle" // spindle example in chainPts_demo_coln_arrow()
    ? concat( 
                 // The 1st seg is the cone
                [ for(i=range( 1 , nside+1 ))  
                  [ i, i==1?nside:i-1, 0 ]
                ] 
    
             , joinarr( [ for(c=[0:nseg-3]) // 
                     [ for(f=faces("cubesides", nside)) 
                         [ for(i=f) i+c*nside+1]           
                     ]
                  ])
                        
             // The last seg is the cone
            , let( i0= (nseg-2)*nside+1
                 , il=(nseg-1)*nside 
                 )
              [ for(i=[i0:il])
                [  i, i==il?i0:i+1, il+1] 
              ] 
			)                
                         
    //==================================================
	// tubeEnd
    // 
    // End faces for tubes that has different nsides 
    // on the inside and outside walls
    /*            
         S         0.       P
          `.    _-`  '.   .`
            `..'   4   `.` 
            -``._-` `-_` `. 
          -`   9`.  .` 5   `.  
        3`     |  `O   |     `1
         `.    8 .` `. 6   _-`
           `.  .`-_ _-`. _'
             `.    7   _'.
            .` `.    .'   `.                -`
          .`     `2`        Q
          R
          
       Approach:
       
       1) Align minimal indices (0 and 4 above)
       2) Find the side having smaller nside (outside above)
       3) Find the midpts of each edge (P,Q,R,S above). 
          This divides out-side into nside parts.
       4) Loop through nside_in. ALL indices fall within one 
          part will pair up with the corresponding nside 
          indices. For example, 
          part 0: [0,4]
          part 1: [1,5],[1,6]
          part 2: [2,7]
          part 3: [3,8],[3,9]
       5) If there's only one pair in a part, means it will
          join neighbor parts on both directions to form 
          4-index faces. For example, 
          [0,4] => [4,0,1,5], [0,4,9,3]
          [2,7] => [7,2,3,8], [2,7,6,1]
       6) If there r more than one pair in a part, make 
          3-index faces:
          [1,5],[1,6] => [1,6,5]
          [3,8],[3,9] => [3,9,8]        
    */
    : shape=="tubeEnd"?
      let( 
          // When idx_out and idx_in are not given, auto
          // generate indices for out and in based on nside
          // and nside_in, resp. 
          //   idxo=[0, 1, 2, 3], idxi=[0,1,2,3,4,5]
          //
          // Outer indices
           idxo= or(idx_out,range(or(nside,4)))
         , Lo  = len(idxo)
          // Inner indices
         , _idxi= or(idx_in, range(or(nside_in,6)))
         , Li  = len(_idxi)
         
         // If auto generate, need to convert idxi 
         // from [0,1,2,3,4,5] to [4,5,6,7,8,9] 
         , idxi = (!idx_out && !idx_in)?
                      _idxi +repeat([(last(idxo)+1)], Li)
                    : _idxi
                  
         , idxs= Li>=Lo? idxo// idx w/ small nside
                        : idxi
         , idxl= Li< Lo? idxo // idx w/ large nside
                        : idxi
         , Ls = len(idxs) 
         , Ll = len(idxl) 
         
         , parts= [ for(i= range(idxs) ) // loop thru sma
                     let( a_from = (360*(i-0.5))/Ls 
                        , a_to   = (360*(i+0.5))/Ls
                        , idx = idxs[i]
                        , _pairs= [for(j= range(idxl))
                                let(a = (360*j)/Ll )
                                if( a>=a_from && a<a_to )
                                 [idx, idxl[j]]
                                ] 
                         )
                     concat(  
                        [for(j= range(idxl))
                          let(a = (360*j)/Ll )
                          if( (a-360)>=a_from && (a-360)<a_to )
                          [idx, idxl[j]]
                        ] , _pairs
                        )
                  ]
                        
         /* parts:
            [ [ [0, 4] ]
            , [ [1, 5], [1, 6] ]
            , [ [2, 7] ]
            , [ [3, 8], [3, 9] ]
            ]
         */               
         , _faces= [ 
            for(i=range(parts))
            let( part= parts[i]
               , pre = parts[ i==0?len(parts)-1:i-1 ]
               , next= parts[ i==len(parts)-1?0:i+1 ]
               ) 
              len(part)==1 ?
                [ len(pre)==1?[]
                  :concat( part[0],reverse(last(pre)) )
                , concat( reverse(part[0]), next[0] )  
                ]
             :  concat( 

                    part[0][0]==part[1][0]? // [[1,5],[1,6]]
                    [ for( i= [1:len(part)-1] )
                       app( part[i],part[i-1][1])
                    ]
                   :[ for( i= [1:len(part)-1] ) // [[5,1],[6,1]]
                       app( part[i], part[i-1][0])
                    ]
                , 
                  len(next)>1?
                   [concat( reverse(last( part)), next[0])]
                   :[]
                )
            ]
         , faces= joinarr(_faces)
         )// let
     faces               
  //   [ "idxo",idxo, "idxi", idxi, "parts", arrprn(parts,2,nl=true), "faces_fmt",arrprn(faces,2,nl=true),"faces",faces ]
    
    //==================================================
	// tube
	//             _6-_
	//          _-' |  '-_
	//       _-'   14-_   '-_
	//    _-'    _-'| _-15   '-_
	//   5_  13-'  _-'  |      .7
	//   | '-_ |'12_2-_ |   _-' | 
	//   |    '-_'|    '|_-'    |
	//   |   _-| '-_10_-|'-_    |
	//   |_-'  |_-| 4' _-11 '-_ | 
	//   1_   9-  | |-'       _-3
	//     '-_  '-8'|      _-'    
	//        '-_   |   _-'
	//           '-_|_-'
    //              0
    //  bottom=	 [ [0,3,11, 8]
    //			 , [1,0, 8, 9]
    //			 , [2,1, 9,10]
    //			 , [3,2,10,11] ];
    //
    //	top	=	 [ [4,5,13,12]
    //			 , [5,6,14,13]
    //			 , [6,7,15,14]
    //			 , [7,4,12,15] ]
    : shape=="tube"
    ? (
        nside_in?
        
        concat( faces( "cubesides", nside )  // outside sides
			, [ for(f=faces("cubesides",nside_in )) //inner sides
					reverse( [ for(i=f) i+s2] )
			  ]
            , [ for(f = faces("tubeEnd" 
                       , idx_out= (range(nside))
                       , idx_in = (range( nside*2, nside*2+nside_in))
                       ) )
                    if(f) reverse(f)
              ]     
			, [ for(f = faces("tubeEnd"
                       , idx_out= (range(nside,nside*2))
                       , idx_in = (range(nside*2+nside_in
                                        ,(nside*2+nside_in*2))
                                  )
                        ))
                   if(f) reverse(f)      
              ]      
			)	  
       
	  : concat( faces( "cubesides", s )  // outside sides
			, [ for(f=faces( "cubesides", s ))    // inner sides
					reverse( [ for(i=f) i+s2] )
			  ]
			, [ for(i=range(s))        // bottom faces
				  [i, mod(i+s-1,s), mod(i+s-1,s)+s2, i+s2 ] 
			  ]
			, [ for(i=range(s,s2))    // top faces
				  [i, i==s2-1?s:i+1, i==s2-1?s3:i+s2+1, i+s2 ]
			  ]
			)	  
      ) 
	//==================================================
	// chain                
	//       _6-_-------10------14
	//    _-' |  '-_    | '-_   | '-_
	//  2'_   |     '-5-----'9-------13
	//   | '-_|   _-'|  |    |  |    |
	//   |    |-1'   |  |    |  |    |
	//   |   _-_|----|-11_---|--15   |
	//   |_-' 7 |'-_ |    '-_|    '-_| 
	//  3-_     |   '-4------8-------12
	//     '-_  | _-'
	//        '-.'
	//          0
	//
	:shape=="chain"
	? concat( [ reverse(range(s))]            // starting face
			, [ range( nseg*s,(nseg+1)*s) ] // ending face
			, joinarr( [ for(c=range(nseg))
				 [ for(f=faces("cubesides", nside))
					[ for(i=f) i+c*nside]
				 ]
			  ])
			)  
                    
    //==================================================
	// ring 
 	/*  
        ring is a chain-like shape with the beg
        and end faces joined.
                    
        A ring with nseg=6, nside=4            
    
               13-------------9
             _-`|`-_______10-` `-_
          _-'   _-`|       |`-_   `-_ 
      17.'____.' -_|_____11|   `-6____.5
        |`-_  |`-_-`        `-_-`| _-`| 
        |   `-_ _-`-22____2_-`  _-`   | 
      16|_____|`-_/_|______|\_-` |7__ |4
         `-_   21|  |      | |1 `  _-`
            `-_  |  |23___3| |  _-`
               `-|-/________\|-`
                 20          0
    
        Using a faces("chain", nside=4, nseg=5) ### NOTE: nseg=5
        
        Lets see what we need, and reverse the process of getting it.
        
        We need joining faces, which are extra faces needed for 
        the closing link:
        
        fj= [ [ 0,1,21,20 ]
            , [ 1,2,22,21 ]
            , [ 2,3,23,22 ]
            , [ 3,0,20,23 ]
            ]
            
        We have a roll func: roll([0,1,2,3],-1)= [1,2,3,0]
            
        A transposed fj would be:
        
                               Tis column is where we start:
                               
        fjt=[ [  0, 1, 2, 3 ]  range(nside) = A  (head face)
            , [  1, 2, 3, 0 ]  roll( A ,-1)
            , [ 21,22,23,20 ]  roll( B ,-1)
            , [ 20,21,22,23 ]  range( (nseg-1)*nside, nseg*nside) = B
            ]                  B= bottom face
            
            
    */
    
    	
    : shape=="ring"?
     let( _tailface = range( (nseg)*nside, (nseg+1)*nside)
        , tailface = tailroll? roll( _tailface, tailroll):_tailface
        )
     concat( transpose(
               [ range(nside)
               , roll( range(nside), -1)
               , roll(tailface,-1) //roll( range( (nseg)*nside, (nseg+1)*nside),-1)
               , tailface //range( (nseg)*nside, (nseg+1)*nside)
               ] 
              )
            , joinarr( [ for(c=range(nseg))
				 [ for(f=faces("cubesides", nside))
					[ for(i=f) i+c*nside]
				 ]
			  ])
			)                      
    
                    //------------------------------------                
	:[]
);

//echofh( faces("tubeEnd", nside=4, nside_in=9));
         
//========================================================
// Return the first i that the angleAt(i) is not 180 deg
function ibend(pts, _i=1)=
(
   let( a = angle( sel(pts,[_i-1,_i,_i+1]) )
      )
   !is0(a-180) ? _i
   : _i== len(pts)-2? undef
   : ibend(pts, _i+1)
);
//pts= [[1,0,0],[2,0,0],[3,1,1]];
//echo( ibend(pts)); // 1
//
//pts1= [[1,0,0],[2,0,0],[3,0,0],[4,0,0],[10,1,1]];
//echo( ibend(pts1)); // 3
//
//pts2= [[1,0,0],[2,0,0],[3,0,0],[4,0,0],[5,0,0]];
//echo( ibend(pts2)); // undef           
           

//========================================================
function getBezierPtsAt(  // Return Cubic Bezier pts from 
                          // pts[i] to pts[i+1].
   pts
   , i //=1 
   , n //=6 // extra pts to be added 
   , closed //=false
   , ends  //= [true,true] to include both end pts
   , aH //= undef //[1,1]
   , dH  //= undef //[0.5,0.5]
   , opt=[]
   )=
(
 /*
 When pts is 4 pts:
    
 1) Make an abi pt, A1, on the PQR below
 2) From there, make a tangent pt, G, which
    is the HCP(Handle Control Point) of Q  
 3) Do the same HCP H on R. 
 4) Do a Cubic Bezier Curve calc on Q,R
    with G,H as their HCP, resp. 
     
            G--...__
    dH1=   :        ``H
     dQR  :   _.---._  : dH2 = dQR (default)
         :_-``       `-_:
        Q`---------------R       
       : \`'-_      _-'`/ :
      :   \   A1  A2   /   :
           \          /     : 
          X=P        S

   When pts is a 3-pointer, we insert a X
   as the 1st pt, which is a mirror of S. 
   
 The shape of resultant Bezier curve depends
 on aH and dH: 
 
 aH (angle of handler, aGQR and aQRH).
    aH is entered as a ratio of the default aGQR 
    or aQRH shown above. aH=1: default. Set aH 
    an array to set aH1 and aH2 separately.
    Deafult: [0.4,1]  ( len(pts)==3 )
             [     ]  ( len(pts)>3 )
             
 dH dQR. Again, enter as a ratio of dQR.
    dH = 0.5 (default) means, both dQG and dRH
    are half of dQR. dH= 
    Or a single ratio
    to set both the same
    Deafult: [0.5,0.3]  ( len(pts)==3 )
             [       ]  ( len(pts)>3 )
             
*/
    let(
    
     //------------------------------------------------
     
       opt = popt(opt) // handle sopt
       
     , pts = or(pts, hash( opt, "pts")) 
     , n   = or(n  , hash( opt, "n", 6))
     , i   = fidx(pts, isnum(i)?i:hash(opt,"i",i)) 
      , _aH = und( aH, hash(opt,"aH", 0.5) )
      , aH  = isarr(_aH)?_aH:[_aH,_aH]
      , _dH = und( dH, hash(opt,"dH", 0.38) )
      , dH  = isarr(_dH)?_dH:[_dH,_dH]
      , closed= und( closed, hash( opt, "closed", false) )
     
//     , _aH = aH==undef? hash(opt,"aH", 0.5):aH
//     , aH  = isarr(_aH)?_aH:[_aH,_aH]
//     , _dH = dH==undef? hash(opt,"dH", 0.38):dH
//     , dH  = isarr(_dH)?_dH:[_dH,_dH]
//     , closed= closed==undef? closed:hash( opt, "closed", false)
     , ends= or(ends, hash( opt, "ends", [true,true]))
     
     //------------------------------------------------
     
    , isend= i==len(pts)-1 
     
    , pqrs= 

        i==0?
             //   Q-----------R     at= 0, a1 be set = a2       
             //     a1     a2 |         
             //               S       
             [ for(i=[0])
                let( 
                     Q = pts[0]
                   , R = pts[1]
                   , S = pts[2] 
                   , P = closed? last(pts)
                         :anglePt( [ R,Q,S],a=angle([Q,R,S]))
                   )
                [P,Q,R,S]
            ][0] 
             
       : isend?
            // Q-----------R(0)   at= last angle 
            //  \ a1    a2  |     possible only when closed
            //   p         S(1)      
             
            // [ for(ii=[0])
                let( ii= len(pts)-1
                   , P = pts[ii-1]
                   , Q = pts[ii]
                   , R = pts[0] 
                   , S = pts[1]
                   )
                [P,Q,R,S]
            //][0]
       : i== len(pts)-2?
             
            //  Q-----------R   
            //   \ a1    a2     
            //    p         S(0)      
             
             //[ for(ii=[0])
                let( 
                     P = pts[i-1]
                   , Q = pts[i]
                   , R = pts[i+1] 
                   , S = closed? pts[0]
                         : anglePt( [ Q,R,P],a=angle([P,Q,R]))
                                
                   )
                [P,Q,R,S]
           // ][0]
       : sel(pts, [i-1,i,i+1,i+2])
    
    ,P = pqrs[0]
    ,Q = pqrs[1]
    ,R = pqrs[2]
    ,S = pqrs[3]
             
    ,dQR= dist(Q,R)
    ,Xq = onlinePt( [Q,P], len=-1)         
    ,Xr = onlinePt( [R,S], len=-1)         
    ,Hi = anglePt( [R,Q,Xq], a= (180-angle([R,Q,P]))*aH[0]
                          , len= dQR* dH[0] )
    ,Hj = anglePt( [Q,R,Xr], a= (180-angle([Q,R,S]))*aH[1]
                          , len= dQR* dH[1] )
             
   , bzpts = [ for (ii=range(n+2))
                let( r= ii/(n+1)
                  , P1= onlinePt( [Q,Hi], ratio= r )
                  , P2= onlinePt( [Hi,Hj], ratio= r ) 
                  , P3= onlinePt( [Hj,R], ratio= r )
                  , PP1= onlinePt( [P1,P2], ratio= r ) 
                  , PP2= onlinePt( [P2,P3], ratio= r ) 
                  )
                onlinePt( [PP1,PP2], ratio=r)
             ]
   ) // let   
/*
       Hi--...__
       :        ``Hj
   dH1:   _.---._  : dH2 = dRS/2 (default)
     :_-``       `-_:
    Q`---------------R       
   : \`'-_      _-'`/ :
  :   \   A1  A2   /   :
       \          /     : 
        X=P      S
*/
   ["bzpts", ends[0]&&ends[1]? bzpts
             :ends[0]? slice(bzpts,0,-1)
             :ends[1]? slice(bzpts,1)
             : slice(bzpts,1,-1)
   ,"pts", pts
   ,"Hi", Hi
   ,"Hj", Hj 
   ,"aH", aH
   ,"dH", dH
   ,"i",i
   ,"n",n
   ,"pqrs", [P,Q,R,S]
   ] 
   
//   [ //bzpts, 
//     ends[0]&&ends[1]? bzpts
//     :ends[0]? slice(bzpts,0,-1)
//     :ends[1]? slice(bzpts,1)
//     : slice(bzpts,1,-1)
//     
//   ,[P,Q,R,S]
//   ,[ [Hq, aH,dH], [Hr,aH,dH]]
//   ] 


);
function getBezierPtsAt_150629_working( 
   pts
   , at=1 
   , n=6 // extra pts to be added 
   , closed=false
   , ends= [true,true]
   , aH= undef //[1,1]
   , dH= undef //[0.5,0.5]
   )=
(
 /*
 When pts is 4 pts:
    
 1) Make an abi pt, A1, on the PQR below
 2) From there, make a tangent pt, G, which
    is the HCP(Handle Control Point) of Q  
 3) Do the same HCP H on R. 
 4) Do a Cubic Bezier Curve calc on Q,R
    with G,H as their HCP, resp. 
     
            G--...__
    dH1=   :        ``H
     dQR  :   _.---._  : dH2 = dQR (default)
         :_-``       `-_:
        Q`---------------R       
       : \`'-_      _-'`/ :
      :   \   A1  A2   /   :
           \          /     : 
          X=P        S

   When pts is a 3-pointer, we insert a X
   as the 1st pt, which is a mirror of S. 
   
 The shape of resultant Bezier curve depends
 on aH and dH: 
 
 aH (angle of handler, aGQR and aQRH).
    aH is entered as a ratio of the default aGQR 
    or aQRH shown above. aH=1: default. Set aH 
    an array to set aH1 and aH2 separately.
    Deafult: [0.4,1]  ( len(pts)==3 )
             [     ]  ( len(pts)>3 )
             
 dH dQR. Again, enter as a ratio of dQR.
    dH = 0.5 (default) means, both dQG and dRH
    are half of dQR. dH= 
    Or a single ratio
    to set both the same
    Deafult: [0.5,0.3]  ( len(pts)==3 )
             [       ]  ( len(pts)>3 )
             
*/
    let( 
     // Make a temp X if pts is 3-ptr. This X is a
     // mirror of pts[2] and will be pts[0]
     //isL3 = len(pts)==3

     //,aQRS = angleAt( pts, isbeg==0? 1:2 )
     at = fidx(pts,at)
     , atend= at==len(pts)-1 
//       , aH= aH==undef?( at==0?[0.6,0.95]:atend?[0.95,0.6]:[0.95,0.95] )
//             :isarr(aH)?aH:[aH,aH]
//       , dH= dH==undef?( at==0?[0.45,0.3]:atend?[0.3,0.45]:[0.35,0.35] )
//             :isarr(dH)?dH:[dH,dH]
       , aH= aH==undef?[0.5,0.5]
             :isarr(aH)?aH:[aH,aH]
       , dH= dH==undef?[0.38,0.38]
             :isarr(dH)?dH:[dH,dH]
     
     , pqrs= 

        at==0?
             //   Q-----------R     at= 0, a1 be set = a2       
             //     a1     a2 |         
             //               S       
             [ for(i=[0])
                let( 
                     Q = pts[0]
                   , R = pts[1]
                   , S = pts[2] 
                   , P = closed? last(pts)
                         :anglePt( [ R,Q,S],a=angle([Q,R,S]))
                   )
                [P,Q,R,S]
            ][0] 
             
       : atend?
            // Q-----------R(0)   at= last angle 
            //  \ a1    a2  |     possible only when closed
            //   p         S(1)      
             
             [ for(i=[0])
                let( 
                     P = pts[at-1]
                   , Q = pts[at]
                   , R = pts[0] 
                   , S = pts[1]
                   )
                [P,Q,R,S]
            ][0]
       : at== len(pts)-2?
             
            //  Q-----------R   
            //   \ a1    a2     
            //    p         S(0)      
             
             [ for(i=[0])
                let( 
                     P = pts[at-1]
                   , Q = pts[at]
                   , R = pts[at+1] 
                   , S = closed? pts[0]
                         : anglePt( [ Q,R,P],a=angle([P,Q,R]))
                                
                   )
                [P,Q,R,S]
            ][0]
       : sel(pts, [at-1,at,at+1,at+2])
    
    ,P = pqrs[0]
    ,Q = pqrs[1]
    ,R = pqrs[2]
    ,S = pqrs[3]
             
    ,dQR= dist(Q,R)
    ,Xq = onlinePt( [Q,P], len=-1)         
    ,Xr = onlinePt( [R,S], len=-1)         
    ,Hq = anglePt( [R,Q,Xq], a= (180-angle([R,Q,P]))*aH[0]
                          , len= dQR* dH[0] )
    ,Hr = anglePt( [Q,R,Xr], a= (180-angle([Q,R,S]))*aH[1]
                          , len= dQR* dH[1] )
             
   , bzpts = [ for (i=range(n+2))
                let( r= i/(n+1)
                  , P1= onlinePt( [Q,Hq], ratio= r )
                  , P2= onlinePt( [Hq,Hr], ratio= r ) 
                  , P3= onlinePt( [Hr,R], ratio= r )
                  , PP1= onlinePt( [P1,P2], ratio= r ) 
                  , PP2= onlinePt( [P2,P3], ratio= r ) 
                  )
                onlinePt( [PP1,PP2], ratio=r)
             ]
   ) // let   
/*
        G--...__
       :        ``H
   dH1:   _.---._  : dH2 = dRS/2 (default)
     :_-``       `-_:
    Q`---------------R       
   : \`'-_      _-'`/ :
  :   \   A1  A2   /   :
       \          /     : 
        X=P      S
*/
    //[Hp,Hq]  
    //pqrs 
   [ //bzpts, 
     ends[0]&&ends[1]? bzpts
     :ends[0]? slice(bzpts,0,-1)
     :ends[1]? slice(bzpts,1)
     : slice(bzpts,1,-1)
     
   ,[P,Q,R,S]
   ,[ [Hq, aH,dH], [Hr,aH,dH]]
   ] 


);

//========================================================
function getBezier_simple( pq,rs, n=6)=
(
   /* Return a list of pts between P and Q (include P,Q)
      representing the Cubic Bezier Curve. 
      The R,S in st are the Handle Control Points
      for P,Q, respectively. 
      
      n: number of points filled in between P and Q
      Return: [P, p1,p2...pn, Q]
      
          R--.._
         :      ``'-S
        :   _.---.  |  
       :_-``      `.|
      P`            Q
      
      Ref: 
      
      1. Cubic Bezier Curves - Under the Hood
      https://vimeo.com/106757336
      
      2. A Primer on Bezier Curves
      http://pomax.github.io/bezierinfo 
   */
   let( pqrs= rs==undef? 
               ( len(pq)==3? [pq[0],pq[2],pq[1],pq[1]]
                 : pq )
              //: typeplp(pq)=="pl"? app(pq,rs)
              : gtype(pq)=="pl"? app(pq,rs)
              : concat( pq,rs)
      , P=pqrs[0]   
      , Q=pqrs[1]
      , R=pqrs[2]
      , S=pqrs[3]
      , bz= [for (i=range(n+2))
               let( r= i/(n+1)
                  , P1= onlinePt( [P,R], ratio= r )
                  , P2= onlinePt( [R,S], ratio= r ) 
                  , P3= onlinePt( [S,Q], ratio= r )
                  , PP1= onlinePt( [P1,P2], ratio= r ) 
                  , PP2= onlinePt( [P2,P3], ratio= r ) 
                  )
               onlinePt( [PP1,PP2], ratio=r)
           ]
      )
   bz
);

//========================================================

function getSideFaces(topIndices
                    , botIndices
                    , clockwise=true)=
let( ts= topIndices
   , bs= botIndices
   , tlast = last(ts)
   , blast = last(bs)
   )
(
  clockwise
  ? [for (i=range(ts))
      i==0
      ? [ ts[0], tlast, blast, bs[0] ]
      : [ ts[i], ts[i-1], bs[i-1], bs[i] ]
     ]
  : [for (i=range(ts))
      i<len(ts)-1
      ? [ ts[i], ts[i+1], bs[i+1], bs[i] ]
      : [ tlast, ts[0], bs[0], blast ]
      ]
);
     
//========================================================
getTubeSideFaces=["getTubeSideFaces","indexSlices, clockwise=true","array", "faces"
, "Given an array of indexSlices (an indexSlice
;; is a list of indices representing the order 
;; of points for a circle), return an array of 
;; faces for running polyhedon(). Typically
;; usage is in a column or tube of multiple
;; segments. For example, CurvedCone.
;;
;; See getSideFaces that is for tube of only
;; one segment. 
"];  
function getTubeSideFaces(
         indexSlices, clockwise=true)=  
  let( ii = indexSlices )
( 
    //mergeA( 
      [ for (i=range(len(ii)-1))
        getSideFaces( topIndices= ii[i]
                    , botIndices= ii[i+1]   
                    , clockwise=clockwise
                    )
      ]
    //)
);
 
//========================================================

function incenterPt(pqr)=
(
	intsec( [ pqr[0], angleBisectPt( sel(pqr, [1,0,2]) ) ]
	      , [ pqr[1], angleBisectPt( pqr ) ]
		  )
//	lineCrossPt( pqr[0], angleBisectPt( rearr(pqr, [1,0,2]) )
//			  , pqr[1], angleBisectPt( pqr )
//			  )
);

//  ln-ln: http://math.stackexchange.com/questions/270767/find-intersection-of-two-3d-lines

//========================================================
function intsec( a,b )= // intersection, return: pt|ln|undef
(    
   let( 
         c= b==undef?a[0]:a
       , d= b==undef?a[1]:b
       , t= str(gtype(c), gtype(d)) //typeplp(c),typeplp(d))  
            /* 
              t==lnln
                      K    J        _-`
                    --+----+-------M`-----R----S
                      |    |   _-` 
                      |    |_-` m 
                      |  _-P   
                      Q-`
                 m/PQ =  PJ/(QK-PJ)
                 m  = PQ*PJ/(QK-PJ)
                 m=  dPQ*dPrs/( dQrs-dPrs)
                 M = onlinePt( [P,Q], len=-m) 
                 
                                   Q
                      J         _-`|
                    --+-------M`---K---R----S
                      |   _-` 
                      |_-` m 
                    _-P
                     
                 m = PQ*PJ/(QK+PJ)    
            */
       , dPrs= t=="lnln"? dist( c[0], d ):undef
       , dQrs= t=="lnln"? dist( c[1], d ):undef
       , lnPtSign = t=="lnln"?
                   (isSameSide( c
                   , [d[0],d[1], N( concat(d,[c[0]]))] )?-1:1
                   ) : undef
       , m= t=="lnln"? dist(c)*dPrs/( dQrs+ lnPtSign*dPrs) :undef
   )
   isparal(c,d)? undef
  :t=="lnln"? (!is0(dist(c,d))?undef: onlinePt( c, len= lnPtSign*m))
  :t=="lnpl"? intsec_lnpl( c, d )
  :t=="plln"? intsec_lnpl( d, c )
  :t=="plpl"? undef // under construction
  : undef  
);

//========================================================

//function lineCrossPt( P,Q,M,N )=
//    let( pqmn = N? [P,Q,M,N]      // lineCrossPt( P,Q,M,N )
//                :Q? concat( P,Q ) // lineCrossPt( pq, mn )
//                :len(P)==2?[ P[0][0],P[0][1],P[1][0],P[1][1] ]
//                :P               // lineCrossPt( pqmn )
//       , P = pqmn[0]
//       , Q = pqmn[1]
//       , M = pqmn[2]
//       , N = pqmn[3]
//       , err= !isOnPlane( [P,Q,M],N) 
//       )
//(
//// Ref: Paul Burke tutorial 
////   http://paulbourke.net/geometry/pointlineplane/
//// and his code in c :
////   http://paulbourke.net/geometry/pointlineplane/lineline.c
//                   
////	P + (Q-P) * ( ((M-P)*(M-N))* ((M-N)*(P-Q)) - ((M-P)*(P-Q))*((M-N)*(M-N)))
//// 	 		 / ( ((P-Q)*(P-Q))* ((M-N)*(M-N)) - ((M-N)*(P-Q))*((M-N)*(P-Q)))
//               
//	err?_red(str( "Error in lineCrossPt: 2 lines are not on same plane. line1: "
//           , [P,Q], ", line2: ", [M,N] ))
//    :P + (Q-P) * ( dot(M-P,M-N)* dot(M-N,P-Q) - dot(M-P,P-Q)* dot(M-N,M-N) )
// 	 		 / ( dot(P-Q,P-Q)* dot(M-N,M-N) - dot(M-N,P-Q)* dot(M-N,P-Q) )
//);

//========================================================

function linePts(pq,len=undef, ratio=[1,1])=
 let( len=isnum(len)?[len,len]:len
	, ratio=isnum(ratio)?[ratio,ratio]:ratio
	)
( 
   [ onlinePt( pq,  len= -len[0], ratio=-ratio[0])
   , onlinePt( p10(pq), len= -len[1], ratio=-ratio[1])
   ]
);		



//function lncoef(P,Q)=
//(
//    // good site: http://geomalgorithms.com/a02-_lines.html
//    
//    let( pq = Q==undef?P:[P,Q]
//       , P = pq[0], Q= pq[1]
//       )
//    [ Q.
//
//); 

//========================================================

function longside(a,b)= sqrt(pow(a,2)+pow(b,2));



//========================================================

function intsec_lnpl(pq, tuv)=
 let( P=pq[0]
	, Q=pq[1]
	, L= dist(pq)
	, dp = distPtPl(P, tuv)
	, dq = distPtPl(Q, tuv)
	, sameside = sign(dp)==sign(dq)
	, ratio = sameside ?  dp/(dp-dq)
					 :abs(dp)/(abs(dp)+abs(dq))
	)
(
	onlinePt( pq, ratio = ratio)
);

//========================================================

function midPt(PQ)= (PQ[1]-PQ[0])/2 + PQ[0];

//========================================================

function normal(pqr, len=undef)=
(  
   let( n = cross( pqr[0]-pqr[1], pqr[2]-pqr[1] ) )
   len? onlinePt( [ORIGIN, n], len=len) : n
//	isnum(len)? onlinePt( [ORIGIN, cross( pqr[0]-pqr[1], pqr[2]-pqr[1] )], len=len)
//	:cross( pqr[0]-pqr[1], pqr[2]-pqr[1] )
);

//========================================================

function normalLn( pqr, len=undef, sign=1)=
  let( Q=pqr[1], N=N(pqr) 
     )
(
   sign==1 && len==undef? [Q,N] : onlinePt( [Q,N], len=sign*(len?len:norm(N-Q)) ) 
);   

//========================================================
normalPt=["normalPt", "pqr,len,reversed", "point", "Point",
 " Given a 3-pointer (pqr), optional len and reversed, return a 
;; point representing the normal of pqr."
];
function normalPt(pqr, len=1, reversed=false)=
(
	len==undef? (reversed?-1:1)*normal(pqr)+pqr[1]
			  :onlinePt( [ pqr[1]
                      , (reversed?-1:1)*normal(pqr)+pqr[1]
                       ]
                      , len= len*mm )
);

		


//========================================================
function normalPts(pts, len=1)=
 let(tripts= subarr(pts,  cycle=true)
	)
( 
	[for(pqr=tripts) normalPt(pqr,len=len)]
);

//========================================================


function onlinePt(pq, len=undef, ratio=0.5)=
(   let(L = isnum(len)?mm*len/dist(pq):ratio)
	[ L*dist(pq,"x")+pq[0].x
	, L*dist(pq,"y")+pq[0].y 
	, L*dist(pq,"z")+pq[0].z ] 
);


//========================================================

function onlinePts(pq, lens=false, ratios=false)=
(
	ratios==false?
	( len(lens)==0?[]
	  :	[for(l=lens) onlinePt( pq, len=l)]
//        concat( [onlinePt( pq, len=lens[0])]
//		  , onlinePts( pq, lens=shrink(lens) )
//		  )
	):
	( len(ratios)==0?[]
      : [for(r=ratios) onlinePt(pq,ratio=r)]
//	  :	concat( [onlinePt( pq, ratio=ratios[0])]
//		  , onlinePts( pq, ratios= shrink(ratios) )
//		  )
	)
);
      


//========================================================

function othoPt( pqr )= 
(
/*  Return [x,y] of D
           Q          ___
           ^\          :
          /: \         :   
       a / :  \_  b    : h
        /  :    \_     :
       /   :-+    \    :    
     P-----D-!------R ---

	|------ c ------|  
        
	a^2 = h^2+ PD^2
	b^2 = h^2+ DR^2  
	a^2- PD^2 = b^2-(c-PD)^2
			  = b^2-( c^2- 2cPD+PD^2 )
			  = b^2- c^2 + 2cPD -PD^2
	a^2 = b^2-c^2+ 2cPD
	PD = (a^2-b^2+c^2)/2c
	ratio = PD/c = (a^2-b^2+c^2)/2c^2    
*/
    let( c2= pw(d02(pqr)) )
	onlinePt( [pqr[0],pqr[2]]
	        //, ratio=( d01pow2(pqr) - d12pow2(pqr) + d02pow2(pqr))/2/d02pow2(pqr) 
	        , ratio=(   pw(d01(pqr)) - pw(d12(pqr)) + c2)/2/c2
            
	)
);

//========================================================
function planeDataAt( 

        pts
        , i
        , Rf  // Ref pt to decides the first pt of pl (pl[0]) starts
        , rot // Rot about seg (i-1,i)
        , a      // = angle [ pts[i-1], pts[i], Rf ]
        , a2next // = angle [ pts[i+1], pts[i], Rf ]
                 //   This is the same as a2R in planeDataAtQ()
                 //   If this is defined, a is ignored. 
        , rot2   // The 2nd rot to rot the pl (after angle is applied)
                 //   about pl's normal 
        , closed
        )=
(
    let( //Rf = ispt(Rf)? Rf 
         //     : len(pts)==2? randofflnPt( pts) : undef
       //, ai = angle( p3 )
       //, 
       pldata= 
       
            //--------------------------------------------------
            i==0?
                /* For i==0, only a is allowed (a2next is ignored). 
                   This a will be the a2R
                
                    a  closed |  a2R for planeDataAtQ()
                    ----------+---------
                    -    -    |  90
                    num  -    |  a 
                    -    true |  und
                    num  true |  a 
                */
                let( p3= len(pts)==2? 
                            [ onlinePt( pts, -1), pts[0], pts[1] ] 
                            : get3(pts,0)
                   , a= isnum(a)? a
                        : !closed? 90:undef
                   , pldata= planeDataAtQ( p3
                                        , rot=-rot
                                        , a2R= a 
                                        , rot2=-rot2
                                        , Rf=Rf )
                   )                 
               concat( pldata, ["pts", pts,"i",i, "p3", p3 ] )            
                 
            //--------------------------------------------------
            :0<i && i < len(pts)-1 ?
            
                let( p3 = get3(pts, i) )
                concat( 
                        planeDataAtQ( pqr = p3
                                    , rot=rot, a=a, a2R=a2next
                                    , rot2=rot2, Rf=Rf )
                                    
                      , ["pts", pts,"i",i, "p3", p3 ] 
                      )            

            //-------------------------------------------- i = last
            :   /* For i==last, a2next is ignored. 
                
                    a  closed |  a for planeDataAtQ()
                    ----------+-----------
                    -    -    |  90
                    num  -    |  a 
                    -    true |  und
                    num  true |  a 
                */    
                let( p3= len(pts)==2? 
                            [ pts[0], pts[1], onlinePt( p10(pts), -1) ] 
                            : get3(pts,i)
                   , a= isnum(a)? a
                        : !closed? 90:undef
                   , Rf= Rf? Rf 
                         : closed?undef
                         : iscoln( get3(pts, -2))?  
                           randofflnPt( get(pts, [-1,-2]))
                           : get(pts, -3)
                   , pldata= planeDataAtQ( p3
                                         , rot=rot, a=a, rot2=rot2
                                         , Rf=Rf )
                   )
                   concat( pldata, ["pts", pts,"i",i, "p3", p3 ] )     
    )// let
  
    pldata//concat( pldata, ["pts", pts,"i",i, "p3", p3 ] )
  
);


//========================================================
function planeDataAtQ( // Rtn a plane (3-pointer) at Q
              pqr 
             ,rot=0     // angle of pl rotation about PQ (PQ is normal to pl) 
             ,a =undef  // angle of pl bending toward P
             ,a2R=undef // angle of pl bending toward R. If this is set
                        // a is ignored 
             ,rot2=0    // Rotate about the normal of newly created pl
             ,Rf = undef // Ref Pt to indicate the starting dir of pl (i.e., pl[0]) 
             )= 
(   
       /* 
          Rotate R to Rx by a 
          
                      N
                      |     _ Rx
                      |  _-`.`
              - - - - Q`- .:- - - - M 
                     / `.:    
                    / .:   `-_ 
                   /.: a      `-_
                  J- - - - - - -R    
                 /
                P 
          
         More Rx to Rxx such that a_PQRxx = a
         
                Rx
                /     Rxx
               /    _-`
              /  _-`
             /_-` a
            Q`-----------------P
       
        So, if rot=a=90, the result is a plane 90d to PQ
        
       */
       
    !ispts(pqr)? assert( unit="planeDataAtQ"
                      , var="pqr"
                      , val= pqr
                      , check= "gtype"
                      , want= "pts"
                      , pass = false
                      )          
    :( // validated             
    
    let( aPQR= angle(pqr)
       , iscoln = isequal(aPQR,180) || isequal(aPQR,0)
       , p01 = p01(pqr)
       , p12 = p12(pqr)
       , isa2R = isnum(a2R)
       
       // This Rf decides which dir to start pl (i.e., pl[0])
       //   on the pl surface
       , Rf = Rf?Rf: 
               let( Rf0= iscoln? randofflnPt(p01)
                               : pqr[ isa2R?0:2 ]
                               )
               onlinePt( [ pqr[1], Rf0], dist(p12)) 
       
        // p[0] on the pl after rot
       , P0rot= rot? rotPt( Rf, isa2R?p12:p01, rot ): Rf
       
       // Nrot will be in the final pl, no matter what a and rot2 are
       , Nrot = N( [ pqr[ isa2R?2:0], pqr[1], P0rot ] )
       
       , a = a==undef? ( iscoln || rot ?90
                         : angle(pqr)/2
                         ): a
       , P0a = isa2R? anglePt( [pqr[2], pqr[1], P0rot], a=a2R, len= dist(p12))
               : a  ? anglePt( [pqr[0], pqr[1], P0rot], a=a, len= dist(p12))
               : P0rot

       // axrot2: the rot ax for rot2
       , axrot2 = [ pqr[1], N( [Nrot, pqr[1],P0a] )] 
       
       , P0rot2 = rot2? rotPt( P0a, axrot2, rot2 )
                   : P0a
       , Nrot2 = rot2? rotPt( Nrot, axrot2, rot2 )
                   : Nrot
       , pl= [ P0rot2, pqr[1], Nrot2 ]
       )
    [ "pl", pl
    , "pqr", pqr
    , "aPQR", aPQR
    , "rot",rot
    , "a",a
    , "a2R", a2R
    , "rot2",rot2
    , "Rf",Rf
    , "P0rot",P0rot
    , "Nrot", Nrot
    , "P0a",P0a
    , "axrot2", axrot2
    , "P0rot2",P0rot2
    , "iscoln", iscoln
//    , "isa2R", isa2R
//    , "aPQpl0", angle( [pqr[0], pqr[1], pl[0]] )
//    , "aRQpl0", angle( [pqr[2], pqr[1], P0a] )
    
    ]
    )//___validated


//    :( // validated             
//    let( aPQR= angle(pqr)
//       , iscoln = aPQR==180 || aPQR==0
//       , rot = rot==undef?0:rot
//       , a = a==undef? ( iscoln || rot ?90
//                         : angle(pqr)/2
//                         ): a
//       , Rrot1 = iscoln? pqr[2] //onlinePt( p10(pqr), -1)
//                    : rot==0? pqr[2]: rotPt( pqr[2], p01(pqr), rot )
//       , Ra = iscoln?
//               ( let( Rf = Rf?Rf:randofflnPt(p01(pqr)) )
//                 anglePt( [pqr[0],pqr[1],Rf],a)
//               )
//               : anglePt( [pqr[0], pqr[1], Rrot1], a, len= dist(p12(pqr)) )
//       , Rrot2= isnum(rot2)? rotPt( Ra, p01(pqr), rot2): Ra         
//       , N  = N( [ pqr[0], pqr[1], Rf?Rf:Rrot2 ] )
//       , pl= [ Rrot2, pqr[1], N ]
//       )
//    [ "pl", pl
//    , "aPQR", aPQR
//    , "rot",rot
//    , "a",a
//    , "Rf",Rf
//    , "Rrot1",Rrot1
//    , "Ra",Ra
//    , "Rrot2",Rrot2
//    , "iscoln", iscoln
//    ]
//    )//___validated
);



//========================================================

function planecoefs(pqr, has_k=true)=
(
	/*
	https://answers.yahoo.com/question/index?qid=20110121141727AAncAu4

	Using normal vector <5, 6, -3> and point (2, 4, -3), we get equation of plane: 
	5 (x-2) + 6 (y-4) - 3 (z+3) = 0 
	5x - 10 + 6y - 24 - 3z - 9 = 0 
	5x + 6y - 3z = 43

	normal = [a,b,c]
	k = -(ax+by+c)

	*/	
	concat( normal(pqr) 
		  , has_k?[-( normal(pqr)[0]* pqr[0][0]
			 + normal(pqr)[1]* pqr[0][1]
			 + normal(pqr)[2]* pqr[0][2]
             )]:[]			 
		)	
);

//========================================================
function plat(pts,i)= // plane at i that is 90deg to axis before i
(
    let( Rf = randPt()
       , Ni = i==0? N( [pts[1],pts[0],Rf] )
              : N( [pts[0],pts[1],Rf] ) 
       , Mi = i==0? N2( [pts[1],pts[0],Rf] )
              : N2( [pts[0],pts[1],Rf] ) 
       )
    [pts[i],Ni,Mi]
);

//========================================================
function pqr(a=undef, p=undef,r=undef)=
(   let(pqr=randPts(3)
       , P0=pqr[0], Q=pqr[1], R0=pqr[2]
       , dp= or(p, dist(Q,P0))
       , dr= or(r, dist(Q,R0))
       , P = onlinePt( [Q,P0], len=dp)
       )
    isnum(a)? [P, Q, anglePt( pqr, a, len=dr)]
    : [ P, Q, onlinePt( [Q,R0], len=dr) ]  
);  
//echo( pqr(p=1,r=2,a=90));
    
//========================================================
    
function pqr90(pqr, d=3,x,y,z,seed)= 
    let( pqr=pqr==undef?randPts(3,d,x,y,z,seed):pqr)   
(    
    [ pqr[0], pqr[1], anglePt(pqr,90) ]
);
    
//========================================================
function projPt( a,b, along )=
(    
   let( c= b==undef?a[0]:a
   , d= b==undef?a[1]:b
   , t= str(gtype(c),gtype(d))  // ptln,lnpt, plpt,ptpl, lnpl,plln 
   , v= isln(along)?(along[1]-along[0]):undef
   )
   v?
   (
       t=="ptln"? intsec( [c,c+v], d )
      :t=="lnpt"? intsec( c, [d,d+v] )
      :t=="ptpl"? intsec( [c,c+v], d )
      :t=="plpt"? intsec( c, [d,d+v] )
      :t=="lnpl"? [ intsec( [ c[0], c[0]+v ], d )
                  , intsec( [ c[1], c[1]+v ], d ) ]
      :t=="plln"? [ intsec( c, [ d[0], d[0]+v ] )
                  , intsec( c, [ d[1], d[1]+v ] ) ] 
      : undef      
   
   ):(
   
       t=="ptln"? othoPt( [d[0],c,d[1]] )
      :t=="lnpt"? othoPt( [c[0],d,c[1]] )
      :t=="ptpl"? c- dot( c-d[1], (normal(d)/norm(normal(d)))
                        )*(normal(d)/norm(normal(d)))
      :t=="plpt"? d- dot( d-c[1], (normal(c)/norm(normal(c)))
                        )*(normal(c)/norm(normal(c)))
      :t=="lnpl"? [projPt( c[0], d), projPt( c[1], d) ]
      :t=="plln"? [projPt( d[0], c), projPt( d[1], c) ]
      : undef
  
  )  
	/*    http://stackoverflow.com/questions/8942950/how-do-i-find-the-orthogonal-projection-of-a-point-onto-a-plane

    The projection of a point q = (x, y, z) onto a plane 
       given by a point p = (a, b, c) and a normal n = (d, e, f) is

    q_proj = q - dot(q - p, n) * n

	n = (normal(pqr)/norm(normal(pqr)))
	
	pt - dot(pt-pqr[1], (normal(pqr)/norm(normal(pqr))))*(normal(pqr)/norm(normal(pqr)))
   */ 
);    

//========================================================

function projPts( pts,target, along )=  // pts proj to target 
(
   [for(p=pts) projPt(p,target,along) ]   
);


//========================================================
quadrant=["quadrant", "pqr,s", "int", "Geometry",
 " Given a 3-pointer (pqr) and a point, return the quadrant of S on the 
;; yz plane in a coordinate system where P is on the positive X and Q 
;; is the origin. 
;;
;;       
;;       6
;;  +----N-----+
;;  : 2  |  1  :
;;  :    |     :
;; 7-----P-----R- 5
;;  :    :     :
;;  : 3  :  4  :
;;  +----:-----+ 
;;       8                 R
;;                        /|  
;;                       / |
;;                      /  |
;;                     /   |
;;                    /    |
;;        S.---------/--T  |
;;        |:        /   |  |   _.'
;;        | :      /    |  :.-'
;;        U.-:----/-----+-'
;;          '.:  /  _.-'
;;            ':/.-'
;;   N---------Q---------
;;       _.-' 
;;    P-'
;;
;; quadrant(pqr,S) => 1
;;
"];

function quadrant(pqr,s)=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, N = N(pqr)
	, dST = distPtPl( s, pqr )
	, dSU = distPtPl( s, [N,Q,P] )
	, signs = [ -sign( dST ), sign( dSU )]
	)
(
	 hash( [[1,1],1
			,[-1,1],2
			,[-1,-1],3
			,[1,-1],4
			,[0,0],0
			,[0,1], 6
			,[-1,0], 7
			,[0,-1], 8
			,[1,0],  5
			], signs )
);
 

//========================================================
function randchain(
       n //=3, 
     , a //=[-360,360]
     , a2 //=[-360,360]
     , seglen //= [0.5,3] // rand len btw two values
     , lens //=undef
     , seed //=undef // base pqr
     , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
                     //   , "closed",false,details=false]
//     , d //=3
//     , x //=undef
//     , y //=undef
//     , z // =undef
     , opt=[]
     , _rtn=[] // for noUturn
     , _smsegs=[] // segments of smoothed pts
     ,_i=0 // for noUturn
     ,_debug
    )
=(
//  !n? _red(str( "ERROR: the n(# of pts) given to randchain() is invalid: ",n ))
//  :(
    _i==0? //####################################### i==0
    
    let( 
        //=========[ initiation ] =======

          opt = popt(opt) // handle sopt
        //, n = hash(opt,"n", n)
        , n = n>1?n: hash(opt,"n",2)
        , a = und(a, hash(opt,"a", [170,190])) // = aPQR
        , a2= und(a2,hash(opt,"a2",[-45,45])) // = aRQN
        , seglen= or(seglen, hash(opt, "seglen"))
        , _lens = or(lens, hash(opt, "lens"))
        , seed = or(seed, hash(opt, "seed", randPts(3)))
        , smooth= und(smooth, hash(opt, "smooth"))

        //==============================

        ,lens = // If lens < n-1, repeat lens        
           _lens && isarr(_lens) && len(_lens)< n-1?
             repeat(_lens, ceil((n-1)/len(_lens)))
            : _lens
            
//        , L = isarr(lens)? lens[_i-1]
//            : isarr(seglen)? rand(seglen[0],seglen[1]) 
//            : isnum(seglen)? seglen
//            : rand(0.5,3)
        , pt= seed? seed[0]:randPt() //d,x,y,z)
        , newrtn = [pt]  
              
     )//let @_i==0
     
     randchain(n=n,a=a,a2=a2,seglen=seglen, lens=lens
              , seed = seed
              //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1
              , smooth=smooth, _debug=_debug)   
              
   :let(  // ###################################### i > 0
   
     isend= _i==n-1
     ,_a = isarr(a)? rand(a[0],a[1]):a
     ,_a2= isarr(a2)? rand(a2[0],a2[1]):a2
     ,L = isarr(lens)? lens[_i-1]
          : isarr(seglen)? rand(seglen[0],seglen[1]) 
          : isnum(seglen)? seglen
          : rand(0.5,3)
     ,pt= _i==1? (seed? onlinePt( p01(seed), len=L)
                      :onlinePt( [last(_rtn)
                               , randPt()],len=L)
                  )    
          :  let( P = get(_rtn,-2)
                  , Q = get(_rtn,-1)
                  , Rf = _i==2 && seed? seed[2] //onlinePt( p12(seed), len=L)
                         : iscoln( sel(_rtn, [-3,-2,-1]))
                            ?randofflnPt( get(_rtn,[-2,-1]))
                           : sel(_rtn,-3)
                  )
               anglePt( [P,Q,Rf], a=_a, a2=_a2, len=L)   
                       
     ,newrtn = app(_rtn, pt ) 
     , smpts = isend?
              ( smooth==true? smoothPts(newrtn)
                :smooth? smoothPts(newrtn, opt=smooth) 
//                   smoothPts( newrtn, n=hash(smooth, "n")
//                            , aH=hash(smooth,"aH")
//                            , dH=hash(smooth,"dH")
//                            , closed= hash(smooth, "closed")
//                            , details= hash(smooth, "details")
//                       )
                       
                   //n=10, aH=0.5, dH=0.3, closed=false, details=false
                  :newrtn
              ):[]
          
//     ,_debug= str(_debug, "<br/>"
//                 ,"<br/>, <b>_i</b>= ", _i
//                 ,"<br/>, <b>len</b>= ", len
//                 ,"<br/>, <b>L</b>= ", L
//                 ,"<br/>, <b>_rtn</b>= ", _rtn
//                 ,"<br/>, <b>newrtn</b>= ", newrtn
//                 ,"<br/>, <b>actual len</b>= ", dist( sel(newrtn,[-1,-2]))
//                 ,"<br/>, <b>pt</b>= ", pt
//                 )
     )//let

  //isend? (smooth? [newrtn,smpts]: newrtn ) 
  isend? 
    [ "pts", smpts
    , "unsmoothed", newrtn
    , "smooth", smooth
    , "seed", seed
    , "lens",lens
    , "seglen", seglen
    , "L",L
    , "a",a
    , "a2", a2
    , "debug",_debug
    ]
    // (smooth? [newrtn,smpts]: newrtn ) 
  
  : randchain(n=n,a=a,a2=a2,seglen=seglen, lens=lens, seed = seed
             //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1, smooth=smooth, _debug=_debug)
  //)// if_n
);


         
//========================================================
function randofflnPt(pq,d=3,x,y,z)=
(
    let(pt= randPt(d=d,x=x,y=y,z=z))
    iscoln( pq, pt )? randofflnPt(pq,d=3,x,y,z)
                    : pt
);
//echo( randofflnPt( randPts(2) ) );

////========================================================
function randPts(count=3, d=3, x,y,z, seed)=
(
  [ for(i=range(count)) randPt(d,x,y,z,seed) ]
);


//========================================================

function randInPlanePt(pqr)=
(
	[onlinePt( 
			[pqr[0], onlinePt([pqr[1],pqr[2]], ratio=rand())]
		   , ratio=rand()
		)
	,onlinePt( 
			[pqr[1], onlinePt([pqr[2],pqr[0]], ratio=rand())]
		   , ratio=rand()
		)
	,onlinePt( 
			[pqr[2], onlinePt([pqr[0],pqr[1]], ratio=rand())]
		   , ratio=rand()
		)
	][ floor(rand(3))]
);



//========================================================

function randInPlanePts(pqr, count=2)=
(
   count>0? concat( [randInPlanePt( pqr )]	
                  , randInPlanePts( pqr, count-1)
                  ) :[]
);

//========================================================

//
function randOnPlanePt(pqr=randPts(3), r=undef, a=360)=
(
//  onlinePt( [pqr[floor(rand(3))], randInPlanePt(pqr)], ratio= rand(3) )
   anglePt( pqr
//		 [  pqr[0], incenterPt(pqr)
//		   ,pqr[1]
//		   ]
		  , a = isnum(a)?(rand(1)*a)
					: rands( a[0], a[1],1)[0]
			,r = r==undef? max( d01(pqr), d02(pqr), d12(pqr) ) * rand(1)
				:isarr(r)? rands( r[0], r[1],1)[0]
				:r
		)	
);



//========================================================

function randOnPlanePts(count=2, pqr=randPts(3), r=undef, a=360 )=
(
   count>0? concat( [randOnPlanePt( r=r, pqr, a=a )]	
                  , randOnPlanePts( count-1, pqr, r=r,a=a )
                  ) :[]
);



//========================================================
function randRightAnglePts( 
    r=rand(5), 
    dist=randPt(), 
    r2=-1
    )=
( 
    RMs( [ rand(360), rand(360), rand(360)] 
	   , //shuffle(
			[
			 [      r,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
			,[      0,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
			,[      0,r2>0?r2:rand(r), 0]+(isnum(dist)?[dist,dist,dist]:dist) 
		 	]
		//)
	   ) 
); 
 

//========================================================
randConvexPts=[[]];
/*
convex polygons – those with the property that 
given any two points inside the polygon, the entire line segment connecting those two 
points must lie inside the polygon. Such polygons are the intersection of half-planes, 
which means that if you extend the boundary edges into infinite lines the polygon will 
always lie completely on one side of each edge line. If a point is on the same side of each 
edge as the polygon, then it must lie inside the polygon.

http://www.cs.oberlin.edu/~bob/cs357.08/VectorGeometry/VectorGeometry.pdf
*/
function randOnPlaneConvexPts( count=4, pqr=randPts(3)  )=
(
3
 // THIS IS HARD	

);




//========================================================


function randPt(d=3, x,y,z, seed)=
   let( d=  isnum(d)?[-d,d]:d 
      , _x= x==undef?d: isnum(x)?[-x,x]:x
      , _y= y==undef?d: isnum(y)?[-y,y]:y
      , _z= z==undef?d: isnum(z)?[-z,z]:z
      )
(   mm*( seed? 
    [ x==0?x: rands( _x[0], _x[1],1, seed )[0]
    , y==0?y: rands( _y[0], _y[1],1, seed )[0]
    , z==0?z: rands( _z[0], _z[1],1, seed )[0]
    ]
    :[ x==0?x: rands( _x[0], _x[1],1 )[0]
    , y==0?y: rands( _y[0], _y[1],1 )[0]
    , z==0?z: rands( _z[0], _z[1],1 )[0]
    ])
);


//========================================================
RM=[ "RM","a3","matrix", "Geometry",
"
;; Given a rotation array (a3=[ax,ay,az] containing rotation angles about
;; x,y,z axes), return a rotation matrix. Example:
;;
;;  p1= [2,3,6]; 
;;  a3=[90,45,30]; 
;;  p2= RM(a3) * p1;  // p2= coordinate after rotation
;;
;; In OpenScad the same rotation is carried out this way:
;;
;;   rotate( a3 )
;;   translate( p1 )
;;   sphere(r=1); 
;;
;; This rotates a point marked by a sphere from p1 to p2
;; but the info of p2 is lost. RM(a3) fills this hole, thus
;; provides a possibility of retrieving coordinates of
;; rotated shapes.
;;
;; The reversed rotation from p2 to p1 can be achieved by simply applying
;; a transpose :
;;
;;  p3 = transpose(RM(a3)) * p2 = p1
"];


function RM(a3)=
(
	a3.z==0?
	IM*RMy(a3.y) * RMx(a3.x) 
	:RMz(a3.z) * RMy(a3.y) * RMx(a3.x) 

//  No idea why the following doesn't work with [0,y,0]:
//  Note: Run demo_reverse( "On y", p0y0(randPt()), "lightgreen");
//        in rotangles_p2z_demo();
//	(a3.z==0?IM:RMz(a3.z)) * 
//	(a3.y==0?IM:RMy(a3.y)) *
//	(a3.x==0?IM:RMy(a3.x)) 
	
);

module RM_test(ops){ doctest(RM,
[],ops); }

//RM_test( [["mode",22]] );
//doc(RM);

module RM_demo()
{
	a3 = randPt(r=1)*360;
	echo("in RM, a3= ",a3);
	rm = RM(a3);
	echo(" rm = ", rm);
}
//RM_demo();

function rotMx(a)=[ [1,0,0], 
                    [0,cos(a),-sin(a)], 
                    [0,sin(a),cos(a)]
                  ]; 

function rotMy(a)=[ [cos(a),0,sin(a)], 
                    [0,1,0], 
                    [-sin(a),0,cos(a)]
                  ]; 

function rotMz(a)=[ [cos(a),-sin(a),0], 
                    [sin(a),cos(a),0],
                    [0,0,1] 
                  ]; 


//========================================================
rotM= ["rotM","axis,a", "matrix", "Geometry",
" Rotation matrix for rotating angle a about line axis. 
;;
;; To rotate 30 degreen about x-axis:
;;
;;    x_axis= [ [1,0,0], [0,0,0] ]
;;    m = rotM( x_axis, 30 ) 
;;    pt2 = m*(pt- x_axis[1]) + x_axis[1]  
;;
;; Mainly used in in rotPt() and rotPts(). Most likely the rotPts()
;; is the one to go for rotation, in which the translation ( +/-
;; x_axis[1]) part is taken care of. So the above example usage can be:
;;
;;    pt2= rotPt( pt, x_axis, 30 )
;;    
;; Check out rotPt_demo() (in file scadx_demo.scad)
;; 
;; To apply this to OpenScad object (non-polyh):
;;
;;    translate( axis[1] ) 
;;    multmatrix(m= rotM( axis,a) ) 
;;    {
;;       translate( loc-axis[1] ) // loc: where obj is in xyz coord
;;       cube( [3,2,1] );
;;    }
;;        
;; Note: rotation of pts about any axis can also be achieved through
;; anglePt(). However, anglePt() can't be used on the OpenScad
;; made, non-polyhedron objects for such a task. In that case, 
;; rotM is needed. 
;; http://forum.openscad.org/Transformation-matrix-function-library-td5154.html
"]; 

function rotM(axis,a) = 
   /* 
       Rotate about any arbitrary axis pq.
       
       http://www.fastgraph.com/makegames/3drotation/
   */   
   let( 
//       qp = axis //reverse(axis)
//        /* if we move pq by moving p to ORIGIN, p=>q will be looking
//           outward from the ORIGIN. This will result in wrong direction
//           of rotation. So we need to reverse it.
//        */
//      //, a=-a
      , uv= onlinePt( reverse(axis) ,len=1)- axis[1]   // unit vector
      , x=uv[0]
      , y=uv[1]
      , z=uv[2]
      , c=cos(a)
      , s=sin(a)
      , t=1-c
      , m= [ [ t*(x*x)+c,   t*(x*y)-s*z, t*(x*z)+s*y ]
           , [ t*(x*y)+s*z, t*(y*y)+c,   t*(y*z)-s*x ]
           , [ t*(x*z)-s*y, t*(y*z)+s*x, t*(z*z)+c   ]
           ]     
//       , _d= chkbugs([],
//                [  "pq", pq
//               , "x", x  
//               , "y", y 
//               , "z", z  
//               , "c",c
//               , "s",s
//               , "t",t
//               , "m",m
//               ]
//               )
       ) 
( // _d               
   m
);

//========================================================
rotMx=["rotMx", "a", "matrix", "Math",
,""
];

function rotMx(a)=[[1,0,0,0], 
                     [0,cos(-a),-sin(-a),0], 
                     [0,sin(-a),cos(-a),0], 
                     [0,0,0,1]]; 

module rotMx_test(ops=["mode",13])
{
    doctest( rotMx,
    [
      [ 90, slice(rotMx(90)*[2,1,-2,1],0,-1)
            , [2, 2,1]
            , ["funcwrap", "slice({_}*[2,1,-2,1])"] 
      ]
    
    ]
    ,ops);
}
//angle=90;
//M= [ [cos(angle), -sin(angle), 0, 0],
//                [sin(angle), cos(angle), 0, 0],
//                [0, 0, 1, 1],
//                [0, 0, 0,  1]
//              ];
//echo( Mpt= M*[0,2,1,1] );

//========================================================
rotpqr=["rotpqr","pqr,a","pqr","Geom",
"Rotate R in pqr by a."
];
	// (1)
	//      R_             PQ > TQ
	//      | '-._
	//	p---J-----'Q
	//
	// (2)           R     PQ > TQ     
	//              /|
	//     P-------Q J
	//   
	// (3)   R_            PQ < TQ, Need extended P
	//       |  '-._
	//       |       '-._
	//       J  p-------'Q
    
function rotpqr(pqr,a)=
(   let( P=pqr[0]
       , Q=pqr[1]
       , R=pqr[2]
       , J=projPt( R,[P,Q] )
       , L=dist(J,R)
       , dJQ = dist(J,Q)
       , dPQ = dist(P,Q)
       , Px= onlinePt( [Q,P], len=2*(dJQ+dPQ)) // extended P
       )
    [ P
    , Q
    , anglePt( [ Px // Make sure a2 rot to right direction
               , J // othoPt([P,R,Q])
               , R
               ], a=90, a2=a, len=L ) 
    ]
);

//========================================================
rotx=["rotx", "pt,a", "matrix", "Math",
,""
];

function rotx(pt,a)=
    let( pt = rotMx(a)* [ pt.x,pt.y,pt.z,1] )
    [pt.x,pt.y,pt.x]; 

module rotx_test(ops=["mode",13])
{
    doctest( rotx,
    [
      [ "[2,1,-1], 90", rotx([2,1,-1], 90)
            , [2, 1,1]
      ]
      , [ "[1,2,3], 90", rotx([1,2,3], 90)
            , [1,-3,2]
      ]
      , [ "[2,1,-1], 90", RMx(90)*([2,1,-1])
            , [2, 1,1]
      ]
      , [ "[1,2,3], 90", RMx(90)*([1,2,3])
            , [1,-3,2]
      ]
    ]
    ,ops);
}


//========================================================
RMx=["RMx","ax","matrix", "Geometry",
"
 Given an angle (ax), return the rotation matrix for
;; rotating a pt about x-axis by ax. Example:
;;
;;  pt = [2,3,4]; 
;;  pt2= RMx(30)*pt;  
;;
;; In openscad, you can rotate the pt to pt2 by thiss:
;;
;;  rotate( [30,0,0 ])
;;  translate( pt )
;;  sphere(r=1); 
;;
;; However,  the coordinate info of pt2 is lost. RMx
;; (and RMy, RMz) provides a way to retrieve it. This
;; is a work around solution to an often asked question:
;; how to obtain the coordinates of a shape.
"];	

function RMx(ax)= [
	[1,0,0]
	,[0, cos(-ax), sin(-ax) ]
	,[0,-sin(-ax), cos(-ax) ]
];

module RMx_test(ops)
{
	doctest( RMx
	,[
	
	],ops );
}
//RMx_test( ["mode",22] );
//doc(RMx);




////========================================================
RMy=["RMy","ay","matrix", "Geometry",
"
 Given an angle (ay), return the rotation matrix for
;; rotating a pt about y-axis by ay. See RMx for details.
"];	
function RMy(ay)=[
	 [cos(-ay), 0, -sin(-ay) ]
	,[0,        1,        0  ]
	,[sin(-ay), 0,  cos(-ay) ]
];
module RMy_test(ops)
{
	doctest( RMy
	,[
	
	],ops );
}
//RMy_test( ["mode",22] );



//========================================================
RMz=["RMz","az","matrix", "Geometry",
"
 Given an angle (az), return the rotation matrix for
;; rotating a pt about z-axis by az. See RMx for details.
"];

function RMz(az)=[
	 [ cos(-az), sin(-az), 0 ]
	,[-sin(-az), cos(-az), 0 ]
	,[       0,         0, 1 ]
];
module RMz_test(ops)
{
	doctest( RMz
	,[
	
	],ops );
}
//RMz_test( ["mode",22] );
//doc(RMz);


//========================================================
RMs=[[]];
function RMs( angles, pts, _i_=0 )=
(
	_i_<len(pts)? concat( [ RM(angles)*pts[_i_] ]
                         , RMs( angles, pts, _i_+1)
					  ):[]  
);




//========================================================
RM2z=["RM2z","pt,keepPositive=true", "matrix", "Geometry", 
"
 Given a pt, return the rotation matrix that rotates
;; the pt to z-axis. If keepPositive is true, the matrix will
;; always rotate pt to positive z. The xyz of the new pt on z-axis, ;;
;; pt2, is thus:
;;
;;  pt2 = RM2z(pt)*pt
;;
;; The reversed rotation, i.e., rotate a point on z back to pt,
;; can be achieved as:
;;
;;  pt3 = transpose( RM2z(pt) )*pt2  = pt
"];

function RM2z(pt,keepPositive=true)=
(
	/*pt.z==0
	?RM( rotangles_p2z(pt,keepPositive=true) ) 
	:RMy((keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/noRM(pt)))
	*RMx( sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z)) )
	*/
	RM( rotangles_p2z(pt,keepPositive=true) )
);

//========================================================
RMz2p=[ "RMz2p","pt","array", "Geometry",
"
 Given a pt, return the rotation matrix that rotates the pt to
;; z-axis. The xyz of the new pt, pn, is:
;;
;;  pn = RM2z(pt)*pt
"];

function RMz2p(pt)=
(
3
//ax = -sign(pt.z)*asin( pt.y/L );  // rotate@y to xz-plane
//	ay = pt.z==0?90:atan(pt.x/pt.z); 	// rotate from xz-plane to target 	
	
);

//echo("a<span style='font-size:20px'><i>this is a&nbsp;test</i></span>b");


//========================================================


function rodfaces(sides=6,hashole=false)=
 let( sidefaces = rodsidefaces(sides)
	, holefaces = hashole? 
				  [ for(f=sidefaces) 
					 reverse( [for(i=f) [i+sides*2] ])
				  ]:[]
	)
(
	hashole
	? concat( range(sides-1, -1)
			, range(sides, sides*2)
			, sidefaces
			) 	
	: concat( [ range(sides-1, -1) ]
			, [ range(sides, sides*2) ]
			, sidefaces
			)
);

//	p_faces = reverse( range(sides) );
//	// faces need to be clock-wise (when viewing at the object) to avoid 
//	// the error surfaces when viewing "Thrown together". See
//	// http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
//	q_faces = range(sides, sides*2); 
//concat( ops("has_p_faces")?[p_faces]:[]
//				 , ops("has_q_faces")?[q_faces]:[]
//				 , rodsidefaces(sides)
//				 , ops("addfaces")
//				 );




//========================================================

function rodPts( pqr=randPts(3), opt=[] )=
 let( 
     df_opt = 
      [ "r", 0.5
	  , "rP", undef
      , "rQ", undef
      , "nside",     6
      
      , "len", d01(pqr)
      , "ext", 0  // Extend outward. A # for len or "r#" for ratio
                  //   Could be array [extP,extQ] 
      , "extP",0  // extend outward from P pt
      , "extQ",0  // extend outward from Q pt
        
	  , "ptsP",undef  // pts on the P end
	  , "ptsQ",undef  // pts on the q end      
      , "xsecpts", undef // cross-section pts. These are 2D
                         // pts ref. to ORIGIN.
      
	  , "aP",90   // cutting angle on p end
	  , "aQ",90   // cutting angle on q end
	  , "rotate",0  // rotate about the PQ line, away from xz plane
	  , "rotQ", 0 // see twistangle()
      , "rotB4cutA",true // to determine if cutting angle 
                              // first or rotate first. 
	  

	  ]
    //--------------------------------------------------
    ,_opt = isstr(opt)? sopt(opt):opt
	,opt  = update( df_opt, _opt ) //concat( opt, _opt )
    
	,r    = hash(opt, "r")
    ,rP   = or( hash(opt,"rP"), r)
    ,rQ   = or( hash(opt,"rQ"), r)
    
    ,xsecpts= hash(opt, "xsecpts")
	,nside= or(len(xsecpts),hash(opt,"nside"))
    
    //------------------------------------------------
    ,L    = hash(opt, "len")
    ,ext  = hash(opt, "ext")
    ,_extP = or(hash(opt, "extP"), len(ext)==2? ext[0]:ext)
    ,_extQ = or(hash(opt, "extQ"), len(ext)==2? ext[1]:ext)
    ,extP  = (begwith(_extP,"r")
                ?num(slice(_extP,1))*L: _extP)*mm 
    ,extQ = (begwith(_extQ,"r")
                ?num(slice(_extQ,1))*L: _extQ)*mm 
    ,Po   = pqr[0]
    ,Qo   = onlinePt( pqr, len=L)
    ,Ro   = pqr[2]
    
    ,P    = onlinePt( [Po,Qo], len=-extP) 
    ,Q    = onlinePt( [Qo,Po], len=-extQ) 
    ,R    = rotpqr( pqr, hash(opt,"rotate",0 ))[2]
    ,J    = projPt( [P,Q],R )
    ,Px   = onlinePt( [P,Q]  // extend P outward to ensure direction
            , len=-2*(dist(J,Q)+dist(P,Q)))
    ,uvN   = uvN( [Px,P,R] )
//    ,Np   = N([Px,P,R])
//    ,Nq   = N([Px,Q,R])
    ,Mp   = N2([Px,P,R])
    ,Mq   = N2([Px,Q,R])
    //------------------------------------------------
    ,ptsP = xsecpts? // pts to be on pl [Px, P, Mp]
            [ for(i=range(xsecpts)) 
               let( pt = xsecpts[i]  
                  , ptx= onlinePt( [P,Mp], len= pt.x)
                  , pty= ptx+ uvN*pt.y
                  )
               pty                
            ]
            :  arcPts( [Px,P,R], a=90, a2= 360*(nside-1)/nside, n=nside,rad=r )
            
    ,ptsQ = xsecpts? // pts to be on pl [P, Q, Mq]
            [ for(i=range(xsecpts)) 
               let( pt = xsecpts[i]  
                  , ptx= onlinePt( [Q,Mq], len= pt.x)
                  , pty= ptx+ uvN*pt.y
                  )
               pty                
            ]
            : arcPts( [Px,Q,R], a=90, a2= 360*(nside-1)/nside, n=nside,rad=r )
            
    )//let
( 
	//[Px,Q,R, Mp,Mq] 
    [ptsP,ptsQ]
    // [[Px,P,R],[Px,Q,R] ]
            
);    
            
 


//========================================================

function rodsidefaces( sides=6, twist=0) =
 let( top= twist
		   ? roll( range(sides,2*sides), count=twist) 
		   : range(sides,2*sides)
	)
(
	[ for(i = range(sides) )
	  [ i
	  , i==sides-1?0:i+1
	  , i==sides-1? top[0]: top[i+1]
	  , top[i]
//	  , i + 1 + (i- twist==sides-1?0:sides) -twist
//	  , i+sides - twist +( i==0&&twist>0?sides:0)
	  ]
	]
);


//========================================================
rotangles_p2z=["rotangles_p2z","pt,keepPositive=true","Array","Geometry",
"
 Given a 3D pt, return the rotation array that
;; would rotate pt to a point on z-axis, ptz= [0,0,L],
;; where L is the len of pt to the ORIGIN.
;; Two ways to rotate pt to ptz using the rotation
;; array (RA) is obtained:
;;
;; 1. Scadex way: (has the target coordinate)
;;
;;    RA = rotangles_p2z( pt ); 
;;    ptz = RM( RA )*pt; 
;;    translate( ptz )
;;    sphere9 r=1 ); 
;;
;; 2. OpenScad way: (lost the target coordinate)
;;
;;    RA = rotangles_p2z( pt ); 
;;    rotate( RA )
;;    translate( pt )
;;    sphere( r=1 ); 
"
];
        
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);





//========================================================
rotangles_z2p=["rotangles_z2p","pt","Array","Geometry",
"
 Given a 3D pt, return the rotation array that
;; would rotate a pt on z, ptz (=[0,0,L] where L is
;; the distance between pt and the origin), to pt
;; Usage:
;;
;;    RA= rotangles_z2p( pt ); 
;;    rotate( RA )
;;    translate( ptz )
;;    sphere( r=1 ); 
"
];

/*
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);
*/
function rotangles_z2p(pt)=
(
	//// 2014.5.20: add 
	////   (pt.x<0?-1:1)
	//// to cover the case of : [x,0,0] when x<0
	////
	(pt.z==0&&pt.y!=0)
	?[ -sign(pt.y)*90, 0, -sign(pt.x)*sign(pt.y)*abs(atan(pt.x/pt.y))]  
	:[-sign(pt.z)*asin( pt.y/ norm(pt) )  // rotate@y to xz-plane
	 , pt.z==0?((pt.x<0?-1:1)*90):atan(pt.x/pt.z) 	      // rotate from xz-plane to target 	
	 , 0]

);	

module rotangles_z2p_test(ops)
{ doctest( rotangles_z2p, ops=ops ); }

//rotangles_z2p_test( ["mode",22] );
//rotangles_z2p_demo();

//========================================================
rotObj= ["rotObj", "axis,a,center=[0,0,0]", "n/a","Geometry",
" Module to rotate obj @center about axis for angle a.
;;
;; center is the translation when the obj is created.
;;
;;    rotObj( axis, a, center=loc )
;;        color(clr, 0.4) 
;;        cube( lens ); 
"
];

module rotObj( axis
             , a 
             , center=[0,0,0] // = translation of obj when it is built.
             )
{
    translate( axis[1] ) 
    multmatrix(m= rotM( axis,a) ) 
    {
       translate( center-axis[1] ) // center: where obj is in xyz coord
       children();
    }           
}    

//========================================================

function rotPt(pt,axis,a)=
   let( m=rotM(axis,a)
      )
(
   m*(pt-axis[0]) + axis[0] 
);


//========================================================
function rotPts(pts,axis,a)=
   let( m=rotM(axis,a)
      )
(
   [for(pt=pts) m*(pt-axis[0]) + axis[0] ] 
);


//========================================================
scaleM=["scaleM", "v", "matrix", "Math",
"scale matrix
;;  ???
"
];

function scaleM(v)=[[v[0],0,0,0], 
                   [0,v[1],0,0], 
                   [0,0,v[2],0], 
                   [0,0,0,1]]; 

module scaleM_test(ops=["mode",13])
{
    doctest( scaleM,
    [ 
        [ [2,3,1], slice(scaleM([2,1,1])*[2,1,-2,0],0,-1)
            , [], ["funcwrap", "slice({_}*[2,1,-2,0])"] ]
    
    ]
    ,ops);
}       

//========================================================
//
//function sel(arr,indices)=
//   [for(i=indices) isarr(i)?sel(arr,i): get(arr,i)];

//========================================================

function sel(o,i, app=[], pp=[], cycle=false)= 
(
    let( iarr = isarr(i)
       , rtn = iarr? [for(j=i) sel(o,j)] 
                   : o[ fidx(o,i,cycle=cycle) ]
       , rtn2= app? concat( iarr?rtn:[rtn]
                          , ispt(app)?[app]:app )
                 : rtn 
       )
       pp ? concat( ispt(pp)?[pp]:pp, rtn2  )
                 : rtn2
);   
   
      
//========================================================

function shortside(b,c)= sqrt(abs(pow(c,2)-pow(b,2)));

//========================================================


function sinpqr( pqr )=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, T= othoPt( [P,R,Q] )
	, RT= dist([R,T])
	, RQ= dist([R,Q])
	, onQside = norm(T-P) > norm(Q-P)?1:-1
	)
(
	sign(onQside)* sin( angle([R,Q,T]) )
);
       


//========================================================
function slope(p,q=undef)= q!=undef?
						((q[1]-p[1])/(q[0]-p[0]))
						:((p[1][1]-p[0][1])/(p[1][0]-p[0][0]));


//========================================================
function smoothPts( 
          pts
        , n  //=6
        , aH // =0.5
        , dH // =0.38
        , closed //=false
        //, sameintv=false // Add pts to make intvs more or 
                         // less the same as smallest one
                         // Note that this means the # of
                         // pts is unpredictable
        , details //=false  // When false, ret pts; When true,
                         // ret [ bzAt, bzAt ....] where
                         // bzAt is the ret of getBezierPtsAt
                         // at each pt, a hash w/ following keys:
                         //  bzpts,pts,Hi,Hj,aH,dH,i,n,pqrs
        , rng // indices where smoothing applied to. 
              // default: range(pts) when closed
              //          range(0,len(pts)-1) when open
        , opt=[] 
        )=
/*
   Assume we have 4 pts: 0~3 (PQRS)
   To add pts between 0~1 or 2~3, we need to add extra pts,
   B and E, resp., to generate HCP (Handler Control Pts) 
   
                                      
                                     E
           _-P-----Q-_              : 
        _-`           `-_     __..-S 
      B-                 `R-``        
                         
                         
   getBezierPtsAt( 
   pts
   , at=1 
   , n=6 // extra pts to be added 
   , closed=false
   , ends= [true,true]
   , aH= undef //[1,1]
   , dH= undef //[0.5,0.5]
   , opt=[]
   )                
                   
*/
(
   let(
   
      //-----------------------------------------
      
        opt= popt(opt) // handle sopt
        
      , pts= or(pts, hash(opt,"pts") )
      , n  = n>0? n: hash(opt,"n", 6 )
      , _aH = und( aH, hash(opt,"aH", 0.5) )
      , aH  = isarr(_aH)?_aH:[_aH,_aH]
      , _dH = und( dH, hash(opt,"dH", 0.38) )
      , dH  = isarr(_dH)?_dH:[_dH,_dH]
      , closed= und(closed,hash( opt, "closed", false))
      
      //-----------------------------------------

      , rtn= [ for(i=[ 0
                      : len(pts)-(closed?1:2)])
                let ( bzrtn = getBezierPtsAt( pts
                              , i=i, n=n
                              , aH=aH, dH=dH
                              , closed=closed
                              , ends=[ i==0
                                     , closed && (i==len(pts)-1)
                                          ?false:true] 
                              )
                    )
                details? bzrtn:hash(bzrtn,"bzpts")
//                 : i==0? bzrtn[0]
//                 : pts[1]==last(bzrtn[0]) // last seg when closed
//                     ? slice(bzrtn[0],1,-1)
//                 : slice(bzrtn[0],1)
            ]
      )
   details? rtn:joinarr(rtn)
);   

//========================================================
function smoothPts_new( 
        pts
        , n=6
        , aH=0.5
        , dH=0.38
        , closed=false
        , details=false
        , opt=[]
        )=
/*
   Assume we have 4 pts: 0~3 (PQRS)
   To add pts between 0~1 or 2~3, we need to add extra pts,
   B and E, resp., to generate HCP (Handler Control Pts) 
   
                                      
                                     E
           _-P-----Q-_              : 
        _-`           `-_     __..-S 
      B-                 `R-``        
                         
                         
   getBezierPtsAt( 
   pts
   , at=1 
   , n=6 // extra pts to be added 
   , closed=false
   , ends= [true,true]
   , aH= undef //[1,1]
   , dH= undef //[0.5,0.5]
   )                
                   
*/
(
   let( 
        //n=n==undef?6:n
//      , aH= aH==undef?0.5:aH
//      , dH= dH==undef?0.38:dH
        n = hash(opt,"n", n==undef?6:n )
      , aH= hash(opt,"aH", aH==undef?0.5:aH )
      , dH= hash(opt,"dH", dH==undef?0.38:dH )
      , closed= hash(opt,"closed",closed)
      , details= hash(opt,"details",details)
      
      , rtn= [ for(i=[ 0
                      : len(pts)-(closed?1:2)])
                let ( bzrtn = getBezierPtsAt( pts
                              , at=i, n=n
                              , aH=aH, dH=dH
                              , closed=closed
                              , ends=[ i==0
                                     , closed && (i==len(pts)-1)
                                          ?false:true] 
                              )
                    )
                details? bzrtn:bzrtn[0]
//                 : i==0? bzrtn[0]
//                 : pts[1]==last(bzrtn[0]) // last seg when closed
//                     ? slice(bzrtn[0],1,-1)
//                 : slice(bzrtn[0],1)
            ]
      )
   details? rtn:joinarr(rtn)
);   

 
//========================================================

//\\
// If size is given:\\
//\\
//         size
//   p---------------n
//   |'-           -'|
//   |  '-       -'  |
//   |    '-   -'    |
//   |      'x'      |
//   |        '-     |
//   |          '-   |
//   |            '- |
//   m---------------q

/*
;; Given [p,q,r], return [P,Q,N,M]
;;   P-------Q
;;   |      /|
;;   |     / |
;;   |    /  |
;;   M---R---N
;;
;;   P------Q
;;   |'-_   |:
;;   |   '-_| :
;;   |      '-_:
;;   |      |  ':
;;   M------N    R
;;
;; New 2014.7.15: new OPTIONAL arg: p, r to set size. 
;;
;;          p  
;;   P---|-----Q
;;   |   |    /| 
;;   |   |   / | r
;;   |   +--/---
;;   |     /   |
;;   '----R----'
*/
function squarePts(pqr, p=undef, r=undef)=
(
	p
	?( r
	   ? squarePts( [ anglePt(pqr,a=90, len=r) //anyAnglePt(pqr,a=90, len=r,pl="xy")
				  , Q(pqr)
				  , onlinePt(p10(pqr),len=r) 
				  ] )
	   : squarePts( [ onlinePt(p10(pqr),len=p), Q(pqr), R(pqr) ] )	 
	 ) 
	: r
	  ? squarePts( [ P(pqr)
				  , Q(pqr)
                  , anglePt( pqr, a=90, len=r)
				  //, anyAnglePt(pqr,a=90, len=r,pl="xy")	
				  //, onlinePt(p12(pqr),dist=r) 
				  ] )
	  : [  pqr[0]
	 	, pqr[1]
	 	, cornerPt( [pqr[2], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[1]]) 
	 	, cornerPt( [pqr[0], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[2]])
		]
); 


//========================================================

function symedianPt(pqr)=
   let( P=pqr[0], Q=pqr[1], R=pqr[2] )
(
    onlinePt( [centroidPt(pqr), angleBisectPt(pqr)], len=2)
);    

//========================================================
function tanglnPt_P(pqr, len)=
(  /*
       tangent line pt at P

               D    T
       Q ---R--+----+-
          `-_  |  -`
             `-|-`
               P 
       For pqr, the tangent line, PT, to the circle centering
       Q and passing thru P, can be obtained knowing:
       
       PD^2 = QD * DT
       
       T can also be obtained with anglePt( QPR, a=90)
       
    */
  anglePt( p102(pqr),a=90,len=len)
//   let( P=P(pqr), Q=Q(pqr), R=R(pqr), D=othoPt( [R,P,Q] )
//      , T= onlinePt( [D,Q], len= -pow( dist(P,D),2)/ dist(Q,D) )
//      )
//   len?onlinePt([P,T],len=len) :T
);

function tangLn(pqr, len)= // Return: [A,B,abi]
(  // tangent line 90-deg to angle bisect line of pqr 
   // len: int (extend to P and Q sides dir by len)
   //      arr (extend to P by len[0], Q by len[1]
   /*
      tangent line is the line passing Q and 90d to Q-Abi
      where abi is the angle bisect point.

              abi    
       P -     +    R
          `-_  |  -`
             `-|-`
          A----Q---B
               
      len=[a,b] where a= dAQ, b= dBQ. 
      or =c to set both the same 
      or if not set, = [ dPQ/2, dQR/2] 
   */    
   
   let( len= len==undef? [ d01(pqr)/2, d12(pqr)/2 ]
             :isarr(len)? len
             : [len,len]
      , abi = angleBisectPt( pqr )
      , tPt = anglePt( [abi,pqr[1],pqr[2]], a=90 )
      )
   [ onlinePt( [pqr[1], tPt], len=-len[0] )
   , onlinePt( [pqr[1], tPt], len=len[1] )
   , abi
   ]
);

function textPts()=
(
3
   // https://www.microsoft.com/en-us/Typography/default.aspx
   // http://nodebox.github.io/opentype.js

);

function trslN(pts, d=1)=
(  // Translate pts along the normal dir of the first 3 pts
   //    by distance d
   // trslN( [2,3,4], 1)
   // trslN( [[2,3,4],[5,6,7]], -2 )
   let( t= uvN( sel(pts,[0,1,2]))*d )
   [ for(p=pts) p+ t ]
);    

function trslN2(pts, d=1)=
(  // Translate pts along the N2 dir of the first 3 pts
   //    by distance d
   // trslN( [2,3,4], 1)
   // trslN( [[2,3,4],[5,6,7]], -2 )
   let( t= uv( pts[1], N2( sel(pts,[0,1,2]) ) )*d )
   [ for(p=pts) p+ t ]
);

function trslP(pts, d=1)=
(  // Translate pts along the P dir of the first 3 pts
   //    by distance d
   // trslN( [2,3,4], 1)
   // trslN( [[2,3,4],[5,6,7]], -2 )
   let( t= uv( pts[0],pts[1] )*d )
   [ for(p=pts) p+ t ]
);    
   
//========================================================
   
function trfPts( pts, pqr ) // Transform pts to align with pqr
= let(
   
    pqr0  = len(pts)==3? pts: slice( pts,0,3 ) // pick 1st 3 pts as pqr0
   , a_pt2 = angle( pqr0 )  
  , QR    = d12(pts)  
  , pqrc  =[ onlinePt( p10(pqr), len=d01(pqr0) )// Make a copy of pqr0
           , pqr[1]                              // on the pqr plane 
           , onlinePt( p12(pqr), len=QR )] // Used as the template
  
  // Translate to attach Qs 
  , Q = Q(pqr) // This Q remains unchanged till the end   
  , pts_t= [for(p=pts) p + Q-Q(pts) ] 
  
  // 1st rotation to align PQs of pqrtrl and pqrc
  , pqr_v = [ P(pts_t), Q, P(pqr) ]  // pqr to produce axis
  , a1= angle( pqr_v )
  , axis1 = reverse(normalLn( pqr_v) )         
  , m     = rotM( axis1, a1)  
  , pts_r1= rotPts( pts_t              // pqr after 1st rotation
                  , axis= axis1
                  , a = a1
                  )
  , D = othoPt( p021( pts_r1 ) )
     // Find a pt on pqr where R(pqr_r1) corresponds 
  , R2 = onlinePt( [Q, anglePt( pqr,  a_pt2) ]
                   ,len= QR )
     
    , pqr_v2 = [ R(pts_r1), D, R2 ] 
     
     ,axis2 = p01(pts_r1) 

     ,a2= angle( pqr_v2, Rf= axis2[0])
     
     
     ,m2 = rotM( axis2, a2)
     ,pts_r2 = rotPts( pts_r1
                  , axis= axis2
                 ,a = a2
               )
   )
(
    pts_r2
);
     
   
//========================================================

function twistangle( p,q,r,s )=
(
  let( P = r==undef? p[0]:p
     , Q = r==undef? p[1]:q
     , R = q==undef? p[2]: r==undef? q[0]:r
     , S = q==undef? p[3]: r==undef? q[1]:s

     , plrtn= planeDataAtQ( [Q,R,S], a=90, a2=90) 
     , pl = hash(plrtn, "pl")
     , Jp= projPt( pl, P) 
     , Js= projPt( pl, S)
     , _a = angle( [Jp,R,Js], Rf=Q ) 
     , a = isnan(_a)|| abs(_a)<1e-5?0:_a // Escape the nan value
     )     
     a
       
     //[P,Q,R,S,"<br/>",pl,Jp,Js]
     //[Jp, Js]
     
);

module twistangle_chk(){
    
  pts= [ [6.77079, 4.14925, 6.00621]
       , [6.7593, 4.21674, 5.98853]
       , [6.15651, 2.98036, 5.65985]
       , [6.21062, 3.00029, 5.70077]];   
 echo( twistangle = twistangle( pts ) );   
    
}    

//twistangle_chk();   


//========================================================
function uv(p,q)=             // uv( [2,3,4],[5,6,7] )
(   let( pq= q==undef?        // uv( [ [2,3,4],[5,6,7] ])
             (isarr(p[0])?p:[ORIGIN,p])  // uv( [2,3,4] )
             :[p,q] )
    onlinePt( pq ,len=1)- pq[0] 
);

//========================================================

function uvN( pqr )= // unit vector of pqr normal
( 
    uv( pqr[1], N(pqr) )  
);

