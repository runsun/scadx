/*
  Makes.scad
  
  Make obj with the options of producing BOM.  
  
  -- 170527_2: Rename data.cutoffs to data.partlib, and cutoffs_dir to partlib.
               (considering to add "addons" later, so partlib will be a store
                for both cutoffs and addons)
  -- 170527: bug fix for logging
  -- 170526: 1) Change: ("shape", "poly", "size", [...]) or ("shape", "cube", "r", 2)
                to: ("shape, "poly", "sizedef", ["size", [...]] )
             2) Complete 1st draft of bug-free colored, layered debugging system  
  -- 170524: 1) Change: ("shape", ["poly", ["size",[...]]]) in partdef changed to
                 ("shape", "poly", "size", [...]). For primitive like cube:
                 ("shape", "cube", "size", [...]) or ("shape", "cube", "r", 2)
             2) Working on a colored, layered debugging system  
  -- 170522 : Able to use primitive "cube" in both partdef and cutoffs
  -- 170521_2: Change: ("size", [...]) in partdef changed to 
               ("shape", ["poly", ["size",[...]]]) to ready for incorporation
               of primitive solids  
  -- 170521: Rename Makes() to Make()annd move from makes/makes.scad to scadx_make.scad
  -- 170520: cutoff implanted.
  -- 170519: Make copydef consistent with partdef by adding "actions" key ( i.e., 
             ["actions",["rot",[...]] instead of just ["rot",[...]];
  -- 170518.2: Code clean-up and restructuring inside Makes(); Prep for *cutoffs*;
  -- 170518: Add 2 major features: markPts and dim
  -- 170517: Incorporate materials to data; Provide error msg when both part.pts 
             and part.size are not given (means there is no pts to draw); 
             Re-write demos;
             
  -- 170516: apply showtime (using the newly developed get_animate_visibility()
             in scadx_animate.scad ) allowing each part, or any copy of it, to
             show/hide during animation
             
  -- 170515_2: remove _digest_part_copy_pts(); add new key "actions" to partdef, allows
               all copies of a part be acted upon at once. So the transPts originally
               only acted on copydefs, now it first acts on partdef.actions (for all
              copies) then copydefs (for individual copy);
              no need for conversion of data to opt_processed data
               
  -- 170515: rewrite digest_actions to fix individual "visible" and "color" 
  -- 170514_2: rename: opts => copydefs, digest_part_copy_pts()=> digest_part_copy_pts()
  -- 170514: fix bugs( undef title ), add new features( parts and individual 
             copydefs can be visible=false)
  -- 170512: add BOM_opt for Makes()
  -- 170511： done. For obj that requires BOM, see Make_demo_has_materials___PlanterBox().
             otherwise see Make_demo_no_materials___Patio( )
  -- 170510: "Making" section works nicely. To do: BOM section
  -- Start coding 170509, attempt to combine rough_data, materials and 
     cuts_arrangement into one *data*
  
  materials:  // defines materials only when BOM is needed
  [ 
    <material_name1>, [x,y,z]
  , <material_name2>, [x,y,z]
  ]
     
  data: [ "title",<title>
        , "parts", [ ]
        , "cuts", [ ]
        ]
        
        parts: [ <partname1>, partdef1
               , <partname2>, partdef2
               , ...
               ]
               
               partdef: [ "color"       , <color_name> | [ <color_name>,0~1 ]
                        , "label_bcolor", <color_name>
                        , "material"    , <material_name>
                        , "size"    , [x,y,z]
                        , "faces"       , rodfaces(4)
                        , "actions"     , ["rot", [x,y,z], "rot",[x,y,z], "transl",[x,y,z]]
                                          // Note: partdef.actions is optional
                                          
                        , "copydefs"    , [ copydef1, copydef2 ...] 
                        ]
               
                        copydef: [ "rot"   , [x,y,z]
                                 , "transl", [x,y,z]
                                 , "color" , <color_name> | [ <color_name>,0~1 ] 
                                 ]
                                 
       cuts: [ material_label_1, part_names
             , material_label_2, part_names
             , ...
             ]
             
             material_label = (string) "<material_name> #n"
             part_names = [ <partname1>, <partname1>, <partname1>, <partname2> ...]
        
*/ 

include <../scadx/scadx.scad>

/*
##########################################################################
      Set units
##########################################################################
*/

FT6= 72;
FT8= 96;
FT10= 120;

// Actual lengths in inches
LEN1= 0.75;
LEN2= 1.5;
LEN4= 3.75;
LEN3= LEN2+(LEN4-LEN2)/2; 
LEN8= 7.25;

/*
##########################################################################
      Set user parameters
##########################################################################
*/

// Width of saw blade in inch. Used in BOM string output.
KERF_W= 0.0745; 

// Set a TOLERANCE (in inch) in case of cutting error 
// This is added to the sum of length calc in 
// get_final_cuts_string(). Used in BOM string output.
CUTS_TOLERANCE = 0.5;  

// The distance all parts move to break up the box for the purpose
// of demo each part clearly. Set to 0 to see the intact box.
// For animation, set to 2*$t; 
break_dist=0; 
BD = break_dist; 

//=======================================================  

        
//=======================================================  

module Make( data
            , title= undef
            , log_target= undef
           // , materials
            , BOM_opt=[]  // ==> Define custom parameters for BOM string. Currently
                          //     could have two keys: ["KERF_W", 1, "CUT_TOLERANCE", 0.2 ]
            , _parts=[]   
            , _partlib=[]           
            , _i=0 
            )  
{
  
   /* 
     data: [ "title", "..." // title string to be replaced by the title in Make(title ...)
           , "parts", [ <partname>, <partdef>, <partname>, <partdef> ... ]
           , "partlib", [ <cutoffname>, <cutoffdef>, <cutoffname>, <cutoffdef> ... ]
  
           , "materials", []
           , "cuts", [  ]  // No need if BOM is not wanted
           ]
     
     "parts"
     , [ <partname>, <partdef>, <partname>, <partdef> ... ]
  
 
     obj_name: "side_bar"
     data:
          [ "color", "brown"
          , "material", ["1x3", 25, 2]
          , "x", 25, "y", 0.75, "z", 3
          , "faces", [[3,2,1,0], [4,5,6,7], [0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
          
          , "opts", [ [ "color", "red" ]  //<=== each opt is digested except color
                    , [] 
                    ]
          , "pts", [[pt, pt, pt, ...] ]   // <=== created pts if not existed
          ]  


   possible markPts value:
 
      "g" 
      "l;cl=green" 
      "b=false;g;l;cl=blue"
       ["cl=green", "grid",true]
      "cl=blue;g=[color,gold,r,0.05]" );
      "g=[color,blue]"
      "cl=red;l=test" );
      "cl=green;l=[isxyz,true]" );
      ["cl=blue;r=0.2"
      ,"label",["scale=0.015"
               ,"isxyz",true
               ,"tmpl","{*`a|d3|wr=,in}"
               ]
      ]
      ["cl=purple;ball=false"
      ,"label",["scale=0.05"
               ,"isxyz",true
               ,"tmpl","{*`a|d3|wr=,in}"
               ]
      ]
      
    MarkPts( trslN(pqr,10), "ch;cl=teal;rng=1;l=AB"); 
    MarkPts( trslN(pqr,12), "ch;cl=purple;rng=[0,-1];l=BC"); 
    MarkPts( trslN(pqr,14), "ch;cl=black;rng=1;l=[isxyz,true]"); 
  "l;ch;echopts"
  ["samesize",true]
  "samesize,label||color=red"
  "g;c;ch")
  
   */

   log_NO_BOLD = 0;
   log_BOLD_KEYS = 1;
   log_BOLD_VALS = 2;
   
   
   if(_i==0) // Initiation
   {
      echo( _mono(_b("--------------------------- Start Make() ---------------------------" )));

      title = und(_span( _b( hash(data, "title"), "font-size:20px")) );
      echo(title);
      _partlib = hash( data, "partlib" );
      partlib = _partlib
                ? [ for( i=[0: len(_partlib)-1] ) i%2
                    ? let( cutoff_name = _partlib[_i-1]
                         , cutoff_def = _partlib[i] )
                      update( cutoff_def
                            , 
                              [ "pts", get_part_pts( _partlib[i])
                              , "faces", hash( _partlib[i], "faces", rodfaces(4) )
                              ]
                            ) 
                    : _partlib[i] 
                  ]
                : undef;
       
                // partlib is a hash like :
                //   [ groove_l   : ["pts", [...], "faces", [...]]
                //   , handle_hole: ["pts", [...], "faces", [...]]
                //   ]
                
      Make( data = data
            , title= title
            , log_target = log_target
            , BOM_opt=[]
            , _parts = hash(data, "parts")
            , _partlib = partlib 
            
            , _i=1 
            );      
   }
   else if( _i>= len( _parts ) )
   {     
     echo( _mono(_b("--------------------------- Done Make() ---------------------------" )));

     //echo( get_cut_sizes_str_n_sum( data, mat_label) );
     materials = hash( data, "materials" );  
     if (hash(data, "cuts")) 
        echo_bom( data, materials, BOM_opt ); 
   }
   
   else if (_i%2!=0) 
   {
     
      partname = _parts[_i-1];
      partdef  = _parts[_i];
      partsize = hashs( partdef, ["sizedef", "size"] );
      partshape= hash( partdef, "shape");
      partpts = get_part_pts( partdef );
     
      log_part( _mono(repeat("--------",5)));
      log_part( [ "partname", partname ], log_BOLD_VALS );
      log_part( [ "partdef", _fmth(partdef) ] );
      log_part( ["partpts", partpts] );
      
      if (!partpts )  // if pts not existing, skip the drawing of this part and go to next
      {
        log_error( 
         str( "part \"", partname, "\" doesn't provide points info. Add "
            , partshape=="poly"
               ?"either <u>[\"pts\", &lt;pt list>]</u> or <u>[\"sizedef\", [\"size\", [x,y,z]]]</u>"
               :_s("<u>[\"sizedef\", &lt;hash of {_}'s sizedef>]</u>", partshape)
            , _s(" to {_}'s <b>partdef</b>.", partname)
            )
        , indent=2
        );
      }   
      else  
      { 
        
        // Continue to process part
        log_part( "Continue to apply partpts to each copy." );
        
        part_color = hash( partdef, "color");
        part_showtime= hash( partdef, "showtime");
        part_actions = hash( partdef, "actions", [] );
        part_markPts = hash( partdef, "markPts" );        
        part_visible = hash( partdef, "visible", true)
                          && get_animate_visibility( part_showtime ) ;
        p_cutoff_defs = hash( partdef, "cutoffs" ); //: ["handle",[], "groove",[]]
        
        faces= hash( partdef, "faces",  rodfaces(4));
        
        
        copydefs = hash( partdef, "copydefs"
                       , [[]] // It is essential to give a default value at least
                              // one empty [] to make sure the part is drawn
                       );

        log_copy( s=str("Looping through copydefs: ", copydefs), indent=1 );
        
        for( i = [0: len(copydefs)-1] ) // NOTE: copydefs will be [[]] if not given
        {  
          //echo( _color( str(">>>>> i= ", i, ", copydef = ", _fmth(copydef) ), "brown")); 
          
          copydef = copydefs[i];
          copyname = str(partname, " #", i);
          copy_actions = hash( copydef, "actions", []);
          copy_visible= get_copy_visibility( part_visible, copydef );
          copy_color = get_copy_color( part_color, copydef );
          
          log_copy( ["copyname", copyname, "copydef", copydef], mode=2 );          
          
          c_cutoff_defs = hash( copydef, "cutoffs");
          
          copypts = digest_actions( partpts, copydef );
          log_copy( ["copypts", copypts], indent=3 );
          
          
          if( copy_visible )
          difference()
          {
             if( copy_visible ) 
             {
                color( copy_color[0], copy_color[1] )
                Draw_copies( partdef, copyname, copypts, faces, part_actions, copy_actions);
             }
            if(p_cutoff_defs || c_cutoff_defs )
              Draw_all_cutoffs( p_cutoff_defs,c_cutoff_defs
                            , _partlib, part_actions, copy_actions);            
          }  
         else 
         {  
           log_copy( _u(_color(str(_b(copyname), " : visible= false; "
                         , (_b("skipped"))), "indianred")), indent=3 );
            if(p_cutoff_defs || c_cutoff_defs )
              Draw_all_cutoffs( p_cutoff_defs,c_cutoff_defs
                            , _partlib, part_actions, copy_actions);            
         }
         
//          difference()
//          {
//             if( copy_visible ) 
//             {
//                color( copy_color[0], copy_color[1] )
//                  Draw_copies( partdef, copyname, copypts, faces);
//             }
//             else 
//             {  
//               log_copy( str(copyname, " : visible= false; ", (_b("skipped"))), indent=3 );
//             }
//             
//            if(p_cutoff_defs || c_cutoff_defs )
//              Draw_all_cutoffs( p_cutoff_defs,c_cutoff_defs
//                            , _partlib, part_actions, copy_actions);            
//          } 

            markPts = hash( copydef, "markPts", part_markPts );
            if (markPts)
            {
              log_markPts( "Processing markPts");
              log_markPts( str("markPts: ", markPts), indent=4);
              MarkPts( copypts, markPts );     
            }         
            
            //---------------------------------------------

            dims = hash( copydef, "dims" );
            if(dims) 
            {
              Draw_dims( copypts, dims );              
            } 
        } //_for loop copydefs
        
      } //_ has valid pts    
      
      Make( data=data
           , title=title
           , log_target=log_target
           , BOM_opt = BOM_opt
           , _parts = _parts
           , _partlib = _partlib 
           , _i=_i+2 
           );
     
   } // _ if (_i%2!=0)
   
   module make_log(target, ss, color, mode=1, indent=0)  
   { // ss could be str or arr
     // If ss = arr: 
     //   mode=0: no highlight
     //   mode=1: highlight keys
     //   mode=2: highlight vals 
     //echo( log_target= log_target, name=name );
     if( has(log_target, "all") || has(log_target, target))
        //|| has(log_target, partname) )
     {  
       mode= mode==undef? 1:mode;
       _indent = _mono( repeat( "&nbsp;", indent));
       prompt= _span(  "&nbsp;"
                    , str( "background:", color==undef?"darkgray":color));
       s = isarr(ss)?join( [ for(i=[1:2:len(ss)])
                    let(k= ss[i-1], v= ss[i])
                    str( mode==1?_b(k):k
                        , "=", isstr(v)?"\"":""
                        , mode==2? _b(v):v 
                        , isstr(v)?"\"":"")
                  ]
               , ", ")
          : ss ; 
       echo(_color(str( _indent, prompt," ", s), color)); 
       }
   }  
   module log_error(s,mode,indent=0){ 
        make_log( "all", str(_b("ERROR! "), s), "red", mode, indent);}
   module log_part(s,mode,indent=1){ make_log( "parts", s, "blue", mode, indent); }
   module log_copy( s,mode,indent=2){ make_log( "copies", s, "indigo", mode, indent); }
   module log_cutoff( s,mode,indent=4){ make_log( "cutoffs", s, "teal", mode, indent); }
   module log_markPts( s,mode,indent=4){ make_log( "markPts", s, "darkkhaki", mode, indent); }
   module log_dim( s,mode,indent=4){ make_log( "dims", s, "DarkOliveGreen", mode, indent); }
   
 
   //------------------------------------------------
   function getCubePts(xyz)=
   (
     [ for (p= cubePts( [[xyz.x,0,0], ORIGIN, [0,xyz.y,0]], h=xyz.z ) )
       p+[0,0,xyz.z]
     ]
   );
   
   //------------------------------------------------
   function digest_actions( pts, def=[], _actions=[], _i=-1 )= 
   (                 // def contains "actions" key 
     def==[]? pts
     : _i==-1? digest_actions( pts, def
                             , hash( def, "actions"), _i=0 ) 
     : !_actions || _i> len(_actions)? pts
     : _i%2==0? digest_actions( pts=pts, def=def, _actions=_actions, _i=_i+1)
     : let( key= _actions[_i-1]
          , val= _actions[_i]
          , pts= transPts( pts, [key,val] )
          )
       digest_actions( pts=pts, def=def, _actions=_actions, _i=_i+1)
            
   );//digest_actions     
     
     
   //------------------------------------------------
   function get_part_pts( partdef )=
   (
     // Given a partdef, resolve the source of pts (either pts or size)
     // and apply hash( partdef, "actions" )
     //
     // Return pts 
     
     /*
     "side_l"  // <== partname

       partdef: 
      , [ 
        , "shape","poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        ] 
        
       partdef: 
      , [ 
        , "shape","cube"
        , "sizedef", ["size", [x,y,z] ]
        , "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        ] 
        
     */    
     let( shapename = hash( partdef, "shape")
        //, shapedata = shapedef[1]
        ,  _pts = hash( partdef, "pts")  // if "pts" is given , use it. Otherwise, 
                                         // get it from the size
        , size = hashs( partdef, ["sizedef","size"])
        , no_pts =  !_pts && !size 
        )
     no_pts? undef
     : let( pts = _pts?_pts: getCubePts( size)
          , actions = hash( partdef, "actions" )
          )
       actions? transPts( pts, actions): pts  
  
  

   ); 

   //------------------------------------------------
   
   function get_copy_visibility( part_visible, copydef )=
   (
      //get_animate_visibility(  hash( copydef, "showtime"))
      let( copy_showtime = hash( copydef, "showtime")
         , _copy_visible = hash( copydef, "visible", true)
                           && get_animate_visibility( copy_showtime)
         )
      part_visible && _copy_visible 
   );
   
   //------------------------------------------------

   function get_copy_color( part_color, copydef )=
   (
      let( _color= hash( copydef, "color", part_color) )
      isstr(_color)?[_color,1]:_color
   );        

   //------------------------------------------------

  module Draw_copies( partdef, copyname, copypts, faces, part_actions, copy_actions)
  {
     shape = hash( partdef, "shape", "cube");
     log_copy( _s("copy \"{_}\" is visible, drawing ...", copyname), indent=3 );

     if(shape=="poly") 
     {
        log_copy( "<u>Drawing a <b>\"poly\"</b></u>", indent=3 );
        polyhedron( copypts, faces) ; 
     }
     else
     { 
       shape_sizedef= hash( partdef, "sizedef" );
       log_copy( _s("<u>Drawing a <b>\"{_}\"</b></u>", shape), indent=3 );
       log_copy( ["shape sizedef: ", shape_sizedef], indent=3 );
       Transforms( concat( part_actions, copy_actions ))
       Primitive( shape, shape_sizedef ); 
     }  
  }               
  function get_cutoff_init_actions( partlib, cutoff_name )=
   (
     hashs( partlib, [cutoff_name,"actions"] ) 
   ); 

   module Draw_all_cutoffs(p_cutoff_defs,c_cutoff_defs, _partlib, part_actions, copy_actions)
   {
     log_cutoff( _u("Entering <b>Draw_all_cutoffs</b>()")); 
    
      //if(p_cutoff_defs || c_cutoff_defs ) 
      //{
              //log_cutoff( "Processing cutoff"); 
              log_cutoff( ["_partlib", (_partlib)]); 
              log_cutoff( ["part_actions", (part_actions)]); 
              log_cutoff( ["copy_actions", (copy_actions)]); 
      //}
      union(){ 
        if(p_cutoff_defs)
        {
          Draw_part_cutoffs( p_cutoff_defs, _partlib, part_actions, copy_actions);

        
        } //_if(p_cutoff_defs)
      
        if( c_cutoff_defs )
        {
          Draw_copy_cutoffs(c_cutoff_defs, _partlib, part_actions, copy_actions);

        } //_if(c_cutoff_defs)
      } //_union()

     log_cutoff( _u("Leaving <b>Draw_all_cutoffs()</b>")); 
  }
  
   module Draw_part_cutoffs( cutoff_defs, _partlib, part_actions, copy_actions)
   {  Draw_cutoffs( "part", cutoff_defs, _partlib, part_actions, copy_actions); }
      
   module Draw_copy_cutoffs( cutoff_defs, _partlib, part_actions, copy_actions)
   {  Draw_cutoffs( "copy", cutoff_defs, _partlib, part_actions, copy_actions); }
          
   module Draw_cutoffs( part_or_copy
                      , cutoff_defs
                      , _partlib
                      , part_actions
                      , copy_actions
                      )
   {
     
    log_cutoff( _u(_s("<b>Draw_cutoffs</b>( \"{_}\", cutoff_defs, _partlib, part_actions, copy_actions) ", part_or_copy) ), indent=5); 
    log_cutoff( [ str( part_or_copy, " cutoff_defs: "), cutoff_defs], indent=6);  
     
    cutoff_names = keys(cutoff_defs);    //: ["handle","groove"]
     
    
    log_cutoff( ["Looping through cutoff_names: ", cutoff_names], mode=2, indent=6);
      
    for( cutoff_name= cutoff_names )
    {      
      log_cutoff(["cutoff_name", cutoff_name], mode=2, indent=6);
      
      cutoff_init_def = hash( _partlib, cutoff_name);
      log_cutoff( [ _s("{_}'s <b>cutoff_init_def</b> (= data.cutoffs)", cutoff_name)
                  , _fmth(cutoff_init_def)], mode=0, indent=7 );
      
      
      cutoff_def = hash( cutoff_defs, cutoff_name ); // cutoff_defs could be part_cutoff_defs
                                                     // or copy_cutoff_defs 
      log_cutoff( ["cutoff_def", cutoff_def], indent=7 );
        
      cutoff_actions_sets= concat( part_actions
                                   , hash( cutoff_def, "actions", [] )
                                   , copy_actions
                                   ); 
      log_cutoff( ["cutoff_actions_sets", cutoff_actions_sets], indent=7 );      
      
      _cutoff_pts= hashs( _partlib, [cutoff_name,"pts"]);       
      cutoff_pts = transPts( _cutoff_pts, cutoff_actions_sets);      
      log_cutoff( [ str(part_or_copy, " cutoff_pts"), cutoff_pts], indent=7 );
      
      cutoff_shape_name = hash( cutoff_init_def, "shape" );
        
      if( cutoff_shape_name=="poly")
      {
        faces = hash( cutoff_def, "faces", rodfaces(4));
        log_cutoff( ["faces",faces], indent=7 );
        log_cutoff( "Drawing poly", indent=7 );
        polyhedron( cutoff_pts, faces );
      }
      else{ 
        actions_sets_2 = concat(
             get_cutoff_init_actions( _partlib, cutoff_name )
             , cutoff_actions_sets
             );
        log_cutoff( str("Drawing a ", cutoff_shape_name), indent=7 );
        log_cutoff( ["actions_sets_2", actions_sets_2], indent=7 );
        cutoff_sizedef= hash( cutoff_init_def, "sizedef");
        log_cutoff( ["cutoff_sizedef", cutoff_sizedef], indent=7 );
        
        Transforms( actions_sets_2 )
        Primitive( cutoff_shape_name, cutoff_sizedef );        
      } //_if( cutoff_shape=="poly")  
      
      markPts = hash(cutoff_def, "markPts");
      if(markPts) 
      {  
        log_markPts( str("markPts: ", markPts), indent=8);
        MarkPts(cutoff_pts, markPts);
      }
      
      dims = hash(cutoff_def, "dims");
      if(dims) Draw_dims(cutoff_pts, dims);

    } //_for( cutoff_name= cutoff_names )   

  log_cutoff( _u("Leaving <b>Draw_cutoffs</b>()"), indent=5 ); 
  } 
  

   //------------------------------------------------
   module Draw_dims( pts, dims )
   {    
      // dims = [ dim_data, dim_data, ... ]
      // dim_data= [
      //      "pqr", [...]   ==> each point could be a pt, or an int as the 
      //                          index 
      //      ,"transl_p", [xyz]
      //      ,"transl_q", [xyz]
      //      ,"transl_r", [xyz]
      //      , "opt", []
      //      ]
      log_dim( "Draw_dims(pts,dims)");
      log_dim(["target pts", pts, "dims", dims] );
      
      for( dim = dims )
      {  
        __pqr = hash( dim, "pqr" );
        _pqr = [ for(x= __pqr) isint(x)?pts[x]:x ];
        pqr = [ _pqr[0]+ hash( dim, "transl_p", [0,0,0])
              , _pqr[1]+ hash( dim, "transl_q", [0,0,0])
              , _pqr[2]+ hash( dim, "transl_r", [0,0,0])
              ];
        log_dim( ["pqr", pqr] );
        
        Dim(pqr, hash(dim, "opt",[]) );      
        
      } 
    }
   //------------------------------------------------
   
}

/*
================================================================

Tools for BOM Output

================================================================
*/

function get_cut_sizes_str_n_sum( data, mat_label, _i=0, _str="", _sum=0, _debug=[])=
(
  //
  // Return an array of [ formatted cut_size_string, sum_of_length ] for mat_label:
  //
  //  ["side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794", 59.914]
  //
  // This is needed in get_final_cuts_string(...)
   
  let( parts = hash(data, "parts")
     , cuts  = hash(data, "cuts")
     , cut_partnames = hash(cuts, mat_label) // like: ["side_l", "side_l"...]    
     )  
  _i<len( cut_partnames ) 
  ? let( partname = cut_partnames[_i]   // like: "side_l"
       , partdef  = hash( parts, partname)
       , len = hashs( partdef, ["sizedef", "size"])[0] //get needed len
       , _str= str(_str, _i==0?"":", ", _b(partname), "=", _fmt(len))
       , _sum= _sum + len
       , _debug = concat( _debug, [partname])
       )
    get_cut_sizes_str_n_sum( data= data
                   , mat_label= mat_label
                   , _i=_i+1
                   , _str=_str, _sum=_sum, _debug=_debug)
  : [_str, _sum]
);


function get_final_cuts_string( data, materials, BOM_opt, _i=0, _rtn="")=
(
  /* Return a string like this:
  
    cedar_picket_1x6_Lowes #1 : 71 x 5.53 x 0.603
  side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794 => sum: 60.2865 (including KERF_W*5=0.3725 + CUTS_TOLERANCE=0.5)

  cedar_picket_1x6_Lowes #2 : 71 x 5.53 x 0.603
  side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794 => sum: 60.2865 (including KERF_W*5=0.3725 + CUTS_TOLERANCE=0.5)

  cedar_1x2_Lowes #1 : 96 x 1.542 x 0.7135
  v_bar=9.7435, v_bar=9.7435, v_bar=9.7435, v_bar=9.7435, h_bar=11.06, h_bar=11.06, h_bar=11.06 => sum: 72.6755 (including KERF_W*7=0.5215 + CUTS_TOLERANCE=0.5)

  */
  
  let( parts = hash(data, "parts")
     , cuts  = hash(data, "cuts")
     , mat_labels = keys( cuts )  // like:  "cedar_1x2_Lowes #1"
     //, cut_partnames = hash(cuts, mat_label) // like: ["side_l", "side_l"...]    
     )  
   _i<len(mat_labels)
   ? let( mat_label = mat_labels[_i]
      , mat_name = trim(split(mat_label,"#")[0])
      , cut_str_n_sum = get_cut_sizes_str_n_sum( data, mat_label ) // [cut_string, sum of len]
      , sum = cut_str_n_sum[1]
      , nPieces = len(hash(cuts, mat_label))
      , kerf_w= hash(BOM_opt, "KERF_W", KERF_W)*nPieces
      , cut_tolerance = hash( BOM_opt, "CUTS_TOLERANCE", CUTS_TOLERANCE)
      , label = _span( str( _u(mat_label), " : ")
                        , "font-size:16px; font-weight:900; color:darkblue")
      , mat_size = join(hash(materials, mat_name)," x ")
      , remark = _span( str( " (including KERF_W*", nPieces, "="
                              , kerf_w
                              , " + CUTS_TOLERANCE=", cut_tolerance, ")"                          )
                         , "font-size:10px")      
      , _rtn = str( _rtn, "<br/>"
                  , label
                  , mat_size
                  ,"<br/>"
                  , _span( str( cut_str_n_sum[0], " => ", _b("sum: "), 
                              , _fmt(  sum+kerf_w ) 
                              )
                         , "font-size:14px")
                  , remark
                  , sum+kerf_w+ cut_tolerance
                    > hash(materials, mat_name)[0]? 
                    _red(_b("&nbsp;&nbsp;&nbsp;!! MATERIAL LENGTH EXCEEDED !!")):""
                  ,"<br/>"
                  )   
      )
    get_final_cuts_string(
              data = data
             , materials=materials
             , BOM_opt=BOM_opt
             , _i=_i+1, _rtn=_rtn)   
   : _rtn
);


//function get_title_string()=
//( _span(
//       str( "<hr/>", _b("Planter Box: "), _fmt(outer_l)
//          , " x ", _fmt(outer_w), " x ", _fmt(outer_h)
//          ,"<br/>"
//          ) 
//       ,  "font-size:20px"
//      )
//);      

function get_parts_check_string( data )=
(
  //
  //  Check if draw_parts and cut_parts are matched
  //  Both of them are hash
  //
  let( parts = hash( data, "parts")
     , cuts  = hash( data, "cuts")
     , draw_parts= [for(i = [0:len(parts)-1])
           let( copydefs = hash(parts[i], "copydefs") )
           i%2!=0?( copydefs?len(copydefs):1)
           : parts[i]
         ]
     , cut_parts= countAll( joinarr( vals(cuts)) )
     )
  str( _span("Parts Check: ", "font-weight:900;font-size:16px")
     //, "<br/>"
     , "Check if numbers of draw_parts and cut_parts are matched"
     , "<br/><br/>"
     , _span( str(
             _b("draw_parts: "), _fmth(draw_parts)
             , "<br/>"
             , _b("&nbsp;&nbsp;&nbsp;cut_parts: "), _fmth(cut_parts)
             ), "font-size:14px"
             )
     , "<br/>"
     ) 
); 
  
module echo_bom( data, materials, BOM_opt=[], title=undef)
{
  // section 1: title_string
  // section 2: cuts_arrangement_string
  // section 3: parts_check_string
  /*
  -----------------------------------------------------------------------------
  Planter Box: 13 x 12.266 x 11.06
  -----------------------------------------------------------------------------
  cedar_picket_1x6_Lowes #1 : 0.603 x 5.53 x 71
  side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794 => sum: 60.2865 (including KERF_W*5=0.3725 + CUTS_TOLERANCE=0.5)
  -----------------------------------------------------------------------------
  Parts Check: Check if draw_parts and cut_parts are matched

     draw_parts: [side_l=4, side_w=4, bottom=2, v_bar=4, h_bar=3]
     cut_parts: [side_l=4, side_w=4, bottom=2, v_bar=4, h_bar=3]  
  -----------------------------------------------------------------------------
 */
  
  echo( str( "<br/><br/>"
       , und(_span( _b( hash(data, "title"), "font-size:24px")) )
       , "<br/>"
       , get_final_cuts_string( data,materials, BOM_opt )
       , "<hr/>"
       , get_parts_check_string( data)
       ));
}


/*
================================================================

                            Demo

================================================================
*/

module Make_demo_minimal(log_target="parts")
{
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  // a hash containing upto 4 keys: title, parts, materials and cuts
  [ 
   "title", "Make_demo_minimal"
   
  ,"parts"  // a hash w/ key = partname, val = partdef (=a hash defining part properties)
  , [       
      "side_l"  // <== partname

      , [ 
          "shape", "poly"  // <== shape type, like "cube", "cylinder", "poly". Default: "cube" 
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
        , "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        ]        
        
    , "side_w"
  
      , [ "shape","cube" // <== use the built-in primitive
        , "sizedef", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0,wood_th,wood_th]]
        ]          
          
    , "bottom"

      , [ "shape","poly" // skip "sizedef", give "pts" directly
        , "pts", [[13, 0, 0.603], [0, 0, 0.603], [0, 5.53, 0.603], [13, 5.53, 0.603]
                 ,[13, 0, 0], [0, 0, 0], [0, 5.53, 0], [13, 5.53, 0]]
        ]   
 
   , "dummy_poly"
  
     , [ "shape", "poly" ] // no sizedef given, will echo an error
   
   , "dummy_cube"
  
     , [ "shape", "cube" ] // no sizedef given, will echo an error
      
    ] //_parts
  
  ]; 

  Make( data = data, log_target=log_target );  
  
}


  
  
  
module Make_demo_BOM(log_target)
{
  /*
      Demo how to setup for BOM (Bill of Materials) output

      1. Add "material" to each part
      2. Add "materials" to data
      3. Add "cuts" to data  
  
  */ 
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_BOM"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "shape", "poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "material", "cedar_picket_1x6_Lowes" // defines materials for BOM  {{ 1 }}
        , "actions", ["transl", [0, 0, wood_th]] 
        ]        
        
    , "side_w"
  
      , [ "shape", "poly"
        , "sizedef", ["size", [ outer_h-wood_th, wood_w-wood_th, wood_th ]]
        , "material", "cedar_picket_1x6_Lowes"  // defines materials for BOM
        , "actions", ["rot", [0,-90,0], "transl", [ wood_th,wood_th,wood_th]]
        ]          
          
    , "bottom"

      , [ "material", "cedar_picket_1x6_Lowes" // defines materials for BOM
        , "shape", "poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]] 
        ]          
    ] //_parts
  
  //========= define materials and cuts for BOM =============
  
  , "materials"  // {{ 2 }}  < mat_name >:< mat_size >
  , [ 
      "cedar_picket_1x6_Lowes", [FT6-1, wood_w, wood_th] // -1: corner cut of picket
    ] 
   
  , "cuts"       // {{ 3 }}  // < mat_label >: < part_list >
  , [                        // < mat_label > = <mat_name> or <mat_name> + "#" + n  
      "cedar_picket_1x6_Lowes #1"   
    , [ "side_l", "side_w", "bottom" ]
    ]
  ]; 

  Make( data = data, log_target=log_target );  
  
}


module Make_demo_multiple_copies(log_target)
{
  /*
      Multiple copies of a part

      --- Add "copydefs" to part definition. The value of copydefs
          is a list containing definition for each copy. Each definition
          is a hash containing actions ("rot", "transl"), "color", 
          "visible", and other settings like "showtime".
  
          , "copydefs": 
          , [ 
              ["rot",[], "transl",[], "visible",0, "color","blue", "showtime",.3 ]
            , []  // <=== a copy w/o any copy-specific definition 
            ]
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_multiple_copies"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "shape", "poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]]
        , "copydefs"     
        , [
            []
          , ["actions", ["transl", [0, -outer_w+wood_th, 0] ] ]
          ]
        ]        
        
    , "side_w"
  
      , [ "shape", "poly"
        , "sizedef", ["size", [ outer_h-wood_th, wood_w-wood_th*2, wood_th ] ]
        , "actions", ["rot", [0,-90,0], "transl", [ wood_th,wood_th,wood_th]]
        , "copydefs"
        , [
            []
          , ["actions", ["transl", [outer_l-wood_th, 0,0] ] ]
          ]        
        ]          
          
    , "bottom"

      , [ "shape", "poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ] ]
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data, log_target=log_target );  
  
}

module Make_demo_colors(log_target)
{
  /*
      colors of a part of a copy. 
  
      -- Add "color" to the part definition for part color
      -- Add "color" to the copy definition for copy-specific color
  
      -- A color can be specified as "blue" or ["blue", 0.6]

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_colors"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["blue", 0.8]  // part color with transparancy 
      
        , "shape", "poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "copydefs"     
        , [
            []
          , ["actions", ["transl", [0, -outer_w+wood_th, 0] ]]
          ]
        ]        
        
        
    , "side_w"
  
      , [ "color", "teal"    // part color 
      
        , "shape", "poly"
        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "copydefs"
        , [
            ["color", ["brown",.9] ] // copy color
          , ["actions", ["transl", [outer_l-wood_th, 0,0] ]]
          ]        
        ]          
 
    , "bottom"

      , [ "shape", "poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ] ]
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data, log_target=log_target );  
  
}


module Make_demo_irregular_shape(log_target)
{
  // 
  // irregular shape is achieved by giving pts instead of size 
  //
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_irregular_shape"
   
  ,"parts"  
  , [       
      "irregular"  // <== part_name

      , [ "shape","poly"
        , "pts", [ [-12, 6, 0], [-6, 11, 0], [11, 8, 0], [13, -6, 0]
                 , [-12, 6, 2], [-6, 11, 2], [11, 8, 2], [13, -6, 2]]
        //, "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        , "copydefs"
        , [
            []
          , ["actions",["transl", [3,3,3]] ]  
          , ["actions",["transl", [6,6,6]], "color","teal" ]
          ]
        ]     
    ] //_parts
  
  ]; 

  Make( data = data, log_target=log_target );  
  
}


module Make_demo_visible(log_target)
{
  /*
      visibility of a part or a copy
      -- Add "visible" to the part definition for part visibility
      -- Add "visible" to the copy definition for copy-specific visibility
  
      -- A visibility can be specified as 1|0 or true|false

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_visible"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", "darkkhaki"
        , "shape","poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "copydefs"     
        , [
            []
          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]
            , "visible", 0                         // <== hide this copy
            ]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal" 
        , "shape","poly"
        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]
        , "actions", ["transl", [0,wood_th,wood_th]]
        
        , "visible", 0                            // <== hide this part 
        
        , "copydefs"
        , [
            [ ] 
          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ]]
          ]        
        ]          
          
    , "bottom"

      , [ "shape","poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]]
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data, log_target=log_target );  
  
}

module Make_demo_showtime( $t=$t )
{
  /*
    demo the showtime settings for animation
  
    1. Setup a time sequence
    2. Assign time sequence by adding "showtime" to part definition
  
    For this particular demo, set FPS= Steps = 4 for animation

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  time_sequence=    // {{ 1 }} setup a time sequence
  [
    "bottom", .25  
  , "side_l", .5  // => part "side_l" shows up at and after $t=0.5
  , "side_w", .75
  ]; 
  
  data=  
  [ 
   "title", "Make_demo_showtime"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", "darkkhaki"
        , "shape","poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "showtime", hash( time_sequence, "side_l" )  // {{ 2 }} assign time sequence
        , "copydefs"     
        , [
            []
          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal" 
        , "shape","poly"
        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]] 
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "showtime", hash( time_sequence, "side_w" ) // {{ 2 }} assign time sequence
        , "copydefs"
        , [
            [ ] 
          , ["actions", ["transl", [outer_l-wood_th, 0,0] ]]
          ]        
        ]          
          
    , "bottom"       // {{ 2 }} part "bottom" is not given a "showtime"
      , [ "shape","poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]]
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}
module Make_demo_showtime_onoff( $t=$t, log_target="all" )
{
  /*
    demo the adv showtime settings to turn on/off the display of parts
  
    For this particular demo, set FPS= Steps = 5 for animation

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  time_sequence=    
  [
    "bottom", [ .2, .9 ]   
  , "side_l", [ 0.2, 0.8 ]  // => shows up at $t=0.2 and turn off at $t=0.8
  , "side_w", [ 0.4, 0.6 ]
  ]; 
  
  data=  
  [ 
   "title", "Make_demo_showtime_onoff"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", "darkkhaki"
        , "shape","poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "showtime", hash( time_sequence, "side_l" )  //<====== 
        , "copydefs"     
        , [
            []
          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal" 
        , "shape","poly"
        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "showtime", hash( time_sequence, "side_w" )  //<=====
        , "copydefs"
        , [
            [ ] 
          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ]]
          ]        
        ]          
          
    , "bottom"       
      , [ "shape","poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ] ]
        , "showtime", hash( time_sequence, "bottom" ) //<=====
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data,log_target=log_target );  
  
}


module Make_demo_showtime_copies( $t=$t, log_target )
{
  /*
    demo the adv showtime settings:
 
    1. copy-specific showtime
    2. adv showtime: on-off-on-off multiple times

    For this particular demo, set FPS= Steps = 10 for animation

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  time_sequence=    
  [
    "bottom", [ .1, .4, .6, .9 ]  // on-off-on-off multiple times 
  , "side_l", [ 0.2, 0.8 ]  
  , "side_w", [ 0.3, 0.8 ]
  , "side_l2", [ 0.4, 0.7 ]  
  , "side_w2", [ 0.5, 0.6 ]
  ]; 
  
  data=  
  [ 
   "title", "Make_demo_showtime_copies"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", .5]
        , "shape","poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
          
        , "copydefs"     
        , [
            [ "showtime", hash( time_sequence, "side_l" )] // copy-specific showtime
          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]
            , "showtime", hash( time_sequence, "side_l2" )] // copy-specific showtime
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal" 
        , "shape","poly"
        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "showtime", hash( time_sequence, "side_w" ) // <===== part-specific
        , "copydefs"
        , [
            [ "showtime", hash( time_sequence, "side_w" )] 
          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ]
            , "showtime", hash( time_sequence, "side_w2" )]  // <===== copy-specific
          ]        
        ]          
          
    , "bottom"       
      , [ "shape","poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]] 
        , "showtime", hash( time_sequence, "bottom" )  // <===== part-specific
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data, log_target=log_target );  
  
}





module Make_demo_markPts(log_target)
{
  /*
      markPts on a part or a copy. 
  

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_markPts"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["blue", 0.8]  // part color with transparancy 
      
        , "shape","poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "visible", 0
        , "copydefs"     
        , [
           []
          ,  ["actions", ["transl", [0, -outer_w+wood_th, 0] ]]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal"    // part color 
      
        , "shape","poly"
        , "sizedef", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0,0,wood_th]]
        , "copydefs"
        , [
            ["color", "brown"
            , "markPts",  ["label",["isxyz",true]] ] // show coordinate as label
          , ["actions", ["transl", [outer_l-wood_th, 0,0]]
            , "markPts", "l"]                        // show index as label
          ]        
        ]          
          
    , "bottom"

      , [ "markPts", "b"                            // show balls only
        , "shape","poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]] 
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data, log_target=log_target );  
  
}


module Make_demo_dim(log_target)
{
  /*
      Dimensioning
  
      Add "dims" to copydefs 
      
       , "dims"
       ,  [
            <dim>
          , <dim>
          , ...
          ]
  
      Each <dim> is [ "pqr", [a,b,c]
                    , "transl_p", [xyz]
                    , "transl_q", [xyz]
                    , "transl_r", [xyz]
                    , "opt", dim_opt (refer to the Dim() module in scadx_obj2.scad )
                    ]

                   [a,b,c]: each could be either pt or index 
                   transl_p: move the point p 
                   
                   Only "pqr" is required. The rest are optional
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_dim"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", 0.8]  
      
        , "shape","poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "visible", 1
        , "copydefs"     
        , [
             [ "markPts", "l"
             
             , "dims"                      // <=== dim
                , [ ["pqr", [3, 7,2], "transl_q", [0,0, -wood_th]]
                  , ["pqr", [2,3,7]]
                  ]
             ]  
          ,  [ "actions", ["transl", [0, -outer_w+wood_th, 0] ]
             , "markPts", "l"
             , "dims", [["pqr", [5,1,0]] ] // <=== dim
             ]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal"    // part color 
      
        , "shape","poly"
        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "visible", 1
        , "copydefs"
        , [
            [] // show coordinate as label
          , [ "actions", ["transl", [outer_l-wood_th, 0,0]]
            ]                        // show index as label
          ]        
        ]          
          
    , "bottom"

      , [ "shape","poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]] 
        //, "markPts", "l"
        , "copydefs"
        , [ 
            [ "dims"  // <=== dim
            , [
                ["pqr", [4,7,3], "opt", ["label",["rot", [180,0,0 ]]]]
              ]
            ]         
          ]        
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data, log_target=log_target );  
  
}



module Make_demo_cutoffs(log_target)
{
  /*
     How to do "cutoffs" out of the object.
  
     This demo shows a cutoff, "handle_holl", is applied to part "side_l"
     in its partdef ( so each copy of the same "handle_hole" cutoff), and
     another one, "groove_l", is applied to both copies of "side_l" 
     separately (so each copy has the groove_l but at different locations) 
  
     1. Add a "cutoffs":<h_cutoff_defs> pair to *parts* : 
  
        , "cutoffs"
        , [ <cutoff_name>, <cutoff_def>, <cutoff_name>, <cutoff_def>  ...]
  
        <cutoff_def> is a stripped-down version of partdef or copydef, containing
        only up to 2 keys: "actions" and ( "size" or "pts" ):
  
            [ "size", [ ... ]
            , "actions", ["transl",[...] ]   // <=== optional
            ]
  
        This serves as a cutoff directory and could be referred to multiple times 
        inside the code for multiple copies of a cutoff.
  
     2. Add "cutoffs":<h_cutoff_def> pair to *partdef* for part-specific cutoffs.
        
         , "cutoffs"
         , ["handle_hole",[]]  
         
     3. Add "cutoffs":<h_cutoff_def> pair to *copydef* for copy-specific cutoffs.
            
         , "cutoffs"
         , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]] ]
         
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_cutoffs"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", 0.9]   
      
        , "shape","poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
        , "actions", ["transl", [0, 0, wood_th+0.001]]
        , "cutoffs", ["handle_hole",[]]                  // <==== {{ 2 }}
        , "visible",1
        , "copydefs"     
        , [
            [ "cutoffs", ["groove_l", [] ]              // <==== {{ 3 }}
            ]
            
          , [ 
              "actions", ["transl", [0, outer_w-wood_th, 0] ]
            , "cutoffs"
            , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]]  // <==== {{ 3 }}
              ]
            ]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal"     
      
        , "shape", "poly"
        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "visible", 0
        , "copydefs"
        , [
            [ ] 
          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
          ]        
        ]          
          
    , "bottom"

      , [ "color", "brown"
        , "visible", 0
        , "shape","poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ] ]
        ]          
          
    ] //_parts
  
  , "partlib"   // <================================= {{ 1 }}  
  , [
       "groove_l"
       
         , [ "shape","poly"
           , "sizedef", ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]
           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
           ]
         
    , "handle_hole"
     
        , [ "shape","poly"
          , "sizedef", ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]
          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
          ]
  
    ]
  ]; 

  Make( data = data, log_target=log_target );  
  
}

module Make_demo_cutoffs_markPts(log_target)
{
  /*
     How to do "cutoffs" out of the object.
  
     This demo shows a cutoff, "handle_holl", is applied to part "side_l"
     in its partdef ( so each copy of the same "handle_hole" cutoff), and
     another one, "groove_l", is applied to both copies of "side_l" 
     separately (so each copy has the groove_l but at different locations) 
  
     1. Add a "cutoffs":<h_cutoff_defs> pair to *parts* : 
  
        , "cutoffs"
        , [ <cutoff_name>, <cutoff_def>, <cutoff_name>, <cutoff_def>  ...]
  
        <cutoff_def> is a stripped-down version of partdef or copydef, containing
        only up to 2 keys: "actions" and ( "size" or "pts" ):
  
            [ "size", [ ... ]
            , "actions", ["transl",[...] ]   // <=== optional
            ]
  
        This serves as a cutoff directory and could be referred to multiple times 
        inside the code for multiple copies of a cutoff.
  
     2. Add "cutoffs":<h_cutoff_def> pair to *partdef* for part-specific cutoffs.
        
         , "cutoffs"
         , ["handle_hole",[]]  
         
     3. Add "cutoffs":<h_cutoff_def> pair to *copydef* for copy-specific cutoffs.
            
         , "cutoffs"
         , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]] ]
         
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_cutoffs_markPts"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", 0.9]   
      
        , "shape", "poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
        , "actions", ["transl", [0, 0, wood_th+0.001]]
        , "cutoffs", ["handle_hole",["markPts","l"]]                  // <==== {{ 2 }}
        , "visible",0
        , "copydefs"     
        , [
            [ "cutoffs", ["groove_l", [] ]              // <==== {{ 3 }}
            ]
            
          , [ 
              "actions", ["transl", [0, outer_w-wood_th, 0] ]
            , "cutoffs"
            , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]
                           , "markPts", "l"]  // <==== {{ 3 }}
              ]
            ]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal"     
      
        , "shape","poly"
        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "visible", 0
        , "copydefs"
        , [
            [ ] 
          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
          ]        
        ]          
          
    , "bottom"

      , [ "color", "brown"
        , "visible", 0
        , "shape","poly"
        , "sizedef", ["size", [ outer_l, outer_w, wood_th ] ]
        ]          
          
    ] //_parts
  
  , "partlib"   // <================================= {{ 1 }}  
  , [
       "groove_l"
       
         , [ "shape","poly"
           , "sizedef", ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]
           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
           ]
         
    , "handle_hole"
     
        , [ "shape","poly"
          , "sizedef", ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]
          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
          ]
  
    ]
  ]; 

  Make( data = data, log_target=log_target );  
  
}


module Make_demo_solid_cutoffs(log_target)
{
  /*
     How to draw primitive objects and do "cutoffs".
         
     New: set shape in partdef to primitive :
  
       , "shape"
       , [ "cube", ["size", n | [x,y,z] ] ]
    
     This is needed 'cos poly can't display cutoffs properbably   
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_solid_cutoffs"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", 0.9]   
      
        , "shape", "cube"        // shape set to cube but not poly 
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
        , "actions", ["transl", [0, 0, wood_th+0.001]]
        , "cutoffs", ["handle_hole",[]]//"markPts", "l"]]                  // <==== {{ 2 }}
        , "visible", 1
        , "copydefs"     
        , [
            [ "cutoffs", ["groove_l", []]//"markPts","l"] ]              // <==== {{ 3 }}
            ]
            
          , [ 
              "actions", ["transl", [0, outer_w-wood_th, 0] ]
            , "cutoffs"
            , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]]  // <==== {{ 3 }}
              ]
            ]
          ]
        ]        
        
//    , "side_w"
//  
//      , [ "color", "teal"     
//      
//        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]] 
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        , "visible", 0
//        , "copydefs"
//        , [
//            [ ] 
//          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
//          ]        
//        ]          
//          
//    , "bottom"
//
//      , [ "color", "brown"
//        , "visible", 0
//        , "shape",["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
//        ]          
          
    ] //_parts
  
  , "partlib"   // <================================= {{ 1 }}  
  , [
       "groove_l"
       
         , [ "shape","cube"      // shape set to cube but not poly
           , "sizedef", ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]
           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
           ]
         
    , "handle_hole"
     
        , [ "shape","cube"      // shape set to cube but not poly
          , "sizedef", ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]
          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
          ]
  
    ]
  ]; 

  Make( data = data, log_target=log_target );  
  
}


module Make_demo_solid_markPts(log_target)
{
  /*
     How to draw primitive objects and do "cutoffs".
         
     New: set shape in partdef to primitive :
  
       , "shape"
       , [ "cube", ["size", n | [x,y,z] ] ]
    
     This is needed 'cos poly can't display cutoffs properbably   
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_solid_markPts"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", 0.9]   
      
        , "shape", "cube"        // shape set to cube but not poly 
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
        , "actions", ["transl", [0, 0, wood_th+0.001]]
        //, "cutoffs", ["handle_hole",["markPts", "l"]]                  // <==== {{ 2 }}
        , "markPts", "l"
        , "visible",1
        , "copydefs"     
        , [
            [ //"cutoffs", ["groove_l", [] 
              //            ]              // <==== {{ 3 }}
              
            ]
            
          , [ 
              "actions", ["transl", [0, outer_w-wood_th, 0] ]
           // , "cutoffs"
           // , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]]  // <==== {{ 3 }}
          //    ]
            ]
          ]
        ]        
        
//    , "side_w"
//  
//      , [ "color", "teal"     
//      
//        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]] 
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        , "visible", 0
//        , "copydefs"
//        , [
//            [ ] 
//          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
//          ]        
//        ]          
//          
//    , "bottom"
//
//      , [ "color", "brown"
//        , "visible", 0
//        , "shape",["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
//        ]          
          
    ] //_parts
  
//  , "partlib"   // <================================= {{ 1 }}  
//  , [
//       "groove_l"
//       
//         , [ "shape", ["cube"      // shape set to cube but not poly
//                    , ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]]
//           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
//           //, "markPts", "l"
//           ]
//         
//    , "handle_hole"
//     
//        , [ "shape",["cube"      // shape set to cube but not poly
//                  , ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]]
//          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
//          ]
//  
//    ]
  ]; 

  Make( data = data, log_target=log_target );  
  
}

//===============================================================

//Make_demo_minimal(log_target="all"); //["parts", "copies", "markPts", "all"]);
//Make_demo_BOM(log_target="all");
//Make_demo_multiple_copies(log_target="all");
//Make_demo_colors(log_target="all");
//Make_demo_irregular_shape(log_target="all");
//Make_demo_visible(log_target="all");
//Make_demo_showtime( $t=$t, log_target="all"); // Steps = 4 for animation
//Make_demo_showtime_onoff( $t=$t, log_target="all"); // Steps = 5 for animation
//Make_demo_showtime_copies( $t=$t, log_target="all"); // Steps = 10 for animation
//Make_demo_markPts(log_target="all");
//Make_demo_dim(log_target="all");
//Make_demo_cutoffs(log_target="all");
//Make_demo_cutoffs_markPts(log_target="cutoffs");
//Make_demo_solid_cutoffs(log_target="all");
//Make_demo_solid_markPts(log_target="all");//log_target="cutoffs_markPts" );

