/*
  Makes.scad
  
  Make obj with the options of producing BOM.
  
  -- 170522 : Able to use primitive "cube" in both partdef and cutoffs
  -- 170521_2: Change: ("size", [...]) in partdef changed to 
               ("shape", ["poly", ["size",[...]]]) to ready for incorporation
               of primitive solids  
  -- 170521: Rename Makes() to Make()annd move from makes/makes.scad to scadx_make.scad
  -- 170520: cutoff implanted.
  -- 170519: Make copydef consistent with partdef by adding "actions" key ( i.e., 
             ["actions",["rot",[...]] instead of just ["rot",[...]];
  -- 170518.2: Code clean-up and restructuring inside Makes(); Prep for *cutoffs*;
  -- 170518: Add 2 major features: markPts and dim
  -- 170517: Incorporate materials to data; Provide error msg when both part.pts 
             and part.size are not given (means there is no pts to draw); 
             Re-write demos;
             
  -- 170516: apply showtime (using the newly developed get_animate_visibility()
             in scadx_animate.scad ) allowing each part, or any copy of it, to
             show/hide during animation
             
  -- 170515_2: remove _digest_part_copy_pts(); add new key "actions" to partdef, allows
               all copies of a part be acted upon at once. So the transPts originally
               only acted on copydefs, now it first acts on partdef.actions (for all
              copies) then copydefs (for individual copy);
              no need for conversion of data to opt_processed data
               
  -- 170515: rewrite digest_actions to fix individual "visible" and "color" 
  -- 170514_2: rename: opts => copydefs, digest_part_copy_pts()=> digest_part_copy_pts()
  -- 170514: fix bugs( undef title ), add new features( parts and individual 
             copydefs can be visible=false)
  -- 170512: add BOM_opt for Makes()
  -- 170511： done. For obj that requires BOM, see Make_demo_has_materials___PlanterBox().
             otherwise see Make_demo_no_materials___Patio( )
  -- 170510: "Making" section works nicely. To do: BOM section
  -- Start coding 170509, attempt to combine rough_data, materials and 
     cuts_arrangement into one *data*
  
  materials:  // defines materials only when BOM is needed
  [ 
    <material_name1>, [x,y,z]
  , <material_name2>, [x,y,z]
  ]
     
  data: [ "title",<title>
        , "parts", [ ]
        , "cuts", [ ]
        ]
        
        parts: [ <partname1>, partdef1
               , <partname2>, partdef2
               , ...
               ]
               
               partdef: [ "color"       , <color_name> | [ <color_name>,0~1 ]
                        , "label_bcolor", <color_name>
                        , "material"    , <material_name>
                        , "size"    , [x,y,z]
                        , "faces"       , rodfaces(4)
                        , "actions"     , ["rot", [x,y,z], "rot",[x,y,z], "transl",[x,y,z]]
                                          // Note: partdef.actions is optional
                                          
                        , "copydefs"    , [ copydef1, copydef2 ...] 
                        ]
               
                        copydef: [ "rot"   , [x,y,z]
                                 , "transl", [x,y,z]
                                 , "color" , <color_name> | [ <color_name>,0~1 ] 
                                 ]
                                 
       cuts: [ material_label_1, part_names
             , material_label_2, part_names
             , ...
             ]
             
             material_label = (string) "<material_name> #n"
             part_names = [ <partname1>, <partname1>, <partname1>, <partname2> ...]
        
*/ 

include <../scadx/scadx.scad>

/*
##########################################################################
      Set units
##########################################################################
*/
FT6= 72;
FT8= 96;
FT10= 120;

// Actual lengths in inches
LEN1= 0.75;
LEN2= 1.5;
LEN4= 3.75;
LEN3= LEN2+(LEN4-LEN2)/2; 
LEN8= 7.25;

/*
##########################################################################
      Set user parameters
##########################################################################
*/

// Width of saw blade in inch. Used in BOM string output.
KERF_W= 0.0745; 

// Set a TOLERANCE (in inch) in case of cutting error 
// This is added to the sum of length calc in 
// get_final_cuts_string(). Used in BOM string output.
CUTS_TOLERANCE = 0.5;  

// The distance all parts move to break up the box for the purpose
// of demo each part clearly. Set to 0 to see the intact box.
// For animation, set to 2*$t; 
break_dist=0; 
BD = break_dist; 

  
module Make( data
            , title= undef
           // , materials
            , BOM_opt=[]  // ==> Define custom parameters for BOM string. Currently
                          //     could have two keys: ["KERF_W", 1, "CUT_TOLERANCE", 0.2 ]
            , _parts=[]   
            , _cutoff_dir=[]           
            , _i=0 
            )  
{
  
   /* 
     data: [ "title", "..." // title string to be replaced by the title in Make(title ...)
           , "parts", [  ]
           , "cuts", [  ]  // No need if BOM is not wanted
           ]
  
 
     obj_name: "side_bar"
     data:
          [ "color", "brown"
          , "material", ["1x3", 25, 2]
          , "x", 25, "y", 0.75, "z", 3
          , "faces", [[3,2,1,0], [4,5,6,7], [0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
          
          , "opts", [ [ "color", "red" ]  //<=== each opt is digested except color
                    , [] 
                    ]
          , "pts", [[pt, pt, pt, ...] ]   // <=== created pts if not existed
          ]  


   possible markPts value:
 
      "g" 
      "l;cl=green" 
      "b=false;g;l;cl=blue"
       ["cl=green", "grid",true]
      "cl=blue;g=[color,gold,r,0.05]" );
      "g=[color,blue]"
      "cl=red;l=test" );
      "cl=green;l=[isxyz,true]" );
      ["cl=blue;r=0.2"
      ,"label",["scale=0.015"
               ,"isxyz",true
               ,"tmpl","{*`a|d3|wr=,in}"
               ]
      ]
      ["cl=purple;ball=false"
      ,"label",["scale=0.05"
               ,"isxyz",true
               ,"tmpl","{*`a|d3|wr=,in}"
               ]
      ]
      
    MarkPts( trslN(pqr,10), "ch;cl=teal;rng=1;l=AB"); 
    MarkPts( trslN(pqr,12), "ch;cl=purple;rng=[0,-1];l=BC"); 
    MarkPts( trslN(pqr,14), "ch;cl=black;rng=1;l=[isxyz,true]"); 
  "l;ch;echopts"
  ["samesize",true]
  "samesize,label||color=red"
  "g;c;ch")
  
   */

   if(_i==0)
   {
      title = und(_span( _b( hash(data, "title"), "font-size:20px")) );
      echo(title);
      _cutoff_dir = hash( data, "cutoffs" );
      cutoff_dir = _cutoff_dir
                ? [ for( i=[0: len(_cutoff_dir)-1] ) i%2
                    ? let( cutoff_name = _cutoff_dir[_i-1]
                         , cutoff_def = _cutoff_dir[i] )
                      update( cutoff_def
                            , 
                              [ "pts", get_part_pts( _cutoff_dir[i])
                              , "faces", hash( _cutoff_dir[i], "faces", rodfaces(4) )
                              ]
                            ) 
                    : _cutoff_dir[i] 
                  ]
                : undef;
       
                // cutoff_dir is a hash like :
                //   [ groove_l   : ["pts", [...], "faces", [...]]
                //   , handle_hole: ["pts", [...], "faces", [...]]
                //   ]
                           
      //echo( "_i=0, cutoffs = ", _fmth( cutoffs ) );          
      //echo( _cutoff_dir = (_cutoff_dir), cutoff_dir = (cutoff_dir) );
      //echo( _cutoff_dir = _fmth(_cutoff_dir), cutoff_dir = _fmth(cutoff_dir) );
                
      Make( data = data
            , title= title
            , BOM_opt=[]
            , _parts = hash(data, "parts")
            , _cutoff_dir = cutoff_dir 
            , _i=1 
            );      
   }
   else if( _i>= len( _parts ) )
   {     
     echo( _b("--- Done Make() ---" ));

     //echo( get_cut_sizes_str_n_sum( data, mat_label) );
     materials = hash( data, "materials" );  
     if (hash(data, "cuts")) 
        echo_bom( data, materials, BOM_opt ); 
   }
   
   else if (_i%2!=0) 
   {
     
      partname = _parts[_i-1];
       
      echo(_blue(str(
             "Make(...), _i= ",_i
          ,", partname= \"", _b(partname)
           , "\", size= ", join(hash( partdef, "size"), " x ")
          )));  
       
      partdef  = _parts[_i];
     
      pts = get_part_pts( partdef ); 
      if( hash(partdef, "echoPts") ) 
         echo( str( _b( partname ), " : pts= ", _fmt(pts)));
      
      if (!pts )  // if pts not given, skip the drawing of this part and go to next
      {
        echo(_red(_b( str( 
          "Error !! part '", partname, "' doesn't provide points."
          , "Add [\"pts\",<point list>] or [\"size\", [x,y,z]] to the part definition."
       ))));
      }   
      else
      { 
        
        copydefs = hash( partdef, "copydefs"
                       , [[]] // It is essential to give a default value of at
                              // least one empty [] to make sure the part is drawn
                       );
        part_showtime = hash( partdef, "showtime");
        part_visible = hash( partdef, "visible", true)
                          && get_animate_visibility( part_showtime ) ;  
        part_actions = hash( partdef, "actions", [] );
        part_markPts = hash( partdef, "markPts" );
        faces= hash( partdef, "faces",  rodfaces(4));
        part_color = hash( partdef, "color");
        //echo( part_color = part_color );
        //echo( _cutoff_dir = _cutoff_dir );
        _part_cutoff_defs = hash( partdef, "cutoffs" ); //: ["handle",[], "groove",[]]
        part_cutoff_names = keys(_part_cutoff_defs);    //: ["handle","groove"]
        
        //echo( _part_cutoff_defs = _part_cutoff_defs );
        part_cutoff_defs = [ for( name= part_cutoff_names )
                             let (_pts= hashs( _cutoff_dir, [name,"pts"])
                                 )
                             each [name, ["pts", digest_actions(_pts, partdef) ]]
                           ];
        //echo( part_visible= part_visible); 
          
        //echo( part_cutoff_defs = part_cutoff_defs );
        //echo( _cutoffs = _cutoffs );
//        echo(_red("debug ====== beg"));
//        
//        
//        
//        echo(_red("debug ====== end "));
        
        for( i = [0: len(copydefs)-1] ) // NOTE: copydefs will be [[]] if not given
        {  
          //echo( _color( str(">>>>> i= ", i, ", copydef = ", _fmth(copydef) ), "brown")); 
          
          copydef = copydefs[i];
          copy_actions = hash( copydef, "actions", []);
          copy_name = str(partname, " #", i);
//          copy_showtime = hash( copydef, "showtime");
//          echo( copy_showtime = copy_showtime );
//          echo( copydef_visible = hash( copydef, "visible", true) );
//          echo( get_animate_visibility = get_animate_visibility( copy_showtime) );
//          _copy_visible = hash( copydef, "visible", true)
//                           && get_animate_visibility( copy_showtime);
//          echo( _copy_visible = _copy_visible );
//          echo( test = get_copy_visibility( part_visible, copydef ));
          //echo( copydef = _fmt( copydef ) );
          //echo( "copy # ", i, " opt = ", _fmth(opt) );
          this_copy_visible= get_copy_visibility( part_visible, copydef );
          //echo( this_copy_visible = this_copy_visible );
          
          //if( this_copy_visible ) 
          //{  
            echo( _green( str(" copy_label = ", copy_name) ));
            //echo( part_color= part_color, copydef = copydef );
            copy_color = get_copy_color( part_color, copydef );
            //echo( copy_color = copy_color );
            
            pts = digest_actions( pts, copydef );
            if( hash(copydef, "echoPts") ) 
                  echo( str( _b( copy_name ), " : pts= ", _fmt(pts)));
            //echo( pts = pts );
            copy_cutoff_defs = hash( copydef, "cutoffs");
            

            color( copy_color[0], copy_color[1] )
            //echo( part_cutoff_defs= part_cutoff_defs, copy_cutoff_defs= copy_cutoff_defs );
            
            difference()
            {
               if( this_copy_visible ) 
               {
                   shapedef = hash( partdef, "shape");
                   if(shapedef[0]=="poly") polyhedron( pts, faces) ; 
                   else
                   { //echo( str(" ... drawing primitive \"" , shapedef[0], "\", shapedef=", shapedef));
                     //echo( all_actions = concat( part_actions, copy_actions ));
                     Transforms( concat( part_actions, copy_actions ))
                     Primitive( shapedef[0], shapedef[1] ); 
                   }  
               }
               else echo( str( copy_name, " : visible= false; skipped") );
               
               
               Draw_cutoffs( partdef
                       , copydef           // Note: copydef is [[]] if partdef 
                                           //       doesn't have a "copydefs" 
                       , cutoff_dir=_cutoff_dir
                       ) ;
            } 
              
             
            //---------------------------------------------
           // if( this_copy_visible ) 
           // {  
              markPts = hash( copydef, "markPts", part_markPts );
              if (markPts) MarkPts( pts, markPts );              

              //---------------------------------------------

              dims = hash( copydef, "dims" );
              if(dims) Draw_dims( pts, dims );              
           // }    

         // } // _ if copy visible
          
          //else
           //echo(part_label = str(partname, " #", i, " : visible= false; skipped") );
         
          //echo( _color( _u(str("&lt;&lt;&lt;&lt;&lt; i= ", i, ", end </div>" )),"brown")); 
        } //_for loop copydefs
        
      } //_ has valid pts    
      
      Make( data=data
           , title=title
           , BOM_opt = BOM_opt
           , _parts = _parts
           , _cutoff_dir = _cutoff_dir 
           , _i=_i+2 
           );
     
   } // _ if (_i%2!=0)
   
   //------------------------------------------------
   function getCubePts(xyz)=
   (
     [ for (p= cubePts( [[xyz.x,0,0], ORIGIN, [0,xyz.y,0]], h=xyz.z ) )
       p+[0,0,xyz.z]
     ]
   );
   
   //------------------------------------------------
   function digest_actions( pts, def=[], _actions=[], _i=-1 )= 
   (                 // def contains "actions" key 
     def==[]? pts
     : _i==-1? digest_actions( pts, def
                             , hash( def, "actions"), _i=0 ) 
     : !_actions || _i> len(_actions)? pts
     : _i%2==0? digest_actions( pts=pts, def=def, _actions=_actions, _i=_i+1)
     : let( key= _actions[_i-1]
          , val= _actions[_i]
          , pts= transPts( pts, [key,val] )
          )
       digest_actions( pts=pts, def=def, _actions=_actions, _i=_i+1)
            
   );//digest_actions     
     
//   function digest_actions( pts, copydef=[], _i=0 )= 
//   (  
//     copydef==[]? pts
//     : _i> len(copydef)? pts
//     : _i%2==0? digest_actions( pts=pts, copydef=copydef, _i=_i+1)
//     : let( key= copydef[_i-1]
//          , val= copydef[_i]
//          , pts= has( ["rot","transl"], key)? transPts( pts, [key, val])
//                 : pts
//          )
//       digest_actions( pts=pts, copydef=copydef, _i=_i+1)
//
//            
//   );//digest_actions
     
   //------------------------------------------------
   function get_part_pts( partdef )=
   (
     // Given a partdef, resolve the source of pts (either pts or size)
     // and apply hash( partdef, "actions" )
     //
     // Return pts 
     
     /*
     "side_l"  // <== partname

       partdef: 
      , [ 
        , "shape",[ "poly", ["size", [outer_l, wood_th, outer_h-wood_th ]]] 
        , "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        ] 
        
       partdef: 
      , [ 
        , "shape",[ "cube", ["size", [x,y,z]]] 
        , "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        ] 
        
     */    
     let( shapedef = hash( partdef, "shape")
        , shapename = shapedef[0]
        , shapedata = shapedef[1]
        ,  _pts = hash( shapedata, "pts")  // if "pts" is given , use it. Otherwise, 
                                         // get it from the size
        , size = hash( shapedata, "size")
        , no_pts =  !_pts && !size 
        )
     no_pts? undef
     : let( pts = _pts?_pts: getCubePts( size)
          , actions = hash( partdef, "actions" )
          )
       actions? transPts( pts, actions): pts  
  
  
//     /*
//     "side_l"  // <== partname
//
//       partdef: 
//      , [ 
//        , "shape",[ "poly", ["size", [outer_l, wood_th, outer_h-wood_th ]]] 
//        , "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
//        ] 
//     */    
//     let( polydef = hashs( partdef, ["shape","poly"] )   
//        ,  _pts = hash( polydef, "pts")  // if "pts" is given , use it. Otherwise, 
//                                         // get it from the size
//        , size = hash( polydef, "size")
//        , no_pts =  !_pts && !size 
//        )
//     no_pts? undef
//     : let( pts = _pts?_pts: getCubePts( size)
//          , actions = hash( partdef, "actions" )
//          )
//       actions? transPts( pts, actions): pts  

   ); 

   //------------------------------------------------
   
   function get_copy_visibility( part_visible, copydef )=
   (
      //get_animate_visibility(  hash( copydef, "showtime"))
      let( copy_showtime = hash( copydef, "showtime")
         , _copy_visible = hash( copydef, "visible", true)
                           && get_animate_visibility( copy_showtime)
         )
      part_visible && _copy_visible 
   );
   
   //------------------------------------------------

   function get_copy_color( part_color, copydef )=
   (
      let( _color= hash( copydef, "color", part_color) )
      isstr(_color)?[_color,1]:_color
   );        

   //------------------------------------------------

   module Draw_cutoffs( partdef
                       , copydef           // Note: copydef is set to [[]] if partdef 
                                           //       doesn't have a "copydefs" 
                       , cutoff_dir
                       ) 
    {
      /* 
        partdef: [ "size", [...]
                 , "pts", [...]
                 , "actions", [...]
                 , "cutoffs", [
      
       cutoff_dir is a hash like :
      
                 [ groove_l   : ["pts", [...], "faces", [...]]
                 , handle_hole: ["pts", [...], "faces", [...]]
                 ]
      
      2 types of cutoffs:
       
        1. part_cutoffs -- part-specific for every copy of this part
        2. copy_cutoffs -- copy-specific for this copy 
         
       part_cutoff_defs= ["handle_hole", ["actions",[...]] ]
       copy_cutoff_defs= ["handle_hole", ["actions",[...]] ]
      
      , "cutoffs"  // the cutoff_dir
      , [
          "handle_hole"
         
            , [ "size", [ LEN4, wood_th+0.5+0.1, LEN1]
              , "faces", [ ... ]
              , "actions", [ ... ]  // <==== cutoff's init actions (1)
              ]
        ]
      
      , "parts"
      , [
          "part1"
          , [ "actions", [...]                // part_actions (2)
            , "cutoffs", [ <cutoff_name> 
                         , [ "actions", [...] ] // cutoff's part-specific actions (3)
                         ]
            , "copydefs"
            , [ 
                 [ "actions",[...]              // copy_actions (4)
                 , "cutoffs", [ <cutoff_name>
                              , ["actions, [...]]  // cutoff's copy-specific actions (5)
                              ]
                 ]
              ]
           ]  
      
      ---------------------------------------------------------------
      
      There are 5 levels of actions involved:
      
      (1) In cutoff direction (as an initiation)
      (2) part-specific actions
      (3) part_cutoff actions
      (4) copy-specific actions
      (5) copy_cutoff actions

      The actions-(1) is alreaded processed into the cutoff_dir before sending
      to this function. So what we need to do is to process (2) to (5)
       
      If copydefs NOT defined (only one copy = the part itself):
          
          A cutoff goes thru: (1),(2),(3),(4)
          
      If copydefs defined (means, have multiple copies):
      
         Each copy_cutoff goes thru:
           (1), (2), (4), (5)
     
     */
      //echo(_color( "Enter Draw_cutoffs", "brown"));
      //echo( cutoff_dir = cutoff_dir );
      
      h_part_cutoffs = hash( partdef, "cutoffs" ); // ["hole",["action",[...]], ]
      h_part_actions = hash( partdef, "actions", [] );
      
      h_copy_cutoffs = hash( copydef, "cutoffs" ); // ["hole",["action",[...]], ]
      h_copy_actions = hash( copydef, "actions", [] );
      
      function get_cutoff_init_actions( cutoff_name )=
      (
         hashs( cutoff_dir, [cutoff_name,"actions"] ) 
      ); 
      
      if (h_part_cutoffs)
      {
//         echo( _color( "Handling part_cutoffs", "teal" ));
         //echo( h_part_cutoffs = h_part_cutoffs );
        
         for( i = [0:2:len(h_part_cutoffs)-1] )
        {  
            part_cutoff_name = h_part_cutoffs[i];
            h_part_cutoff_def = h_part_cutoffs[i+1];  // ["actions",["transl",[...]]]
            h_part_cutoff_actions = hash( h_part_cutoff_def,"actions", []);
            
            //echo( i=i, name = part_cutoff_name, h_part_cutoff_def= h_part_cutoff_def );
          
            _cutoff_pts  = hashs( cutoff_dir, [part_cutoff_name, "pts"] );
            cutoff_faces= hashs( cutoff_dir, [part_cutoff_name, "faces"] );
            cutoff_shape_def = hashs( cutoff_dir, [part_cutoff_name, "shape"] );
            cutoff_shape_name = cutoff_shape_def[0];
            //echo( _cutoff_pts = _cutoff_pts );
            //echo( h_part_actions= h_part_actions
            //    , h_part_cutoff_actions = h_part_cutoff_actions
            //    , h_copy_actions = h_copy_actions );
          
            actions_sets= concat( h_part_actions
                         , h_part_cutoff_actions
                         , h_copy_actions
                         );
//            cutoff_pts = process_actions_sets( 
//                        pts = _cutoff_pts
//                      , actions_sets = actions_sets );
            //echo( cutoff_pts = cutoff_pts );
            cutoff_pts = process_actions_sets( 
                        pts = _cutoff_pts
                      , actions_sets = actions_sets );
              
            if( cutoff_shape_name=="poly")
            {
              polyhedron( cutoff_pts, cutoff_faces );
            }
            else{ 
              actions_sets_2 = concat(
                   get_cutoff_init_actions( part_cutoff_name )
                   , actions_sets
                   );
//              echo( "Drawning solid cutoffs, ", cutoff_shape_name 
//                  , actions_sets = actions_sets
//                  , actions_sets_2 = actions_sets_2
//                  , cutoff_shape_def= cutoff_shape_def[1] 
//                   );
              Transforms( actions_sets_2 )
              Primitive( cutoff_shape_name, cutoff_shape_def[1]);
            }  
        }  
      }  

      if (h_copy_cutoffs)
      {
//         echo( _color( "Handling part_cutoffs", "teal" ));
//         echo( part_cutoffs = part_cutoffs );
        
         for( i = [0:2:len(h_copy_cutoffs)-1] )
        {  
            copy_cutoff_name = h_copy_cutoffs[i];
            h_copy_cutoff_def = h_copy_cutoffs[i+1];  // ["actions",["transl",[...]]]
            h_copy_cutoff_actions = hash( h_copy_cutoff_def,"actions", []);
            
            //echo( i=i, name = copy_cutoff_name, h_copy_cutoff_def= h_copy_cutoff_def );
          
            _cutoff_pts  = hashs( cutoff_dir, [copy_cutoff_name, "pts"] );
            cutoff_faces= hashs( cutoff_dir, [copy_cutoff_name, "faces"] );
            cutoff_shape_def = hashs( cutoff_dir, [copy_cutoff_name, "shape"] );
            cutoff_shape_name = cutoff_shape_def[0];
            //echo( _cutoff_pts = _cutoff_pts );
            //echo( h_part_actions= h_part_actions
            //    , h_copy_actions = h_copy_actions 
            //    , h_copy_cutoff_actions = h_copy_cutoff_actions
            //    );
            actions_sets= concat( h_part_actions
                                       , h_copy_actions
                                       , h_copy_cutoff_actions
                                       );
            cutoff_pts = process_actions_sets( 
                        pts = _cutoff_pts
                      , actions_sets = actions_sets );
          
            if( cutoff_shape_name=="poly")
            {
              polyhedron( cutoff_pts, cutoff_faces );
            }
            else{ 
              actions_sets_2 = concat(
                   get_cutoff_init_actions( copy_cutoff_name )
                   , actions_sets
                   );
//              echo( "Drawning solid cutoffs, ", cutoff_shape_name 
//                  , actions_sets = actions_sets
//                  , actions_sets_2 = actions_sets_2
//                  , cutoff_shape_def= cutoff_shape_def[1] 
//                   );
              Transforms( actions_sets_2 )
              Primitive( cutoff_shape_name, cutoff_shape_def[1]);
            } 
        }  
      }
      
      
      function process_actions_sets( pts, actions_sets, _i=0)=
      (
        // actions: [ "transl", [...], "rot", [...] ]
        // actions sets: [ <actions>, <actions> ...]
        // 
        _i>len(actions_sets)? pts
        : let( actions = actions_sets[_i])
          process_actions_sets( actions? transPts( pts, actions): pts
                                  , actions_sets, _i=_i+1)
      );      
      

    //echo(_color( "Leaving Draw_cutoffs", "brown"));
     
    }  
    

   //------------------------------------------------
   module Draw_dims( pts, dims )
   {    
      // dims = [ dim_data, dim_data, ... ]
      // dim_data= [
      //      "pqr", [...]   ==> each point could be a pt, or an int as the 
      //                          index 
      //      ,"transl_p", [xyz]
      //      ,"transl_q", [xyz]
      //      ,"transl_r", [xyz]
      //      , "opt", []
      //      ]
      
      for( dim = dims )
      {  
        __pqr = hash( dim, "pqr" );
        _pqr = [ for(x= __pqr) isint(x)?pts[x]:x ];
        pqr = [ _pqr[0]+ hash( dim, "transl_p", [0,0,0])
              , _pqr[1]+ hash( dim, "transl_q", [0,0,0])
              , _pqr[2]+ hash( dim, "transl_r", [0,0,0])
              ];
        //echo( __pqr = __pqr, _pqr = _pqr, pqr = pqr  );
        
        Dim(pqr, hash(dim, "opt",[]) );      
      } 
    }
   //------------------------------------------------
   
}

/*
================================================================

Tools for BOM Output

================================================================
*/

function get_cut_sizes_str_n_sum( data, mat_label, _i=0, _str="", _sum=0, _debug=[])=
(
  //
  // Return an array of [ formatted cut_size_string, sum_of_length ] for mat_label:
  //
  //  ["side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794", 59.914]
  //
  // This is needed in get_final_cuts_string(...)
   
  let( parts = hash(data, "parts")
     , cuts  = hash(data, "cuts")
     , cut_partnames = hash(cuts, mat_label) // like: ["side_l", "side_l"...]    
     )  
  _i<len( cut_partnames ) 
  ? let( partname = cut_partnames[_i]   // like: "side_l"
       , partdef  = hash( parts, partname)
       , len = hash( partdef, "size")[0] //get needed len
       , _str= str(_str, _i==0?"":", ", _b(partname), "=", _fmt(len))
       , _sum= _sum + len
       , _debug = concat( _debug, [partname])
       )
    get_cut_sizes_str_n_sum( data= data
                   , mat_label= mat_label
                   , _i=_i+1
                   , _str=_str, _sum=_sum, _debug=_debug)
  : [_str, _sum]
);


function get_final_cuts_string( data, materials, BOM_opt, _i=0, _rtn="")=
(
  /* Return a string like this:
  
    cedar_picket_1x6_Lowes #1 : 71 x 5.53 x 0.603
  side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794 => sum: 60.2865 (including KERF_W*5=0.3725 + CUTS_TOLERANCE=0.5)

  cedar_picket_1x6_Lowes #2 : 71 x 5.53 x 0.603
  side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794 => sum: 60.2865 (including KERF_W*5=0.3725 + CUTS_TOLERANCE=0.5)

  cedar_1x2_Lowes #1 : 96 x 1.542 x 0.7135
  v_bar=9.7435, v_bar=9.7435, v_bar=9.7435, v_bar=9.7435, h_bar=11.06, h_bar=11.06, h_bar=11.06 => sum: 72.6755 (including KERF_W*7=0.5215 + CUTS_TOLERANCE=0.5)

  */
  
  let( parts = hash(data, "parts")
     , cuts  = hash(data, "cuts")
     , mat_labels = keys( cuts )  // like:  "cedar_1x2_Lowes #1"
     //, cut_partnames = hash(cuts, mat_label) // like: ["side_l", "side_l"...]    
     )  
   _i<len(mat_labels)
   ? let( mat_label = mat_labels[_i]
      , mat_name = trim(split(mat_label,"#")[0])
      , cut_str_n_sum = get_cut_sizes_str_n_sum( data, mat_label ) // [cut_string, sum of len]
      , sum = cut_str_n_sum[1]
      , nPieces = len(hash(cuts, mat_label))
      , kerf_w= hash(BOM_opt, "KERF_W", KERF_W)*nPieces
      , cut_tolerance = hash( BOM_opt, "CUTS_TOLERANCE", CUTS_TOLERANCE)
      , label = _span( str( _u(mat_label), " : ")
                        , "font-size:16px; font-weight:900; color:darkblue")
      , mat_size = join(hash(materials, mat_name)," x ")
      , remark = _span( str( " (including KERF_W*", nPieces, "="
                              , kerf_w
                              , " + CUTS_TOLERANCE=", cut_tolerance, ")"                          )
                         , "font-size:10px")      
      , _rtn = str( _rtn, "<br/>"
                  , label
                  , mat_size
                  ,"<br/>"
                  , _span( str( cut_str_n_sum[0], " => ", _b("sum: "), 
                              , _fmt(  sum+kerf_w ) 
                              )
                         , "font-size:14px")
                  , remark
                  , sum+kerf_w+ cut_tolerance
                    > hash(materials, mat_name)[0]? 
                    _red(_b("&nbsp;&nbsp;&nbsp;!! MATERIAL LENGTH EXCEEDED !!")):""
                  ,"<br/>"
                  )   
      )
    get_final_cuts_string(
              data = data
             , materials=materials
             , BOM_opt=BOM_opt
             , _i=_i+1, _rtn=_rtn)   
   : _rtn
);


//function get_title_string()=
//( _span(
//       str( "<hr/>", _b("Planter Box: "), _fmt(outer_l)
//          , " x ", _fmt(outer_w), " x ", _fmt(outer_h)
//          ,"<br/>"
//          ) 
//       ,  "font-size:20px"
//      )
//);      

function get_parts_check_string( data )=
(
  //
  //  Check if draw_parts and cut_parts are matched
  //  Both of them are hash
  //
  let( parts = hash( data, "parts")
     , cuts  = hash( data, "cuts")
     , draw_parts= [for(i = [0:len(parts)-1])
           let( copydefs = hash(parts[i], "copydefs") )
           i%2!=0?( copydefs?len(copydefs):1)
           : parts[i]
         ]
     , cut_parts= countAll( joinarr( vals(cuts)) )
     )
  str( _span("Parts Check: ", "font-weight:900;font-size:16px")
     //, "<br/>"
     , "Check if numbers of draw_parts and cut_parts are matched"
     , "<br/><br/>"
     , _span( str(
             _b("draw_parts: "), _fmth(draw_parts)
             , "<br/>"
             , _b("&nbsp;&nbsp;&nbsp;cut_parts: "), _fmth(cut_parts)
             ), "font-size:14px"
             )
     , "<br/>"
     ) 
); 
  
module echo_bom( data, materials, BOM_opt=[], title=undef)
{
  // section 1: title_string
  // section 2: cuts_arrangement_string
  // section 3: parts_check_string
  /*
  -----------------------------------------------------------------------------
  Planter Box: 13 x 12.266 x 11.06
  -----------------------------------------------------------------------------
  cedar_picket_1x6_Lowes #1 : 0.603 x 5.53 x 71
  side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794 => sum: 60.2865 (including KERF_W*5=0.3725 + CUTS_TOLERANCE=0.5)
  -----------------------------------------------------------------------------
  Parts Check: Check if draw_parts and cut_parts are matched

     draw_parts: [side_l=4, side_w=4, bottom=2, v_bar=4, h_bar=3]
     cut_parts: [side_l=4, side_w=4, bottom=2, v_bar=4, h_bar=3]  
  -----------------------------------------------------------------------------
 */
  
  echo( str( "<br/><br/>"
       , und(_span( _b( hash(data, "title"), "font-size:24px")) )
       , "<br/>"
       , get_final_cuts_string( data,materials, BOM_opt )
       , "<hr/>"
       , get_parts_check_string( data)
       ));
}


/*
================================================================

                            Demo

================================================================
*/

module Make_demo_minimal()
{
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  // a hash containing upto 4 keys: title, parts, materials and cuts
  [ 
   "title", "Make_demo_minimal"
   
  ,"parts"  // a hash w/ key = part_name, val = part_def, a hash defining part properties
  , [       
      "side_l"  // <== part_name

      , [ 
          "shape",[ "poly", ["size", [outer_l, wood_th, outer_h-wood_th ]]] 
        , "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        ]        
        
    , "side_w"
  
      , [ "shape",[ "poly", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ]]] 
        , "actions", ["transl", [0,wood_th,wood_th]]
        ]          
          
    , "bottom"

      , [ "shape",[ "poly", ["size", [ outer_l, outer_w, wood_th ]]] 
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}


  
  
  
module Make_demo_BOM()
{
  /*
      Demo how to setup for BOM (Bill of Materials) output

      1. Add "material" to each part
      2. Add "materials" to data
      3. Add "cuts" to data  
  
  */ 
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_BOM"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "shape", ["poly", ["size", [outer_l, wood_th, outer_h-wood_th ] ]]
        , "material", "cedar_picket_1x6_Lowes" // defines materials for BOM  {{ 1 }}
        , "actions", ["transl", [0, 0, wood_th]] 
        ]        
        
    , "side_w"
  
      , [ "shape", ["poly", ["size", [ outer_h-wood_th, wood_w-wood_th, wood_th ]]] 
        , "material", "cedar_picket_1x6_Lowes"  // defines materials for BOM
        , "actions", ["rot", [0,-90,0], "transl", [ wood_th,wood_th,wood_th]]
        ]          
          
    , "bottom"

      , [ "material", "cedar_picket_1x6_Lowes" // defines materials for BOM
        , "shape", ["poly", ["size", [ outer_l, outer_w, wood_th ]]] 
        ]          
    ] //_parts
  
  //========= define materials and cuts for BOM =============
  
  , "materials"  // {{ 2 }}  < mat_name >:< mat_size >
  , [ 
      "cedar_picket_1x6_Lowes", [FT6-1, wood_w, wood_th] // -1: corner cut of picket
    ] 
   
  , "cuts"       // {{ 3 }}  // < mat_label >: < part_list >
  , [                        // < mat_label > = <mat_name> or <mat_name> + "#" + n  
      "cedar_picket_1x6_Lowes #1"   
    , [ "side_l", "side_w", "bottom" ]
    ]
  ]; 

  Make( data = data );  
  
}


module Make_demo_multiple_copies()
{
  /*
      Multiple copies of a part

      --- Add "copydefs" to part definition. The value of copydefs
          is a list containing definition for each copy. Each definition
          is a hash containing actions ("rot", "transl"), "color", 
          "visible", and other settings like "showtime".
  
          , "copydefs": 
          , [ 
              ["rot",[], "transl",[], "visible",0, "color","blue", "showtime",.3 ]
            , []  // <=== a copy w/o any copy-specific definition 
            ]
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_multiple_copies"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "shape", ["poly", ["size", [outer_l, wood_th, outer_h-wood_th ] ]]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]]
        , "copydefs"     
        , [
            []
          , ["actions", ["transl", [0, -outer_w+wood_th, 0] ] ]
          ]
        ]        
        
    , "side_w"
  
      , [ "shape", ["poly", ["size", [ outer_h-wood_th, wood_w-wood_th*2, wood_th ] ]]
        , "actions", ["rot", [0,-90,0], "transl", [ wood_th,wood_th,wood_th]]
        , "copydefs"
        , [
            []
          , ["actions", ["transl", [outer_l-wood_th, 0,0] ] ]
          ]        
        ]          
          
    , "bottom"

      , [ "shape", ["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}

module Make_demo_colors()
{
  /*
      colors of a part of a copy. 
  
      -- Add "color" to the part definition for part color
      -- Add "color" to the copy definition for copy-specific color
  
      -- A color can be specified as "blue" or ["blue", 0.6]

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_colors"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["blue", 0.8]  // part color with transparancy 
      
        , "shape", ["poly", ["size", [outer_l, wood_th, outer_h-wood_th ] ]]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "copydefs"     
        , [
            []
          , ["actions", ["transl", [0, -outer_w+wood_th, 0] ]]
          ]
        ]        
        
        
    , "side_w"
  
      , [ "color", "teal"    // part color 
      
        , "shape", ["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "copydefs"
        , [
            ["color", ["brown",.9] ] // copy color
          , ["actions", ["transl", [outer_l-wood_th, 0,0] ]]
          ]        
        ]          
 
    , "bottom"

      , [ "shape", ["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}


module Make_demo_irregular_shape()
{
  // 
  // irregular shape is achieved by giving pts instead of size 
  //
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_minimal"
   
  ,"parts"  
  , [       
      "irregular"  // <== part_name

      , [ "shape",["poly", [ "pts", [ [-12, 6, 0], [-6, 11, 0], [11, 8, 0], [13, -6, 0]
                 , [-12, 6, 2], [-6, 11, 2], [11, 8, 2], [13, -6, 2]]]]
        //, "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        , "copydefs"
        , [
            []
          , ["actions",["transl", [3,3,3]] ]  
          , ["actions",["transl", [6,6,6]], "color","teal" ]
          ]
        ]     
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}


module Make_demo_visible()
{
  /*
      visibility of a part or a copy
      -- Add "visible" to the part definition for part visibility
      -- Add "visible" to the copy definition for copy-specific visibility
  
      -- A visibility can be specified as 1|0 or true|false

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_visible"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", "darkkhaki"
        , "shape",["poly", ["size", [outer_l, wood_th, outer_h-wood_th ] ]]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "copydefs"     
        , [
            []
          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]
            , "visible", 0                         // <== hide this copy
            ]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal" 
        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]]
        , "actions", ["transl", [0,wood_th,wood_th]]
        
        , "visible", 0                            // <== hide this part 
        
        , "copydefs"
        , [
            [ ] 
          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ]]
          ]        
        ]          
          
    , "bottom"

      , [ "shape",["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}

module Make_demo_showtime( $t=$t )
{
  /*
    demo the showtime settings for animation
  
    1. Setup a time sequence
    2. Assign time sequence by adding "showtime" to part definition
  
    For this particular demo, set FPS= Steps = 4 for animation

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  time_sequence=    // {{ 1 }} setup a time sequence
  [
    "bottom", .25  
  , "side_l", .5  // => part "side_l" shows up at and after $t=0.5
  , "side_w", .75
  ]; 
  
  data=  
  [ 
   "title", "Make_demo_showtime"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", "darkkhaki"
        , "shape",["poly", ["size", [outer_l, wood_th, outer_h-wood_th ] ]]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "showtime", hash( time_sequence, "side_l" )  // {{ 2 }} assign time sequence
        , "copydefs"     
        , [
            []
          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal" 
        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]] 
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "showtime", hash( time_sequence, "side_w" ) // {{ 2 }} assign time sequence
        , "copydefs"
        , [
            [ ] 
          , ["actions", ["transl", [outer_l-wood_th, 0,0] ]]
          ]        
        ]          
          
    , "bottom"       // {{ 2 }} part "bottom" is not given a "showtime"
      , [ "shape",["poly", ["size", [ outer_l, outer_w, wood_th ]]] 
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}
module Make_demo_showtime_onoff( $t=$t )
{
  /*
    demo the adv showtime settings to turn on/off the display of parts
  
    For this particular demo, set FPS= Steps = 5 for animation

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  time_sequence=    
  [
    "bottom", [ .2, .9 ]   
  , "side_l", [ 0.2, 0.8 ]  // => shows up at $t=0.2 and turn off at $t=0.8
  , "side_w", [ 0.4, 0.6 ]
  ]; 
  
  data=  
  [ 
   "title", "Make_demo_showtime_onoff"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", "darkkhaki"
        , "shape",["poly", ["size", [outer_l, wood_th, outer_h-wood_th ] ]]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "showtime", hash( time_sequence, "side_l" )  //<====== 
        , "copydefs"     
        , [
            []
          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal" 
        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "showtime", hash( time_sequence, "side_w" )  //<=====
        , "copydefs"
        , [
            [ ] 
          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ]]
          ]        
        ]          
          
    , "bottom"       
      , [ "shape",["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
        , "showtime", hash( time_sequence, "bottom" ) //<=====
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}


module Make_demo_showtime_copies( $t=$t )
{
  /*
    demo the adv showtime settings:
 
    1. copy-specific showtime
    2. adv showtime: on-off-on-off multiple times

    For this particular demo, set FPS= Steps = 10 for animation

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  time_sequence=    
  [
    "bottom", [ .1, .4, .6, .9 ]  // on-off-on-off multiple times 
  , "side_l", [ 0.2, 0.8 ]  
  , "side_w", [ 0.3, 0.8 ]
  , "side_l2", [ 0.4, 0.7 ]  
  , "side_w2", [ 0.5, 0.6 ]
  ]; 
  
  data=  
  [ 
   "title", "Make_demo_showtime_copies"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", .5]
        , "shape",["poly", ["size", [outer_l, wood_th, outer_h-wood_th ]]] 
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
          
        , "copydefs"     
        , [
            [ "showtime", hash( time_sequence, "side_l" )] // copy-specific showtime
          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]
            , "showtime", hash( time_sequence, "side_l2" )] // copy-specific showtime
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal" 
        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "showtime", hash( time_sequence, "side_w" ) // <===== part-specific
        , "copydefs"
        , [
            [ "showtime", hash( time_sequence, "side_w" )] 
          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ]
            , "showtime", hash( time_sequence, "side_w2" )]  // <===== copy-specific
          ]        
        ]          
          
    , "bottom"       
      , [ "shape",["poly", ["size", [ outer_l, outer_w, wood_th ]]] 
        , "showtime", hash( time_sequence, "bottom" )  // <===== part-specific
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}





module Make_demo_markPts()
{
  /*
      markPts on a part or a copy. 
  

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_markPts"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["blue", 0.8]  // part color with transparancy 
      
        , "shape",["poly", ["size", [outer_l, wood_th, outer_h-wood_th ] ]]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "visible", 0
        , "copydefs"     
        , [
           []
          ,  ["actions", ["transl", [0, -outer_w+wood_th, 0] ]]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal"    // part color 
      
        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ] ]]
        , "actions", ["transl", [0,0,wood_th]]
        , "copydefs"
        , [
            ["color", "brown"
            , "markPts",  ["label",["isxyz",true]] ] // show coordinate as label
          , ["actions", ["transl", [outer_l-wood_th, 0,0]]
            , "markPts", "l"]                        // show index as label
          ]        
        ]          
          
    , "bottom"

      , [ "markPts", "b"                            // show balls only
        , "shape",["poly", ["size", [ outer_l, outer_w, wood_th ]]] 
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}


module Make_demo_dim()
{
  /*
      Dimensioning
  
      Add "dims" to copydefs 
      
       , "dims"
       ,  [
            <dim>
          , <dim>
          , ...
          ]
  
      Each <dim> is [ "pqr", [a,b,c]
                    , "transl_p", [xyz]
                    , "transl_q", [xyz]
                    , "transl_r", [xyz]
                    , "opt", dim_opt (refer to the Dim() module in scadx_obj2.scad )
                    ]

                   [a,b,c]: each could be either pt or index 
                   transl_p: move the point p 
                   
                   Only "pqr" is required. The rest are optional
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_dim"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", 0.8]  
      
        , "shape",["poly", ["size", [outer_l, wood_th, outer_h-wood_th ] ]]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "visible", 1
        , "copydefs"     
        , [
             [ "markPts", "l"
             
             , "dims"                      // <=== dim
                , [ ["pqr", [3, 7,2], "transl_q", [0,0, -wood_th]]
                  , ["pqr", [2,3,7]]
                  ]
             ]  
          ,  [ "actions", ["transl", [0, -outer_w+wood_th, 0] ]
             , "markPts", "l"
             , "dims", [["pqr", [5,1,0]] ] // <=== dim
             ]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal"    // part color 
      
        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]]
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "visible", 1
        , "copydefs"
        , [
            [] // show coordinate as label
          , [ "actions", ["transl", [outer_l-wood_th, 0,0]]
            ]                        // show index as label
          ]        
        ]          
          
    , "bottom"

      , [ "shape",["poly", ["size", [ outer_l, outer_w, wood_th ]]] 
        //, "markPts", "l"
        , "copydefs"
        , [ 
            [ "dims"  // <=== dim
            , [
                ["pqr", [4,7,3], "opt", ["label",["rot", [180,0,0 ]]]]
              ]
            ]         
          ]        
        ]          
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}

module Make_demo_echoPts()
{
  /*
      set echoPts = 1 to echo pts to the console.

  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_echoPts"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["blue", 0.8]   
      
        , "shape", ["poly", ["size", [outer_l, wood_th, outer_h-wood_th ] ]]
        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
        , "echoPts", 1       // <===========
        , "copydefs"     
        , [
            ["echoPts", 1]   // <===========
          , ["echoPts", 1    // <===========
            , "actions", ["transl", [0, -outer_w+wood_th, 0] ]]
          ]
        ]                 
          
    ] //_parts
  
  ]; 

  Make( data = data );  
  
}


module Make_demo_cutoffs()
{
  /*
     How to do "cutoffs" out of the object.
  
     This demo shows a cutoff, "handle_holl", is applied to part "side_l"
     in its partdef ( so each copy of the same "handle_hole" cutoff), and
     another one, "groove_l", is applied to both copies of "side_l" 
     separately (so each copy has the groove_l but at different locations) 
  
     1. Add a "cutoffs":<h_cutoff_defs> pair to *parts* : 
  
        , "cutoffs"
        , [ <cutoff_name>, <cutoff_def>, <cutoff_name>, <cutoff_def>  ...]
  
        <cutoff_def> is a stripped-down version of partdef or copydef, containing
        only up to 2 keys: "actions" and ( "size" or "pts" ):
  
            [ "size", [ ... ]
            , "actions", ["transl",[...] ]   // <=== optional
            ]
  
        This serves as a cutoff directory and could be referred to multiple times 
        inside the code for multiple copies of a cutoff.
  
     2. Add "cutoffs":<h_cutoff_def> pair to *partdef* for part-specific cutoffs.
        
         , "cutoffs"
         , ["handle_hole",[]]  
         
     3. Add "cutoffs":<h_cutoff_def> pair to *copydef* for copy-specific cutoffs.
            
         , "cutoffs"
         , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]] ]
         
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_cutoffs"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", 0.9]   
      
        , "shape",["poly", ["size", [outer_l, wood_th, outer_h-wood_th ]]] 
        , "actions", ["transl", [0, 0, wood_th+0.001]]
        , "cutoffs", ["handle_hole",[]]                  // <==== {{ 2 }}
        , "visible",1
        , "copydefs"     
        , [
            [ "cutoffs", ["groove_l", [] ]              // <==== {{ 3 }}
            ]
            
          , [ 
              "actions", ["transl", [0, outer_w-wood_th, 0] ]
            , "cutoffs"
            , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]]  // <==== {{ 3 }}
              ]
            ]
          ]
        ]        
        
    , "side_w"
  
      , [ "color", "teal"     
      
        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]] 
        , "actions", ["transl", [0,wood_th,wood_th]]
        , "visible", 0
        , "copydefs"
        , [
            [ ] 
          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
          ]        
        ]          
          
    , "bottom"

      , [ "color", "brown"
        , "visible", 0
        , "shape",["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
        ]          
          
    ] //_parts
  
  , "cutoffs"   // <================================= {{ 1 }}  
  , [
       "groove_l"
       
         , [ "shape",["poly", ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]]
           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
           ]
         
    , "handle_hole"
     
        , [ "shape",["poly", ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]]
          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
          ]
  
    ]
  ]; 

  Make( data = data );  
  
}




module Make_demo_shape_cube_cutoffs()
{
  /*
     How to do "cutoffs" out of the *** Primitive *** object.
         
     New: set shape in partdef to primitive :
  
       , "shape"
       , [ "cube", ["size", n | [x,y,z] ] ]
    
     This is needed 'cos poly can't display cutoffs properbably   
  */   
  
  wood_th = 0.603;
  wood_w = 5.53;
  
  outer_l= 13; 
  outer_w= wood_w;   
  outer_h= wood_w + wood_th;
  
  data=  
  [ 
   "title", "Make_demo_shape_cube_cutoffs"
   
  ,"parts"  
  , [       
      "side_l"  

      , [ "color", ["darkkhaki", 0.9]   
      
        , "shape",[ "cube"        // shape set to cube but not poly 
                  , ["size", [outer_l, wood_th, outer_h-wood_th ]]]  
        , "actions", ["transl", [0, 0, wood_th+0.001]]
        , "cutoffs", ["handle_hole",[]]                  // <==== {{ 2 }}
        , "visible",1
        , "copydefs"     
        , [
            [ "cutoffs", ["groove_l", [] ]              // <==== {{ 3 }}
            ]
            
          , [ 
              "actions", ["transl", [0, outer_w-wood_th, 0] ]
            , "cutoffs"
            , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]]  // <==== {{ 3 }}
              ]
            ]
          ]
        ]        
        
//    , "side_w"
//  
//      , [ "color", "teal"     
//      
//        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]] 
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        , "visible", 0
//        , "copydefs"
//        , [
//            [ ] 
//          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
//          ]        
//        ]          
//          
//    , "bottom"
//
//      , [ "color", "brown"
//        , "visible", 0
//        , "shape",["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
//        ]          
          
    ] //_parts
  
  , "cutoffs"   // <================================= {{ 1 }}  
  , [
       "groove_l"
       
         , [ "shape",["cube"      // shape set to cube but not poly
                    , ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]]
           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
           ]
         
    , "handle_hole"
     
        , [ "shape",["cube"      // shape set to cube but not poly
                  , ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]]
          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
          ]
  
    ]
  ]; 

  Make( data = data );  
  
}

//===============================================================

//Make_demo_minimal();
//Make_demo_BOM();
//Make_demo_multiple_copies();
//Make_demo_colors();
//Make_demo_irregular_shape();
//Make_demo_visible();
//Make_demo_showtime( $t=$t); // Steps = 4 for animation
//Make_demo_showtime_onoff( $t=$t); // Steps = 5 for animation
//Make_demo_showtime_copies( $t=$t); // Steps = 10 for animation
//Make_demo_markPts();
//Make_demo_dim();
//Make_demo_echoPts();
//Make_demo_cutoffs();
Make_demo_shape_cube_cutoffs();

