include <../scadx/scadx.scad>
include <../doctest/doctest.scad> 
 
/*
  scadx_make2.scad
  
  ==============================================================================
  Git Branch "Unite-parts" (20170628): Unite "parts" and "namedParts" in "assemblies"
  ====================================
  
  Instead of having "parts" and "namedParts" in "assemblies":
  
            data= [ 
             "partlib", [ "board1", [ "shape","cube"
                                     , "sizedef", ["size",[10,1,4]]
                                    ]
                        ,  "board2", [ "shape","poly"
                                     , "sizedef", ["size",[10,1,4]]
                                    ]
                        ]
  
             ,"assemblies" 
            , [  
                "long-side", 
              ,  [ 
              
                  "parts", [ [ "shape","cube"
                             , "sizedef", ["size",[10,1,4]]
                             , "color", ["teal", 0.7]
                             ] 
                           ]
               
                , "namedParts", ["board1"
                                 ,["board2", ["actions", ["transl",[1,1,0]]
                                             , "color""teal"]
                                  ] 
                                ]
                 ]
              ]   
            ]; 
            
            
  Change to use "parts" only:
  
            data=[ 
             "partlib", [ "board1", [ "shape","cube"
                                     , "sizedef", ["size",[10,1,4]] // partlibMod
                                    ]
                        ,  "board2", [ "shape","poly"
                                     , "sizedef", ["size",[10,1,4]]
                                    ]
                        , "board3", [ "shape"
                                      , [ "board1"
                                        , ["shape", "board2", "color","red"]
                                           // comboPartMod
                                        ] 
                                    , "color", "blue"  // comboMod
                                    ]                         ]
  
             ,"assemblies" 
            , [  
                "long-side", 
              ,  [ 
              
                  "parts", [ [ "shape","cube"
                                       , "sizedef", ["size",[10,1,4]]
                                       , "color", ["teal", 0.7]
                             ]
                           , [ "shape", "board1" ]
                           , [ "shape", "board2"
                                       , "color", "teal" // partMod
                                   
                             ]
                           ] 
                 , <assemMod>            
                 ]
              ]   
            ];   
  
   The change will also provide a consistent API between "assemblies/parts" and 
   "cutoffs"
   
   =======================================================================
 
     // partlibMod
     // comboPartMod
     // comboMod
     // assemMod
     // partMod
     // copies
  
  -----------------------------------------
  -- 170605 : rewrite based on flowchart
  -- 170601 : pass Make_demo_minimal
  
  Make obj with the options of producing BOM.  
  
  -- 170527_2: Rename data.cutoffs to data.partlib, and cutoffs_dir to partlib.
               (considering to add "addons" later, so partlib will be a store
                for both cutoffs and addons)
  -- 170527: bug fix for logging
  -- 170526: 1) Change: ("shape", "poly", "size", [...]) or ("shape", "cube", "r", 2)
                to: ("shape, "poly", "sizedef", ["size", [...]] )
             2) Complete 1st draft of bug-free colored, layered debugging system  
  -- 170524: 1) Change: ("shape", ["poly", ["size",[...]]]) in partdef changed to
                 ("shape", "poly", "size", [...]). For primitive like cube:
                 ("shape", "cube", "size", [...]) or ("shape", "cube", "r", 2)
             2) Working on a colored, layered debugging system  
  -- 170522 : Able to use primitive "cube" in both partdef and cutoffs
  -- 170521_2: Change: ("size", [...]) in partdef changed to 
               ("shape", ["poly", ["size",[...]]]) to ready for incorporation
               of primitive solids  
  -- 170521: Rename Makes() to Make()annd move from makes/makes.scad to scadx_make.scad
  -- 170520: cutoff implanted.
  -- 170519: Make copydef consistent with partdef by adding "actions" key ( i.e., 
             ["actions",["rot",[...]] instead of just ["rot",[...]];
  -- 170518.2: Code clean-up and restructuring inside Makes(); Prep for *cutoffs*;
  -- 170518: Add 2 major features: markPts and dim
  -- 170517: Incorporate materials to data; Provide error msg when both part.pts 
             and part.size are not given (means there is no pts to draw); 
             Re-write demos;
             
  -- 170516: apply showtime (using the newly developed get_animate_visibility()
             in scadx_animate.scad ) allowing each part, or any copy of it, to
             show/hide during animation
             
  -- 170515_2: remove _digest_part_copy_pts(); add new key "actions" to partdef, allows
               all copies of a part be acted upon at once. So the transPts originally
               only acted on copydefs, now it first acts on partdef.actions (for all
              copies) then copydefs (for individual copy);
              no need for conversion of data to opt_processed data
               
  -- 170515: rewrite digest_actions to fix individual "visible" and "color" 
  -- 170514_2: rename: opts => copydefs, digest_part_copy_pts()=> digest_part_copy_pts()
  -- 170514: fix bugs( undef title ), add new features( parts and individual 
             copydefs can be visible=false)
  -- 170512: add BOM_opt for Makes()
  -- 170511： done. For obj that requires BOM, see Make_demo_has_materials___PlanterBox().
             otherwise see Make_demo_no_materials___Patio( )
  -- 170510: "Making" section works nicely. To do: BOM section
  -- Start coding 170509, attempt to combine rough_data, materials and 
     cuts_arrangement into one *data*
  
  materials:  // defines materials only when BOM is needed
  [ 
    <material_name1>, [x,y,z]
  , <material_name2>, [x,y,z]
  ]
     
data=  // a hash containing upto 4 keys: title, parts, materials and cuts
  [ 
   "title", "Make_demo_minimal"
   
  ,"partlib"
  , [       
      "long_board"  // <== partname

      , [ 
          "shape", "poly"  // <== shape type, like "cube", "cylinder", "poly". Default: "cube" 
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
        , "actions", [ ... ]
        ]        
        
    , "short_board"
  
      , [ "shape","cube" // <== use the built-in primitive
        , "sizedef", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ] ]
        ]          
          
    , "long_board2"

      , [ "shape","poly" // skip "sizedef", give "pts" directly
        , "pts", [[13, 0, 0.603], [0, 0, 0.603], [0, 5.53, 0.603], [13, 5.53, 0.603]
                 ,[13, 0, 0], [0, 0, 0], [0, 5.53, 0], [13, 5.53, 0]]
        ]   
        
   ] //_ partlib
    
  
  ,"assemblies" // a hash w/ key = partname, val = partdef (=a hash defining part properties)
  , [       
      "side_l"  // <== objname
      , [ "body", [ "long_board" 
                  , [ "color",[]
                    , "actions", ["transl", [0, 0, wood_th]]] // "rot" or "transl"
                    , "markPts", [ ]
                    , "dims", [ ]
                    , "copies"
                    , [ [] 
                    ,   [ actions:[] | color:[ ] | markPts:[] | dims:[ ] ]
                      ] 
                    ]  
                  ]
       ]
       
      , "side_w"
      , [ "body", [ "short_board"
                  , [ "actions", ["transl", [0,wood_th,wood_th]] ]
                  ]
        ]          
          
      , "bottom"
      , [ "body", "bottom"   // if no modification, use partname
        ]   
   
      
    ] //_ assemblies
    
        
*/ 

 //: Parameters
/*
##########################################################################
      Set units
##########################################################################
*/

FT6= 72;
FT8= 96;
FT10= 120;

// Actual lengths in inches
LEN1= 0.75;
LEN2= 1.5;
LEN4= 3.75;
LEN3= LEN2+(LEN4-LEN2)/2; 
LEN8= 7.25;

/*
##########################################################################
      Set user parameters
##########################################################################
*/

// Width of saw blade in inch. Used in BOM string output.
KERF_W= 0.0745; 

// Set a TOLERANCE (in inch) in case of cutting error 
// This is added to the sum of length calc in 
// get_final_cuts_string(). Used in BOM string output.
CUTS_TOLERANCE = 0.5;  

// The distance all parts move to break up the box for the purpose
// of demo each part clearly. Set to 0 to see the intact box.
// For animation, set to 2*$t; 
break_dist=0; 
BD = break_dist; 

//=======================================================  

        
//=======================================================  

MAKE_RESERVED_SHAPE_NAMES=["cube","sphere","cylinder","poly"];

    LOG_PARAMS= ["LOG_LEVEL",10]; 
    
    LEVEL_ASSEM= 1;
    LEVEL_COPY= 2;
    LEVEL_PART= 3;
    LEVEL_CUTOFF= 4;
    LEVEL_MARKPT= 5;
    LEVEL_DIMS = 5; 
    
   RESERVED_SHAPE_NAMES=["cube","sphere","cylinder","poly"];


//. == Make2() =================
module Make2( data
            , title= undef
            , log_level = 10
            , log_target= "all"
            
            , _partlib = []
            , _assems  = []
            
            , _i=0 
            )  
{ //: Make2
  
   /* 
     data: [ "title", "..." // title string to be replaced by the title in Make(title ...)
           , "parts", [ <partname>, <partdef>, <partname>, <partdef> ... ]
           , "partlib", [ <cutoffname>, <cutoffdef>, <cutoffname>, <cutoffdef> ... ]
  
           , "materials", []
           , "cuts", [  ]  // No need if BOM is not wanted
           ]
     
     "parts"
     , [ <partname>, <partdef>, <partname>, <partdef> ... ]
  
 
     obj_name: "side_bar"
     data:
          [ "color", "brown"
          , "material", ["1x3", 25, 2]
          , "x", 25, "y", 0.75, "z", 3
          , "faces", [[3,2,1,0], [4,5,6,7], [0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
          
          , "opts", [ [ "color", "red" ]  //<=== each opt is digested except color
                    , [] 
                    ]
          , "pts", [[pt, pt, pt, ...] ]   // <=== created pts if not existed
          ]  


   possible markPts value:
 
      "g" 
      "l;cl=green" 
      "b=false;g;l;cl=blue"
       ["cl=green", "grid",true]
      "cl=blue;g=[color,gold,r,0.05]" );
      "g=[color,blue]"
      "cl=red;l=test" );
      "cl=green;l=[isxyz,true]" );
      ["cl=blue;r=0.2"
      ,"label",["scale=0.015"
               ,"isxyz",true
               ,"tmpl","{*`a|d3|wr=,in}"
               ]
      ]
      ["cl=purple;ball=false"
      ,"label",["scale=0.05"
               ,"isxyz",true
               ,"tmpl","{*`a|d3|wr=,in}"
               ]
      ]
      
    MarkPts( trslN(pqr,10), "ch;cl=teal;rng=1;l=AB"); 
    MarkPts( trslN(pqr,12), "ch;cl=purple;rng=[0,-1];l=BC"); 
    MarkPts( trslN(pqr,14), "ch;cl=black;rng=1;l=[isxyz,true]"); 
  "l;ch;echopts"
  ["samesize",true]
  "samesize,label||color=red"
  "g;c;ch")
  
   */
      
   if(_i==0) //: _i=0 (initiation)
   {
      echo( _mono(_b("--------------------------- Start Make() ---------------------------" )));

      title = und(_span( _b( hash(data, "title"), "font-size:20px")) );
      echo(title); 
      log_main( "Make() initiating ... " );
      
      _partlib = hash( data, "partlib", []);      
      _assems  = hash(data, "assemblies"); // so we don't need to do it every cycle
      
      log_main( ["_partlib", _fmth(_partlib) ] );
      log_main( ["_assems", _fmth(_assems) ] );   
      
      if( _assems )
      {  
       Make2( data
            , title     = title
            , log_level = log_level
            , log_target= log_target
           
            , _partlib  = _partlib
            , _assems   = _assems
            
            , _i=1  
            );      
      }
      else { log_error( "data.assemblies not given. NOthing to make." ); }      
   }
   else if( _i>= len( _assems ) ) //: _i> len (Ending)
   {     
     echo( und(_span( str("Leaving ", _b( hash(data, "title"))), "font-size:20px")) );
      
     echo( _mono(_b("--------------------------- Done Make() ---------------------------" )));

     materials = hash( data, "materials" );  
     if (hash(data, "cuts")) 
        echo_bom( data, materials, BOM_opt ); 
   }
   
   else if (_i%2!=0) //: Loop _i thru assemblies
   {
     log_assem_b(["Loop assems, _i",_i],add_ind=-1);
      
      assemName = _assems[_i-1];
      assem  = _assems[_i]; // assem: [ "parts": [ <hPart>, ... ]
                            //        , <hModifier>  (including color, etc)
                            //        , "cutoffs": [ <hPart>, ...]
                            //        , "copies": [ <hModifier>, ...]
                            //        ] 
      
      parts = hash( assem, "parts" );  
      
      assemCopies= hash(assem, "copies", [[]]);
      assemColor = hash( assem, "color" ); // DO NOT set default here. We
                                           // need an undef later
      //Remove some keys to get pure assemModifier:                              
      assemMod= delkey(assem, "parts"); 
      
      
      log_assem_b( [ "assemName"
                   , _Make_get_name_and_colorMark( _b(assemName), assemColor) 
                   ] );
      log_assem( ["assem", assem ] );
      log_assem( ["assemblies.parts", parts]);
      log_assem( ["assemModifier", assemMod] );  

      // Obj chain (modifier order):
      //
      //  1. libPartMod
      //  2. comboPartMod
      //  3. comboMod
      //  4. partMod
      //  5. assemMod
      //  6. copies
          
      log_part_b("Resolve assem.parts with <b>_Make_get_parts_by_shape()</b>");      
      _aAllParts = _Make_get_parts_by_shape(
               parts, _partlib, premod= assemMod);
      
      // Giving all no-name parts a name:
      aAllParts = assign_unNamed_parts(_aAllParts);
              
      log_part( ["aAllParts", aAllParts]);
      log_part( ["aAllParts", str("<br/>",arrprn(aAllParts))]);      
      log_part_e("Done with <b>_Make_get_parts_by_shape()</b>");  
      
      
      log_assem( [ _s("assem.copies ({_} copies)", len(assemCopies)), assemCopies ]);
      
      for( copy_i= [0: len( assemCopies)-1] )
      {  
        copyMod = assemCopies[copy_i];
        log_copy( _span(_s("<u>Looping {_}.copies</u> #{_}"
                    , [_b(assemName)
                      , _b(copy_i+1)
                      ]), s="font-size:14px;background:#eeeeff"), add_ind=-1 );                            
        log_copy( ["copyMod", copyMod] );
        Draw_a_copy_of_assemParts( assemName, assemColor
                                 , _partlib
                                 , aAllParts
                                 , copyMod 
                                 , copy_i+1 );
      }        

       Make2( data      = data
            , title     = title
            , log_level = log_level
            , log_target= log_target
            
            , _partlib  = _partlib
            , _assems   = _assems
            
            , _i=+2 
            );
     
    }//_ if (_i%2!=0)
        
     //............................................
     module Draw_a_copy_of_assemParts( assemName, assemColor
                                 , partlib
                                 , aAllParts
                                 , copyMod 
                                 , copy_i )

      {
        copyName = str( assemName, "#", copy_i);
        copyColor= hash( copyMod, "color");//, assemColor ); 
         
       log_copy_b( _u(_s(" Entering <b>Draw_a_copy_of_assemParts()</b> to draw {_}"
                        , _b(_Make_get_name_and_colorMark(copyName, copyColor)) ))
                    );
        
        log_copy(["copyMod",copyMod]);
        
        log_copy( _s("Draw each part defined in <b>{_}</b>'s <b>parts</b> (= aAllParts ) for this copy", _b(assemName)));
           
        for( part_i = [0:len(aAllParts)-1] )
        {
          partName = hash(aAllParts[part_i],"name");
          
          //part = _Make_update_part( aAllParts[part_i], copyMod);
          part = aAllParts[part_i];
          
          partColor = hash(part,"color");
          partName_and_colorMark= _Make_get_name_and_colorMark( partName, partColor );
          
          log_part_b( _s("Looping <b>{_}.aAllParts</b>[{_}]: partName={_}"
                      , [assemName, _b(part_i), _b(partName_and_colorMark)] )
                     );
          log_part(["part", part]);
          
          shape = hash( part, "shape" );

          _pts =  hash( part, "pts", get_part_pts( part ));
          pts = transPts( _pts, hash(copyMod, "actions",[]) );
          
          log_part( ["pts", pts] ); //get_part_pts( part )] );
          
          
          if(shape=="poly"&&!pts )  // if pts not existing, 
          {
            log_error
            ( 
               str( "part \"", partName, "\" doesn't provide points info. In partlib, add "
                  , shape=="poly"
                    ? str("either ", _u("[\"pts\", &lt;pt list>]")
                         , "or ", _u("[\"sizedef\", [\"size\", [x,y,z]]]") )
                    : _s("<u>[\"sizedef\", &lt;hash of {_}'s sizedef>]</u>", shape)
                 , _s(" to {_}'s <b>partdef</b> .", partName)
                 )
            , indent=2
            );
          }
          else {
            
            log_part( _s( shape=="poly"?"partDef.pts exist. Draw them"
                                       :str("Draw a ", shape)
                         , "Draw for copy {_}", _b(copyName) ));
            //log_part([ "assemColor", assemColor
                     //, "partColor", hash(aAllParts[part_i][1], "color")
                     //, "copyColor", copyColor
                     //]);
            color = isstr(copyColor)?[copyColor,1]:copyColor; 

                Draw_a_part( assemName
                           , partlib
                           , partName
                           , part= shape=="poly"?update(part, ["pts",pts]):part
                           , modifier= copyMod
                           , copy_i = copy_i
                           , part_i = part_i 
                           );

          }  
        log_part_e(str("Done ", _b(partName)));
        }         
        
        log_copy_e( _u(_s(" Leaving <b>Draw_a_copy_of_assemParts()</b> for {_}"
                        , _b(copyName) ))
                    );
       
      }  //: END Draw_a_copy_of_assemParts                    


  module Draw_a_part( assemName   //: Draw_a_part (or cutoff)
                    , partlib
                    , partname
                    , part 
                    , modifier=[]
                    , copy_i= 0
                    , part_i= undef
                    , iscutoff=0) 
   {      
     LEVEL = iscutoff?LEVEL_CUTOFF:LEVEL_PART;
     ADD_IND= iscutoff?0:2;
     type = iscutoff?"cutoff":"part";
     log_b(_s("Entered Draw_a_part() for <b>{_}</b> ({_}_{_})"
             , [ str(partname,".copy_", copy_i)
               , type
               , part_i //cutoff_i==undef?"part":str(".cutoff_",cutoff_i)
               ]),LEVEL, add_ind=ADD_IND);
     
      part = _Make_update_part(part, modifier);
      //partname = namedPartDef[0];
      shape = hash( part, "shape" );
      faces = hash( part, "faces", rodfaces(4) );
      old_pts = hash( part, "pts" );

      //actions= concat( hash(part, "actions", [])
      //                         , hash(modifier,"actions",[]) 
      //               );     
      actions= hash(part, "actions", []);
                     
      //log_( ["part", _fmth(part)], LEVEL, add_ind=1 );
      log_( ["part", (part)], LEVEL, add_ind=ADD_IND );
      log_( ["modifier", _fmth(modifier)], LEVEL, , add_ind=ADD_IND );
      log_( ["actions (from part and modifier)", _fmth(actions)], LEVEL,  add_ind=ADD_IND );
      
      pts = old_pts; 
        
      _color= hash( part, "color" );
      color= isstr(_color)?[_color,1]:_color; 
      cutoffs = hash( part, "cutoffs",[]);
      log_( ["cutoffs", _fmth(cutoffs)], LEVEL, , add_ind=ADD_IND );
      //..................
      
      difference()
      { 
        Draw_main_part();
        if(cutoffs 
          && part_i==0  // This is required to limited the cutoffs is/are
                        // drawn only once, preventing unnecessary duplicates
          ) 
        Draw_cutoffs(assemName, partlib, partname, cutoffs, modifier);
      }
      
         //.......................................................................
         module Draw_main_part()
         { 
            color( color[0], color[1] )
           if(shape=="poly") 
           {
              log_( "<u>Drawing a <b>\"poly\"</b></u> using polyhedron()", LEVEL,add_ind=ADD_IND );
              log_( ["pts", pts ], LEVEL,add_ind=ADD_IND);
              log_( ["faces", faces ], LEVEL,add_ind=ADD_IND );
                
              polyhedron( pts, faces) ; 
           }
           else
           { 
             sizedef= hash( part, "sizedef" );
             
             log_( _s("<u>Drawing a <b>\"{_}\"</b> using Primitive()</u>", shape), LEVEL,add_ind=ADD_IND );
             log_( ["sizedef: ", sizedef], LEVEL,add_ind=ADD_IND );
             //log_( ["actions", actions], LEVEL,add_ind=ADD_IND );
             
             Transforms( actions )
             Primitive( shape, sizedef ); 
           }
         } //_ draw_main_part()
         //.......................................................................
         module Draw_cutoffs(assemName, partlib, partname, cutoffs, modifier=modifier)
         {
           log_b("Entered <b>Draw_cutoffs()</b>", LEVEL_CUTOFF, add_ind=ADD_IND);
           cutoffs = _Make_get_parts_by_shape(
                       hash( part, "cutoffs", [] )
                       , partlib=partlib );
           log_( str("<u>Drawing cutoffs, cutoffs= </u>", cutoffs), LEVEL_CUTOFF, add_ind=ADD_IND );
           log_( ["modifier",modifier], LEVEL_CUTOFF, add_ind=ADD_IND );
           
           //cutoffs= _Make_get_parts_by_shape( cutoffs );
           //for( i=[0:len(cutoffs)-1])
           //{
              //Draw_a_part( assemName
                         //, partlib = partlib
                         //, partname= partname //str(partname, ".copy_", copy_i)
                         //, part= cutoffs[i]
                         //, modifier=modifier 
                         //, copy_i= copy_i
                         //, part_i= i
                         //, iscutoff=1
                         //) ;
           //} 
           for( i=[0:len(cutoffs)-1])
           {
              _cutoff = cutoffs[i];
              shape= hash(_cutoff, "shape", "cube");
              log_(["shape",shape], LEVEL_CUTOFF, add_ind=ADD_IND );
              part_in_lib= hash(partlib, shape);
              cutoff = part_in_lib? update_def( part_in_lib
                                              , delkey(_cutoff,"shape")
                                              ):_cutoff; 
              Draw_a_part( assemName
                         , partlib = partlib
                         , partname= partname //str(partname, ".copy_", copy_i)
                         , part= cutoff
                         , modifier=modifier 
                         , copy_i= copy_i
                         , part_i= i
                         , iscutoff=1
                         ) ;
              //Draw_a_part( cutoff, modifier=[] );
           }
           log_e("Leaving <b>Draw_cutoffs()</b>", LEVEL_CUTOFF, add_ind=ADD_IND);
         }  
         //.......................................................................

   
//     markPts = hash(assemblydef, "markPts");
//     if(markPts) 
//      {  
//        log_markPts( str("markPts: ", markPts), indent=8);
//        MarkPts(pts, markPts);
//      }
//      
//      dims = hash(assemblydef, "dims");
//      if(dims) Draw_dims(assemblydef, dims);
//        /// under construct

      log_e(_s("Leaving Draw_a_part() for {_} <b>{_}</b>",[type, partname]),LEVEL, add_ind=ADD_IND);
     
   } //: End Draw_a_part
          

   //------------------------------------------------
   function getCubePts(xyz)=
   (
     [ for (p= cubePts( [[xyz.x,0,0], ORIGIN, [0,xyz.y,0]], h=xyz.z ) )
       p+[0,0,xyz.z]
     ]
   );
   
   //------------------------------------------------
   function digest_actions( pts, def=[], _actions=[], _i=-1 )= 
   (                 // def contains "actions" key 
     def==[]? pts
     : _i==-1? digest_actions( pts, def
                             , hash( def, "actions"), _i=0 ) 
     : !_actions || _i> len(_actions)? pts
     : _i%2==0? digest_actions( pts=pts, def=def, _actions=_actions, _i=_i+1)
     : let( key= _actions[_i-1]
          , val= _actions[_i]
          , pts= transPts( pts, [key,val] )
          )
       digest_actions( pts=pts, def=def, _actions=_actions, _i=_i+1)
            
   );//digest_actions     
     
     
   //------------------------------------------------
   function get_part_pts( hPartDef )=
   (
     // Given a partdef, resolve the source of pts (either pts or size)
     // and apply hash( partdef, "actions" )
     //
     // Return pts 
     
     /*
     "side_l"  // <== partname

       partdef: 
      , [ 
        , "shape","poly"
        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
        , "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        ] 
        
       partdef: 
      , [ 
        , "shape","cube"
        , "sizedef", ["size", [x,y,z] ]
        , "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
        ] 
        
     */    
     let( shapename = hash( hPartDef, "shape")
        //, shapedata = shapedef[1]
        ,  _pts = hash( hPartDef, "pts")  // if "pts" is given , use it. Otherwise, 
                                         // get it from the size
        , size = hashs( hPartDef, ["sizedef","size"])
        , no_pts =  !_pts && !size 
        )
     no_pts? undef
     : let( pts = _pts?_pts: getCubePts( size)
          , actions = hash( hPartDef, "actions" )
          )
       actions? transPts( pts, actions): pts  

   ); 
   
   //------------------------------------------------

  
  
   //module Draw_part_cutoffs( cutoff_defs, _partlib, part_actions, copy_actions)
   //{  Draw_cutoffs( "part", cutoff_defs, _partlib, part_actions, copy_actions); }
      //
   //module Draw_copy_cutoffs( cutoff_defs, _partlib, part_actions, copy_actions)
   //{  Draw_cutoffs( "copy", cutoff_defs, _partlib, part_actions, copy_actions); }
          //
   //module Draw_cutoffs( part_or_copy
                      //, cutoff_defs
                      //, _partlib
                      //, part_actions
                      //, copy_actions
                      //)
   //{
     //
    //log_cutoff( _u(_s("<b>Draw_cutoffs</b>( \"{_}\", cutoff_defs, _partlib, part_actions, copy_actions) ", part_or_copy) ), indent=5); 
    //log_cutoff( [ str( part_or_copy, " cutoff_defs: "), cutoff_defs], indent=6);  
     //
    //cutoff_names = keys(cutoff_defs);    //: ["handle","groove"]
     //
    //
    //log_cutoff( ["Looping through cutoff_names: ", cutoff_names], mode=2, indent=6);
      //
    //for( cutoff_name= cutoff_names )
    //{      
      //log_cutoff(["cutoff_name", cutoff_name], mode=2, indent=6);
      //
      //cutoff_init_def = hash( _partlib, cutoff_name);
      //log_cutoff( [ _s("{_}'s <b>cutoff_init_def</b> (= data.cutoffs)", cutoff_name)
                  //, _fmth(cutoff_init_def)], mode=0, indent=7 );
      //
      //
      //cutoff_def = hash( cutoff_defs, cutoff_name ); // cutoff_defs could be part_cutoff_defs
                                                     // or copy_cutoff_defs 
      //log_cutoff( ["cutoff_def", cutoff_def], indent=7 );
        //
      //cutoff_actions_sets= concat( part_actions
                                   //, hash( cutoff_def, "actions", [] )
                                   //, copy_actions
                                   //); 
      //log_cutoff( ["cutoff_actions_sets", cutoff_actions_sets], indent=7 );      
      //
      //_cutoff_pts= hashs( _partlib, [cutoff_name,"pts"]);       
      //cutoff_pts = transPts( _cutoff_pts, cutoff_actions_sets);      
      //log_cutoff( [ str(part_or_copy, " cutoff_pts"), cutoff_pts], indent=7 );
      //
      //cutoff_shape_name = hash( cutoff_init_def, "shape" );
        //
      //if( cutoff_shape_name=="poly")
      //{
        //faces = hash( cutoff_def, "faces", rodfaces(4));
        //log_cutoff( ["faces",faces], indent=7 );
        //log_cutoff( "Drawing poly", indent=7 );
        //polyhedron( cutoff_pts, faces );
      //}
      //else{ 
        //actions_sets_2 = concat(
             //get_cutoff_init_actions( _partlib, cutoff_name )
             //, cutoff_actions_sets
             //);
        //log_cutoff( str("Drawing a ", cutoff_shape_name), indent=7 );
        //log_cutoff( ["actions_sets_2", actions_sets_2], indent=7 );
        //cutoff_sizedef= hash( cutoff_init_def, "sizedef");
        //log_cutoff( ["cutoff_sizedef", cutoff_sizedef], indent=7 );
        //
        //Transforms( actions_sets_2 )
        //Primitive( cutoff_shape_name, cutoff_sizedef );        
      //} //_if( cutoff_shape=="poly")  
      //
      //markPts = hash(cutoff_def, "markPts");
      //if(markPts) 
      //{  
        //log_markPts( str("markPts: ", markPts), indent=8);
        //MarkPts(cutoff_pts, markPts);
      //}
      //
      //dims = hash(cutoff_def, "dims");
      //if(dims) Draw_dims(cutoff_pts, dims);
//
    //} //_for( cutoff_name= cutoff_names )   
//
  //log_cutoff( _u("Leaving <b>Draw_cutoffs</b>()"), indent=5 ); 
  //} 
  

   //------------------------------------------------
   module Draw_dims( pts, dims )
   {    
      // dims = [ dim_data, dim_data, ... ]
      // dim_data= [
      //      "pqr", [...]   ==> each point could be a pt, or an int as the 
      //                          index 
      //      ,"transl_p", [xyz]
      //      ,"transl_q", [xyz]
      //      ,"transl_r", [xyz]
      //      , "opt", []
      //      ]
      log_dim( "Draw_dims(pts,dims)");
      log_dim(["target pts", pts, "dims", dims] );
      
      for( dim = dims )
      {  
        __pqr = hash( dim, "pqr" );
        _pqr = [ for(x= __pqr) isint(x)?pts[x]:x ];
        pqr = [ _pqr[0]+ hash( dim, "transl_p", [0,0,0])
              , _pqr[1]+ hash( dim, "transl_q", [0,0,0])
              , _pqr[2]+ hash( dim, "transl_r", [0,0,0])
              ];
        log_dim( ["pqr", pqr] );
        
        Dim(pqr, hash(dim, "opt",[]) );      
        
      } 
    }
   //------------------------------------------------

 
} //: == End Make2()     
    
//. == log modules =============== 
    
   module log_main(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_(s, 0, mode, add_ind, debug=debug, hParams=hParams);}
   module log_error(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_(s, [0,"red"], mode, add_ind, debug=debug, hParams=hParams);}
                  
   module log_assem(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_(s, LEVEL_ASSEM, mode, add_ind, debug=debug, hParams=hParams);}
                  
   module log_assem_b(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_b(s, LEVEL_ASSEM, mode, add_ind, debug=debug, hParams=hParams);}
   module log_assem_e(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_e(s, LEVEL_ASSEM, mode, add_ind, debug=debug, hParams=hParams);}

   module log_copy(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_(s, LEVEL_COPY, mode, add_ind, debug=debug, hParams=hParams);}
                  
   module log_copy_b(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_b(s, LEVEL_COPY, mode, add_ind, debug=debug, hParams=hParams);}
   module log_copy_e(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_e(s, LEVEL_COPY, mode, add_ind, debug=debug, hParams=hParams);}
   
   module log_part(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_(s, LEVEL_PART, mode, add_ind, debug=debug, hParams=hParams);}
                  
   module log_part_b(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_b(s, LEVEL_PART, mode, add_ind, debug=debug, hParams=hParams);}
   module log_part_e(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_e(s, LEVEL_PART, mode, add_ind, debug=debug, hParams=hParams);}

   module log_cutoff(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_(s, LEVEL_CUTOFF, mode, add_ind, debug=debug, hParams=hParams);}
                  
   module log_cutoff_b(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_b(s, LEVEL_CUTOFF, mode, add_ind, debug=debug, hParams=hParams);}
   module log_cutoff_e(s,mode,add_ind=0,debug=0, hParams=LOG_PARAMS)
                  { log_e(s, LEVEL_CUTOFF, mode, add_ind, debug=debug, hParams=hParams);}

   


//------------------------------------------------------------

//. == Make tools ===============  

//------------------------------------------------------------
function _Make_update_part( part1, part2 )=
(
  let( // If part1 have def ("shape","sizedef" and "pts"), we keep them; 
       // This prevents the def section of a part from being overwritten
       // by later parts. 
       // If not exist, we get them from part2 and make sure they are placed
       // in the beginning of a part. 
       shape= hash(part1, "shape", hash(part2,"shape")) 
     , sizedef= hash(part1, "sizedef", hash(part2,"sizedef")) 
     , pts= hash(part1, "pts", hash(part2,"pts")) 
     , def = [for( x= [ shape?["shape", shape]:[]  
                      , sizedef?["sizedef", sizedef]:[] 
                      , pts?["pts",pts]:[] 
                      ] ) each x]
     , part1 = concat( def, delkeys(part1, ["shape","sizedef","pts"] ) )
     
     // Turn part2 into a mod (modifier). Items in a mod are updated 
     // differently
     // color: the last value
     // visible: both true
     // debug: any is true
     // markPts: any is true
     // dims: any is true
     // cutoffs: concat (w/ special treatment)
     // actions: concat
     , mod2 = 
  [ haskey(part2,"color")?["color", vals_of_key([part2,part1], "color")[0]]: []
  , haskey(part2,"visible")?["visible", all_in_hashs( [part2,part1], "visible")]:[]
  , haskey(part2,"debug")?["debug"  , any_in_hashs( [part2,part1], "debug")]:[]
  , haskey(part2,"markPts")?["markPts", any_in_hashs( [part2,part1], "markPts")]:[]
  , haskey(part2,"dims")?["dims"   , any_in_hashs( [part2,part1], "dims")]:[]
  
  // cutoffs could be: (1) ["shape",...] or (2) [["shape",...]]. We want to 
  // make sure (1) is [["shape",...]] before a concat is made 
  , haskey(part2,"cutoffs")?let( c1=hash( part1,"cutoffs",[])
                               , c2=hash( part2,"cutoffs",[])
                               )
                            ["cutoffs"
                            , concat( isstr(c1)||c1[0]=="shape"?[c1]:c1
                                    , isstr(c2)||c2[0]=="shape"?[c2]:c2 
                                    )
                            ]
                            :[]
  //, haskey(part2,"cutoffs")?["cutoffs"
                            //, concat( hash( part1,"cutoffs",[])
                                    //, hash( part2,"cutoffs",[]) 
                                    //)
                            //]:[]
  , haskey(part2,"actions")?["actions", concat( hash( part1,"actions",[])
                        , hash( part2,"actions",[]) )]:[]
  ]
  )
  update( part1, [for(x=mod2) each x] )  
);

    _Make_update_part=[ "_Make_update_part", "part1,part2"
     , "array", "Make" 
     ,"Update part1 with part2. A *part* is a hash having two sections:
     ;; &lt;def> and &lt;mod> (=modifier) 
     ;; 
     ;; &lt;def> defines basic obj parameters ('shape','sizedef','pts')
     ;; &lt;mod> defines modifications ('color', 'actions', 'visible', ...)
     ;; 
     ;; At least one of part1, part2 must have def.
     ;;
     ;; Items in a mod are not updated in the same way:
     ;; 
     ;; // color: the last value
     ;; // visible: both true
     ;; // debug: any is true
     ;; // markPts: any is true
     ;; // dims: any is true
     ;; // cutoffs: concat
     ;; // actions: concat
     "
     ,"_Make_update_parts"
    ];

    function _Make_update_part_test( mode=MODE, opt=[] )=
    (
      let( c1= ["shape","cube","color","gold" ]   
         , c2= ["color", "blue"]
         , c3= ["shape","poly","color", "red"]
         
         , v1= ["shape","cube","visible",1 ]   
         , v2= ["visible", 1]
         , v3= ["visible", 0]
         
         , d1= ["shape","cube","debug",0 ]   
         , d2= ["debug", 1]
         , d3= ["debug", 0]
         
         , a1= ["shape","cube","actions", ["rot",[90,0,0] ] ]   
         , a2= ["actions", ["transl",[1,2,3] ]]
         
         )
      doctest( _Make_update_part,
      [
//       _h3("color") 
//      ,_h4("color") 
//      ,_h5("color") 
//      ,_h6("color") 
//      ,
       _span(_u("color"), "font-size:14px")
       ,"" 
      , "var c1", "var c2", "var c3", ""
      , [_Make_update_part(c1,c2),["shape", "cube", "color", "blue"], "c1,c2"]
      
      , ""
      , "// Note the shape is taken from c1 and placed in the beginning of c2:"
      , [_Make_update_part(c2,c1),["shape", "cube", "color", "gold"], "c2,c1"]
      , ""
      , "// Note that <b>shape</b> is NOT replaced by new value:"
      , [_Make_update_part(c1,c3),["shape", "cube", "color", "red"], "c1,c3"]
        
      ,"" 
      ,_span(_u("visible"), "font-size:14px")
      ,"" 
      , "var v1", "var v2", "var v3", ""
      , [_Make_update_part(v1,v2),["shape", "cube", "visible", 1], "v1,v2"]
      , [_Make_update_part(v1,v3),["shape", "cube", "visible", 0], "v1,v3"]
       
      ,"" 
      ,_span(_u("debug"), "font-size:14px")
      ,"" 
      ,"var d1", "var d2", "var d3", ""
      , [_Make_update_part(d1,d2),["shape", "cube", "debug", 1], "d1,d2"]
      , [_Make_update_part(d1,d3),["shape", "cube", "debug", 0], "d1,d3"]
      
       
      ,"" 
      ,_span(_u("actions"), "font-size:14px")
      ,"" 
      , "var a1", "var a2"
      , [_Make_update_part(a1,a2),["shape", "cube", "actions", ["rot", [90, 0, 0], "transl", [1, 2, 3]]], "a1,a2"]
      , [_Make_update_part(a2,a1),["shape", "cube", "actions", ["transl", [1, 2, 3], "rot", [90, 0, 0]]], "a2,a1"]
      
      ]
      , mode=mode, opt=opt, scope=[ "c1", c1, "c2", c2, "c3", c3
                                  , "v1", v1, "v2", v2, "v3", v3 
                                  , "d1", d1, "d2", d2, "d3", d3 
                                  , "a1", a1, "a2", a2
                                  ]
      )
    );   
    
// -----------------------------------------
function _Make_update_parts( parts, _rtn=[], _i=0 )=
(
   _i==0? _Make_update_parts( parts, _rtn= parts[_i], _i=_i+1)
   :_i>=len( parts )? _rtn
   : _Make_update_parts( parts
                      , _rtn= _Make_update_part(_rtn, parts[_i]), _i=_i+1)
);

     _Make_update_parts=[ "_Make_update_parts", "parts"
     , "array", "Make" 
     ,"Update all parts to parts[0]. See _Make_update_part().
     "
     ,"_Make_update_part"
    ];

    function _Make_update_parts_test( mode=MODE, opt=[] )=
    (
      let( c1= ["shape","cube","color","gold" ]   
         , c2= ["color", "blue"]
         , c3= ["shape","poly","color", "red"]
         
         , v1= ["shape","cube","visible",1 ]   
         , v2= ["visible", 1]
         , v3= ["visible", 0]
         
         , d1= ["shape","cube","debug",0 ]   
         , d2= ["debug", 1]
         , d3= ["debug", 0] 
         
         , a1= ["shape","cube","actions", ["rot",[90,0,0] ] ]   
         , a2= ["actions", ["transl",[1,2,3] ]]
         
         , x1= [ "shape","cube"
               , "color","gold","visible",1,"debug",0,"actions",["rot",[90,0,0]] ]
         , x2= [ "color","gray","visible",1,"debug",0,"actions",["transl",[1,2,3]]]
         , x3= [ "color","blue","visible",1,"debug",0]
         , x4= [ "color","red" ,"visible",0,"debug",1,"actions",["transl",[5,5,5]]]
         )
      doctest( _Make_update_parts,
      [
       _span(_u("color"), "font-size:14px")
       ,"" 
      , "var c1", "var c2", "var c3", ""
      , [ _Make_update_parts([c1,c2,c3])
        , ["shape", "cube", "color", "red"]
        , "[c1,c2,c3]"
        ]
      ,"" 
      , "var v1", "var v2", "var v3", ""
      , [_Make_update_parts([v2,c2,c3])
           , ["shape", "poly", "visible", 1, "color", "red"]
           , "[v2,c2,c3]"
        ]
      ,"" 
      ,"var x1", "var x2", "var x3", "var x4", ""
      , [_Make_update_parts([x1,x2,x3])
           , ["shape", "cube", "color", "blue", "visible", 1, "debug", 0, "actions", ["rot", [90, 0, 0], "transl", [1, 2, 3]]]
           , "[x1,x2,x3]"
        ]
      , [_Make_update_parts([x1,x2,x4])
           , ["shape", "cube", "color", "red", "visible", 0, "debug", 1, "actions", ["rot", [90, 0, 0], "transl", [1, 2, 3], "transl", [5, 5, 5]]]
           , "[x1,x2,x4]"
        ]
         
      ]
      , mode=mode, opt=opt, scope=[ "c1", c1, "c2", c2, "c3", c3
                                  , "v1", v1, "v2", v2, "v3", v3 
                                  , "d1", d1, "d2", d2, "d3", d3 
                                  , "a1", a1, "a2", a2
                                  , "x1", x1, "x2", x2, "x3", x3, "x4", x4
                                  ]
      )
    );   

//------------------------------------------------------------
//function _Make_update_def( def1, def2 )=
//(
//  let( shape = hash(def2, "shape")
//     , sizedef= hash(def2, "sizedef")
//     , pts= hash(def2, "pts")
//     , def0 = [ shape? ["shape",shape]:[]
//              , sizedef? ["sizdef", sizedef]:[]
//              , pts? ["pts", pts]:[]
//              ]
//     , _def2 = 
//  [ shape? ["shape",shape]:[]
//  , sizedef? ["sizdef", sizedef]:[]
//  , pts? ["pts", pts]:[]
//  , haskey(def2,"color")?["color", vals_of_key([def2,def1], "color")[0]]: []
//  , haskey(def2,"visible")?["visible", all_in_hashs( [def2,def1], "visible")]:[]
//  , haskey(def2,"debug")?["debug"  , any_in_hashs( [def2,def1], "debug")]:[]
//  , haskey(def2,"markPts")?["markPts", any_in_hashs( [def2,def1], "markPts")]:[]
//  , haskey(def2,"dims")?["dims"   , any_in_hashs( [def2,def1], "dims")]:[]
//  , haskey(def2,"cutoffs")?["cutoffs", concat( hash( def1,"cutoffs",[])
//                        , hash( def2,"cutoffs",[]) )]:[]
//  , haskey(def2,"actions")?["actions", concat( hash( def1,"actions",[])
//                        , hash( def2,"actions",[]) )]:[]
//  ]
//  )
//  update( def1, [for(x=_def2) each x] )  
//);
//
//    _Make_update_def=[ "_Make_update_def", "def1, def2"
//     , "array", "Make" 
//     ,"Update part def1 with part def2.   
//      ;; represented by that name."
//     ,"_Make_get_parts_by_shape"
//    ];
//
//    function _Make_update_def_test( mode=MODE, opt=[] )=
//    (
//      let( color1= ["shape","cube","color","gold" ]   
//         , color2= ["color", "blue"]
//         )
//      doctest( _Make_update_def,
//      [ 
//        "var color1", "var color2", ""
//      , [_Make_update_def(color1,color2),[], "color1,color2"]
//      , [_Make_update_def(color2,color1),[], "color2,color1"]
//        
//     
//      ]
//      , mode=mode, opt=opt, scope=[ "color1", color1
//                                  , "color2", color2 
//                                  ]
//      )
//    );                       
//// -----------------------------------------
//function _Make_update_defs( defs, _rtn=[], _i=0 )=
//(
//   _i>=len( defs )? _rtn
//   : _Make_update_defs( defs, _rtn= _Make_update_def(_rtn, defs[_i]), _i=_i+1)
//);
        
function _Make_get_name_and_colorMark(name,color)=
(
   let( color=isstr(color)?color:color==undef?"gold":color[0]
      , colorMark = _span( "&nbsp;&nbsp;&nbsp;", s= str("background:",color))
      )
   str( colorMark, " ", name, " ", colorMark)
);
    
    _Make_get_name_and_colorMark=[ "_Make_get_name_and_colorMark"
      , "name,color", "str", "Make" 
    ,"Return a str containing *name* flanked by colored mark."
    ,""
    ];

    function _Make_get_name_and_colorMark_test( mode=MODE, opt=[] )=
    (
      let(rtn= "<span style=\"background:teal\">&nbsp;&nbsp;&nbsp;</span> test <span style=\"background:teal\">&nbsp;&nbsp;&nbsp;</span>"
         )
        doctest( _Make_get_name_and_colorMark,
        [ 
          str("_Make_get_name_and_colorMark(\"test\",\"teal\")= "
             , rtn)
        ]
        , mode=mode, opt=opt 
        )
    );   
//............................................
    
function _Make_get_mod(part)=
(
   delkeys(part,["shape","sizedef"])
);    

    _Make_get_mod=[ "_Make_get_mod", "part", "array", "Make" 
    ,"Remove *shape* and *sizedef* from *part* to obtain the mod part"
    ,""
    ];

    function _Make_get_mod_test( mode=MODE, opt=[] )=
    (
        let( part= [ "shape","sphere"
                   , "sizedef", ["r",1]
                   , "color","red"
                   ] )
        doctest( _Make_get_mod,
        [ 
          "var part", ""
    	, [ _Make_get_mod(part), ["color", "red"], "part" ] 
        ]
        , mode=mode, opt=opt, scope=["part",part] 
        )
    );
        
//............................................
    
function _Make_get_libParts(libPartName, partlib)=
(
 // Given a libPartName, return a collection of libParts 
 // represented by that name 
 // partlib=[ "b1", ["shape", "cube", <mod1> ]   // a pure libPart
 //         , "b2", ["shape", "b1", <mod2>]      // a modified libPart
 //         , "b3", ["shape",                    // combo 
 //                    [ "b1"                        
 //                    , ["shape","poly", <mod31>]
 //                    , [shape", "b1", <mod32>]
 //                    ]
 //                 , <mod3>
 //                 ]
 //         ]
 // get_libParts("b1")=[ ["shape","cube",<mod1>] ]
 // get_libParts("b2")=[ ["shape","cube",<mod1>+<mod2] ]
 // get_libParts("b3")=[ ["shape","cube",<mod1>+<mod3>]
 //                    , ["shape","poly",<mod31>+<mod3>] 
 //                    , ["shape","cube",<mod1>+<mod32>+<mod3>] 
 //                    ]
         
 //  
   !partlib?
   _red(str("ERROR <b>_Make_get_libParts</b>(<i>libPartName</i>, <i>partlib</i>):"
           ," partlib not given.")
       )    
   :let( libPart = hash( partlib, libPartName ) 
      , shape = libPart[1]
      , mod  = _Make_get_mod(libPart)
      )
   libPart
   ? ( libPart[0]!="shape"
     ? _red(str( "ERROR <b>_Make_get_libParts</b>(<i>libPartName</i>, <i>partlib</i>):"
            , _s(" The libPart, {_}, represented by libPartName(\"{_}\") must be "
                , [ libPart, libPartName ] )
            , "an array starting with \"shape\": "
            , "[\"shape\", ...] "
            ) )
     : ( has(MAKE_RESERVED_SHAPE_NAMES, shape)    // condition b1
       ?  [ update( libPart //_Make_update_part( libPart , mod)
                  , ["name", libPartName]) ] 
       : isstr(shape)                           // condition b2
         ? let( parts = _Make_get_libParts(shape, partlib))  
         //  [ for(p=parts) _Make_update_part( p, mod ) ]
           [ for(p=parts) _Make_update_part( p, mod ) ]
         : // shape is an array                   // condition b3  
           _Make_get_parts_by_shape(shape, partlib, postmod=mod)
       )        
     )   
   :_red(str( "ERROR <b>_Make_get_libParts</b>(<i>libPartName</i>, <i>partlib</i>):"
            , _s(" libPartName, \"{_}\", is not found in partlib keys: {_}"
                , [libPartName, keys(partlib)] )
            ) )
);

    _Make_get_libParts=[ "_Make_get_libParts", "libPartName, partlib"
     , "array", "Make" 
     ,"Given a libPartName, return a collection of libParts 
      ;; represented by that name."
     ,"_Make_get_parts_by_shape"
    ];

    function _Make_get_libParts_test( mode=MODE, opt=[] )=
    (
      let(partlib=[ "b1", ["shape","cube","color","gold" ]   // a pure libPart
                  , "b2", ["shape","b1","color","red"]      // a modified libPart
                  , "b3", ["shape",                    // combo 
                             [ "b1"                        
                             , ["shape","poly", "color","blue"]
                             , ["shape", "b1", "color","green"]
                             ]
                          ]
                  ]
         , partlib2=[ "b1", ["shape","cube","color","gold" ]   // a pure libPart
                  , "b2", ["shape","b1","color","red"]      // a modified libPart
                  , "b3", ["shape",                    // combo 
                             [ "b1"                        
                             , ["shape","poly", "color","blue"]
                             , ["shape", "b1", "color","green"]
                             ]
                          , "color","purple"  // <=== new in partlib2
                          ]
                  ]
         )
      doctest( _Make_get_libParts,
      [ 
        "var partlib", ""
      //, str("b1=" ,hash( partlib, "b1"))  
      , [ _Make_get_libParts("b1",partlib)
            , [["shape", "cube", "color", "gold", "name", "b1"]], "'b1',partlib" ] 
      , [ _Make_get_libParts("b2",partlib)
            , [["shape", "cube", "color", "red", "name", "b1"]], "'b2',partlib" ] 
      , [ _Make_get_libParts("b3",partlib)
            , [ ["shape", "cube", "color", "gold", "name", "b1"]
              , ["shape", "poly", "color", "blue", "name",undef]
              , ["shape", "cube", "color", "green", "name", "b1"]
              ]
            , "'b3',partlib" ] 
      , ""
      , "partlib2 has a color=purple at b3, setting everything in b3 to purple"
      , ""
      , "var partlib2"
      ,""
      , [ _Make_get_libParts("b3",partlib2)
            , [ ["shape", "cube", "color", "purple", "name", "b1"]
              , ["shape", "poly", "color", "purple", "name", undef]
              , ["shape", "cube", "color", "purple", "name", "b1"]
              ]
            , "'b3',partlib2" ] 
      ,""
      ,_b("Error reporting:")
      ,""
      , "_Make_get_libParts(\"b1\",partlib=[]) = "
      , _Make_get_libParts("b1",partlib=[])      
      ,""
      , "_Make_get_libParts(\"x\",partlib) = "
      , _Make_get_libParts("x",partlib)      
      ,""
      , "_Make_get_libParts(\"b1\",partlib=[\"b1\",\"b2\"]) = "
      , _Make_get_libParts("b1",partlib=["b1",["b2"]])      
      ]
      , mode=mode, opt=opt, scope=["partlib",partlib,"partlib2",partlib2] 
      )
    );

//............................................
    
    function _Make_get_parts_by_shape(shape, partlib, premod=[], postmod=[])=
    ( 
      //  Note: <pn>= <partName>, <sn>= <shapeName>
      //
      // shape could be: (2017.6.28)
      //
      //  (1) <pn>  // A single string
      //  (2) ["shape",<pn>, <mod>] // A "part" w/ shape = libPart 
      //  (3) ["shape",<sn>, <mod>] // A "part" w/ reserved shape
      //  (4) [<pn>, ["shape",<sn|pn>, <mod>] ] // An arr of 1~3
      //
      // A <pn> points to a libPart <LP>, which could be a pure shape,  
      // a modified version of another libPart, or a combination of both. 
      // 
      // A. This *shape* could be applied to:
      //
      //    assemblies.<assemName>.parts. In this case, <mod> = <partMod>
      //
      //    For example: 
      //       "assemblies"
      //          , [ <assemName>
      //            , [ <assemMod>
      //              , "parts", <shape> // <mod> of each item in
      //                                 // <shape> is a <partMod>
      //              , "copies", [ <copyMod> 
      //                          , <copyMod>
      //                          ]
      //              ] 
      //            ] 
      //
      // B. Items of above <shape> points to <LP>s. Each could again 
      //    contains a shape structure like (1~4) above in its
      //    own *shape* value. Lets say,
      //
      //    (1') <pn>  < ============= makes no sense in this context
      //    (2') ["shape",<pn>, <mod2>]  
      //    (3') ["shape",<sn>, <mod2>] 
      //    (4') [<pn>, ["shape",<sn|pn>, <mod2>] ] 
      //
      //    Where <mod2> could mean different things:
      //
      //    "partlib"
      //      , [ <pn1>, ["shape", <sn>,  <libPartMod1> ]
      //        , <pn2>, ["shape", <pn1>, <comboMod2> ]
      //        , <pn3>, ["shape", [ "pn1"
      //                           , ["shape", <sn>, <libPartMod3> ]]
      //                           , ["shape", <pn1>, <comboPartMod3> ]]
      //                           ]
      //                 , <comboMod3>
      //                 ]
      //
      // NOTE !! <comboMode> applied to every parts above (comboParts). But for
      //         cutoffs, we don't want every part in comboParts gets the same
      //         cutoff repeatedly. Only need 1 cutoffs for a set of combpParts
      //
      // It's needed to make this clear due to the order of obj(mod) chain
      //
      //  (1) <libPartMod>
      //  (2) <comboPartMod>
      //  (3) <comboMod>
      //  (4) <assemMod>
      //  (5) <partMod>
      //  (6) <copyMod>
      
      let( shapes= isstr(shape)||shape[0]=="shape"?[shape]:shape
         , aa= 
              [ for( p2 = shapes ) 
               // p2 could be one of:
               //   "b1"                       // b3.1       
               //   ["shape","cube", <mod32>]  // b3.2
               //   [shape", "b1", <mod31>]    // b3.3
               let( p2mod = _Make_get_mod(p2) )
               isstr(p2)
               ? [ for(p3=_Make_get_libParts(p2, partlib))       // b3.1
                   _Make_update_parts( [p3, premod, p2mod, postmod] )
                 ]
               //? let(_parts=_Make_get_libParts(p2, partlib))  // b3.1
                 //[ for(i=len(_parts)-1) 
                    //_Make_update_parts( [_parts[i]
                          //, i>0?delkey(premod, "cutoffs"):premod
                          //, i>0?delkey(p2mod, "cutoffs"):p2mod
                          //, i>0?delkey(postmod, "cutoffs"):postmod] )
                 //]
                  
               : // b3.2. This is a no-name (runtime) part. To make sure the
                 // premod works, need to add p2mod back to the mod chain,
                 // but without "actions" and "cutoffs", 'cos they will add up
                 // and become duplicate with the first item, p2
                 
                 has(MAKE_RESERVED_SHAPE_NAMES, p2[1]) // b3.2
               
                 ? [ update(_Make_update_parts( 
                            	[ p2, premod
                            	, delkeys(p2mod,["actions","cutoffs"])
                             	, postmod
                             	] )
                           ,["name",undef])
                   ]
                 : let( _parts = _Make_get_libParts( p2[1], partlib ) // b3.3
                                           // p2[1] is either "b1" in this examp 
                      ) [ for(p3=_parts ) 
                          _Make_update_parts( [p3, premod, p2mod, postmod] )
                        ] 
                      //) [ for(i=len(_parts)-1) 
                          //_Make_update_parts( [_parts[i]
                                //, i>0?delkey(premod, "cutoffs"):premod
                                //, i>0?delkey(p2mod, "cutoffs"):p2mod
                                //, i>0?delkey(postmod, "cutoffs"):postmod] )
                        //] 
              ]
        )           
      [ for(a=aa) each a ] 
    ); 

    _Make_get_parts_by_shape=[ "_Make_get_parts_by_shape"
     , "shape, partlib, mod"
     , "array", "Make" 
     ,"Given a shape, return a collection of parts.
     ;;
     ;; shape: could be one of :
     ;;
     ;;  1. &lt;pn>  // A single string
     ;;  2. [\"shape\",<partName>, &lt;mod>] // An array where item-0 is \"shape\"
     ;;  3. [\"shape\",<shapeName>, &lt;mod>] // An array where item-0 is \"shape\"
     ;;  4. [&lt;pn>, [\"shape\",<pn>, &lt;mod>] ] // An array containing
     ;;                                    // either a str or part
     ;;
     ;; mod: a modifier applied to each part AFTER each part's own def
     ;;
     ;; Note the difference between _Make_get_parts_by_shape() and 
     ;; _Make_get_libParts()
     ;; 
     ;; _Make_get_parts_by_shape(shape,...): shape is either (1) a string 
     ;;    representing a key in partlib, or (2) an array representing either
     ;;    a part def, or (3) an array containing any # of (1) and (2)   
     ;;
     ;; _Make_get_libParts(libPartName,...): libPartName is just a string 
     ;;    representing a key in partlib.
     
     "
     ,"_Make_get_libParts"
    ];

    function _Make_get_parts_by_shape_test( mode=MODE, opt=[] )=
    (
      let(partlib=[ "b1", ["shape","cube","color","gold" ]   // a pure libPart
                  , "b2", ["shape","b1","color","red"]      // a modified libPart
                  , "b3", ["shape",                    // combo 
                             [ "b1"                        
                             , ["shape","poly", "color","blue"]
                             , ["shape", "b1", "color","green"]
                             ]
                          ]
                  ]
         , partlib2=[ "b1", ["shape","cube","color","gold" ]   // a pure libPart
                  , "b2", ["shape","b1","color","red"]      // a modified libPart
                  , "b3", ["shape",                    // combo 
                             [ "b1"                        
                             , ["shape","poly", "color","blue"]
                             , ["shape", "b1", "color","green"]
                             ]
                          , "color","purple"  // <=== new in partlib2
                          ]
                  ]
         , assemblies=
           [
              "obj1", [ "parts", "b3"]
           ,  "obj2", [ "color","black", "parts", "b3"]
           ]         
         )
      doctest( _Make_get_parts_by_shape,
      [ 
        "var partlib", ""
      //, str("b1=" ,hash( partlib, "b1"))  
      , [ _Make_get_parts_by_shape("b1",partlib)
            , [["shape", "cube", "color", "gold", "name", "b1"]], "'b1',partlib" ] 
      , [ _Make_get_parts_by_shape("b2",partlib)
            , [["shape", "cube", "color", "red", "name", "b1"]], "'b2',partlib" ] 
      , [ _Make_get_parts_by_shape("b3",partlib)
            , [ ["shape", "cube", "color", "gold", "name", "b1"]
              , ["shape", "poly", "color", "blue", "name", undef]
              , ["shape", "cube", "color", "green", "name", "b1"]
              ]
            , "'b3',partlib" ] 
            
      , ""
      , "partlib2 has a color=purple at b3, setting everything in b3 to purple"
      , ""
      , "var partlib2"
      ,""
      , [ _Make_get_parts_by_shape("b3",partlib2)
            , [ ["shape", "cube", "color", "purple", "name", "b1"]
              , ["shape", "poly", "color", "purple", "name", undef]
              , ["shape", "cube", "color", "purple", "name", "b1"]
              ]
            , "'b3',partlib2" 
        ] 
        
        
      ,""
      , "//When shape is just a <b>non-libpart def</b>, it will just return it: "
      ,""
      , [ _Make_get_parts_by_shape(["shape","poly", "color","blue"],partlib)
            , [["shape", "poly", "color", "blue", "name", undef]]
            , "['shape','poly', 'color','blue'],partlib]"  
        ]
      ,""
      ,"//Or you can set postmod (=['color','red']) to modify it:"
      ,""  
      , [ _Make_get_parts_by_shape(["shape","poly", "color","blue"]
            ,partlib, postmod=["color","red"])
            , [["shape", "poly", "color", "red", "name", undef]]
            , "['shape','poly', 'color','blue'],partlib, postmod=['color','red']]"  
        ]
        
      ,""
      , "//When shape is a modified <b>libpart</b>, it retrieves and modifies it: "
      ,""
      , [ _Make_get_parts_by_shape(["shape","b1", "color","blue"],partlib)
            , [["shape", "cube", "color", "blue", "name", "b1"]]
            , "['shape','b1', 'color','blue'],partlib]"  
        ]
      ,""  
      
      ,""
      , "//calling from assembly"
      ,""
      , "var assemblies",""
      
      , [ _Make_get_parts_by_shape("b3",partlib,premod= hash(assemblies,"obj1"))
        , [ ["shape", "cube", "color", "gold", "name", "b1"]
          , ["shape", "poly", "color", "blue", "name", undef]
          , ["shape", "cube", "color", "green", "name", "b1"]
          ]
        , "'b3',partlib,premod= hash(assemblies,'obj1')"
        ]
      ,""       , [ _Make_get_parts_by_shape("b3",partlib2,premod= hash(assemblies,"obj1"))
        , [ ["shape", "cube", "color", "purple", "name", "b1"]
          , ["shape", "poly", "color", "purple", "name", undef]
          , ["shape", "cube", "color", "purple", "name", "b1"]
          ]
        , "'b3',partlib2,premod= hash(assemblies,'obj1')"
        ]
      ,""  
      , [ _Make_get_parts_by_shape("b3",partlib2,premod= hash(assemblies,"obj2"))
        , [ ["shape", "cube", "color", "black", "name", "b1"]
          , ["shape", "poly", "color", "black", "name", undef]
          , ["shape", "cube", "color", "black", "name", "b1"]
          ]
        , "'b3',partlib2,premod= hash(assemblies,'obj2')"
        ]
      
      ]  
      , mode=mode, opt=opt, scope=["partlib",partlib,"partlib2",partlib2
                                  ,"assemblies",assemblies] 
      )
    );
    
   //............................................

   function assign_unNamed_parts(parts,_rtn=[],_i=0,_n=1)=
   (
     _i>= len(parts) ? _rtn
     : let( p = parts[_i] 
          , _name= hash( parts[_i], "name")
          , name = _name?_name: str("noName_", _n)
          , p2 = update(p, ["name", name] )
          )
       assign_unNamed_parts( parts
                           , _rtn= concat( _rtn, [p2] ) 
                           , _i=_i+1
                           , _n=_n +(_name?0:1)
                           )
   );
   
//================================================================
//. == BOM tools ==============
//================================================================


function get_cut_sizes_str_n_sum( data, mat_label, _i=0, _str="", _sum=0, _debug=[])=
(
  //
  // Return an array of [ formatted cut_size_string, sum_of_length ] for mat_label:
  //
  //  ["side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794", 59.914]
  //
  // This is needed in get_final_cuts_string(...)
   
  let( parts = hash(data, "parts")
     , cuts  = hash(data, "cuts")
     , cut_partnames = hash(cuts, mat_label) // like: ["side_l", "side_l"...]    
     )  
  _i<len( cut_partnames ) 
  ? let( partname = cut_partnames[_i]   // like: "side_l"
       , partdef  = hash( parts, partname)
       , len = hashs( partdef, ["sizedef", "size"])[0] //get needed len
       , _str= str(_str, _i==0?"":", ", _b(partname), "=", _fmt(len))
       , _sum= _sum + len
       , _debug = concat( _debug, [partname])
       )
    get_cut_sizes_str_n_sum( data= data
                   , mat_label= mat_label
                   , _i=_i+1
                   , _str=_str, _sum=_sum, _debug=_debug)
  : [_str, _sum]
);


function get_final_cuts_string( data, materials, BOM_opt, _i=0, _rtn="")=
(
  /* Return a string like this:
  
    cedar_picket_1x6_Lowes #1 : 71 x 5.53 x 0.603
  side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794 => sum: 60.2865 (including KERF_W*5=0.3725 + CUTS_TOLERANCE=0.5)

  cedar_picket_1x6_Lowes #2 : 71 x 5.53 x 0.603
  side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794 => sum: 60.2865 (including KERF_W*5=0.3725 + CUTS_TOLERANCE=0.5)

  cedar_1x2_Lowes #1 : 96 x 1.542 x 0.7135
  v_bar=9.7435, v_bar=9.7435, v_bar=9.7435, v_bar=9.7435, h_bar=11.06, h_bar=11.06, h_bar=11.06 => sum: 72.6755 (including KERF_W*7=0.5215 + CUTS_TOLERANCE=0.5)

  */
  
  let( parts = hash(data, "parts")
     , cuts  = hash(data, "cuts")
     , mat_labels = keys( cuts )  // like:  "cedar_1x2_Lowes #1"
     //, cut_partnames = hash(cuts, mat_label) // like: ["side_l", "side_l"...]    
     )  
   _i<len(mat_labels)
   ? let( mat_label = mat_labels[_i]
      , mat_name = trim(split(mat_label,"#")[0])
      , cut_str_n_sum = get_cut_sizes_str_n_sum( data, mat_label ) // [cut_string, sum of len]
      , sum = cut_str_n_sum[1]
      , nPieces = len(hash(cuts, mat_label))
      , kerf_w= hash(BOM_opt, "KERF_W", KERF_W)*nPieces
      , cut_tolerance = hash( BOM_opt, "CUTS_TOLERANCE", CUTS_TOLERANCE)
      , label = _span( str( _u(mat_label), " : ")
                        , "font-size:16px; font-weight:900; color:darkblue")
      , mat_size = join(hash(materials, mat_name)," x ")
      , remark = _span( str( " (including KERF_W*", nPieces, "="
                              , kerf_w
                              , " + CUTS_TOLERANCE=", cut_tolerance, ")"                          )
                         , "font-size:10px")      
      , _rtn = str( _rtn, "<br/>"
                  , label
                  , mat_size
                  ,"<br/>"
                  , _span( str( cut_str_n_sum[0], " => ", _b("sum: "), 
                              , _fmt(  sum+kerf_w ) 
                              )
                         , "font-size:14px")
                  , remark
                  , sum+kerf_w+ cut_tolerance
                    > hash(materials, mat_name)[0]? 
                    _red(_b("&nbsp;&nbsp;&nbsp;!! MATERIAL LENGTH EXCEEDED !!")):""
                  ,"<br/>"
                  )   
      )
    get_final_cuts_string(
              data = data
             , materials=materials
             , BOM_opt=BOM_opt
             , _i=_i+1, _rtn=_rtn)   
   : _rtn
);


//function get_title_string()=
//( _span(
//       str( "<hr/>", _b("Planter Box: "), _fmt(outer_l)
//          , " x ", _fmt(outer_w), " x ", _fmt(outer_h)
//          ,"<br/>"
//          ) 
//       ,  "font-size:20px"
//      )
//);      

function get_parts_check_string( data )=
(
  //
  //  Check if draw_parts and cut_parts are matched
  //  Both of them are hash
  //
  let( parts = hash( data, "parts")
     , cuts  = hash( data, "cuts")
     , draw_parts= [for(i = [0:len(parts)-1])
           let( copydefs = hash(parts[i], "copydefs") )
           i%2!=0?( copydefs?len(copydefs):1)
           : parts[i]
         ]
     , cut_parts= countAll( joinarr( vals(cuts)) )
     )
  str( _span("Parts Check: ", "font-weight:900;font-size:16px")
     //, "<br/>"
     , "Check if numbers of draw_parts and cut_parts are matched"
     , "<br/><br/>"
     , _span( str(
             _b("draw_parts: "), _fmth(draw_parts)
             , "<br/>"
             , _b("&nbsp;&nbsp;&nbsp;cut_parts: "), _fmth(cut_parts)
             ), "font-size:14px"
             )
     , "<br/>"
     ) 
); 
  
  
module echo_bom( data, materials, BOM_opt=[], title=undef)
{
  // section 1: title_string
  // section 2: cuts_arrangement_string
  // section 3: parts_check_string
  /*
  -----------------------------------------------------------------------------
  Planter Box: 13 x 12.266 x 11.06
  -----------------------------------------------------------------------------
  cedar_picket_1x6_Lowes #1 : 0.603 x 5.53 x 71
  side_l=13, side_l=13, side_w=11.06, side_w=11.06, bottom=11.794 => sum: 60.2865 (including KERF_W*5=0.3725 + CUTS_TOLERANCE=0.5)
  -----------------------------------------------------------------------------
  Parts Check: Check if draw_parts and cut_parts are matched

     draw_parts: [side_l=4, side_w=4, bottom=2, v_bar=4, h_bar=3]
     cut_parts: [side_l=4, side_w=4, bottom=2, v_bar=4, h_bar=3]  
  -----------------------------------------------------------------------------
 */
  
  echo( str( "<br/><br/>"
       , und(_span( _b( hash(data, "title"), "font-size:24px")) )
       , "<br/>"
       , get_final_cuts_string( data,materials, BOM_opt )
       , "<hr/>"
       , get_parts_check_string( data)
       ));
}



//================================================================

//. == tests ===============  

//================================================================

function get_Make_test_results( title="Make Doctests"
                              , mode=MODE, opt=["showmode",false] )=
[
  _Make_update_part_test ( mode=MODE, opt=opt )
  , _Make_update_parts_test ( mode=MODE, opt=opt )
//, _Make_get_mod_test ( mode=MODE, opt=opt )
//, _Make_get_name_and_colorMark_test ( mode=MODE, opt=opt ) 
, _Make_get_libParts_test ( mode=MODE, opt=opt ) 
, _Make_get_parts_by_shape_test ( mode=MODE, opt=opt )
];

module Make_tests()
{
  rtn= get_Make_test_results();
  for(x=rtn) echo( x[1] );
}
//Make_tests();

//================================================================

//. == demo ===============  

//================================================================


module Make_demo_simplest(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_simplest"
   
   ,"assemblies" 
  , [  
      "long-side"
    , ["parts", [ 
                    "shape","cube"
                    , "sizedef", ["size",[10,1,4]]
                   
                    
               ]                  
      ] 
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  
//Make_demo_simplest();

//------------------------------------------------------

module Make_demo_simplest_actions(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_simplest_actions"
   
   ,"assemblies" 
  , [  
      "long-side"
    , ["parts",  [
                    "shape","cube"
                    , "sizedef", ["size",[10,1,4]]
                    , "actions", ["rot", [0,0,90], "transl", [0,0, 2]]
                   
                  ]  
                                
      ] 
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  
//Make_demo_simplest_actions();

//------------------------------------------------------
module Make_demo_multiple_parts_1(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_multiple_parts_1"
   
   ,"assemblies" 
  , [  
      "long-side"
    , ["parts", [ [
                    "shape","cube"
                    , "sizedef", ["size",[10,1,4]]
                    , "color", ["teal", 0.7]
                   
                  ]
               , [
                    "shape","poly"
                    , "sizedef", ["size",[10,1,4]]
                    , "actions", ["transl", [0,3,0]]
                    , "color", ["blue", 0.9]
                   
                  ]  
               ]                  
      ] 
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_multiple_parts_1();

//------------------------------------------------------
module Make_demo_multiple_parts_2(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_multiple_parts_2"
   
   ,"assemblies" 
  , [  
      "long-side"
    , ["parts", [ [
                    "shape","cube"
                    , "sizedef", ["size",[10,1,4]]
                    , "color", ["teal", 0.7]
                   
                  ]
               , [
                    "shape","poly"
                    , "sizedef", ["size",[10,1,4]]
                    , "actions", ["rotz",90, "transl", [1,1,0]]
                    //, "actions", ["rot",[0,0,90], "transl", [1,1,0]]
                    , "color", ["blue", 0.9]
                   
                  ]  
               ]                  
      ] 
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_multiple_parts_2();

//------------------------------------------------------
module Make_demo_multiple_parts_3(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_multiple_parts_2"
   
   ,"assemblies" 
  , [  
      "long-side"
    , ["parts", [ [
                    "shape","cube"
                    , "sizedef", ["size",[10,1,4]]
                    , "color", ["teal", 0.7]
                   
                  ]
               , [
                    "shape","poly"
                    , "sizedef", ["size",[10,1,4]]
                    , "actions", ["rotz",90, "transl", [1,1,0]]
                    , "color", ["blue", 0.9]
                   
                  ]  
               , [
                    "shape","cylinder"
                    , "sizedef", ["center",1, "r1",1, "r2",1, "$fn",32, "h", 5]
                    , "color", ["darkkhaki", 0.9]
                   
                  ]  
                   
                  
               ]                  
      ] 
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_multiple_parts_3();

//------------------------------------------------------
module Make_demo_copies(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_copies"
   
   ,"assemblies" 
  , [  
      "long-side"
    , ["color", "purple"         
      , "parts", [ [
                    "shape","cube"
                    , "sizedef", ["size",[5,1,4]]
                    //, "color", ["teal", 0.7]
                   
                  ]
               , [
                    "shape","poly"
                    , "sizedef", ["size",[5,1,4]]
                    , "actions", ["rotz",90, "transl", [1,1,0]]
                   , "color", ["blue", 0.9]
                   
                  ]  
               ]
      , "copies", [ [] 
                  , ["actions", ["rotz", 180]
                    , "color", "red"
                    ]
                  ]      
      ] 
    ]   
  ]; 
 
  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_copies();


//------------------------------------------------------
module Make_demo_copies_and_color_priority(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_copies_and_color_priority"
   
   ,"assemblies" 
  , [  
      "long-side"
    , ["color", "darkkhaki"
      , "parts", [ [
                    "shape","cube"
                    , "sizedef", ["size",[10,1,4]]
                   ] 
                 , [
                    "shape","sphere"
                    , "sizedef", ["r",1]
                    , "actions", ["transl", [0,0.5,0]]
                    , "color", ["teal", 0.7]
                   ] 
                ]    
      , "copies", [ []
                  , ["color",["blue",0.7], "actions", ["transl", [0,3,0]]]
                  ]
      ] 
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_copies_and_color_priority();


//------------------------------------------------------
module Make_demo_partlib(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_partlib"
  
  ,"partlib", [ "board1", [ "shape","poly"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              ]
  
   ,"assemblies" 
  , [  
      "long-side", ["parts","board1"]
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_partlib();


//------------------------------------------------------
module Make_demo_partlib_2(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_partlib_2"
  
  ,"partlib", [ "board1", [ "shape","cube"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              ,  "board2", [ "shape","poly"
                           , "sizedef", ["size",[10,1,4]]
                           , "actions", ["rot",[0,0,90], "transl",[1,1,0]]
                           , "color", ["teal", 0.5]
                          ]
              ]
  
   ,"assemblies" 
  , [  
      "long-side", [ "parts", ["board1","board2" ] ]
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_partlib_2();


//------------------------------------------------------
module Make_demo_partlib_3(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_partlib_3"
  
  ,"partlib", [ "board1", [ "shape","cube"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              ,  "board2", [ "shape","poly"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              ]
  
   ,"assemblies" 
  , [  
      "long-side", 
    ,  [ "parts", ["board1"
                  , ["shape","board2", "actions", ["rot",[0,0,90], "transl",[1,1,0]]
                             , "color", ["teal", 0.5]
                    ]
                  ]
       ]
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_partlib_3();

//------------------------------------------------------
module Make_demo_partlib_copies(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_partlib_copies"
  
  ,"partlib", [ "board1", [ "shape","poly"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              ]
  
   ,"assemblies" 
  , [  
      "long-side", 
    ,  [ "parts", "board1"
       , "copies", [ []
                   , [ "actions", ["transl",[0,5,0]]]
                   ]
       ]
    
    ]   
  ]; 
 
  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_partlib_copies();

//------------------------------------------------------
module Make_demo_partlib_combo_0(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_partlib_combo_0"
  
  ,"partlib", [ "board1", [ "shape","cube"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              , "board2", [ "shape","poly"
                           , "sizedef", ["size",[10,1,4]]
                           , "actions", ["rot",[0,0,90], "transl",[1,1,0]]
                           , "color", ["teal", 0.5]
                          ]
              , "board3", [ "shape", "board2" 
                          , "actions", ["rot",[0,90,0]]
                          , "color", "red"
                          ] 
              ]
  
   ,"assemblies" 
  , [  
      "long-side", 
    ,  [ "color","blue",                
        "parts", "board3"
       , "actions",["transl",[0,0,2]]
       ]
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_partlib_combo_0();


//------------------------------------------------------
module Make_demo_partlib_combo(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_partlib_combo"
  
  ,"partlib", [ "board1", [ "shape","cube"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              , "board2", [ "shape","poly"
                           , "sizedef", ["size",[10,1,4]]
                           , "actions", ["rot",[0,0,90], "transl",[1,1,0]]
                           , "color", ["teal", 0.5]
                          ]
              , "board3", [ "shape", ["board1","board2"] ] 
              ]
  
   ,"assemblies" 
  , [  
      "long-side", 
    ,  [ "parts", "board3"
                       
       ]
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_partlib_combo();

//------------------------------------------------------
module Make_demo_partlib_combo_actions_and_colors(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_partlib_combo_actions_and_colors"
  
  ,"partlib", [ "board1", [ "shape","cube"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              , "board2", [ "shape","poly"
                           , "sizedef", ["size",[10,1,4]]
                           , "actions", ["rot",[0,0,90], "transl",[1,1,0]]
                           , "color", ["teal", 0.5] // <== set color as "partlib modifier" -- priority #4
                          ]
              , "board3", [ "shape", ["board1"
                                     ,[ "shape", "board2", 
                                                    "color","red"
                                      ] // as "combo-part modifier" -- priority #3
                                     ] 
                          //, "color", "blue"  // <====== as "combo modifier" --- priority $2
                          , "actions", ["transl", [2,2,0] ]
                          ] 
              ]
  
   ,"assemblies" 
  , [  
      "long-side", 
      
    ,  [ //"color", "orange",  // <===== set color as "assem modifier"  --- priority $5
        "parts", "board3"
       , "copies", [ ["color", "black"  // <===== set color as "copy modifier" --- priority #1  
                     ] 
                   ]
                   
       ]
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_partlib_combo_actions_and_colors();

//------------------------------------------------------
module Make_demo_partlib_combo_and_copies(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_partlib_combo_and_copies"
  
  ,"partlib", [ "board1", [ "shape","cube"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              , "board2", [ "shape","poly"
                           , "sizedef", ["size",[10,1,4]]
                           , "actions", ["rot",[0,0,90], "transl",[1,1,0]]
                           , "color", ["teal", 0.5] // <== set color as "partlib modifier" -- priority #5
                          ]
              , "board3", [ "shape", ["board1"
                                     ,["shape", "board2", //"color","red"
                                                 
                                      ] // as "combo-part modifier" -- priority #4
                                     ] 
                          , "color", "blue"  // <====== as "combo modifier" --- priority $3
                          , "actions", ["transl", [2,2,0] ]
                          ] 
              ]
  
   ,"assemblies" 
  , [  
      "long-side", 
      
    ,  [ "color", "purple",  // <===== set color as "assem modifier"  --- priority $6
        "parts", [ "board1"
                 , ["shape", "board2"
                              , "color","green"
                                   
                   ]
                 ] // <=== as "namedPart modifier" -- priority $2
       , "copies", [ []
                   , ["actions", ["transl", [-2,-2,0]]
                     //,"color", "black"
                     ] // <===== set color as "copy modifier" --- priority #1
                   ]
       ]
    ]   
  ]; 

  Make2( data = data,  log_level=10 );  
  
}  

//Make_demo_partlib_combo_and_copies();


//------------------------------------------------------
module Make_demo_color_priority(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_color_priority"
  
  ,"partlib", [ "board1", [ "shape","cube"
                           , "sizedef", ["size",[10,1,4]]
                          ]
              , "board2", [ "shape","poly"
                           , "sizedef", ["size",[10,1,4]]
                           , "actions", ["rot",[0,0,90], "transl",[1,1,0]]
                           , "color", ["teal", 0.5] // <== set color as "partlib modifier" -- 1
                          ]
              , "board3", [ "shape", ["board1"
                                     ,["shape","board2", "color","red"] // as "combo-part modifier" -- 2
                                     ] 
                          //, "color", "blue"  // <====== as "combo modifier" --- priority #3
                          , "actions", ["transl", [2,2,0] ]
                          ] 
              ]
  
   ,"assemblies" 
  , [  
      "long-side", 
      
    ,  [ 
       //"color", "purple",  // <===== set color as "assem modifier"  --- priority #4
       "parts", [ "board1"
                 , ["shape", "board2",
                               "color","green"  // <=== as "namedPart modifier" -- #5
                   ]
                 , ["shape","board3"
                    , "actions", ["transl", [0,0,5] ]
                             ,"color", "brown" // <=== as "namedPart modifier" -- #5
                   ]  
                 ] 
       , "copies", [ []
                   //, ["actions", ["transl", [-5,-5,0]]
                     //,"color", "black" // <===== set color as "copy modifier" --- #6
                   //  ]
                   ]
       ]
    ]   
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_color_priority();


//------------------------------------------------------
module Make_demo_cutoffs_on_parts(log_level=10)
{
  
  
  data=  
  [ 
   "title", "Make_demo_cutoffs_on_parts"
   
   ,"assemblies" 
  , [  
      "long-side"
    , ["parts", [ 
                    "shape","cube"
                    , "sizedef", ["size",[10,1,4]]
                     ,"cutoffs", [ 
                                   ["shape","cube"
                                   , "sizedef", ["size",[3,2,1]]
                                   , "actions", ["transl",[5,-0.5,2]]
                                   ]
                                , [
                                   "shape", "sphere"
                                   ,"sizedef",["r",1]
                                   , "actions", ["transl",[2,0.5,2]]
                                 ]   
                                ]
                  
                   
               ]  
      ] 
    ]   
  
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_cutoffs_on_parts();

//------------------------------------------------------
module Make_demo_cutoffs_on_partlib(log_level=10)
{
  
  
  data=  
  [ 
   "title", "Make_demo_cutoffs_on_partlib"
  
  , "partlib", [ "obj1"
                , [
                    "shape","cube"
                    , "sizedef", ["size",[10,1,4]]
                    ,"cutoffs", [ 
                                  [ "shape","cube"
                                   , "sizedef", ["size",[3,2,1]]
                                   , "actions", ["transl",[5,-0.5,2]]
                                  ]
                                , [
                                   "shape", "sphere"
                                   ,"sizedef",["r",1]
                                   , "actions", ["transl",[2,0.5,2]]
                                 ]   
                                ]
                  ]
                   
               ]  
     
   
   ,"assemblies" 
  , [  
      "long-side", [ "parts", "obj1"
                   , "copies", [ []
                               , ["actions", ["rot", [0,-90,0]
                                             , "transl",[0,-5,0]]
                                             ]
                               ] 
                   ]
    ]   
  
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_cutoffs_on_partlib();

//------------------------------------------------------

module Make_demo_cutoffs_on_comboPart(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_cutoffs_on_comboPart"
  
  , "partlib", [ "obj1"
                , [
                    "shape","cube"
                    , "sizedef", ["size",[10,1,4]]
                    ,"cutoffs", [ 
                                   "shape","cube"
                                   , "sizedef", ["size",[3,2,1]]
                                   , "actions", ["transl",[5,-0.5,2]]
                                  
                                ]
                  ]
               , "obj2"
               , [ "shape", "obj1"
                 , "cutoffs", [
                                 "shape", "cube"
                                 ,"sizedef",["size",[2,1,2]]
                                 , "actions", ["transl",[4,-0.5,2.5]]
                               ]
                 
                 ]  
                   
               ]  
     
   
   ,"assemblies" 
  , [  
      "long-side"
      , [ "parts", "obj2"
      	, "cutoffs", [
                       "shape", "sphere"
                       ,"sizedef",["r",0.5]
                       , "actions", ["transl",[1.5,0, 1.5]]
                     ] 
        ]
    ]   
  
  ]; 

 Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_cutoffs_on_comboPart();

//------------------------------------------------------

module Make_demo_cutoffs_using_partlib(log_level=10)
{
  
  data=  
  [ 
   "title", "Make_demo_cutoffs_using_partlib"
  
  , "partlib", [ "obj1"
                , [
                    "shape","cube"
                  , "sizedef", ["size",[4,3,2]]
                  , "actions", ["transl",[-0.01,-0.01,-0.01]]
                  ]
               , "obj2"
               , [ "shape", "cube"
                 , "sizedef",["size",[10,1,6]]
                 ]   
               , "obj3"
               , [ "shape", [ "obj2"
                            , ["shape", "obj1"  
                              ,"actions", ["transl", [5,-3,0]]
                              ]
                            ]
                 , "cutoffs", "obj1"
                 ]  
               ]  
     
   
   ,"assemblies" 
  , [  
      "long-side", ["parts","obj3"]
    ]   
  
  ]; 

 Make2( data = data,  log_level=log_level );  
  
}  

//Make_demo_cutoffs_using_partlib();

//----------------------------------------------------

module Make_demo_woodworking_mortise_tenon (log_level=10)
{
  
  
  data=  
  [ 
   "title", "Make_demo_woodworking_mortise_tenon"
  
  , "partlib", [ "toung"
                , [
                    "shape","cube"
                    , "sizedef", ["size",[2,3,1]]
                  ]
               , "board"
                , [
                    "shape","cube"
                    ,"color",["gold",0.7]
                    , "sizedef", ["size",[8,4,2]]
                    , "actions", ["transl",[0,0,0]]
                  ]
               , "tenon"
               , [
                   "shape", [["shape","board", "actions", ["x", 12] 
                             //["shape","board", "actions", ["transl", [12,0,0]] 
                             ]
                            ,["shape","toung","actions",["transl",[10,0.5,.5]]
                             ]
                            ]
                 , "color", "darkkhaki"            
                 ]
               , "mortise"
               , [
                   "shape", "board","actions",["transl",[0,0,0]]
                 , "cutoffs", ["shape","toung"
                              , "actions", ["transl",[6.01,.5,0.5]]
                              ]
                 ]   
               ]  
     
   
   ,"assemblies" 
  , [  
      "long-side"
      , [ "parts", ["mortise", "tenon"]
        ]
    ]   
  
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  
//Make_demo_woodworking_mortise_tenon();


//----------------------------------------------------

module Make_demo_woodworking_box_joint (log_level=10)
{
  
  w=1;
  
  data=  
  [ 
   "title", "Make_demo_woodworking_box_joint"
  
  , "partlib", [ "board"
                , [
                    "shape","cube"
                    , "sizedef", ["size",[10,w,6]]
                  ]
               , "board1"
                , [
                    "shape","board"
                    ,"actions", ["rot",[0,0,90],"transl",[w,0,0]]
                    ,"cutoffs"
                    , [ ["shape","tooth","actions",["transl",[-0.01,-0.01,-0.015]] ]
                      , ["shape","tooth","actions",["transl",[-0.01,-0.01,2*w]] ]
                      , ["shape","tooth","actions",["transl",[-0.01,-0.01,4*w]] ]
                      ]
                    
                  ]
               , "tooth"
               , [
                    "shape","cube"
                    , "sizedef", ["size",[w+0.015,w,w]]
                    ,"color",["teal",1]
                ]                  
               , "board2"
                , [
                    "shape",["board"
                            , ["shape","tooth","actions",["y",-w] ]
                            //, ["shape","tooth","actions",["transl",[-w,0,0]] ]
                            , ["shape","tooth","actions",["transl",[-w,0,2*w]] ]
                            , ["shape","tooth","actions",["transl",[-w,0,4*w]] ]
                            ]
                    ,"color",["darkkhaki",1]
                    ,"actions", ["transl",[w*2.5,0,0]]
                  ]
               ]  
     
   
   ,"assemblies" 
  , [  
      "box"
      , [ "parts", ["board1","board2"]
        ]
    ]   
  
  ]; 

  Make2( data = data,  log_level=log_level );  
  
}  
 
//Make_demo_woodworking_box_joint();




//------------------------------------------------------
//module Make_demo_partlib_modified_on_assemblies(log_level=10)
//{
//  
//  data=  
//  [ 
//   "title", "Make_demo_partlib_modified_on_assemblies"
//  
//  ,"partlib", [ "board1", [ "shape","cube"
//                           , "sizedef", ["size",[10,1,4]]
//                          ]
//              , "board2", [ "shape","poly"
//                           , "sizedef", ["size",[10,1,4]]
//                           , "actions", ["rot",[0,0,90], "transl",[1,1,0]]
//                           , "color", ["teal", 0.5] 
//                          ]
//              , "board3", [ "shape", ["board1"
//                                     ,["board2", ["color","red"]] 
//                                     ] 
//                           , "actions", ["transl", [2,2,0] ]
//                          ] 
//              ]
//  
//   ,"assemblies" 
//  , [  
//      "long-side", 
//      
//    ,  [ "color", "orange"  
//       , "namedParts", ["board1", "board2"]
//       ]
//    ]   
//  ]; 
//
//  Make2( data = data,  log_level=log_level );  
//  
//}  

//Make_demo_partlib_modified_on_assemblies();

//------------------------------------------------------
//module Make_demo_multiple_parts(log_level=10)
//{
//  
//  data=  
//  [ 
//   "title", "Make_demo_multiple_parts"
//   
//   ,"assemblies" 
//  , [  
//      "long-side"
//    , ["parts", [ [
//                    "shape","cube"
//                    , "sizedef", ["size",[10,1,4]]
//                    , "actions", ["transl", [1,0,0]]
//                    , "color", ["teal", 0.7]
//                   
//                  ]
//               , [
//                    "shape","poly"
//                    , "sizedef", ["size",[1, 5, 4]]
//                    , "actions", ["transl", [1,1,0]]
//                    , "color", ["blue", 0.9]
//                   
//                  ]  
//               ]                  
//      ] 
//    ]   
//  ]; 
//
//  Make2( data = data,  log_level=log_level );  
//  
//}  
//
//Make_demo_multiple_parts();



//------------------------------------------------------
//module Make_demo_(log_level=10)
//{
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 72; 
//  outer_w= 24;   
//  outer_h= 30;
//  
//  data=  
//  [ 
//   "title", "Make_demo_basic"
//   
//   ,"assemblies" 
//  , [  
//      "long-side"
//    , ["parts", [ [
//                    "shape","cube"
//                    , "sizedef", ["size",[10,1,4]]
//                   
//                  ]  
//               ]
//      ,"copies", [ []
//                 , [ "actions", ["transl", [0, 6, 0] ] ]
//                 ]      
//      //, "color", "red"
////      , "copies", [[]
////                  ,["color","red", "actions", ["rot",[0,0,180], "transl",[ LEN4, outer_w, 0 ]]]
////                  ,["color","green", "actions", ["rot",[0,0,0], "transl",[ outer_l, 0,0 ]]]
////                  ,["color","blue", "actions", ["rot",[0,0,180], "transl",[ outer_l+LEN4, outer_w,0 ]]]
////                  ]  
//                  
//      ] 
//
//    ] //_ assemblies
//  
//  ]; 
//
//  Make2( data = data,  log_level=log_level );  
//  
//}  //_ Make_demo_multiple_parts
//
////Make_demo_basic();
//
//
//module Make_demo_minimal(log_target="parts")
//{
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  // a hash containing upto 4 keys: title, parts, materials and cuts
//  [ 
//   "title", "Make_demo_minimal"
//   
//  ,"partlib"
//  , [       
//      "long_board"  // <== partname
//
//      , [ 
//          "shape", "poly"  // <== shape type, like "cube", "cylinder", "poly". Default: "cube" 
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
//        ]        
//        
//    , "short_board"
//  
//      , [ "shape","cube" // <== use the built-in primitive
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ] ]
//        ]          
//          
//    , "long_board2"
//
//      , [ "shape","poly" // skip "sizedef", give "pts" directly
//        , "pts", [[13, 0, 0.603], [0, 0, 0.603], [0, 5.53, 0.603], [13, 5.53, 0.603]
//                 ,[13, 0, 0], [0, 0, 0], [0, 5.53, 0], [13, 5.53, 0]]
//        ]   
//        
//   ] //_ partlib
//    
//  
//  ,"assemblies" // a hash w/ key = partname, val = partdef (=a hash defining part properties)
//  , [       
//      "side_l"  // <== objname
//      , [ "body", [ "long_board" 
//                  , [ "actions", ["transl", [0, 0, wood_th]]] // "rot" or "transl"
//                  ]
//       ]
//       
//      , "side_w"
//      , [ "body", [ "short_board"
//                  , [ "actions", ["transl", [0,wood_th,wood_th]] ]
//                  ]
//        ]          
//          
//      , "bottom"
//      , [ "body", "long_board2"   // if no modification, use partname
//        ]   
//   
////   , "dummy_poly"
////  
////     , [ "shape", "poly" ] // no sizedef given, will echo an error
////   
////   , "dummy_cube"
////  
////     , [ "shape", "cube" ] // no sizedef given, will echo an error
//      
//    ] //_ assemblies
//    
//  
//  ]; 
//
//  Make2( data = data, log_target=log_target );  
//  
//}  //_Make_demo_minimal
//
//
//  
//  
//  
//module Make_demo_BOM(log_target)
//{
//  /*
//      Demo how to setup for BOM (Bill of Materials) output
//
//      1. Add "material" to each part
//      2. Add "materials" to data
//      3. Add "cuts" to data  
//  
//  */ 
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_BOM"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "shape", "poly"
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
//        , "material", "cedar_picket_1x6_Lowes" // defines materials for BOM  {{ 1 }}
//        , "actions", ["transl", [0, 0, wood_th]] 
//        ]        
//        
//    , "side_w"
//  
//      , [ "shape", "poly"
//        , "sizedef", ["size", [ outer_h-wood_th, wood_w-wood_th, wood_th ]]
//        , "material", "cedar_picket_1x6_Lowes"  // defines materials for BOM
//        , "actions", ["rot", [0,-90,0], "transl", [ wood_th,wood_th,wood_th]]
//        ]          
//          
//    , "bottom"
//
//      , [ "material", "cedar_picket_1x6_Lowes" // defines materials for BOM
//        , "shape", "poly"
//        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]] 
//        ]          
//    ] //_parts
//  
//  //========= define materials and cuts for BOM =============
//  
//  , "materials"  // {{ 2 }}  < mat_name >:< mat_size >
//  , [ 
//      "cedar_picket_1x6_Lowes", [FT6-1, wood_w, wood_th] // -1: corner cut of picket
//    ] 
//   
//  , "cuts"       // {{ 3 }}  // < mat_label >: < part_list >
//  , [                        // < mat_label > = <mat_name> or <mat_name> + "#" + n  
//      "cedar_picket_1x6_Lowes #1"   
//    , [ "side_l", "side_w", "bottom" ]
//    ]
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}
//
//
//module Make_demo_multiple_copies(log_target)
//{
//  /*
//      Multiple copies of a part
//
//      --- Add "copydefs" to part definition. The value of copydefs
//          is a list containing definition for each copy. Each definition
//          is a hash containing actions ("rot", "transl"), "color", 
//          "visible", and other settings like "showtime".
//  
//          , "copydefs": 
//          , [ 
//              ["rot",[], "transl",[], "visible",0, "color","blue", "showtime",.3 ]
//            , []  // <=== a copy w/o any copy-specific definition 
//            ]
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_multiple_copies"
//   
//  ,"partlib"
//  , [       
//      "long_board"  // <== partname
//
//      , [ 
//          "shape", "poly"  // <== shape type, like "cube", "cylinder", "poly". Default: "cube" 
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
//        ]        
//        
//    , "short_board"
//  
//      , [ "shape","cube" // <== use the built-in primitive
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ] ]
//        ]          
//          
//    , "long_board2"
//
//      , [ "shape","poly" // skip "sizedef", give "pts" directly
//        , "pts", [[13, 0, 0.603], [0, 0, 0.603], [0, 5.53, 0.603], [13, 5.53, 0.603]
//                 ,[13, 0, 0], [0, 0, 0], [0, 5.53, 0], [13, 5.53, 0]]
//        ]   
//        
//   ] //_ partlib
//  
//  ,"assemblies" // a hash w/ key = partname, val = partdef (=a hash defining part properties)
//  , [       
//      "side_l"  // <== objname
//      , [ "body", [ "long_board" 
//                  , [ "actions", ["transl", [0, 0, wood_th]]
//                    , "copies"     
//                    , [
//                        []
//                      , ["actions", ["transl", [0, outer_w-wood_th, 0] ] ]
//                      ]] // "rot" or "transl"
//                  ]
//       ]
//       
//      , "side_w"
//      , [ "body", [ "short_board"
//                  , [ "actions", ["transl", [0,wood_th,wood_th]] 
//                    , "copies"
//                    , [
//                        []
//                      , ["actions", ["transl", [outer_l-wood_th, 0,0] ] ]
//                      ] 
//                    ]
//                  ]
//        ]          
//          
//      , "bottom"
//      , [ "body", "long_board2"   // if no modification, use partname
//        ]
//        
//    ] //_ assemblies
//  
//  ]; 
//
//  Make2( data = data, log_target=log_target );  
//  
//} //_ Make_demo_multiple_copies
//
//
//module Make_demo_colors(log_target)
//{
//  /*
//      Set color  
//  
//      -- color can be set at 3 levels:
//         (1) a part at partlib level --- all assemblies using this part 
//         (2) a bodypart inside an assembly
//         (3) a copy of a bodypart
//  
//      -- A color can be specified as "blue" or ["blue", 0.6]
//
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_colors"
//   
//  ,"partlib"
//  , [       
//      "long_board"  
//
//      , [ 
//          "shape", "poly"   
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
//        ]        
//        
//    , "short_board"
//  
//      , [ "shape","cube"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ] ]
//        ]          
//          
//    , "long_board2"
//
//      , [ "shape","poly" 
//        , "pts", [[13, 0, 0.603], [0, 0, 0.603], [0, 5.53, 0.603], [13, 5.53, 0.603]
//                 ,[13, 0, 0], [0, 0, 0], [0, 5.53, 0], [13, 5.53, 0]]
//                 
//        , "color", "purple" // <=== set color in the partlib level   (1)
//        ]   
//        
//   ] //_ partlib
//   
//   
//   ,"assemblies" 
//  , [       
//      "side_l"  
//      
//      , [ "body", [ "long_board" 
//                  , [ "color", ["blue",0.8]  // <=== set color at bodypart level  (2)
//                    , "actions", ["transl", [0, 0, wood_th]]
//                    , "copies"     
//                    , [
//                        []
//                      , ["actions", ["transl", [0, outer_w-wood_th, 0] ]
//                         ]
//                      ]] 
//                  ]
//       ]
//       
//      , "side_w"
//      
//      , [ "body", [ "short_board"
//                  , [ "actions", ["transl", [0,wood_th,wood_th]] 
//                    , "color", ["green", 0.5]
//                    , "copies"
//                    , [
//                        []
//                      , ["actions", ["transl", [outer_l-wood_th, 0,0] ] 
//                      
//                        , "color", "red"  // <=== set color at copy level (3) 
//                        ]
//                      ] 
//                    ]
//                  ]
//        ]          
//          
//      , "bottom"
//      , [ "body", "long_board2"   
//        ]
//        
//    ] //_ assemblies
//  
//  ]; 
//
//  Make2( data = data, log_target=log_target );  
//  
//}  //_ Make_demo_colors
//
//
//module Make_demo_multiple_parts_(log_target,log_level)
//{
//  /*
//      Set color  
//  
//      -- color can be set at 3 levels:
//         (1) a part at partlib level --- all assemblies using this part 
//         (2) a bodypart inside an assembly
//         (3) a copy of a bodypart
//  
//      -- A color can be specified as "blue" or ["blue", 0.6]
//
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 72; 
//  outer_w= 24;   
//  outer_h= 30;
//  
//  data=  
//  [ 
//   "title", "Make_demo_multiple_parts"
//   
//  ,"partlib"
//  , [       
//      "leg_board_in"  
//
//      , [ "sizedef", ["size", [LEN4, LEN2, outer_h-LEN4 ]]
//        , "color", "teal"
//        , "cutoffs", []
//        ]        
//
//      , "leg_board_out"  
//
//      , [ "shape","poly"
//        , "sizedef", ["size", [LEN4, LEN2, outer_h-LEN4 ]]
//        , "cutoffs", []
//        ] 
//  
//
//    , "short_board"
//  
//      , [ "shape","cube"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ] ]
//        ]          
//          
//    , "long_board2"
//
//      , [ "shape","poly" 
//        , "pts", [[13, 0, 0.603], [0, 0, 0.603], [0, 5.53, 0.603], [13, 5.53, 0.603]
//                 ,[13, 0, 0], [0, 0, 0], [0, 5.53, 0], [13, 5.53, 0]]
//                 
//        , "color", "purple" // <=== set color in the partlib level   (1)
//        ]   
//        
//   ] //_ partlib
//   
//   
//   ,"assemblies" 
//  , [  
//      "leg"
//    , ["src", [ "leg_board_out"
//              , ["leg_board_in", [ "actions", ["transl", [0,LEN1,LEN2]]]
//                ]  
//              ] 
//      //, "color", "red"
//      , "copies", [[]
//                  ,["color","red", "actions", ["rot",[0,0,180], "transl",[ LEN4, outer_w, 0 ]]]
//                  ,["color","green", "actions", ["rot",[0,0,0], "transl",[ outer_l, 0,0 ]]]
//                  ,["color","blue", "actions", ["rot",[0,0,180], "transl",[ outer_l+LEN4, outer_w,0 ]]]
//                  ]  
//                  
//      ] 
////      "side_l"  
////      
////      , [ "body", [ "long_board" 
////                  , [ "color", ["blue",0.8]  // <=== set color at bodypart level  (2)
////                    , "actions", ["transl", [0, 0, wood_th]]
////                    , "copies"     
////                    , [
////                        []
////                      , ["actions", ["transl", [0, outer_w-wood_th, 0] ]
////                         ]
////                      ]] 
////                  ]
////       ]
////       
////      , "side_w"
////      
////      , [ "body", [ "short_board"
////                  , [ "actions", ["transl", [0,wood_th,wood_th]] 
////                    , "color", ["green", 0.5]
////                    , "copies"
////                    , [
////                        []
////                      , ["actions", ["transl", [outer_l-wood_th, 0,0] ] 
////                      
////                        , "color", "red"  // <=== set color at copy level (3) 
////                        ]
////                      ] 
////                    ]
////                  ]
////        ]          
////          
////      , "bottom"
////      , [ "body", "long_board2"   
////        ]
////        
//    ] //_ assemblies
//  
//  ]; 
//
//  Make2( data = data, log_target=log_target, log_level=log_level );  
//  
//}  //_ Make_demo_multiple_parts
//
//
//module Make_demo_irregular_shape(log_target)
//{
//  // 
//  // irregular shape is achieved by giving pts instead of size 
//  //
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_irregular_shape"
//   
//  ,"parts"  
//  , [       
//      "irregular"  // <== part_name
//
//      , [ "shape","poly"
//        , "pts", [ [-12, 6, 0], [-6, 11, 0], [11, 8, 0], [13, -6, 0]
//                 , [-12, 6, 2], [-6, 11, 2], [11, 8, 2], [13, -6, 2]]
//        //, "actions", ["transl", [0, 0, wood_th]] // either "rot" or "transl"
//        , "copydefs"
//        , [
//            []
//          , ["actions",["transl", [3,3,3]] ]  
//          , ["actions",["transl", [6,6,6]], "color","teal" ]
//          ]
//        ]     
//    ] //_parts
//  
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}
//
//
//module Make_demo_visible(log_target)
//{
//  /*
//      visibility of a part or a copy
//      -- Add "visible" to the part definition for part visibility
//      -- Add "visible" to the copy definition for copy-specific visibility
//  
//      -- A visibility can be specified as 1|0 or true|false
//
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_visible"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", "darkkhaki"
//        , "shape","poly"
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
//        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
//        , "copydefs"     
//        , [
//            []
//          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]
//            , "visible", 0                         // <== hide this copy
//            ]
//          ]
//        ]        
//        
//    , "side_w"
//  
//      , [ "color", "teal" 
//        , "shape","poly"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        
//        , "visible", 0                            // <== hide this part 
//        
//        , "copydefs"
//        , [
//            [ ] 
//          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ]]
//          ]        
//        ]          
//          
//    , "bottom"
//
//      , [ "shape","poly"
//        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]]
//        ]          
//          
//    ] //_parts
//  
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}
//
//module Make_demo_showtime( $t=$t )
//{
//  /*
//    demo the showtime settings for animation
//  
//    1. Setup a time sequence
//    2. Assign time sequence by adding "showtime" to part definition
//  
//    For this particular demo, set FPS= Steps = 4 for animation
//
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  time_sequence=    // {{ 1 }} setup a time sequence
//  [
//    "bottom", .25  
//  , "side_l", .5  // => part "side_l" shows up at and after $t=0.5
//  , "side_w", .75
//  ]; 
//  
//  data=  
//  [ 
//   "title", "Make_demo_showtime"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", "darkkhaki"
//        , "shape","poly"
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
//        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
//        , "showtime", hash( time_sequence, "side_l" )  // {{ 2 }} assign time sequence
//        , "copydefs"     
//        , [
//            []
//          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]]
//          ]
//        ]        
//        
//    , "side_w"
//  
//      , [ "color", "teal" 
//        , "shape","poly"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]] 
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        , "showtime", hash( time_sequence, "side_w" ) // {{ 2 }} assign time sequence
//        , "copydefs"
//        , [
//            [ ] 
//          , ["actions", ["transl", [outer_l-wood_th, 0,0] ]]
//          ]        
//        ]          
//          
//    , "bottom"       // {{ 2 }} part "bottom" is not given a "showtime"
//      , [ "shape","poly"
//        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]]
//        ]          
//          
//    ] //_parts
//  
//  ]; 
//
//  Make( data = data );  
//  
//}
//module Make_demo_showtime_onoff( $t=$t, log_target="all" )
//{
//  /*
//    demo the adv showtime settings to turn on/off the display of parts
//  
//    For this particular demo, set FPS= Steps = 5 for animation
//
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  time_sequence=    
//  [
//    "bottom", [ .2, .9 ]   
//  , "side_l", [ 0.2, 0.8 ]  // => shows up at $t=0.2 and turn off at $t=0.8
//  , "side_w", [ 0.4, 0.6 ]
//  ]; 
//  
//  data=  
//  [ 
//   "title", "Make_demo_showtime_onoff"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", "darkkhaki"
//        , "shape","poly"
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
//        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
//        , "showtime", hash( time_sequence, "side_l" )  //<====== 
//        , "copydefs"     
//        , [
//            []
//          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]]
//          ]
//        ]        
//        
//    , "side_w"
//  
//      , [ "color", "teal" 
//        , "shape","poly"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        , "showtime", hash( time_sequence, "side_w" )  //<=====
//        , "copydefs"
//        , [
//            [ ] 
//          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ]]
//          ]        
//        ]          
//          
//    , "bottom"       
//      , [ "shape","poly"
//        , "sizedef", ["size", [ outer_l, outer_w, wood_th ] ]
//        , "showtime", hash( time_sequence, "bottom" ) //<=====
//        ]          
//          
//    ] //_parts
//  
//  ]; 
//
//  Make( data = data,log_target=log_target );  
//  
//}
//
//
//module Make_demo_showtime_copies( $t=$t, log_target )
//{
//  /*
//    demo the adv showtime settings:
// 
//    1. copy-specific showtime
//    2. adv showtime: on-off-on-off multiple times
//
//    For this particular demo, set FPS= Steps = 10 for animation
//
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  time_sequence=    
//  [
//    "bottom", [ .1, .4, .6, .9 ]  // on-off-on-off multiple times 
//  , "side_l", [ 0.2, 0.8 ]  
//  , "side_w", [ 0.3, 0.8 ]
//  , "side_l2", [ 0.4, 0.7 ]  
//  , "side_w2", [ 0.5, 0.6 ]
//  ]; 
//  
//  data=  
//  [ 
//   "title", "Make_demo_showtime_copies"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", ["darkkhaki", .5]
//        , "shape","poly"
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
//        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
//          
//        , "copydefs"     
//        , [
//            [ "showtime", hash( time_sequence, "side_l" )] // copy-specific showtime
//          , [ "actions", ["transl", [0, -outer_w+wood_th, 0]]
//            , "showtime", hash( time_sequence, "side_l2" )] // copy-specific showtime
//          ]
//        ]        
//        
//    , "side_w"
//  
//      , [ "color", "teal" 
//        , "shape","poly"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        , "showtime", hash( time_sequence, "side_w" ) // <===== part-specific
//        , "copydefs"
//        , [
//            [ "showtime", hash( time_sequence, "side_w" )] 
//          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ]
//            , "showtime", hash( time_sequence, "side_w2" )]  // <===== copy-specific
//          ]        
//        ]          
//          
//    , "bottom"       
//      , [ "shape","poly"
//        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]] 
//        , "showtime", hash( time_sequence, "bottom" )  // <===== part-specific
//        ]          
//          
//    ] //_parts
//  
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}
//
//
//
//
//
//module Make_demo_markPts(log_target)
//{
//  /*
//      markPts on a part or a copy. 
//  
//
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_markPts"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", ["blue", 0.8]  // part color with transparancy 
//      
//        , "shape","poly"
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
//        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
//        , "visible", 0
//        , "copydefs"     
//        , [
//           []
//          ,  ["actions", ["transl", [0, -outer_w+wood_th, 0] ]]
//          ]
//        ]        
//        
//    , "side_w"
//  
//      , [ "color", "teal"    // part color 
//      
//        , "shape","poly"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th, outer_h-wood_th ] ]
//        , "actions", ["transl", [0,0,wood_th]]
//        , "copydefs"
//        , [
//            ["color", "brown"
//            , "markPts",  ["label",["isxyz",true]] ] // show coordinate as label
//          , ["actions", ["transl", [outer_l-wood_th, 0,0]]
//            , "markPts", "l"]                        // show index as label
//          ]        
//        ]          
//          
//    , "bottom"
//
//      , [ "markPts", "b"                            // show balls only
//        , "shape","poly"
//        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]] 
//        ]          
//          
//    ] //_parts
//  
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}
//
//
//module Make_demo_dim(log_target)
//{
//  /*
//      Dimensioning
//  
//      Add "dims" to copydefs 
//      
//       , "dims"
//       ,  [
//            <dim>
//          , <dim>
//          , ...
//          ]
//  
//      Each <dim> is [ "pqr", [a,b,c]
//                    , "transl_p", [xyz]
//                    , "transl_q", [xyz]
//                    , "transl_r", [xyz]
//                    , "opt", dim_opt (refer to the Dim() module in scadx_obj2.scad )
//                    ]
//
//                   [a,b,c]: each could be either pt or index 
//                   transl_p: move the point p 
//                   
//                   Only "pqr" is required. The rest are optional
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_dim"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", ["darkkhaki", 0.8]  
//      
//        , "shape","poly"
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ] ]
//        , "actions", ["transl", [0, outer_w-wood_th, wood_th]] 
//        , "visible", 1
//        , "copydefs"     
//        , [
//             [ "markPts", "l"
//             
//             , "dims"                      // <=== dim
//                , [ ["pqr", [3, 7,2], "transl_q", [0,0, -wood_th]]
//                  , ["pqr", [2,3,7]]
//                  ]
//             ]  
//          ,  [ "actions", ["transl", [0, -outer_w+wood_th, 0] ]
//             , "markPts", "l"
//             , "dims", [["pqr", [5,1,0]] ] // <=== dim
//             ]
//          ]
//        ]        
//        
//    , "side_w"
//  
//      , [ "color", "teal"    // part color 
//      
//        , "shape","poly"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ] ]
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        , "visible", 1
//        , "copydefs"
//        , [
//            [] // show coordinate as label
//          , [ "actions", ["transl", [outer_l-wood_th, 0,0]]
//            ]                        // show index as label
//          ]        
//        ]          
//          
//    , "bottom"
//
//      , [ "shape","poly"
//        , "sizedef", ["size", [ outer_l, outer_w, wood_th ]] 
//        //, "markPts", "l"
//        , "copydefs"
//        , [ 
//            [ "dims"  // <=== dim
//            , [
//                ["pqr", [4,7,3], "opt", ["label",["rot", [180,0,0 ]]]]
//              ]
//            ]         
//          ]        
//        ]          
//          
//    ] //_parts
//  
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}
//
//
//
//module Make_demo_cutoffs(log_target)
//{
//  /*
//     How to do "cutoffs" out of the object.
//  
//     This demo shows a cutoff, "handle_holl", is applied to part "side_l"
//     in its partdef ( so each copy of the same "handle_hole" cutoff), and
//     another one, "groove_l", is applied to both copies of "side_l" 
//     separately (so each copy has the groove_l but at different locations) 
//  
//     1. Add a "cutoffs":<h_cutoff_defs> pair to *parts* : 
//  
//        , "cutoffs"
//        , [ <cutoff_name>, <cutoff_def>, <cutoff_name>, <cutoff_def>  ...]
//  
//        <cutoff_def> is a stripped-down version of partdef or copydef, containing
//        only up to 2 keys: "actions" and ( "size" or "pts" ):
//  
//            [ "size", [ ... ]
//            , "actions", ["transl",[...] ]   // <=== optional
//            ]
//  
//        This serves as a cutoff directory and could be referred to multiple times 
//        inside the code for multiple copies of a cutoff.
//  
//     2. Add "cutoffs":<h_cutoff_def> pair to *partdef* for part-specific cutoffs.
//        
//         , "cutoffs"
//         , ["handle_hole",[]]  
//         
//     3. Add "cutoffs":<h_cutoff_def> pair to *copydef* for copy-specific cutoffs.
//            
//         , "cutoffs"
//         , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]] ]
//         
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_cutoffs"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", ["darkkhaki", 0.9]   
//      
//        , "shape","poly"
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
//        , "actions", ["transl", [0, 0, wood_th+0.001]]
//        , "cutoffs", ["handle_hole",[]]                  // <==== {{ 2 }}
//        , "visible",1
//        , "copydefs"     
//        , [
//            [ "cutoffs", ["groove_l", [] ]              // <==== {{ 3 }}
//            ]
//            
//          , [ 
//              "actions", ["transl", [0, outer_w-wood_th, 0] ]
//            , "cutoffs"
//            , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]]  // <==== {{ 3 }}
//              ]
//            ]
//          ]
//        ]        
//        
//    , "side_w"
//  
//      , [ "color", "teal"     
//      
//        , "shape", "poly"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        , "visible", 0
//        , "copydefs"
//        , [
//            [ ] 
//          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
//          ]        
//        ]          
//          
//    , "bottom"
//
//      , [ "color", "brown"
//        , "visible", 0
//        , "shape","poly"
//        , "sizedef", ["size", [ outer_l, outer_w, wood_th ] ]
//        ]          
//          
//    ] //_parts
//  
//  , "partlib"   // <================================= {{ 1 }}  
//  , [
//       "groove_l"
//       
//         , [ "shape","poly"
//           , "sizedef", ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]
//           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
//           ]
//         
//    , "handle_hole"
//     
//        , [ "shape","poly"
//          , "sizedef", ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]
//          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
//          ]
//  
//    ]
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}
//
//module Make_demo_cutoffs_markPts(log_target)
//{
//  /*
//     How to do "cutoffs" out of the object.
//  
//     This demo shows a cutoff, "handle_holl", is applied to part "side_l"
//     in its partdef ( so each copy of the same "handle_hole" cutoff), and
//     another one, "groove_l", is applied to both copies of "side_l" 
//     separately (so each copy has the groove_l but at different locations) 
//  
//     1. Add a "cutoffs":<h_cutoff_defs> pair to *parts* : 
//  
//        , "cutoffs"
//        , [ <cutoff_name>, <cutoff_def>, <cutoff_name>, <cutoff_def>  ...]
//  
//        <cutoff_def> is a stripped-down version of partdef or copydef, containing
//        only up to 2 keys: "actions" and ( "size" or "pts" ):
//  
//            [ "size", [ ... ]
//            , "actions", ["transl",[...] ]   // <=== optional
//            ]
//  
//        This serves as a cutoff directory and could be referred to multiple times 
//        inside the code for multiple copies of a cutoff.
//  
//     2. Add "cutoffs":<h_cutoff_def> pair to *partdef* for part-specific cutoffs.
//        
//         , "cutoffs"
//         , ["handle_hole",[]]  
//         
//     3. Add "cutoffs":<h_cutoff_def> pair to *copydef* for copy-specific cutoffs.
//            
//         , "cutoffs"
//         , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]] ]
//         
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_cutoffs_markPts"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", ["darkkhaki", 0.9]   
//      
//        , "shape", "poly"
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
//        , "actions", ["transl", [0, 0, wood_th+0.001]]
//        , "cutoffs", ["handle_hole",["markPts","l"]]                  // <==== {{ 2 }}
//        , "visible",0
//        , "copydefs"     
//        , [
//            [ "cutoffs", ["groove_l", [] ]              // <==== {{ 3 }}
//            ]
//            
//          , [ 
//              "actions", ["transl", [0, outer_w-wood_th, 0] ]
//            , "cutoffs"
//            , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]
//                           , "markPts", "l"]  // <==== {{ 3 }}
//              ]
//            ]
//          ]
//        ]        
//        
//    , "side_w"
//  
//      , [ "color", "teal"     
//      
//        , "shape","poly"
//        , "sizedef", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]
//        , "actions", ["transl", [0,wood_th,wood_th]]
//        , "visible", 0
//        , "copydefs"
//        , [
//            [ ] 
//          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
//          ]        
//        ]          
//          
//    , "bottom"
//
//      , [ "color", "brown"
//        , "visible", 0
//        , "shape","poly"
//        , "sizedef", ["size", [ outer_l, outer_w, wood_th ] ]
//        ]          
//          
//    ] //_parts
//  
//  , "partlib"   // <================================= {{ 1 }}  
//  , [
//       "groove_l"
//       
//         , [ "shape","poly"
//           , "sizedef", ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]
//           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
//           ]
//         
//    , "handle_hole"
//     
//        , [ "shape","poly"
//          , "sizedef", ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]
//          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
//          ]
//  
//    ]
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}
//
//
//module Make_demo_solid_cutoffs(log_target)
//{
//  /*
//     How to draw primitive objects and do "cutoffs".
//         
//     New: set shape in partdef to primitive :
//  
//       , "shape"
//       , [ "cube", ["size", n | [x,y,z] ] ]
//    
//     This is needed 'cos poly can't display cutoffs properbably   
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_solid_cutoffs"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", ["darkkhaki", 0.9]   
//      
//        , "shape", "cube"        // shape set to cube but not poly 
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
//        , "actions", ["transl", [0, 0, wood_th+0.001]]
//        , "cutoffs", ["handle_hole",[]]//"markPts", "l"]]                  // <==== {{ 2 }}
//        , "visible", 1
//        , "copydefs"     
//        , [
//            [ "cutoffs", ["groove_l", []]//"markPts","l"] ]              // <==== {{ 3 }}
//            ]
//            
//          , [ 
//              "actions", ["transl", [0, outer_w-wood_th, 0] ]
//            , "cutoffs"
//            , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]]  // <==== {{ 3 }}
//              ]
//            ]
//          ]
//        ]        
//        
////    , "side_w"
////  
////      , [ "color", "teal"     
////      
////        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]] 
////        , "actions", ["transl", [0,wood_th,wood_th]]
////        , "visible", 0
////        , "copydefs"
////        , [
////            [ ] 
////          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
////          ]        
////        ]          
////          
////    , "bottom"
////
////      , [ "color", "brown"
////        , "visible", 0
////        , "shape",["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
////        ]          
//          
//    ] //_parts
//  
//  , "partlib"   // <================================= {{ 1 }}  
//  , [
//       "groove_l"
//       
//         , [ "shape","cube"      // shape set to cube but not poly
//           , "sizedef", ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]
//           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
//           ]
//         
//    , "handle_hole"
//     
//        , [ "shape","cube"      // shape set to cube but not poly
//          , "sizedef", ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]
//          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
//          ]
//  
//    ]
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}
//
//
//module Make_demo_solid_markPts(log_target)
//{
//  /*
//     How to draw primitive objects and do "cutoffs".
//         
//     New: set shape in partdef to primitive :
//  
//       , "shape"
//       , [ "cube", ["size", n | [x,y,z] ] ]
//    
//     This is needed 'cos poly can't display cutoffs properbably   
//  */   
//  
//  wood_th = 0.603;
//  wood_w = 5.53;
//  
//  outer_l= 13; 
//  outer_w= wood_w;   
//  outer_h= wood_w + wood_th;
//  
//  data=  
//  [ 
//   "title", "Make_demo_solid_markPts"
//   
//  ,"parts"  
//  , [       
//      "side_l"  
//
//      , [ "color", ["darkkhaki", 0.9]   
//      
//        , "shape", "cube"        // shape set to cube but not poly 
//        , "sizedef", ["size", [outer_l, wood_th, outer_h-wood_th ]]
//        , "actions", ["transl", [0, 0, wood_th+0.001]]
//        //, "cutoffs", ["handle_hole",["markPts", "l"]]                  // <==== {{ 2 }}
//        , "markPts", "l"
//        , "visible",1
//        , "copydefs"     
//        , [
//            [ //"cutoffs", ["groove_l", [] 
//              //            ]              // <==== {{ 3 }}
//              
//            ]
//            
//          , [ 
//              "actions", ["transl", [0, outer_w-wood_th, 0] ]
//           // , "cutoffs"
//           // , ["groove_l", ["actions", ["transl", [0, -wood_th*2, 0]]]  // <==== {{ 3 }}
//          //    ]
//            ]
//          ]
//        ]        
//        
////    , "side_w"
////  
////      , [ "color", "teal"     
////      
////        , "shape",["poly", ["size", [ wood_th, wood_w-wood_th*2, outer_h-wood_th ]]] 
////        , "actions", ["transl", [0,wood_th,wood_th]]
////        , "visible", 0
////        , "copydefs"
////        , [
////            [ ] 
////          , [ "actions", ["transl", [outer_l-wood_th, 0,0] ] ]
////          ]        
////        ]          
////          
////    , "bottom"
////
////      , [ "color", "brown"
////        , "visible", 0
////        , "shape",["poly", ["size", [ outer_l, outer_w, wood_th ] ]]
////        ]          
//          
//    ] //_parts
//  
////  , "partlib"   // <================================= {{ 1 }}  
////  , [
////       "groove_l"
////       
////         , [ "shape", ["cube"      // shape set to cube but not poly
////                    , ["size", [ outer_l-wood_th*2, wood_th*2, wood_th]]]
////           , "actions", ["transl",[wood_th, wood_th/2, LEN1/2 ] ]
////           //, "markPts", "l"
////           ]
////         
////    , "handle_hole"
////     
////        , [ "shape",["cube"      // shape set to cube but not poly
////                  , ["size", [ LEN4, wood_th+0.5+0.1, LEN1]]]
////          , "actions", ["transl", [ outer_l/2-LEN2, -0.001, outer_h-1-LEN1 ] ]
////          ]
////  
////    ]
//  ]; 
//
//  Make( data = data, log_target=log_target );  
//  
//}

//===============================================================

//Make_demo_multiple_parts(log_target="all", log_level=1);







//Make_demo_minimal(log_target="all"); //["parts", "copies", "markPts", "all"]);
//Make_demo_multiple_copies(log_target="all");
//Make_demo_colors(log_target="all");
//Make_demo_irregular_shape(log_target="all");
//Make_demo_visible(log_target="all");
//Make_demo_showtime( $t=$t, log_target="all"); // Steps = 4 for animation
//Make_demo_showtime_onoff( $t=$t, log_target="all"); // Steps = 5 for animation
//Make_demo_showtime_copies( $t=$t, log_target="all"); // Steps = 10 for animation
//Make_demo_markPts(log_target="all");
//Make_demo_dim(log_target="all");
//Make_demo_cutoffs(log_target="all");
//Make_demo_cutoffs_markPts(log_target="cutoffs");
//Make_demo_solid_cutoffs(log_target="all");
//Make_demo_solid_markPts(log_target="all");//log_target="cutoffs_markPts" );
// skip this one at this moment Make_demo_BOM(log_target="all");

//function delkey(h,k)=
//(
//   [ for(i=[0:len(h)-1])  
//     if((i%2==0 && h[i]!=k)||(i%2!=0 && h[i-1]!=k)) h[i] 
//   ]
//);
//
//h= ["a",1, "b",2, "c",3]; 
//echo( delkey( h, "b" ) );   
//     
//function delkeys(h,ks)=
//(
//   [ for(i=[0:len(h)-1])  
//     if(i%2==0 && !has(ks,h[i])||(i%2!=0 && !has(ks,h[i-1]))) h[i] 
//   ]
//);     
//     
//echo( delkeys( h, ["a","b","d"] ) );  