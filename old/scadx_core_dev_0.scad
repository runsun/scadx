/*
=================================================================
                    scadx -- an OpenScad lib.
=================================================================

-- scadx_core.scad
-- Function doc and Test Tools 
III. Shape

Naming convention:

    arr : array
    str : string
    int : integer

    pq  : [P,Q]
    pqr : [P,Q,R]

    v,pt: a vector, a point 
    pl  : a plane
    pts : many points 

    2-pointer: pq, st
    3-pointer: pqr, stu
    4-pointer: pqrs

    Single capital letters: points/vector (P=[2,5,0]);

    aPQR: angle of P-Q-R
   
    function: starts with lower case (angle(), hash(...)) 
    module for object: starts with capitals (Line, MarkPts)

=================================================================
*/

$fn = 12; 
GAPFILLER = 0.0001; // used to extend len to cover gap
ZERO   = 1e-13;     // used to chk a value that's supposed to be 0: abs(v)<zero
ORIGIN = [0,0,0];
IM     = [[1,0,0],[0,1,0],[0,0,1]]; //Identity matrix (3x3)
PI     = 3.14159265358979323846264338327950;
COLORS = ["red","green","blue","purple","brown","khaki"];   // default colors

CUBEFACES=[ [0,1,2,3], [4,5,6,7]
		  , [0,1,5,4], [1,2,6,5]
		  , [2,3,7,6], [3,0,4,7] 
		  ];

// A common option for shapes
OPS=[ "r" , 0.1
	, "h", 1
	, "w", 1
	, "len",1 
	, "color", undef 
	, "transp", 1
	, "fn", $fn
	, "rotate", 0
	, "rotates", [0,0,0]
	, "translate", [0,0,0]	 
	];
 
scadx_categories=
[
	  "Array"
	, "Doctest"
	, "Geometry"
	, "Hash"
	, "Index"
	, "Inspect"
	, "Math"
	, "Number"
	, "Point"
	, "2D_Object"
	, "3D_Object"
	, "String"
	, "Type"
];

/*
=================================================================
               toolets
=================================================================
*/

    // Convert a 2-pointer pq to a vector
    function p2v(pq)= pq[1]-pq[0];          

    function dx(pq)= pq[1].x - pq[0].x;  
    function dy(pq)= pq[1].y - pq[0].y;
    function dz(pq)= pq[1].z - pq[0].z;

    function d01(pqr)= norm(pqr[1]-pqr[0]); // dist btw p and q 
    function d12(pqr)= norm(pqr[2]-pqr[1]); 
    function d02(pqr)= norm(pqr[2]-pqr[0]);

    // square of dist btw p and q 
    function d01pow2(pqr)= pow(norm(pqr[1]-pqr[0]),2);  
    function d12pow2(pqr)= pow(norm(pqr[2]-pqr[1]),2);
    function d02pow2(pqr)= pow(norm(pqr[2]-pqr[0]),2);

    function minx(pq)= min(pq[0][0],pq[1][0]); // smallest x btw p and q 
    function miny(pq)= min(pq[0][1],pq[1][1]);
    function minz(pq)= min(pq[0][2],pq[1][2]);
    function maxx(pq)= max(pq[0][0],pq[1][0]); // largest x btw p and q
    function maxy(pq)= max(pq[0][1],pq[1][1]);
    function maxz(pq)= max(pq[0][2],pq[1][2]);

    function px00(p)=[ p[0],0,0];  // for pt [x,y,z] => [x,0,0]
    function p0y0(p)=[ 0, p[1],0 ];
    function p00z(p)=[ 0,0, p[2]];
    function pxy0(p)=[ p[0],p[1],0]; // for pt [x,y,z] => [x,y,0]
    function p0yz(p)=[ 0, p[1],p[2] ];
    function px0z(p)=[ p[0],0, p[2]];

    function p01(pts)= [pts[0], pts[1]];  // pick pt-0, pt-1 from pts 
    function p10(pts)= [pts[1], pts[0]];
    function p02(pts)= [pts[0], pts[2]];
    function p20(pts)= [pts[2], pts[0]];
    function p03(pts)= [pts[0], pts[3]];
    function p30(pts)= [pts[3], pts[0]];
    function p12(pts)= [pts[1], pts[2]];
    function p21(pts)= [pts[2], pts[1]];
    function p13(pts)= [pts[1], pts[3]];
    function p31(pts)= [pts[3], pts[1]];
    function p23(pts)= [pts[2], pts[3]];
    function p32(pts)= [pts[3], pts[2]];

    function p012(pts)= [pts[0],pts[1],pts[2]];
    function p013(pts)= [pts[0],pts[1],pts[3]];
    function p021(pts)= [pts[0],pts[2],pts[1]];
    function p023(pts)= [pts[0],pts[2],pts[3]];
    function p031(pts)= [pts[0],pts[3],pts[1]];
    function p032(pts)= [pts[0],pts[3],pts[2]];

    function p102(pts)= [pts[1],pts[0],pts[2]];
    function p103(pts)= [pts[1],pts[0],pts[3]];
    function p120(pts)= [pts[1],pts[2],pts[0]];
    function p123(pts)= [pts[1],pts[2],pts[3]];
    function p130(pts)= [pts[1],pts[3],pts[0]];
    function p132(pts)= [pts[1],pts[3],pts[2]];

    function p201(pts)= [pts[2],pts[0],pts[1]];
    function p203(pts)= [pts[2],pts[0],pts[3]];
    function p210(pts)= [pts[2],pts[1],pts[0]];
    function p213(pts)= [pts[2],pts[1],pts[3]];
    function p230(pts)= [pts[2],pts[3],pts[0]];
    function p231(pts)= [pts[2],pts[3],pts[1]];

    function p301(pts)= [pts[3],pts[0],pts[1]];
    function p302(pts)= [pts[3],pts[0],pts[2]];
    function p310(pts)= [pts[3],pts[1],pts[0]];
    function p312(pts)= [pts[3],pts[1],pts[2]];
    function p320(pts)= [pts[3],pts[2],pts[0]];
    function p321(pts)= [pts[3],pts[2],pts[1]];

    function pp2(pts,i,j)= [pts[i], pts[j]];    // pick 2 points 
    function pp3(pts,i,j,k)= [pts[i], pts[j], pts[k]];
    function pp4(pts,i,j,k,l)= [pts[i], pts[j], pts[k], pts[l]];

    function newx(pt,x)= [x, pt.y, pt.z];
    function newy(pt,y)= [pt.x, y, pt.z];
    function newz(pt,z)= [pt.x, pt.y, z];

    // product of difference, used in lineCrossPt
    function podPt(P,Q,R,S)=[ (Q-P).x * (S-R).x   
                            , (Q-P).y * (S-R).y
                            , (Q-P).z * (S-R).z ];
                            
    // P*Q is generally easier than dot(P,Q). But if P and Q are 
    //  complex, then dot() might be easier. For example
    //  dot(P-Q,R-S) * dot(P-R,Q-S)  would be easier than
    //  ((P-Q)*(R-S))*((P-R)*(Q-S))
    // EX: See it in lineCrossPt() 
    function dot(P,Q)= P*Q; 

    function P(pqr) = pqr[0];
    function Q(pqr) = pqr[1];
    function R(pqr) = pqr[2];
    function N(pqr,len=1,reverse=false) = normalPt(pqr,len=len,reverse=reverse);
    function N2(pqr,len=1,reverse=false) = 
            normalPt( [P(pqr), Q(pqr), N(pqr)],len=len,reverse=reverse);



/*
=================================================================
               Argument design pattern
=================================================================

1. Undef arg with default val

    module obj( ops=[] )
    {
        ops= update( [ "r", 3 ], ops );
    }

2. Two or more related SIMPLE args, set it at once or seperate

    module obj( ops=[] )
    {
        ops = update( ["r", 3], ops );
        function ops(k,df) = hash(ops, k, df);
        r = ops("r");
        rP= ops("rP", r);  // if rP undef, use r
        rQ= ops("rQ", r);  // if rQ undef, use r
        ...
    }
    
3. ONE complex arg

    module obj( ops=[] )
    {
        u_ops  = ops;
        df_ops = [...];
        df_head = ["r",2];
        
        ops = update( df_ops, u_ops );
        function ops(k,df) = hash(ops, k, df);

        ops_head= update( df_head, ops("head",[]) ); 
        ...
    }    
               
4. two or more complex args 
   (example, heads in Arrow. Or Dim2() ):

    module obj( ops=[] )
    {
        u_ops = ops;
        df_ops= [ "r",0.02
                , "heads",true
                , "headP",true
                , "headQ",true
                ];
        df_heads= ["r", 0.5, "len", 1];  
        df_headP= [];
        df_headQ= [];
                
        ops = update( df_ops, u_ops );
        function ops(k,df)= hash(ops,k,df);
        
        com_keys = [ "color","transp","fn"];
        
        ops_headP= getsubops( ops=u_ops, df_ops=df_ops
                    , com_keys= com_keys;
                    , sub_dfs= ["heads", df_heads, "headP", df_headP ]
                    );
        ops_headQ= getsubops( ops=u_ops, df_ops=df_ops
                    , com_keys=com_keys;
                    , sub_dfs= ["heads", df_heads, "headQ", df_headQ ]
                    );
            
        function ops_headP(k) = hash( ops_headP,k );
        function ops_headQ(k) = hash( ops_headQ,k ); 
    }
    
    NOTE:
        
        For multiple layer sub-units, like:
        
        body - arm - finger
        
        An each approach is to allow setting, for example, fingers like:
        
        obj( ["arms", ["finger", ["count",3]]] ) 
        
=================================================================

*/

module ArgumentPatternDemo_simple()
{
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= concat( ops
            , [ "color", "blue" ]  // <== set default here
            , OPS );
        function ops(k,df) = hash(ops,k,df);
        color = ops("color");
        len   = ops("len", 2); // <== or here. Note that len doesn't
                               //     need to be defined in ops
        translate(transl) color( ops("color"), 0.4) 
        cube(x=0.5,y=0.5,z=1 );
    }  
    Obj();    
    Obj( ["color","red"], [0,1,0] );
}    

module ArgumentPatternDemo_multiple_simples()
{
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= concat( ops
            , [ "limlen", 2
              ], OPS );
        function ops(k,df) = hash(ops,k,df);
        lim = ops("limlen");
        arm = ops("armlen",lim);
        leg = ops("leglen",lim); 
        
        //echo("ll, arm,leg", ll, arm, leg);
        translate(transl)
        color( ops("color"), 0.4) { 
            cube( size=[0.5,1,2] );
            translate( [0,1, 1]) cube( size=[0.5,arm,0.5] );
            translate( [0,0.5, -leg]) cube( size=[0.5,0.5,leg] );
        }    
    }  
    Obj();    
    Obj( ["limlen",1, "color","red"], [2,0,0] );
    Obj( ["armlen",0.5, "color","blue"], [4,0,0] );
    LabelPt( [0, 0, 2], "Obj()=> both lims= default 2" );
    LabelPt( [2, 1, 1.5], "Obj([\"limlen\",1])=> both lims=1",["color","red"] );
    LabelPt( [4, 1, 1.5], "Obj([\"armlen\",0.5])=> arm len=0.5", ["color","blue"] ); 
}    

module ArgumentPatternDemo_complex()
{_mdoc("ArgumentPatternDemo_complex","");
  
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= update( ["arm", true]
                    ,ops);
        function ops(k,df)= hash(ops,k,df);

        df_arm=["len",3];
        
        ops_arm= getsubops( ops
                    , com_keys= ["transp"]
                    , sub_dfs= ["arm", df_arm ]
                    );
        function ops_arm(k,df) = hash(ops_arm,k,df);
        
        echo("ops_arm",ops_arm);
        
        translate(transl) color(ops("color"), ops("transp"))
        cube( size=[0.5,1,2] );
        if ( ops_arm )
        {   translate( transl+[0,1, 1]) 
            color(ops_arm("color"), ops_arm("transp"))
            cube( size=[0.5,ops_arm("len"),0.5] );
        }   
    }  
    Obj();    
    Dim( [ [0,4,1.5], [0,1,1.5], [2,1,1.5]] );
    Obj( ["arm",false], [1.5,0,0] );
    Obj( ["arm",["len",2]], [3,0,0] );
    Obj( ["arm",["color","red"]], [4.5,0,0] );
    Obj( ["color", "green"], [6,0,0] );
    Obj( ["transp", 0.5], [7.5,0,0] );
    
    LabelPt( [0.5,4.2,1.5], "1.Obj()");
    LabelPt( [1.8,3,1.2], "2.Obj([\"arm\",false])");
    LabelPt( [3.6,3.5,1.5], "3.Obj([\"arm\",[\"len\",2]])");
    LabelPt( [4.8,4.2,1.5], "4.Obj([\"arm\",[\"color\",\"red\"]])",["color","red"]);
    LabelPt( [6.5,4.2,1.5], "5.Obj([\"color\",\"green\"])",["color","green"]);
    
    LabelPt( [8,4.2,1.5], "6.Obj([\"transp\",0.5])");
    
    LabelPt( [10, 0, 0], "Note #5,#6: #5 color set body color but not"
    ,["color","blue"]);
    LabelPt( [10, 0, -0.6], "arm color, but #6 set transp of both, "
    ,["color","blue"]);
    LabelPt( [10, 0, -1.2], "because transp is in com_keys "
    ,["color","blue"]);
    
}  
module ArgumentPatternDemo_multiple_complice()
{
  _mdoc("ArgumentPatternDemo_complice"
," 0: armL has transp=0.6 'cos  df_armL=[''transp'',0.6]
;; 1,2,4: no armL 'cos armL= false 
;; 2    : no arms 'cos arms= false
;; 3~7  : both len = 2 'cos arms=[''len'',2]
;; 5,6  : armL ops placed inside arms [] have no effect
;; 7    : armL ops should have one [] by its own 
;; 8    : set color on obj-level doesn't apply to arms but transp,
;;        does 'cos transp is in com_keys but color is not.
;; 9    : transp set on obj-level can be overwritten by that 
;;      : set in armL [].
");
    
    module Obj( ops=[], transl=[0,0,0] )
    {
        df_ops= ["arms", true
               , "armL", true
               , "armR", true
               ];

        _ops= update(df_ops, ops);
        function ops(k,df)= hash(_ops,k,df);

        df_arms=["len",3];
        df_armL=["transp",0.6];
        df_armR=[];
        
        ops_armL= getsubops( 
                      ops  
                    , df_ops = df_ops
                    , com_keys= ["transp"]
                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]
                    );
        ops_armR= getsubops( 
                      ops
                    , df_ops = df_ops
                    , sub_dfs= ["arms", df_arms, "armR", df_armR ]
                    , com_keys= ["transp"]
                    );
        function ops_armL(k,df) = hash(ops_armL,k,df);
        function ops_armR(k,df) = hash(ops_armR,k,df);

        echo("");
        echo("ops", ops);
        echo("df_ops", df_ops);
        echo("ops_armL", ops_armL);
        echo("ops_armR", ops_armR);
        
        translate(transl) color(ops("color"), ops("transp"))
        cube( size=[0.5,1,2] );
        if ( ops_armR )
        {   
            translate( transl+[0,1, 1]) 
            color(ops_armR("color"), ops_armR("transp"))
            cube( size=[0.5,ops_armR("len"),0.5] );
        }    
        if ( ops_armL )
        {    translate( transl+[0,-ops_armL("len"), 1]) 
            color(ops_armL("color"), ops_armL("transp"))
            cube( size=[0.5,ops_armL("len"),0.5] );
            
        }   
    }  
    Obj();    
    //Dim( [ [0,4,1.5], [0,1,1.5], [2,1,1.5]] );
    
    all_ops= [
        ["arms",false]  // 1
        ,["armL",false]  // 2
        ,["arms",["len",2]] // 3
        ,["arms",["len",2], "armL", false] // 4
        ,["arms",["len",2, "armL", false]] // 5
        ,["arms",["len",2, "armL", ["color","red"]]] // 6
        ,["arms",["len",2], "armL", ["color","red", "len",1]] // 7
        ,["color","red", "transp", 0.3] // 8
        ,["transp", 0.5, "armL", ["transp", 1] ] // 9  
        ,["armR",["color", "blue"]]
    ];
    for(i=range(all_ops)){
        Obj( all_ops[i], [1.5*(i+1), 0, 0] );
        LabelPt( [(i+1)*1.5+0.3, 4.3, 1.5], str(i+1, " ops=", all_ops[i]) 
               , ["color", "blue"] );
    }
 
}  
//ArgumentPatternDemo_simple();
//ArgumentPatternDemo_multiple_simples();
//ArgumentPatternDemo_complex();
//ArgumentPatternDemo_multiple_complice();



//=========================================================
accumulate=[ "accumulate", "arr", "array", "Math" ,
" Given a numeric array [n0,n1,n2 ...], return an array 
;; [ n0, n0+n1, n0+n1+n2 ... ]
"];

function accumulate(arr, i=0)=
(
    [for (i=range(arr)) sum( slice(arr, 0,i+1))]
    [for (i=range(arr)) 
);

    module accumulate_test( ops=["mode",13] )
    {
        doctest( accumulate,
        [ ["[3,4,2,5]", accumulate([3,4,2,5]), [3,7,9,14] ]
        ],  ops );
    }	


//========================================================
addx=[ "addx", "pt,x=0", "array", "Point"
, "Given a point or points (pt) and a x value, add x to pt.x.
;; When pt is a collection of points, add x to each pt if x is
;; a number, or add x[i] to pt[i].x if x is an array. That is, 
;; by setting x as array, we can add different values to each 
;; pt[i].x. 
;;
;; Note that addz() - unlike addx, addy - expands a 2D pt to 3D.
"];

function addx(pt,x=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addx(pt[i], isarr(x)?x[i]:x) ]
	: len(pt)==3
       ? [ pt.x+x,pt.y,pt.z ] 
	   : [ pt.x+x,pt.y]  
);
    
    module addx_test( ops=["mode",13] )
    {
    pts = [[2,3,4],[5,6,7]];

	doctest( addx,
	[ "// To a point:"
	//, ["[2,3]", addx([2,3]), [2,3]]
	, ["[2,3], 1", addx([2,3],x=1), [3,3]]
	//, ["[2,3,4]", addx([2,3,4]), [2,3,4]]
	, ["[2,3,4], 1", addx([2,3,4],x=1), [3,3,4]]
	, "// To points:"
	//, ["[[2,3],[5,6]]", addx([[2,3],[5,6]]), [[2,3],[5,6]]]
	, ["[[2,3],[5,6]], 1", addx([[2,3],[5,6]],x=1), [[3,3],[6,6]]]
	, ["[[2,3],[5,6]], [8,9]", addx([[2,3],[5,6]],x=[8,9]), [[10,3],[14,6]]]
	//, ["pts", addx(pts), [[2,3,4],[5,6,7]]]
	, ["[[2,3,4],[5,6,7]], 1", addx(pts,x=1), [[3,3,4],[6,6,7]]]
	, ["[[2,3,4],[5,6,7]], [2,3]", addx(pts,x=[2,3]), [[4,3,4],[8,6,7]]]
	]); //, ops, ["pts",pts]);
    }
    
//========================================================
addy=[ "addy", "pt,y=0", "array", "Point"
, "Add y to pt. See doc in addx"
];
    
function addy(pt,y=0,_I_=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addy(pt[i], isarr(y)?y[i]:y) ]
	: len(pt)==3
       ? [ pt.x,pt.y+y,pt.z ] 
	   : [ pt.x,pt.y+y] 
);
    
    module addy_test( ops=["mode",13] )
    {
	pts = [[2,3,4],[5,6,7]];

	doctest( addy,
	[ "// To a point:"
	//, ["[2,3]", addy([2,3]), [2,3]]
	, ["[2,3], 1", addy([2,3],y=1), [2,4]]
	//, ["[2,3,4]", addy([2,3,4]), [2,3,4]]
	, ["[2,3,4], 1", addy([2,3,4],y=1), [2,4,4]]
	, "// To points:"
	//, ["[[2,3],[5,6]]", addy([[2,3],[5,6]]), [[2,3],[5,6]]]
	, ["[[2,3],[5,6]], 1", addy([[2,3],[5,6]],y=1), [[2,4],[5,7]]]
	, ["[[2,3],[5,6]], [8,9]", addy([[2,3],[5,6]],y=[8,9]), [[2,11],[5,15]]]
	//, ["pts", addy(pts), [[2,3,4],[5,6,7]]]
	, ["[[2,3,4],[5,6,7]], 1", addy(pts,y=1), [[2,4,4],[5,7,7]]]
	, ["[[2,3,4],[5,6,7]], [2,3]", addy(pts,y=[2,3]), [[2,5,4],[5,9,7]]]
	], ops, ["pts",pts]);
    }

    
//========================================================
addz=[ "addz", "pt,z=0", "array", "Point"
, "Add z to pt. See doc in addx."
];
    
function addz(pt,z=0,_I_=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addz(pt[i], isarr(z)?z[i]:z) ]
	: len(pt)==3
       ? [ pt.x,pt.y,pt.z+z ] 
	   : [ pt.x,pt.y,z] 
);    
    
    module addz_test( ops=["mode",13] )
    {
	pts = [[2,3,4],[5,6,7]];

	doctest( addz,
	[ "// To a point:"
	//, ["[2,3]", addz([2,3]), [2,3,0]]
	, ["[2,3], 1", addz([2,3],z=1), [2,3,1]]
	//, ["[2,3,4]", addz([2,3,4]), [2,3,4]]
	, ["[2,3,4], 1", addz([2,3,4],z=1), [2,3,5]]
	, "// To points:"
	//, ["[[2,3],[5,6]]", addz([[2,3],[5,6]]), [[2,3,0],[5,6,0]]]
	, ["[[2,3],[5,6]], 1", addz([[2,3],[5,6]],z=1), [[2,3,1],[5,6,1]]]
	, ["[[2,3],[5,6]], [8,9]", addz([[2,3],[5,6]],z=[8,9]), [[2,3,8],[5,6,9]]]
	//, ["pts", addz(pts), [[2,3,4],[5,6,7]]]
	, ["[[2,3,4],[5,6,7]], 1", addz(pts,z=1), [[2,3,5],[5,6,8]]]
	, ["[[2,3,4],[5,6,7]], [2,3]", addz(pts,z=[2,3]), [[2,3,6],[5,6,10]]]
	], ops, ["pts",pts]);
    }

//========================================================
angle=["angle","pqr","number","Angle, Geometry",
 " Given a 3-pointer (pqr), return the angle of P-Q-R. "
];

function angle(pqr)=
 let( P=pqr[0], Q=pqr[1], R=pqr[2] )
(
	acos( (( R-Q)*( P- Q) ) / d01(pqr) / d12(pqr) )
);

    module angle_test( ops = ["mode",13] )
    {
	p = [1,0,0];
	q = [0,sqrt(3),0];
	r = ORIGIN;

	scope=[ "pqr1", [ p,q,r]
		,  "pqr2", [ p,r,q]
		,  "pqr3", [ r,p,q]
		,  "pqr4", randPts(3)
		,  "pqr5", randPts(3)
		,  "pqr6", randPts(3)
		,  "pqr7", [ORIGIN, [1,0,0], [3,2,0]]
		];
	function scp(k)= hash(scope, k);
	
	doctest( angle,
	[
		["pqr1", angle(scp("pqr1")),30 ]
		,["pqr2", angle(scp("pqr2")),90 ]
		,["pqr3", angle(scp("pqr3")),60 ]
		,["pqr7", angle(scp("pqr7")),135 ]
		,"// pqr4,5,6 are Random 3-pointers (can't be tested): "
		,str(">> angle(pqr4) = ", _fmt( angle( scp("pqr4"))))
		,str(">> angle(pqr5) = ", _fmt( angle( scp("pqr5"))))
		,str(">> angle(pqr6) = ", _fmt( angle( scp("pqr6"))))
	
	], ops, scope
	);

    }
//angle_test(); 
//doc(angle);
module angle_demo_s()
{_mdoc("angle_demo_s", 
" P = [1,0,0]
;; Q = ORIGIN
;; R = [0,sqrt(3),0]
");    
    P = [1,0,0];
    Q = ORIGIN;
    R = [0,sqrt(3),0];
	
    pqr = [P,Q,R];
	Chain( pqr );
	MarkPts( pqr, ["labels", "PQR"] );
	echo("angle( pqr )=", angle([P,Q,R]));
	echo("angle( rpq )=", angle([R,P,Q]));
    echo("angle( prq )=", angle([P,R,Q]));
    
//	pqr = randPts(3);
//	P=P(pqr); Q=Q(pqr); R=R(pqr);
//	S = randPt();
//	//echo( projPt(S, pqr));
//	T= othoPt( [P,S,Q] );
//	J= projPt( S, pqr );
//
//	Chain( pqr );
//	MarkPts( pqr, ["labels", "PQR"] );
//	MarkPts([S,T,J],["labels","STJ"] );
//
//	color("red", 0.5) Chain( [P,S,Q], ["r",0.03] );
//	color("green", 0.5) Chain( [S,T,J ], ["r",0.03, "closed",true] );
//	Mark90( [P,T,S] );
//    Mark90( [T,J,S] );
//	Plane( pqr, ["mode",4] );
//	//Plane( [P,Q, normalPt(pqr)], ["mode",4] );
//
//	a = angle( pqr,S );
//	echo("angle( pqr,S )=", a);
//	echo("angle([J,T,S])=", angle([J,T,S]) );
//	scale(0.03) text( str( a ) );

}
//angle_demo_s();
    


///// core ========================================================
angleBisectPt=["angleBisectPt", "pqr,len=undef,ratio=undef", "point", "Angle,Point,Geometry",
" Given a 3-pointer (pqr), return a point D on line PR such that the
;; line QD is aPQR's angle bisector (divide aPQR evenly).
;;
;;          _.Q
;;        _- /|
;;      _'  / |
;;   _-'   /  |
;;  P-----D---R
;; 
;; New 150125: Add len and ratio arguments
"]; 

function angleBisectPt( pqr, len=undef, ratio=undef )= 
(
    let( Q= pqr[1]
        ,D= onlinePt( p02(pqr) 
            , ratio= norm(pqr[0]-pqr[1])
                    /(norm(pqr[0]-pqr[1])
                     +norm(pqr[1]-pqr[2])) 
                   )
     )
    len!=undef? onlinePt( [Q,D], len=len )
        :ratio!=undef? onlinePt( [Q,D], ratio=ratio)
        :D
);

module angleBisectPt_test( ops=["mode",13] )
{ 

	scope=[ "pqr1", randPts(3)
		 , "pqr2", randPts(3)
		 ];
	pqr1= hash(scope, "pqr1");
	pqr2= hash(scope, "pqr2");

	angle1 = angle( pqr1 );
	angle2 = angle( pqr2 );

	doctest( angleBisectPt, 
	[
		""
		, "We get 2 random 3-pointers, pqr1 and pqr2, to see if the angle is divided evenly"
		,""
		,["pqr1", angle([pqr1[0],pqr1[1]
                 , angleBisectPt( pqr1 )])
                , angle(pqr1)/2, ["funcwrap","angle([p,q,{_}])= angle(pqr1)/2"] ]
		,""
		,"With pqr2:"
		,""
		,["pqr2", angle([pqr2[0],pqr2[1], angleBisectPt( pqr2 )])
				, angle(pqr2)/2
				, ["funcwrap","angle([p,q,{_}])= angle(pqr2)/2"] ]
		,""
		,"Note that this is an example of using other function, angle(), to test the"
		,"targeted one (angleBisectPt), which showcases the power of doctest()."
	]
	, ops,scope
	);

}

module angleBisectPt_demo_1()
{
	echom( "angleBisectPt_demo_1" );
	pqr = randPts(3);
	
	p=pqr[0];
	q=pqr[1];
	r=pqr[2];

	d = angleBisectPt(pqr);

	s=_s("<br/>|>--------&lt;  angleBisectPt_demo_1  >--------
     <br/>|> Get 3 random pts: 
     <br/>|> pqr={_}
     <br/>|> d = angleBisectPt(pqr)= {_}
     <br/>|> Check if two angles divided by QD are equal:
     <br/>|> angle( [p,q,d] ) = {_}
     <br/>|> angle( [r,q,d] ) = {_}
     <br/>|>"
      ,[ _fmt(pqr)
	  , _fmt(angleBisectPt(pqr))
       , _fmt( angle([p,q,d]) )
       , _fmt( angle([r,q,d]) )
       ]
    );
 
	echo(s);

	/*echo_("pqr= {_}", [_fmt(pqr)]);
	echo( "angleBisectPt(pqr)= d =  ", _fmt(angleBisectPt(pqr)  ));	
	echo( "angle([p,q,d]) = ", _fmt( angle([p,q,d]) ));
	echo( "angle([r,q,d]) = ", _fmt( angle([r,q,d]) ));
	*/
}

module angleBisectPt_demo_2(){
	echom("angleBisectPt_demo_2");
    ops= ["r",0.04,"markpts",true];
	
	pqr=[ [0,-1,0 ], [1,2,0 ],[4,0,0 ]];
    pqr = randPts(3);
    Q = pqr[1];
    Chainf( pqr, ops=["closed",true] );
    
    D= angleBisectPt( pqr );
    
    E = angleBisectPt( pqr, ratio=0.5 );
    F = angleBisectPt( pqr, len=5 );

    MarkPts([D,E,F], ["labels","DEF"] );
    Line0( [Q,F], ["r", 0.01] ); 
    
    Dim2( [Q,F, N([ P(pqr),D,F])] ); 
    
}

//angleBisectPt_demo_1();
//angleBisectPt_demo_2();




//==============================================
anglePt=["anglePt","pqr,a,len", "point", "Angle,Point,Geometry",
" Given a 3-pointer (pqr), an angle (a) and a length (len), return the point
;; S that is len away from Q with an angle *a* away from PQ line on the R
;; side of PQ line. It is co-plane with pqr. Refer to *anyAnglePt()* for an 
;; anglePt in any plane.
;;
;;           R     _ S
;;         -'   _-' 
;;       -'  _-'  len
;;     -' _-' 
;;   -'_-'  a
;;  Q-------------------P
"];

function anglePt( pqr, a, len=1)=
(


/*
       B-----------_R-----A 
       |         _:  :    |
       |  T._---:-----:---+ S 
       |  |  '-:_      :  | 
       |  |   :  '-._   : |
       |  |  :     a '-._:| 
       p--+--+-----------'Q
          U  D    


       B----_R-----A--._ 
       |      :    |    ''-_
       |       : S +--------T 
       |        :  |      -'|
       |         : |   _-'  |
       |          :|_-'     |
       p----------'Q--------U

            a = a_PQT
*/
	abs(a)==90? // if a=90, return S
		onlinePt( [ Q(pqr), squarePts( pqr )[2] ]  // [Q,A]
			    , len= sign(a)*len
				)
	:abs(a)==270? // if a=270
		onlinePt( [ Q(pqr), squarePts( pqr )[2] ]  // [Q,A]
			    , len= -sign(a)*len
				)
	:squarePts(
	[ 
		// U
		onlinePt( p10(pqr) // [Q,P] // squarePts( pqr )[2] ]  // [Q,A]
		    , len= len*cos(a)
			)
		// Q	
		, Q(pqr)

		// S
		,onlinePt( [ Q(pqr), squarePts( pqr )[2] ]  // [Q,A]
		    , len= len*sin(a)
			)	
	])[3]

);

module anglePt_test( ops=["mode",13] )
{ 
	doctest( anglePt, [], ops);
}

module anglePt_demo_1()
{ _mdoc("anglePt_demo_1"
,"Make angle 30 and 60"
);
    pqr = randPts();
    P= P(pqr);
    Q= Q(pqr);
    R= R(pqr);
    
    Chain( pqr, ["closed",false,"r",0.02]);
    MarkPts( pqr );
    LabelPts( pqr, "PQR");
    
    A30 = anglePt( pqr, 30, len=1.5);
    A60 = anglePt( pqr, 60, len=2.5);
    
    Chain( [A30, Q, A60], ["closed",false, "r",0.03,"color","green"] );
    
    MarkPts( [A30,A60] );
    LabelPts( [A30,A60], [" A30"," A60"]);
    
	// make a random square, pqab

//	pqab = squarePts(randPts(3));
//	P = pqab[0]; 
//	Q = pqab[1]; 
//	A = pqab[2]; 
//	B = pqab[3];
//	Chain( pqab, ["r",0.01] );
//	MarkPts( pqab );
//	LabelPts( pqab, "PQAB" );
//
//	// Get the R with arbitrary distance between A,B:
//
//	ratio = rands(0,1,1)[0];
//	R = onlinePt( [A,B], ratio=ratio );
//	LabelPt( R, "R", ["color","black"] );
//	pqr = [P,Q,R];	
//	Plane( pqr, ["mode",3] );
//
//	ang = 30; 
//	len = 1.5; 
//	T = onlinePt( [P,B], len= len); //len2>0?sin(a)*len2:len )
//	D = onlinePt( [P,Q], len= atan2(ang)*len); //len2>0?sin(a)*len2:len )
//	
//	S = squarePts( [D,P,T ] )[3];
//		 
//	LabelPts( [T,D,S], "TDS" );
//
//	Plane( [P,D,S,T]);



//	Ap = anglePt( [P,Q,R], ang, len=1.5);
//	echo( "angle DPAp= ", angle( [D,P,Ap] ));
//	MarkPts([Ap]);
//	LabelPt( Ap, ["Ap"] );
//	Line0([P,Ap]);

}

module anglePt_demo_2()
{_mdoc("anglePt_demo_2"
,""
);
	// make a random square, pqab

	pqr = randPts(3);
	as = 6;
	MarkPts( pqr );
	LabelPts( pqr, "PQR" );
	Chain( pqr, ["closed",false] );
	P = pqr[0];
	Q = pqr[1];
	R = pqr[2];

	//echo( "lens = ", lens );

	module plotAngp(a,i)
	{ 
		//echo( "a = ", a );
		p = anglePt( pqr, a=a, r= min( d01(pqr), d02(pqr),d12(pqr)) );
		LabelPt( p, str("P",i));
		Line0( [Q,p], ["r", 0.01]);
		MarkPts( [p] );
	}
	for (i = [0:as-1] ){
		a = 360*i/as;
        //echo("i = ", i, "a= ", a);
		plotAngp( a,i );
	}
//	for (i = [0:as-1] ) assign(a = 360*i/as){
//		//echo("i = ", i, "a= ", a);
//		plotAngp( a,i );
//	}

}
module anglePt_demo_3_90()
{ _mdoc("anglePt_demo_3_90"
," First we made a random right-angle pqr using
;; randRightAnglePts(), then make a S that is on the pqr
;; plane using anglePt(pqr), then using anglePt(pqs, 90) 
;; to make a pt90 that is right angle to line PQ.
"    
);
// NOTE (7/7/2014) :
//
// anglePt(pqr, a=90) sometimes produces [nan,nan,nan]
//
// 4 example cases in which it goes wrong:
//
// ECHO: "pqr", [[2.06056, 0.685988, 6.36888], [3.00336, 1.06282, 2.49988], [2.22362, -0.236707, 2.1833]]
// ECHO: "pqs", [[2.06056, 0.685988, 6.36888], [3.00336, 1.06282, 2.49988], [1.95778, -0.257458, 3.5786]]
//ECHO: "pt90", [nan, nan, nan]

//ECHO: "pqr", [[4.62891, -4.93576, 0.252527], [2.45234, -1.79303, 1.42973], [2.24605, -2.05891, 1.75812]]
//ECHO: "pqs", [[4.62891, -4.93576, 0.252527], [2.45234, -1.79303, 1.42973], [2.60142, -3.70384, 2.00122]]
//ECHO: "pt90", [nan, nan, nan]

//ECHO: "pqr", [[1.15245, -2.62106, 1.36749], [1.01775, -2.65868, -2.63006], [1.05541, -3.91357, -2.61952]]
//ECHO: "pqs", [[1.15245, -2.62106, 1.36749], [1.01775, -2.65868, -2.63006], [1.1078, -4.05891, -1.20484]]
//ECHO: "pt90", [nan, nan, nan]

//ECHO: "pqr", [[-0.394555, 0.856362, -1.03282], [0.291087, -0.669823, 2.60045], [-0.6279, -0.364459, 2.90214]]
//ECHO: "pqs", [[-0.394555, 0.856362, -1.03282], [0.291087, -0.669823, 2.60045], [-1.23265, 0.295526, 1.73654]]
//ECHO: "pt90", [nan, nan, nan]

	echom("anglePt_demo_3_90");
	pqr = randRightAnglePts(r=4); // make a right triangle pqr
	echo("pqr", pqr);
	echo("angle pqr", angle(pqr));

	P = pqr[0];
	Q = pqr[1];
	R = pqr[2];
	S = anglePt(pqr, a=45, r=2); // create a 45 deg pt, S, on pqr plane
		
	echo("pqs", [P,Q,S]);
	pt90 = anglePt( [P,Q,S], a=90, r=2 ); // use pqs as new 3-pointer to get 
									 // a 90-deg pt
	echo("pt90", pt90);
	MarkPts([pt90], ["r",0.1, "transp",0.2]);
	//Line0( [Q, pt90], ["transp",0.2] );

	MarkPts( [P,Q,R,S], ["labels", "PQRS"] );
    LabelPt( pt90, " pt90");
	Line0( [Q,S], ["r", 0.02] );
	Mark90( pqr );
	//Line0([P,Q]); //Chain( pqr );
    Chain( pqr, ["r",0.01, "closed",false]);
}

module anglePt_demo_4_90_debug()
{_mdoc("anglePt_demo_4_90_debug"
," This is a debug version of demo_3_90. It simply checks
;; several cases of pqr in which anglePt(pqr, a=90) returns 
;; [nan,nan,nan]. It should have been fixed by now. 
");
    
// NOTE (7/7/2014) : use the example cases in demo_3 that generate errors:
//
// anglePt(pqr, a=90) sometimes produces [nan,nan,nan]
//
// 4 example cases in which it goes wrong:
//
	pqr1= [[2.06056, 0.685988, 6.36888], [3.00336, 1.06282, 2.49988], [1.95778, -0.257458, 3.5786]];
	pqr2= [[4.62891, -4.93576, 0.252527], [2.45234, -1.79303, 1.42973], [2.24605, -2.05891, 1.75812]];
	pqr3= [[1.15245, -2.62106, 1.36749], [1.01775, -2.65868, -2.63006], [1.05541, -3.91357, -2.61952]];
	pqr4= [[-0.394555, 0.856362, -1.03282], [0.291087, -0.669823, 2.60045], [-1.23265, 0.295526, 1.73654]];

    pt1= anglePt( pqr1, a=90, r=2 );
    pt2= anglePt( pqr2, a=90, r=2 );
    pt3= anglePt( pqr3, a=90, r=2 );
    pt4= anglePt( pqr4, a=90, r=2 );
    
    echo("pqr1=>", pt1);
    echo("pqr2=>", pt2);
    echo("pqr3=>", pt3);
    echo("pqr4=>", pt4);
	
//	//echom("anglePt_demo_4_90_debug");
//	pqr = pqr2;
//	echo("pqr1", pqr);
//	echo("angle pqr1", angle(pqr));
//
//	P = pqr[0];
//	Q = pqr[1];
//	R = pqr[2];
//	S = anglePt(pqr, a=45, r=2); // create a 45 deg pt, S, on pqr plane
//		
//	echo("pqs", [P,Q,S]);
//	echo("angle pqs", angle([P,Q,S]));
//
//	pt90 = anglePt( [P,Q,S], a=90, r=2 ); // use pqs as new 3-pointer to get 
//									 // a 90-deg pt
//	echo("pt90", pt90);
//	MarkPts([pt90], ["r",0.1, "transp",0.2]);
//	Line0( [Q, pt90], ["transp",0.2] );
//
//	MarkPts( [P,Q,R,S], ["labels", "PQRS"] );
//	Line0( [Q,S], ["r", 0.02] );
//	Mark90( pqr );
//	Line0( [P,Q] ); //Chain( pqr );

	// Check the code in anglePt() that is sent to squarePts:
	//
	//	squarePts(
	//	[ 
	//		// U
	//		onlinePt( p10(pqr) // [Q,P] // squarePts( pqr )[2] ]  // [Q,A]
	//		    , len= r*cos(a)
	//			)
	//		// Q	
	//		, pqr[1]
	//
	//		// S
	//		,onlinePt( [ pqr[1], squarePts( pqr )[2] ]  // [Q,A]
	//		    , len= r*sin(a)
	//			)	
	//	])[3]

//	U = onlinePt( p10([P,Q,S]) , len= 2*cos(90) );
//	Q2= [P,Q,S][1];
//	S2= onlinePt( [ pqr[1], squarePts( [P,Q,S] )[2] ], len= 2*sin(90));
//
//	echo("[U,Q2,S2]", [U,Q2,S2]);
//	echo("squarePts([U,Q2,S2])", squarePts([U,Q2,S2]));


}

module anglePt_demo()
{
	anglePt_demo_1();
	//anglePt_demo_2();
	//anglePt_demo_3_90();
	//anglePt_demo_4_90_debug();
}
//anglePt_demo();




//==============================================
anyAnglePt=["anyAnglePt","pqr,a,len=1,pl=undef", "point", "Angle,Point,Geometry",
" Given 3-pointer (pqr), angle (a), length (len) and plane (pl), return a point 
;; that is *len* away from Q at an angle *a*. The pl decides on what plane the angle
;; is formed. Referring to the following graph, where N is the normalPt of pqr, and 
;; M is the normalPt of NQP:
;;
;;   pl= undef (default): aRQN
;;   pl= xy,pm or pr: aPQR or aPQM ( = anglePt(pqr) )
;;   pl= yz,mn: aMQN
;;   pl= xz,pn: aNQP
;;
;; plane= pn or xz
;; a = a3
;; dir: aPQN  (z)
;;             N      a2 .--
;;        --.  |         |'-_      plane= mn or yz
;;     a3 -'|  |_________    '-_   a = a2
;;      -'    /| '-_     |         dir: aMQN
;;           / |    '-_  |
;;          |  |       : |
;;          |  Q-------|---------------------------- M (y)
;;          | / '-._/  |     .--     plane = undef
;;          |/_____/'-.|     |'.     a=a0
;;          /           '-._    '.   dir: aRQN
;;         /      ._        '-._  a0
;;        /    ------>  a1      '-._
;;       /        -'                '-._
;;      /                               R
;;     /  plane = xy, pm or pr
;;    P   a = a1
;;   (x)  dir: aPQM
"];

function anyAnglePt( pqr, a, len=1, pl=undef)=  // pqn, pqr, qnn, qrn
(
	anglePt
	( 
	   pl=="yz"||pl=="mn"?          [ N2(pqr,len=-1), Q(pqr), N(pqr) ]
	 : pl=="xy"||pl=="pm"||pl=="pr"? pqr //[ P(pqr), Q(pqr), N2(pqr,reverse=true) ]
	 : pl=="xz"||pl=="pn"?          [ P(pqr), Q(pqr), N(pqr) ]
	 :                              [ R(pqr), Q(pqr), N(pqr) ]
	, a=a, len=len
	)
);


module anyAnglePt_test( ops=["mode",13] )
{ 
	doctest( anyAnglePt, 
	[], ops
	);

}
module anyAnglePt_demo_1()
{_mdoc("anyAnglePt_demo_1"
," pqr: random
;; N,M: normal to pqr
;; rn : anyAnglePt( pqr, a=30, len=L)
;; xy : anyAnglePt( pqr, a=30, len=L, pl=''xy'')
;; xz : anyAnglePt( pqr, a=30, len=L, pl=''xz'')
;; yz : anyAnglePt( pqr, a=30, len=L, pl=''yz'')     
");
    
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "color","red" ] );
	Line0([Q,N],["color","green"]);
	Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	L = norm(R-Q)/2;

	pt_rn =anyAnglePt( pqr, a=30, len=L);
	pt_xy =anyAnglePt( pqr, a=30, len=L, pl="xy");
	pt_xz =anyAnglePt( pqr, a=30, len=L, pl="xz");
	pt_yz =anyAnglePt( pqr, a=30, len=L, pl="yz");

	Line0( [Q, pt_rn], ["r",0.01,"color","red"] );
	Line0( [Q, pt_xy], ["r",0.01,"color","green"] );
	Line0( [Q, pt_xz], ["r",0.01,"color","blue"] );
	Line0( [Q, pt_yz], ["r",0.01,"color","purple"] );

	Line( [ projPt(pt_rn, pqr), pt_rn],["head",["style","cone"]] );
	Line( [ projPt(pt_xy, [P,Q,N]), pt_xy],["head",["style","cone"]] );
	Line( [ projPt(pt_xz, pqr), pt_xz],["head",["style","cone"]] );
	Line( [ projPt(pt_yz, pqr), pt_yz],["head",["style","cone"]] );
    
//	Line( [ onlinePt([Q,R], len=norm(pt_rn-Q)), pt_rn],["head",["style","cone"]] );
//	Line( [ onlinePt([Q,P], len=norm(pt_xy-Q)), pt_xy],["head",["style","cone"]] );
//	Line( [ onlinePt([Q,P], len=norm(pt_xz-Q)), pt_xz],["head",["style","cone"]] );
//	Line( [ onlinePt([Q,M], len=norm(pt_yz-Q)), pt_yz],["head",["style","cone"]] );


	MarkPts( [pt_rn, pt_xy, pt_xz, pt_yz]
		,["labels", ["rn", "xy","xz","yz"]] );
	

}


module anyAnglePt_demo_2()
{
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "color","red" ] );
	Line0([Q,N],["color","green"]);
	Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	function getpts(count=30, a=rand(360), len=1, pl="yz")=
	(

		count>0? concat( [anyAnglePt( pqr,a=a,len=len,pl=pl)]
					, getpts( count-1, a=rand(360), len=len, pl=pl )
					):[]
	);


	pts_xy = getpts(pl="xy", len=1);
	MarkPts( pts_xy, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );

	pts_yz = getpts(40, pl="yz", len=1.5);
	//echo("pts_yz", pts_yz);
	MarkPts( pts_yz, ["colors",["green"], "samesize",true
				, "sametransp",true, "r",0.05] );


	pts_xz = getpts(50, pl="xz", len=2);
	MarkPts( pts_xz, ["colors",["blue"], "samesize",true
				, "sametransp",true, "r",0.05] );

}

module anyAnglePt_demo_3()
{
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "color","red" ] );
	Line0([Q,N],["color","green"]);
	Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );


	function getpts(count=30,  len=1, pl="yz",_I_=0)=
	(
		_I_<count? concat( [anyAnglePt( pqr,a= _I_*360/count,len=len,pl=pl)]
					, getpts( count, len=len, pl=pl,_I_=_I_+1 )
					):[]
	);

 
	pts_xy = getpts(pl="xy", len=1);
	MarkPts( pts_xy, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts_xy, ["r",0.01, "color","red"] );

	pts_yz = getpts(30, pl="yz", len=1.5);
	//echo("pts_yz", pts_yz);
	MarkPts( pts_yz, ["colors",["green"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts_yz, ["r",0.01, "color","green"] );


	pts_xz = getpts(30, pl="xz", len=2);
	MarkPts( pts_xz, ["colors",["blue"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts_xz, ["r",0.01, "color","blue"] );
}

module anyAnglePt_demo_4()
{
	echom("anyAnglePt_demo_4");
	pqr = randPts(3);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "color","red" ] );
	Line0([Q,N],["color","green"]);
	Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	function getpts(count=30,  len=1,_I_=0)=
	(
		_I_<count? concat( [anyAnglePt( pqr,a= _I_*360/count,len=len)]
					, getpts( count, len=len, pl=pl,_I_=_I_+1 )
					):[]
	);

 
	pts = getpts(len=dist([R,Q]));
	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts, ["r",0.01, "color","red"] );
	for( i = range( 30) ) Line0( [Q, pts[i]], ["r",0.01, "transp",0.4]);

}

module anyAnglePt_demo_5()
{_mdoc("anyAnglePt_demo_5"
," Follow a sine wave.
");
    
	echom("anyAnglePt_demo_5");
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false] );
	//Line0([Q,N],["color","green"]);
	//Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	count=36;
	function getSinePts( count=count, len=3, _I_=0)=
	(
		_I_<count? concat( 
			[
				//360/count*_I_, len*sin(360/count*_I_), 
				anyAnglePt( [P,Q, anglePt(pqr, 360/count*_I_)]
						,a=360/count*_I_
                        , len= len*sin(360/count*_I_))]
					, getSinePts( count, len=len, _I_=_I_+1 )
		):[]
	);


	pts = getSinePts();
	//echo("pts",pts);

	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts, ["r",0.01, "color","red"] );

	for( i = range( count) ) 
        Line0( [Q, pts[i]], ["r",0.01, "transp",0.4]);

}

//anyAnglePt_demo_1();
//anyAnglePt_demo_2();
//anyAnglePt_demo_3();
//anyAnglePt_demo_4();
//anyAnglePt_demo_5();




// =========================================================
arcPts=["arcPts", "pqr, rad=2, a, count=6, pl=''xy'', cycle, pitch=0", "points", "Point,Geometry",
" Given a 3-pointer(pqr), an angle(a), radius(rad), count of points from 
;; 0 ~ a (when cycle is not given) or count of points per cycle ( when
;; cycle is given), and optional cycle and pitch, return an array of 3d 
;; points for a curve spanning angle 0~a. Note that count 6= 6 points.
;; 
;; Since 1 cycle = 360 degrees, if cycle is given, the angle a is meaningless,
;; so will be ignored.
;;
;; The points will span a curve originating from the point Q, and on a 
;; plane defined by pl (see anyAnglePt doc for definition). 
;; 
;; If *pitch* is given, which defines the distant of *pitch* of a spiral, 
;; then a spiral will be drawn along the PQ line (i.e., the arc be drawn
;; on the yz plane => pl=''yz''), starting from Q to P. A negative pitch 
;; produces a spiral going P-to-Q direction starting from Q. 
;;
;; Note that the spiral arc feature is available only along the PQ line. 
;; 
 
"];
function arcPts( 
    pqr=randPts(3)
    , a=undef
    , r=1
    , count=6
    , pl="xy"
    , cycle=undef
    , twist = 0 
    , pitch=0)=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, _a= a==undef?angle(pqr):a
	, a = cycle?cycle*360:_a 
	, pl= pitch?"yz":pl 
	)
(
	[ for(i=range(count)) 
		anyAnglePt( [ P
					, onlinePt([Q,P],len= i*pitch/count)
					, R
				 	] , a= twist+ i*a/(count-(a==360?0:1))
				  , len=r, pl=pl) ]
);

module arcPts_test( ops=["mode",13] )
{
	doctest( arcPts, ops=ops );
}

module arcPts_demo_1()
{_mdoc("arcPts_demo_1",
" arcPts( pqr=randPts(3), a=90, r=1, count=6, pl=''xy'', cycle, pitch=0)
;; -- random pqr
;; -- red  : arcPts( pqr )
;; -- green: arcPts( pqr, r=2, a=150, count=10 ) 
;; -- blue : arcPts( pqr, r=3, a=360, count=12, pl=''yz'' )
");
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "transp",0.4] );
	//Line0([Q,N],["color","green"]);
	//Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	//----------------------------------
	pts = arcPts( pqr );
	echo("pts",pts);
	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts, ["r",0.01, "color","red","closed",false] );
	for( i = range( 6) ) Line0( [Q, pts[i]], ["r",0.01, "transp",0.4]);	


	//----------------------------------
	pts2= arcPts( pqr, r=2, a=150, count=10, pl="xz" );
	//echo("pts",pts);
	MarkPts( pts2, ["colors",["green"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts2, ["r",0.01, "color","green", "closed",false] );
	for( i = range( 10) ) Line0( [Q, pts2[i]], ["r",0.01, "transp",0.4]);	
	LabelPts( pts2, range(pts2) );


	//----------------------------------
	pts3= arcPts( pqr, r=3, a=360, count=12, pl="yz" );
	//echo("pts",pts);
	MarkPts( pts3, ["colors",["blue"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts3, ["r",0.01, "color","blue"] );
	for( i = range( 12 ) ) Line0( [Q, pts3[i]], ["r",0.01, "transp",0.4]);	
	LabelPts( pts3, range(pts3) );
}

module arcPts_demo_2()
{_mdoc("arcPts_demo_2",
"Use the cycle and pitch to make a spiral arc:
;;
;; arcPts( pqr, r=2, count=count, pl=''yz'', cycle=3, pitch= 2)
;;
;; pitch: distance between 2 cycles
");
	echom("arcPts_demo_2");
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["r",0.1,"transp",0.4, "closed",false] );
	Line0([Q,N],["color","green","r",0.06, "transp",0.2]);
	Line0([Q,M],["color","blue","r",0.06, "transp",0.2]);

// 	Mark90( [P,Q,N]);
//	Mark90( [P,Q,M] );
//	Mark90( [N,Q,M] );
//	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	// We are gonna make a spiral here. 
	cycles = 5; 
	pts_per_cycle   = 12;  // Each cycle has ? pts.
	pitch= 3;    // The distance between the start of this cycle
                           // to the next one
    //shift_per_pt = pitch / pts_per_cycle;
	
	//pts = [ for (c=range(cycles))
			
	 
	count = 36;
	pts = arcPts( pqr, r=2, count=count, pl="yz", cycle=3, pitch= 3);
	//echo("pts",pts);
	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05, "transp",0.5] );
	Chain( pts, ["r",0.01, "color","red", "transp", 0.5] );
	for( i = range( count) ) 
		Line0( [ pts[i], othoPt( [P, pts[i], Q]) ]
			, ["r",0.03, "transp",0.8]);	

}


module arcPts_demo_3_twist()
{_mdoc("arcPts_demo_3_twist",
" 
");
    
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= onlinePt([ Q, N(pqr)], len=4); 
    M= N2(pqr, len=-4);

// 	Mark90( [P,Q,N]);
//	Mark90( [P,Q,M] );
//	Mark90( [N,Q,M] );
//	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	Chain( [N,Q,M], ["r",0.01,"closed",false] );
    MarkPts( [N,M], ["labels","NM"]);
    Chainf(pqr);

    module fmtchain( pts, color, labelbeg=0)
    {
        MarkPts( pts, ["colors",[color], "samesize",true
				, "sametransp",true, "r",0.05] );
        Chain( pts, ["r",0.01, "color",color,"closed",false] );
        for( i = range(pts) ) Line0( [Q, pts[i]]
            , ["r",0.01, "transp",0.4]);	
        if( labelbeg=="P" )
        {LabelPts( pts, "PQRSTUVWXYZ", ops=["scale", 0.02] ); }
        if( labelbeg==0 )
        {LabelPts( pts, range(pts), ops=["scale", 0.02] ); }
        
    };    
	//----------------------------------
	pts = arcPts( pqr, a=90, r=2, count=4 );
	fmtchain(pts);
    
	pts2 = arcPts( pqr, a=90, r=2.5, count=4, twist=10 );
	fmtchain(pts2, "red");

    
//    MarkPts( pts, ["colors",["red"], "samesize",true
//				, "sametransp",true, "r",0.05] );
//	Chain( pts, ["r",0.01, "color","red","closed",false] );
//	for( i = range( 6) ) Line0( [Q, pts[i]], ["r",0.01, "transp",0.4]);	

}

//arcPts_demo_1();
//arcPts_demo_2();
//arcPts_demo_3_twist();



//// core ========================================================
arrblock=[ "arrblock","arr,indent=''  
'',v=''| '',t=''-'',b=''-'',lt=''.'',lb=\"'\"","array","Array, Doc",
 " Given an array of strings, reformat each line
;; for echoing a string block . Arguments:
;;  indent: prefix each line; 
;;  v : vertical left edge, default '| '
;;  t : horizontal line on top, default '-'
;;  b : horizontal line on bottom, default '-'
;;  lt: left-top symbol, default '.'
;;  lb: left-bottom symbol, default '
"
];

function arrblock(arr, indent="  "
					, v="| "
					, t="-"
					, b="-"
					, lt="."
					, lb="'"
					, lnum=undef
					, lnumdot=". "
					 
)=// _i_=-1)=
(
  concat( [ str( indent, lt,repeat(t, 45) ) ]
		, isint(lnum)
		  ? [ for( i=range(arr) ) str( indent, v, i+lnum, lnumdot, arr[i]) ]
		  : [ for( i=range(arr) ) str( indent, v,                , arr[i]) ]
		, [ str( indent, lb,repeat(b, 45) ) ]
  ) 
//	_i_==-1?concat( t?
//					[ str( indent, lt,repeat(t, 45) )						 
//					]:[]
//				  ,	arrblock( arr, indent=indent,v=v,t=t,b=b ,lt=lt,lb=lb, _i_=0 )
//				  )
//	  :_i_==len(arr)? (b? [str(indent, lb, repeat(b, 45) )	]:[])
//	    : concat( [str( indent, v, arr[_i_] )]
//				, arrblock( arr, indent=indent,v=v,t=t,b=b ,lt=lt,lb=lb, _i_=_i_+1)
//				)
);

module arrblock_test(ops)
{

	doctest
	(
		arrblock
		,[
			["[''abc'',''def'']", join(arrblock(["abc","def"]),"<br/>")
			, join( [ "  .---------------------------------------------"
			  		, "  | abc"
			  		, "  | def"
			  		, "  '---------------------------------------------"
					], "<br/>" ) 
			  		
			, ["funcwrap", "join({_}, ''&lt;br/&gt;'')" ]
			]

		,	["[''abc'',''def'', ''ghi'']"
			, join(arrblock(["abc","def","ghi"], lnum=1),"<br/>")
             , join( [ "  .---------------------------------------------"
			  		, "  | 1. abc"
			  		, "  | 2. def"
			  		, "  | 3. ghi"
			  		, "  '---------------------------------------------"
					], "<br/>" ) 
			  		
			, ["funcwrap", "join({_}, ''&lt;br/&gt;'')" ]
			]
		 ]
		,ops
	);
}
//for (line=arrblock(["abc","def"])) echo(line);
//arrblock_test( ["mode", 22] );
//doc(arrblock);


//========================================================
begwith=[ "begwith", "o,x", "true|false",  "String, Array, Inspect",
" Given a str|arr (o) and x, return true if o starts with x.
;;
;; New 2014.8.11:
;;
;; x can be an array, return true if o begins with any item in x.
"];

function begwith(o,x)= 
(
	x==""||x==[]
	? undef
	: isarr(x)
	  ? sum( [for(i=range(x)) begwith(o,x[i])?1:0 ] )>0
	  : index(o,x)==0
);
module begwith_test(ops)
{

	begwith_tests=
	[ 
  	  [ "''abcdef'', ''def''", begwith("abcdef", "def"), false ]
	, [ "''abcdef'', ''abc''",  begwith("abcdef", "abc"),true ]
	, [ "''abcdef'', ''''",  begwith("abcdef", ""),undef ]
	, [ "[''ab'',''cd'',''ef''], ''ab''", begwith(["ab","cd","ef"], "ab"),true]
	, [ "[], ''ab''", begwith([], "ab"),false]
	, [ "'''', ''ab''", begwith("", "ab"),false]
	, "// New 2014.8.11"
  	, [ "''abcdef'', [''abc'',''ghi'']", begwith("abcdef", ["abc","ghi"]), true ] 
  	, [ "''ghidef'', [''abc'',''ghi'']", begwith("ghidef", ["abc","ghi"]), true ] 
  	, [ "''aaadef'', [''abc'',''ghi'']", begwith("aaadef", ["abc","ghi"]), false ] 
 	, [ "[''ab'',''cd'',''ef''], [''gh'',''ab'']", begwith(["ab","cd","ef"], ["gh","ab"]),true]
	];
	
	doctest( begwith, begwith_tests, ops );
}
//begwith_test( ["mode",22] );
//doc(begwith);

// ========================================================
between=["between", "low,n,high,include=0", "T|F", "Inspect"  ,
" Given a number (n) and low/high bounds, return true if n is between them.
;; include decides if the bounds be included. Set it to 0 (open, not included)
;; or 1 (closed, included), or [0,1] or [1,0] to set them differently.
"];

function between( low, n, high, include=0 )=
(
  (
	(include==1 || include[0]) ? 
		( low<=n) : (low<n )
  )&&
  (
	(include==1 || include[1]) ? 
		( n<=high) : (n<high )
  )
);

module between_test( ops = ["mode",13] )
{
	a=5;
   doctest( between,
	[
	  ["3,a,8", between(3,a,8), true ]
	  ,["3,a,5", between(3,a,5), false ]
	  ,["3,a,5,[0,1]", between(3,a,5,[0,1]), true, ["rem","Include right end"] ]
	  ,["3,a,5,1", between(3,a,5,1), true, ["rem","Include both ends"] ]
	  ,""	
	  ,["5,a,8", between(5,a,8), false ]
	  ,["5,a,8,[0,1]", between(5,a,8,[0,1]), false, ["rem","Include right end"] ]
	  ,["5,a,8", between(5,a,8), false ]
	  ,["5,a,8,[1,0]", between(5,a,8,[1,0]), true, ["rem","Include left end"] ]
	  ,["5,a,8,1", between(5,a,8,1), true, ["rem","Include both ends"] ]
	], ops, ["a",a] );    
}

//between_test();


//// core ========================================================
boxPts= ["boxPts","pq,bySize=true", "Array",  "3D, Point",
 " Given 2-pointer (pq) and bysize(default= true), return
;; a 8-pointer array representing the 8 corners of a box that
;; has all edges perpendicular to axes. The box starts from the
;; first pt (=p), and, if bysize=true, then the 2nd pt (q) is
;; the sizes of box along x,y,z. If bySize=false,  q is used
;; as the opposite corner. No matter which corner the p is at,
;; the return 8 points starts w/ the left-bottom pt  (the one
;; has the smaller x,y,z), then go anti-clockwise  to include
;; all 4 bottom pts, then same manner of the top  4 pts. So the
;; 5th pt (i=4) is right on top of the 1st.
;;
;;       z      4----------7_
;;       |      | '-_      | '-_
;;       |      0_---'-_---3_   '-_
;;   '-_ |        '-_   '5---------6
;;  ----'+------y    '-_ |      '-_|
;;       | '-_          '1---------2
;;            ' x
;;
;; NOTE: 
;;
;; 1) Use cubePts that is similar but produces box points not 
;;    necessary perpendicular to the axes;
;; 2) Use rodfaces(4) to generate the faces needed for polyhedron.
;;     
"];
 
function boxPts(pq, bySize=true)=
(
 	bySize?  
	  // q is edge lengths. Just replace pq below with [pq[0], pq[0]+pq[1]]
    [ [ minx([pq[0], pq[0]+pq[1]])
	  , miny([pq[0], pq[0]+pq[1]])
	  , minz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , miny([pq[0], pq[0]+pq[1]])
      , minz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , minz([pq[0], pq[0]+pq[1]]) ]
	, [ minx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , minz([pq[0], pq[0]+pq[1]]) ]
	, [ minx([pq[0], pq[0]+pq[1]])
      , miny([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , miny([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	, [ minx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	]
	: // we need min*, max* to "sort" the point coordinates
	[ [ minx(pq), miny(pq), minz(pq) ]
	, [ maxx(pq), miny(pq), minz(pq) ]
	, [ maxx(pq), maxy(pq), minz(pq) ]
	, [ minx(pq), maxy(pq), minz(pq) ]
	, [ minx(pq), miny(pq), maxz(pq) ]
	, [ maxx(pq), miny(pq), maxz(pq) ]
	, [ maxx(pq), maxy(pq), maxz(pq) ]
	, [ minx(pq), maxy(pq), maxz(pq) ]
	]
);

module boxPts_test( ops )
{
	scope=["pq",[[1,2,-1],[2,1,1]]];
	pq = hash(scope,"pq");

	doctest
	(
		boxPts
		,[
		  [	"pq", boxPts(pq), [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
							, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0]]
		  ]
		 ,[ "pq,true",boxPts(pq,true)
			, [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
			, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0]]      
		  ]
		]
		,ops, scope
	);
}

module boxPts_demo(pq = [ [2,1,-1],[3,2,1]], bySize=true)
{
	//Line0(pq);
	//MarkPts(pq);

	pts = boxPts( pq, bySize );

	for (i=[0:7]){
		translate(pts[i])
		if(i==0||i==4){
			color("red")
			sphere(r= i<4?0.08:0.05);
		}else if(i==1||i==5){
			color("green")
			sphere(r= i<4?0.08:0.05);
		}else if(i==2||i==6){
			color("blue")
			sphere(r= i<4?0.08:0.05);
		}else{
			sphere(r= i<4?0.08:0.05);
		}
	}

	for(p0=pq){
		translate(p0)
		color("green",0.1)
		sphere(r=0.2);
	}
}  	
//boxPts_demo(bySize=false);
//boxPts_test();
//



//========================================================
cirfrags=[ "cirfrags", "r,fn,fs=0.01,fa=12", "number", "Geometry",
 " Given a radius (r) and optional fn, fs, fa, return the number 
;; of fragments (NoF) in a circle. Similar to $fn,$fs,$fa but
;; this function (1) doesn't put a limit to r and (2) could return
;; a float. In reality divising a circle shouldn't have given partial
;; fragments (like 5.6 NoF/cicle). But we might need to calc the 
;; frags for an arc (part of a circle) so it'd better not to round it
;; off at circle level. 
;; fa: min angle for a fragment. Default 12 (30 NoF/cycle). 
;; fs: min frag size. W/ this, small circles have smaller 
;; NoFs than specified using fa. Default=0.01 (Note: built-in $fs 
;; has default=2). 
;; fn: set NoF directly. Setting it disables fs & fa. 
;;
;; When using fs/fa for a circle, min NoF = 5. 
;;
;; See: http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn

"];
 
function cirfrags(r, fn, fs=0.01, fa=12)=
( // from: get_fragments_from_r
  // http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn
    fn? max(fn,3)
	: max(min(360 / fa, r*2*PI / fs), 5)
);

module cirfrags_test( ops=["mode",13] )
{
	doctest( cirfrags, 
	[ 
		"// Without any frag args, the default frag is 30 (no matter r is):"
		,["r=6",   cirfrags(6), 30]
		,["r=3",   cirfrags(3), 30]
		,["r=0.1", cirfrags(0.1), 30]

		,""
		,"// fn (num of frags) - set NoF directly. Independent of r, too:"
		,""
		
		,["r=3,fn=10", cirfrags(3,fn=10), 10]
		,["r=3,fn=3",  cirfrags(3,fn=3), 3]
		,["r=0.05, fn=12", cirfrags(0.05, fn=12), 12]

		,""
		,"// fs (frag size)"
		,""
		,["r=1,fs=1",  cirfrags(1,fs=1), 6.28319-4.69282*1e-6]
		,["r=1,fs=1",  cirfrags(1,fs=1), 6.28319, ["prec",6,"rem","prec=6"]]
		,["r=1,fs=1",  cirfrags(1,fs=1), 6.28319, ["prec",5,"rem","prec=5"]]
		,["r=1,fs=1",  cirfrags(1,fs=1), 6.28319, ["prec",4,"rem","prec=4"]]
		,["r=3,fs=1",  cirfrags(3,fs=1), 18.8496]
		,["r=6,fs=1",  cirfrags(6,fs=1), 30]
		,["r=6,fs=2",  cirfrags(6,fs=2), 18.8496]
		,["r=12,fs=4", cirfrags(12,fs=4),18.8496]
		,["r=3,fs=10", cirfrags(3,fs=10),5]
		,["r=6,fs=100",cirfrags(6,fs=100), 5,["rem","min=5 using fa/fs"] ]

		,""
		,"// fa (frag angle)"
		,""
		,["r=3,fa=10", cirfrags(3,fa=10), 36]
		,["r=6,fa=100", cirfrags(6,fa=100), 5,["rem","min=5 using fa/fs"] ]
		,""

	]
	, ops
	);
}



//========================================================
cornerPt=[ "cornerPt", "pqr", "pt", "3D, Geometry, Point, Math",
"
 Given a 3-pointer (pqr), return a pt that's on the opposite side
;; of Q across PR line.
;;
;;        Q-----R  Q--------R
;;       /   _//    ^.      |^.
;;      /  _/ /       ^.    |  ^.
;;     /_/   /          ^.  |    ^.
;;    //    /             ^.|      ^. 
;;   P     pt               P        pt
"];
 

function cornerPt(pqr)=
(
	// midpt: (pqr[2]-poq[0])/2+pqr[0]
    onlinePt( [pqr[1],midPt	([pqr[0],pqr[2]])], ratio=2)
);

module cornerPt_test(ops)
{ doctest( cornerPt, 
	[ 
		[ "[  [2,0,4],ORIGIN, [1,0,2]   ]", cornerPt([ [2,0,4],ORIGIN,[1,0,2]  ]), [3,0,6]   ]
	], ops
 );
}
//cornerPt_test( ["mode",22] );
//doc(cornerPt);
 


//========================================================
countArr= ["countArr", "arr", "int", "Array, Inspect",
" Given array (arr), count the number of arrays in arr.
"];

function countArr(arr)= countType(arr, "arr");

module countArr_test( ops )
{
	doctest
	( 
	  countArr
	, [
	   [ "[''x'',2,[1,2],5.1]", countArr(["x",2,[1,2],5.1]), 1 ]
	  ]
	, ops
	);

} 
//doc(countArr);




//========================================================
countInt=["countInt", "arr", "int", "Array, Inspect",
" Given array (arr), count the number of integers in arr.
"];

function countInt(arr)= countType(arr, "int");

module countInt_test( ops )
{
	doctest
	( 
	  countInt
	, [
		[ "[''x'',2,''3'',5.3,4]", countInt(["x",2,"3",5.3,4]), 2 ]
	  ]
	, ops
	);

} 
//doc(countInt);




//========================================================
countNum= ["countNum", "arr", "int", "Array, Inspect",
" Given array (arr), count the number of numbers in arr.
"];

function countNum(arr)= countType(arr, "num");

module countNum_test( ops )
{
	doctest
	( 
	  countNum
	, [
		[ "[''x'',2,''3'',5.3,4]", countNum(["x",2,"3",5.3,4]), 3 ]
	  ]
	, ops
	);
} 
//doc(countNum);




//========================================================
countStr =  ["countStr", "arr", "int", "Array, Inspect",
" Given array (arr), count the number of strings in arr.
"];

function countStr(arr)= countType(arr, "str") ;
module countStr_test( ops )
{
	doctest
	( 
	  countStr
	, [
		[ "[''x'',2,''3'',5]", countStr(["x",2,"3",5]), 2 ]
	  ,	[ "[''x'',2, 5]", countStr(["x",2,5]), 1 ]
	  ]
	, ops
	);

} // countStr_test();
//doc(countStr);




//========================================================
countType= ["countType", "arr,typ", "int", "Array, Inspect",
" Given array (arr) and type name (typ), count the items of type typ in arr.\\
"];

function countType(arr, typ, _i_=0)=
(
	
     _i_<len(arr)?
	 ( 
		typ=="num"?
		((isnum(arr[_i_])?1:0)+countType(arr,"num",_i_+1))
		:((type(arr[_i_])==typ?1:0)+countType(arr, typ, _i_+1)) 
	 )
	 :0
	
	/*  
	//===================================================
	// #	The following version works, and skips the need of 
    // #     extra _i_=0. However, it seems to be much slower.
		
	len(arr)==0? 0
	: ( typ=="num"&& isnum(arr[0])
	  ||	 type(arr[0])==typ ? 1:0
      )+ (len(arr)>1 ? 
    //      countType(shrink(arr),typ) 
	    countType(slice(arr,1),typ) 
		 :0
         )
	//===================================================
	*/ 
);

module countType_test( ops )
{

	countType_tests=  
	[
		[ "[''x'',2,[1,2],5.1], ''str''"
			, countType(["x",2,[1,2],5.1], "str"), 1 ]
	  ,	[ "[''x'',2,[1,2],5.1,3], ''int''"
			, countType(["x",2,[1,2],5.1,3],"int"), 2 ]
	  ,	[ "[''x'',2,[1,2],5.1,3], ''float''"
			, countType(["x",2,[1,2],5.1,3],"float"), 1 ]
	];

	doctest(countType, countType_tests,ops);

} 
//doc(countType);



//========================================================
cubePts=["cubePts", "pqr,p=undef,r=undef,h=1", "points", "Points",
" Like boxPts but is not limited to axes-parallel boxes. That is, 
;; it produces 8 points for a cube of any size at any location along 
;; any direction. 
;; 
;; The input pqr = [P,Q,R0] is used to make the square PQRS that is
;; coplanar with R0. It is then added the h to make TUVW by finding 
;; the normal of pqr. Optional p,r,h: numbers indicating lengths as 
;; shown belo.
;;
;;     U------------------V_ 
;;     |'-_     R0        | '-_
;;     |   '-_ -''-_      |    '-_
;;     |   _. '-T---'-------------W
;;     |_-'     |      '-_|       |
;;     Q_-------|---------R_      | h
;;       '-_    |    r      '-_   |
;;       p  '-_ |              '-_|
;;             'P-----------------S
;;                        r
;; Return a 8-pointer: [P,Q,R,S,T,U,V,W] with order:
;;
;;     5----------6_ 
;;     | '-_      | '-_
;;     1_---'-_---2_   '-_
;;       '-_   '4---------7
;;          '-_ |      '-_|
;;             '0---------3
;;
;; Use rodfaces(4) to generate the faces needed for polyhedron. 
;; 
;; New 20141003: shift 
;;
;;     U------------------V_ 
;;     |'-_               | '-_
;;     |   '-_            |    '-_
;;    Q|------'-T-----------------W
;;     | '-_    |  r      |       |
;;     |_---'-_-|----------_      | 
;;       '-_   'P           '-_   | ---
;;          '-_ |              '-_|   -shift
;;             'X------------------ ---
"];

function cubePts( pqr, p=undef, r=undef, h=1,shift)=
 let( Q = Q(pqr)
	, h_vector = normalPt(pqr, len=h)-Q
	, shiftvec = onlinePt( [N(pqr), Q], len=-shift )-Q
	, pqr = isnum(shift)
			? [ for (p=pqr) p+shiftvec ] 
			:pqr 
    )	
(	concat( squarePts( pqr, p=p,r=r )
		  , squarePts( //addv( pqr, normalPt(pqr, len=h)-Q(pqr))
					  [for (pt=pqr) sign(h)>0?(pt-h_vector):(pt+h_vector) ]
                      ,p=p,r=r )
	)		
);

module cubePts_test(ops){ doctest(cubePts,ops=ops);}

module cubePts_demo()
{
	module demo(color="red", p=undef, r=undef, h=2)
	{
		pqr = randPts(3,r=4);
		MarkPts([ R(pqr)], ["labels",["R0"]] );
		Chain( pqr, ["r",0.02]);

		echo("p,r,h= ", p, r, h);

		cpts = cubePts(pqr,p=p,r=r,h=h);
		MarkPts( cpts, ["labels", "PQRSTUVW"] );
		Chain( slice(cpts, 0, 4 ), ["r",0.01]);
		Chain( slice(cpts, 4, 8 ), ["r",0.01]);
	
		Mark90( p3(cpts,4,0,1) );
		Mark90( p3(cpts,4,0,3) );
	
		//Normal( pqr );
		//MarkPts( [normalPt(pqr,len=1)], ["r",0.2,"transp",0.3] );//
//		Chain( addv( pqr, normalPt(pqr, len=1)-Q(pqr) ) );
//		Chain( 
//			squarePts( addv( pqr, normalPt(pqr, len=1)-Q(pqr)),p,r )
//			,["r",0.2,"transp",0.3] );
	
		pqrs = squarePts( pqr, p=p,r=r );
		Chain( pqrs, ["r",0.1, "transp",0.3] );		

		//echo("pq, qr = ", dist
		color(color, 0.2)
		polyhedron( points= cpts, faces = CUBEFACES );
	
		Dim( p3(cpts, 0,1,2),["color","red", "prefix", "p="] ); // measure pq
		Dim( p3(cpts, 2,1,0),["color","green", "prefix","r="]  ); // measure qr
		Dim( p3(cpts, 4,0,3),["color","blue", "prefix","h="]  ); // measure pt

	}

	//demo();
	//demo("green",p=3);
	//demo("blue",r=3);
	demo("purple",p=5,r=3);

}

//cubePts_demo();



//========================================================
del=["del", "arr,x,byindex=false,count=1", "array", "Array",
 " Given an array arr, an x and optional byindex, delete all
;; occurances of x from the array, or the item at index x when
;; byindex is true. 
;;
;; New 2014.8.7: arg *count* when byindex=true: del a number 
;; of items starting from index x.
"];

function del(arr,x, byindex=false,count=1)=
let( x = byindex? fidx(arr, x):x )
(
	(len(arr)==0?[]
	: byindex
	  ? [for(i=range(arr)) if (i<x || i>=x+count) arr[i] ]
	  : [for(k=arr) if (k!=x) k] 
//	  : concat( arr[0]==x? []:[arr[0]]
//				, del(shrink(arr),x, byindex=false)
//				)
	)
);

module del_test( ops )
{
	doctest
	(
		del
		,[ 
			 [ "[2,4,4,8], 4", del( [2,4,4,8], 4), [2,8] ]
			,[ "[2,4,6,8], 6", del( [2,4,6,8], 6), [2,4,8] ]
			,[ "[2,4,6,8], 2", del( [2,4,6,8], 2), [4,6,8] ]
			,[ "[3,undef, 4, undef], undef", del( [ 3,undef, 4, undef], undef) , [3,4] ]
			,"// byindex = true"
			,[ "[2,4,6,8], 0, true", del( [2,4,6,8], 0, true), [4,6,8] ]
			,[ "[2,4,6,8], 3, true", del( [2,4,6,8], 3, true), [2,4,6] ]
			,[ "[2,4,6,8], 2, true", del( [2,4,6,8], 2, true), [2,4,8] ]
			,"//"
			,"// New 2014.8.7: arg *count* when byindex=true"
			,"//"
			,[ "[2,4,6,8], 1, true", del( [2,4,6,8], 1, true), [2,6,8] ]
			,[ "[2,4,6,8], 1, true,2", del( [2,4,6,8], 1, true,2), [2,8] ,["rem","del 2 tiems"]]
			,[ "[2,4,6,8], 2, true,2", del( [2,4,6,8], 2, true,2), [2,4], ["rem","del only 1 'cos it's in the end."] ]
			,[ "[], 1, true", del( [], 1, true), [] ]
			

		 ]
		,ops
	);
}
//del_test(["mode",22]);
//doc(del);





//========================================================
delkey=[ "delkey", "h,k", "hash",  "Hash",
" Given a hash (h) and a key (k), delete the [key,val] pair from h. \\
"];
function delkey(h,key, _i_=0)=
(
     _i_>=len(h)? []
	 : concat( h[_i_]==key? []:[h[_i_], h[_i_+1]]
	         , delkey( h, key, _i_+2) 
			 )
);

module delkey_test( ops )
{
	scope=["h1", ["a",1,"b",2]
		  ,"h2", [1,10,2,20,3,30] ];
	h1 = hash( scope, "h1" );
	h2 = hash( scope, "h2");

	doctest
	(
	 delkey
	,[
	   ["[], ''b''", delkey([], "b"), [] ]
	 , ["h1, ''b''", delkey(h1, "b"), ["a",1] ]
	 , ["h1, ''c''", delkey(h1, "c"), ["a",1,"b",2] ]
	 , ["h2, 1", delkey(h2, 1), [2,20,3,30]]
	 , ["h2, 2", delkey(h2, 2), [1,10,3,30]]
	 , ["h2, 3", delkey(h2, 3), [1,10,2,20]]
	 ], ops,  scope
	);
}
//delkey_test( ["mode",22] );
//doc(delkey);


//========================================================
dels=["dels", "a1,a2", "array", "Array",
 " Given arrays a1, a2 delete all items in a2 from a1.
;;
;; Ref: arrItcp
"];

function dels(a1,a2)=
let( a2=isarr(a2)?a2:[a2] )
(
	[for(x=a1) if (index(a2,x)==-1) x] 
);

module dels_test( ops )
{
	doctest
	(
		dels
		,[ 
			 [ "[2,4,4,8], 4", dels( [2,4,4,8], 4), [2,8] ]
			,[ "[2,4,6,8], [4,6]", dels( [2,4,6,8], [4,6]), [2,8] ]
		 ]
		,ops
	);
}


//========================================================
det=["det","pq","array","Array,Math,Geometry",
" Given a 2-pointer (pq), return the determinant. "
];
module det_test(ops=["mode",13])
{
	doctest( det, 
	[
	]
	,ops );
}


function det(pq)=
(
	dot( pq[0], pq[0] ) * dot( pq[1],pq[1]) - pow( dot(pq[0],pq[1]),2)
//( N1 . N1 ) ( N2 . N2 ) - ( N1 . N2 )2
);


//========================================================
dist=["dist","pq","number","Math, Geometry",
"
 Given a 2-pointer (pq), return the distance between p,q.
"];
function dist(pq) = norm(pq[1]-pq[0]);  // Get distance between a 2-pointer
module dist_test( ops )
{
	pq = randPts(2);
	p = pq[0]; q=pq[1];
	d = sqrt( pow(p.x-q.x,2)+pow(p.y-q.y,2)+pow(p.z-q.z,2) );
	doctest( dist, 
	[
		["pq", dist(pq), d]
	]	,ops, ["pq",pq]
	);
}

//========================================================
distPtPl=["distPtPl", "pt,pqr", "number", "Math, Geometry, 3D" ,
 " Given a pt and a 3-pointer (pqr) , return the signed distance between
;; the plane formed by pqr and the point.
"];
	
/*
	Pt = [x0, y0, z0 ]
	Pl = Ax + By + Cz + D = 0;

 	      Ax0 + By0 + Cz0 + D
	d= --------------------------
		sqrt( A^2 + B^2 + C^2 ) = norm([A,B,C])

	pc = planecoefs( pqr ) = [A,B,C,D]

	Ax0 + By0 + Cz0 + D=  [pc[0],pc[1],pc[2]] * Pt  + pc[3] = 

*/
function distPtPl(pt,pqr)=
(
	( planecoefs(pqr, has_k=false)* pt  + planecoefs(pqr)[3]) 
	/ norm(	planecoefs(pqr, has_k=false) )
);

module distPtPl_test( ops )
{
	pt= [1,2,0];
	pqr = [ [-1,0,2], [1,0,-3], [2,0,4]];
	//echo( "planecoefs({_}, false) =", planecoefs(pqr));
    //echo( pt * planecoefs(pqr, has_k=false) );
	
	doctest
	(
		distPtPl
		,[
			 ["pt,pqr", distPtPl(pt,pqr), 2 ]
		], ops, [ "pt", pt
		  , "pqr", pqr ]	
	);
}

module distPtPl_demo()
{
	pt = randPts(1);
	pqr= randPts(3);
}
//distPtPl_test(["mode",22]);
//doc(distPtPl);




//========================================================
distx = ["distx", "pq", "number", "Math, Geometry",
" Given a 2-pointer pq, return difference of x between pq. i.e., 
;;
;; distx(pq) = pq[1][0]-pq[0][0]
"];

function distx(pq)= pq[1][0]-pq[0][0];

module distx_test( ops )
{
	pq = [[0,3,1],[4,5,-1]];
	pq2= reverse(pq);
	
	scope=["pq", pq, "pq2",pq2];

	doctest( distx
			,[ 
				[ "pq", distx(pq), 4 ]			   		
				,[ "pq2", distx(pq2), -4 ]			   		
			 ],ops, scope
		   );
}
//distx_test(["mode",22]);
//doc(distx);

//========================================================
disty= ["disty", "pq", "number", "Math, Geometry",
" Given a 2-pointer pq, return difference of y between pq. i.e.,
;;
;; disty(pq) = pq[1][1]-pq[0][1]
"];

function disty(pq)= pq[1][1]-pq[0][1];

module disty_test( ops )
{
	pq = [[0,3,1],[4,5,-1]];
	pq2= reverse(pq);
	
	scope=["pq", pq, "pq2",pq2];

	doctest( disty
			,[ 
				[ "pq", disty(pq), 2 ]			   		
				,[ "pq2", disty(pq2), -2 ]			   		
			 ],ops,  scope
		   );
}
//disty_test(["mode",22]);
//doc(disty);

//========================================================
distz= ["distz", "pq", "number", "Math, Geometry",
" Given a 2-pointer pq, return difference of z between pq. i.e.,
;;
;; distz(pq) = pq[1][2]-pq[0][2]
"];

function distz(pq)= pq[1][2]-pq[0][2];

module distz_test( ops )
{
	pq = [[0,3,1],[4,5,-1]];
	pq2= reverse(pq);
	
	scope=["pq", pq, "pq2",pq2];

	doctest( distz
			,[ 
				[ "pq", distz(pq), -2 ]			   		
				,[ "pq2", distz(pq2), 2 ]			   		
			 ],ops,  scope
		   );
}
//distz_test(["mode",22]);
//doc(distz);


//========================================================
echoblock=[ "echoblock", "arr", "n/a", "Doctest, Console" , 
" Given an array of strings, echo them as a block. Arguments:
;;
;;   indent: prefix each line
;;   v : vertical left edge, default ''| ''
;;   h : horizontal line on top and bottom, default ''-''
;;   lt: left-top symbol, default ''.''
;;   lb: left-bottom symbol, default \"'\"
"];
module echoblock(arr, indent="  ", v="| ",t="－",b="－", lt=".", lb="'")
{
	lines = arrblock(arr, indent=indent, v=v,t=t,b=b, lt=lt, lb=lb);

	//for ( line= lines )echo(line);
	//echo(  str("<span style='font-family:Droid Sans Mono'><br/>", join(lines, "<br/>" ), "<br/></span>"));
	_Div( join(lines,"<br/>")
		, "font-family:Droid Sans Mono"
		);  
}
 
module echoblock_test( ops ){ doctest( echoblock, ops=ops); }

module echoblock_demo()
{
	echom( "echoblock_demo" );
	echo( "echoblock([\"line1\",\"line2\",\"line3\"]) shows:");
	echoblock(["line1","line2","line3"]);
}
//echoblock_demo();
//doc(echoblock);
//echo( join( ["<br/>","line1","line2","line3"], "<br/>"));


//========================================================
echo_=["echo_", "s,arr=undef", "n/a", "Console",
" Given a string containing one or more ''{_}'' as blanks to fill,
;; and an array (arr) as the data to fill, echo a new s with all
;; blanks replaced by arr items one by one. If the arr is given as 
;; a string, it is the same as echo( str(s,arr) ).
;;
;; Example usage:
;;
;; echo_( ''{_}.scad, version={_}'', [''scadex'', ''20140531-1''] );
"];


module echo__test( ops ){ doctest( echo_, ops=ops); }

module echo_(s, arr=undef)
{
	if(arr==undef){ echo(s); }
	else if (isarr(arr)){
		echo( _s(s, arr));
	}else { echo( str(s, arr) ); }
}

module echo__demo()
{
	echo_("echox");
	echo_("{_},{_}", ["name","scadex"]);
	echo_("{_}", ["scadex"]);
}
//echo__demo();
//doc(echo_);



//========================================================
echoh=["echoh","s,h,showkey","n/a", "Hash, Console",
" Given a string template (s) containing ''{key}'' where key is a key of
;; a hash, and a hash (h), return a new string in which all keys in {??}
;; are replaced by their corresponding values in h. If showkey is true, 
;; replaced by key=val.
"]; 

module echoh(s, h, showkey) 
{  echo( _h(s, h, showkey=showkey));
}

module echoh_test(ops){ doctest( echoh, ops=ops);}

module echoh_demo()
{
	echoh("{name},{ver}", ["name","scadex","ver",3.3]);
	echoh("{name},{ver}", ["name","scadex","ver",3.3], true);
}
//echoh_demo();



//========================================================

module echom(s)
{
	echo_( "--------<  {_}  >--------", [s] );
}

module echom_demo()
{
	echom("echom");
}

//echom_demo();
//accumulate_test(["mode",2]);


//========================================================
endwith=[ "endwith", "o,x", "T/F",  "String, Array, Inspect",
"
 Given a str|arr (o) and x, return true if o ends with x.
;;
;; New 2014.8.11:
;;
;; x can be an array, return true if o ends with any item in x.
"];

function endwith(o,x)= 
let( x=isarr(x)?x:[x] )
( 
	isarr( o )
	? sum( [for(xx=x) last(o)==xx?1:0 ] )>0
	: isstr( o )
	  ? sum( [for(xx=x) slice(o,-len(xx))==xx?1:0 ] )>0
	  : false
);

module endwith_test( ops )
{
	doctest
	(
	 endwith
	,[ 
	    [ "''abcdef'', ''def''", endwith("abcdef", "def"), true ]
	  , [ "''abcdef'', ''def''", endwith("abcdef", "abc"), false ]
	  , [ "[''ab'',''cd'',''ef''], ''ef''",  endwith(["ab","cd","ef"], "ef"),true ]
	  , [ "[''ab'',''cd'',''ef''], ''ab''",  endwith(["ab","cd","ef"], "ab"),false ]
	  , [ "33, ''def''", endwith(33, "abc"), false ]
	  , "// New 2014.8.11"
  	, [ "''abcdef'', [''def'',''ghi'']", endwith("abcdef", ["def","ghi"]), true ] 
  	, [ "''abcghi'', [''def'',''ghi'']", endwith("abcghi", ["def","ghi"]), true ] 
  	, [ "''abcddd'', [''def'',''ghi'']", endwith("aaaddd", ["def","ghi"]), false ] 
 	, [ "[''ab'',''cd'',''ef''], [''gh'',''ef'']", endwith(["ab","cd","ef"], ["gh","ef"]),true]	
	  ], ops
	);
}
//endwith_test(["mode",22]);
//doc(endwith);




//========================================================
expandPts=["expandPts", "pts,dist=0.5,cycle=true", "points", "Points, Geometry",
" Given an array of points (pts), return an array of points that *expand* every 
;; point outward as indicated by Q ==> Q2 in the graph below:
;;
;; d = dist
;;
;; ---------------------Q2
;;                     /
;;                    /
;; P----------Q_     /
;;           /  '-_ /
;;          /   d  :
;;         /      /
;;        /      /
;;       R      /
"
];


/*
 	d = dist
 
   ---------------------Q2
                       /        
                      /        
   P----------Q_     /
             /  '-_ / 
            /   d  : 
           /      / 
          /      / 
         R      /     



*/
module expandPts_test( ops=["mode",13] ){ doctest( expandPts, ops=ops ); }

function expandPts(pts,dist=0.5, cycle=true, _i_=0)=
 let( p3s = subarr( pts,cycle=cycle )
	)
//	, p3s = [for(ijk=i3s) pts[
(	// 
	[ for(i=range(pts))
		onlinePt( [pts[i], angleBisectPt( p3s[i] )]
				, len=-dist/sin(angle(p3s[i])/2)
			    )
	]
				  
//[for(ijk=tris)  
//	[ for(p=range(-1,len(pts)+1, obj=pts, cycle=true)) i ]

//	[ for(i=range(-1,len(pts)+1)) fidx(pts, i, cycle=true) ]

//	_i_<len(pts)?
//	concat( 
//		[ onlinePt
//           ( 
//			[ pts[_i_]
//			, angleBisectPt
//			  ( 
//				neighbors(pts,_i_, cycle=true) 
//			  )
//			], len=-dist/
//					sin(angle(neighbors(pts,_i_, cycle=true))/2)
//		  )// onlinePt
//		]
//	, expandPts( pts, dist, cycle, _i_+1)
//	):[]
);

module expandPts_demo_1_open()
{
	echom("expandPts");
	pts = randOnPlanePts( randPts(3), 5);
	pts = randPts(6);
	larger= expandPts(pts, dist=0.5);
	smaller= expandPts(pts,dist=-0.5);
	Chain( pts, ["color","red", "r",0.01, "closed",false] );
	//Chain( larger, ["color","red","transp",0.3]);
	//Chain( smaller);
	MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [pts[i], larger[i]], ["r",0.01, "head",["style","cone"]] );
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1), larger[i] ]);

	}
	//Dim( [pts[0], larger[0]] );
}

module expandPts_demo_2()
{
	echom("expandPts");
	pts = randOnPlanePts( 5 );
	pts = randPts(6);
	larger = expandPts(pts, dist=0.3);
	smaller= expandPts(pts, dist=-0.3);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	//Chain( larger, ["color","red","transp",0.3]);
	//Chain( smaller);
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}
	//Dim( [pts[0], larger[0]] );
}

module expandPts_demo_3()
{
	echom("expandPts");
	pts = randOnPlanePts(5);
	//pts = randPts(4);
	larger = expandPts(pts, dist=0.1);
	smaller= expandPts(pts, dist=-0.1);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green","r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01, "closed",true] );
	MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}
	//Dim( [pts[0], larger[0]] );
}

module expandPts_demo_4_arc()
{
	pts = arcPts(r=3, a=120, count=24);
	//echo("pts:", pts);
	larger = expandPts(pts, dist=0.1);
	smaller= expandPts(pts, dist=-0.1);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green","r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01, "closed",true] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}

}
module expandPts_demo()
{
	echom("expandPts_demo");
	//p6 = randPts(6);
	//echo( "p6:",p6);
	//echo( "xp6:",expandPts( p6 ));

	//expandPts_demo_1_open();
	//expandPts_demo_2();
	//expandPts_demo_3();
	expandPts_demo_4_arc();
}	  
//expandPts_demo();	

//angle_test( ["mode",13] );
//arcPts_test( ["mode",13]);

////========================================================
//expand=[[]];
//
//function expand(pts,ratio=1.2, _i_=0)=
//(
//	len(pts)==2?
//	[ onlinePt( pts, ratio=ratio/2 )
//	, onlinePt( pts, ratio=-ratio/2)
//	]
//	: len(pts)==3?
//	  [ onlinePt( [pts[0], incenterPt(pts)], ratio = ratio]
//	  , onlinePt( [pts[1], incenterPt(pts)], ratio = ratio]
//	  , onlinePt( [pts[2], incenterPt(pts)], ratio = ratio]
//	  ]
//	  : _i_<len(pts)?
//		concat(  [ get(pts, _i_-1 )
//				 ,get(pts, _i_   )
//				 ,get(pts, _i_==len(pts)-1?0:_i_+1 )] 





//========================================================
linePts=["linePts","pq, len, ratio", "pq", "Array, Points, Geometry, Line",
 " Extend/Shrink line. Given a 2-pointer (pq) and len or ratio , return 
;; a new 2-pointer for a line with extended/shortened lengths. 
;;
;; Both len and ratio can be positive|negative. Positive= extend, negative=shrink.
;; 
;; They can be either 2-item array (like [1,0]) or a number. If a number, it's the
;; same as [number,number].
;;
;; If array [a,b], a,b for p, q side, respectively. For example, 
;;
;;   linePts( pq, len=-1): both sides shrink by 1
;;   linePts( pq, len=[1,1]): both sides extends by 1

;; Compared to onlinePt, this function:
;; (1) is able to extend both ends. 
;; (2) return 2-pointers.
;;
;; linePts(pq, len=1) = [ onlinePt( pq, len=-1)
;;                      , onlinePt( p10(pq), len=-1 ]
"];


function linePts(pq,len=undef, ratio=[1,1])=
 let( len=isnum(len)?[len,len]:len
	, ratio=isnum(ratio)?[ratio,ratio]:ratio
	)
( 
   [ onlinePt( pq,  len= -len[0], ratio=-ratio[0])
   , onlinePt( p10(pq), len= -len[1], ratio=-ratio[1])
   ]
);		

module linePts_test( ops=["mode",13] )
{
	doctest( linePts,ops=ops );
}

module linePts_demo_1_len()
{_mdoc("linePts_demo_1_len",
 " yellow: linePts(pq, len=0.5)
;; red   : linePts(pq, len=[1,0])
;; blue  : linePts(pq, len=[1.5,1.5])
");

	pq=randPts(2);
	MarkPts( pq, ["labels","PQ", "r",0.2] );
	Line0(pq);
    
	xpq = linePts(pq, len=0.5);
	Line0(xpq, ["r",0.25,"transp",0.5,"color","red"] );

	x2pq = linePts(pq, len=[1,0]);
	Line0(x2pq, ["r",0.4,"color","green", "transp",0.3] );

	x3pq = linePts(pq, len=[1.5,3]);
	Line0(x3pq, ["r",0.55,"color","blue", "transp",0.2] );
    
    P= pq[0]; Q=pq[1]; R= randPt();
    pqr = concat( pq, [R] );
    LabelPt( anglePt( [P, midPt([P,Q]), R] , a=90, len=1.5), "linePts(pq)" );
    LabelPt( anglePt( pqr, a=110, len=1.5), "linePts(pq,len=0.5)"
            , ["color","red"] );
    LabelPt( anglePt( [Q,P,R], a=120, len=2), "linePts(pq,len=[1,0])"
            , ["color","green"] );
    LabelPt( anglePt( [x3pq[0], x3pq[1],R], a=120, len=2), "linePts(pq,len=[1.5,3])"
            , ["color","blue"] );    
    
}
module linePts_demo_2_ratio()
{
	pq=randPts(2);
	MarkPts( pq, ["labels","PQ"] );
	Line0(pq);
	xpq = linePts(pq, ratio=0.2);
	Line0(xpq, ["r",0.25,"transp",0.5] );

	x2pq = linePts(pq, ratio=[0.2,0]);
	Line0(x2pq, ["r",0.4,"color","red", "transp",0.3] );

	x3pq = linePts(pq, ratio=[0.4, 0.4]);
	Line0(x3pq, ["r",0.55,"color","blue", "transp",0.2] );
}
module linePts_demo_3_shrink()
{
	_pq=randPts(2);
	P= _pq[0];
    Q= onlinePt(_pq, len=3);
    pq = [P,Q];
    
	x2pq = linePts(pq, ratio=[-0.2,0]);
	Line0(x2pq, ["r",0.4,"color","red", "transp",0.3] );

	x3pq = linePts(pq, ratio=[-0.4, -0.4]);
	Line0(x3pq, ["r",0.55,"color","blue", "transp",0.2] );
    
    MarkPts( pq, ["labels","PQ"] );
	xpq = linePts(pq, ratio=-0.2);
	Line0(xpq, ["r",0.25,"transp",0.5] );
    Line0(pq, ["transp",0.3]);
	
}

//linePts_demo_1_len();
//linePts_demo_2_ratio();
//linePts_demo_3_shrink();

//========================================================
fac=[ "fac", "n", "int",  "Math",
"
 Given n, return the factorial of n (= 1*2*...*n) 
"];

function fac(n=5)= n*(n>2?fac(n-1):1);
module fac_test( ops )
{
	doctest
	( fac
	,  [ 
		[ "5",  fac(5), 120 ]
	  , [ "6", fac(6), 720 ]
	  ], ops
    );
}

//fac_test( ["mode",22] );
//doc(fac);



//========================================
faces=["faces","shape,sides,count", "array", "Array, Geometry",
 " Given a shape (''rod'', ''cubesides'', ''tube'', ''chain''), an int
;; (sides) and an optional int (count), return an array for faces of
;; a polyhedron for that shape.
"];

module faces_test( ops=["mode", 13] )
{
    doctest( faces,
	ops=ops
	);
}


function faces(shape="rod", sides=6, count=3)= // count is used for shape that has
 let( s = sides                                // segments unknown until run-time
	, s2= 2*sides                              // For example, chain
	, s3= 3*sides
)(
	// cube, cubesides
	//       _6-_
	//    _-' |  '-_
	// 5 '_   |     '-7
	//   | '-_|   _-'| 
	//   |    |-4'   |
	//   |   _-_|    |
	//   |_-' 2 |'-_ | 
	//  1-_     |   '-3
	//     '-_  | _-'
	//        '-.'
	//          0
	shape=="cubesides"
	? [ for(i=range(s))
		  [ i
			, i==s-1?0:i+1
			, (i==s-1?0:i+1)+s
			, i+s
		  ]
	  ]
 	: shape=="rod"
	? concat( [ reverse(range(s))], [range(s,s2) ],
			  faces("cubesides", s) ) 
	// tube
	//             _6-_
	//          _-' |  '-_
	//       _-'   14-_   '-_
	//    _-'    _-'| _-15   '-_
	//   5_  13-'  _-'  |      .7
	//   | '-_ |'12_2-_ |   _-' | 
	//   |    '-_'|    '|_-'    |
	//   |   _-| '-_10_-|'-_    |
	//   |_-'  |_-| 4' _-11 '-_ | 
	//   1_   9-  | |-'       _-3
	//     '-_  '-8'|      _-'    
	//        '-_   |   _-'
	//           '-_|_-'
    //              0
	: shape=="tube"
	? concat( faces( "cubesides", s )  // outside sides
			, [ for(f=faces( "cubesides", s ))    // inner sides
					reverse( [ for(i=f) i+s2] )
			  ]
			, [ for(i=range(s))        // bottom faces
				  [i, mod(i+s-1,s), mod(i+s-1,s)+s2, i+s2 ] 
			  ]
			, [ for(i=range(s,s2))    // top faces
				  [i, i==s2-1?s:i+1, i==s2-1?s3:i+s2+1, i+s2 ]
			  ]
			)	  
	
			//  bottom=	 [ [0,3,11, 8]
			//			 , [1,0, 8, 9]
			//			 , [2,1, 9,10]
			//			 , [3,2,10,11] ];
			//
			//	top	=	 [ [4,5,13,12]
			//			 , [5,6,14,13]
			//			 , [6,7,15,14]
			//			 , [7,4,12,15] ]

	//                 
	//       _6-_-------10------14
	//    _-' |  '-_    | '-_   | '-_
	//  2'_   |     '-5-----'9-------13
	//   | '-_|   _-'|  |    |  |    |
	//   |    |-1'   |  |    |  |    |
	//   |   _-_|----|-11_---|--15   |
	//   |_-' 7 |'-_ |    '-_|    '-_| 
	//  3-_     |   '-4------8-------12
	//     '-_  | _-'
	//        '-.'
	//          0
	//
	:shape=="chain"
	? concat( [ reverse(range(s))]            // starting face
			, [ range( count*s,(count+1)*s) ] // ending face
			, joinarr( [ for(c=range(count))
				 [ for(f=faces("cubesides", sides))
					[ for(i=f) i+c*sides]
				 ]
			  ])
			)  
	:[]
);

module faces_demo_1()
{
	pqr = randPts(3);
	S= cornerPt(pqr);
	pqrs = concat( pqr, [S]);
	
	vector = randPt();

	tuvw = [for(p=pqrs) p+vector ];

	pts = concat( pqrs, tuvw );

	MarkPts( pts, ["labels",true] );
    
    LabelPt( pts[0], "  faces=faces(\"rod\",sides=4)");
	polyhedron( points = pts
			 , faces = faces("rod", sides=4)
			 );
}
//faces_demo_1();

module faces_demo_2_cube()
{
	pqr = randRightAnglePts();
	S= cornerPt(pqr);
	pqrs = concat( pqr, [S]);
	
	vector = normal(pqr);

	tuvw = [for(p=pqrs) p+vector ];

	pts = concat( pqrs, tuvw );

	MarkPts( pts, ["labels",true] );

	polyhedron( points = pts
			 , faces = faces("rod", sides=4)
			 );
}
//faces_demo_2_cube();


module faces_demo_3_cylinder()
{
	pqr = randPts(3);
    MarkPts( pqr );
    
	Q = Q(pqr);
	N = normalPt( pqr );
	Chain(pqr);
	sides = 8;

	pts= joinarr(rodPts( pqr, ["r",2,"sides",sides] )); //arcPts( pqr, r=2, a=360, count=sides, plane="yz");
	//q_pts= [for(p=p_pts) p+(N-Q)];	

	//pts = concat( p_pts,q_pts );

	MarkPts( pts, ["labels",true] );

	faces = faces("rod", sides=sides);

//	echo("faces_demo_3_cylinder.N=", N);
//	echo("faces_demo_3_cylinder.p_pts=", p_pts);
//	echo("faces_demo_3_cylinder.q_pts=", q_pts);
	echo("faces_demo_3_cylinder.faces=", faces);
	color(false, 0.6)
	polyhedron( points = pts
			 , faces = faces
			 );
}
//faces_demo_3_cylinder();


module faces_demo_4_tube()
{
	pqr = randPts(3);
	Chain(pqr);
	MarkPts( pqr, ["labels","PQR"] );

	Q = Q(pqr);
	N = normalPt( pqr );
	G = onlinePt( [Q,N], len=3 );
	sides = 48;

	//p_pts= arcPts( pqr, r=2, a=360, count=sides, pl="yz");
	//q_pts= [for(p=p_pts) p+(G-Q)];	
	pts1 = joinarr(rodPts( pqr, ["r",2,"sides",sides] )); //concat( p_pts,q_pts );

	//p_pts2= expandPts( p_pts, dist=-1);
	//q_pts2= expandPts( q_pts, dist=-1);
	pts2 = joinarr(rodPts( pqr, ["r",1, "sides",sides])); //concat( p_pts2,q_pts2 );

	pts = concat(pts1,pts2);

	//MarkPts( pts, ["labels",true] );

	faces = faces("tube", sides=sides);

//	echo("faces_demo_4_tube.N=", N);
//	echo("faces_demo_4_tube.pts1=", pts1);
//	echo("faces_demo_4_tube.pts2=", pts2);
//	echo("faces_demo_4_tube.faces=", faces);
	color(false, 0.8)
	polyhedron( points = pts
			 , faces = faces
			 );
}
//faces_demo_4_tube();

module faces_demo_5_chain()
{
	sides = 4;
	count = 4;
	
	linepts= randPts(count+1);

	//Chain( linepts );
	//MarkPts( linepts, ["labels",true] );

	_allrps = [for(i=range(count-1))
				let( pqr = [ linepts[i], linepts[i+1], linepts[i+2] ]
				   , ang = angle(pqr)/2
				)
				rodPts( pqr
					  , ["r",0.5, "sides", sides
						,"q_angle", i>=count-2?90:ang
						]
					  )
			 ];

//	allrps = concat( _allrps[0][0], [ for(rp=_allrps) rp[1] ]);
	allrps = concat( _allrps[0][0], joinarr( [ for(rp=_allrps) rp[1] ] ));
	LabelPts( allrps, labels="");
	//Chain( allrps );					

//	p00  = onlinePt( p01(linepts), len=-1 );
//	plast= onlinePt( [get(linepts,-1), get(linepts,-2)] , len=-1 );
//
//	linepts2= concat( [p00], linepts, [plast] );
//
//	Chain( linepts2, ["transp",0.5, "r",0.3, "color","red"] );
//
//	//rp = rodPts( linepts,["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	//Chain( rp );
//
//	rp1 = rodPts( slice(linepts, 0,3),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp1 );
//
//	rp2 = rodPts( slice(linepts, 1,4),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp2 );
		
//	pts= joinarr( 
//			[for(c=range(1,count+1))
//				let( pqr=[ linepts2[c-1]
//						, linepts2[c]
//						, linepts2[c+1]
//						]
//				   , ang = angle(pqr)/2
//					)
//				rodPts(pqr,["sides",sides,"q_angle", ang] )
//			]
//		);

//[ "r", 0.05
//	  , "sides",     6
//	  , "label", "" 
//	  , "p_pts",undef  // pts on the P end
//	  , "q_pts",undef  // pts on the q end
//	  , "p_angle",90   // cutting angle on p end
//	  , "q_angle",90   // cutting angle on q end
//	  , "rotate",0  // rotate about the PQ line, away from xz plane
//	  , "cutAngleAfterRotate",true // to determine if cutting first, or
//						  // rotate first. 
//	  ]
	faces = faces("chain", sides=sides, count=count);

//	echo("faces_demo_5_chain.N=", N);
	echo("<br/>faces_demo_5_chain._allrps=", _allrps);
	echo("<br/>faces_demo_5_chain.allrps=", allrps);
	//echo("<br/>faces_demo_5_chain.pts=", pts);
	echo("<br/>faces_demo_5_chain.faces=", faces);
	color(false, 0.8)
	polyhedron( points = allrps
			 , faces = faces
			 );
}


//========================================================
fidx=["fidx", "o,i,cycle=false,fitlen=false", "int|undef", "Index, Range, Array, String, Inspect",
" Fit index. Check if index i is in range of o (a str|arr). i could be negative.
;; If yes, convert it to *legal* index value (means, positive int). For o of len L,
;; this would mean -L &lt;= i &lt; L. That is, i should be in range *(-L,L]*. 
;; 
;; New 2014.8.17: o could be an integer for range 0 ~ o
;;
;; If i out of this range, return undef (when cycle=false) or:
;; -- set fitlen=true to return the last index
;; -- set cycle=true to loop from the other end of o. So if cycle=true, a given i 
;;     will never go undef, no matter how large or small it is.  
"];

function fidx(o,i,cycle=false, fitlen=false)=
let( L=isint(o)?o:len(o), j = cycle? mod(i,L):i )
(
	/*
	len: 1 2 3 4 5
		 0 1 2 3 4   i>=0
   		-5-4-3-2-1   i <0    

  				   0 1 2 3 4   : legal index
  		-5-4-3-2-1 0 1 2 3 4   : proper index

	When out of range: 

		if cycle=false: return false
		if cycle=true: 
			cycle through array as many as needed. So if cycle is set
					to true, it never returns false.  

	*/

	-L<=j&&j<L 
	? ( (j<0? L:0)+j )
	:fitlen==true?(L-1):undef
);

module fidx_test( ops )
{

	doctest
	( 
	 fidx, [
		"// string "
	  , [ "''789'',2", fidx("789",2), 2 ]
	  ,	[ "''789'',3", fidx("789",3), undef, ["rem","out of range"] ]
	  ,	[ "''789'',-2", fidx("789",-2), 1 ]
	  ,	[ "''789'',-3", fidx("789",-3), 0 ]
	  ,	[ "''789'',-4", fidx("789",-4), undef, ["rem","out of range"] ]
	  ,	[ "'',0", fidx("",0), undef ]
	  ,	[ "''789'',''a''", fidx("789","a"), undef ]
	  ,	[ "''789'',true", fidx("789",true), undef ]
	  ,	[ "''789'',[3]", fidx("789",[3]), undef ]
	  ,	[ "''789'',undef", fidx("789",undef), undef ]
	  , "// array"
	  ,	[ "[7,8,9],2", fidx([7,8,9],2), 2 ]
	  ,	[ "[7,8,9],3", fidx([7,8,9],3), undef, ["rem","out of range"] ]
	  ,	[ "[7,8,9],-2", fidx([7,8,9],-2), 1 ]
	  ,	[ "[7,8,9],-3", fidx([7,8,9],-3), 0 ]
	  ,	[ "[7,8,9],-4", fidx([7,8,9],-4), undef, ["rem","out of range"] ]
	  ,	[ "[],0", fidx([],0), undef ]
	  ,	[ "[7,8,9],''a''", fidx([7,8,9],"a"), undef ]
	  ,	[ "[7,8,9],true", fidx([7,8,9],true), undef ]
	  ,	[ "[7,8,9],[3]", fidx([7,8,9],[3]), undef ]
	  ,	[ "[7,8,9],undef", fidx([7,8,9],undef), undef ]

	  , "// If out of range, return undef, or:"
	  , "//  -- Set cycle to go around the other end."
	  , "//     (it never goes out of bound) "
	  , "//  -- Set fitlen to return the last index."
	  , "//"
	  ,	[ "[7,8,9],3", fidx([7,8,9],3), undef, ["rem","out of range"] ]
	  ,	[ "[7,8,9],3,fitlen=true", fidx([7,8,9],3,fitlen=true), 2, ["rem","fitlen"] ]
	  ,,	[ "[7,8,9],3, cycle=true", fidx([7,8,9], 3 , true), 0 ]
	  ,	[ "[7,8,9],4, cycle=true", fidx([7,8,9], 4 , true), 1 ]
	  ,	[ "[7,8,9],7, cycle=true", fidx([7,8,9], 7 , true), 1 ]
	  ,	[ "[7,8,9],-4", fidx([7,8,9],-4), undef, ["rem","out of range"] ]
	  ,	[ "[7,8,9],-4, cycle=true", fidx([7,8,9],-4,true), 2  ]
	  ,	[ "[7,8,9],-16, cycle=true", fidx([7,8,9],-16,true), 2 ]
	  ,	[ "[7,8,9],-5, cycle=true", fidx([7,8,9],-5,true), 1 ]
	  
	  ,"//"
	  ,"// New 2014.8.17: o can be an integer for range 0~o"
	  ,"//"
	  ,	[ "3,5", fidx(3,5), undef, ["rem","out of range"] ]
	  ,	[ "3,5,fitlen=true", fidx(3,5,fitlen=true), 2, ["rem","get the last idx"] ]
	  ,	[ "3,4,cycle=true", fidx(3,4,cycle=true), 1, ["rem","cycle from begin"] ]
	  , [ "3,-2", fidx(3,-2), 1 ]
	  ,	[ "3,-3", fidx(3,-3), 0 ]
	  ,	[ "3,-4", fidx(3,-4), undef, ["rem","out of range"] ]
	  ,	[ "3,-4,cycle=true", fidx(3,-4,cycle=true), 2, ["rem","cycle=true"] ]
	  , "//"	

	  ], ops
	);
}




//
////========================================================
//fitrange=[ "fitrange", "o,i", "int|false", "Index, Array, String, Inspect",
//" Check if index i is in range of o (a str|arr). i could be negative.
//;; If yes, convert it to proper index value (means, if <0, convert to
//;; positive). Else return false.
//;; 
//;; Note: get(o,i) == o[ fitrange(o,i) ], i could be <0 in both cases.
//"];
//
//function fitrange(o,i)=
//(
//	/*
//	len: 1 2 3 4 5
//		 0 1 2 3 4   i>=0
//   		-5-4-3-2-1   i <0    
//	*/
//	
//	(-len(o)<=i) && ( i<len(o)) 
//	? ( (i<0? len(o):0)+i )
//	: false
//);
//
//module fitrange_test( ops )
//{
//
//	doctest
//	( 
//	 fitrange, [
//		"## string ##"
//	  , [ "''789'',2", fitrange("789",2), 2 ]
//	  ,	[ "''789'',3", fitrange("789",3), false ]
//	  ,	[ "''789'',-2", fitrange("789",-2), 1 ]
//	  ,	[ "''789'',-3", fitrange("789",-3), 0 ]
//	  ,	[ "''789'',-4", fitrange("789",-4), false ]
//	  ,	[ "'',0", fitrange("",0), false ]
//	  ,	[ "''789'',''a''", fitrange("789","a"), false ]
//	  ,	[ "''789'',true", fitrange("789",true), false ]
//	  ,	[ "''789'',[3]", fitrange("789",[3]), false ]
//	  ,	[ "''789'',undef", fitrange("789",undef), false ]
//	  , "## array ##"
//	  ,	[ "[7,8,9],2", fitrange([7,8,9],2), 2 ]
//	  ,	[ "[7,8,9],3", fitrange([7,8,9],3), false ]
//	  ,	[ "[7,8,9],-2", fitrange([7,8,9],-2), 1 ]
//	  ,	[ "[7,8,9],-3", fitrange([7,8,9],-3), 0 ]
//	  ,	[ "[7,8,9],-4", fitrange([7,8,9],-4), false ]
//	  ,	[ "[],0", fitrange([],0), false ]
//	  ,	[ "[7,8,9],''a''", fitrange([7,8,9],"a"), false ]
//	  ,	[ "[7,8,9],true", fitrange([7,8,9],true), false ]
//	  ,	[ "[7,8,9],[3]", fitrange([7,8,9],[3]), false ]
//	  ,	[ "[7,8,9],undef", fitrange([7,8,9],undef), false ]
//	  ], ops
//	);
//}


//========================================================
//
// UNDER CONSTRUCT
//
_fmth=[["_fmth"
       ,"h, pairbreak='' , '', keywrap=''<b>,</b>'', valwrap=''<u>,</u>''" 
	   ,"string","String, Doc"
       ]];

function _fmth(h, pairbreak=" , "
			  , keywrap="<u><b>,</b></u>"
			  , valwrap="<i>,</i>"
			  , _i_=0)=
(
	_i_>len(h)-1? []  
	: str( _i_==0?"[":""
		 , replace(keywrap,",",_fmt(h[_i_]))

		 , _i_>len(h)-2?""
                        :str( ":"
                            , replace(valwrap,",",_fmt(h[_i_+1]))
                            ) //_arg(str(h[_i_+1]))) )		
		 , _i_>len(h)-3?"":pairbreak 
		 , _i_>len(h)-3?"]":_fmth( h, pairbreak, keywrap, valwrap, _i_=_i_+2)

		 )    
);

module _fmth_test( ops )
{
	scope=[ "h", ["num",23,"str","test", "arr", [2,"a"] ]
		  , "h2",["pqr", [2,3,4]]
		  , "h3",["pqr", [[0,1,2],[2,3,4]] ]
		  ];
	h= hash(scope, "h");
	h2= hash(scope, "h2");
	h3= hash(scope, "h3");

	r1 = _fmth(h);
	//r2 = _fmth(h2);
	//r3 = _fmth(h3);

	doctest(
	 _fmth
	,[
		//["h", _fmth(h), ""]
	  //, 
	//	_fmth(h)
	 //"No test is done."
	//, "2nd line string"
	 str("_fmth(h): ", _fmth(h))
	,str("_fmth(h2): ", _fmth(h2))
	,str("_fmth(h3): ", _fmth(h3))

//	  ["[''a'',23]", _fmth(["a",23]), "[\"a\",23]" ]
//	, ["h", _fmth(h ),""]
//	, ["h2", _fmth( h2),""]
//	, ["h3", _fmth( h3),""]

	],  ops, ,scope 
	);
}
//_fmth_test();
//test_example_test();
//accumulate_test();

//echo( _fmth(["num",23,"str","test", "arr", [2,"a"] ]) );

//========================================================
_fmt=["_fmt", "x,[s,n,a,ud,fixedspace]", "string", "String" 
," Given an variable (x), and formattings for string (s), number (n),
;; array (a) or undef (ud), return a formatted string of x. If fixedspace
;; is set to true, replace spaces with html space. The formatting is
;; entered in ''(prefix),(suffix)'' pattern. For example, a ''{,}'' converts
;; x to ''{x}''.
"
];


function _fmt(x, s=_span("&quot;,&quot;", "color:purple") 
			, n=_span(",", "color:brown") //magenta")//blueviolet")//brown")
			, a=_span(",", "color:navy")//midnightblue")//cadetblue")//mediumblue")//steelblue") //#993300") //#cc0060")  
			, ud=_span(",", "color:brown") //orange") 
			//, level=2
			, fixedspace= true
		   )=
(
	x==undef? replace(ud, ",", x) 
	:isarr(x)? replace(a, ",", replace(str(x),"\"","&quot;")) 
	: isstr(x)? replace(s,",",fixedspace?replace(str(x)," ", "&nbsp;"):str(x)) 
		:isnum(x)||x==true||x==false?replace(n,",",str(x))
				:x
);

module _fmt_test( ops )
{

	doctest(
	 _fmt
	,[
	   str("_fmt(3.45): ", _fmt(3.45))
     ,  str("_fmt([''a'',[3,''b''], 4]): ", _fmt(["a",[3,"b"], 4] ))
     ,  str("_fmt(''a string with     5 spaces''): ", _fmt("a string with     5 spaces"))			, str("_fmt(false): ", _fmt(false))

	],  ops
	);
}

module _fmt_demo()
{
	echom("_fmt");
	echo( str("# ", _fmt(345), " and array ", _fmt([2,"a",[3,"b"], 4]), " are ", _fmt("formatted"), " by ", _fmt("fmt with     5 spaces") 
			) ); 
	echo( _fmt(true), _fmt(false), _fmt(undef));
 
}
//_fmt_demo();
//doc(_fmt);
//accumulate_test();




//========================================================
ftin2in=["ftin2in","ftin","number","Math, Number, Unit",
" Given a ftin ( could be a string like 3'6'', or an array like [3,6], 
;; or a number 3.5, all for 3 feet 6 in ), convert to inches.
"];

function ftin2in(ftin)=
(
	isnum(ftin)
	? ftin*12
	: isarr(ftin)
	  ? ftin[0]*12+ftin[1]
	  : endwith(ftin,"'")
		? num( replace(ftin,"'",""))*12

	    : index(ftin,"'")>0
		   ? num( split(ftin,"'")[0] )*12 
			+ num( replace(split(ftin,"'")[1],"\"",""))
		   : num( replace(ftin,"\"",""))
);

module ftin2in_test( ops=["mode",13] )
{
	doctest( ftin2in,
	[
	  [ "3.5", ftin2in(3.5), 42 ]
	 ,[ "[3,6]", ftin2in([3,6]), 42 ]
	, [ "3'6''", ftin2in("3'6\""), 42 ]
	, [ "8'", ftin2in("8'"), 96 ]
	, [ "6''", ftin2in("6\""), 6 ]
	], ops
	);
}

//========================================================
get=["get", "o,i,cycle=false", "item", "Index, Inspect, Array, String, Range" ,
 " Given a str or arr (o), an index (i), get the i-th item. 
;; i could be negative.
;;
;; New argument *cycle*: cycling from the other end if out of range. This is
;; useful when looping through the boundary points of a shape.
"];

function get(o,i, cycle=false)= 
( 
	o[ fidx(o,i,cycle=cycle) ]	
);

module get_test( ops=["mode",13] )
{
	arr = [20,30,40,50];

	doctest
	( get
		,[ 
		  "## array ##"
		 , [ "arr, 1", get( arr, 1 ), 30 ]
    		 , [ "arr, -1", get(arr, -1), 50 ]
    		 , [ "arr, -2", get(arr, -2), 40 ]
    		 , [ "arr, true", get(arr, true), undef ]
    		 , [ "[], 1", get([], 1), undef ]
		 , [ "[[2,3],[4,5],[6,7]], -2", get([[2,3],[4,5],[6,7]], -2), [4,5] ]
    		 , "## string ##"
    		 , [ "''abcdef'', -2", get("abcdef", -2), "e" ]
    		 , [ "''aabcdef'', false", get("abcdef", false), undef ]
    		 , [ "'''', 1", get("", 1), undef ]
		 , "//New 2014.6.27: cycle"
		 , [ "arr, 4", get(arr,4), undef]
		 , [ "arr, 4, cycle=true", get(arr,4,cycle=true), 20]
		 , [ "arr, 10, cycle=true", get(arr,10,cycle=true), 40]
		 , [ "arr, -5", get(arr,-5), undef]
		 , [ "arr, -5, cycle=true", get(arr,-5,cycle=true), 50]

    		 ],ops,  ["arr", arr]
	);
}
//get_test(  );
//doc( get );


//========================================================
getcol=[ "getcol","mm,i=0", "array", "Array, Index",
 " Given a matrix and index i, return a 1-d array containing
;; item i of each matrix row (i.e., the column i of the multi
;; matrix. The index i could be negative for reversed indexing.
"];
				
function getcol(mm,i=0)=
(
	[ for(row=mm) get(row,i) ]
);

module getcol_test( ops )
{
	mm= [[1,2,3],[3,4,5]];
	doctest
	(
		getcol
	,	[
			[ "mm", getcol(mm), [1,3] ]
		,	[ "mm,2", getcol(mm,2), [3,5] ]
		,	[ "mm,-2", getcol(mm,-2), [2,4] ]
		], ops, ["mm", [[1,2,3],[3,4,5]] ]
	);
}
//getcol_test(["mode",22]);
//doc( getcol );

//========================================================
getSideFaces=["getSideFaces"
    ,"topIndices, botIndices, isclockwise=true"
    ,"array", "Array,Geometry"
, "Given topIndices and botIndices (both are array
;; of indices), return an array of indices for the 
;; side faces arguments for the polyhedron() function
;; to create a column object (note that the top
;; and bottom faces are not included).
;; isclockwise: true if looking down from top to 
;; bottom, the order of indices is clockwise. Default
;; is true.
"];
/*   
  clockwise (default)
;;       _2-_
;;    _-' |  '-_
;;   1_   |    '-3
;;   | '-_|   _-'| 
;;   |    '-0'   |
;;   |   _6_|    |
;;   |_-'   |'-_ | 
;;   5 _     |   7
;;     '-_  | _-'
;;        '-4' 

  => [ [0,3,7,4], [1,0,4,5], [2,1,5,6],[3,2,6,7]]

  clockwise = false
;;       _2-_
;;    _-' |  '-_
;;   3_   |    '-1
;;   | '-_|   _-'| 
;;   |    '-0'   |
;;   |   _6_|    |
;;   |_-'   |'-_ | 
;;   7 _    |   '5
;;     '-_  | _-'
;;        '-4' 

  => [ [3,0,4,7], [2,3,7,6], [1,2,5,6],[0,1,5,4] ]
 [[0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]] #FAIL#
"
*/
function getSideFaces(topIndices
                    , botIndices
                    , clockwise=true)=
let( ts= topIndices
   , bs= botIndices
   , tlast = last(ts)
   , blast = last(bs)
   )
(
  clockwise
  ? [for (i=range(ts))
      i==0
      ? [ ts[0], tlast, blast, bs[0] ]
      : [ ts[i], ts[i-1], bs[i-1], bs[i] ]
     ]
  : [for (i=range(ts))
      i<len(ts)-1
      ? [ ts[i], ts[i+1], bs[i+1], bs[i] ]
      : [ tlast, ts[0], bs[0], blast ]
      ]
);

module getSideFaces_test(ops)
{ 
    d1=[ [0,1,2,3],[4,5,6,7]];
	d2=[ [6,7,8],[9,10,11]];
	doctest(getSideFaces,
	[ ["d1[0], d1[1]", getSideFaces(d1[0],d1[1]),
        [[0,3,7,4], [1,0,4,5], [2,1,5,6], [3,2,6,7]]
      ]
     ,["d1[0], d1[1], clockwise=false"
        , getSideFaces(d1[0],d1[1],false),
        [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
      ]
     ,["d2[0], d2[1]", getSideFaces(d2[0],d2[1]),
        [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
      ]
     ,["d2[0], d2[1], clockwise=false"
        , getSideFaces(d2[0],d2[1],false),
        [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
      ]
	]
	,ops, ["d1", d1, "d2",d2]);
  
}

//function hashex(h, keys, _i_=0)=
//  _i_<len(keys)
//  ? concat( [ keys[_i_], hash(h, keys[_i_])], hashex(h, keys, _i_+1) )
//  :[];
function hashex(h, keys, _i_=0)=
  let( hh = [for(k=keys) [k, hash(h,k)] ] )
  [ for(kv=hh, x=kv) x ]   ;

//========================================================
getsubops= ["getsubops","ops, com_keys, sub_dfs"
            ,"hash or false", "module"
," Define the options of multiple sub objs. The scenario is that an obj O 
;; can have n common subobjs, such as left-arm, right-arm. They belong to
;; ''arms''. We want :
;;
;; 1. Both arms can be turned off by O[''arms'']=false
;; 2. Common arm props can be set on the obj level by
;;    O[''r'',1] that applies to entire obj including other parts
;; 3. Common arm props can also be set on the obj level by
;;    by O[''arms'']=[''r'',2] that will only apply to arms. 
;; 4. Each arm can be turned off by O[''leftarm'']=false
;; 5. Each arm can be set by O[''leftarm'']= [''r'',1]
;;
;; ops: obj-level ops 
;; subs: a hash of 2 k-v pairs: [''arms'', df_arms, ''armL'', df_armL]
;;       ''arms'': name used in ops as ops=[ ... ''arms'', true ...]
;;       df_arms : hash for default arms 
;;       ''armL'': name  used in ops as ops=[ ... ''armL'', true ...]
;;       df_armL : hash for default left arm
;; com_keys: like ''len'',''color''. This is the common keys that
;;           will set on entire obj and also carries over to all arms
;;
;; NOTE: com_keys SHOULD shows up in ops but NEVER in df_arms or df_armL
"];

function getsubops( 
      ops
    , df_ops=[] // default ops given at the design time
    , sub_dfs   // a hash showing layers of sub op defaults
                // ["arms", df_arms, "armL", df_armL]
                // The 1st k-v pair, ("arms",df_arms) is the group [name
                // , default], to which the next ("armL",df_armL), belongs.
    , com_keys  // names of sub ops, which you want them to be also be set 
                // from the obj level. Ex,["r", "color"]
    )=
(  let(subnames = keys( sub_dfs ) // name of subunit   ["arms", "armL"]
      ,df_subs  = vals( sub_dfs ) // df opt of subunit 
                                  // [df_arms, df_armL, df_armR]
      ,_df_ops    = [ for(i=range(df_ops)) // part of user ops that have 
                                           // keys showing up in com_keys
                      let( key= df_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) df_ops[i]
                  ] // ["r",1] 
      ,u_ops    = [ for(i=range(ops)) // part of user ops that have keys 
                                      // showing up in com_keys
                      let( key= ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) ops[i]
                  ] // ["r",1] 
      ,u_subops = [ for(sn= subnames)  // Extract subops settings from 
                    hash(ops,sn) ]     // user input ops ops, like 
                                       // ["arms",true, "armL",["r",1]]
      // order of reversed priority:
      // [  df_ops, df_arms, df_armL, u_ops,u_arms, u_armL ] 
      ,subops = updates(
          _df_ops
        , [df_subs[0]
          , df_subs[1]
          , u_ops
          , u_subops[0]==true? []:ishash(u_subops[0])?u_subops[0]:false
          , u_subops[1]==true? []:ishash(u_subops[1])?u_subops[1]:false
          ])

       ,show= sum( [ for(k=subnames) hash(ops,k)==false?1:0])==0
       )
       //_//df_ops
       //[ for(s=u_subops) s==false?1:0] //u_subops
    show? subops 
        : false
);
    

module getsubops_test( ops=[] )
{
    
    //=========================================
    //=========================================
    
    opt=[];
    df_arms=["len",3,"r",1];
    df_armL=["fn",5];
    df_armR=[];
    df_ops= [ "len",2
            , "arms", true
            , "armL", true
            , "armR", true
            ];
    com_keys= ["fn"];
    
    /*
        setops: To simulate obj( ops=[...] )
        args  : ops  
                // In real use, following args are set inside obj()
                df_ops
                df_subops // like ["arms",[...], "armL",[...]] 
                com_keys 
                objname   // like "obj","armL","armR"          
                propname  // like "len"
    */
    function setops(   
            ops=opt
            , df_ops=df_ops
            , df_subops=[ "arms",df_arms, "armL",df_armL,"armR",df_armR]
            , com_keys = com_keys
            , objname ="obj"
            , propname=undef
            )=
        let(
             _ops= update( df_ops, ops )
           , df_arms= hash(df_subops, "arms")  
           , df_armL= hash(df_subops, "armL")  
           , df_armR= hash(df_subops, "armR")  
           , ops_armL= getsubops( _ops, df_ops = df_ops
                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]                    
                    , com_keys= com_keys
                    )
            , ops_armR= getsubops( _ops, df_ops = df_ops
                        , sub_dfs= ["arms", df_arms, "armR", df_armR ]
                        , com_keys= com_keys
                        )
            , ops= objname=="obj"?_ops
                    :objname=="df_ops"?df_ops
                    :objname=="armL"?ops_armL
                    :ops_armR
            )
        // ["ops",_ops,"df_arms", df_arms, "ops_armL",ops_armL, "ops_armR",ops_armR];
        propname==undef? ops:hash(ops, propname);
      
     /**
        Debugging Arrow(), in which a getsubops call results in the warning:
        DEPRECATED: Using ranges of the form [begin:end] with begin value
        greater than the end value is deprecated.
            
     */       
     function debug_Arrow(ops=["r", 0.1, "heads", false, "color","red"])=
        let (
        
           df_ops=[ "r",0.02
               , "fn", $fn
               , "heads",true
               , "headP",true
               , "headQ",true
               , "debug",false
               , "twist", 0
               ]
          ,_ops= update(df_ops,ops) 
          ,df_heads= ["r", 1, "len", 2, "fn",$fn]    
          , ops_headP= getsubops( _ops, df_ops=df_ops
                , com_keys= [ "color","transp","fn"]
                , sub_dfs= ["heads", df_heads, "headP", [] ]
                ) 
        )
        ops_headP;
        
     doctest("getsubops"
        , [ ""
            ,"In above settings, ops is the user input, the rest are"
            ,"set at design time. With these:"
            ,""
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]]
            ,["ops"
                 , setops()
                 , ["len",2,"arms",true,"armL", true, "armR", true]]
            ,["ops_armL"
                 , setops(objname="armL")
                 , ["len", 3, "r", 1, "fn", 5]]
            ,["ops_armR"
                 , setops(objname="armR")
                 , [ "len", 3, "r", 1]]
                            
            ,""
            ,"Set r=4 at the obj level. Since r doesn't show up in com_keys, "
            ,"it only applies to obj, but not either arm. "
            ,""
            
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true],]
            ,["ops"
                 , setops(ops=["r",4])
                 , ["len",2,"arms",true,"armL", true, "armR", true,"r",4],]
            ,["ops_armL"
                 , setops(ops=["r",4],objname="armL")
                 , ["len", 3, "r", 1,"fn", 5]]
            ,["ops_armR"
                 , setops(ops=["r",4],objname="armR")
                 , [ "len", 3, "r", 1]]
                            
            ,""
            ,"Set fn=10 at the obj level. Since fn IS in com_keys, "
            ,"it applies to obj and both arms. "
            ,""
            
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true],]
            ,["ops"
                 , setops(ops=["fn",10])
                 , ["len",2,"arms",true,"armL", true, "armR", true, "fn",10],]
            ,["ops_armL"
                 , setops(ops=["fn",10],objname="armL")
                 , ["len", 3, "r", 1, "fn", 10]]
            ,["ops_armR"
                 , setops(ops=["fn",10],objname="armR")
                 , ["len", 3, "r", 1, "fn", 10]]  
                 
        
            ,""
            ,"Disable armL:"
            ,""
            
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]]
            ,["ops"
                 , setops(ops=["armL",false])
                 , ["len",2,"arms",true,"armL", false, "armR", true]]
            ,["ops_armL"
                 , setops(ops=["armL",false],objname="armL")
                 , false]
            ,["ops_armR"
                 , setops(ops=["armL",false],objname="armR")
                 , ["len", 3, "r", 1]]


            ,""
            ,"Disable both arms:"
            ,""
            
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]]
            ,["ops"
                 , setops(ops=["arms",false])
                 , ["len",2,"arms",false,"armL", true, "armR", true]]
            ,["ops_armL"
                 , setops(ops=["arms",false],objname="armL")
                 , false]
            ,["ops_armR"
                 , setops(ops=["arms",false],objname="armR")
                 , false]
        
        
//            ,""
//            ,"Debugging Arrow(), in which a getsubops call results in the warning:"
//            ,"DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated."
//            ,""
//            
//            ,["debug_Arrow(ops=[''r'', 0.1, ''heads'', false, ''color'',''red'']"
//             , debug_Arrow(ops=["r", 0.1, "heads", false, "color","red"]),[]
//             ]
                            
          ]
          
          

          
        , ops, [ "df_ops",df_ops, 
               , "df_arms", df_arms
               , "df_armL", df_armL
               , "df_armR", df_armR
               , "com_keys", com_keys
               , "ops",opt
               ]
    );
}


//========================================================
getTubeSideFaces=["getTubeSideFaces",
  "indexSlices, clockwise=true",
  "array", "Array, Geometry"
, "Given an array of indexSlices (an indexSlice
;; is a list of indices representing the order 
;; of points for a circle), return an array of 
;; faces for running polyhedon(). Typically
;; usage is in a column or tube of multiple
;; segments. For example, CurvedCone.
;;
;; See getSideFaces that is for tube of only
;; one segment. 
"];  
function getTubeSideFaces(
         indexSlices, clockwise=true)=  
  let( ii = indexSlices )
( 
    //mergeA( 
      [ for (i=range(len(ii)-1))
        getSideFaces( topIndices= ii[i]
                    , botIndices= ii[i+1]   
                    , clockwise=clockwise
                    )
      ]
    //)
);

module getTubeSideFaces_test(ops)
{ 
    d1=[ [6,7,8,9],[10,11,12,13],[14,15,16,17] ];
    
	doctest(getTubeSideFaces,
	[ ["d1", getTubeSideFaces(d1),
        [[[ 6, 9,13,10], [ 7, 6,10,11]
         ,[ 8, 7,11,12], [ 9, 8,12,13]]
        ,[[10,13,17,14], [11,10,14,15]
         ,[12,11,15,16], [13,12,16,17]]
        ]
      ]
//     ,["d1[0], d1[1], clockwise=false"
//        , getSideFaces(d1[0],d1[1],false),
//        [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
//      ]
//     ,["d2[0], d2[1]", getSideFaces(d2[0],d2[1]),
//        [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
//      ]
//     ,["d2[0], d2[1], clockwise=false"
//        , getSideFaces(d2[0],d2[1],false),
//        [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
//      ]
	]
	,ops, ["d1", d1]);
  
}

//========================================================
getWrapPts=[ "Given n points , return a nx2 multmatrix that each"
			,"2-item array has 2 points corresponding to the "
			,"original point given. If it's in the edge of line,"
			,"the points are othoCrossLinePts; otherwise, "
			,"midAngleCrossLinePts. Each new pt is r away from the"
			,"original pt. The main purpuse of this is to provide "
			,"points to make a 'wrapping' or 'coating' along a "
			,"series of pts."
			];
function getWrapPts( pts, r=1, _i_=0 )=
(
	//_i_==len(pts)-1?[]
	//:
	_i_>= len(pts)-1? [ begCrossLinePts( [ [pts[_i_],pts[_i_-1]]], r ) ]
		:concat( [_i_==0?  begCrossLinePts( [pts[0],pts[1]], r ) 
				:midAngleCrossLinePts( [pts[_i_],pts[_i_+1],pts[_i_+2]],r)
				]
			   , getWrapPts( pts, r=r, _i_=_i_+1 ) 
			   )	
);

module getWrapPts_demo()
{
	pts= [[2,-3,1], [1,0,3], [-2,1,4], [-4,2,2], [-4,4,0] ];
	Chain( pts, usespherenode=true );
	npts= getWrapPts(pts);
	echo(npts);

}


//========================================================
_h= [ "_h", "s,h, showkey=false, wrap=''{,}''", "string", "Hash, String",
"
 Given a string (s), a hash (h) and a wrapper string like {,},
;; replace the {key} found in s with the val where [key,val] is
;; in h. If showkey=true, replace {key} with key=val. This
;; is useful when echoing a hash:
;;
;; _h( ''{w},{h}'', [''h'',2,''w'',3])=> ''3,2''
;; _h( ''{w},{h}'', [''h'',2,''w'',3], true)=> ''w=3,h=2''
"];

function _h(s,h, showkey=false, wrap="{,}",isfmt=true, _i_=0)= 
( 
	len(h)<2?s
	:_i_>len(h)?s
	  :_h(
		  replace(s, replace(wrap,",",h[_i_])
				, str( showkey?h[_i_]:"", showkey?"=":""
					, h[_i_+1] //isfmt?_fmt(h[_i_+1]):h[_i_+1] 
					)
				)
		 , h
		 , wrap= wrap
		 , showkey= showkey
		 , _i_=_i_+2
		 )
);
	
module _h_test( ops )
{
	data=["m",3,"d",6];
	tmpl= "2014.{m}.{d}";
	doctest
	( 
		_h, [
		 	[
				str( tmpl, ", ", data )
				, _h("2014.{m}.{d}",  data)
				, "2014.3.6"
		 	]
			,"// showkey=true :"
	 	 	,[
				"''2014.[m].[d]'', data, showkey=true, wrap=''[,]''"
				, _h("2014.[m].[d]",  data, true, "[,]")
				, "2014.m=3.d=6"
		  	]
			,"// hash has less key:val " 
			,[
				"tmpl, [''m'',3]"
				, _h("2014.{m}.{d}", ["m",3])
				, "2014.3.{d}"
		 	]
			,"// hash has more key:val " 
			,[
				"tmpl, [''a'',1,''m'',3,''d'',6]"
				, _h("2014.{m}.{d}", ["a",1,"m",3,"d",6] )
				, "2014.3.6"
		 	]	
	  	  ], ops , ["tmpl", tmpl, "data", data]
	);
}
//h__test( ["mode",22] );
//doc( _h );


//========================================================
has=["has","o,x","T|F","String, Array, Inspect",
"
 Given an array or string (o) and x, return true if o 
;; contains x. 
"];
function has(o,x)= index(o,x)>-1;

module has_test(ops)
{ 
	doctest(has,
	[ ["''abcdef'',''d''",has("abcdef","d"),true]
	, ["''abcdef'',''m''",has("abcdef","m"),false]
	, ["[2,3,4,5],3",has([2,3,4,5],3),true]	
	, ["[2,3,4,5],6",has([2,3,4,5],6),false]	
	]
	,ops);
}	
//has_test(["mode",22]);
//doc( has );

//========================================================
hasAny=["hasAny", "o1,o2", "T|F", "Inspect, Array, String",
" Given two arrays/string, o, o2, return true if and only
;; if any of items in one appears in another. Same as
;; len(arrItcp(o1,o2))>0, but arrItcp is for arr only.
"];

function hasAny(o1,o2)= 
(
	sum( [for(i=range(o2)) has(o1,o2[i])?1:0 ] )>0
);
//	len(o2)==0?false
//	: ((index(o1, o2[0])>-1)||hasAny(o1,shrink(o2)) );

module hasAny_test( ops )
{
	doctest
	(
		hasAny
		,[
		  ["''abcdef'', [''b'',''x'']", hasAny("abcdef", ["b","x"]), true]
		 ,["''abcdef'', [''y'',''x'']", hasAny("abcdef", ["y","x"]), false]
		 ,["''abcdef'', ''bx''", hasAny("abcdef", "bx" ), true]
		 ,["[2,4,6], [4,5]", hasAny([2,4,6], [4,5]), true]
		 ,["[2,4,6], [5,9]", hasAny([2,4,6], [5,9]), false]
		]
		, ops
	);
}
//hasAny_test();
//doc( hasAny );




//========================================================
hash=[ "hash", "h,k, notfound=undef, if_v=undef, then_v=undef, else_v=undef", "Any","Hash",
" Given an array (h, arranged like h=[k,v,k2,v2 ...] to serve
;; as a hash) and a key (k), return a v from h: hash(h,k)=v.
;; If key not found, or value not found (means k is the last item
;; in h), return notfound. When if_v is defined, check if v=if_v.
;; If yes, return then_v. If not, return else_v if defined, or v
;; if not.
"];

function hash(h,k, notfound=undef, if_v=undef, then_v=undef, else_v=undef, _i_=0)= 
(
	!(isarr(h)||isstr(h))? notfound      // not a hash
	:_i_>len(h)-1? notfound  // key not found
		: h[_i_]==k?        // key found
			 (_i_==len(h)-1?notfound  // value not found
				: (if_v==undef    
					? (h[_i_+1]==undef?notfound:h[_i_+1])     
                            // if v=undef, return notfound; else v 
					: h[_i_+1]==if_v  // else, ask if v==if_v
						? then_v  	  // if yes, return if_v  
						:( else_v==undef    // else, ask if else_v defined
								?h[_i_+1]  // if no, return v
								:else_v)   // if yes, return else_v 
				  )	 
			)
			: hash( h=h, k=k, notfound=notfound, if_v=if_v, then_v=then_v
				  , else_v=else_v, _i_=_i_+2)           
);


module hash_test( ops )
{
	h= 	[ -50, 20 
			, "xy", [0,1] 
			, 2.3, 5 
			,[0,1],10
			, true, 1 
			, 1, false
			,-5
			];

	doctest
	(
		hash  
		,[ [ "h, -50", hash(h, -50), 20 ]
    		 , [ "h, ''xy''", hash(h, "xy"), [0,1] ]
		 , [ "h, 2.3", hash(h, 2.3), 5 ]
    		 , [ "h, [0,1]", hash(h, [0,1]), 10 ]
		 , [ "h, true", hash(h, true), 1 ]
    		 , [ "h, 1", hash(h, 1), false ]
    		 , [ "h, ''xx''", hash(h, "xx"), undef ]
		 , "## key found but it's in the last item without value:"
    		 , [ "h, -5", hash(h, -5), undef ]
		 , "## notfound is given: "
    		 , [ "h, -5,''df''", hash(h, -5,"df"), "df" , ["rem", "value not found"] ]
	 	 , [ "h, -50, ''df''", hash(h, -50,"df"), 20 ]
    		 , [ "h, -100, ''df''", hash(h, -100,"df"), "df" ]
		 , "## Other cases:"
	 	 , [ "[], -100", hash([], -100), undef ]
	 	 , [ "true, -100", hash(true, -100), undef ]
    		 , [ "[[undef,10]], undef", hash([ [undef,10]], undef), undef ]
 		 , "  "
	 	 , "## Note below: you can ask for a default upon a non-hash !!"
		 , [ "true, -100, ''df''", hash(true, -100, "df"), "df"]
	 	 , [ "true, -100 ", hash(true, -100), undef]
	 	 , [ "3, -100, ''df''", hash(3, -100, "df"), "df"]
	 	 , [ "''test'', -100, ''df''", hash("test", -100, "df"), "df"]
		 , [ "false, 5, ''df''", hash(false, 5, "df"), "df"]
		 , [ "0, 5, ''df''", hash(0, 5, "df"), "df" ]
		 , [ "undef, 5, ''df''", hash(undef, 5, "df"), "df" ]
		 , "  "
		 , "## New 20140528: if_v, then_v, else_v "
		 , [ "h, 2.3, if_v=5, then_v=''got_5''", hash(h, 2.3, if_v=5, then_v="got_5"), "got_5" ]
		 , [ "h, 2.3, if_v=6, then_v=''got_5''", hash(h, 2.3, if_v=6, then_v="got_5"), 5 ]
		 , [ "h, 2.3, if_v=6, then_v=''got_5'', else_v=''not_5''"
					, hash(h, 2.3, if_v=6, then_v="got_5", else_v="no_5"), "no_5" ]
		 ,"  "
		 ,"## New 20140730: You can use string for single-letter hash:"
		 ,["''a1b2c3'',''b''", hash("a1b2c3","b"), "2"]
           ," "
           ,"## 20150108: test concat "
           ,["[''a'',3,''b'',4,''a'',5]", hash(["a",3,"b",4,"a",5],"a"), 3] 
		], ops,["h",h]
	);
}
//hash_test(["mode",13]);
//doc(hash );


//========================================================
hashkvs=["hashkvs", "h", "array of arrays", "Hash",
"
 Given a hash (h), return [[k1,v1], [k2,v2] ...].
"];

function hashkvs(h, _i_=0)=
(
//	!isarr(h)? undef        // not a hash
//	:_i_>len(h)-1? []       // end of search
//		: concat( 
//				 [[h[_i_],h[_i_+1]]]
//			    , hashkvs(h, _i_=_i_+2)
//				)
	[for(i=[0:2:len(h)-2]) [h[i],h[i+1]]]
);

module hashkvs_test( ops )
{
	h = ["a",1,  "b",2, 3,33];
	//doc(hashkvs);
	//test(hashkvs, recs, ops);

	doctest( hashkvs,
	[		
		[ str(h), hashkvs(h), [["a",1],["b",2],[3,33]] ]
	], ops
	);	
}
//hashpairs_test( ["mode",2]);
//doc(hashkvs);




//========================================================
haskey=[ "haskey", "h,k", "T|F", "Hash, Inspect",
"
 Check if the hash (h) has a key (k)
"];

function haskey(h,k,_i_=0)=
(
	index(keys(h),k)>=0
/*	o[_i_][0]==x?true
	:(_i_<len(o)?
		haskey( o, x, _i_+1)
		: false		
	 )
*/

);

module haskey_test( ops )
{
	table = 	[ -50, 20 
			, "80", 25 
			, 2.3, "yes" 
			,[0,1],5
			, true, "YES" 
			];

	doctest
	(haskey, 
		 [ [ "table, -50", haskey(table, -50), true ]
    		 , [ "table, ''80''", haskey(table, "80"), true ]
		 , [ "table, ''xy''", haskey(table, "xy"), false ]
		 , [ "table, 2.3", haskey(table, 2.3), true ]
    		 , [ "table, [0,1]", haskey(table, [0,1]), true ]
		 , [ "table, true", haskey(table, true), true ]
    		 , [ "table, -100", haskey(table, -100), false ]
    		 , [ "[], -100", haskey([], -100), false ]
    		 , [ "[[undef,1], undef", haskey( [undef,1], undef), true
			, [["rem","###### NOTE THIS!"]] ]
    		 ],ops, ["table", table]
	);

}
//haskey_test(["mode",22]);
//doc(haskey);



//========================================================
incenterPt=["incenterPt", "pqr", "point", "Geometry"
,
 " Given a 3-pointer (pqr), return the incenter point of the triangle, which
;; is where all 3 angle bisector lines meet."
];

function incenterPt(pqr)=
(
	lineCrossPt( pqr[0], angleBisectPt( rearr(pqr, [1,0,2]) )
			  , pqr[1], angleBisectPt( pqr )
			  )
);

module incenterPt_test(ops=["mode",13]) { doctest (incenterPt, ops=ops); }

module incenterPt_demo()
{
	pqr = randPts(3);
	ic  = incenterPt(pqr);
	MarkPts( concat(pqr,[ic]) );
	Chain( pqr );
}
//incenterPt_demo();

//========================================================
idx=["index", "o,x", "int", "Index,Array,String,Inspect",
" Return the index of x in o (a str|arr), or undef if not found.
"];
function idx(a,x)=
    [for(i=[0:len(a)-1]) if(a[i]==x)i][0];
        
//========================================================
index=["index", "o,x", "int", "Index,Array,String,Inspect",
" Return the index of x in o (a str|arr), or -1 if not found.
"];	 

function index(o,x, wrap=undef, _i_=0)= 
 let( x  = wrap?replace(wrap,",",x):x
	, oi = isstr(o)
		   ? slice(o, _i_, _i_+len(x))
		   : o[_i_]
    , end= _i_==len(o)
	, found= isnum(x)? isequal(oi,x): oi== x 
    )
(
	x=="" || end ?-1
	: found 
	  ? _i_
	  : index(o,x,undef,_i_+1)
);

//index_test();

module index_test( ops )
{

     arr = ["xy","xz","yz"];
	arr2= ["I_am_the_findw"];

	doctest
	(index,  
	 [ 
	 "## Array indexing ##"
	 , [ "arr, ''xz''", index(["xy","xz","yz"],"xz"),  1 ]
    	 , [ "arr, ''yz''", index(["xy","xz","yz"],"yz"),  2 ]
    	 , [ "arr, ''xx''", index(["xy","xz","yz"],"xx"),  -1 ]
    	 , [ "[], ''xx''", index([],"xx"),  -1 ]
    	 ,"## String indexing ##"
	 , [ "arr2, ''a''", index( "I_am_the_findw","a"),  2 ]
    	 , [ "arr2, ''am''", index( "I_am_the_findw","am"),  2 ]
    	 , [ "''abcdef'',''f''", index(  "abcdef","f"),  5 ]
    	 , [ "''findw'',''n''", index( "findw","n"),  2 ]
    	 , [ "arr2, ''find''", index( "I_am_the_findw","find"),  9 ]
    	 , [ "arr2, ''the''", index( "I_am_the_findw","the"),  5 ]
    	 , [ "arr2, ''findw''", index("I_am_the_ffindw","findw"),  10 ]
    	 , [ "''findw'', ''findw''", index("findw", "findw"),  0 ]
    	 , [ "''findw'', ''dwi''", index( "findw","dwi"),  -1 ]
    	 , [ "arr2, ''The''", index("I_am_the_findw","The"),  -1 ]
	 , [ "arr2, ''tex''",index( "replace__text","tex"),  9 ]
    	 , [ "''replace__ttext'', ''tex''", index("replace__ttext","tex"), 10 ]
    	 , [ "''replace_t_text'',''tex''", index( "replace_t_text","text"),  10]
	 , [ "''abc'', ''abcdef''",index("abc", "abcdef"),  -1 ]
	 , [ "'''', ''abcdef''",index( "","abcdef"),  -1 ]
	 , [ "'''', ''abc''",index( "abc",""),  -1 ]
	 ,"## String indexing with wrap ##"
	 , [ "''{a}bc'',''a'', ''{,}''", index("{a}bc","a",wrap="{,}"), 0] 
	 , [ "''a}bc'',''a'', '',}''", index("a}bc","a",wrap=",}"), 0] 
	 , [ "''a{b}c'',''b'', ''{,}''", index("a{b}c","b",wrap="{,}"), 1] 
	 , [ "''a//bc'',''b'', ''//,''", index("a//bc","b",wrap="//,"), 1] 
	 , [ "''abc:d'',''c'', '',:''", index("abc:d","c",wrap=",:"), 2] 
	 , [ "''a{_b_}{_c_}'',''c'', ''{_,_}''", index("a{_b_}{_c_}","c",wrap="{_,_}"), 6] 
	 ,"## Others ##"
	 , [ "[2, ,3, undef], undef", index([2,3, undef], undef), 2 ]
    	 ],ops, ["arr",arr, "arr2",arr2]
	);
}
//index_test(["mode",22]);
//doc(index);


//========================================================
inrange=[ "inrange", "o,i", "T|F", "Index, Array, String, Inspect",
" Check if index i is in range of o (a str|arr). i could be negative.\\
"];

function inrange(o,i)=
(
	/*
	len: 1 2 3 4 5
		 0 1 2 3 4   i>=0
   		-5-4-3-2-1   i <0    
	*/
	
	(-len(o)<=i) && ( i<len(o)) 

);

module inrange_test( ops )
{

	doctest
	( 
	 inrange, [
		"## string ##"
	  , [ "''789'',2", inrange("789",2), true ]
	  ,	[ "''789'',3", inrange("789",3), false ]
	  ,	[ "''789'',-2", inrange("789",-2), true ]
	  ,	[ "''789'',-3", inrange("789",-3), true ]
	  ,	[ "''789'',-4", inrange("789",-4), false ]
	  ,	[ "'',0", inrange("",0), false ]
	  ,	[ "''789'',''a''", inrange("789","a"), false ]
	  ,	[ "''789'',true", inrange("789",true), false ]
	  ,	[ "''789'',[3]", inrange("789",[3]), false ]
	  ,	[ "''789'',undef", inrange("789",undef), false ]
	  , "## array ##"
	  ,	[ "[7,8,9],2", inrange([7,8,9],2), true ]
	  ,	[ "[7,8,9],3", inrange([7,8,9],3), false ]
	  ,	[ "[7,8,9],-2", inrange([7,8,9],-2), true ]
	  ,	[ "[7,8,9],-3", inrange([7,8,9],-3), true ]
	  ,	[ "[7,8,9],-4", inrange([7,8,9],-4), false ]
	  ,	[ "[],0", inrange([],0), false ]
	  ,	[ "[7,8,9],''a''", inrange([7,8,9],"a"), false ]
	  ,	[ "[7,8,9],true", inrange([7,8,9],true), false ]
	  ,	[ "[7,8,9],[3]", inrange([7,8,9],[3]), false ]
	  ,	[ "[7,8,9],undef", inrange([7,8,9],undef), false ]
	  ], ops
	);
}
//inrange_test(["mode",22]);
//doc(inrange);


//========================================================
int=[ "int", "x", "int", "Type, Number",
" Given x, convert x to integer. 
;;
;; int(''-3.3'') => -3
"];

function int(x,_i_=0)=
(
	isint(x)? x
	:isfloat(x)? sign(x)*round(abs(x))
	 :isarr(x)? undef
	   :x==""? 0
	  :index(x,".")==0? 0
		: ( _i_==0 && x[0]=="-"? 
			-int(x,_i_+1)
			:(_i_< min( len(x),
					  index(x,".")<0?
					  len(x):index(x,".")
					)?
			(pow(10, (min( len(x),
							index(x,".")<0?
							len(x):index(x,".")
						 )-_i_-1
					 )
				)
			* hash(  ["0",0, "1",1, "2",2
					,"3",3, "4",4, "5",5
					,"6",6, "7",7, "8",8
					,"9",9], x[_i_] )
			
			+ int(x, _i_+1)
			 ):0
			)
		  )
);
module int_test( ops )
{
	doctest
	( 
		int, [
		["3",int(3),3]
	  , ["3.3",int(3.3),3]
	  , ["-3.3",int(-3.3),-3]
	  , ["3.8",int(3.8),4]
	  , ["9.8",int(9.8),10]
	  , ["-3.8",int(-3.8),-4]
	  , ["''-3.3''",int("-3.3"),-3]
	  , ["''-3.8''",int("-3.8"),-3, ["rem"," !!Note this!!"]]
	  , ["''3''",int("3"),3]
	  , ["''3.0''",int("3.0"),3]
	  , ["''0.3''",int("0.3"),0]
	  , ["''34''",int("34"),34]
	  , ["''-34''",int("-34"),-34]
	  , ["''-34.5''",int("-34.5"),-34]
	  , ["''345''",int("345"),345]
	  , ["''3456''",int("3456"),3456]
	  , ["''789987''",int("789987"),789987]
	  , ["''789.987''",int("789.987"),789, ["rem"," !!Note this!!"]]
	  , ["[3,4]",int([3,4]),undef]
	  , ["''x''",int("x"),undef]
	  , ["''34-5''",int("34-5"),undef]
	  , ["''34x56''",int("34x56"),undef]
	 ],ops	
	);
}
//int_test(["mode",22]);
//doc(int);


//========================================================
intersectPt=["intersectPt", "pq, target", "point", "Array, Point, Geometry"
, " 

"];

module intersectPt_test( ops=["mode",13] )
{
	doctest( intersectPt, ops=ops );
}

/*

Intersection of line and plan:
http://paulbourke.net/geometry/pointlineplane/

The equation of a plane (points P are on the plane with normal N and point P3 on the plane) can be written as
N dot (P - P3) = 0
The equation of the line (points P on the line passing through points P1 and P2) can be written as
P = P1 + u (P2 - P1)
The intersection of these two occurs when
N dot (P1 + u (P2 - P1)) = N dot P3
Solving for u gives

u = (N dot (P3-P1) ) / (N dot (P2-P1))

P = P1 +   (N dot (P3-P1) ) / (N dot (P2-P1)) * (P2 - P1)


[P1,P2] => pq
[P3, P4, P5] = target
N = normal(rst)

P = pq[0] + dot(normal(rst), target[0]-pq[0]) 
		 / dot(normal(rst), pq[1]    -pq[0])
		 * (pq[1]-pq[0])


*/

function intersectPt( pq, target) = 
(
	len(target)>2
	?  // target =[r,s,t ...]: line pq intersets with plane rst
//	  pq[0] + dot(normal(rst)+target[1], target[1]-pq[0]) 
//		 / dot(normal(rst)+target[1], pq[1]    -pq[0])
//		 * (pq[1]-pq[0])
	  onlinePt( pq, len= dist([pq[0],projPt( pq[0], target )])
			/cos( angle([projPt( pq[0], target ),pq[0],pq[1]])) )	
	: //target =[r,s]: line pq intersets with line rs
	lineCrossPt( pq[0], pq[1], mn[0],mn[1] )
);

module intersectPt_demo()
{
	echom("intersectPt");

	rstu=  [[-2.65857, 0.0436345, -1.84591], [-1.33038, -1.84482, 1.12422], [3.58766, 0.935956, 0.693008], [2.25947, 2.8244, -2.27714]];

	T = rstu[2];

	MarkPts( rstu, ["labels","RSTU"] );
	
	pq= [[-0.591133, -2.27006, -1.82472], [2.36348, 2.54317, 0.528988]];

	pq2 = [ onlinePt( pq, ratio=0.25 ), onlinePt( pq, ratio= 0.75 ) ];
	P = pq2[0];
	Q = pq2[1];
	//echo( "pq2", pq2 );

	J = projPt( P, rstu );

	Line0( [P,J], ["r",0.01] );
	Mark90([P,J,T]);

	aJPQ = angle([J,P,Q]);
	echo("aJPQ", aJPQ);
	PX = dist([P,J])/cos( aJPQ);
	//X = onlinePt( [P,Q], dist = PX);

	X = onlinePt( [P,Q], len= dist([P,projPt( P, rstu )])/cos( angle([projPt( P, rstu ),P,Q])) );

	X = intersectPt( pq, rstu );

	MarkPts([P,Q,J,X], ["labels", "PQJX"]); 

	Line0( pq, ["r", 0.01] );
	Plane( rstu );
}

module intersectPt_demo_2()
{
	echom( "intersectPt_demo_2" );
	pq = randPts(2);
 	pq = [[-1.21325, 0.501097, -1.23254], [1.71766, 1.58609, 0.690459]];

	P = pq[0];
	Q = pq[1];
	rstu = randOnPlanePts(4);
	//rstu= [[2.51256, 0.0578135, -3.09367], [1.32865, 4.43852, -1.8887], [-5.60561, 0.027173, 3.29636],[0.429039, -0.251856, -1.47247] ];

	echo("pq", pq);
	echo("rstu",rstu);

	J = projPt( pq[0], rstu );
	echo("J", J );

	aJPQ = angle( [J,P,Q]);
	echo("aJPQ", aJPQ);

	//Dim( [P,J,rstu[1] ] );
	
	PJ = dist([P,J]);
	echo("PJ",PJ);

	PX = PJ / cos(aJPQ);
	echo("PX", PX);

	X = onlinePt( [P,Q], len = PX );
	echo( "X", X);
	MarkPts([J,X], ["labels","JX"]);

	Line0( [J, rstu[0]], ["r",0.01] );	
	Line0( [J, rstu[1]], ["r",0.01] );	
	Line0( [J, rstu[2]], ["r",0.01] );	
	Line0( [J, rstu[3]], ["r",0.01] );	

	Plane( rstu );
	MarkPts( pq, ["labels", "PQ"] );
	MarkPts( rstu, ["labels","RSTU"] );
	//X = intersectPt( pq, rstu );
	//MarkPts( X, ["labels","X"] );
	Line0(pq, ["r",0.02] );
}

//intersectPt_demo();
//intersectPt_demo_2();


/*


	P1 = Q + t * (P-Q)
	P2 = N + s * (M-N)

	when P1=P2

	Q.x + t(P-Q).x = N.x + t(M-N).x


	t = 

	let:	A = a point on both pq and mn
		c = PA / PQ
 		
	=> A = [ c * dx(pq), c * dy(pq), c * dz(pq) ] 
		= c * [ dx(pq), dy(pq), dz(pq) ] 
		= c* (Q-P)
		= PA (Q-P) / PQ

	let d = MA/MN
	=> A = MA (N-M ) / MN

	PA(Q-P)/PQ = MA(N-M)/MN

	 PA							   MA
	---- [ dx(pq), dy(pq), dz(pq) ] = ---- [ dx(mn), dy(mn), dz(mn) ] 
	 PQ							   MN	

	let a = PA/PQ, b = MA/MN

	a [ dx(pq), dy(pq), dz(pq) ] = b [ dx(mn), dy(mn), dz(mn) ] 
	
	a * dx(pq) = b * dx(mn)
	a * dy(pq) = b * dy(mn)
	a * dz(pq) = b * dz(mn)

	a =  b * dx(mn) / dx(pq)
	    (a * dy(pq) / dy(mn)) * dx(mn) / dx(pq)

			(N-M) PQ
	PA = MA ---------
			(Q-P) MN

	//-----------------------------------------------

      P3 
	   &'-_
	   |&  '-._     
	   | &     '-._
	   |  &        '-_ 
	   |   &         _'P2
	   |  b &      -' /
	   |     &  _-'  /	
	   |      X'    /
	   |  a -' &   /
	   |  -'    & /
	   |-'_______&
     P1            P4


http://paulbourke.net/geometry/pointlineplane/

Line a = P:Q
Line b = M:N

This note describes the technique and algorithm for determining the intersection point of two lines (or line segments) in 2 dimensions.

The equations of the lines are

Pa = P + ua ( Q - P )
Pb = M + ub ( N - M )

Solving for the point where Pa = Pb gives the following two equations in two unknowns (ua and ub)

P.x + ua (Q.x - P.x) = M.x + ub (N.x - M.x)

and 

P.y + ua (Q.y - P.y) = M.y + ub (N.y - M.y)

Solving gives the following expressions for ua and ub

k = [ (N.y-M.y)(Q.x-P.x) - (N.x-M.x)(Q.y-P.y) ]

ua = [ (N.x-M.x)(P.y-M.y) - (N.y-M.y)(P.x-M.x) ] / k

ub = [ (Q.x-P.x)(P.y-M.y) - (Q.y-P.y)(P.x-M.x) ] / k




Line a = P1:P2
Line b = P3:P4

This note describes the technique and algorithm for determining the intersection point of two lines (or line segments) in 2 dimensions.

The equations of the lines are

Pa = P1 + ua ( P2 - P1 )
Pb = P3 + ub ( P4 - P3 )

Solving for the point where Pa = Pb gives the following two equations in two unknowns (ua and ub)

x1 + ua (x2 - x1) = x3 + ub (x4 - x3)

and 

y1 + ua (y2 - y1) = y3 + ub (y4 - y3)

Solving gives the following expressions for ua and ub

k = [ (y4-y3)(x2-x1) - (x4-x3)(y2-y1) ]

ua = [ (x4-x3)(y1-y3) - (y4-y3)(x1-x3) ] / k

ub = [ (x2-x1)(y1-y3) - (y2-y1)(x1-x3) ] / k

*/

//);





//========================================================
is90=["is90", "pq1,pq2", "T|F", "Inspect, Geometry",
"
 Given 2 2-pointers pq1, pq2 (each represents a line), return
;; true if the angle between two lines is 90 degree.
;;
;; New 20140613: can take a single 3-pointer, pqr:
;;
;;   is90( pqr );
;;
;; Same as angle( pqr )==90. Refer to : othoPt_test() for testing.
"];

function is90(pq1, pq2)=
( 
	pq2==undef                   // When pq1 is a 3-pointer and pq2 not given; is90(pqr)
	? (pq1[1]-pq1[0]) * (pq1[2]-pq1[1])<ZERO  
	: p2v(pq1)*p2v(pq2)< ZERO    // is90( [p1,q1],[p2,q2] )
);

module is90_test(ops)
{
	pqr = randPts(3);           // 3 random points
	sqPts = 	squarePts(pqr);   //  This gives 4 points of a square, must be perpendicular
							  // The first 2 pts = pq[0], pq[1]. 	

	pq1 = [ pqr[0], pqr[1]   ];  // Points 0,1 of the square
	_pq2= [ pqr[1], sqPts[2] ];  // Points 1,2 of the square

	// Make a random translation to pq2x:
	t = randPt();
	pq2 = [ _pq2[0]+t, _pq2[1]+t ];

	doctest( is90
	,[
	  "// A 3-pointer, pqr, is randomly generated. It is then made to generate"
	 ,"// a square with sqarePts(pqr), in which side PQ is our pq1, and a side"
	 ,"// perpendicular to pq is translated to somewhere randomly and taken as pq2."
	,  [ "pq1,pq2", is90(pq1,pq2), true ]
	],ops, ["pq1",pq1,"pq2",pq2]  );
}

module is90_demo()
{
	pqr = randPts(3);

	sqPts = 	squarePts(pqr);   // This gives 4 points of a square, must be perpendicular

	//MarkPts(sqPts);

	pq = [ pqr[0], pqr[1]   ];
	pq2= [ pqr[1], sqPts[2] ];
	
	//echo(( pqr[0]-pqr[1])*(pqr[1]-sqPts[2] ));
	p3 = concat( pq, [pq2[1]] ); // 3 points that the middle is the 90-deg angle 

	//v=pow(norm( p3[0]-p3[1]),2)+ pow(norm(p3[1]-p3[2]),2)- pow( norm(p3[0]-p3[2]),2);
	//echo(v, v<1e-10, v ==0);

	Plane( p3, [["mode",3]] );
	MarkPts( p3 );
	echo( is90( pq, pq2 ) );

}
//is90_test();
//is90_demo();
//doc(is90);


//========================================================
isarr=[ "isarr","x", "T|F", "Type, Inspect, Array",
"
 Given x, return true if x is an array.
"];

function isarr(x)= type(x)=="arr";

module isarr_test( ops )
{
	doctest
	(
		isarr, [
		[ [1,2,3], isarr([1,2,3]), true]
	 ,	[ "''a''", isarr("a"), false]
	 ,	[ "true", isarr(true), false]
	 ,	[ "false", isarr(false), false]
	 ,	[ "undef", isarr(undef), false]
	 ],ops
	);
}
//isarr_test(["mode",22]);
//doc(isarr);



//========================================================
isbool=[ "isbool","x", "T|F", "Type, Inspect",
"
 Given x, return true if x is a boolean (true|false).
"];

function isbool(x)= x==true || x==false; 

module isbool_test( ops )
{
	doctest
	(
		isbool, [
		[ [1,2,3], isbool([1,2,3]), false]
	 ,	[ "''a''", isbool("a"), false]
	 ,	[ "true", isbool(true), true]
	 ,	[ "false", isbool(false), true]
	 ,	[ "undef", isbool(undef), false]
	 ],ops
	);
}
//doc(isbool);


//========================================================
isequal=["isequal", "a,b", "T|F", "Inspect",
_s(" Given 2 numbers a and b, return true if they are determined to
;; be the same value. This is needed because sometimes a calculation
;; that should return 0 ends up with something like 0.0000000000102.
;; So it fails to compare with 0.0000000000104 (another shoulda-been
;; 0) correctly. This function checks if a-b is smaller than the
;; predifined ZERO constant ({_}) and return true if it is.
", [ZERO])
];

function isequal(a,b)=
(
  	isnum(a)&&isnum(b)
	? (abs(a-b)< ZERO)
	: a==b
);

module isequal_test( ops )
{
	doctest( isequal, 
	[ 
		str("// ZERO = ",ZERO)
	 	,["1e-15,0", isequal(1e-15,0),true]
	 	,["1e-12,0", isequal(1e-12,0),false]
		,["1e-9,1e-10", isequal(1e-9,1e-10),false]
		,["1e-12,1e-13", isequal(1e-12,1e-13),false]
		,["1e-14,1e-13", isequal(1e-14,1e-13),true]
		,["''1e-15'',0", isequal("1e-15",0),false]
	 	,["''1e-15'',''0''", isequal("1e-15","0"),false]
	 	,["''abc'',''abc''", isequal("abc","abc"),true]
		,["undef,undef", isequal(undef,undef),true]
		,["undef,false", isequal(undef,false),false]
	 ],ops
	);
}


//doc(isequal);





//========================================================
isfloat=[ "isfloat","x", "T|F","Type, Inspect, Number",
"
 Check if x is a float.
"];

function isfloat(x)= type(x)=="float";

module isfloat_test( ops )
{
	doctest(isfloat, 
	[
		[ "3", isfloat(3), false]
	 ,	[ "3.2", isfloat(3.2), true]
	 ,	[ "''a''", isfloat("a"), false]
	],ops
	);
}
//isfloat_test(["mode",22]);
//doc(isfloat);




//========================================================
ishash = [ "ishash","x", "T|F", "Type, Inspect, Number",
"
 Check if x **could** be a hash, i.e., an array with even number length.
"];

function ishash(x)=  (type(x)=="arr") && ( len(x)/2 == floor(len(x)/2));

module ishash_test( ops=["mode",13] )
{
	doctest(ishash, 
	[
		[ "[1,2,3]", ishash([1,2,3]), false]
	 ,	[ "[1,2,3,4]", ishash([1,2,3,4]), true]
	 ,	[ "3", ishash(3), false]
	 ,	[ "3.2", ishash(3.2), false]
	 ,	[ "''a''", ishash("a"), false]
	],ops
	);
}
//ishash_test();


//========================================================
isint = [ "isint","x", "T|F", "Type,Inspect, Number",
"
 Check if x is an integer.
"];

function isint(x)=   type(x)=="int";

module isint_test( ops )
{
	doctest(isint, 
	[
		[ "3", isint(3), true]
	 ,	[ "3.2", isint(3.2), false]
	 ,	[ "''a''", isint("a"), false]
	],ops
	);
}
//isint_test(["mode",22]);
//doc(isint);



//========================================================
isnum =[ "isnum","x", "T|F", "Type,Inspect,Number",
"
 Check if x is a number.
"];

function isnum(x)=   type(x)=="int" || type(x)=="float";

module isnum_test( ops )
{
	doctest(isnum, 
	[
		[ "3", isnum(3), true]
	 ,	[ "3.2", isnum(3.2), true]
	 ,	[ "''a''", isnum("a"), false]
	],ops
	);
}
//isnum_test( ["mode",22] );
//doc(isnum);



//========================================================
isOnPlane=["isOnPlane", "pqr,pt,planecoefs", "T|F", "Inspect, Point, Plane",
"
 Given a 3-pointer (pqr) and a point (pt) and that will be converted to planecoefs,
;; (= [a,b,c,k] or [a,b,c]) or given planecoefs directly, check if the pt is on
;; the plane:
;;
;;   a*pt.x + b*pt.y + c*pt.z + k == ZERO
"];

function isOnPlane( pqr, pt )=
 let( pco = isarr(pqr[0])?planecoefs(pqr):pqr  
	, got =  pco[0]*pt.x
	   + pco[1]*pt.y
	   + pco[2]*pt.z
	   + (len(pco)==3?0:pco[3])
	)
(
	is0(got)
);

module isOnPlane_test( ops )
{
	pqr    = randPts(3);
	pcoefs = planecoefs( pqr );
	othopt = othoPt( pqr );
	abipt  = angleBisectPt( pqr );
	midpt  = midPt( p01(pqr) );

	doctest( isOnPlane,
	[
		
		 "## Randomly create a 3-pointer, pqr, and get its planecoefs, pcoefs."
		,"## From pqr also create othoPt, angleBisectPt and midPt (of p,q)."
		,"## They should all be on the same plane."
		,[ "pqr,P(pqr)", isOnPlane( pqr,P(pqr) ), true ]
		,[ "pqr, othopt", isOnPlane( pqr, othopt  ), true ]
		,[ "newy(pqr,othopt), othopt", isOnPlane( newy(pqr,othopt), othopt  ), true ]
		,[ "pcoefs, othopt", isOnPlane( pcoefs,othopt), true ]
		,"// angleBisectPt: "
		,[ "newy(pqr,abipt), abipt", isOnPlane( newy( pqr,abipt), abipt  ), true ]
		,[ "pcoefs, abipt", isOnPlane( pcoefs, abipt ), true ]
		,"// Mixed:"
		,[ "[othopt, abipt, midpt], pqr[2]"
			, isOnPlane( [othopt, abipt, midpt] , pqr[2] ), true ]
	], ops, ["pqr", pqr
		    , "pcoefs", pcoefs
			, "othopt", othopt
			, "abipt", abipt
			, "midpt", midpt]
	);
}



//========================================================
isstr= [ "isstr","x", "T|F", "Type,String,Inspect",
"
 Check if x is a string.\\
"];

function isstr(x)=   type(x)=="str";

module isstr_test( ops )
{
	doctest(isstr, 
	[
		[ "3", isstr(3), false]
	 ,	[ "3.2", isstr(3.2), false]
	 ,	[ "''a''", isstr("a"), true]
	],ops
	);
}
//isstr_test( ["mode",22] );
//doc(isstr);


//
////========================================================
is0=["is0","n","T|F","Inspect, Number",
str( " Given a number, check if it is 'equivalent' to zero. The 
;; decision is made by comparing it to the constant, ZERO=", ZERO )
,"isequal"
];

function is0(n) = abs(n)<ZERO;

module is0_test( ops=["mode",13] )
{
	doctest( is0
	,[
		"// A value, 1e-15, is NOT zero:"
		,str("//   1e-15==0: ", 1e-15==0 )
		,"// but it's so small that it is equivalent to zero:"
		,[ "1e-15",is0(1e-15), true ]
	 ],ops
	);
}


//========================================================
istype=["istype","o,t","T|F", "core, inspect",
"
  Given a variable (o) and a typename (t), return true if 
;; o is type t. It can also check if o is one of multiple types
;; given, in which case, t could be entered as:
;;
;; istype(o, [''arr'',''str'']
;; istype(o, ''arr,str'')
;; istype(o, ''arr str'')
"];

function istype(o,t)=
let( t = index(t,",")>=0
		? split(t,",")
		:index(t," ")>=0
		? split(t," "): t )
(	
//	[t, [for(x=t) type(o)==x?1:0 ] , sum([for(x=t) type(o)==x?1:0 ])]

	isstr(t)
	? type(o)==t
	: isarr(t)
	  ? has(t, type(o)) 
	  : false
);

module istype_test( ops=["mode",13] )
{
	doctest( istype,
	[
		["''3'',''str''", istype("3","str"), true]
		,["''3'',[''str'',''arr'']", istype("3",["str","arr"]), true]
		,["3,[''str'',''arr'']", istype(3,["str","arr"]), false]
		,["3,[''str'',''arr'',''int'']", istype(3,["str","arr","int"]), true]
		,["3,''str,arr,int''", istype(3,"str,arr,int"), true]
		,["3,''str arr int''", istype(3,"str arr int"), true]
		,["3,''str arr bool''", istype(3,"str arr bool"), false]
	], ops
	);
}



//========================================================
join=[ "join", "arr, sp=\",\"" , "string","Array",
" Join items of arr into a string, separated by sp.
"];

function join(arr, sp="", _i_=0)=
//let(L=len(arr)) 
(
	_i_<len(arr)?
	str(
		arr[ _i_ ]
		, _i_<len(arr)-1?sp:""
		,join(arr, sp, _i_+1)
	):""

//	L==0?"":str( arr[0], L==1?"":sp, join(slice(arr,1),sp=sp) )

); 

module join_test( ops )
{
	doctest( join, 
	
	[
	    [ "[2,3,4]",      join([2,3,4]),      "234" ]
	  , [ "[2,3,4], ''|''", join([2,3,4], "|"), "2|3|4" ]
	  , [ "[2,[5,1],''a''], ''/''", join([2,[5,1],"a"], "/"), "2/[5, 1]/a" ]
	  , [ "[], ''|''", join([], "|"), "" ]
	],ops
	);
}
//join_test( ["mode",22] );
//doc(join);



//========================================================
joinarr=[ "joinarr", "arr" , "array","Array",
" Given an array(arr), concat items in arr. See the difference below:
;; 
;;  concat( [1,2], [3,4] ) => [1,2,3,4] 
;;  joinarr( [ [1,2],[3,4] ] ) => [1,2,3,4] 
;;  join( [ [1,2],[3,4] ] ) => ''[1,2],[3,4]'' 
"];

function joinarr(arr,_I_=0)= 
(
	_I_>len(arr)-1? []
	: concat( arr[_I_], joinarr( arr, _I_+1) )  
); 

module joinarr_test( ops )
{
	doctest( joinarr, 
	
	[
	    [ "[[2,3],[4,5]]",  joinarr([[2,3],[4,5]]),    [2,3,4,5] ]
//	  , [ "[2,3,4], ''|''", joinarr([2,3,4], "|"), "2|3|4" ]
//	  , [ "[2,[5,1],''a''], ''/''", joinarr([2,[5,1],"a"], "/"), "2/[5, 1]/a" ]
//	  , [ "[], ''|''", joinarr([], "|"), "" ]
	],ops
	);
}
//join_test( ["mode",22] );
//doc(join);



//========================================================
keys=["keys", "h", "array", "Hash",
"
 Given a hash (h), return keys of h as an array.
"];

function keys(h)=
(
	[for(i=[0:2:len(h)-2]) h[i]]
);

module keys_test( ops )
{
	doctest
	( keys, 
	  [
		[ "[''a'',1,''b'',2]", keys(["a",1,"b",2]), ["a","b"] ]
	  ,	[ "[1,2,3,4]", keys([1,2,3,4]), [1,3] ]
	  ,	[ "[1,2],0,3,4", keys([[1,2],0,3,4]), [[1,2],3] ]
	  ,	[ "[]", keys([]), [] ]
	  ],ops
	);
}
//keys_test( ["mode",22] );
//doc(keys);


//========================================================
kidx=["kidx", "h,k", "hash", "Array, Hash"
, "Return index of key k in hash h. It's the order of k
;; in keys(h).
;; 2015.1.30"
];
function kidx(h,k)=
    [for(i=[0:2:len(h)-2]) if(h[i]==k)i/2][0];



//========================================================
lcase=["lcase", "s","str","String",
 " Convert all characters in the given string (s) to lower case. "
];
_alphabetHash= split("A,a,B,b,C,c,D,d,E,e,F,f,G,g,H,h,I,i,J,j,K,k,L,l,M,m,N,n,O,o,P,p,Q,q,
R,r,S,s,T,t,U,u,V,v,W,w,X,x,Y,y,Z,z",",");
function lcase(s, _I_=0)=
(
  	_I_<len(s)
	? str( hash( _alphabetHash, s[_I_], notfound=s[_I_] )
		, lcase(s,_I_+1)
		)
	:""
);
    
//========================================================
last=[ "last", "o", "item", "Index, String, Array" ,
"
 Given a str or arr (o), return the last item of o.
"];

function last(o)= o[len(o)-1]; 

module last_test( ops )
{
	doctest
	( last, 
	 [ [ [20,30,40,50], last([20,30,40,50]), 50 ]
    	 , [ "abcdef", last("abcdef"), "f" ]
     ],ops
	);
}
//last_test( ["mode",22] );
//doc(last);



module lcase_test( ops= ["mode",13] )
{
	doctest( lcase,
	[
		["''aBCC,Xyz''", lcase("aBC,Xyz"), "abc,xyz"]
	], ops
	);
}




//========================================================
lineCrossPt=["lineCrossPt", "P,Q,M,N","point", "Geometry",
"
 Given 4 points (P,Q,M,N), return the point (R below) where extended
;; lines of PQ and MN intesects.
;;
;;     M        
;;      :       Q
;;       :    -'
;;        R-'
;;      -' :
;;    P'    N
;;
;;   M
;;    : 
;;     :
;;      N      Q 
;;           -'
;;        R-'
;;      -'
;;    P'
"];

function lineCrossPt( P,Q,M,N )=
(
// Ref: Paul Burke tutorial 
//   http://paulbourke.net/geometry/pointlineplane/
// and his code in c :
//   http://paulbourke.net/geometry/pointlineplane/lineline.c
                   
//	P + (Q-P) * ( ((M-P)*(M-N))* ((M-N)*(P-Q)) - ((M-P)*(P-Q))*((M-N)*(M-N)))
// 	 		 / ( ((P-Q)*(P-Q))* ((M-N)*(M-N)) - ((M-N)*(P-Q))*((M-N)*(P-Q)))
                   
	P + (Q-P) * ( dot(M-P,M-N)* dot(M-N,P-Q) - dot(M-P,P-Q)* dot(M-N,M-N) )
 	 		 / ( dot(P-Q,P-Q)* dot(M-N,M-N) - dot(M-N,P-Q)* dot(M-N,P-Q) )
);

module lineCrossPt_test(ops=["mode",13]){ doctest( lineCrossPt, ops=ops ); }

module lineCrossPt_demo_1()
{
	echom("lineCrossPt_demo");
	pqm = randPts(3);
	P = pqm[0];
	Q = pqm[1];
	R = onlinePt( [P,Q], ratio= rands(0.2,1,1)[0] ); 
	M = pqm[2];
	N = onlinePt( [M,R], ratio = rands(1.5,2,1)[0]);
	
	//echo("[P,Q,M,N] = ", [P,Q,M,N]);

	MarkPts([P,Q,M,N,R]);
	Line0( [P,Q], ["r",0.01, "color","red"] );
	Line0( [M,N], ["r",0.01, "color","red"] );

	A = lineCrossPt(P,Q,M,N);
	B = crossPt( [P,Q], [M,N] );
	//echo("R: ", R );
	//echo("A: ", A );
	//echo("B: ", B );
	MarkPts( [A], ["r",0.2, "transp",0.4] );
	MarkPts( [B], ["r",0.3, "colors",["green"], "transp",0.2] );
	LabelPts( [P,Q,M,N,R,A], "PQMNRA" );
}


module lineCrossPt_demo_2_nocontact()
{
	echom("lineCrossPt_demo");
	pqm = randPts(3);
	P = pqm[0];
	Q = pqm[1];
	R = onlinePt( [P,Q], ratio= rands(0.2,1,1)[0] ); 
	M = pqm[2];
	N = onlinePt( [M,R], ratio = rands(0,0.8,1)[0]);
	
	//echo("[P,Q,M,N] = ", [P,Q,M,N]);

	MarkPts([P,Q,M,N,R]);
	Line0( [P,Q], ["r",0.02, "color","green"] );
	Line0( [M,N], ["r",0.02, "color","green"] );

	A = lineCrossPt(P,Q,M,N);
	B = crossPt( [P,Q], [M,N] );
	//echo("R: ", R );
	//echo("A: ", A );
	//echo("B: ", B );
	MarkPts( [A], ["r",0.2, "transp",0.4] );
	MarkPts( [B], ["r",0.3, "colors",["green"], "transp",0.2] );
	LabelPts( [P,Q,M,N,R,A], "PQMNRA" );
}
module lineCrossPt_demo()
{
	lineCrossPt_demo_1();
	lineCrossPt_demo_2_nocontact();
}
//lineCrossPt_demo();


//========================================================
longside=["longside","a,b","number","Math,Geometry,Triangle",
 " Given two numbers (a,b) as the lengths of two short sides of a 
;; right triangle, return the long side c ( c^2= a^2 + b^2 )."
,"shortside"
];
function longside(a,b)= sqrt(pow(a,2)+pow(b,2));
module longside_test( ops=["mode",13] )
{ doctest( longside, [
	[ "3,4", longside(3,4), 5 ]
	], ops
	);
}


lpCrossPt=["lpCrossPt", "pq,tuv", "point", "Geometry",
 " Given a 2-pointer for a line (pq) and a 3-pointer for a plane (tuv),
;; return the point of intercept between pq and tuv. 
;;
    m=dist(pq)

    P   m
    |'-._	  
    |    'Q._  n
    |dp   |  '-._
    |     |dq    '-.
   -+-----+----------X-

    (m+n)/m = dp/(dp-dq) 

    onlinePt( pq, ratio = dp/(dp-dq) )

"];

function lpCrossPt(pq, tuv)=
 let( P=pq[0]
	, Q=pq[1]
	, L= dist(pq)
	, dp = distPtPl(P, tuv)
	, dq = distPtPl(Q, tuv)
	, sameside = sign(dp)==sign(dq)
	, ratio = sameside ?  dp/(dp-dq)
					 :abs(dp)/(abs(dp)+abs(dq))
	)
(
	onlinePt( pq, ratio = ratio)
);


module lpCrossPt_demo()
{
	pq = randPts(2);
	tuv = randPts(3);
	Rod( pq, ["label",true] );
	Plane( squarePts(tuv), ["mode",4] );

	X = lpCrossPt( pq, tuv );
	MarkPts( [X], ["labels","X"] );

}

//lpCrossPt_demo();



//========================================================
parsedoc=["parsedoc","string","string", "String, Doc",
 " Given a string for documentation, return formatted string.
"];

function parsedoc( s
			, prefix ="|"
			, indent =""
			, replace=[]
			, linenum
			)=
 let( s   = split(s, ";;")	)
(
	
  join(
	[for( i=range(s) )
		 str( indent
			, prefix
			, isnum(linenum)?str((linenum+i),". "):"" 
			, replace
			  (
				replace
				( 
				  replace( s[i], "''", "&quot;" )
				, "<", "&lt;"
				)
			  , ">", "&gt;"
			  )
			)
	],"<br/>")									
);

module parsedoc_test( ops=["mode",13] )
{
	doctest( parsedoc, ops=ops );
}

//========================================================
_mdoc=["_mdoc", "mname,s", "n/a", "Doc",
 " Given a module name (mname) and a doc string (s), parse s (using 
;; parsedoc()) and echo to the console.
"];

module _mdoc(mname, s){ 
	echo(_pre(_code(
			str("<br/><b>"
			, mname
			, "()</b><br/><br/>"
			, parsedoc(s)
			)
			,s="color:blue"
		)));
}				  

module _mdoc_test( ops=["mode",13] )
{
	doctest( _mdoc, ops=ops );
}
//	function _doc_dt_getDocLinesString( lines , tags, _i_=0 )=
//	(
//		_i_>len(lines)-1?""
//		:str(
//			 _i_==0? prefix :""
//			, dt_replace(lines[_i_], "''", "&quot;" )  // #2
//				, "<br/>"
//				, prefix 
//			,  _doc_dt_getDocLinesString( lines, tags, _i_=_i_+1 )
//			, _i_==len(lines)-1? str("<br/>", prefix, " <u>Tags:</u> ",tags):""
//		)	
//	
//	); 


//========================================================
mergeA=["mergeA", "a_list_of_arrays"
    ,"array", "Array"
, "merge the arrays into one. This is useful to merge
;; the result of a list comprehension:
;;
;; mergeA( [for (i...) ...] )
"];
function mergeA(arrays,_i_=0)=
//[ for (a = l) for (b = a) b ]; 
(    _i_<len(arrays)
    ? concat( arrays[_i_], mergeA(arrays, _i_+1) )
    :[]
);

module mergeA_test(ops=[])
{
  As = [[0, 2, 1], [0, 3, 2], [0, 4, 3]];
  As2= [[[0,1,2], [3,4,5]], [[6,7,8], [9,10,11]]]; 
  
  doctest(mergeA,
    [
      ["As",  mergeA(As),  [0,2,1,0,3,2,0,4,3]]
    , ["As2", mergeA(As2), [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]]
    ]
  , ops, ["As",As,"As2",As2]
  );
}

//========================================================
midPt=[ "midPt", "PQ", "pt", "Math, Geometry, Point" ,
"
 Given a 2-pointer PQ, return the pt that's in the middle of P and Q
"];

function midPt(PQ)= (PQ[1]-PQ[0])/2 + PQ[0];

module midPt_test(ops)
{ doctest( midPt, 
	[ 
	[  "[ [2,0,4],[1,0,2] ]", midPt([ [2,0,4],[1,0,2]  ]), [1.5,0,3]   ]
	], ops
 );
}
//midPt_test( ["mode",22] );
//doc(midPt);
 


//========================================================
minPt=[ "minPts", "pts", "array", "3D, Point, Math",
"
 Given an array of points (pts), return a pt that
;; has the min x then min y then min z.
"];
 
function minPts(pts)=
(
	minzPts( minyPts( minxPts( pts ) ) )[0]
);

module minPt_test( ops )
{
	pts1= [[2,1,3],[0,2,1] ];
	pts2= [[2,1,3],[0,3,1],[0,2,0], [0,2,1]];
	
	doctest
	(	minPt
		,[
			 ["pts1", minPts( pts1 ), [0,2,1] ]
			,["pts2", minPts( pts2 ), [0,2,0] ]
		 ], ops, ["pts1", pts1, "pts2", pts2]
	);
}
//minPt_test();
//doc(minPt);





//========================================================
minxPts=[ "minxPts", "pts", "array", "Math"	,
"
 Return an array containing the pt(s) that has the min x.
"];
 
function minxPts(pts, _i_=0)=
(
    /* minx = min(getcol(pts,0)); */
	_i_<len(pts)?
	 concat( pts[_i_][0]==min(getcol(pts,0))? [pts[_i_]]:[]
			, minxPts( pts, _i_+1) 
			)
	 :[]
);

module minxPts_test( ops )
{
	pts1= [[2,1,3],[0,2,1] ];
	pts2= [[2,1,3],[0,3,1],[0,2,0]];
	
	doctest
	( minxPts
		,[
			[ "pts1" , minxPts( pts1 ), [[0,2,1]] ]
			,["pts2", minxPts( pts2 ), [[0,3,1],[0,2,0]] ]
		 ],ops, ["pts1", pts1, "pts2", pts2]
	);
}
//minxPts_test( ["mode",22] );
//doc(minxPts);



//========================================================
minyPts=[ "minyPts", "pts", "array", "Math",
"
 Return an array containing the pt(s) that has the min y.
"];
function minyPts(pts, _i_=0)=
(
    /* minx = min(getcol(pts,0)); */
	_i_<len(pts)?
	 concat( pts[_i_][1]==min(getcol(pts,1))? [pts[_i_]]:[]
			, minyPts( pts, _i_+1) 
			)
	 :[]
);

module minyPts_test( ops )
{
	pts1= [[2,1,3],[0,2,1],[3,3,0]];
	pts2= [[2,1,3],[0,3,1],[3,1,0]];
	
	doctest
	( minyPts
		,[
			 ["pts1", minyPts( pts1 ), [[2,1,3]] ]
			,["pts2", minyPts( pts2 ), [[2,1,3],[3,1,0]] ]
		 ],ops, ["pts1", pts1, "pts2", pts2]
	);
}
//minyPts_test( ["mode",22] );
//doc(minyPts);



//========================================================
minzPts=[ "minzPts", "pts", "array", "Math",
"
 Return an array containing the pt(s) that has the min y.
"];

function minzPts(pts, _i_=0)=
(
    /* minx = min(getcol(pts,0)); */
	_i_<len(pts)?
	 concat( pts[_i_][2]==min(getcol(pts,2))? [pts[_i_]]:[]
			, minzPts( pts, _i_+1) 
			)
	 :[]
);

module minzPts_test( ops )
{
	pts1= [[2,1,3],[0,2,1]];
	pts2= [[2,1,3],[0,3,1],[0,2,1]];
	
	doctest
	( minzPts
		,[
			 ["pts1", minzPts( pts1 ), [[0,2,1]] ]
			,["pts2", minzPts( pts2 ), [[0,3,1],[0,2,1]] ]
		 ], ops, ["pts1", pts1, "pts2", pts2]
	);
}
//minzPts_test( [["mode", 22]] );
//doc(minzPts);


//========================================================
mod=["mod", "a,b", "number", "Number, Math, Core",
" Given two number, a,b, return the remaining part of division a/b.
;;
;; Note: OpenScad already has an operator for this, it is the percentage
;; symbol. This is here because this code file might be parsed by python
;; that reads the percentage symbol as a string formatting site and causes
;; errors.
"];

function mod(a,b) = sign(a)*(abs(a) - floor(abs(a)/abs(b))*abs(b) );

module mod_test( ops=["mode",13] )
{
	doctest( mod,
	[
	 ["5,3", mod(5,3), 2]
	,["-5,3", mod(-5,3), -2 ]
	,["5,-3", mod(5,-3), 2]
	,["7,3", mod(7,3),1]
	,["-7,3", mod(-7,3), -1 ]
	,["3,3", mod(3,3), 0 ]
	,["3.8,1", mod(3.8,1), 0.8 ]
	,["38,1", mod(38,1), 0 ]
	], ops
	);
}
//mod_test();



//========================================================
normal= ["normal","pqr","pt","Math, Geometry",
"
 Given a 3-pointer pqr, return the normal (a vector) to the plane
;; formed by pqr.
"];

function normal(pqr, len=undef)=
(
	isnum(len)? onlinePt( [ORIGIN, cross( pqr[0]-pqr[1], pqr[2]-pqr[1] )], len=len)
	:cross( pqr[0]-pqr[1], pqr[2]-pqr[1] )
);

module normal_test( ops )
{
	pqr = [[-1, -2, 2], [1, -4, -1], [2, 3, 3]];
	doctest
	(
		normal, 
		[
			[ pqr , normal(pqr), [-13, 11, -16] ]

		], ops
	);
}

module normal_demo()
{
	//pqr = [[-1, -2, 2], [1, -4, -1], [2, 3, 3]];
	pqr = randPts(3);
	PtGrid( pqr[0] );
	PtGrid( pqr[1] );
	PtGrid( pqr[2] );

	Chain( pqr, ["closed",false]);

	//PtGrid( normal(pqr) );
    
    nm = normal(pqr);
	color("blue")
	Line0( [[0,0,0],nm]);
    
	Plane(pqr, ["mode",4] );

    // We create a line that is perpendicular to the normal and 
    // passing through pqr[0]. This line has to be on the plane [pqr]

    // othopt = pqr[1]'s projection on line [ORIGIN:nm],
    //        = normal's projection on the plane
    othopt = othoPt( [ ORIGIN
                    , pqr[1]
                      , nm ] );
                      
    // Red line: the line between one corner and the othopt                  
    Line0( [othopt, pqr[1]], ["r",0.05, "color","red"] );                   
    
    
    // Yellow line; extension of the normal to reach the plane:
    Line( [nm, othopt], ["r",0.1, "color","yellow", "transp",0.3] );
    
    // If an L-shape shows up, means it's 90-degree:
    Mark90( [pqr[1], othopt, nm],["color","black", "r",0.02] );


	// len = 1
	nm2 = normal(pqr, len=1);
	Line0( [ORIGIN, nm2], ["r",0.2, "transp",0.3] );

	MarkPts( pqr, ["labels","PQR"] );
	MarkPts( [nm,nm2], ["labels",["N","N1"]]);
}
//normal_test();
//normal_demo();
//doc(normal);


//========================================================
normalPt=["normalPt", "pqr,len,reversed", "point", "Geometry, Point",
 " Given a 3-pointer (pqr), optional len and reversed, return a 
;; point representing the normal of pqr."
];
function normalPt(pqr, len=undef, reversed=false)=
(
	len==undef? (reversed?-1:1)*normal(pqr)+pqr[1]
			  :onlinePt( [ pqr[1], (reversed?-1:1)*normal(pqr)+pqr[1]], len= len )
);

module normalPt_test( ops=["mode",13] )
{
	doctest( normalPt,
	[]
	, ops
	);
}
		

module normalPt_demo()
{
	echom("normalPt");
	pqr = randPts(3);
	P = pqr[0];  Q=pqr[1]; R=pqr[2];
	Chain( pqr, ["closed", false] );
	N = normalPt( pqr );
	echo("N",N);
	MarkPts( concat( pqr,[N]) , ["labels","PQRN"] );
	//echo( [Q, N] );
	Mark90( [N,Q,P] );
	Mark90( [N,Q,R] );
	Dim( [N, Q, P] );
	Line0( [Q, N], ["r",0.02,"color","red"] );
	Nr = normalPt( pqr, len=1, reversed=true);
	echo("Nr",Nr);
	Mark90( [Nr,Q,P] );
	Mark90( [Nr,Q,R] );
	Dim( [Nr, Q, P] );
	Line0( [Q, Nr], ["r",0.02,"color","green"] );

}
//normalPt_demo();
//Dim_test( );



//========================================================
normalPts=[[]];
function normalPts(pts, len=1)=
 let(tripts= subarr(pts,  cycle=true)
	)
( 
	[for(pqr=tripts) normalPt(pqr,len=len)]
);


module normalPts_demo_1()
{_mdoc("normalPts_demo_1",
"");

	pts = randOnPlanePts( 4 ); //randPts(3), 4);
	tripts= subarr( pts, cover=[1,1],cycle=true);
	
	norms= [for(pqr=tripts) normalPt(pqr,len=1)-Q(pqr) ];
	

	angles = [for(pqr=tripts) angle(pqr) ];

	up = normalPts(pts, len=1);
	down= normalPts(pts, len=-0.5);

	Chain( pts, ["color","red", "r",0.03, "closed",true] );
	Chain( up, ["color","green","r",0.02,"closed",true]);
	MarkPts( pts, ["labels",true, "r",0.05] );
	
	range_planeIndices= range(pts,cover=[0,1],cycle=true);
	rngp = range_planeIndices;
	echo("rngp",rngp);

	for (i=range(pts)){

		Line( [down[i], up[i]], ["r",0.005, "color","blue"] );
		Plane ( [ up  [ rngp[i][0] ]
				, up  [ rngp[i][1] ]
				, down[ rngp[i][1] ]
				, down[ rngp[i][0] ]
				]);	
	}
	//Dim( [pts[0], larger[0]] );
}

module normalPts_demo_2_arc()
{
	pts2d = arcPts(r=3, a=120, count=9);
	
	pts= pts2d;
	//echo("pts:", pts);
	larger = normalPts(pts, len=0.5);
	smaller= normalPts(pts, len=-0.5);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green","r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01, "closed",true] );
	MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}

}

module normalPts_demo_3_arc()
{
	pts = arcPts(r=3, a=120, count=9);
	//echo("pts:", pts);
	larger = expandPts(pts, dist=0.2);
	smaller= expandPts(pts, dist=-0.2);
	up = normalPts(pts, len=0.2);
	down= normalPts(pts, len=-0.2);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green", "r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01,"closed",true] );
	Chain( up, ["color","green", "r",0.01,"closed",true]);
	Chain( down, ["color","blue", "r",0.01,"closed",true] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(up,i+1, cycle=true), up[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(down,i+1, cycle=true), down[i] ]);
	}

}

module normalPts_demo_4_arc()
{
	count=12;
	pts = arcPts(r=3, a=120, count=count);
	
	larger = expandPts(pts, dist=0.2);
	smaller= expandPts(pts, dist=-0.2);
	up = normalPts(pts, len=0.2);
	down= normalPts(pts, len=-0.2);
//	Chain( pts, ["color","red", "r",0.01, "closed",true] );
//	Chain( larger, ["color","green", "r",0.01,"closed",true]);
//	Chain( smaller, ["color","blue", "r",0.01,"closed",true] );
//	Chain( up, ["color","khaki", "r",0.01,"closed",true]);
//	Chain( down, ["color","purple", "r",0.01,"closed",true] );
	MarkPts( up, ["r",0.05] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );
	//LabelPts( larger, labels=range(larger) );
	//LabelPts( up, labels=repeat([len(smaller)],len(smaller))+ range(smaller));
	//LabelPts( smaller, labels=repeat([len(smaller)*2],len(smaller))+ range(smaller));
	//LabelPts( down, labels=repeat([len(smaller)*3],len(smaller))+ range(smaller));
 
	echo( "len pts:", len(pts));
    allpts = concat( larger, up, smaller, down );
	function getfaces(_I_=0)=
	(	_I_<len(pts)-1?
		concat(  [ [_I_, _I_+1, _I_+len(pts)+1, _I_+len(pts) ]
				, [ _I_+len(pts), _I_+len(pts)+1, _I_+2*len(pts)+1, _I_+2*len(pts)]
				, [ _I_+len(pts)*2, _I_+len(pts)*2+1, _I_+len(pts)*3+1, _I_+len(pts)*3]
				, [ _I_+len(pts)*3, _I_+len(pts)*3+1, _I_+1, _I_] //_I_+len(pts)*4+1, _I_+len(pts)*4]
				]
			  , getfaces(_I_+1)
			):[]
	); 
	faces = concat( [ ]//[0, count, 2*count, 3*count ]]
//				    , [count-1,
				 , getfaces());
	echo("faces", faces);

	polyhedron( points = allpts, faces = faces );

}
module normalPts_demo_5_convexity()
{
	//
	// show normal sign(s) along a path for convexity determination later
	// 

	echom("normalPts_demo_5_convexity");

	
	pts2d = arcPts(r=3, a=120, count=9);
	
	function to3dPts(pts,z=0,_i_=0)=
	( _i_<len(pts)? concat(
		 [[pts[_i_][0], pts[_i_][1], z]]
		, to3dPts( pts, z, _i_+1)
		):[]
	);
	pts = to3dPts(pts2d);
	//echo("pts:", pts);
	larger = expandPts(pts, dist=0.2);
	smaller= expandPts(pts, dist=-0.2);
	up = normalPts(pts, len=0.2);
	down= normalPts(pts, len=-0.2);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green", "r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01,"closed",true] );
	Chain( up, ["color","green", "r",0.01,"closed",true]);
	Chain( down, ["color","blue", "r",0.01,"closed",true] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(up,i+1, cycle=true), up[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(down,i+1, cycle=true), down[i] ]);
	}

}
//normalPts_demo_1();
//normalPts_demo_2_arc();
//normalPts_demo_3_arc();
//normalPts_demo_4_arc();


//========================================================
num=[ "num", "x", "number", "Type, Number",
" Given x, convert x to number. x could be a number, a number string,
; or a string for feet-inches like 3'4'' that will be converted to number
; in feet.
;
; num(''-3.3'') => -3.3
; num(''2'6'''') => 2.5  // 2.5 feet 
"];

function num(x,_i_=0)=
(
	isnum(x)
	 ? x
	 : isstr(x)
	   ?(
		 x==""
		 ? undef
		 : endwith(x,"'")||endwith(x,"\"")
		 ? ftin2in(x)/12
		 : _i_<len(x)
		   ?(
			index(x,".")==0
		  	? num( str("0",x), _i_=0)	
		 	: _i_==0 && x[0]=="-"
		   		?-num(x,_i_=1)
		   		: (_i_== index(x,".") //x[_i_]=="."
				  ? 0
					// 345.6      3456
					// 210 -1     3210  <== pow(10, ?)
					// 01234      0123  <== _i_
					//    3         -1  <== index(x,".")
					
				  : pow(10, (index(x,".")<0
							?(len(x)-_i_-1)
							: ( index(x,".")-_i_ )
							  -( index(x,".")>_i_?1:0)	
						   )
					  )
				    * hash( ["0",0, "1",1, "2",2
				 		,"3",3, "4",4, "5",5
						,"6",6, "7",7, "8",8
						,"9",9], x[_i_] )

				  )+ num(x, _i_+1)
		    )
		   :0 // out of bound
		)
		:undef // everything other than string and num
);
module num_test( ops )
{
	doctest
	( 
		num, [
	   ["-3.8",num(-3.8),-3.8]
	  , ["''3''",num("3"),3]
	  , ["''340''",num("340"),340]
	  , ["''3040''",num("3040"),3040]
	  , ["''3.4''",num("3.4"),3.4]
	  , ["''30.04''",num("30.04"),30.04]
	  , ["''340''",num("340"),340]
	  , ["''3450''",num("3450"),3450]
	  , ["''.3''",num(".3"),0.3]
	  , ["''-.3''",num("-.3"),-0.3]
	  , ["''0.3''",num("0.3"),0.3]
	  , ["'-'0.3''",num("-0.3"),-0.3]
	  , ["''-15''",num("-15"),-15]
	  , ["''-3.35''",num("-3.35"),-3.35]
	  , ["''789.987''",num("789.987"),789.987]
	  , ""	
	  , "// Convert to feet:"
	  , ["''2'6''''", num("2'6\""), 2.5]
	  , ["''3''''", num("3\""), 0.25]
	  , ["''3'\"", num("3'"), 3]
	  , ""	
	  , ["[3,4]",num([3,4]),undef]
	  , ["''x''",num("x"),undef]
	  , ["''34-5''",num("34-5"),undef]
	  , ["''34x56''",num("34x56"),undef]
	  , ["''''",num(""),undef]
	 ],ops	
	);
}

function _getdeci(n)= index(str(n),".")<0?0:len(split(str(n),".")[1]);

//========================================================
numstr=["numstr","n,d=undef,dmax=undef, convert=undef", "string", "Number, String",
" Convert a number to string. d = decimal:
; 
; numstr(3.8, d=3) = 3.80   ... (a)
; numstr(3.8765, d=3) = 3.877 ... (b)
; 
; If we want to limit decimal points (b) but don't want to add
; extra 0 (a), then we can use dmax:
;
; numstr(3.8, dmax=3) = 3.8
; numstr(3.8765, dmax=3) = 3.877
"];
function numstr(n,d=undef, dmax=undef, conv=undef)=
(
	conv=="ft:ftin"
	?(n<1
	   ? str( numstr(n*12, d, dmax) , "\"")
	   : mod(n,1)==0
		? str(n, "'")
		: str( floor(n), "'"
			, numstr( mod(n,1)*12,d,dmax), "\"")	
	 )
	:conv=="in:ftin"
	? numstr( ( isstr(n)&&endwith(n,"\"")?num(shift(n,"\"","")):n )
			 /12, d=d, dmax=dmax, conv="ft:ftin")
	:conv
	? numstr( uconv(n, from=split(conv,":")[0], to=split(conv,":")[1] )
			, d=d, dmax=dmax )
//	: conv=="in:cm"
//	? numstr( n*2.54, d=d, dmax=dmax)
//	: conv=="ft:cm"
//	? numstr( n*12*2.54, d=d, dmax=dmax)

//	: conv=="ftin:cm" && isstr(n)
//	? numstr( endwith(n,"'")
//			 ? num(shift(n)*12 
//			index(n, "'")>0? // 3'2", 3'
//			( num(split(n,"'")[0])*12
//			 )+(len(split(n,"'"))>1?
//			  split(n,"'")[0]*12:0
//			 ) n*12*2.54, d=d
//			)

	// Finish conv part above. Start processing decimal 

	: isint(d)&&d>-1
	  ?(
		isint(dmax)  // when both defined
		? _getdeci(n)== min(d,dmax)
           ? str(n)
		  : _getdeci(n)< min(d,dmax)
		    ? str( n, repeat("0", min(d,dmax)-_getdeci(n)))
			: str( round(n*pow(10,min(d,dmax)))
				        /pow(10,min(d,dmax))
				)	
		: // d given, dmax not
		  _getdeci(n)== d
           ? str(n)
		  : _getdeci(n)< d
		    ? str( n, isint(n)?".":"", repeat("0", d-_getdeci(n)))
			: str( round(n*pow(10, d))
				        /pow(10,d)
				)
	   )
	  :(
		isint(dmax)
		?_getdeci(n)<= dmax
           ? str(n)
		  : str( round(n*pow(10, dmax))
				        /pow(10, dmax)
			  )			 
		:str(n)
	   )
);

module numstr_test( ops=["mode",13])
{
	doctest( numstr,
	[
		"// Integer: "
		,["0", numstr(0), "0"]
		,["5", numstr(5), "5"]
		,["5,dmax=2", numstr(5,dmax=2), "5"]
		,["5,d=2", numstr(5,d=2), "5.00"]
		,["38,d=0", numstr(38, d=0), "38"]
		,["38,d=2", numstr(38, d=2), "38.00"]
		,"// Float. Note the difference between d and dmax: "
		,["3.80", numstr(3.80), "3.8"]
		,["38.2,dmax=3", numstr(38.2, dmax=3), "38.2"]
		,["38.2,d=3, dmax=2", numstr(38.2, d=3, dmax=2), "38.20"]
		,["38.2,d=3, dmax=4", numstr(38.2, d=3, dmax=4), "38.200"]
		,["38.2,d=5, dmax=4", numstr(38.2, d=5, dmax=4), "38.2000"]
		,["3.839, d=0", numstr(3.839,d=0), "4"]
		,["3.839, d=1", numstr(3.839,d=1), "3.8"]
		,["3.839, d=2", numstr(3.839,d=2), "3.84"]
		,["3.839, d=3", numstr(3.839,d=3), "3.839"]
		,["3.839, d=4", numstr(3.839,d=4), "3.8390"]
		,["3.839, d=1, dmax=2", numstr(3.839,d=1, dmax=2), "3.8"]
		,["3.839, d=2, dmax=2", numstr(3.839,d=2, dmax=2), "3.84"]
		,["3.839, d=3, dmax=2", numstr(3.839,d=3, dmax=2), "3.84"]
		,["3.839, d=4, dmax=2", numstr(3.839,d=4, dmax=2), "3.84"]		
		,["-3.839, d=1", numstr(-3.839,d=1), "-3.8"]
		,["-3.839, d=2", numstr(-3.839,d=2), "-3.84"]
		,["-3.839, d=4", numstr(-3.839,d=4), "-3.8390"]

		,"// conversion: "
		,"// conv = ''ft:ftin''"

		,["0.5,conv=''ft:ftin''", numstr(0.5,conv="ft:ftin"), "6\""]
		,["3,conv=''ft:ftin''", numstr(3,conv="ft:ftin"), "3'"]
		,["3.5,conv=''ft:ftin''", numstr(3.5,conv="ft:ftin"), "3'6\""]
		,["3.5,d=2,conv=''ft:ftin''", numstr(3.5,d=2,conv="ft:ftin"), "3'6.00\""]
		,["3.343,conv=''ft:ftin''", numstr(3.343,conv="ft:ftin"), "3'4.116\""]
		,["3.343,d=2, conv=''ft:ftin''", numstr(3.343,d=2,conv="ft:ftin"), "3'4.12\""]
		,"// conv = ''in:ftin''"
		,["96,conv=''in:ftin''", numstr(96,conv="in:ftin"), "8'"]
		,["32,conv=''in:ftin''", numstr(32,conv="in:ftin"), "2'8\""]
		,"// d is ignored if negative:"
		,["38,d=-1", numstr(38, d=-1), "38"]
		
	], ops );
}
//numstr_test();


//========================================================
onlinePt=[ "onlinePt", "PQ,len=false,ratio=0.5", "pt", "Point",
"
 Given a 2-pointer (PQ), return a point (pt) that lies along the
;; line between P and Q, with its distance away from P defined by 
;; *dist* or by *ratio* if dist is not given. When ratio=1, return Q.
;; Both *dist* and *ratio* can be negative.
"];

function onlinePt(pq, len=false, ratio=0.5)=
(
	[ (isnum(len)?len/dist(pq):ratio)*distx(pq)+pq[0].x
	, (isnum(len)?len/dist(pq):ratio)*disty(pq)+pq[0].y 
	, (isnum(len)?len/dist(pq):ratio)*distz(pq)+pq[0].z ] 
	
);

module onlinePt_test( ops )
{
	pq= randPts(2);
	d1pt= onlinePt( pq, len=1 );
	r3pt= onlinePt( pq, ratio=0.3);
	distpq = dist(pq);
 
	doctest( onlinePt, 
	[
	   [ "pq,0", onlinePt(pq,0), pq[0] ]
	 , [ "pq,ratio=1", onlinePt(pq,ratio=1), pq[1] ]

	 , [ "pq,len=1", norm(pq[0]- onlinePt(pq,len=1))
		, 1, ["funcwrap", "norm({_}-pq[0])"]]
	 , [ "pq,len=-2", norm(pq[1]- onlinePt(pq,len=-2))
		, distpq+2, ["funcwrap", "norm({_}-pq[1])"]]

	 , [ "pq,ratio=0.3", norm(pq[0]- onlinePt(pq,ratio=0.3))
		, distpq*0.3, ["funcwrap", "norm({_}-pq[0])"]]
	 , [ "pq,ratio=-0.3", norm(pq[1]- onlinePt(pq,ratio=-0.3))
		, distpq*1.3, ["funcwrap", "norm({_}-pq[1])"]]	 
	 ]
	, ops, ["pq", pq, "distpq", distpq, "d1pt", d1pt, "r3pt", r3pt]
            );
}

module onlinePt_demo()
{
	pr = randPts(2);
	q = onlinePt( pr, len=-0.8 );
	Line(pr);
	MarkPts( [pr[0],q,pr[1]], ["grid",true,"echopts",true] );

	eg = randPts(2); 
	f = onlinePt( eg, ratio=0.33 );
	Line(eg);
	MarkPts( [eg[0],f,eg[1]], ["grid",true,"echopts",true] );

}
//onlinePt_test( ["mode",22] );
//onlinePt_demo();
//doc(onlinePt);





//========================================================
onlinePts=["onlinePts","PQ,lens=false,ratios=false","path","Point",
"
 Given a 2-pointer (PQ) and a numeric array assigned to either lens
;; or ratios, return an array containing pts along the PQ line with 
;; distances (from P to each pt) defined in the given array. Refer to
;; onlinePt().
;; This is good for getting multiple points on pq line. For example, to
;; get 3 points that divide pq line evenly into 4 parts:
;;
;;    onlinePts( pq, ratios=[ 0.25, 0.5, 0.75 ] );
"];

function onlinePts(pq, lens=false, ratios=false)=
(
	ratios==false?
	( len(lens)==0?[]
	  :	concat( [onlinePt( pq, len=lens[0])]
		  , onlinePts( pq, lens=shrink(lens) )
		  )
	):
	( len(ratios)==0?[]
	  :	concat( [onlinePt( pq, ratio=ratios[0])]
		  , onlinePts( pq, ratios= shrink(ratios) )
		  )
	)
);

module onlinePts_test( ops )
{
	pq= [[0,1,0],[10,1,0]]; //randPts(2);
	lens= [-1,2];
	ratios= [0.2, 0.4, 0.6];
	distpq = dist(pq);
 
	doctest( onlinePts, 
	[
	   [ "pq,[-1,2]", onlinePts(pq,lens), [[-1, 1, 0], [2, 1, 0]] ]
	,  [ "pq,ratios=[0.2, 0.4, 0.6]", onlinePts(pq,ratios=ratios), [[2, 1, 0], [4, 1, 0], [6, 1, 0]]]
	 ]
	, ops, ["pq", pq] 
            );
}

/*
module onlinePts_test( ops )
{
	pq = [ORIGIN, [5,5,1]];
	lens = [0.5, 1, 2, 2.5];

	doctest( onlinePts
	,[
		[ [pq ,lens]
		, onlinePts( pq, lens= lens )
		,  [[0.35007, 0.35007, 0.070014], [0.70014, 0.70014, 0.140028], [1.40028, 1.40028, 0.280056], [1.75035, 1.75035, 0.35007]]
		]
	], update(ops, ["asstr",true])
	);
}
*/
module onlinePts_demo_1()
{
	pq = randPts(2);
	lens = [0.5, 1, 2, 2.5];
	pts = onlinePts( pq, lens = lens );
	
	Line0(pq, ["r",0.05, "color", "red", "transp",0.5] );
	MarkPts( pts );

	ratios = accumulate(repeat([0.2],5));
	pq2 = randPts(2);
	pts2 = onlinePts( pq2, ratios=ratios );
	//echo("in onlinePts, ratios = ", ratios);
	//echo("in onlinePts, pts2 = ", pts2);
	
	Line0(pq2, ["r",0.05, "color", "green", "transp",0.5] );
	MarkPts( pts2 );
}

module onlinePts_demo_2_evenly_divide()
{
	module evenDiv(count, color="red")
	{
		ratios = accumulate(repeat([1/count],count));
		pq2 = randPts(2);
		pts2 = onlinePts( pq2, ratios=ratios );	
		Line0(pq2, ["r",0.05, "color", color, "transp",0.5] );
		MarkPts( pts2 );
	}

	
	evenDiv( 4 );
	evenDiv( 6, "green");
}
module onlinePts_demo()
{
	//onlinePts_demo_1();
	onlinePts_demo_2_evenly_divide();
}

//onlinePts_test( ["mode",22] );
//onlinePts_demo();
//doc( onlinePts );




//========================================================
othoPt= [ "othoPt", "pqr", "pt", "Point",
"
 Given a 3-pointer (pqr), return the coordinate of the point D
;; such that line QD is at 90 degree with line PR.
;;
;;         Q
;;         :.
;;        /: :
;;       / :  :
;;      /  :   :
;;     /   :_   :
;;    /____:_l___:
;;   P     D      R
"];

function othoPt( pqr )= 
(
/*  Return [x,y] of D
           Q          ___
           ^\          :
          /: \         :   
       a / :  \_  b    : h
        /  :    \_     :
       /   :-+    \    :    
     P-----D-!------R ---

	|------ c ------|  
        
	a^2 = h^2+ PD^2
	b^2 = h^2+ DR^2  
	a^2- PD^2 = b^2-(c-PD)^2
			  = b^2-( c^2- 2cPD+PD^2 )
			  = b^2- c^2 + 2cPD -PD^2
	a^2 = b^2-c^2+ 2cPD
	PD = (a^2-b^2+c^2)/2c
	ratio = PD/c = (a^2-b^2+c^2)/2c^2    
*/
	onlinePt( [pqr[0],pqr[2]]
	        , ratio=( d01pow2(pqr) - d12pow2(pqr) + d02pow2(pqr))/2/d02pow2(pqr) 
	)
);
module othoPt_test( ops )
{
	pqr= randPts(3);
	opt= othoPt( pqr );
	doctest( othoPt,
	[
		[ "pqr", is90([ pqr[0], opt],[opt, pqr[1]]), true,
			["funcwrap", "is90([ pqr[0], {_}], [othoPt(pqr),  pqr[1]])"]
		]
		,[ "pqr", is90([ pqr[0], opt, pqr[1]]), true,
			["funcwrap", "is90([ pqr[0], {_},  pqr[1]])"]
		]
		,[ "pqr", angle([ pqr[0], opt, pqr[1]]), 90,
			["funcwrap", "angle([ pqr[0], {_}, pqr[1]])"]
		]
	], ops, ["pqr",pqr]
	);
}
//is90_test();

module othoPt_demo(){
    ops = [["r",0.02],["color","blue"]];

	//pqr=[ [0,-1,0], [1,2,3],[4,0,-1]];
	pqr=randPts(3);
	otho = othoPt(pqr);
	
	Chain( pqr, ["closed",true] );
	Line0( [pqr[1],othoPt( pqr)], ops);
	MarkPts( pqr);
	Mark90( [ norm(pqr[0]-pqr[1])>norm(pqr[1]-pqr[2])?pqr[0]:pqr[2], otho, pqr[1] ] );
	//echo("pqr= ", pqr);
	//echo("is90 pqr: ", is90([ pqr[0], pqr[1]], [ pqr[1], othoPt(pqr)] ) );
	
	//pqr2=[ [3,1,-1], [1,0,2],[-4,0,1]];
	pqr2= randPts(3);
	otho2 = othoPt(pqr2);
	//echo("pqr2= ", pqr2);
	MarkPts( pqr2 );
	Plane( pqr2, ["mode",3] ) ;//Chain( pqr2, [["closed",true]]);
	Line0( [pqr2[1],othoPt( pqr2)], ops);
	Mark90( [ norm(pqr2[0]-pqr2[1])>norm(pqr2[1]-pqr2[2])?pqr2[0]:pqr2[2], otho2, pqr2[1] ] );
	
	//echo("is90 pqr2: ", is90([ pqr2[0], pqr2[1]], [ pqr2[1], othoPt(pqr2)] ) );
	
}
//othoPt_demo();
//othoPt_test( ["mode",22] );
//doc(othoPt);
//




//========================================================
permute=["permute","arr","array","Array,Math",
 " Given an array, return its permutation."
];

function permute(arr, _I_=0)=
(
	len(arr)==2
	? [arr,[arr[1],arr[0]]]
	:len(arr)==3
	? [arr, p021(arr), p102(arr), p120(arr), p201(arr), p210(arr) ]  
	:_I_<len(arr)
	? concat(
			prependEach( permute( del(arr,_I_, byindex=true),_I_+1)
						, arr[_I_]
						)
			, permute(arr,_I_+1) )
	: []

);

module permute_test( ops=["mode",13] )
{
	doctest( permute
	,[
	  ["[2,4,6]", permute([2,4,6])
		, [[2,4,6],[2,6,4],[4,2,6],[4,6,2],[6,2,4],[6,4,2]]
	  ]
	 , ["[1,2,3,4]", permute([1,2,3,4]),
		[ [1, 2, 3, 4], [1, 2, 4, 3], [1, 3, 2, 4]
		, [1, 3, 4, 2], [1, 4, 2, 3], [1, 4, 3, 2]
		, [2, 1, 3, 4], [2, 1, 4, 3], [2, 3, 1, 4]
		, [2, 3, 4, 1], [2, 4, 1, 3], [2, 4, 3, 1]
		, [3, 1, 2, 4], [3, 1, 4, 2], [3, 2, 1, 4]
		, [3, 2, 4, 1], [3, 4, 1, 2], [3, 4, 2, 1]
		, [4, 1, 2, 3], [4, 1, 3, 2], [4, 2, 1, 3]
		, [4, 2, 3, 1], [4, 3, 1, 2], [4, 3, 2, 1]
	    ] 
	   ]
	,  ["range(3)", permute(range(3)),[[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]]]
	], ops
	);
}
//permu_test();

//========================================================
pick=["pick", "o,i", "pair", "Array",  
"
 Given an array or a string (o) and index (i), return a two-member array :
;;
;; [ o[i], the rest of o ] // when o is array
;; [ o[i], [ pre_i string, post_i_string] ] //when o is a string
;;
;; Note that index could be negative.
"];
function pick(o,i) = 
(
  	concat( [get(o,i)]
		  , isarr(o)
			? [ del(o,i, byindex=true) ] 
			: [[ slice(o,0,i), slice(o,i+1) ]]
		  )
  
);

module pick_test(ops)
{
	arr = range(1,5);
	arr2= [ [-1,-2], [2,3],[5,7], [9,10]] ;
	
	doctest( pick
	,[
		"//Array:"
		,["arr,2", pick(arr, 2), [ 3, [1,2,4] ] ]
		,["arr,-3", pick(arr,-3), [ 2, [1,3,4] ] ]
		,["arr2,2", pick(arr2,2), [ [5,7], [ [-1,-2], [2,3], [9,10]] ] ]
		,"//String:"
		,["''abcdef'',3", pick("abcdef",3), ["d", ["abc", "ef"]] ]
		,["''this is'',2", pick("this is",2), [ "i", ["th", "s is"] ] ]
		,"//Note:"
		,str("split(''this is'',''i'') = ", split("this is","i") )
	],ops, ["arr", arr, "arr2", arr2]
	);
}
//pick_test( ["mode",13] );






//========================================================
planecoefs=["planecoefs", "pqr, has_k=true", "arr", "Geometry, 3D, Math" ,
"
 Given 3-pointer pqr, return [a,b,c,k] for the plane ax+by+cz+k=0.
"];

function planecoefs(pqr, has_k=true)=
(
	/*
	https://answers.yahoo.com/question/index?qid=20110121141727AAncAu4

	Using normal vector <5, 6, -3> and point (2, 4, -3), we get equation of plane: 
	5 (x-2) + 6 (y-4) - 3 (z+3) = 0 
	5x - 10 + 6y - 24 - 3z - 9 = 0 
	5x + 6y - 3z = 43

	normal = [a,b,c]
	k = -(ax+by+c)

	*/	
	concat( normal(pqr) 
		  , has_k?[-( normal(pqr)[0]* pqr[0][0]
			 + normal(pqr)[1]* pqr[0][1]
			 + normal(pqr)[2]* pqr[0][2]
             )]:[]			 
		)	
);


module planecoefs_demo()
{
	// How do we know if the result of planecoefs are correct ?
	// 1. get random 3 points (randPts(3))
	// 2. Calc planecoefs
	// 3. Calc otho pt 
	// 4. Chk planecoefs using otho pt

	pqr = randPts(3);
	Plane(pqr, ["mode",4] );
	MarkPts( pqr );
	pcs = planecoefs(pqr); // =[ a,b,c,k ]
	a= pcs[0];
	b= pcs[1];
	c= pcs[2];
	k= pcs[3];
	
	otho = othoPt( pqr );
	MarkPts( [otho ], ["colors",["purple"], "transp",0.4] );

	isOnPlane= a*otho.x + b*otho.y + c*otho.z+k <= ZERO;

	echo("isOnPlane = ", isOnPlane);

}
module planecoefs_test(ops)
{
	pqr = randPts(3);
	//pcs = planecoefs(pqr); // =[ a,b,c,k ]
	//a= pcs[0];
	//b= pcs[1];
	//c= pcs[2];
	//k= pcs[3];
	othopt = othoPt( pqr );
	//abpt= angleBisectPt( pqr );
	//mcpt= cornerPt( pqr );
	
	doctest( planecoefs, 
	[
		[ "pqr", isOnPlane( planecoefs(pqr),othopt ), true 
		, ["funcwrap","isOnPlane( othopt, planecoefs= {_} )"]
		]
		,"// Refer to isOnPlane_test() for more tests."

	], ops, ["pqr", pqr, "othopt", othopt] );



}
//planecoefs_test();
//planecoefs_demo();
//doc(planecoefs);





//========================================================
projPt=[["projPt", "pt,pqr", "point", "Geometry, Point"],
"
 Given a point (pt) and a 3-pointer (pqr), return the projection of pt\\
 onto the pqr plane.\\
"];

function projPt( pt, pqr )=
(
	/*
http://stackoverflow.com/questions/8942950/how-do-i-find-the-orthogonal-projection-of-a-point-onto-a-plane

		The projection of a point q = (x, y, z) onto a plane 
given by a point p = (a, b, c) and a normal n = (d, e, f) is

q_proj = q - dot(q - p, n) * n

	n = (normal(pqr)/norm(normal(pqr)))
	*/
	pt - dot(pt-pqr[1], (normal(pqr)/norm(normal(pqr))))*(normal(pqr)/norm(normal(pqr))) 
);

module projPt_demo( )
{
	echom("projPt");

	pqrs = randPts(4 );
	echo( pqrs );
	P= pqrs[0];  Q=pqrs[1];  R=pqrs[2];  S=pqrs[3];
	pqr = [P,Q,R];

	J = projPt( S, pqr);
	echo("J: ", J);
	Plane( pqr, ["mode",3] );

	C= incenterPt(pqr);

	MarkPts( pqrs );
	MarkPts( [J,C] );
	LabelPts( [P,Q,R,S,J,C], "PQRSJC" );
	
	Chain( [C,J,S], ["closed",false] );
	Mark90( [C,J,S] );

	echo( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;Check if the distance = distPtPl(...): ");
	echo( "norm( J - S )  = ", norm(J-S) );
	echo( "distPtPl(S,pqr)= ", distPtPl( S, pqr ) );
}

//projPt_demo();



//========================================================
quadrant=["quadrant", "pqr,s", "int", "Integer, Math, Geometry",
 " Given a 3-pointer (pqr) and a point, return the quadrant of S on the 
;; yz plane in a coordinate system where P is on the positive X and Q 
;; is the origin. 
;;
;;       
;;       6
;;  +----N-----+
;;  : 2  |  1  :
;;  :    |     :
;; 7-----P-----R- 5
;;  :    :     :
;;  : 3  :  4  :
;;  +----:-----+ 
;;       8                  R
;;                        /|  
;;                       / |
;;                      /  |
;;                     /   |
;;                    /    |
;;        S.---------/--T  |
;;        |:        /   |  |   _.'
;;        | :      /    |  :.-'
;;        U.-:----/-----+-'
;;          '.:  /  _.-'
;;            ':/.-'
;;   N---------Q---------
;;       _.-' 
;;    P-'
;;
;; quadrant(pqr,S) => 2
;;
"];

function quadrant(pqr,s)=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, N = N(pqr)
	, dST = distPtPl( s, pqr )
	, dSU = distPtPl( s, [N,Q,P] )
	, signs = [ -sign( dST ), sign( dSU )]
	)
(
	 hash( [[1,1],1
			,[-1,1],2
			,[-1,-1],3
			,[1,-1],4
			,[0,0],0
			,[0,1], 6
			,[-1,0], 7
			,[0,-1], 8
			,[1,0],  5
			], signs )
);
 
module quadrant_test( ops=["mode",13] )
{
	doctest( quadrant,
	ops=ops
	);
}

module quadrant_demo()
{

	pqr = randPts(3); S=randPt();
	P=P(pqr); Q=Q(pqr); R=R(pqr);
	N=N(pqr); Z=N( [N,Q,P] );


	T = projPt( S, pqr );
	U = projPt( S, [N,Q,P] );

	Chain( [Q,T,S ], ["r",0.02] ); Mark90( [Q,T,S] );
	Chain( [Q,U,S ], ["r",0.02] ); Mark90( [Q,U,S] );
	Line0( [S,Q] );

	Chain( pqr );
	MarkPts( [P,Q,R,N,S,Z,T,U], ["labels", "PQRNSZTU"] );	

	color(false, 0.2){

	Plane( pqr, ["mode",4] );
	Plane( [P,Q,N], ["mode",4] );
	Plane( [P,Q,N], ["mode",4] );
	Plane( [P,Q,N], ["mode",4] );

	}

	echo(quadrant(pqr,S)) ;
}

//quadrant_demo();


//========================================================
rand=["rand", "m=1, n=false", "Any", "Random, Number, Array, String",
" Given a number (m), return a random number between 0~m.
;; If another number (n) is given, return a random number between m~n.
;; If m is an array or string, return one of its member randomly.
"
];
function rand(m=1, n=false)=
(
	isarr( m )||isstr(m) ? m[ floor(rand(len(m))) ]
	: (isnum(n)
		? rands( min(m,n), max(m,n), 1)
		: rands( 0, m, 1)
	  )[0]
);

module rand_test( ops=["mode",13] )
{
	arr=["a","b","c"];
	doctest( rand,
	[
		"// rand(a)"
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,"//rand(a,b)"
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,"//rand(arr)"
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,"// rand(len(arr))=> float"
		,"// rand(range(arr))=> integer = floor(rand(len(arr)))"
		,str(">> rand(range(arr)):", rand(range(arr)))
		,str(">> rand(range(arr)):", rand(range(arr)))
		,str(">> rand(range(arr)):", rand(range(arr)))

	], ops, ["arr",arr]
	);
}
//module 


//========================================================
randc=["randc", "r=[0,1],g=[0,1],b=[0,1]", "arr", "Core" ,
"
 Given r,g,b (all have default [0,1], which is the max range),
;; return an array with random values for color. Examples:
;;
;; >> color( randc() )  sphere(r=1);
;; >> darker = [0, .4];
;; >> color( randc( r=darker, g=darker ) )  sphere(r=1);
"];
 
function randc(r=[0,1],g=[0,1],b=[0,1])=
(
	[ rands(r[0],r[1],1)[0]
	, rands(g[0],g[1],1)[0]
	, rands(b[0],b[1],1)[0] ]	
);
module randc_test( ops ){ doctest(randc, [], ops=ops);}
module randc_demo()
{
	light = [0.8,1];
	dark  = [0,0.4];
	translate(randPt()) color(randc(r=dark,g=dark,b=dark)) sphere(r=0.5);
	translate(randPt()) color(randc()) sphere(r=1);
	translate(randPt()) color(randc(r=light, g=light,b=light)) sphere(r=1.5);
}
//randc_test( ["mode",22] );
//randc_demo();
//doc(randc);





//========================================================
randConvexPts=[[]];
/*
convex polygons – those with the property that 
given any two points inside the polygon, the entire line segment connecting those two 
points must lie inside the polygon. Such polygons are the intersection of half-planes, 
which means that if you extend the boundary edges into infinite lines the polygon will 
always lie completely on one side of each edge line. If a point is on the same side of each 
edge as the polygon, then it must lie inside the polygon.

http://www.cs.oberlin.edu/~bob/cs357.08/VectorGeometry/VectorGeometry.pdf
*/
function randOnPlaneConvexPts( count=4, pqr=randPts(3)  )=
(
3
 // THIS IS HARD	

);




//========================================================
randPt=["randPt", "r=false,x,y,z", "pt", "Geometry, Point, 3D",  
"
 Given a common range (r, default=false) or ranges x,y,z (each
;; default to [-3,3]), return a random pt that is in the range
;; defined by x,y,z, or by [-r,r] if r is given. Refer torandPts
;; that generates multiple random points.
"];

function randPt(r=false, x=[-3,3],y=[-3,3],z=[-3,3])=
(
 randPts(1,r=r,x=x,y=y,z=z)[0]
);

module randPt_test(ops){ doctest( randPt, ops=ops ); }

module randPt_demo()
{
	MarkPts( [randPt(1)
			,randPt(3)
			,randPt(x=[4,6],y=[1,3],z=[2,4])
			]
			, ["grid",true] );
}
//randPt_test( ["mode", 22] );
//randPt_demo();
//doc(randPt);





//========================================================
randPts=["randPts", "count,r,x,y,z", "pts", "Geometry, Point, 3D",
"
 Given a count of pts and a common range (r, default=false) or ranges
;; x,y,z (each default to [-3,3]), return a random pts that are in the
;; range defined by x,y,z, or by [-r,r] if r is given.
"];
/*
	,[ "Return an array of random points (len=count, default= 3). "
	 , "The x,y,z, each default to [-3,3], define the range. The 2nd "
	 , "argument, r, (default=false), replaces x,y,z settings with "
	 , "[-i,i] if set to an integer i. Refer to randPt(r,x,y,z) for "
	 , "a single random pt."]
	];
*/
function randPts(count=3, r=false, x=[-3,3],y=[-3,3],z=[-3,3])=
(
  count==0? []
	:concat( [[ x==0?0:(rands(isnum(r)?(-r):x[0],isnum(r)?r:x[1],1)[0])
 			 , y==0?0:(rands(isnum(r)?(-r):y[0],isnum(r)?r:y[1],1)[0])
 			 , z==0?0:(rands(isnum(r)?(-r):z[0],isnum(r)?r:z[1],1)[0])
 			 ]], randPts(count-1, r=r, x=x,y=y,z=z)
		  )
);

module randPts_test(ops){ doctest( randPts, ops=ops ); }

module randPts_demo()
{
	pts = randPts(3);
	for (i=[0:len(pts)-1]){ echo( pts[i] ); 
	}
	MarkPts( pts, ["grid",true]);
	
}
//randPts_demo();
//randPts_test();
//doc(randPts);




//========================================================
//randCirclingPts=[[]];
//function randCirclingPts(count=4, r=undef)
//(
//3
//
//);

//========================================================
randInPlanePt=[[]];
function randInPlanePt(pqr)=
(
	[onlinePt( 
			[pqr[0], onlinePt([pqr[1],pqr[2]], ratio=rand())]
		   , ratio=rand()
		)
	,onlinePt( 
			[pqr[1], onlinePt([pqr[2],pqr[0]], ratio=rand())]
		   , ratio=rand()
		)
	,onlinePt( 
			[pqr[2], onlinePt([pqr[0],pqr[1]], ratio=rand())]
		   , ratio=rand()
		)
	][ floor(rand(3))]
);

module randInPlanePt_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	S = randInPlanePt(pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	LabelPts( concat(pqr,[S]) , "PQRS");
	MarkPts( [P,Q,R,S] , ["grid",false]);	
	
}
//randInPlanePt_demo();



//========================================================
randInPlanePts=[[]];
function randInPlanePts(pqr, count=2)=
(
   count>0? concat( [randInPlanePt( pqr )]	
                  , randInPlanePts( pqr, count-1)
                  ) :[]
);

module randInPlanePts_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randInPlanePts(pqr, 4); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	LabelPts( ps );
	MarkPts( concat( pqr, ps));	
}
module randInPlanePts_demo_2()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randInPlanePts(pqr, 100); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps) { MarkPts([p], ["r",0.08]); }
}

//randInPlanePts_demo();
//randInPlanePts_demo_2();


////========================================================
//randOneFacePts=[[]];
//function randOneFacePts( count=5, pqr = randPts(3) )=
//(
//	// average angle = 360 / 5
//3	
//	
//
//
//);

//========================================================
randOnPlanePt=[[]];
//
// optional r : distance from pqr's incenterPt. Can be: a number, an array [a,b] for range
// optional a : random range of aXQP (X = returned Pt). Default 360. Can be [a1,a2] for range]  
//
function randOnPlanePt(pqr=randPts(3), r=undef, a=360)=
(
//  onlinePt( [pqr[floor(rand(3))], randInPlanePt(pqr)], ratio= rand(3) )
   anglePt( pqr
//		 [  pqr[0], incenterPt(pqr)
//		   ,pqr[1]
//		   ]
		  , a = isnum(a)?(rand(1)*a)
					: rands( a[0], a[1],1)[0]
			,r = r==undef? max( d01(pqr), d02(pqr), d12(pqr) ) * rand(1)
				:isarr(r)? rands( r[0], r[1],1)[0]
				:r
		)	
);

module randOnPlanePt_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	S = randOnPlanePt(pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	pqrs = concat(pqr,[S]);
	LabelPts( pqrs , "PQRS");
	MarkPts( pqrs , ["grid",false]);	
	Chain( pqrs, ["r", 0.08, "color", "red", "transp",0.1]);
	
}
//randOnPlanePt_demo();







//========================================================
randOnPlanePts=[[]];
function randOnPlanePts(count=2, pqr=randPts(3), r=undef, a=360 )=
(
   count>0? concat( [randOnPlanePt( r=r, pqr, a=a )]	
                  , randOnPlanePts( count-1, pqr, r=r,a=a )
                  ) :[]
);

module randOnPlanePts_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randOnPlanePts(4, pqr=pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	LabelPts( ps );
	MarkPts( pqr);
	MarkPts(ps, ["r",0.2, "transp", 0.4]);	
}
module randOnPlanePts_demo_2()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	MarkPts( [incenterPt(pqr)], ["r",.6, "color", "blue"] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randOnPlanePts(100, pqr=pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps) { MarkPts([p], ["r",0.05]); }
}

module randOnPlanePts_demo_3_stars()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	MarkPts( [Q], ["r",.6, "colors", ["blue"], "transp",0.3] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randOnPlanePts(100, pqr, r=[2.2, 3]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps) { MarkPts([p], ["r",0.05, "colors", [randc()]]); }
}


module randOnPlanePts_demo_4_angle()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//MarkPts( [incenterPt(pqr)], ["r",.6, "color", "blue"] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps30 = randOnPlanePts(30, pqr, a=30); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	ps4560 = randOnPlanePts(30, pqr, a=[75, 90]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	ps = randOnPlanePts(30, pqr, a=[100, 120], r=[2.3, 3]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps30) { MarkPts([p], ["r",0.05, "color", ["red"]]); }
	for(p=ps4560) { MarkPts([p], ["r",0.05, "colors", ["green"]]); }
	for(p=ps) { MarkPts([p], ["r",0.05, "colors", ["blue"]]); }

}

module randOnPlanePts_demo_5_angle()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];
	MarkPts( pqr, ["labels","PQR"] );

	//pt2 = anglePt( pqr, a=90, r=1); randOnPlanePts(2, pqr, a=[90,90] );
	P2 = normalPt( pqr );
	R2 = normalPt( [P,Q,P2] ); //anglePt( pqr, a=90, r=1);

//	pqr2 =  [ pt2[0], pqr[1], pt2[1] ] ;


//	P2= pqr[0];
	//Q= pqr[1];
//	R2= pqr[2];
	//echo( P2, Q, R2 );

	pqr2 = [ P2, Q, R2 ];

	Chain( pqr, ["r", 0.02]);

	Chain( pqr2, ["r", 0.02, "color", "green"]);


	pts = randOnPlanePts( 20, pqr );
	//MarkPts( pts, ["r",0.05,"samesize",true,"colors",["red"]] );

	pts2 = randOnPlanePts(100, pqr2);
	//echo(pts2);
	MarkPts( pts2, ["r",0.05,"samesize",true,"colors",["green"]] );
	

	//MarkPts( [incenterPt(pqr)], ["r",.6, "color", "blue"] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	//ps30 = randOnPlanePts(30, pqr, a=30); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
//	ps4560 = randOnPlanePts(30, pqr, a=[75, 90]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
//	ps = randOnPlanePts(30, pqr, a=[100, 120], r=[2.3, 3]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
//	
//	LabelPts( pqr , "PQR");
//	//LabelPts( ps );
//	//MarkPts(  ps);	
//	for(p=ps30) { MarkPts([p], ["r",0.05, "color", ["red"]]); }
//	for(p=ps4560) { MarkPts([p], ["r",0.05, "colors", ["green"]]); }
//	for(p=ps) { MarkPts([p], ["r",0.05, "colors", ["blue"]]); }

}


//randOnPlanePts_demo();
//randOnPlanePts_demo_2();
//randOnPlanePts_demo_3_stars();
//randOnPlanePts_demo_4_angle();
//randOnPlanePts_demo_5_angle();




//========================================================
randRightAnglePts=[[],
"
 r: first arm length (default: rand(5) )
 dist: dist from ORIGIN. It could be a point (indicating translocation) or integer (default: rand(3))
	 If it is an integer, will draw at range inside [dist,dist,dist] location, and the Q will always be the one
	 closest to ORIGIN.	
 r2: 2nd arm length. If not given (default), auto pick a random value
	between 0~r

 randRightAnglePts( r=3, r2=3) gives points of a *special right triangle*
 in which 3 angles are 90, 45, 45.

"];

function randRightAnglePts( 
    r=rand(5), 
    dist=randPt(), 
    r2=-1
    )=
( 
    RMs( [ rand(360), rand(360), rand(360)] 
	   , //shuffle(
			[
			 [      r,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
			,[      0,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
			,[      0,r2>0?r2:rand(r), 0]+(isnum(dist)?[dist,dist,dist]:dist) 
		 	]
		//)
	   ) 
); 

module randRightAnglePts_demo()
{
	echom("randRightAnglePts");
	as = [ rand(360), rand(360), rand(360)];
	rm = RM(as);
	
	//echo( "as = ", as);
	//echo( "rm = ", rm);

	pqr = randRightAnglePts();
	echo( "pqr", pqr );

	Chain( pqr, ["r",0.02] );
	MarkPts( pqr,["grid",true] );
	Mark90 (pqr );
	LabelPts (pqr, "PQR" );
	ColorAxes();
}

//randRightAnglePts_demo();



//========================================================
range=["range", "i,j,k,cycle", "array", "Core",

 " Given i, optional j,k, return a range or ranges of ordered numbers 
;; without the end point. 
;;
;; (1) basic
;;
;;  range(3) => [0,1,2]   // from 0 to 3, inc by 1 (= for(n=[0:i-1] )
;;  range(3,6) => [3,4,5] // inc by 1  (= for(n=[i:j-1] )
;;  range(3,2,6)= > [3,5] // inc by 2  (= for(n=[i:j:k-1] )
;;  range(6,3)
;;  range(6,2,3)  
;;
;; (2) i= array or string
;;
;;   range([3,4,5]) => [0,1,2]
;;
;;   for(x=arr)        ... the items
;;   for(i=range(arr)) ... the indices 
;;
;;   range([3,4,5],isitem=true) => [3,4,5]
;;
;; (3) cycle= i,-i, true: cycling (see tests)
;;
;; (4) cover= [m,n] 
;;
;;   range(4, cover=[0,1],cycle=true )= [[0,1],[1,2],[2,3],[3,0]] 
;;   range(4, cover=[0,1],cycle=false)= [[0,1],[1,2],[2,3],[3]] 
;;
;; (6) obj, isitem -- return items but not indices
;;
;;   range( 3,cover=[1,1],isitem=true,obj=obj )
;;      = [[12, 10, 11], [10, 11, 12], [11, 12, 10]]
;;   range( obj,cover=[1,1],isitem=true )
;;      = [[13, 10, 11], [10, 11, 12], [11, 12, 13], [12, 13, 10]]
",
"subarr"
];


function range(i,j,k, cycle)=
let ( 
	 obj = isarr(i)||isstr(i)? i :undef
	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
	,beg= inum==1 
            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
    ,end= inum==1 
		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
		   : inum==2?j:k
		   
	,cycle=cycle==true?1:cycle

	,intv = sign(end-beg)        // when inum=3, the middle one is
			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	

	,residual= or( mod(abs(end-beg), abs(intv)), intv)

	,extraidx= abs(intv)>abs(beg-end) // End points to be skipped, 
	           ? 1                    // depending on the intv. If
	           : residual             // intv too large, set to 1
	,objrange= obj
			   ? range ( len(obj)
						, cycle=cycle ) 
			   : []
											 
)(
//	[beg, intv, end ]
//    [ for(i=[beg:intv:end-extraidx]) i	]

	( intv==0 || beg==end || (beg==0&&end==undef) || obj==[] || obj=="" )
	? []
	: obj  // Allowing range(arr) or range(str)
	  ? objrange

	  // Non obj : =================

	  : cycle>0
		  ? concat( 
				[ for(i=[beg:intv:end-extraidx]) i ]
				,[ for(i=[beg:intv:beg+(cycle-1)*intv]) i]
				)
		  : cycle<0
			?  concat( 
				[ for(i=[end+cycle*intv:intv:end-intv]) i]
				,[ for(i=[beg:intv:end-extraidx]) i ]
				)
		 	:[ for(i=[beg:intv:end-extraidx]) i	]
		  
);

module range_test( ops )
{
	obj = [10,11,12,13];
	doctest( range
	,[
		""
		,"// 1 arg: starting from 0"
		,["5", range(5), [0,1,2,3,4]]
		,["-5", range(-5), [0, -1, -2, -3, -4] ]
//		,["0", range(0), [] ]
//		,["[]", range([]), [] ]
		,""
		,"// 2 args: "
		,["2,5", range(2,5), [2,3,4]]
		,["5,2", range(5,2), [5,4,3]]
		,["-2,1", range(-2,1), [-2,-1,0]]
		,["-5,-1", range(-5,-1), [-5,,-4,-3,-2]]

		,"", "// Note these are the same:"
		,["3", range(3), [0,1,2]]
		,["0,3", range(0,3), [0,1,2]]
		,["-3", range(-3), [0,-1,-2]]
		,["0,-3", range(0,-3), [0,-1,-2]]
		,["1", range(1),[0] ]
		,["-1", range(-1),[0] ]
		

		,""
		,"// 3 args, the middle one is interval. Its sign has no effect"
		,["2,0.5,5", range(2,0.5, 5), [2,2.5,3,3.5, 4, 4.5]]
		,["2,-0.5,5", range(2, -0.5, 5), [2,2.5,3,3.5, 4, 4.5]]
		,["5,-1,0", range(5,-1,0), [5,4,3,2,1]]
		,["5,1,2", range(5,1,2), [5,4,3]]
		,["8,2,0", range(8,2,0),[8,6,4,2]]
		,["0,2,8", range(0,2,8),[0, 2, 4, 6]]			
		,["0,3,8", range(0,3,8),[0, 3, 6]]
		
		,""
		,"// Extreme cases: "
		
		,["", range(), [], ["rem", "Give nothing, get nothing"]]
		,["0", range(0),[], ["rem", "Count from 0 to 0 gets you nothing"] ]
		,["2,2", range(2,2),[], ["rem", "2 to 2 gets you nothing, either"] ]
		,["2,0,4", range(2,0,4),[], ["rem", "No intv gets you nothing, too"] ]
		,["0,1", range(0,1),[0] ]
		,["0,1,1", range(0,1,1),[0] ]

		,"// When interval > range, count only the first: "
		,["2,5,4", range(2,5,4),[2] ]
		,["2,5,-1", range(2,5,-1),[2] ]
		,["-2,5,-4", range(-2,5,-4),[-2] ]
		,["-2,5,1", range(-2,5,1),[-2] ]

		,""
		," range( <b>obj</b> )"
		,""
		,"// range( arr ) or range( str ) to return a range of an array"
		,"// so you don't have to do : range( len(arr) )"
		,""
		,str("> obj = ",obj)
		,""
		,["obj",range(obj),[0,1,2,3]] 
		,["[3,4,5]", range([3,4,5]), [0,1,2] ]
		,["''345''", range("345"), [0,1,2] ]
		,["[]", range([]), [] ]
		,["''''", range(""), [] ]

		,""
		," range( ... <b>cycle= i,-i,true</b>...)"
		,""
		,"// New 2014.9.7: "
		,"// cycle= +i: extend the index on the right by +i count "
		,"// cycle= -i: extend the index on the right by -i count"
		,"// cycle= true: same as cycle=1 "
		,"" 
		,["4,cycle=1", range(4,cycle=1), [0,1,2,3,0], ["rem","Add first to the right"] ]
		,["4,cycle=-1", range(4,cycle=-1), [3,0,1,2,3], ["rem","Add last to the left"]]
		,["4,cycle=true", range(4,cycle=true), [0,1,2,3,0],["rem","true=1"] ]
		,""

		,["2,5,cycle=1", range(2,5,cycle=1), [2,3,4,2], ["rem","Add first to the right"] ]
		,["2,5,cycle=2", range(2,5,cycle=2), [2,3,4,2,3], ["rem","Add 1st,2nd to the right"] ]
		,["2,5,cycle=-1", range(2,5,cycle=-1), [4,2,3,4],["rem","Add last to the left"] ]
		,["2,5,cycle=-2", range(2,5,cycle=-2), [3,4,2,3,4] ]
		,["2,5,cycle=true", range(2,5,cycle=true), [2,3,4,2], ["rem","true=1"] ]
		

		,""
		,["2,1.5,8,cycle=2", range(2,1.5,8,cycle=2), [2, 3.5, 5, 6.5, 2, 3.5] ]
		,["2,1.5,8,cycle=-2", range(2,1.5,8,cycle=-2), [5, 6.5, 2, 3.5, 5, 6.5] ]
		,["2,1.5,8,cycle=true", range(2,1.5,8,cycle=true), [2, 3.5, 5, 6.5, 2] ]
		
		,""
		," range(<b>obj</b>,<b>cycle=i,-1,true</b>...)"
		,""
		,str("> obj = ",obj)
		,["obj,cycle=1",   range(obj,cycle=1)	,[0,1,2,3,0]] 
		,["obj,cycle=2",   range(obj,cycle=2)	,[0,1,2,3,0,1]] 
		,["obj,cycle=-1",range(obj,cycle=-1)	,[3,0,1,2,3]] 
		,["obj,cycle=-2",range(obj,cycle=-2)	,[2,3,0,1,2,3]] 

	]
	, ops
	);
}


//========================================================
ranges=["ranges", "i,j,k,cover,cycle", "array", "Core",

 " Given i, optional j,k, return a range or ranges of ordered numbers 
;; without the end point. 
;;
;; (1) basic
;;
;;  range(3) => [0,1,2]   // from 0 to 3, inc by 1 (= for(n=[0:i-1] )
;;  range(3,6) => [3,4,5] // inc by 1  (= for(n=[i:j-1] )
;;  range(3,2,6)= > [3,5] // inc by 2  (= for(n=[i:j:k-1] )
;;  range(6,3)
;;  range(6,2,3)  
"];


function ranges(i,j,k, cover, cycle)=
let ( 
	 obj = isarr(i)||isstr(i)? i :undef
	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
	,beg= inum==1 
            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
    ,end= inum==1 
		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
		   : inum==2?j:k
		   
	,intv = sign(end-beg)        // when inum=3, the middle one is
			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	

	,xbeg =  cover?cover[0]:0    // extra beg
	,xend =  cover?cover[1]:0    // extra end

	,residual= or( mod(abs(end-beg), abs(intv)), intv)

	,extraidx= abs(intv)>abs(beg-end) // End points to be skipped, 
	           ? 1                    // depending on the intv. If
	           : residual             // intv too large, set to 1
	,objrange= obj
			   ? range ( len(obj)
						, cycle=cycle
						, cover=cover ) 
			   : []
											 
)(
	( intv==0 || beg==end || (beg==0&&end==undef) || obj==[] || obj=="" )
	? []
	: obj  // Allowing range(arr) or range(str)
	  ? ranges ( len(obj)
				, cover=cover
				, cycle=cycle
				)

	  // Non obj : =================

	  : cover 
		? [ for( i=[beg:intv:end-extraidx] )  
				[ for(jj=range( (i-xbeg<beg && !cycle) ? beg: i-xbeg 
							 , (i+xend>=end && !cycle) ? end: i+xend+1
							 )) 
				  cycle
				  ? jj< beg || jj>=end 
					? fidx(abs(beg-end),jj-abs(beg),cycle=true)+abs(beg)
					: jj
				  : jj		
				]
		  ]

		// no cover  ---------------
		: cycle
		  ? concat( [for(i=[beg:intv:end-extraidx])[i] ], [[beg]] )
		  :[ for(i=[beg:intv:end-extraidx]) [i]	]
	  
);


module ranges_test( ops )
{
	obj = [10,11,12,13];
	doctest( ranges
	,[
		""
		,"// 1 arg: starting from 0"
		,["5", ranges(5), [[0], [1], [2], [3], [4]] ]
		,["-5", ranges(-5), [[0], [-1], [-2], [-3], [-4]] ]
		,""
		,"// 2 args: "
		,["2,5", ranges(2,5), [[2], [3], [4]]]
		,["5,2", ranges(5,2), [[5], [4], [3]]]
		,["-2,1", ranges(-2,1), [[-2], [-1], [0]]]
		,["-5,-1", ranges(-5,-1), [[-5], [-4], [-3], [-2]]]

		,"", "// Note these are the same:"
		,["3", ranges(3), [[0], [1], [2]]]
		,["0,3", ranges(0,3), [[0], [1], [2]]]
		,["-3", ranges(-3), [[0],[-1],[-2]]]
		,["0,-3", ranges(0,-3), [[0],[-1],[-2]]]
		,["1", ranges(1),[[0]] ]
		,["-1", ranges(-1),[[0]] ]
		

		,""
		,"// 3 args, the middle one is interval. Its sign has no effect"
		,["2,0.5,5", ranges(2,0.5, 5), [[2], [2.5], [3], [3.5], [4], [4.5]]]
		,["2,-0.5,5", ranges(2, -0.5, 5), [[2], [2.5], [3], [3.5], [4], [4.5]]]
		,["5,-1,0", ranges(5,-1,0), [[5], [4], [3], [2], [1]]]
		,["5,1,2", ranges(5,1,2), [[5], [4], [3]]]
		,["8,2,0", ranges(8,2,0),[[8], [6], [4], [2]]]
		,["0,2,8", ranges(0,2,8),[[0], [2], [4], [6]]]			
		,["0,3,8", ranges(0,3,8),[[0], [3], [6]] ]
		
		,""
		,"// Extreme cases: "
		
		,["", ranges(), [], ["rem", "Give nothing, get nothing"]]
		,["0", ranges(0),[], ["rem", "Count from 0 to 0 gets you nothing"] ]
		,["2,2", ranges(2,2),[], ["rem", "2 to 2 gets you nothing, either"] ]
		,["2,0,4", ranges(2,0,4),[], ["rem", "No intv gets you nothing, too"] ]
		,["0,1", ranges(0,1),[[0]] ]
		,["0,1,1", ranges(0,1,1),[[0]] ]

		,"// When interval > range, count only the first: "
		,["2,5,4", ranges(2,5,4),[[2]] ]
		,["2,5,-1", ranges(2,5,-1),[[2]] ]
		,["-2,5,-4", ranges(-2,5,-4),[[-2]] ]
		,["-2,5,1", ranges(-2,5,1),[[-2]] ]

		,""
		," ranges( <b>obj</b> )"
		,""
		,"// ranges( arr ) or ranges( str ) to return a range of an array"
		,"// so you don't have to do : ranges( len(arr) )"
		,""
		,str("> obj = ",obj)
		,""
		,["obj",ranges(obj),[[0], [1], [2], [3]]] 
		,["[3,4,5]", ranges([3,4,5]), [[0], [1], [2]]]
		,["''345''", ranges("345"), [[0], [1], [2]] ]
		,["[]", ranges([]), [] ]
		,["''''", ranges(""), [] ]

		,""
		," ranges( ... <b>cycle= i,-i,true</b>...)"
		,""
		,"// cycle= true: extend the index on the right by +i count "
		,"" 
		,["4,cycle=true", ranges(4,cycle=true), [[0],[1],[2],[3],[0]], ["rem","Add first to the right"] ]
		,["2,5,cycle=true", ranges(2,5,cycle=true), [[2],[3],[4],[2]] ]
		,["2,1.5,8,cycle=true", ranges(2,1.5,8,cycle=true), [[2],[3.5],[5],[6.5],[2]] ]
		
		,""
		," ranges(<b>obj</b>,<b>cycle=true</b>...)"
		,""
		,str("> obj = ",obj)
		,["obj,cycle=true",   ranges(obj,cycle=true)	,[[0],[1],[2],[3],[0]]] 
	
		,""
		," ranges(... <b>cover</b>=[i,j] )"
		,""
		,"// cover=[1,2] means, instead of returning i, return"
		,"//[i-1,i,i+1,i+2]. Note that when cover is set, return array of arrays."
		,"// Also note that w/cover, cycle=+i/-i is treated as cycle=true."

		,["3", ranges(3), [[0],[1],[2]]]
		,["3,cover=[0,1]", ranges(3,cover=[0,1],cycle=false)
			, [[0, 1], [1, 2], [2]]]
		,["3,cover=[0,1],cycle=true", ranges(3,cover=[0,1],cycle=true)
			, [[0, 1], [1, 2], [2,0]]]
		
		,""
		,["3,cover=[1,0]", ranges(3,cover=[1,0])
			, [[0], [0, 1], [1, 2]]]
		,["3,cover=[1,0],cycle=true", ranges(3,cover=[1,0],cycle=true)
			, [[2,0], [0, 1], [1, 2]]]

		,""
		,["3,cover=[1,2]", ranges(3,cover=[1,2])
			, [[0, 1, 2], [0, 1, 2], [1, 2]], ["rem","This is tricky. Compare next."]]
		,["3,cover=[1,2],cycle=true", ranges(3,cover=[1,2],cycle=true)
			, [[2,0, 1, 2], [0, 1, 2, 0], [1, 2, 0, 1]]]
		,["3,cover=[0,3],cycle=true", ranges(3,cover=[0,3],cycle=true)
			, [[0, 1, 2, 0], [1, 2, 0, 1], [2,0,1,2]]]

		,""
		,["3,cover=[0,0]", ranges(3,cover=[0,0]), [[0], [1], [2]]]
		,""
		,["5,8", ranges(5,8), [[5], [6], [7]]]
		,["5,8,cover=[0,0]", ranges(5,8,cover=[0,0]), [[5], [6], [7]]]
		,["5,8,cover=[1,0]", ranges(5,8,cover=[1,0])
						, [[5], [5, 6], [6, 7]]]
		,["5,8,cover=[0,1]", ranges(5,8,cover=[0,1])
						, [[5, 6], [6, 7], [7]]]
		,["5,8,cover=[1,0],cycle=true", ranges(5,8,cover=[1,0],cycle=true)
						, [[7,5], [5, 6], [6, 7]]]
		,["5,8,cover=[0,2],cycle=true", ranges(5,8,cover=[0,2],cycle=true)
						, [[5, 6,7], [6, 7, 5], [7,5,6]]]

		,""
		,["0,2,8,cover=[0,1]", ranges(0,2,8,cover=[0,1])
							,[[0, 1], [2, 3], [4, 5], [6, 7]]
							,["rem","Not:[[0,2],[2,4],[4,6],[6]]"]
							]
		,["0,3,8,cover=[0,1]", ranges(0,3,8,cover=[0,1])
							,[[0, 1], [3, 4], [6, 7]]
							,["rem","Not:[[0,3],[3,6],[6]]"]
							]
		,["0,2,6,cover=[0,2],cycle=true", ranges(0,2,6,cover=[0,2],cycle=true)
				,[[0, 1, 2], [2, 3, 4], [4, 5, 0]]
				,["rem","Not [0,2,4],[2,4,0],[4,0,2]"] ]

		,""
		,["obj",ranges(obj),[[0],[1],[2],[3]]] 
		,["obj,cover=[1,1]",ranges(obj,cover=[1,1])
			,[[0,1],[0,1,2],[1,2,3],[2,3]] ]  
		,["obj,cover=[1,1], cycle=true",ranges(obj,cover=[1,1], cycle=true)
			,[[3,0,1],[0,1,2],[1,2,3],[2,3,0]] ]  

	]
	, ops
	);
}



//========================================================
rearr=[ "rearr", "arr, order", "array", "Array" ,
 " Given an array (arr) and an integer array (order), return a new array containing
;; items i-th specified in *order*. For example, rearr( arr_of_3, [0,1,2,0,1,2] )
;; will duplicate the array, and rearr(arr_of_2, [1,0]) reverses it. Application
;; example, for a triangle defined by pqr, its 3 angles can be obtained with:
;;
;;  angle( pqr ) // = angle( rearr(pqr,[0,1,2]) ) = angle( rearr(pqr,[]) )
;;  angle( rearr(pqr, [0,2,1]) )
;;  angle( rearr(pqr, [1,0,2]) )
;;
;; NOTE 2014.8.9: the new list comp can handle it:
;;
;;  angle( [for(i=[1,0,2]) pqr[i]] )
;;
;; but doesn't look as straightforward. i.e., needs to think a bit about it.
"];

function rearr(arr, order=[],_i_=0)=
(
	[for(i=order) arr[i]]
);

module rearr_test( ops )
{
	pqr = randPts(3);
	doctest( rearr,
	[
	  ["pqr,[0,2,1]", rearr(pqr, [0,2,1]), [pqr[0], pqr[2],pqr[1]] ]
	, ["pqr,[2,0,1]", rearr(pqr, [2,0,1]), [pqr[2], pqr[0],pqr[1]] ]
	 //,[]
	], ops, ["pqr", pqr]
	);
}
//rearr_test(["mode", 13]);



//========================================================
repeat=["repeat", "x,n=2", "str|arr", "Array,String" ,
"
 Given x and n, duplicate x (a str|int|arr) n times.
"];

function repeat(x,n=2)=
(
	isarr(x)?
	concat(x, n>1?repeat(x,n-1):[])
	:str(x, n>1?repeat(x,n-1):"")
);

module repeat_test( ops )
{
	doctest( 
	repeat, [ 
		[ "''d''", repeat("d"), "dd" ]
	 ,	[ "''d'',5''", repeat("d",5), "ddddd" ]
	 ,	[ "[2,3]", repeat([2,3]), [2,3,2,3] ]
	 ,	[ "[2,3],3", repeat([2,3],3), [2,3,2,3,2,3] ]
	 ,  ["3",  repeat(3), "33" ]
	 ,  ["3,5",  repeat(3,5), "33333", ["rem","return a string"] ]
	],ops
	);
} 
//multi_test( ["mode",22] ); 
//doc(multi);




//========================================================
replace =[ "replace", "o,x,new=\"\"", "str|arr", "String, Array",
"
 Given a str|arr (o), x and optional new. Replace x in o with new when
;; o is a str; replace the item x (an index that could be negitave) 
;; with new when o is an array.
" ];

function replace(o,x,new="")=
let( x= isarr(o)?fidx(o,x):x)
( 
  isarr(o)
  ? (inrange(o,x)
	? concat( slice(o, 0, x), [new], x==len(o)-1?[]:slice(o,x+1)): o 
	)
  : (index(o,x)<0? 
	  o :str( slice(o,0, index(o,x))
			, new 
			, (index(o,x)==len(o)-len(x)?""
				:replace( slice(o, index(o,x)+len(x)), x, new) )
		   )
    )
);


module replace_test( ops )
{
	doctest( replace, 
	[
	  ["''a_text'', ''e''", 			replace("a_text", "e"), "a_txt"]
	 ,["''a_text'', ''''", 			replace("a_text", ""), "a_text"]
	 ,["''a_text'', ''a_t'', ''A_T''", replace("a_text", "a_t", "A_T"), 	"A_Text"]
	 ,["''a_text'', ''ext'', ''EXT''", 	replace("a_text", "ext", "EXT"), "a_tEXT"]
	 ,["''a_text'', ''abc'', ''EXT''", 	replace("a_text", "abc", "EXT"), "a_text"]
	 ,["''a_text'', ''t'',   ''{t}''", 	replace("a_text", "t", 	"{t}"), "a_{t}ex{t}"]
	 ,["''a_text'', ''t'', 4",     replace("a_text", "t", 	4), 		"a_4ex4"]
	 ,["''a_text'', ''a_text'', ''EXT''", replace("a_text", "a_text","EXT"), "EXT"]
    	 ,["'''', ''e''", 			replace("", "e"), ""]
	 ,"# Array # "
	 ,["[2,3,5], 0, 4", replace([2,3,5], 0, 4), [4,3,5] ]
	 ,["[2,3,5], 1, 4", replace([2,3,5], 1, 4), [2,4,5] ]
	 ,["[2,3,5], 2, 4", replace([2,3,5], 2, 4), [2,3,4] ]
	 ,["[2,3,5], 3, 4", replace([2,3,5], 3, 4), [2,3,5] ]
	 ,["[2,3,5], -1, 4", replace([2,3,5], -1, 4), [2,3,4] ]

	 ], ops
	);
} 		



//replace_test( ["mode",22] );
//doc(replace);



//========================================================
reverse=[ "reverse", "o", "str|arr", "Array, String",
"
 Reverse a string or array.\\
"];

function reverse(o)=
(
	isarr(o)
	? [ for(i=range(o)) o[len(o)-1-i] ]
	: join( [ for(i=range(o)) o[len(o)-1-i] ], "" )
);

module reverse_test( ops ){

	doctest(
	reverse
	,[
		[  [2,3,4], reverse([2,3,4]), [4,3,2] ]
	   ,[  [2], reverse([2]), [2] ]
	   ,[ "''234''",   reverse( "234" ), "432"  ]
	   ,[ "''2''",   reverse( "2" ), "2"  ]
	   ,[ "''",   reverse( "" ), ""  ]
	 ], ops
	);
}
//reverse_test( ["mode",22] );
//doc(reverse);




//========================================================
ribbonPts=[[]];
/*


	pts = [ p0,p1,p2 ...]

	ribbonPts( pts ) = [ pts1, pts2 ] 
		where pts1 = [N1,N2,N3 ...]
			 pts2 = [S1,S2,S3 ...]

	ribbonPts( pts, paired=true ) = [ pair_1, pair_2 ... ] 
		where pair_i = [Ni,Si]

	Note: 

	let 
		pairs = ribbonPts( pts, paired=true );
	then
		ribbonPts( pts, paired=false ) = [ getcol( pairs,0), getcol(pairs,1) ]
 
	or even easier:

		ribbonPts( pts, paired=false ) = transpose( pairs )


 // neighbors(arr, i, left=1, right=1, cycle=false)
 // neighborsAll(arr, left=1, right=1, cycle=false)

*/
function ribbonPts( pts, len=1, paired=true, closed=false,_I_=0 )=
(
	closed?[]
	:(
	_I_< len(pts)?
	concat( 	[ [normalPt( neighbors( pts, _I_), len=len ) 
			  ,normalPt( neighbors( pts, _I_), len=-len ) 
			  ]
			 ]
		 , ribbonPts( pts, len=len, paired=paired, closed=closed,_I_=_I_+1 )
		):[]
	)
  
);

module ribbonPts_demo_1()
{
	nPt=5;
	echom("ribbonPts_demo_1");
	pts = randOnPlanePts(nPt);
	echo("pts", pts);
	MarkPts( pts, ["labels", range(pts)] );

	rbpairs = ribbonPts( pts, len=0.3);
	
	echo("len(rbpairs)", len(rbpairs));
	Chain( pts, ["closed", false, "r",0.01, "color","black"] );
	rbs = transpose( rbpairs ); 

	echo("transpose correct?", transpose(rbs)==rbpairs );
	echo("rbs = ", len(rbs));
	Chain( rbs[0], 
			["closed", false, "color", "red", "r", 0.02, "transp",0.2] );
	//Chain( rbs[1], 
	//		["closed", false, "color", "green", "r", 0.02, "transp",0.2] );
	for(i=[0:len(rbpairs)-1]){
		echo_("i={_}, pts[i]={_}, rbpairs[{_}] = {_}"
					, [i,pts[i],i, rbpairs[i] ]) ;
		MarkPts( rbpairs[i] );
		Line0 ( rbpairs[i], ["r",0.01, "color","black"] );

		if( i>0 && i<len(rbpairs)-1)
			LabelPt( rbpairs[i][0], 
				str("aN =", angle([ normal(neighbors(pts,i-1))
								, ORIGIN
								,normal(neighbors(pts,i))
								] )
					)
			); 
		if( i< len(rbpairs)-1)
		Plane( concat( rbpairs[i], reverse(get(rbpairs, i+1, cycle=false)))  );
		//Line0( [pts[i],rbpairs[i][0] ] );
	}

}
//ribbonPts_demo_1();


//========================================================
RM=[ "RM","a3","matrix", "Math, Geometry",
"
;; Given a rotation array (a3=[ax,ay,az] containing rotation angles about
;; x,y,z axes), return a rotation matrix. Example:
;;
;;  p1= [2,3,6]; 
;;  a3=[90,45,30]; 
;;  p2= RM(a3) * p1;  // p2= coordinate after rotation
;;
;; In OpenScad the same rotation is carried out this way:
;;
;;   rotate( a3 )
;;   translate( p1 )
;;   sphere(r=1); 
;;
;; This rotates a point marked by a sphere from p1 to p2
;; but the info of p2 is lost. RM(a3) fills this hole, thus
;; provides a possibility of retrieving coordinates of
;; rotated shapes.
;;
;; The reversed rotation from p2 to p1 can be achieved by simply applying
;; a transpose :
;;
;;  p3 = transpose(RM(a3)) * p2 = p1
"];


function RM(a3)=
(
	a3.z==0?
	IM*RMy(a3.y) * RMx(a3.x) 
	:RMz(a3.z) * RMy(a3.y) * RMx(a3.x) 

//  No idea why the following doesn't work with [0,y,0]:
//  Note: Run demo_reverse( "On y", p0y0(randPt()), "lightgreen");
//        in rotangles_p2z_demo();
//	(a3.z==0?IM:RMz(a3.z)) * 
//	(a3.y==0?IM:RMy(a3.y)) *
//	(a3.x==0?IM:RMy(a3.x)) 
	
);

module RM_test(ops){ doctest(RM,
[],ops); }

//RM_test( [["mode",22]] );
//doc(RM);

module RM_demo()
{
	a3 = randPt(r=1)*360;
	echo("in RM, a3= ",a3);
	rm = RM(a3);
	echo(" rm = ", rm);
}
//RM_demo();


//========================================================
RMx=["RMx","ax","matrix", "Math, Geometry",
"
 Given an angle (ax), return the rotation matrix for
;; rotating a pt about x-axis by ax. Example:
;;
;;  pt = [2,3,4]; 
;;  pt2= RMx(30)*pt;  
;;
;; In openscad, you can rotate the pt to pt2 by thiss:
;;
;;  rotate( [30,0,0 ])
;;  translate( pt )
;;  sphere(r=1); 
;;
;; However,  the coordinate info of pt2 is lost. RMx
;; (and RMy, RMz) provides a way to retrieve it. This
;; is a work around solution to an often asked question:
;; how to obtain the coordinates of a shape.
"];	

function RMx(ax)= [
	[1,0,0]
	,[0, cos(-ax), sin(-ax) ]
	,[0,-sin(-ax), cos(-ax) ]
];

module RMx_test(ops)
{
	doctest( RMx
	,[
	
	],ops );
}
//RMx_test( ["mode",22] );
//doc(RMx);




////========================================================
RMy=["RMy","ay","matrix", "Math, Geometry",
"
 Given an angle (ay), return the rotation matrix for
;; rotating a pt about y-axis by ay. See RMx for details.
"];	
function RMy(ay)=[
	 [cos(-ay), 0, -sin(-ay) ]
	,[0,        1,        0  ]
	,[sin(-ay), 0,  cos(-ay) ]
];
module RMy_test(ops)
{
	doctest( RMy
	,[
	
	],ops );
}
//RMy_test( ["mode",22] );



//========================================================
RMz=["RMz","az","matrix", "Math, Geometry",
"
 Given an angle (az), return the rotation matrix for
;; rotating a pt about z-axis by az. See RMx for details.
"];

function RMz(az)=[
	 [ cos(-az), sin(-az), 0 ]
	,[-sin(-az), cos(-az), 0 ]
	,[       0,         0, 1 ]
];
module RMz_test(ops)
{
	doctest( RMz
	,[
	
	],ops );
}
//RMz_test( ["mode",22] );
//doc(RMz);


//========================================================
RMs=[[]];
function RMs( angles, pts, _i_=0 )=
(
	_i_<len(pts)? concat( [ RM(angles)*pts[_i_] ]
                         , RMs( angles, pts, _i_+1)
					  ):[]  
);

module RMs_demo()
{
	echom("RMs");
	a3 = randPt(r=1)*360;
	echo("in RMs, a3= ",a3);
	rm = RM(a3);
	echo(" rm = ", rm);
	p = randPt();
	echo( " p = ", p);
	echo( "rm*p = ", rm*p );
	echo( "RMs(a3, [p]) = ", RMs(a3, [p]) );
	
}
//RMs_demo();


//========================================================
RM2z=["RM2z","pt,keepPositive=true", "matrix", "Math, Geometry", 
"
 Given a pt, return the rotation matrix that rotates
;; the pt to z-axis. If keepPositive is true, the matrix will
;; always rotate pt to positive z. The xyz of the new pt on z-axis, ;;
;; pt2, is thus:
;;
;;  pt2 = RM2z(pt)*pt
;;
;; The reversed rotation, i.e., rotate a point on z back to pt,
;; can be achieved as:
;;
;;  pt3 = transpose( RM2z(pt) )*pt2  = pt
"];

function RM2z(pt,keepPositive=true)=
(
	/*pt.z==0
	?RM( rotangles_p2z(pt,keepPositive=true) ) 
	:RMy((keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/noRM(pt)))
	*RMx( sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z)) )
	*/
	RM( rotangles_p2z(pt,keepPositive=true) )
);

module RM2z_demo()
{
	pt= randPt();
		//If wanna check a pt on the xy-plane :
		//pt= [randPt()[0],randPt()[0],0];
	Line( [O,pt] );
	pt2 = RM2z(pt)*pt;
	Line( [O,pt2] );
	//echo_("In RM2z_demo, pt={_}, pt2= {_}", [pt,pt2]);
	//echo_("In RM2z_demo, len pt - len pt2= {_}", [norm(pt)-norm(pt2)]);
	//echo_("In RM2z_demo, RM2z= {_}", [RM2z(pt)]);
	MarkPts( [pt, pt2] );

	pt3 = transpose( RM2z(pt))*pt2;
	translate(pt3)
	color("blue", 0.2)
	sphere(r=0.3);
	PtGrid(pt3);

}
//RM2z_demo();


module RM2z_demo2()
{
	pt= randPts(1)[0];
	ptz= [0,0,pt.z];
}
module RM2z_test(ops)
{
	doctest( RM2z
	,[
	
	],ops );
}
//RM2z_demo();
//RM2z_test( ["mode",22] );
//doc(RM2z);





//========================================================
RMz2p=[ "RMz2p","pt","array", "Math, Geometry",
"
 Given a pt, return the rotation matrix that rotates the pt to
;; z-axis. The xyz of the new pt, pn, is:
;;
;;  pn = RM2z(pt)*pt
"];

function RMz2p(pt)=
(
3
//ax = -sign(pt.z)*asin( pt.y/L );  // rotate@y to xz-plane
//	ay = pt.z==0?90:atan(pt.x/pt.z); 	// rotate from xz-plane to target 	
	
);

//echo("a<span style='font-size:20px'><i>this is a&nbsp;test</i></span>b");

module RMz2p_demo()
{
	pt= randPts(1)[0];  // random pt
	Line( [O,pt] );
	PtGrid(pt);

	echo("pt = ", pt);
	L = norm(pt);       
	ptz = [0,0,sign(pt.z)*L];      // ptz: projection pt on z-axis
	Line( [O,ptz]); 

	// rot=[ 0, -atan( z/slopelen(x,y) ), x==0?90:atan(y/x)];
	// ay = -atan( z/slopelen(x,y) );  // rotate@y to xz-plane
	// az = x==0?90:atan(y/x); 		   // rotate from xz-plane to target 	
	/*
		rotate from x to xz-plane to target
		x ==> xz ==> target
		
		x, y, z
		   ay = -atan( z/slopelen(x,y) );  // rotate@y to xz-plane
		      az = x==0?90:atan(y/x); 	// rotate from xz-plane to target 	
	
		z, x, y
		   ax = -atan( y/slopelen(z,x) );  // rotate@y to xz-plane
		      ay = z==0?90:atan(x/z); 	// rotate from xz-plane to target 	
		
		
		

	*/

	sloxz = slopelen(pt.x,pt.z);
	sloyz = slopelen(pt.y,pt.z);

	// rotate at y to yz-plane
	//ax = -atan(pt.z/sloyz);   // rotate about x-axis from z-axis
	//ay = pt.x==0?90:atan(pt.z/pt.x) ;   // rotate about y axis from
											// where ax already applied
    //ax = -sign(pt.z)*sign(pt.y)*atan( abs(pt.y/slopelen(pt.z,pt.x)) );  // rotate@y to xz-plane
	ax = -sign(pt.z)*asin( pt.y/L );  // rotate@y to xz-plane
	ay = pt.z==0?90:atan(pt.x/pt.z); 	// rotate from xz-plane to target 	
	/*
	++ -
	+- +
	-+ +
	-- -

	pt.y	, pt.z, slozx, 
	  +    -    +       +
	  -    +    -       +
	  -    -    -       - 
      +    +    +       - 

	*/
	echo("pt.y = ", pt.y);
	echo("pt.z = ", pt.z);
	echo("slozx= ", pt.y/slopelen(pt.z,pt.x) );

	echo("ax", ax);
	echo("ay", ay);
	//=======================
	// rotate ax from z-axis 
	rotate( [ax, 0, 0 ] )
	translate( ptz )
	color("green", 0.3)
	sphere(r=0.1);

	// pt after rotating ax from z-axis. This pt must be yz plane
	pt2 = RMx(ax)*ptz;
	//Line([O,pt2]);

	echo("pt2=", pt2);

	// translate to pt2 directly
	translate( pt2 )
	color("green", 0.2)
	sphere(r=0.15);
	color("green", 0.3)
	Line( [O, pt2] );
	//=============================

	
	//rotate( [0, ay, 0 ] )
	//rotate( [ax, 0, 0 ] )
	rotate( [ax, ay, 0 ] )
	translate( ptz )
	color("blue", 0.3)
	sphere(r=0.2);


	pt3 = RMy(ay)*RMx(ax)*ptz;
	translate( pt3 )
	color("blue", 0.2)
	sphere(r=0.3);
	
	//=============================


	//MarkPts([pt,ptz, pt2]);


/*
	RMz2p = RMx(ax); //RMy(ay)*RMx(ax);

	pt3 = RMz2p*ptz;

	MarkPts( [pt, ptz] );

	Line([O,pt3]);
	translate(pt3 )
	color("blue", 0.2)
	sphere(r=0.3);

	*/	
	/*
	pt2 = RM2z(pt)*pt;
	Line( [O,pt2] );
	echo_("In RM2z_demo, pt={_}, pt2= {_}", [pt,pt2]);
	echo_("In RM2z_demo, len pt - len pt2= {_}", [norm(pt)-norm(pt2)]);
	echo_("In RM2z_demo, RM2z= {_}", [RM2z(pt)]);
	MarkPts( [pt, pt2] );

	pt3 = transpose( RM2z(pt))*pt2;
	translate(pt3)
	color("blue", 0.2)
	sphere(r=0.3);
	*/
}
//RMz2p_demo();

module RM2z_demo2()
{
	pt= randPts(1)[0];
	ptz= [0,0,pt.z];
}

//========================================================
rodfaces=["rodfaces","sides=6, hashole","array", "Array, Geometry, Faces",
 " Given count of sides (sides) of a rod( column, cylinder), return the 
;; faces required for the polyhedron() function to render the sides of
;; that rod. For a rod with sides=4 below: 
"];

function rodfaces(sides=6,hashole=false)=
 let( sidefaces = rodsidefaces(sides)
	, holefaces = hashole? 
				  [ for(f=sidefaces) 
					 reverse( [for(i=f) [i+sides*2] ])
				  ]:[]
	)
(
	hashole
	? concat( range(sides-1, -1)
			, range(sides, sides*2)
			, sidefaces
			) 	
	: concat( [ range(sides-1, -1) ]
			, [ range(sides, sides*2) ]
			, sidefaces
			)
);

//	p_faces = reverse( range(sides) );
//	// faces need to be clock-wise (when viewing at the object) to avoid 
//	// the error surfaces when viewing "Thrown together". See
//	// http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
//	q_faces = range(sides, sides*2); 
//concat( ops("has_p_faces")?[p_faces]:[]
//				 , ops("has_q_faces")?[q_faces]:[]
//				 , rodsidefaces(sides)
//				 , ops("addfaces")
//				 );

//========================================================
rodsidefaces=["rodsidefaces","sides=6,twist=0","array", "Array, Geometry, Faces",
 " Given count of sides (sides) of a rod( column, cylinder), return the 
;; faces required for the polyhedron() function to render the sides of
;; that rod. For a rod with sides=4 below: 
;;         
;;       _6-_
;;    _-' |  '-_
;; 5 '_   |     '-7
;;   | '-_|   _-'| 
;;   |    '(4)   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;; 
;; twist=0
;;
;;   [0,1,5,4]
;; , [1,2,6,5]
;; , [2,3,7,6]
;; , [3,0,4,7]
;;
;; twist = 1:
;;
;;       _5-_
;;    _-' |  '-_
;;(4)'_   |     '-6
;;   | '-_|   _-'| 
;;   |    '-7'   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;;
;;   [0,1,4,7]
;; , [1,2,5,4]
;; , [2,3,6,5]
;; , [3,0,7,6]
;;
;;
;; twist = 2:
;;       (4)_
;;    _-' |  '-_
;; 7 '_   |     '-5
;;   | '-_|   _-'| 
;;   |    '-6'   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;;
;;   [0,1,7,6]
;; , [1,2,4,7]
;; , [2,3,5,4]
;; , [3,0,6,5]
;;
;; Note that the top and bottom faces are NOT included. 
"];

function rodsidefaces( sides=6, twist=0) =
 let( top= twist
		   ? roll( range(sides,2*sides), count=twist) 
		   : range(sides,2*sides)
	)
(
	[ for(i = range(sides) )
	  [ i
	  , i==sides-1?0:i+1
	  , i==sides-1? top[0]: top[i+1]
	  , top[i]
//	  , i + 1 + (i- twist==sides-1?0:sides) -twist
//	  , i+sides - twist +( i==0&&twist>0?sides:0)
	  ]
	]
);



module rodsidefaces_test( ops=["mode",13] )
{
	doctest( rodsidefaces,
	[
	 ["4", rodsidefaces(4), 
			[ [0,1,5,4]
			, [1,2,6,5]
			, [2,3,7,6]
			, [3,0,4,7]
			]
	]
	,["5", rodsidefaces(5), 
			[ [0, 1, 6, 5]
			, [1, 2, 7, 6]
			, [2, 3, 8, 7]
			, [3, 4, 9, 8]
			, [4, 0, 5, 9]
			]
	 ]
	,["4,twist=1", rodsidefaces(4,twist=1), 
			[ [0,1,4,7]
			, [1,2,5,4]
			, [2,3,6,5]
			, [3,0,7,6]
			]
	]
	,["4,twist=-1", rodsidefaces(4,twist=-1), 
			[ [0,1,6,5]
			, [1,2,7,6]
			, [2,3,4,7]
			, [3,0,5,4]
			]
	]
	,["4,twist=2", rodsidefaces(4,twist=2), 
			[ [0,1,7,6]
			, [1,2,4,7]
			, [2,3,5,4]
			, [3,0,6,5]
			]
	 ]

	], ops
	);
}


module rodsidefaces_demo_1()
{
	//==== sides=4
	pqr = randPts(3); 
	bps = cubePts(pqr ); // function cubePts( pqr, p=undef, r=undef, h=1)
    *MarkPts( bps );
	*LabelPts( bps, range(bps) );
	fs = rodsidefaces( 4 );
	color(undef, 0.4)
	*polyhedron( points = bps, faces = fs ); 

	//==== sides= 5
	c=5;
	pqr2 = randPts(3);
	
	cps1 = arcPts( pqr2, r=2, count=c, pl="yz", cycle=1 );

	newp = onlinePt(p01(pqr2), len=-0.2);
	cps2 = arcPts( [ newp, P(pqr2), R(pqr2) ] , r=2, count=c, pl="yz", cycle=1 );

	cps = concat( cps1, cps2);
	MarkPts( cps );
	LabelPts( cps, range(cps) );
	fs2 = rodsidefaces(c);
	echo("fs2 = ", fs2);
  	
	//LabelPts( cps1);
	//LabelPts( cps2);
	color(undef, 0.4)
	polyhedron( points = cps, faces = fs2 ); 
	Chain( cps1, ops=["closed",true]);
	Chain( cps2, ops=["closed",true]);
}

//rodsidefaces_demo_1();

//========================================================
rodPts=["rodPts","","pts","Point, Array, Geometry",
 " Given 
;;
"];

function rodPts( pqr=randPts(3), ops=[] )=
 let( _ops = [ "r", 0.05
	  , "sides",     6
	  , "label", "" 
	  , "p_pts",undef  // pts on the P end
	  , "q_pts",undef  // pts on the q end
	  , "p_angle",90   // cutting angle on p end
	  , "q_angle",90   // cutting angle on q end
	  , "rotate",0  // rotate about the PQ line, away from xz plane
	  , "q_rotate", 0 // see twistangle()
	  , "cutAngleAfterRotate",true // to determine if cutting first, or
						  // rotate first. 
	  ]
	,ops = concat( ops, _ops )
	,r       = hash(ops, "r")
	,rot     = hash(ops,"rotate")
	,sides   = hash(ops,"sides")
	,p_angle = hash(ops,"p_angle")
	,q_angle = hash(ops,"q_angle")
	,q_rotate = hash(ops,"q_rotate")
	,qidx_rotate= round(q_rotate/360)*sides
	,cutAngleAfterRotate= hash(ops,"cutAngleAfterRotate")

	,pqr0= pqr==undef
			? randPts(3)
		 	: len(pqr)==2
		   	  ? concat( pqr, [randPt()] )
		   	  : pqr 
	,P = pqr0[0]
	,Q = pqr0[1]
	,R1= pqr0[2]
	,pq= [P,Q]
	,N = N(pqr0)
	,M = N2(pqr0)
	,T = othoPt( [ P,R1,Q ] )
	,p1_for_rot= angle(pqr0)>=90
				? P
				: onlinePt([norm(T-Q)>=norm(P-Q)? T:P,Q], len=-1 )
	,R2 = anyAnglePt( [ p1_for_rot, T, R1]     
                   , a= rot 
                   , pl="yz"
                   , len = dist([T,R1]) 
				  )
	,P2 = onlinePt( [P,Q], len=-1)

	,norot_pqr_for_p = [P2,P, R1]
	,norot_pqr_for_q = [P, Q, R1]
	,rot_pqr_for_p   = [P2,P, R2]
	,rot_pqr_for_q   = [P, Q, R2]

//	pqr_for_p = cutAngleAfterRotate&&rot>0
//				? rot_pqr_for_p
//				: norot_pqr_for_p;
//	pqr_for_q = cutAngleAfterRotate&&rot>0
//				? rot_pqr_for_q
//				: norot_pqr_for_q;		

	,p_pts90= arcPts( rot_pqr_for_p
				  , r=r, a=360, pl="yz", count=sides )

	,_q_pts90= arcPts( rot_pqr_for_q
				   , r=r, a=360, pl="yz", count=sides )
	, q_pts90= qidx_rotate
			   ? concat( slice(_q_pts90, qidx_rotate), slice(_q_pts90, 0, qidx_rotate)) 
			   : _q_pts90

	//-------------------------------------------------
	// Cut an angle from P end. 
	,Ap = anyAnglePt( cutAngleAfterRotate
					? [P2,P,R1]
					: [P2,P,R2]
				  , a=p_angle+(90-p_angle)*2
				  , pl="xy")
	,Np = N([ Ap, P, Q ])  // Normal point at P
	,p_plane = [Ap, Np, P ]  // The head surface 
	,p_pts=or( hash(ops,"p_pts")
			  , p_angle==0
			   ? p_pts90
			   : 	[ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], p_plane ) ]
			  )
	//-------------------------------------------------
	// Cut an angle from Q end. 
	,Aq = anyAnglePt( cutAngleAfterRotate
					? [P,Q,R1]
					: [P,Q,R2]					
				  , a=q_angle //90-q_angle
				  , pl="xy") // Angle point for tail
	,Nq = N( cutAngleAfterRotate
					? norot_pqr_for_q
					: rot_pqr_for_q )  // Normal point at Q
	,q_plane = [Aq, Nq, Q ]//:pqr0;  // The head surface 
	,q_pts =or( hash(ops,"q_pts")
			  , q_angle==0
			    ? q_pts90
			    : [ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], q_plane ) ]
			  )
//	,q_pts= qidx_rotate>0
//			? concat( slice( _q_pts, sides-qidx_rotate)
//		 			, slice( _q_pts, 0, sides-qidx_rotate)
//					)
//			: qidx_rotate<0
//	  			? concat( slice(_q_pts, qidx_rotate)
//						, slice(_q_pts, 0+qidx_rotate)
//		 				)
//				: _q_pts
	
)( 
	[p_pts,q_pts]
);


module rodPts_demo_twistangle()
{
 echom("rodPts_demo_twistangle");

 pqr = randPts(3);
}





//========================================================
roll=["roll", "arr,count=1", "arr", "Array"
," Given an array (arr) and an int (count), roll arr items
;; as shown below:
;;
;; roll( [0,1,2,3] )   => [0,1,2],[3] => [3],[0,1,2] => [3,0,1,2]
;; roll( [0,1,2,3],-1) => [0],[1,2,3] => [1,2,3],[0] => [1,2,3,0] 
;; roll( [0,1,2,3], 2) => [0,1],[2,3] => [2,3],[0,1] => [2,3,0,1]
;;
;; Compare range w/cycle, which (1) returns indices (2) doesn't 
;; maintain array size:
;;
;; |> obj = [10, 11, 12, 13]
;; |> range( obj,cycle=1 )= [0, 1, 2, 3, 0]
;; |> range( obj,cycle=2 )= [0, 1, 2, 3, 0, 1]
;; |> range( obj,cycle=-1 )= [3, 0, 1, 2, 3]
;; |> range( obj,cycle=-2 )= [2, 3, 0, 1, 2, 3]

"];

function roll(arr,count=1)=
 let(L= len(arr)
	,count= count<0?L+count:count)
(
 	count==0 || count>L-1? arr
	:concat( [for(i= [L-count:L-1]) arr[i]]
		   , [for(i= [0:L-count-1]) arr[i]]
		   )
); 

module roll_test( ops=["mode",13])
{
	doctest( roll,
	[
		 [ "[0,1,2,3]", roll([0,1,2,3]), [3,0,1,2] ]
		,[ "[0,1,2,3],-1", roll([0,1,2,3],-1), [1,2,3,0] ]
		,[ "[0,1,2,3], 2", roll([0,1,2,3], 2), [2,3,0,1] ]
		,[ "[0,1,2,3,4,5], 2", roll([0,1,2,3,4,5], 2), [4,5,0,1,2,3] ]
		,[ "[0,1,2,3],0", roll([0,1,2,3],0), [0,1,2,3] ]
    ], ops
	);
}




//========================================================
rotangles_p2z=["rotangles_p2z","pt,keepPositive=true","Array","Math, Geometry",
"
 Given a 3D pt, return the rotation array that
;; would rotate pt to a point on z-axis, ptz= [0,0,L],
;; where L is the len of pt to the ORIGIN.
;; Two ways to rotate pt to ptz using the rotation
;; array (RA) is obtained:
;;
;; 1. Scadex way: (has the target coordinate)
;;
;;    RA = rotangles_p2z( pt ); 
;;    ptz = RM( RA )*pt; 
;;    translate( ptz )
;;    sphere9 r=1 ); 
;;
;; 2. OpenScad way: (lost the target coordinate)
;;
;;    RA = rotangles_p2z( pt ); 
;;    rotate( RA )
;;    translate( pt )
;;    sphere( r=1 ); 
"
];
        
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);

module rotangles_p2z_demo()
{
	echom("rotangles_p2z");

	module demo( label, pt, col)
	{
	    echo_("<{_}>, {_} line:  pt={_}, "
			,[label, col, pt ] );
 
		Line0([O,pt],[["markpts",false],["color",col]]);
		PtGrid(pt);

		// get the angles
		r2za= rotangles_p2z(pt, true 
				//,pt.x==0 && pt.y==0 && pt.z<0?false:true
			);   
		//echo_("pt={_}",[pt]);
		echo_("r2za ={_} ", [r2za]);


		// Demo two ways of rotating a pt to the z-axis using r2za:
		// Two spheres should be at the same spot

		// #1  Rotate from pt to z   (shown as small, dense sphere)
		rotate( r2za )     
		translate(pt)
		color( col, 1)
		sphere(r=0.1);

		// #2   Translate to the rotated pt (shown as large, transparant sphere )
		pt_on_z = RM(r2za)*pt; //RMy(r2za.y)*RMx(r2za.x) * pt;
		translate( pt_on_z )
		color(col,0.2)
		sphere(r=0.2);

	}

    //// The transparant ball must match 
    //// the solid ball on the z-axis:

	//demo( "Random pt", randPt(), "orange");
	//demo( "On xy-plane", pxy0(randPt()), "red");
	//demo( "On yz-plane", p0yz(randPt()), "green");
	//demo( "On xz-plane", px0z(randPt()), "blue");
	//demo( "On x", px00(randPt()), "red");
	//demo( "On y", p0y0(randPt()), "lightgreen");
	//demo( "On z", p00z(randPt()), "lightblue");


	module demo_reverse( label, pt, col)  // z=>p
	{
	    echo_("<{_}>, {_} line:  pt={_}, "
			,[label, col, pt ] );
 
		Line0([O,pt],[["markpts",false],["color",col]]);
		PtGrid(pt);
		MarkPts([ pt ],[["colors",[col]]] );

		r2za= rotangles_p2z(pt);   // get the angles
	
		RM_p2z = RM( r2za );              // RM for p to z
		RM_z2p = transpose( RM_p2z );	  // RM for z to p

		//echo("RM_z2p = ", RM_z2p);

		pz = [0,0, norm(pt)];
		translate( pz )
		sphere(r=0.1);		

		//echo("pz = ", pz);

		pt2 = RM_z2p* pz ;

		//echo("pt2 = ", pt2);

		translate( pt2 )
		color(col,0.2)
		sphere(r=0.2);		


	}

    //// The transparant ball must match 
    //// the solid ball on the end of line:

	//demo_reverse( "Random pt", randPt(), "orange");
	//demo_reverse( "On xy-plane", pxy0(randPt()), "red");
	//demo_reverse( "On yz-plane", p0yz(randPt()), "green");
	//sdemo_reverse( "On xz-plane", px0z(randPt()), "blue");
	//demo_reverse( "On x", px00(randPt()), "red");
	//demo_reverse( "On y", p0y0(randPt()), "lightgreen");  // <======
	//demo_reverse( "On z", p00z(randPt()), "lightblue");
}
module rotangles_p2z_test(ops)
{ doctest( rotangles_p2z, ops=ops ); }
//rotangles_p2z_test( ["mode",22] );

//rotangles_p2z_demo();
//doc(rotangles_p2z);





//========================================================
rotangles_z2p=["rotangles_z2p","pt","Array","Math, Geometry",
"
 Given a 3D pt, return the rotation array that
;; would rotate a pt on z, ptz (=[0,0,L] where L is
;; the distance between pt and the origin), to pt
;; Usage:
;;
;;    RA= rotangles_z2p( pt ); 
;;    rotate( RA )
;;    translate( ptz )
;;    sphere( r=1 ); 
"
];

/*
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);
*/
function rotangles_z2p(pt)=
(
	//// 2014.5.20: add 
	////   (pt.x<0?-1:1)
	//// to cover the case of : [x,0,0] when x<0
	////
	(pt.z==0&&pt.y!=0)
	?[ -sign(pt.y)*90, 0, -sign(pt.x)*sign(pt.y)*abs(atan(pt.x/pt.y))]  
	:[-sign(pt.z)*asin( pt.y/ norm(pt) )  // rotate@y to xz-plane
	 , pt.z==0?((pt.x<0?-1:1)*90):atan(pt.x/pt.z) 	      // rotate from xz-plane to target 	
	 , 0]

);	

module rotangles_z2p_demo()
{
	echom("rotangles_z2p_demo");

	module demo( label, pt, col)
	{
	    echo_("<{_}>, {_} line:  pt={_}, "
			,[label, col, pt ] );
 
		Line0([O,pt],[["markpts",false],["color",col]]);
		PtGrid(pt);
	translate( pt )
	color(col)
	sphere(r=0.1);

	echo("pt = ", pt);
	a_p2z= rotangles_p2z(pt, keepPositive=false);
	echo("a_p2z = ", a_p2z);

	ptz = RMy(a_p2z.y)*RMx(a_p2z.x) * pt;
	//MarkPts( [ pt, ptz ], [["grid",true]] );
	translate( ptz )
	sphere(r=0.1);


	a_z2p = rotangles_z2p( pt );
	echo("a_z2p", a_z2p);

	color(col,0.2)
	rotate( a_z2p )
	translate( ptz )
	sphere(r=0.3);

	}

	//demo( "Random pt", randPt(), "orange");
	//demo( "On xy-plane", pxy0(randPt()), "red");
	//demo( "On yz-plane", p0yz(randPt()), "green");
	//demo( "On xz-plane", px0z(randPt()), "blue");
	//demo( "On x", px00(randPt()), "red");   // <=======
	//demo( "On y", p0y0(randPt()), "lightgreen");
	demo( "On z", p00z(randPt()), "lightblue");
}
module rotangles_z2p_test(ops)
{ doctest( rotangles_z2p, ops=ops ); }

//rotangles_z2p_test( ["mode",22] );
//rotangles_z2p_demo();



//========================================================
_s= [ "_s", "s,arr,sp=''{_}''" , "str", "String",
"
 Given a string (s) and an array (arr), replace the sp in s with
;; items of arr one by one. This serves as a string template.
"
] ;

function _s(s,arr, sp="{_}", _i_=0)= 
( 
	index(s,sp)>=0 && len(arr)>_i_ ?
		str( slice(s,0, index( s,sp))
			, arr[_i_] 
			,  index(s,sp)+len(sp)==len(s)?
				"":(_s( slice( s, index(s,sp)+len(sp)), arr, sp, _i_+1))
		   ): s
);
	
module _s_test( ops )
{
	doctest( 
	_s
	, [
		 ["''2014.{_}.{_}'', [3,6]", _s("2014.{_}.{_}", [3,6]), "2014.3.6"]
	 	,["'''', [3,6]", _s("", [3,6]), ""]
	 	,["''2014.{_}.{_}'', []", _s("2014.{_}.{_}", []), "2014.{_}.{_}"]
	 	,["''2014.{_}.{_}'', [3]", _s("2014.{_}.{_}", [3]), "2014.3.{_}"]
	 	,["''2014.3.6'', [4,5]",     _s("2014.3.6", [4,5]), "2014.3.6"]
		,["''{_} car {_} seats?'', [''red'',''blue'']"
			,_s("{_} car {_} seats?", ["red","blue"]), "red car blue seats?"]	 
		 ], ops
	);
}
//s__test( ["mode",22] );


//========================================================
shift= [ "shift", "o,i=1", "str|arr", "String,Array" ,
" Return a str|arr short than o by one on the right side
;; = slice(o,0,-1).
" 
];
   
function shift( o )=
( 
	slice(o,0,-1)
);

module shift_test( ops )
{
	doctest(
	shift ,[
		[  "[2,3,4]", shift([2,3,4]), [2,3] ]
	   ,[  "[2]" , shift([2]), [] ]
	   ,[  "[]" , shift([]), [] ]
	   ,[  "''234''",   shift( "234" ), "23"  ]
	   ,[ "''2''",   shift( "2" ), ""  ]
	   ,[ "''''",   shift( "" ), ""  ]
	 ],ops
	);
}
//shift_test( ["mode",22] );



//========================================================
shortside=["shortside","b,c","number","Math,Geometry,Triangle",
 " Given two numbers (b,c) as the lengths of the longest side and one of 
;; the shorter sides a right triangle, return the the other short side a.
;; ( c^2= a^2 + b^2 )."
,"longside"
];
function shortside(b,c)= sqrt(abs(pow(c,2)-pow(b,2)));

module shortside_test( ops=["mode",13] )
{ doctest( shortside, [
	[ "5,4", shortside(5,4), 3 ]
	,[ "5,3", shortside(5,3), 4 ]
	], ops
	);
}



shrink= [ "shrink", "o,i=1", "str|arr", "String, Array",
"
 Return a str|arr short (on the left side) than o by one.\\
"
];

function shrink( o )=
(
	isarr(o)
	? ( len(o)<=1?[]: [for(i=[1:len(o)-1]) o[i]] )
	: ( len(o)<=1?"": join( [for(i=[1:len(o)-1]) o[i]] ,"")  )
);
module shrink_test( ops )
{
	doctest(
	shrink
	,[
		[  [2,3,4], shrink([2,3,4]), [3,4] ]
	   ,[  [2] , shrink([2]), [] ]
	   ,[  "''234''",   shrink( "234" ), "34"  ]
	   ,[ "''2''",   shrink( "2" ), ""  ]
	   ,[ "''''",   shrink( "" ), ""  ]
	 ], ops
	);
}
//shrink_test( ["mode",22] );




//========================================================
shuffle=["shuffle", "arr", "array", "Array",
" Given an array (arr), return a new one with all items randomly shuffled.
"];
function shuffle(arr, _i_=rand(1))=
(
	len(arr)>0? concat( [pick(arr, floor( len(arr)*_i_))[0] ]
					, shuffle( pick(arr, floor( len(arr)*_i_))[1], _i_=rand(1) )
					):[]
);

module shuffle_test( ops=["mode",13] )
{
	arr = range(6);
	arr2= [ [-1,-2], [2,3],[5,7], [9,10]] ;
	doctest( shuffle,
	[
		str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr2)= ", shuffle(arr2) )
		,str( "shuffle(arr2)= ", shuffle(arr2) )
		,str( "shuffle(arr2)= ", shuffle(arr2) )
	], ops, ["arr", arr, "arr2", arr2]
	);
}
//shuffle_test();



//========================================================
sinpqr= ["sinpqr","pqr","number","Math,Geometry",
 " Given a 3-pointer (pqr), return sin( RT/QR ) where T is 
;; the R's projection on PQ line (T=othoPt([P,R,Q])).
;;
;;           R
;;          /| 
;;         / |.'
;;        / .'T
;;       /.'
;;  ----Q------
;;    .'
;;   P
"]; 

function sinpqr( pqr )=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, T= othoPt( [P,R,Q] )
	, RT= dist([R,T])
	, RQ= dist([R,Q])
	, onQside = norm(T-P) > norm(Q-P)?1:-1
	)
(
	sign(onQside)* sin( angle([R,Q,T]) )
);

module sinpqr_demo()
{
	pqr= randPts(3);
	//pqr= randRightAnglePts();

	P=P(pqr); Q=Q(pqr); R=R(pqr);
	T= othoPt( [P,R,Q] );
	Chain( pqr );
	MarkPts( [P,Q,R,T], ["labels", "PQRT"] );
	Plane( pqr, ["mode",4]);
	n = sinpqr( pqr ); 
	a = asin(n);
	scale(0.03) text( str( n ) );

_mdoc("sinpqr_demo",
_s(" RT={_}
;; RQ={_}
;; a(pqr)= {_}
;; onQside={_}
", [ dist([R,T]), dist([R,Q]), angle(pqr), norm(T-P) > norm(Q-P)?1:-1]
)); 
	
}	
//sinpqr_demo();


//========================================================
slice=[ "slice", "s,i,j", "str|arr", "String,Array" ,
 " Slice s (str|arr) from i to j-1. 
;;
;; - If j not given or too large, slice from i to the end. 
;; - Both could be negative/positive.
;; - If i >j, do a reversed slicing
;;
;; Note that this func doesn't do cycling (i.e., when it is
;; out of range, it doesn't continue from the other end). 
;; Use *range()* for that purpose.
"
];

/*
http://forum.openscad.org/Recursion-problem-tc7666.html
function subseq(v,start,end,i=0) = 
  i < len(v) 
    ? i >= start && i <= end 
      ? concat([v[i]], subseq(v,start,end,i+1)) 
      : subseq(v,start,end,i+1) 
    : [] ; 
*/
/*
function slice(o,i,j)= 
let( i=i<-(len(o))?0:fidx(o,i)
   , j=j==undef||j>len(o)-1? len(o): fidx(o, j)
   )
(
	isarr(o)
	? range( i,j, obj=o, isitem=true)
	: join( range( i,j, obj=o, isitem=true ), "" ) 
//	[i,j]
);
*/

function slice(o,i,j)= 
 let( i= i<-(len(o))?0:fidx(o,i)
    , j=j==undef||j>len(o)-1? len(o): fidx(o, j)
	, L=len(o)
	, ret= i<j? [for(k=[i:j-1])o[k]]
			:i>j? [for(k=[L-i:L-j-1])o[L-k]]
		    :[]
	)
(
	isarr(o)? ret: join(ret,"")
);

module slice_test( ops )
{
	s="012345";
	a=[10,11,12,13,14,15];
	doctest( 
	slice
	, [
	  "## slice a string ##"
	  
	  ,["''012345'',2",slice(s,2),"2345"]		 
	  ,["''012345'',2,4",slice(s,2,4),"23"]
	  ,["''012345'',0,2",slice(s,0,2),"01"]		 
	  ,["''012345'',-2",slice(s,-2),"45"]		 
	  ,["''012345'',1,-1",slice(s,1,-1),"1234"]		 
	  ,["''012345'',-3,-1",slice(s,-3,-1),"34"]		 
	  ,["''012345'',3,3",slice(s,3,3),""]		 
	  ,["'''',3",slice("",3),""]		 
	  
	  , "## slice an array ##" 	
	  , [ "[10,11,12,13,14,15],2", slice(a, 2), [12,13,14,15]]
    	  , [ "[10,11,12,13,14,15],2,5", slice(a, 2,5), [12,13,14]]
    	  , [ "[10,11,12,13,14,15],2,20", slice(a, 2,20), [12,13,14,15], ["rem", "When out of range"]]
	  , [ "[10,11,12,13,14,15],-3", slice(a, -3), [13,14,15]]
    	  , [ "[10,11,12,13,14,15],-20,-3", slice(a, -20, -3), [10,11,12], ["rem", "When out of range"]]
    	  , [ "[10,11,12,13,14,15],-8,-3", slice(a, -8, -3), [10,11,12], ["rem", "When out of range"]]
    	  , [ "[[1,2],[3,4],[5,6]],2"
	  	, slice([[1,2],[3,4],[5,6]], 2), [[5,6]]]
    	  , [ "[[1,2],[3,4],[5,6]], 1,1"
	  	, slice([[1,2],[3,4],[5,6]], 1,1), []]
    	  , [ "[], 1", slice([], 1), []]
    	  , [ "[9], 1", slice([9], 1), []]

    	  ,""
	  ,"// Reversed slicing:"
	  ,""
	  , [ "[10,11,12,13,14,15], 2,5", slice(a, 2,5), [12,13,14]]
	  , [ "[10,11,12,13,14,15], 5,2", slice(a, 5,2), [15,14,13]]
	   , [ "''012345'', 5,2", slice(s, 5,2), "543"]
	  ,[ "''012345'', 3,1", slice(s, 3,1), "32"]
	  
	  ,[ "''012345'', -3,-5", slice(s, -3,-5), "32"]
	  ] , ops
	);
}
//slice_test( ["mode",22] );


//========================================================
slope= [ ["slope", "p,q=undef", "num",  "Math,Line" ],
"
 Return slope. slope(p,q) where p,q are 2-D points or\\
 slope(p) where p = [p1,p2]\\
"
];

function slope(p,q=undef)= q!=undef?
						((q[1]-p[1])/(q[0]-p[0]))
						:((p[1][1]-p[0][1])/(p[1][0]-p[0][0]));

module slope_test( ops )
{	
	doctest( slope
		   , [ 
				[ [[0,0],[4,2]], slope([0,0],[4,2]), 0.5 ]
		   	 ,  [ [ [[0,0],[4,2]] ], slope( [[0,0],[4,2]] ), 0.5 ]
			 ,  [ [ [[1,1],[4,2]] ], slope( [[0,0],[4,2]] ), 0.5 ]
			 ],ops
		  // , [["echo_level", 1]]	
		   );
}

//slope_test( ["mode",22] );




//========================================================
split= [ "split", "s, sp=\" \"", "arr", "String" ,
"
 Split a string into an array.
"
];

function split(s,sp=" ")=
 let(i = index(s,sp) 
	)	
(
	i<0
	? [s]
	: concat(
		 [ slice(s, 0, i)]
		 , split( slice(s, i+len(sp)), sp ) 
//		   , endwith(s,sp)?
//			 "":split( slice(s, i+len(sp)), sp ) 
		)
);


module split_test( ops )
{
	doctest( 
	split
	,[ 
		[ "''this is it''", split("this is it"), ["this","is","it"]]
	 ,	[ "''this is it'',''i''", split("this is it","i"), ["th","s ","s ","t"]]
	 ,	[ "''this is it'',''t''", split("this is it","t"), ["","his is i",""]]
	 ,	[ "''this is it'',''is''", split("this is it","is"), ["th"," "," it"]]
	 ,	[ "''this is it'','',''", split("this is it",","), ["this is it"]]
	 ,	[ "''Scadex'',''Scadex''", split("Scadex","Scadex"), ["",""]]
	], ops
	);
}


//========================================================
splits= [ "splits", "s, sp=[]", "arr", "String",
 " Split a string into an array based on the item in array sp . 
;;
;; splits(''abcdef'',[''b'',''e'']) => [''a'',''cd'',''f'']
;;
;; Developer note: 
;;
;; Most other recursions in this file go through items in a list
;; with a pattern:
;;
;;   _I_&lt;len(arr)
;;   ? ...
;;   : []           // Note this
;; 
;; This code, however, demos a rare case of recursion into layers 
;; of a tree:
;;
;;   _I_&lt;len(arr)
;;   ? ...
;;   : arr          // major difference
"];

function splits(s,sp=[], _I_=0)=
let(s = isarr(s)?s:[s] )
(
	_I_<len(sp)
	?  splits( joinarr([ for(x=s) split(x, sp=sp[_I_]) ]), sp, _I_+1)
	:s	
);

module splits_test( ops )
{
	//s="this is it";
	s="abc_def_ghi";
	doctest( 
	splits
	,[ 
	 [ "s,[''b'']", splits(s,["b"]), ["a", "c_def_ghi"]]
	,[ "s1,[''b'',''_'']", splits(s,["b","_"]), ["a", "c","def", "ghi"]]	
	,[ "s1,[''b'',''_'',''e'']",splits(s,["b","_","e"]), ["a", "c","d","f", "ghi"]]
	], ops, ["s",s]
	);
}


//========================================================
squarePts=["squarePts", "pqr,p,r", "pqrs", "Math, Geometry",
" Given a 3-pointer pqr, return a 4-pointer that has p,q of the
;; given pqr plus two pts, together forms a square passing through
;; r or has r lies along one side. This is useful to find the
;; square defined by p,q,r.
;;
;; Given [p,q,r], return [P,Q,N,M]
;;   P-------Q
;;   |      /|
;;   |     / |
;;   |    /  |
;;   M---R---N
;;
;;   P------Q
;;   |'-_   |:
;;   |   '-_| :
;;   |      '-_:
;;   |      |  ':
;;   M------N    R
;;
;; New 2014.7.15: new OPTIONAL arg: p, r to set size. 
;;
;;          p  
;;   P---|-----Q
;;   |   |    /| 
;;   |   |   / | r
;;   |   +--/---
;;   |     /   |
;;   '----R----'
"
];

//\\
// If size is given:\\
//\\
//         size
//   p---------------n
//   |'-           -'|
//   |  '-       -'  |
//   |    '-   -'    |
//   |      'x'      |
//   |        '-     |
//   |          '-   |
//   |            '- |
//   m---------------q

    
function squarePts(pqr, p=undef, r=undef)=
(
	p
	?( r
	   ? squarePts( [ anyAnglePt(pqr,a=90, len=r,pl="xy")
				  , Q(pqr)
				  , onlinePt(p10(pqr),len=r) 
				  ] )
	   : squarePts( [ onlinePt(p10(pqr),len=p), Q(pqr), R(pqr) ] )	 
	 ) 
	: r
	  ? squarePts( [ P(pqr)
				  , Q(pqr)
				  , anyAnglePt(pqr,a=90, len=r,pl="xy")	
				  //, onlinePt(p12(pqr),dist=r) 
				  ] )
	  : [  pqr[0]
	 	, pqr[1]
	 	, cornerPt( [pqr[2], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[1]]) 
	 	, cornerPt( [pqr[0], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[2]])
		]
);

module squarePts_demo()
{
	ops= ["closed",true,"r",0.03];
	ops2= ["closed",true,"r",0.2,"transp",0.3];

	pqr = randPts(3);
	Chain( pqr,ops );
	p4 = squarePts(pqr);
	MarkPts(p4,["labels","PQRS"]);
	Chain( p4,ops2 );

	pqr2 = randPts(3);
	Chain( pqr2, ops);
	pp4 = squarePts(pqr2);
	MarkPts(pp4,["labels","PQRS"]);
	Chain( pp4,concat(["color","red"], ops2) );
}

module squarePts_demo_2_debug()
{
	echom("squarePts_demo_2_debug");
// 7/7/2014: 
// anglePt(a=90), that uses squarePts, sometimes goes wrong. See anglePt_demo_3_90() and anglePt_demo_4_90_debug().

// Example wrong cases:

// [[3.00336, 1.06282, 2.49988], [3.00336, 1.06282, 2.49988], [1.99609, -0.615917, 2.09092]]

// bug found: it turns out that the P and Q are the same !!! Fix this in
// anglePt()

	pqr1= [[3.00336, 1.06282, 2.49988], [3.00336, 1.06282, 2.49988], [1.99609, -0.615917, 2.09092]];

	ops= ["closed",true,"r",0.03];

	echo("pqr1", pqr1);
	MarkPts(pqr1, ["grid",true]);
	//p4_1 = squarePts(pqr1);
	//echo("p4_1", p4_1);

}

module squarePts_demo_3_setsize()
{
	//ops= ["closed",true,"r",0.03];
	pqr = randPts(3,r=4);
	MarkPts(pqr,["labels","PQR"]);
	pqrs = squarePts(pqr);
	Chain( pqrs,["r",0.02,"color","red"] );


	pqrs2= squarePts(pqr, p=2);
	//MarkPts(pqrs2);
	//pp4 = squarePts(pqr2);
	Chain( pqrs2, ["r",0.06, "transp",0.3, "color","green"] );

	pqrs3= squarePts(pqr, r=2);
	//MarkPts(pqrs2);
	//pp4 = squarePts(pqr2);
	Chain( pqrs3, ["r",0.1, "transp",0.3, "color","blue"] );

	pqrs4= squarePts(pqr, p=3,r=3);
	//MarkPts(pqrs2);
	//pp4 = squarePts(pqr2);
	Chain( pqrs4, ["r",0.15, "transp",0.3, "color","purple"] );
	Dim( p012(pqrs4),["spacer",1], ["color","purple"]);
	Dim( p123(pqrs4),["spacer",1], ["color","purple"]);


	Dim( p301(pqrs), ["color","red"] );
	Dim( p230(pqrs), ["color","red"] );
	Dim( p012(pqrs2), ["color","green"]);
	Dim( p123(pqrs3), ["color","blue"]);


}

module squarePts_test( ops )
{

	pqr = randPts(3);
	sqs = squarePts( pqr );
	
    doctest( squarePts, 
	[
	  [ "pqr", is90( rearr( squarePts( pqr),[0,1,2] ) ), true
		, ["funcwrap", "is90( select( {_}, [0,1,2] ) )"]
	  ]

	  ,[ "pqr", is90( rearr( squarePts( pqr),[1,2,3] ) ), true
		, ["funcwrap", "is90( select( {_}, [1,2,3] ) )"]
	  ]

	,[ "pqr", is90( rearr( squarePts( pqr),[2,3,0] ) ), true
		, ["funcwrap", "is90( select( {_}, [2,3,0] ) )"]
	  ]
	], ops, ["pqr",pqr, "sqs", sqs]
     );
}
//squarePts_demo();
//squarePts_demo_2_debug();
//squarePts_demo_3_setsize();
//squarePts_test( ["mode",22] );
//doc(squarePts);




//=============================================================
subops=["subops", "parentops=false, subopsname='''',default=[]", "hash", "Hash",
"
 Set the ops of a sub-component of a parent (like, leg features of a
;; robot). Given a parent ops (for robot properties), a subopsname (''leg''),
;; and a default hash (like [''count'',4]), return:
;;
;;   [],      if parentops[subopsname] is false; 
;;   default, if parentops[subopsname] is true; 
;; else:
;;   update( default, parentops[subopsname])
;;
;; A case study: F below has a subunit called Arm that is not added by
;; default. Users can do F( [''arm'',true] ) to show it in predefined
;; default, Arm ops, or give runtime ops to customize it.
;;
;; module F(ops){
;;   ops= update( [ ( F's default ops here )
;;                , ''arm'',false              // disabled arm
;;                ], ops );                   // updated with runtime ops
;;   armops= subops( ops, ''arm'', [''len'',5]); // set armops default and allows
;;                                           // it be updated at runtime
;;   if(armops) Arm( armops ); // armops has one of the following values:
;;                             // false, [''len'',5], or updated version like [''len'',7]
;; } 
"
];

function subops(parentops=false, subopsname="",default=[])=
(
	hash(parentops, subopsname)==false?[]
	:hash(parentops, subopsname)==true? default	
		:update( default, hash(parentops, subopsname) )
);

module subops_test(ops)
{
	scope=[ "tire_dft", ["a",10]
		 , "car1", ["seat",2, "tire",false ]
		 , "car2", ["seat",2,"tire",true]
		 , "car3", ["seat",2,"tire",["a",8, "d",15] ]
		  ];
	car1= hash(scope, "car1");
	car2= hash(scope, "car2");
	car3= hash(scope, "car3");
	dft = hash(scope, "tire_dft");

	doctest( subops,
		[	
		 "// When tire=false :"
		, ["parentops=car1, subopsname=''tire'', default= tire_dft"
			, subops( parentops=car1, subopsname="tire", default=dft)
			,[]
			]
		,"// When tire is set to true at runtime :"
		,["parentops=car2, subopsname=''tire'', default= tire_dft"
			, subops( parentops=car2, subopsname="tire", default=dft)
			,["a",10]
			]
		,"// When tire is given a value at runtime :"
		,["parentops=car3, subopsname=''tire'', default= tire_dft"
			, subops( parentops=car3, subopsname="tire", default=dft)
			,["a", 8, "d", 15]
			]
		,"// When parentops not given:"
		,["default=tire_dft"
			, subops( default=dft)
			,["a", 10]
			]
		], ops,["scope",scope]
		);
}
//doc(subops);
//subops_test();		




//========================================================
sumto =[  "sumto", "n", "int", "Math" ,
"
 Given n, return 1+2+...+n \\
"
];

function sumto(n)= n+ (n>1?sumto(n-1):0);

module sumto_test( ops )
{
	doctest( 
	sumto
	,[ [ "5", sumto(5), 15 ]
    	 , [ "6", sumto(6), 21 ]
	 ],ops
	);
}
//sumto_test( ["mode", 22 ] );



//========================================================
sum=["sum", "arr", "number", "Math",
"
 Given an array, return the sum of its items.\\
"
];

function sum(arr)=
(
	len(arr)==0?0
	:( arr[0] + sum(shrink(arr)) )

    // [ for(i=range(arr)) 
);

module sum_test( ops )
{
    doctest( sum, 
    [
        [[2,3,4,5],sum([2,3,4,5]),14]
    ], ops );
}
//sum_test( ["mode",22] );




ERRMSG = ["rangeError"
		, "Asking {iname}={index} for *{vname}* (len={len}) in {fname}()"
		//, "{iname}={index} IN *{fname}()* OUT OF RANGE OF *{vname}* (LEN={len})"
		,"typeError"
		//, "*{vname}*(a {vtype}) IN *{fname}*() should have been a *{tname}*" 
		, "*{vname}* in {fname}() should be *{tname}*. {vtype} given " 
		];
function errmsg(errname,data)=
(
  str("# ",errname, ": ", _h( hash(ERRMSG, errname), data), " #" )
);



//========================================================
switch=["switch","arr,i,j","array", "Array",
 " Given an array and two indices (i,j), return a new array
;; with items i and j switched. i,j could be negative. If either
;; one out of range, return undef.
"];

function switch(arr,i,j)=
 let(i=fidx(arr,i)
	,j=fidx(arr,j)
	,ii=min(i,j)
	,jj=max(i,j))
(
	ii==jj
	? arr
	: concat( slice(arr,0,ii)
		  	, [arr[jj]]
		  	, slice(arr,ii+1,jj)
		  	, [arr[ii]]
		  	, slice(arr,jj+1)
		  	)

//	!isarr(arr)
//	? errmsg( "typeError"
//			,["fname","switch","vname","arr", "vtype",type(arr), "tname","array"] )
//	:!inrange(arr,i)
//	? errmsg( "rangeError"
//			,["fname","switch", "iname","i","index",i, "vname","arr", "len",len(arr)] )
//	: 
//	!inrange(arr,j)
//	? errmsg( "rangeError"
//			,["fname","switch", "iname","j","index",j, "vname","arr", "len",len(arr)] )
//	: 
//	(i>=0?i:(len(arr)+i))==( j>=0?j:(len(arr)+j)) 
//	? arr
//	:
//	concat( slice(arr,0, min( i>=0?i:(len(arr)+i)
//						 , j>=0?j:(len(arr)+j)
//						  )
//				)
//		, get(arr, max( i>=0?i:(len(arr)+i)
//					 , j>=0?j:(len(arr)+j)
//					)
//			)
//		, slice(arr, min( i>=0?i:(len(arr)+i)
//					  , j>=0?j:(len(arr)+j))+1
//				  , max(i>=0?i:(len(arr)+i)
//					  ,j>=0?j:(len(arr)+j))
//			  )
//		, get(arr, min(i>=0?i:(len(arr)+i)
//					,j>=0?j:(len(arr)+j))
//			 )
//		, max( i>=0?i:(len(arr)+i)
//			, j>=0?j:(len(arr)+j))>=len(arr)-1
//		  ? [] 
//		  : slice( arr, max( i>=0?i:(len(arr)+i)
//					    , j>=0?j:(len(arr)+j))+1
//			    )
//		)
);

module switch_test( ops=["mode",13] )
{
	arr=[2,3,4,5,6];
	doctest( switch,
	[		
		["arr,1,2", switch(arr,1,2), [2,4,3,5,6] ]
	,	["arr,0,2", switch(arr,0,2), [4,3,2,5,6] ]
	,	["arr,0,-1", switch(arr,0,-1), [6,3,4,5,2] ]
	,	["arr,-1,-5", switch(arr,-1,-5), [6,3,4,5,2] ]
	,	["arr,-1,4", switch(arr,-1,4), [2,3,4,5,6], ["rem","No switch"] ]
	,"//"
	,"// Error message when giving wrong arguments: (new as of 20140730)"
	,"//"
	,	["arr,-1,9", switch(arr,-1,9), "# rangeError: j=9 IN *switch()* OUT OF RANGE OF *arr* (LEN=5) #"] 
	,	["arr,-1,-9", switch(arr,-1,-9), "# rangeError: j=-9 IN *switch()* OUT OF RANGE OF *arr* (LEN=5) #"] 
	,	["arr,-9,-1", switch(arr,-9,-1), "# rangeError: i=-9 IN *switch()* OUT OF RANGE OF *arr* (LEN=5) #"]
	,	["''23456'',-9,-1", switch("23456",-9,-1), "# typeError: *arr*(a str) IN *switch*() should have been a *array* #"]
	],ops, ["arr", arr]
	);
}


//========================================================
subarr=["subarr","arr, cover=[1,1], cycle=true", "array", "Array",
 " Given an array (arr) and range of cover (cover), which is in the form
;; of [m,n], i.e., [1,1] means items i-1,i,i+1, return an array of items.
;;
;; Set cycle=true (default) to go around the beginning or end and resume 
;; from the other side. 
"];

function subarr(arr, cover=[1,1],cycle=true)=
 let( rngs = ranges( arr, cycle=cycle, cover=cover) )
(
 [ for(a=rngs) [for(i=a) arr[i]]]
);

module subarr_test(ops=["mode",13])
{
	arr=[10,11,12,13];
	doctest( subarr,
	[
		 ["arr", subarr( arr ), [[13,10,11],[10, 11,12],[11,12,13],[12,13,10]] ]
		,["arr,cycle=false", subarr( arr,cycle=false ), [[10,11],[10, 11,12],[11,12,13],[12,13]] ]

		,["arr,cover=[0,1]", subarr( arr,cover=[0,1] ), [[10,11],[11,12],[12,13],[13,10]] ]
		,["arr,cover=[2,0]", subarr( arr,cover=[2,0] ), [[12,13,10],[13,10,11],[10,11,12],[11,12,13]] ]
	],ops, ["arr",arr]
	);
}




//========================================================
transpose= [ "transpose", "mm", "mm",  "Array, Math" ,
"
 Given a multmatrix, return its transpose.
"
];

function transpose(mm)=
(
	[ for(c=range(mm[0])) [for(r=range(mm)) mm[r][c]] ]

//	_i_>len(mm[0])-1?[]
//	: concat( [ getcol(mm,_i_)]
//			, transpose( mm, _i_+1)
//			)
);

module transpose_test( ops )
{
	mm = [[1,2,3],[4,5,6]];
	mm2= [[1,4],[2,5],[3,6]];
	doctest
	( 
		transpose
	,	[
			[  mm , transpose(mm), mm2 ]
		,	[  mm2 , transpose(mm2), mm ]
		], ops
	);
}

//transpose_test( ["mode",22] );


//========================================================
or=["or","x,dval","any|dval",  "Hash",
"
 Given x,dval, if x is evaluated as true, return x. Else return
;; dval. Same as x?x:dval but x only appears once.
"  
];

function or(x,dval)=x?x:dval;

module or_test( ops )
{
	doctest
	(
		or
		,[
			 ["false, 3", or(false,3), 3]
			,["undef, 3", or(undef,3), 3]
			,["0,     3", or(0    ,3), 3]
			,["50,    3", or(50   ,3), 50]
		 ], ops
	);
}
//trueor_test( ["mode",22] );	



//========================================================
twistangle=["twistangle","pqrs", "number", "Angle, Geometry",
 " Given a 4-pointer (pqrs), return the angle between PQ and RS. 
;;
;;  +-----:-----+   a: 0~90
;;  :     :  S  :  
;;  :     : /   : 
;;  :     :R    :
;;  ------Q-----P 

;;  +-----:-----+   a:90~180
;;  : S   :     :   
;;  :  '. :     : 
;;  :    R:     :
;;  ------Q-----P 

;;  ------Q-----P  a: 0~ -90
;;  :     :R    :
;;  :     : '.  :
;;  :     :   S :
;;  +-----:-----+ 

;;  ------Q-----P  a: -90~ -180
;;  :    R:     :
;;  :   / :     :
;;  :  S  :     :
;;  +-----:-----+ 
;;
;;                     D   S
;;                     :  /|
;;                     : / |
;;                     :/  |   
;;                     /   |  .'
;;                    /:  .|'
;;       B        :  / :.' |
;;  P    |        : / .C---:
;;  |'.  |   :    :/.'_.-''
;;  |  '.| --:----R--'------
;;  |    |.  :  .'
;;  |    | '.:.'
;; -|----|-_.Q -------    
;;  |.'_.|'.'
;;  '----A'
;;     .'
"];
function twistangle( pqrs )=
 let( pqr= p012(pqrs), P=P(pqr), Q=Q(pqr), R=R(pqr), S= pqrs[3]
	, C= othoPt( [Q,S,R] )
	, D= projPt( S, pqr )
	, a= angle([D,C,S])	
	, quad = quadrant( [Q,R,P],S)
	, ks= [0,0,1,a,2,a,3,180-a,4,-a, 5,0,6,90,7,180,8,-90 ]
	)
( 
	 hash( ks, quad )
);

module twistangle_test(ops=["mode",13])
{
	doctest( twistangle,
	ops=ops
	);
}
module twistangle_demo()
{	
	pqrs = randPts(4);
	P=P(pqrs); Q=Q(pqrs); R=R(pqrs);
	S=pqrs[3];
	
	MarkPts([P,Q,R,S],["labels","PQRS"]);
	Chain( [P,Q,R,S ] );
	//Line0( [Q,S] );

	echo( "quad:", quadrant( [Q,R,P],S), "twistangle:", twistangle( [P,Q,R, S] ));

}
//twistangle_demo();



//========================================================
type=[ "type", "x", "str", "Type, Inspect" ,
"
 Given x, return type of x as a string.
"
];

function type(x)= 
(
	x==undef?undef:
	( abs(x)+1>abs(x)?
	  floor(x)==x?"int":"float"
	  : str(x)==x?"str"
		: str(x)=="false"||str(x)=="true"?"bool"
			:(x[0]==x[0])? "arr":"unknown"
	)
);

module type_test( ops )
{
	doctest
	(
	type,[  
		[[2,3], type([2,3]), "arr" ]
    	 , [ "[]", type([]), "arr" ]
	 , [ "-4", type(-4), "int"]
	 , [ "0", type( 0 ), "int"]
	 , [ "0.3", type( 0.3 ), "float"]
	 , [ "-0.3", type( -0.3 ), "float"]
	 , [ "''a''",type("a"), "str"]
	 , [ "''10''", type("10"), "str"]
	 , [ "true", type(true), "bool"]
	 , [ "false", type(false), "bool"]
	 , [ "undef", type(undef), undef]
    	 ],ops
	);
}
//type_test( ["mode",22] );


//========================================================
ucase=["ucase", "s","str","String",
 " Convert all characters in the given string (s) to upper case. "
];
_alphabetHash2= split("a,A,b,B,c,C,d,D,e,E,f,F,g,G,h,H,i,I,j,J,k,K,
l,L,m,M,n,N,o,O,p,P,q,Q,r,R,s,S,t,T,u,U,v,V,w,W,x,X,y,Y,z,Z",",");

function ucase(s, _I_=0)=
(
  	_I_<len(s)
	? str( hash( _alphabetHash2, s[_I_], notfound=s[_I_] )
		, ucase(s,_I_+1)
		)
	:""
);

module ucase_test( ops= ["mode",13] )
{
	doctest( ucase,
	[
		["''abc,Xyz''", ucase("abc,Xyz"), "ABC,XYZ"]
	], ops
	);
}

//========================================================
uconv=["uconv", "n, units=''in,cm'', uconv_table=UCONV_TABLE)", "number", "Math, Number",
" Convert n's unit based on the *units*, which has default ''in,cm''. 
;; The conversion factor is stored in a constant UCONV_TABLE. To use
;; something not specified in UCONV_TABLE, do this:
;;
;;   uconv(n, ''u1,u2'', factor )
;;
;; Or you can define own table:
;;
;;   table=[''u1,u2'',factor]
;;   uconv( n, ''u1,u2'', table )
;;
;; n can be a string like \"2'8''\". If so the arg *from* is ignored.
;;
;; Related:
;;
;; num(''3'6\"'')= 3.5  // in feet
;; numstr( 3.5, conv=''ft:ftin'') = ''3'6\"'' // return str
;; ftin2in(''3'6\"'') = 42 
"];

UCONV_TABLE= [ "in,cm", 2.54
			  , "cm,in", 1/2.54
			  , "ft,cm", 2.54*12
			  , "cm,ft", 1/(2.54*12)
			  , "ft,in", 12
			  , "in,ft", 1/12	
			  ];
function uconv(n,units="in,cm", uconv_table=UCONV_TABLE)=
let( 
	isftin= endwith(n, ["'","\""] ) 
	, m = isstr(n)
		  ? isftin 
			? ftin2in(n)
			: num(n)
		  :n
    , from_= isftin? "in": split(units,",")[0]
    , to_  = split(units,",")[1]
	, from = or(from_, "in")
	, to   = or(to_,"cm")
	, units= str(from, ",", to) 
	, uconv_table= isnum(uconv_table)? [units, uconv_table]:uconv_table
   	)
(
	(from==to)
	? m
	: m * hash( uconv_table
			  , units
			  , notfound="units not found") // Why this notfound doesn't show up ?
										   // The functions used, ftin2in and hash,
				                            // and everything else work fine, can't
										   // find where this error-reporting goes 
										   // wrong. The result is correct, though. 
										   // 2014.8.12	
);

module uconv_test( ops=["mode",13])
{
	doctest( uconv,
	[
	  ["3,''in,''", uconv(3,"in,"), 3*2.54, ["rem","Default 'to' = 'cm'"] ]
	  ,["3,''cm,in''", uconv(3,"cm,in"), 3/2.54 ]
	  ,["100, ''cm,ft''", uconv(100,"cm,ft"), 100/2.54/12]
	  ,"// Use own units:"
	  ,["100, ''cm,aaa''", uconv(100,"cm,aaa"), undef]
	  ,["100, ''cm,aaa'', 2", uconv(100,"cm,aaa", 2), 200, ["rem","Enter a factor"]]
	  ,["100, ''cm,aaa'', [''cm,aaa'',3]", uconv(100,"cm,aaa", ["cm,aaa",3]), 300, ["rem","Enter a table"]]
	  ,"// When n is a string like \"2'8''\", arg *from* is ignored:"
	  ,["''1'3'''', '',in''", uconv("1'3\"", ",in"), 15]
	  ,["''1'3'''', '',cm''", uconv("1'3\"", ",cm"), 15*2.54]
	  ,["''1'3'''', '',ft''",    uconv("1'3\"", ",ft"), 15/12]
	  ,["''1'3'''', '',aaa''",    uconv("1'3\"", ",aaa"), undef]
	  
 
	],ops);
} 

//========================================================
update= ["update", "h,h1", "hash", "Hash" ,
"
 Given two hashes (h,h1) update h with items in h1.
"
];

function update(h,g)=
  let( h= isarr(h)?h:[]
     , g= isarr(g)?g:[] )
  [ for(i2=[0:len(h)+len(g)-1])      // Run thru entire range of h + g
      let(is_key= round(i2/2)==i2/2  // i2 is even= this position is a key
         ,is_h = i2<len(h)           // Is i2 currently in the range of h ?
         ,cur_h = is_h?h:g           // Is current hash the h or g?
         ,cur_i = is_h?i2:(i2-len(h)) // Convert i2 to h or g index
         ,k= cur_h[ is_key?cur_i:cur_i-1 ]  // Current key
         ,overlap = kidx((is_h?g:h),k)>=0   // Is cur k-v overlap btw h & g?
      )
      if( !(overlap&&!is_h)) // Skip if overlap and current hash is g
         is_key? k
               : overlap?hash(g,k):cur_h[cur_i]    
];
      
      
//function update(h,h1,_I_=0)=
//let(  
//	 newk = h1[_I_]
//	,newv = h1[_I_+1]
//	,keyindex= index( keys(h), newk )
//	,h = keyindex>-1
//		  ? replace( h, keyindex*2+1, newv )
//		  : concat( h, [newk, newv] ) 
//	)
//(
//	_I_<len(h1)-2
//	? update( h, h1, _I_+2)
//	: h 
//);

module update_test( ops )
{

	doctest(
	 update
	  
	,[
	   [ "[1,11],[2,22]",update( [1,11],[2,22] ), [1,11,2,22]  ]
	 , [ "[1,11,2,22],[2,33]",update( [1,11,2,22], [2,33] ), [1,11,2,33]  ]
	 , [ "[1,11,2,22],[1,3,3,33]",update([1,11,2,22],[1,3,3,33]), [1,3,2,22,3,33]]
	 , [ "[1,11,2,22],[]",update([1,11,2,22],[]), [1,11,2,22]]
	 , ""
	 , " Clone a hash: "
	 , ""
	 , [ "[],[3,33]",update( [],[3,33] ), [3,33]  ]
	 , ""
	 , " Show that the order of h2 doesn't matter"
	 , ""
	 , [ "[''a'',1,''b'',2,''c'',3], [''b'',22,''c'',33]"
		, update( ["a",1,"b",2,"c",3],["b",22,"c",33] )
		, ["a",1,"b",22,"c",33]  ]
	 , [ "[''a'',1,''b'',2,''c'',3], [''c'',33,''b'',22]"
		,update( ["a",1,"b",2,"c",3], ["c",33,"b",22] )
		,["a",1,"b",22,"c",33]  ]
	 ], ops
	);
}

//update_test( ["mode",2]);




//========================================================
updates= ["updates", "h,hs", "hash", "Hash" ,
"
 Given a hash (h) and an array of multiple hashes (hs),
;; update h with hashes one by one.
;; 
;; New 2015.2.1: enter h,hs as a one-layer array: updates(hs).
;; That is, updates the hs[0] with the rest in hs
"
];

function updates(h,hs)=
    let ( _h= hs==undef? h[0]:h
        , hs= hs==undef? shrink(h): hs 
        )
(	len(hs)==0? _h
	: updates( update(_h,hs[0]), shrink(hs) )
);
//(
//	len(hs)==0? h
//	: updates( update(h,hs[0]), shrink(hs) )
//);

module updates_test( ops )
{ 
	doctest( updates	  
	,[
	   [ [[1,11,2,22],[[2,20],[1,3,3,33]]]
		,updates( [1,11,2,22],[[2,20],[1,3,3,33]] )
		, [1, 3, 2, 20, 3, 33] 
 		   
 	   ]	 
	 , [ [[1,11,2,22],[[1,3,3,33],[2,20]]]
		,updates( [1,11,2,22],[[1,3,3,33],[2,20]] )
		, [1, 3, 2, 20, 3, 33] 
 	   ]	 
	 , [ [[1,11,2,22],[[1,3,3,33],[],[2,20]]]
		,updates( [1,11,2,22],[[1,3,3,33],[],[2,20]] )
		, [1, 3, 2, 20, 3, 33] 
 	   ]	 
	 , [ [[1,11,2,22],[[1,3,3,33,2,20]]]
		,updates( [1,11,2,22],[[1,3,3,33,2,20]] )
		, [1, 3, 2, 20, 3, 33] 
 	   ]	 
	 , [ [[1,11,2,22],[[1,3,3,33],[2,20],[2,12,4,40]]]
		,updates( [1,11,2,22],[[1,3,3,33],[2,20],[2,12,4,40]] )
		, [1,3, 2,12, 3,33, 4,40] 
 	   ]
    , [ [[],[[1,3,3,33]]]
		,updates( [],[[1,3,3,33]] )
		, [1,3,  3,33] 
 	   ]
    , [ [[],[[1,3,3,33],[2,20]]]
		,updates( [],[[1,3,3,33],[2,20]] )
		, [1,3, 3,33, 2,20] 
 	   ]		 
    , [ [[],[[1,3,3,33],[2,20],[2,12,4,40]]]
		,updates( [],[[1,3,3,33],[2,20],[2,12,4,40]] )
		, [1,3, 3,33, 2,12, 4,40] 
 	   ]		
     ,""
     ,"New 2015.2.1: enter (h,hs) as a one-layer array: updates(hs). That is, "
     ," updates the hs[0] with the rest in hs"
     ,""
     , ["[[''a'',1,''b'',2,''c'',3], [''a'',11,''d'',4],[''c'',33,''e'',5] ]"
        ,updates( [["a",1,"b",2,"c",3],["a",11,"d",4],["c",33,"e",5] ])
        ,["a",11,"b",2,"c",33,"d",4,"e",5]
       ] 
     , ["[[''a'',1,''b'',2,''c'',3],[],[''c'',33,''e'',5] ]"
        ,updates( [["a",1,"b",2,"c",3],[],["c",33,"e",5] ])
        ,["a",1,"b",2,"c",33,"e",5]
       ] 
     , ["[[], [''a'',11,''d'',4],[''c'',33,''e'',5] ]"
        ,updates( [[],["a",11,"d",4],["c",33,"e",5] ])
        ,["a",11,"d",4,"c",33,"e",5]
       ]     
	 ], ops
	);
}
//updates_test( ["mode",22]);

//========================================================
vals=[  "vals", "h", "arr",  "Hash",
"
 Return the vals of a hash as an array.
"
];

function vals(h)=
(
	[ for(i=[0:2:len(h)-2]) h[i+1] ]
);
module vals_test( ops )
{
	doctest( 
	vals, [
		[ "[''a'',1,''b'',2]", vals(["a",1,"b",2]), [1,2] ]
	  ,	[ [1,2,3,4], vals([1,2,3,4]), [2,4] ]
	  ,	[ [[1,2],3, 0,[3,4]], vals([[1,2],3, 0,[3,4]]), [3,[3,4]] ]
	  ,	[ [], vals([]), [] ]
	  ], ops
	//, [["echo_level",1]]
	);
}
//vals_test( ["mode",22] );



//======================================
vlinePt=["vlinePt", "pqr,len=1", "points", "Array, Geometry, Points",
 " Given a 3-pointer pqr and a len(default 1), return a point T such that
;; 
;; (1) TQ is perpendicular to PQ 
;; (2) T is on the plane of PQR
;; (3) T is on the R side of PQ line
;; 
;;           T   R
;;        len|  / 
;;        ---| /  
;;        |  |/
;;   P-------Q
"];

module vlinePt_test( ops=["mode",13] )
{
	doctest( vlinePt, ops=ops );
}

function vlinePt(pqr,len=1)=
 let(Q=Q(pqr)
	,N=N(pqr)
	,T=N([N,Q,P(pqr)])
	)
(
	onlinePt( [Q,T], len=len )
);

module vlinePt_demo_0()
{_mdoc("vlinePt_demo_0",
"");

	pqr= randPts(3);
	P=P(pqr); Q= Q(pqr); R=R(pqr);


	T = vlinePt(pqr);
	echo("T=",T);
	//Line0( [Q,T] );
	pqt = [P,Q,T];
	Mark90( pqt, ["r",0.05, "color","red"] );
	Plane( pqt, ["mode",3] );
	MarkPts( [P,Q,R,T], ["labels","PQRT"] );
	Chain(pqt, ["r",0.05, "transp",1,"color","red"]); 
	Chain(pqr, ["r",0.15, "transp",0.5]); 
	
}
//vlinePt_demo_0();



//======================================
// The rotation needed for a line
// on X-axis to align with line-p1-p2
// 
function lineRotation(p1,p2)=
(
 [ 0,-sign(p2[2]-p1[2])*atan( abs((p2[2]-p1[2])
		                    /slopelen((p2[0]-p1[0]),(p2[1]-p1[1]))))
    , p2[0]==p1[0]?0
	  :sign(p2[1]-p1[1])*atan( abs((p2[1]-p1[1])/(p2[0]-p1[0])))]
);

//===========================================
//
// Console html tools 
//
//===========================================

_SP  = "&nbsp;";
_SP2 = "　";  // a 2-byte space 
_BR  = "<br/>"; 

function _tag(tagname, t="", s="", a="")=
(
	str( "<", tagname, a?" ":"", a
		, s==""?"":str(" style='", s, "'")
		, ">", t, "</", tagname, ">")
);

function _div (t, s="", a="")=_tag("div", t, s, a);
function _span(t, s="", a="")=_tag("span", t, s, a);
function _table(t, s="", a="")=_tag("table", t, s, a);
function _pre(t, s="font-family:ubuntu mono;margin-top:5px;margin-bottom:0px", a="")=_tag("pre", t, s, a);
function _code (t, s="", a="")=_tag("code", t, s, a);
function _tr   (t, s="", a="")=_tag("tr", t, s, a);
function _td   (t, s="", a="")=_tag("td", t, s, a);
function _b   (t, s="", a="")=_tag("b", t, s, a);
function _i   (t, s="", a="")=_tag("i", t, s, a);
function _u   (t, s="", a="")=_tag("u", t, s, a);
function _h1  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h1", t, s, a); 
function _h2  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h2", t, s, a); 
function _h3  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h3", t, s, a); 
function _h4  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h4", t, s, a); 

function _color(t,c) = _span(t, s=str("color:",c));
function _red  (t) = _span(t, s="color:red");
function _green(t) = _span(t, s="color:green");
function _blue (t) = _span(t, s="color:blue");
function _gray (t) = _span(t, s="color:gray");

function _tab(t,spaces=4)= dt_replace(t, dt_multi("&nbsp;",spaces)); // replace tab with spaces
function _cmt(s)= _span(s, "color:darkcyan");   // for comment

// Format the function/module argument
function _arg(s)= _span( s?str("<i>"
					,dt_replace(dt_replace(s,"''","&quot;"),"\"","&quot;")//"“"),"\"","“")
					,"</i>"):""
					, "color:#444444;font-family:ubuntu mono");


//===========================================
//
//           Theories
//
//===========================================




//===========================================
//
//       Version and history 
//
//===========================================

old_scadex_ver=[
["20150202_1", "getsubops: fixed warning (beg shouldn't be larger 
than end) by inserting let(h=isarr(h)?h:[],g=isarr(g)?g:[]) to update()"
] 
,["20150201_1", "updates() now can take one argument (instead of 2); Still debugging getsubops about beg shouldn't be larger than end warning."] 
,["20150131_1", "getsubops revised; tested"] 
,["20150130_1", "kidx(); Updated update();"] 
,["20150125_5", "Cone() new arg: twist."] 
,["20150125_4", "arcPts() new arg: twist."] 
,["20150125_3", "Arc() done."] 
,["20150125_2", "angleBisecPt: new arg len, ratio; hash:let notfound be shown when value is set to undef; Chainf(): fix bug."] 
,["20150125_1", "Chainf(): formatted Chain; Arc in progress"] 
,["20150124_1", "Change Arrow args from arrow to head. Text() in progress"] 
,["20150123_1", "Dim2's guides and arrows seem to be done. "] 
,["20150122_1", "hashex done. getsubops seems tested ok, checking if can use in Dim2 "] 
,["20150121_2", "getsubops in progress. hashex in progress "] 
,["20150121_1", "Dim2 in progress. "] 
,["20150120_1", "Arrow doc and demos. "] 
,["20150119_2", "Arrow (the line and arrow faces not yet aligned. "] 
,["20150119_1", "Cone(), w/new feature: set markpts on the fly"] 
,["20150117_2", "CurvedCone_demo_1() cleanup"]
,["20150117_1", "getSideFaces(), tested"
              , "getTubeSideFaces(), tested"
              , "mergeA(), tested"
              , "CurvedCone, CurvedCone_demo_1()"
               ]
,[ "20150116-1", "CurvedCone first step done. Check CurvedCone_demo_1()", 
    "Change some argument name r to len" 
]
,[ "20140921-1","Change args of isOnPlane()" 
]
,[ "20140918-1","New:roll(arr,count)" 
]
,[ "20140916-1","join(): change default sp from , to empty str."
			,"numstr(): change arg maxd back to dmax." 
]
,[ "20140914-1","New ranges(): similar to range, but return array of arrays."
			, "Both range() and ranges() now return only indices. No more return obj content. Both need docs." 
]
,[ "20140912-1","range(): Revert to entire 0909-1. Re-design range to have simpler args: range(i,j,k, cycle)." 
]
,[ "20140910-2_segmentation_fault","range(): revert this func back to 0909-1, but it causes segmentation fault. Next version: copy entire 0909-1" 
]
,[ "20140910-2","range(): revert it back to 0909-1." 
]
,[ "20140910-1","Re-design range() for partial obj ranging: not yet succeed but already very very slow (too much condition checks in each call). Will revert it back to 0909-1 in next ver." 
]
,[ "20140909-1","Re-design range(): remove args: maxidx, obj; allow cycle= i,-i" 
]
,[ "20140901-1","New:vlinePt()." 
]
,[ "20140831-2","onlinePt: change arg name dist to len." 
]
,[ "20140831-1","Misc improvements. (mainly Rod())" 
]
,[ "20140825-1","New: twistangle()" 
]
,[ "20140823-1","New arg s for angle().","New quadrant()" 
]
,[ "20140822-3","faces() demo of shape ''tube''. Pretty cool." 
]
,[ "20140822-2","New: rodPts(). Makes Rod() uses it."
			, "New: rodfaces(), faces()" 
]
,[ "20140822-1","Rod(): Fix another rotation bug. Doc done, too."
			, "LabelPts: default prefix changed from P to '', begnum from 1 to 0" 
]
,[ "20140821-3","Rod(): Fix rotation bug found earlier today"
]
,[ "20140821-2","Rod(): New args markrange, markOps, labelrange, labelOps; remove showsideindex",
	"Rename mdoc() to parsedoc()"
]
,[ "20140821-1","Bug in Rod : sometime rotate to wrong side. See Rod_demo_8_rotate()"
]
, [ "20140820-1","New mdoc(), _mdoc()"
			  , "Fix major Rod bugs (guess so). More demo with _mdoc"
]
,[ "20140819-1","New Rod arg: rotate; more Rod demos (including gears)"
]
,[ "20140818-3","New: subarr()"
]
,[ "20140818-2","Revised range(): add new args: obj and isitem"
]
,[ "20140818-1", "Revised fidx(o,i): o can be an integer"
			 ,"Revised range(): remove arg obj, add new arg cover and maxidx"
]
,[ "20140817-2", "New: lpCrossPt()"]
,[ "20140817-1", "Rename: rodfaces() to rodsidefaces()"
			 , "New: Rod()"]
,[ "20140816-3", "New: rodfaces()"]
,[ "20140816-2", "New arcPts, good implementation. "]
,[ "20140816-1", "New for LabelPts: args: prefix, suffix, begnum, usexyz; Use new builtin text()"]
,["20140815-1","Done with the migration to OpenSCAD 2014.05.31 snapshot in terms
			of doctest behaviors. Need to fix shape making. Somehow it seems that
			the rotation of points goes wrong."]
,["20140814-1","New:dels()"
	, "Improved: isequal(), now can take var of non-number. tests done"
	, "is0: doc and tests done."]
,["20140812-1","Fix bug: endwith(), istype(). Doc and tests done."]
,["20140811-2","New: begwith and endwith can check if beg/end with any of [a,b,...] ", "Working on: new istype(), improving uconv()"]
,["20140811-1","New: splits()", "Rename: trueor()=> or()"]
,["20140810-1","Disable neighbors(). The new range() can cover it."
			, "consider to disable rearr(). The new list comp can handle it.==> maybe not"
			, "Make range() accept str as the 1st arg as well, so range(''abc'')=[0,1,2]. This is used in reversed()"
			, "New arg for fidx: fitlen"
			, "Simplify slice code, allowing reversed slicing" 
			, "New: joinarr()"]
,["20140809-1","Rename all *iscycle* to *cycle*"
			, "Del range() and copy range2() code to range()"
			, "range2(): new arg: obj,cycle. Doc and test done."
]
,["20140808-3","Remove fitrange(). It can be replaced by fidx()"
			,"New: range2(), much elegantly written than range()."
			,"Re-evaluating the need of neighbors ... it might be able to use range2() instead"
]
,["20140808-2","New function: fidx(). This should be very powerful. "
]
,["20140808-1","Disabled for future removal: mina, maxa, inc, hashi, hashkv"
			,"Renamed: multi()=>repeat(), midCornerPt()=>cornerPt()"
]
,["20140807_1", "START CODING WITH 2014.05.31/Linux 64bit"
			, "Remove addv (too easy to code with list comp so no need a func)"
			, "New arg for del(): *count*."
			, "New func: fitrange() (consider to replace inrange in the future )" 
			, "Re-chk code for use of OpenScad_DocTest for doc testing --- down to hash()"
		    
]
,["20140730_1", "New: prependEach(), permute()"
]
,["20140729_1", "New:switch()", "New: ucase(), lcase()"
			,"New: Error message reporting in switch(). Consider apply it to other functions in the future"
]
,["20140715_5", "New:cubePts(), Cube() done (Cube() needs doc)"]
,["20140715_4", "squarePts(): fix runtime error. "]
,["20140715_3", "Dim(): Fixed bug (S= squarePt(pqr,[''mode'',4]): remove mode. "]
,["20140715_2", "New: addv()"]
,["20140715_1", "New optional args p,q for squarePts() to set sizes." 
             , "_TODO_: to change boxPts from anti-clockwise to clockwise for pqr polyhedron geometry making. --- may not need it after squarePts() has p,r "]
,["20140714_1", "New: p012(pts) ...p320(pts) 24 funclets"
			, "New: p2(), p3(), p4() 3 funclets" ]
,["20140713_2", "numstr(): change arg dmax to maxd; Dim(): new arg maxdecimal." ]
,["20140713_1", "New args for Dim(): conv, decimal." ]
,["20140712_1", "numstr() done." ]
,["20140711_4", "New: uconv() --- need more tests." ]
,["20140711_3", "num() now can convert f'i'' to feet." ]
,["20140711_2", "New: ftin2in()" ]
,["20140711_1", "New: num(numstr)=>num" ]
,["20140710_1", "Working on: new: numstr() for Dim() to use" ]
,["20140709_2", "rand() can take an array or string, return one item randomly." ]
,["20140709_1", "Introducing groupdoc feature and apply it to addx/addy/addz" ]
,["20140708_3", "addx, addy, addz improved, plus their docs and tests. Remove add_z_to_pts" ]
,["20140708_2", "rename doctest to doctest_old, doctest2 to doctest; MOVE this new doctest to a new file scadex_doctest.scad" ]
,["20140708_1", "doctest2: simplify doc from fname=[[fname,arg,retn,tags],doclines] to [fname,arg,retn,tags,doclines]; also using ; instead of \\ for linebreak in doclines." ]
,["20140707_2", "More demos for anyAnglePt." 
]
,["20140707_1", "anglePt bug fixed: failed when a=90 (detected by anyAnglePt_demo_4();" 
]
,["20140703_1", "neighbors(), neighborsAll(): change default of cycle to true"
		   , "normalPts(): fix bug (arg *len* was written as dist in the code)"
		   , "normalPt: change default:len=undef, new: reversed=false" 
]
,["20140701_1", "angle(): use simpler formula.","anyangle()", "P(),Q(),R(),N(),M()"
			 ]
,["20140630_1", "randOnPlanePt: new arg a (for angle); and change the center of pts from incenterPt to Q" ]
,["20140629_1", "anglePt(): change angle to aPQR (originally aQPR) and reaname arg from (pqr, ang, len) to (pqr,a,r).", "Fix randOnPlanePts (it uses anglePt) accordingly" ]
,["20140628_2", "normalPt(), normalPts()"]
,["20140628_1", "expandPts(). cycle=false needs fix."]
,["20140627_4", "neighbors(), neighborsAll()"]
,["20140627_3", "mod()", "remove increase(). Has an inc() already."]
,["20140627_2", "get() got a new arg cycle", "In work: intersectPt(), expand()"]
,["20140627_1", "Output scadex version info when loaded."]
,["20140626_5", "LabelPts_demo_2_scale()"]
,["20140626_4", "increase(pts,n=1)", "LabelPts and MarkPts_demo() can set labels=range(pts) instead of range(len(pts))"]
,["20140626_3", "LabelPts Bug fixed.(demo with LabelPts_demo() and MarkPts_demo())"]
,["20140626_2", "MarkPts: Now can set labels", "LabelPts Bug found: fail to output label when pts contains only one item."]
,["20140626_1", "ishash()"
			, "Improved LabelPts arguments (shift)"
			, "range can do range(arr)"]
,["20140625_5", "Pie() 2D"]
,["20140625_4", "projPt() code, doc and test done."]
,["20140625_3", "Normal() new argument:reverse; Also show arrow but not just line."]
,["20140625_2", "Revising Arc()=> failed. But made a good 2D pie."]
,["20140625_1", "between() code, doc and test done."]
,["20140624_7", "randRightAnglePts improved (allows for dist= a point or an integer."]
,["20140624_6", "Bugfix for pick() and shuffle()."]
,["20140624_5", "shuffle() code, doc and test done."]
,["20140624_4", "pick() code, doc and test done."]
,["20140624_3", "range() code, doc and test done. "]
,["20140624_2", "randRightAnglePts() improved to allow fixed sizes right tria. "]
,["20140624_1", "randRightAnglePts() done. "]
,["20140623_4", "det(), RMs()", "Working on randRightAnglePts()"]
,["20140623_3", "Fix randOnPlanePt(), randOnPlanePts()"]
,["20140623_2", "Bug fix for anglePt()"]
,["20140623_1", "p021, p120, p102, p201, p210", "rearr()", "incenterPt()", "Bug in doctest when mode= 2"]
,["20140622_3", "randInPlanePts(), randOnPlanePt, randOnPlanePts():this one needs fixes"
]
,["20140622_2", "rand()", "p01() ... p03() ... p30()", "randInPlanePt()"
]
,["20140622_1", "lineCrossPt(), crossPt()", "dot()", "is0()", "prodPt()", "addx, addy, addz, dx, dy, dz"
]
,["20140620_1", "Default *closed* param in Chain set to true"
 , "Mark90 seems to have false-positive error"
 , "making anglePt() ==> doesn't work very well. Need more work"
 , "include PGreenland's TextGenerator."
 , "COLORS for default colors. Set LabelPts default colors to COLORS"
]
,["20140619_3", "Plane improved: len(pqr) can be >3. But all faces connect to pqr[0]"]
,["20140619-2", "LabelPt(), LabelPts(). Both need doc. "
  ,"randPt, randPts x,y,z range doesn't seem to work", "LabelPts()" ]
,["20140619-1", "ColorAxes()"]
,["20140618-1", "Dim(), needs fixes on the angle when vp=false"]
,["20140613-5", "Rename: dx=>distx, dy=>disty, dz=>distz."]
,["20140613-4", "Done rewriting doc and test to fit new doctest."]
,["20140613-3", "new func: select()"
		,"Rewriting doc and test to fit new doctest ... down to split()"]
,["20140613-2", "newx(), newy(), newz()", "tests for isOnlinePt, planecoefs"
		,"Rewriting doc and test to fit new doctest ... down to planecoefs()"]
,["20140613-1", "New: isOnPlane()"
		,"Rewriting doc and test to fit new doctest ... down to Plane()"]
,["20140612-2", "onlinePt takes only 3d pts.", "Random tests for onlinePt()"
		,"Rewriting doc and test to fit new doctest ... down to onlinePts()"]
,["20140612-1", "Done migration from index(x,o) to index(o,x)."]
,["20140611-3", "About to make a big change to change index(x,o) to index(o,x) to be 
consistent with other functions."]
,["20140611-2", "doctest: change arg from (doc,recs=[], ops=[], ops2=[]) 
to (doc,recs=[], ops=[], scope=[]). So we can do:
 doctest(doc, recs, ops, [''i'',3])
but not 
 doctest(doc, recs, ops, [''scope'',[''i'',3]] )
"]
,["20140611-1", "doctest: for functions difficult to test (like, _fmt), testcount is 0, 
so do not show 'Tests:', show 'Usage:' instead."
]
,["20140610-5", "scadex_doctest: some improvements."
			, "Rewriting doc and test to fit new doctest ... down to echoblock()" ]
,["20140610-4", "scadex_doctest: some improvement on its header" ]
,["20140610-3", "doctest(): header adds some highlighting; need re-factoring but we'll leave it for now. " ]
,["20140610-2", "doctest() done. " ]
,["20140610-1", "Rename scadex_test to scadex_doctest and improve header, footer" ]
,["20140609-3", "Fine-tuning doctest()"
		, "On the way to change doctest mode 0 to list all func names" ]
,["20140609-2", "DT_MODES; New doctest mostly done. Needs only minor adjustment. " ]
,["20140609-1", "Combining doc() and test() into doctest() about complete. " ]
,["20140608-1", "Combine doc() and test() into doctest(). They were previously
together but separated around 5/29. Put them back to doctest(), and aiming to
have doc() and test() call doctest() with specific parameters." ]
,["20140607-1", "Using concat (instead of update) to setup ops in test(). Will gradually adapt this pattern for all modules." ]
,["20140606-3", "New module Normal(pqr)", "Fix bug in angle() for angle >90"
			, "MarkPts: Fix err that [''grid'',[ops]] doesn't have effect" ]
,["20140606-2", "New ops *funcwrap* for test. Chk angleBisectPt_test() for example"]
,["20140606-1", "New func angle(), _fmth() ", "test() uses isequal() for number comparison" ]
,["20140605-4","Rename h_() to _h(), s_() to _s()"]
,["20140605-3","test() refined."]
,["20140605-2","New func subops(); test() now can show doc (when [''showdoc'',true])."]
,["20140605-1","New function _getDocLines(), so doc can be called to either doc() or test()."]
,["20140604-1","doc for test() finished."]
,["20140603-1","Rename midAnglePt to angleBisectPt"
	,"Rename pts_2d_2_3d to add_z_to_pts"
	,"Done revising doc of each funcmod for new doc() from 
      top down"]
,
["20140601-2","New: isbool, isequal", "Remove ishash, LineNew"
	,"Disable midAngleCrossLinePts()"
	,"Revising doc of each funcmod for new doc() from 
      top down ... to midAnglePt() "]
, 
["20140601-1","Revising doc of each funcmod for new doc() from 
              \\ top down ... to isarr() "]
,["20140531-1","Remove fontPts(), hasht()", "Rename hashitems to hashhkvs"
		, "When showing doc header, replace _ with ＿ (_ -> ＿) to make it higher" 
		, "Revising doc of each funcmod for new doc() from top down ... to hashkvs "]
,["20140530-7","Remove Box()"
		 , "Revising doc of each funcmod for new doc() from top down ... to echoblock."]
,["20140530-6","Revising doc of each funcmod for new doc() from top down ... to Block."]
,["20140530-5","doc(): better algorithm to speed up. Also allows for correct \\
	display of mono-spacing text. We finally have a system that displays code and \\
	arranged ascii graph correctly in both the code editor and console. "]
,["20140530-4","doc(): new doc format that uses double back-slash \\
	to indicate line-breaking", "_tab(): convert tab to html spaces"]
,["20140530-3","test() done, including scoping for test records "]
,["20140530-2","test() mostly done. "]
,["20140530-1","test() basic done. Still buggy."]
,["20140529-3","Start to seperate doctest into doc() and test(). doc() done."]
,["20140529-2","Basic feature of doctests works fine."]
,["20140529-1","Rename fmt to _fmt. Failed to make deep fmt into array."]
,["20140528-4","console html tools", "Working on doctest2", "fmt()"]
,["20140528-3","onlinePts()"]
,["20140528-2","PtGrids for multiple points. Note that the 'scale' feature in PtGrid is disabled now. "]
,["20140528-1","Disgard hashif; add new arg to hash: if_v, then_v, else_v"]
,["20140527-3","hash: fix default argument recursion error", "working on hashif()"]
,["20140527-2","Mark90 auto adjusts the size of L to fit small pt xyz"]
,["20140527-1","Remove PtFrame; Remove the DashLine style op in PtGrid"]
,["20140526-1","Rename PtGrids to PtGrid and significantly simplies it to reduce process time, including change the use of DashLine to Line"]
,["20140525-1","accumulate()", "onlinePts()"]
,["20140524-1","randPts() allows setting x,y,z = 0 to put pt on plane or axis."]
,["20140522-6","Rename Line0 to Line_by_x_align and LineNew to Line0"]
,["20140522-5", "Dashline: Fix bug of over-draw when dash > line length  " ]
,["20140522-4", "LineNew()." ]
,["20140522-3", "RodCircle: done. Seems rodrotate in all conditions are good." ]
,["20140522-2", "RodCircle:Fix problems with the rodrotate --- almost" ]
,["20140522-1", "RodCircle, almost done", "inc()", "pts_2d_2_3d()", "Fixed arcPts: at a=360, should remove the last one, but not the first" ]
,["20140521-3", "randc() for random color", "OPS as a common ops for all shapes" ]
,["20140521-2", "New func: updates(h,hs) for multiple updates."
			, "Remove the feature of multiple hashes in update." ]
,["20140521-1", "Improved update: can update up to 3 hashes at once" ]
,["20140520-4", "Working on : More Rings; improving update" ]
,["20140520-3", "One more demo for Ring" ]
,["20140520_2-2", "Done docs for new hash design. Ready to merge back to scadex" ]
,["20140520_2-1", "Found that all files before this have wrong year tag (2013 should have been 20140. Embarrassing.", "Finish the doc for rotangles_p2z and rotangles_z2p" ]
,["20130518-3", "New: hash2 using [k,v,k,v ...] but not [[k,v],[k,v]...] as data. " ]
,["20130518-2", "Ring: add feature to mark rod colors with ''markrod''", "Ring_demo() --- weird circle line around the line through. " ]
,["20130518-1", "Line: new arg extension0/extension1 for line extension", "arcPts: add ''a==360&&_i_==0?[]:'' to skip _i_=0 when a=360"
			,"shortside(),longside()" ]
,["20130517-4", "New module Ring()"]
,["20130517-3", "New function dist(pq) (same as norm(p-q))", "onlinePt got new argument *dist*"]
,["20130517-2", "Rename all rm (rotation matrix) to RM."]
,["20130517-1", "Let rm2z uses rotangles_p2z"]
,["20130516-3", "Revising doc down to reverse()", "replace(): add replacing array item"]
,["20130516-2", "Mark90()"]
,["20130516-1", "is90()"]
,["20130515-1", "Found Line's bug source in rotangles_z2p( Not in rotangles_p2z). Fixed"]
,["20130514-5", "Fix bug (unable to rotate [x,y,0] to z) in rotangles_p2z. The bug in Line still there"]
,["20130514-4", "The bug in Line could be due to the new-found bug in rotangles_p2z -- unable to rotate [x,y,0] to z-axis. "]
,["20130514-3", "Revising function docs to use new doctest"
  , "Found bug in Line (but not in Line0): unable to draw lines on xy-plane (like [[1,1,0],[-1,2,0]]. "]
,["20130514-2", "Revising function docs to use new doctest"
  , "Add *markpts* argument to Line0 and Line"]
,["20130514-1", "Revising function docs to use new doctest", "Rename getd() to trueor()", "Make hash return [v1,v2...] when item = [k,v1,v2...]", "Rename the argument *strick* in ishash to *relax"]
,["20130513-6", "Revising function docs to use new doctest", "Remove h_ feature from echo_ and create a new func echoh"]
,["20130513-5", "Revising function docs to use new doctest", "Func del allows byindex" ]
,["20130513-4", "Revising function docs to use new doctest", "Rename countType??? to count???" ]
,["20130513-3", "doctest: fine tuning; Revising function docs to use new doctest" ]
,["20130513-2", "doctest: fine tuning; Revising function docs to use new doctest" ]
,["20130513-1", "doctest: fine tuning, looks good" ]
,["20130512-4", "doctest: fine tuning, new arg: mode" ]
,["20130512-3", "doctest done, remove DT_old and doctest3" ]
,["20130512-2", "doctest3 clean up", "rename: doctest=>DT_old, doctest3=>doctest" ]
,["20130512-1", "doctest3, mainly based on doctest, works fine." ]
,["20130509-2", "sum(arr)","Trying doctests, good idea, almost done, but extremely slow, maybe more than 100x slower than doctest, has to give up" ]
,["20130509-1", "arrblock()" ]
,["20130507-3", "Remove Arrow()", "Adding thickness for plane(working)" ]
,["20130507-2", "Plane: allows for changing frame settings " ]
,["20130507-1", "randPt() (only one pt)" ]
,["20130506-1", "Block " ]
,["20130505-4", "Plane: done mode 1 ~ 4 " ]
,["20130505-3", "squarePts()" ]
,["20130505-2", "Plane: mode 1 done" ]
,["20130505-1", "Chain: change argument pattern to ops" ]
,["20130504-2", "Plane: allows 4-side plane", "midPt()", "midCornerPt()" ]
,["20130504-1", "Line: failed to make touch work","Rename Surface to Plane" ]
,["20130503-4", "Line: found mechanism to find the p3rd -- the pt on surface. Still under construction." ]
,["20130503-3", "Line: let shift apply not only to block but to line, too", 
				"Remove Line2. In Line, rename blockAnchor to shift" ]
,["20130503-1", "Line style block: allows any shift" ]
,["20130502-4", "echom(modulename).", "Detailed Line_demo_???"
			, "Line:Fix rotate in block style" ]
,["20130502-3", "Del Line, rename Line3 to Line, style=block to be fixed."]
,["20130502-2", "module Line0, to replace Line as the simple line module."]
,["20130502-1", "Fix Line3 (same problems also in Line2) head/tail at wrong side when switched"]
,["20130501-2", "Working on Line3 head/tail cone"]
,["20130501-1", "rotangles_z2p() made with RMz2p_demo(). Leave RMz2p_demo for future references", "Rename rot2z_angles to rotangles_p2z"]
,["20130428-1", "rmx(), rmy(), rmz(), rm2z(), rot2z_angles()"]
,["20130425-3", "Complete PtGrid new scale feature and doc"]
,["20130425-2", "hasAny()", "arrItcp()"]
,["20130425-1", "has()", "Add 'scale' to PtGrid()"]
,["20130424-1", "Remove dot(pq) ==> simply p*q; PtGrid takes new arg 'scale'"]
,["20130423-4", "Surface()","distPtPl()", "dot(pq)"]
,["20130423-3", "Line2: Improved. New shift:corner2."]
,["20130423-2", "Line2: Improve doc."]
,["20130423-1", "Line2: Rename style panel to block."]
,["20130422-6", "Add new style panel to Line2"
		     , "randPts() take new argument r."]
,["20130422-5", "Fix Line2 head/tail color and transp issues."]
,["20130422-4", "Re-organize Line2 examples."]
,["20130422-3", "Bugfix Line2."]
,["20130422-2", "Add fn arg to DashLine, Line"
			,"Fix arrow position problem in Line2" ]
,["20130422-1", "Rename markPts to MarkPts. Improve MarkPts()"]
,["20140421-1", "markPts(), randPts()"]
,["20140420-3", "Line2(): new head/teail style: cylinder"]
,["20140420-2", "Improved Line2()"]
,["20140420-1", "del()", "Raname: arrmax=>maxa, arrmin=>mina", "Line2()"]
,["20140419-3", "minPt()"]
,["20140419-2", "minxPts(), minyPts(), minzPts()"]
,["20140419-1", "mina(),maxa()"]
,["20140418-5", "Improved Box()"]
,["20140418-4", "Add a strick flag to ishash()"]
,["20140418-3", "Improve update: item order of h2 doesn't matter."]
,["20140418-2", "hashi()", "hashkv()"]
,["20140418-1", "Rename hashd to hasht", "New hashd()"]
,["20140417-3","Box()."]
,["20140417-2","DashBox()."]
, ["20140417-1","boxPts() improved."]
,["20140416-3","boxPts()"]
,["20140416-2","getd()", "hashd()"]
,["20140416-1","Remove DottedLine()", "Modify PtGrid to take hash argument", "PtGrid_test()"]
,["20140415-4","Modify DashLine to take hash argument"]
,["20140415-3", "echo_()"]
,["20140415-2","Rename rep() to s_()", "h_()"]
,["20140415-1", "normal_demo()","Allow wrap in index()"]
,["20140413-1", "PtGrid()"]
,["20140412-1","Rename Lines to Chain", "scadex_demo()", "DashLine()","DottedLine()"]
,["20140409-2","normal()", "planecoefs()"]
,["20140409-1","getcol()", "transpose()","Rename semiAnglePt to midAnglePt"
			, "Rename crossLinePts to othoCrossLinePts"
			, "Rename crossPts to begCrossLinePts" ]
,["20140406-5", "othoCrossLinePts()", "semiAnglePt()"]
,["20140406-4","Line_test() and _demo()"
			, "Use convert_scadex.py to create scadex.scad and scadex_test.scad" ]
,["20140406-3", "Fix othoPt", "Line() module"]
,["20140406-2","Restructure the code layout a bit to make them consistent"]
,["20140406-1","disable dist()", "Define doc of each function with name of it"]
,["20140405-1", "Change [_] in rep() to {_}", "add echo_level arg to doctest"
	, "dx(),dy(),dz()","linecoef", "slope()", "othoPt()", "begCrossLinePts()"
	, "Rename arcPoints to arcPts"]
,["20140402-1", "Rename curvePoints() to arcPoints"]
,["20140401-2", "reverse();"]
,["20140401-1", "Hey it's April Fool!; curvePoints();In shift(), change arg i to _i_;"]
,["20140326-2", "Remove test_run() (use doctest instead); Update doc for doctest(); scad_ver" ]
,["20140326-1", "doctest(); Rename items() to vals(); Remove stepdown()" ]
,["20140324-1", "multi_test(); echoblock()" ]
,["20140321-2", "shrink()" ] 
,["20140321-1", "stepdown()" ]
,["20140320-3", "Trying to add doc feature in test_run(), under construction." ]
,["20140320-2", "Allows update to add single [k,v] pair more easily
			get(o,i) now get(o,i,[j]) that can retrieve multiarray data." ]
,["20140320-1", "more examples for update()." ]
,["20140319-6", "update(). Surprisingly easy." ] 
,["20140319-5", "ishash();delkey()" ]
,["20140319-4", "bugfix: test_run(): make sure no * is shown when the test-specific 
			asstr is set to false in cases where function-wise asstr is true;
			Setup norm_test() to demo the use of test_run() (asstr, rem, etc);
			Add indent to output when test fails in test_run()" ]

,["20140319-3", "countType(); Rename count???Items to count???Type, and use countType( except countNum )" ]
,["20140319-2", "Only int allowed in i in get(o,i) now;
			 slice(s,i,j)=undef when i,j order wrong" ]
,["20140319-1", "inrange_test()" ]
,["20140318-1", "keys()" ]
,["20140315-1", "Start using 2013.03.11 with concat feature turned on; 
           bugfix: previous ver doesn't show * for test-specific asstr;
           slice works on array; 
           split() " ]
,["20140313-1", "int(),countArr(),countInt(), countNum(); Add "*" for asstr" ] 
,["20140312-2", "Re-doc for test_run(); minor bug fixes." ]
,["20140312-1", "1.Restructure again for easier test cases writing for user." ]
,["20140311-1", "1.Restructure test arguments to allow for 'asstr' and 'rem' options
		    2. One test_echo() for all (including those with optional args)" ]
,["20140309-3", "rename from tools to scadex" ]
,["20140309-2", "More doc for the functions. " ]
,["20140309-1", "Remove findw (use index instead)" ] 
,["20140308-3", "more thorough doc." ]
,["20140308-2", "doc for test tools" ]
,["20140308-1", "test features done; code structured" ]
,["20140307-4", "test_echo1(), test_echo2()" ]
,["20140307-3", "rep()" ]
,["20140307-2", "make index() take string. deprecricating findw()." ]
,["20140307-1", "fix bugs in findw." ]
];

            
            
scadx_ver=[
["20150204-1", "Migrated from scadex."]
];

SCADX_VERSION = scadx_ver[0][0];

echo_( "<code>||.........  <b>scadx version: {_}</b>  .........||</code>",[SCADX_VERSION]);
echo_( "<code>||Last update: {_} ||</code>",[join(shrink(scadx_ver[0])," | ")]);    