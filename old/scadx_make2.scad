include <../scadx/scadx.scad>
include <../doctest/doctest.scad> 
 
/*
  scadx_make.scad
  
  Building OpenSCAD objects in an OOP (Object-Oriented Programming) way.
  
  * An object is represented by a data structure <obj>, which is a hash 
    containing even number of items, representing a set of key:val pairs.
  
    <obj>: [ "units", <shape> || [<shape>,...] ||  <part> || [<part>,...]  
           , <mod>
           , "copies", [<mod>,...]
           , "cutoffs", [<obj>,...]
           ]
         
    <shape>: [ "shape", "cube"|"cylinder"|"sphere"|"poly"
             , "def", <param>
             , <mod> 
             , "copies", [<mod>,...]
             , "cutoffs", [<obj>,...]
    	     ]
    	     
    	     <param>: type hash, parameters for building shape
    	     
    <part> : [ "part", <partname>
             , <mod> 
             , "copies", [<mod>,...]
             , "cutoffs", [<obj>,...]
    	     ]  	     
    	     
    	     <partname>: string as name of part template

    <mod> : a optional modifier, an embedded hash containing key:val to modify 
            the properties of its parent.
            
            , "color", <color>
            , "ops", <ops>
            , "islogging", 0|1
            , "visible", 0|1
            
            <color>: type string ( "red" ) or array ( ["red", 0.5] )
                
            <ops>: type hash for operations:
            
                  [ "t"|"transl", [x,y,z]
                  , "x"|"y"|"z", n
                  , "rot", [x,y,z]
                  , "rotx"|"roty"|"rotz", n
                  , "mirror", pt|line|plane
                  ]
       
*/

    //LOG_PARAMS= ["LOG_LEVEL",10]; 
    //
    //LEVEL_ASSEM= 1;
    //LEVEL_COPY= 2;
    //LEVEL_PART= 3;
    //LEVEL_CUTOFF= 4;
    //LEVEL_MARKPT= 5;
    //LEVEL_DIM = 5; 
    
   RESERVED_SHAPE_NAMES=["cube","sphere","cylinder","poly"];

   ISLOGGING= true;
   LOGLEVEL = 0;
   
  
module Draw_a_bare_shape( shname, def, pts=[], mod=[], loglevel=LOGLEVEL, loglayer=4 )
{
  /* A bare shape doesn't take into account of "copies" and "cutoffs"
  
     <shname>  : cube|sphere|cylinder|poly
     <def> : list of parameters
     <pts>     : optional
     <mod>     : [ "ops", [...]
                 , "color", 
                 ]
  */
  module _log(s,mode){ log_(s, loglayer, mode);} 
  module _log_b(s,mode){ log_b(s, loglayer, mode);} 
  module _log_e(s,mode){ log_e(s, loglayer, mode);} 
  
  islogging = hash(mod, "islogging",0) || loglayer <= loglevel ;
   
  //echo(shape_islogging = hash(mod, "islogging")
      //, loglayer=loglayer, LOGLEVEL=LOGLEVEL, islogging=islogging ); 
      
  if(islogging) 
  {
  _log_b(_s("Draw_a_bare_shape(shname=<b>\"{_}\"</b>)"
            ,_Make_get_name_and_colorMark(shname, color[0])));
   
   _log(["shname", shname] );
   _log(["added mod", mod]);
  }
   
   ops = hash(mod, "ops",[]);
   pts = transPts( pts, ops );
   _color = hash( mod, "color", ["gold",1] );
   color = isstr(_color)?[_color,1]:_color;
   
   //visible = hash( mod, "visible", 1);
    
   //if(visible)
   //{
     color( color[0], color[1] )
     if(shname=="poly") 
     {
        faces = hash(mod, "faces", faces(4));
      
        if( islogging )
        {
        _log( "<u>Drawing a <b>\"poly\"</b></u> using polyhedron()", loglayer );
        _log( ["pts", pts ], loglayer);
        _log( ["faces", faces ], loglayer );
        } 
       
        polyhedron( pts, faces ) ; 
     }
     else
     { 
             
       if( islogging )
       {
       _log( _s("<u>Drawing a <b>\"{_}\"</b> using Primitive()</u>", shname));
       _log( ["def: ", def] );
       
       }
             
       Transforms( ops )
       Primitive( shname, def ); 
     }
   //}  
   //else{
   //
    //_log("shape.visible is false, skip making"); 	
   //}  
   
   if(islogging) _log_e("Leaving Draw_a_bare_shape()");

}
     
module Draw_a_shape( objname, shape, mod=[], partlib = []
                   , loglevel=LOGLEVEL, loglayer=3)
{
  /* <shape>:
  
   [ "shape", <shName>
   , "def", [...]
   , "ops", [...]
   , "copies", [...]
   , "cutoffs", <obj>
   ]   
  */
  module _log(s,mode){ log_(s, loglayer, mode);} 
  module _log_b(s,mode){ log_b(s, loglayer, mode);} 
  module _log_e(s,mode){ log_e(s, loglayer, mode);} 
  
  islogging = hash(shape, "islogging",0) || loglayer <= loglevel ;
  
  //echo(shape_islogging = hash(shape, "islogging")
      //, loglayer=loglayer, LOGLEVEL=LOGLEVEL, islogging=islogging ); 
      
  if(islogging)
  {
   _log_b(_s("Draw shape <b>{_}</b> with Draw_a_shape(), def={_}"
           ,[_Make_get_name_and_colorMark(objname
                     , hash(mod,"color", hash(shmod, "color", "gold")))
            ,def]));

  _log(["shape",shape]);
  _log(["added mod to this shape", mod]);
  }
  
  //echo(loglevel=loglevel, loglayer=loglayer, islogging=islogging);
  
  shmod  = delkeys(shape,["shape","parts","copies", "def"]);
  copies = hash( shape, "copies",[[]] );
  _cutoffs= hash( shape, "cutoffs");
    
  shname = hash( shape, "shape" );
  def= hash( shape, "def" );
  shpts  = hash( shape, "pts" );
  shapevisible = hash( shape, "visible", 1);
  
  if(islogging) _log(_s("Loop thru [{_}] copies of shape", len(copies)));  
  
      for( ci = [0:len(copies)-1] )
      {
        difference()
        {
          newmod = _Make_update_parts( [shmod, copies[ci], mod] );
          
          if(islogging)
          { _log(_s("===<b>shape copies*{_}</b>===",ci));
            _log(["newmod", newmod],mode=0);
          } 
          if( hash(newmod,"visible",1) )
          {
            Draw_a_bare_shape( shname, def, shpts, newmod 
                             , loglevel=loglevel, loglayer = loglayer+1
                         );
          } else {
            if(islogging) 
            _log( _color("==> This copy's visible=0. Skip drawing", "orange"));
          }   
        
        
        if(_cutoffs)
          {
            if(islogging)_log("Drawing cutoffs:");
            cutoffs = _cutoffs[0]=="units"?[_cutoffs]:_cutoffs;
            for( coi = [0:len(cutoffs)-1] )
            {
              _cutoff = cutoffs[coi];
              //newmod2 = delkeys(_Make_update_parts( [shmod, copies[ci], mod] ), ["shape","cutoffs"]);
      	
            	// We want a cutoff's visibility=1 when either (1) shape.visible=1 
            	// (so it will be a cutoff as we want) or (2) shape.visible=0 AND 
            	// cutoff.visible=1 (means, we want to see how a cutoff is displaced
            	// so in this case a cutoff is drawn without its parent shape)
            	cutoffvisible = shapevisible?1:
            	                 hash( _cutoff, "visible", 0);
            	if(islogging)
            	{
            	_log( ["_cutoff", _cutoff, "visible", hash(_cutoff, "visible") ]);
            	_log(["shapevisible", shapevisible, "cutoffvisible", cutoffvisible]);
      	        _log(["newmod", newmod]);
                }
            	cutoff = update(_cutoff, ["visible", cutoffvisible, "color", "gray"]);                  
            	//newmod = update( _newmod, ["visible", cutoffvisible]);
              
              Draw_copies_of_an_obj( objname = str( objname, ".cutoff*", coi)
            	           , obj = cutoff
            	           , mod = delkey(newmod , "visible")
            	           , partlib= partlib
            	           , loglevel=loglevel
            	           , loglayer = loglayer+1
                         );
            }              
          }
        
        }  
                    
      } 
  if(islogging) _log_e( _s("Leaving Draw_a_shape( {_} )", objname));

} 


//module Draw_shapes( objname, shapes, mod=[], partlib = []
//                  , loglevel=LOGLEVEL, loglayer=2)
//{
//
//  /* <shapes>: a list of shapes 
//  
//   [ [ "shape", <shName>
//     , "def", "ops", [...]
//     , "copies", [...]
//     , "cutoffs", <obj>
//     ]
//   , [ "shape", <shName>
//     , "def", "ops", [...]
//     , "copies", [...]
//     , "cutoffs", <obj>
//     ]
//   ]
//  */
//  module _log(s,mode,add_ind){ log_(s, loglayer, mode,add_ind=add_ind);} 
//  module _log_b(s,mode,add_ind){ log_b(s, loglayer, mode,add_ind=add_ind);} 
//  module _log_e(s,mode,add_ind){ log_e(s, loglayer, mode,add_ind=add_ind);} 
//  
//  islogging = hash(mod, "islogging",0) || loglayer <= loglevel ;
//  
//  shapes= shapes[0]=="shape"?[shapes]:shapes;
//  
//  if(islogging)
//  {
//    _log_b(_s("Draw <b>{_}.shapes</b> with Draw_shapes()",objname));
//    _log(["shapes", shapes]);
//    _log(["added mod to shapes", mod]);
//  }
//  //echo(loglevel=loglevel);
//  for( si = [0:len(shapes)-1] )
//  {
//    fullshapename = str(objname,".shape*", si,":", shapes[si][1]);
//    if(islogging)
//    _log(_s("Looping thru {_} shapes of {_}: <b>{_}</b>",[len(shapes),objname,fullshapename] ));
//    
//    Draw_a_shape( fullshapename, shapes[si], mod
//                , loglevel=loglevel, loglayer = loglayer+1);
//
//  }
//  
//  if(islogging)
//  _log_e(_s("Leaving Draw_shapes(), {_}.{_}"
//           ,[objname, [for(sh=shapes) sh[1]]]));
//}


module Draw_a_part( objname, part, fullpartname, mod=[], partlib = []
                   , loglevel=LOGLEVEL, loglayer=3)
{
  /* <part>: 
  
    [ "part", <partname> ]
    
    [ "part", <partname>
     , "ops", [...]
     , "copies", [...]
     , "cutoffs", <obj>
     ]
     
   Each part is defined in partlib as:
   
   "partlib", [ <partname1>, <obj1>
              , <partname2>, <obj2>
              , ... 
              ]      
  */  
  
  module _log(s,mode){ log_(s, loglayer, mode);} 
  module _log_b(s,mode){ log_b(s, loglayer, mode);} 
  module _log_e(s,mode){ log_e(s, loglayer, mode);} 
  
  islogging = hash(part, "islogging",0) || loglayer <= loglevel ;
  partmod  = delkeys(part,["shape","parts","copies", "def"]);
  
  //echo(shape_islogging = hash(shape, "islogging")
      //, loglayer=loglayer, LOGLEVEL=LOGLEVEL, islogging=islogging ); 
  partname = hash(part, "part");
  coloredPartName= _Make_get_name_and_colorMark(fullpartname
                     , hash(mod,"color", hash(partmod, "color", "gold")));     
  if(islogging)
  {
   _log_b(_s("Draw <b>{_}</b> with Draw_a_part()"
           ,coloredPartName 
           ));

  _log(["partlib",partlib]);
  _log(["part",part]);
  _log(["added mod to this part", mod]);
  }
  
  copies = hash( part, "copies",[[]] );
  _cutoffs= hash( part, "cutoffs");
    
  partvisible = hash( part, "visible", 1);
  
  // the obj represented by partname
  partobj = hash( partlib, partname );
  
  if(islogging) _log(_s("Loop thru {_} copies of <b>{_}</b>"
                      , [len(copies), coloredPartName] ));  
  
      for( ci = [0:len(copies)-1] )
      {
        difference()
        {
          newmod = _Make_update_parts( [partmod, copies[ci], mod] );
          
          if(islogging)
          { _log(_s("===<b>{_}.copy*{_}</b>===",[fullpartname,ci]));
            _log(["newmod", newmod],mode=0);
          } 
          if( hash(newmod,"visible",1) )
          {
          
            Draw_copies_of_an_obj( fullpartname, //str(objname,".",partname)
                                 , partobj, mod=newmod, partlib=partlib
                            , loglevel=loglevel, loglayer=loglayer+1);
                            
            //Draw_a_bare_shape( shname, def, shpts, newmod 
                             //, loglevel=loglevel, loglayer = loglayer+1
                         //);
          } else {
            if(islogging) 
            _log( _color("==> This copy's visible=0. Skip drawing", "orange"));
          }   
        
        
        if(_cutoffs)
          {
            if(islogging)
            {
              _log("Drawing cutoffs:");
              _log(["newmod", newmod]);
            }
            cutoffs = _cutoffs[0]=="units"?[_cutoffs]:_cutoffs;
            for( coi = [0:len(cutoffs)-1] )
            {
              _cutoff = cutoffs[coi];
              
              //newmod2 = delkeys(_Make_update_parts( [shmod, copies[ci], mod] ), ["shape","cutoffs"]);
      	
            	// We want a cutoff's visibility=1 when either (1) shape.visible=1 
            	// (so it will be a cutoff as we want) or (2) shape.visible=0 AND 
            	// cutoff.visible=1 (means, we want to see how a cutoff is displaced
            	// so in this case a cutoff is drawn without its parent shape)
            	cutoffvisible = shapevisible?1:
            	                 hash( _cutoff, "visible", 0);
            	if(islogging)
            	{
            	_log( ["_cutoff", _cutoff, "visible", hash(_cutoff, "visible") ]);
            	_log(["shapevisible", shapevisible, "cutoffvisible", cutoffvisible]);
      	        _log(["newmod", newmod]);
                }
            	cutoff = update(_cutoff, ["visible", cutoffvisible, "color", "gray"]);                  
            	//newmod = update( _newmod, ["visible", cutoffvisible]);
              
              Draw_copies_of_an_obj( objname = str( objname, " cutoff[", coi, "]")
            	           , obj = cutoff
            	           , mod = delkey(newmod , "visible")
            	           , partlib= partlib
            	           , loglevel=loglevel
            	           , loglayer = loglayer+1
                         );
            }              
          }
        
        }  
                    
      } 
  if(islogging) _log_e( _s("Leaving Draw_a_part( obj:{_}, part:{_} )", [objname, part[1]]));

} 

//module Draw_parts( objname, parts, mod=[], partlib = []
//                  , loglevel=LOGLEVEL, loglayer=2)
//{
//
//  /* <parts>: a list of parts 
//  
//   [ [ "part", <partname>
//     ]
//   , [ "part", <partname>
//     , "ops", [...]
//     , "copies", [...]
//     , "cutoffs", <obj>
//     ]
//   ]  
//   
//   Each part is defined in partlib as:
//   
//   "partlib", [ <partname1>, <obj1>
//              , <partname2>, <obj2>
//              , ... 
//              ] 
//   
//  */
//  module _log(s,mode,add_ind){ log_(s, loglayer, mode,add_ind=add_ind);} 
//  module _log_b(s,mode,add_ind){ log_b(s, loglayer, mode,add_ind=add_ind);} 
//  module _log_e(s,mode,add_ind){ log_e(s, loglayer, mode,add_ind=add_ind);} 
//  
//  islogging = hash(mod, "islogging",0) || loglayer <= loglevel ;
//  
//  parts= parts[0]=="part"?[parts]:parts;
//  
//  if(islogging)
//  {
//    _log_b(_s("Draw <b>{_}.parts:{_}</b> with Draw_parts()"
//             ,[ objname, [for(p=parts)p[1]]]));
//    _log(["parts", parts]);
//    _log(["added mod to parts", mod]);
//  }
// 
//  for( pi = [0:len(parts)-1] )
//  {
//    partname = hash( parts[pi], "part");
//    fullpartname= str(objname,".part*", pi, ":",partname);
//    if(islogging)
//    //_log(_s("Looping thru {_} <b>{_}.parts</b>: <b>{_}.parts[{_}]:{_}</b>:"
//    //     , [len(parts),objname, objname, pi,partname] ));
//    _log(str("Ready to draw <b>", fullpartname,"</b>"));
//    Draw_a_part( objname, parts[pi], 
//                , fullpartname
//                , mod
//                , loglevel=loglevel
//                , partlib=partlib
//                , loglevel=loglevel, loglayer = loglayer+1);
//
//  }
//  
//  if(islogging)
//  _log_e(_s("Leaving Draw_parts(), {_}.{_}"
//           ,[objname, [for(p=parts) p[1]]]));
//}


module Draw_units( objname, units, mod=[], partlib = []
                  , loglevel=LOGLEVEL, loglayer=2)
{

  /* <units>: a list of units 
  
   [ [ "shape", <shName>
     , "def", [...]
     , "ops", [...]
     , "copies", [...]
     , "cutoffs", <obj>
     ]
   , [ "part", <partname> ]
   , [ "part", <partname>
     , "ops", [...]
     , "copies", [...]
     , "cutoffs", <obj>
     ]
   , 
   ] 
   
   Each part is defined in partlib as:
   
   "partlib", [ <partname1>, <obj1>
              , <partname2>, <obj2>
              , ... 
              ] 
   
  */
  module _log(s,mode,add_ind){ log_(s, loglayer, mode,add_ind=add_ind);} 
  module _log_b(s,mode,add_ind){ log_b(s, loglayer, mode,add_ind=add_ind);} 
  module _log_e(s,mode,add_ind){ log_e(s, loglayer, mode,add_ind=add_ind);} 
  
  islogging = hash(mod, "islogging",0) || loglayer <= loglevel ;
  
  units= units[0]=="part"
        || units[0]=="shape" ?[units]:units;
  
  if(islogging)
  {
    _log_b(_s("Draw <b>{_}.units:{_}</b> with Draw_units()"
             ,[ objname, [for(p=units)p[1]]]));
    _log(["units", units]);
    _log(["mod to be added to units", mod]);
  }
 
  for( ui = [0:len(units)-1] )
  {
    unit = units[ui];
    unitname = hash( unit, "part", hash(unit, "shape"));
    fullunitname= str(objname,".unit*", ui, ":", unitname);
    if(islogging)
    //_log(_s("Looping thru {_} <b>{_}.parts</b>: <b>{_}.parts[{_}]:{_}</b>:"
    //     , [len(parts),objname, objname, pi,partname] ));
    _log(str("Ready to draw <b>", fullunitname,"</b>"));
    if( unit[0] == "part")
    Draw_a_part( objname, unit, 
                , fullunitname
                , mod
                , loglevel=loglevel
                , partlib=partlib
                , loglevel=loglevel, loglayer = loglayer+1);
    if( unit[0] == "shape")
        Draw_a_shape( fullunitname, unit, mod
                , loglevel=loglevel, loglayer = loglayer+1);
  }
  
  if(islogging)
  _log_e(_s("Leaving Draw_units(), {_}.{_}"
           ,[objname, [for(p=units) p[1]]]));
}



module Draw_copies_of_an_obj( objname, obj, mod=[], partlib=[]
                            , loglevel=LOGLEVEL, loglayer=1)
{
  /*
  <shName>: cube, sphere, cylinder, poly 
  
  <obj>, [ "shapes", [ [ "shape", <shName>
                       , "def", "ops", [...]
                       , "copies", [...]
                       , "cutoffs", <obj>
                       ]
                     , [ "shape", <shName>
                       , "def", "ops", [...]
                       , "copies", [...]
                       , "cutoffs", <obj>
                       ]
                     ]   
          , "parts", [ [ "part", <partName>
                       ]
                     , [ "part", <partName>
                       , "ops", [...]
                       , "copies", [...]
                       , "cutoffs", <obj>
                       ]
                     ]                                  
          , "ops", []
          , "parts" , [ ]
          , "copies", [ [], [] ]
          , "color", ""
          , "cutoffs", <obj>
          ]
  */

  module _log(s,mode,add_ind){ log_(s, loglayer, mode,add_ind=add_ind);} 
  module _log_b(s,mode,add_ind){ log_b(s, loglayer, mode,add_ind=add_ind);} 
  module _log_e(s,mode,add_ind){ log_e(s, loglayer, mode,add_ind=add_ind);} 
  
  islogging = hash(obj, "islogging",0) || loglayer <= loglevel ;
  
  if(islogging)
  {
    _log_b(_s("Draw <b>{_}</b> with Draw_copies_of_an_obj()"
             , _Make_get_name_and_colorMark(objname, hash(obj,"color","gold"))));
    _log(["obj", obj]);         
    _log(["added mod", mod]);
  }
  //shapes = hash( obj, "shapes",[]);
  //parts = hash( obj, "parts",[]);
  units = hash( obj, "units",[]);
  
  if(!units)
  {
    _log(_red(_s("obj <b>{_}</b>: {_}", [objname, obj]) )); 
    _log(_red(
        _s("Error!! obj <b>{_}</b> doesn't have <b>units</b>"
        , objname)));
  }
  else
  {
    
    objMod = delkeys(obj,["units","copies"]);
    
    copies = hash( obj, "copies",[[]] );
    
    if(islogging){
     _log( ["objMod", objMod] );
     _log(_s("Looping through {_} copies of {_}",[len(copies), objname]));
    }
    
    difference()
    {
      for( ci = [0:len(copies)-1] )
      {
        newmod = _Make_update_parts( [objMod, copies[ci], mod] );
        fullobjname = str(objname,".copy*",ci);
        coloredname = _Make_get_name_and_colorMark( 
                                      _b(fullobjname)
                                         , hash(newmod,"color","gold")
                                         );
                  
        if(islogging)
        {
         _log_b( str("Drawing ", coloredname),add_ind=1);
          //_log( ["mod", mod], add_ind=1 );
          _log( ["newmod", newmod], add_ind=1 );
        }
        if( hash( newmod, "visible", 1))
        {
          Draw_units( fullobjname
                   , units
                   , newmod
                   , partlib = partlib
                   , loglevel= loglevel
                   , loglayer= loglayer+1
                   ) ;
          //if(parts)         
          //Draw_parts( fullobjname
                   //, parts
                   //, newmod
                   //, partlib = partlib
                   //, loglevel= loglevel
                   //, loglayer= loglayer+1
                   //) ;
                            
          //if(islogging)
          //_log_e(_s("Done drawing <b>{_}</b>'s copies[{_}]",[objname,ci]),add_ind=1);
        
        } else {
          if(islogging)
          {
           _log( _color("==> This copy's visible=0. Skip drawing", "orange"),add_ind=1);
           //_log_e(_s("Leaving Draw_copies_of_an_obj(objname=<b>\"{_}\"</b>)",objname,add_ind=1));
          }
        }
        
        if(islogging)
        _log_e(str( "Done drawing ", coloredname),add_ind=1);
      }    
                   
      _cutoffs = hash(obj, "cutoffs");
      cutoffs = _cutoffs[0]=="units"?[_cutoffs]:_cutoffs;
      
      if(cutoffs)
      { 
        if(islogging)
        _log(_s("Looping throu {_}.cutoffs: {_}", [objname, cutoffs]));
        
        for( coi = [0:len(cutoffs)-1] )
        { 
      	Draw_copies_of_an_obj( objname = str( objname, ".cutoff*", coi)
      	           , obj = cutoffs[ coi ]
      	           , mod = update(_Make_update_part(objMod,mod), ["color","gray"])  
      	           , partlib = partlib
      	           , loglevel = loglevel
      	           , loglayer = loglayer+1
                   );
        }           
      }             
    }
  }
  if(islogging)
  _log_e(_s("Leaving Draw_copies_of_an_obj(<b>\"{_}\"</b>)",objname,add_ind=1));

}
  
//. Make   
module Make ( data
            , title= undef
            , loglevel = LOGLEVEL
            , _partlib = []
            , _assems  = []
            ) 
{
  /*
  
  <obj>, [ "shapes", [ <shName>, ["def", ...]
                                  , <shName>, ["def", ...]
                                  ]
          , "parts" , [ ]
          , "copies", [ [], [] ]
          , "color", ""
          , "ops", []
          , "cutoffs", <obj>
          ]
          
                      
    data: [
            "partlib", []
          , "objs"
            , [
                "obj1", [ "shapes", [ <shName>, ["def", ...]
                                    , <shName>, ["def", ...]
                                    ]
                        , "parts" , [ ]
                        , "copies", [ [], [] ]
                        , "color", ""
                        , "ops", []
                        , "cutoffs", []
                        ]
              ]  ...
              
          ] //_data    
  */
  if(loglevel)
  echo( _mono(_b("--------------------------- Start Make() ---------------------------" )));

  title = und(_span( _b( hash(data, "title"), "font-size:20px")) );
  _note = hash(data, "note");
  echo(title);  
       
  //if(loglevel)
  //{
    if(_note)
    {
      note= isarr(_note)? join(_note, str(_mono("<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"), "// "))
                       : _note;  
      echo( _green(str("// ",note)));
    }    
  //}
  	
  objs = hash(data, "objs", []);
  partlib = hash( data, "partlib",[]);
  
  //objs = haskey(_objs,"copies")?_objs:update(_objs, ["copies",[[]]]);
  if(loglevel)
  {
    log_main( ["partlib", partlib] );
    log_main( ["data", data] );
    log_main( ["objs", objs] );
    
  }
  if(objs)
  {
    for( i=[1:2:len(objs)-1] )
    {
       //echo(i=i);
       objname = objs[i-1];
       obj = objs[i];
       Draw_copies_of_an_obj(objname, obj, mod=[]
                           , partlib = partlib, loglevel=loglevel);
    }
  }
  else { echo(_red("Error! no *objs* in data. Nothing to make")); }

  if(loglevel)
  {
    echo( und(_span( str("Leaving ", _b( hash(data, "title"))), "font-size:20px")) );      
    echo( _mono(_b("--------------------------- Done Make() ---------------------------" )));
  }
} //__Make
    
        
//------------------------------------------------------------

//. == Make tools ===============  

module log_main(s,mode,add_ind=0,debug=0) 
                  { log_(s, 0, mode, add_ind, debug=debug);}

function _Make_update_part( part1, part2 )=
(
  let( // If part1 have def ("shape","def" and "pts"), we keep them; 
       // This prevents the def section of a part from being overwritten
       // by later parts. 
       // If not exist, we get them from part2 and make sure they are placed
       // in the beginning of a part. 
       shape= hash(part1, "shape", hash(part2,"shape")) 
     , def= hash(part1, "def", hash(part2,"def")) 
     , pts= hash(part1, "pts", hash(part2,"pts")) 
     , core1= hashex( part1, ["shape","def","pts"])
     , core2= hashex( part2, ["shape","def","pts"])
     //, core1= [ shape?["shape", shape]:[]  
              //, def?["def", def]:[] 
              //, pts?["pts",pts]:[] 
              //]
     //, def = core1? [for( x= core1 ) each x]:[]
     , _def = core1 ? core1: core2
     , part1 = concat( _def, delkeys(part1, ["shape","def","pts"] ) )
     
     // Turn part2 into a mod (modifier). Items in a mod are updated 
     // differently
     // color: the last value
     // visible: both true
     // debug: any is true
     // markPts: any is true
     // dims: any is true
     // cutoffs: concat (w/ special treatment)
     // ops: concat 
     , mod2 = 
  [ haskey(part2,"color")?["color", vals_of_key([part2,part1], "color")[0]]: []
  , haskey(part2,"visible")?["visible", all_in_hashs( [part2,part1], "visible")]:[]
  , haskey(part2,"islogging")?["islogging"  , any_in_hashs( [part2,part1], "islogging")]:[]
  , haskey(part2,"markPts")?let( _m1= hash(part1, "markPts")
                               , _m2= hash(part2, "markPts")
                               , s1= isstr(_m1)?_m1:len(_m1)%2?_m1[0]:""
                               , s2= isstr(_m2)?_m2:len(_m2)%2?_m2[0]:""
                               , m1= isstr(_m1)?[]:len(_m1)%2?slice(_m1,1):_m1
                               , m2= isstr(_m2)?[]:len(_m2)%2?slice(_m2,1):_m2
                               )
                            concat( [join([s1,s2],";")], m1,m2)   
                            :[]
  , haskey(part2,"dims")?["dims"   , hash(part2,"dims")]:[]
  
  // cutoffs could be: (1) ["shape",...] or (2) [["shape",...]]. We want to 
  // make sure (1) is [["shape",...]] before a concat is made 
  , haskey(part2,"cutoffs")?let( c1=hash( part1,"cutoffs",[])
                               , c2=hash( part2,"cutoffs",[])
                               , cos= ["cutoffs"
                                      , concat( isstr(c1)||c1[0]=="shape"?[c1]:c1
                                              , isstr(c2)||c2[0]=="shape"?[c2]:c2 
                                              )
                                      ]
                               )
                              cos 
                            : [] 
                            
  , haskey(part2,"ops")?["ops", concat( hash( part1,"ops",[])
                        , hash( part2,"ops",[]) )]:[]
  ]
  )
  update( part1, [for(x=mod2) each x] )  
);

    _Make_update_part=[ "_Make_update_part", "part1,part2"
     , "array", "Make" 
     ,"Update part1 with part2. A *part* is a hash having two sections:
     ;; &lt;def> and &lt;mod> (=modifier) 
     ;; 
     ;; &lt;def> defines basic obj parameters ('shape','def','pts')
     ;; &lt;mod> defines modifications ('color', 'ops', 'visible', ...)
     ;; 
     ;; At least one of part1, part2 must have def.
     ;;
     ;; Items in a mod are not updated in the same way:
     ;; 
     ;; // color: the last value
     ;; // visible: both true
     ;; // debug: any is true
     ;; // markPts: any is true
     ;; // dims: any is true
     ;; // cutoffs: concat
     ;; // ops: concat
     "
     ,"_Make_update_parts"
    ];

    function _Make_update_part_test( mode=MODE, opt=[] )=
    (
      let( c1= ["shape","cube","color","gold" ]   
         , c2= ["color", "blue"]
         , c3= ["shape","poly","color", "red"]
         
         , v1= ["shape","cube","visible",1 ]   
         , v2= ["visible", 1]
         , v3= ["visible", 0]
         
         , d1= ["shape","cube","debug",0 ]   
         , d2= ["debug", 1]
         , d3= ["debug", 0]
         
         , a1= ["shape","cube","ops", ["rot",[90,0,0] ] ]   
         , a2= ["ops", ["transl",[1,2,3] ]]
         
         )
      doctest( _Make_update_part,
      [
//       _h3("color") 
//      ,_h4("color") 
//      ,_h5("color") 
//      ,_h6("color") 
//      ,
       _span(_u("color"), "font-size:14px")
       ,"" 
      , "var c1", "var c2", "var c3", ""
      , [_Make_update_part(c1,c2),["shape", "cube", "color", "blue"], "c1,c2"]
      
      , ""
      , "// Note the shape is taken from c1 and placed in the beginning of c2:"
      , [_Make_update_part(c2,c1),["shape", "cube", "color", "gold"], "c2,c1"]
      , ""
      , "// Note that <b>shape</b> is NOT replaced by new value:"
      , [_Make_update_part(c1,c3),["shape", "cube", "color", "red"], "c1,c3"]
        
      ,"" 
      ,_span(_u("visible"), "font-size:14px")
      ,"" 
      , "var v1", "var v2", "var v3", ""
      , [_Make_update_part(v1,v2),["shape", "cube", "visible", 1], "v1,v2"]
      , [_Make_update_part(v1,v3),["shape", "cube", "visible", 0], "v1,v3"]
       
      ,"" 
      ,_span(_u("debug"), "font-size:14px")
      ,"" 
      ,"var d1", "var d2", "var d3", ""
      , [_Make_update_part(d1,d2),["shape", "cube", "debug", 1], "d1,d2"]
      , [_Make_update_part(d1,d3),["shape", "cube", "debug", 0], "d1,d3"]
      
       
      ,"" 
      ,_span(_u("ops"), "font-size:14px")
      ,"" 
      , "var a1", "var a2"
      , [_Make_update_part(a1,a2),["shape", "cube", "ops", ["rot", [90, 0, 0], "transl", [1, 2, 3]]], "a1,a2"]
      , [_Make_update_part(a2,a1),["shape", "cube", "ops", ["transl", [1, 2, 3], "rot", [90, 0, 0]]], "a2,a1"]
      
      ]
      , mode=mode, opt=opt, scope=[ "c1", c1, "c2", c2, "c3", c3
                                  , "v1", v1, "v2", v2, "v3", v3 
                                  , "d1", d1, "d2", d2, "d3", d3 
                                  , "a1", a1, "a2", a2
                                  ]
      )
    );   
    
// -----------------------------------------
function _Make_update_parts( parts, _rtn=[], _i=0 )=
(
   _i==0? _Make_update_parts( parts, _rtn= parts[_i], _i=_i+1)
   :_i>=len( parts )? _rtn
   : _Make_update_parts( parts
                      , _rtn= _Make_update_part(_rtn, parts[_i]), _i=_i+1)
);

     _Make_update_parts=[ "_Make_update_parts", "parts"
     , "array", "Make" 
     ,"Update all parts to parts[0]. See _Make_update_part().
     "
     ,"_Make_update_part"
    ];

    function _Make_update_parts_test( mode=MODE, opt=[] )=
    (
      let( c1= ["shape","cube","color","gold" ]   
         , c2= ["color", "blue"]
         , c3= ["shape","poly","color", "red"]
         
         , v1= ["shape","cube","visible",1 ]   
         , v2= ["visible", 1]
         , v3= ["visible", 0]
         
         , d1= ["shape","cube","debug",0 ]   
         , d2= ["debug", 1]
         , d3= ["debug", 0] 
         
         , a1= ["shape","cube","ops", ["rot",[90,0,0] ] ]   
         , a2= ["ops", ["transl",[1,2,3] ]]
         
         , x1= [ "shape","cube"
               , "color","gold","visible",1,"debug",0,"ops",["rot",[90,0,0]] ]
         , x2= [ "color","gray","visible",1,"debug",0,"ops",["transl",[1,2,3]]]
         , x3= [ "color","blue","visible",1,"debug",0]
         , x4= [ "color","red" ,"visible",0,"debug",1,"ops",["transl",[5,5,5]]]
         )
      doctest( _Make_update_parts,
      [
       _span(_u("color"), "font-size:14px")
       ,"" 
      , "var c1", "var c2", "var c3", ""
      , [ _Make_update_parts([c1,c2,c3])
        , ["shape", "cube", "color", "red"]
        , "[c1,c2,c3]"
        ]
      ,"" 
      , "var v1", "var v2", "var v3", ""
      , [_Make_update_parts([v2,c2,c3])
           , ["shape", "poly", "visible", 1, "color", "red"]
           , "[v2,c2,c3]"
        ]
      ,"" 
      ,"var x1", "var x2", "var x3", "var x4", ""
      , [_Make_update_parts([x1,x2,x3])
           , ["shape", "cube", "color", "blue", "visible", 1, "debug", 0, "ops", ["rot", [90, 0, 0], "transl", [1, 2, 3]]]
           , "[x1,x2,x3]"
        ]
      , [_Make_update_parts([x1,x2,x4])
           , ["shape", "cube", "color", "red", "visible", 0, "debug", 1, "ops", ["rot", [90, 0, 0], "transl", [1, 2, 3], "transl", [5, 5, 5]]]
           , "[x1,x2,x4]"
        ]
         
      ]
      , mode=mode, opt=opt, scope=[ "c1", c1, "c2", c2, "c3", c3
                                  , "v1", v1, "v2", v2, "v3", v3 
                                  , "d1", d1, "d2", d2, "d3", d3 
                                  , "a1", a1, "a2", a2
                                  , "x1", x1, "x2", x2, "x3", x3, "x4", x4
                                  ]
      )
    );   

function _Make_get_name_and_colorMark(name,color)=
(
   let( color=isstr(color)?color:color==undef?"gold":color[0]
      , colorMark = _span( "&nbsp;&nbsp;&nbsp;", s= str("background:",color))
      )
   str( name, " ", colorMark)
   //str( colorMark, " ", name, " ", colorMark)
);
    
    _Make_get_name_and_colorMark=[ "_Make_get_name_and_colorMark"
      , "name,color", "str", "Make" 
    ,"Return a str containing *name* flanked by colored mark."
    ,""
    ];

    function _Make_get_name_and_colorMark_test( mode=MODE, opt=[] )=
    (
      let(rtn= "<span style=\"background:teal\">&nbsp;&nbsp;&nbsp;</span> test <span style=\"background:teal\">&nbsp;&nbsp;&nbsp;</span>"
         )
        doctest( _Make_get_name_and_colorMark,
        [ 
          str("_Make_get_name_and_colorMark(\"test\",\"teal\")= "
             , rtn)
        ]
        , mode=mode, opt=opt 
        )
    );  


//. == demo (duplication) ===============  

//================================================================
module Make_demo_1shape(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_1shape"
  , "note", "<b>obj1</b> has 1 shape."
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units",[ "shape", "cube"
                   , "def", ["size",[10,1,4]]
                   , "islogging", 0
                   ]
        , "ops", ["y", 4] 
        , "color", "green"         
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_1shape(loglevel=0);


//================================================================
module Make_demo_2shapes_by_shape_array(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_2shapes_by_shape_array"
  , "note", "<b>obj1</b>'s <b>shapes</b> has 2 shapes(gold and green cubes)"
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units",[["shape", "cube"
                    , "def", ["size",[10,1,4]]
                   ]
                   , ["shape", "cube"
                     , "def", ["size",[10,1,4]]
                     , "ops", ["y", 4] 
                     , "color", "green"
                     ]
                   ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_2shapes_by_shape_array(loglevel=0);


//================================================================
module Make_demo_2shapes_by_shape_copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_2shapes_by_shape_copies"
  , "note", "<b>obj1</b> has only one shape, but this shape has 2 copies(gold and brown)"
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units",["shape", "cube"
                    , "def", ["size",[10,1,4]]
                    , "copies", [ [] 
                                , [ "ops", ["y", 5] 
                                  , "color", "brown"]
                                ]
                   ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_2shapes_by_shape_copies(loglevel=0);


//================================================================
module Make_demo_2shapes_by_obj_copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_obj_copies"
  , "note", "<b>obj1</b> has only one shape, but this obj1 itself has 2 copies(gold and blue)"
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units",[["shape", "cube"
                  , "def", ["size",[10,1,4]]
                  ]
                 ] 
      , "copies", [ [] 
                  , [ "ops", ["y", 5] 
                    , "color", "blue"]
                  ]           
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_2shapes_by_obj_copies(loglevel=0);

//================================================================
module Make_demo_8shapes(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_8shapes"
  , "note", ["Combine *shape array*,*shape copies* and *obj copies* approaches demoed above."
            ,"to generate 8 shapes"]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units",[["shape", "cube"
                  , "def", ["size",[5,1,2]]
                  , "copies", [["color", "red"]
                              ,["ops",["rotz",20, "y",1]
                               , "color", ["red",0.7]
                               ] 
                              ]
                  ]
                 ,["shape", "sphere"
                  , "def", ["r",1, "$fn", 36]
                  , "ops", ["x", 5,"y",1]
                  , "copies", [["color", "green"]
                              ,["ops",["y",2]
                               , "color", ["green",0.7]
                               ] 
                              ]
                  ] 
                 ] 
      , "copies", [ [] 
                  , [ "ops", ["roty", -45, "y", 5] 
                    //, "color", "brown"
                    ]
                  ]           
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_8shapes(loglevel=0);


//. == demo (cutoffs) ===============  


//================================================================
module Make_demo_shape_cutoff(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_cutoff"
  , "note", ["<b>obj1</b> has one shape and one cutoff. "
            ,"The property '<b>cutoffs</b>' could be <b>[\"shapes\"...]</b> or <b>[[\"shapes\"...]]</b>"
            ,"Note that the <b>shape's 'ops'</b> are applied to the cutoff."
            ," So a cutoff follows both shape's and its own ops combined."
            ,"Also note that you can adjust '<b>visible</b>' properties of shape and cutoffs"
            ," as follows to decide what's visible:"
            ,_mono(" shape.visible | cutoffs.visible | shape | cutoffs" )
            ,_mono(replace("     1            1/0/undef        yes      no  "," ","&nbsp;") )
            ,_mono(replace("     0             0/undef         no       no  "," ","&nbsp;") )
            ,_mono(replace("     0               1             no       yes  "," ","&nbsp;") )
            ,"This is critical for debugging."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units",["shape", "cube"
                    , "def", ["size",[10,1,4]]
                    , "ops", ["roty", -15]
                    , "cutoffs", [ "units", [ "shape", "cube"
                  		              , "def", ["size",[2,4,2]]
                  		              , "ops", ["t",[2,-0.1,1]]
                  		              ]
                                  ]   
                   ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_cutoff(loglevel=0);

//================================================================
module Make_demo_shape_specific_cutoffs_with_shape_copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_specific_cutoffs_with_shape_copies"
  , "note", ["<b>obj1</b> has one shape and one cutoff, but the shape is duplicated multiple times."
            ,"There can be wo types of cutoffs: <b>shape-specific</b> and <b>cross-shapes</b>"
            ,"A <b>shape-specific cutoff</b> follows both shape's and its own ops."
            ,"So it follows its parent shape whereever it goes, and be duplicated "
            , "whenever its parent shape is duplicated."
            ,"A <b>cross-shapes cutoff</b> doesn't have a specific parent shape and is applied"
            ,"across a set of shapes."
            ,"This demo shows the <b>shape-specific cutoff</b> with shape duplication."
            , "('copies' is on the shape level)"
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                    , "def", ["size",[5,1,4]]
                    , "ops", ["roty", -15]
                    , "visible",1
                    , "cutoffs", [ "units", [ "shape", "cube"
					    , "def", ["size",[2,4,2]]
					    , "ops", ["t",[2,-0.1,1]]
					    ]
		                 //,"visible",1
				 ] 
                    
                    , "copies", [ [ ] 
                                , [ "ops", ["rotx", -90], "color", "green" ]
                                , [ "ops", ["mirror", [[0,0,1],[0,0,0]]], "color", "blue" ]
                                ]  
                   ]
                 ]
          
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_specific_cutoffs_with_shape_copies(loglevel=0);

//================================================================
module Make_demo_shape_specific_cutoffs_with_obj_copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_specific_cutoffs_with_obj_copies"
  , "note", ["This demo shows the <b>shape-specific cutoff</b> with obj duplication."
            , "('copies' is on the obj level)"
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                    , "def", ["size",[5,1,4]]
                    , "ops", ["roty", -15]
                    , "visible",1
                    , "cutoffs", [ "units", [ "shape", "cube"
					    , "def", ["size",[2,4,2]]
					    , "ops", ["t",[2,-0.1,1]]
					    ]
		                 ,"visible",1
				 ] 
                    ]
                 ]
                    
      , "copies", [ [ ] 
                  , [ "ops", ["rotx", -90], "color", "green" ]
                  , [ "ops", ["mirror", [[0,0,1],[0,0,0]]], "color", "blue" ]
                  ]            
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_specific_cutoffs_with_obj_copies(loglevel=0);

//================================================================
module Make_demo_cross_shape_cutoffs(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_cross_shape_cutoffs"
  , "note", ["A <b>cross-shapes cutoff</b>:"
            , " -- Its parent is a set of shapes;"
            , " -- doesn't follow the 'ops' of a specific shape; "
            , " -- will cut through all copies of the shapes."
            , ""
            , "Set shape 'visible' to 0 to see the cutoff"
            , "Note the '<b>copies</b>' is done on the <i>shape</i> level."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                    , "def", ["size",[5,1,4]]
                    , "ops", ["roty", -15]
                    , "visible",1
                    , "copies", [ [ ] 
                              , [ "ops", ["rotx", -90], "color", "green" ]
                              , [ "ops", ["mirror", [0,0,1]], "color", "blue" ]
                              ] 
                    ]
                 ]
                    
      , "cutoffs", [ "units", [ "shape", "cube"
			      , "def", ["size",[2,10,2]]
			      , "ops", ["t", [1,-2,-0.8]]
			      ]
		    , "visible",1	      
		   ]            
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_cross_shape_cutoffs(loglevel=0);

//================================================================
module Make_demo_cross_subshapes_cutoffs(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_cross_subshapes_cutoffs"
  , "note", ["A <b>cross-shapes cutoff</b>:"
            , " -- Its parent is a set of shapes;"
            , " -- doesn't follow the 'ops' of a specific shape; "
            , " -- will cut through all copies of the shapes."
            , ""
            , "Set shape 'visible' to 0 to see the cutoff"
            , "Note the '<b>copies</b>' is done on the <i>shape</i> level."
            , "but the '<b>cutoffs</b>' is done on the <i>obj</i> level."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                    , "def", ["size",[5,1,4]]
                    , "ops", ["roty", -15]
                    , "visible",1
                    ]
                  , ["shape", "cube"
                    , "def", ["size",[5,1,4]]
                    , "color", "blue"
                    , "ops", ["roty", -15, "mirror", [0,0,1]]
                    , "visible",1
                    ]
                 ]
      , "visible", 1              
      , "cutoffs", [ "units", [ "shape", "cube"
			      , "def", ["size",[2,10,2]]
			      , "ops", ["t", [1,-2,-0.8]]
			      ]
		    , "visible",1	      
		   ]            
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_cross_subshapes_cutoffs(loglevel=0);

//================================================================
module Make_demo_cross_shape_cutoffs2(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_cross_shape_cutoffs2"
  , "note", ["Another case of <b>cross-shapes cutoff</b>:"
            , "Both <b>copies</b> and <b>cutoffs</b> are done on the <i>obj</i> level."
            ]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ ["shape", "cube"
                    , "def", ["size",[5,1,4]]
                    , "ops", ["roty", -15]
                    , "visible",1
                    
                    ]
                 ]
      
      , "copies", [ [ ] 
                  , [ "ops", ["rotx", -90], "color", "green" ]
                  , [ "ops", ["mirror", [0,0,1]], "color", "blue" ]
                  ]        
                         
      , "cutoffs", [ "units", [ "shape", "cube"
			      , "def", ["size",[2,10,2]]
			      , "ops", ["t", [1,-2,-0.8]]
			      ]
		    , "visible",1	      
		   ]            
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_cross_shape_cutoffs2(loglevel=0);



//================================================================
module Make_demo_shape_2cutoffs(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_2cutoffs"
  , "note", ["<b>obj1</b> has one shape and 2 cutoffs. "
            ,"Note that the shape's ops are applied to the cutoff."]
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
        "units", [ "shape", "cube"
                    , "def", ["size",[7,1,4]]
                    , "ops", ["roty", -45]
                    , "visible", 1
                    , "cutoffs", [ ["units", [ "shape", "cube"
                  		              , "def", ["size",[2,4,2]]
                  		              , "ops", ["t",[1,-0.1,1]]
                                              ]
                                   ]
                                 , ["units", [ "shape", "sphere"
                  		              , "def", ["r",1.2, "$fn", 48]
                  		              , "ops", ["t",[5,0,2]]
                                              ]
                                   ]
                                 ]
                   ]
      , "copies", [ []
                  , ["ops",["mirror", [-1,0,1] ], "color", "blue"]
                  ]              
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_2cutoffs(loglevel=0);

//================================================================
module Make_demo_shape_2cutoffs_2copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_shape_2cutoffs_2copies"
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units",["shape", "cube"
                    , "def", ["size",[10,1,2]]
                    , "copies", [ ["visible",1 ] 
                                , [ "ops", ["y", 2] 
                                  , "color", "brown"
                                  , "visible",1
                                  ]
                                ]
                    , "cutoffs", [[ "units", [ "shape", "cube"
                  		              , "def", ["size",[2,2.5,2]]
                  		              , "ops", ["t",[2,-0.1,1]]
                                              ]
                                  ]
                                 ,[ "units", [ "shape", "sphere"
                  		              , "def", ["r",1.5, "$fn", 48]
                  		              , "ops", ["t",[6,0.8,2]]
                  		              , "islogging", 0
                                              ]
                                  ]
                                 ]
                   ] 
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_shape_2cutoffs_2copies(loglevel=0);


//================================================================
module Make_demo_2shapes_2cutoffs_2copies(loglevel=10)
{
  
  data=  
  [ 
   "title", "Make_demo_2shapes_2cutoffs_2copies"
   
   ,"objs" 
  , [  
      "obj1"
    , [ 
          "units", [ [ "shape", "cube"
                      , "def", ["size",[5,1,2]]
                      , "copies", [ ["visible",1] 
                                  , [ "ops", ["y", 1.5] 
                                    , "color", "brown"
                                    , "visible",1
                                    ]
                                  ]
                      , "cutoffs", [[ "units", [ "shape", "cube"
                    		              , "def", ["size",[2,2.5,2]]
                    		              , "ops", ["t",[2,-0.1,1]]
                                                ]
                                    //, "visible",1            
                                    ]
                                   ]
                     ] 
                     , ["shape", "cube"
                        , "def", ["size",[5,1,2]]
                        , "ops", ["x", 6]
                        , "copies", [ ["visible",1 
                                      , "color", "darkkhaki"
                                      ] 
                                    , [ "ops", ["y", 1.5] 
                                      , "color", "blue"
                                      , "visible",1
                                      ]
                                    ]
                        , "cutoffs", [[ "units", [ "shape", "sphere"
                      		              , "def", ["r",1, "$fn", 48]
                      		              , "ops", ["t",[2,0,2]]
                                                  ]
                                      , "visible",1            
                                      ]
                                     ]
                       ]
                 ]   
      ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_2shapes_2cutoffs_2copies(loglevel=0);

//. == demo (parts) ===============  

//================================================================
module Make_demo_part(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_part"
  , "note", "<b>obj1</b> has 1 part."
  , "partlib", [ 
                 "p1", ["units",[ "shape", "cube"
                                 , "def", ["size",[10,1,4]]
                                 //, "islogging", 1
                                 ]
                      , "ops", ["y", 4] 
                      , "color", "green"         
                      ] 
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units",[ ["part","p1"] ]         
        ] 
    ]    
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_part(loglevel=10);

//================================================================
module Make_demo_1part_of_2shapes(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_1part_of_2shapes"
  , "note", "<b>obj1</b> has 1 part w/ 2 shapes."
  , "partlib", [ 
                 "p1", ["units",[[ "shape", "cylinder"
                                   , "def", ["r",1, "h",3, "$fn", 24]
                                   ]
                                 , [ "shape", "cylinder"
                                   , "def", ["r",2, "$fn",6]
                                   ]
                                 ]
                      , "ops", ["y", 4] 
                      , "color", "green"         
                      ] 
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units",["part","p1"]          
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_1part_of_2shapes(loglevel=0);

//================================================================
module Make_demo_1part_of_2subshapes_w_cutoff(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_1part_of_2subshapes_w_cutoff"
  , "note", "<b>obj1</b> has 1 part, 2 shapes and 1 cross-shape cutoff."
  , "partlib", [ 
                 "p1", ["units",[[ "shape", "cylinder"
                                   , "def", ["r",1, "h",2, "$fn", 24]
                                   , "visible", 1
                                   ]
                                 , [ "shape", "cylinder"
                                   , "def", ["r",2, "$fn",6]
                                   , "visible", 1
                                   ]
                                 ]
                      , "ops", ["y", 3] 
                      , "color", "green" 
                      //, "visible", 0
                      , "cutoffs", ["units",["shape", "cylinder"
                                             ,"def",["r",0.8, "h", 7, "$fn",12]
                                             , "visible",1
                                             , "ops", ["z",-1]
                                             ]
                                   ]        
                      ] 
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units", ["part","p1" ]         
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_1part_of_2subshapes_w_cutoff(loglevel=0);


//================================================================
module Make_demo_1part_of_2subshapes_w_cutoff_x2(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_1part_of_2subshapes_w_cutoff_x2"
  , "note", "<b>obj1</b> has 2 copies of 1 part that has 2 shapes and 1cutoff."
  , "partlib", [ 
                 "p1", ["units",[
                 		[ "shape", "cylinder"
                                   , "def", ["r",1, "h",2, "$fn", 24]
                                   , "visible", 1
                                   ]
                                 , 
                                 [ "shape", "cylinder"
                                   , "def", ["r",2, "$fn",6]
                                   , "visible", 1
                                   ]
                                 ]
                      , "ops", ["y", 3] 
                      , "color", "green" 
                      //, "visible", 0
                      , "cutoffs", ["units",["shape", "cube"
                                             ,"def",["size",[1,1,5]]
                                             , "visible",1
                                             , "ops", ["t",[-0.5,-0.5,-1]]
                                             ]
                                   ]        
                      ] 
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units", ["part","p1" ] 
        , "copies", [ [] 
                    , ["ops",["mirror", [0,1,0]], "color","blue"]
                    ]          
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_1part_of_2subshapes_w_cutoff_x2(loglevel=0);


//================================================================
module Make_demo_2parts(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_2parts"
  , "note", "<b>obj1</b> has 2 parts."
  , "partlib", [ 
                 "p1", ["units",[[ "shape", "cylinder"
                                   , "def", ["r",1, "h",3, "$fn", 24]
                                   ]
                                 ]
                      , "color", "green"         
                      ] 
                      
              , "p2", ["units",[[ "shape", "cylinder"
                                 , "def", ["r",2, "$fn",6]
                                 ]
                                 ]
                      , "color", "blue"         
                      ]         
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units",[ ["part","p1"], ["part","p2"] ]
        , "ops", ["y", 4]    
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_2parts(loglevel=0);

//================================================================
module Make_demo_comboParts(loglevel=0)
{
  
  data=  
  [ 
   "title", "Make_demo_comboParts"
  , "note", "<b>obj1</b> has a part that is a comboPart containing 2 parts."
  , "partlib", [ 
                 "p1", ["units",[[ "shape", "cylinder"
                                   , "def", ["r",1, "h",3, "$fn", 24]
                                   ]
                                 ]
                      , "color", "green"         
                      ] 
                      
              , "p2", ["units",[[ "shape", "cylinder"
                                 , "def", ["r",2, "$fn",6]
                                 ]
                                 ]
                      , "color", "blue"         
                      ]    
              , "p12", ["units", [ ["part","p1"]
                                 , ["part","p2"]
                                 ]
                       ]             
              ]        
        
   ,"objs" 
  , [  
      "obj1"
      , [ 
          "units",[ "part","p12"]
        ] 
    ]   
  ]; 

  Make( data = data,  loglevel=loglevel );  
   
}  
//Make_demo_comboParts(loglevel=0);


//. == demo (practical) ======
//----------------------------------------------------

module Make_demo_woodworking_mortise_tenon (loglevel=10)
{
  
  
  data=  
  [ 
   "title", "Make_demo_woodworking_mortise_tenon"
  ,"note", [ "A part, <b>tongue</b>, is used as a shape in <b>tenon</b>(green) "
           , "and a cutoff in <b>mortise</b> (blue)."
           ]
  
  , "partlib", [ "tongue"
                , [
                    "units", ["shape","cube"
                              , "def", ["size",[2,3,1]]
                              ]
                  ]
               , "board"
                , [
                    "units", ["shape","cube"
                            ,"color",["gold",0.7]
                            , "def", ["size",[4,4,2]]
                            ]
                  ]
               , "tenon"
               , [
                   "units", [["part","board", "ops", ["x", 8] 
                             ]
                            ,["part","tongue","ops",["transl",[6,0.5,.5]]
                             ]
                            ]
                 , "color", "green"            
                 ]
               , "mortise"
               , [
                   "units", ["part", "board","ops",["transl",[0,0,0]]]
                 , "color", "blue"
                 , "cutoffs", ["units", ["part","tongue"
                                         , "ops", ["transl",[2.01,.5,0.5]]
                                        ]
                              ]          
                 ]   
               ]  
     
   
   ,"objs" 
  , [  
      "long-side"
      , [ "units", [["part","mortise"], ["part","tenon"]]
        ]
    ]   
  
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_woodworking_mortise_tenon(loglevel=0);


//----------------------------------------------------

module Make_demo_woodworking_box_joint (loglevel=10)
{
  
  w=1;
  
  data=  
  [ 
   "title", "Make_demo_woodworking_box_joint"
  
  , "partlib", [ "board"
                , [
                    "units", [ "shape","cube"
                    	      , "def", ["size",[10,w,6]]
                    	      ]
                  ]
               , "board1"
                , [
                    "units",["part","board"]
                    ,"ops", ["rot",[0,0,90],"transl",[w,0,0]]
                    ,"cutoffs"
                    , [ ["units",["part","tooth","ops",["transl",[-0.01,-0.005,-0.015]] ]]
                      , ["units",["part","tooth","ops",["transl",[-0.01,-0.005,2*w]] ]]
                      , ["units",["part","tooth","ops",["transl",[-0.01,-0.005,4*w]] ]]
                      ]
                    
                  ]
               , "tooth"
               , [
                    "units", ["shape","cube"
                              , "def", ["size",[w+0.02,w+0.01,w]]
                              ,"color",["teal",1]
                              ]
                ]                  
               , "board2"
                , [
                    "units",[ ["part","board"]
                            , ["part","tooth","ops",["x",-w] ]
                            , ["part","tooth","ops",["transl",[-w,0,2*w]] ]
                            , ["part","tooth","ops",["transl",[-w,0,4*w]] ]
                            ]
                    ,"color",["darkkhaki",1]
                    ,"ops", ["transl",[w*2.5,0,0]]
                  ]
               ]  
     
   
   ,"objs" 
  , [  
      "box"
      , [ "units", [["part","board1"],["part","board2"]]
        ]
    ]   
  
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
//Make_demo_woodworking_box_joint(loglevel=0);

//----------------------------------------------------

module Make_demo_woodworking_box_joint_2 (loglevel=10)
{
  
  w=1;
  
  data=  
  [ 
   "title", "Make_demo_woodworking_box_joint_2"
  
  , "partlib", [ "board"
                , [
                    "units", [ "shape","cube"
                    	      , "def", ["size",[10,w,6]]
                    	      ]
                  ]
               , "board1"
                , [
                    "units",["part","board"]
                    ,"ops", ["rot",[0,0,90],"transl",[w,0,0]]
                    ,"cutoffs", [ "units",["part","teeth"], "ops",["t",[-0.005,-0.005,-0.005]]]
                    
                  ]
               , "teeth"
               , [ "units", ["shape","cube"
                              , "def", ["size",[w+0.02,w+0.01,w]]
                              ,"color",["teal",1]
                              ]
                 , "copies", [ [] 
                             , ["ops", ["z", 2*w]] 
                             , ["ops", ["z", 4*w]] 
                             ]
               
                 ]   
                              
               , "board2", [
                    "units",[ ["part","board"]
                            , ["part","teeth", "ops", ["x",-w]]
                            ]
                    ,"color",["darkkhaki",1]
                    ,"ops", ["transl",[w*2.5,0,0]]
                  ]
               ]  
     
   
   ,"objs" 
  , [  
      "box"
      , [ "units", [ ["part","board1"]
                   , ["part","board2"]]
        ]
    ]   
  
  ]; 

  Make( data = data,  loglevel=loglevel );  
  
}  
 
//Make_demo_woodworking_box_joint_2(loglevel=0);



