/*

   angle_compare.scad

   This report studies and compares methods of calculating an angle Q
   formed by a 3-pointer pqr = [P,Q,R].

*/
include <scadx_obj_basic_dev.scad>

/*

              _R
           _-' |
        _-'    |
     _-'       |
   Q-----------D---P
                     _R 
                 _-:' |
             _-:'     |
         _-:'         |
     _-:'             |
   Q------------P.....D


*/



function angle_atan(pqr, refPt=undef)=
 let( P=pqr[0], Q=pqr[1], R=pqr[2]
    , D = othoPt(sel(pqr,[0,2,1]))
    , PD = norm(P-D)
    , RD = norm(R-D)
    , QD = norm(Q-D)
    , _a= atan( RD/QD ) 
    , a = PD>QD && PD> norm(P-Q)? (180-_a):_a
    , np = normalPt( pqr )
    , sign= refPt==undef?1: isSameSide( [np, refPt], pqr)?1:-1
    )
(
	sign* a
);


function angle_acos(pqr, refPt=undef)=
 let( P=pqr[0], Q=pqr[1], R=pqr[2]
    , D = othoPt(sel(pqr,[0,2,1]))
    , PQ = norm(P-Q)
    , QR = norm(Q-R)
    , PD = norm(P-D)
    , RD = norm(R-D)
    , QD = norm(Q-D)
    , _a= acos( QR/QD ) 
    , a = PD>QD && PD> PQ? (180-_a):_a
    , np = normalPt( pqr )
    , sign= refPt==undef?1: isSameSide( [np, refPt], pqr)?1:-1
    )
(
	sign* a
);

function angle_asin(pqr, refPt=undef)=
 let( P=pqr[0], Q=pqr[1], R=pqr[2]
    , D = othoPt(sel(pqr,[0,2,1]))
    , PQ = norm(P-Q)
    , QR = norm(Q-R)
    , PD = norm(P-D)
    , RD = norm(R-D)
    , QD = norm(Q-D)
    , _a= asin( QR/RD ) 
    , a = PD>QD && PD> PQ? (180-_a):_a
    , np = normalPt( pqr )
    , sign= refPt==undef?1: isSameSide( [np, refPt], pqr)?1:-1
    )
(
	sign* a
);

//==================================================
/* calc angle w/o trigonometry

  http://mathforum.org/library/drmath/view/55341.htm

  Calc angle A, 3 sides = a,b,c

  x = (b^2+c^2-a^2)/2bc
  K = (1/2)x^3/3 + (1/2)(3/4)x^5/5 + (1/2)(3/4)(5/6)x^7/7 ...  

         1  x^3      1 3 x^5      1 3 5 x^7
    =   --------  +  --------  +  ----------- + ....
         2   3       2 4 5        2 4 6  7 
       
                       g x^( 2(i+1)+1 )
   =  (sign(-i+0.5))  -----------------
                       h ( 2(i+1)+1 )
       
      g = range( 1,2, 2(i+1)+1 )
      h = range( 2,2, 2(i+1)+1 )
     
   ==> j = 2(i+1)+1
 
                  prod( range(1,2,j) ) x^j
   K = series of -------------------------
                  prod( range(2,2,j) )  j
       
  A = Pi/2 - x - K
      
       
  A is a radian. Multiply to degrees by x180/PI
*/ 

function angle_cordic(pqr, prec=4)=   // prec = len(K)
    let ( a= d02(pqr) // PR
        , b= d01(pqr) // PQ
        , c= d12(pqr) // QR
        , x= (pow(b,2)+pow(c,2)-pow(a,2))/ (2*b*c)
        , K = [ for(i=range(prec))
                  let( j = 2*i+3     
                     , g = prod( range(1,2,j) )
                     , h = prod( range(2,2,j) )
                     )
                   g * pow(x,j) / h / j
              ] 
        )          

(
   (PI/2-x-sum(K))*180/PI
);
        

module test_1()
{
    // Test by creating pqr based on known angle(s)
    

    _pqr = randPts(3);  // random pqr
    P= P(_pqr);        // P representing X-axis 
    //L = 2;              // len of QR, make it a constant 
    Q = Q(_pqr);        // Q will be a fixed pt
    
    // We will make this a process of scanning through angles
    // with a radius = L
    
    angs = concat( [0,5,15], range( 30, 30, 181) );       
    
    for( a= angs )
    {
       D = othoPt( sel(_pqr, [0,2,1]) ); // a proper D on  line Q::P
       /*
          Recalc R according to D and a 
        
                      _R0
                L  _-' |
                _-'    | h
             _-' a     |
           Q-----------D---Px
                 r=QD
        
          L^2 = r^2 + h^2
          
          L  = sqrt( r^2 + h^2 )  
          Area = sqrt( s(s-L)(s-h)(s-r) ) = h*r/2   s=(L+h+r)/2
          s(s-L)(s-h)(s-r) = h^2 * r^2 / 4
          = s(s-2L/2)(s-2h/2)(s-2r/2) 
          = s( (h+r-L)/2  * (L+h-r)/2 * (L+r-h)/2 ) 
          = s(h+r-L)(L+h-r)(L+r-h)/8 
          = (L+h+r)(h+r-L)(L+h-r)(L+r-h)/16 = h^2 * r^2 / 4
          
          (L+h+r)(h+r-L)(L+h-r)(L+r-h)= 4* h^2 * r^2
        
       */  
          
    
    
    
    
    }    




}    
        
//for( i= range(20))
//{    
//    pqr = randPts(3 );
//    a = angle(pqr);
//    if (abs(a)>150)
//    {   echo( i, pqr );
//        Chain( pqr, ["closed",false]);
//        LabelPt( pqr[0], i );
//        for (prec = [5,10,15,20,40,80] ) 
//          echo( prec=prec, ang = a, ang(pqr, prec=prec ));
//    }     
//}            
//echo( rng = range(1,2,0) );
//echo( rng = range(1,2,1) );
//echo( rng = range(1,2,2) );
//echo( rng = range(1,2,3) );


module chk_angle_prec()
{
 
    
   module chk(pqr)
   {
      
      a=angle(pqr);
      r = d01(pqr); 
      arc =  (a*PI/180)*r;
       
      peri= 2*r*PI; 
      a2 = arc/peri*360;
       
       
      //echo( r=r, peri=peri, a=a, a2=a2); //, pqr=pqr );
      echo( a=a, a2=a2); //, pqr=pqr );
   }    
  colors=[undef, "red","green","blue","purple"];  
  for( i = range(20) )
  {  
     pqr = randPts(3);
     P = P(pqr); Q=Q(pqr); R=R(pqr);
     R2 = onlinePt( [Q, R], len= d01(pqr) );
     lenQ = dist( [R,R2] );
     //color(colors[i]) Chain( pqr, ["closed",false] ); 
     //Dim( [P,R2,Q] );    
     //Dim( pqr );
      chk( pqr, lenQ );    
  }  
}    

//chk_angle_prec();   