
import pprint 

#=================================================
# get current date time
#
from datetime import datetime
date= str(datetime.now())
# like '2011-05-03 17:45:35.177000'
date = ':'.join(date.split(':')[:-1])

istestcode    = False      
isNextlineVer = False
header= ("//\n"
        + "// This file, %s, is extracted\n"
        + "// from scadx_dev.scad (version: %s)\n"
        + "// by scadx_tools.py on " +date 
        + "\n//\n\n")
        
filename =  'scadx_core_dev.scad'       
print "Loading: ", filename        
lines= open( filename,'r').readlines()

def convert_stringlines_2_wholestring(lines):
    ''' convert a string array:
        
         ["Given an angle (ay), return "
         ,"the rotation matrix for "
         ,"rotating a pt about y-axis "
         ]
         
         to 

    "
     Given an angle (ay), return the rotation\\
     matrix for rotating a pt about y-axis.\\
    "
    '''
    lines = ['"']+[ x.rstrip()=="" and '\\\\' or (' '+x.rstrip() + ' \\\\') for x in lines ]+['"']
    return '\n'.join(lines)
    
    
print( convert_stringlines_2_wholestring(
["Given a 3D pt, return the rotation array that "
            ,"would rotate a pt on z, ptz (=[0,0,L] where L is "
            ,"the distance between pt and the origin), to pt" 
            , "Usage:                                    "
            , "                                           "
            , "   RA= rotangles_z2p( pt );                "
            , "   rotate( RA )                            "
            , "   translate( ptz )                         "
            , "   sphere( r=1 );                          "
            ]
))

'''
 >>> s
'function abc(def)'
>>> (re.compile("function ([\w_]*)")).findall( s )
['abc']
'''

import re

blocks = { "function": [re.compile("\nfunction ([\w_]*)")
                        ,re.compile("\n\);")]
         , "module": [ re.compile("\nmodule ([\w_]*)")
                        ,re.compile("\n\}")]
         }
         
funcnames = []
modnames=[]
funcmodnames=[]
testnames=[]
demonames=[]

funcmap = []
modmap=[]
funcmodmap=[]
testmap=[]
demomap=[]

tags=[]
mods = []
tests = []
demos = []
all = []

#ptn_funcname= re.compile("function ([\w_]*)\(")
#ptn_modname= re.compile("module ([\w_]*)\(")

def getfuncname(line):
    return (re.compile("^function ([\w_]*)")).findall( line )[0].strip()
    '''
        # This will grab func name and args:
        func = re.split('\) *='
                        , ' '.join( 
                                re.sub(" +\(","(",line).split(" ")[1:] 
                                ).strip()
                        )[0] +")" 
    '''
def gettags(line):
    return (re.compile("[\w_]*=\[.*, *")).findall( line )[0].strip()
    

def getmodname(line):
    return (re.compile("^module ([\w_]*)")).findall( line )[0].strip()


def extract_funcmod_names():
    for line in lines:

        if isNextlineVer:   # extract version
                            # ["20140406-5", "...
            version = line.split(',')[0].replace('"','').replace('[','') 
            isNextlineVer= False

        if line.startswith("function "):
            '''
            # This will grab func name and args:
            func = re.split('\) *='
                            , ' '.join( 
                                    re.sub(" +\(","(",line).split(" ")[1:] 
                                    ).strip()
                            )[0] +")" 
            '''
            funcname = getfuncname(line)
            funcnames.append( '"%s"'%funcname )
            funcmodnames.append( '"%s"'%funcname )
            funcmap.append( '"%s", %s'%(funcname, funcname) )
            
        elif line.startswith("module "):
            modname = getmodname(line)
            if modname.find("_demo")>-1:
                demonames.append('"%s"'%modname)
                demomap.append( '"%s", %s'%(modname,modname) )
                
            elif modname.find("_test")>-1:
                testnames.append('"%s"'%modname)
                testmap.append( '"%s", %s'%(modname,modname) )
            else:    
                modnames.append( '"%s"'%modname )
                funcmodnames.append( '"%s"'%modname )
                modmap.append( '"%s", %s'%(modname,modname) )
                funcmodmap.append( '"%s", %s'%(modname,modname) )
            
        if ( re.compile( "^[\w_]* *= *\[ *\[" ).match( line )
            #and line.endswith(",") #re.compile( ",$" ).match( line )
           # and (line.endswith(",") or line.endswith(", ")) #re.compile( "] *, *$" ).match( line )
           ):       
            #if line.endswith(","):
                print line
                fn = line.split('=')[0].strip()
            #try:       
                tag = fn #+ ": " + str(re.split( " *, *", eval('['.join(line.split('[')[1:]).strip()[:-1])[-1]))
                tags.append( tag )#
            #except:
            #    tag=''

        if line.strip().startswith("scadex_ver=["):   # extract version
            isNextlineVer= True        
              
    '''    
            name = str(line.split("function ")[1].split("(")).strip()
            all.append( name )
            funcs.append( name )
            if name.endswith("_test"): tests.append( name )
            if (name.endswith("_demo")
                or name.find("demo")>0): demos.append( name )
        elif line.startswith("module "):
            name = str(line.split("module ")[1].split("(")).strip()
            all.append( name )
            funcs.append( name )
            if name.endswith("_test"): tests.append( name )
            if (name.endswith("_demo")
                or name.find("demo")>0): demos.append( name )
    '''
    funcnames.sort()
    modnames.sort()
    testnames.sort()
    demonames.sort()

    funcmap.sort()
    modmap.sort()
    funcmodmap.sort()

    out = []
    nl = '\n'
    out.append( nl.join( [ "funcs", nl ] ) )
    out.append( nl.join( funcnames ))


    funcstring = ("// scadex function count: %s\n"%len(funcnames)
                 +"scadex_funcname_list= [" + ", ".join(funcnames) + "];")
    modstring = ("// scadex module count: %s\n"%len(modnames)
                 +"scadex_modname_list= [" + ", ".join(modnames) + "];")
    funcmodstring = ("// scadex funcmod count: %s\n"%len(funcmodnames)
                 +"scadex_funcmodname_list= [" + ", ".join(funcmodnames) + "];")
    demostring = ("// scadex demo count: %s\n"%len(demonames)
                 +"scadex_demo_list= [" + ", ".join(demonames) + "];")
    teststring = ("// scadex test count: %s\n"%len(testnames)
                 +"scadex_test_list= [" + ", ".join(testnames) + "];")
    funcmapstring= ("scadex_func_map= [" + ", ".join(funcmap) + "];")
    modmapstring= ("scadex_mod_map= [" + ", ".join(modmap) + "];")
    funcmodmapstring= ("scadex_funcmod_map= [" + ", ".join(funcmodmap) + "];")

    outstring = ( header%( "scadex_funcmod_list.scad", version)
                + funcstring + nl+nl
                + funcmapstring + nl+nl
                + modstring + nl+nl 
                + modmapstring + nl+nl 
                + funcmodstring + nl+nl 
                + teststring + nl+nl 
                + demostring )
    print("Scadex functions: %s"%len(funcnames) )
    print("Scadex modules: %s"%len(modnames) )
    print("Scadex tests: %s"%len(testnames) )
    print("Scadex demos: %s"%len(demonames) )
    open('scadex_funcmod_list.scad', 'w').write( outstring )
    open('scadex_tags.txt', 'w').write( nl.join( tags ) )

tagnames=[]    

tag_funcs={} # { "tag1":["fn1","fn2" ...] }
func_tags={}

def extract_tags():
    
    re_tags = re.compile("^[\w_]*=\[.*, *")
    prevline = lines[0]
    for i,line in enumerate(lines):
        if i>1 and i< 10000 and  line :
            _tags = re_tags.findall( line ) 
            if _tags and prevline.startswith("//========"):
                funcname = line.split('=')[0]
                funcnames.append(funcname)
                try:
                    _tags = eval( '["'+line.strip().split('"')[-2].replace(",",'","')+'"]')
                    _tags = [x.strip() for x in _tags]
                    _tags = [x[0].upper()+ x[1:] for x in _tags]
                    func_tags.setdefault(funcname,[]).append( _tags )
                    #print "i=", i ,  line , "      ", funcname, ", tags= ", tags
                    for t in _tags:
                        tag_funcs.setdefault(t, []).append( funcname )
                except: 
                    pass
        prevline = lines[i]     
    
    print
    print
    print "# funcs: ", len(funcnames)

    # print 
    # pprint.pprint( tag_funcs ) 

    tagnames = tag_funcs.keys() 
    tagnames.sort()
    #print "tagnames= ", tagnames 
    
    tag_funcs2 = {}
    for t in tagnames:
        tag_funcs2[t] = ', '.join( tag_funcs[t] )
    
    pprint.pprint( tag_funcs2 )
    
    similarfuncs = {}
    for fn in funcnames:
        _tags = func_tags[fn]
        _tags.sort()
        for fn2 in funcnames: 
            _tags2 = func_tags[fn2]
            _tags2.sort()
            if _tags==_tags2:
                similarfuncs.setdefault( fn, [ _tags ]).append( fn2 )
    
    
        # for tag, taggedfns in tag_funcs.items():
            # if fn in taggedfns:
                # similarfuncs.setdefault( fn, []).append( [tag, taggedfns ] )
    
    #pprint.pprint( similarfuncs )
    
    
extract_tags()    


























"""
extract_scadex.py:

Takes scadex_dev.scad, extract lines to 
separate code and test code and save to two files:

    scadex.scad
    scadex_test.scad
    
    
Put this file in the folder of scadex_dev.scad and 
run:
        python extract_scad.py
        
"""

"""
from datetime import datetime
date= str(datetime.now())
# like '2011-05-03 17:45:35.177000'
date = ':'.join(date.split(':')[:-1])

lines= open('scadex_dev.scad','r')

istestcode    = False      
nextlineisver = False

header= ("//\n"
    + "// This file, %s, is extracted from\n"
    + "// file scadex_dev.scad by extract_scadex.py\n"
    + "// on " +date + ", version: %s\n"
    + "// \n")

out      = [ header%("scadex.scad","%s") ]
test_out = [ header%("scadex_test.scad", "%s")
           , "include <scadex.scad>\n\n" ]

for line in lines:

    if nextlineisver:   # extract version
                        # ["20140406-5", "...
        version = line.split(',')[0].replace('"','').replace('[','') 
        nextlineisver= False

    if (line.strip().endswith( '_test()' )        # open test code block
        or line.strip().endswith( '_test(){' ) 
        or line.strip().endswith( '_demo()' )       
        or line.strip().endswith( '_demo(){' ) 
        or line.strip().startswith( "module scadex_tests(" )
        ):
        istestcode=True

    '''
    if istestcode:
        test_out.append( line )
    else:
        out.append(line)
    '''
    
    (istestcode and test_out or out).append( line )

    if istestcode:                        # close test code block
        if line.startswith("}"):
            istestcode=False

    if line.strip().startswith("scadex_ver=["):   # extract version
        nextlineisver= True
        
test_out.append( 'scadex_tests( demo=false );' )
        
open('scadex.scad', 'w').write( ''.join(out)%version )        
open('scadex_test.scad', 'w').write( ''.join(test_out)%version )
"""