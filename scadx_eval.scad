/*
  calc{
  
    calc_basic([3,"+","x","+",3], scope=["x",3]) => 9


  }

  calc_flat("2bb^2",scope) = 32

  calc_shrink( data
                      , ops=["%", "^","* /","+-","><=&|"]
                      , _i=1
                      ,  _debug=[] 
                      )=
  (
      // data: [5, "+", 4, "*", 3, "+", 2]
      // ops: ["^","* /","+-"] : sets of operators. Each set is a str    //                        containing one or more single-char 
      //                        operators, treated equally. 
  Shrink data (for internal use of calc_flat)
    
  calc_func("sin(x)^2",["x",30]),0.25

*/

//========================================================

//========================================================
{ // calc_basic
function calc_basic( arr // arr is an array containing no arrs:
                         // ["x","+","2"], ["x"], ["2"] 
            , scope = ["x",2,"y",3] // variables must be single digits
            , isdebug=false
            , _opbuf= undef
            , _rtn  = undef
            , _sign = 1
            , _debug= []
            )=
    let( 
        ops=["*","/", "+","-","^"]   // Single digits
        ,funcs= CALC_FUNCS //["sin","ceil"] 
       , v0 = arr[0]  
     
       //--------------------------------
       // Set value. Anything other than num or var will 
       // set val = undef. But it's ok, 'cos we will set
       // conditions later to avoid using this undef
       , val= isnum(num(v0)) // If it's a num, convert to num
              ?_sign*num(v0) 
              :hash(scope,v0) // If it's var defined in scope, 
                                 // convert to value it defines.
                                 //// If not def in scope, return itself.
       , nx = slice(arr,1) // Next s.
    
       , isNegative = (v0=="-"&&_opbuf)  // When "-", and it's in op mode (means
                                         // it is behind an operator), like x*-2, 
                                         // set to turn it's next num to negative. 
//                                         
//       //--------------------------------
//       // Actual calc happens here:
//       // Hint: this would be a way to design customized 
//       //       operator, if so desired. 
       , n_rtn= isNegative? _rtn             // When neg sign, do nothing to _rtn 
                :_opbuf=="^" ? pow(_rtn,val) 
                :_opbuf=="*"? _rtn*val
                :_opbuf=="/"? _rtn/val
                :_opbuf=="+"? _rtn+val
                :_opbuf=="-"? _rtn-val
//                                             // in case of 2(x+1) when x is like 3
//                                             // it'd be 2,3 w/o operator in between.
//                                             // We put a * here. ==> need more work
//                :(_rtn!=undef && _opbuf==undef) ? _rtn*val
                :_rtn 
//                
       ,n_sign= isNegative?-1:1  // Sign carried over to next cycle       
       ,n_opbuf= isNegative      // Set the op state. 
                  ? _opbuf       // If isNegative, do nothing
                  : has(ops, v0)?v0:undef  // If v0 is any operator, store it
                  
       ,_debug= _fmth(
                concat( _debug
                      ,[ "<br/>",""
                      // , "fml", fml
                      // , "_fml", _fml
                       ]
                      ) )           
       )         
(   
    
    !isarr(arr)?
      errmsg("typeError", ["vname","arr"
                          ,"fname","calc_basic"
                          ,"want","array"
                          ,"got",type(arr)
                          ,"val", arr ] )
   :countArr(arr)>0?     
       errmsg("arrItemTypeError", ["vname","arr"
                      ,"fname","calc_basic"
                      ,"nowant","array"
                      //,"vtype",type(arr)
                      ,"val", arr ] )   
   :(
      has(funcs, arr[0])    // v0 is a func name, proceed with the
                            // rest by setting _func= func name
      ? run( arr[0], calc_basic( nx, scope=scope, isdebug=isdebug, _debug=_debug) ) 
      : ( _rtn==undef   // start up, _rtn is undef
          ? (
              v0=="-" 
              ? calc_basic(  nx, scope=scope, _opbuf=undef  ,_rtn= undef, _sign=-1, isdebug=isdebug, _debug=_debug )
              : calc_basic(  nx, scope=scope, _opbuf=n_opbuf,_rtn= val  , _sign=n_sign, isdebug=isdebug, _debug=_debug )
            )
          : arr 
             ? calc_basic(nx, scope=scope, _opbuf=n_opbuf,_rtn=n_rtn, _sign=n_sign, isdebug=isdebug, _debug=_debug )
             : (isdebug?_debug:_rtn)  // When running out of upks, return _rtn
       )    
   )   
);      
    calc_basic= ["calc_basic", "arr,scope", "val", "eval"
    ,"This is mainly used for calc(). Given arr representing an one-dimension, 
    ;; calculable array (containing no array), and scope given as a hash, calc 
    ;; the result.
    ;;
    ;; calc_basic( ['x','+',1], ['x',3] ) => 4 
    ;; calc_basic( ['x','^',2], ['x',3] ) => 9 
    ;;
    ;; Note that calc is done left to right, no matter what operator is:
    ;;
    ;; calc_basic( [2,'+','x','^',2], ['x',3] ) => 25 
    ;;
    "];
   
    function calc_basic_test( mode=MODE, opt=[] )=
    (
        let( isdebug=false)        
        doctest( calc_basic
        ,[ 
           [ calc_basic([5]),5, [5]]
          ,[ calc_basic(["x"], scope=["x",3]), 3, "['x'],['x',3]"]
          ,[ calc_basic(["x","+",1],["x",3]), 4, "['x','+',1], ['x',3]" ]
          ,[ calc_basic(["x","+","3"], scope=["x",3]), 6, "['x','+','3'],['x',3]"]
          ,[ calc_basic(["x","*","2"], scope=["x",3]),6,"['x','*','2'],['x',3]"]
          ,[ calc_basic([3,"+","x","+",3], scope=["x",3]),9,"[3,'+','x','+',3],['x',3]"]
          ,[ calc_basic([2,"+","x","*",2], scope=["x",3]),10,"[2,'+','x','*',2],['x',3]"]
    
          ,"// Multiple vars"
          ,[ calc_basic([2,"+","x0","*","y"], scope=["x0",3,"y",4]),20,  
              "[2,'+','x0','*','y'],['x0',3,'y',4]"]
           
           ,""
           ,"// power "
           ,""
           ,[ calc_basic(["x","^","3"], scope=["x",3]), 27,"['x','^','3'],['x',3]"]
           
           ,[ calc_basic(["x","^","2","/","4"], scope=["x",3]),2.25, "['x','^','2','/','4'],['x',3]"]
           ,[ calc_basic(["x","^","0.5"], scope=["x",4]),2,"['x','^','0.5'],['x',3]"]
           ,[ calc_basic(["x","^","-2"], scope=["x",2]),0.25,"['x','^','-2'],['x',3]"]
    
           ,""
           ,"// negative "
           ,""
           ,[ calc_basic(["x","*","-2"], scope=["x",3]), -6,"['x','*','-2'],['x',3]"]
           ,[ calc_basic(["-2","*","x"], scope=["x",3]),-6,"['-2','*','x'],['x',3]"]
           ,[ calc_basic(["x","-","-21"], scope=["x",3]),24,"['x','-','-21'],['x',3]"]        
           ,""
           ,"// functions. Func name must be the first item."
           ,""
           ,[calc_basic(["sin",30]),0.5,["sin",30]]
           ,[ calc_basic(["ceil",2.7]),3,["ceil",2.7]]
                      
            ,""
            ,"// 2015.2.23: trying to make 2x+3 work:"
            ,""
            ,[calc_basic([2, "*", "x", "+", 1], ["x",3]),7
                    ,"[2,'*','x','+',1], ['x',3]"
                    ]
            ,""
           ,"// It takes only simple array, i.e., the array should NOT have array item."
           ,""
           ,str("> calc('x*2')= ", _red(calc_basic("x*2") ))
           ,""
           ,str("> calc((['2', '+', 'ceil', ['2.7']])= "
                , _red( calc_basic(["2", "+", "ceil", ["2.7"]]) )
                )
    //       ,""
    //       ,"// debugging "
    //       ,""
    //       ,[["sin",30], calc_basic(["sin",30]), 0.5]
           
           
        ], mode=mode 
        )
    );   
   
} // calc_basic

//=============================================================
{ // calc_blk ===> where is this func defined ?
    calc_blk=["calc_blk","s,scope","num","Math",
    "calc"
    ];

    function calc_blk_test( mode=MODE, opt=[] )=
    (  //==========================
        //isdebug=false;
        
        doctest( calc_blk
        ,[ 
           ""
         ,  [calc_blk("11"),11,"'11'" ]
         , [calc_blk("3/2+1"),2.5, "'3/2+1'" ]
         , [calc_blk("x+1", ["x",3]),4, "'x+1',['x',3]" ]
         , [calc_blk("(y+2)*x", ["x",3,"y",4]),18, "'(y+2)*x',['x',3,'y',4]" ]
         , [calc_blk("x*(y+2)", ["x",3,"y",1]),9, "'x*(y+2)',['x',3,'y',1]" ]
         , [calc_blk("x*(y+2)^2", ["x",3,"y",1]),27, "'x*(y+2)^2',['x',3,'y',1]" ]
         , [calc_blk("x*(2y+2)^2", ["x",3,"y",1]),48, "'x*(2y+2)^2',['x',3,'y',1]" ]
        , [calc_blk("x*((y+2)^2)", ["x",3,"y",1]),27, "'x*((y+2)^2)',['x',3,'y',1]" ]
         , [calc_blk("(x*(y+2))^2", ["x",3,"y",1]),81, "'(x*(y+2))^2',['x',3,'y',1]" ]
    //     , [calc_blk("sin(x)", ["x",30]),0.5,"'sin(x)',['x',30]",  0.5 ]
    ////     , [calc_blk("sin(x)*2", ["x",30], isdebug=isdebug),1,"'sin(x)*2',['x',30]"
    ////         ,  1 ]
    //     , [calc_blk("sin(x)^2", ["x",30]),0.25, "'sin(x)^2',['x',30]",  0.25 ]
    //     , [calc_blk("(x+2)+sin(x)", ["x",30]), 32.5, "'(x+2)+sin(x)',['x',30 ]", 32.5 ]
    //     , [calc_blk("(x+2)+(sin(x)^2)", ["x",30]), 32.25,"'(x+2)+(sin(x)^2)',['x',30 ]", 32.25 ]
    //    
    //    
    //     , [calc_blk("sin(x)^2+(cos(x)^2)", ["x",30]), 1,"'sin(x)^2+(cos(x)^2)',['x',30]", 1 ]
    // 
    //       ,""
    //       ,"// 3x is interpretated as 3 * x"
    //       ,""
    //     , [calc_blk("2x", ["x",3]), 6,"'2x',['x',3]", 6 ]
    //     , [calc_blk("2x+1", ["x",3]), 7,"'2x+1',['x',3]", 7 ]
    //     , [calc_blk("2x+1.5y", ["x",3,"y",4]),12,"'2x+1.5y',['x',3,'y',4]",  12 ]
    //     
    //     , ""
    //     , "// Net yet ready for 2(x+1) "
    //     , ""
    //     , [ calc_blk("2(x+1)", ["x",3]), 8, "'2(x+1)', ['x',3]", ["notest",true] ] 
    //     , str(" calc_blk(\"2(x+1)\", [\"x\",3]) wants: 8, but: ", calc_blk("2(x+1)", ["x",3]) )
         
         ], mode=mode )

    //    module debug(fml)
    //    {
    //        uni = getuni(fml, blk=CALC_BLOCKS);
    //        puni= packuni(uni);
    //        echo( "<br/>", _fmth(["fml",fml, "uni",uni,"puni",puni]) ); //uni= _fmt(uni), puni= _fmt(puni) );
    //    }
    //      debug("2(x+1)");
    //    debug("(y+2)*x");
    //    debug("sin(x)*2");   
    //    debug("(x+1)*sin(y+2)");
    //    debug("(x+1)*sin(y+2)*3");
    //    debug("sin(x)^2+cos(x)^2");
    //    debug("(x+2)+(sin(x)^2)");
    //    debug("(sin(x))^2+((cos(x))^2)");
    );    

    //echo( calc_blk_test()[1] );
} // calc_blk

//=============================================================
{ // calc_axb
function calc_axb(a,op,b)=
(
   op=="+"? a+b
   :op=="-"? a-b  
   :op=="*"? a*b  
   :op=="/"? a/b  
   :op=="<"? a<b?1:0
   :op==">"? a>b?1:0
   :op=="="? a==b?1:0
   :op=="&"? a&&b?1:0
   :op=="|"? a||b?1:0
   :op=="%"? a%b  
   :op=="^"? pow(a,b)  
   :undef 
);

//echo( 3_4 = calc_axb( 3,"<",4 ) );
//echo( 3_4 = calc_axb( 3,">",4 ) );
//echo( 3_4 = calc_axb( 5,"<",4 ) );
//echo( 3_4 = calc_axb( 5,">",4 ) );
//echo( 3_4 = calc_axb( 3,"=",4 ) );
//echo( 3_4 = calc_axb( 3,"=",3 ) );
} // calc_axb

//=============================================================
{ // calc_shrink
function calc_shrink( data
                    , ops=["%", "^","*/","+-","><=&|"]
                    , _i=1
                    ,  _debug=[] 
                    )=
(
    // data: [5, "+", 4, "*", 3, "+", 2]
    // ops: ["^","*/","+-"] : sets of operators. Each set is a str    //                        containing one or more single-char 
    //                        operators, treated equally. 
    let(
         opset = ops[0]
         
       , iod= // Find the first item in data matching any of opr in
              //  this opr set. For example, if opset="*/", then 
              //  get the index of first found * or / in data 
              //
              //  old code (working):
              // [ for(i=range(data))
              //        if( index(opset, data[i])>-1) i
              //    ][0] // <=== pick the first one
              //
              // new code. match return like [[2,2, ["/"]],[9,9, ["*"]]]
              // or false. Note: false[0]=> undef
              match( data, ps=[opset] )[0][0]
                  
       , op = data[iod]  // what the found operator is
                  
       , newops= iod==undef? // If this opset fail, go next set 
                  slice( ops, 1) : ops
                  
       , newdata= iod==undef? data 
              :[ for( i=range(data) ) //if (data[i]!=op) data[i] 
                  if( i<iod || i>iod+1 ) // Say data= [ .... x,"+",y ....]
                                         // Item before x and after y are unchanged 
                                         // When reach x, calc x+y, and continue
                                         // to the item after y 
                     i==iod-1? calc_axb( data[i], op, data[i+2] )
                     : data[i]     
                ]   
       , newi = iod==undef
                  ? 0        // opset has no match in data, reset _i to 0 for next opset 
                  : (_i+1)   // opset has match in data, continue to see if more match
                             //  like: [5, "*", 4, "+", 3, "*", 2] has 2 *   
//      ,_debug=  concat(_debug,
//                [_fmth([ "data", data
//                       , "ops", ops
//                       , "opset", opset
//                      , "_di", _di
//                      , "iod", iod
//                      //, "_oi", _oi
//                      , "op", op
//                      , "di", di
//                      , "newops", newops
//                      , "newdata", newdata
//                      ]
//                     ) 
//                ])           
       )  
    // NOTE: len(data)>3 is enough. We throw in "ops" to prevent 
    //       anything goes wrong resulting in endless run
    len(data)>3 && ops 
       ? calc_shrink( newdata, newops, _di=newi, _debug=_debug)
    : calc_axb( data[0], data[1], data[2] )
    //: str( "<br/>",join(_debug, "<br/>"))
                  
//    _oi<len(ops)? calc_shrink( new, ops, _oi=_oi+1)
//    : new[0]
); 
    calc_shrink=["calc_shrink","data,level","arr","calc"
    , "Shrink data (for internal use of calc_flat)"
    ];

    function calc_shrink_test( mode=MODE, opt=[] )=
    (
       let( data1 = [3,"-",4,"*",5,"+", 3 ]
          )
       doctest( calc_shrink,
       [
         "var data1"
       , [ calc_shrink(data1),-14,data1 ]
       ]
       , mode=mode, opt=opt, scope=["data1",data1]
       )
    );

    //echo( calc_shrink_test()[1] );

} // calc_shrink
        
//=======================================================
{// calc_flat
function calc_flat(   // calc flat formula (no function name, no ( ) )
            s
          , scope= []
          , _i =0
          , _buf=[]
          , _debug=""
          , _lv=0
          )=
(
    let(lv=_lv)
    //echo(log_b(_s("calc_flat({_},{_})",[s,scope]), lv))
    let( s=str(s)
       , ops = "%^*/+-<>=&|" 
         // Match named patterns to current i
         // Returned m => ["word",2,4,["abc"] ]
       , ptns=[                  
                ["op",[[ops,1]], -1]
              , ["word",[ A_zu], -1]  
              , ["num", [0_9d],  -1]
              , ["sp", [" "],    -1]  // space to skip
              ]
      // , cond = split(s, "?")
      // , cond_tf= len(cond)>1? split(cond[1],":"):undef
      // , m = len(cond)==1? match_nps_at( s, _i, ptns):undef
       , 
       , m = match_nps_at( s, _i, ptns )//[n,i,j,ms,ma]
       , name = m[0]
       , v    = m[4][0]
       , _val = name=="num"? num(v)
                : name=="word"? hash(scope, v)
                : undef
       //----------------------------------------------
       // Handle 2 conditions:
       // (1) 3x => add additional "*" between 3 and x
       // (2) 5+-6 => when we at 6, the previous 2 items
       //              in _buf are an op and a "-"
       //          => make 6=-6, and take "-" away from _buf
       , last_is_op = has( ops, get(_buf,-1))
       , isneg  = _val!=undef 
                  && last(_buf)=="-" 
                  && ( _i==1 || has( ops, get(_buf,-2)))
       , val = isneg? -_val:_val
               
       , newi = _i +( m ? len(v):1) 
       , _buf = val && !last_is_op&&_buf
                 ? app( _buf, "*" )     //... condition (1)
                :isneg ? slice(_buf,0,-1) //... condition (2)
                :_buf
                
       , newbuf= concat( _buf, val!=undef?[val]:name=="op"?[v]:[]
                       )  
       , _debug= str( _debug,"<br/>"
                    ,"{ s[_i=",_i,"]= ", s[_i] 
                    ,", m= ", m
                    ,", name= ", name
                    ,", v= ", v
                    ,", isneg= ", isneg
                    ,", val= ", val 
                    ,", newi= ", newi
                    ,", newbuf= ", newbuf
                    ,"}"
                    )
       )
   //echo(log_(["_i",_i, "newbuf",newbuf
   //          ,"calc_shrink(newbuf)", calc_shrink( newbuf )],lv))
                    
   let( rtn =    _i>=len(s)-1?  ( len(newbuf)==1?newbuf[0]
                		   : calc_shrink( newbuf ) )
		   : calc_flat( s, scope, newi, newbuf, _debug, _lv=lv+1)              
       )
   //_i>=len(s)-1? _debug
   //cond_tf? (
   //_i>len(s)-1 ? _debug
             
   //echo(log_e(["rtn=",rtn],lv))
   
   rtn
  

);
    calc_flat=["calc_flat","s,scope","number","Math"
    , "Calc simple formula s (no ( ) ).
    ;; 
    ;; operator priority: %, ^, */, +-, &|><=
    ;;
    "
    ];
    function calc_flat_test( mode=MODE, opt=[] )=
    (
        let(scope=["a",3,"bb",4])
        doctest( calc_flat,
        [
          str("scope= ", _fmt(scope))
        , [ calc_flat("2+3",scope), 5, "'2+3',scope" ]    
        , [ calc_flat(5,scope), 5, "5,scope" ]    
        , [ calc_flat("5",scope), 5, "'5',scope" ]    
        , [ calc_flat("-5",scope), -5, "'-5',scope" ]    
        , [ calc_flat("a+bb",scope), 7, "'a+bb',scope" ]    
        , [ calc_flat("a",scope), 3, "'a',scope" ]    
        , [ calc_flat(" a + bb/ 2",scope), 5, "' a + bb/ 2',scope" ]    
        , [ calc_flat("a+bb^2",scope), 19, "'a+bb^2',scope" ]    
        , [ calc_flat("a^bb",scope), 81, "'a^bb',scope" ]    
        , [ calc_flat("a/bb*3",scope), 2.25, "'a/bb*3',scope" ]    
        , [ calc_flat("a*bb^2",scope), 48, "'a*bb^2',scope" ]    
        , [ calc_flat("2bb^2",scope), 32, "'2bb^2',scope" ]    
        , [ calc_flat("3.6/a",scope), 1.2, "'3.6/a',scope" ]    
        , [ calc_flat("2bb+3.6",scope), 11.6, "'2bb+3.6',scope" ]    
        , ""
        , [ calc_flat("a+2bb",scope), 11, "'a+2bb',scope" ] 
        ,""
        ,"// Allow new operators:"
        , [ calc_flat("1+a%2+bb",scope), 6, "'1+a%2+bb',scope" ]    
        , [ calc_flat("3=3",scope), 1, "'3=3',scope" ]    
        , [ calc_flat("a>bb",scope), 0, "'a>bb',scope" ]    
        , [ calc_flat("a<bb",scope), 1, "'a&lt;bb',scope" ]    
        , [ calc_flat("a&bb",scope), 1, "'a&bb',scope",["//","a and b"] ]    
        , [ calc_flat("a|bb",scope), 1, "'a|bb',scope",["//","a or b"] ]    
        , [ calc_flat("3<4",scope), 1, "'3&lt;4',scope" ]    
        
        ]
        , mode=mode
        , opt= opt //update(opt,["showscope",true])
        , scope=scope
        )
    );

    module calc_flat_test0(){
        echo( module_name = parent_module(0) );
        scope= ["a",3,"bb",4];
    echo( calc_flat( "a/bb*3",scope ) ); 

    echo( "2.25? ",calc_flat( "a/bb*3",scope )==2.25 ); 
    echo( "2.25? ",calc_flat( "a*3/bb",scope )==2.25 ); 
    echo( "19 ? ", calc_flat( "a+bb^2", scope)==19 ); 
    echo( "11 ? ", calc_flat("a*bb/2+5",scope)==11);
    echo( "10 ? ", calc_flat("a+bb/2+5",scope)==10);
    echo( calc_flat("  a+ bb /2+ 5",scope));
    echo( "10 ? ", calc_flat("  a+ bb /2+ 5",scope)==10 );
    echo( "24 ? ", calc_flat("a+4bb+5",scope)==24 ); 
    echo( "24 ? ", calc_flat("1+a*2bb^2*2",scope)==193 ); 
    echo( "305 ? ", calc_flat("a+bb/2+300",scope)==305 ); 
    echo( "305.15 ? ", calc_flat("a+bb/2+300.15",scope)==305.15 );
    echo( "305 ? ", calc_flat("a+300+bb/2",scope)==305 );    
    echo( "305.16 ? ", calc_flat("a+300.16+bb/2",scope)==305.16 ); 
    echo( "2.25 ? ", calc_flat("a/bb*3",scope)==2.25 ); 
    echo( repeat("-",20) );
    echo( calc_flat( "5") );
    echo( calc_flat( "-5") );
    echo( calc_flat( "3*-5") );
    echo( calc_flat( "3*5%2+1") );
    echo( "calc_flat('a<b',scope)= ", calc_flat("a<b",["a",3,"b",4]));
    echo( "calc_flat('a<bb',scope)= ", calc_flat("a<b",scope=scope));
    echo( "calc_flat('a<b',scope)= ", calc_flat("a<b",["a",3,"b",4]));
    echo( calc_flat( "3-5") );
        } 

    //calc_flat_test0();
} // calc_flat
      
//=======================================================
//{ // calc_func
//    
////ISLOG=0;    
//function calc_func(
//          s           // s is a formula that might have paranthesis 
//        , scope = []  // like: ["x",3,"age",20]
//        , returnWithVars=0 // 18.10.8: when true, return the val(s) with corresponding var(s)
//        , _pis= undef // paranthesis indices, indices of "(", like:  [0,9]
//        , _debug= ""
//        , _lv=0
//        )=
//(
//   let(lv=_lv)
//   //echo( log_b( _s("calc_func(\"{_}\"), {_}",[s,scope]), lv) )
//   let( ops= "%^*/+-<>=&|"
//      //---------------------------------------------------------
//      , scp_vals= vals(scope) //:: ["x",[2,3,4],"y",5]=> [ [2,3,4], 5 ]
//      , scp_arr_is = [ for(i=range(scp_vals))   
//                      if( isarr( scp_vals[i])) i] // indices of arr in scope
//      , run_scp = scp_arr_is  // need to run thru scope to resolve array value
//      //---------------------------------------------------------
//      // Chk if s contains aaa?bb:cc struct. If yes, all 
//      // critical assignments inside this let() will be skipped
//      // and it will be resolved in the func body first. 
//      , cond = split(s,"?")
//      //, run_cond= !(run_scp || len(cond)==1)
//      , run_cond= !(scp_arr_is || len(cond)==1)
//      , cond_tf= run_cond? split(cond[1],":"):undef
//      //---------------------------------------------------------
//      // Find the indices of "(". This is done only on the
//      // first run. The generated pis (paranthesis indices)
//      // will be carried over down the calc. 
//      //
//      , isbeg= _pis==undef   // Is this the first run?
//      , blkm = !run_cond &&isbeg?   // Match blocks in the first run    
//                 match(s,ps=[["(",1]])
//                      //:[[0,0,"(",["("]],[9,9,"(", ["("]]]
//                 :undef
//      ,_pis = blkm ? [ for(m=blkm)m[0] ]  //:[0,9]
//                 : _pis
//      
//      //---------------------------------------------------------
//      // Matching blocks, starting backward from the last item
//      // of _pis. blk: [3, 7, "(y+2)", ["(", "y+2", ")"]]           
//      , blk = !run_cond && _pis?         
//                match_ps_at( s
//                           , i= last(_pis)   // Starting backward
//                           , ps=[["("],str(A_zu,0_9d,ops),[")"]]
//                           )    
//               :undef     
//      , _blkval= blk?        // Convert extracted content: "y+2"=> 3 
//                calc_flat( blk[3][1], scope) 
//                : undef
//      
//      //---------------------------------------------------------
//      // Check if there's a func name before the found "("
//      // If yes, do a calc with it
//      , prestr = !run_cond?slice(s,0, blk[0]):undef// substr before "("
//      , _fname = !run_cond?
//                  matchr_at( prestr,-1,CALC_FUNCS ) //:[0,4,"round"]
//                  : undef
//      , fname = _fname[2]
//      , blkval = fname?
//                 run( fname, _blkval):_blkval
//      //, blkval = is0(__blkval)?0:__blkval 
//                 
//      //---------------------------------------------------------
//      // Adjust the ending index of str on the left
//      , prestr_lasti = blk[0]-1-(fname?len(fname):0) //
//      
//      //---------------------------------------------------------
//      // Prepare for the next run
//      , newpis = !run_cond && _pis? slice(_pis,0,-1): undef
//      , news = !run_cond&&_pis?     //: blk= [3, 7, ["(", "y+2", ")"]]     
//               str( slice(s,0, prestr_lasti+1) //blk[0]-(fname?len(fname):0))
//                  , //prestr_lasti>0
//                    s[ prestr_lasti ] //: not the first item
//                    && 
//                    !has( str("(",ops), s[prestr_lasti])
//                      ? "*":""
//                  , blkval
//                  //, slice(s, blk[1]+1)
//                  , slice(s, blk[1])
//                  ):undef
//   )
//   //echo(log_( ["scp_vals", scp_vals, "scp_arr_is", scp_arr_is],lv ))
//   //echo(log_( ["_pis", _pis, "newpis", newpis, "blk",blk],lv ))
//   //echo(log_( ["blkval",blkval, "news", news],lv ))
//   //echo(log_( [ "run_cond", run_cond
//              //, "isbeg && !blkm ",isbeg && !blkm 
//              //, "!newpis", !newpis
//              //], lv ))
//   let(                               
//       rtn =    scp_arr_is?  
//                  //echo( log_("Calling next calc_func to deal with array vars", lv))
//                  //echo( log_(["scp_vals",scp_vals], lv))
//                  let( permutedVals= permutes(scp_vals) 
//                     )  
//                     //echo( log_(["permutedVals",permutedVals], lv) )
//                     returnWithVars?
//                     [ for(vals=permutedVals)
//                       let( newscope= zip(keys(scope),vals,isflat=1))
//                       //echo( log_(["vals",vals, "keys", keys(scope), "newscope", newscope], lv ))
//                       concat( vals(newscope), [calc_func( s, scope= newscope )])
//                     ]              
//                     : [ for(vals=permutedVals)
//                       let( newscope= zip(keys(scope),vals,isflat=1))
//                       //echo( log_(["vals",vals, "keys", keys(scope), "newscope", newscope], lv ))
//                       calc_func( s, scope= newscope )
//                     ]              
//                                     
//                  //[ for(v = scp_vals[ scp_arr_is[0] ] )
//                    //calc_func( s, scope= [ for(x=scope) isarr(x)?v:x ]
//                             //, _lv=lv+1 )  
//                  //]
//                  
//                 :run_cond?
//                   //echo( log_("Calling next calc_func to deal with a?b:c", lv))
//                   (  
//                      calc_func( cond[0], scope,_lv=lv+1 )
//                     ?calc_func( cond_tf[0], scope,_lv=lv+1 )
//                     :calc_func( cond_tf[1], scope,_lv=lv+1 )
//                   )
//                 :isbeg && !blkm ? calc_flat( s,scope,_lv=lv+1)
//                 :!newpis? calc_flat( news,scope,_lv=lv+1)
//                 : calc_func(news, scope, _pis=newpis, _debug=_debug,_lv=lv+1)   
//      )
//
//   //echo(log_e("",lv))
//   rtn
//);  
//
//    calc_func=["calc_func","s,scope, returnWithVars","num","Math",
//    "Calc a string formula with variables defined in scope.
//    ;; 
//    ;; 1) Allowed operators in order of priority:
//    ;;
//    ;;  %, ^, */, +-, &lt;>=&|
//    ;;
//    ;; 2) Boolean operations (&lt;>=) are indicated with single
//    ;; letter op, i.e., 
//    ;;  
//    ;;   a&b is a&&b, a|b is a||b, a=b is a==b
//    ;; 
//    ;; There's no way to carry out >=, &lt;= or &lt;>.
//    ;;
//    ;; 3) Also acceptable is conditional statement:
//    ;; 
//    ;;  a>b?b:c
//    ;;
//    ;; 4) 2a is treated as 2*a, and 2(a+1) as 2*(a+1)
//    ;; 
//    ;; 5) Spaces will be ignored.
//    ;;
//    ;; 6) Set returnWithVars=1 to return the vals with their corresponding vars
//    ;;    This allows for computation of pts 
//    "
//    ,"calc_axb,calc_flat"
//    ];
//
//    function calc_func_test( mode=MODE, opt=[] )=
//    (  //==========================
//        //isdebug=false;
//        
//        doctest( calc_func
//        ,[ 
//           "// Simple operations. Note that */ has priority over +-:"
//           
//         , [calc_func("11"),11,"'11'" ]
//         , [calc_func("-11"),-11,"'-11'" ]
//         , [calc_func("1+3/2"),2.5, "'1+3/2'" ]
//         ,""
//         ,"// With variables:"
//         
//         , [calc_func("x+1", ["x",3]),4, "'x+1',['x',3]" ]
//         , [calc_func("x+yz^2", ["x",3,"yz",4]),19, "'x+yz^2',['x',3,'yz',4]" ]
//         , ""
//         , "// With paranthesis:"
//         
//         , [calc_func("(y+2)*x", ["x",3,"y",4]),18, "'(y+2)*x',['x',3,'y',4]" ]
//         , [calc_func("x*(y+2)^2", ["x",3,"y",1]),27, "'x*(y+2)^2',['x',3,'y',1]" ]
//         , [calc_func("x*(2y+2)^2", ["x",3,"y",1]),48, "'x*(2y+2)^2',['x',3,'y',1]" ]
//         , [calc_func("(x*(y+2))^2", ["x",3,"y",1]),81, "'(x*(y+2))^2',['x',3,'y',1]" ]
//         
//         ,""
//         ,"// built-in functions:"
//         
//         , [calc_func("sin(x)",["x",30]),0.5,"'sin(x)',['x',30]" ]
//         , [calc_func("cos(x)",["x",30]),0.866025,"'cos(x)',['x',30]"]
//         , [calc_func("round(x/2+1)",["x",3]),3,"'round(x/2+1)',['x',3]"]
//              
//         , [calc_func("sin(x)^2",["x",30]),0.25, "'sin(x)^2',['x',30]"]
//         , [calc_func("(x+2)+sin(x)", ["x",30]), 32.5, "'(x+2)+sin(x)',['x',30 ]"]
//         , [calc_func("(x+2)+(sin(x)^2)", ["x",30]), 32.25,"'(x+2)+(sin(x)^2)',['x',30 ]"]
//         , [calc_func("cos(x)sin(x)", ["x",30]), 0.433013,"'(x+2)+(sin(x)^2)',['x',30 ]", ["asstr",true]]
//         , ""
//         ,"// The following: sin(x)^2+cos(x)^2 produces 0.999999, requiring "
//         ,"//  round(0.999999) to make it 1:"
//         , [round(calc_func("sin(x)^2+cos(x)^2", ["x",30])), 1,"'sin(x)^2+(cos(x)^2)',['x',30]" ]
//             
//           ,""
//           ,"// 3x is interpretated as 3 * x"
//           
//         , [calc_func("2x", ["x",3]), 6,"'2x',['x',3]"]
//         , [calc_func("2x+1", ["x",3]), 7,"'2x+1',['x',3]"]
//         , [calc_func("2x+1.5y", ["x",3,"y",4]),12
//                ,"'2x+1.5y',['x',3,'y',4]"]
//         , [ calc_func("2(x+1)", ["x",3]), 8, "'2(x+1)', ['x',3]" ] 
//         , [calc_func("2sin(x)",["x",30]),1,"'2sin(x)',['x',30]" ]
//    //     ,""
//         ,""
//         ,"// negative"
//         , [calc_func("sin(270)"),-1,"'sin(270)'"]
//         
//         ,""
//         , "// conditional"
//         , [calc_func("a<b?2a:2b+1",["a",3,"b",4]),6
//                ,"a&lt;b?2a:2b+1,['a',3,'b',4]"]
//         , [calc_func("a>b?2a:2b+1",["a",3,"b",4]),9
//                ,"a>b?2a:2b+1,['a',3,'b',4]"]
//         , ""
//         , "// array in scope returns a list "
//         , [calc_func("sin(x)",["x",[0,90,270]]),[0,1,-1]
//                ,"'sin(x)',['x',[0,90,270]"]
//         , [calc_func("sin(180*x)",["x",range(0,0.5,2)]),[0, 1, 0, -1]
//                ,"'sin(180*x)',['x',[0:0.5:2]"]
//         , [calc_func("1+x^2",["x",range(4)]),[1,2,5,10]
//                ,"'1+x^2',['x',range(4)"]
//         , [calc_func("x%2?x^2:0",["x",range(6)]),[0,1,0,9,0,25]
//                ,"'x%2?x^2:0',['x',range(6)]"]
//         , ""
//         , "// More than one array/dimension "
//         , ""
//         , [calc_func("x^2+y", ["x",[0,1,2],"y",[0,1,2]])
//            ,[0, 1, 2, 1, 2, 3, 4, 5, 6]
//            ,"'x^2+y', ['x',[0,1,2],'y',[0,1,2]]"
//            ]
//         
//         , ""
//         , "// Set returnWithVars=1 to return the vals with their corresponding vars"
//         , ""
//            
//         , [calc_func("x^2+y", ["x",[0,1,2],"y",[0,1,2]], returnWithVars=1)
//            ,[[0, 0, 0], [0, 1, 1], [0, 2, 2]
//            , [1, 0, 1], [1, 1, 2], [1, 2, 3]
//            , [2, 0, 4], [2, 1, 5], [2, 2, 6]]
//            ,"'x^2+y', ['x',[0,1,2],'y',[0,1,2]]"
//            , ["ml",1]
//            ]
//            
//                   
//         , ""   
//         , "// Note:Use permutes([x,y]) to get a list of [x,y]'s:"
//         , ">>> permutes( [ [0,1,2], [0,1,2] ] ) = "       
//         , str("xys= ", permutes( [ [0,1,2], [0,1,2] ] ))
//         
//         ], mode=mode, opt=opt )
//
//    );    
//
//} // calc_func


//========================================================
{ // eval
function eval(s,scope=[])=
(                
   let( s = trim(s)
      , n = num(s)
      , _arr = s[0]=="[" && endwith(s,"]")
      )
//   arrc   
   s=="true"?true
   : s=="false"?false
   : s=="undef"?undef
   : isnum(n)? n
   : _arr? evalarr( s,scope=scope) 
   : s[0]=="\"" && endwith(s,"\"")? slice(s,1,-1)     
   : (scope?hash(scope, s, s):s)
);
//echo( evalarr("[0,1]"));

    /*   

        [2, [3, 4],5,[a, [ b,6]]]
        0123456789 123456789 1234
        
        i c  l  blk rtn nlys    nblk       nrtn  
        0 [  [] []  []  [0]    [[]]        []
        1 2             [0]    [[2]]       [2]
        4 [             [0,1]  [[2],[]]   [2]
        5 3               "    [[2],[3]]   [2]
        8 4               "    [[2],[3,4]]   [2]       
        9 ]             [0]    [[2,[3,4]]   ]   [2,[3,4]]       
       11 5             [0]    [[2,[3,4],5] ]   [2,[3,4],5]  # if at root, can add to rtn            
       13 [             [0,1]  [[2,[3,4],5],[]]      "
       14 a               "    [[2,[3,4],5],[a]]     "
       17 [            [0,1,2] [[2,[3,4],5],[a],[]]     "  
       19 b               "   [[2,[3,4],5],[a],[b]]     "
       21 6               "   [[2,[3,4],5],[a],[b,6]]     "
       22 ]             [0,1] [[2,[3,4],5],[a,[b,6]] ]   [2,[3,4],5]  #  NOTE
       23 ]              [0]  [[2,[3,4],5,[a,[b,6]]] ]   [2,[3,4],5, [a,[b,6]]] 
                                                               [2,[3,4],5,["a",["b",6]]]

    */
    eval=["eval","s,scope","any","String,Core,Inspect",
      "Eval s to a value"
    ,""];

    function eval_test( mode=MODE, opt=[] )=
    (
        doctest( eval, 
        [
          [ eval("3"), 3, "'3'"]
        , [ eval("-3.5"), -3.5, "'-3.5'"]
        , [ eval("true"), true, "'true'"]
        , [ eval("false"), false, "'false'"]
        , [ eval("undef"), undef, "'undef'"]
        , [ eval("\"false\""), "false", "''false''"]
        , [ eval("[0,1,23]"), [0,1,23], "'[0,1,23]'"] 
        , [ eval(" [3.4, 5, \"age\"] "), [3.4,5,"age"]
                  , "' [3.4, 5, \"age\"] '"]
        , [ eval(" age ",["age",20]), 20, "' age ', ['age',20]"]
        , [ eval(" [5, age] ",["age",20]), [5,20]
                  , "' [5, age] ', ['age',20]"]
        , [ eval(" [5, \"age\"] ",["age",20]), [5,"age"]
                  , "' [5, \"age\"] ', ['age',20]"]
        , ""         
        , "//Nested array taken care by evalarr():"
        , [ eval("[5, [age, 10]]",["age",20]), [5,[20,10]]
                  , "'[5, [age, 10]]', ['age',20]"
                  ,["nl",true]]
        ]
        , mode=mode, opt=opt )
    );
} // eval

//=======================================================
{ // evalarr

function evalarr( 
     s
   ,scope=[]
   ,blksymbol="[]"
   ,sp = "," 
   ,itemptn = str(A_zu,0_9d,"- \"") 
   , _lys= []  // indeice of Layer of nested blocks. 
               //  [0,4,5] means, current block is the 5th blk inside the 4th one on the root level
   , _blk= [] // Contains the root block. Each item is an array representing a block
   , _rtn= []
   , _i=0              
   , _debug=[]
   )= 
(
  let (  s= _i==0?trim(s):s
        , _c= s[_i]
        , skip =  _c==sp || _c==" "
        , b = _c== blksymbol[0] //"[" 
        , e = _c== blksymbol[1] //"]" 
        , m = !b&&!e&&!skip?
                match_ps_at( s, _i
                           , ps=[ itemptn //str(A_zu,0_9d,"=- \"")
                                , [str(sp,blksymbol),1]
                                ]
                           , want=0     
                           )
                //, [",[]",1]])
               :false 
        //, cc = m?slice( s, _i, m[1]) :_c
        , cc = m?m[3][0] :_c
        , c= !b&&!e&&!skip? eval(cc, scope):cc
        , nlys = skip? _lys
                 :b? concat( _lys, len(_blk))   // Add new block index
                 :e? slice( _lys,0, -1)        // Reduce block indices
                 : _lys                        
        
        , nblk= skip? _blk
                :b? app( _blk, [])
               :e && nlys? appi( slice( _blk,0,-1), -1, last(_blk))
               : appi( _blk, -1, c) //hash(scope,c,c)) //eval(c,scope))
        ,nrtn = skip? _rtn
                :e&&nlys==[0]? app( _rtn, last(_blk))
               //:c&&nlys==[0]&&!b&&!e? app( _rtn, c)  <== cause wrong result
                                              
               :nlys==[0]&&!b&&!e? app( _rtn, c) 
               :_rtn               
        , _debug = str(_debug
                      , "<br/>"
                      , _fmth(["_i", _i
                              ,"c",c
                              , "m", m 
                              ,"nlys", nlys
                              ,"nblk", nblk
                              ,"_rtn", _rtn
                              ,"nrtn", nrtn
                              ]
                             )
                      )//debug
  )//let

    _i<len(s) && nlys ?
         evalarr( s, scope, blksymbol,sp, itemptn
                , _lys=nlys
                , _blk=nblk
                 , _rtn=nrtn 
                 , _i = _i+ len(str(cc))
                 , _debug=_debug
                 )
    : _rtn
    //: _code(_debug)
);  

    evalarr=["evalarr","s,scope","any","String,Core,Inspect",
      "Eval an arr"
    ,""];

    function evalarr_test( mode=MODE, opt=[] )=
    (  
        let( s1= "[2, [3, 4],5,[a, [ b,6]]]"
           , s2= "[35,[2.5,[a,-1],3,[b,2],1]"
           , scp=["a",10,"b",11]
           )
        doctest( evalarr, 
        [
          "", "var s1",""
        , [ evalarr(s1)
                , [2,[3, 4],5,["a", ["b",6]]]
                , "s1"
                ]
        , "", "var scp",""      
        , [ evalarr(s1,scope=scp)
                , [2,[3, 4],5,[10, [11,6]]]
                , "s1,scp"
                ]
                
        , "", "var s2",""        
        , [ evalarr(s2)
                , [35,[2.5,["a",-1],3,["b",2],1]]
                , "s2"
                ]
        , [ evalarr(s2,scope=scp)
                , [35,[2.5,[10,-1],3,[11,2],1]]
                , "s2,scp"
                ]
        ,""
        ,[ evalarr("[5, [age, 10]]")
                , [5,["age",10]]
                , "`[5, [age, 10]]`"
                ]
        ,[ evalarr("[5, [\"age\", 10]]")
                , [5,["age",10]]
                , "`[5, [\"age\", 10]]`"
                ]
        
        ,""
        ,[ evalarr( "[0,1]" ),[0,1],"'[0,1]'"]
        ,[ evalarr( "[true,false]" ),[true,false]
                  , "'[true,false]'"]
        ,[ evalarr( "[false,true]" ),[false,true]
                  ,"'[false,true]'"]
        
    //    ,""  
    //    ,"<b>blksymbol</b> and <b>sp</b>:"
    //    ,""
    //    ,[ evalarr( "[a;b=1;l=[c;d=2,e,3]]"//,sp=";"
    //              , itemptn= str(A_zu,0_9d,";=,- \"")
    //              )
    //              , ["a","b=1","l",["c","d=2","e",3]]
    //              ,"[a;b=1;l=[c;d=2,e,3]]"
    //              ,["nl",true]
    //     ]
        ]//doctest
        , mode=mode, opt=opt, scope=["s1",s1,"s2",s2,"scp",scp] 
        )
    );

    //echo( evalarr_test( mode=12 )[1]);

    //echo( evalarr("[1,2]") );
    //echo( evalarr("[0,1]") );


    //s2="[2, [3, 4],5,[a, [ b,6]]]";
    //
    //echo(s2=s2);
    //s22= evalarr(s2);
    //echo( s22); 

    //s23= evalarr(s2); //,["a",10,"b",11]);
    //echo( s23); 
    //s3="[35,[2.5,[a,-1],3,[b,2],1]" ;
    //echo(s3=s3);
    //s33= evalarr(s3);
    //echo( s33 );  
    
} // evalarr

//echo(  evalarr_test( mode=112, opt=[] )[1] ); 
//echo( eval("[2  ,3]") );

{ // func
    
//ISLOG=0;    
function func(
          s           // s: the func formula 
        , scope = []  // like: ["x",3,"age",[2,3,5] ]
        , returnWithVars=0 // 18.10.8: when true, return the val(s) with corresponding var(s)
        , _pis= undef // paranthesis indices, indices of "(", like:  [0,9]
        , _debug= ""
        , _lv=0
        )=
(
   let(lv=_lv)
   //echo( log_b( _s("func(\"{_}\"), {_}",[s,scope]), lv) )
   let( ops= "%^*/+-<>=&|"
      //---------------------------------------------------------
      , scp_vals= vals(scope) //:: ["x",[2,3,4],"y",5]=> [ [2,3,4], 5 ]
      , scp_arr_is = [ for(i=range(scp_vals))   
                      if( isarr( scp_vals[i])||type(scp_vals[i])=="range") i] // indices of arr in scope
      , run_scp = scp_arr_is  // need to run thru scope to resolve array value
      //---------------------------------------------------------
      // Chk if s contains aaa?bb:cc struct. If yes, all 
      // critical assignments inside this let() will be skipped
      // and it will be resolved in the func body first. 
      , cond = split(s,"?")
      //, run_cond= !(run_scp || len(cond)==1)
      , run_cond= !(scp_arr_is || len(cond)==1)
      , cond_tf= run_cond? split(cond[1],":"):undef
      //---------------------------------------------------------
      // Find the indices of "(". This is done only on the
      // first run. The generated pis (paranthesis indices)
      // will be carried over down the calc. 
      //
      , isbeg= _pis==undef   // Is this the first run?
      , blkm = !run_cond &&isbeg?   // Match blocks in the first run    
                 match(s,ps=[["(",1]])
                      //:[[0,0,"(",["("]],[9,9,"(", ["("]]]
                 :undef
      ,_pis = blkm ? [ for(m=blkm)m[0] ]  //:[0,9]
                 : _pis
      
      //---------------------------------------------------------
      // Matching blocks, starting backward from the last item
      // of _pis. blk: [3, 7, "(y+2)", ["(", "y+2", ")"]]           
      , blk = !run_cond && _pis?         
                match_ps_at( s
                           , i= last(_pis)   // Starting backward
                           , ps=[["("],str(A_zu,0_9d,ops),[")"]]
                           )    
               :undef     
      , _blkval= blk?        // Convert extracted content: "y+2"=> 3 
                calc_flat( blk[3][1], scope) 
                : undef
      
      //---------------------------------------------------------
      // Check if there's a func name before the found "("
      // If yes, do a calc with it
      , prestr = !run_cond?slice(s,0, blk[0]):undef// substr before "("
      , _fname = !run_cond?
                  matchr_at( prestr,-1,CALC_FUNCS ) //:[0,4,"round"]
                  : undef
      , fname = _fname[2]
      , blkval = fname?
                 run( fname, _blkval):_blkval
      //, blkval = is0(__blkval)?0:__blkval 
                 
      //---------------------------------------------------------
      // Adjust the ending index of str on the left
      , prestr_lasti = blk[0]-1-(fname?len(fname):0) //
      
      //---------------------------------------------------------
      // Prepare for the next run
      , newpis = !run_cond && _pis? slice(_pis,0,-1): undef
      , news = !run_cond&&_pis?     //: blk= [3, 7, ["(", "y+2", ")"]]     
               str( slice(s,0, prestr_lasti+1) //blk[0]-(fname?len(fname):0))
                  , //prestr_lasti>0
                    s[ prestr_lasti ] //: not the first item
                    && 
                    !has( str("(",ops), s[prestr_lasti])
                      ? "*":""
                  , blkval
                  //, slice(s, blk[1]+1)
                  , slice(s, blk[1])
                  ):undef
   )
   //echo(log_( ["scp_vals", scp_vals, "scp_arr_is", scp_arr_is],lv ))
   //echo(log_( ["_pis", _pis, "newpis", newpis, "blk",blk],lv ))
   //echo(log_( ["blkval",blkval, "news", news],lv ))
   //echo(log_( [ "run_cond", run_cond
              //, "isbeg && !blkm ",isbeg && !blkm 
              //, "!newpis", !newpis
              //], lv ))
   let(                               
       rtn =    scp_arr_is?  
                  //echo( log_("Calling next func to deal with array vars", lv))
                  //echo( log_(["scp_vals",scp_vals], lv))
                  let( permutedVals= permutes(scp_vals) 
                     )  
                     //echo( log_(["permutedVals",permutedVals], lv) )
                     returnWithVars?
                     [ for(vals=permutedVals)
                       let( newscope= zip(keys(scope),vals,isflat=1))
                       //echo( log_(["vals",vals, "keys", keys(scope), "newscope", newscope], lv ))
                       concat( vals(newscope), [func( s, scope= newscope )])
                     ]              
                     : [ for(vals=permutedVals)
                       let( newscope= zip(keys(scope),vals,isflat=1))
                       //echo( log_(["vals",vals, "keys", keys(scope), "newscope", newscope], lv ))
                       func( s, scope= newscope )
                     ]              
                                     
                  //[ for(v = scp_vals[ scp_arr_is[0] ] )
                    //func( s, scope= [ for(x=scope) isarr(x)?v:x ]
                             //, _lv=lv+1 )  
                  //]
                  
                 :run_cond?
                   //echo( log_("Calling next func to deal with a?b:c", lv))
                   (  
                      func( cond[0], scope,_lv=lv+1 )
                     ?func( cond_tf[0], scope,_lv=lv+1 )
                     :func( cond_tf[1], scope,_lv=lv+1 )
                   )
                 :isbeg && !blkm ? calc_flat( s,scope,_lv=lv+1)
                 :!newpis? calc_flat( news,scope,_lv=lv+1)
                 : func(news, scope, _pis=newpis, _debug=_debug,_lv=lv+1)   
      )

   //echo(log_e("",lv))
   rtn
);  

    func=["func","s,scope, returnWithVars","num","Math",
    "Calc a string formula with variables defined in scope.
    ;; 
    ;; 1) s: string formula
    ;;
    ;; -- Allowed operators in order of priority:
    ;;
    ;;    %, ^, */, +-, &lt;>=&|
    ;;
    ;; -- Boolean operations (&lt;>=) are indicated with single
    ;;    letter op, i.e., 
    ;;  
    ;;      a&b is a&&b, a|b is a||b, a=b is a==b
    ;; 
    ;;    - There's no way to carry out >=, &lt;= or &lt;>.
    ;;    - Put the 2nd boolean check in (): x<2&(x<5), x>2|(y>0)
    ;;    
    ;; -- Accept conditional statement:
    ;; 
    ;;    a>b?b:c
    ;;
    ;; -- 2a is treated as 2*a, and 2(a+1) as 2*(a+1)
    ;; 
    ;; -- Spaces will be ignored.
    ;;
    ;; 2) scope: a hash, values could be single number or array:
    ;; 
    ;;       ['x', [3,4,5], 'y', 3]
    ;;
    ;;    or a range: ['x', [0:5], 'y', [1:0.5:3] ] 
    ;;
    ;;    ### Compared to a range, using a range() function seems to be faster in Plot
    ;;      
    ;; 3) returnWithVars: set to 1 to return the vals with their 
    ;;    corresponding vars. This allows for easy output of pts
    ;; 
    ;; 4) Most built-in functions are available to use, like 'round(x)'. This is 
    ;;    handled by calc_basic() and its calc is carried out by run(). 
    "
    ,"calc_axb,calc_flat"
    ];

    function func_test( mode=MODE, opt=[] )=
    (  //==========================
        //isdebug=false;
        
        doctest( func
        ,[ 
           "// Simple operations. Note that */ has priority over +-:"
           
         , [func("11"),11,"'11'" ]
         , [func("-11"),-11,"'-11'" ]
         , [func("1+3/2"),2.5, "'1+3/2'" ]
         , ""
         , "//----------------------------------------"
         , "// With variables:"
         
         , [func("x+1", ["x",3]),4, "'x+1',['x',3]" ]
         , [func("x+yz^2", ["x",3,"yz",4]),19, "'x+yz^2',['x',3,'yz',4]" ]
         , ""
         , "// With paranthesis:"
         
         , [func("(y+2)*x", ["x",3,"y",4]),18, "'(y+2)*x',['x',3,'y',4]" ]
         , [func("x*(y+2)^2", ["x",3,"y",1]),27, "'x*(y+2)^2',['x',3,'y',1]" ]
         , [func("x*(2y+2)^2", ["x",3,"y",1]),48, "'x*(2y+2)^2',['x',3,'y',1]" ]
         , [func("(x*(y+2))^2", ["x",3,"y",1]),81, "'(x*(y+2))^2',['x',3,'y',1]" ]
                  ,""
         , "//----------------------------------------"
         , "// boolean ops:"
         , ""  
         , [func("x>1",["x",2]),1,"'x>1',['x',2]" ]
         , [func("x=2",["x",2]),1,"'x=2',['x',2]" ]
         , [func("x<2",["x",2]),0,"'x&lt;2',['x',2]" ]
         , [func("2<x",["x",2]),0,"'2&lt;x',['x',2]" ]
         , [func("2=x",["x",2]),1,"'2=x',['x',2]" ]
         , [func("2>x",["x",2]),0,"'2>x',['x',2]" ]
         , ""
         , "// Quote the 2nd boolean check :"
         , ""
         , [func("2<x&(x<5)",["x",1]),0,"'2&lt;x&(x&lt;5)',['x',1]" ]
         , [func("2<x&(x<5)",["x",3]),1,"'2&lt;x&(x&lt;5)',['x',3]" ]
         , [func("2<x&(x<5)",["x",5]),0,"'2&lt;x&(x&lt;5)',['x',5]" ]
         , [func("x<2&(y<2)",["x",1,"y",1]),1,"'x&lt;2&(y&lt;2)',['x',1,'y',1]" ]
         , [func("x<2&(y=2)",["x",1,"y",1]),0,"'x&lt;2(&y=2)',['x',1,'y',1]" ]
         , [func("x=2&(y<2)",["x",1,"y",1]),0,"'x=2&(y&lt;2)',['x',1,'y',1]" ]
         , [func("x<2|(y=2)",["x",1,"y",1]),1,"'x&lt;2|(y=2)',['x',1,'y',1]" ]
         , [func("x=2|(y<2)",["x",1,"y",1]),1,"'x=2|(y&lt;2)',['x',1,'y',1]" ]
         , ""
         , [func("2<x&(x<5)",["x",1,"y",1]),0,"'2&lt;x(&x&lt;5)',['x',1,'y',1]" ]
         , [func("2<x&(x<5)",["x",3,"y",1]),1,"'2&lt;x(&x&lt;5)',['x',3,'y',1]" ]
         , [func("2<x&(x<5)",["x",6,"y",1]),0,"'2&lt;x(&x&lt;5)',['x',6,'y',1]" ]
         , [func("2<x&(x<5)&(2<y)",["x",3,"y",1]),0,"'2&lt;x(&x&lt;5)&(2&lt;y)',['x',3,'y',1]" ]
         , [func("2<x&(x<5)&(2<y)",["x",3,"y",3]),1,"'2&lt;x(&x&lt;5)&(2&lt;y)',['x',3,'y',3]" ]
         , "// The following check if [x,y] is inside a block of x=[2,5], y=[2,5]:"
         , [func("2<x&(x<5)&(2<y)&(y<5)",["x",1,"y",1]),0,"'2&lt;x(&x&lt;5)&(2&lt;y)&(y&lt;5)',['x',1,'y',1]" ]
         , [func("2<x&(x<5)&(2<y)&(y<5)",["x",1,"y",3]),0,"'2&lt;x(&x&lt;5)&(2&lt;y)&(y&lt;5)',['x',1,'y',3]" ]
         , [func("2<x&(x<5)&(2<y)&(y<5)",["x",3,"y",3]),1,"'2&lt;x(&x&lt;5)&(2&lt;y)&(y&lt;5)',['x',3,'y',3]" ]
         , [func("2<x&(x<5)&(2<y)&(y<5)",["x",6,"y",3]),0,"'2&lt;x(&x&lt;5)&(2&lt;y)&(y&lt;5)',['x',6,'y',3]" ]
         , "// To exclude [x,y] from a block:"
         , "// ((x<-2.3|(x>2.3))|((y<-2.3)|(y>2.3))"
//         , ""
//         , "//----------------------------------------"
//         , "// built-in functions:"
//         
//         , [func("sin(x)",["x",30]),0.5,"'sin(x)',['x',30]" ]
//         , [func("cos(x)",["x",30]),0.866025,"'cos(x)',['x',30]"]
//         , [func("round(x/2+1)",["x",3]),3,"'round(x/2+1)',['x',3]"]
//              
//         , [func("sin(x)^2",["x",30]),0.25, "'sin(x)^2',['x',30]"]
//         , [func("(x+2)+sin(x)", ["x",30]), 32.5, "'(x+2)+sin(x)',['x',30 ]"]
//         , [func("(x+2)+(sin(x)^2)", ["x",30]), 32.25,"'(x+2)+(sin(x)^2)',['x',30 ]"]
//         , [func("cos(x)sin(x)", ["x",30]), 0.433013,"'(x+2)+(sin(x)^2)',['x',30 ]", ["asstr",true]]
//         , ""
//         ,"// The following: sin(x)^2+cos(x)^2 produces 0.999999, requiring "
//         ,"//  round(0.999999) to make it 1:"
//         , [round(func("sin(x)^2+cos(x)^2", ["x",30])), 1,"'sin(x)^2+(cos(x)^2)',['x',30]" ]
//             
//           ,""
//           ,"// 3x is interpretated as 3 * x"
//           
//         , [func("2x", ["x",3]), 6,"'2x',['x',3]"]
//         , [func("2x+1", ["x",3]), 7,"'2x+1',['x',3]"]
//         , [func("2x+1.5y", ["x",3,"y",4]),12
//                ,"'2x+1.5y',['x',3,'y',4]"]
//         , [ func("2(x+1)", ["x",3]), 8, "'2(x+1)', ['x',3]" ] 
//         , [func("2sin(x)",["x",30]),1,"'2sin(x)',['x',30]" ]
//    //     ,""
//         ,""
//         ,"// negative"
//         , [func("sin(270)"),-1,"'sin(270)'"]
//         
//         ,""
//         , "// conditional"
//         , [func("a<b?2a:2b+1",["a",3,"b",4]),6
//                ,"a&lt;b?2a:2b+1,['a',3,'b',4]"]
//         , [func("a>b?2a:2b+1",["a",3,"b",4]),9
//                ,"a>b?2a:2b+1,['a',3,'b',4]"]
//         , ""
//         , "// array in scope returns a list "
//         , [func("sin(x)",["x",[0,90,270]]),[0,1,-1]
//                ,"'sin(x)',['x',[0,90,270]", ["//", "A list"]]
//         , [func("sin(180*x)",["x",range(0,0.5,2)]),[0, 1, 0, -1]
//                ,"'sin(180*x)',['x',range(0,0.5,2)", ["//", "A list entered w/ range()"]]
//         , [func("1+x^2",["x",range(4)]),[1,2,5,10]
//                ,"'1+x^2',['x',range(4)", ["//", "A list entered w/ range()"]]
//         , [func("x%2?x^2:0",["x",range(6)]),[0,1,0,9,0,25]
//                ,"'x%2?x^2:0',['x',range(6)]", ["//", "A list entered w/ range()"]]
//         , [func("x%2?x^2:0",["x",[0:1:5]]),[0,1,0,9,0,25]
//                ,"'x%2?x^2:0',['x',[0:1:5]]", ["//", "A range"]]
//         ,        
//         , ""
//         , "// More than one array/dimension "
//         , ""
//         , [func("x^2+y", ["x",[0,1,2],"y",[0,1,2]])
//            ,[0, 1, 2, 1, 2, 3, 4, 5, 6]
//            ,"'x^2+y', ['x',[0,1,2],'y',[0,1,2]]"
//            ]
//         
//         , ""
//         , "// Set returnWithVars=1 to return the vals with their corresponding vars"
//         , ""
//            
//         , [func("x^2+y", ["x",[0,1,2],"y",[0,1,2]], returnWithVars=1)
//            ,[[0, 0, 0], [0, 1, 1], [0, 2, 2]
//            , [1, 0, 1], [1, 1, 2], [1, 2, 3]
//            , [2, 0, 4], [2, 1, 5], [2, 2, 6]]
//            ,"'x^2+y', ['x',[0,1,2],'y',[0,1,2]]"
//            , ["ml",1]
//            ]
//            
//                   
//         , ""   
//         , "// Note:Use permutes([x,y]) to get a list of [x,y]'s:"
//         , ">>> permutes( [ [0,1,2], [0,1,2] ] ) = "       
//         , str("xys= ", permutes( [ [0,1,2], [0,1,2] ] ))
//         
         ], mode=mode, opt=opt )

    );    

} // func

//========================================================
{ // run
function run(f, d, prec=6)=
(
    //echo("run=========================")
    let( v= isarr(d)?d[0]:d 

       , rtn= isarr(f)? run( f[0], slice(f,1))
              : f=="min"? min(d)
              : f=="max"? max(d)
              : f=="abs"? abs(v)
              : f=="sign"? sign(v)
              : f=="sin"? sin(v)
              : f=="cos"? cos(v)
              : f=="tan"? tan(v)
              : f=="acos"? acos(v)
              : f=="asin"? asin(v)
              : f=="atan"? atan(v)
              : f=="atan2"? atan2(v)
              : f=="concat"? concat( v, len(d)<=1?[]:run("concat", slice(d,1)))
              : f=="floor"? floor(v)
              : f=="round"? round(v)
              : f=="ceil"? ceil(v)
              : f=="ln"? ln(v)
              : f=="len"? len(v)
              : f=="log"? log(v)
              : f=="pow"? pow(v, len(d)==1||len(d)==undef?2:d[1])
              : f=="sqrt"? sqrt(v)
              : f=="exp"? exp(v)
              : f=="rands"? len(d)==3? rands(d[0],d[1],d[2])
                                     : rands(d[0],d[1],d[2],d[3])                
              : undef
        )
    is0(rtn)?0: rtn// 2018.10.7 ---- fixed to make sure 0 value returns 0

); 

    
//function run(f, d)=
//    let( v= isarr(d)?d[0]:d )
//(
//    isarr(f)? run( f[0], slice(f,1))
//    : f=="min"? min(d)
//    : f=="max"? max(d)
//    : f=="abs"? abs(v)
//    : f=="sign"? sign(v)
//    : f=="sin"? sin(v)
//    : f=="cos"? cos(v)
//    : f=="tan"? tan(v)
//    : f=="acos"? acos(v)
//    : f=="asin"? asin(v)
//    : f=="atan"? atan(v)
//    : f=="atan2"? atan2(v)
//    : f=="concat"? concat( v, len(d)<=1?[]:run("concat", slice(d,1)))
//    : f=="floor"? floor(v)
//    : f=="round"? round(v)
//    : f=="ceil"? ceil(v)
//    : f=="ln"? ln(v)
//    : f=="len"? len(v)
//    : f=="log"? log(v)
//    : f=="pow"? pow(v, len(d)==1||len(d)==undef?2:d[1])
//    : f=="sqrt"? sqrt(v)
//    : f=="exp"? exp(v)
//    : f=="rands"? len(d)==3? rands(d[0],d[1],d[2])
//                           : rands(d[0],d[1],d[2],d[3])                
//    : undef
//);     

    run=["run", "f,d", "val", "Core"
    ," 
    "];
    function run_test( mode=MODE, opt=[] )=
    (
       doctest( run,
        [
          [ run("abs",-3), 3 , "'abs', -3"]
        , [ run("abs",[-3]), 3 , "'abs', [-3]"]
        , [ run("sign",-3), -1 , "'sign', -3"]
        , [ run("sin",30), 0.5 , "'sin', 30"]
        , [ run("max",[3,1,4,2]), 4 , "'max', [3,1,4,2]"]
        , [ run("pow",3), 9 , "'pow', 3"]
        , [ run("pow",[3,3]), 27 , "'pow', [3,3]"]
        , [ run("concat",[[3,4],[5,6],[7,8]])   
                , [3,4,5,6,7,8], "'concat', [[3,4],[5,6],[7,8]]"
                ,  ]
        ,""
        ,"// New: you can do: run(['sin, 30])"
        ,"//      instead of run(sin, 30)"
        ,""
        , [ run(["sin",30]), 0.5 , ["sin", 30]]
            
        ], mode=mode, opt=opt
        )
    ); 

} // run


/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_eval_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_eval.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_eval_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_eval", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
    //, calc_axb_test( mode, opt ) // to-be-coded
    //, calc_basic_test( mode, opt ) // to-be-coded
     calc_flat_test( mode, opt )
    //, calc_func_test( mode, opt )
    , calc_shrink_test( mode, opt )
    , func_test( mode, opt )
    , eval_test( mode, opt )
    , evalarr_test( mode, opt )
    , run_test( mode, opt )
    ]
);
