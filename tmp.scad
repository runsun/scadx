include <scadx.scad>

module dev_search_based_match()
{
  s="a+cossin(2)+sin(5)";
  // 0123456789 1234567
  p="abcdefhijklmnopqrstuvwxyz";
  function f(s, i=2, p=p, maxn=-1, _m=undef, _rtn="")=
  ( 
     // this ver is for str only
     
     let( maxn= _m==undef? (maxn<0? len(s):maxn+i):maxn )
     i>=maxn || _m==[] ? (_rtn==""?false:_rtn)
     : let( _m = search( s[i], p) )
       f(s, i+1, p,  maxn, _m,  _m==[]?_rtn:str(_rtn, s[i]) )
  );
  function f2(s, i=2, p=p, maxn=-1, _i0=undef, _m=undef, _rtn="")=
  ( 
     isstr(p)?
     (  let( maxn= _i0==undef? (maxn<0? len(s):maxn+i):maxn 
           , _i0 = _i0==undef? i:_i0
           )
       i>=maxn || _m==[] ? [1,2,3] //(_rtn==""?false:[_i0, i, _rtn] )
       : let( _m = search( s[i], p,0) )
         //["_m",_m]
         f(s, i+1, p,  maxn, _i0, _m,  _m==[]?_rtn:str(_rtn, s[i]) )
     )
     : begwith( slice(s,i), p)
  );
  echo( f2(s) );
  echo( f2(s, maxn=2));
  echo( f2(s, 0));
  echo( f2(s, 12));
  echo( f2(s, p=["cos","sin"]));  
  
 } 
//dev_search_based_match();
 
fml = "a+cos(2)+sin(5)";
   //  0123456789 1234
 
echo( match_at(fml, 2, a_z) );// [2,4,"cos"]
echo( match_at(fml, 1, a_z) );//, false,"'a+cos(2)+sin(5)',1, a_z"]
echo( match_at(fml, 3, a_z) );//, [3,4,"os"],"'a+cos(2)+sin(5)',3, a_z"]
echo( match_at(fml, 2, a_z,2) );//, [2,3,"co"],"'a+cos(2)+sin(5)',2, a_z, n=2"]
echo( match_at(fml, 3, a_z,2) );//, [3,4,"os"],"'a+cos(2)+sin(5)',3, a_z, n=2"]
echo( match_at("abcd", 3, "defg") );//, [3,4,"os"],"'a+cos(2)+sin(5)',3, a_z, n=2"]

echo( "------------------------------"); 
echo( match_at(fml, 12, str("(",0_9,")")) );//, [12,14,"(5)"]
              //  ,"'a+cos(2)+sin(5)',12, str('(',0_9,')')"]
echo( match_at(fml, 2, str(a_z,0_9,"(")) );//, [2,6,"cos(2"],"'a+cos(2)+sin(5)',2, str(a_z,0_9,'(')"]
echo( match_at(fml, 2, str(a_z,0_9,"("),4) );//, [2,5,"cos("],"'a+cos(2)+sin(5)',2, str(a_z,0_9,'('),n=4"]
echo( match_at(fml, 14, "[]{}()"));//, [14,14,")"] ,"'a+cos(2)+sin(5)',14,'[]{}()'"]
        
       // , "","// When the pattern is an array, match any item in the array ONLY ONCE:"
       // , ""
echo( "------------------------------"); 
echo( "p is array: ");
echo( match_at(fml, 2, ["abc","def"]) );//, false,"'a+cos(2)+sin(5)',2, ['abc','def']"]
echo( match_at(fml, 2, ["abc","sin"]) );//, false,"'a+cos(2)+sin(5)',2, ['abc','sin']"]
echo( match_at(fml, 2, ["abc","co"]) );//, [2,3,"co"],"'a+cos(2)+sin(5)',2, ['abc','co']"]
echo( match_at(fml, 2, ["cos","co"]) );//, [2,4,"cos"],"'a+cos(2)+sin(5)',2, ['cos','co']"]
echo( match_at(fml, 2, ["co","cos"]) );//, [2,3,"co"],"'a+cos(2)+sin(5)',2, ['co','cos']"]
//        , ""
//        , "//If there's spaces : "
//        , ""
//echo( match_at("a+cos (2)", 2, str(a_z,0_9,"(")), [2,4,"cos"],"'a+cos (2)',2, str(a_z,0_9,'(')"]
//        , ""
//        ,"// Next one returns sin, not sincos. When p is array, match only once"
//echo( match_at("a+sincos(2)", 2, ["cos","sin"])
//                     , [2,4,"sin"],"'a+sincos(2)', 2, ['cos','sin']"
//                     ]
//        ,"", "// New 2016.12.30: pattern '..' for ANY"
//        ,""
//echo( match_at("a+cos(2)", 2, ".."), [2,7,"cos(2)"],"'a+cos(2)',2, '..'"]
//echo( match_at("a+cos(2)", 3, ".."), [3,7,"os(2)"],"'a+cos(2)',3, '..'"]
//echo( match_at("a+cos(2)", 3, "..",2), [3,4,"os"],"'a+cos(2)',3, '..',2"]
//        
//        ,"", "// New 2016.12.31: p='..abc': as many of ANY chars AND n # of a,b,c"
//        ,""
//echo( match_at("a+cos(120)", 2, "..012"), [2,8,"cos(120"],"'a+cos(120)',2, '..012'"]
//echo( match_at("a+cos(120)", 3, "..012"), [3,8,"os(120"],"'a+cos(120)',3, '..012'"]
//echo( match_at("a+cos(120)", 1, "..012",2), [1,7,"+cos(12"],"'a+cos(120)',1, '..012',2"]
//                
         