include <scadx.scad>

pq = randPts(2);
// failed:
// pq = [[0.08, -0.3, -1.59], [-2.87, 1.41, 1.96]];
// pq = [[0.38, 0.08, -0.44], [-2.33, 0.66, 1.2]];
//pq = [[0.81, 1.79, -1], [2.27, 2.84, -1.6]];
pq = [[0.55, -2.79, -0.85], [0.05, -1.01, 1.04]];


// pq = [[-1.17, -1.51, -1.42], [1.23, -0.73, 1.29]];
 
echo(pq=pq);
Black() MarkPts(pq, "PQ", Grid=3);

/*
pt = i*P + j*Q

i+j=1

pt.z=0
=> i*Pz + j*Qz = 0
   iPz = -jQz
   j= -iPz/Qz = 1-i
   (1-Pz/Qz)i=1
   i = 1/(1-Pz/Qz)
   j = 1- 1/(1-Pz/Qz)
   
if P.y> Q.y: // Q is closer to XY plane
   i<0,j>0
   

if P.y< Q.y:
   i>0,j<0
   
*/
ColorAxes(opt=["r",0.5]);

P=pq[0];
Q=pq[1];

i= 1/(1-P.z/Q.z);
j= 1-i;
//pt = i*P + j *Q;
pt = i*P + (1-i)*Q; // Interaction of PQ line with XY plane
echo(i=i,j=j, pt=pt);
MarkPt( pt, "Pt");

function oPt(pt,pq)=
(
   let( P=pq[0], Q=pq[1]
      )
     3 
   


);
/*
  pt= iP+jQ = [iPx+jQx, iPy+jQy, iPz+jQz];
  
  [i,j] | Px Py Pz |
        | Qx Qy Qz |
  
  (Q-pt)*(P-pt) = 0;
  [Qx-iPx-jQx, Qy-iPy-jQy, Qz-iPz-jQz] * 
  [Px-iPx-jQx, Py-iPy-jQy, Pz-iPz-jQz]  
  
  [(1-j)Qx-iPx, (1-j)Qy-iPy, (1-j)Qz-iPz] * 
  [(1-i)Px-jQx, (1-i)Py-jQy, (1-i)Pz-jQz]  



*/

/*
C= abs(P.z)>abs(Q.z)? Q:P; // P or Q whichever closer to XY plane
DashLine( [ C, pt] );
//N = N([O,pt,P]);
//MarkPts([N,O, pt], Label=["text",["N","",""]]);

//J = projPt(C,[O,pt]);
//d = C*uv(pt); 
d = C*( pt/norm(pt));
r = d/norm(pt);
//J = r*pt;
J= ((C*(pt/norm(pt)))/norm(pt))*pt; // C projection on line [O,pt]

MarkPt(J,"J");    
Mark90( [O,J,C] );
C0 = newz(C,0);
Mark90( [O,J,C0] );

C1= onlinePt([J,C0], len=norm(C-J));
MarkPt(C1, "C1");
MarkPt(C0, "C0");

a = -(angle([C,J,C0],Rf=O));

function angle2(pqr)=
(
   let( A=pqr[0], B=pqr[1], C=pqr[2]
      , D = norm(cross( A-B, C-B ))
      //, E = norm(A-B)*norm(C-B) 
      , E = dist(A,B) * dist(C,B) 
      )echo(pqr=pqr, A=A,B=B,C=C)
   asin( D/E )
);
*/


/*
Use the sign of the determinant of vectors (AB,AM), where M(X,Y) is the query point:

position = sign((Bx - Ax) * (Y - Ay) - (By - Ay) * (X - Ax))
It is 0 on the line, and +1 on one side, -1 on the other side.

((b.X - a.X)*(c.Y - a.Y) - (b.Y - a.Y)*(c.X - a.X)) > 0
*/

//echo(norm(J), norm(pt),norm(J)+norm(pt), norm(J-pt)); 
//echo(a=a, aa= -angle2([C,J,C0]));
//Red() rotate( a= a,v=J) //pt)
//MarkPts(pq, "PQ");
//
//Arrow( arcPts( [C,J,C1] ) ); 
//rotFromTo( Q, C1) 
//Green() MarkPts(pq, "PQ");

//====================== testing sideness
A= [3,2,0];
B= [1,1,0];
C= [1,-2,0];

echo(cross=cross(X,A),cross(X,B),cross(X,C));
echo(A*B);
