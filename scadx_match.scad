//=============================================================

function match(
      s
    , ps           // list of patterns, each p could be string or array,
                   // match 
    , want=undef
    , _i=0           
    , _rtn=[]            
    , _debug=[]
    )=
(
   //echo("match================")
   let( 
        m= match_ps_at(s,_i,ps,want) 
           // [3,7,"cos(2)", ["cos","(","2",")"]] or false
      ,newi= m? m[1]: _i+1
      ,newrtn= m? app(_rtn,m):_rtn
//      , _debug= concat(_debug,
//         str(
//          [ "_i", _i
//          , "s[_i]", s[_i]
////          , "newi", newi
////          , "_rtn", _rtn
////          , "newrtn", newrtn
//          ]
//         ))
      )
    _i<len(s)-len(ps)+1?  
      match( s,ps,want, _i=newi, _rtn=newrtn, _debug=_debug )
    //: join( _debug, "<br/>") 
      : _rtn
);
    
    //======================================
    match=["match","s,ps,want","arr","Inspect"
     ,"Match each pattern p in ps to s, return array of [i,j,m1m2.., [m1,m2,m3...]] or false 
    ;; 
    ;; ps: a list of patterns. A pattern p is in the form of 
    ;;     px or [px,n] where px is either a string containing
    ;;     all possible chars, or an array containing all possible
    ;;     words. n is the count chars to match, works only when px 
    ;;     is a string. See match_ps_at() for more. 
    ;;    
    ;; Return array of [i,j,m1m2..,[m1,m2...]] where i is the starting i, j 
    ;; is the last i of matched. Each m corresponds to each px given.
    ;; 
    ;; want: int or array of indices determining what match to report.
    ;; 
    ;; For example, 
    ;;  ps= [a_z,'(',0_9,')']
    ;;  match(s,ps )=    [2, 7,'cos(2)', ['cos', '(', '2', ')']]
    ;;  match(s,ps,2 )=  [2, 7,'cos(2)', ['2']]
    ;; match(s,ps,[0,2])=[2, 7,'cos(2)', ['cos', '2']]
    ;;  
    ;; Note: s could be array
    "
    ,"match_at,match_ps_at"
    //;; An item of returned array could be [i,[m1]], 
    //;; [i,[m1,m2]] or [i,[m1,m2,m3]] depending on how 
    //;; many among p1,p2 and p3 are given. A match requires
    //;; matching of all p`s given.
    //;; 
    //;; A pattern can be a string like 'abcde', which matches
    //;; any char from a to e; or array like ['sin','cos'] which
    //;; matches either 'sin' or 'cos'. 
    //;; 
    //;; Global constants: 
    //;; 
    //;;   A_Z: A to Z
    //;;   a_z: a to z
    //;;   A_z: A to z
    //;;   A_zu: A to z + '_'
    //;;   0_9: 0 to 9
    //;;   0_9d: 0 to 9 + '.'
    //;; 
    //;; Example:
    //;; 
    //;; fml= 'a+cos(2)+sin(5)'
    //;; match(fml, p1=['sin','cos'])= [[2,['cos']],[9,['sin']]]
    //;; match(fml, p1=A_z,p2='(')= [[4, ['s','(']], [11, ['n','(']]]

    ];
     
     function match_test( mode=MODE, opt=[] )=
     (
        let( fml = "a+cos(2)+sin(5)"
                 // 0123456789 1234
           ) 
        doctest( match,
        [
          "var fml"
         ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
         , ""
          
        , "// 1-item ps"  
        
        , [ match( fml, ps=[A_z] )
                 , [[0,1,"a",["a"]], [2,5, "cos",["cos"]], [9,12,"sin",["sin"]]]
                 , "'a+cos(2)+sin(5)', ps=[A_z]"
           , ["nl",true]
          ]
          
        // The following pattern, [A_z,2], failed to match anything:
        //   
        //, "// 1-item ps with max match count for each pattern,"
        //, "// In this case, return max 2 found in A_z"
        //, [ match( fml, ps=[A_z,2] )
        //         , [[2,3, "co",["co"]], [9,10,"si",["si"]]]
        //         , "'a+cos(2)+sin(5)', ps=[A_z,2]"
        //   , ["nl",true]
        //  ]
        
        , [ match( fml, ps=[["cos","sin"]] )
                 , [[2,5,"cos", ["cos"]], [9,12, "sin",["sin"]]]
                 , "'a+cos(2)+sin(5)', ps=[['cos','sin']]"
           , ["nl",true]
          ]
        , [ match( fml, ps=[["sin"]] )
                 , [[9, 12,"sin",["sin"]]]
                 , "'a+cos(2)+sin(5)', ps=[['sin']]"
           , ["nl",true]
          ]
        ,""
        ,"// 2-item ps"  
        
        , [ match( fml, ps=[A_z,"(["] )
                 , [[2,6, "cos(", ["cos","("]], [9,13, "sin(", ["sin","("]]]
                 , "'a+cos(2)+sin(5)', ps=[A_z,'([']"
           , ["nl",true]
          ]

        , [ match( "EE+ccc+AAaa+BBbb+DDD", ps=[A_Z,a_z] )
                 , [[7, 11,"AAaa", ["AA","aa"]]
                   ,[12,16,"BBbb", ["BB","bb"]]]
                 , "'EE+ccc+AAaa+BBbb+DDD', ps=[A_Z,a_z]"
           , ["nl",true]
          ]
        
        ,""
        ,"// more-item ps" 
        
        , [ match( fml, ps=[["sin","cos"], "[(", 0_9,")]"] )
                 , [ [2,8, "cos(2)", ["cos","(","2",")"]]
                   , [9,15,"sin(5)", ["sin","(","5",")"]]]
                 , "'a+cos(2)+sin(5)', ps=[['cos(','sin('],'[(',0_9,')]']"
          , ["nl",true]
          ]

        , [ match( fml, ps=["+-", ["sin(","cos("], 0_9,")]"] )
                 , [ [1,8, "+cos(2)", ["+", "cos(","2",")"]]
                   , [8,15,"+sin(5)", ["+", "sin(","5",")"]]]
                 , "'a+cos(2)+sin(5)', ps=[['cos(','sin('],'[(',0_9,')]']"
          , ["nl",true]
          ]
          
        ,""
        ,"// Want only item 0 and 2 in the matches:"
        
        , [ match( fml, ps=["+-", ["sin(","cos("], 0_9,")]"] 
                  , want=[0,2]
                  )
                 , [ [1,8, "+cos(2)", ["+", "2"]]
                   , [8,15,"+sin(5)", ["+", "5"]]]
                 , "'a+cos(2)+sin(5)', ps=[['cos(','sin('],'[(',0_9,')]'], want=[0,2]"
          , ["nl",true]
          ]      
        , [ match( fml, ps=["(",0_9,")"], want=[1] )
                 , [[5, 8, "(2)", ["2"]]
                 , [12, 15, "(5)",["5"]]]
                 , "'a+cos(2)+sin(5)', ps=['(',0_9,')'], want=[1]"
           , ["nl",true]
          ]
     
         ,""
         ,"// Each p, if a string of chars, can set how many"
         ,"// chars to match"
        , [ match( fml, ps=[[A_z,2]] )
                 , [[2, 4, "co", ["co"]], [9, 11, "si", ["si"]]]
                 , "'a+cos(2)+sin(5)', ps=[[A_z,2]]"
           , ["nl",true]
          ]    

        , [ match( fml, ps=[[A_z,2],"(",0_9,")"] )
                 , [[3, 8, "os(2)", ["os","(","2",")"]]
                  , [10, 15, "in(5)", ["in","(","5",")"]]]
                 , "'a+cos(2)+sin(5)', ps=[[A_z,2],'(',0_9,')']"
           , ["nl",true]
          ]
          //===
        , "", "// Nested blocks "
        , ""  
        , [ match( "a((c+(d+1))+1)"
                 , ps=[["(",1],str(a_z,"+",0_9),[")",1]] 
                 )
                 , [[5, 10, "(d+1)", ["(", "d+1", ")"]]]
//                 , [[1, 13, "((c+(d+1))+1)", ["(","(c+(d+1))+1",")"]]
//                  , [2, 10, "(c+(d+1))", ["(","(d+1)",")"]]
//                  , [5, 9, "(d+1)", ["(", "d+1", ")"]]
//                   ]
                 , "'a((c+(d+1))+1)', ps=[['(',1],str(a_z,'+',0_9),[')',1]]"
           , ["nl",true]
          ]

        ], mode=mode, opt=opt, scope=["fml",fml]
        )
     );
    //echo( match_test(mode=112)[1] );    
    

//======================================
//match=["match","s,ps,want","arr","Inspect"
// ,"Match each pattern p in ps to s, return array of [i,j,m1m2.., [m1,m2,m3...]] or false 
//;; 
//;; ps: a list of patterns. A pattern p is in the form of 
//;;     px or [px,n] where px is either a string containing
//;;     all possible chars, or an array containing all possible
//;;     words. n is the count chars to match, works only when px 
//;;     is a string. See match_ps_at() for more. 
//;;    
//;; Return array of [i,j,m1m2..,[m1,m2...]] where i is the starting i, j 
//;; is the last i of matched. Each m corresponds to each px given.
//;; 
//;; want: int or array of indices determining what match to report.
//;; 
//;; For example, 
//;;  ps= [a_z,'(',0_9,')']
//;;  match(s,ps )=    [2, 7,'cos(2)', ['cos', '(', '2', ')']]
//;;  match(s,ps,2 )=  [2, 7,'cos(2)', ['2']]
//;; match(s,ps,[0,2])=[2, 7,'cos(2)', ['cos', '2']]
//;;  
//;; Note: s could be array
//"
//,"match_at,match_ps_at"
//];
// 
// function match_test( mode=MODE, opt=[] )=
// (
//    let( fml = "a+cos(2)+sin(5)"
//              0123456789 1234
//       ) 
//    doctest( match,
//    [
//      "var fml"
//     ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
//     , ""
//      
//    , "// 1-item ps"  
//    
//    , [ match( fml, ps=[A_z] )
//             , [[0,0,"a",["a"]], [2,4, "cos",["cos"]], [9,11,"sin",["sin"]]]
//             , "fml, ps=[A_z]"
//       , ["nl",true]
//      ]
//    , [ match( fml, ps=[["cos","sin"]] )
//             , [[2,4,"cos", ["cos"]], [9,11, "sin",["sin"]]]
//             , "fml, ps=[['cos','sin']]"
//       , ["nl",true]
//      ]
//    , [ match( fml, ps=[["sin"]] )
//             , [[9, 11,"sin",["sin"]]]
//             , "fml, ps=[['sin']]"
//       , ["nl",true]
//      ]
//    ,""
//    ,"// 2-item ps"  
//    
//    , [ match( fml, ps=[A_z,"(["] )
//             , [[2,5, "cos(", ["cos","("]], [9,12, "sin(", ["sin","("]]]
//             , "fml, ps=[A_z,'([']"
//       , ["nl",true]
//      ]
//
//    , [ match( "EE+ccc+AAaa+BBbb+DDD", ps=[A_Z,a_z] )
//             , [[7, 10,"AAaa", ["AA","aa"]]
//               ,[12,15,"BBbb", ["BB","bb"]]]
//             , "'EE+ccc+AAaa+BBbb+DDD', ps=[A_Z,a_z]"
//       , ["nl",true]
//      ]
//    
//    ,""
//    ,"// more-item ps" 
//    
//    , [ match( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] )
//             , [ [2,7, "cos(2)", ["cos","(","2",")"]]
//               , [9,14,"sin(5)", ["sin","(","5",")"]]]
//             , "fml, ps=[['cos(','sin('],'[(',0_9,')]']"
//      , ["nl",true]
//      ]
//
//    , [ match( fml, ps=["+-", ["sin(","cos("], 0_9,")]"] )
//             , [ [1,7, "+cos(2)", ["+", "cos(","2",")"]]
//               , [8,14,"+sin(5)", ["+", "sin(","5",")"]]]
//             , "fml, ps=[['cos(','sin('],'[(',0_9,')]']"
//      , ["nl",true]
//      ]
//      
//    ,""
//    ,"// Want only item 0 and 2 in the matches:"
//    
//    , [ match( fml, ps=["+-", ["sin(","cos("], 0_9,")]"] 
//              , want=[0,2]
//              )
//             , [ [1,7, "+cos(2)", ["+", "2"]]
//               , [8,14,"+sin(5)", ["+", "5"]]]
//             , "fml, ps=[['cos(','sin('],'[(',0_9,')]'], want=[0,2]"
//      , ["nl",true]
//      ]      
//    , [ match( fml, ps=["(",0_9,")"], want=[1] )
//             , [[5, 7, "(2)", ["2"]]
//             , [12, 14, "(5)",["5"]]]
//             , "fml, ps=['(',0_9,')'], want=[1]"
//       , ["nl",true]
//      ]
// 
//     ,""
//     ,"// Each p, if a string of chars, can set how many"
//     ,"// chars to match"
//    , [ match( fml, ps=[[A_z,2]] )
//             , [[2, 3, "co", ["co"]], [9, 10, "si", ["si"]]]
//             , "fml, ps=[[A_z,2]]"
//       , ["nl",true]
//      ]    
//
//    , [ match( fml, ps=[[A_z,2],"(",0_9,")"] )
//             , [[3, 7, "os(2)", ["os","(","2",")"]]
//              , [10, 14, "in(5)", ["in","(","5",")"]]]
//             , "fml, ps=[[A_z,2],'(',0_9,')']"
//       , ["nl",true]
//      ]
//      ===
//    , "", "// Nested blocks "
//    , ""  
//    , [ match( "a((c+(d+1))+1)"
//             , ps=[["(",1],str(a_z,"+",0_9),[")",1]] 
//             )
//             , [[1, 13, "((c+(d+1))+1)", ["(","(c+(d+1))+1",")"]]
//              , [2, 10, "(c+(d+1))", ["(","(d+1)",")"]]
//              , [5, 9, "(d+1)", ["(", "d+1", ")"]]
//               ]
//             , "fml, ps=[[A_z,2],'(',0_9,')']"
//       , ["nl",true]
//      ]
//
//    ], mode=mode, opt=opt, scope=["fml",fml]
//    )
// );
//echo( match_test(mode=112)[1] );
    
    
//==========================================================================

//function match_at(s,i,p,n=undef, _i0=-1, _imax=undef, _m=undef, _rtn="") = //, _debug="")=
//(
//   i>=len(s) || _i0+n>len(s) ? false
//   
//   : p==".."? // NEW: p="..": match ANY for as many as possible (2016.12.30)
//     ( i>=len(s) 
//         || i+n-1>=len(s) 
//         || i+n[0]-1>=len(s)
//         ? false
//       :n>0? [i, n+i, slice(s,i, n+i)]
//       :[i, len(s), slice(s,i)]
//     ) 
//     
//   : p[0]=="."&&p[1]=="."? // NEW: p="..abc": as many of ANY chars AND n # of a,b,c(2016.12.31)
//     ( let( pc= slice(p,2)                // pc: pattern char, like the "abc" after ".." 
//           , idx_pc = search( pc           // Search the index of first match to pc 
//                            , slice(s,i) ) // Return [ia,ib,ic] or []
//           , m= idx_pc==[]? false
//                : match_at( s, _i+ min(idx_pc), pc, n) 
//                                          // min(idx_pc): index of the earlist match of pc
//           ) 
//        m ? [i, m[1], slice(s,i,m[1])] 
//        : false
//     )  
//     
//   : p[0]=="-"&&p[1]=="-"? // New 17.1.2: p='--abc': as many of, or n count of, ANY EXCEPT abc"
//     ( let( pc= slice(p,2)         // pc: pattern char, like the "abc" after ".." 
//           , s2= slice(s,i)
//           , i_fail = min(search(pc, s2)) +i
//                                   // Search the index of first match to pc 
//                                   // Return min of [ia,ib,ic] or []
//
//          )
//       n==undef? 
//        ( i_fail ?  [i, i_fail, slice(s,i,i_fail)]
//        : [i, len(s), s2] 
//        )
//        : isint(n)?
//          ( i_fail? 
//             ( i_fail<= i+n-1 ? false 
//             : [i, i+n, slice(s,i,i+n)] 
//             )
//          : [i, i+n, slice(s,i,i+n)] 
//          )
//        : // When n = [min,max]: 
//           ( i_fail? 
//             ( i_fail<= i+n[0]-1 ? false 
//             : i_fail<= i+n[1]-1? [i, i_fail, slice(s,i,i_fail)] 
//             : [i, i+n[1], slice(s,i,i+n[1])] 
//             )
//          : [i, i+n[1], slice(s,i,i+n[1])] 
//          ) 
//     )  
//     
//   : str(p)==p?  // when p is a string, we do a recursion of match_at()
//     (  
//       let( _imax= _i!=-1? _imax     // In recursive runs, no change
//                    : n==undef?       // In init run, set to last i if n not set
//                      len(s)-1
//                    : isarr(n)? _i+n[1]-1
//                    : _i+n-1
//          //, _i= _i==-1? i:_i       // Keep a record of first i 
//          , _m = search( s[i],p )     // Return [2,4...] or []
//          )
//       _i>_imax||_m==[] ?              // Exit condition met
//       (
//           //_rtn==""?
//             
//           (isint(n) && len(_rtn)!=n) 
//           || (isarr(n) && (len(_rtn)<n[0] || len(_rtn)>n[1] ))
//           ? false
//           : _rtn==""?
//             (
//                n[0]==0 && i==_i0  // For cases like match_at( "abc",1,0_9,[0,1] )
//                ? [i,i,""]         // ==> [1,1,""]
//                : false
//             ) 
//           : [i,_i,_rtn]             // <==== return
//       )   
//       :match_at(s, i+1, p,  n, _i0, _imax, _m,  _m==[]?_rtn:str(_rtn, s[i]) 
//                )
//     )
//
//     : // When p is an array:
//       let( rtn= begwith( slice(s,i), p) )
//       rtn? [i, i+len(rtn), rtn]
//       : false
//);

function match_at(s,i,p,n=undef, _i0=-1, _imax=undef, _m=undef, _rtn="") = //, _debug="")=
(
   //echo(str("match_at(\"",s,"\")"))
   i>len(s) || _i0+n>len(s) ? false
   
   : p==".."? // NEW: p="..": match ANY for as many as possible (2016.12.30)
     ( i>=len(s) 
         || i+n-1>=len(s) 
         || i+n[0]-1>=len(s)
         ? false
       :n>0? [i, n+i, slice(s,i, n+i)]
       :[i, len(s), slice(s,i)]
     ) 
     
   : p[0]=="."&&p[1]=="."? // NEW: p="..abc": as many of ANY chars AND n # of a,b,c(2016.12.31)
     ( let( pc= slice(p,2)                // pc: pattern char, like the "abc" after ".." 
           , idx_pc = search( pc           // Search the index of first match to pc 
                            , slice(s,i) ) // Return [ia,ib,ic] or []
           , m= idx_pc==[]? false
                : match_at( s, i+ min(idx_pc), pc, n) 
                                          // min(idx_pc): index of the earlist match of pc
           ) 
        m ? [i, m[1], slice(s,i,m[1])] 
        : false
     )  
     
   : p[0]=="-"&&p[1]=="-"? // New 17.1.2: p='--abc': as many of, or n count of, ANY EXCEPT abc"
     ( let( pc= slice(p,2)         // pc: pattern char, like the "abc" after ".." 
           , s2= slice(s,i)
           , i_fail = min(search(pc, s2)) +i
                                   // Search the index of first match to pc 
                                   // Return min of [ia,ib,ic] or []
          )
       n==undef? 
        ( i_fail ?  [i, i_fail, slice(s,i,i_fail)]
        : [i, len(s), s2] 
        )
        : isint(n)?
          ( i_fail? 
             ( i_fail<= i+n-1 ? false 
             : [i, i+n, slice(s,i,i+n)] 
             )
          : [i, i+n, slice(s,i,i+n)] 
          )
        : // When n = [min,max]: 
           ( i_fail? 
             ( i_fail<= i+n[0]-1 ? false 
             : i_fail<= i+n[1]-1? [i, i_fail, slice(s,i,i_fail)] 
             : [i, i+n[1], slice(s,i,i+n[1])] 
             )
          : [i, i+n[1], slice(s,i,i+n[1])] 
          ) 
     )  
     
   : str(p)==p?  // when p is a string, we do a recursion of match_at()
     (  
       let( _imax= _i0!=-1? _imax     // In recursive runs, no change
                    : n==undef?       // In init run, set to last i if n not set
                      len(s)-1
                    : isarr(n)? i+n[1]-1
                    : i+n-1
          , _i0= _i0==-1? i:_i0       // Keep a record of first i 
          , _m = search( s[i],p )     // Return [2,4...] or []
          )
       i>_imax||_m==[] ?              // Exit condition met
       (
           //_rtn==""?
             
           (isint(n) && len(_rtn)!=n) 
           || (isarr(n) && (len(_rtn)<n[0] || len(_rtn)>n[1] ))
           ? false
           : _rtn==""?
             (
                n[0]==0 && i==_i0  // For cases like match_at( "abc",1,0_9,[0,1] )
                ? [i,i,""]         // ==> [1,1,""]
                : false
             ) 
           : [_i0,i,_rtn]             // <==== return
       )   
       :match_at(s, i+1, p,  n, _i0, _imax, _m,  _m==[]?_rtn:str(_rtn, s[i]) 
                )
     )

     : // When p is an array:
       let( rtn= begwith( slice(s,i), p) )
       rtn? [i, i+len(rtn), rtn]
       : false
);
  
    match_at=[ "match_at", "s,i,p,n", "array|false","Inspect",
    "Match a single pattern p to substring starting from index i 
    ;;  of string s. Return [i,j,word] or false. 
    ;; 
    ;; p: pattern, like: A_Z, 0_9, 'abcdef', ['cos','sin'] , '..', 
    ;;    p='..':    match any for as many as possible. 
    ;;    p='..abc': match as many of ANY chars AND n # of a,b,c
    ;; 
    ;; If p a str, match s[i] to any char of p, continue for
    ;; a number of n (or end of s if n not defined);
    ;;
    ;; If n is given, match exactly n chars. If not, match as  
    ;; many as possible.
    ;;
    ;; If p an arr, match any item in p to the substr starting
    ;; from i for only once (means, n has no effect).
    ;; 
    ;; If matched, return [i,j,word] where word is the matched
    ;; word and j is its last index.
    "
    , "match_ps_at"
    ];

    function match_at_test( mode=MODE, opt=[] )=
    (
       let( fml = "a+cos(2)+sin(5)"
                // 0123456789 1234
           )
       doctest( match_at,
       [
         "var fml"
        ,"// 0123456789 1234"
        , ""
        , [ match_at(fml, 2, a_z), [2,5,"cos"],"'a+cos(2)+sin(5)',2, a_z"]
        , [ match_at(fml, 1, a_z), false,"'a+cos(2)+sin(5)',1, a_z"]
        , [ match_at(fml, 3, a_z), [3,5,"os"],"'a+cos(2)+sin(5)',3, a_z"]
        , [ match_at(fml, 2, a_z,2), [2,4,"co"],"'a+cos(2)+sin(5)',2, a_z, n=2"]
        , [ match_at(fml, 3, a_z,2), [3,5,"os"],"'a+cos(2)+sin(5)',3, a_z, n=2"]
        , [ match_at(fml, 1, a_z,2), false,"'a+cos(2)+sin(5)',1, a_z, n=2"]
        , [ match_at(fml, 12, str("(",0_9,")")), [12,15,"(5)"]
                ,"'a+cos(2)+sin(5)',12, str('(',0_9,')')"]
        , [ match_at(fml, 2, str(a_z,0_9,"(")), [2,7,"cos(2"],"'a+cos(2)+sin(5)',2, str(a_z,0_9,'(')"]
        , [ match_at(fml, 2, str(a_z,0_9,"("),4), [2,6,"cos("],"'a+cos(2)+sin(5)',2, str(a_z,0_9,'('),n=4"]
        , [ match_at(fml, 14, "[]{}()"), [14,15,")"],"'a+cos(2)+sin(5)',14,'[]{}()'"]
        , [ match_at("3.1416cm", 2, 0_9), [2,6,"1416"],"'3.1416cm',2,0_9"]
        , [ match_at("a+cos(120)",6, "012",3), [6,9,"120"], "'a+cos(120)',6, '012',3"]
        , [ match_at("a+cos(120)",6, "012",4), false, "'a+cos(120)',6, '012',4"]
   
        ,"","// New 2017.1.10: n could be array: [min,max], to limit the size of match."
        ,""
        , [ match_at("a+cos(120)",2, a_z,[2,4]), [2,5,"cos"], "'a+cos(120)',2, a_z,[2,4]"]
        , [ match_at("a+co(120)",2, a_z,[2,4]), [2,4,"co"], "'a+co(120)',2, a_z,[2,4]"]
        , [ match_at("a+c(120)",2, a_z,[2,4]), false, "'a+c(120)',2, a_z,[2,4]"]
        , [ match_at("a+c(120)",2, a_z,[1,4]), [2,3,"c"], "'a+c(120)',2, a_z,[1,4]"]
        , [ match_at("a+cosd(120)",2, a_z,[2,4]), [2,6,"cosd"], "'a+cosd(120)',2, a_z,[2,4]"]
        , [ match_at("a+cos(120)",2, a_z,[1,2]), [2,4,"co"], "'a+cos(120)',2, a_z,[1,2]"]
        , [ match_at("a+cos(120)",2, a_z,[0,1]), [2,3,"c"], "'a+cos(120)',2, a_z,[0,1]"]
        
        ,"", "// NOTE this: The following returns a match that matches nothing. Usually"
        ,"// this should have been false, but We ask for either none or 1 of a_z at i=1, "
        ,"// and 'found none' is expected, so this does return something."
        , [ match_at("a+cos(120)",1, a_z,[0,1]), [1,1,""], "'a+cos(120)',2, a_z,[0,1]"]
   
        , "","// When the pattern is an array, match any item in the array ONLY ONCE:"
        , ""
        , [ match_at(fml, 2, ["abc","def"]), false,"'a+cos(2)+sin(5)',2, ['abc','def']"]
        , [ match_at(fml, 2, ["abc","sin"]), false,"'a+cos(2)+sin(5)',2, ['abc','sin']"]
        , [ match_at(fml, 2, ["abc","co"]), [2,4,"co"],"'a+cos(2)+sin(5)',2, ['abc','co']"]
        , [ match_at(fml, 2, ["cos","co"]), [2,5,"cos"],"'a+cos(2)+sin(5)',2, ['cos','co']"]
        , [ match_at(fml, 2, ["sin","cos"]),[2,5,"cos"],"'a+cos(2)+sin(5)',2, ['sin','cos']"]
        , [ match_at(fml, 2, ["co","cos"]), [2,4,"co"],"'a+cos(2)+sin(5)',2, ['co','cos']"]
        , ""
        , "//If there's spaces : "
        , ""
        , [ match_at("a+cos (2)", 2, str(a_z,0_9,"(")), [2,5,"cos"],"'a+cos (2)',2, str(a_z,0_9,'(')"]
        , [ match_at("a+cos (2)", 2, str(a_z,0_9,"( ")), [2,8,"cos (2"],"'a+cos (2)',2, str(a_z,0_9,'( ')"]
        , ""
        ,"// Next one returns sin, not sincos. When p is array, match only once"
        , [ match_at("a+sincos(2)", 2, ["cos","sin"])
                     , [2,5,"sin"],"'a+sincos(2)', 2, ['cos','sin']"
                     ]
        ,"", "// New 2016.12.30: pattern '..' for ANY"
        ,""
        , [ match_at("a+cos(2)", 2, ".."), [2,8,"cos(2)"],"'a+cos(2)',2, '..'", ["//","ANY from i=2 to end"]]
        , [ match_at("a+cos(2)", 3, ".."), [3,8,"os(2)"],"'a+cos(2)',3, '..'", ["//","ANY from i=3 to end"]]
        , [ match_at("a+cos(2)", 3, "..",2), [3,5,"os"],"'a+cos(2)',3, '..',2",["//","2 of ANY from 3"]]
        , [ match_at("a+cos(2)", 7, ".."), [7,8,")"],"'a+cos(2)',7, '..'",["//","ANY from 7 to end"]]
        , [ match_at("a+cos(2)", 8, ".."), false,"'a+cos(2)',8, '..'",["//","Any from 8 is out of bound"]]
        , [ match_at("a+cos(2)", 7, "..",2), false,"'a+cos(2)',7, '..',2",["//","2 of ANY from 7 out of bound"]]
        
        ,"", "// New 2016.12.31: p='..abc': as many of ANY chars AND n # of a,b,c"
        ,""
        , [ match_at("a+cos(120)", 2, "..xyz"), false,"'a+cos(120)',2, '..xyz'"]
        , [ match_at("a+cos(120)", 2, "..012"), [2,9,"cos(120"],"'a+cos(120)',2, '..012'"]
        , [ match_at("a+cos(120)", 3, "..012"), [3,9,"os(120"],"'a+cos(120)',3, '..012'"]
        , [ match_at("a+cos(120)", 1, "..012",2), [1,8,"+cos(12"],"'a+cos(120)',1, '..012',2"]
        , [ match_at("a+cos(120)", 1, "..012",4), false,"'a+cos(120)',1, '..012',4"]
        , [ match_at("a+cos(120)", 1, "..012)",4), [1,10,"+cos(120)"],"'a+cos(120)',1, '..012)',4"]
        , [ match_at("a+cos(120)", 6, "..012"), [6,9,"120"],"'a+cos(120)',6, '..012'"]
        
        ,"","// New 17.1.10: when n is [min,max] to limit the range of count"
        ,"//    of match for the 012 in '..012': "
        ,""
        , [ match_at("a+cos(12012)", 1, "..012",[2,4]), [1,10,"+cos(1201"]
            ,"'a+cos(120120)',1, '..012',[2,4]"]    
        , [ match_at("a+cos(1201)", 1, "..012",[2,4]), [1,10,"+cos(1201"]
            ,"'a+cos(1201)',1, '..012',[2,4]"]    
        , [ match_at("a+cos(120)", 1, "..012",[2,4]), [1,9,"+cos(120"]
            ,"'a+cos(120)',1, '..012',[2,4]"]    
        , [ match_at("a+cos(12)", 1, "..012",[2,4]), [1,8,"+cos(12"]
            ,"'a+cos(12)',1, '..012',[2,4]"]    
        , [ match_at("a+cos(1)", 1, "..012",[2,4]), false
            ,"'a+cos(1)',1, '..012',[2,4]"]    
        

        ,"", "// New 2017.1.2: p='--abc': as many of, or n count of, ANY chars EXCEPT abc"
        ,""
        , [ match_at("a+cos(120)", 2, "--xyz"), [2,10,"cos(120)"],"'a+cos(120)',2, '--xyz'"]
        , [ match_at("a+cos(120)", 2, "--xyz",3), [2,5,"cos"],"'a+cos(120)',2, '--xyz',n=3"]
        , [ match_at("a+cos(120)", 2, "--012"), [2,6,"cos("],"'a+cos(120)',2, '--012'"]
        , [ match_at("a+cos(120)", 3, "--012"), [3,6,"os("],"'a+cos(120)',3, '--012'"]
        , [ match_at("a+cos(120)", 1, "--012",2), [1,3,"+c"],"'a+cos(120)',1, '--012',n=2"]

        ,"// Note: n is expected len of matched string. When given, need"
        ,"//       exactly n count of non-pattern  chars (in this case, 012)"
                   // 0123456789
        , [ match_at("a+cos(120)", 1, "--012"), [1,6,"+cos("],"'a+cos(120)',1, '--012'"]
        , [ match_at("a+(120)", 1, "--012"), [1,3,"+("],"'a+(120)',1, '--012'"]
        , [ match_at("a+(120)", 1, "--012", 2), [1,3,"+("],"'a+(120)',1, '--012',2"]
        , [ match_at("a+(120)", 1, "--012", 3), false,"'a+(120)',1, '--012',3"]
        , [ match_at("a+cos(120)", 1, "--012",4), [1,5,"+cos"],"'a+cos(120)',1, '--012', n=4"]
        , [ match_at("a+cos(120)", 1, "--012",2), [1,3,"+c"],"'a+cos(120)',1, '--012', n=2"]
        , [ match_at("a+cos(120)", 1, "--012",6), false,"'a+cos(120)',1, '--012', n=6"]
        , [ match_at("a+cos(120)", 1, "--012",7), false,"'a+cos(120)',1, '--012', n=7"]
        , [ match_at("a+cos(120)", 1, "--012",17), false,"'a+cos(120)',1, '--012', n=17"]
        ,""
        ,"// New 17.1.10: n could be [min,max]:"
        ,""
        , [ match_at("0123456_8", 1, "--_"),[1,7,"123456"], "'0123456_8', 1, '--_'"]
        , [ match_at("0123456_8", 1, "--_",[2,4]),[1,5,"1234"], "'0123456_8', 1, '--_',[2,4]"]
        , [ match_at("01234_6_8", 1, "--_",[2,4]),[1,5,"1234"], "'01234_6_8', 1, '--_',[2,4]"]
        , [ match_at("0123_56_8", 1, "--_",[2,4]),[1,4,"123"], "'0123_56_8', 1, '--_',[2,4]"]
        , [ match_at("012_456_8", 1, "--_",[2,4]),[1,3,"12"], "'012_456_8', 1, '--_',[2,4]"]
        , [ match_at("01_3456_8", 1, "--_",[2,4]),false, "'01_3456_8', 1, '--_',[2,4]"]
        
        
        
       ]
       , mode=mode, opt=opt, scope=["fml",fml]
       )
    );

    //echo( match_at_test(mode=112)[1] );                    
    
     
//==========================================================================
     
function matchblk( s 
                    , blk="[]"
                    , keep=true
                    , _buf= [] // store indices
                    , _rtn=[]
                    , _i=0
                    , _debug=""
                    )=
(  
    let( 
         mh = s[_i]==blk[0] // a head is matched
       , mt = s[_i]==blk[1] // a tail matched
       , mi= mt?last(_buf):-1 // If mt, mi is the i of block head
       , m = mt? ( keep? [mi,_i, slice(s,mi,_i+1)]
                   : [mi,_i, slice(s,mi+1,_i)]
                 ):[]  
       ,_rtn = mt? (mi< _rtn[0][0]? 
                   concat( [m],_rtn)
                   :app( _rtn, m))
               :_rtn
       ,_newbuf = mh? app(_buf,_i)
              :mt? slice(_buf,0,-1): _buf
       ,_debug= str(_debug,"<br/>",
              [ "_i", _i
                    , "s[_i]", s[_i] 
                    , "mh", mh
                    , "mt", mt
                    , "mi", mi
                    , "m", m     
                    , "_buf", _buf
                    , "_rtn", _rtn
                    ])
       )//let    
                    
    _i==len(s)-1? _rtn 
     :matchblk( s, blk, keep, _newbuf,_rtn, _i+1,_debug)
); 

//s= "a([c+(d+1)]+(e+1))";
////  0123456789 1234567
//echo( matchblk( s, blk="()" ) );
//echo( matchblk( s, blk="()",keep=true ) );
//s2= "[3,[a,4],[5,7]]";
//echo( matchblk( s2 ) );

//function evalarr(s, _rtn=[])=
//(
//   let( ps= [["[",1],str(a_zu,0_9d,"\","),["]",1]] 
//      , m = match(s,ps)    
//      , meval = m? [ for(x=m) eval(x[2]) ]:undef
//      )    
//   3
//
//
//
//);
//=============================================================

function match_at_rev(s,i,p,n=undef, _i=undef, _imin=undef, _m=undef, _rtn="") = //, _debug="")=
(
   let( i= i<0? len(s)+i:i
      //, _i= _i==undef? i: _i  // the running index
      )
   
   i>=len(s) || (i-n<0)
   ? false
   
   //-------------------------------
   : p==".."? // NEW: p="..": match ANY for as many as possible (2016.12.30)
     ( n>0? [i-n+1, i+1, slice(s,i-n+1, i+1)]
       :[0, i+1, slice(s,0, i+1)]
     ) 
   
   //-------------------------------
   : p[len(p)-1]=="." && // p="abc..": as many of ANY chars AND n # of a,b,c
     p[len(p)-2]=="."? 
     ( let( pc= slice(p,0, -2)             // pc: pattern char, like the "abc" BEFORE ".." 
           , idx_pc = search( pc           // Search the index of last match to pc 
                            , slice(s,0,i+1) 
                            ) // Return [ia,ib,ic] or []
           , m= idx_pc==[]? false
                : match_at_rev( s, max(idx_pc), pc, n) 
                                          // man(idx_pc): index of the last match of pc
           ) 
        m ? [m[0],i+1,  slice(s,m[0],i+1)] 
        : false
     )  

   //-------------------------------
   : p[len(p)-1]=="-" && // p='abc--': as many of, or n count of, ANY EXCEPT abc"
     p[len(p)-2]=="-"?  
     ( let( pc= slice(p,0,-1)       // pc: pattern char, like the "abc" after ".." 
          , s2= slice(s,0, i+1)
          , i_fail = max(search(pc, s2))
                                   // Search the index of last match to pc 
                                   // Return max of [ia,ib,ic] or []
          )
       n==undef? 
       ( i_fail ? [i_fail+1,i+1, slice(s, i_fail,i+1) ] 
                : [0, i+1, s2] 
       )
       : // n is int
         isint(n)? 
         ( i_fail? 
             ( i_fail> i-n ? false 
             : [i-n, i+1, slice(s,i-n,i+1)] 
             )
         : [i-n, i+1, slice(s,i-n,i+1)] 
         )
       : // When n = [min,max]: 
         ( i_fail? 
            ( i_fail> i-n[1] ? false 
            : i_fail> i+n[0]-1? [i_fail, i+1, slice(s,i_fail,i+1)] 
            : [i-n[1], i+1, slice(s,i-n[1],i+1)] 
            )
         : [i-n[1], i+1, slice(s,i-n[1],i+1)] 
         ) 
     )  
 
   //-------------------------------
   : str(p)==p?  // when p is a string, we do a recursion of match_at_rev() (backward)
     (  
       let( _imin= _i!=undef? _imin     // In recursive runs, no change
                    : n==undef?  0     // In init run, set to last i if n not set
                    : isarr(n)? i-n[0]-1
                    : i-n+1
          , _i= _i==undef? i:_i       // Keep a record of first i 
          , _m = search( s[_i],p )     // Return [2,4...] or []
          )
         //["i",i, "_i",_i,"n",n,"_imin=",_imin] 
       _i<_imin||_m==[] ?              // Exit condition met
       (
           //_rtn==""?
             
           (isint(n) && len(_rtn)!=n) 
           || (isarr(n) && (len(_rtn)<n[0] || len(_rtn)>n[1] ))
           ?  false //[false, _i, i+1, _rtn, n , _imin]
           : _rtn==""?
             (
                n[0]==0 && i==_i  // For cases like match_at( "abc",2,0_9,[0,1] )
                ? [i,i,""]         // ==> [2,2,""]
                : false
             ) 
           : [_i+1, i+1,_rtn]             // <==== return
       )   
       :match_at_rev(s=s, i=i, p=p, n=n
                    , _i=_i-1, _imin=_imin, _m=_m
                    , _rtn= _m==[]?_rtn:str(s[_i], _rtn) 
                    )
     )

     : // When p is an array:
       let( rtn= endwith( slice(s,0, i+1), p) )
       rtn? [ i-len(rtn)+1, i+1,rtn]
       : false
);

match_at_rev=[ "match_at_rev", "s,i,p,n", "array|false","Inspect",
    "Match a single pattern p to substring starting from index i 
    ;;  of string s. Return [i,j,word] or false. 
    ;; 
    ;; p: pattern, like: A_Z, 0_9, 'abcdef', ['cos','sin'] , '..', 
    ;; 
    ;; If p a str, match s[i] to any char of p, continue for
    ;; a number of n (or end of s if n not defined);
    ;;
    ;; If n is given, match exactly n chars. If not, match as  
    ;; many as possible.
    ;;
    ;; If p an arr, match any item in p to the substr starting
    ;; from i for only once (means, n has no effect).
    ;; 
    ;; If matched, return [i,j,word] where word is the matched
    ;; word and j is its last index.
    "
    , "match_ps_at"
    ];

    function match_at_rev_test( mode=MODE, opt=[] )=
    (
       let( fml = "a+cos(2)+sin(5)"
                // 0123456789 1234
           )
       //echo(mode=mode)    
       doctest( match_at_rev,
       [
         "var fml"
        ,"//i= i: 0123456789 1234"
//        , ""
        , [ match_at_rev(fml, 4, a_z), [2,5,"cos"],"'a+cos(2)+sin(5)',4, a_z"]
        , [ match_at_rev(fml, 3, a_z), [2,4,"co"],"'a+cos(2)+sin(5)',3, a_z"]
        , [ match_at_rev(fml, 2, a_z), [2,3,"c"],"'a+cos(2)+sin(5)',2, a_z"]
        , [ match_at_rev(fml, 4, a_z,2), [3,5,"os"],"'a+cos(2)+sin(5)',4, a_z, n=2"]
        , [ match_at_rev(fml, 3, a_z,2), [2,4,"co"],"'a+cos(2)+sin(5)',3, a_z, n=2"]
        , [ match_at_rev(fml, 2, a_z,2), false,"'a+cos(2)+sin(5)',2, a_z, n=2"]
        , [ match_at_rev(fml, -1, str("(",0_9,")")), [12,15,"(5)"]
                ,"'a+cos(2)+sin(5)',-1, str('(',0_9,')')"]
        , [ match_at_rev(fml, -2, str(a_z,0_9,"(")), [9,14,"sin(5"],"'a+cos(2)+sin(5)',-2, str(a_z,0_9,'(')"]
        , [ match_at_rev(fml, -1, str(a_z,0_9,"()"),4), [11,15,"n(5)"],"'a+cos(2)+sin(5)',-1, str(a_z,0_9,'()'),n=4"]
        
        , [ match_at_rev(fml, 14, "[]{}()"), [14,15,")"],"'a+cos(2)+sin(5)',14,'[]{}()'"]
        , [ match_at_rev("3.1416cm", 5, 0_9), [2,6,"1416"],"'3.1416cm',5,0_9"]
        , [ match_at_rev("a+cos(120)",8, "012",3), [6,9,"120"], "'a+cos(120)',8, '012',3"]
        , [ match_at_rev("a+cos(120)",8, "012",4), false, "'a+cos(120)',8, '012',4"]
   
        ,"","// New 2017.1.10: n could be array: [min,max], to limit the size of match."
        ,""
        , [ match_at_rev("a+cos(120)",4, a_z,[2,4]), [2,5,"cos"], "'a+cos(120)',4, a_z,[2,4]"]
        , [ match_at_rev("a+co(120)",3, a_z,[2,4]), [2,4,"co"], "'a+co(120)',3, a_z,[2,4]"]
        , [ match_at_rev("a+c(120)",2, a_z,[2,4]), false, "'a+c(120)',2, a_z,[2,4]"]
        , [ match_at_rev("a+c(120)",2, a_z,[1,4]), [2,3,"c"], "'a+c(120)',2, a_z,[1,4]"]
        , [ match_at_rev("a+acosd(120)",6, a_z,[2,4]), [3,7,"cosd"], "'a+acosd(120)',5, a_z,[2,4]"]
        , [ match_at_rev("a+cos(120)",3, a_z,[1,2]), [2,4,"co"], "'a+cos(120)',3, a_z,[1,2]"]
        , [ match_at_rev("a+cos(120)",2, a_z,[0,1]), [2,3,"c"], "'a+cos(120)',2, a_z,[0,1]"]
        
        ,"", "// NOTE this: The following returns a match that matches nothing. Usually"
        ,"// this should have been false, but We ask for either none or 1 of a_z at i=1, "
        ,"// and 'found none' is expected, so this does return something."
        , [ match_at_rev("a+cos(120)",1, a_z,[0,1]), [1,1,""], "'a+cos(120)',2, a_z,[0,1]"]
   
        , "","// When the pattern is an array, match any item in the array ONLY ONCE:"
        , ""
        , [ match_at_rev(fml, 4, ["abc","def"]), false,"'a+cos(2)+sin(5)',4, ['abc','def']"]
        , [ match_at_rev(fml, 4, ["abc","sin"]), false,"'a+cos(2)+sin(5)',4, ['abc','sin']"]
        , [ match_at_rev(fml, 3, ["abc","co"]), [2,4,"co"],"'a+cos(2)+sin(5)',3, ['abc','co']"]
        , [ match_at_rev(fml, 4, ["cos","sin"]), [2,5,"cos"],"'a+cos(2)+sin(5)',4, ['cos','sin']"]
        , [ match_at_rev(fml, 4, ["sin","cos"]),[2,5,"cos"],"'a+cos(2)+sin(5)',4, ['sin','cos']"]
        , [ match_at_rev(fml, 3, ["co","cos"]), [2,4,"co"],"'a+cos(2)+sin(5)',3, ['co','cos']"]
//        , ""
//        , "//If there's spaces : "
//        , ""
//        , [ match_at_rev("a+cos (2)", 2, str(a_z,0_9,"(")), [2,5,"cos"],"'a+cos (2)',2, str(a_z,0_9,'(')"]
//        , [ match_at_rev("a+cos (2)", 2, str(a_z,0_9,"( ")), [2,8,"cos (2"],"'a+cos (2)',2, str(a_z,0_9,'( ')"]
//        , ""
        ,"// Next one returns cos, not sincos. When p is array, match only once"
        , [ match_at_rev("a+sincos(2)", 7, ["cos","sin"])
                     , [5,8,"cos"],"'a+sincos(2)', 7, ['cos','sin']"
                     ]
        ,"", "// New 2016.12.30: pattern '..' for ANY"
        ,""
        , [ match_at_rev("a+cos(2)", 2, ".."), [0,3,"a+c"],"'a+cos(2)',2, '..'", ["//","ANY from i=2 to end"]]
        , [ match_at_rev("a+cos(2)", 3, ".."), [0,4,"a+co"],"'a+cos(2)',3, '..'", ["//","ANY from i=3 to end"]]
        , [ match_at_rev("a+cos(2)", 3, "..",2), [2,4,"co"],"'a+cos(2)',3, '..',2",["//","2 of ANY from 3"]]
        , [ match_at_rev("a+cos(2)", 7, ".."), [0,8,"a+cos(2)"],"'a+cos(2)',7, '..'",["//","ANY from 7 to end"]]
        , [ match_at_rev("a+cos(2)", 8, ".."), false,"'a+cos(2)',8, '..'",["//","Any from 8 is out of bound"]]
        , [ match_at_rev("a+cos(2)", 7, "..",2), [6,8,"2)"], "'a+cos(2)',7, '..',2",["//","2 of ANY from 7 out of bound"]]
        
        ,"", "// p='abc..': as many of ANY chars AND n # of a,b,c"
        ,""
        , [ match_at_rev("a+cos(120)", 8, "xyz.."), false,"'a+cos(120)',8, 'xyz..'"
          , ["//", "Can't find xyz"]]
        , [ match_at_rev("a+cos(120)", 8, str(a_z,"..")), [2,9,"cos(120"],"'a+cos(120)',8, str(a_z,'..')"]
        , [ match_at_rev("a+cos(120)", 9, "012.."), [6,10,"120)"],"'a+cos(120)',9, '012..'"]
        , [ match_at_rev("a+cos(120)", 8, str(a_z,".."),2), [3,9,"os(120"],"'a+cos(120)',8, str(a_z,'..'),2"]
        , [ match_at_rev("a+cos(120)", 8, str(a_z,".."),4), false,"'a+cos(120)',8, str(a_z,'..'),4"]
        , [ match_at_rev("a+cos(120)", 8, str(a_z,"(.."),4), [2,9,"cos(120"],"'a+cos(120)',8, str(a_z,'(..'),4"]
//        ,"","// New 17.1.10: when n is [min,max] to limit the range of count"
//        ,"//    of match for the 012 in '012..': "
//        ,""
//        , [ match_at_rev("a+cos(12012)", 1, "012..",[2,4]), [1,10,"+cos(1201"]
//            ,"'a+cos(120120)',1, '012..',[2,4]"]    
//        , [ match_at_rev("a+cos(1201)", 1, "012..",[2,4]), [1,10,"+cos(1201"]
//            ,"'a+cos(1201)',1, '012..',[2,4]"]    
//        , [ match_at_rev("a+cos(120)", 1, "012..",[2,4]), [1,9,"+cos(120"]
//            ,"'a+cos(120)',1, '012..',[2,4]"]    
//        , [ match_at_rev("a+cos(12)", 1, "012..",[2,4]), [1,8,"+cos(12"]
//            ,"'a+cos(12)',1, '012..',[2,4]"]    
//        , [ match_at_rev("a+cos(1)", 1, "012..",[2,4]), false
//            ,"'a+cos(1)',1, '012..',[2,4]"]    
//        
//
//        ,"", "// New 2017.1.2: p='--abc': as many of, or n count of, ANY chars EXCEPT abc"
//        ,""
//        , [ match_at_rev("a+cos(120)", 2, "--xyz"), [2,10,"cos(120)"],"'a+cos(120)',2, '--xyz'"]
//        , [ match_at_rev("a+cos(120)", 2, "--xyz",3), [2,5,"cos"],"'a+cos(120)',2, '--xyz',n=3"]
//        , [ match_at_rev("a+cos(120)", 2, "--012"), [2,6,"cos("],"'a+cos(120)',2, '--012'"]
//        , [ match_at_rev("a+cos(120)", 3, "--012"), [3,6,"os("],"'a+cos(120)',3, '--012'"]
//        , [ match_at_rev("a+cos(120)", 1, "--012",2), [1,3,"+c"],"'a+cos(120)',1, '--012',n=2"]
//
//        ,"// Note: n is expected len of matched string. When given, need"
//        ,"//       exactly n count of non-pattern  chars (in this case, 012)"
//                   // 0123456789
//        , [ match_at_rev("a+cos(120)", 1, "--012"), [1,6,"+cos("],"'a+cos(120)',1, '--012'"]
//        , [ match_at_rev("a+(120)", 1, "--012"), [1,3,"+("],"'a+(120)',1, '--012'"]
//        , [ match_at_rev("a+(120)", 1, "--012", 2), [1,3,"+("],"'a+(120)',1, '--012',2"]
//        , [ match_at_rev("a+(120)", 1, "--012", 3), false,"'a+(120)',1, '--012',3"]
//        , [ match_at_rev("a+cos(120)", 1, "--012",4), [1,5,"+cos"],"'a+cos(120)',1, '--012', n=4"]
//        , [ match_at_rev("a+cos(120)", 1, "--012",2), [1,3,"+c"],"'a+cos(120)',1, '--012', n=2"]
//        , [ match_at_rev("a+cos(120)", 1, "--012",6), false,"'a+cos(120)',1, '--012', n=6"]
//        , [ match_at_rev("a+cos(120)", 1, "--012",7), false,"'a+cos(120)',1, '--012', n=7"]
//        , [ match_at_rev("a+cos(120)", 1, "--012",17), false,"'a+cos(120)',1, '--012', n=17"]
//        ,""
//        ,"// New 17.1.10: n could be [min,max]:"
//        ,""
//        , [ match_at_rev("0123456_8", 1, "--_"),[1,7,"123456"], "'0123456_8', 1, '--_'"]
//        , [ match_at_rev("0123456_8", 1, "--_",[2,4]),[1,5,"1234"], "'0123456_8', 1, '--_',[2,4]"]
//        , [ match_at_rev("01234_6_8", 1, "--_",[2,4]),[1,5,"1234"], "'01234_6_8', 1, '--_',[2,4]"]
//        , [ match_at_rev("0123_56_8", 1, "--_",[2,4]),[1,4,"123"], "'0123_56_8', 1, '--_',[2,4]"]
//        , [ match_at_rev("012_456_8", 1, "--_",[2,4]),[1,3,"12"], "'012_456_8', 1, '--_',[2,4]"]
//        , [ match_at_rev("01_3456_8", 1, "--_",[2,4]),false, "'01_3456_8', 1, '--_',[2,4]"]
        
       
       ]
       
       , mode=mode, opt=opt, scope=["fml",fml]
       )
    );
    
    
function matchr_at(s,i,p,n)=  // for i<0, match from the end
(
   let( 
   isarrp= isarr(p)
   , _i = i<0?(len(s)+i):i // real i
   , endi = isarrp? undef   // endi is the first unmatched
            :isint(n)?               // limit match count
             ( [ for(j=range(_i,_i-n-1)) // Found an unmatched i within _i ~ _i-n-1
                 if(!has(p,s[j])) false  // Record false if unmatch 
               ]? undef                  // Means match fails
                : _i-n                    // Success 
             )
             : [ for(j=range( _i, -1 )
                    )  // Note: run to the i behind 0
                     
                   if(!has(p,s[j])) j    // to pick up the
               ][0]                      // first char of s                  
   
   , rtn=   isarrp?                // p is an array: for each item, px,
              [ for( px=p )                 // check if each char in px
                  if(![for( j= range(px) )  // is the same as s[_i-?]   
                    if( s[_i-len(px)+j+1]!=px[j]) false]) px 
              ][0]
            : endi==undef? slice(s,0,_i+1)
              //: endi!=undef && 
            : endi!=_i? 
                 //(endi==len(s)? slice(s,i): slice( s,i,endi ))
                  slice(s,endi+1,_i+1)   
             :undef
  )
    //rtn? [i, i+len(rtn)-1, rtn]: false        
    rtn?  [isarrp?(_i-len(rtn)+1)
                 :endi==undef?0:(endi+1), _i+1, rtn]: false        
); 
  
    matchr_at=[ "matchr_at", "s,i,p,n", "array|false","Inspect",
    "Reversed match pattern p to index i of string s. Return [i,j,word] or false. 
    ;; 
    ;; p: pattern, like: A_Z, 0_9, 'abcdef', ['cos','sin'] 
    ;; 
    ;; If p a str, match s[i] to any char of p, continue for
    ;; a number of n (or end of s if n not defined);
    ;; 
    ;; If p an arr, match any item in p to the substr starting
    ;; from i for only once (means, n has no effect).
    ;; 
    ;; If matched, return [i,j,word] where word is the matched
    ;; word and j is its last index.
    "
    , "match_ps_at"
    ];


    function matchr_at_test( mode=MODE, opt=[] )=
    (
       let( fml = "a+cos(2)+sin(5)"
                // 0123456789 1234
           )
       doctest( matchr_at,
       [
         "var fml"
        ,"// i: 0123456789 1234"
        , ""
        , [ matchr_at(fml, -1, ")"), [14,15,")"],"'a+cos(2)+sin(5)',-1, ')'"]
        , [ matchr_at("abc+def", -1, a_z), [4,7,"def"],"'abc+def',-1, a_z"]
        , [ matchr_at("abc+def", -2, a_z), [4,6,"de"],"'abc+def',-2, a_z"]
        , [ matchr_at("abc+def", 2, a_z), [0,3,"abc"],"'abc+def',2, a_z"]
        , [ matchr_at("abc+def", -1, a_z,2), [5,7,"ef"],"'abc+def',-1, a_z,2"]
        , [ matchr_at("abc+def", -1, str(a_z,"+")), [0,7,"abc+def"],"'abc+def',-1, str(a_z,'+')"]
        
        , [ matchr_at("abc+def", -1, ["abc","def"])
                   , [4,7,"def"],"'abc+def',-1, ['abc','def']"]
        , [ matchr_at("abc+def", -2, ["abc","def"])
                   , false,"'abc+def',-2, ['abc','def']"]
        , [ matchr_at("abc+def", 6, ["abc","def"])
                   , [4,7,"def"],"'abc+def',6, ['abc','def']"]
        , [ matchr_at("abc+sin", -1, CALC_FUNCS)
                   , [4,7,"sin"],"'abc+sin',-1, CALC_FUNCS"]
                   
        
    //    , [ matchr_at(fml, 3, a_z), [3,4,"os"],"fml,3, a_z"]
    //    , [ matchr_at(fml, 2, a_z,2), [2,3,"co"],"fml,2, a_z, n=2"]
    //    , [ matchr_at(fml, 3, a_z,2), [3,4,"os"],"fml,3, a_z, n=2"]
    //    , [ matchr_at(fml, 12, str("(",0_9,")")), [12,14,"(5)"]
    //            ,"fml,12, str('(',0_9,')')"]
    //    , [ matchr_at(fml, 2, str(a_z,0_9,"(")), [2,6,"cos(2"],"fml,2, str(a_z,0_9,'(')"]
    //    , [ matchr_at(fml, 2, str(a_z,0_9,"("),4), [2,5,"cos("],"fml,2, str(a_z,0_9,'('),n=4"]
    //    , [ matchr_at(fml, 2, ["abc","def"]), false,"fml,2, ['abc','def']"]
    //    , [ matchr_at(fml, 14, "[]{}()"), [14,14,")"],"fml,14,'[]{}()'"]
    //    , ""
    //    ,"// Next one returns sin, not sincos. When p is array, match only once"
    //    , [ matchr_at("a+sincos(2)", 2, ["cos","sin"])
    //                 , [2,4,"sin"],"'a+sincos(2)', 2, ['cos','sin']"
    //                 ]
        
       ]
       , mode=mode, opt=opt, scope=["fml",fml]
       )
    );

    //echo( matchr_at_test(mode=112)[1] );

//=============================================================
function match_replace( s
    , ps          // list of patterns, each p could be string or array
    , new=""      // str or array
    , h = []
    , want=undef // int or arr
    )=
(
    let(m = match(s,ps) //: NOTE that we don't enter *want* here
                         //  such that it always returns:
                         //    [ [2,4,"(2)", ["(","2",")"]]
                         //  but not
                         //    [ [2,4,"(2)", ["2"]]
                         // 'cos we need the "(", in ["(","2",")"] to
                         // calc the starting index of this replace
       , want=isint(want)?[want]:want
       , ss= m?
             (want==undef ?
               str( slice( s,0,m[0][0])
                  , join( [for( mi = range(m) )
                            let(mrec =m[mi] // //: [2,4,"(2)", ["(","2",")"]
                               ,mw = mrec[2] 
                                )
                             str( hash(h, mw, isstr(new)?new:new[mi])
                                , mi< len(m)-1?
                                    slice( s, mrec[1], m[mi+1][0])
                                    :slice( s, mrec[1])
                                )                   
                            ], "")
                  )//str
               :join( 
                  [ for( mi = range(m) )
                      let( mrec =m[mi] //: [2,6,"(2,a)", ["(","2","a",")"]
                         , ma = mrec[3] //:["(","2","a",")"]
                         , b = join(slice( ma, 0, want[0]),"")
                         , e = join(slice( ma, last(want)+1),"")
                         , di = len(join(slice( ma, 0,want[0]+1),""))
                         //, dj = len(join(slice( ma, last(want),"")))
                         , mw = join( [for(ii=want) ma[ii]],"")
                        )
                     str( 
                          mi==0? slice( s,0, mrec[0]):""
                        , b
                        , hash(h, mw, isstr(new)?new:new[mi])
                        , e 
                        , mi< len(m)-1?
                            str( slice( s, mrec[1], m[mi+1][0]))
                            :slice( s, mrec[1])
                        //,"//["
                        //,"b=",b, ", e=",e 
                        //,"]//"
                        )                   
                  ]
                 , "")//join
               )//m?
               :s          
       )//let
      ss 
);

    
    match_replace=["match_replace","s,ps,new|h[,want]","str","Inspect, match",
    "Replace what's matched by the pattern with new string(s).
    ;;
    ;; s: target string
    ;; ps: patterns 
    ;; new: a str (all matched will be replaced by it)
    ;;      or arr (matches replaced one by one)
    ;; h: Or you can use a hash, h, to replace matches
    ;;    that can find the key in h with its corresponding
    ;;    value in h.
    ;; want: int: to replace the i-th item in the match.
    ;;            For example, if match = 
    ;;            [[2, 7, 'cos(5)', ['cos','(','5',')']],
    ;;            want=2 will get 5 replaced.
    ;;       arr: [1,2,3] or [1,3]: replace items 1~3 
    ;;            In the above example, [1,3] will replace (5).
    ","replace,_s,_s2"
    ];

    function match_replace_test( mode=MODE, opt=[] )=
    (

     let( fml = "a+cos(2)+sin(5)"
                 // 0123456789 1234
           ) 
        doctest( match_replace,
        [
          "var fml"
         ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
         , ""
         ,"Note: match( 'a+cos(2)+sin(5)', ps=[A_z] ) ="
         , replace(str(match( fml, ps=[A_z] ))," ","")
         ,""
         
        , [ match_replace( fml, ps=[A_z],new="XX" )
                 , "XX+XX(2)+XX(5)"
                 , "'a+cos(2)+sin(5)', ps=[A_z],'XX'"
           , ["nl",false]
          ]

        , [ match_replace( fml, ps=[A_z],new=["B","SIN","COS"] )
                 , "B+SIN(2)+COS(5)"
                 , "'a+cos(2)+sin(5)', ps=[A_z],['B','SIN','COS']"
           , ["nl",false]
          ]

        , "","// Using hash:", ""
        , [ match_replace( fml, ps=[A_z]
             ,h=["a",5, "sin","COS", "cos","TAN"] )
              , "5+TAN(2)+COS(5)"
              , "'a+cos(2)+sin(5)', ps=[A_z],h=['a',5,'sin','COS','cos','TAN']"
           , ["nl",true]
          ]
          
        ,"", "// more-item ps", ""

        , [ match_replace( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] 
                    , new="XX")
                 , "a+XX+XX"
                 , "'a+cos(2)+sin(5)', ps=[['cos(','sin('],'[(',0_9,')]'],'XX'"
          , ["nl",true]
          ]      

        ,""
        ,"//Using <b>want</b>:"
        ,"// Note want=[1,2,3] is the same as want=[1,3]"
        ,"", "var fml"
        , [ match_replace( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] 
                    , new="XX", want=[1,2,3])
                 , "a+cosXX+sinXX"
                 , "'a+cos(2)+sin(5)', [['cos(','sin('],'[(',0_9,')]'],'XX', want=[1,2,3]"
          , ["nl",true]
          ] 

        , [ match_replace( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] 
                    , new="XX", want=[1,3])
                 , "a+cosXX+sinXX"
                 , "'a+cos(2)+sin(5)', [['cos(','sin('],'[(',0_9,')]'],'XX', want=[1,3]"
          , ["nl",true]
          ] 
          
         ,"" 
         ,"// match('a+cos(2)+sin(5)', ps=[['cos(','sin('],'[(',0_9,')]'], want=2 )="
         , str("//", match( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"],want=2 ))
         ,""  
           
        , [ match_replace( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] 
                    , new="XX", want=2)
                 , "a+cos(XX)+sin(XX)"
                 , "'a+cos(2)+sin(5)', [['cos(','sin('],'[(',0_9,')]'], 'XX',want=2"
          , ["nl",true]
          ]      
          
        ]
        , mode=mode, opt=opt, scope=["fml",fml]
        )
     );
    
    // echo( match_replace_test( mode=112)[1] );

//s2="[[3, 4],[5, a, [ b,5]]]";
//
//echo(s2=s2);
//echo( match_replace(s2, [[",",1]," ","["],undef,"xx") ); //,["a",10,"b",11]) );                
                         
      
//=============================================================
// What is this ???
// 
//function match_pn(s
//    , ps           // hash of patterns: ["name1",p1,"name2",p2 ...]
//                   // Each p is: str, [str,n], or array of words
//    , want=undef
//    , _pnames= []
//    , _ps = []
//    , _i=0           
//    , _rtn=[]            
//    , _debug=[]
//    )=
//(
//   let( _pnames= keys( ps )
//      , _ps = vals( ps )
//      , m= match_ps_at(s,_i,_ps,want) //: [3,7,["cos","(","2",")"]] or false
//      ,newi= m? m[1]+1: _i+1
//      ,newrtn= m? app(_rtn,m):_rtn
////      , _debug= concat(_debug,
////         str(
////          [ "_i", _i
////          , "s[_i]", s[_i]
//////          , "newi", newi
//////          , "_rtn", _rtn
//////          , "newrtn", newrtn
////          ]
////         ))
//      )
//    _i<len(s)-len(ps)+1?  
//      match( s,ps,want, _i=newi, _rtn=newrtn, _debug=_debug )
//    //: join( _debug, "<br/>") 
//      : _rtn
//);

//=============================================================


//=============================================================
function match_ps_at( s
                    , i
                    , ps
                    , want // indices of matched parts wanted
                    , _i0
                    , _ms=""  // matched string, "cos(20)" 
                    , _ma=[]  // matched parts, ["cos","(","20",")"]
                    , _pi=0
                    , _debug=[])=
                    
(
   //echo("match_ps_at ==================")
   let( _i0= _i0==undef? i: _i0 // keep track of starting i
      , want= want==undef? [for(i=[0:len(ps)-1])i]    // Take all if undef
              :isint(want)?[want]:want // Make sure it's arr
    
      , _p = ps[_pi]                // _p could be: "abcd", ["abcd",2], ["abc","def]
      , p = len(_p)==2 && isint(_p[1])? // # of matches with this p
             _p : [_p,undef]            // undef= as many as possible
                                    // p could be: ["abcd",undef],["abcd",2],[["abc","def],undef]
      , _m = match_at( s, i, p=p[0], n=p[1] ) // _m:[i,j,word]
   )
   !_m? false       // quit as soon as any p fails
       //[ str("i=",i), str("_pi=",_pi), str("p=",p),  str("s[i]=",s[i]), str("_m=",_m)] 
   : let(   
        , m = _m? _m[2]: undef                  // m: word 
        , _ms = str(_ms, m)                 // matched string 
        , _ma = has(want, _pi)? concat( _ma, [m] ):_ma // matched array of string parts
        )
        _pi == len(ps)-1 ?           // all ps matched
          [_i0, _m[1] ,_ms, _ma ]
        : match_ps_at( s, _m[1], ps, want, _i0, _ms, _ma, _pi+1)         
);  

//(
//   let( _i0= _i0==undef? i: _i0 // keep track of starting i
//      , want= want==undef? [for(i=[0:len(ps)-1])i]    // Take all if undef
//              :isint(want)?[want]:want // Make sure it's arr
//    
//      , _p = ps[_pi]
//      , p = len(_p)==2 && isint(_p[1])? // # of matches with this p
//             _p : [_p,undef]            // undef= as many as possible
//      , _m = match_at( s, i, p=p[0], n=p[1] ) // _m:[i,j,word]
//      
//      , m = _m? _m[2]: undef                  // m: word 
//      , allm = m && _pi == len(ps)-1          // all ps matched
//        
//      , newi = m? _m[1]+1:undef
//      , _newpi= m && !allm ? _pi+1:undef  // next p only when m and !allm
//      , _ms = str(_ms, m)                 // matched string 
//      , _ma = has(want, _pi)? app( _ma, m ):_ma // matched array
////      , _debug= concat( _debug,
////           str( [ str("s[",i,"]"), s[i]
////           // , "ps", ps
////           // , "_pi", _pi
////           // , "ps[_pi]", ps[_pi]
////            , "p", p
////            , "m", m
////            , "mm", match_at( s, i, p=p[0], n=p[1] )
////            , "_rtn",_rtn
////            ])
////            )
//      )//let    
//     
//   !m? false // quit as soon as any p not match
//   :allm? [_i0, _m[1] ,_ms, _ma ]
//    : match_ps_at( s, newi, ps, want, _i0, _ms, _ma, _newpi)         
//);                       


    match_ps_at=[ "match_ps_at", "s,i,ps,want", "arr|false","Inspect",
    "Match multiple pattern ps to s starting from i. Return [i,j,m1m2..., [m1,m2...]] or false.
    ;; 
    ;; ps: a list of patterns. A pattern p is in one of the following forms:
    ;;   
    ;;     p= string 
    ;;         
    ;;          like: \"abcde\", a_z, 0_9, \"(\" ... 
    ;;          => match as many char as possible continuously
    ;;          For example, 0_9 will match: \"2315\", \"3\", \"99820\"  
    ;;
    ;;     p= [string,n]
    ;;
    ;;          where n decides the maxinum number of matches 
    ;;
    ;;          [\"abcde\",2] will match \"ab\", \"bc\", \"be\" ... 
    ;;
    ;;     p= [ string, string ...]
    ;;
    ;;          like: [\"(\",0_9,\")\"] matches \"(2)\", \"(512)\" ... 
    ;; 
    ;; All patterns in a ps need to be matched in order to return a match.  
    ;;    
    ;; Example: ps= [ [\"abcde\",2], [\"(\",0_9,\")\"] ] matches \"cd(4)\"
    ;;
    ;;
    ;; Return [i,j,m1m2...,[m1,m2...]] where i is the starting i, j is the
    ;; last i of matched, and each m corresponds to each px given (m1m2...
    ;; is the matched string, [m1,m2,...] is the matched-parts)
    ;; 
    ;; want: array of indices determining what match to report.
    ;; 
    ;; For example, given ps=[a_z,'(',0_9,')'], you could get: 
    ;;    [2, 7, 'cos(2)', ['cos', '(', '2', ')']]
    ;; 
    ;; If want=[0,2], get: [2, 7, 'cos(2)', ['cos', '2']]
    "
    ,"match_at"
    ];

    function match_ps_at_test( mode=MODE, opt=[] )=
    (
       let( fml = "a+cos(2)+sin(5)"
                // 0123456789 1234
           )
       doctest( match_ps_at,
       [
         "var fml"
        ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
        , ""
        , str("0_9 = \"", 0_9, "\"")
        , ""
        , [ match_ps_at(fml, 2, [a_z])
          , [2,5,"cos",["cos"]]
          , "'a+cos(2)+sin(5)',2,[a_z]"
          ]
          
        , [ match_ps_at(fml, 2, [[a_z,2]])
          , [2,4,"co",["co"]]
          , "'a+cos(2)+sin(5)',2,[[a_z,2]]", ["nl",false]
          ]

        , [ match_ps_at(fml, 3, [[a_z,2]])
          , [3,5,"os",["os"]]
          , "'a+cos(2)+sin(5)',3,[[a_z,2]]", ["nl",false]
          ]

        , [ match_ps_at(fml, 2, [["cos","sin"],"("])
          , [2,6,"cos(",["cos","("]]
          , "'a+cos(2)+sin(5)',2,[['cos','sin'],'(']", ["nl",false]
          ]

        , [ match_ps_at(fml, 9, [["cos","sin"],"("])
          , [9,13,"sin(",["sin","("]]
          , "'a+cos(2)+sin(5)',9,[['cos','sin'],'(']", ["nl",false]
          ]

        , [ match_ps_at("a+cos(300)", 2, [["sin","cos"],"(",0_9,")"])
          , [ 2,10,"cos(300)",["cos","(","300",")"]]
          , "'a+cos(300)',2,[['sin','cos'],'(',0_9,')']", ["nl",false]
          ]
          
        , [ match_ps_at("abcdefgh1", 4, [a_z,0_9])
          , [ 4,9,"efgh1",["efgh","1"]]
          , "'abcdefgh1',4,[a_z,0_9]", ["nl",false]
          ]
        , [ match_ps_at("abcdefgh123", 4, [a_z,0_9])
          , [ 4,11,"efgh123",["efgh","123"]]
          , "'abcdefgh123',4,[a_z,0_9]", ["nl",false]
          ]
        , [ match_ps_at("abcdefghi", 4, [a_z])
          , [ 4,9,"efghi",["efghi"]]
          , "'abcdefghi',4,[a_z]", ["nl",false]
          ]
          
        , [ match_ps_at("p=3.14159", 4, [0_9])
          , [ 4,9,"14159",["14159"]]
          , "'p=3.14159',4,[0_9]", ["nl",false]
          ]
        
       , [ match_ps_at("p=3.14159", 2, [0_9,".",0_9])
          , [ 2,9,"3.14159",["3",".","14159"]]
          , "'p=3.14159',2,[0_9,'.',0_9]", ["nl",false]
          ]
          
          
        ,""
        ,"<b>set n</b> for count of match when a pattern is a string"
        ,""
        , [ match_ps_at(fml, 2, [a_z,"(",0_9,")"])
          , [2, 8, "cos(2)", ["cos", "(", "2", ")"]]
          , "'a+cos(2)+sin(5)',2,[a_z,'(',0_9,')']", ["nl",false]
          ]
        
        , "", "// At i=2, want exactly 2 from a_z, followed by (...), but no such match:" 
        , [ match_ps_at(fml, 2, [[a_z,2],"(",0_9,")"])
          , false
          , "'a+cos(2)+sin(5)',2,[[a_z,2],'(',0_9,')']", ["nl",false]
          ]

        , "","// Move the starting i from 2 to 3 to locate the match:"
        , [ match_ps_at(fml, 3, [[a_z,2],"(",0_9,")"])
          , [3, 8, "os(2)", ["os", "(", "2", ")"]]
          , "'a+cos(2)+sin(5)',3,[[a_z,2],'(',0_9,')']", ["nl",false]
          ]
            
       , [ match_ps_at(fml, 3, [[a_z,2],"(",[0_9,1],")"])
         , [3, 8, "os(2)", ["os", "(", "2", ")"]]
         , "'a+cos(2)+sin(5)',3,[[a_z,2],'(',[0_9,1],')']", ["nl",false]
         ]
            
       , [ match_ps_at("a+cos(75)", 2, [a_z,"(",0_9,")"])
         , [2, 9, "cos(75)", ["cos", "(", "75", ")"]]
         , "'a+cos(75)',2,[a_z,'(',0_9,')']", ["nl",false]
         ]
            
      , "// The following requests a single digit inside (), like (2),(5), etc, using"
      , "// pattern, ['(',[0_9,1],')'], but it find no such match in the string:"      
      , [ match_ps_at("a+cos(75)", 2, [a_z,"(",[0_9,1],")"])
        , false
        , "'a+cos(75)',2,[a_z,'(',[0_9,1],')']", ["nl",false]
        ]
  

       ,""
       ,"<b>want=[...]</b>"
       ,""
       , [ match_ps_at(fml, 3, [[a_z,2],"(",0_9,")"],[0,2])
         , [3, 8, "os(2)", ["os", "2"]]
         , "'a+cos(2)+sin(5)',3,[[a_z,2],'(',0_9,')'], want=[0,2]", ["nl",false]
         ]

       , [ match_ps_at(fml, 3, [[a_z,2],"(",0_9,")"],2)
         , [3, 8, "os(2)", ["2"]]
         , "'a+cos(2)+sin(5)',3,[[a_z,2],'(',0_9,')'], want=2", ["nl",false]
         ]
        
       ,""
       ,"// New pattern: "
       ,"//  '..' match ANY as many as possible"
       ,"//  '..abc' match ANY as many as possible AND any of abc as many as possible"
       ,"//  ['..abc',2] match ANY as many as possible AND 2 from abc"
       ,"//  '--abc' match ANY EXCEPT abc as many as possible"
       ,"//  ['--abc', 3] match at most 3 of any EXCEPT abc"
       ,"//  ['--abc', [2,4]] match at least 2 and at most 4 of any EXCEPT abc"
       ,""
       
       , [ match_ps_at( "(2+(3+4))", 3, ["(","..)"])
         , [ 3,9,"(3+4))", ["(","3+4))"]]
         , "'(2+(3+4))', 3, ['(','..)']"
         ]

       , [ match_ps_at( "(2+(3+4))", 3, ["(",["..)",1]])
         , [ 3,8,"(3+4)", ["(","3+4)"]]
         , "'(2+(3+4))', 3, ['(',['..)',1]]"
         ]
         
       , [ match_ps_at( "(2+(3+4))", 3, ["(","--)",[")",1]])
         , [ 3,8,"(3+4)", ["(","3+4",")"]]
         , "'(2+(3+4))', 3, ['(','--)',[')',1]]"
         ]

       , [ match_ps_at( "(2+(3+4))", 0, ["(","--)",[")",1]])
         , [ 0,8,"(2+(3+4)", ["(","2+(3+4",")"]]
         , "'(2+(3+4))', 3, ['(','--)',[')',1]]"
         ]

       , [ match_ps_at( "(2+(3+4))", 0, ["(","--)",[")",2]])
         , [ 0,9,"(2+(3+4))", ["(","2+(3+4","))"]]
         , "'(2+(3+4))', 3, ['(','--)',[')',2]]"
         ]


       , "","// To extract a substring:"
       , [ match_ps_at( "a=\"xyz\"", 2, ["\"","--\"",["\"",1]])
         , [ 2,7,"\"xyz\"", ["\"","xyz","\""]]
         , "'a=\"xyz\"', 2, ['\"','--\"',['\"',1]]"
         ]

         
    //    , "", "// Nested blocks... can't do this yet "
    //    , ""  
    //    , [ match_ps_at( "a((c+(d+1))+1)", 2
    //             , ps=[["(",1],str(a_z,"+(",0_9),[")",1]] 
    //             )
    //             , [[2, 10, "(c+(d+1))", ["(","(d+1)",")"]]
    //               ]
    //             , "fml, ps=[['(',1],str(a_z,'+(',0_9),[')',1]]"
    //       , ["nl",true]
    //      ]
          
       ]//doctest2
       , mode=mode, opt=opt, scope=["fml",fml]
       )
    );

    //echo( match_ps_at_test(mode=112)[1] );
//=============================================================
/* 
   np: [ "name1",  ps=[p,p...], want  ] : a named pattern
                       each p could be p or [p,n]
   nps: [ np1,np2 ... ]
   
   return: ["func", 2, 4, ["cos"]] or false
   
   Note:
   
   match_ps_at(s,i,ps,want) checks if ps matches s[i+]
   match_nps_at(s,i,nps=[ ["name", ps, want]   checks WHICH ps matches s[i+]
                          , ["name", ps, want]
                          , ...] )//
*/
function match_nps_at( s, i, nps, _npi=0, _debug=[])=
(
   let( np = nps[_npi]           //_pn: ["name", [ [p,p..], n] ]
      , name= np[0]
      , ps =  np[1]
      , _want= np[2]
      //, ps = _ps //len(_ps)==2 && isint(_ps[1])? _ps : [_ps,undef] 
      , want= _want==-1 || !_want?[for(i=[0:len(ps)-1])i] //range(ps[0])
              :_want
      
      , m = match_ps_at( s, i, ps=ps, want=want ) // _m:[i,j,word]
      
//      , _debug= concat( _debug,
//           str( [// "nps",nps
//                 "_npi", _npi
//                , "np", np
//                , "ps", ps
//                , "name",name
//                , "want",want
//                , "m", m
//                ] )
//            )
      )    
//   debug:
//    _npi<len(nps)?
//     match_nps_at( s, i, nps, _npi=_npi+1, _debug=_debug ) 
//     : str("<br/>", join(_debug, "<br/><br/>"),"<br/>")
   m? concat( [name], m )
   : _npi< len( nps) ? match_nps_at( s, i, nps, _npi=_npi+1 ) 
   : false 
);       

    match_nps_at=[ "match_nps_at", "s,i,nps", "arr|false","Inspect",
    "Match any named ps in nps to s starting from i. Return [name, i,j,[m1,m2...]] or false.
    ;; 
    ;; nps: a list of named patterns: [np,np...]. A named pattern is:
    ;; 
    ;; np: [name, ps, want]
    ;; 
    ;; name: name of this pattern set;
    ;; ps  : list of patterns = [p,p,...]. All have to be matched in 
    ;;       order for this pattern set to be matched;
    ;;       Each p could be:
    ;;        -- a string, say, A_Z,a_z,0_9,'abcdef', etc, matching all chars in this
    ;;           string as many times as possible
    ;;        -- an array with [string, n], matching all chars in the string n times
    ;;        -- an array with [str1, str2 ...], matching ANY of the str only once
    ;; want: array of indices indicating which pattern (among ps)
    ;;       result to be returned. -1= all, [2]= the 3rd pattern
    ;; 
    ;; p   : A pattern p is in the form of px or [px,n] where px is 
    ;;       either a string containing all possible chars, or an 
    ;;       array containing all possible words. n is the count 
    ;;       chars to match for this p, works only when px is a string.
    ;;    
    ;; Return [name, i,j,m1m2..., [m1,m2...]] where i is the starting i, j is 
    ;; the last i of matched, and each m corresponds to each px given.
    "
    ,"match, match_at, match_ps_at"
    ];

    function match_nps_at_test( mode=MODE, opt=[] )=
    (
       let( fml = "a+cos(2)+sin(5)"
                // 0123456789 1234
          , nps1= [ ["num", [0_9],-1]
                  , ["word",[a_z],-1]
                  ]
          , nps2= [ ["num", [0_9],-1]
                  , ["word",[a_z],-1]
                  , ["op",["^*/+-",["cos","sin"]], -1] 
                  ]
          , nps3= [ ["num",[0_9],-1]
                  , ["op", ["^*/+-",["cos","sin"]],1]
                  ] 
          
           )
       doctest( match_nps_at,
       [
         "var fml"
         ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
         , "", "var nps1"
         ,""
        , [ match_nps_at(fml, 2, nps=nps1 )
          , ["word",2,5,"cos",["cos"] ]
          , "'a+cos(2)+sin(5)',2, nps=nps1"
          , ["nl",false]
          ]
        , ""
        , "// nps2 : match numbers, word or op+cos/sin."
        , "// return all (n=-1)"
        , "var nps2"
        , ""
        , [ match_nps_at(fml, 1, nps=nps2 )
          , ["op", 1, 5, "+cos", ["+", "cos"]]
          , "'a+cos(2)+sin(5)',1, nps=nps2"
          , ["nl",false]
          ]
        , ""
        , "// [str,n] to decide how many chars to match:"
        , "" 
        , [ match_nps_at("a+cos(52)", 3, nps= [["XX", [[a_z,2],"(",[0_9,1]]]] )
          , ["XX", 3, 7, "os(5", ["os", "(","5"]]
          , "'a+cos(52)',3, nps= [['XX', [[a_z,2],'(',[0_9,1]]]"
          , ["nl",false]
          ]
  
        , ""
        , "// nps3 : match either numbers or op+cos. If number,"
        , "// return all (n=-1). If op+cos, return item 1."
        , "var nps3"
        , ""
        , [ match_nps_at(fml, 1, nps=nps3 )
          , ["op", 1, 5, "+cos", ["cos"]]
          , "'a+cos(2)+sin(5)',1, nps=nps3"
          , ["nl",false]
          ]      
        , [ match_nps_at(fml, 8, nps=nps3 )
          , ["op", 8, 12, "+sin", ["sin"]]
          , "'a+cos(2)+sin(5)',8, nps=nps3"
          , ["nl",false]
          ]      
        , [ match_nps_at(fml, 7, nps=nps3 )
          , false
          , "'a+cos(2)+sin(5)',7, nps=nps3"
          , ["nl",false]
          ]      
          //------------------------
    //    , [ match_nps_at(fml, 2, [[a_z,2]]), [2,3,["co"]],"fml,2,[[a_z,2]]", ["nl",false]]
    //
    //    , [ match_nps_at(fml, 3, [[a_z,2]]), [3,4,["os"]],"fml,3,[[a_z,2]]", ["nl",false]]
    //
    //    ,""
    //    ,"<b>set n</b> for count of match when a pattern is a string"
    //    ,""
    //    , [ match_nps_at(fml, 2, [a_z,"(",0_9,")"]), [2, 7, ["cos", "(", "2", ")"]],"fml,2,[a_z,'(',0_9,')']", ["nl",false]]
    //    , [ match_nps_at(fml, 2, [[a_z,2],"(",0_9,")"]), false,"fml,2,[[a_z,2],'(',0_9,')']", ["nl",false]]
    //
    //   , [ match_nps_at(fml, 3, [[a_z,2],"(",0_9,")"]), [3, 7, ["os", "(", "2", ")"]],"fml,3,[[a_z,2],'(',0_9,')']", ["nl",false]]
    //
    //   ,""
    //   ,"<b>want=[...]</b>"
    //   ,""
    //   , [ match_nps_at(fml, 3, [[a_z,2],"(",0_9,")"],[0,2])
    //        , [3, 7, ["os", "2"]]
    //        , "fml,3,[[a_z,2],'(',0_9,')'], want=[0,2]", ["nl",false]]
    //
    //   , [ match_nps_at(fml, 3, [[a_z,2],"(",0_9,")"],2)
    //        , [3, 7, ["2"]]
    //        , "fml,3,[[a_z,2],'(',0_9,')'], want=2", ["nl",false]]
        
       ]
       , mode=mode, opt=opt, scope=["fml",fml, "nps1",nps1
                                   ,"nps2",nps2 
                                   ,"nps3",nps3 
                                   ]
       )
    );

    //echo( match_nps_at_test(mode=112)[1] );

//function get_scadx_match_test_results( title="scadx_match Doctests", mode=MODE, opt=["showmode",false] )=
//[
//     //match_test( mode, opt )
//   // match_at_test( mode, opt )
//    match_at_rev_test( mode, opt )
//   
//   //, match_nps_at_test( mode, opt )
//   //, match_ps_at_test( mode, opt )
//   //, match_replace_test( mode, opt )
//
////   , matchr_at_test( mode, opt )
////   //, match_pn_test( mode, opt ) <=== what is match_pn ?
////   //, matchblk_test( mode, opt )
//
//]; 

/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_match_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_match.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_match_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_match", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      match_test( mode, opt )
    , match_at_test( mode, opt )
    , match_at_rev_test( mode, opt )
    , match_nps_at_test( mode, opt )
    , match_ps_at_test( mode, opt )
    , match_replace_test( mode, opt )
    //, matchblk_test( mode, opt ) // to-be-coded
    , matchr_at_test( mode, opt )
    ]
);
