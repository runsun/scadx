/// include 
//echo("=== include scadx_init.scad ==="); include <scadx_init.scad>
//echo("=== include scadx_funclet.scad ==="); include <scadx_funclet.scad>
//echo("=== include scadx_num.scad ==="); include <scadx_num.scad>
//echo("=== include scadx_array.scad ==="); include <scadx_array.scad>
//echo("=== include scadx_range.scad ==="); include <scadx_range.scad>
//echo("=== include scadx_hash.scad ==="); include <scadx_hash.scad>
//echo("=== include scadx_inspect.scad ==="); include <scadx_inspect.scad>
//echo("=== include scadx_geometry.scad ==="); include <scadx_geometry.scad>
//echo("=== include scadx_string.scad ==="); include <scadx_string.scad>
//echo("=== include scadx_log.scad ==="); include <scadx_log.scad>
//echo("=== include scadx_eval.scad ==="); include <scadx_eval.scad>
//echo("=== include scadx_match.scad ==="); include <scadx_match.scad>
//echo("=== include scadx_error.scad ==="); include <scadx_error.scad>
//echo("=== include scadx_args.scad ==="); include <scadx_args.scad>
//echo("=== include scadx_args_old.scad ==="); include <dev/scadx_args_old.scad>
//echo("=== include scadx_animate.scad ==="); include <scadx_animate.scad>
include <scadx_init.scad>
include <scadx_funclet.scad>
include <scadx_num.scad>
include <scadx_array.scad>
include <scadx_range.scad>
include <scadx_hash.scad>
include <scadx_inspect.scad>
include <scadx_faces.scad>
include <scadx_geometry.scad>
include <scadx_rand.scad>
include <scadx_string.scad>
include <scadx_log.scad>
include <scadx_eval.scad>
include <scadx_match.scad>
include <scadx_error.scad>
include <scadx_args.scad>
include <dev/scadx_args_old.scad>
include <scadx_animate.scad>
include <scadx_subdiv.scad>
//
    
//. Main

//========================================================
function assureColor(x,df=["gold",1])= // 2018.2.4, given color_name, transparancy or both
                         // return [color, transp]
(
   //echo(str("assureColor(",x,".  df=", df, ")"))
   let( df= isstr(df)?[df,1]:df
      , rtn= isstr(x)? [x,1]
             :isarr(x)? x
             :isnum(x)? [df[0],x>1?1:x]
             :df
      )
   //echo(str("assureColor(",x,") => ", rtn))
   rtn    
);

function assurePair(x, default_2nd)=  // 2017.10.13
( 
   isarr(x)?x: [x, default_2nd]
);

//========================================================
function assureTriplet(x)=  // 2017.10.13
( 
   isnum(x)? [x,x,x]: x
);

//========================================================
//function det(pq)=
//
//	dot( pq[0], pq[0] ) * dot( pq[1],pq[1]) - pow( dot(pq[0],pq[1]),2)
//;
function det(pqr, dim=3)=
(
  // pqr= [ [Px,Py,Pz], [Qx,Qy,Qz], [Rx,Ry,Rz] ]
  /*
     = | Px, Py, Pz |
       | Qx, Qy, Qz |
       | Rx, Ry, Rz |
       
     determinent    
     
     = Px(QyRz-QzRy) - Py(QxRz-QzRx) + Pz(QxRy-QyRx)
  */ 
  dim==2 || len(pqr)==2 || len(pqr[0])==2?
  let(P=pqr[0], Q=pqr[1]) P.x*Q.y-Q.x*P.y
  :let(P=pqr[0], Q=pqr[1], R=pqr[2])
  P.x*(Q.y*R.z-Q.z*R.y) - P.y*(Q.x*R.z-Q.z*R.x) + P.z*(Q.x*R.y-Q.y*R.x)
);

    det=["det","pqr","array","Array,Math",
    " Given a pqr, return the determinant
    ;; 
    ;; If dim=2, or pqr= [P,Q], or pqr[0]=[x,y], then do 
    ;; a 2x2 determinent
    "
    ];

    function det_test(mode=MODE, opt=[] )=
    (
        doctest( det, 
        [
          [det( [[1,-5,2],[7,3,4],[2,1,5]])
          , 148, [[1,-5,2],[7,3,4],[2,1,5]]
          ]
        , [det( [[2,3],[1,-2]])
          , -7, "[[2,3],[1,-2]"
          ]
        ]
        , mode=mode, opt=opt )
    );

//========================================================
function dot(P,Q)= 
    let( pq= Q==undef? P:[P,Q] )
(   pq[0]*pq[1]
);
    dot=["dot", "P,Q", "num", "Geometry, Math"
    , "dot product of P,Q, same as P*Q . P*Q is generally easier than dot(P,Q). 
    ;; But if P and Q are complex, then dot() might be easier. For example
    ;; dot(P-Q,R-S) * dot(P-R,Q-S)  would be easier and more descriptive than 
    ;; ((P-Q)*(R-S))*((P-R)*(Q-S))
    ;;
    ;;   P= [2,3,4];  Q= [-1,0,2]; 
    ;;   dot( P,Q )= 6
    ;;   dot( [P,Q] )= 6
    ;;
    "];

    function dot_test(mode=MODE, opt=[] )=
    ( 
        let( P = [2,3,4]
           , Q = [-1,0,2]
           )
        
        doctest( dot,
        [
          "var P", "var Q", str( "P*Q = ", P*Q )
        , ""
        , [ dot(P,Q), 6,"P,Q"]
        , [ dot([P,Q]), 6,"[P,Q]" ]
        ]
        , mode=mode, opt=opt, scope=["P",P,"Q",Q] 
        )
    );
//========================================================
function fac(n=5)= n==0?1:n*(n>2?fac(n-1):1);

    fac=[ "fac", "n", "int",  "Math",
    "
     Given n, return the factorial of n (= 1*2*...*n) 
    "];

    function fac_test( mode=MODE, opt=[] )=(//====================
        
        doctest( fac,
        [ 
          [ fac(5), 120, 5 ]
        , [ fac(6), 720, 6 ]
        , [ fac(0), 1, 0]
        ], mode=mode, opt=opt
        )
    );

//========================================================
function ftin2in(ftin)=
(
	isnum(ftin)
	? ftin*12
	: isarr(ftin)
	  ? ftin[0]*12+ftin[1]
	  : endwith(ftin,"'")
		? num( replace(ftin,"'",""))*12

	    : index(ftin,"'")>0
		   ? num( split(ftin,"'")[0] )*12 
			+ num( replace(split(ftin,"'")[1],"\"",""))
		   : num( replace(ftin,"\"",""))
);

    ftin2in=["ftin2in","ftin","number","Unit, len",
    "Given a ftin ( could be a string like 3`6``, or an array like [3,6], 
    ;; or a number 3.5, all for 3 feet 6 in ), convert to inches.
    ;;
    ;;   ftin2in( 3.5 )= 42
    ;;   ftin2in( [3,6] )= 42
    ;;   ftin2in( 3`6`` )= 42
    ;;   ftin2in( 8` )= 96
    ;;   ftin2in( 6`` )= 6
    ;;
    "];

    function ftin2in_test( mode=MODE, opt=[] )=(//====================
        doctest( ftin2in,
        [
          [ ftin2in(3.5), 42, "3.5",  ]
         ,[ ftin2in([3,6]), 42, "[3,6]",  ]
        , [ ftin2in("3'6\""), 42, "3`6'" ]
        , [ ftin2in("8'"), 96, "8'" ]
        , [ ftin2in("6\""), 6, "6'" ]
        ], mode=mode, opt=opt
        )
    );

//========================================================
function func2d( funcname="line"
               , opt
               
               , xbeg, xend
               , xpts
               
               , ybeg, yend
               
               , n
               )=
(  
   /*
      n is the count of resulting pts, including beg and end pts.
      default = 6. 
      
      x boundaries are decided by either: (xbeg,xend) or xpts
      If xpts is given, use the accumulated seglens of xpts 
      as xs. That means that the intervals between x's might
      be varying. See usage in chainData_demo_r():
      
      rs= func2d("line", ybeg=r0, yend=rlast, pts=bone3);     



   */
   let( opt = popt(opt)
   
      , n   = or( hash( opt, "n", 6))      
 
      , xpts= und(xpts, hash( opt, "xpts" ))
      , xbeg= und(xbeg, hash( opt, "xbeg", 0))
      , xend= und(xend, hash( opt, "xend", 5)) 
      , xs  = xpts? concat( [0], accum( [ for(i=[0:len(xpts)-2]) 
                                          dist( xpts[i], xpts[i+1])] )  
                          )
                  : let( d = (xend-xbeg)/(n-1))
                    range(xbeg, d, xend+d)
 
      , ybeg= und(ybeg, hash( opt, "ybeg", 0))
      , yend= und(yend, hash( opt, "yend", 1))        
   )
      
   funcname=="line"?
      let( slope = (yend-ybeg)/(last(xs)-xs[0]) )
      [ for(x=xs) x*slope+ybeg ]
   : undef       
          
  
);
//========================================================
//function func2d( funcname="line"
//               , opt
//               
//               , xbeg, xend
//               , xpts
//               
//               , ybeg, yend
//               , pq
//               , baseline
//               
//               , n
//               )=
//(  
//   /*
//      n is the count of resulting pts, including beg and end pts.
//      default = 6. 
//      
//      x boundaries are decided by either: (xbeg,xend) or xpts
//      If xpts is given, use the accumulated seglens of xpts 
//      as xs. That means that the intervals between x's might
//      be varying. See usage in chainData_demo_r():
//      
//      rs= func2d("line", ybeg=r0, yend=rlast, pts=bone3);
//      
//      y boundaries are decided by either: (ybeg,yend) or (pq, baseline)
//      where both ybeg and yend are numbers, and both pq and baseline
//      are 2-pointers. 
//      
//      If (pq,baseline), the projections of P,Q onto baseline
//      will be the ybeg and yend, resp. The resulting pts will 
//      be going from P to Q.
//
//   */
//   let( opt = popt(opt)
//   
//      , n   = or( hash( opt, "n", 6))      
// 
//      , xpts= und(xpts, hash( opt, "xpts" ))
//      , xbeg= und(xbeg, hash( opt, "xbeg", 0))
//      , xend= und(xend, hash( opt, "xend", pq?dist(pq):5)) 
//      , xs  = xpts? concat( [0], accum( [ for(i=[0:len(xpts)-2]) 
//                                          dist( xpts[i], xpts[i+1])] )  
//                          )
//                  : let( d = (xend-xbeg)/(n-1))
//                    range(xbeg, d, xend+d) 
//      
//      , pq  = und( pq, hash( opt, "pq" ) )
//      , baseline = und( baseline, hash( opt, "baseline", pq ) )
//      , usepq = isarr(pq) && isarr(baseline)
//      , ybeg= usepq? dist( pq[0], projPt(pq[0],baseline))
//                   : und(ybeg, hash( opt, "ybeg", 0))
//      , yend= usepq? dist( pq[1], projPt(pq[1],baseline))
//                   : und(yend, hash( opt, "yend", 1))  
//      
//   )
//      
//   funcname=="line"?
//      let( slope = (yend-ybeg)/(last(xs)-xs[0]) )
//      [xs, [ for(x=xs) x*slope+ybeg ] ]
//   : undef       
//          
//  
//);
//xi=10;
//xj=5;
//n=8;
//d= (xj-xi)/(n-1);
//rng = range(xi, d, xj+d);
//echo( rng=rng );
//echo( func2d(ybeg=2,yend=5));
      
     
//========================================================
function ifthen(a,b,c)= a==b?c:a;

    ifthen=["ifthen", "a,b,c", "x", "Core",
    " Same as a==b?c:a, but this allows that a be presented only once,
    ;; save repeated calculation in some cases.
    "
    ,"or, und"
    ];

    function ifthen_test( mode=MODE, opt=[] )=
    (
        doctest( ifthen,
        [
          [ ifthen(3,3,5), 5, "3,3,5"]
        , [ ifthen(3,4,5), 3, "3,4,5"]
        ],mode=mode, opt=opt )
    );
  
  
//========================================================
function getColorArg(color,opt, df=["gold",1])=
(
  // Assure color is set to array [color,transparancy]
  // 
  // getColorArg("red")  ==> ["red", 1]
  // getColorArg
  assurePair( color!=undef?color:hash(opt, "color", df),1)
);  
    getColorArg=["getColorArg", "color,opt, df=['gold',1]", "array", "Color",
    " Assure color is set to array [color,transparancy].
    ;;
    ;; getColorArg('red')  ==> ['red', 1]
    ;; getColorArg(['red',0.5])  ==> ['red', 0.5]
    ;; getColorArg( undef,['color','red']  )  ==> ['red', 1]
    ;; getColorArg( undef,['color',['red',0.5]]  )  ==> ['red', 0.5]
    "];

    function getColorArg_test( mode=MODE, opt=[] )=
    (
        doctest( getColorArg,
        [
        ]
        , mode=mode, opt=opt
        )
    );
    

//========================================================
function mod(a,b) = sign(a)*(abs(a) - floor(abs(a)/abs(b))*abs(b) );

    mod=["mod", "a,b", "number", "Number, Math",
    " Given two number, a,b, return the remaining part of division a/b.
    ;;
    ;; Note: OpenScad already has an operator for this, it is the percentage
    ;; symbol. This is here because this code file might be parsed by python
    ;; that reads the percentage symbol as a string formatting site and causes
    ;; errors.
    "];

    function mod_test( mode=MODE, opt=[] )=
    (
        doctest( mod,
        [
          [ mod(5,3), 2, "5,3"]
        , [ mod(-5,3), -2 , "-5,3"]
        , [ mod(5,-3), 2, "5,-3"]
        , [ mod(7,3),1, "7,3"]
        , [ mod(-7,3), -1 , "-7,3"]
        , [ mod(3,3), 0 , "3,3"]
        , [ mod(3.8,1), 0.8 , "3.8,1"]
        , [ mod(38,1), 0 , "38,1"]
        ]
        , mode=mode, opt=opt
        )
    );

//========================================================
function or(x,dval)=x?x:dval;

    or=["or","x,dval","any|dval",  "Core",
    "
     Given x,dval, if x is evaluated as true, return x. Else return
    ;; dval. Same as x?x:dval but x only appears once.
    "  
    ];
     
    function or_test( mode=MODE, opt=[] )=
    (
        doctest( or, 
        [ [ or(false,3), 3, "false, 3"]
        , [ or(undef,3), 3, "undef, 3"]
        , [ or(0    ,3), 3, "0,     3"]
        , [ or(50   ,3), 50, "50,    3"]
        ], mode=mode, opt=opt
        )
    );
    //echo( or_test()[1] );	

//========================================================

    //echo( permute_test()[1] );


//function permute(arr, _rtn=[], _i=-2)=
//(   //This one works , but it's not tail recursion (take 41 sec for len(arr)=5):            
//    len(arr)==2? [ arr, [arr[1],arr[0]]]
//    : joinarr( 
//       [ for( i = [0:len(arr)-1] )
//            let( pleft = pick( arr, i )
//               , p = pleft[0]
//               , ar= pleft[1]
//               , ar2= permute( ar )
//               )
//            [ for(x = ar2) concat([p],x) ]
//       ] 
//     )
//);

// Not quite get there yet:            
//function permute(arr,_i=0)=
//(
//   _i>len(arr)-1? arr
//   : [ for(j=[_i:len(arr)-1])
//       let(
//            rtn = switch( arr,j)
//          )
//       permute( rtn, _i=_i+1)
//    ] 
//);            

//========================================================
function pick(o,i) = 
(
  	concat( [get(o,i)]
		  , isarr(o)
			? [ del(o,i, byindex=true) ] 
			: [[ slice(o,0,i), slice(o,i+1) ]]
		  )
  
);
    pick=["pick", "o,i", "pair", "Array",  
    "
     Given an array or a string (o) and index (i), return a two-member array :
    ;;
    ;; [ o[i], the rest of o ] // when o is array
    ;; [ o[i], [ pre_i string, post_i_string] ] //when o is a string
    ;;
    ;; Note that index could be negative.
    "];

    function pick_test( mode=MODE, opt=[] )=
    (
        let( arr = range(1,5)
           , arr2= [ [-1,-2], [2,3],[5,7], [9,10]] 
           )
        doctest( pick
        ,[
            "//Array:"
            ,[ pick(arr, 2), [ 3, [1,2,4] ], "arr,2" ]
            ,[ pick(arr,-3), [ 2, [1,3,4] ], "arr,-3" ]
            ,[ pick(arr2,2), [ [5,7], [ [-1,-2], [2,3], [9,10]] ], "arr2,2" ]
            ,"//String:"
            ,[ pick("abcdef",3), ["d", ["abc", "ef"]], "'abcdef',3" ]
            ,[ pick("this is",2), [ "i", ["th", "s is"] ], "'this is',2" ]
            ,"//Note:"
            ,str("split('this is','i') = ", split("this is","i") )
        ]
        ,mode=mode, opt=opt, scope=["arr", arr, "arr2", arr2]
        )
    );

//========================================================
function prod(arr)=
   arr? arr[0]* prod(slice(arr,1)) :1;
   
    prod=["prod", "arr","n","Math",
    " prod( [n1,n2,n3...] ) = n1*n2*n3*...
    "];
    function prod_test( mode=MODE, opt=[])=
    (
       doctest( prod,
      [
        [ prod([1,2,3]), 6, [1,2,3] ]
       ,[ prod(range(1,0.5,3)),7.5, "range(1,0.5,3)" ]
      ], mode=mode, opt=opt )
    );   
       

//========================================================
function roundto(n,d=2)=
(
   isarr(n)?[ for (x=n) roundto(x,d) ]
   : round(n/pow(10,-d))*pow(10,-d)
); 

    roundto=["roundto", "x,d=2", "x", "Core",
    " Round number n to the desired decimal.
    ;;
    ;; n could be a num, array of n, or even nested array of n.
    ",
    ""
    ];

    function roundto_test( mode=MODE, opt=[] )=
    (
        doctest( roundto,
        [
          str(PI)
        , [ roundto(PI, 4), 3.1416, "PI,4"]
        , [ roundto([PI,PI], 3), [3.142,3.142], "[PI,PI],3", ["asstr",true]]
        , [ roundto([PI,[PI,PI]], 2), [3.14,[3.14,3.14]], "[PI,[PI,PI]],2", ["asstr",true]]
        , [ roundto(12345, 2), 12345, "12345,2"] //, ["asstr",true]]
        , [ roundto(12345, -2), 12300, "12345,-2"] //, ["asstr",true]]
        ],mode=mode, opt=opt )
    );
//========================================================

function solve2(ab,c)=  // ab is a 2x2 array, c is 2x1
(
   /*
      a0x + b0y = c0
      a1x + b1y = c1
      
      abs = [ [a0,b0], [a1,b1] ]
      c =   [c0,c1 ]
   */
   
   let( D= det(ab)
      , a= [ ab[0][0], ab[1][0] ]
      , b= [ ab[0][1], ab[1][1] ]
      , x= det( [ [c[0],b[0]], [c[1],b[1]] ] )
      , y= det( [ [a[0],c[0]], [a[1],c[1]] ] )
      )
   //echo(D=D, a=a, b=b)   
   [x,y]/D
);
    solve2=["solve2", "ab,c", "x", "Core",
    " Solving a 2-variable equations with given ab and c where
    ;;
    ;;   ab= [ [a0,b0], [a1,b1] ]
    ;;   c = [ c0,c1 ]
    ;;
    ;; for
    ;;
    ;;   a0x + b0y = c0
    ;;   a1x + b1y = c1
    ;; 
    ;; return [ x, y ]
    ;;
    ;;   | a0 b0 | | x |   | c0 |
    ;;   |       | |   | = |    |
    ;;   | a1 b1 | | y |   | c1 |
    ;;
    ;;          | P0 P1 |       | P0 P1 |      | P0 P1 |
    ;;  [a,b] * |       | =  a* |       | + b* |       |
    ;;          | Q0 Q1 |       | Q0 Q1 |      | Q0 Q1 |
    ;; 
    ;; This is useful for 2D coordinate transition studied in
    ;; dev_189p_vectors.scad:
    ;;
    ;; For the most commonly used Cartesian coordinate in 2D (CC2), 
    ;; unit vectors along the dir of x,y are [1,0] and [0,1],
    ;; respectively.
    ;;
    ;; Any P,Q on a CC2 can serve as unit vectors for a new coordinate (NC),
    ;; in which a point [a,b] can be converted back to CC2 with:
    ;; 
    ;;   aP + bQ = S
    ;;
    ;;   a* [p0,p1] + b* [q0,q1] = [s0,s1]
    ;;
    ;;   ap0 + bq0 = s0
    ;;   ap1 + bq1 = s1
    ;;
    ;;   [ p0 q0 ] [ a ]   [ s0 ]
    ;;   [       ] [   ] = [    ]
    ;;   [ p1 q1 ] [ b ]   [ s1 ]
    ;;
    ;; 
    
    
    ;; If we already have a pt S on CC2, how can it be converted to NC? We
    ;; need to calc [a,b] w/ known P,Q and S:
    ;;
    ;;  [a,b]*[P,Q] = a*[P,Q] + b*[P,Q] = [(a+b)*P, (a+b)*Q]  
    ;;
    ;;          | P0 P1 |       | P0 P1 |      | P0 P1 |
    ;;  [a,b] * |       | =  a* |       | + b* |       |
    ;;          | Q0 Q1 |       | Q0 Q1 |      | Q0 Q1 |
    ;;
    ;;
    ;;  [a,b] = solve2( [P,Q], S )
    ;;
    ;; The coordinate transition is a way of distorting an obj.
    ;; 
    ;; To get S, S=[P,Q]*[a,b]
    ",
    ""
    ];

    function solve2_test( mode=MODE, opt=[] )=
    (
        //
        echo(solve2__ = [1,2]*[3,4] ) //  [1,2] * [ 3 ]
                                      //          [   ]
                                      //          [ 4 ]
                                      // 1*3 + 2*4 = 11
                                      
        echo(solve2__ = [3,4]*[1,2] ) //  [3,4] * [ 1 ]
                                      //          [   ]
                                      //          [ 2 ]
                                      // 1*3 + 2*4 = 11
                                      
        echo(solve2__ = [1,2]*[ [3,4],[5,6] ] ) 
                                      //  [1,2] * [ [3,4] ]
                                      //          [       ]
                                      //          [ [5,6] ]
                                      // 1*[3,4] + 2 *[5,6] = [13,16]
                                      
        echo(solve2__ = [ [3,4],[5,6] ]* [1,2] ) 
                                      // [ [3,4 ] ]   [ 1 ]
                                      // [        ] * [   ]
                                      // [ [5,6 ] ]   [ 2 ]
                                      // [ 1*3+4*2, 1*5+2*6 ] = [ 11, 17 ]
       
        //echo(solve2__ = [3,-1]*[[2,3],[1,-2]] ) // [5,11]
        //echo(solve2__ = 3*[2,3]+-1*[1,-2] )     // [5,11]
        
        //echo(solve2__ = [[2,3],[1,-2]]*[3,-1] ) // [3,5]= [a,b] in aP+bQ
        //echo(solve2__ = [[3,-1]*[2,3], [3,-1]*[1,-2]] ) // [3,5]
        
        echo(solve2__ = [3,-1] * [[2,3],[1,-2]] ) 
                                      //  [3,-1]* [ [2,3] ]
                                      //          [       ]
                                      //          [ [1,-2] ]
                                      // 3*[2,3] + -1 *[1,-2] = [5,11]
        

        doctest( solve2,
        [
          [ solve2( [[2,3],[1,-2]], [3,5]), [3,-1], "[[2,3],[1,-2]], [3,5]" ]
        ],mode=mode, opt=opt )
    );



function solve3(abc,d)=  // abc is a 3x3 array
(
   /*
      a0x + b0y + c0z = d0
      a1x + b1y + c1z = d1
      a2x + b2y + c2z = d2
      
                | a0 b0 c0 |   | d0 | 
      [x,y,z] * | a1 b1 c1 | = | d1 |
                | a2 b2 c2 |   | d2 |
      
      abc = [ [a0,b0,c0], [a1,b1,c1], [a2,b2,c2] ]
          = [ [ a[0], b[0], c[0] ]
            , [ a[1], b[1], c[1] ]
            , [ a[2], b[2], c[2] ]
            ]  
      d =   [ d0,d1,d2 ]
   */
   
   let( D= det(abc)
      , a= [ abc[0][0], abc[1][0], abc[2][0] ]
      , b= [ abc[0][1], abc[1][1], abc[2][1] ]
      , c= [ abc[0][2], abc[1][2], abc[2][2] ]
      , x= det( [ [ d[0], b[0], c[0] ]
                , [ d[1], b[1], c[1] ]
                , [ d[2], b[2], c[2] ]
                ] )
      , y= det( [ [ a[0], d[0], c[0] ]
                , [ a[1], d[1], c[1] ]
                , [ a[2], d[2], c[2] ]
                ] )
      , z= det( [ [ a[0], b[0], d[0] ]
                , [ a[1], b[1], d[1] ]
                , [ a[2], b[2], d[2] ]
                ] )
      )   //echo(D=D, a=a, b=b)  
   //echo(" In solve3: ", abc=abc, a=a,b=b,c=c)    
   //echo(" In solve3: ", abc=abc, d=d, x=x,y=y,z=z)    
   [x,y,z]/D);

    solve3=["solve3", "abc,d", "x", "Core",
    " Solving a 3-variable equations with given abc and d where
    ;;
    ;;   abc = [ [a0,b0,c0], [a1,b1,c1], [a2,b2,c2] ]
    ;;   d =   [ d0,d1,d2 ]
    ;;
    ;; for
    ;;
    ;;   a0x + b0y + c0z = d0
    ;;   a1x + b1y + c1z = d1
    ;;   a1x + b1y + c1z = d2
    ;;
    ;; This is useful for 3D coordinate transition studied in
    ;; dev_189p_vectors.scad:
    ;;
    ;; For the most commonly used Cartesian coordinate in 3D (CC3), 
    ;; unit vectors along the dir of x,y,z are [1,0,0], [0,1,0] and [0,0,1],
    ;; respectively.
    ;;
    ;; Any P,Q,R on a CC3 can serve as unit vectors for a new coordinate (NC),
    ;; in which a point [a,b,c] can be converted back to CC3 with:
    ;; 
    ;;   aP + bQ + cR= S
    ;;
    ;; If we already have a pt S on CC3, how can it be converted to NC? We
    ;; need to calc [a,b,c] w/ known P,Q,R and S:
    ;;
    ;;  [a,b,c] = solve3( [P,Q,R], S )
    ;;
    ;; The coordinate transition is a way of distorting an obj.
    ;; ------------------------------------------------------------
    ;; 2018.11.13 -- From demo_3variables_eq() in dev_189p_vectors.scad:
    ;; 
    ;; Transfer a pt [i,j,k] to a PQR-coord system, D:
    ;;   D = {P,Q,R}
    ;;   ijk = [i,j,k]_D
    ;;
    ;;   iP + jQ + kR = [i,j,k] * [P,Q,R] = S
    ;;
    ;; Transfer a pt S from a PQR-coord system to Cartesien:
    ;;
    ;;   ijk = [i,j,k] = solve3( transpose([P,Q,R]),S)
    ;; ------------------------------------------------------------
    
    ",
    ""
    ];

    function solve3_test( mode=MODE, opt=[] )=
    (
        doctest( solve3,
        [
          [ solve3( [[1,1,1],[-2,-1,1],[1,-2,-1]], [-6,-2,1])
                  , [-2,1,-5], "[[1,1,1],[-2,-1,1],[1,-2,-1]], [-6,-2,1]" 
          ]
        , [ solve3( [[0.7, 2.91, 1.01], [-0.9, 1.77, 1.8], [-0.08, -0.15, -0.35]]
                  ,  [-0.252, 1.23, 0.782]
                  )
                  , [0.2,0.4,0.4]
                  , "[[0.7, 2.91, 1.01], [-0.9, 1.77, 1.8], [-0.08, -0.15, -0.35]], [-0.252, 1.23, 0.782]"
                  , ["nl",1]
          ]       
        , str([-2,1,-5]*[[1,1,1],[-2,-1,1],[1,-2,-1]])  
        , str([-2.96133, 1.37009, -2.14459]* [[0.7, 2.91, 1.01], [-0.9, 1.77, 1.8], [-0.08, -0.15, -0.35]])
           
        ],mode=mode, opt=opt )
    );



function und(x,df)= x==undef?df:x; 

    und=["und", "x,df", "x", "Core",
    " Return df if x = undef. Else return x.
    ",
    "und,und_opt,und_update,und_concat"
    ];

    function und_test( mode=MODE, opt=[] )=
    (
        doctest( und,
        [
          [ und(undef, 3), 3, "undef, 3"]
        , [ und(0, 3), 0, "0, 3"]
        , [ und(-1, 3), -1, "-1, 3"]
        , [ und("x", 3), "x", "'x', 3"]
        ],mode=mode, opt=opt )
    );
    
//========================================================
function und_opt(x, opt, varname, df)= 
(
  x==undef?hash(opt,varname,df):x
); 

    und_opt=["und_opt", "x,opt,varname,df", "x", "Core",
    
    " If x is undef, return hash(opt,varname, df). Else return x.
    ;; This is especially useful in merging args with opt args:
    ;; 
    ;;   func( a, b, opt)=
    ;;      let( a = und_opt( a, opt, 'a', df ) )
    ;;      ...
    ;; 
    ;;   where opt could have ['a', v1, 'b', v2]
    ;;  
    ;; 2018.1.7.
    ",
    "und,und_opt,und_update,und_concat"
    ];

    function und_opt_test( mode=MODE, opt=[] )=
    (
        doctest( und_opt,
        [
//          [ und(undef, 3), 3, "undef, 3"]
//        , [ und(0, 3), 0, "0, 3"]
//        , [ und(-1, 3), -1, "-1, 3"]
//        , [ und("x", 3), "x", "'x', 3"]
        ],mode=mode, opt=opt )
    );
    
function und_opt_update(x, opt, varname, df)= 
( 
  let( optdata = hash(opt,varname,df) )
  x==undef?optdata: update(isarr(optdata)?optdata:[], x) 
);
    und_opt_update=["und_opt_update", "x,opt,varname,df", "x", "Core",
    
    " If x is undef, return hash(opt,varname, df). Else return 
    ;; hash(opt,varname,df) updated with x.
    ;;
    ;; This is especially useful in merging args with opt args:
    ;; 
    ;;   func( a, b, opt)=
    ;;      let( a = und_opt( a, opt, 'a', df ) )
    ;;      ...
    ;; 
    ;;   where opt could have ['a', ['r',1,'h',h] , 'b', v2]
    ;;  
    ;; 2018.1.19.
    ",
    "und,und_opt,und_update,und_concat"
    ];

    function und_opt_update_test( mode=MODE, opt=[] )=
    (
        doctest( und_opt_update,
        [
//          [ und(undef, 3), 3, "undef, 3"]
//        , [ und(0, 3), 0, "0, 3"]
//        , [ und(-1, 3), -1, "-1, 3"]
//        , [ und("x", 3), "x", "'x', 3"]
        ],mode=mode, opt=opt )
    );
    
function und_opt_concat(x, opt, varname, df)= 
( 
  let( optdata = hash(opt,varname,df) )
  x==undef?optdata: concat(isarr(optdata)?optdata:[], x) 
);
    und_opt_concat=["und_opt_concat", "x,opt,varname,df", "x", "Core",
    
    " If x is undef, return hash(opt,varname, df). Else return 
    ;; concat( hash(opt,varname,df), x ).
    ;;
    ;; This is especially useful in merging args with opt args:
    ;; 
    ;;   func( a, b, opt)=
    ;;      let( a = und_opt( a, opt, 'a', df ) )
    ;;      ...
    ;; 
    ;;   where opt could have ['a', ['r',1,'h',h] , 'b', v2]
    ;;  
    ;; 2018.1.19.
    ",
    "und,und_opt,und_update,und_concat"
    ];

    function und_opt_concat_test( mode=MODE, opt=[] )=
    (
        doctest( und_opt_concat,
        [
//          [ und(undef, 3), 3, "undef, 3"]
//        , [ und(0, 3), 0, "0, 3"]
//        , [ und(-1, 3), -1, "-1, 3"]
//        , [ und("x", 3), "x", "'x', 3"]
        ],mode=mode, opt=opt )
    );
    

//echo( sum([ [1,2,3],[4,5,6]]));      

//========================================================
//function uopt(opt1,opt2, df=[])=
//(  /*
//     Allow a module to mix sopt and opt:
//     
//       MarkPts( pts, "l,c|color=red", ["grid","true"] );
//       MarkPts( pts, ["color","red"], "l,c|color=red" );
//       
//       df=["l","label","c","chain"]
//   */
//   let( _1 = isstr(opt1)? sopt(opt1,df):opt1?opt1:[]
//      , _2 = isstr(opt2)? sopt(opt2,df):opt2?opt2:[]
//      , intra_sopt1 = hash(_1, "sopt","")
//      , intra_sopt2 = hash(_2, "sopt","")
//      , sopt_from_opt1= intra_sopt1? sopt( intra_sopt1, df ):[]
//      , sopt_from_opt2= intra_sopt2? sopt( intra_sopt2, df ):[]
//      )
//   updates( [ intra_sopt1?delkey(_1,"sopt"):_1
//            ,  sopt_from_opt1
//            , intra_sopt1?delkey(_2,"sopt"):_2
//            , sopt_from_opt2 ] )
//);






//ERRMSG = [
//  "rangeError"
//    , "<br/> => Index {iname}(={got}) out of range of {vname} (len={len})"
// ,"typeError"
//    , "<br/> => {vname}'s type {rel} {want}{unwant}. A {got} is given (value={val}) "
// ,"arrItemTypeError"
//    , "<br/> => Array {vname} {rel} item of type {want}{unwant}. ({vname}: {val})"
//,"gTypeError"
//    , "<br/> => {vname}'s gtype {rel} {want}{unwant}. A {got} is given (value={val}) "
//,"lenError"    
//    , "<br/> => {vname}'s len {rel} {want}{unwant}. A {got} is given (value={val}) "
//];   
               


////======================================
//vlinePt=["vlinePt", "pqr,len=1", "points", "Point",
// " Given a 3-pointer pqr and a len(default 1), return a point T such that
//;; 
//;; (1) TQ is perpendicular to PQ 
//;; (2) T is on the plane of PQR
//;; (3) T is on the R side of PQ line
//;; 
//;;           T   R
//;;        len|  / 
//;;        ---| /  
//;;        |  |/
//;;   P-------Q
//"];
//
//module vlinePt_test( ops=["mode",13] )
//{
//	doctest( vlinePt, ops=ops );
//}
//
//function vlinePt(pqr,len=1)=
// let(Q=Q(pqr)
//	,N=N(pqr)
//	,T=N([N,Q,P(pqr)])
//	)
//(
//	onlinePt( [Q,T], len=len )
//);
//
//module vlinePt_demo_0()
//{_mdoc("vlinePt_demo_0",
//"");
//
//	pqr= randPts(3);
//	P=P(pqr); Q= Q(pqr); R=R(pqr);
//
//
//	T = vlinePt(pqr);
//	echo("T=",T);
//	//Line0( [Q,T] );
//	pqt = [P,Q,T];
//	Mark90( pqt, ["r",0.05, "color","red"] );
//	Plane( pqt, ["mode",3] );
//	MarkPts( [P,Q,R,T], ["labels","PQRT"] );
//	Chain(pqt, ["r",0.05, "transp",1,"color","red"]); 
//	Chain(pqr, ["r",0.15, "transp",0.5]); 
//	
//}
////vlinePt_demo_0();

/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_core_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_core.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_core_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_core", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      //assureColor_test( mode, opt ) // to-be-coded
    //, assurePair_test( mode, opt ) // to-be-coded
    //, assureTriplet_test( mode, opt ) // to-be-coded
     det_test( mode, opt )
    , dot_test( mode, opt )
    , fac_test( mode, opt )
    , ftin2in_test( mode, opt )
    //, func2d_test( mode, opt ) // to-be-coded
    , getColorArg_test( mode, opt )
    , ifthen_test( mode, opt )
    , mod_test( mode, opt )
    , or_test( mode, opt )
    , pick_test( mode, opt )
    , prod_test( mode, opt )
    , roundto_test( mode, opt )
    , und_test( mode, opt )
    , und_opt_test( mode, opt )
    , und_opt_concat_test( mode, opt )
    , und_opt_update_test( mode, opt )
    ]
);
