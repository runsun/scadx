// tetrahedron with a missin face 
p = [ [0,0,0], [10,0,0], [0,10,0], [0,0,10] ]; 
f = [ [0,1,2], [0,3,1], [0,2,3] ]; 
// additional faces to close it 
n=3; 
degface = false; 
ps = [for(i=[0:n+1])  p[1]*i/(n+1) + p[2]*(n+1-i)/(n+1) ];   
fs = [for(i=[1:n+1]) [ 3, 3+i, 4+i ] ]; 
df = n>0 && degface ? [for(i=[n+2:-1:1]) 3+i ] : []; 
// the full tetrahedron 
difference(){ 
    polyhedron(concat(p,ps),concat(f,fs,[df])); 
    cube(3,center=true); 
    translate([10,0,0]) cube(3,center=true); 
}