﻿__version__= "180210-1"
__description__ = r"""
   Output the OpenSCAD result shapes to make a doc file. 
   The source .scad file must follow some rules:\n\r
   
   1. A module name should contain a word 'demo', like:
      
      module demo_Cube_default(...),
      module Cube_demo(...); 
   
   2. End of module block should contain a line starts with "}"
      
      module demo_something(){
      ...
      } <=====  
      
   Usage Examples:
   
   demodoc.py ..\demos\demo_Cube.scad 
              -t C:\Users\Daniu\Documents\prog\openSCAD\scadx_demodoc  -doc html  
              
   This creates a 'Cube' folder in the -t folder.             
       
"""
__author__= "--- ver:{ver} ---by Runsun Pan (runsun(at)gmail(dot)com)".format(ver=__version__)


r"""
>>> demodoc.py --help
usage: C:\Users\Daniu\Documents\prog\openSCAD\scadx\py\demodoc.py
       [-h] [--version] [-t Target_folder] [-d Dry run] [-o OpenSCAD path]
       [-m Output_format]
       src

Output the OpenSCAD result shapes to make a doc file. The source .scad file
must follow some rules: 1. A module name should contain a word 'demo', like:
module demo_Cube_default(...), module Cube_demo(...); 2. End of module block
should contain a line starts with "}" --- ver:180210-1 ---by Runsun Pan
(runsun(at)gmail(dot)com)

positional arguments:
  src               Source_file_or_folder

optional arguments:
  -h, --help        show this help message and exit
  --version         Shows version and exits.
  -t Target_folder  Target folder
  -d Dry run        Dry run. Default=True
  -o OpenSCAD path  Path to OpenSCAD program. In Windows it should be
                    ...OpenSCAD.com (not .exe). Default='openscad.com'
  -m Output_format  html|md|wiki
       
""" 

########################################################################## 
## demodoc.py
##
## Run ALL demo modules in an OpenSCAD file xxx.scad and 
## generate a doc file (one of xxx.html, xxx.md, xxx.wiki) with output
## of shapes
## 
## by Runsun Pan (2018.2.*)
###########################################3############################## 
import sys, os, re, argparse, pprint, datetime
from collections import *
print("\n\ndemodoc.py\n\n")
# argument setup and parse
argp = argparse.ArgumentParser( 
                 prog= __file__
                 ,description=  __description__+ "\n" + __author__ )
argp.add_argument('--version', action='version'
                 , version='%(prog)s '+__version__ 
                 , help='Shows version and exits.') 
argp.add_argument("-t", metavar="Target_folder", type=str, default=".", help="Target folder")
argp.add_argument("-d", metavar="Dry run", type=type(True), default=False, help="Dry run w/o calling OpenSCAD. Default=True")
argp.add_argument("-o", metavar="OpenSCAD path", type=str, default="openscad.com" 
                 , help="Path to OpenSCAD program. In Windows it should be ...OpenSCAD.com (not .exe). Default='openscad.com'")
argp.add_argument("-doc", metavar="Output_doc_format", type=str, default="html"
                 , help="Output doc format (html|md|wiki). Default 'html'. Set to '' to disable doc. Will be '' if -mods is set.")
argp.add_argument("-imgds", metavar="Image display size", type=str, default="250,200"
                 , help="Image display size; imgds=width,height")
argp.add_argument("-imgcs", metavar="Image create size", type=str, default="600,450"
                 , help="Image create size; imgcs=width,height")
argp.add_argument("-mods", metavar="module name", type=str, default=""
                 , help="Module names to call. Default '' to call all. -mod=name1,name2 ... ")
#argp.add_argument("-a", metavar="axes", type==type(True), default=True, help="Show axes")
argp.add_argument("s", metavar="src", type=str, help="Source_file_or_folder")
            
args= argp.parse_args()

SRC_FULLPATH = args.s  # <path>\demo_Cube.scad
SRC_FOLDER   = os.path.split(SRC_FULLPATH)[0]
SRC_FILE     = os.path.split(SRC_FULLPATH)[1]
TOPIC        = SRC_FILE.split('.')[0].split('_')[-1]   # Cube
OUT_FOLDER   = os.path.join( args.t, TOPIC) 
OUT_IMG_FOLDER   = os.path.join(OUT_FOLDER,"img")

if not os.path.exists(OUT_FOLDER):
    os.makedirs(OUT_FOLDER)
if not os.path.exists(OUT_IMG_FOLDER):
    os.makedirs(OUT_IMG_FOLDER)

IMG_CREATE_SIZE  = args.imgcs
IMG_DISPLAY_SIZE = args.imgds
OPENSCAD_PROG    = args.o

# Set args.mod=[] (default) will call ALL valid modules in the SRC_FILE
# Set args.mod=demo1,demo4 : call modules demo1() and demo4()
MODULE_NAMES = (args.mods and args.mods.split(',')) or [] 

# DOC_FORMAT: html|md|wiki. Set to '' to disable doc generation. 
# It is set to '' if -mods is given. This is to provide a mean
# to "re-make the output image(s)" w/o re-write the doc 
DOC_FORMAT = (MODULE_NAMES!=[] and "") or args.doc 

# If args.d==True, run w/o actually calling OpenSCAD. It provides
# a mean to rewrite the doc w/o regenerating the output imgs.
DEBUG = args.d

print("DEBUG=",DEBUG)



mod_patttern  = re.compile("module (.*demo.*)\(")

data= OrderedDict( [ ('topic',TOPIC)
                   , ('outfolder',OUT_FOLDER)
                   , ('outimgfolder',OUT_IMG_FOLDER)
                   , ('modules',OrderedDict())
                   ] )
modules = data["modules"]

def getcontent(fullpath): 
   ''' Return an OrderedDict representing data for a .scad demo file 
   
       { 'topic':'Cube'
       , 'modules': #another OrderedDict 
                    { 'demo_Cube_default': 
                        { 'name': 'demo_Cube_default'
                        , 'camera': '0,1,1.5,50,0,45,0'
                        , 'content': <module content>
                        } 
                    ,  'demo_Cube_size': 
                        { 'name': 'demo_Cube_size'
                        , 'camera': '0,1,1.5,50,0,45,0'
                        , 'content': <module content>
                        } 
                    }
       }
   ''' 
   lines = open(SRC_FULLPATH, "r").read().split("\n")
   matches=[]

   for i,ln in enumerate(lines):
      
      match = (not ln.startswith("//") # Exclude comment lines
              and mod_patttern.search(ln))  # Find module header demo_xxx_yyy 
                                            # or xxx_demo_yyy
      if match:
         modname = match.groups()[0]
      else:
         modname = ""   
      
      #print("-------------\n", i, ln, mod_patttern.search(ln), match, ", modname=", modname
         #, (modname in MODULE_NAMES), (match and MODULE_NAMES==[]), MODULE_NAMES, type(MODULE_NAMES)
         #) 
      
      if (modname in MODULE_NAMES) or (match and MODULE_NAMES==[]):
         
         #if modname and (modname in MODULE_NAMES or MODULE_NAMES==[]):  
         #modname = match.groups()[0]
         
         matches.append( modname )
         
         modules[modname]= OrderedDict([('modname',modname)])   
         imgpath = os.path.join( OUT_IMG_FOLDER, modname+".png")         
         modules[modname]['imgpath']= imgpath
         
         # Find the module block end then process module content
         # An end of module block is a line starts with "}"
         for j,ln in enumerate(lines[i:]):
            
           isModuleEnd= ln.startswith("}")  
                  
           if( isModuleEnd ):
           
             modcontent = lines[i:i+j+1]
             
             #-------------------------------------------
             # Find module-specific camera setting. Could be
             #   //--camera=tx,ty,tz,rx,ry,rz,d;
             #   //--camera=[ 0, 1, 1.5 ],[ 50, 0, 45 ],12;
             # Use the OpenSCAD editor's 'Paste viewport translation/rotation'
             # to make the 2nd one.  
             
             camera = [ line for line in modcontent 
                        if line.strip().startswith("//--camera=") ]
             if camera:
               camera = camera[0].split("//")[1].split(";")[0]
               camera = (camera.replace('][',',').replace('[','')
                        .replace(']','').replace(' ',''))
               modules[modname]['camera']= camera
             
             #-------------------------------------------
             # Mod-specific image CREATE size, decides size of created .png 
             #
             # //--imgcs=width,height
             # 
             # A global imgcs can be set by the argument to this py program:
             # 
             # >>> py demodoc.py ... --imgcs=300,200
             #
             # It is assigned to a constant IMG_CREATE_SIZE (default 600,500) 
             # The value is fed to the '--imgsize' argument
             #  for the call to OpenSCAD in command line
                         
             imgcs = [ line for line in modcontent 
                        if line.strip().startswith("//--imgcs=") ]
             if imgcs:
               imgcs = imgcs[0].split("//--imgcs=")[1].split(";")[0]
             else:
               imgcs = IMG_CREATE_SIZE
             modules[modname]['imgcs']= imgcs
               
             #-------------------------------------------
             # Mod-specific image DISPLAY size=> displayed size of the png  
             #
             # //--imgds=width,height
             #
             # A global imgds can be set by the argument to this py program:
             # 
             # >>> py demodoc.py ... --imgds=250,200
             #
             # It is assigned to a constant IMG_DISPLAY_SIZE (default 600,500) 
             # The value is fed to the <img> tag when creating html
                         
             imgds = [ line for line in modcontent 
                        if line.strip().startswith("//--imgds=") ]
             if imgds:
               imgds = imgds[0].split("//--imgds=")[1].split(";")[0]
             else:
               imgds = IMG_DISPLAY_SIZE
             modules[modname]['imgds']= imgds
                 
                 
             #-------------------------------------------
             # Mod-specific axes DISPLAY --- =0 to turn it off 
             #
             # //--axes=0
             
             axes = [ line.strip() for line in modcontent 
                        if line.strip().startswith("//--axes=") ]
             #print("axes", axes, axes and axes[0].split("//--axes="))  
             axes = (( axes and axes[0].split("//--axes=")[1])
                      or "1")
             modules[modname]['axes']= axes  
             #print("axes", axes)  
             #-------------------------------------------
                           
             # Remove unnecessary lines               
             modcontent = [ line for line in modcontent
                   if not line.strip().startswith("//")
                   and not line.strip().startswith("echo(")
                   and not line.strip().startswith("echom(")
                   and not line.strip().startswith("_echo(")
                   ] 
                   
             #-------------------------------------------
                           
             modules[modname]['content']= modcontent 
             break
 
         #print('b4 coord,  axes=', modules[modname]['axes'], type(modules[modname]['axes']) )
         coord = modules[modname]['axes']!="0" and "Coord(r=0.01);" or "" 
         
         opcmd = '-D {coord}{modname}()'.format(coord=coord, modname=modname)
         #print("opcmd=", opcmd)              
         cmd= ('{openscad} "{srcfile}" --enable="lc-each" '+
               '--preview=1 {imgsize} {camera} '+
               '--projection={projection} -o {outpath} {params}').format(
                 openscad=OPENSCAD_PROG
               , srcfile= fullpath
               , outpath = imgpath
               , params = opcmd
               , camera = ( (camera and camera) or  
                            "--camera=0,1,1.5,50,0,45,0"
                          ) 
               , imgsize = "--imgsize="+imgcs
               , projection = "p" # --projection=(o)rtho|(p)ersp
               )
         modules[modname]['cmd']=cmd
         #print("\nFor {modname}(), cmd= {cmd}".format(modname=modname, cmd=cmd))     
         
   return data
      
         
#f= open( imgpath = os.path.join( args.t, 'scadx_'+ demotopic+".html"),  )   
                 
'''
         "C:\Program Files\OpenSCAD\openscad.exe"
         cmd = 'openscad %(scadfile)s -o %(outfile)s %(opt)s '%params
		if not dryrun:
			os.system(cmd)
		print "* cmd=", cmd
	'''	
'''
        if args.i==0:
           #match = word in ln or pattern.search("{0}".format(ln))
           match = pattern.search("{0}".format(ln))
           if match:
             if not thisFileHasMatches:
             	print("| "+f+":")
             	thisFileHasMatches = True
             mcount = mcount+1
             files.setdefault(fullpath,0)
             files[fullpath] += 1             	
             print( "|   #%s: %s "%(i, ln)) #.strip()) )
        else:
           #match = pattern.match(ln) or pattern.search("{0}".format(ln))
           match = pattern.search("{0}".format(ln))
           if match:
             if not thisFileHasMatches:
             	print("| "+f+":")
             	thisFileHasMatches = True
             mcount = mcount+1
             files.setdefault(fullpath,0)
             files[fullpath] += 1             	
             print( "|  #%s: %s "%(i, ln.strip()) )
        '''     	
   
def getHTMLdoc( data ):

  #print("--- ENTER getHTMLdoc() ---")
  modules = data['modules']
  
  ## css is modified from:
  ## https://cdn.rawgit.com/google/code-prettify/master/loader/prettify.css
  css="""
    .pln{color:#000}
    img {border:px solid gray}
    table,div{padding:0px;margin:0px}
    @media screen{.str{color:brown}
                  .kwd{color:blue}//#008}
                  .com{color:green} //comment
                  .typ{color:darkblue} 
                  .lit{color:teal} //number
                  .clo,
                  .opn,
                  .pun{color:purple}
                  .tag{color:#008}
                  .atn{color:darkblue} 
                  .atv{color:#080}
                  .dec,
                  .var{color:darkblue} 
                  .fun{color:red}
                  }
    @media print,projection{
                  .kwd,.tag,.typ{font-weight:700}
                  .str{color:brown}
                  .kwd{color:#006}
                  .com{color:green //#600
                      ;font-style:italic}
                  .typ{color:#404}
                  .lit{color:#044}
                  .clo,.opn,.pun{color:purple} //#440}
                  .tag{color:#006}
                  .atn{color:#404}
                  .atv{color:#060}
                  }
    pre.prettyprint{padding:10px;margin:0px;border:0px solid white}
    ol.linenums{margin-top:0;margin-bottom:0}
    li.L0,li.L1,li.L2,li.L3,li.L5,li.L6,li.L7,li.L8{list-style-type:none}
    li.L1,li.L3,li.L5,li.L7,li.L9{background:#eee}
  """
  js="""
  function toggleImgSize(img, imgcw, imgch, imgdw, imgdh)
  {
     img.isOriginalSize = ("isOriginalSize" in img)?
                          (img.isOriginalSize?0:1)
                          :img.isOriginalSize=1;
     if(img.isOriginalSize){ img.style.width= imgcw+'px';
                             img.style.height= imgch+'px';
                           }
     else { img.style.width= imgdw+'px';
            img.style.height= imgdh+'px';
          }
  }
  """
  # <img src="smiley.gif" alt="Smiley face" height="42" width="42">
  IMG_TEMPLATE=("""<TD><DIV class='imgcontent'"""
               +""" style="padding:0px;font-size:10px;text-align:center"">"""
               +"""<IMG border="1px solid gray" width='{imgdw}' height='{imgdh}' src='{imgpath}'"""
               +""" onclick="toggleImgSize(this, """
               +""" {imgcw},{imgch},{imgdw},{imgdh});">"""
               +""" <br/><code>create:{imgcw}x{imgch}, display: {imgdw}x{imgdh}</code>"""
               +""" </DIV><br/></TD>""")
  
  MODULE_TEMPLATE=("""<TD style="vertical-align:top">"""
                  +"""<DIV class="modcontent" style="padding:0px;vertical-align:top"><CODE>"""
                  +"""<PRE class="prettyprint" style="vertical-align:top;margin:0px">{modcontent}</PRE>"""
                  +"""</CODE></DIV><br/></TD>""")
  
  HTML_TEMPLATE="""
  <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
  <HTML>
  <HEAD>
  <TITLE>Scadx {topic} Demo</TITLE>
  <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
  <STYLE type="text/css">
  {css}  
  </STYLE>
  <SCRIPT>
  {js}
  </SCRIPT>
  </HEAD>
  <BODY><H2>Scadx demo: {topic}</H2>
  <TABLE border=0 style='borderSpacing:0px'>{tablecontent}</TABLE>
  <HR/>
  <CODE>Generated with <b>{thisfile}</b> on <i>{date}</i><br/>
  <span style='color:gray'>{cmd}</span>
  </CODE>
  
  </BODY>
  </HTML>
  """
  def highlight(line):
    #funcmod_pattern = re.compile("[a-zA-Z_0-9]!\(")
    #funcmod = funcmod_pattern.match(line)
    greenhead="""<span style='color:green'>"""
    ss = line.split('//')
    line= (len(ss)==1 and line) or ( ss[0]
                            + greenhead+ "//{}</span>".format(''.join(ss[1:]))
                           )
    return line.replace("*/","*/</span>").replace("/*",greenhead+"/*")
                            
  def getModContent(moddata):
    #print("--- ENTER getModContent() ---")
    modcontent = moddata['content']
    headerline = modcontent[0].replace("module "
                   , "<b>module</b> <b style='color:darkblue'>").replace("(","</b>(")
    modcontent = [headerline]+ modcontent[1:]
    modcontent = [ highlight(line) for line in modcontent ]
    modcontent = "<br/>".join( modcontent)
    return modcontent.replace("<br><br>","")
                 
  tableccontent=[]     
  for modname, moddata in modules.items():
    imgcw,imgch = moddata['imgcs'].split(',')
    imgdw,imgdh = moddata['imgds'].split(',')
    imgstr= IMG_TEMPLATE.format( imgcw=imgcw  
                                , imgch= imgch
                                , imgdw = imgdw
                                , imgdh = imgdh
                                , imgpath = moddata['imgpath']
                                )
    codestr= MODULE_TEMPLATE.format(modcontent= getModContent(moddata))     
    tableccontent.append( "<TR style='vertical-align:top'>"
                + imgstr +codestr +  "</TR>")
        
    #if DEBUG:  
    #   print('>>> img str: '+ imgstr)
              
  return HTML_TEMPLATE.format(
           topic = data['topic']
         , css=css
         , js=js
         , tablecontent = ''.join( tableccontent )
         , thisfile = os.path.split(__file__)[1]
         , date = datetime.date.today()
         , cmd =' '.join(sys.argv).replace(os.path.dirname(os.path.abspath(__file__))+'\\','>>> ')
                  .replace(SRC_FOLDER,"&lt;SRC_FOLDER>").replace(args.t,"&lt;OUT_FOLDER>") 
  
         )

data= getcontent(SRC_FULLPATH)
modules=data['modules']
#print("data['modules'].keys()=", "\n"+"\n".join(list(modules.keys())))

for modname in modules:
  mod = modules[modname]
  print("\n"+ modname+'()\n')
  print("imgcs=", mod['imgcs'], ", imgds=", mod['imgds'] )
  print("Img be saved to ", mod['imgpath'])
  cmd= mod['cmd']
  #print("cmd:", cmd)
  if not DEBUG:
    os.system(cmd)

if DOC_FORMAT == "html":  
  htmldoc = getHTMLdoc(data)
  #pprint.pprint( data )
  #print( htmldoc )

  out_html_path = os.path.join( OUT_FOLDER, "Scadx_demo_"+data['topic']+'.html')
  print("\n\nWritting htmldoc to " + out_html_path)
  #print( htmldoc)
  open(out_html_path,"w").write(htmldoc)






'''
runscad.py

 	>>> python runscad_animate.py <scad_file> <output_file> <opt> <opt> ...
          
For example:

    >>> python runscad.py tmp.scad out.png n=3 D=t=0.2*(%(n)s-1);run() 

outfile:
		
	Setting outfile to xxx.png will save the shape to xxx.png.  But if 
	n (number of runs)>0, it will run n times and save to 
	xxx00001.png, xxx00002.png, xxx00003.png ... 
	This is done by converting "." to "00001.", so the outfile name
	must have a ".".

opt:
 
 	Options: All name/value pairs are in 'name=value' format or simply 'name'
	if it's a flag (for example, version). Pairs are separated by spaces:
	
		imgsize=100,200 version D="..." dryrun
	
  	opt has 2 parts: 

	(1) Original openscad command line options:

    	d=deps_file
		m=make_command
		D=a=2;b=3;run();
		version 
		info
		camera=translatex,y,z,rotx,y,z,dist | camera=eyex,y,z,centerx,y,z 
        imgsize=width,height
		projection=(o)rtho|(p)ersp]
		render | preview[=throwntogether]
		csglimit=num

	(2) runscad.py options:

		dryrun: 

			Debug flag. When set, it will not call openscad but will output
			parameters and settings for debugging. 

			>>> python runscad.py tmp.scad dump.png D="a=3;run();" dryrun

		n: 	# of repeat run. To run it 3 times:

			>>> python runscad.py tmp.scad dump.png D="a=3;run();" n=3

			If n >0, we also get two internal vars:

			i: 	index of run in each repeating run
			t: 	i*1.0/n. It is between 0~1. This can be used to run animation.

			n,i,t can be sent to your scad file by giving them in the D option:

			D="index=%(i)s;time_fraction=%(t)s;nRun=%(n)s"

			>>> python runscad.py tmp.scad dump.png D="n=%(n)s" n=3

		Dquote: either "'" or '"'

			The D to send statements to openscad and is used as:

				python runscad.py tmp.scad out.scad D="a=3;"

			wihch is converted to an openscad command-line:

				openscad tmp.scad out.scad -D "a=3;"

			But if you want to send string by attempting :

				python runscad.py tmp.scad out.scad D="a=\"left\";"

			which creates a syntex error:

				openscad tmp.scad out.scad -D "a="left";"

			To avoid this, set Dquote="'":

				python runscad.py tmp.scad out.scad D="a=3;" Dquote="'"

			which converts to the following command line:

				openscad tmp.scad out.scad -D 'a="left";'

https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/Using_OpenSCAD_in_a_command_line_environment

 "
'''
'''
else:

	scadfile = sys.argv[1]
	outfile = sys.argv[2]
	print "\n* num of args = ", narg

	p = re.compile("^([a-zA-Z]*)=(.*)") # match "o somefile.scad" or "camera=..." 

	print "\n* Extract opt:"
	opt = [ p.match(x)!=None and p.match(x).groups() or (x,"")  for x in sys.argv[3:]
		  ]
	print "  opt = ", opt

	print "* Convert opt to dict:"
	opt = dict( opt )
	print "  opt = ", opt
	
	if 'o' in opt: 
		print "* Remove the 'o' setting from opt --- which should be set in outfile=..."
		del opt['o']
	
	n = int(opt.get('n',0))
	print "* n is found to be %s"%n 	
	if 'n' in opt: 
		print "* Remove n from opt:"
		del opt['n']
		print "  opt = ", opt

	Dquote= opt.get('Dquote','"')
	if 'Dquote' in opt: 
		print "* Remove Dquote from opt:"
		del opt['Dquote']
		print "  opt = ", opt

	dryrun= False 
	if 'dryrun' in opt: 
		dryrun=True
		print "* dryrun is found to be true. Remove Dquote from opt:"
		del opt['dryrun']
		print "  opt = ", opt

	print "* Convert python dict to openscad command line:"
	opt = [ k in ("o","d","m","D")
			and "-"+k+ " "+ (k=="D" and Dquote or '')+ opt[k]+ (k=="D" and Dquote or '')
		    or "--"+k + (opt[k] and ("="+opt[k]) or "")
		    for k in opt ] 
	print "  opt = ", opt
	
	opt = " ".join( opt )
	print "  opt = ", opt

	if dryrun:
		print "\nStart dryrun --- openscad is not executed"

	for i in range(n):
		print "\nn=", n, ", i=", i
		t = i*1.0/n
		_outfile = n and outfile.replace(".", str(i+1).rjust(5,'0')+".") or outfile
		optvars = {"i":i,"n":n,"t":t,"outfile":_outfile}
		print "* vars available to -D: ", optvars 

		params={ "scadfile": scadfile
			  , "outfile" : _outfile
			  , "opt": opt%optvars
			  }		
		cmd = 'openscad %(scadfile)s -o %(outfile)s %(opt)s '%params
		if not dryrun:
			os.system(cmd)
		print "* cmd=", cmd
		
##############################
'''