__description__ = "Test all scadx functions."
__author__= "Runsun Pan"
__version__= "181010-1"



import os, sys, re, argparse, datetime
'''
# argument setup and parse
argp = argparse.ArgumentParser( 
       prog= __file__            # Needed in --version below  
       ,description=  __description__ )
       
argp.add_argument('--version', action='version'
                 , version='%(prog)s '+__version__ 
                 , help='Shows version and exits.')    
                    
argp.add_argument("-fn", metavar="filename", type=str, help="Target file" )
argp.add_argument("-fo",  metavar="folder",  type=str, default=".", help="Target folder")
argp.add_argument("-sc",  metavar="showContent",  type=str, default=False
                 , help="Show the target file content.")
                                               
args= argp.parse_args()

error = ""
'''

'''
Note: when running openscad from the command line, use "2> <output_file>":
 
#  openscad  --enable=lc-each scadx_tests.scad -o dump.stl -D "scadx_tests(FILES_TO_TEST)" 2> scadx_tests.log

But when use python to run it, we need to use ">> <output_file>"

cmd = 'openscad --enable=lc-each ../scadx_tests.scad -o dump.stl -D "scadx_tests(FILES_TO_TEST)" >> scadx_tests.log'
os.system(cmd)

'''

#print("args=", args)

cmd = 'openscad --enable=lc-each ../scadx_tests.scad -o dump.stl -D "scadx_tests(FILES_TO_TEST)" >> scadx_tests.log'
os.system(cmd)

now = str(datetime.datetime.now())[2:] # Set now to 18-10-10 19:05:44.338408
now =  ''.join( now.split(":")[:-1]).replace("-","").replace(" ",".")  # Set now to 181010.1955
print("now:", now )

output_file= 'scadx_tests.log'
lines = open( output_file, 'r').readlines()

#if lines and not lines[-1].strip().endswith("\n"):  # Make sure the last line ends with NEWLINE 
#   lines[-1] = lines[-1].strip()+"\n"

# Remove (1) the starting 'ECHO: "' and trailing " on each line
# (2) the last line containing "Current top level object is empty."

newlines=[]
ptn = re.compile(r'ECHO: \"|"$')
for i,line in enumerate(lines):
      if not line.startswith("Current top"):
        newlines.append( re.sub( ptn,'', line).replace("\\", "") )

newlines[-1]= now+ ", "+ newlines[-1].replace("scadx_tests: ","")  # Add time stamp to the last line

newlines =  [ x for x in newlines if x.strip() ]  ## Remove empty lines
newlines = [ newlines[-1] ] + newlines[:-1]       ## Move the last line to the top
open( output_file, 'w').write( ''.join( newlines ))