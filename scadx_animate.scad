function get_animate_visibility(showtime, $t=$t, _i=-1)=
(
  /*
   Return 1|0 according to showtime and $t to represent turn-on / off 
   of the visibility.

   usage example: 

     if ( get_animate_visibility( showtime=[0.2,0.4] ) )
     {
         polyhedron( pts, faces)
     }


   showtime: either a fraction between (0,1), or a list of them, or undef

             A list: Instruct visibility in [on,off,on,off,...] manner
            
                showtime= [0.2, 0.4] return 1 when $t>0.2 (turn on)
                                            0 when $t>0.4 (turn off)
                0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
                -  -  +  +  -  -  -  -  -  - -

                showtime= [0.3, 0.41, 0.7, 0.9]
                0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
                -  -  -  +  +  -  -  +  +  - - 
            
            A fraction: 
            
                showtime= 0.5
                0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
                -  -  -  -  -  +  +  +  +  + + 
           
           undef: 
 
                showtime= undef
                0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
                +  +  +  +  +  +  +  +  +  + +  

  result of test:
  
    showtime= [0.2, 0.4]
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    -  -  +  +  -  -  -  -  -  - - 
    "
    ECHO: "
    showtime= [0.2, 0.41]
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    -  -  +  +  +  -  -  -  -  - - 
    "
    ECHO: "
    showtime= [0.3, 0.41, 0.7, 0.9]
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    -  -  -  +  +  -  -  +  +  - - 
    "
    ECHO: "
    showtime= [0.5, 0.8]
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    -  -  -  -  -  +  +  +  -  - - 
    "
    ECHO: "
    showtime= [0.5, 1]
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    -  -  -  -  -  +  +  +  +  + - 
    "
    ECHO: "
    showtime= [0.5, 1.1]
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    -  -  -  -  -  +  +  +  +  + + 
    "
    ECHO: "
    showtime= [0.5]
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    -  -  -  -  -  +  +  +  +  + + 
    "
    ECHO: "
    showtime= 0.5
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    -  -  -  -  -  +  +  +  +  + + 
    "
    ECHO: "
    showtime= [0.2, 0.4, 0.6]
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    -  -  +  +  -  -  +  +  +  + + 
    "
    ECHO: "
    showtime= undef
    0 .1 .2 .3 .4 .5 .6 .7 .8 .9 1
    +  +  +  +  +  +  +  +  +  + + 
 
  */
  showtime==undef? 1
  : _i==-1? 
     (isarr(showtime) && last(showtime)!=1?
      get_animate_visibility( concat(showtime, [1.1]), $t=$t, _i=_i+1)
     :get_animate_visibility(showtime, $t=$t, _i=_i+1)
     )  
  : isnum(showtime) ? (showtime<0||1<showtime?0:( showtime<=$t?1:0))   //: showtime = 0.5
  : len(showtime)==1? ( showtime[0]<=$t?1:0)               //: showtime = [0.5]
  : _i> len(showtime)? 0
  : $t< showtime[_i] ? ( _i%2 )
  : get_animate_visibility(showtime, $t=$t, _i=_i+1)
);

module get_animate_visibility_test()
{
  for( showtime= [ [.2, .4], [.2,.41], [ .3, .41, .7, .9] 
                 , [.5,.8], [.5,1], [.5,1.1], [.5], 0.5, [ .2, .4, .6 ]
                 , undef                  
                 ] )
  { echo( str("<br/>", _b("showtime"), "= ", _fmt(showtime)
             , test_a_showtime( showtime ) 
             ) 
        ); 
  }  
  
  ts = [0,.1,.2,.3,.4,.5,.6,.7,.8,.9,1];
  function test_a_showtime( showtime, _ts="", _rtn="", _i=0)=
  (
    _i>= len(ts)? str("<br/>", setfont(replace(_ts, "0.", " ."))
                     ,"<br/>", setfont(_rtn)
                     , "<br/>" 
                     )
    : let( t= ts[_i]
         , v= get_animate_visibility( showtime, $t=t )
         )
      test_a_showtime( showtime
                      , _ts = str( _ts, _i==len(ts)-1?" ":"", v?_green( t):t )
                      , _rtn= str( _rtn
                                 , _i==0||_i==len(ts)-1?"":"&nbsp;"
                                , v?_green("+&nbsp;"):"-&nbsp;")
                      , _i=_i+1
                    )
  );
  
  function setfont(s)=_span( s, s="font-family:Lucida Console");
}

function get_scadx_animate_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_animate.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_animate_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_animate", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      //get_animate_visibility_test( mode, opt ) // to-be-coded
    //, setfont_test( mode, opt ) // to-be-coded
    //, test_a_showtime_test( mode, opt ) // to-be-coded
    ]
);
