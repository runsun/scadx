include <scadx_obj2.scad>
  
module test_tubeEnd_150629(){
    
    echom("test_tubeEnd_150629");
    pqr = pqr();

    n1= 4; //int(rand(3,5));
    n2= 6; //int(rand(6,8));
    sq= circlePts( pqr, rad=4, n=n1);
    //sq =[ for(i=range(4)) anglePt( pqr, a=90, a2 = i*360/4 ,len=4) ];
    //six= [ for(i=range(6)) anglePt( pqr, a=90, a2 = i*360/6, len=2 ) ];
    six= circlePts( pqr, rad=2, n=n2);
    echo(sq=sq);
    pts = concat( sq,six);
    echo(sq=sq);

    MarkPts(sq, "ch=[closed,true];l" );
    MarkPts(six, ["ch=[closed,true]]","label","456789"] );

    midpts = [ for(i=range(n1)) 
               midPt( get(sq, [i, sq[i+1]?i+1:0]))
    //              , i>len(sq)-1?0:i+1]) )
             ];
    echo(midpts= midpts);

    //MarkPts( get(midpts,[0,2]), "ch;ball=false");
    //MarkPts( get(midpts,[1,3]), "ch;ball=false");

    //a=[1,2,3];
    //echo(range(a,cycle=[-1,1]));

    tubeEnd_faces = faces("tubeEnd");
    echo( tubeEnd_faces = tubeEnd_faces );
    
    for( i= range(tubeEnd_faces) ){
        f = tubeEnd_faces[i];
        color(COLORS[i],0.6)
        Plane( get( pts, f) );
    }

}
//test_tubeEnd_150629();

module GatewayArch_150629(){
    /* 
        St.Luis Gateway Arch
    
    * Following a hyperbolic cos: cosh(a) = ( exp(a)+exp(-a) ) / 2   
      2015.6.29
    */
    
    echom("GatewayArch_150629");
    
    n=43;  // this has to be odd number
    
    // Construct the hyperbolic cos data
    xs0= [for(i=range(n/2)) i ]; 
    xs=  concat( reverse(xs0)*-1 , slice(xs0,1) )/8;
    ys= [for(x=xs) (exp(x)+exp(-x))/2];
    echo(xs=xs);
    echo(ys=ys);   
    
    // Make the bone chain
    maxy = max(ys);    
    pts = [for(i=range(n))
            [xs[i],0, maxy- ys[i]]
          ];
    echo(pts= arrprn(pts,2));
    //MarkPts( pts, "ch;r=0.05;samesize;cl=red;sametransp"); 
    
    // Make the list of r (radius) for use along the 
    // bone chain.
    rs = [ for(r=xs/20) ((exp(r)+exp(-r))-2)*20];
    echo(rs= arrprn(rs,1));
           
    // Make chain points. 
    chret = chaindata( pts, nside=3, r=0.16, r=rs );
    chpts = hash( chret, "cuts");
//    echo(len_chpts= len(chpts)
//        , chpts= arrprn( chpts, 2));
    
    // Make the object
    faces = faces("chain", nside=3, nseg= n-1);
    color("gray",1)
    polyhedron( points= joinarr(chpts)
              , faces= faces);
}    
//GatewayArch_150629();

module precision_display_test_150725(){
    
    echom("precision_display_test_150725");
    
    x= 5;
    prec=10;

    for(n=[1:9]){  // n: single digit added number, like 
                   //    the 2,6 in 5.02, 5.00006, resp. 
        echo(str("<hr/>x= ",x, ", n= ",n, ", xx= x+n*pow(10-prec) >x?"));
        for( p=[0:prec] ){
            xx = x + n*pow(10,-p);
            echo( prec=p, xx=fullnumstr(xx), /*str(aa), "xx>x? "*/, xx>x );
        }
    }
}    
precision_display_test_150725();

module polygon_hole_150725(){
    echom("polygon_hole_150725", "Make polygon w/ hole using getPairingData");
    
    pqr= pqr();
     echo( pqr = pqr );

    pl1= circlePts( pqr, rad=4, n= int(rand(3,15)));
    pl2= circlePts( pqr, rad=1.5, n=int(rand(3,15)));
    
    //echo( _red(_s("<b>pl1: {_}, pl2: {_}</b>",[len(pl1),len(pl2)])));
    MarkPts( pl1, "ch;sametransp");Line0( get(pl1,[0,-1]));
    MarkPts( pl2, "ch;sametransp;r=0.05");Line0( get(pl2,[0,-1]));

    pairdata = getPairingData( pl1,pl2); //,c1=pqr[1],c2=pl2[1] );
    //echo( pairdata= pairdata );
    C1 = hash(pairdata, "C1");
    C2 = hash(pairdata, "C2");
    
    afips = hash(pairdata, "afips");
    afips1 = hash(pairdata, "afips1");
    afips2 = hash(pairdata, "afips2");
    //echo("afips:");
    //for( x = afips ) echo(x );
   
    echo(""); 
    //echo("afips2:");
    //for( x = afips2 ) echo(x );
        
    pl1new = hash(pairdata,"pts1");
    pl2new = hash(pairdata,"pts2");
    
    // Raise/Sink pl2new by some distance 
    pl2new2 = trslN( pl2new, rand(0,2));
    MarkPts( pl2new2, "ch;sametransp");Line0( get(pl2,[0,-1]));


    MarkPts( pl1new, ["r=0.1;trp=0.2;sametransp"] );
    //MarkPts( pl2new, ["r=0.1;trp=0.2;sametransp"]);
        
    //echo( pl1new= _colorPts(pl1new), len(pl1new));
    echo( pl2new= _colorPts(pl2new), len(pl2new));
    
    faces = hash( pairdata, "faces" );
    echo( lenface= len(faces), faces = faces );
        
    pts = concat( pl1new, pl2new2); //hash( pairdata, "pts" );
    //MarkPts( pts, ["ball=false", "label",["color","black","transp",1]] );
    
    for(i = range(pl1new) )
        color( get(COLORS, i, cycle=true), 0.6 )
        Plane( get(pts, faces[i]));
}    
//polygon_hole_150725();

module polygon_hole_150913(){
    echom("polygon_hole_150913"
    , "Chk if polygon can be done w/o getPairingData"
    );
/*


       5 +
         |
         +             o   
         |     o  
         +             
         |   o             o
         +            
         |    o              o
         +       o   o 
         |
   ------+---+---+---+---+---+---+---
         |   1   2   3   4   5   6        
*/

pts_out =  [ [0,0],[6,0],[6,5],[0,5] ];
pts_in =  [ [2,1],[3,1],[5,1.5],[3.7,1.5],[4.5,2.4]
          , [3.4,4], [1.4,3.2],[1,2.5]];
pts_in =  [ [2,1],[4.5,2.4], [3.4,4]];
nsideout = len(pts_out);
nsidein = len(pts_in);
echo(nsideout=nsideout, nsidein=nsidein);
//MarkPts( addz(pts_out), "ch=closed;l");
//MarkPts( addz(pts_in,0.6), "ch=closed;l");

pts_bot = concat( pts_out, pts_in );
echo(pts_bot = pts_bot );
MarkPts( addz(pts_bot), "l=[color,black];ball=0");
nsides= nsideout+nsidein;

path = [range(pts_out), [ for(i=range(pts_in))i+len(pts_out)]];
echo(polygonpath = path);
color(undef, 0.3)
linear_extrude(0.1)
polygon( points = pts_bot, paths = path );
//--------------------------------------------------
pts_top = addz( pts_bot, 3);
//MarkPts( pts_top, "ch");
pts = concat(addz(pts_bot),pts_top);
echo(pts = pts );
//MarkPts( pts, "l");
//MarkPts( get(pts, range(nsideout)), "ch=closed;ball=0");
//MarkPts( get(pts, range(nsideout,nsides)), "ch=closed;ball=0");
//MarkPts( get(pts, range(nsides,nsides+nsideout)), "ch=closed;ball=0");
//MarkPts( get(pts, range(nsides+nsideout,2*nsides)), "ch=closed;ball=0");
//for( i=range(nsides)) MarkPts( get(pts,[i,i+nsides]), "ch=closed;ball=0");
    
outfaces = [ for(i=range(pts_out))
              [ i
              , i+nsides
              , let(j=i+nsides+1) j>=2*nsideout+nsidein?nsides:j
              , let(j=i+1) j>=nsideout?0:j
              ]
           ];
infaces= [  for(i=range(pts_in))
              [ i+nsideout
              , let(j=i+nsideout+1) j>=nsides?nsideout:j
              , let(j=i+nsides+nsideout+1) j>=2*nsides?nsides+nsideout:j
              , let(j=i+nsides+nsideout) j //>=2*nsideout+nsidein?nsides:j
              ]
           ];
echo( outfaces = outfaces );
echo( infaces = infaces );
topfaces = joinarr([ range(nsides,nsides+nsideout)
             , range(nsides+nsideout, 2*nsides) ]);
botfaces = joinarr([ range(pts_out), range(nsideout, nsides) ]);

topfaces = [ range(nsides,nsides+nsideout)
             , range(nsides+nsideout, 2*nsides) ];
botfaces = [ range(pts_out), range(nsideout, nsides) ];

echo(topfaces=topfaces);
echo(botfaces=botfaces);

faces = concat(  outfaces,infaces 
              , botfaces, topfaces
                 );
echo(faces = faces );

//color(undef,0.8)
//polyhedron( points = pts
//          , faces = faces 
//          );

}
//polygon_hole_150913();

module polygon_hole_150913_2(){
    echom("polygon_hole_150913_2"
    , "Chk if polygon can be done w/o getPairingData"
    );
/*
       5 +
         |
         +             o   
         |     o  
         +             
         |   o             o
         +            
         |    o              o
         +       o   o 
         |
   ------+---+---+---+---+---+---+---
         |   1   2   3   4   5   6        
*/

pts_out =  [ [0,0],[6,0],[6,5],[0,5] ];
pts_in =  [ [2,1],[5,2], [3,4]];

faces1= [[0, 7, 8, 1], [1, 8, 9, 2], [2, 9, 10, 3], [3, 10, 7, 0], [4, 5, 12, 11], [5, 6, 13, 12], [6, 4, 11, 13], [0, 1, 2, 3], [4, 5, 6], [7, 8, 9, 10], [11, 12, 13]];
faces2 = [[0, 7, 8, 1], [1, 8, 9, 2], [2, 9, 10, 3], [3, 10, 7, 0], [4, 5, 12, 11], [5, 6, 13, 12], [6, 4, 11, 13], [0, 1, 2, 3, 4, 5, 6], [7, 8, 9, 10, 11, 12, 13]];

nsideout = len(pts_out);
nsidein = len(pts_in);
echo(nsideout=nsideout, nsidein=nsidein);
//MarkPts( addz(pts_out), "ch=closed;l");
//MarkPts( addz(pts_in,0.6), "ch=closed;l");

pts_bot = concat( pts_out, pts_in );
echo(pts_bot = pts_bot );
nsides= nsideout+nsidein;

path = [range(pts_out), [ for(i=range(pts_in))i+len(pts_out)]];
echo(polygonpath = path);

//--------------------------------------------------
pts_top = addz( pts_bot, 3);
//MarkPts( pts_top, "ch");
pts = concat(addz(pts_bot),pts_top);
// pts =  [[0, 0, 0], [6, 0, 0], [6, 5, 0], [0, 5, 0], [2, 1, 0], [5, 2, 0], [3, 4, 0], [0, 0, 3], [6, 0, 3], [6, 5, 3], [0, 5, 3], [2, 1, 3], [5, 2, 3], [3, 4, 3]]
echo(pts = pts );
MarkPts( pts, "l=[color,black]");
MarkPts( get(pts, range(nsideout)), "ch=closed;ball=0");
MarkPts( get(pts, range(nsideout,nsides)), "ch=closed;ball=0");
MarkPts( get(pts, range(nsides,nsides+nsideout)), "ch=closed;ball=0");
MarkPts( get(pts, range(nsides+nsideout,2*nsides)), "ch=closed;ball=0");
for( i=range(nsides)) MarkPts( get(pts,[i,i+nsides]), "ch=closed;ball=0");
    
outfaces = [ for(i=range(pts_out))
              [ i
              , i+nsides
              , let(j=i+nsides+1) j>=2*nsideout+nsidein?nsides:j
              , let(j=i+1) j>=nsideout?0:j
              ]
           ];
infaces= [  for(i=range(pts_in))
              [ i+nsideout
              , let(j=i+nsideout+1) j>=nsides?nsideout:j
              , let(j=i+nsides+nsideout+1) j>=2*nsides?nsides+nsideout:j
              , let(j=i+nsides+nsideout) j //>=2*nsideout+nsidein?nsides:j
              ]
           ];
echo( outfaces = outfaces );
echo( infaces = infaces );
topfaces = joinarr([ range(nsides,nsides+nsideout)
             , range(nsides+nsideout, 2*nsides) ]);
botfaces = joinarr([ range(pts_out), range(nsideout, nsides) ]);

//topfaces = [ range(nsides,nsides+nsideout)
//             , range(nsides+nsideout, 2*nsides) ];
//botfaces = [ range(pts_out), range(nsideout, nsides) ];

echo(topfaces=topfaces);
echo(botfaces=botfaces);

faces = concat(  outfaces,infaces 
              , [botfaces], [topfaces]
                 );
echo(faces = faces );

//color(undef, 0.8) linear_extrude(3) 
//polygon( points = pts_bot, paths = path );

color(undef,0.8)
polyhedron( points = pts
          , faces = faces2 
          );

}
//polygon_hole_150913_2();

module polygon_hole_150913_3_don(){
    echom("polygon_hole_150913_3_don");
//http://forum.openscad.org/Polyhedron-tube-with-irregular-sides-is-it-possible-tp13813.html

pts =  [[0, 0, 0], [6, 0, 0], [6, 5, 0], [0, 5, 0], [2, 1, 0], [5, 2,
0],
[3, 4, 0], [0, 0, 5], [6, 0, 5], [6, 5, 5], [0, 5, 5], [2, 1, 5], [5, 2,
5], [3, 4, 5]];

MarkPts( pts, "l=[color,black]" );

faces1= [[0, 7, 8, 1], [1, 8, 9, 2], [2, 9, 10, 3], [3, 10, 7, 0],
[4, 5,12, 11], [5, 6, 13, 12], [6, 4, 11, 13],
[0, 1, 2, 3, 4, 6, 5, 4, 3],
[10, 9, 8, 7, 11, 12, 13, 11, 7]];

//color(undef, 0.2)
//polyhedron( points = pts, faces = faces1  ); 

MarkPts( get(pts, [0, 1, 2, 3, 4, 6, 5, 4, 3]), "ch=closed;ball=0]"); 

MarkPts( get(pts, [10, 9, 8, 7, 11, 12, 13, 11, 7]), "ch=closed;ball=0"); 
}
//polygon_hole_150913_3_don();


module ellipse_crossection_test_150806(){
    echom("ellipse_crossection_test_150806");
/*
   Let pl90 is a 90deg crossection of a rod.
   
   If we tilt it, say, pl60 or pl45, will that 
   crossection be an ellipse ? 

*/    
    nside=36;
    n=2;
    pqr = pqr();
    
    bone= randPts(2,d=8); //app( p01(pqr), onlinePt( p10(pqr), -3) );
    Q = last(bone);
    
    MarkPts( bone, "ch");
    chdata = chaindata( bone, r=1.5, nside=nside );
    
    cuts = hash(chdata, "cuts");
    MarkPts( last(cuts), "ch;r=0.05");
    
    pl_bone_pl90_0= [bone[0],last(bone),last(cuts)[0] ];
    p_pl90_0_tilt45 = anglePt( pl_bone_pl90_0, a=45, a2=0, len=2);
    P = p_pl90_0_tilt45;
    //MarkPt(P, "l=P");
    //Plane( pl_bone_pl90_0, ["transp",0.2]);
    
    p_bone_pl90_90= N( pl_bone_pl90_0, len=2 );
    R=p_bone_pl90_90;
    //MarkPt(R, "l=R");
    
    pl_bone_pl90_90= [bone[0],last(bone),p_bone_pl90_90 ];
    //Plane(pl_bone_pl90_90, ["color","red","transp",0.2]);
  
    pl60 = [ P,  last(bone) , R];
    pl60x= [ onlinePt( [P,Q], ratio=-1 )
           , onlinePt( [R,Q], ratio=-1 )
           , onlinePt( [Q,P], ratio=-2 )
           , onlinePt( [Q,R], ratio=-2 ) ];
    //pl60x2= [ for(p=pl60x) p+ get(bone,-2)-Q ];
    pivotPt = onlinePt( [last(bone),bone[0]], ratio=0.4); 
    MarkPt(pivotPt);
    pl60x2= [ for(p=pl60x) p+ pivotPt-Q ];       
    pivotN = N( [pl60x2[0], pivotPt, pl60x2[1] ] );
    MarkPts( extendPts( [pivotPt, pivotN], len=1), "ch;ball=false");
    Plane(pl60x2, ["color","green","transp",0.2]);
  
    Js = projPts( cuts[0], p012(pl60x2), p01(bone) );
    echo( cuts0 = cuts[0] );
    echo( Js = Js );
    
    MarkPts( Js, "ch;r=0.05");
    
    Chain( chdata, "trp=0.5" );
  
}  
//ellipse_crossection_test_150806();//8"

//module helix_circle_150808(){   // forum challenge
////helix_circle_150808();

module explore_multmatrix_150830(){
  echom("explore_multmatrix_150830");
  m  = [ [1,0,0,0]
       , [0,1,0,0]
       , [0,0,1,0]
       , [0,0,0,1]
       ]; 

  function newm(r,c,value=1)=
     replace( m, r, replace( m[r], c,value));
  
  txt = "Scadx";
  txtw = textWidth(txt);  
  d = 10;
  P = [d,0,0];
  Q = [0,0,0];
  R = [0,d,0];
  S = [d,d,0];
  T = [d,0,d];
  U = [0,0,d];
  V = [0,d,d];
  W = [d,d,d];
   
  pts = [P,Q,R,S,T,U,V,W];
  frame_r = 0.1 ; 
  color("darkcyan"){
      MarkPts( [P,Q,R,S], "l=[text,PQRS,scale,0.1,shift,[0,0,-1]]");
      //MarkPts( [T,U,V,W], "ch;ball=0");
      Chain([P,Q,R,S],["r",frame_r, "closed",1,"rot",45]);  
      Chain([T,U,V,W],["r",frame_r, "closed",1,"rot",45]);
      for(i=[0:3]) Chain( get(pts,[i,i+4]), Rf= pts[i+1], ["r",frame_r, "closed",1, "rot",45] ); 
  }  
  //polyhedron( points= pts, faces=faces("rod", nside=4,nseg=1));
  //echo( m = m(m, 0,1) );
  module chknewm( r,c,value=1){
      color("black") //color("green",0.5)
      multmatrix(m = newm(m, r,c,value) )  
      text( txt); 
  }

  //---------------------------------------------------
  // stretch_x: 
  module stretch_x(ratio){  Arrow2( [[txtw,-2,0],[ ratio*txtw,-2,0]], opt=["color","darkcyan", "r", 0.3, "nside", 8] );
                            color(undef, 0.5) children();
                            color("black")
                            multmatrix( m=newm( 0,0,ratio) )
                                children();
                          }
  //chknewm( 0, 0, 0.5 );                        
  //stretch_x( 0.5 ) text( txt ); 
                
  //---------------------------------------------------
                          
  module tilt_x(ratio){  Arrow2( [[0,11,0],[ ratio*guessTextWidth(txt[0]),11,0]], opt=["color","darkcyan", "r", 0.3, "nside", 8] );
                            color(undef, 0.5) children();
                            color("black")
                            multmatrix( m=newm( 0,1,ratio) )
                                children();
                          }
  //chknewm( 0,1,1);  
  //tilt_x( 2 ) text(txt);
                          
  //---------------------------------------------------
                          
  module smear_x(ratio){  Arrow2( [[0,11,0],[ ratio*guessTextWidth(txt[0]),11,0]], opt=["color","darkcyan", "r", 0.3, "nside", 8] );
                            color(undef, 0.5) children();
                            translate( [0,-5,0])
                            color("black", 0.5)
                            multmatrix( m=newm( 0,2,ratio) )
                                children();
                          }
  //chknewm( 0,2,2);     
  //smear_x( 2 ) text(txt);
  
  //---------------------------------------------------
                          
  module move_x(ratio){  Arrow2( [[0,11,0],[ ratio,11,0]], opt=["color","darkcyan", "r", 0.3, "nside", 8] );
                            color(undef, 0.5) children();
                            //translate( [0,-5,0])
                            color("black", 0.5)
                            multmatrix( m=newm( 0,3,ratio) )
                                children();
                          }
  //chknewm( 0,3,2);     
  //move_x( 2 ) text(txt);
  
  /*
  m  = [ [1,0,0,0]
       , [0,1,0,0]
       , [0,0,1,0]
       , [0,0,0,1]
       ];                          
    m= [ [ ext_x, xoy  , xoz  , addx ]
       , [ yox  , ext_y, yoz  , addy ]
       , [ zox  , zoy  , ext_z, addz ]
       , [ 0    , 0    , 0    , shrink ]
       ];
  */                            
//  multmatrix( m = newm( 3,3,0.5) )
//  {
//     cube([d,d,d]);
//  }     
  
//  multmatrix(  m = [ [1,1,1,2]
//                   , [0,1,0,0]
//                   , [0,0,1,0]
//                   , [0,0,0,1]
//                   ] )
//  {
//     cube([d,d,d]);
//     color("black") 
//     linear_extrude(2) 
//     text(txt);
//  }
  
  module transform( 
      movex=0,movey=0,movez=0 /* move */
    , expx=1,expy=1,expz=1    /* expend(stretch) ratio. Note that when set
                                 to negative, a reflection+expand is performed: 
                                 expx=-1:  mirror on yz plane, no expansion
                                 expx=-2:  mirror on yz plane, expand twise as big
                                 expx=-1, expy=-1: mirror on pl{ [1,-1,0],z-axis }
                                                   (or pl{ [-1,1,0],z-axis } )
                                                   Or, easier, mirror on yx then on xz
                                 expx=expy=expz=-1: mirror thru [0,0,0].
                                 */
    , xoy=0,xoz=0   
    , yox=0,yoz=0
    , zox=0,zoy=0 /* The ?o? is a tilting angle where o is the 
                     origin ([0,0,0]). 
                     xoy means: xoz surface remans unchanged, but
                     yoz surface tilts angle xoy (=angle POU below)
                     toward x:   
                                 y
                                 |
                                 R___U____S___T
                                 |  /     |  /
                                 | /      | /
                                 |/       |/
                                 O--------P------x
                  */   
    , exp=1  /* exp ratio. This expends along all x,y,z directions 
                on top of expx, expy, expz
              */
    ){
        
    function al(ang)= 
        /* al(ang): Convert angle to len  
           When doing tilting, the argument m to multmatrix takes 
           len, but not angle:   
            
             m = [ [ expx, xoy,  xoz, addx ]   
                 , [ yox , expy, yoz, addy ]
                 , [ zox , zoy,  expz, addz ]
                 , [ 0   , 0  ,  0   , 1/exp ] 
                 ];
           where xoy is the ratio RU/RO in the figure: 
                 y
                 |
                 R___U____S___T
                 |  /     |  /
                 | /      | /
                 |/       |/
                 O--------P------x
           transform(), takes angles, so need conversion. 
        */
        ang?tan(ang):0;
    
      
    m = [ [ expx*exp   , al(xoy),  al(xoz) ]   
        , [ al(yox), expy*exp   ,  al(yoz) ]
        , [ al(zox), al(zoy),  expz*exp    ]
        ];   
    if( movex || movey || movez ){        
        translate( [movex,movey,movez] )
        multmatrix( m = m ){ children();}  
    }else{
        multmatrix( m = m ){ children();}  
    }    
  }    
  
  transform( expx=-1, expy=-1, expz=-1){ //expx=-1, expz=-1, expy=-1 ){
     //cube([d,d,d]);
     //color("black") 
     linear_extrude(5) 
     text(txt);
  }
//  linear_extrude(3) 
//  text(txt);
}    
//explore_multmatrix_150830();

module tapering_150831(){ 
    d= 5;
    t= 8;
      module obj(){ linear_extrude(3) text("Scadx"); };
    Transform( expz=-1 )
    union(){  
    obj();
    Transform( xoz=t,yoz=t ) obj();  
    Transform( yoz=t,xoz=-t ) obj();  
    Transform( xoz=-t,yoz=-t ) obj();  
    Transform( yoz=-t,xoz=t ) obj();  
    }  
}
//tapering_150831();

module text_on_curve_150831(){
    
  arcang = 60;  
  arcn   = 6;
  h =1; // text height
    
  // seeding  
  pqr0 = pqr();
  pqr = replace(pqr0,2, anglePt(pqr0,a=arcang));
  pqr = [[2.47339, 1.89419, 2.81681], [-2.65672, 0.268655, -2.49365], [0.0830584, -5.22254, 1.92244]];
  echo(pqr = pqr ); 
  rad = dist(pqr);
  //MarkPts( pqr, "ch;l=PQR" );
  Q= pqr[1];
    
  // make arc  
  arclen = arcang/360*2*PI*rad; 
  seglen = arclen / (arcn-1);  
  pts= arcPts( pqr, n=arcn);
  //MarkPts( pts, "ch;r=0.03");
    
  // text
  txt = "Scadx";  
  txtlen = guessTextWidth(txt); 
  txt_width_scale = arclen/txtlen; // this scale only apply to horizontal direction
  echo(txt_width_scale=txt_width_scale);
  
  module WriteAt( i=0 ){
    
    Pi = pts[i];
    Pj = pts[i+1];
    //Pk = extPt( [Pi,Pj] );
    Pk = -4*Pi+5*Pj; //extPt( [Pi,Pj] );
    Ni = N( [Pj,Pi,Q] );
    
    Pi2= onlinePt( [Pi,Pj], len=-i*seglen);  
    
    difference(){  
      Move( to= [Pj,Pi2,Ni]) {
      Transform( expx=txt_width_scale, expy= txt_width_scale )
      linear_extrude(h)
      translate( [0,0,h] )    
      text(txt,valign="center");        
      }
     
         
      Move( to= [Q,Pi,Pj]){
      translate( [ -8, -20, -5] ) // [ move_to_Q, move_along_PiPj_to,P, move_to_N(pqr)]  
      cube( [20,20,10] );}

      Move( to= [Q,Pj,Pk]){
      translate( [ -8,0,-5] )  
      cube( [20,20,10] );}
      

    }   
  }    
  
  for( i=[0:arcn-2]) WriteAt(i);
  //for( i=3) WriteAt(i);
   
  
//  module combine(_i=0){
//     if(_i<arcn-1)
//        combine(    
//      
//      
//  }    
  
//  Transform( expx=txt_width_scale, expy= txt_width_scale )
//  linear_extrude(0.1)
//  text(txt,valign="center");  
    
}
//text_on_curve_150831();

module text_on_curve2_150831(){
  echom("text_on_curve2_150831");  
  arcang = 60;  
  arcn   = 6;
  h =0.2; // text height
  rad = 5;   
    
  // seeding  
  //pqr00 = pqr();
//  P = randPt();
//  Q = onlinePt( [P,randPt()], len=rad+h );
//  echo(Q=Q);  
//  R = anglePt( [P,Q,randPt()], a=arcang);  
//  pqr = [P,Q,R];
  pqr = [[0.601075, -0.612236, 1.0554], [-2.99692, 3.82994, 2.87789], [0.00956704, 4.33327, -2.29006]]  ;
  Q= pqr[1];
  echo(pqr = pqr ); 
  //MarkPts( pqr, "ch;l=PQR" );
    
  // make arc  
  arclen = arcang/360*2*PI*(rad+h/2); 
  seglen = arclen / (arcn-1);  
  pts= arcPts( pqr, n=arcn);
  //MarkPts( pts, "ch;r=0.03");
    
  // text
  txt = "Scadx Test";  
  txtlen = guessTextWidth(txt); 
  txt_width_scale = arclen/txtlen; // this scale only apply to horizontal direction
  echo(txt_width_scale=txt_width_scale);
  //Dim(pqr);
  
  module WriteAt( i=0 ){
    
    Pi = pts[i];
    Pj = pts[i+1];
    //Pk = extPt( [Pi,Pj] );
    Pk = -4*Pi+5*Pj; //extPt( [Pi,Pj] );
    Ni = N( [Pj,Pi,Q] );
    
    Pi2= onlinePt( [Pi,Pj], len=-i*seglen);  
    
    difference(){  
      Move( to= [Pj,Pi2,Ni]) {
      Transform( expx=txt_width_scale, expy= txt_width_scale )
      translate( [0,0,-h] )    
      linear_extrude(h)
      text(txt);        
      }
     
         
      Move( to= [Q,Pi,Pj]){
      translate( [ -8, -20, -5] ) // [ move_to_Q, move_along_PiPj_to,P, move_to_N(pqr)]  
      cube( [20,20,10] );}

      Move( to= [Q,Pj,Pk]){
      translate( [ -8,0,-5] )  
      cube( [20,20,10] );}
      

    }   
  }    
  
  for( i=[0:arcn-2]) WriteAt(i);
  //for( i=3) WriteAt(i);
   
  
//  module combine(_i=0){
//     if(_i<arcn-1)
//        combine(    
//      
//      
//  }    
  
//  Transform( expx=txt_width_scale, expy= txt_width_scale )
//  linear_extrude(0.1)
//  text(txt,valign="center");  
    
}
//text_on_curve2_150831();

module Bend_dev_20150901(){ // FAILED
   echom("Bend_dev_20150901");
   echo(_blue("Bend attempt. Approach: 
   1) Cut obj into segments based on the ratio of 
      obj_width / arc_len. Each seg corresponds to
      each arc seg on the arc
   2) Make total of nseg objs. Move each x-wise to
      the negative side, each obj by ong more seg len
   3) Move each obj to its corresponding arc seg
   4) Cut away from the obj seg the unwanted parts
      (those before and after the seg) using preblock
      and postblock
    "));
    
    
   arc_n=6;
   objheight=1;
   rad = 10;
    
   //$fn=24;
    
   module Bend( pts, objheight=objheight, shieldingCubeSize=60 ){

       module PreBlock(i){
            Move( to= [ Q, pts[i]
                      , i==0? -4*pts[1]+5*pts[0] // Get a far pt on the pts[0] side 
                            : pts[i-1]] )
            translate( [ -shieldingCubeSize/2, 0, -shieldingCubeSize/2]) // -objheight] ) // [ move_to_Q, move_along_PiPj_to,P, move_to_N(pqr)]  
            cube( [cs,cs,cs] ); 
       }       
       module PostBlock(i){
            Move( to= [ Q, pts[i+1]
                      , i==len(pts)-2? 
                           -4*pts[-2]+5*pts[-1] // Get a far pt on the pts[-1] side 
                         : pts[i+2]] 
                       )
            translate( [ -shieldingCubeSize/2, 0, -shieldingCubeSize/2]) // -objheight] ) // [ move_to_Q, move_along_PiPj_to,P, move_to_N(pqr)]  
            cube( [cs,cs,cs] ); 
       }  
       // Move the obj seg
       module Moveto(i){
            Move( to= [ pts[i+1], pts[i], N( [pts[i+1],pts[i],Q]) ] )
            translate( [ -seglen*i, 0, -objheight] ) // [ move_to_Q, move_along_PiPj_to,P, move_to_N(pqr)] 
            children(); 
       }    
       
       Q = pts[1];
       //MarkPts( pts,"ch;r=0.2" );

       // Plot the normal arrows //
       for(i=range(pts)){ 
            pi = pts[i];
            //pii= und(pts[i+1], extPt( get(pts,[-2,-1])));
            pii= und( pts[i+1]
                    , -4*pts[-2]+5*pts[-1] // Get a far pt on the pts[-1] side 
                    );
            N = N( [pii,pi,Q], len=15);
            Arrow2( [ onlinePt([pi,N], len=-15), N], "r=0.1;cl=yellow");
       }
           
           
       cs = shieldingCubeSize;
       i=4;
       //for(i=[0:20])
       //for(i=[0:len(pts)-2])
       //difference(){
           Moveto(i) children();
           color(undef, 0.3) PreBlock(i);
           color("red", 0.3) PostBlock(i);
       //}
       

   }//..Bend
 
   bonedata = randchain(n=3, a=[60,120], seglen=10); //pqr(p=30,r=30);
   pqr = hash(bonedata, "pts");
   pqr = [[-0.881903, -0.253075, 2.38609], [0.465149, 2.97525, -0.61697], [-2.54546, 2.39592, -1.81612]];
   echo(pqr=pqr);
   MarkPts( pqr, "ch;l=PQR;r=0.2");  
   txt = "Scadx";
   
   pts = arcPts( pqr, n=arc_n, rad=rad+objheight );
   arcang = angle(pqr);
   arclen = arcang/360*2*PI*(rad+ objheight/2); 
   seglen = arclen / (arc_n-1);  
       
   Bend(pts) linear_extrude(objheight) text("Scadx");
   
  
   
}//..Bend_dev_20150901
//Bend_dev_20150901();

module Piggie_Corner_Shelf_20150902( isdim=true, usecolor=true ){
    
   echom("Piggie_Corner_Shelf_20150902"); 
    
   module Room(d=170,th=5){        
      color("goldenrod", 0.8) translate([-th,-th,-th]) cube( [d+th,d+th,th] ); 
      color("cornsilk", 0.8)  mvx(-th) cube( [th,d,d*.7] );
      color("cornsilk", 0.8)  mvxy(-th,-th) cube( [d+th,th,d*.7]) ;
   }     
    
   module Table(th=1){
      l = 84;
      w = 30; 
      leg2side = 5;
      module Leg(){ color("black") cylinder( h=28,r=0.8 ); }          
      
      color("cornsilk") mvz(28) cube( [l,w,1] );
      mvxy( leg2side,leg2side ) Leg();
      mvxy( l-leg2side,leg2side ) Leg();
      mvxy( leg2side,w-leg2side ) Leg();
      mvxy( l-leg2side,w-leg2side ) Leg();
      }      
  
  module Shelf(){
  
    d = 1.5; // width of a 2x2  
      
    boardW= 22+5/8;
    boardD= 18+1/8;
    boardH= 5.6/8;
      
    backboardTh = 0.5;
      
    tableH = 29; // = top of 2nd-floor board's  y position;
    F2H= 15; // = vertical space of 2nd floor of shelf (floor top to floor top)
    F3H= 13; // = vertical space of 3rd floor of shelf (floor top to floor top)
    
    earH = 9; // H of the ear (from floor to Hbeam top )
      
    shelfH = tableH + F2H + F3H + earH;  
    shelfW = boardW + d*2;
    shelfD = boardD + backboardTh;
    echo( boardH=boardH, boardW=boardW, boardD=boardD, earH=earH );  
    echo( shelfH=shelfH, shelfW=shelfW, shelfD=shelfD );  
      
    tilt_a = 87;
    tiltBeamForward = boardD*0.75-d/2; // dist between A,B where A is the 
                                   // botHBeam meets VBeam, B is the 
                                   // botHBeam meats tiltBeam. Both VBeam
                                   // and tiltBeam are outside of A,B, resp.
    virtualH = tiltBeamForward*tan( tilt_a ); 
                        // Extending the VBeam upward untill intersects with 
                        // TiltBeam, virtualH is the y-value of this intersection
                        
    dimopt = ["r",0.08, "guides",["len",2*d], "color","navy","transp",1
             ,"label", [ "size",1
                       , "scale",1.2
                       , "transp",1
                       , "rot",[0,180,0]
                       , "th",0.1
                       , "suffix","'"
                       ] 
             ]; 
          
    //-------------------------------------------

    module VBeams( ang=87){
        
        module VBeam(){ 
            l = shelfH-d*2; 
            module 2x2() { cube( [d,d,l] ); }
            color( usecolor?"red":undef){ 
                mv( 0, backboardTh, d ) 2x2();    
                mv( boardW+d, backboardTh, d ) 2x2();
            }    
            echo( _color( str("VBeam: (2x2x",l,") : ",2), "red"));

            if(isdim)
            color("red")
            Dim( [ [0,-4,d], [0,-4,d+l], [0,-5,1]], opt=dimopt ); 
            
        }
        VBeam();
        
        //------------------------------------------- 
        
        module TiltBeam(){
            
            beam_extension = d/tan(tilt_a); 
            module 2x2(){ 
                    mvy( tiltBeamForward )
                        rotate( [ 90-tilt_a,0,0] ) 
                        mvz( d-beam_extension ) 
                        cube( [d,d,shelfH-d+ beam_extension] );
            }
            color( usecolor?"red":undef){
                2x2(ang);
                mvx( 1.5+boardW ) 2x2(ang);
            }
            l = shortside(tiltBeamForward, shelfH-d);
            echo( _color( str("TiltBeam: (2x2x",l,") : ",2), "red"));
            
            if(isdim)
            color("red")
            Dim( [ [ shelfW, tiltBeamForward/virtualH*(virtualH),  1 ]
                 , [ shelfW, tiltBeamForward/virtualH*(virtualH-shelfH), shelfH ]
                 , [ shelfW,tiltBeamForward/virtualH*(virtualH), -shelfH ]
                 ], opt = update( dimopt, ["spacer",2.5]) ); 

        }     
        TiltBeam();
        
      if(isdim)
      color("red")
      Dim( [ [shelfW,tiltBeamForward,-2]
           , [shelfW,backboardTh,-2]
           , [shelfW, 0,-10]
           ], opt = update( dimopt,["h",0.2]) );
        
    }// .. VBeams
    VBeams();
    
    //-------------------------------------------  
    module HBoards(){  
        module Board(){ 
            color("lemonChiffon")
            translate( [d,0,0] ) // the '2' in 4x2
            cube( [ boardW, boardD, boardH ] ); 
            }
        mvyz( backboardTh, 0) Board();
        mvyz( backboardTh, tableH-boardH ) Board();
        mvyz( backboardTh, tableH-boardH+ F2H ) Board();
        mvyz( backboardTh, tableH-boardH+ F2H+ F3H ) Board();
            
        echo(_color( str("Hboard: ( "
                        , boardW, "x", boardD, "x", boardH
                        , " ): 4" 
                        ), "olive"
                   )
            );       
    }
    HBoards();

    //-------------------------------------------
    module BackBoard(){
        color("darkkhaki")
        cube( [ 1.5*2+ boardW, backboardTh, shelfH ] );
    }
    //-------------------------------------------
    module HBeams(){
        bTh = backboardTh;
        
        module SideBeams(){
            
             
           //------------------------------------------
           beam4_z = tableH-d+ F2H+ F3H+ earH;
           beam4_len = tiltBeamForward/virtualH* (virtualH-beam4_z)+d-backboardTh;
           //------------------------------------------
           beam3_z = tableH-d+ F2H+ F3H;
           beam3_len = tiltBeamForward/virtualH* (virtualH-beam3_z)-d-backboardTh;
           //------------------------------------------
           beam2_z = tableH-d+ F2H;
           beam2_len = tiltBeamForward/virtualH* (virtualH-beam2_z)-d-backboardTh;
           //------------------------------------------
           beam1_z = tableH-d;
           beam1_len = tiltBeamForward/virtualH* (virtualH-beam1_z)-d-backboardTh;
           //------------------------------------------
                           
           module OneSideBeams(dx=0){
               
                module 2x2(l=boardD){ mvy(backboardTh+d) cube( [d,l,d] ); }
                
                mv( dx, -d, beam4_z) 2x2( beam4_len); // top H beam
                mv( dx,  0, beam3_z) 2x2(beam3_len); 
                mv( dx,  0, beam2_z) 2x2(beam2_len); 
                mv( dx,  0, beam1_z ) 2x2( beam1_len); 
                mv( dx, -d, 0) 2x2(); // bottom H Beam
           }
           
           color( usecolor?"green":undef ){
                    OneSideBeams();
                    OneSideBeams(dx=boardW+d);
           } 
           echo(_color( str( "SideHBeam: (2x2x", beam4_len, "): 2"), "green"));
           echo(_color( str( "SideHBeam: (2x2x", beam3_len, "): 2"), "green"));
           echo(_color( str( "SideHBeam: (2x2x", beam2_len, "): 2"), "green"));
           echo(_color( str( "SideHBeam: (2x2x", beam1_len, "): 2"), "green"));
           echo(_color( str( "SideHBeam: (2x2x", boardD, "): 2"), "green"));
           
           if( isdim )
           color("green"){
               
               Dim( [ [shelfW, shelfD,-4]
                    , [shelfW,bTh,-4]
                    , [shelfW,bTh,-10] ] 
                  , opt = update( dimopt, ["h",0.2]));
               
               Dim( [ [shelfW, beam1_len+bTh+d,beam1_z+2]
                    , [shelfW, bTh+d,beam1_z+2], [shelfW,bTh,-10] ] 
                  , opt = dimopt);
               Dim( [ [shelfW, beam2_len+bTh+d,beam2_z+2]
                    , [shelfW, bTh+d,beam2_z+2], [shelfW,bTh,-10] ] 
                  , opt = dimopt);
               Dim( [ [shelfW, beam3_len+bTh+d,beam3_z+2]
                    , [shelfW, bTh+d,beam3_z+2], [shelfW,bTh,-10] ] 
                  , opt = dimopt);
               Dim( [ [shelfW, beam4_len+bTh,beam4_z+2]
                    , [shelfW, bTh, beam4_z+2], [shelfW,bTh,-10] ] 
                  , opt = dimopt);
                 

                Dims( reverse(
                     [ [0, shelfD-4, beam4_z+d]
                     , [0, shelfD-4, beam3_z+d]     
                     , [0, shelfD-4, beam2_z+d]
                     , [0, shelfD-4, beam1_z+d]
                     , [0, shelfD-4, 0]
                     ]), opt = update( dimopt, ["Rf", [ 0,0,0], "h",0.9, "guides",["len",7]]) );
          }      
       }    
       SideBeams();
       
        module Supportbeams(){
            
           module 2x1(l=boardD){ 
               mvxy( d,backboardTh) 
               color(usecolor?"lightblue":undef) cube( [1,l,d] ); 
           }
           module OneSide(dx=0){
               mv( dx,d,tableH-boardH-d ) 2x1(l=boardD/2+ 2*d);
               mv( dx,d,tableH-boardH-d+F2H ) 2x1(l=boardD/2+d);
               mv( dx,d, tableH-boardH-d+F2H+F3H ) 2x1(l=boardD/2+d);
           }    

            module BackHbeams(){
               module 2x2(l=boardW){ color(usecolor?"blue":undef)
                    mvxy(d,backboardTh) cube([l,d,d]);} 
               
               mvz(tableH-boardH-d) 2x2(); 
               mvz(tableH-boardH-d+F2H) 2x2(); 
               mvz(tableH-boardH-d+F2H+F3H) 2x2(); 
            }
            OneSide();
            OneSide( boardW-1 );    
            BackHbeams();
            
            echo(_color( str( "Supportbeam: (2x2x", boardW, "): 4"), "blue"));
            echo(_color( str( "Supportbeam: (2x1x", boardD, "): 6"), "blue"));
           
        }
        Supportbeams();
        
//        SideHbeam();
//        movex( 1.5+boardW ) SideHbeam();
//        BackHbeam();
//        Earbeam();
        
        
    }// .. HBeams 
    HBeams();
    
    //-------------------------------------------
   
    
       arrowpqr= [ [ shelfW, tiltBeamForward/2+2*d, d ]
                 , [ shelfW, tiltBeamForward+d-2, 5 ]
                 , [ shelfW, tiltBeamForward+d, d ]
                 ];
      // MarkPts( arrowpqr, "ch;l=PQR"); 
       if(isdim)
       color("red"){      
           ArcArrow( pqr = arrowpqr, rad=4
                     ,opt=["r",0.1] );
           Write( str(tilt_a,chDEG)
                , [ [ shelfW, tiltBeamForward+d, d*2 ]
                  , [ shelfW, tiltBeamForward/2+d, d*2 ]
                  , [ shelfW, tiltBeamForward/2+2*d, 10 ]
                  ]
                , opt= ["size",5, "scale",0.2]
                );  
       }    
                 
  }//.. Shelf    
  
  //scale(50)ColorAxes();
  //mvxy(1,2) Shelf();
  Shelf();
  mvx(31) Table(); 
  Room();
  
  /*BOM:
        VBeam: (2x2x63) : 2
        TiltBeam: (2x2x63.2083) : 2
        Hboard: ( 22.625x18.125x0.7 ): 4
        SideHBeam: (2x2x10.4634): 2
        SideHBeam: (2x2x7.93512): 2
        SideHBeam: (2x2x8.61642): 2
        SideHBeam: (2x2x9.40254): 2
        SideHBeam: (2x2x18.125): 2
        Supportbeam: (2x2x22.625): 4
        Supportbeam: (2x1x18.125): 6
  */
}
//Piggie_Corner_Shelf_20150902( isdim=0, usecolor=false);

module Bend_dev_20150904(){  // FAILED 
    
   echom("Bend_dev_20150904");
   echo(_blue("Bend attempt. Approach: 
    Something like the obj segmentation in Bend_dev_20150901,
    but (1) apply shielding before Move() (i.e., when obj is still
    on the ORIGIN) (2) apply tilting of obj segments after shielding
    and before move to make a tapering effect (3) Union() 3 obj segs 
    into one (before Move()): tilt_left, obj_seg, tilt right (4) Move()
    "));
    
   arc_n=8;
   objheight=1;
   rad = 10;
    
   //$fn=24;
    
   module Bend( pts
              , seglen=seglen
              , objheight=objheight
              , shieldingCubeSize=60 
              ){

       module PreBlock(i){
            color(undef, 0.3) 
            //mvx( -cs/2+ seglen*i) 
            mvx( -cs/2 )
            cube( [cs,cs,cs], center=true); 
       }       
       module PostBlock(i){
            color("red", 0.3) 
            //mvx( cs/2+seglen*(i+1)) 
            mvx( cs/2+seglen ) 
            cube( [cs,cs,cs], center=true); 
       }  
       
       module Taper(){
            a = arcang/nseg;
            union(){
                children();
                Transform( xoz= a ) children(); // tilt z to x
                Transform( xoz=-a ) children(); // tilt z to -x
            }
       }
       
       // Move the obj seg
       module Moveto(i){
            Move( to= [ pts[i+1], pts[i], N( [pts[i+1],pts[i],Q]) ] )
            translate( [ -seglen*i, 0, -objheight] ) 
            children(); 
       }    
       
       Q = pqr[2]; // pts[1];
       //MarkPts( pts,"ch;r=0.2" );

//       // Plot the normal arrows //
//       for(i=range(pts)){ 
//            pi = pts[i];
//            pii= und(pts[i+1], extPt( get(pts,[-2,-1])));
//            N = N( [pii,pi,Q], len=15);
//            Arrow2( [ onlinePt([pi,N], len=-15), N], "r=0.1;cl=yellow");
//       }
       
       module CutSeg(i=i){
           difference(){ 
                mvx( -i*seglen )children();
                PreBlock(i);
                PostBlock(i);
           }
       }               
       cs = shieldingCubeSize;
       //i=0;
       //for(i=[0:20])
       for(i=[0:len(pts)-2]){
           //difference(){
           //Moveto(i) children();
           
           Move( to= [ pts[i+1], pts[i], N( [pts[i+1],pts[i],Q]) ] )
           Taper()
           CutSeg(i=i) children();
       }

   }//..Bend
 
   bonedata = randchain(n=3, a=[60,120], seglen=10); //pqr(p=30,r=30);
   pqr = hash(bonedata, "pts");
   pqr = [[-0.881903, -0.253075, 2.38609], [0.465149, 2.97525, -0.61697], [-2.54546, 2.39592, -1.81612]];
   echo(pqr=pqr);
   //MarkPts( pqr, "ch;l=PQR;r=0.2");  
   txt = "Scadx";
   
   pts = arcPts( pqr, n=arc_n, rad=rad+objheight );
   //MarkPts( pts, "ch");
   arcang = angle(pqr);
   arclen = arcang/360*2*PI*(rad+ objheight/2);
   nseg = arc_n-1; 
   seglen = arclen / nseg;  
       
   Bend(pts) 
   linear_extrude(objheight) 
   text(txt, size= textWidth(txt)/arclen);
   
  
   
}//..Bend_dev_20150904
//Bend_dev_20150904();

module Bend_dev_20150904_2( istextcurved=false ){ // SUCCESS !! 
            // istextcurved makes the char surface curved as well. It
            // don't seem to be critical because non-curved char looks
            // pretty good. Besides, it slows down significantly.
    
   echom("Bend_dev_20150904_2");
   echo(_blue("Bend attempt. Approach: 
    Unlike previous 2 approaches that cut text into even segs, display
    txt char by char (so no shielding, no taper)."));
    
   //$fn=24;
    
   module Bend( txt
              , pqr
              , objH= 2
              , rad = 10
              , ang = 60
              , rot = [0,0,0]
              ){
                  
        arclen = 2*PI*rad* ang/360;
        
        /// width for each char           
        ws= [ for(i=range(txt)) textWidth(txt[i]) ];
        echo( ws = ws );    
        txtW = sum(ws);
        
        /// char width adjusted to requested angle (=ang )
        adjws = [ for(w=ws) w* arclen/txtW ];
        echo( adjws = adjws );    
        
        /// ang for each char
        angs = [ for( w=adjws ) anglebylen( rad,w,rad ) ];
        echo( angs = angs );
        echo( sum_angs = sum( angs ) );    

        /// accumulated ang for each char
        accumangs = concat( [0], accum( angs ) );
        echo( accumangs = accumangs );
        
        _pts = [ for(i=range(txt)) anglePt( pqr, a=accumangs[i], len=rad) ];
        pts = concat( _pts
                    , [anglePt( pqr, a = last(accumangs)+last(adjws),len=rad)]
                    );
        //MarkPts( pts, "ch" );
        
        module WriteText(){
            for( i = range(txt) ){
                
                Pi = pts[i];
                Pj = pts[i+1];
                //Rf = extPt( [pqr[1],Pi] );
                Rf = -4*pqr[1]+5*Pi; // Get a far pt on the Pi side
                N  = N( [Pj,Pi, i==len(txt)-1?pts[0]:last(pts)] );
                Move( to= [Pj,Pi,N] ) 
                    scale( arclen/txtW )
                    rotate( rot )
                    linear_extrude( objH*2 )
                    text( txt[i], $fn=36 );
            }  
        }
        
        module InnerShield(){
            
            Move( to= pqr )
            mvz( objH/2 )
            cylinder( r=rad, center=true, h= objH*10, $fn=48 );
        }   
       
        module OuterShield(){
        
            
            Move( to= pqr )
            mvz( objH/2 )
            cylinder( r=(rad+ objH/2) // * 0.8 //cos(ang
                    , center=true, h= objH*10
             );
            
            
        }

        if(istextcurved){
            intersection(){
                difference(){
                    WriteText();
                    InnerShield();
                }
                OuterShield();
            }
        }else { 
            WriteText(); }    
            
   }//..Bend
 
   bonedata = randchain(n=3, a=[60,120], seglen=10); //pqr(p=30,r=30);
   pqr = hash(bonedata, "pts");
   pqr = [[-0.881903, -0.253075, 2.38609], [0.465149, 2.97525, -0.61697], [-2.54546, 2.39592, -1.81612]];
   //pqr = [ [1,0,0],[0,0,0],[0,1,0]];
   echo(pqr=pqr);
   //MarkPts( pqr, "ch;l=PQR;r=0.2");  
   txt = "Parallel"; //"Scadx on Arc";
   
     Bend( txt
         , pqr
         , objH= 2
         , ang = 60
         );

   color("red")
   Bend( txt
         , trslN(pqr, 5)
         , objH= 2
         , ang = 90
         , rot=[90,0,0]
         );


   color("green")
   Bend( txt
         , trslN(pqr, 8)
         , objH= 2
         , ang = 60
         , rot=[0,-90,0]
         );
         
}//..Bend_dev_20150904_2
//Bend_dev_20150904_2( istextcurved=false);

module sawhorse_dev_150905(){
    echom("sawhorse_dev_150905");
    
    d=1.5; // w and h of a 2x2
    2d= 2*d;
    module 22L(l=5){ cube([d,d,l]);}
    module 2L2(l=5){ cube([d,l,d]);}
    module L22(l=5){ cube([l,d,d]);}
    module 12L(l=5){ cube([d/2,d,l]);}
    module 1L2(l=5){ cube([d/2,l,d]);}
    module 21L(l=5){ cube([d,d/2,l]);}
    module 2L1(l=5){ cube([d,l,d/2]);}
    module L12(l=5){ cube([l,d/2,d]);}
    module L21(l=5){ cube([l,d,d/2]);}
    module 42L(l=5){ cube([2d,d,l]);}
    module 4L2(l=5){ cube([2d,l,d]);}
    module 24L(l=5){ cube([d,2d,l]);}
    module 2L4(l=5){ cube([d,l,2d]);}
    module L42(l=5){ cube([l,2d,d]);}
    module L24(l=5){ cube([l,d,2d]);}

    function 42LPts(l=5)= let(d=1.5,2d=2*d)
      [ [0,0,0], [0,d,0], [2d,d,0], [2d,0,0]
      , [0,0,l], [0,d,l], [2d,d,l], [2d,0,l] ]; 
    function L24Pts(l=5)= let(d=1.5,2d=2*d)
      [ [0,0,0],  [0,d,0],  [l,d,0],  [l,0,0]
      , [0,0,2d], [0,d,2d], [l,d,2d], [l,0,2d] ];
    function L42Pts(l=5)= let(d=1.5,2d=2*d)
      [ [0,0,0],  [0,2d,0],  [l,2d,0], [l,0,0]
      , [0,0,d], [0,2d,d], [l,2d,d], [l,0,d] ];
    module 42LP(l=5){ polyhedron( points=42LPts(l), faces=CUBEFACES);}
    module L24P(l=5){ polyhedron( points=L24Pts(l), faces=CUBEFACES);}
    module L42P(l=5){ polyhedron( points=L42Pts(l), faces=CUBEFACES);}
    

    
    ang_legopen= 30; /*   -----
                          --|--
                           / \
                          /   \
                         /  a  \
                     */
                      
    ang_legtilt= 30;  /*  -------------------
                          -------------------
                             /|          \
                            / |           \
                           / b|            \
                     */ 
   
    ang_leg2z = angleAfter2Rots(ang_legtilt, ang_legopen/2); 
    
    H = 30; 
    legL = H/cos( ang_leg2z);
    echo( ang_leg2z= ang_leg2z, legL = legL);
    
    
    module MainHBeam(){
     
        
     
     
    }    
    
    
    module Legs(){
        
        leglen = 35;
        
        module Leg(){
            
             axa_xy = 30; // axis angle on xy surface, away from x-ax 
             axa_xz = 30; // axis angle on xz surface, away from x-ax
            
             axlen = 10;
             axpt = [ axlen*cos( axa_xy )
                    , axlen*sin( axa_xy )
                    , axlen*sin( axa_xz )
                    ];
             PtGrid( axpt, ["mode",3, "r",0.05]);
             pts = L24Pts( leglen );
             mvy(-2d-d) L24P( leglen );
             ax = [ 3*[2d,-d,-2d], ORIGIN ];
             Arrow2( extendPts(ax,5) ,opt="r=0.2;cl=yellow");
             
             pts1= rotPts( pts, ax, 180);
             //MarkPts( pts1, "ch");
             polyhedron( points=pts1, faces=CUBEFACES );
            
        }    
        
        
        Leg();
//        
//        sq3 = sqrt(3);
//        lbz= H; //3*lbx; 
//        lbx= 5;
//        lby= lbx*sq3/3;
//        
//        PtGrid( [ -lbx,-lby,-lbz], ["mode",3,"r",0.05] );
//        
//        /// opened leg:
//        
//        //Transform( xoz=30, yoz=30) rotate( L24(30);
//        
//        olpts_bot = [ [-lbx,-lby,-lbz]
//                , [-lbx+ 3*cos(ang_leg2z), -lby, -lbz]
//                , [-lbx+ 3*cos(ang_leg2z), -lby-d, -lbz]
//                , [-lbx, -lby-d, -lbz]
//                ]; 
//        olpts_top = [ [0,0,0]
//                    , [d/sq3, 0,0]
//                    , [d/sq3, -d,0]
//                    , [0, -d,0]
//                    ];
//        Plane( olpts_bot );
//        Plane( olpts_top );
//        
//        polyhedron( points= concat( olpts_bot, olpts_top)
//                  , faces = faces( "rod", nside=4)
//                  ); 
//        
        //pts_legbox= [ORIGIN, [0,-lbY,0], 
        
        //color("red") 
        //multmatrix( m=rotM( [ [], ORIGIN], 180))
        //Leg();
        //Move( to= to= [ [2,0,-1],ORIGIN,[0,-1,0]]
        //                 , from=[ [2,0,-1],ORIGIN,[0,0,1]]
        //                 ) Leg(); 
        //rotate([ 90,30,0]) Leg();
        
        /*
        ;; To apply this to OpenScad object (non-polyh):
;;
;;    translate( axis[1] ) 
;;    multmatrix(m= rotM( axis,a) ) 
;;    {
;;       translate( loc-axis[1] ) // loc: where obj is in xyz coord
;;       cube( [3,2,1] );
;;    }
        */
        
    }    
    
    //L24(30);
    Legs();
    
}
//sawhorse_dev_150905();

module Enhanced_saw_150905(){
    
    echom("Enhanced_saw_150905");
    d=1.5; // w and h of a 2x2
    2d= 2*d;

    module 22L(l=5){ cube([d,d,l]);}
    module 2L2(l=5){ cube([d,l,d]);}
    module L22(l=5){ cube([l,d,d]);}
    module 12L(l=5){ cube([d/2,d,l]);}
    module 1L2(l=5){ cube([d/2,l,d]);}
    module 21L(l=5){ cube([d,d/2,l]);}
    module 2L1(l=5){ cube([d,l,d/2]);}
    module L12(l=5){ cube([l,d/2,d]);}
    module L21(l=5){ cube([l,d,d/2]);}
    module 42L(l=5){ cube([2d,d,l]);}
    module 4L2(l=5){ cube([2d,l,d]);}
    module 24L(l=5){ cube([d,2d,l]);}
    module 2L4(l=5){ cube([d,l,2d]);}
    module L42(l=5){ cube([l,2d,d]);}
    module L24(l=5){ cube([l,d,2d]);}
    
    //-----------------------------------------
    
}    
//Enhanced_saw_150905();    

