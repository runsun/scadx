include <scadx.scad> //_core.scad>
//include <scadx_edgeface.scad>
include <../doctest/doctest.scad>
//include <scadx_ref.scad> // need version info
//include <scadx_test_chainData.scad> // scadx_objtest needs this




FORHTML=false;
MODE=10;
SILENT = 1;

/*

scadx_tests() ===>  show help message

scadx_tests(FILES_TO_TEST); // All functions in all files: 

2 ways to test a file:

   scadx_file_test(fname="num", mode=11);  
   scadx_tests("num");  

Test multiple files:

   scadx_tests(["num", "array"]);  

Test a func:

    echo( numstr_test(mode=12)[1] ;  // test function
    
--------------------------------------------------------

Command line:

>>> openscad  --enable=lc-each scadx_tests.scad -o dump.stl -D "scadx_tests(FILES_TO_TEST)" 2> scadx_tests.log


Note: when running openscad from the command line, use "2> <output_file>":
 
#  openscad  --enable=lc-each scadx_tests.scad -o dump.stl -D "scadx_tests(FILES_TO_TEST)" 2> scadx_tests.log

But when use python to run it, we need to use ">> <output_file>"

cmd = 'openscad --enable=lc-each ../scadx_tests.scad -o dump.stl -D "scadx_tests(FILES_TO_TEST)" >> scadx_tests.log'
os.system(cmd)

*/

/*
time python ~/Documents/prog/openscad/doctest/runscad_echo.py ~/Documents/prog/openscad/scadx/scadx_tests.scad 'FORHTML=true;MODE=112' scadx_core_api.htm
*/


/* 

/* doctest modes:

   000, "Demo mode=0: No doc, no test, no usage. Show title only."
  ,002, "Demo mode=2: No doc, no test. Show title and usage (all test cases w/o test)."
    
  ,010, "Demo mode=10: No doc, tested. Show only title indicating 7 fails"
  ,011, "Demo mode=11: No doc, tested. Show title and 7 failed cases"
  ,012, "Demo mode=12: No doc, tested. Show title and all test cases."
    
  ,100, "Demo mode=100: show doc, no test, no usage. Show title and doc."
  ,102, "Demo mode=102: show doc, no test. Show title, doc and usage (all test cases w/o test)"
    
  ,110, "Demo mode=110: show doc, tested. Show doc and title indicating 6 fails"
  ,111, "Demo mode=111: show doc, tested. Show title, doc and 7 failed casess"
  ,112, "Demo mode=112: show doc, tested. Show title, doc and all tests"
*/

   
FILES_TO_TEST=[
        "num"     
      , "array"   
      , "core"
      , "range"   
      , "hash"    
      , "inspect"
      , "faces" 
      , "geometry"
      , "rand"
      , "string"  
      , "log"    
      , "eval"   
      , "match"  
      , "error"  
      , "args"   
      , "animate" 
      , "subdiv"
];   



//===================================================
//===================================================

module scadx_doctests( mode=MODE, opt=[], FOR=undef) //FOR ) 
{
     /* Run from command line:
   
     >>> time python ../doctest/runscad_echo.py scadx_tests.scad 'FORHTML=true;MODE=112' scadx_api_tmp.htm

     Or:

     >>> openscad scadx_tests.scad -o dump.stl -D "FORHTML=false;MODE=112" 
    
     >>> openscad scadx_tests.scad -o dump.stl -D "FORHTML=true;MODE=112" 2> scadx_api_tmp.htm
    
     */
     opt = update( ["showmode",false], opt );
    
     results = concat(
   
         geometry_tests( mode=mode, opt=opt )
   
     );  
    
    doctests( title="Scadx doc & tests"
            , subtitle= str("scadx version: ", SCADX_VERSION)
            , mode=MODE
            , results=results
            , htmlstyle=htmlstyle 
            , FOR=FOR
            );
    
}   


function get_all_test_results()=
(
  /*
   A get_scadx_???_test_results() returns:  [ [ info, rtn, ntest, nfail ] 
                                            , [ info, rtn, ntest, nfail ] 
                                            , [ info, rtn, ntest, nfail ] 
                                            ...
                                            ]  
  */
  let(
      MODE= 10
     , files0=
      [
      //, "funclet" , get_scadx_funclet_test_results( mode=MODE, opt=["showmode",false] )
      //, 
      "num"     , get_scadx_num_test_results( mode=MODE, opt=["showmode",false] )
      , "array"   , get_scadx_array_test_results( mode=MODE, opt=["showmode",false] )
      , 
      "range"   , get_scadx_range_test_results( mode=10, opt=["showmode",false] )
      , "hash"    , get_scadx_hash_test_results( mode=MODE, opt=["showmode",false] )
      , "inspect" , get_scadx_inspect_test_results( mode=MODE, opt=["showmode",false] )
      , "geometry", get_scadx_geometry_test_results( mode=MODE, opt=["showmode",false] )
      , "string"  , get_scadx_string_test_results( mode=MODE, opt=["showmode",false] )
     // , "log"     , get_scadx_log_test_results( mode=MODE, opt=["showmode",false] )
     // , "eval"    , get_scadx_eval_test_results( mode=MODE, opt=["showmode",false] )
     // , "match"   , get_scadx_match_test_results( mode=MODE, opt=["showmode",false] )
      , "error"   , get_scadx_error_test_results( mode=MODE, opt=["showmode",false] )
      //, "args"    , get_scadx_args_test_results( mode=MODE, opt=["showmode",false] )
      //, "animate" , get_scadx_animate_test_results( mode=MODE, opt=["showmode",false] )
      ]
    , files= [ for(f=[0:len(files0)-1]) 
                 f%2? [ for(func0=files0[f]) 
                         let(func=slice(func0,1)) // func:["num ( x )=number ( tested:29 )", 29, 0]
                         replace( func,0
                                , last(split(split(func[0], "</b")[0], ">"))  
                                ) //=>  ["num", 29, 0]
                      ]
                    : files0[f] 
               ]
             //  files=> ["num", [ ["fracstr", 49, 0]
             //                  , ["fullnumstr", 110, 0]
             //                  , ["getPowerOrder", 13, 0]
             //                  , ... ]
             //          ,"array", ... 
             //          ]
    , nfiles = len(files)/2             
    , nfuncs = sum( [for (r=vals(files)) len(r)] )  
    , ntests = sum( [for (r=vals(files)) sum(getcol(r, 1))] )
    , nfails = sum( [for (r=vals(files)) sum(getcol(r, 2))] )
    , failedFiles = nfails?
                    [ for(fn=keys(files)) 
                         let(nfails = sum(getcol( hash(files,fn), 2))
                            )
                         if(nfails) each( [fn, nfails] )    
                    ]
                    :[]
    //, fails = [ for(i=[0:len(files)-1]) 
                  //let(v = files[i%2?i:(i+1)]
                     //,failed = sum( getcol( v, 2) )
                     //)
                  //if(failed) v  
                  //]
    
    , return = str( str( ("Scadx tests: ")
                          , _color( str("files: ", _b(nfiles)), "purple")
                          , ", " 
                          , _color( str("funcs: ", _b(nfuncs)), "teal")
                          , ", " 
                          , _color( str("tests: ", _b(ntests)), "darkblue")
                          , ", " 
                          , _color( str("fails: ", _b(nfails)), nfails?"red":"green")
                          , ", "
                          , _color( str("%: ", _b(numstr(100*(ntests-nfails)/ntests), d=2)), "green" ) 
                          )
                      
                  , "<br/>"
                  , "files: ", keys(files)
                  , failedFiles? _red(str(", fails: ", failedFiles)) :"" 
                  ) 
                     
  )   
  return

);

//echo(  get_all_test_results() );

//module scadx_tests( mode= MODE
//                  , opt=[]
//                  , objtest_isdetails= 0 
//                  ) //, FOR= FOR)
//{
//    opt = update( ["silent",SILENT], opt );
//    
//    results= get_scadx_num_test_results( title="scadx_num Doctests"
//    , mode=MODE, opt=["showmode",false] );
//
//    for(x=results ) echo( x[1] );
//    
////    doctests( title="Scadx doc & tests"
////            , subtitle= str("scadx version: ", SCADX_VERSION)
////            , mode=MODE
////            , results=results
////            , htmlstyle=htmlstyle 
////            , FOR=FOR
////            );
//    
//    //scadx_doctests( mode = doctest_mode, opt = doctest_opt, FOR=FOR );
//    //scadx_objtests( isdetails = objtest_isdetails );                  
//}                      


//FOR=FOR_OS;
//FOR=FOR_OPENSCAD;
SILENT=0;
//MODE = 11;
//scadx_tests( doctest_mode=MODE
//           , doctest_opt=["silent",SILENT]
//           , FOR=FOR
//           ); //MODE );
//scadx_tests( doctest_mode=MODE
//           , doctest_opt=["silent",SILENT]
//           , FOR=FOR_OPENSCAD
//           ); //MODE );
//scadx_tests( doctest_mode=MODE
//           , doctest_opt=["silent",SILENT]
//           , FOR=FOR_WEB
//           ); //MODE );

//echo( index_test(mode=12)[1] );
//echo( num_test(mode=112)[1] );
//echo( numstr_test(mode=12)[1] );
//echo( slice_test(mode=112)[1] );
//fullnumstr(1234.5);

//results= fullnumstr_test(mode=12);
//echo( results[1]); echo( _span(str(_green(results[2]), "/",_red(results[3])),s="font-size:24px"));

// fullnumstr(8000001) want: "8000001" got: "8000000.1")

//echo( _red("fullnumstr(800000001) (should be '800000001')= "), fullnumstr(800000001));
//echo("");
//echo( _red("fullnumstr(8000000001) (should be '8000000001')= "), fullnumstr(8000000001));

//echo( "fullnumstr(80000000001) (should be '80000000001')= ", fullnumstr(80000000001));
//echo(fullnumstr(0.0111)); // want: "0.0111" got: "0.00111"

//fullnumstr_more_tests(ntest= 10);

//echo( fullnumstr_try( 3.0000002 ) );
//echo( fullnumstr_try( 300.00000002 ) );
//echo( fullnumstr_try( 3.123456 ) );

//scadx_tests(mode=12);
//echo( uconv_test( mode=12 )[1] );
//echo( getPowerOrder_test( mode=12 )[1] );
//echo( fullnumstr_test( mode=12 )[1] );
//echo( fracstr_test( mode=12 )[1] );
//echo( intstr_test( mode=12 )[1] );
//
//for(i=[1:16])
//  echo(i, 1/pow(10,-i+1));
//
//echo("<br/>");

//echo(str(" i", "\t", "b", "\t","c", "\t","d","\t","e"));
//echo(str("---", "\t", "---", "\t","---", "\t","---","\t","-------"));
//a= 8;  
//for(i=[1:16])
//{
//   b= a*pow(10,i);
//   c= b+0.1;
//   d = c-b;
//   e = (c*10-b*10)/10;
//   f = ((10*c) %10)/10;
//   echo( str(i, "\t", b, "\t",c, "\t",d,"\t",e, "\t", f));
//}  
//echo("<br/>");

//aa= 8;  
//p = 10; //pow(10,15);
//for(i=[1:16])
//{
//   bb= aa*pow(10,i);
//   cc= bb+0.1;
//   dd = (cc*p-bb*p)/p;
//   //e = d*pow(10,i+1)/10;
//   echo( i, "\t", bb, cc, dd);
//}  


//echo( _red("intstr(123456):"), intstr(123456) );
//echo( _red("intstr(123401):"), intstr(123401) );
//echo("");
////echo( _red("intstr(1234012):"), intstr(1234012) );
//echo("");
//echo( _red("intstr(12345678901):"), intstr(12345678901) );
//echo("");
//echo( _red("intstr(123456789012):"), intstr(123456789012) );
//echo("");
//echo( _red("intstr(123456789001):"), intstr(123456789001 ) );
//echo("");
//echo( _red("intstr(1234567890001):"), intstr(1234567890001 ) );
//
//echo( _red("intstr(1234501):"), intstr(1234501) );
//echo( _red("intstr(800001):"), intstr(800001) );
//echo("");
//echo( _red("intstr(8000001):"), intstr(8000001) );
//echo("");
//echo( _red("intstr(80000001):"),  intstr(80000001) );
//echo("");
//echo( _red("intstr(800000001):"), intstr(800000001) );
//echo("");
//echo( str(_red("intstr(80002000300001):"), "<br/>", intstr(80002000300001) ));
//echo("");
//echo( str(_red("intstr(800020000300001):"), "<br/>", intstr(800020000300001) ));
//echo("");
//echo( _red("intstr(800200030004001):"), intstr(800200030004001) );

//echo("");
//echo( _red("intstr(12345678901234500):"), intstr(12345678901234500) );
//echo("");
//echo( _red("intstr(800020000300001):"), intstr(800020000300001) );
//echo("");
//echo( _red("intstr(800200030004001):"), intstr(800200030004001) );
//echo("");
//echo( _red("intstr(800000000000):"), intstr(800000000000) );



//echo("fracstr( 0.123456789012345)=", fracstr( 0.123456789012345));

//scadx_num_tests(); //mode=12);
module scadx_file_test_0(title, results, showEach=false)
{
  /*
     Display testing results of a scadx file.
     
     - title: like "scadx_num" (= file name w/o ".scad")
     - results: an array of doctest results like:
     
                [ [ info, rtn, ntest, nfail ]
                , [ info, rtn, ntest, nfail ]
                ...  ]
     
                It can be obtained with a PAGE-TEST function defined in the target file, 
                like:
                 
                get_scadx_num_test_results( mode=MODE, opt=["showmode",true] )
  
     - showEach: if false (default), this module echos only one line:
     
                scadx_num tests: failed/tests = 0/321
                
                If true, individual records will be shown. The extent of details
                is determined by the *mode* when obtaining the results (not controlled
                by this module).
                
     Typical PAGE-test function:
     
     	function get_scadx_num_test_results( mode=MODE, opt=["showmode",true] )=
      	(
          [ fracstr_test( mode, opt )
          , ...
          ]
      	);
      	
    Then we do:
    
    	scadx_file_test( title= "scadx_num"
                       , results= get_scadx_num_test_results(...) 
                       , showEach = false
                       );
  */  
  
  total= sum([ for(r=results) r[2] ]);
  failed= sum([ for(r=results) r[3] ]);

  if(showEach)  
  	for(r = results) echo( r[1] );
  
  echo(_s( _span("scadx_file_test(title=\"<b>{_}</b>\"): failed/tests = {_}/{_}", s="font-size:16px")
         , [  title, failed?_red(failed):failed, _blue(total) ] 
         ) 
      );  
  
  if(failed)
     echo( _s("<br/>{_} fails out of {_}. Set showEach=true to see details: <br/>
              <b>scadx_file_test(title=\"{_}\", results=..., {_})</b> <br/>"
             , [_red(failed), _blue(total), title, _purple("showEach=true") ] 
             ));
                   
}

function arr_file_test_result(fname, mode=MODE, opt=[])=
(
   /*  returns: [ [ info, rtn, ntest, nfail ] 
                , [ info, rtn, ntest, nfail ] 
                , [ info, rtn, ntest, nfail ] 
                ...
                ]  
   */
   //echo(str("arr_file_test_result(\"",fname,"\")"))
   let( opt = update(["showmode", false], opt) )
   fname=="scadx_num"? get_scadx_num_test_results( mode=mode, opt=opt )
  :fname=="scadx_array"? get_scadx_array_test_results( mode=mode, opt=opt )
  :fname=="scadx_core"? get_scadx_core_test_results( mode=mode, opt=opt )
  :fname=="scadx_range"? get_scadx_range_test_results( mode=mode, opt=opt )
  :fname=="scadx_hash"? get_scadx_hash_test_results( mode=mode, opt=opt )
  :fname=="scadx_inspect"? get_scadx_inspect_test_results( mode=mode, opt=opt )
  :fname=="scadx_faces"? get_scadx_faces_test_results( mode=mode, opt=opt )
  :fname=="scadx_geometry"? get_scadx_geometry_test_results( mode=mode, opt=opt )
  :fname=="scadx_rand"? get_scadx_rand_test_results( mode=mode, opt=opt )
  :fname=="scadx_string"  ? get_scadx_string_test_results( mode=mode, opt=opt )
  :fname=="scadx_log"? get_scadx_log_test_results( mode=mode, opt=opt )
  :fname=="scadx_eval"? get_scadx_eval_test_results( mode=mode, opt=opt )
  :fname=="scadx_match"? get_scadx_match_test_results( mode=mode, opt=opt )
  :fname=="scadx_error"? get_scadx_error_test_results( mode=mode, opt=opt )
  :fname=="scadx_args"? get_scadx_args_test_results( mode=mode, opt=opt )
  :fname=="scadx_animate"? get_scadx_animate_test_results( mode=mode, opt=opt )
  :fname=="scadx_subdiv"? get_scadx_subdiv_test_results( mode=mode, opt=opt )
  : []
);

function get_file_test_summary(test_results, _rtn=[], _i=0)=
(
   /*  test_results: 
               [ [ info, rtn, ntest, nfail ] 
               , [ info, rtn, ntest, nfail ] 
               , [ info, rtn, ntest, nfail ] 
               ...
               ]
      return:  [ "nfunc", nfunc
               , "ntest", ntest
               , "nfail", nfail
               , "tested", arr_func_names
               , "fails", [ "func1", 2, "func2",1 ]
               ]                       
   */
   let( rec = test_results[_i]
      , funcname= rec[0][0] 
      , rtn= _i==0
              ? [ "funcs", [funcname]
                , "ntest", rec[2]
                , "nfail", rec[3]
                , "fails", rec[3]? [ funcname, rec[3] ]:[]  
                ]
              : [ "funcs", concat( hash(_rtn, "funcs"), [funcname] )
                , "ntest", rec[2]+ hash(_rtn, "ntest")
                , "nfail", rec[3]+ hash(_rtn, "nfail")
                , "fails", update( hash(_rtn, "fails"), rec[3]? [ funcname, rec[3] ]:[])  
                ]
      )
   _i>=len(test_results)
   ? update( ["nfunc", len(test_results)], _rtn )
   : get_file_test_summary(test_results, _rtn=rtn, _i=_i+1)
);

module scadx_file_test(fname, mode=12, summaryOnly=true, opt=[])
{
  //echom(str("scadx_file_test(\"",fname,"\")"));
  results = arr_file_test_result(fname, mode=mode, opt=opt);
  summary= get_file_test_summary ( results );
  
  //echo(results0=results[0]);
  //echo(summary=summary);
  if(!summaryOnly)
  { 
    echo( _u(_span(_s("{_}.scad",fname)
          , s="color:navy;font-size:14px;font-weight:900")));
    for( rec = results ) echo( rec[1] );
  }
  echo( 
    _fmth( 
      update( ["file",fname], delkey( summary, "funcs") )
    ) 
  );
}


//module scadx_files_test( fnames=["scadx_array","scadx_num"]
//                       , isOneLine=false, mode=10,  opt=[]) 
//{  /*
//   // 2018.10.5
//   
//   >>> scadx_files_test(fnames=["scadx_inspect","scadx_string"]);
//   
//   isOneLine=false (default):
//   
//   "[file="scadx_inspect", nfunc=42, ntest=270, nfail=17, fails=["endword", 17]]"
//   "[file="scadx_string", nfunc=19, ntest=183, nfail=3, fails=["_table", 1, "lcase", 1, "ucase", 1]]"
//   "[nfunc_total=61, ntest_total=453, nfail_total=20, fails=[["scadx_inspect", ["endword", 17]], ["scadx_string", ["_table", 1, "lcase", 1, "ucase", 1]]]]
//
//   isOneLine=true:
//   "[nfunc_total=61, ntest_total=453, nfail_total=20, fails=[["scadx_inspect", ["endword", 17]], ["scadx_string", ["_table", 1, "lcase", 1, "ucase", 1]]]]
//
//   */
//   results= [ for(fn=fnames) 
//             //echo(fn=fn)
//              let(res= arr_file_test_result(fn, mode=mode, opt=opt) 
//                 ,summary= get_file_test_summary ( res ))
//              update( ["file",fn], delkey( summary, "funcs") )
//            ];    
//   if(!isOneLine)
//   for(res=results) echo(_fmth(res));// "[file="scadx_string", nfunc=19, ntest=183, nfail=3, fails=["_table", 1, "lcase", 1, "ucase", 1]]
//   
//   nfunc = dt_sum([ for(res=results) dt_hash(res, "nfunc") ]);
//   ntest = dt_sum([ for(res=results) dt_hash(res, "ntest") ]);
//   nfail = dt_sum([ for(res=results) dt_hash(res, "nfail") ]);
//   fails = [ for(res=results) 
//              if(dt_hash(res,"nfail"))
//              [dt_hash(res,"file"), dt_hash(res, "fails")] ];
//    
//   totalSummary= [ 
//      "nfunc_total", nfunc
//      ,"ntest_total", ntest
//      ,"nfail_total", nfail
//      ,"fails", fails
//   ];
//   
//   echo(_span(_fmth(totalSummary),s="font-size:16px"));
//}



function h_Files_test_results(fnames=FILES_TO_TEST, mode=MODE, opt)=
( 
  /*= [ "num",   [ [ info, rtn, ntest, nfail ] 
                 , [ info, rtn, ntest, nfail ] 
                 , [ info, rtn, ntest, nfail ] 
                 ...
                 ] 
      , "array",  [ info, rtn, ntest, nfail ] 
                  , [ info, rtn, ntest, nfail ] 
                  , [ info, rtn, ntest, nfail ] 
                  ...
                  ]
      ...            
      ]          
  */ 
  let( fns= isstr(fnames)?[fnames]
           :isarr(fnames)?fnames
           :[]
     , opt= update(["showmode", false], opt)       
     )
  [ for(fn=fns) each [fn, arr_file_test_result(fn, mode=mode, opt=opt)] ]
);


module scadx_tests(fnames, display=1, html=undef)
{
  /*
  Note: when running openscad from the command line, use "2> <output_file>":
 
     > openscad  --enable=lc-each scadx_tests.scad -o dump.stl -D "scadx_tests(FILES_TO_TEST)" 2> scadx_tests.log

  But when use python to run it, we need to use ">> <output_file>"

    cmd = 'openscad --enable=lc-each ../scadx_tests.scad -o dump.stl -D "scadx_tests(FILES_TO_TEST)" >> scadx_tests.log'
    os.system(cmd)
    
  */
  
  if(fnames==undef||fnames=="?"||fnames=="h"||fnames=="help")
  {
     echo( "You called scadx_tests():");
     echo( "");
     echo( ">>> scadx_tests( ) ==> show this message");
     echo( ">>> scadx_tests( FILES_TO_TEST ) ==> All functions in all files" ); 
     echo( _s(">>> FILES_TO_TESTS= {_}", str(FILES_TO_TEST)));
     echo( ">>> scadx_tests( [\"array\",\"hash\"] ) ==> All functions in array and hash (html default=0)" ); 
     echo( ">>> scadx_tests( \"array\" ) ==> All functions in array (html default=1)" ); 
     echo( ">>> scadx_tests( ... display=2 ) ==> Show more details" ); 
     echo( ">>> scadx_tests( ... html=1 ) ==> Turn on html-styling" ); 
     echo( ">>>" );
     echo( ">>> scadx_file_test( fname=\"num\", mode=11 ) ==> file test: test scadx_num.scad" );
     echo( ">>> echo( numstr_test( mode=12 )[1]) ==> test single function" );
  }
  else
  {
    html= html==undef && isstr(fnames)?1:html;
    
    fnames=[ for(fn=fnames) begwith(fn,"scadx")?fn:str("scadx_",fn) ];
    h_results = h_Files_test_results(fnames=fnames, mode=10);
                // h_results: [ "scadx_num",   [ [ info, rtn, ntest, nfail ] 
                //                             , [ info, rtn, ntest, nfail ] 
                //                             , [ info, rtn, ntest, nfail ] 
                //                             ... ] 
                //             , "scadx_array", [[["accum", "arr", "array", "Array", "Given [a,b,c...], return [a,a+b,a+b+c...] "], "accum ( arr )=array ( tested:1 )", 1, 0]
                //                              ...]
                //             ]          
     //echo(h_results= h_results); //h(h_results,"endword"));
     files= [ for(f=[0:len(h_results)-1]) 
                   f%2? [ for(func0=h_results[f]) 
                           let(func=slice(func0,1)) // func:["num ( x )=number ( tested:29 )", 29, 0]
                           replace( func,0
                                  , last(split(split(func[0], "</b")[0], ">"))  
                                  ) //=>  ["num", 29, 0]
                        ]
                      : h_results[f] 
                 ];
               //  files=> ["scadx_num", [ ["fracstr", 49, 0]
               //                        , ["fullnumstr", 110, 0]
               //                        , ["getPowerOrder", 13, 0]
               //                        , ... ]
               //          ,"scadx_array", ... 
               //          ]
               
      nfiles = len(files)/2 ;            
      nfuncs = sum( [for (r=vals(files)) len(r)] ) ; 
      ntests = sum( [for (r=vals(files)) sum(getcol(r, 1))] );
      nfails = sum( [for (r=vals(files)) sum(getcol(r, 2))] );
      fails = [ for(kv=hashkvs(files)) 
                 if(sum(getcol(kv[1], 2)))
                   each [ kv[0],
                         [ for(func=kv[1]) // func= ["fracstr", 49, 0]  
                           if(func[2]) each [func[0], func[2]] 
                         ]
                        ] 
              ];   
      failedFiles = nfails?
                      [ for(fn=keys(files)) 
                           let(nfails = sum(getcol( hash(files,fn), 2))
                              )
                           if(nfails) each( [fn, nfails] )    
                      ]
                      :[];
                      
      //echo(failedFiles=failedFiles);
      //echo(fails=fails);
       
      _return= concat( display>0? ["nfile", nfiles, "nfunc", nfuncs, "ntest", ntests, "nfail", nfails]:[] 
                       , nfails? ["nfail", nfails]:[]
                       //, display>=1?["failed", failedFiles]:[]
                       , display>=1&&nfails?["fails", fails]:[] 
                       , display>=2?["files", keys(files)]:[] 
                     );
                                
      return = _s( html?"<b>scadx_tests:</b> {_}"
                       :"scadx_tests: {_}"
                 , html? slice( _fmth(_return), 1,-1)
                                 : join([for(kv=hashkvs(_return))
                                         str( kv[0], "=", kv[1] )
                                        ],", ") 
                    
                 );
//      return = _s( html?"<b>scadx_tests(display={_}):</b> {_}"
//                       :"scadx_tests(display={_}): {_}"
//                 , [display, html? slice( _fmth(_return), 1,-1)
//                                 : join([for(kv=hashkvs(_return))
//                                         str( kv[0], "=", kv[1] )
//                                        ],", ") 
//                   ] 
//                 );
              
    echo(return);
  }  
}    

//scadx_tests(); 
//scadx_file_test(fname="core", mode=11);
//scadx_file_test(fname="string", mode=11);
//echo( arg_test(mode=12)[1] );
//echo( _f_test(mode=12)[1] );
//echo( _s2_test(mode=12)[1] );
//echo( sizeArg_test(mode=12)[1] );
//echo( colorArg_test(mode=12)[1] );
//echo( scaleArg_test(mode=12)[1] );
//echo( argx_test(mode=12)[1] );
//echo( subprop_test(mode=12)[1] );
//echo( updatekey_test(mode=12)[1] );
//echo( isnums_test(mode=12)[1] );
//echo( istype_test(mode=12)[1] );
//echo( ishash_test(mode=12)[1] );
//echo( xtype_test(mode=12)[1] );
//echo( istype_test(mode=12)[1] );
//echo( update2_test(mode=12)[1] );
//echo( centerArg_test(mode=12)[1] );
//echo( sortArrs_test(mode=12)[1] );
//echo( get_test(mode=12)[1] );
//echo( vecOp_test(mode=12)[1] );
//echo( get_face_edges_test(mode=12)[1] );
//echo( get_edges_from_faces_test(mode=12)[1] );
//echo( get_edgeFace_test(mode=12)[1] );
//echo( get_ptFaceList_test(mode=12)[1] );
//echo( has_test(mode=12)[1] );
//echo( uniq_test(mode=12)[1] );
//echo( edgeFace_update_test(mode=12)[1] );
//echo( relist_face_indices_test(mode=12)[1] );
//echo( ef2faces_test(mode=12)[1] );
//echo( get_test(mode=12)[1] );
//echo( re_align_a_pis_test(mode=12)[1] );
//echo( get_aFis_on_pts_test(mode=12)[1] );
//echo( arr_diff_test(mode=12)[1] );
//echo( get_edgefi_test(mode=12)[1] );
//echo( get_subpis_on_pi_test(mode=12)[1] );
//echo( clock_wize_fis_on_pi_test(mode=12)[1] );
//echo( sum_test(mode=12)[1] );
//echo( get3_test(mode=12)[1] );
//echo( normal_test(mode=12)[1] );
//echo( get_aFis_on_pts_test(mode=12)[1] );
//echo( get_test(mode=12)[1] );
//echo( idx_test(mode=12)[1] );
//echo( prev_test(mode=12)[1] );
//echo( next_test(mode=12)[1] );
//echo( zip_test(mode=12)[1] );
//echo( getv_test(mode=12)[1] );
//echo( hash_test(mode=12)[1] );
//echo( update_test(mode=12)[1] );
//echo( update2_test(mode=12)[1] );
//echo( updates_test(mode=12)[1] );
//echo( get_fis_have_pis_test(mode=12)[1] );
//echo( get_fis_have_edges_test(mode=12)[1] );
//echo( get_face_edges_test(mode=12)[1] );
//echo( contain_test(mode=12)[1] );
//echo( get_edgefis_test(mode=12)[1] );
//echo( dels_test(mode=12)[1] );
//echo( insert_test(mode=12)[1] );
//echo( _f_test(mode=12)[1] );
//echo( numstr_test(mode=12)[1] );
//echo( num_test(mode=12)[1] );
//echo( hash_test(mode=12)[1] );
//echo( getdeci_test(mode=12)[1] );
//echo( share_test(mode=12)[1] );
//echo( istype_test(mode=12)[1] );
//echo( subArrs_test(mode=12)[1] );
//echo( _s_test(mode=12)[1] );
//echo( det_test(mode=12)[1] );
//echo( solve2_test(mode=12)[1] );
//echo( solve3_test(mode=12)[1] );
//echo( circlePtsOnPQR_test(mode=12)[1] );
//echo( circumCenterPt_test(mode=12)[1] );

//results = get_scadx_hash_test_results();
//for(r = results) echo( r[1] );

//results = get_scadx_array_test_results(mode=MODE,opt=[]);
//for(r = results) echo( r[1] );

//scadx_file_test( "scadx_num"); 
//scadx_file_test( "scadx_array"); 
//scadx_file_test( "scadx_range"); 
//scadx_file_test( "scadx_hash"); 
//scadx_file_test( "scadx_hash", mode=111, summaryOnly=false); 
//ISLOG=1;
//echo( cornerPt_test(mode=12)[1] );

//scadx_file_test( "scadx_inspect"); 
//scadx_file_test( "scadx_array", mode=112, summaryOnly=false); 
//echo( zipx_test(mode=112)[1] );

//scadx_file_test( "scadx_faces", mode=112, summaryOnly=false); 
//scadx_file_test( "scadx_faces", mode=111, summaryOnly=false); 

//scadx_file_test( "scadx_geometry", mode=011, summaryOnly=false); 
//echo( onlinePt_test(mode=112)[1] );
//echo( func_test(mode=112)[1] );

//scadx_file_test( "scadx_string", mode=011);//, summaryOnly=false); 
//echo( randchain_test(mode=112)[1] );
//echo( _table_test(mode=112)[1] );
//echo( roundto_test(mode=112)[1] );
//echo( calc_test(mode=112)[1] );
//echo( permute2_test(mode=112)[1] );
//echo( cavePF_test(mode=12)[1] );
//echo( onlinePt_test(mode=112)[1] );
//echo( _f_test(mode=12)[1] );
//echo( replaces_test(mode=12)[1] );
//echo( _hl_test(mode=12)[1] );
//echo( match_test(mode=12)[1] );
//echo( _ss_test(mode=12)[1] );
//echo( _s_test(mode=12)[1] );
//echo( othoPt_test(mode=12)[1] );
//echo( sievefaces_test(mode=12)[1] );
//echo( _s2_test(mode=12)[1] );
//echo( updateStyle_test(mode=12)[1] );
//echo( _f_test(mode=12)[1] );
//echo( _hl_test(mode=12)[1] );
//echo( _fmt_test(mode=12)[1] );
//echo( _table_test(mode=12)[1] ); // ------------- very slow
//echo( _table_test(mode=12)[1] );
//echo( argu_test(mode=12)[1] );
//echo( solve3_test(mode=12)[1] );
//echo( calc_func("sin(x)",["x",[0,90,270]]) ); 
//echo( calc_func("x^2+y", ["x",[0,1,2],"y",[0,1,2]]) ); //=> [0,1,2,1,2,3,4,5,6]

//scadx_files_test(fnames=["scadx_num","scadx_array","scadx_range"], isOneLine=true);
//scadx_files_test(fnames=["scadx_string"]);
//echo( func_test(mode=112)[1] );
//echo( isrange_test(mode=112)[1] );
//echo( permutes_test(mode=112)[1] );
//scadx_tests(["array","range","string"],display=2,html=1);
//scadx_tests(["inspect"],display=2,html=1);
//scadx_tests("eval");//,display=1,html=1);
//scadx_tests();
//scadx_tests("eval");
//scadx_tests("match", display=2);
//scadx_tests("error");
//scadx_tests("subdiv");
//scadx_tests("core", display=2);
//scadx_tests(FILES_TO_TEST, display=2);


//scadx_tests(FILES_TO_TEST,display=1,html=1);
/* 181010: scadx_tests: nfile=17, nfunc=248, ntest=1810, fails=[] 
*/ 

//pts0= cubePts(2);
//pts2= translatePts(pts0*[0.8*X,0.8*Y,1.05*Z], [.2,.2,.2]) ;
////pts2= pts0*[0.8,0.8,1];
//echo(pts0=pts0);
//echo(pts2=pts2);
////MarkPts(pts2);
//polyhedron( concat( pts0,pts2), faces= faces("tube"));

//pts0= [O, 0.8*Y+0.5*X, 1.5*Y+X/2, 2*X+Y, 1.5*X];
////MarkPts(pts0);
//pts1= smoothPts(pts0, closed=1);
//pts11 = concat(pts1, translatePts(pts1, [0,0,2]) ); 
////Line(pts11);
//echo(pts11=len(pts11));
//pts2= expandPts( pts11, -0.2); //pts11*[ 0.8*X, 0.8*Y, Z];
////Line(pts2);
////pts3= translatePts( pts2, centroidPt(pts11)-centroidPt(pts2) );
//pts3= translatePts( pts2, [maxx(pts11)-maxx(pts2), maxy(pts11)-maxy(pts2), 0]/2  );
////Line(pts3);
//Line(concat(pts11,pts3));
//polyhedron( concat(pts11,pts3)
//   , faces= faces("tube", nside= len(pts3)));

//echo("=================== in scadx_test.scad ==================");
//echo( )

//scadx_file_test( "scadx_args", mode=12, summaryOnly=false, opt=[] );
//echo( arg_test(mode=12)[1] );
//echo( argx_test(mode=12)[1] );
//echo( argu_test(mode=12)[1] );
//echo( scaleArg_test(mode=12)[1] );
//echo( subprop_test(mode=12)[1] );
echo( growPts_test(mode=12)[1] );
echo( split_test(mode=12)[1] );
echo( slice_test(mode=12)[1] ); // new 2020.7.17: can take slice(o,[i,j])

