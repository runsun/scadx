include <scadx_obj_adv_dev.scad>
/* 
http://forum.openscad.org/Transformation-matrix-function-library-td5154.html
http://www.fastgraph.com/makegames/3drotation/

Note: this is a left-handed system. To change to right-handed: change the
      sign of all sin:  sin(a) => -sin(a)

*/

function rotMx(a)=[ [1,0,0], 
                    [0,cos(a),-sin(a)], 
                    [0,sin(a),cos(a)], 
                  ]; 

function rotMy(a)=[ [cos(a),0,sin(a)], 
                    [0,1,0], 
                    [-sin(a),0,cos(a)], 
                  ]; 

function rotMz(a)=[ [cos(a),-sin(a),0], 
                    [sin(a),cos(a),0], 
                    [0,0,1], 
                  ]; 

module rotM_demo()
{
   ang= -30;
   //=================================
   pqr=randPts(3,d=[-4,6]);
   Chain(pqr, ["closed",false]);
   MarkPts( pqr );
   //PtGrid( pqr[0]); 
    
   axis= randPts(2); 
   axisP= onlinePt(axis,len=5);
   Ds = [for(p=pqr) othoPt([axis[0],p,axis[1]])];
    
   //axis= [[0,0,0], [5,0,0]]; 
   Line( extendPts(axis), ["r",0.08] ); 
   MarkPts(axis);
   for(i=range(3)) //p=pqr)
   {  
      p= pqr[i];  
      D= Ds[i]; //othoPt([axis[0],p,axis[1]]); 
      Line( [p, D], ["r",0.01,"transp",0.5,"color","red"] );
      Mark90( [p,D,axisP] );
       
   }       
   //=================================
    
   m =  rotM(ang, axis);
   echo(m=m);
   //newpqr= [ for(p=pqr) slice(m*app(p-axis[0],0),0,-1)+axis[0] ]; 
   //newpqr= [ for(p=pqr) rotMx(30)*p ];
   newpqr= [ for(p=pqr) m*(p-axis[1])+axis[1] ];
       
   for(i=range(3)) //p=newpqr)
   {   
      p= newpqr[i];   
      D= Ds[i]; //othoPt([axis[0],p,axis[1]]); 
      Line( [p, D], ["r",0.01,"transp",0.5,"color","red"] );
      Mark90( [p,D,axisP] );
      //Plane( [p,D, pqr[i]], ["mode",3]);  
   }       
//   for(p=newpqr) 
//       Line( [p, othoPt([axis[0],p,axis[1]])], ["r",0.01, "transp",0.5,"color","red"] );  
   
   echo( newpqr = newpqr );   
   color("red") Chain(newpqr, ["closed",false]);
   MarkPts( newpqr );
   
   echo(ang_pqr= angle(pqr), ang_new= angle(newpqr));
    
   color("yellow", 0.3) Plane(pqr, ["mode",3]);
   color("red", 0.3) Plane(newpqr,["mode",3]);
   for(i=range(3))
   {
       pdn = [ pqr[i], Ds[i], newpqr[i] ];
       echo( angle= angle(pdn));
       Chain( pdn, ["color", COLORS[i], "r",0.05, "transp",0.5] );   
       
   }
}
//rotM_demo();

//function rotx(p,a)= rotMx(a)*p;   
//function roty(p,a)= rotMy(a)*p;   
//function rotz(p,a)= rotMz(a)*p;   
   

  
module test_rotx() 
{
    a= 90;


    p1 = [ 1,2, 1];

    MarkPts( [p1] );
    LabelPt( p1, str("1=",p1));
    PtGrid( p1, ["mode",3] );

    p1x = rotx(p1,a); //slice(rotMx(a)*concat(p1,[1]), 0,-1); 

    color("green") MarkPts( [p1x]);
    LabelPt(p1x, "1' (rotx*p1)");
    PtGrid( p1x, ["mode",3] );

    p1t= rotx(p1,a,[1,1,1]); //slice(rotMx(a,[1,1,1])*concat(p1,[1]), 0,-1); 

    color("blue") MarkPts( [p1t]);
    LabelPt(p1t, "1'' (rotx*p1 then transfer [1,1,1])");
    PtGrid( p1t , ["mode",3]);
}

//test_rotx();

module rot_plane_to_xy_by_zyx()
{
   
   echo("rot_plane_to_xy_by_zyx() in rot_test.scad");     
   echo("Rotate any plane to the xy plane by rotating against z,y then x");     
   ColorAxes();
   
    
   //============================================ 
/*
    The following pqrs_ give results that R or S is not on plane:
        
    (1)
    
     [[-1.34404, -2.87038, -5.93947], [-1.24529, 0.641596, -0.432519], [-2.9228, -2.88292, -4.76534], [-3.02156, -6.39489, -10.2723]]

        ECHO: is_final_R_on_plane = true
        ECHO: is_final_S_on_plane = false
        ECHO: aPz = 91.6106, " ", aPx = -57.4626, " ", aRx = -156.701
        ECHO: pqrs_final = [[6.53225, 2.02699e-15, -3.47874e-15], [0, 0, 0], [5.57301, 1.71788, -1.11022e-16], [12.1053, 1.71788, 1.28927e-05]]    
    
    (2)
         [[-0.321365, 0.679663, 0.0837559], [0.00790744, 1.76269, 2.89847], [-1.25515, -0.100823, -1.80652], [-1.58443, -1.18385, -4.62123]]
    
        ECHO: is_final_R_on_plane = true
        ECHO: is_final_S_on_plane = false
        ECHO: aPz = 106.911, " ", aPx = -68.0918, " ", aRx = -160.23
        ECHO: pqrs_final = [[3.03381, 6.98814e-16, -2.10835e-15], [0, 0, 0], [5.16754, 0.708114, -4.71845e-16],
        
        
    The following looked like should go wrong as above but didn't :

    (a)
    ECHO: pqrs = [[-3.63859, 1.19557, -2.14873], [-4.34415, -2.04615, -0.52589], [-0.605486, -1.96042, 0.198952], [0.100082, 1.2813, -1.42389]]
    ECHO: is_final_R_on_plane = true
    ECHO: is_final_S_on_plane = true
    ECHO: aPz = -77.7209, " ", aPx = -26.0659, " ", aRx = -164.073
    ECHO: pqrs_final = [[3.69326, 5.0359e-16, 1.06735e-15], [0, 0, 0], [0.470992, 3.78002, -2.44249e-15],

*/    
    
   pqr = randPts(3);
   //pqrs_ = squarePts( pqr );
   pqrs_ = app( pqr, cornerPt( pqr ) ); 
//   pqrs_=           [[-1.34404, -2.87038, -5.93947], [-1.24529, 0.641596, -0.432519], [-2.9228, -2.88292, -4.76534], [-3.02156, -6.39489, -10.2723]];
    
   pqrs = [ for(p=pqrs_) p+pqrs_[0]];

   crosslen = 2*max( dist( [pqrs[0], pqrs[2] ])
                    , dist( [pqrs[1], pqrs[3] ]) );  
   echo(pqrs = pqrs ); 
   //echo(crosslen= crosslen);
    //Plane( pqrs ); 
   //============================================ 
   
   MarkPts( pqrs ); LabelPts( pqrs, "PQRS");
   Chain( pqrs , ["r", 0.02]); //Plane( pqrs ); 
    
   //============================================
   // Moved to ORIGIN 
   pqrs2 =  [ for(p=pqrs) p-pqrs[1] ];
       
   MarkPts( pqrs2 ); LabelPts( pqrs2, "PQRS");
   color("red") Chain( pqrs2, ["r", 0.02]); //Plane( pqrs2 ); 
   //PtGrid( pqrs2[0], ["mode",3] ); 
   
   arrop= ["headP",false
           ,"headQ",["len",0.5, "r", 0.15]
            ];
   Arrow2( [pqrs[1], pqrs2[1]], arrop) ;
   //============================================ 
   // Rot@z to xz plane
   
   P = pqrs2[0];
   //pts_z =  [P, p00z(P), px0z(P) ];
   //Chain( pts_z, ["closed",false, "transp",0.5] );
   aPz = angle( [P, p00z(P), [1, 0, P.z]], refPt=[0,0, crosslen] );
   Pxz = rotz( P, aPz); 
   //MarkPts([Pxz]); //LabelPt(Pxz, "Pxz");
   Arrow2( [P,Pxz], arrop);  
    
   
   //============================================ 

   pqrs_xz = [ for(p=pqrs2) rotz(p, aPz) ];
       
   //MarkPts( pqrs_xz ); //LabelPts( pqrs_xz, "PQRS");
   color("green") Chain( pqrs_xz, ["r", 0.02]);
   //PtGrid( pqrs_xz[0], ["mode",3] ); 
                
   //============================================ 
   // rot@y to xy plane
   aPx = angle( [ Pxz, ORIGIN, [1,0,0] ], refPt=[0,crosslen,0] ); 
   pqrs_xy= [ for(p=pqrs_xz) roty( p, aPx) ];
       
   //MarkPts( pqrs_xy ); //LabelPts( pqrs_xy, "PQRS");
   color("blue") Chain( pqrs_xy, ["r", 0.02]);
   Arrow2( [Pxz,pqrs_xy[0]], arrop);  
    
   //============================================ 
   // rot@x finally
   Rx = pqrs_xy[2];
   aRx = angle( [ Rx, px00(Rx), 
                  , [ Rx.x, crosslen, 0] ], refPt=[crosslen,0,0] ); 
   Chain([ Rx, px00(Rx), [ Rx.x, crosslen/2, 0] ], ["closed", false]);
   //echo(aRx = aRx);
   pqrs_final= [ for(p=pqrs_xy) rotx( p, aRx) ];
   MarkPts( pqrs_final ); LabelPts( pqrs_final, "PQRS");
   //color("purple") Chain( pqrs_final);
       
   Plane( pqrs_final );
   
   echo( is_final_R_on_plane= isOnPlane( [[1,0,0], ORIGIN, [0,1,0] ], pqrs_final[2]) );
   echo( is_final_S_on_plane= isOnPlane( [[1,0,0], ORIGIN, [0,1,0] ], pqrs_final[3]) );
   echo( aPz=aPz, " ", aPx=aPx, " ", aRx=aRx); 
   echo( pqrs_final= pqrs_final );
}    
//rot_plane_to_xy_by_zyx();

module transf_dev(pqr1,pqr2)
{
  // try how to to transform an obj from a coordnate (defined by pqr)
  //  to another (pqr2). Args pqr, pqr2 are any 3-pointer.   
  echom( "transf_dev" );  
    
  module transf(pqr1,pqr2)
  {  
      
     // Setup

//     _pqr1= or(pqr1,pqr90(d=[3,5]));
//     _pqr2= or(pqr2, pqr90(d=[0,2]));
//     //echo( pqr=pqr, pqr2=pqr2 ); 
//     c1 = coord( _pqr1, 3 );
//     c2 = coord( _pqr2, 3);
//     
     module Paint( pqr, clr, labels)
     { 
         Chain( pqr, ["color", clr]); 
         Plane( pqr, ["mode",3, "color",clr, "transp",0.3] ); 
         if(labels) LabelPts( pqr, labels );    
     }
     
     //=================================================
     // We have the starting pqr, that is to be transformed to pqr_end
     pqr1= pqr1; //shift(c1);
     
     // Enlarge pqr2 for better demo
     pqr2 = [ onlinePt( [Q(pqr2), P(pqr2)], len=1.5*d01(pqr2) )
            , Q(pqr2)
            , onlinePt( [Q(pqr2), R(pqr2)], len=rand(2,4)*d12(pqr1) )
            ];
     a_pqr = angle(pqr1);
     _pqr2 = app( p01(pqr2)
                , anglePt( pqr2, a_pqr ) );
     
     pqr_end= [ onlinePt( p10(_pqr2), len= d01(pqr1))
              , Q(_pqr2)
              , onlinePt( p12(_pqr2), len= d02(pqr1))
              ]
     
     ; //shift(c2);
      
     Paint(pqr1,clr="yellow", labels="PQR");
     
     //===========================================
     // 1st: translation pqr1 -> pqr2
      
     pqr_red = [for(p=pqr1) p + Q(pqr_end)-Q(pqr1) ]; 
         
     arrop = ["headP",false, "headQ",["len",0.5,"r",0.15]];       
     color("red") Arrow2( [ Q(pqr1), Q(pqr_red)], arrop );
     
     //Paint(pqr_red,clr="red", labels=["P","",""]); 

     //===========================================
     // 2nd: rotate pqr2 to pqr3 
     // to align PQ of pqr2=>pqr_end 
     
     pqr_v = [ P(pqr_red), Q(pqr_red), P(pqr_end) ]; 
     axis1 = reverse(normalLn( pqr_v) ); //extendPts( [Q(pqr2), N( pqr_v )] ); 
     Line( axis1, ["color","green", "transp",0.5, "r", 0.05]);
     a= angle( pqr_v );
     m = rotM( axis1, a);  
     pqr_green = rotPts( pqr_red
                  , axis= axis1
                 ,a = a
               );
               
     Paint(pqr_green,clr="green", labels="PQR");
//     color("green") 
//        Arc([ P(pqr_red), Q(pqr_red), P(pqr_green)], ["r",0.015] );
        
     //===========================================
     // 3rd: rotate pqr3 to pqr4 
     // to align PQ of pqr2=>pqr_end 
     
     // R(pqr_green) proj on PQ:
     D = othoPt( p021( pqr_green) ); 
     // Find a pt on pqr2 where R(pqr_green) corresponds 
     R2 = onlinePt( [Q(pqr_green), anglePt( pqr2,  a_pqr) ]
                   ,len= d12( pqr_green) );
     
     echo( R2=R2, D=D );
     pqr_v2 = [ R(pqr_green), D, R2 ]; 
     
     //axis2 = reverse(normalLn( pqr_v2)); //extendPts( [Q(pqr2), N( pqr_v )] ); 
     axis2 = extendPts(p01(pqr_green), len=2.5);
     
     //Line( axis2
     //      , ["color","purple", "transp",0.8, "r", 0.04]);
     Arrow2( axis2,       
             , ["color","purple", "transp",0.8, "r", 0.04
               , "headP",false]);
     _a2= angle( pqr_v2, refPt= axis2[0]); //P(pqr2) );
     a2 = _a2; //>90? 180-a:a;
     echo(axis2=axis2, a2=a2);
     
     LabelPts( axis2, ["ax0","ax1"]);
     m2 = rotM( axis2, a2);  
     pqr_blue = rotPts( pqr_green
                  , axis= axis2
                 ,a = a2
               );
     
     color("blue") 
       Arrow2( p02( pqr_v2), ops=arrop );
       //Arc( pqr_v2, ["r",0.015] );
     
     LabelPt( anglePt(  pqr_v2, a=a_pqr/2, len=dist([D,R2]))
            , a2, ["color","blue"]
            );    
     Chain( pqr_v2 );
//     
//     LabelPts(pqr_end,"PQR"); 
//     
     Chain(pqr2, ["r",0.1,"transp",0.4]); 
     MarkPts( sel(pqr2,[0,2]), ["labels","PR"] );
     
//     LabelPt( pqr1[0], str("Src pts, a=",a_pqr));
//     LabelPt( pqr2[2], "target pts");
     
//     LabelPt( midPt( [pqr_red[1], pqr1[1]] )
//             , "Translate to target pts"
//            , ["color","red"]
//            );

     //LabelPt( R(pqr_blue), "Is R2 On       
     //LabelPt( R2, "V2.R" );
    
     // Check if pqr_blue is on plane pqr2 by  chking R(pqr_blue)
     isOn = isOnPlane( pqr2, R(pqr_blue));
     echo( _blue(str(
        "isOnPlane( pqr2, R(pqr_blue))= ", isOn
        )));
     
     MarkPts( [R2, D], ["labels",["V2.R","D"]] );
     LabelPt( R(pqr_blue), str( a_pqr, ", ", isOn) );
     
     Paint(pqr_blue,clr="blue");
     Plane( pqr2, ["mode",3, "color","yellow", "transp", 0.5]);
//     Chain( pqr_v2, ["r",0.1,"color","blue","transp",0.3]);

    // true : 0,0
    // false: 0,0
    
  }  
  transf(pqr1,pqr2);  
}    

//transf_dev( randPts(3,d=[3,6])
//           ,randPts(3,d=[0,3]));
 
  
module transf_dev2(pqr1,pqr2)
{
  // try how to to transform an obj from a coordnate (defined by pqr)
  //  to another (pqr2). Args pqr, pqr2 are any 3-pointer.   
  echom( "transf_dev2" );  
    
  module transf(pqr1,pqr2)
  {  
      
     // Setup

//     _pqr1= or(pqr1,pqr90(d=[3,5]));
//     _pqr2= or(pqr2, pqr90(d=[0,2]));
//     //echo( pqr=pqr, pqr2=pqr2 ); 
//     c1 = coord( _pqr1, 3 );
//     c2 = coord( _pqr2, 3);
//     
     module Paint( pqr, clr, labels)
     { 
         Chain( pqr, ["color", clr]); 
         Plane( pqr, ["mode",3, "color",clr, "transp",0.3] ); 
         if(labels) LabelPts( pqr, labels );    
     }
     
     //=================================================
     // We have the starting pqr, that is to be transformed to pqr_end
     pqr1= pqr1; //shift(c1);
     
     // Enlarge pqr2 for better demo
     pqr2 = [ onlinePt( [Q(pqr2), P(pqr2)], len=1.5*d01(pqr2) )
            , Q(pqr2)
            , onlinePt( [Q(pqr2), R(pqr2)], len=rand(2,4)*d12(pqr1) )
            ];
     a_pqr = angle(pqr1);
     _pqr2 = app( p01(pqr2)
                , anglePt( pqr2, a_pqr ) );
     
     pqr_end= [ onlinePt( p10(_pqr2), len= d01(pqr1))
              , Q(_pqr2)
              , onlinePt( p12(_pqr2), len= d02(pqr1))
              ]
     
     ; //shift(c2);
      
     
     //===========================================
     // 1st: translation pqr1 -> pqr2
      
     pqr_red = [for(p=pqr1) p + Q(pqr_end)-Q(pqr1) ]; 
         
     //arrop = ["headP",false, "headQ",["len",0.5,"r",0.15]];       
     //color("red") Arrow2( [ Q(pqr1), Q(pqr_red)], arrop );
     
     //Paint(pqr_red,clr="red", labels=["P","",""]); 

     //===========================================
     // 2nd: rotate pqr2 to pqr3 
     // to align PQ of pqr2=>pqr_end 
     
     pqr_v = [ P(pqr_red), Q(pqr_red), P(pqr_end) ]; 
     axis1 = reverse(normalLn( pqr_v) ); //extendPts( [Q(pqr2), N( pqr_v )] ); 
     //Line( axis1, ["color","green", "transp",0.5, "r", 0.05]);
     a= angle( pqr_v );
     m = rotM( axis1, a);  
     pqr_green = rotPts( pqr_red
                  , axis= axis1
                 ,a = a
               );
               
     //Paint(pqr_green,clr="green", labels="PQR");
//     color("green") 
//        Arc([ P(pqr_red), Q(pqr_red), P(pqr_green)], ["r",0.015] );
        
     //===========================================
     // 3rd: rotate pqr3 to pqr4 
     // to align PQ of pqr2=>pqr_end 
     
     // R(pqr_green) proj on PQ:
     D = othoPt( p021( pqr_green) ); 
     // Find a pt on pqr2 where R(pqr_green) corresponds 
     R2 = onlinePt( [Q(pqr_green), anglePt( pqr2,  a_pqr) ]
                   ,len= d12( pqr_green) );
     
     //echo( R2=R2, D=D );
     pqr_v2 = [ R(pqr_green), D, R2 ]; 
     
     //axis2 = reverse(normalLn( pqr_v2)); //extendPts( [Q(pqr2), N( pqr_v )] ); 
     axis2 = extendPts(p01(pqr_green), len=2.5);
     
     //Line( axis2
     //      , ["color","purple", "transp",0.8, "r", 0.04]);
//     Arrow2( axis2,       
//             , ["color","purple", "transp",0.8, "r", 0.04
//               , "headP",false]);
     _a2= angle( pqr_v2, refPt= axis2[0]); //P(pqr2) );
     a2 = _a2; //>90? 180-a:a;
     //echo(axis2=axis2, a2=a2);
     
     //LabelPts( axis2, ["ax0","ax1"]);
     
     m2 = rotM( axis2, a2);  
     pqr_blue = rotPts( pqr_green
                  , axis= axis2
                 ,a = a2
               );
     
//     color("blue") 
//       Arrow2( p02( pqr_v2), ops=arrop );
       //Arc( pqr_v2, ["r",0.015] );
     
//     LabelPt( anglePt(  pqr_v2, a=a_pqr/2, len=dist([D,R2]))
//            , a2, ["color","blue"]
//            );    
//     Chain( pqr_v2 );
//     
//     LabelPts(pqr_end,"PQR"); 
//     
//     Chain(pqr2, ["r",0.1,"transp",0.4]); 
//     MarkPts( sel(pqr2,[0,2]), ["labels","PR"] );
     
//     LabelPt( pqr1[0], str("Src pts, a=",a_pqr));
//     LabelPt( pqr2[2], "target pts");
     
//     LabelPt( midPt( [pqr_red[1], pqr1[1]] )
//             , "Translate to target pts"
//            , ["color","red"]
//            );

     //LabelPt( R(pqr_blue), "Is R2 On       
     //LabelPt( R2, "V2.R" );
    
     // Check if pqr_blue is on plane pqr2 by  chking R(pqr_blue)
     isOn = isOnPlane( pqr2, R(pqr_blue));
     echo( _blue(str(
        "isOnPlane( pqr2, R(pqr_blue))= ", isOn
        , ", dist pt pl = ", distPtPl( R(pqr_blue), pqr2 )
        )));
     
//     MarkPts( [R2, D], ["labels",["V2.R","D"]] );
//     LabelPt( R(pqr_blue), str( a_pqr, ", ", isOn) );
     
     Paint(pqr_blue,clr="blue", labels="PQR");
     Paint(pqr2,clr="yellow", labels=["P2","","R2"]);
     Paint(pqr1,clr="red", labels=["P1","Q1","R1"]);
     
     //Plane( pqr2, ["mode",3, "color","yellow", "transp", 0.5]);
//     Chain( pqr_v2, ["r",0.1,"color","blue","transp",0.3]);
   
    // true : 0,0
    // false: 0,0
    
  }  
  transf(pqr1,pqr2);  
}    

//transf_dev2( randPts(3,d=[1,4])
//           ,randPts(3,d=[0,3]));
 

module transf_dev3(pqr1,pqr2)
{
  // try using coordPts as the mediater --- transfer to coord, then
  // use coord to transform, then transfer back to new coord 
    
  echom( "transf_dev3" );  
    
  module transf(pqr1,pqr2)
  {  
      
     // Setup

//     _pqr1= or(pqr1,pqr90(d=[3,5]));
//     _pqr2= or(pqr2, pqr90(d=[0,2]));
//     //echo( pqr=pqr, pqr2=pqr2 ); 
//     c1 = coord( _pqr1, 3 );
//     c2 = coord( _pqr2, 3);
//     
     module Paint( pqr, clr, labels)
     { 
         Chain( pqr, ["color", clr]); 
         Plane( pqr, ["mode",3, "color",clr, "transp",0.3] ); 
         if(labels) LabelPts( pqr, labels );    
     }
     
     //=================================================
     // We have the starting pqr, that is to be transformed to pqr_end
     pqr1= pqr1; //shift(c1);
     
     // Enlarge pqr2 for better demo
     pqr2 = [ onlinePt( [Q(pqr2), P(pqr2)], len=1.5*d01(pqr2) )
            , Q(pqr2)
            , onlinePt( [Q(pqr2), R(pqr2)], len=rand(2,4)*d12(pqr1) )
            ];
     a_pqr = angle(pqr1);
     _pqr2 = app( p01(pqr2)
                , anglePt( pqr2, a_pqr ) );
     
     pqr_end= [ onlinePt( p10(_pqr2), len= d01(pqr1))
              , Q(_pqr2)
              , onlinePt( p12(_pqr2), len= d02(pqr1))
              ]
     
     ; //shift(c2);
      
     
     //===========================================
     // 1st: translation pqr1 -> pqr2
      
     pqr_red = [for(p=pqr1) p + Q(pqr_end)-Q(pqr1) ]; 
         
     //arrop = ["headP",false, "headQ",["len",0.5,"r",0.15]];       
     //color("red") Arrow2( [ Q(pqr1), Q(pqr_red)], arrop );
     
     //Paint(pqr_red,clr="red", labels=["P","",""]); 

     //===========================================
     // 2nd: rotate pqr2 to pqr3 
     // to align PQ of pqr2=>pqr_end 
     
     pqr_v = [ P(pqr_red), Q(pqr_red), P(pqr_end) ]; 
     axis1 = reverse(normalLn( pqr_v) ); //extendPts( [Q(pqr2), N( pqr_v )] ); 
     //Line( axis1, ["color","green", "transp",0.5, "r", 0.05]);
     a= angle( pqr_v );
     m = rotM( axis1, a);  
     pqr_green = rotPts( pqr_red
                  , axis= axis1
                 ,a = a
               );
               
     //Paint(pqr_green,clr="green", labels="PQR");
//     color("green") 
//        Arc([ P(pqr_red), Q(pqr_red), P(pqr_green)], ["r",0.015] );
        
     //===========================================
     // 3rd: rotate pqr3 to pqr4 
     // to align PQ of pqr2=>pqr_end 
     
     // R(pqr_green) proj on PQ:
     D = othoPt( p021( pqr_green) ); 
     // Find a pt on pqr2 where R(pqr_green) corresponds 
     R2 = onlinePt( [Q(pqr_green), anglePt( pqr2,  a_pqr) ]
                   ,len= d12( pqr_green) );
     
     //echo( R2=R2, D=D );
     pqr_v2 = [ R(pqr_green), D, R2 ]; 
     
     //axis2 = reverse(normalLn( pqr_v2)); //extendPts( [Q(pqr2), N( pqr_v )] ); 
     axis2 = extendPts(p01(pqr_green), len=2.5);
     
     //Line( axis2
     //      , ["color","purple", "transp",0.8, "r", 0.04]);
//     Arrow2( axis2,       
//             , ["color","purple", "transp",0.8, "r", 0.04
//               , "headP",false]);
     _a2= angle( pqr_v2, refPt= axis2[0]); //P(pqr2) );
     a2 = _a2; //>90? 180-a:a;
     //echo(axis2=axis2, a2=a2);
     
     //LabelPts( axis2, ["ax0","ax1"]);
     
     m2 = rotM( axis2, a2);  
     pqr_blue = rotPts( pqr_green
                  , axis= axis2
                 ,a = a2
               );
     
//     color("blue") 
//       Arrow2( p02( pqr_v2), ops=arrop );
       //Arc( pqr_v2, ["r",0.015] );
     
//     LabelPt( anglePt(  pqr_v2, a=a_pqr/2, len=dist([D,R2]))
//            , a2, ["color","blue"]
//            );    
//     Chain( pqr_v2 );
//     
//     LabelPts(pqr_end,"PQR"); 
//     
//     Chain(pqr2, ["r",0.1,"transp",0.4]); 
//     MarkPts( sel(pqr2,[0,2]), ["labels","PR"] );
     
//     LabelPt( pqr1[0], str("Src pts, a=",a_pqr));
//     LabelPt( pqr2[2], "target pts");
     
//     LabelPt( midPt( [pqr_red[1], pqr1[1]] )
//             , "Translate to target pts"
//            , ["color","red"]
//            );

     //LabelPt( R(pqr_blue), "Is R2 On       
     //LabelPt( R2, "V2.R" );
    
     // Check if pqr_blue is on plane pqr2 by  chking R(pqr_blue)
     isOn = isOnPlane( pqr2, R(pqr_blue));
     echo( _blue(str(
        "isOnPlane( pqr2, R(pqr_blue))= ", isOn
        , ", dist pt pl = ", distPtPl( R(pqr_blue), pqr2 )
        )));
     
//     MarkPts( [R2, D], ["labels",["V2.R","D"]] );
//     LabelPt( R(pqr_blue), str( a_pqr, ", ", isOn) );
     
     Paint(pqr_blue,clr="blue", labels="PQR");
     Paint(pqr2,clr="yellow", labels=["P2","","R2"]);
     Paint(pqr1,clr="red", labels=["P1","Q1","R1"]);
     
     //Plane( pqr2, ["mode",3, "color","yellow", "transp", 0.5]);
//     Chain( pqr_v2, ["r",0.1,"color","blue","transp",0.3]);
   
    // true : 0,0
    // false: 0,0
    
  }  
  transf(pqr1,pqr2);  
}    

//transf_dev3( randPts(3,d=[1,4])
//           ,randPts(3,d=[0,3]));
   


