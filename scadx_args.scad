/*
   scadx_args.scad
   
   Tools for arg handlers for Scadx's Rich Argument System (RAS).
   (RAS is formally named on 2018.5.15)
*/

//========================================================
//. Arg handlers (or, better description, arg retrievers )
/*

   An arg handler(retriever) is a function to set and return an arg inside
   a function or module. 
    
     module m( arg_name, opt=['arg_name', opt_val] )
     {
        arg_val = arg_handler( arg_name, opt, 'arg_name', df);
     }
   
   This allows users to set arg in one of the following ways:
   
      f( x= 3 )          // --- plain args
      f( opt=["x",3] )   // --- packed args
        
   Also allows packaging all args in the opt:
   
      f( x, n, color, Label )
      f( opt= ["x",x, "n",n, "color",color, "Label", label] )
   
   This design is extremely useful if a pack of args are delivered into 
   this function, and this function can pick whatever arg to re-assign it. 
   For example, 
    
      f(x, opt)
   
   Let opt = ["x",3, "y",4]
   
   then inside f: 
   
      x = arg( arg=x, opt=opt, argname="x")   
   
   will pick up x if it is given first, then if not given, pick from opt=["x",...]
    
   An arg_handler checks arg_name to see if its value is given and combined 
   it with opt_val in different ways. 
    
   There are different types of arg_handler depending on how the
   arg_val and opt_val are combined:
    
   ___Simple___ (replacement)
     
     -- For x=1,y=2, etc. 
     -- The arg_val, if defined, replaces opt_val
         m(size,opt=[]) ==> size will replaces opt.size
     -- Handled by arg()           
               
   ___Paired___ (replaced but ensure it is a 2-item array)

     -- For color="red"|["red",0.5] 
     -- arg replaces opt.arg
     -- It makes sure it's an array: [color, transp]    
     -- Handled by colorArg(colorArg,opt,df)
                 
   ___Triplet___ (replaced but ensure it is a 3-item array) 
   
     -- For size, center, direction, alignment, scale ... 
     -- = n|[i,j,k] 
     -- arg replaces opt.arg
     -- make sure it's a triplet    
     -- Handled by centerArg, sizeArg, scaleArg
   
   ___Extended___ ( array extension: opt.x + x )

     -- The arg_val must be an arr.
     -- If defined (must be arr), extends opt_val
            m(actions,opt=[], df) ==> concat( df, opt.actions, actions)
     -- Handled by argx()
     -- Very useful for actions:
            f( actions = ["x",3], opt=["actions",["y",2]] )
         
         actions = argx( actions, opt, "actions" )
                 = ["y",2, "x",3]        
     

   ___Updated___ ( hash update )

     -- The arg_val must be an arr.
     -- If defined (must be arr), updates opt_val
            m(actions,opt=[], df) ==> updates( df, [opt.actions, actions])
     -- Handled by argu()
     
     
   ___Compound___ 
   
     -- The arg_val, if defined (must be hash), updates opt_val
         m( frame,opt=[]) ==> update( opt.frame, frame )
     -- For setting the properties of a sub-obj.
     -- Handled by subprop()    
    

   Examples: Cube, Line, MarkPt, MarkPts
   
   
subprop( sub          // subobj variable (??? it seems to have only true/false or undefined) 
       , opt          // an array containing run-time parameters of one or more subobjs.
                      //   like: ['arm', [...], 'leg',[...] ]
                      // It's up to subname below to select the subobj to use.
       , subname      // subobj name
       , dfsub        // default subunit opt. This is set at dev-time and containing only
                           the settings for one single subobj
       , dfPropName   // name of the default subunit property 
       , dfPropType   // type of the default subunit property
       , debug=0)=
   
subprop examples:
   
In scadx_geometry.scad, function getDecoData*():
   
  [ "color", ColorArg(color, opt) // Assure color is [name, transp]
  , "Frame", subprop( Frame, opt, "Frame"
                    , dfPropName="r"
                    , dfPropType="num"
                    , df=["r",0.01, "FrameShape", "rod"]
                    )
                    //, df=["shape", hash(opt, "FrameShape","rod")] )
  , "MarkPts", subprop(MarkPts, opt, "MarkPts", []) 
  , "Dim", subprop(Dim, opt, "Dim",[]) 
  ]
   
*/                      


function arg(arg,opt,argname,df)=
(
  arg==undef?hash(opt,argname,df):arg  
);
   
arg=["arg","arg,opt,argname,df","any","core",
    "An arg handler for a simple argument.
    ;;
    ;; Usage:
    ;;
    ;;  module m(w, opt){
    ;;    w = arg(w,opt,\"w\", 5);
    ;;  }
    ;;
    ;;  m(3);
    ;;  m(opt=[\"w\",3]);
    ;;
    ;; Work as:
    ;;
    ;;  w? w
    ;;  : opt['w']? opt['w']
    ;;  : df

    ",
    "arg,argx,subarg,Color"
    ];

function arg_test(mode=MODE, opt=[] )=
(
  let(opt1=["r",1])

  doctest( arg, 
  [
    [ arg(2), 2,"2"]
  , [ arg(2,[]), 2,"2,[]"]
  , [ arg(undef, opt1), undef,"undef, ['r',1]", ["//", "opt provided but no designated var"]]
  , [ arg(undef, opt1,"r" ), 1,"undef, ['r',1], 'r'", ["//", "opt and the var name provided"]]
  , [ arg(undef, opt1,"x" ), undef,"undef, ['r',1], 'x'"]
  , [ arg(undef, opt1,"x",df=3 ), 3,"undef, ['r',1], 'x',df=3"]
  , [ arg(2, opt1,"x",df=3 ), 2,"2, ['r',1], 'x',df=3"]
  , [ arg(undef, ["pt",[4,5,6]],"pt"), [4,5,6]
      ,"undef,['pt',[4,5,6]],'pt'"]
  , [ arg([1,2,3], ["pt",[4,5,6]],"pt"), [1,2,3]
      ,"[1,2,3],['pt',[4,5,6]],'pt'"]
  ]
  , mode=mode, opt=opt)
);
   
    
function argx(arg,opt,argname,df)=
(
   let( optarg = hash(opt,argname,df) )
   arg==undef?
     optarg
   : concat( isarr(optarg)?optarg:[], arg)   
);
   
    argx=["argx","arg,opt,argname,df","any","core",
    "An arg handler for an extended argument.    
    ",
    "arg,argx,subarg,Color"
    ];

    function argx_test(mode=MODE, opt=[] )=
    (
        let(opt1=["actions",["rot",45]] 
           )

        doctest( argx, 
        [
          "var opt1"
        , [ argx(["x",1]), ["x",1],"['x',1]"]
        , [ argx(undef, opt1,"actions" ), ["rot",45],"undef, ['actions',['rot',45]], 'actions'"]
        , [ argx(undef, ["size",3],"actions" ), undef,"undef, ['size',3], 'actions'"]
        , [ argx(undef, ["size",3],"actions", ["y",2] ), ["y",2],"undef, ['size',3], 'actions',['y',2]", ["//","set default"]]
        , [ argx(["x",1], opt1,"actions", ["y",2] ), ["rot",45,"x",1],"['x',1], ['actions',['rot',45]], 'actions',['y',2]", ["//","Extended"]]
        , [ argx(["x",1], opt1,"actions" ), ["rot",45,"x",1],"['x',1], ['actions',['rot',45]], 'actions'"]
        , [ argx(["rot",90], opt1,"actions" ), ["rot",45,"rot",90],"['rot',90], ['actions',['rot',45]], 'actions'", ["//","Note: extended, not updated"]]
        ]
        , mode=mode, opt=opt, scope=["opt1",opt1])
    );
    
    
function argu(arg,opt,argname,df)=
(
   let( optarg = hash(opt,argname,df) )
   //echo("In argu, ", optarg=optarg, arg=arg)
   arg==undef?
     optarg
   : updates( df, [isarr(optarg)?optarg:[], arg] )   
);    


    argu=["argu","arg,opt,argname,df","any","core",
    "An arg handler for an updated argument.    
    ;; 
    ;; NOTE that the df is kept and updated (but not replaced or extended in other arg funcs)
    ",
    "arg,argu,subarg,Color"
    ];

    function argu_test(mode=MODE, opt=[] )=
    (
        let(opt1=["actions",["rot",45]] 
           )

        doctest( argu, 
        [
          "var opt1"
        , [ argu(["x",1]), ["x",1],"['x',1]"]
        , [ argu(undef, opt1,"actions" ), ["rot",45],"undef, opt1, 'actions'"]
        , [ argu(undef, ["size",3],"actions" ), undef,"undef, ['size',3], 'actions'"]
        , [ argu(undef, ["size",3],"actions", ["y",2] ), ["y",2],"undef, ['size',3], 'actions',['y',2]"]
        , ""
        , "// NOTE that -- unlike other arg funcs ---  the df ['y',2] is kept:"
        , [ argu(["x",1], opt1,"actions", ["y",2] ), ["y", 2, "rot", 45, "x", 1],"['x',1], opt1, 'actions',['y',2]"]
        , [ argu(["x",1], opt1,"actions" ), ["rot",45,"x",1],"['x',1], opt1, 'actions'"]
        , [ argu(["rot",90], opt1,"actions" ), ["rot",90],"['rot',90], opt1, 'actions'"]
        , [ argu(["rot",90], opt1,"actions" ), ["rot",90],"['rot',90], ['actions',['rot',45]], 'actions'", ["//","Note: updated, not extended"]]
        , ""
        , "// Use sopt (str-style opt) 'rot=90':"
        , [ argu("rot=90", opt1,"actions" ), ["rot",90],"'rot=90', opt1, 'actions'"]
        ]
        , mode=mode, opt=opt, scope=["opt1",opt1])
    );

//echo(_table( [[1, 0], [2, 0.333333], [3, 0.666667], [4, 1]], header=1 ));  
    
function centerArg(center,opt, df=[1,1,1])= // application: size
(
   let( c= arg(center,opt,"center",df) ) 
   c==false||c==0||c==undef?[0,0,0]
   :c==1? [0,0,1]
   :c==10? [0,1,0]
   :c==11? [0,1,1]
   :c==100? [1,0,0]
   :c==101? [1,0,1]
   :c==110? [1,1,0]
   :c==true||c>0? [1,1,1]
   :isarr(c)? [ for(x=c)x?1:0]
   :c?[1,1,1]:[0,0,0]
);
    centerArg=["centerArg","center,opt, df=[1,1,1]","arr","core",
    "An arg handler for an argument 'center'.
    ;;
    ;;  centerArg(1) = centerArg()
    ;;  centerArg
      
    ",
    "arg,argx,subarg,Color"
    ];
    

    function centerArg_test(mode=MODE, opt=[] )=
    (
        //let(opt1=["r",1])

        doctest( centerArg, 
        [
          [ centerArg(), [1,1,1], "" ]
        , [ centerArg(2), [1,1,1], "2" ]
        , [ centerArg(false), [0,0,0], "false" ]
        , [ centerArg([2,1,0]), [1,1,0], "[2,1,0]" ]
        , [ centerArg(opt=["center",2]), [1,1,1], "opt=['center',2]" ]
        , [ centerArg(center=1, opt=["center",2]), [0,0,1], "center=1,opt=['center',2]" ]
        , [ centerArg(center=true, opt=["center",false]), [1,1,1], "center=true,opt=['center',false]" ]
        , [ centerArg(center=[1,2,3],opt=["center",2]), [1,1,1], "center=[1,2,3], opt=['center',2]" ]
        , ""
        , [ centerArg(11), [0,1,1], "11" ]
        , [ centerArg(101), [1,0,1], "101" ]
        , [ centerArg(111), [1,1,1], "111" ]
        , [ centerArg(true), [1,1,1], "true" ]
        , [ centerArg(opt=["center",11]), [0,1,1], "opt=['center',11]" ]
        , [ centerArg(opt=["center",101]), [1,0,1], "opt=['center',101]" ]
        , [ centerArg(opt=["center",true]), [1,1,1], "opt=['center',true]" ]
        
        
        ]
        , mode=mode, opt=opt)
    );    
        
function colorArg(color,opt,df, separate=false)= 
(
   let(separate = isstr(color)?separate:true)
   separate?  // 'separate' added 2018.2.7 (gotta be a better way to do this)
              // Setting separate=true allows to keep transparancy (so it 
              //   won't be over-written)
   (          // See usage :
              //   separate=!solid_color 
              // in Plane() for example usage 
     
     isarr(color)?color
     : let( _df = assureColor(df)
          , _optc = hash(opt,"color")
          , c = isstr(color)? color
                : isstr(_optc)? _optc
                : isarr(_optc)? _optc[0]
                : _df[0]  
         , t = isnum(color)? color
                : isnum(_optc)? _optc
                : isarr(_optc)? _optc[1]
                : _df[1]  
         ) [c==undef?"gold":c,t]
   )     
   :
   let( c= arg(color,opt,"color",df) )
   //echo( _optc=_optc, c=c,t=t)
   assureColor(c)
   
   //c==undef?undef:isarr(cc)&&cc[0]==undef?["gold",cc[1]]:cc
);

    colorArg=["colorArg","color,opt,df=['gold',1]","arr","core",
    "An arg handler for a color argument.
    ",
    "arg,argx,subarg,Color"
    ];
    

    function colorArg_test(mode=MODE, opt=[] )=
    (
        //let(opt1=["r",1])

        doctest( colorArg, 
        [
          [ colorArg(), ["gold",1], "" , ["//","= Default"]]
        , [ colorArg(.8), ["gold",.8], "0.8" ]
        , [ colorArg("red"), ["red",1], "'red'" ]  
        , [ colorArg(["red",0.5]), ["red",0.5], "['red',0.5]" ]  
        , [ colorArg(opt=["color", "red"]), ["red",1], "opt=['color','red']" ]  
        , [ colorArg(opt=["color", ["red",0.5]]), ["red",0.5], "opt=['color',['red',0.5]]" ]  
        , [ colorArg(color="blue",opt=["color", ["red",0.5]]), ["blue",1], "color='blue', opt=['color',['red',0.5]]" ]  
        , [ colorArg(color=["blue",0.8],opt=["color", ["red",0.5]]), ["blue",0.8], "color=['blue',0.8], opt=['color',['red',0.5]]" ]  
        ,""
        ,"// 2018.2.7: add 'separate' arg, allowing setting color or transparancy "
        ,"// seperately WHEN color is a string."
        ,""
        , [ colorArg("red", df=["gold",0.5]), ["red",1], "'red', df=['gold',0.5]"
            ,["//", "Set solid red"] 
          ]
        , [ colorArg("red", df=["gold",0.5], separate=true), ["red",0.5]
            , "'red', df=['gold',0.5], separate=true" 
            , ["//", "Only set color, not transparancy"]
          ]
        , ""  
        , "// 'separate' is effective only when the first argument is a string:"    
        , ""
        , [ colorArg(0.5), ["gold",0.5], "0.5" ]
        , [ colorArg(0.5, separate=true), ["gold",0.5], "0.5, separate=true" ]
        , [ colorArg(0.5, ["color",["red",0.1]] ), ["red",0.5], "0.5, ['color',['red',0.1]]" ]
        , [ colorArg(0.5, ["color",["red",0.1]], separate=true ), ["red",0.5], "0.5, ['color',['red',0.1]], separate=true" ]
        
        ]
        , mode=mode, opt=opt)
    );

 
function sizeArg(arg,opt, argname="size", df=[1,1,1])= // application: size
(
   let( c= arg(arg,opt,argname,df) )
   c==undef?undef: isarr(c)?c:[c,c,c]
);
    sizeArg=["sizeArg","arg,opt,argname, df=[1,1,1]","arr","core",
    "An arg handler for an argument that could be a number or a 3-item array.
    ;; Return an array. Application: size argument.
    ",
    "arg,argx,subarg,Color"
    ];
    

    function sizeArg_test(mode=MODE, opt=[] )=
    (
        //let(opt1=["r",1])

        doctest( sizeArg, 
        [
          [ sizeArg(), [1,1,1], "" ]
        , [ sizeArg(2), [2,2,2], "2" ]
        , [ sizeArg([2,1,0]), [2,1,0], "[2,1,0]" ]
        , [ sizeArg(opt=["size",2]), [2,2,2], "opt=['size',2]" ]
        , [ sizeArg(opt=["size",2],argname="size"), [2,2,2], "opt=['size',2],argname='size'", ["//", "argname is optional"] ]
        , [ sizeArg(arg=1, opt=["size",2]), [1,1,1], "arg=1,opt=['size',2]" ]
        , [ sizeArg(arg=[1,2,3],opt=["size",2]), [1,2,3], "arg=[1,2,3], opt=['size',2]" ]
        , ""
        , "// Apply sizeArg to other arguments similar to 'size':"
        , ""
        , [ sizeArg(opt=["lens",2],argname="lens"), [2,2,2], "opt=['lens',2],argname='lens'"]
        , [ sizeArg([1,2,3], opt=["lens",2],argname="lens"), [1,2,3], "[1,2,3], opt=['lens',2],argname='lens'"]
        , [ sizeArg(2, opt=["lens",[1,2,3]],argname="lens"), [2,2,2], "2, opt=['lens',[1,2,3]],argname='lens'"]
        
        
        ]
        , mode=mode, opt=opt)
    );

//function scaleArg(arg,opt, argname, df)= // application: scale
//(
//   let( _arg = und(arg, [1,1,1])
//      , arg = _arg==undef? undef: isarr(_arg)?_arg:[_arg,_arg,_arg]
//      , _optarg = hash( opt, argname, und(df,[1,1,1]) )
//      , optarg = _optarg==undef? undef: isarr(_optarg)?_optarg:[_optarg,_optarg,_optarg]
//      )
//   arg==undef && optarg==undef? undef   
//   : [for(i=[0,1,2])  arg[i]*optarg[i] ]   
//);
function scaleArg(arg,opt, df)= // application: scale
(
   let( _arg = und(arg, [1,1,1])
      , arg = _arg==undef? undef: isarr(_arg)?_arg:[_arg,_arg,_arg]
      , _optarg = hash( opt, "scale", und(df,[1,1,1]) )
      , optarg = _optarg==undef? undef: isarr(_optarg)?_optarg:[_optarg,_optarg,_optarg]
      )
   arg==undef && optarg==undef? undef   
   : [for(i=[0,1,2])  arg[i]*optarg[i] ]  
   //update2( hash(opt,"scale",df=1), arg==undef?[]:arg ) 
);
 
    scaleArg=["scaleArg","arg,opt,argname, df=[1,1,1]","arr","core",
    "An arg handler for an argument that could be a number or a 3-item array.
    ;; Return an array. 
    ;;
    ;; Similar to sizeArg() except that the value obtained from the opt, [x,y,z],
    ;; is multiplied by that from arg, [a,b,c] => [a*x,b*y,c*z]
    ;;
    ;; Application: scale argument.
    ;;
    ;; IMPORTANT: it might need to del the key 'scale' from an opt before 
    ;; sending it to a subobj for scaling, otherwise the scaling set in the
    ;; opt will be applied twice. See LabelPt() for example.
    ;;
    ;; Note: 
    ;;  If it's too complicated, use sizeArg(arg,opt,argname='scale',df) to 
    ;;  avoid duplicate scaling. 
    ",
    "arg,argx,subarg,Color"
    ];
    

    function scaleArg_test(mode=MODE, opt=[] )=
    (
        //let(opt1=["r",1])

        doctest( scaleArg, 
        [
          [ scaleArg(), [1,1,1], "" ]
        , [ scaleArg(2), [2,2,2], "2" ]
        , [ scaleArg([2,1,0]), [2,1,0], "[2,1,0]" ]
        , [ scaleArg(opt=["scale",2]), [2,2,2], "opt=['scale',2]" ]
        , [ scaleArg(opt=["x",2]), [1,1,1], "opt=['x',2]" ]
        , [ scaleArg(opt=["x",2],df=0.1), [0.1,0.1,0.1], "opt=['x',2],df=0.1" ]
        , [ scaleArg(2, opt=["x",2],df=0.1), [0.2,0.2,0.2], "2,opt=['x',2],df=0.1" ]
        , [ scaleArg(opt=["scale",2],argname="scale"), [2,2,2], "opt=['scale',2],argname='scale'" ]
        , [ scaleArg(arg=1, opt=["scale",2],argname="scale"), [2,2,2], "arg=1,opt=['scale',2],argname='scale'" ]
        , [ scaleArg(arg=[1,2,3],opt=["scale",2],argname="scale"), [2,4,6], "arg=[1,2,3], opt=['scale',2],argname='scale'" ]
        , [ scaleArg(arg=2,opt=["scale",[1,2,3]],argname="scale"), [2,4,6], "arg=2, opt=['scale',[1,2,3]],argname='scale'" ]
        
        ]
        , mode=mode, opt=opt)
    );
     
  

function subprop(sub,opt,subname,dfsub, dfPropName, dfPropType, debug=0)=
(
   // sub: true/false/undefined or any other value
   //      Set a single subobj property value
   
   sub==false ? false
   : let( optsub0= hash(opt,subname) )
      sub==undef && ( opt==undef 
                    || opt==false
                    || optsub0==undef
                    || optsub0==false ) ? false
     : let( _dfsub= isarr(dfsub)?dfsub:[ ] 
          , optsub1= optsub0==true? []
                   :  isstr(dfPropName) 
                     && (istype(optsub0, dfPropType) || dfPropType==undef)? 
                     [dfPropName, optsub0]
                   : isarr(optsub0)? optsub0 
                   : false                //==> false |  [...]
          ,  _sub= sub==true?[]
                   : isstr(dfPropName) && (istype(sub, dfPropType) || dfPropType==undef) ?
                     [ dfPropName, sub]
                   : isarr(sub)? sub
                   : false         
          , rtn= sub==undef ?
                ( 
                  (optsub0==undef||optsub0==false) ? false
                  : optsub1==false?false :update( _dfsub, optsub1)
                )            
                //: updates( _dfsub, [optsub0==false||optsub0==undef?[]:optsub1, _sub] )
                : update2s( _dfsub, [optsub0==false||optsub0==undef?[]:optsub1, _sub] )
     
      )
   debug?
   echo( _fmth(
     [ "rtn", rtn
     , "sub", sub 
     , "type(sub)", type(sub)
     , "dfPropType", dfPropType
     , "isnum(sub)", isnum(sub)
     , "istype(sub,dfPropType)", istype(sub,dfPropType)
     , "_sub", _sub
     , "opt", opt
     , "optsub0", optsub0
     , "optsub1", optsub1
     //, "optsub2", optsub2 
     , "dfsub", dfsub
     , "_dfsub", _dfsub
     //, "rtn", rtn
     //, "df", df
     //, "dfPropName", dfPropName
     //, "dfPropType", dfPropType
     //, "canDisable", canDisable
     //, "isarr(optarg)?optarg:[]",isarr(optarg)?optarg:[]
     //, "isstr(dfPropName)",isstr(dfPropName) 
     //, "type(sub)", type(sub)
     //, "istype(sub, dfPropType)",istype(sub, dfPropType)
     //, "isstr(dfPropName) && type(sub)==dfPropType",isstr(dfPropName) && type(sub)==dfPropType
     //, "[dfPropName, sub]", [dfPropName, sub]
     ]
   )) rtn==undef?false:rtn
   : rtn==undef?false:rtn 
);  
 
//echo("istype(33, \"str num nums\")=",istype(33, ["str", "num", "nums"]));
   
    subprop=["subprop","sub,opt,subname,dfsub,dfPropName,dfPropType","any","core",
    
    "An arg handler to set the properties of subobj(s) of an obj.
    ;; Return either false or a hash.
    ;; 
    ;;  subprop( sub User opt for the target 
    ;;         , opt = opt
    ;;         , subname = 'Arms'
    ;;         , dfsub = ['len', 3, 'count', 2 ] 
    ;;         , dfPropName = 'len'
    ;;         , dfPropType = 'num'
    ;;         )
    ;;
    ;; Assume you have an obj named Body, which has two subobjs, Arms and Legs.
    ;; 
    ;; subprop allows:
    ;;
    ;;   Body( Arms = true )
    ;;   Body( Arms = false, Legs = true )
    ;;   Body( Arms = ['len', 2, 'count', 2], Legs=['color','red', 'count',2] )
    ;;   Body( Arms = 2, Legs = 4 )
    ;;   Body( opt= ['Arms', ['len', 2], 'Legs', ['count',2] ] ) 
    ;;   Body( Arms= 3, opt= ['Arms', ['len', 2], 'Legs', ['count',2] ] ) 
    ;; 
    ;; module Body( Arms, Legs, opt )
    ;; {
    ;;    Arms = subprop( sub = Arms // User opt for the target subobj, Arms
    ;;                  , opt = opt  // User opt for ALL subobjs (Arms and Legs)
    ;;                  , subname = 'Arms'  // Name to pick target subobj from opt
    ;;                  , dfsub = ['len', 3, 'count', 2 ]  // df opt for subobj (= Arms)
    ;;                  , dfPropName = 'len' // df prop for the target subobj Arms
    ;;                  , dfPropType = 'num'  
    ;;                  )
    ;;    Legs = subprop( sub = Legs
    ;;                  , opt = opt
    ;;                  , subname = 'Legs'
    ;;                  , dfsub = ['color', 'green', 'count', 2 ] 
    ;;                  , dfPropName = 'count'
    ;;                  , dfPropType = 'num'
    ;;                  )
    ;; }
    ;; Note that df is just the properties of ONE subobj like ['len',4] 
    ;; but opt could contain settings of MANY subobjs (mentioned as opt.subs in doc):
    ;;
    ;;   opt = ['arm',['len',4], 'leg',['count',2] ]
    ;;
    ;; it is up to subname (set to 'arm' or 'leg') to decide which subobj setting to use. 
    ;; 
    ;;  sub: a hash = subobj settings to be added to dfsub (return hash)
    ;;       true = enable this subobj (return hash)
    ;;       false = disable this subobj (return false)
    ;;       num/str = if dfPropName given, set its value (return hash)
    ;;                 otherwise, return [] 
    ;;       not-given = return either hash or false, depending on opt
    ;;  opt:
    ;;  subname:
    ;;  dfsub: 
    ;;  dfPropName:
    ;;  dfPropType:
    ;;  debug=0
    ;;
    ;; Example:
    ;;
    ;;  subprop( opt=['arm',true, 'leg', false]
    ;;         , subname='arm'
    ;;         , dfsub=['x',3]
    ;;         )= ['x', 3]
    ;;  subname picks 'arm' from opt, found it is set to true, return the dfsub  
    ;;  
    ;;  subprop( sub=true
    ;;         , opt=['arm',false]
    ;;         , subname='arm'
    ;;         , dfsub=['x',3]
    ;;         )= ['x', 3]
    ;;  subname picks 'arm' from opt, found it to be false(disabled)
    ;;   but it is overturned by sub, so return the dfsub  
    
    "
//    ;;             opt.subs     sub           matches
//    ;;    sub      [subName]    name dfSub  dfPropName rtn
//                                               ='x'    
//    ;;   ------ -------------  ----- ---- ----------- -------  
//    ;;     -         -	         -    -       -       false    
//    ;;     -        true         -    -       -       false    
//    ;;     -         -           -   true     -       [ ]    
//    ;;     -         -           -    3       -       [ ]    
//    ;;     -         -           -  false     -       [ ]    
//    ;;     -         -	         -  ['x',3]   -       ['x',3]   
//    ;;     -        true         -  ['x',3]   -       ['x',3]    
//    ;;   ------ -------------  ----- ---- ----------- -------  
//    ;;    true       -	         -    -       Y       [ ]    
//    ;;    true       -	         -    -       Y       [ ]    
//    ;;    true       -	         -    -       -       [ ]    
//    ;;    true      true         -    -       -       [ ]    
//    ;;    true       -           -   true     -       [ ]    
//    ;;    true       -           -    3       -       [ ]    
//    ;;    true       -           -  false     -       [ ]    
//    ;;    true       -	         -  ['x',3]   -       ['x',3]   
//    ;;    true      true         -  ['x',3]   -       ['x',3]    
//    ;;   ------ -------------  ----- ---- ----------- -------  
//    ;;    false      -	         -    -       -       false    
//    ;;    false     true         -    -       -       [ ]    
//    ;;    false      -           -   true     -       [ ]    
//    ;;    false      -           -    3       -       [ ]    
//    ;;    false      -           -  false     -       [ ]    
//    ;;    false      -	         -  ['x',3]   -       ['x',3]   
//    ;;    false     true         -  ['x',3]   -       ['x',3]    
//    ;;   ------ -------------  ----- ---- ----------- -------  
//    ;;    true       -	         -    -       -       [ ]    
//    ;;    true       -           -  ['x',3]   -       ['x',3] 
//    ;;   ------ -------------  ----- ---- ----------- -------  
//    ;;     -         -	         -    3       -       false   
//    ;;     -         -	         -    3      'y'      ['y',3]   
//    ;;     -        true         -    3   -  'y'      ['y',3]    
//    ;;     -         -           -   true     -       false    
//    ;;     -         -           -   true  -  'y'     ['y',true]
//        
//    ;;     -    ['a',['x',3]]   'a'   -       -       ['x',3]    
//    ;;     -    ['a',true ]]    'a'   -       -       ['x',3]    
//    ;;     -    ['a',['x',3]]    -    -       -       ['x',3]    
//    ;;     -    ['a',true ]]     -    -       -       ['x',3]    
//    ;;   undef   false  ['x',3]   n/a      false    
//    ;;   undef   undef  undef     n/a      false    
//    ;;   undef   undef  undef     n/a      false    
//    ;;   undef   undef  undef     n/a      false
//    ;;    
//    ;; --------
//    ;; An sub handler is a function to set and return an sub inside
//    ;; a function or module. 
//    ;;
//    ;;  module m( arg_val, opt=['arg_name', opt_val] )
//    ;;  {
//    ;;     arg_val = arg_handler( arg_val, opt, 'arg_name', df);
//    ;;  }
//    ;;
//    ;;  the arg_handler checks arg_val to see if it is given and combined 
//    ;;  it with opt_val in different ways, with some processing if needed. 
//    ;;
//    ;;  There are different types of arg_handler depending on how the
//    ;;  arg_val and opt_val are combined:
//    ;;
//    ;;  -- sub() : arg_val, if defined, replaces opt_val
//    ;;                   m(size,opt=[]) ==> size will replaces opt.size
//    ;;            
//    ;;  -- Paired sub : for color setting only. See Color()
//    ;;
//    ;;  -- argx() : arg_val, if defined, extends opt_val
//    ;;                   m(actions,opt=[]) ==> concat( opt.actions, actions)
//    ;;
//    ;;  -- subprop() : arg_val, if defined, updates opt_val
//    ;;                   m( frame,opt=[]) ==> update( opt.frame, frame )
//    ;;                   For setting the properties of a sub-obj.
//    "
    ,"sub,argx,subprop,Color"
    ];

    function subprop_test(mode=MODE, opt=[] )=
    (
        let(opt1=["frame",["rot",45]] 
           )

        doctest( subprop, 
        [
        
                
         "", "// Both sub and opt not given ------<br/> "
        
        , [ subprop(), false,""]
        , [ subprop(dfsub=["x",3], debug=false), false,"dfsub=['x',3]"]
        
        , "", "// [1] opt not given --------<br/>"
        
        , [ subprop(dfsub=["x",3]), false,"dfsub=['x',3]", ["//", "Set default"]]
        , ""
        , [ subprop(3), [],"3", ["//", "dfsub is updated w/ that hash"]]
        , [ subprop(3, dfPropName="x",debug=0), ["x",3],"3, dfPropName='x'", ["//", "dfsub is updated w/ that hash"]]
        , [ subprop(3, dfsub=["x",4],debug=0), ["x",4],"3, dfsub=['x',4]", ["//", "dfsub is updated w/ that hash"]]
        , [ subprop(["x",4], dfsub=["x",3]), ["x",4],"['x',4], dfsub=['x',3]", ["//", "Need a hash to take effect"]]
        , [ subprop(["a",4], dfsub=["x",3],debug=0), ["x",3, "a",4],"['a',4], dfsub=['x',3]", ["//", "dfsub is updated w/ that hash"]]
        
        , "// sub=true: "
        , [ subprop(true), [],"true"]
        , [ subprop(true, dfsub=["x",3]), ["x",3],"true, dfsub=['x',3]"]
        , [ subprop(true, dfPropName="y"), [],"true, dfPropName='x'", ["//", "dfsub is updated w/ that hash"]]
        , [ subprop(true, dfsub=["x",3], dfPropName="y"), ["x",3],"true, dfsub=['x',3], dfPropName='y'", ["//", "dfsub is updated w/ that hash"]]
        
        , "// sub==false: "
        , [ subprop(false), false,"false"]
        , [ subprop(false, dfsub=["x",3],debug=0), false,"false, dfsub=['x',3]", ["//", "Need a hash to take effect"]]
        , [ subprop(false, dfsub=["x",3], canDisable=true,debug=0), false,"false, dfsub=['x',3], canDisable=true", ["//", "Or set cadDisable to disable it"]]
        , [ subprop(false, dfsub=["x",3], dfPropName="y",debug=0), false,"false, dfsub=['x',3], dfPropName='y'", ["//", "dfsub is updated w/ that hash"]]
        
        ,"","// [2] sub not given ------<br/>"
        
                
        , [ subprop(opt=false), false,"opt=false"]
        , [ subprop(opt=["x",4]), false,"opt=['x',4]", ["//","No effect-- opt is a collection of many properties, need a subname to specify"]]
        , ""
        , [ subprop(opt=["x",4], dfsub=["x",3]), false,"opt=['x',4], dfsub=['x',3]", ["//", "opt still no effect"]]
        , [ subprop(opt=["x",4], dfsub=["x",3], subname="x",debug=0), false,"opt=['x',4], dfsub=['x',3], subname='x'", ["//", "The subname specified but still no effect ?"]]
        
        , [ subprop(opt=["arm",["x",4]], dfsub=["x",3], subname="arm",debug=0), ["x",4],"opt=['arm',['x',4]], dfsub=['x',3], subname='arm'", ["//", "Because the sub value should be a hash"]]
        , [ subprop(opt=["arm",["a",5]], dfsub=["x",3], subname="arm"), ["x",3,"a",5],"opt=['arm',['a',5]], dfsub=['x',3], subname='arm'", ["//", "The dfsub is updated with opt.arm"]]
                
        , [ subprop(opt=["arm",true],subname="arm", debug=0),[]
                     ,"opt=['arm',true], subname='arm'"]
                     
        , [ subprop(opt=["arm",true],subname="arm",dfsub=["x",3], debug=0),["x",3]
                     ,"opt=['arm',true], subname='arm',dfsub=['x',3]"]
        , [ subprop(opt=["arm",false],subname="arm",dfsub=["x",3]),false
                     ,"opt=['arm',false], subname='arm',dfsub=['x',3]"]
        , [ subprop(true, opt=["arm",false],subname="arm",dfsub=["x",3]),["x",3]
                     ,"true, opt=['arm',false], subname='arm',dfsub=['x',3]"]
                     
        , [ subprop(opt=["arm",3],subname="arm"), false
                     ,"opt=['arm',3], subname='arm'"]
                     
        ,""             
        , [ subprop(opt=["arm",3],subname="arm", dfPropName="x",debug=0),["x",3]
                     ,"opt=['arm',3], subname='arm', dfPropName='x'"]

        , [ subprop(opt=["arm",3],subname="arm", dfPropName="x",dfPropType="num"),["x",3]
                     ,"opt=['arm',3], subname='arm', dfPropName='x', dfPropType='num'"]
                     
        , [ subprop(opt=["arm","a"],subname="arm", dfPropName="x",dfPropType="num"),false
                     ,"opt=['arm','a'], subname='arm', dfPropName='x', dfPropType='num'"]
                     
        , [ subprop(opt=["arm","a"],subname="arm", dfPropName="x",debug=0), ["x","a"]
                     ,"opt=['arm','a'], subname='arm', dfPropName='x'"]
                     
        , [ subprop(opt=["arm","a"],subname="arm"),false
                     ,"opt=['arm','a'], subname='arm'"]
        , [ subprop(opt=["arm",["x",3]]),false
                     ,"opt=['arm',['x',3]]"
                     , ["//", "opt given, but subname is not set"] ]
        , [ subprop(opt=["arm",true]),false
                     ,"opt=['arm',true]"
                     , ["//", "opt given, but subname is not set"] ]
        , [ subprop(opt=["arm",["x",3]],subname="leg"),false
                     ,"opt=['arm',['x',3]], subname='leg'"
                     , ["//", "Can't find 'leg' in the opt"] ]
        , ""
        , [ subprop(opt=["arm",["x",3]],subname="arm"),["x",3]
                     ,"opt=['arm',['x',3]], subname='arm'"]                

        , "", "// Both sub & opt given ------<br/>"        

        
        , [ subprop(true,opt=["arm",["x",3]]),[]
                     ,"true, opt=['arm',['x',3]]", ["//","opt given, but subname is not set"]]
        , [ subprop(true,opt=["arm",["x",3]], subname="leg"),[]
                     ,"true, opt=['arm',['x',3]], subname='leg'"
                     , ["//","subname given, but can't find leg in the opt"]]
        , [ subprop(true,opt=["arm",["x",3]],subname="arm"),["x",3]
                     ,"true, opt=['arm',['x',3]], subname='arm'"]
         
        , [ subprop(false,opt=["arm",["x",3]]), false
                     ,"false, opt=['arm',['x',3]]", ["//","opt given, but subname is not set"]
                     ]
        , [ subprop(false,opt=["arm",["x",3]], subname="leg"),false
                     ,"false, opt=['arm',['x',3]], subname='leg'"
                     , ["//","subname given, but can't find leg in the opt"]]
        , [ subprop(false,opt=["arm",["x",3]],subname="arm"),false
                     ,"false, opt=['arm',['x',3]], subname='arm'"]
                     
        , "", _red("// All sub, opt and dfsub are given ------<br/>")        

        , [ subprop(sub=["z",4], opt=["arm",["y",3]],subname="arm",dfsub=["x",2]),["x",2,"y",3,"z",4]
                     ,"sub=['z',4], opt=['arm',['y',3]], subname='arm',dfsub=['x',2]"]
        
        //, [ subprop(false,opt=["arm",["x",3]],subname="arm",canDisable=true), false
                     //,"false, opt=['arm',['x',3]], subname='arm',canDisable=true"
                     //,["//", "Set <u>canDisable</u> when sub=false"]
                     //]
                     
//        , ""
//        , "// Both opt and subname are given ------<br/>"        
//
//        , [ subprop(opt=["arm",["x",3]],subname="arm"),["x",3]
//                     ,"opt=['arm',['x',3]], subname='arm'"]
//        , [ subprop(1,opt=["arm",["x",3]],subname="arm", debug=false),["x",3]
//                     ,"1, opt=['arm',['x',3]], subname='arm'"
//                     ,["//","sub=1 is ignored -- no dfPropName/dfPropType, can't assign it"] 
//                     ]
                             
        
        , ""
        , "// When sub is a hash, update opt with it:"             
        , [ subprop(sub=["y",1],opt=["arm",["x",3]],subname="arm"),["x",3,"y",1]
                     ,"['y',1], opt=['arm',['x',3]], subname='arm'"]
        , ""             
        , "// Set the dfPropName/dfPropType:"             
        , [ subprop(1,opt=["arm",["x",3]],subname="arm",dfPropName="y",dfPropType="num")
                     ,["x",3,"y",1]
                     ,"1, opt=['arm',['x',3]], subname='arm',dfPropName='y',dfPropType='num'"
                     ,["//", "dfPropName given, and type(sub)=dfPropType"] ]
                     
        , [ subprop(10,opt=["arm",["x",3]],subname="arm",dfPropName="x",dfPropType="num")
                     ,["x",10]
                     ,"10, opt=['arm',['x',3]], subname='arm',dfPropName='x',dfPropType='num'"
                     ,["//", "dfPropName given, and type(sub)=dfPropType"] ]
                     
        , [ subprop("a",opt=["arm",["x",3]],subname="arm",dfPropName="x",dfPropType="num")
                     ,["x",3]
                     ,"'a', opt=['arm',['x',3]], subname='arm',dfPropName='x',dfPropType='num'"
                     ,["//", "dfPropName given, but type(sub)!=dfPropType"] ]
                     
        , [ subprop("a",opt=["arm",["x",3]],subname="arm",dfPropName="x")
                     ,["x","a"]
                     ,"'a', opt=['arm',['x',3]], subname='arm',dfPropName='x'"
                     ,["//", "dfPropType NOT given, any type would do"] ]
                     
        , [ subprop("a",opt=["arm",["x",3]],subname="arm")
                     ,["x",3]
                     ,"'a', opt=['arm',['x',3]], subname='arm'"
                     ]
                     
        , [ subprop("a", debug=0),[], "'a'"]
        
        
        ]
        , mode=mode, opt=opt)
    );
    







function get_scadx_args_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_args.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_args_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_args", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      arg_test( mode, opt )
    , argx_test( mode, opt )
    , argu_test( mode, opt )
    , centerArg_test( mode, opt )
    , colorArg_test( mode, opt )
    , scaleArg_test( mode, opt )
    , sizeArg_test( mode, opt )
    , subprop_test( mode, opt )
    ]
);












