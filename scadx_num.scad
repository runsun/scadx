//========================================================
{ // getdeci
    
function getdeci(n)= 
(
   index(str(n),".")==undef?0:len(split(str(n),".")[1])
);
   
    getdeci=["getdeci","n","num","Math",
    "Return number of decimal points"
    ];

    function getdeci_test( mode=MODE, opt=[] )=
    (
        doctest( getdeci,
        [
        ]
        , mode=mode, opt=opt
        )
    );
} // getdeci

//========================================================
{ // int
function int(x,_i_=0)=
(
  isint(x)? x
  :isfloat(x)? sign(x)*round(abs(x))
  :isarr(x)? undef
  :x=="" || x[0]=="."? 0 //index(x,".")==0? 0
  :  _i_==0 && x[0]=="-"? -int(x,_i_+1)
  : 
    let( //p=  min( len(x), und(index(x,"."), len(x)) )
         p = und(idx(x,"."), len(x))     
       )
    //echo( str("int( ",x, " )"), _i_=_i_, p = p)
    (_i_< p ? pow(10, p-_i_-1)
		   * hash(  ["0",0, "1",1, "2",2
                            ,"3",3, "4",4, "5",5
                            ,"6",6, "7",7, "8",8
                            ,"9",9], x[_i_] 
                         )+ int(x, _i_+1)
	   :0
    )
);
    int=[ "int", "x", "int", "Type, Number",
    " Given x, convert x to integer. 
    ;;
    ;; int('-3.3') => -3
    "];

    function int_test( mode=MODE, opt=[] )=
    (
        doctest( int, 
        [ 
          [ int(3),3, "3"]
        , [ int(3.3),3, "3.3"]
        , [ int(-3.3),-3, "-3.3"]
        , [ int(3.8),4, "3.8"]
        , [ int(9.8),10, "9.8"]
        , [ int(-3.8),-4, "-3.8"]
        , [ int("-3.3"),-3, "'-3.3'"]
        , [ int("-3.8"),-3, "'-3.8'", ["//"," !!Note this!!"]]
        , [ int("3"),3, "'3'"]
        , [ int("3.0"),3, "'3.0'"]
        , [ int("0.3"),0, "'0.3'"]
        , [ int("34"),34, "'34'"]
        , [ int("-34"),-34, "'-34'"]
        , [ int("-34.5"),-34, "'-34.5'"]
        , [ int("345"),345, "'345'"]
        , [ int("3456"),3456, "'3456'"]
        , [ int("789987"),789987, "'789987'"]
        , [ int("789.987"),789, "'789.987'", ["//"," !!Note this!!"]]
        , [ int([3,4]),undef, "[3,4]"]
        , [ int("x"),undef, "'x'"]
        , [ int("34-5"),undef, "'34-5'"]
        , [ int("34x56"),undef, "'34x56'"]
        ]
        ,mode=mode, opt=opt	
        )
    );

}//int

//========================================================
{ // num
function num(x,_i_=0)=
(
	isnum(x)
	 ? x
	 : isstr(x)
	   ?(
		 ( x=="" || x=="undef" || x=="-")
		 ? undef
		 : endwith(x,"'")||endwith(x,"\"")
		 ? ftin2in(x)//12
         : has(x,"e")? 
             let( xs=split(x,"e"))
             num(xs[0]) *pow( 10, num( replace(xs[1],"+","")))
         : has(x,"E")? 
             let( xs=split(x,"E"))
             num(xs[0]) *pow( 10, num( replace(xs[1],"+","")))
		 : _i_<len(x)
		   ?(
			index(x,".")==0
		  	? num( str("0",x), _i_=0)	
		 	: _i_==0 && x[0]=="-"
		   		?-num(x,_i_=1)
		   		: (_i_== index(x,".") //x[_i_]=="."
				  ? 0
					// 345.6      3456
					// 210 -1     3210  <== pow(10, ?)
					// 01234      0123  <== _i_
					//    3         -1  <== index(x,".")
					
				  : pow(10, (index(x,".")==undef //<0
							?(len(x)-_i_-1)
							: ( index(x,".")-_i_ )
							  -( index(x,".")>_i_?1:0)	
						   )
					  )
				    * hash( ["0",0, "1",1, "2",2
				 		,"3",3, "4",4, "5",5
						,"6",6, "7",7, "8",8
						,"9",9], x[_i_] )

				  )+ num(x, _i_+1)
		    )
		   :0 // out of bound
		)
		:undef // everything other than string and num
);

    num=[ "num", "x", "number", "Type, Number",
    " Given x, convert x to number. x could be a number, a number string,
    ; or a string for feet-inches like 3`4' that will be converted to number
    ; in inches.
    ;
    ; num('-3.3') => -39  
    ; num('2`6\"') => 18   
    "];
    // Other implementations: 
    // http://forum.openscad.org/Output-inch-hardware-sizes-td19204.html
    
    function num_test( mode=MODE, opt=[] )=
    (
        doctest( num, 
        [
          [ num(-3.8),-3.8, "-3.8"]
        , [ num("3"),3, "'3'"]
        , [ num("340"),340, "'340'"]
        , [ num("3040"),3040, "'3040'"]
        , [ num("3.4"),3.4, "'3.4'"]
        , [ num("30.04"),30.04, "'30.04'"]
        , [ num("340"),340, "'340'"]
        , [ num("3450"),3450, "'3450'"]
        , [ num(".3"),0.3, "'.3'"]
        , [ num("-.3"),-0.3, "'-.3'"]
        , [ num("0.3"),0.3, "'0.3'"]
        , [ num("-0.3"),-0.3, "'-'0.3'"]
        , [ num("-15"),-15, "'-15'"]
        , [ num("-3.35"),-3.35, "'-3.35'"]
        , [ num("789.987"),789.987, "'789.987'"]
        , [ num("1"),1, "'1'"]
        , [ num(1),1, "1"]
        , ""	
        , "// Convert to inches:"
        , [ num("2'6\""), 30, "2`6'"]
        //, [ num("3""), 3, "'3'"]
        , [ num("3'"), 36, "3\""]
        , ""	
        , [ num([3,4]),undef, "[3,4]"]
        , [ num("x"),undef, "'x'"]
        , [ num("34-5"),undef, "'34-5'"]
        , [ num("34x56"),undef, "'34x56'"]
        , [ num(""),undef, "''"]
        , [ num("x"),undef, "'x'"]
        , [ num([3]),undef, [3]]
        , [ num(true),undef, "true"]
        , [ num("-"),undef, "-"]
        ,""
        ,"// Scientific notation (2015.7.29)"
        ,[ num("3e-2"),0.03, "'3e-2'"]
        ,[ num("3E2"),300, "'3E2'"]
        ,[ num("3e-08"),3e-08, "'3e-08'"]
        ,[ num("3e+08"),3e+08, "'3e+08'"]
        
        ]
        ,mode=mode,opt=opt

        )
    );
} // num

//========================================================
{ // numstr
    
function numstr(n,d=undef, dmax=undef, conv=undef)=
(
	conv=="ft:ftin" ?
	 ( abs(n)<1 ?
	     str( numstr(n*12, d, dmax) , "\"")
	   : str( sign(n) * abs(n>0?floor(n):floor(n+1)) 
	        , "'"
                , n%1==0?"":str(numstr((abs(n)%1)*12,d,dmax),"\"")
                )
	 )  
	: conv=="in:ftin" ?
	  numstr( ( isstr(n)&&endwith(n,"\"")?
	             num(shift(n,"\"","")):n 
	           )/12
	        , d=d
	        , dmax=dmax
	        , conv="ft:ftin"
	        )
	: conv ?
	  numstr( uconv(n, units=conv) 
	  	, d=d, dmax=dmax, conv=undef 
	  	)
//	: conv=="in:cm"
//	? numstr( n*2.54, d=d, dmax=dmax)
//	: conv=="ft:cm"
//	? numstr( n*12*2.54, d=d, dmax=dmax)

//	: conv=="ftin:cm" && isstr(n)
//	? numstr( endwith(n,"'")
//			 ? num(shift(n)*12 
//			index(n, "'")>0? // 3'2", 3'
//			( num(split(n,"'")[0])*12
//			 )+(len(split(n,"'"))>1?
//			  split(n,"'")[0]*12:0
//			 ) n*12*2.54, d=d
//			)

	// Finish conv part above. Start processing decimal 

	: isint(d)&&d>-1 ?
	  (  //echo(n=n, deci_n= getdeci(n))
	     isint(dmax)?  // when both defined
	      ( //echo(dmax = dmax, deci_n= getdeci(n))
	        getdeci(n)== min(d,dmax)? 
	          str(n)
	      : getdeci(n)< min(d,dmax)?
	        str( n, isint(n)?".":"", repeat("0", min(d,dmax)-getdeci(n)))
	      : str(  round(n*pow(10,min(d,dmax)))
		      / pow(10,min(d,dmax))  )
              )       	
    	    : // d given, dmax not
      	      getdeci(n)== d ? str(n)
      	    : getdeci(n)< d ?
      	       //echo("getdeci(n)< d")
      	       str( n, isint(n)?".":"", repeat("0", d-getdeci(n)) )
      	    : //echo("getdeci(n)> d", n_pow_d= n*pow(10,d), pow_d = pow(10,d) )
      	       str( round(n*pow(10, d))/pow(10,d))
  	  )
	: isint(dmax) ?
	  (
	    getdeci(n)<= dmax ? str(n)
	  : str( round(n*pow(10, dmax)) /pow(10, dmax) )
	  )		  			 
	 : str(n)
	   
);

    numstr=["numstr","n,d=undef,dmax=undef,convert=undef", "string", "Number, String",
     " Convert a number to string. d = decimal
    ;; 
    ;;   numstr(3.8, d=3) = '3.800'   ... (a)
    ;;   numstr(3.8765, d=3) = '3.877' ... (b)
    ;; 
    ;; If we want to limit decimal points (b) but don't want to add
    ;; extra 0 (a), then we can use dmax:
    ;;
    ;;   numstr(3.8, dmax=3) = '3.8'
    ;;   numstr(3.8765, dmax=3) = '3.877'
    ;;
    ;;   numstr( 3.839, d=1 )= '3.8'
    ;;   numstr( 3.839, d=2 )= '3.84'
    ;;   numstr( 3.839, d=3 )= '3.839'
    ;;   numstr( 3.839, d=4 )= '3.8390'
    ;;   numstr( 3.839, d=1, dmax=2 )= '3.8'
    ;;   numstr( 3.839, d=2, dmax=2 )= '3.84'
    ;;   numstr( 3.839, d=3, dmax=2 )= '3.84'
    ;;   numstr( 3.839, d=4, dmax=2 )= '3.84'
    ;;
    ;;   numstr( 0.5,conv='ft:ftin' )= '6''
    ;;   numstr( 96,conv='in:ftin' )= '8''
    "];

    function numstr_test( mode=MODE, opt=[] )=
    (
        doctest( numstr,
        [
          "// Integer: "
        , [ numstr(0), "0", "0"]
        , [ numstr(5), "5", "5"]
        , [ numstr(5,d=2), "5.00", "5,d=2"]
        , [ numstr(5.0,d=2), "5.00", "5.0,d=2"]
        , [ numstr(5.5,d=2), "5.50", "5.5,d=2"]
        , [ numstr(5,dmax=2), "5", "5,dmax=2"]
        , [ numstr(5,d=2,dmax=2), "5.00", "5,d=2,dmax=2"]
        , [ numstr(38, d=0), "38", "38,d=0"]
        , [ numstr(38, d=2), "38.00", "38,d=2"]
        , "// Float. Note the difference between d and dmax: "
        , [ numstr(3.80), "3.8", "3.80"]
        , [ numstr(38.2, dmax=3), "38.2", "38.2,dmax=3"]
        , [ numstr(38.2, d=3, dmax=2), "38.20", "38.2,d=3, dmax=2"]
        , [ numstr(38.2, d=3, dmax=4), "38.200", "38.2,d=3, dmax=4"]
        , [ numstr(38.2, d=5, dmax=4), "38.2000", "38.2,d=5, dmax=4"]
        , [ numstr(3.839,d=0), "4", "3.839, d=0"]
        , [ numstr(3.839,d=1), "3.8", "3.839, d=1"]
        , [ numstr(3.839,d=2), "3.84", "3.839, d=2"]
        , [ numstr(3.839,d=3), "3.839", "3.839, d=3"]
        , [ numstr(3.839,d=4), "3.8390", "3.839, d=4"]
        , [ numstr(3.839,d=1, dmax=2), "3.8", "3.839, d=1, dmax=2"]
        , [ numstr(3.839,d=2, dmax=2), "3.84", "3.839, d=2, dmax=2"]
        , [ numstr(3.839,d=3, dmax=2), "3.84", "3.839, d=3, dmax=2"]
         ,[ numstr(3.839,d=4, dmax=2), "3.84", "3.839, d=4, dmax=2"]	
        , [ numstr(-3.839,d=1), "-3.8", "-3.839, d=1"]
        , [ numstr(-3.839,d=2), "-3.84", "-3.839, d=2"]
        , [ numstr(-3.839,d=4), "-3.8390", "-3.839, d=4"]
       
        , "", "// conversion: ", ""
        , "// This function handles conversion of ft:ftin and in:ftin pretty well."
        , "// All other conversions can be done with uconv(...)."
        , ""
        , [ numstr(1,conv="in:cm"), "2.54", "1,conv='in:cm'"]
        , [ numstr(-1,conv="in:cm"), "-2.54", "-1,conv='in:cm'"]
        , [ numstr(3.5, conv="ft:in"), "42", "3.5, conv='ft:in'" ]
        , "// Same as: "
        , [ str(uconv(-1,conv="in:cm")), "-2.54", "", ["fname", "str(uconv(-1,conv=\in:cm\"))"]] 
        
         ,"// conv = 'ft:ftin'"
        , [ numstr(0.5,conv="ft:ftin"), "6\"", "0.5,conv='ft:ftin'"]
        , [ numstr(-0.5,conv="ft:ftin"), "-6\"", "0.5,conv='ft:ftin'"]
        , [ numstr(3,conv="ft:ftin"), "3'", "3,conv='ft:ftin'"]
        , [ numstr(3.5,conv="ft:ftin"), "3'6\"", "3.5,conv='ft:ftin'"]
        , [ numstr(-3.5,conv="ft:ftin"), "-3'6\"", "3.5,conv='ft:ftin'"]
        , [ numstr(3.5,d=2,conv="ft:ftin"), "3'6.00\"", "3.5,d=2,conv='ft:ftin'"]
        , [ numstr(3.343,conv="ft:ftin"), "3'4.116\"", "3.343,conv='ft:ftin'"]
        , [ numstr(3.343,d=2,conv="ft:ftin"), "3'4.12\"", "3.343,d=2, conv='ft:ftin'"]
        , "// conv = 'in:ftin'"
        , [ numstr(96,conv="in:ftin"), "8'", "96,conv='in:ftin'"]
        , [ numstr(32,conv="in:ftin"), "2'8\"", "32,conv='in:ftin'"]
        , [ numstr(-32,conv="in:ftin"), "-2'8\"", "-32,conv='in:ftin'"]
        , "// d is ignored if negative:"
        , [ numstr(38, d=-1), "38", "38,d=-1"]
        , "",""
        , "// Debug: "
        , [ numstr((1284-19)/1284, d=2), "0.99", "(1284-19)/1284,d=2"]
         , [ numstr((1285-20)/1285, d=2), "0.98", "(1285-20)/1285,d=2"]
        //, [ numstr(1234567890123456), "1234567890123456"
        //    , "1234567890123456" ]
            
        ], mode=mode, opt=opt )
    );
    //echo( numstr_test(112)[1] );
}//numstr



//========================================================
{ // fullnumstr

//function fullnumstr_try(  // 2017.11.8: an attempt to make a clearer code for
//                          // easier debugging. Failed.  
//                   n, _rtn=""
//                   , _sign=undef, _isLessThan1=undef, _debug=[], _i=0)=
//(
//  /*
//     for n = 123.45:
//     
//     x= n_p    n=x*p
//    -int(n_p)          p    n_p   int(n_p)    rtn
//    ========= ======  ===  ====== ======== ==========
//              123.45  100  1.2345   1      "1"
//    0.2345     23.45   10  2.345    2      "12"
//    0.345       3.45    1  3.45     3      "123."
//    0.45        0.45  0.1  4.5      4      "123.4"  when p=0.1, add a "."
//               x/p                                  
//    0.5         5       1  5        5      "123.45" when previous p=0.1, use x/p but not x*p 
//                                                    
//  */
//  is0(n)?"0"
//  //:isint(n) && n<10? str( _sign==-1?"-":"", _rtn )
//  :let( _sign= und(_sign, n<0?-1:1)// isnum(_sign)?_sign:(n<0?-1:1) //:_sign //und(_sign, n<0?-1:1)
//      //, isLessThan1= und(isLessThan1, n<1||n>-1)
//     //_gt1= n>=1||n<=-1
//     , p = pow(10, floor(log(n<0?-n:n)))
//     , n_p = num(str(p==1?n:n/p))
//     , x = n_p-floor(n_p)
//     //, newn= _isLessThan1? x*p : x/p
//     , newn= p<1? x/p : x*p
//     , _rtn = str(_rtn, _isLessThan1?".":"", floor(n_p) ) 
//     , _debug= concat(_debug,
//                  //_fmth([ "n",n, "_sign", _sign,"p",p, "p_lt_1", p<1, "n_p", n_p
//                        //, "x(n_p-floor(n_p))",x,"newn", newn
//                        //, "isExit", isint(newn)&&newn<10 , "_rtn", _rtn ])
//                  [_fmth([ "_i", _i, "n",n 
//                        , "p",p, "p_is1", p==1, "n_p", n_p, "n_p_is_2", isequal(n_p,2)                        , "type(n)", type(n), "type(p)", type(p), "type(n_p)", type(n_p)
//                        , "x(n_p-floor(n_p))",x, _s("newn({_})", p<1?"x/p":"x*p"), newn
//                        , "type(newn)", type(newn) 
//                        , "isExit", isint(newn)&&newn<10, "_rtn", _rtn ])
//                  ]      
//                  )
//     )
//     //_debug
//     _i>=2? str("<br/>", join(_debug, "<br/><br/>"))
//     :
//     isint(newn)&&newn<10
//     ? 
//     str( _sign==-1?"-":"", _rtn )
//     : 
//     fullnumstr( n=newn, _rtn=_rtn, _sign=_sign, _debug=_debug, _i=_i+1) //, _isLessThan1=_isLessThan1)
//);

/*
  Tricky fraction in large values:
    
  Let 
     a= 8
     b= a*10^x
      = 80, 800, 8000, .... 800000000, etc
    
  Add 0.1 to each:
    
     c = 80.1, 800.1, 8000.1, ... 800000000, etc
    
  Then subtract a:
    
     d = c-b = 0.1, 0.1, 0.1 ... 0.1 etc
    
  But d is not always 0.1 (see below), and if we raise them up by 1 order:
     
     e = (c*10-b(10)/10
    
  then most issue of d is solved:
    
    ECHO: " i	b	c	d	e"
    ECHO: "--	---	---	---	-------"
    ECHO: "1	80	80.1	0.1	0.1"
    ECHO: "2	800	800.1	0.1	0.1"
    ECHO: "3	8000	8000.1	0.1	0.1"
    ECHO: "4	80000	80000.1	0.1	0.1"
    ECHO: "5	800000	800000	0.1	0.1"
    ECHO: "6	8e+006	8e+006	0.1	0.1"
    ECHO: "7	8e+007	8e+007	0.1	0.1"
    ECHO: "8	8e+008	8e+008	0.1	0.1"
    ECHO: "9	8e+009	8e+009	0.1	0.1"
    ECHO: "10	8e+010	8e+010	0.100006	0.1"
    ECHO: "11	8e+011	8e+011	0.0999756	0.1"
    ECHO: "12	8e+012	8e+012	0.0996094	0.1"
    ECHO: "13	8e+013	8e+013	0.09375	0.1"
    ECHO: "14	8e+014	8e+014	0.125	0.1"
    ECHO: "15	8e+015	8e+015	0	0"
    ECHO: "16	8e+016	8e+016	0	0"
    
 
    
  Test code:
    
  echo(str(" i", "\t", "b", "\t","c", "\t","d","\t","e"));
  echo(str("---", "\t", "---", "\t","---", "\t","---","\t","-------"));
  a= 8;  
  for(i=[1:16])
  {
     b= a*pow(10,i);
     c= b+0.1;
     d = c-b;
     e = (c*10-b*10)/10;
     echo( str(i, "\t", b, "\t",c, "\t",d,"\t",e));
  }    
  
*/
  

function fullnumstr(n, d=undef)=
(
  
  let( sign= n<0?"-":""
     , n = d>=0? round(n*pow(10,d))/pow(10,d):n
     , int= floor(abs(n))
     , p = 10000//pow(10,14)
     , frac= (abs(n)*p-int*p)/p
     //, frac= abs(n)-int
     )
  // [ abs(n)<1?"0":int, frac]  // <== debugging   
  str(sign, intstr(int)
    , frac==0 && d>0? str(".", repeat("0",d)): ""
     , frac==0 ?"":fracstr(frac, d=d, noInt0=true))

);

function fullnumstr_old( 
           n
           , d=undef // # of fraction digits requested by users after "."
                     // >>> fullnumstr(800,d=2)= "800.00"
           , dmax=15 // max fraction digits after "."
           , _rtn=""       // return buffer
           , _sign=1       // sign to be added back when exiting
           , _init_int=undef  // If init n is an int
           , _init_p=undef // Init power of 10, needed for inserting
                           // . back to the string
           , _p2go = undef // The # of *power of 10* left to handle. 
                           // Added upon solving an issue like 800000001:
                           // After the 1st round 80000 is taken out, leaving
                           // 0001 that is handled as 1. Need this _p2go to 
                           // add 000 back (2017.11.8)   
           , _init_L=undef // init length of digits                              
           , _sd=undef  // sd means the last significant digit and 
                     // a rounding up/down is needed at _sd+1
                     
           , _int_end0= 0  // Count of end 0(s) for int. This value is obtained
                           // in init for adding back "0" upon exiting              
           , _dep=0        // depth of recursion as a safe guard
           , _original=undef
           , _debug=""
           )=
(
   isint(n)? intstr(n)
   :let( init=_rtn==""
      , _sign = init?sign(n):_sign
      , _original = init?n:_original
      ,_init_int = init?(isint(n)||d==0):_init_int
      ,_int_end0 = init            // 1234000 => _int_end0 = 3
                   && _init_int?   // 1234560 => _int_end0 = 1
                  ( let( end0_list= [ for( i=[0:20] )
                                    if( n % pow(10,i))i-1]
                     ) end0_list?end0_list[0]:0
                  )                  
               :_int_end0
      , _sd = d>=0? min(d,dmax):dmax
      ,  p = getPowerOrder(abs(n)) // 12345 => p=4
                                 // 45    => p=1
                                 // 5     => p=0
                                 // 0     => p=undef
                                 // 0.2   => p=-1
                                 // 0.02  => p=-2
      , _init_L= init?(p>0?p+1:abs(p)):_init_L // init length of digits                           
        
      , n = init? round(n* pow(10,_sd))/pow(10,_sd):n
      //, n = d==0? round(pn):pn                              
      )
   
   init && _init_int && abs(n)<1000000? 
     str(n, (d>0? str(".",repeat("0",d)):"")) // 123 => "123.00" when d=2
   : let( _n = abs(n) 
        , _p2go = init? p-5 : _p2go
        , _sd = min(_sd, abs(p))                            
        , _init_p= init?p:_init_p
                             
        //, _n2 = _n<1? _n/pow(10,p):_n/pow(10,p-4)                             
        , _n2= _n/pow(10,p-4) // If n=1234567 or 0.001234567, n2=12345.7
                             // Means we pick 5 digits at a time. OpenScad's
                             // displays only 6 significant digits, and the
                             // 6th one could be a result of rounding-up of
                             // the 7th, which will not be the original 6th. 
                             // So picking 5 is safer.
                              
        , n2 = _dep>1? round(_n2):_n2 // When _dep>1, it means we already
                                      // pass the maximum sig. limit (15)
                                      // At this moment we want to round
                                      // the very last digit in the fraction
                                      //     1.3333333333333329
                                      // ==> 1.333333333333333
        
        , i = floor(num(str(n2))) // Garbage digits cleanup
                                  // We know that i has to be int. So do
                                  // a reverse type-casting, num-str-num,
                                  // to get rid of garbage digits, if exits.
                                  // Otherwise, a value 1.99999999 (should be 2)
                                  // could be flooded down to 1.
        //, f = abs(n)<1? (n2* 1e+15 - i* 1e+15  )/  1e+15
        //      : (n-i*10000) 
        , new = ((n2* 1e+15 - i* 1e+15  )/  1e+15)// * (_p2go?pow(10,_p2go+1):1)
               
              /*
                 f (fraction) is used to check exit condition. It is 
                 the n2-i, but we know that simple arithmetic operation
                 of small values causes error due to binary rounding:
                 http://forum.openscad.org/Simple-addition-of-numbers-introduces-error-tp14408.html
                 So we raise it up by an order of 15, do the subtraction
                 then bring it back down 
              */
        
        , isexit = new< 1e-14 || (!_init_int && _dep>1)
                   /* Set the exit condition. Note that this f<1e-14 depends
                      on the raising-order in previous step: pow(10,15)
                   */
                   
        , __rtn=str( _rtn
                   , isexit ? 
                       str( p<0 && abs(p)!=abs(_init_p)? repeat("0",-p-1):"" 
                             //-- abs(p)!=abs(_init_p): 
                             //   Fix fullnumstr(0.0111) // want: "0.0111" got: "0.00111"  
                             //-- repeat 0: fix the bug of failing
                             //   to add 02 in this case:
                             //   3.000002=> 3.00002 
                          ,  //_p2go>0 ? repeat("0", _p2go):""            
                          , trim(str(i), r="0") // trim away ending 0's
                                                // when exiting 
                          //, "x"                      
                          )
                       : i //_dep>1? round(i):i 
                   ) 
        , _rtn = isexit?
         
                 let( rtn= str( 
                 
                           _sign==-1?"-":""
                     
                          , _int_end0 ? // Add ending 0s back for int
                        
                              str( __rtn , repeat("0", _int_end0) )  // for int
                               
                            :_init_p<0? // Add preceeding 0s back for float
                          
                                str( 
                                    "0." 
                                   , repeat("0", -_init_p-1)  
                                        // In cases like 0.~2300034~, the 000 could
                                        // be skipped by log() when next step is 3.4~ 
                                   , __rtn                   
                                   )
                                   
                            : _init_p< len(__rtn)-1 ?  // Add "." back 
                            
                                   insert(__rtn, ".", _init_p+1)
                                   
                            :__rtn //str("y",__rtn) 
                                
                         , _init_int?
                            "" // (d>0? str(".",repeat("0",d)):"")
                           :d>0?
                              let( fd = len(__rtn)-_init_p-1 )
                                repeat("0", min(d,dmax)-fd)
                              :""
                        )//str
               )//let     
                ( // take care of (800.01,d=1)=> "800.0"
                  d>=0&& index(rtn,".")==-1? insert(rtn, ".",-d)
                   //str(rtn, ".", repeat("0",d))
                 :rtn
                )     
               : __rtn
               
      , _debug = str( _debug
                   , "<hr/>"
                   , "<br/>n = ", n
                   , "<br/>_original = ", _original
                   , "<br/>_dep = ", _dep  
                   , "<br/>d (requested fraction digits)= ", d  
                   , "<br/>dmax = ", dmax
                   , "<br/>_sd (the last significant digit)= ", _sd  
                   , "<br/>_init_p = ", _init_p
                   , "<br/>_init_int = ", _init_int
                   , "<br/>_int_end0 = ", _int_end0
                   , "<br/>n = ", n  
                   , "<br/>p = ", p  
                   , "<br/>_p2go = ", _p2go  
                   , "<br/>n2 (= n/100000 )= ", n2
                   , "<br/>i (assured int of n2) = floor(n2) = ", i
                   , "<br/>new ( fraction left ) =n2-i = ", new   
                   , "<br/>isexit (f&lt;1e-14 || (!_init_int && _dep>1))= ", isexit  
                   , "<br/>__rtn (i or i_trim_0) = ", __rtn  
                   , "<br/>_rtn = ", _rtn  
                   , "<br/>x= ", str( p<0? repeat("0",-p-1):""
                          , trim(str(i), r="0")  
                          )
                   )
      )
  isexit ?
        //[_debug,_rtn]
        _rtn
        
//      /* NOTE 2016/11/12: does the following issue still exists in the 
//          current version?
//
//         For final return, _rtn is supposed to be good enough.
//         But we encounter cases like :
//            p= 15: -82.005866719659800
//            p= 14: -82.00586671965980
//            p= 13: -82.0058667196598
//            p= 12: -82.0058667196991
//            p= 11: -82.0058667199988
//            p= 10: -82.0058667199988
//            p= 09: -82.005866710
//            p= 08: -82.00586671
//         In which: 1) len is incorrect 2) value is off
//         We try to at least make the len correct. Note this
//         doesn't solve the value problem so we still have this;
//         
//            n= -54.149821406573
//            p= 15: -54.149821406573000
//            p= 14: -54.14982140657300
//            p= 13: -54.1498214065701
//            p= 12: -54.149821406599 <== wrong
//            p= 11: -54.14982140700 <== wrong
//            p= 10: -54.1498214099 <== wrong
//            p= 09: -54.149821400
//            
//         NOTE: these problems do exist if it is carried out 
//         individually:
// 
//            fullnumstr(-54.149821406573, d=12)=> "-54.149821406573"
//            fullnumstr(-54.149821406573, d=11)=> "-54.14982140657"
//            fullnumstr(-54.149821406573, d=10)=> "-54.1498214066" 
//      
//      */ 
//      let( final = d>0 ? let( x= split(_rtn,".") ) 
//                         str( x[0],".",slice(x[1],0, d)): _rtn 
//                         )  
//      final 
        
   : fullnumstr( n=new, _rtn =_rtn
               , d= d
               , dmax=dmax
               , _sd = _sd
               , _init_int = _init_int
                , _int_end0 = _int_end0
                , _dep=_dep+1, _init_p=_init_p, _p2go=_p2go, _sign=_sign
                , _original = _original
        , _debug = _debug 
        )       
);

    fullnumstr=["fullnumstr","n [,d]","str","str"
    ,"Return the full number string representation of n.
    ;; 
    ;;    d: # of digit in the fraction
    ;;
    ;; Openscad displays only 6 significant digits. Beyond that,
    ;; rounding up or down of number 5 follows an irregular and 
    ;; unpredictable manner. See 
    ;; 
    ;; http://forum.openscad.org/Inconsistent-conversion-of-floating-number-to-string-at-7th-significant-digit-td14350.html
    ;; 
    ;; The above discussion also exposes OpenScad's failure to 
    ;; round numbers in some cases. 
    ;;
    ;; This func returns a string of the full number of n, and, if d
    ;; is specified, it rounds up/down values correctly. 
    ;; 
    ;; Note: it returns correct digits only up to the 15th significant 
    ;; digits, which is the internal precision limit of OpenScad.
    ;; 
    ;;  fullnumstr(0.123456789012345 )=> '0.123456789012345'
    ;;  fullnumstr(0.1234567890123456)=> '0.123456789012345579976681618063...'
    ;;  fullnumstr(1234567890.12345 )=> '1234567890.12345'
    ;;  fullnumstr(1234567890.123456)=> '1234567890.1234559816657565352286...'
    ;; 
    "];

    function fullnumstr_test( mode=12, opt=[])=
    (
        doctest( fullnumstr,
        [
          ""
        ,"<b>Integer:</b>"
        , ""
        , [ fullnumstr(0), "0", "0"]
        , [ fullnumstr(5), "5", "5"]
        , [ fullnumstr(12), "12", "12" ]
        , [ fullnumstr(200), "200", "200" ]
        , [ fullnumstr(1230), "1230", "1230" ]
        , [ fullnumstr(123456), "123456", "123456" ]
        , [ fullnumstr(1234567), "1234567", "1234567" ]
        , [ fullnumstr(123456789), "123456789", "123456789" ]
        , [ fullnumstr(123456000), "123456000", "123456000" ]
        , [ fullnumstr(12345670), "12345670", "12345670" ]
        , [ fullnumstr(123456789012345), "123456789012345", "123456789012345" ]
        , [ fullnumstr(-123456789012345), "-123456789012345", "-123456789012345" ]
        , [ fullnumstr(12345678901234500), "12345678901234500", "12345678901234500" ]
        ,""
        ,"<b>Float:</b>"
        , ""
        , [ fullnumstr(1234.5), "1234.5", "1234.5" ]
        , [ fullnumstr(12345.6), "12345.6", "12345.6" ]
        , [ fullnumstr(12345.67), "12345.67", "12345.67" ]
        , [ fullnumstr(0.12345), "0.12345", "0.12345" ]
        , [ fullnumstr(0.12), "0.12", "0.12" ]
        , [ fullnumstr(0.123), "0.123", "0.123" ]
        , [ fullnumstr(0.1234), "0.1234", "0.1234" ]
        , [ fullnumstr(0.123456), "0.123456", "0.123456" ] 
        , [ fullnumstr(0.0000123456), "0.0000123456", "0.0000123456" ] 
        , [ fullnumstr(0.123456789012345), "0.123456789012345", " 0.123456789012345" ]
        , [ fullnumstr(-0.123456789012345), "-0.123456789012345", "-0.123456789012345" ]
        , [ fullnumstr(0.1), "0.1", "0.1" ]
        , [ fullnumstr(0.11), "0.11", "0.11" ]
        , [ fullnumstr(0.111), "0.111", "0.111" ]
        , [ fullnumstr(0.0111), "0.0111", "0.0111" ]
        , [ fullnumstr(0.00111), "0.00111", "0.00111" ]
        , [ fullnumstr(0.01), "0.01", "0.01" ]
        , [ fullnumstr(0.001), "0.001", "0.001" ]
        , [ fullnumstr(0.0001), "0.0001", "0.0001" ]
        , [ fullnumstr(0.00001), "0.00001", "0.00001" ]
        , [ fullnumstr(0.000001), "0.000001", "0.000001" ]
        , [ fullnumstr(0.0000001), "0.0000001", "0.0000001" ]
        , [ fullnumstr(0.00000001), "0.00000001", "0.00000001" ]
        , [ fullnumstr(0.000000001), "0.000000001", "0.000000001" ]
        , [ fullnumstr(0.0000000001), "0.0000000001", "0.0000000001" ]
        , [ fullnumstr(0.00000000001), "0.00000000001", "0.00000000001" ]
        , [ fullnumstr(0.0000000000001), "0.0000000000001", "0.0000000000001" ]
        , [ fullnumstr(-0.000000001), "-0.000000001", "-0.000000001" ]
        , [ fullnumstr(-0.0000000001), "-0.0000000001", "-0.0000000001" ]
        , [ fullnumstr(1/3), "0.333333333333333", "1/3" ]
        , [ fullnumstr(2/3), "0.666666666666666", "2/3", ["//", "Note this"]  ]
        , [ fullnumstr(2/3, d=15), "0.666666666666666", "2/3, d=15", ["//", "Note this"] ]
        , [ fullnumstr(2/3, d=14), "0.66666666666667", "2/3, d=14", ["//", "Note this"] ]
        , [ fullnumstr(2/3,d=5), "0.66667", "2/3,d=5" ]
        , [ fullnumstr(1/6), "0.166666666666666", "1/6" ]
        , [ fullnumstr(2/7), "0.285714285714285", "2/7" ]
        , [ fullnumstr(2/7, d=3), "0.286", "2/7, d=3" ]
        , [ fullnumstr(2/7, d=4), "0.2857", "2/7, d=4" ]
        , [ fullnumstr(3.000002), "3.000002", "3.000002" ]
        , [ fullnumstr(3.0000002), "3.0000002", "3.0000002" ]
        , [ fullnumstr(3.00000002), "3.00000002", "3.00000002" ]
        , [ fullnumstr(3.000000002), "3.000000002", "3.000000002" ]
        , [ fullnumstr(30.000002), "30.000002", "30.000002" ]
        , [ fullnumstr(-300.00000002), "-300.00000002", "-300.00000002" ]
        , [ fullnumstr(-3.00000002), "-3.00000002", "-3.00000002" ]
        , [ fullnumstr(-3.000000002), "-3.000000002", "-3.000000002" ]
        , [ fullnumstr(-30.000002), "-30.000002", "-30.000002" ]
        , [ fullnumstr(-300.00000002), "-300.00000002", "-300.00000002" ]
        
        , ""
        , "<b>Use d to set digits of fraction</b>: "
        , ""
        , [ fullnumstr(5,d=2), "5.00", "5,d=2"]
        , [ fullnumstr(-5,d=2), "-5.00", "-5,d=2"]
        //, [ fullnumstr(5,dmax=2), "5", "5,dmax=2"]
        , [ fullnumstr(38, d=0), "38", "38,d=0"]
        , [ fullnumstr(38, d=2), "38.00", "38,d=2"]
        , [ fullnumstr(80,d=0), "80", "80,d=0" ]
        
        , [ fullnumstr(3.80), "3.8", "3.80"]
//        , [ fullnumstr(38.2, dmax=3), "38.2", "38.2,dmax=3"]
//        , [ fullnumstr(38.2, d=3, dmax=2), "38.20", "38.2,d=3, dmax=2"]
//        , [ fullnumstr(38.2, d=3, dmax=4), "38.200", "38.2,d=3, dmax=4"]
//        , [ fullnumstr(38.2, d=5, dmax=4), "38.2000", "38.2,d=5, dmax=4"]
        , [ fullnumstr(3.839,d=0), "4", "3.839, d=0"]
        , [ fullnumstr(3.839,d=1), "3.8", "3.839, d=1"]
        , [ fullnumstr(3.839,d=2), "3.84", "3.839, d=2"]
        , [ fullnumstr(3.839,d=3), "3.839", "3.839, d=3"]
        , [ fullnumstr(3.839,d=4), "3.8390", "3.839, d=4"]
//        , [ fullnumstr(3.839,d=1, dmax=2), "3.8", "3.839, d=1, dmax=2"]
//        , [ fullnumstr(3.839,d=2, dmax=2), "3.84", "3.839, d=2, dmax=2"]
//        , [ fullnumstr(3.839,d=3, dmax=2), "3.84", "3.839, d=3, dmax=2"]
//        , [ fullnumstr(3.839,d=4, dmax=2), "3.84", "3.839, d=4, dmax=2"]	
//        , [ fullnumstr(43.839,d=4, dmax=2), "43.84", "43.839, d=4, dmax=2"]	
        , [ fullnumstr(-3.839,d=1), "-3.8", "-3.839, d=1"]
        , [ fullnumstr(-3.839,d=2), "-3.84", "-3.839, d=2"]
        , [ fullnumstr(-3.839,d=4), "-3.8390", "-3.839, d=4"]    
        , ""
        , [ fullnumstr(0.123456789012345,d=6), "0.123457", "0.123456789012345,d=6" ]
        , [ fullnumstr(0.123456789012345,d=7), "0.1234568", "0.123456789012345,d=7" ]
        , [ fullnumstr(0.123456789012345,d=8), "0.12345679", "0.123456789012345,d=8" ]
        , [ fullnumstr(0.123456789012345,d=9), "0.123456789", "0.123456789012345,d=9" ]
        , [ fullnumstr(800,d=2), "800.00", "800,d=2" ]
        , [ fullnumstr(800.1,d=0), "800", "800.1,d=0" ]
        , [ fullnumstr(800.01,d=0), "800", "800.01,d=0" ]
        , [ fullnumstr(800.01,d=1), "800.0", "800.01,d=1" ]
        , [ fullnumstr(801 ), "801" ,"801"]
        , [ fullnumstr(8001 ), "8001" ,"8001"]
        , [ fullnumstr(80001 ), "80001" ,"80001"]
        , [ fullnumstr(800001 ), "800001" ,"800001"]
        , [ fullnumstr(8000001 ), "8000001" ,"8000001"]
        , [ fullnumstr(80000001 ), "80000001" ,"80000001"]
        , [ fullnumstr(800000001 ), "800000001" ,"800000001"]
        , [ fullnumstr(8000000001 ), "8000000001" ,"8000000001"]
        , [ fullnumstr(80000000001 ), "80000000001" ,"80000000001"]
        , [ fullnumstr(800000000009), "800000000009","800000000009"]
        , [ fullnumstr(800000000008), "800000000008","800000000008"]
        , [ fullnumstr(800000000007), "800000000007","800000000007"]
        , [ fullnumstr(800000000006), "800000000006","800000000006"]
        , [ fullnumstr(800000000005), "800000000005","800000000005"]
        , [ fullnumstr(800000000004), "800000000004","800000000004"]
        , [ fullnumstr(800000000003), "800000000003","800000000003"]
        , [ fullnumstr(800000000002), "800000000002","800000000002"]
        , [ fullnumstr(800000000001), "800000000001","800000000001"]
        , [ fullnumstr(8000000000001), "8000000000001","8000000000001"]
        , [ fullnumstr(800000000000), "800000000000","800000000000"]
        , [ fullnumstr(800000000000.1), "800000000000.1","800000000000.1"]
        , [ fullnumstr(800000000000.01), "800000000000.01","800000000000.01"]
        , [ fullnumstr(800000000000.01,d=1), "800000000000.0",
                                              //8000000000000.0
       "800000000000.01,d=1" ]
        , [ fullnumstr(79.516556302228700,d=0), "80", "79.516556302228700,d=0" ]
        , [ fullnumstr(5.000000001), "5.000000001", "5.000000001"]
        , [ fullnumstr(5.0000000001), "5.0000000001", "5.0000000001"]
        , [ fullnumstr(0.123456789012345), "0.123456789012345", "0.123456789012345"]
        
        , ""
        , "Cannot stringify correctly if the significant digits > 15:" 
        , ""
        
        , str( "fullnumstr(800000000000.0999756) = \"", fullnumstr(800000000000.0999756), "\"")
        , str( "fullnumstr(12345678901234567890) = \"", fullnumstr(12345678901234567890), "\"")
        , str( "fullnumstr(123456789012345.123456789012345) = \"", fullnumstr(123456789012345.123456789012345), "\"")
        , str( "fullnumstr(1234567890123.123456789012345) = \"", fullnumstr(1234567890123.123456789012345), "\"")
        , str( "fullnumstr(12345678901.123456789012345) = \"", fullnumstr(12345678901.123456789012345), "\"")
        , str( "fullnumstr(123456789.123456789012345) = \"", fullnumstr(123456789.123456789012345), "\"")
        //, [ fullnumstr(800000000000.0999756), "800000000000.0999756","800000000000.0999756", ["mode", 102] ]
        //, [ fullnumstr(800000000000.0999756, d=5), "800000000000.09998","800000000000.0999756, d=5"]
        //, [ fullnumstr(12345678901234567890), "12345678901234567890", "12345678901234567890", ["//", 12345678901234567890] ]
        //, [ fullnumstr(123456789012345.123456789012345), "123456789012345.123456789", "123456789012345.123456789012345"]
        //, [ fullnumstr(1234567890123.123456789012345), "1234567890123.123456789", "1234567890123.123456789012345"]
        //, [ fullnumstr(12345678901.123456789012345), "12345678901.123456789", "12345678901.123456789012345"]
        //, [ fullnumstr(123456789.123456789012345), "123456789.123456789", "123456789.123456789012345"]
        
        ]
        , mode=mode, opt=opt
        )
    );
    
    module fullnumstr_demo(){
        pt3 = pqr();
        echo( pt3 = pt3 );

        function showPts( pts, d=undef, dmax=15)=
        (
           let( pts = [ for(p=pts)
                          [for(n=p) fullnumstr( n, d=d, dmax=dmax )]
                      ]
              )
           pts
        );
        echo( str("<br/>", arrprn( showPts( pt3 ) ) ) );                  
        echo( str("d=10: ", "<br/>", arrprn( showPts( pt3, 10 ) ) ));
        echo( str("d= 6: ", "<br/>", arrprn( showPts( pt3, 6 ) ) ) );
        echo( str("d= 3: ", "<br/>", arrprn( showPts( pt3, 3 ) ) ) );
        
    }          

    module fullnumstr_more_tests(ntest= 10){

       for( i=[1:ntest] ){
          n0 = rand([1,-1])*rand(1); //rand(pow(10,16) );
          n1 = num(fullnumstr(n0)); 
                // Make sure it's what it appears
                // This prevents errors like :
                // p= 15: -91.501405099361300
                // p= 14: -91.50140509936270
                // p= 13: -91.5014050993598
                // p= 12: -91.5014050993991
                // p= 11: -91.5014050990004
                // p= 10: -91.5014050999986
                // p= 09: -91.501405090
                
          s = str( "<br/>", i, ": ", fullnumstr(n1),
                 , "<br/>"
                 , join([ for(pp=[0:15] //[15:-1:0]
                              ) 
                          let(p = round(15-pp))
                      str("p= ", len(str(p))==1?"0":"", fullnumstr(p), ": ", fullnumstr(n1,d=p)) ], "<br/>")
                 );
          echo(s);
       }
    }    
    
} // fullnumstr


function intstr(n, _rtn="", _sign="", _nextlen=undef, _pre0s="", _debug=[])= // 0.1234, 0.000123
( /*
    Get the full str representation of an int (n). 
    
     OpenScad displays only 6 significant digits, and the 6th one could be 
     a result of rounding-up of the 7th, which will not be the original 6th. 
     
      echo(str(12345))    => "12345"
      echo(str(123456))   => "123456"
      echo(str(1234567))  => "1.23457e+006" 
      echo(str(12345678)) => "1.23457e+006"
      
     So it's safe to pick 5 digits at a time:
     
      12345|67890|123 ... 
      
     So the first run takes 5 digits and use str(n5) directly.
     The last run -- supposed to be 5 or less digits, use str(), too. 
     
     Later runs need to take care of :
     
         |00003|  -- pre0s
         |30000|  -- post0s
         |00300|  -- pre and post 0s

  */
  abs(n)< 100000? // can be directly strigified with str()
      let( final= str(_sign
                     , _rtn
                     , _pre0s
                     , n
                     , is0(n)&& _nextlen>1?   // = post0s like 1234500 
                       repeat("0", _nextlen-1):"" 
                     )
         , _debug = concat( _debug
                          , [[ "final", final]] 
                          ) 
         , __debug = str("<br/>", join( [for(d=_debug) _fmth(d)], "<br/>"))   
         )
      final
      //__debug      
      
  : _rtn=="" ?
   let( _sign = n<0?"-":""
      , _n=abs(n)
      , _len= getPowerOrder(_n)+1
      , n5    = _n/pow(10, _len-5)
      , n5int = floor(num(str(n5)))
      , _new_n = (n5* 1e+15 - n5int * 1e+15  )/  1e+15
      , new_n = int(_new_n * pow(10, _len-5)) 
      , _nextlen = _len-5
      , _pre0s = repeat("0", _nextlen-getPowerOrder(new_n)-1)
      , _rtn = str(n5int)
      )
      intstr( n = new_n
            , _rtn = _rtn 
            , _sign = _sign
            , _nextlen = _nextlen
            , _pre0s = _pre0s
            , _debug= [[ "n", n
                       , "_sign", _sign
                       , "_len", _len
                       , "_nextlen", _nextlen
                       , "getPowerOrder(new_n)", getPowerOrder(new_n)
                       , "n5", n5
                       , "n5int", n5int
                       , "_new_n", _new_n  
                       , "new_n", new_n  
                       , "_rtn", _rtn  
                       , "_pre0s", _pre0s 
                       ]]
            )
      
   : let( _len= getPowerOrder(n)+1
        , _expectedlen = _nextlen
        , _nextlen = _nextlen-5
        , n5    = n/pow(10, _expectedlen-5)
        , n5int = floor(num(str(n5)))
        , _new_n = (n5* 1e+15 - n5int * 1e+15  )/  1e+15
        , new_n = int(_new_n * pow(10, _nextlen)) 
        //, new = int(_new*pow(10, init?(p-4):(_len2go-5)))
        , _rtn = str(_rtn
                    , _pre0s //repeat("0", _expectedlen - _len)
                    , n5int)
        , _pre0s =  repeat("0", _nextlen-getPowerOrder(new_n)-1)
        )
        intstr( n = new_n
              , _rtn = _rtn 
              , _sign = _sign
              , _nextlen = _nextlen
              , _pre0s = _pre0s
              , _debug= concat( _debug, [[ "n", n
                         , "_sign", _sign
                         , "_expectedlen", _expectedlen
                         , "_len", _len
                         , "_nextlen", _nextlen
                         , "n5", n5
                         , "n5int", n5int
                         , "_new_n", _new_n  
                         , "new_n", new_n  
                         , "_rtn", _rtn  
                         , "_pre0s", _pre0s 
                         ]])
              )
);    


    intstr=["intstr", "n", "number", "num",
    " Get the power order of given number n. 
    "];

    function intstr_test( mode=MODE, opt=[] )=
    (
        doctest( intstr,
        [
         [ intstr(0), "0", "0"]
        , [ intstr(5), "5", "5"]
        , [ intstr(12), "12", "12" ]
        , [ intstr(200), "200", "200" ]
        , [ intstr(1230), "1230", "1230" ]
        , [ intstr(12345), "12345", "12345" ]
        , [ intstr(123456), "123456", "123456" ]
        , [ intstr(1234501), "1234501", "1234501" ]
        , [ intstr(1234567), "1234567", "1234567" ]
        , [ intstr(12345670), "12345670", "12345670" ]
        , [ intstr(123456000), "123456000", "123456000" ]
        , [ intstr(123456789), "123456789", "123456789" ]
        , [ intstr(1234567890), "1234567890", "1234567890" ]
        , [ intstr(12345678901), "12345678901", "12345678901" ]
        , [ intstr(123456789012), "123456789012", "123456789012" ]
        , [ intstr(1234567890123), "1234567890123", "1234567890123" ]
        , [ intstr(12345678901234), "12345678901234", "12345678901234" ]
        , [ intstr(123456789012345), "123456789012345", "123456789012345" ]
        , [ intstr(1234567890123456), "1234567890123456", "1234567890123456" ]
        , [ intstr(12345678901234567), "12345678901234567", "12345678901234567" ]
        , [ intstr(12345678901234500), "12345678901234500", "12345678901234500" ]       
        , [ intstr(-123456789012345), "-123456789012345", "-123456789012345" ]
        , [ intstr(8001 ), "8001" ,"8001"]
        , [ intstr(80001 ), "80001" ,"80001"]
        , [ intstr(800001 ), "800001" ,"800001"]
        , [ intstr(8000001 ), "8000001" ,"8000001"]
        , [ intstr(80000001 ), "80000001" ,"80000001"]
        , [ intstr(800000001 ), "800000001" ,"800000001"]
        , [ intstr(8000000001 ), "8000000001" ,"8000000001"]
        , [ intstr(80000000001 ), "80000000001" ,"80000000001"]
        , [ intstr(800000000001 ), "800000000001" ,"800000000001"]
        , [ intstr(8000000000001 ), "8000000000001" ,"8000000000001"]
        , [ intstr(80000000000001 ), "80000000000001" ,"80000000000001"]
        , [ intstr(800000000000001 ), "800000000000001" ,"800000000000001"]
        , [ intstr(800010000000001 ), "800010000000001" ,"800010000000001"]
        , [ intstr(800020000300001 ), "800020000300001" ,"800020000300001"]
        , [ intstr(800200030004001 ), "800200030004001" ,"800200030004001"]
        , [ intstr(800000000009), "800000000009","800000000009"]
        , [ intstr(800000000008), "800000000008","800000000008"]
        , [ intstr(800000000007), "800000000007","800000000007"]
        , [ intstr(800000000006), "800000000006","800000000006"]
        , [ intstr(800000000005), "800000000005","800000000005"]
        , [ intstr(800000000004), "800000000004","800000000004"]
        , [ intstr(800000000003), "800000000003","800000000003"]
        , [ intstr(800000000002), "800000000002","800000000002"]
        , [ intstr(800000000001), "800000000001","800000000001"]
        , [ intstr(8000000000001), "8000000000001","8000000000001"]
        , [ intstr(800000000000), "800000000000","800000000000"]         
        ],mode=mode, opt=opt )
    );



function fracstr(f, d=undef, noInt0=false, _rtn="", _sign=1, _i=0, _debug="")= // 0.1234, 0.000123
( 
  // 
  // noInt0: if true, "0.123" will be ".123"
  //
  let( _sign = _rtn==""?(f<0?"-":""):_sign
     , _f=abs( d>=0? round( f*pow(10,d))/pow(10,d) // rounding 
                   : f )
     , p= getPowerOrder(_f) // should be <0
     , powered = _f* pow(10,-p)
     , _int = floor(num(str(powered)))
     //, new = powered - _int
     , new = ((powered* 1e+15 - _int* 1e+15  )/  1e+15)
     , _rtn = str(_rtn
                 , _rtn==""? (noInt0?".":"0."):""
                 , p==-1?"":repeat("0", -p-1)
                 , _int 
                   //+ ( _i==d-1? ( floor(10*new)>4?1:0):0 ) // rounding the last digit
                 //, _i>=d-1?str("(",10*new,")"):""
                 ) 
  
     , _debug = str( _debug
                   , "<hr/>"
                   , "<br/>_i = ", _i
                   , "<br/>_sign = ", _sign
                   , "<br/>f = ", f
                   , "<br/>p = ", p  
                   , "<br/>powered = ", powered  
                   , "<br/>_int = ", _int  
                   , "<br/>new = ", new  
                   , "<br/>_rtn = ", _rtn  
                   , "<hr/>"
                   )
    )
    is0(new)? 
       str( _sign, _rtn
          , _i<d? repeat("0", d-_i-1):""  // Add post-0s
          )  
    : _i>=14||_i>=d-1 ? //_debug
                      str(_sign,_rtn)
    : fracstr(f=new, d=d, _rtn=_rtn, _sign=_sign, _i=_i+abs(p), _debug=_debug) 
    //_rtn
);    


    fracstr=["fracstr", "n", "number", "num",
    " Get the power order of given number n. 
    "];

    function fracstr_test( mode=MODE, opt=[] )=
    (
        doctest( fracstr,
        [
          [ fracstr(0.1234), "0.1234" , "'0.1234'"]
        , [ fracstr(0.004), "0.004" , "'0.004'"]
        , [ fracstr(0.1234), "0.1234", "0.1234" ]
        , [ fracstr(0.120034), "0.120034", "0.120034" ]
        , [ fracstr(0.123456), "0.123456", "0.123456" ] 
        , [ fracstr(0.10203040506), "0.10203040506", "0.10203040506" ] 
        , [ fracstr(0.0000123456), "0.0000123456", "0.0000123456" ] 
        , [ fracstr(0.1234567891), "0.1234567891", " 0.1234567891" ]
        , [ fracstr(0.12345678911), "0.12345678911", " 0.12345678911" ]
        , [ fracstr(0.12345678901), "0.12345678901", " 0.12345678901" ]
        , [ fracstr(0.123456789012), "0.123456789012", " 0.123456789012" ]
        , [ fracstr(0.1234567890123), "0.1234567890123", " 0.1234567890123" ]
        , [ fracstr(0.12345678901234), "0.12345678901234", " 0.12345678901234" ]
        , [ fracstr(0.123456789012345), "0.123456789012345", " 0.123456789012345" ]
        , [ fracstr(0.123456789012345,d=3), "0.123", " 0.123456789012345, d=3" ]
        , [ fracstr(0.123456789012345,d=5), "0.12346", " 0.123456789012345, d=5" ]
        , [ fracstr(0.123456789012345,d=8), "0.12345679", " 0.123456789012345, d=8" ]
        , [ fracstr(-0.123456789012345), "-0.123456789012345", "-0.123456789012345" ]
        , [ fracstr(0.1), "0.1", "0.1" ]
        , [ fracstr(0.1,d=1), "0.1", "0.1, d=1" ]
        , [ fracstr(0.1,d=2), "0.10", "0.1, d=2" ]
        , [ fracstr(0.1,d=3), "0.100", "0.1, d=3" ]
        , [ fracstr(0.1,d=4), "0.1000", "0.1, d=4" ]
        , [ fracstr(0.11), "0.11", "0.11" ]
        , [ fracstr(0.111), "0.111", "0.111" ]
        , [ fracstr(0.0111), "0.0111", "0.0111" ]
        , [ fracstr(0.00111), "0.00111", "0.00111" ]
        , [ fracstr(0.01), "0.01", "0.01" ]
        , [ fracstr(0.001), "0.001", "0.001" ]
        , [ fracstr(0.0001), "0.0001", "0.0001" ]
        , [ fracstr(0.00001), "0.00001", "0.00001" ]
        , [ fracstr(0.000001), "0.000001", "0.000001" ]
        , [ fracstr(0.0000001), "0.0000001", "0.0000001" ]
        , [ fracstr(0.00000001), "0.00000001", "0.00000001" ]
        , [ fracstr(0.000000001), "0.000000001", "0.000000001" ]
        , [ fracstr(0.0000000001), "0.0000000001", "0.0000000001" ]
        , [ fracstr(0.00000000001), "0.00000000001", "0.00000000001" ]
        , [ fracstr(0.0000000000001), "0.0000000000001", "0.0000000000001" ]
        , [ fracstr(-0.000000001), "-0.000000001", "-0.000000001" ]
        , [ fracstr(-0.0000000001), "-0.0000000001", "-0.0000000001" ]
        
        , [ fracstr(1/3), "0.333333333333333", "1/3" ]
        , [ fracstr(2/3), "0.666666666666666", "2/3", ["//", "Note this"] ]
        , [ fracstr(2/3, d=15), "0.666666666666666", "2/3, d=15", ["//", "Note this"] ]
        , [ fracstr(2/3, d=14), "0.66666666666667", "2/3, d=14", ["//", "Note this"] ]
        , [ fracstr(2/3,d=5), "0.66667", "2/3,d=5" ]
        
        , [ fracstr(0.000002), "0.000002", "0.000002" ]
        , [ fracstr(0.0000002), "0.0000002", "0.0000002" ]
        , [ fracstr(0.00000002), "0.00000002", "0.00000002" ]
        , [ fracstr(0.000000002), "0.000000002", "0.000000002" ]
        ],mode=mode, opt=opt )
    );


    

function getPowerOrder(n)= // 1234=>3, 1=>0, 0.1=>-1, 0.00123=>-3 (new 2017.11.8)
(
  isnum(n)? floor(num(str(log(n))))
  :undef
              // Garbage digits cleanup:
              // We know that it has to be int. So do
              // a reverse type-casting, num-str-num,
              // to get rid of garbage digits before flood().
              // Otherwise, a value 1.99999999999 (should be 2)
              // could be flooded down to 1.
);

    getPowerOrder=["getPowerOrder", "n", "number", "num",
    " Get the power order of given number n. 
    "];

    function getPowerOrder_test( mode=MODE, opt=[] )=
    (
        doctest( getPowerOrder,
        [
         [ getPowerOrder(1234), 3 , "1234"]
        ,[ getPowerOrder(4), 0 , "4"]
        ,[ getPowerOrder(0), undef , "0"]
        ,[ getPowerOrder(0.1234), -1 , "0.1234"]
        ,[ getPowerOrder(0.01234), -2 , "0.01234"]
        ,[ getPowerOrder(123.01234), 2 , "123.01234"]
        ,[ getPowerOrder(0.0001), -4 , "0.0001"]
        ,""
        ,[ getPowerOrder(-12345), undef , "-12345"]
        ,[ getPowerOrder(abs(-12345)), 4 , "abs(-12345)"]
        ,""
        ,[ getPowerOrder("a"), undef , "'a'"]
        ,[ getPowerOrder(undef), undef , undef]
        ,""
        ,[ getPowerOrder(0.004), -3 , "0.004"]
        ,[ getPowerOrder(0.00012), -4 , "0.00012"]
         
        ],mode=mode, opt=opt )
    );



//=============================================================
//roundintstr=["roundintstr","t, i","arr","str"
//, "For internal use. Rounding up/down a string representation of an int, t, at
//;; the i position:
//;;
//;;  roundintstr('12345', 2) => ['12',0]
//;;  roundintstr('1256', 2) => ['13',0]
//;;  roundintstr('995', 2) =>  ['00',1]
//;;  roundintstr('95', 1) =>  ['0',1]
//"];
//
//function roundintstr(t,i, isroundup=false, _rtn="",_sign="")=
//(
//    let( i   = begwith(t,"-")?i+1:i 
//       , _rtn= _rtn==""?t:_rtn  
//       , rtn = i<=len(t)? _rtn
//       
//       )
//    : 
//        
//
//);




function tobin(n, _rtn=0, _l=0, _i=0)= // 2018.12.26
(
  /*
  log(2,4) = 2
  log(2,8) = 3
  */
  //echo("--------------", _i=_i)
  n<2? _rtn+n
  :let( l= n%2
     ,  p= floor(log(2,n-l))
     , _rtn2= _rtn+ pow(10,p)
     )  
   tobin( n-pow(2,p), _rtn2, _l=l, _i=_i+1) 
      
); 
  
    tobin=["tobin","n","n","core"
    ,"Convert n to binary"
    ];
    
    function tobin_test( mode=MODE, opt=[] )=
    (

        doctest( tobin, 
        [   
          [tobin(0),0,0]
        , [tobin(1),1,1]
        , [tobin(2),10,2]
        , [tobin(3),11,3]
        , [tobin(4),100,4]
        , [tobin(5),101,5]
        , [tobin(6),110,6]
        , [tobin(10),1010,10 ]
        ]	
        ,mode=mode, opt= opt //update( opt, ["showscope",true])
        ,scope=[]
        )
    );
    
    


UCONV_TABLE = [ "in:cm", 2.54
			  , "cm:in", 1/2.54
			  , "ft:cm", 2.54*12
			  , "cm:ft", 1/(2.54*12)
			  , "ft:in", 12
			  , "in:ft", 1/12	
			  ];
function uconv(n,units="in:cm", utable=UCONV_TABLE)=
(  let( 
	  frto = split(units,":")  
    //, isone = len(frto)==1    // units = "cm", "in", ... without ","
    , isftin= endwith(n, ["'","\""] ) 
	, m    = isftin? num(n) // if n is ftin str, conv to number in inches
             : isnum(num(n))?num(n):n //or(num(n), n) 
    , from = (isftin||len(frto)==1) ? "in":frto[0]
	, to   = or( frto[1] , frto[0] ) 
	, units= str(from, ":", to) 
	)
    //isftin
    //[ from, to ]
	(from==to) ? m
	: isnum(utable)? m* utable
    : m * hash( utable
			  , units )
);

    uconv=["uconv", "n, units=''in:cm'', utable=UCONV_TABLE)", "number", "Unit",
    " Convert n's unit based on the *units*, which has default ''in:cm''. 
    ;; The conversion factor is stored in a constant UCONV_TABLE. To use
    ;; something not specified in UCONV_TABLE, do this:
    ;;
    ;;   uconv(n, ''u1:u2'', factor )
    ;;
    ;; Or you can define own table:
    ;;
    ;;   table=[''u1:u2'',factor]
    ;;   uconv( n, ''u1:u2'', table )
    ;;
    ;; n can be a string like \"2'8''\". But the return is always a number.
    ;;
    ;; Related:
    ;;
    ;; num(''3'6\"'')= 41  // in inches // str=> num
    ;; numstr( 3.5, conv=''ft:ftin'') = ''3'6\"'' // num=> str
    ;; ftin2in(''3'6\"'') = 42 
    "];

    function uconv_test( mode=MODE, opt=[] )=
    (
        doctest( uconv,
        [
         [ uconv(3,"cm:in"), 3/2.54 , "3,'cm:in'"]
        , [ uconv(100,"cm:ft"), 100/2.54/12, "100, 'cm:ft'"]
        , [ uconv(3.5, "ft:in"), 42, "3.5, 'ft:in'" ]
        , [ uconv(1, "in:cm"), 2.54, "1, 'in:cm'" ]
          ,"// Use own units:"
        , [ uconv(100,"cm:aaa"), undef, "100, 'cm:aaa'"]
        , [ uconv(100,"cm:aaa", ["cm:aaa",2]), 200, ["//","Use your factor"], "100, 'cm,aaa', ['cm,aaa',2]"]
          ,"// Simply set units= the conv factor: "
        , [ uconv(100,utable=3), 300, "100, utable=3", ["//","Enter a conv factor"]]
           
         ,"// When n is a string like 2'8', arg *from* is ignored:"
        , [ uconv("1'3\"", "in"), 15, "'1'3', 'in'"]
        , [ uconv("1'3\"", "cm"), 15*2.54, "'1'3', 'cm'"]
        , [ uconv("1'3\"", "ft"), 15/12, "'1'3', 'ft'"]
        , [ uconv("1'3\"", "aaa"), undef, "'1'3', 'aaa'"]
     
        ],mode=mode, opt=opt )
    );
    
/////////////////////////////////////////////////////////
//
//         TESTING
//    
/////////////////////////////////////////////////////////
//
//function get_scadx_num_test_results( mode=MODE, opt=["showmode",true] )=
//(
//    // Generated automatically using:
//    // >>> py get_openscad_funcs_for_doctest.py -fo scadx -fn scadx_num.scad
//    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c
//
//    // Usages:
//    //
//    // results = get_scadx_num_test_results(...);
//    //
//    // (1) for(r = results) echo( r[1] )
//    // (2) scadx_file_test( "scadx_num", results, showEach = false );
//    
//    [
//      fracstr_test( mode, opt )
//    , fullnumstr_test( mode, opt )
//    , getPowerOrder_test( mode, opt )
//    , getdeci_test( mode, opt )
//    //, index_test( mode, opt ) // to-be-coded
//    , int_test( mode, opt )
//    , intstr_test( mode, opt )
//    , num_test( mode, opt )
//    , numstr_test( mode, opt )
//    , uconv_test( mode, opt )
//    ]
//);
   

function get_scadx_num_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_num.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_num_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_num", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      fracstr_test( mode, opt )
    , fullnumstr_test( mode, opt )
    //, fullnumstr_old_test( mode, opt ) // to-be-coded
    , getPowerOrder_test( mode, opt )
    , getdeci_test( mode, opt )
    , int_test( mode, opt )
    , intstr_test( mode, opt )
    , num_test( mode, opt )
    , numstr_test( mode, opt )
    , uconv_test( mode, opt )
    ]
);
