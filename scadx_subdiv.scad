//include <scadx.scad>
/*
   2018.4.15
   
   Based on recent development in scadx_edgeface.scad and scadx_subdiv.scad,
   attempt to reconstruct scadx_subdiv for easier debugging. 
   
   The main idea is to seperate the funcs needed to generate 
    
    core_subfaces => rename to core_subfaces here
    tip_subfaces
    edge_subfaces
    
    so they can be individually called to verify the outcome.
   
*/

//. edges

function get_edges(faces, issort=0, uniq=0, _edges=[], _fi=0)=
(  // issort: sort the fis in an edge, [2,1]=[1,2]. default=0
   // uniq:   remove the duplicates
   !issort && !uniq?
     [ for(f=faces) each get2(f) ] 
   :_fi==0? 
      get_edges( faces, issort, uniq
               , issort? [for(eg=get2(faces[0]))quicksort(eg)] 
                       : get2(faces[0])
               , _fi=1)
   :_fi>= len(faces) ?
      _edges
   :
     let( edges = get2( faces[_fi] )
        , new=  [for(eg=edges) 
                 if( !has( _edges, eg) 
                   && !has( _edges, [eg[1],eg[0]] ) )
                   //eg
                   (issort?quicksort(eg):eg)
                ]
        , _edges = concat( _edges, new )       
        )
      get_edges(faces, issort, uniq, _edges, _fi=_fi+1)
);


//. subpts

function get_subFacePts_184p( fpts
                   , shrink_ratio=0.25
                   , borders// an arr of ei. For example, [0,1] means edges 0 and 1 are border
                   , border_shrink_ratio // 0 ~ 1   
                   , opt
                   )=
(
   /*   
        Shrink pts on an internal face (i.e., not on the border of a mesh)
        
        fpts: fpts of a SINGLE face  

        .---------.
        | .-----.  \
        | |      \  \
        | |       \  \
        | |        \  \
        | '--._ _-'  _- 
        '--._  `  _-'  
             `'-.' 
       P2               P3
        +----------------+ ---
        |                |  | ----> shrink_ratio
        |  S2-------S3   | ---
        |   |        |   |
        |  S1-------S0   | ---
        |                | | ----> shrink_ratio
        +---*--------*---+ ---      
       P1            ^   P0
                     |
                     +------------- node
                          
   --- Pts don't need to be co-planar  
 
### Issue of border faces  

      When a face is on the border, or edge, of a poly mesh: 
       
         |          |          |
         8----------9----------10--
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6--
         |  +====+  |  +====+  |
         |  |  0 |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2--

      Its faces =  
       [ [4,5,1,0], [5,6,2,1], [6,7,3,2], [8,9,5,4], [9,10,6,5],.... ]
         
      Its subfaces -- if obtained in the same way as that used in a 
      solid (as shown above) --- reduces the patch size. 
        
      We need to make subfaces like this:
          
         |          |          |
         |          |          |
         8----------9----------10-----
         +=======+  |  +====+  |
         |   3   |  |  | 4  |  |
         +=======+  |  +====+  |
         4----------5----------6-----
         +=======+  |  +====+  |
         |   0   |  |  | 1  |  |
         |       |  |  |    |  |
         0=======+--1--+====+--2-----
        
      That is, special treatment needed for the displayed 4 border edges:
       
         [2,1], [1,0], [0,4], [4,8]
         
       faces[4] shrinks as usual -- for each edge calc nodepairs(@)
       that are determined by the edges before and after it. Then we
       locate the intersections (+). 
        
           4---@----@---5
           |   :    :   |
           @...+====+...@ 
           |   |  0 |   |  
           @...+====+...@
           |   :    :   |
           0---@----@---1
        
         
       For the rest, for each edge, check if the edge[0] (first pi) of 
       it, AND those BEFORE and AFTER it, shows up in the border_pis list.
         
       For this purpose, we make a array Bs (means: "is border?" for each edge).
       B=0 for [4,5], B=1 for [1,0] and [0,4], etc.
               
         face  pis        Bs
         0    [4,5,1,0]   [0,0,1,1] 
         3    [8,9,5,4]   [0,0,0,1]
         4    [9,10,6,5]  [0,0,0,0]
         1    [5,6,2,1]   [0,0,1,0]
   
   ### Shrink a border face 
         
       We use faces[0] as example:
       
         shrinkBorderFacePts( fpts    // pts on the target face
                            , pis     // = the target face
                            , edgefi  // = get_edgefi( faces )
                            , ratio=0.15
                            )                   
                           
       edges: [4,5], [5,1], [1,0], [0,4]
         
       With Bs= [0,0,1,1], it means,
         
       Edges [4,5] and [5,1] are NOT on border;
       Edges [1,0] and [1,4] ARE on border;
                            
       We make a triplet B3:
         
         bi =   0, 1, 2, 3    // index for border 
         Bs = [ 0, 0, 1, 1 ] 	
           
         bi   B3= [ Bs[bi-1], Bs[bi], Bs[bi+1] ]  
         0   	[0,0,1]
         1   	[0,0,1]
         2   	[0,1,1]
         3  	[1,1,0]    
            
       B3s= get3m( Bs )= [ [0,0,1],[0,0,1],[0,1,1],[1,1,0] ]                  
                            
       We then loop through B3s to determine subPts of each face          
             
   //--------------------------------------------------------------
     border_shrink_ratio = undef: 
   
     
         |          |          |
         8----------9----------10-----
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6-----
         |  +====+  |  +====+  |
         |  | 0  |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2-----
   
     Setting it to a number will extend the side of subfaces to the border:
     
     border_shrink_ratio = 0.25 
          |  
          |    |          |          |
          |    8----------9----------10-----
          +--> |          |  +====+  |
               +=======+  |  |    |  |
               |   3   |  |  | 4  |  |
               +=======+  |  |    |  |
               |          |  +====+  |
               4----------5----------6-----
               +=======+  |  +====+  |
               |   0   |  |  | 1  |  |
               |       |  |  |    |  |
               0=======+--1--+====+--2-----
   
     border_shrink_ratio =0  --- this keeps the border
         unchanged for future merging with other meshes    
     
               |          |           |
               8.---------9-----------10-----
               | ``--.._  |   +====+  |
               |   3   |  |   | 4  |  |
               + __..--+  |   +====+  |
               4'---------5-----------6-----
               | ``--.._  |   +====+  |
               |       \  |  /  1  \  |
               |   0    \ | /       \ |  
               |         \|/         \|  
               0==========1===========2----         
       
          
   */ 
   let( center= sum(fpts)/len(fpts) 
      , _shrink_ratio= (shrink_ratio>0.49?0.49 ///- Make no sense to thrink >50% 
                       :shrink_ratio)*2        /// 'cos it'd become negative 
                                               ///- x2 'cos shrinking actually applied to
                                               ///  a line [pt,center] but not the edge 
      , borders = arg(borders, opt, "borders", []) 
      )
     //echo(shrink_ratio= shrink_ratio, _shrink_ratio= _shrink_ratio)
     //echo(borders=borders
     //    , _b_sh_ratio=_b_sh_ratio, b_sh_ratio=b_sh_ratio)
         
     borders && 
     border_shrink_ratio!=undef? /// If border_shrink_ratio undef, proceed as no border
      
       let( _b_sh_ratio= arg( border_shrink_ratio, opt
                            , "border_shrink_ratio", shrink_ratio)   
          , b_sh_ratio= _b_sh_ratio>0.25?0.25:_b_sh_ratio     
                        /// b_sh_ratio is limited to 0~0.25
                        /// 'cos >.25 would make the new face
                        /// border edge larger than border edge
                        /// of remaining face  
          , Bs = [ for(pi=range(fpts)) has(borders,pi)?1:0 ]  
                 /// [1,0,0,1 ...] indicating border
          , B3s= get3m(Bs) 
                 /// triplet of Bs = [ [?,1,0], [1,0,0], [0,0,1] ...]
          )  
         [ for(pi=range(fpts))
            let( prevpi = pi==0?(len(fpts)-1):(pi-1) )
            has(borders,pi)? /// If current edge ( [pi, pi+1] ) is border 
              (
                has(borders,prevpi)? /// If previous edge [pi-1,pi] is border, too
                 fpts[pi]  
                : onlinePt( get2(fpts,pi)
                          , ratio=b_sh_ratio ) 
              ) 
            : has(borders,prevpi)?  /// If previous is border (but not current one)
               onlinePt( get2(fpts,pi-1)
                       , ratio=1-b_sh_ratio )  
               
            : onlinePt( [fpts[pi], center]        /// No border. This is for the internal
                      , ratio=_shrink_ratio*1.25) /// -facing edges. Increase the ratio 
                                                  /// a little to prevent the border 
                                                  /// subfaces getting too large
         ]
     : [for(pt=fpts) onlinePt( [pt,center], ratio = _shrink_ratio )]
);
function get_subFacePts( fpts
                   , shrink_ratio=0.25
                   , border_eis=[] // an arr of ei for bordering edge. 
                                   // For example, [0,1] means edges 0 and 1 are border
                   , border_shrink_ratio=0 // 0 ~ 1   
                   , opt
                   )=
(
   /*   
      184q: we reverse the subdiv approach from get_subFacePts_184p
            to DooSabin_face_shrink
      
      //-----------------------------------------
        Shrink pts on an internal face (i.e., not on the border of a mesh)
        
        fpts: fpts of a SINGLE face  

        .---------.
        | .-----.  \
        | |      \  \
        | |       \  \
        | |        \  \
        | '--._ _-'  _- 
        '--._  `  _-'  
             `'-.' 
       P2               P3
        +----------------+ ---
        |                |  | ----> shrink_ratio
        |  S2-------S3   | ---
        |   |        |   |
        |  S1-------S0   | ---
        |                | | ----> shrink_ratio
        +---*--------*---+ ---      
       P1            ^   P0
                     |
                     +------------- node
                          
   --- Pts don't need to be co-planar  
 
### Issue of border faces  

      When a face is on the border, or edge, of a poly mesh: 
       
         |          |          |
         8----------9----------10--
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6--
         |  +====+  |  +====+  |
         |  |  0 |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2--

      Its faces =  
       [ [4,5,1,0], [5,6,2,1], [6,7,3,2], [8,9,5,4], [9,10,6,5],.... ]
         
      Its subfaces -- if obtained in the same way as that used in a 
      solid (as shown above) --- reduces the patch size. 
        
      We need to make subfaces like this:
          
         |          |          |
         |          |          |
         8----------9----------10-----
         +=======+  |  +====+  |
         |   3   |  |  | 4  |  |
         +=======+  |  +====+  |
         4----------5----------6-----
         +=======+  |  +====+  |
         |   0   |  |  | 1  |  |
         |       |  |  |    |  |
         0=======+--1--+====+--2-----
        
      That is, special treatment needed for the displayed 4 border edges:
       
         [2,1], [1,0], [0,4], [4,8]
         
       faces[4] shrinks as usual -- for each edge calc nodepairs(@)
       that are determined by the edges before and after it. Then we
       locate the intersections (+). 
        
           4---@----@---5
           |   :    :   |
           @...+====+...@ 
           |   |  0 |   |  
           @...+====+...@
           |   :    :   |
           0---@----@---1
        
         
       For the rest, for each edge, check if the edge[0] (first pi) of 
       it, AND those BEFORE and AFTER it, shows up in the border_pis list.
         
       For this purpose, we make a array Bs (means: "is border?" for each edge).
       B=0 for [4,5], B=1 for [1,0] and [0,4], etc.
               
         face  pis        Bs
         0    [4,5,1,0]   [0,0,1,1] 
         3    [8,9,5,4]   [0,0,0,1]
         4    [9,10,6,5]  [0,0,0,0]
         1    [5,6,2,1]   [0,0,1,0]
   
   ### Shrink a border face 
         
       We use faces[0] as example:
       
         shrinkBorderFacePts( fpts    // pts on the target face
                            , pis     // = the target face
                            , edgefi  // = get_edgefi( faces )
                            , ratio=0.15
                            )                   
                           
       edges: [4,5], [5,1], [1,0], [0,4]
         
       With Bs= [0,0,1,1], it means,
         
       Edges [4,5] and [5,1] are NOT on border;
       Edges [1,0] and [1,4] ARE on border;
                            
       We make a triplet B3:
         
         bi =   0, 1, 2, 3    // index for border 
         Bs = [ 0, 0, 1, 1 ] 	
           
         bi   B3= [ Bs[bi-1], Bs[bi], Bs[bi+1] ]  
         0   	[0,0,1]
         1   	[0,0,1]
         2   	[0,1,1]
         3  	[1,1,0]    
            
       B3s= get3m( Bs )= [ [0,0,1],[0,0,1],[0,1,1],[1,1,0] ]                  
                            
       We then loop through B3s to determine subPts of each face          
             
   //--------------------------------------------------------------
     border_shrink_ratio = undef: 
   
     
         |          |          |
         8----------9----------10-----
         |  +====+  |  +====+  |
         |  |  3 |  |  | 4  |  |
         |  +====+  |  +====+  |
         4----------5----------6-----
         |  +====+  |  +====+  |
         |  | 0  |  |  | 1  |  |
         |  +====+  |  +====+  |
         0----------1----------2-----
   
     Setting it to a number will extend the side of subfaces to the border:
     
     border_shrink_ratio = 0.25 
          |  
          |    |          |          |
          |    8----------9----------10-----
          +--> |          |  +====+  |
               +=======+  |  |    |  |
               |   3   |  |  | 4  |  |
               +=======+  |  |    |  |
               |          |  +====+  |
               4----------5----------6-----
               +=======+  |  +====+  |
               |   0   |  |  | 1  |  |
               |       |  |  |    |  |
               0=======+--1--+====+--2-----
   
     border_shrink_ratio =0  --- this keeps the border
         unchanged for future merging with other meshes    
     
               |          |           |
               8.---------9-----------10-----
               | ``--.._  |   +====+  |
               |   3   |  |   | 4  |  |
               + __..--+  |   +====+  |
               4'---------5-----------6-----
               | ``--.._  |   +====+  |
               |       \  |  /  1  \  |
               |   0    \ | /       \ |  
               |         \|/         \|  
               0==========1===========2----         
       
          
   */ 
      //echo(_green("get_subFacePts()"))          
      let( rng=[0:len(fpts)-1]
      , p2 = get2(fpts)
      , nodepairs=[ for(i=rng) 
                    //echo(i=i, has(border_eis, i), border_shrink_ratio=border_shrink_ratio) 
                    onlinePts( p2[i]
                              , ratios= has(border_eis, i)?
                                        [border_shrink_ratio, 1-border_shrink_ratio]
                                       :[shrink_ratio, 1-shrink_ratio]
                              )
                  ]
      )
      ! border_eis?
        [ for(i=rng)
          midPt( lineBridge( [nodepairs[i][0], get(nodepairs,i-2)[1]]
                          , [get(nodepairs,i-1)[1], get(nodepairs,i+1)[0]]
                          )
              ) 
        ]
      : let(Bs= [for(i=rng) has(border_eis,i)?1:0]
           ,B3s = get3m(Bs)
           )
           /*        
                         |          |          |
                         8----------9----------10-----
                         +=======+  |  +====+  |
            for[4,8]:    |   3   |  |  | 4  |  |
                [0,1,0]  +=======+  |  +====+  |
                       \ 4----------5----------6-- [0,0,x]   5: [0,0,1]
            for[4,5]:  / +=======+  |  +====+  |
                [1,0,x]  |   0   |  |  | 1  |  |
                         |       |  |  |    |  |
                         0=======+--1--+====+--2-----
                        /          /          /
                    [1,1,x]    [ 0,1,1]    [ 0,1,0]]
               
         */
        // echo( Bs=Bs )
        // echo(B3s = B3s)
        let(rtn=[ for(ei=rng) 
                   B3s[ei]==[0,0,0] // Edges prior and after cur.edge are NOT a border
                                   // Like pts[9] (faces[2])
                                   //    , pts[10] (faces[2]) above
                   || B3s[ei]==[0,0,1]? // The edge next to cur.edge is border         
                                        // like pts[5] (faces[0])
                                        //      pts[6] (faces[1])
                                        // Same as [0,0,0]                      
                                     
                   midPt( lineBridge( [nodepairs[ei][0], get(nodepairs,ei-2)[1]]
                       , [get(nodepairs,ei-1)[1], get(nodepairs,ei+1)[0]]
                       ))
                 
                :B3s[ei]==[0,1,1]    // The cur.edge itself is a border 
                 ||B3s[ei]==[0,1,0]? // But prior edge is not.
                                     // Like pts[1], pts[2]
                                         
                   nodepairs[ei][0]
                   
                : B3s[ei]==[1,0,0] ? // || B3s[ei]==[1,1,0] ?
                  
                   get(nodepairs,ei-1)[1]
                   
                //:B3s[ei]==[1,1,0]
                // || B3s[ei]==[1,1,1]? 
                 
                : fpts[ei] 
                ]
           )
        rtn          
                 
//      let( rng=[0:len(fpts)-1]
//      , p2 = get2(fpts)
//      , nodepairs=[ for(i=rng) 
//                    :onlinePts( p2[i]
//                              , ratios= has(border_eis, i)?
//                                        [border_shrink_ratio, 1-border_shrink_ratio]
//                                       :[shrink_ratio, 1-shrink_ratio]
//                              )
//                  ]
//      )
//      [ for(i=rng)
//         midPt( lineBridge( [nodepairs[i][0], get(nodepairs,i-2)[1]]
//                          , [get(nodepairs,i-1)[1], get(nodepairs,i+1)[0]]
//                          )
//              ) 
//      ]
   
//------------------------------------------------   
//   let( center= sum(fpts)/len(fpts) 
//      , _shrink_ratio= (shrink_ratio>0.49?0.49 ///- Make no sense to thrink >50% 
//                       :shrink_ratio)*2        /// 'cos it'd become negative 
//                                               ///- x2 'cos shrinking actually applied to
//                                               ///  a line [pt,center] but not the edge 
//      , borders = arg(borders, opt, "borders", []) 
//      )
//     //echo(shrink_ratio= shrink_ratio, _shrink_ratio= _shrink_ratio)
//     //echo(borders=borders
//     //    , _b_sh_ratio=_b_sh_ratio, b_sh_ratio=b_sh_ratio)
//         
//     borders && 
//     border_shrink_ratio!=undef? /// If border_shrink_ratio undef, proceed as no border
//      
//       let( _b_sh_ratio= arg( border_shrink_ratio, opt
//                            , "border_shrink_ratio", shrink_ratio)   
//          , b_sh_ratio= _b_sh_ratio>0.25?0.25:_b_sh_ratio     
//                        /// b_sh_ratio is limited to 0~0.25
//                        /// 'cos >.25 would make the new face
//                        /// border edge larger than border edge
//                        /// of remaining face  
//          , Bs = [ for(pi=range(fpts)) has(borders,pi)?1:0 ]  
//                 /// [1,0,0,1 ...] indicating border
//          , B3s= get3m(Bs) 
//                 /// triplet of Bs = [ [?,1,0], [1,0,0], [0,0,1] ...]
//          )  
//         [ for(pi=range(fpts))
//            let( prevpi = pi==0?(len(fpts)-1):(pi-1) )
//            has(borders,pi)? /// If current edge ( [pi, pi+1] ) is border 
//              (
//                has(borders,prevpi)? /// If previous edge [pi-1,pi] is border, too
//                 fpts[pi]  
//                : onlinePt( get2(fpts,pi)
//                          , ratio=b_sh_ratio ) 
//              ) 
//            : has(borders,prevpi)?  /// If previous is border (but not current one)
//               onlinePt( get2(fpts,pi-1)
//                       , ratio=1-b_sh_ratio )  
//               
//            : onlinePt( [fpts[pi], center]        /// No border. This is for the internal
//                      , ratio=_shrink_ratio*1.25) /// -facing edges. Increase the ratio 
//                                                  /// a little to prevent the border 
//                                                  /// subfaces getting too large
//         ]
//     : [for(pt=fpts) onlinePt( [pt,center], ratio = _shrink_ratio )]
);


function get_subPts(pts, faces, shrink_ratio=0.25)=
(
  //echo("--- get_subPts --- ")
  
//  [ for(face=faces) 
//    each get_subFacePts( fpts = get(pts, face)
//                       , shrink_ratio
//                       )
//  ]
  [ for(face=faces) 
    //let(fpts = get(pts, face))
    //echo(face = face)
    //echo(fpts=fpts) 
    each
    get_subFacePts( fpts = get(pts, face)
                  , shrink_ratio = shrink_ratio
                  )
  ]
);

//. subfaces

function get_core_subfaces(faces,_rtn=[],_L=0, _i=0)=
(  
   // rodfaces(3):
   // [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
   // Re-align the indices to:  
   // [[0,1,2], [3,4,5], [6,7,8,9], [10,11,12,13], [14,15,16,17]]
   _i>=len(faces)?_rtn
   : get_core_subfaces( faces=faces
                       , _rtn= concat(_rtn, [range(_L, _L+len(faces[_i]))] )
                       , _L=_L+len(faces[_i])
                       , _i=_i+1
                       )
   
);

    get_core_subfaces=[ "get_core_subfaces", "faces", "array", "Face",
     "Re-arrange the first item of faces so its first index is 0
      For example, 
      
      faces= [[2, 1, 0], [3, 4, 5], ... ]
      get_core_subfaces(faces)
           = [[0, 2, 1], [3, 4, 5], ... ]
     "
    ,""
    ];

    function get_core_subfaces_test( mode=MODE, opt=[] )=
    (   
        let( faces= rodfaces(3)
           ) 
        doctest( get_core_subfaces,
        [ 
          "var faces" 
        , [ get_core_subfaces( faces)
          , [ [0, 1, 2], [3, 4, 5]
            , [6, 7, 8, 9], [10, 11, 12, 13], [14, 15, 16, 17]]
          , "faces", ["nl",true] 
          ]
          
        , str("rodfaces(4)=")
        , str("&nbsp;&nbsp;&nbsp;", rodfaces(4))  
        , [ get_core_subfaces( rodfaces(4))
          , [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11]
            , [12, 13, 14, 15], [16, 17, 18, 19], [20, 21, 22, 23]]
          
          , "rodfaces(4)", ["nl",true] 
          ]
        ]
        ,mode=mode, opt=opt, scope=["faces",faces]
        )
    );


function get_tip_subfaces( aclockedEFs )=
(
  [ for( cf= aclockedEFs)
     [ for( h=vals( cf ) ) 
       hash(h, "subface")[0]
     ]      
  ]
);

function get_edge_subfaces( aclockedEFs, edges, faces )=
(
  /// edges can be given, or calc if faces is given 
  
  //echo("-------------- get_edge_subfaces()")
  let( edges = edges==undef? get_edges(faces): edges )
  [ for( pi = range(aclockedEFs))
     each
     [ for( kv=hashkvs( aclockedEFs[pi] ) ) 
        if( has(edges, [pi, kv[0]]) )
         hash(kv[1], "subface") 
     ]   
  ]
);

function get_subfaces( aclockedEFs, faces )=
(
  concat( get_core_subfaces( faces )
        , get_tip_subfaces( aclockedEFs )
        , get_edge_subfaces( aclockedEFs, faces=faces )
        )
);


//. clocked-EdgeFaces

function get_clocked_EFs_at_pi(  
         faces, pi, getSubface=false
        , _nPts
        , _near_fis
        , _core_subfaces // needs this to calc subface of each edge
        , _next_edge_end
        , _rtn
        , _i=0
        )=
(
   // get_clocked_EFs_at_pi( CUBEFACES, 4) returns: 
   //   [5, ["L", 2, "R", 1], 7, ["L", 1, "R", 5], 0, ["L", 5, "R", 2]]
   
   // Check get_clocked_EFs() in dev_subdiv.scad for implantation.
   
   // A clocked_EFs of a pt @ pi is defined as a hash of {edge_end:hFis}
   // like:
   //
   //  [ 3,["L",5, "R",0, "subface",[21,20,0,3]]
   //  , 1,["L",0, "R",2, "subface",[3,2,9,8]]
   //  , 4,["L",2, "R",5, "subface",[8,11,22,21]]
   //  ]   
   //
   // If getSubface = false (default):
   //
   //  [ 3,["L",5, "R",0]
   //  , 1,["L",0, "R",2]
   //  , 4,["L",2, "R",5]
   //  ]   
   //
   
   // where the keys, 3,1,4 means: 
   // -- there are 3 edges connected to pi: [pi,3], [pi,1], [pi,4]
   // -- thses 3 edges are arranged in a clock-wize order
   //
   // The value is another hash containing the face-indices of rise and left faces,
   // and the subface of the corresponding edge. 
   /*
   
   To get CEFs for all pts :
   
     clockedEFs = [ for(pi=range(pts)) 
                     get_clocked_EFs_at_pi(faces, pi) 
                   ];
                 
   To get subfaces ( = core_subfaces + tip_subfaces + edge_subfaces )
   
     core_subfaces = get_core_subfaces(faces);
     
     tip_subfaces = [ for( cf= aclockedEFs)
                      [ for( h=vals( cf ) ) 
                         hash(h, "subface")[0]
                      ]   
                    ];
     
     edges = get_edges( faces );
     edge_subfaces = [ for( pi = range(aclockedEFs))
                       each
                       [ for( kv=hashkvs( aclockedEFs[pi] ) ) 
                          if( has(edges, [pi, kv[0]]) )
                           hash(kv[1], "subface") 
                       ]   
                     ];
   
     subfaces = concat( core_subfaces, tip_subfaces, edge_subfaces );
   
   To draw subobj:
   
     subPts = [ for(face=faces) 
                each get_subFacePts( fpts = get(pts, face)
                                   , shrink_ratio = 0.25
                                   )
              ];
              
     polyhedron( points = subPts, faces = subfaces );   
   
   */            
               
               
   //------------------------------------------------------------            
   //echo( _blue("########## get_clocked_EFs_at_pi() ##########"), pi=pi, _i = _i )
   _i==0?
   /*
    We start with the first fi in the unclocked _near_fis: faces[0]
    
    	faces[0] = [0,3,2,1]
    	
    We get:
     
     	edge_end (for edge [0,3]) = 3
    	Rfi = 0
    	Lfi = need to search for this from _near_fis
    	
    	
    from _near_fis, search face:
         
        prev( faces[fi], pi) == edge_end = 3
    
    is the Lfi 
    
        Lfi = 5
        Lface = faces[ Lfi ] = [0,1,5,4]
    
    We then use Lfi (5) and Rfi (0) to search for subface. 
    
    So we get a cf [ edge_end, ["L", Lfi, "R", Rfi, "subface", [...] ]
                   
        cf = [3, ["L",5, "R",0, "subface",[...] ]
               
    We also set next_edge_end: 
    
       next_edge_end = prev( faces[Rfi], pi ) =1
   
   */ 
   //echo(faces = faces)
   //echo("")
   //echo(pi=pi, _red("</br>_i=-1, setting up"))
   
   let( _nPts= _nPts==undef? (max( [for(f=faces)each f])+1): _nPts
      , _core_subfaces= get_core_subfaces(faces)
      , _near_fis = [ for(fi= range(faces) )          // unclocked fis around
                     if( has(faces[fi], pi) ) 
                      fi
                    ]
      
      // For _i=0 only: 
      , Rfi = _near_fis[0]
      , edge_end = next( faces[Rfi], pi) // index of pts next to pi
                                         // = tail pi of the edge 
      , _next_edge_end = prev( faces[Rfi], pi )              
      , Lfi = [ for( i= [0:len(_near_fis)-1] )
                 let( fi = _near_fis[i] ) 
                 if( prev( faces[fi], pi) == edge_end ) 
                 fi
              ][0]
//      , _rtn= [ edge_end 
//              , [ "L", Lfi
//                , "R", Rfi
//                , "subface"
//                , [ _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
//                  , _core_subfaces[Lfi][ idx( faces[Lfi], edge_end) ]
//                  , _core_subfaces[Rfi][ idx( faces[Rfi], edge_end) ]
//                  , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
//                  ]   
//                ] 
//             ]
      , _rtn= [ edge_end 
              , getSubface?
                [ "L", Lfi
                , "R", Rfi
                , "subface"
                , [ _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                  , _core_subfaces[Lfi][ idx( faces[Lfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                  ]   
                ]
               :[ "L", Lfi
                , "R", Rfi
                ]  
             ]
      )
  
   //echo( str(_s("_near_fis of pts[{_}] = {_}", [pi, _near_fis])
         //, _green("// unclocked nearby fis"))
       //)
   //echo( str(_s("Start with the 1st face, fi=_near_fis[0] = {_}", Rfi)
         //, _green("// This fi = Rfi --- index of the right-side face" )
         //)
       //)
   //echo( edge_end= edge_end 
         //, _green(_s("// For edge [{_},{_}]", [pi, edge_end]))
       //)  
   //echo(pi=pi, _near_fis=_near_fis)
   //echo(edge_end = edge_end)
   //echo( [ for( i= [0:len(_near_fis)-1] )
           //let( fi = _near_fis[i] ) 
           //[ "fi",fi, "pis of fi", faces[fi], "prev pi", prev( faces[fi], pi), "edge_end", edge_end] 
         //])  
   //echo( "Search to find the Lfi (prev pi = edge_end): ", Lfi=Lfi) 
   //echo( _next_edge_end = _next_edge_end )     
   //echo(_rtn=_rtn)
   //echo( _b("Done setup, go next to _i=0"))
   get_clocked_EFs_at_pi( faces, pi, getSubface
                      , _nPts
                      , _near_fis
                      , _core_subfaces
                      , _next_edge_end
                      , _rtn=_rtn
                      , _i=_i+1
                      )   
  
  //=====================================================
  
  : _i> len(_near_fis)-1? 
  //:_i>0?
      //echo(_blue(_s("####################  END of pi=[{_}], preparing final _rtn", pi )))
      let( 
           final_for_this_pi = len(_rtn)%2==1? concat(_rtn,[undef]): _rtn
               
         , clocked_faces = [ for(v=vals(final_for_this_pi))
                             hash(v,"R")
                           ] 
         )
      //echo(clocked_faces=clocked_faces)   
      //echo(final_for_this_pi = final_for_this_pi)
      //echo("")
                      
      final_for_this_pi
    
  //=====================================================
    
  : 
  
    //echo( _s("near_fis of pts[{_}][{_}] = {_}", [pi, _i, _near_fis[1]]))
    //echo( previous_rtn = _rtn ) ///=>  like: [3, ["L",5, "R",0, "surface",[21,20,0,3]]]
    //echo( previous_next_edge_end= _next_edge_end
        //, _green(_s("// This will be the edge_end of current edge [{_},{_}]"
                   //, [pi,_next_edge_end] ))
        //)
    //echo( "Setting _rtn" )
    /*
                4
               .+_
             .' | '--_               
           .'   |     '-_ 7  
       5 .' [2] |  [5]  |
         |    .'0-_     |
         |  .'     '--_ |
         |.'   [0]     '+ 3
      1  ._           .'
           '--_     .'  
               --_-' 
                 2    
     
    faces = [[3, 2, 1, 0], [4, 5, 6, 7], [0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]]             
    _near_fis = [0,2,5] (unclocked)
    
    If we flatten it out, and plot the subface in each:
        
       6----------7
       |  6====7  | 
       |  |  1 |  | 
       |  5====4  | 
       5----------4----------7----------6----------5
       | 10====11 | 22====23 | 18====19 | 14====15 |
       |  |  2 |  |  | 5  |  |  | 4  |  |  | 3  |  |
       |  9====8  | 21====20 | 17====16 | 13====12 |
       1----------0----------3----------2----------1
       |  2====3  | 
       |  |  0 |  | 
       |  1====0  | 
       2----------3
                  
    This gives clock-faces at 0 = 
    
      [ 3,["L",5, "R",0, "subface",[21,20,0,3]]
      , 1,["L",0, "R",2, "subface",[3,2,9,8]]
      , 4,["L",2, "R",5, "subface",[8,11,22,21]]
      ]
      
    I. 
    
    We start with the first fi in the unclocked _near_fis: faces[0]
    
    	faces[0] = [0,3,2,1]
    	
    We get:
     
     	edge_end (for edge [0,3]) = 3
    	Rfi = 0
    	Lfi = need to search for this from _near_fis
    	
    	
    from _near_fis, search face:
         
        prev( faces[fi], pi) == edge_end = 3
    
    is the Lfi 
    
        Lfi = 5
        Lface = faces[ Lfi ] = [0,1,5,4]
    
    We then use Lfi (5) and Rfi (0) to search for subface. 
    
    So we get a cf [ edge_end, ["L", Lfi, "R", Rfi, "subface", [...] ]
                   
        cf = [3, ["L",5, "R",0, "subface",[...] ]
               
    We also set next_edge_end: 
    
       next_edge_end = prev( faces[Rfi], pi ) =1
                  
    II.
    
    After the faces[0], we add next edge_end/faces 
        
        
       prev_cf = [3, ["L",5, "R",0, "subface",[...] ] 
       Lfi = prev_Rfi = hash( prev_cf[1], "R")
       edge_end = prev next_edge_end = 1
       Rfi = search:
       
             next(faces[fi],pi)== edge_end 
             
    Note in I. we search for Lfi, thereafter we search for Rfi            
        
    */
    
    let( edge_end = _next_edge_end
       , Lfi = hash( last(_rtn), "R") 
       , Rfi = [ for( i= [1:len(_near_fis)-1] )
                 let( fj = _near_fis[i] ) 
                 if (next(faces[fj],pi)== edge_end)  fj
               ][0]
       , _next_edge_end = prev( faces[Rfi], pi )              
                 
       , cf = [ edge_end 
              , getSubface?
                [ "L", Lfi
                , "R", Rfi
                , "subface"
                , [ _core_subfaces[Lfi][ idx( faces[Lfi], pi) ]
                  , _core_subfaces[Lfi][ idx( faces[Lfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], edge_end) ]
                  , _core_subfaces[Rfi][ idx( faces[Rfi], pi) ]
                  ]   
                ]
                :[ "L", Lfi
                 , "R", Rfi
                 ] 
             ]       
       )   
   //echo(_green(_s("Checking the Lfi and Rfi of edge [{_},{_}]",[pi,edge_end]))) 
   //echo( "... ",edge_end = edge_end )
   //echo( "... ",Lfi = Lfi, _green("// Lfi = the Rfi of previous _i") )
   //echo( "... ",Rfi = Rfi, _green("// Rfi is searched") )
   //echo( "... ",cf = cf )    
   //echo( "... ",_rtn= concat(_rtn,cf) ) 
   get_clocked_EFs_at_pi( faces, pi, getSubface
                      , _nPts
                      , _near_fis
                      , _core_subfaces
                      , _next_edge_end
                      , _rtn = concat(_rtn, cf)
                      , _i=_i+1
                      )
);
module get_clocked_EFs_at_pi_test()
{
  echom("get_clocked_EFs_at_pi_test()");
  echo(CUBEFACES=CUBEFACES);
  echo(rodfaces4 = rodfaces(4));
  faces = rodfaces(4);
  cpts = cubePts([3,2,1]);
  Teal() MarkPts(cpts, Line=0);
  DrawEdges([cpts,faces]);
  MarkFaces(cpts, faces);
  Gold(0.5) cube([3,2,1]);
  for(i=range(8))
  { d =get_clocked_EFs_at_pi( faces, i);
    echo(i=i, d=d);
  }  
}
//get_clocked_EFs_at_pi_test();

function get_clocked_EFs( pts, faces, getSubface=false )=
(
  [ for(pi=range(pts)) 
    get_clocked_EFs_at_pi(faces, pi, getSubface) 
  ]
);

//. Smooth obj

module Smooth_obj( pts,faces, cycle=1
                 , original_frame=[]
                 , is_label = false
                 , is_echo = true  
                 , shrink_ratio = 0.25
                 , _i=0)
{
   echom("Smooth_obj()");
   
   //echo(cycle=cycle, original_frame=original_frame, _i=_i);
   if( _i==0 && original_frame )
   { 
     //echo("drawing frame");
     for( f = faces ) Line( get(pts,f), closed=true, opt=original_frame );
   }

   subPts = get_subPts( pts= pts, faces=faces, shrink_ratio=shrink_ratio );
   clockedEFs= get_clocked_EFs( pts, faces, getSubface=true ); ///<======
   subfaces= get_subfaces( clockedEFs, faces);
   
   if(cycle>_i+1)
   {
     Smooth_obj(pts=subPts
               , faces=subfaces
               , shrink_ratio=shrink_ratio 
               , cycle=cycle
               , is_label = is_label
               , is_echo=is_echo
               ,_i=_i+1);
   }
   else
   {
 
    if(is_echo)
    {
      //echo( cycle=cycle );
      //echo( len_subPts = len(subPts), len_subfaces = len(subfaces) );
      echo(_s("<b>Smooth_obj()</b> w/ shrink_ratio <b>{_}</b>: Got <b>{_}</b> pts and <b>{_}</b> faces after <b>{_}</b> cycles"
             , [shrink_ratio, len(subPts), len(subfaces), cycle ]
             )
          );   
      if(is_echo==2)
      { echo("");
        echo( subPts = subPts );
        echo("");
        echo( subfaces = subfaces );
      }  
    }
    
    
    
    if(is_label)
    {
          
       for( fi = range(subfaces) ) 
        {
          f = subfaces[fi];
          MarkPt( sum( get(subPts, f) ) / len(f)
                , Ball=false
                , Label=["text",fi, "indent",0, "color","black", "scale",0.5] 
                ); 
          if( fi==32 || fi == 33 )      
            Line( get(subPts,f), closed=false, color="red" );
        }
        
               
       core_subfaces = get_core_subfaces( faces );
       tip_subfaces = get_tip_subfaces( clockedEFs );
       edge_subfaces = get_edge_subfaces( clockedEFs, faces=faces );
       
       echo( core_subfaces = core_subfaces );   
       echo( tip_subfaces = tip_subfaces );   
       echo( edge_subfaces = edge_subfaces );          
       echo( subfaces = subfaces );
       
       for( f = core_subfaces )
       { Plane( get(subPts,f), color=["gold",0.5] );}   
       for( f = tip_subfaces )
       { Plane( get(subPts,f), color=["green",0.5] );}   
       for( f = edge_subfaces )
       { Plane( get(subPts,f),  color=["blue",0.5] );}
       //color("gold", 0.7)
       //polyhedron( subPts, subfaces );
       
    }
    else
      polyhedron( subPts, subfaces );
   }
   //MarkPts(pts, Line=["closed",true]);
}
/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_subdiv_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_subdiv.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_subdiv_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_subdiv", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      //get_clocked_EFs_test( mode, opt ) // to-be-coded
    //, get_clocked_EFs_at_pi_test( mode, opt ) // to-be-coded
     get_core_subfaces_test( mode, opt )
    //, get_edge_subfaces_test( mode, opt ) // to-be-coded
    //, get_edges_test( mode, opt ) // to-be-coded
    //, get_subFacePts_test( mode, opt ) // to-be-coded
    //, get_subFacePts_184p_test( mode, opt ) // to-be-coded
    //, get_subPts_test( mode, opt ) // to-be-coded
    //, get_subfaces_test( mode, opt ) // to-be-coded
    //, get_tip_subfaces_test( mode, opt ) // to-be-coded
    ]
);

