//include <scadx_core_dev.scad>
include <scadx_obj_basic_dev.scad>

//echo( calc("3*x+1",["x",10]));

//function reduce( formula, arr, _rtn=0, _i=0 )=
//(
//   _i> len(arr)-1? _rtn
//   : reduce( formula, arr
//           , _rtn= _rtn+calc(formula, ["a", arr[_i], "b", arr[_i+1]])
//           , _i=_i+1 
//           )
//);

//function sum(arr)= reduce("a+b", arr);

// echo( sum( range(5)));

module Column( pqr= randPts(3), opt=[] ){
    echom("Column");
    echo(pqr = pqr );
    //  [[-2.88382, -1.68051, 2.74215], [1.73411, 0.977815, 2.77914], [-1.89791, -0.619723, 1.91149]]
     opt = update(
           [ "formula", 2
           //, "scope", ["r",2,"i",0]
           , "npt", undef
           , "r",2
           , "len", 5
           , "color", undef
           ]
           , opt
           );
    fml = hash( opt, "fml" );
    scope = hash( opt, "scope" );
    r = hash( opt, "r" );
    len= hash( opt, "len" );
    npt = or(hash( opt, "npt"), len([ for(x=scope) if(isarr(x))x][0]) );
   
    echo( _fmth( opt));
    
    PP = onlinePt( p01(pqr), len=-1);
    
    //MarkPts( app( pqr, PP), ["labels", ["P","Q","R", "PP"]] );
    Chain( pqr );
    MarkPts( pqr );
       
//    formulaPts = [ for(i=range(npt)) 
//                      r+calc_func( formula, ["a",i*(360/npt), "i",i] )
//               
//    //                   r +calc_flat( formula, ["a",360/npt*i, "i",i] )
//                ];

    formulaPts = calc_func( hash( opt, "fml")
                          , hash( opt, "scope")
                          );
    echo( npt = npt);
//    echo( as= [ for(i=range(npt)) 
//                      [i*(360/npt), calc_func("sin(x)", ["x", i*(360/npt)])]]);
    
    echo( formulaPts= formulaPts );
    
    ptsP = [ for(i=range(npt))
              anglePt( [PP,P(pqr),R(pqr)], a=90, a2= 360/npt*i
                     , len= formulaPts[i] )
           ];
    
    //MarkPts( ptsP,["samesize",true] );
    
//    for( i= [0:2:len(ptsP)-2] ) MarkPts( [ptsP[i]], ["colors",["purple"]]);
    
    Chainc( [for( i= [0:2:len(ptsP)-2] ) ptsP[i] ],["color","green"] );
    
    ptsQ = [ for(i=range(npt))
              anglePt( pqr, a=90, a2= 360/npt*i
                     , len= formulaPts[i] )
           ];
    
    pts = concat( ptsP, ptsQ );
    
    faces = faces( "rod", sides= npt );
    
    color( hash(opt,"color"), 0.6)
    polyhedron( points = pts, faces=faces );

}


//Column( opt=["fml","3+sin(a*12)", "scope", ["a", range(0, 10,360)]] );

//Column( opt=["fml","3+sin(a*12)/3", "scope", ["a", range(0, 7.5,360)]] );

Column( opt=["fml","r+sin(i*90)/3"
      , "scope", ["r", 1, "i", range(48)]] );

//Column( opt=["fml","3+sin(i*90)/3", "scope", ["i", range(0, 96)]] );
//Column(opt=["fml","3+sin(i*45)/3","scope",["i",range(0,96)]] ); // 1'54", 12 spikes
//Column(opt=["fml","3+sin(i*30)/3","scope",["i",range(0,144)]] );

//pts = calc_func( "sin(x)", ["x", range(0,15,375) ] );
//echo( pts = pts );
//Chain( [ for(i=range(pts)) [ i/10,0,pts[i] ] ] );

module Cone( pqr= randPts(3), opt=[] ){
    echom("Cone");
     opt = update(
           [ "fml", undef
           //, "scope", ["r",2,"i",0]
           , "npt", undef
           , "r1",1
           , "r2",2
           , "len", 5
           , "color", undef
           ]
           , opt
           );
    fml = hash( opt, "fml" );
    scope = hash( opt, "scope" );
    r = hash( opt, "r" );
    len= hash( opt, "len" );
    npt = or(hash( opt, "npt"), len([ for(x=scope) if(isarr(x))x][0]) );
   
    echo( _fmth( opt));
    
    PP = onlinePt( p01(pqr), len=-1);
    
    Chain( pqr );
    MarkPts( pqr );
       
    formulaPts = calc_func( hash( opt, "fml")
                          , hash( opt, "scope")
                          );
    echo( npt = npt);   
    echo( formulaPts= formulaPts );
    
    ptsP = [ for(i=range(npt))
              anglePt( [PP,P(pqr),R(pqr)], a=90, a2= 360/npt*i
                     , len= formulaPts[i] )
           ];
    
    MarkPts( ptsP );
    
    ptsQ = [ for(i=range(npt))
              anglePt( pqr, a=90, a2= 360/npt*i
                     , len= formulaPts[i] )
           ];
    
    pts = concat( ptsP, ptsQ );
    
    faces = faces( "rod", sides= npt );
    
    color( hash(opt,"color"), 0.8)
    polyhedron( points = pts, faces=faces );

}



