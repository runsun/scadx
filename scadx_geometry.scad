
//========================================================
//. a
{ // addx
function addx(pt,x=0,keep=false)=
(
	let( ispt = !isarr(pt[0])
     , rtn = !ispt // pt is a pts (list of points)
              ? [for(i=range(pt)) addx(pt[i], isarr(x)?x[i]:x) ]
              : len(pt)==3
                 ? [ pt.x+x,pt.y,pt.z ] 
                 : [ pt.x+x,pt.y]  
     )
  keep? (ispt? [pt,rtn]: concat(pt,rtn))
      : rtn

//	let( rtn = isarr(pt[0]) // pt is a pts (list of points)
//              ? [for(i=range(pt)) addx(pt[i], isarr(x)?x[i]:x) ]
//              : len(pt)==3
//                   ? [ pt.x+x,pt.y,pt.z ] 
//                 : [ pt.x+x,pt.y]  
//     )
//  keep? concat( [pt],[rtn] ): rtn            
);

    addx=[ "addx", "pt,x=0", "array", "Point",
     "Given a point or points (pt) and a x value, add x to pt.x.
    ` When pt is a collection of points, add x to each pt if x is
    ` a number, or add x[i] to pt[i].x if x is an array. That is, 
    ` by setting x as array, we can add different values to each 
    ` pt[i].x. 
    ` 
    `   addx([2,3,4],x=1) => [3,3,4]
    `   addx([[2,3],[5,6]],x=1) => [[3,3],[6,6]]
    `   addx([[2,3],[5,6]],x=[8,9]) => [[10,3],[14,6]]
    "
    ,"addy,addz,app,appi"
    ];

    function addx_test( mode=MODE, opt=[] )=
    (   
        let( pts= [[2,3,4],[5,6,7]] )
        doctest( addx,
        [ 
          "// To a point:"
        , [addx([2,3],x=1), [3,3], "[2,3],x=1"]
        , [addx([2,3,4],x=1), [3,3,4], "[2,3,4],x=1"]
        , "// To points:"
        , [addx([[2,3],[5,6]],x=1), [[3,3],[6,6]], "[[2,3],[5,6]],x=1"]
        , [addx([[2,3],[5,6]],x=[8,9]), [[10,3],[14,6]], "[[2,3],[5,6]], x=[8,9]"]
        , [addx(pts,x=1), [[3,3,4],[6,6,7]], "[[2,3,4],[5,6,7]], x=1"]
        , [addx(pts,x=[2,3]), [[4,3,4],[8,6,7]], "[[2,3,4],[5,6,7]], x=[2,3]"]
        , "// keep = true: keep the original, append the new one(s):"
        , [addx([2,3],x=1, keep=true), [[2,3],[3,3]], "[2,3],x=1"]
        , [addx(pts,x=1, keep=true), [[2,3,4],[5,6,7],[3,3,4],[6,6,7]], "[[2,3,4],[5,6,7]], x=1, keep=true"]
        , [addx(pts,x=[2,3], keep=true), [[2,3,4],[5,6,7],[4,3,4],[8,6,7]], "[[2,3,4],[5,6,7]], x=[2,3], keep=true"]
        
        ]
        ,mode=mode, opt=opt
        )
    );
}// addx

//========================================================
{ // addy
function addy(pt,y=0,keep=false)=
(
	let( ispt = !isarr(pt[0])
     , rtn = !ispt // pt is a pts (list of points)
              ? [for(i=range(pt)) addy(pt[i], isarr(y)?y[i]:y) ]
              : len(pt)==3
                   ? [ pt.x,pt.y+y,pt.z ] 
                 : [ pt.x,pt.y+y] 
     )
    keep? (ispt? [pt,rtn]: concat(pt,rtn))
        : rtn  
);

    addy=[ "addy", "pt,y=0", "array", "Point"
    , "Add y to pt. See doc in addx"
    ,"addx,addz,app,appi"
    ];
    function addy_test( mode=MODE, opt=[] )=
    (   
        let( pts= [[2,3,4],[5,6,7]] )
        doctest( addy,
        [ 
          "// To a point:"
        , [addy([2,3],y=1), [2,4], "[2,3], y=1"]
        , [addy([2,3,4],y=1), [2,4,4], "[2,3,4], y=1"]
        , "// To points:"
        , [addy([[2,3],[5,6]],y=1), [[2,4],[5,7]], "[[2,3],[5,6]], y=1"]
        , [addy([[2,3],[5,6]],y=[8,9]), [[2,11],[5,15]], "[[2,3],[5,6]], y=[8,9]"]
        , [addy(pts,y=1), [[2,4,4],[5,7,7]], "[[2,3,4],[5,6,7]], y=1"]
        , [addy(pts,y=[2,3]), [[2,5,4],[5,9,7]], "[[2,3,4],[5,6,7]], y=[2,3]"]
        , "// keep = true: keep the original, append the new one(s):"
        , [addy([2,3],y=1,keep=true), [[2,3],[2,4]], "[2,3], y=1,keep=true"]
        , [addy(pts,y=1,keep=true), [[2,3,4],[5,6,7],[2,4,4],[5,7,7]], "[[2,3,4],[5,6,7]], y=1,keep=true"]
        , [addy(pts,y=[2,3],keep=true), [[2,3,4],[5,6,7],[2,5,4],[5,9,7]], "[[2,3,4],[5,6,7]], y=[2,3],keep=true"]
        
        ], mode=mode, opt=opt)
    );
} // addy
//========================================================
 { // addz  
function addz(pt,z=0,keep=false)=
(
	let( ispt = !isarr(pt[0])
     , rtn = !ispt // pt is a pts (list of points)
              ? [for(i=range(pt)) addz(pt[i], isarr(z)?z[i]:z) ]
              : len(pt)==3
                   ? [ pt.x,pt.y,pt.z+z ] 
                 : [ pt.x,pt.y,z] 
     )
    keep? (ispt? [pt,rtn]: concat(pt,rtn))
        : rtn  
);    
    addz=[ "addz", "pt,z=0", "array", "Point"
    , "Add z to pt. See doc in addx.
    ;; 
    ;; Note: addz(), unlike addx or addy, expands a 2D pt to 3D.
    "
    ,"addx,addy,app,appi"
    ];
    
    function addz_test( mode=MODE, opt=[] )=
    (
        let( pts= [[2,3,4],[5,6,7]] )
        
        doctest( addz,
        [ 
          "// To a point:"
        , [addz([2,3],z=1), [2,3,1], "[2,3], z=1"]
        , [addz([2,3,4],z=1), [2,3,5], "[2,3,4], z=1"]
        , "// To points:"
        , [addz([[2,3],[5,6]],z=1), [[2,3,1],[5,6,1]], "[[2,3],[5,6]], z=1"]
        , [addz([[2,3],[5,6]],z=[8,9]),[[2,3,8],[5,6,9]], "[[2,3],[5,6]], z=[8,9]"]
        , [addz(pts,z=1), [[2,3,5],[5,6,8]], "[[2,3,4],[5,6,7]], z=1"]
        , [addz(pts,z=[2,3]), [[2,3,6],[5,6,10]], "[[2,3,4],[5,6,7]], z=[2,3]"]
        , "// keep = true: keep the original, append the new one(s):"
        , [addz([2,3],z=1,keep=true), [[2,3],[2,3,1]], "[2,3], z=1,keep=true"]
        , [addz(pts,z=1,keep=true), [[2,3,4],[5,6,7],[2,3,5],[5,6,8]], "[[2,3,4],[5,6,7]], z=1,keep=true"]
        , [addz(pts,z=[2,3],keep=true), [[2,3,4],[5,6,7],[2,3,6],[5,6,10]], "[[2,3,4],[5,6,7]], z=[2,3],keep=true"]
        
        ], mode=mode, opt=opt )
    );    
}//addz

//========================================================
{ // angle
function angle(pqr, Rf=undef)= 
(
  isSamePt( pqr[0], pqr[1] )||isSamePt( pqr[1],pqr[2] )? undef
  : isSamePt( pqr[0],pqr[2])? 0
  
  : let(    
        arelines = len(pqr)==2 && isline( pqr[0] ) && isline( pqr[1] )
       , pqr = arelines? // two lines, move 2nd pt of line2 
               [ pqr[0][0], pqr[0][1], pqr[1][1]-pqr[1][0]+pqr[0][1]] 
               //[ uv(pqr[0]), ORIGIN, uv(pqr[1])] // conv to unit vectors.
               :pqr
        , P=pqr[0], Q=pqr[1], R=pqr[2]       
        , a= acos( (( R-Q)*( P- Q) ) / d01(pqr) / d12(pqr) )
        , np = normalPt( pqr )
        , sign= Rf==undef?1: isSameSide( [np, Rf], pqr)?1:-1
        , dpq=dist(P,Q), dpr=dist(P,R), dqr=dist(Q,R)
        )
    (
        iscoline(pqr)? ( dpq>=dpr? 0
                     : dqr>dpr? 0: 180
                     )
        :sign*a
       
    )
  /*
   2018.5.17 for future ref (this fails to make use of Rf, though):
   
   let( C = norm(cross( P-Q, R-Q ))
      , D = abs( dist(P,Q) * dist(R,Q) ) 
      )
   asin( C/D )
  */ 
   
);
   
    angle=["angle","pqr,Rf=undef","number","Angle",
     "Given a 3-pointer (pqr), return the angle of P-Q-R. 
    ;;    
    ;; 2015.3.3: new arg *Rf* : a point given to decide the sign of 
    ;;  angle. If the refpt is on the same side of normalPt, the angle
    ;;  is positive. Otherwise, negative. 
    ;;     
    ;; 2015.3.8: 
    ;;    
    ;;  Allows for angle( [line1,line2]). If 2 lines do not meet, then 
    ;;  the angle of both lines' unit vectors is returned.
    "
    ,"angle, angleBisectPt, anglePt, incenterPt, is90, Mark90"
    ];
   
    function angle_test( mode=MODE, opt=[] ) =
    ( 
        let( p = [1,0,0]
            ,q = [0,sqrt(3),0]
            ,r = ORIGIN

            ,pqr1=[ p,q,r]
            ,pqr2=[ p,r,q]
            ,pqr3= [ r,p,q]
            ,pqr4= randPts(3)
            ,pqr5= randPts(3)
            ,pqr6= randPts(3)
            ,pqr7= [ORIGIN, [1,0,0], [3,2,0]]
            
            ,line1 = [[0,0,0],[0,0,1]]
            ,line2 = [[1,0,0],[2,0,0]] 
            ,twolines = [randPts(2), randPts(2)]
        )
    //    function lines2()= [randPts(2), randPts(2)]; 
    //    function angarg(lineHasToMeet)= str( "[randPts(2),randPts(2)], lineHasToMeet="
    //                                       , lineHasToMeet );
        doctest( angle,
        [
            [angle(pqr1),30, "pqr1" ]
            ,[angle(pqr2),90, "pqr2" ]
            ,[angle(pqr3),60, "pqr3" ]
            ,[angle(pqr7),135, "pqr7" ]
            ,[angle([[1,0,0],[2,0,0],[3,0,0]]), 180, [[1,0,0],[2,0,0],[3,0,0]] ]
            ,"// The following 3 are random demos. No test."
            ,[ angle( randPts(3) ),"","randPts(3)",["notest",true] ]
            ,[ angle( randPts(3) ),"","randPts(3)",["notest",true] ]
            ,[ angle( randPts(3) ),"","randPts(3)",["notest",true] ]
            ,"// angle of two lines:"
            //, str("Are line1,line2 on plane? ",isOnPlane( app(line1, line2[0]), line2[1]))
            //, str("lineCrossPt = ", lineCrossPt( line1,line2 ) )
           // ,[ angle( [ line1,line2 ], lineHasToMeet=true), 90
           //  , "[line1,line2], lineHasToMeet=true" ] 
           // ,[ angle( [ line1,line2 ], lineHasToMeet=false), 90
           //  , "[line1,line2], lineHasToMeet=false" ]

            ,"// twolines: randomly generated 2x2 array: [ [p1,q1], [p2,q2] ] "
            , "var twolines"
    //        ,[ angle( twolines, lineHasToMeet=true )
    //          , "twolines, lineHasToMeet=true",""
    //          ,["notest",true]]
            ,[ angle( twolines), ""
              , "twolines"
              ,["notest",true]]

        
        ], mode=mode, scope=["pqr1",pqr1,"pqr2",pqr2, "pqr3",pqr3
                          //  ,"pqr4",pqr4, "pqr5",pqr5, "pqr6",pqr6
                            ,"pqr7", pqr7
                            ,"line1",line1, "line2",line2
                            ,"twolines", twolines
                            ]
                            ,opt=opt
        )
    );
} // angle

//========================================================
{ // anglebylen
function anglebylen(a,b,c)= // Return angle aABC
(
    let( abc = b==undef || c==undef?
               a: [a,b,c]
       , a = abc[0]
       , b = abc[1]
       , c = abc[2]
       , L = a+b+c
       , Lx = L*(L-2*a)*(L-2*b)*(L-2*c)
       , sinB= sqrt( Lx ) / (2*a*c)
       , 90more = b*b>a*a+c*c
       , ang = asin(sinB)
       )
    isequal(b*b,a*a+c*c)?90:90more? (180-ang):ang
);
//echo( a= anglebylen( 1,sqrt(2),1));
//echo( a= anglebylen( 2,sqrt(3),1));
  
    anglebylen = ["anglebylen", "a,b,c", "num", "Geometry"
    , "Given 3 sides of a triangle, a,b,c, return the angle opposite
    ;; the side b. 
    "
    ];
  
    function anglebylen_test( mode = 12, opt=[] )=
    (
        let( pqr10= pqr()
           , pqr20= pqr()
           
           , pqr1 = replace( pqr10,2,anglePt(pqr10, rand(5,85)) )
           , pqr2 = replace( pqr20,2,anglePt(pqr20, rand(95,175)) )
           , pqr90 = replace( pqr10,2,anglePt(pqr10, 90) )
           , pqr180 = replace( pqr10,2,anglePt(pqr10, 180) )
           
           , abc1 = [ d12(pqr1), d02(pqr1), d01( pqr1) ] 
           , abc2 = [ d12(pqr2), d02(pqr2), d01( pqr2) ] 
           , abc90 = [ d12(pqr90), d02(pqr90), d01( pqr90) ] 
           , abc180 = [ d12(pqr180), d02(pqr180), d01( pqr180) ] 
           , a1 = angle(pqr1)
           , a2 = angle(pqr2)
           , a90 = angle(pqr90)
           , a180 = angle(pqr180)
           )
        doctest( anglebylen
        , [  
            [ anglebylen( abc1 ), a1, abc1] 
          , [ anglebylen( abc2 ), a2, abc2] 
          , [ anglebylen( abc90 ), a90, abc90] 
          , [ anglebylen( abc180 ), a180, abc180] 
          ]
        , mode=mode, opt=opt, scope=["pqr1",pqr1,"pqr2",pqr2]
      )
    );      

//    module anglebylen_test(){
//        
//     pqr = pqr();
//     dPQ = dist(p01(pqr));
//     dPR = dist(p02(pqr));
//     dQR = dist(p12(pqr));
//     a = angle(pqr);
//     abl = anglebylen( dPQ,dPR,dQR );
//     same = isequal(abl,a);
//     echo( _color(a, a>=90?"red":"black"), abl, same = same?same:(abl-a) );   
//    }    
} // anglebylen

//========================================================
{ // angleAfter2Rots
function angleAfter2Rots(a,b)=
( /* In anglePt, a pt A is obtained by rotating P about
     NQ line by angle a to X, then rotating X about N(X,Q,N)
     by angle b. This function calcs the angle of aPQA
  */
   let( r=1
      , P_Pj = r*sin(a)
      , X_Pj = r*(1-cos(a))
      , Q_Pj = r- X_Pj
      , A_Pj_2 = pow(r,2)+ pow(Q_Pj,2)- 2*r*Q_Pj*cos(b)
      , A_P = sqrt( A_Pj_2 + pow(P_Pj,2))
      )
   anglebylen( r,A_P,r)
);

    angleAfter2Rots=["angleAfter2Rots", "a,b", "num", "Geometry"
    , "Rotate P about QN to X, then on the N direction, rotate
    ;; X about QM to A (M = N(X,Q,N)), return angle PQA:
    ;;  
    ;;              A
    ;;             / '.
    ;;            /    \\
    ;;           /      : 
    ;;     N    /       | X
    ;;     ^   /      _-```''-._   
    ;;     |  / b  _-`          `-_P
    ;;     | /  _-`  a  __..--''``
    ;;     |/_-`..--''``   
    ;;     Q-------------------> M
    ;;     
    ;; Imagine a walk on earth: P=>X is going east by degree a,
    ;; X=>A is going north by degree b, then we want to know aPQA.
    ;; 
    "];
        
    function angleAfter2Rots_test(mode=12,opt=[])= 
    (
        let( // When a < 90: 
             a1 = floor(rand(5,85))
           , b1_1= floor(rand(5,85))
           , b1_2= 90
           , b1_3= floor(rand(95,175))
           , pqx1= pqr( a= a1, p=5,r=5 )
           , P1 = pqx1[0]
           , Q1 = pqx1[1]
           , X1 = pqx1[2]
           , N1 = N(pqx1), xqn1=[X1,Q1,N1]
           , A1_1= anglePt( xqn1, len=5, a= b1_1 ) // b < 90
           , A1_2= anglePt( xqn1, len=5, a= b1_2 ) // b = 90
           , A1_3= anglePt( xqn1, len=5, a= b1_3 ) // b > 90
           
           // when a=90
           , a2 = 90
           , b2_1= floor(rand(5,85))
           , b2_2= 90
           , b2_3= floor(rand(95,175))
           , pqx2= pqr( a= a2, p=5,r=5 )
           , P2 = pqx2[0]
           , Q2 = pqx2[1]
           , X2 = pqx2[2]
           , N2 = N(pqx2), xqn2=[X2,Q2,N2]
           , A2_1= anglePt( xqn2, len=5, a= b2_1 ) // b < 90
           , A2_2= anglePt( xqn2, len=5, a= b2_2 ) // b = 90
           , A2_3= anglePt( xqn2, len=5, a= b2_3 ) // b > 90

           // when a>90
           , a3 = floor(rand(95,175))
           , b3_1= floor(rand(5,85))
           , b3_2= 90
           , b3_3= floor(rand(95,175))
           , pqx3= pqr( a= a3, p=5,r=5 )
           , P3 = pqx3[0]
           , Q3 = pqx3[1]
           , X3 = pqx3[2]
           , N3 = N(pqx3), xqn3=[X3,Q3,N3]
           , A3_1= anglePt( xqn3, len=5, a= b3_1 ) // b < 90
           , A3_2= anglePt( xqn3, len=5, a= b3_2 ) // b = 90
           , A3_3= anglePt( xqn3, len=5, a= b3_3 ) // b > 90

           )
        doctest( angleAfter2Rots
        , [  
            "// a &lt; 90: "
          , [angleAfter2Rots( a1,b1_1 ), angle([P1,Q1,A1_1]), str(a1,",",b1_1)] 
          , [angleAfter2Rots( a1,b1_2 ), angle([P1,Q1,A1_2]), str(a1,",",b1_2)] 
          , [angleAfter2Rots( a1,b1_3 ), angle([P1,Q1,A1_3]), str(a1,",",b1_3)] 
          , [angleAfter2Rots( b1_3,a1 ), angle([P1,Q1,A1_3]), str(b1_3,",",a1)
              ,["//", "Note this"]] 
 
          , "// a = 90: "
          , [angleAfter2Rots( a2,b2_1 ), angle([P2,Q2,A2_1]), str(a2,",",b2_1)] 
          , [angleAfter2Rots( a2,b2_2 ), angle([P2,Q2,A2_2]), str(a2,",",b2_2)] 
          , [angleAfter2Rots( a2,b2_3 ), angle([P2,Q2,A2_3]), str(a2,",",b2_3)] 

          , "// a > 90: "
          , [angleAfter2Rots( a3,b3_1 ), angle([P3,Q3,A3_1]), str(a3,",",b3_1)] 
          , [angleAfter2Rots( a3,b3_2 ), angle([P3,Q3,A3_2]), str(a3,",",b3_2)] 
          , [angleAfter2Rots( a3,b3_3 ), angle([P3,Q3,A3_3]), str(a3,",",b3_3)] 

          ]
        , mode=mode, opt=opt
      )
    );  
} // angleAfter2Rots

//========================================================
{ // angleAt
function angleAt(pts,i, Rf)=
(
   len(i)==3? angle( get( pts, i ), Rf )
   : angle( get3( pts, i-1 ), Rf )
   // : angle( get( pts, [ i-1,i,i+1] ), Rf )

);
    angleAt=[ "angleAt", "pts,i,Rf", "num", "Geometry"
    , "Return angle defined by 3 points that are defined by i. If i
    ;; is an integer, the 3 points are i-1,i,i+1. If i is an array, 
    ;; like [2,5,1], then the 3 pointers are pts[2], pts[5],pts[1].
    ;; 
    ;; The i could be negative, 0, or the last index.
    ;;
    ;; This function makes this easy:
    ;;
    ;; angles = [ for (i=range(pts)) angleAt(pts,i) ]
    "];
        
    function angleAt_test(mode=12,opt=[])= 
    (
       let( pts = randPts(5)
          , as = [ for (i=range(pts)) angleAt(pts,i) ]
          , negN4 = N( get3(pts,-2), -1)
          )
       doctest( angleAt,
       [
          "var pts",str( "len(pts)= ",len(pts)),""
          
          
         , [angleAt(pts,2), as[2], "pts,2"]
         , [angleAt(pts,0), as[0], "pts,0"]
         , [angleAt(pts,4), as[4], "pts,4"]
         , [angleAt(pts,-1), as[4], "pts,-1", ["//", "i could be negative"]]
          
         ,"","// Check Rf:"
         , "// negN4 is the reversed normalPt at i=4 (=N( get3(pts,3),-1)): " 
         , [angleAt(pts,4, negN4), -as[4], "pts,4,Rf=negN4"]
          
         ,"","// The following are not tests, but just demo:"
         ,""
         , [angleAt(pts,[1,2,0]), undef, "pts,[1,2,0]", ["notest",true]]
          
       ]
        , mode=mode, opt=opt, scope=["pts",pts]
          
       ) 
    );
} // angleAt       

//========================================================
{ // angleBisectPt
function angleBisectPt( pqr, len=undef, ratio=undef,Rf=undef )= 
(
    /*
    ;;          _.Q
    ;;        _- /|
    ;;      _'  / |
    ;;   _-'   /  |
    ;;  P-----D---R
    */
//   iscoline(pqr)?
//      anglePt( [pqr[0],pqr[1], Rf?Rf:randPt()], a=90)
//    :  onlinePt( [pqr[1], pqr[0]]
//              , len=dist( pqr[1], pqr[2])  
//              ) + pqr[2]-pqr[1]

    iscoline(pqr)?
      anglePt( [pqr[0],pqr[1], Rf?Rf:randPt()], a=90)
    :let( Q=pqr[1], r=d01(pqr)/d12(pqr) 
        , D = 0.5* pqr[0] + 0.5*( (1-r)*Q+ r*pqr[2] ) // When a+b=1, aP+bQ is on line PQ  
        )
    len!=undef? onlinePt( [Q,D], len=len )
        :ratio!=undef? onlinePt( [Q,D], ratio=ratio)
        :D


//    iscoline(pqr)?
//      anglePt( [pqr[0],pqr[1], Rf?Rf:randPt()], a=90)
//    :let( Q= pqr[1]
//        ,D= onlinePt( p02(pqr) //: ln_PR
//            , ratio= norm(pqr[0]-pqr[1]) //: d_PR
//                    /(norm(pqr[0]-pqr[1]) //: d_PR + d_RQ
//                     +norm(pqr[1]-pqr[2])) 
//                   )
//     )
//    len!=undef? onlinePt( [Q,D], len=len )
//        :ratio!=undef? onlinePt( [Q,D], ratio=ratio)
//        :D
);

    angleBisectPt=["angleBisectPt", "pqr,len=undef,ratio=undef", "Angle,Point", "Angle, Point, Geometry"
    ,"Return a pt D so aPQD = aPQR/2 and D on lPR (if len and ratio not defined).
    ;; 
    ;; Point D is pqr`s angle bisector (divide aPQR evenly).
    ;; 
    ;;          _.Q
    ;;        _- /|
    ;;      _'  / |
    ;;   _-'   /  |
    ;;  P-----D---R
    "
    ,"angle"
    ]; 
    
    function angleBisectPt_test( mode=MODE, opt=[] )=
    (
        let( pqr1 = randPts(3)
           , pqr2 = randPts(3)   
           , ang1 = angle( pqr1 )
           , ang2 = angle( pqr2 )
        )
        doctest( angleBisectPt, 
        [
            "To test, we get 2 random 3-pointers, pqr1 and pqr2, to see if the angle is divided evenly."
            ,""
            //,"var pqr1"
            ,"var ang1 // =angle(pqr1)"
            ,[angle([pqr1[0],pqr1[1], angleBisectPt( pqr1 )])*2
                    , ang1, "[P,Q,angleBisectPt(pqr1)]"
                    //, ["myfunc","angle([p,q,angleBisectPt(pqr1)])*2"]
                    , ["fname","angle(_)*2"]
            ]
            ,""
            //,str("pqr2= ", arrprn( pqr2, 2))
            ,"var ang2 // =angle(pqr2)"
            ,[angle([pqr2[0],pqr2[1], angleBisectPt( pqr2 )])*2 
                    , ang2, "[P,Q,angleBisectPt(pqr2)]"
                    //, ["myfunc","angle([p,q,angleBisectPt(pqr2)])*2"]
                    , ["fname","angle(_)*2"]
             ]       
        ]
        , mode=mode
        , scope= ["pqr1",pqr1,"pqr2",pqr2, "ang1",ang1,"ang2",ang2 ]
        , opt=opt
        )
    );
} // angleBisectPt

//========================================================
{ // anglePt


function anglePt( pqr, a=0, a2=0, len=undef
                , ratio=1
                , Rf=undef  // Rf could be used when pqr is coln
                )=
(    
    
/*  



    ;;  a2 not fiven:
    ;; 
    ;;           R     _ S    
    ;;         -'   _-'  |
    ;;       -'  _-' len |
    ;;     -' _-'        |
    ;;   -'_-'  a        | 
    ;;  Q----------------J--P

    (P-Q)*(S-Q) = dPQ*dSQ*cos(a) = dJQ * dPQ
    
    dSQ = L
    
    L*cos(a) = dJQ
    
    r = dJQ / dPQ  
      = L*cos(a)/dQP
      
    J = r*P + (1-r)*Q
    S = B-Q+J
 
    ;; 
    ;;  a2 fiven:
    ;; 
    ;;        N         _T
    ;;        |      _-'
    ;;        |   _-:   a2= aSQT
    ;;       /|_-'   :
    ;;      | Q------'. --------- M 
    ;;      |/:'-._/  :
    ;;      /__:__/'-.|
    ;;     /    :      '-._
    ;;    /      R    _-'   S
    ;;   P ... a= aPQS
    ;; 
*/

  //echo( log_b( _s("anglePt(a={_}, a2={_}, len={_})",[a,a2,len]),1))
  let( P=pqr[0], Q=pqr[1], R=pqr[2]
     , _a=a%360
     , isflip = _a>180|| (-180<_a && _a<0)
     , pqr= isflip? newz(pqr, 2*Q-R): pqr
     , N=N(pqr)
     , a= abs(_a)
     , dPQ = norm(P-Q)
     , L = ratio* (len?len:dPQ )
     , r = L*cos(a)/dPQ
     , J = r*P +(1-r)*Q
     , S = a==180? J
           : a==0? onlinePt( [Q,P], len= L)
           : onlinePt( [J, B(pqr)-Q+J]
                   , len= (len<0?-1:1)*sqrt(L*L-norm(J-Q)*norm(J-Q)))
     , S2= a2?anglePt([S,Q,N], a=a2, a2=0, ratio=ratio, len=L): S
             
//   // Keep the following for debugging                   
//     , S2 = a2?
//            //echo(log_b( _s("Resolving a2={_}",a2), 2))
//            let( _pqr2= [S, Q, N]
//               , _a2= a2%360
//               , isflip2 = _a2>180|| (-180<_a2 && _a2<0)
//               , pqr2= isflip2? newz(_pqr2, 2*Q-N): _pqr2
//               //, N2=N(pqr2)
//               , a2= abs(_a2)
//               , dSQ = norm(S-Q)
//               //, L2 = ratio* (len?len:dPQ2 )
//               , r2 = L*cos(a2)/dSQ //dPQ2
//               , J2 = r2*S +(1-r2)*Q
//               , S2 = a2==180? J2
//                      : onlinePt( [J2, B(pqr2)-Q+J2]
//                             , len= (len<0?-1:1)*sqrt(L*L-norm(J2-Q)*norm(J2-Q)))
//               ) 
//            //echo(log_(["_a2",_a2,"a2",a2,"len",len, "Q",Q, "J2",J2, "dPQ2",dPQ2, "N2",N2, "L2",L2,"r2",r2],2))
//            //echo(log_(["pqr2",pqr2],2))
//            //echo(log_(["S2",S2],2))
//            //echo(log_e("",2))                
//            S2
//           : S
     
     
     
     ) 
  //echo( a=a, L=L, r=r, J=J, B=B(pqr) )
  //echo(_a=_a, a=a, L=L, S=S,S2=S2,S3=S3) 
  //echo(log_(["_a",_a,"a",a,"a2",a2,"len",len, "Q",Q, "J",J, "dPQ",dPQ, "L",L,"r",r,"S",S, "S2",S2],1))
  //echo(log_(["pqr",pqr, "len_to_onlinePt", (len<0?-1:1)*sqrt(L*L-norm(J-Q)*norm(J-Q))],1))
  //echo(log_(["a",a, "a2", a2, "a3", a3],1))
  //echo(log_(["pqr",pqr, "J", J, "J2",J2],1))
  //echo(log_(["_a",_a, "L",L,"S",S,"S2",S2,"S3",S3], 1))  
  //echo( log_b( "anglePt()",1))
  //echo( log_(["pqr",pqr],7))
  //echo( log_(["_pqr",_pqr],7))
  //echo( log_(["a",a, "a2",a2],7) )
  //echo( log_(["ratio",ratio],7) )
  //echo( log_(["pt",pt],7) )
  //echo( log_(["rtn",rtn],7) )
  //echo( log_e("",1) )       
  S2
               
);

//_a=45, a=45, a2=180, S=[0.507377, -0.109892, 1.01141], S2=[2.83262, 1.56989, 1.30859]
//_a=45, a=45, a2=180, S=[0.507374, -0.109894, 1.01141], S2=[nan, nan, nan]

//_a=45, a=45, a2=180, L=1.44194, r=0.707105, S=[0.507377, -0.109892, 1.01141], S2=[2.83262, 1.56989, 1.30859]"
//_a=45, a=45, a2=180, L=1.44194, r=0.707107, S=[0.507374, -0.109894, 1.01141], S2=[nan, nan, nan]"

//_a=45, a=45, a2=180, dPQ=1.44194, L=1.44194, r=0.707105, S=[0.507377, -0.109892, 1.01141], S2=[2.83262, 1.56989, 1.30859]"
//_a=45, a=45, a2=180, dPQ=1.44194, L=1.44194, r=0.707107, S=[0.507374, -0.109894, 1.01141], S2=[nan, nan, nan]

//_a=45, a=45, a2=180, len=1.44194, dPQ=1.44194, L=1.44194, r=0.707105, S=[0.507377, -0.109892, 1.01141], S2=[2.83262, 1.56989, 1.30859]"
//_a=45, a=45, a2=180, len=1.44194, dPQ=1.44194, L=1.44194, r=0.707107, S=[0.507374, -0.109894, 1.01141], S2=[nan, nan, nan]"

//_a=180, a=180, a2=0, len=1.44194, dPQ=1.44194, L=1.44194, r=-1, S=[2.83262, 1.56989, 1.30859], S2=[2.83262, 1.56989, 1.30859]"
//ECHO: "     len_to_onlinePt=0"

//_a=180, a=180, a2=0, len=1.44194, dPQ=1.44194, L=1.44194, r=-1, S=[nan, nan, nan], S2=[nan, nan, nan]"
//ECHO: "     len_to_onlinePt=nan"


function anglePt_12cc( pqr, a=undef, a2=0, len=undef
                , ratio=1
                , Rf=undef  // Rf could be used when pqr is coln
                )=
(    
    
/*     2015.5.7: new arg: an (angle to the N (normalPt))
       
       B-----------_R-----A 
       |         _:  :    |
       |  T._---:-----:---+ S 
       |  |  '-:_      :  | 
       |  |   :  '-._   : |
       |  |  :     a '-._:| 
       p--+--+-----------'Q
          U  D    


       B----_R-----A--._ 
       |      :    |    ''-_
       |       : S +--------T 
       |        :  |      -'|
       |         : |   _-'  |
       |          :|_-'     |
       p----------'Q--------U

            a = a_PQT
     _R    B-----------A--._ 
       `-_ |      :    |    ''-_
          `|_      : S +--------T 
           | `-_    :  |      -'|
           |    `-_  : |   _-'  |
           |       `-_:|_-'     |
           p----------'Q--------U
*/
  let( a0 = angle(pqr)
     , a= a==undef? a0:a
     , Q= Q(pqr)
     , QP= p10(pqr)
     , dPQ= dist( QP )
     // Introducing R to deal with colinear pqr
     , R = is0(a0-180)
            ? (ispt(Rf)?Rf: randOfflinePt( p01(pqr)))
            : pqr[2]      
     ,_pqr= [pqr[0],pqr[1],R]
     , L= ratio* (len?len:dPQ )
     , QA= [ Q, squarePts( _pqr )[2] ]
    
     , pt= abs(a)==90? // if a=90, return S
            onlinePt( QA, len= sign(a)*L )
          :abs(a)==270? // if a=270
            onlinePt( QA, len= -sign(a)*L )
          :   
          squarePts(
            [ onlinePt( QP, len= L*cos(a) ) //: U
            , Q
            , onlinePt( QA, len= L*sin(a) ) //: S	
            ])[3]
     , rtn= is0(a)&&(is0(a2)||!a2)?
              onlinePt( QP, len=L )
            :
            a2? anglePt( [ pt
               , Q
               , N(_pqr)
               ], a=a2, len=L ):pt       
     ) 
  //echo( log_b( "anglePt()",1))
  //echo( log_(["pqr",pqr],7))
  //echo( log_(["_pqr",_pqr],7))
  //echo( log_(["a",a, "a2",a2],7) )
  //echo( log_(["ratio",ratio],7) )
  //echo( log_(["pt",pt],7) )
  //echo( log_(["rtn",rtn],7) )
  //echo( log_e("",1) )       
  rtn
               
);

    anglePt=["anglePt","pqr,a=aPQR,a2=0,len=dPQ,ratio=1", "point", "Angle,Point"
    , "Return pt S so aPQS=a, dSQ=len. If a2 given, return T= anglePt(SQN,a=a2,...) where N is pqr`s normal. 
    ;; 
    ;; a: aPQS. Default= aPQR (i.e., S=R) 
    ;; a2: aSQN where N= pqr`s normal. Default=0
    ;; len: default = dPQ
    ;; ratio: default = 1
    ;; 
    ;;  a2 not fiven:
    ;; 
    ;;           R     _ S    
    ;;         -'   _-' 
    ;;       -'  _-'  len
    ;;     -' _-' 
    ;;   -'_-'  a
    ;;  Q-------------------P
    ;; 
    ;;  a2 fiven:
    ;; 
    ;;        N         _T
    ;;        |      _-'
    ;;        |   _-:   a2= aSQT
    ;;       /|_-'   :
    ;;      | Q------'. --------- M 
    ;;      |/:'-._/  :
    ;;      /__:__/'-.|
    ;;     /    :      '-._
    ;;    /      R    _-'   S
    ;;   P ... a= aPQS
    ;; 
    ;; To rotate any pt X about [P,Q] line by angle ang : 
    ;; 
    ;;    anglePt( [P, othoPt( [P,X,Q] ), X], a2= ang ) 
    "
     ];
    function anglePt_test( mode=MODE, opt=[] )=
    (
        doctest( anglePt, [], mode=mode,opt=opt )
    );

} // anglePt

//==============================================
{ // arcPts
function arcPts(  // If set a2, arc will span on plRQN and
                  //   a not given, a will be autoset to aPQR
                pqr 
                , n=6 // n = nPts
                //, nseg=4 // n segments  
                         // Note 2018.12.13: we change n to nseg 
                         // 'cos it would be hard to decide angle between open
                         // and closed system:
                         //       closed:      open
                         //                      3_
                         //       2-----1       /  '-2
                         //       |     |      /     \ 1
                         //       3_____0     +_______|0
                         //
                         // n        4            4
                         // nseg     4            3  
                         // ang    360/4=90     75/(4-1) = 25      
                , rad= undef
                , a = undef
                , a2 = undef
                //, includeLast = true // set to false for closed s
                //, a3 = undef
                , ratio= 1
                )=
(   
 //echo( log_b( "arcPts()",5))
     
 let( pqr = pqr?pqr:pqr()
    //, P=P(pqr), Q=Q(pqr), R=R(pqr)
	  //, n = und(n,6)
    , _a= a==undef?angle(pqr):a
    , _rad= (rad==undef? dist([pqr[0], pqr[1]]):rad)*ratio
            //und(rad, d01(pqr))*ratio
    
    //---------------------------------
    // Use P_last to decide if this is a closed system
    // If P_last==P0 then it's closed
    // The angle_per_segment is different between closed and open cases
    //---------------------------------
    
    , S = anglePt( pqr, a=_a, len=_rad)
    , rtn= a2?
           let( Plast_2 = anglePt( pqr, a=_a, a2=a2, len=_rad)
              , is2closed = isequal(S, Plast_2)                 
              , a2_per = a2/(n- (is2closed?0:1))
              ) 
           [ for(i=[0:n-1]) anglePt(pqr=pqr,a=_a,a2=i*a2_per,len=_rad) ]
          :let( P0_1 = onlinePt( [pqr[1],pqr[0]], len=_rad )
              , is1closed = isequal(P0_1, S)                 
              , a1_per  = _a/(n - (is1closed?0:1))
              )          
          [ for(i=[0:n-1]) anglePt(pqr=pqr,a=i*a1_per,len=_rad) ]
    )
    
    //echo( log_(["pqr",pqr, "ang_pqr", angle(pqr)],5))
    //echo( log_(["a",a, "_a",_a, "n",n,"a1_per", a1_per ],5) )
    //echo( log_(["P0_1",P0_1,"Plast_1",Plast_1, "is1closed", is1closed],5) )
    //echo( log_(["a2",a2, "is2closed", is2closed, "a2",a2, "a2_per", a2_per],5) )
    //echo( log_(["ratio",ratio, "rad",rad, "_rad",_rad],5) )
    //echo( log_(["rtn",rtn],5) )
    //echo( log_e("",5) )
    rtn	
);
//function arcPts(  // If set a2, arc will span on plRQN and
//                  //   a not given, a will be autoset to aPQR
//                pqr 
//                //, n=6 // n = nPts
//                , nseg=4 // n segments  
//                         // Note 2018.12.13: we change n to nseg 
//                         // 'cos it would be hard to decide angle between open
//                         // and closed system:
//                         //       closed:      open
//                         //                      3_
//                         //       2-----1       /  '-2
//                         //       |     |      /     \ 1
//                         //       3_____0     +_______|0
//                         //
//                         // n        4            4
//                         // nseg     4            3  
//                         // ang    360/4=90     75/(4-1) = 25      
//                , rad= undef
//                , a = undef
//                , a2 = undef
//                //, includeLast = true // set to false for closed s
//                //, a3 = undef
//                , ratio= 1
//                )=
//(    
// let( pqr = pqr?pqr:pqr()
//    //, P=P(pqr), Q=Q(pqr), R=R(pqr)
//	  //, n = und(n,6)
//    , a= a==undef?angle(pqr):a
//    , ratio = und(ratio,1)
//    , _rad= (rad==undef? dist([pqr[0], pqr[1]]):rad)*ratio
//            //und(rad, d01(pqr))*ratio
//    , rtn= a2==undef? // a apply to aPQR 
//           let(P0 = onlinePt( pqr, len=-_rad ) 
//              ,rest= [ for(i=[1:nseg-1]) anglePt( pqr=pqr, a=i*a/nseg, a2=0, len=_rad )
//                     ]  
//              ,last= anglePt( pqr=pqr, a=nseg*a/nseg, a2=0, len=_rad )        
//              )
//              concat([P0],rest, isequal(P0, last)?[]:[last])             
//        :  [ for(i=[0:nseg]) 
//              let(pt= anglePt( pqr=pqr, a=a, a2=i*a2/nseg, len=_rad ) )
//              //echo("in arcPts:", i=i, a=a, a2=i*a2/n, _rad=_rad, pt=pt)
//              pt
//              //anglePt( pqr=pqr, a=a, a2=i*a2/n, len=_rad ) 
//           ]   
//    )
//    //echo( log_b( "arcPts()",5))
//    //echo( log_(["pqr",pqr],5))
//    //echo( log_(["a",a, "a2",a2],5) )
//    //echo( log_(["ratio",ratio, "rad",rad, "_rad",_rad],5) )
//    //echo( log_(["rtn",rtn],5) )
//    //echo( log_e("",5) )
//    rtn	
//);
 
    arcPts=["arcPts", "pqr,n=6,rad,a,a2"
    , "points", "Geometry"
    , "Given pqr, return n pts for a curve spanning w/ radius=rad. 
    ;; The a and a2 decide the surface and the angle the same way
    ;; anglePt does:
    ;; 
    ;;             X
    ;;            / '.
    ;;           /    \
    ;;          /      : 
    ;;         /       | R
    ;;        /      _-```''-._   
    ;;       / a2 _-`          `-_ P
    ;;      /  _-`  a  __..--''``
    ;;     /_.:..--''``   
    ;;    Q`
    ;;
    ;; If a2 given but not a, a is set to aPQR.
    ;; 
    ;; New 2018.12.13: automatically adjust angle per segment 
    ;; in closed and open cases.
    ;;
    ;;      closed:      open
    ;;                     3_
    ;;      2-----1       /  '-2
    ;;      |     |      /     \ 1
    ;;      3_____0     +_______|0
    ;;
    ;;   n        4            4
    ;;  ang  =a/4      = a/(4-1)   <===== 
    "
     
    ];
    

    function arcPts_test( mode=MODE, opt=[] )=
    (
        doctest( arcPts, [], mode=mode, opt=opt )
    );    
} // arcPts         
    
//==============================================
//function chainPts0( 
//
//           pts
//         , r 
//         , nside
//         , closed 
//         , opt=[] 
//         , _xsecs=[]
//         , _Rf0=undef
//         , _i=0
//         , _debug=[]
//         )=
//(
//   _i==0?
//   
//   let(//###################################### i = 0 
//   
//      //------------------------------
//      //    param initiation
//
//        opt = popt(opt) // handle sopt
//        
//      , r   = und(r, hash(opt,"r", 0.008))      
//      , nside= or( nside, hash(opt,"nside",4))
//      , closed= und(closed, hash(opt,"closed",false))      
//       
//      , preP = onlinePt( p01(pts), -1)
//      , Rf0  = len(pts)==2? randOfflinePt( p01(pts) ) 
//               : !iscoline( p012(pts))? pts[2]
//               : randOfflinePt( p01(pts) )  
//                         
//      , pl90 = arcPts( [preP, pts[0], Rf0]
//                     , a=90
//                     , a2= 360*(nside-1)/nside
//                     , n=nside, rad= r
//                     )                   
//            
//      , _xsecs= [ pl90 ]
//      , _debug= str( _red("<br/><br/><b>chainPts0 debug:</b>")
//        , "<br/><b>_i</b> = ", _i
//        , "<br/><b>Rf0</b> = ", Rf0
//        , "<br/><b>pl90</b> = ", pl90
//        , "<br/><b>_xsecs</b> = ", _xsecs
//        )     
//      )// let @ i=0
//      
//      chainPts0( pts, r=r
//              , nside = nside
//              , closed= closed
//              , _xsecs= _xsecs
//              ,_Rf0=Rf0, _i=_i+1
//              , _debug=_debug
//              )
//       
//   //####################################### 0 < i < last 
//   
//   : 0<_i && _i< len(pts)-1 ?  
//   
//     let( _Rf= last(_xsecs)[0]
//        , Rf = iscoline( get( pts, [_i-1,_i]), _Rf)?
//                randOfflinePt(get( pts, [_i-1,_i])): _Rf
//        , pl90_i = arcPts( [pts[_i-1], pts[_i], Rf]
//                 , a=90
//                 , a2= 360*(nside-1)/nside
//                 , n=nside, rad= r
//                 )  
//         , patrtn= planeDataAt( pts,_i ) 
//         , pl_i = hash( patrtn, "pl")
//         , xsec_i = projPts( pl90_i //last(_xsecs)
//                           , pl_i
//                           , along=get(pts,[_i-1,_i] )
//                           )
//         , _xsecs = app(_xsecs, xsec_i) 
//      , _debug= str( _debug
//        , "<br/><br/><b>_i</b> = ", _i
//        , "<br/><b>_Rf</b> = ", _Rf
//        , "<br/><b>Rf</b> = ", Rf
//        , "<br/><b>pl_i</b> = ", pl_i
//        , "<br/><b>pl90_i</b> = ", pl90_i
//        , "<br/><b>pts</b> = ", pts
//        , "<br/><b>_xsecs</b> = ", _xsecs
//        )           
//         )// let @ i>0
//
//       chainPts0( pts, r=r
//               , nside =nside
//               , closed=closed
//               , _xsecs=_xsecs
//               ,_Rf0=_Rf0,_i=_i+1
//              , _debug=_debug
//               )
//
//   //#################################### i = last
//   
//   : //_i==len(pts)-1?
//
//     let( pl_i= len(pts)==2?undef
//                  : hash(
//                         planeDataAt( pts,_i,
//                         , Rf= last(_xsecs)[0]
//                         , closed= closed
//                         ), "pl")
//                  
////        //, pl_i = patrtn //hash( patrtn, "pl")
////        , xsec_i =  len(pts)==2?
////                    [ for(pt= last(_xsecs)) pt+ pts[1]-pts[0] ]
////                    : projPts( last(_xsecs)
////                           , pl_i
////                           , along= get( pts,[_i-1,_i] )
////                           )
////                  
////                  
////        , _xsecs = app(_xsecs, xsec_i)
////      , _debug= str( _debug
////        , "<br/><br/><b>_i</b> = ", _i
////       , "<br/><b>pl_i</b> = ", pl_i
////        , "<br/><b>_xsecs</b> = ", _xsecs
////        , _red("<br/><b>chainPts0 debug END</b><br/>")
//                   
////        )   
//        )
//      //_debug
//        [ "xsecs", _xsecs
//        , "bone", pts
//        , "closed", closed
//        , "Rf0", _Rf0
//        , "debug", _debug 
//        
//        ]
//); 
    

//. b
//========================================================
//{ // borderPts
//function borderPts_old(pts, border="min", xyz="x")=
//(
//    /* Return an array containing pts that have the
//       min or max value (determined by *border*) of
//       a group of pts (*pts*) along the x,y,z direction
//       
//       borderPts( pts, "min","x" ) 
//         = those in pts having the min x 
//    */
//    let( xyz= xyz=="x"?0:xyz=="y"?1:2
//       , d = [ for(p=pts) p[ xyz ] ]
//       , b = border=="min"? min(d):max(d)
//       , pts= [ for(p=pts) if(p[xyz]==b) p]
//       )
//    pts   
//);
//       
//    borderPts_old= ["borderPts", "pts,border='min',xyz='x'","pts","geometry"
//    ,"Return an array containing pts that have the
//    ;;   min or max value (determined by *border*) of
//    ;;   a group of pts (*pts*) along the x,y,z direction
//    ;;   
//    ;;   borderPts( pts, 'min','x' ) 
//    ;;     = those in pts having the min x
//    "];
//   
//    function borderPts_test( mode=MODE, opt=[] )=
//    (
//        doctest( borderPts, [], mode=mode, opt=opt )
//    );    
//} // borderPts   

//function boxPts(xyz)=
//(
///*
//   boxPts
//
//         z  
//         |
//         0_--------- y
//           '-_   
//              '-_ 
//                 'x
//         Z
//         |
//       z 4----------7_ 
//         | '-_      | '-_
//         0_---'-_---3----'-_ ------ Y
//           '-_   '5---------6
//              '-_ |      '-_|
//                 '1---------2
//                x  '-_
//                      '-_
//                         ` X
//
//*/
//   [ ORIGIN, [xyz[0],0,0], [xyz[0],xyz[1],0], [0,xyz[1],0]
//   , [0,0,xyz[2]], [xyz[0],0,xyz[2]], [xyz[0],xyz[1],xyz[2]], [0,xyz[1],xyz[2]]
//   ]
//);

//. c

function cavePF( pts,faces
               , template=2
               , dest=[4,5,6]
               , offset=undef
               , depth 
               )=
(
  /* Carve a hole (defined by a 2d template) on the dest of an obj 
     defined by pts and faces.
      
     Return [newpts, newfaces]
     
     template: a collection of 2D pts arranged CCW. Could be defined as:
     
       (1) number: Size of a square
       (2) [x,y]: Size of a rectangular hole. 
       (3) 2D pts: An array of 2d pts on XY plane (counter CW)
                   
     dest: [i,j,k] where i,j,k are indices of pts on the same face, like [4,5,6]
           or an int serves as a fi (face index) in which that the 
           first 3 indices of that face is treated as the dest 
           
                   6-----------7
                .'          .' |
              .'          .'   |
             5----------4'     |
             |          |      3
             |          |    .'
             |          | .'
             1----------0'      
           
           In this case (dest= [4,5,6]), the ORIGIN of template will be 
           anchored on the pts[5]:
     
             y |                   6--------------+
               |                   |              |
               |  +.               |  +.          |
               |  | '.       ===>  |  | '.        |
               |  |   '.           |  |   '.      |
               |  +-----+          |  +-----+     |
               O-------------      5--------------4
                         x

     offset: default: undef, num, or [a,b]
     
        If undef, template is centered
        If num, [num,num] 
        if [a,b], then: 
        
            if template is a num or [x,y], then the square/rectangular 
            is placed with [a,b] as the low-bottom pt on the target face:       
               
               6---------------7
               |               |
               |   +-----+     |
               |   |     |     |
            -- + - +-----+     |
             b |   :           |
            -- 5---+-----------4
               | a |
               
            if template is a list of pts, then the template be placed
            by adding offset to them:
     
             y |                 6--------------+
               |                 |              |
               |   +.            |      +.      |
               |   | '.    ===>  |      | '.    |
               |   |   '.        |      |   '.  |
               |   +-----+       |      +-----+ |
               O-----------      5--------------4
               | x |             | x+ a | 
                                 offset = [a,0] 
     
       if [a,undef] ==> auto centered on y side
       if [undef,b] ==> auto centered on x side
       
    depth: 
    
       How deep down. If not given, cut through. 
       It could be a number d or a single-item array [d]
       
       When [d], d is treated as a ratio
       When d <0, pops up instead of carve in.
        
       
    To do:
     
    dirline= p2 = a pair of pts showing the direction of cutting, 
           like [ pts[5], pts[1] ]
           If not given, cut alone the normal line of the dest
    taper 
           
        
   */ 
   /*  let target site= [P,Q,R ] 
   
            R---------S
          .'        .'|
         Q--------P'  |
         |        |   3
         |        | .'
         1--------0'      
   */
   let( _dest=isint(dest)? get(faces[dest],[0,1,2]) : dest
      , P=pts[_dest[0]], Q=pts[_dest[1]], R=pts[_dest[2]]
      , S = P-Q+R          
      , C = (P+Q+R+S)/4
      , dPQ = dist(P,Q)
      , dQR = dist(Q,R)
      , offset= isnum(offset)?[offset,offset]:offset
      , tmp=  isnum(template)?
              let( offx= offset? und(offset[0], (dPQ-template)/2 ) 
                               : (dPQ-template)/2
                 , offy= offset? und( offset[1], (dQR-template)/2 ) 
                                : (dQR-template)/2 )  
              [ [ offx+template, offy,0]
              , [ offx+template, offy+template,0]
              , [ offx, offy+template,0]
              , [ offx, offy,0]
              ]
           : len(template)==2?
                let( offx= offset?offset[0]: (dPQ-template[0])/2
                   , offy= offset?offset[1]: (dQR-template[1])/2 )  
              [ [ offx+template[0], offy,0]
              , [ offx+template[0], offy+template[1],0]
              , [ offx, offy+template[1],0]
              , [ offx, offy,0]
              ]
           : to3d( offset==[0,0,0]? template
                   : offset? [for(p= template) p+offset ]
                   : // centering when offset==undef
                     let( tmp_C= sum(template)/len(template) 
                        , offxy= [dPQ/2,dQR/2]- tmp_C
                        )
                     [for(p=template) p+offxy ]
                 )   
      , topHolePts = movePts( tmp
                            , from= ORIGIN_SITE
                            , to= [P,Q,R] 
                            )
      , hEdgefis = get_edgefis(faces)
                     // h{edge:hFis}:   
                     // [ [2,3], ["L",3,"R",6,"F",[4], "B",[0,1]] 
                     // , ...
                     // ]
      , fi= hash(edgefis_get( hEdgefis
                         , [_dest[0],_dest[1]] 
                         ), "R" )
      , nearfi= hash(edgefis_get( hEdgefis
                         , [_dest[0],_dest[1]] 
                         ), "L" )
      , normalPi = prev( faces[nearfi], _dest[1]  )
      , oppofi = get_near_fi(faces, nearfi
                            , dels( faces[nearfi], _dest) 
                            )
      , normaldist = dist( Q, pts[normalPi] )  
      
      , _depth = depth==undef || abs(depth)>= normaldist ? undef
                 : len(depth)==1? // :When depth is [d], use d as ratio 
                   ( let(d= depth[0])
                     d>=1? undef // When >1, treated as no depth defined
                     : normaldist* d
                   )
                 : depth 
                 
      , botHolePts= /// NOTE: this is for cube only. Other shapes will likely fail 
                    _depth==undef?
                     [ for(p=topHolePts) 
                      //p+ pts[ normalPi ]- Q  // ==>this is for cube obj only
                      intsec( [p,p+ pts[ normalPi ]- Q]
                            , get(pts, slice(faces[oppofi],1))
                            ) // 2018.10.12: For non-cube obj:
                              // when _depth=undef, means punching thru, we find 
                              // the line extended from each topHolePts and make 
                              // them intersec with the bottom plane. 
                     ]
                    : [ for(p=topHolePts) 
                        p+ onlinePt( [Q,pts[normalPi]], len=_depth )-Q
                      ]   
      , newPts = concat( topHolePts, botHolePts)
      , finalPts = concat( pts, newPts )      
      
      , oldfis = dels( range(faces)
                     , _depth==undef? [fi,oppofi]
                                    : [fi] 
                     )                   
                  
      , newsidefaces= [ for(i=range(tmp))
                        [ len(pts)+ i
                        , len(pts)+ (i==0? (len(tmp)-1): i-1)
                        , len(pts)+ (i==0? (len(tmp)-1): i-1)+len(tmp) 
                        , len(pts)+ i+ len(tmp)
                        ]  
                      ]
      , topfaces = concat( faces[fi], faces[fi][0], range(8,8+len(tmp)), 8 )
      , botfaces = _depth==undef? 
                    concat( faces[oppofi], faces[oppofi][0]
                          , range( len(finalPts)-1
                                 , len(finalPts)-1-len(tmp)
                                 )
                          , len(finalPts)-1 
                          )
                   : range( len(finalPts)-1
                          , len(finalPts)-1-len(tmp)
                          ) 
                           
      , finalfaces= concat( 
            //[ for( fj=range(faces) )
              //if((fj!=fi)&&(fj!=oppofi)) faces[fj]
            //] 
            get(faces, oldfis) 
          , newsidefaces
          , [topfaces]  
          , [botfaces]   
          )                        
      )  
   //echo("-----------")
   //echo(pts=pts)
   //echo(faces=faces)   
   //echo(template=template)
   //echo(offset=offset)
   //echo(tmp=tmp)
   //echo(dest=dest)
   //echo(_dest=_dest)
   //echo(C=C)
   //echo(P=P,Q=Q,R=R,S=S,C=C)
   //echo( topHolePts = topHolePts )
   //echo(hClockedEFs = hClockedEFs)
   //echo(hEdgefis=hEdgefis)
   //echo(edges_in_hEdgefis = keys(hEdgefis))
   //echo(normalPi = normalPi)
   //echo(normaldist=normaldist)
   //echo(depth=depth)
   //echo(_depth=_depth)
   //echo( botHolePts = botHolePts ) 
   //echo(fi=fi, nearfi=nearfi, oppofi = oppofi )
   //echo(botface = faces[oppofi] )
   //echo(botPlanePts = get(pts, slice(faces[oppofi],1) ) )
   //echo(botHolePts=botHolePts )
   //echo(oldfis=oldfis)
   //echo(oldfaces=oldfaces)
   //echo(newsidefaces = newsidefaces)
   //echo(finalfaces=finalfaces)
   [finalPts, finalfaces]
);

    cavePF=[ "cavePF", "pts,faces,template", "num", "Geometry"
    , "Carve a hole (defined by a 2d template) on the dest of an obj 
    ;; defined by pts and faces.
    ;;      
    ;; Return [newpts, newfaces]
    ;;     
    ;; template: a collection of 2D pts arranged CCW. Could be defined as:
    ;;     
    ;; (1) number: Size of a square
    ;; (2) [x,y]: Size of a rectangular hole. 
    ;; (3) 2D pts: An array of 2d pts on XY plane (counter CW)
    ;;                   
    ;; dest: [i,j,k] where i,j,k are indices of pts on the same face, like [4,5,6]
    ;;       or an int serves as a fi (face index) in which that the 
    ;;       first 3 indices of that face is treated as the dest 
    ;;           
    ;;            6--------7
    ;;          .'       .'|
    ;;        5--------4'  |
    ;;        |        |   3
    ;;        |        | .'
    ;;        1--------0'      
    ;;           
    ;;       In this case (dest= [4,5,6]), the ORIGIN of template will be 
    ;;       anchored on the pts[5]:
    ;;     
    ;;        y |                   6--------------+
    ;;          |                   |              |
    ;;          |  +.               |  +.          |
    ;;          |  | '.       ===>  |  | '.        |
    ;;          |  |   '.           |  |   '.      |
    ;;          |  +-----+          |  +-----+     |
    ;;          O-------------      5--------------4
    ;;                   x
    ;;
    ;;offset: default: undef, num, or [a,b]
    ;;     
    ;;  If undef, template is centered
    ;;  If num, [num,num] 
    ;;  If [a,b], then: 
    ;;        
    ;;  If template is a num or [x,y], then the square/rectangular 
    ;;     is placed with [a,b] as the low-bottom pt on the target face:       
    ;;               
    ;;               6---------------7
    ;;               |               |
    ;;               |   +-----+     |
    ;;               |   |     |     |
    ;;            -- + - +-----+     |
    ;;             b |   :           |
    ;;            -- 5---+-----------4
    ;;               | a |
    ;;               
    ;;            if template is a list of pts, then the template be placed
    ;;            by adding offset to them:
    ;;     
    ;;             y |                 6--------------+
    ;;               |                 |              |
    ;;               |   +.            |      +.      |
    ;;               |   | '.    ===>  |      | '.    |
    ;;               |   |   '.        |      |   '.  |
    ;;               |   +-----+       |      +-----+ |
    ;;               O-----------      5--------------4
    ;;               | x |             | x+ a | 
    ;;                                 offset = [a,0] 
    ;;     
    ;;       if [a,undef] ==> auto centered on y side
    ;;       if [undef,b] ==> auto centered on x side
    ;;       
    ;;    depth: 
    ;;    
    ;;       How deep down. If not given, cut through. 
    ;;       It could be a number d or a single-item array [d]
    ;;       
    ;;       When [d], d is treated as a ratio
    ;;       When d <0, pops up instead of carve in.
    ;;        
    ;;       
    ;;    To do:
    ;;     
    ;;    dirline= p2 = a pair of pts showing the direction of cutting, 
    ;;           like [ pts[5], pts[1] ]
    ;;           If not given, cut alone the normal line of the dest
    ;;    taper 
    "];
        
    function cavePF_test(mode=12,opt=[])= 
    (
       let( pts = [[3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1]
                  , [0, 0, 1], [0, 3, 1], [3, 3, 1]]
          , faces= rodfaces(4)        
          )
       doctest( cavePF,
         [
            "var pts"
           , "var faces" 
           , [ cavePF(pts, faces, 1)
             , [ [ [3, 0, 0], [0, 0, 0], [0, 3, 0], [3, 3, 0], [3, 0, 1], [0, 0, 1]
                 , [0, 3, 1], [3, 3, 1], [2, 1, 1], [2, 2, 1], [1, 2, 1], [1, 1, 1]
                 , [2, 1, 0], [2, 2, 0], [1, 2, 0], [1, 1, 0]
                 ]
               , [ [0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7], [8, 11, 15, 12]
                 , [9, 8, 12, 13], [10, 9, 13, 14], [11, 10, 14, 15]
                 , [4, 5, 6, 7, 4, 8, 9, 10, 11, 8]
                 , [3, 2, 1, 0, 3, 15, 14, 13, 12, 15]
                 ] 
               ]
             , "pts,faces,1"  
             ]
         ]  
         , mode=mode, opt=opt, scope=["pts",pts,"faces",faces]          
       ) 
    );
    

function cavePF2( pts,faces
               , template=2
               , dest=[4,5,6]
               , offset=undef
               , depth 
               )=
(
  /* Note on 18.5.25: decoPolyEdge would do a more efficient job.
  
     Same as cavePF, but allow oversized template. This will take 
     some code so we make it into a version 2 and leave the original intact. 
     --- The oversize-checking code only aims for cubic template.
     --- Only when depth is defined
     (2018.5.8)
  
     Carve a hole (defined by a 2d template) on the dest of an obj 
     defined by pts and faces.
      
     Return [newpts, newfaces]
     
     template: a collection of 2D pts arranged CCW. Could be defined as:
     
       (1) number: Size of a square
       (2) [x,y]: Size of a rectangular hole. 
       (3) 2D pts: An array of 2d pts on XY plane (counter CW)
                   
     dest: [i,j,k] where i,j,k are indices on a face, like [4,5,6]
           or an int serves as a fi (face index) such that the 
           first 3 indices is treated as the dest 
           
                   6-----------7
                .'          .' |
              .'          .'   |
             5----------4'     |
             |          |      3
             |          |    .'
             |          | .'
             1----------0'      
           
           In this case (dest= [4,5,6]), the ORIGIN of template will be 
           anchored on the pts[5]:
     
             y |                   6--------------+
               |                   |              |
               |  +.               |  +.          |
               |  | '.       ===>  |  | '.        |
               |  |   '.           |  |   '.      |
               |  +-----+          |  +-----+     |
               O-------------      5--------------4
                         x

     offset: default: undef, num, or [a,b]
     
        If undef, template is centered
        If num, [num,num] 
        if [a,b], then: 
        
            if template is a num or [x,y], then the square/rectangular 
            is placed with [a,b] as the low-bottom pt on the target face:       
               
               6---------------7
               |               |
               |   +-----+     |
               |   |     |     |
            -- + - +-----+     |
             b |   :           |
            -- 5---+-----------4
               | a |
               
            if template is a list of pts, then the template be placed
            by adding offset to them:
     
             y |                 6--------------+
               |                 |              |
               |   +.            |      +.      |
               |   | '.    ===>  |      | '.    |
               |   |   '.        |      |   '.  |
               |   +-----+       |      +-----+ |
               O-----------      5--------------4
               | x |             | x+ a | 
                                 offset = [a,0] 
     
       if [a,undef] ==> auto centered on y side
       if [undef,b] ==> auto centered on x side
       
    depth: 
    
       How deep down. If not given, cut through. 
       It could be a number d or a single-item array [d]
       
       When [d], d is treated as a ratio
       When d <0, pops up instead of carve in.
        
       
    To do:
     
    dirline= p2 = a pair of pts showing the direction of cutting, 
           like [ pts[5], pts[1] ]
           If not given, cut alone the normal line of the dest
    taper 
           
        
   */ 
   /*  let target site= [P,Q,R ] 
   
            R---------S
          .'        .'|
         Q--------P'  |
         |        |   3
         |        | .'
         1--------0'      
   */
   let( _dest=isint(dest)? 
                get(faces[dest],[0,1,2]) 
              : dest
      , Pi=_dest[0], Qi=_dest[1], Ri=_dest[2]  
      , P = pts[Pi], Q = pts[Qi], R = pts[Ri]
      , S = P-Q+R          
      , C = (P+Q+R+S)/4
      , L = len(pts)
      , dPQ = dist(P,Q)
      , dQR = dist(Q,R)
      , offset= isnum(offset)?[offset,offset]:offset
      , tmp=  isnum(template)?
              let( offx= offset? und(offset[0], (dPQ-template)/2 ) 
                               : (dPQ-template)/2
                 , offy= offset? und( offset[1], (dQR-template)/2 ) 
                                : (dQR-template)/2 )  
              [ [ offx+template, offy,0]
              , [ offx+template, offy+template,0]
              , [ offx, offy+template,0]
              , [ offx, offy,0]
              ]
           : len(template)==2?
                let( offx= offset==undef||offset[0]==undef?(dPQ-template[0])/2:offset[0]
                   , offy= offset==undef||offset[1]==undef?(dQR-template[1])/2:offset[1]  
                   )
                //echo("--- len template =2: ", dPQ=dPQ, dQR=dQR, offx=offx, offy=offy )     
              [ [ offx+template[0], offy,0]
              , [ offx+template[0], offy+template[1],0]
              , [ offx, offy+template[1],0]
              , [ offx, offy,0]
              ]
           : to3d( offset==[0,0]? template
                   : offset? [for(p= template) p+offset ]
                   : // centering when offset==undef
                     let( tmp_C= sum(template)/len(template) 
                        , offxy= [dPQ/2,dQR/2]- tmp_C
                        )
                     [for(p=template) p+offxy ]
                 )   
      , topHolePts = movePts( tmp
                            , from= ORIGIN_SITE
                            , to= [P,Q,R] 
                            )
                            
      , hEdgefis = get_edgefis(faces)
                     // h{edge:hFis}:   
                     // [ [2,3], ["L",3,"R",6,"F",[4], "B",[0,1]] 
                     // , ...
                     // ]
      , fi = hash(edgefis_get( hEdgefis, [Pi,Qi] ), "R" )
      , Si = next(faces[fi], Ri)            
      , nearfi= hash(edgefis_get( hEdgefis, [Pi,Qi] ), "L" )
      , normalPi = prev( faces[nearfi], Qi  )
      , oppofi = get_near_fi(faces, nearfi
                            , dels( faces[nearfi], _dest) 
                            )
      , normaldist = dist( Q, pts[normalPi] )  
      
      , _depth = depth==undef || abs(depth)>= normaldist ? undef
                 : len(depth)==1? // :When depth is [d], use d as ratio 
                   ( let(d= depth[0])
                     d>=1? undef // When >1, treated as no depth defined
                     : normaldist* d
                   )
                 : depth 
                 
      , botHolePts= /// NOTE: this is for cube only. Other shapes will likely fail 
                    _depth==undef?
                     [ for(p=topHolePts) 
                      p+ pts[ normalPi ]- Q
                     ]
                    : [ for(p=topHolePts) 
                        p+ onlinePt( [Q,pts[normalPi]], len=_depth )-Q
                      ]   
      , newPts   = concat( topHolePts, botHolePts)
      , finalPts = concat( pts, newPts )    
      
      //===================== checking oversize  
      
      , Pi0 = closestPi(topHolePts, P) // Index of the pt on tmp closest to P 
      , isP0OnSide= isonline( topHolePts[Pi0], [P,Q] ) 
      , Pi1 = next(range( topHolePts), Pi0)  
      , P1 = topHolePts[Pi1]
      , isP1OnSide = isonline( P1, [R,S] ) 
      , oppoNearfi = get_near_fi(faces, fi
                            , dels(faces[fi], [Pi,Qi])
                            )
      , oldfis = dels( range(faces)
                     , _depth==undef? [fi,oppofi]
                       : concat( fi
                               , isP0OnSide?nearfi:[]
                               , isP1OnSide?oppoNearfi:[] 
                               ) 
                     )                   
      , newsidefaces= concat(
                         [ for(i=range(tmp))
                            if (!( (i==Pi0 && isP0OnSide)
                                || (i==next(range(tmp),Pi1) && isP1OnSide)
                               )) 
                            [ L+ i
                            , L+ (i==0? (len(tmp)-1): i-1)
                            , L+ (i==0? (len(tmp)-1): i-1)+len(tmp) 
                            , L+ i+ len(tmp)
                            ]
                         ]    
                      , [ isP0OnSide?  
                          [ each for(pi= faces[nearfi])
                             pi== Qi? [ Qi
                                      , L+3, L+7
                                      , L+4, L                                            
                                      ]
                                    : [pi]       
                          ]
                          :faces[nearfi]
                        ]              
                      , [ isP1OnSide?
                          [ each for(pi= faces[oppoNearfi])
                             //    echo(pi=pi) 
                             pi== Si? 
                                  [ Si
                                  , L+1, L+5
                                  , L+6, L+2
                                  ]
                                : [pi]       
                          ]
                          :faces[oppoNearfi]
                        ]              
                      )
      , topfaces = isP0OnSide?
                   (
                     isP1OnSide?
                     [ 
                       [ Pi, Pi+len(tmp), Pi+len(tmp)+1, L-1]    
                     , [ Qi, Ri, Pi+len(tmp)+2,Pi+len(tmp)+3]  
                     ] 
                     :[[Pi, Pi+len(tmp), Pi+len(tmp)+1, Pi+len(tmp)+2, Pi+len(tmp)+3,
                      , Qi, Ri, Si  
                      ]]
                   )
                   : isP1OnSide?
                     [[Pi,Qi,Ri, L+2, L+3, L, L+1, Si ]                       
                     ]  
                   : [concat( faces[fi], faces[fi][0], range(8,8+len(tmp)), 8 )]
      
      , botfaces = _depth==undef? 
                    concat( faces[oppofi], faces[oppofi][0]
                          , range( len(finalPts)-1
                                 , len(finalPts)-1-len(tmp)
                                 )
                          , len(finalPts)-1 
                          )
                   : range( len(finalPts)-1
                          , len(finalPts)-1-len(tmp)
                          ) 
                           
      , finalfaces= concat( get(faces, oldfis) 
                          , newsidefaces
                          , topfaces 
                          , [botfaces]   
                          )                        
      )  
//   echo("--------------------------")
//   //echo(pts=pts)
//   //echo(faces=faces)   
//   echo(template=template)
//   echo(offset=offset)
//   echo(tmp=tmp)
//   //echo(dest=dest)
//   echo(_dest=_dest)
//   //echo(C=C)
//   //echo(P=P,Q=Q,R=R,S=S,C=C)
//   echo( topHolePts = topHolePts )
//   //echo(hClockedEFs = hClockedEFs)
//   //echo(hEdgefis=hEdgefis)
//   //echo(edges_in_hEdgefis = keys(hEdgefis))
//   //echo(normalPi = normalPi)
//   //echo(normaldist=normaldist)
//   //echo(depth=depth)
//   //echo(_depth=_depth)
//   //echo( botHolePts = botHolePts ) 
//   //echo(fi=fi, nearfi=nearfi, oppofi = oppofi )
//   echo( Pi0=Pi0, Pi1=Pi1)
//   echo(isP0OnSide=isP0OnSide)
//   echo(P1 = P1)
//   echo(isP1OnSide=isP1OnSide)
//   echo(oppoNearfi=oppoNearfi)
//   //echo(oldfis=oldfis)
//   //echo(oldfaces=oldfaces)
//   echo(newsidefaces = newsidefaces)
//   echo(topfaces= topfaces)
//   echo(finalfaces=finalfaces)
   [finalPts, finalfaces]
);


//========================================================
{ // centerPt
function centerPt(pqr)=
(
    iscoline( pqr )? undef
    : let( m1 = midPt( p01(pqr) )
         , m2 = midPt( p12(pqr) )
         , c1= anglePt( [ pqr[0], m1, pqr[2] ], a=90 )
         , c2= anglePt( [ pqr[2], m2, pqr[0] ], a=90 )
         )
    intsec( [m1,c1], [m2,c2] )
);

    centerPt= ["centerPt", "pqr", "pt", "Geometry",
    " Given pqr, return a point that is same distance from pts in pqr"
    ];
    function centerPt_test( mode=MODE, opt=[] )=
    (
        doctest( centerPt, [], mode=mode, opt=opt )
    );   
} // centerPt

//========================================================
{ // centroidPt
    
function centroidPt(pts)=
    sum(pts)/len(pts); // 2018.8.2 
   //let( P=pqr[0], Q=pqr[1], R=pqr[2] )
   //intsec( [P, midPt(Q,R)], [Q, midPt(P,R)] );  

    centroidPt=["centroidPt","pts","Point","Point"
    , "Return the centroid pt of pqr (lines of pt-to-midPt meet).
    ;;
    ;; ref: http://en.wikipedia.org/wiki/Centroid
    "];

    function centroidPt_test( mode=MODE, opt=[] )=
    ( 
        doctest( centroidPt,
        [
        ], mode=mode, opt=opt )
    ); 
} // centroidPt


{ // chainCorePts 
// 2018.10.14:
// chainCorePts: new/ under construct: pts representing the trace of a chain
//               (stripped down from randchain / chainBoneData)
// randchain = chainBoneData : a hash 
// simpleChainData: a stripped down of randchain but still a hash
// chaindata: : pts representing the wrapping pts alone the
//              trace of chain (i.e., chainCorePts)
// Chain: draw chaindata 

function chainCorePts( 
       n =3, 
     , a //=[-360,360]
     , a2 //=[-360,360]
     , seglen //= [0.5,3] // rand len btw two values
     , lens //=undef
     , seed //=undef // base pqr
     , opt=[]
     , _rtn=[] // for noUturn
     ,_i=0 // for noUturn
     ,_debug
    )
= // ["pts", [...], "unsmoothed", [...], "smooth", undef, "seed", p3, "lens", undef
  //, "seglen", undef, "L", 2.46809, "a", [170, 190], "a2", [-45, 45], "debug", undef]
    
(
//  !n? _red(str( "ERROR: the n(# of pts) given to chainCorePts() is invalid: ",n ))
//  :(
    //echo("chainCorePts()")
    _i==0? //####################################### i==0
    
    let( 
        //=========[ initiation ] =======

          opt = popt(opt) // handle sopt
        //, n = hash(opt,"n", n)
        , n = n>1?n: hash(opt,"n",3)
        , a = und(a, hash(opt,"a", [170,190])) // = aPQR
        , a2= und(a2,hash(opt,"a2",[-45,45])) // = aRQN
        , seglen= or(seglen, hash(opt, "seglen"))
        , _lens = or(lens, hash(opt, "lens"))
        , seed = or(seed, hash(opt, "seed", randPts(3)))
       // , smooth= und(smooth, hash(opt, "smooth"))

        //==============================

        ,lens = // If lens < n-1, repeat lens        
           _lens && isarr(_lens) && len(_lens)< n-1?
             repeat(_lens, ceil((n-1)/len(_lens)))
            : _lens
            
        , pt= seed? seed[0]:randPt() //d,x,y,z)
        , newrtn = [pt]  
              
     )//let @_i==0
     
     chainCorePts(n=n,a=a,a2=a2,seglen=seglen, lens=lens
              , seed = seed
              //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1
              ,_debug=_debug)   
              
   :let(  // ###################################### i > 0
   
     isend= _i==n-1
     ,_a = isarr(a)? rand(a[0],a[1]):a
     ,_a2= isarr(a2)? rand(a2[0],a2[1]):a2
     ,L = isarr(lens)? lens[_i-1]
          : isarr(seglen)? rand(seglen[0],seglen[1]) 
          : isnum(seglen)? seglen
          : rand(0.5,3)
     ,pt= _i==1? (seed? onlinePt( p01(seed), len=L)
                      :onlinePt( [last(_rtn)
                               , randPt()],len=L)
                  )    
          :  let( P = get(_rtn,-2)
                  , Q = get(_rtn,-1)
                  , Rf = _i==2 && seed? seed[2] //onlinePt( p12(seed), len=L)
                         : iscoline( get(_rtn, [-3,-2,-1]))
                            ?randOfflinePt( get(_rtn,[-2,-1]))
                           : get(_rtn,-3)
                  )
               anglePt( [P,Q,Rf], a=_a, a2=_a2, len=L)   
                       
     ,newrtn = app(_rtn, pt ) 
          
//     ,_debug= str(_debug, "<br/>"
//                 ,"<br/>, <b>_i</b>= ", _i
//                 ,"<br/>, <b>len</b>= ", len
//                 ,"<br/>, <b>L</b>= ", L
//                 ,"<br/>, <b>_rtn</b>= ", _rtn
//                 ,"<br/>, <b>newrtn</b>= ", newrtn
//                 ,"<br/>, <b>actual len</b>= ", dist( get(newrtn,[-1,-2]))
//                 ,"<br/>, <b>pt</b>= ", pt
//                 )
     )//let

  //isend? (smooth? [newrtn,smpts]: newrtn ) 
  isend? 
    [ "pts", pts
    , "unsmoothed", newrtn
    , "seed", seed
    , "lens",lens
    , "seglen", seglen
    , "L",L
    , "a",a
    , "a2", a2
    , "debug",_debug
    ]
    // (smooth? [newrtn,smpts]: newrtn ) 
  
  : chainCorePts(n=n,a=a,a2=a2,seglen=seglen, lens=lens, seed = seed
             //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1, _debug=_debug)
  //)// if_n
);

} // chainCorePts
     
      
    chainCorePts=["chainCorePts", " n,a,a2,seglen,lens,seed,smooth,opt", "hash","Rand"
    , "Generate a hash for random chain, containing the following keys:
    ;; 
    ;;  'pts'
    ;;  'unsmoothed'
    ;;  'smooth'
    ;;  'seed'
    ;;  'lens'
    ;;  'seglen'
    ;;  'L'  // like 2.46809
    ;;   'a' // like [170, 190]
    ;;   'a2' // like [-45, 45]
    ;;   'debug'
    ;;
    "];

    function chainCorePts_test( mode=MODE, opt=[] )=
    (
        doctest(chainCorePts
        , [
            [chainCorePts(),"","chainCorePts()", ["notest",1] ]
          ]
        , mode=mode, opt=opt 
        )
    );


// Should have named this circumCenterPt
function circumCenterPt(pqr, debug=0)= // center of circle made by pqr
(                                    // 2018.6.5. demoed via circlePtsOnPQR   
   let( N = N(pqr)
      , P=pqr[0], Q=pqr[1], R=pqr[2]
      , midln_pq = [ (P+Q)/2, N( [N,(P+Q)/2,Q] ) ]
      , midln_qr = [ (R+Q)/2, N( [N,(R+Q)/2,Q] ) ]
      , C = intsec( midln_pq, midln_qr ) // circle center
      )
   debug?
     echo("Debugging circumCenterPt...")
     echo(pqr=pqr)
     echo(N=N)
     echo(midln_pq = midln_pq)
     echo(midln_qr = midln_qr)
     echo(C=C)
     C
   : C  
        
);

     circumCenterPt=[ "circumCenterPt", "pqr", "pts"
                 , "Points, Geometry",
     " center of a triangle's circumcircle. It can be found as 
     ;; the intersection of the perpendicular bisectors.
    "];

    function circumCenterPt_test( mode=MODE, opt=[] )=
    (
        doctest( circumCenterPt, 
        [ 
          [ circumCenterPt( pqr = [[1.12, 1.78, -1.31], [-0.21, -0.18, 0.62], [-0.79, -1.29, 0.29]] )  
          ,   [0.0026389, -0.326895, -1.80114]
          ,  [[1.12, 1.78, -1.31], [-0.21, -0.18, 0.62], [-0.79, -1.29, 0.29]]
          , ["asstr",1]    
          ]  

        ]
        , mode=mode, opt=opt
        )
        
    
    );

//========================================================
{ // circlePts
/*
 2018.8.6: Change circlePts signature from :
 
    ;;                N
    ;;            .:'`|`':.
    ;;          :`    |    `:
    ;;       __:_____Q|______:____B
    ;;         :     /|`.    :
    ;;          '_  / |  `._'
    ;;            `/:_|_:-'`. 
    ;;            /          R
    ;;           P
    ;; 
 
to:

    ;;                N
    ;;            .:'`|`':.
    ;;          :`    |    `:
    ;;       __:_____Q|______:____B
    ;;         :      |`.    :
    ;;          '_    |  `._'
    ;;            `.._|_:-'`. 
    ;;                |      R
    ;;                P
    ;;
     
making it more consistent with that of anglePt and arcPts. 
Below are where the circlePts are used in the entire lib. 
Need to update them one by one.   

   old: circlePts( pqr )
   new: circlePts( [B(pqr), pqr[1], N(pqr) ] )
     
| Matches:
| dev.scad:
|   #9:     sq= circlePts( pqr, rad=4, n=n1);
|   #12:     six= circlePts( pqr, rad=2, n=n2);
|   #114:     pl1= circlePts( pqr, rad=4, n= int(rand(3,15)));
|   #115:     pl2= circlePts( pqr, rad=1.5, n=int(rand(3,15)));
| scadx_geometry.scad:
|   #1610: function circlePts( pqr, rad, a, n=8, ratio)=
|   #1612:    //echo(log_b("circlePts()",2))
|   #1623:     circlePts=["circlePts", "pqr,rad,n", "pts","geometry"
|   #1642:     function circlePts_test( mode=MODE, opt=[] )=
|   #1644:         doctest( circlePts, [], mode=mode, opt=opt )
|   #1646: //function circlePts( pqr, rad, n=8)=
|   #1648: //   //echo(log_b("circlePts()",2))
|   #1653: //    circlePts=["circlePts", "pqr,rad,n", "pts","geometry"
|   #1672: //    function circlePts_test( mode=MODE, opt=[] )=
|   #1674: //        doctest( circlePts, [], mode=mode, opt=opt )
|   #1676: } // circlePts
| x #1678: function circlePtsOnPQR(pqr,n=8, debug=0)=
| x #1682:    circlePts( [ N([pqr[0],C,pqr[1]]), C, pqr[1]]
| x #7519:                       :circlePts( pqr=[ [0,0,-_h*2], [0,0,0],[1,0,0]], rad=_d1/2, n= $fn )
| x #7521:                       :circlePts( pqr=[ [0,0,-_h*2], [0,0,_h],[1,0,_h]], rad=_d2/2, n= $fn )
| x #12212:              : circlePts( [preP, pts[0], Rf], n=nside
|   #14481:     , circlePts_test( mode, opt )
| scadx_obj.scad:
| x #276:              : circlePts( [preP, pts[0], Rf], n=nside
| x #557:                      , pts2 = circlePts(
| x #1387:       pts_cut_0 = circlePts( [2*Q-P,P,Rf], rad=r, n=$fn);
| x #1390:       pts_cut_1 = circlePts( [2*Q-P,headBaseCenter,Rf], rad=r, n=$fn);
| x #1391:       pts_cut_2 = circlePts( [2*Q-P,headBaseCenter,Rf], rad= hash(Head,"r"), n=$fn);
| x #2148:              : circlePts( [preP, pts[0], Rf], n=nside
| x #2429 :                      , pts2 = circlePts(
| scadx_ref.scad:
|   #2199: ,["20150724-1", "Branch loft: new: circlePts(), centerPt(), getPairingData_ang_seg()"]

*/
function circlePts( pqr, rad, a, n=8, ratio=1)=
(
   //echo(log_b("circlePts()",2))
   //echo(log_(["pqr",pqr],2))
   //echo(log_e("",2))
   let(pqr = pqr?pqr:pqr())
   a?
   arcPts( [ anglePt( pqr, a=a )
           , pqr[1],N(pqr)
           ]
         , a= 360, n=n, rad=rad, ratio=ratio)
   //:arcPts( pqr, rad=rad, a= 360/n*(n-1), n=n, ratio=ratio)
   :arcPts( pqr, a= 360, n=n, rad=rad, ratio=ratio)
);
    circlePts=["circlePts", "pqr,rad,n", "pts","geometry"
    ,"Return n points as a circle that has normal pointing to Q=>P.
    ;;
    ;; The circle pts are on the MQN plane as shown below, where
    ;; N is normal pt of PQR (or PQM), M is normal pt of NQP:
    ;; 
    ;;                N
    ;;            .:'`|`':.
    ;;          :`    |    `:
    ;;       __:_____Q|______:____M
    ;;         :     /|`.    :
    ;;          '_  / |  `._'
    ;;            `/:_|_:-'`. 
    ;;            /          R
    ;;           P
    ;;
    ;; 
    "];
        
    function circlePts_test( mode=MODE, opt=[] )=
    (
        doctest( circlePts, [], mode=mode, opt=opt )
    );   
//function circlePts( pqr, rad, n=8)=
//(
//   //echo(log_b("circlePts()",2))
//   //echo(log_(["pqr",pqr],2))
//   //echo(log_e("",2))
//   arcPts( pqr?pqr:pqr(), rad=rad, a=90, a2= 360/n*(n-1), n=n)
//);
//    circlePts=["circlePts", "pqr,rad,n", "pts","geometry"
//    ,"Return n points as a circle that has normal pointing to Q=>P.
//    ;;
//    ;; The circle pts are on the MQN plane as shown below, where
//    ;; N is normal pt of PQR (or PQM), M is normal pt of NQP:
//    ;; 
//    ;;                N
//    ;;            .:'`|`':.
//    ;;          :`    |    `:
//    ;;       __:_____Q|______:____M
//    ;;         :     /|`.    :
//    ;;          '_  / |  `._'
//    ;;            `/:_|_:-'`. 
//    ;;            /          R
//    ;;           P
//    ;;
//    ;; 
//    "];
//        
//    function circlePts_test( mode=MODE, opt=[] )=
//    (
//        doctest( circlePts, [], mode=mode, opt=opt )
//    );   
} // circlePts  

function circlePtsOnPQR(pqr,n=8)=
(  
   let( C= circumCenterPt(pqr) )
   echo(pqr=pqr, C=C)
   circlePts( [ pqr[0], C, pqr[1]] // change to use new version of circlePts 18.8.6
             //[ N([pqr[0],C,pqr[1]]), C, pqr[1]]
            , rad = abs(dist(C,pqr[0])), n=n
            )
);

     circlePtsOnPQR=[ "circlePtsOnPQR", "pqr,n=8", "pts"
                 , "Points, Geometry",
     " pts of a circle made out of pqr
    "];

    function circlePtsOnPQR_test( mode=MODE, opt=[] )=
    (
        doctest( circlePtsOnPQR, 
        [ 
          [ circlePtsOnPQR(pqr= [[-1.02, -0.3, -0.99], [-1.15, -0.96, 0.73], [0.6, 0.49, 1.5]]
                        )  
          ,  [ [-1.02, -0.3, -0.99], [-1.31456, -0.887702, -0.0138159]
             , [-0.947507, -0.880869, 1.10436], [-0.133852, -0.283502, 1.70952]
             , [0.649775, 0.554468, 1.44716], [0.944336, 1.14217, 0.470978]
             , [0.577282, 1.13534, -0.647199], [-0.236373, 0.53797, -1.25236]
             ]
          , "[[-1.02, -0.3, -0.99], [-1.15, -0.96, 0.73], [0.6, 0.49, 1.5]]"
          , ["asstr",1]    
          ]  

        ]
        , mode=mode, opt=opt
        )
        
    
    );
    
    
    

//========================================================
{ // cirfrags 
function cirfrags(r, fn, fs=0.01, fa=12)=
( // from: get_fragments_from_r
  // http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn
    fn? max(fn,3)
	: max(min(360 / fa, r*2*PI / fs), 5)
);

    cirfrags=[ "cirfrags", "r,fn,fs=0.01,fa=12", "number", "Object, Geometry",
     " Given a radius (r) and optional fn, fs, fa, return the number 
    ;; of fragments (NoF) in a circle. Similar to $fn,$fs,$fa but
    ;; this function (1) doesn't put a limit to r and (2) could return
    ;; a float. In reality divising a circle shouldn't have given partial
    ;; fragments (like 5.6 NoF/cicle). But we might need to calc the 
    ;; frags for an arc (part of a circle) so it'd better not to round it
    ;; off at circle level. 
    ;; fa: min angle for a fragment. Default 12 (30 NoF/cycle). 
    ;; fs: min frag size. W/ this, small circles have smaller 
    ;; NoFs than specified using fa. Default=0.01 (Note: built-in $fs 
    ;; has default=2). 
    ;; fn: set NoF directly. Setting it disables fs & fa. 
    ;;
    ;; When using fs/fa for a circle, min NoF = 5. 
    ;;
    ;; See: http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn
    "];

    function cirfrags_test( mode=MODE, opt=[] )=
    (
        doctest( cirfrags, 
        [ 
            "// Without any frag args, the default frag is 30 (no matter r is):"
            ,[cirfrags(6),30, "r=6",    30]
            ,[cirfrags(3),30, "r=3",   30]
            ,[cirfrags(0.1),30, "r=0.1", 30]

            ,""
            ,"// fn (num of frags) - set NoF directly. Independent of r, too:"
            ,""
            
            ,[cirfrags(3,fn=10),10,"r=3,fn=10"]
            ,[cirfrags(3,fn=3),3 , "r=3,fn=3"]
            ,[cirfrags(0.05, fn=12),12, "r=0.05, fn=12"]

            ,""
            ,"// fs (frag size)"
            ,""
            ,[cirfrags(1,fs=1), 2*PI*1/1, "r=1,fs=1"]
            ,[cirfrags(2,fs=1), 2*PI*2/1, "r=2,fs=1"]
            ,[cirfrags(3,fs=1), 2*PI*3/1, "r=3,fs=1"]
            //,[cirfrags(6,fs=1), 2*PI*6/1, "r=6,fs=1"]
    //		
    //		,[cirfrags(1,fs=1),6.28319-4.69282*1e-6, "r=1,fs=1"]
    //		,[cirfrags(2,fs=1),6.28319-4.69282*1e-6, "r=2,fs=1"]
    //		,[cirfrags(1,fs=1),6.28319,"r=1,fs=1", ["asstr",true, "prec",6,"//","prec=6"]]
    //		,[cirfrags(1,fs=1),6.28319,"r=1,fs=1", ["notest",true, "prec",6,"//","prec=6"]]
    //		,[ cirfrags(1,fs=1),6.28319,"r=1,fs=1", ["prec",5,"//","prec=5"]]
    //		,[cirfrags(1,fs=1),6.28319,"r=1,fs=1",  ["prec",4,"//","prec=4"]]
    //		,[cirfrags(3,fs=1),18.8496,"r=3,fs=1",  ["asstr",true]]
    //		,[cirfrags(6,fs=1),20,"r=6,fs=1"]

    //		,[cirfrags(6,fs=2),18.8496,"r=6,fs=2", ["asstr",true]]
    //		,[cirfrags(12,fs=4),18.8496,"r=12,fs=4", ["asstr",true]]
    //		,[cirfrags(3,fs=10),5,"r=3,fs=10"]
    //		,[cirfrags(6,fs=100),5,"r=6,fs=100", ["//","min=5 using fa/fs"] ]

            ,""
            ,"// fa (frag angle)"
            ,""
            ,[cirfrags(3,fa=10),36,"r=3,fa=10",  36]
            ,[cirfrags(6,fa=100),5,"r=6,fa=100",  5,["rem","min=5 using fa/fs"] ]
            ,""

        ]
        , mode=mode, opt=opt
        )
    );
} // cirfrags


function cirIntsecPts(pts, n=16, iskeep=1, C
                     , _N, _cpts, _as, _rtn, _i=0)= // 2018.8.3 
( /* 

          |
          |    / _.Q     _-`          _.-'`
      R_.-H-''/G`   \ _-`        _.-'`
          |  /     _-F      _.-'`                
          | /   _-`   \E.-'`           
          |/ _-` _.-'` \      
          /_`.-'`_______\ ______________
         C               P
  
     For a polygon or a list of pts, [P,Q,R ...],
     starting from line PC, circle through entire list, w/ 
     angle increment of 360/n (in this case, 22.5), 
     find and return the intsec E,E,G,H .... 
   
     IF C not given, use centroidPt(gravity) of pts 
       
     Ref: getPairingData
  */
  
  _i==0?
    //echo( log_b( "cirIntsecPts(), i=0",2) )
    //echo( log_b( "cirIntsecPts(), i=0",3) )
    //echo( log_(["pts",pts], 3 ))
    let( C= C!=undef?C:sum(pts)/len(pts))
//    let( C = C?C:sum(pts)/len(pts)
//       , _N = N( [pts[0],C,pts[1]])
//       //, _as = accum( [for(i=range(n)) i*360/n] ) 
//       , _cpts= circlePts(  pqr=[pts[0], C, pts[1]], n=n )  
//       , _ci =0
//       , _rtn=[] //iskeep?[ pts[0] ]:[]  
//       )
    //echo( log_( ["n",n,"C",C], 3 ))
    //echo( log_( ["_N", _N], 3 ))
    //echo( log_( ["_cpts", _cpts], 3 ))
    //echo( log_e( "", 3 ))
    //cirIntsecPts(pts=pts, n=n
                //, C=C, iskeep=iskeep
                //, _N=_N, _cpts=_cpts, _ci=_ci, _rtn=_rtn, _i=1)
    cirIntsecPts(pts=pts, n=n
                , C= C==undef?C:sum(pts)/len(pts)
                , iskeep=iskeep
                , _N= N( [pts[0],C,pts[1]])
                , _cpts= circlePts(  pqr=[pts[0], C, pts[1]], n=n )
                , _ci=0
                , _rtn=[]
                , _i=1)
   
  //============================================================

  :_i< len(pts)+1? 
 
    //echo( log_b( _blue(str("_i=",_i)), 3 ))
    let( _a0 = angle( [pts[0],C,pts[_i==len(pts)?0:_i]], Rf=_N)
       , _a = is0(_a0)?360:_a0<0? (360+_a0): _a0
       , _this_ci = floor(_a/(360/n))
       , seg= [pts[_i-1], pts[_i==len(pts)?0:_i]]
       , ipts= [ for(ci= [_ci+1:_this_ci]) 
                 let(ci=ci==n?0:ci)
                 //echo(log_b( str("Getting intsecs, ci= ",ci), 4)) 
                 //echo(log_( ["seg", seg], 4))
                 //echo(log_( ["cpts[ci]", _cpts[ci]], 4))
                 //echo(log_e("", 4)) 
                  intsec( seg, [C, _cpts[ci]], debug=0 ) 
               ]
       , _rtn = concat(_rtn, ipts
                , _i==len(pts)?[]:iskeep?[pts[_i]]:[])
       )
    //echo( log_( ["_ci",_ci, "_this_ci", _this_ci], 3 ))
    //echo( log_( ["_a",_a], 3 ))
    //echo( log_( ["C",C], 3 ))
    //echo( log_( ["seg",seg], 3 ))
    //echo( log_( ["_rtn",_rtn], 3 ))
    //echo( log_e( "", 3 ))
    cirIntsecPts(pts=pts, n=n, C=C, iskeep=iskeep
                , _N=_N, _cpts=_cpts, _ci=_this_ci, _rtn=_rtn, _i=_i+1)   
    
  //============================================================
  : 
    //echo( log_b( "cirIntsecPts() -- existing", 3 ))
    //echo( log_(["_rtn",_rtn],3 ) )
    //echo( log_e( "", 3 ))
    //echo( log_e( "", 2 ))
    _rtn
    
);

    cirIntsecPts=[ "cirIntsecPts", "pts,n=16,iskeep=1,C", "number"
                 , "Object, Geometry",
     " 
     ;;       |
     ;;       |    / _.Q     _-`          _.-'`
     ;;   R_.-H-''/G`   \ _-`        _.-'`
     ;;       |  /     _-F      _.-'`                
     ;;       | /   _-`   \E.-'`           
     ;;       |/ _-` _.-'` \      
     ;;       /_`.-'`_______\ ______________
     ;;     C               P
     ;;
     ;; For a polygon or a list of pts, [P,Q,R ...],
     ;; starting from line PC, circle through entire list, w/ 
     ;; angle increment of 360/n (in this case, 22.5), 
     ;; find and return the intsec E,E,G,H .... 
     ;; 
     ;; IF C not given, use centroidPt(gravity) of pts 
     ;;   
     ;; Ref: getPairingData
    "];

    function cirIntsecPts_test( mode=MODE, opt=[] )=
    (
        doctest( cirIntsecPts, 
        [ 
          [ cirIntsecPts(pts=[[1.1,1.53,0],[-1.6,1,0],[0.77,-1.72,0],[1.36,1.3,0]]
                        , n=5)  
          , [ [-0.167935, 1.28111, 0], [-1.6, 1, 0], [-0.814182, 0.0981327, 0]
            , [0.452345, -1.35543, 0], [0.77, -1.72, 0], [1.16487, 0.301204, 0]
            , [1.36, 1.3, 0], [1.1, 1.53, 0]
            ]
          , "[[1.1,1.53,0],[-1.6,1,0],[0.77,-1.72,0],[1.36,1.3,0]], n=5"
          , ["asstr",1]    
          ]  

        ]
        , mode=mode, opt=opt
        )
        
    
    );


//=========================================
{ // convPolyPts
function convPolyPts( arr, to="outin")=
(
  to=="outin"?
    switch( transpose( arr ), 2,3)
  : transpose( switch(arr,2,3))
);

    convPolyPts=["convPolyPts","arr,to","arr","Geom",
    " [2018.11.1] Not really understand this, and there seems to be no
    ;; use of this anywhere.
    ;;
    ;; For tube-like shapes: convert pts between out-in order and xsec order.
    ;;
    ;; Let tube T with 5 sides going from X (a pt on x-axis) 
    ;; to O (Origin).
    ;;             
    ;; Looking at T from X (to O), an outin-style pt order is 
    ;; (all pts going counter-clock):
    ;;             
    ;; [ [ P,Q,R,S,T ]  // outer pts on X-end
    ;; , [ A,B,C,D,E ]  //           on O-end
    ;; , [ U,V,W,X,Y ]  // inner pts on X-end
    ;; , [ F,G,H,I,J ]  //           on O-end
    ;; ]             
    ;;
    ;; Now, see T as a ring-like structure, and read its 
    ;; cross-sections along the ring circle, an xsec-style is:
    ;;
    ;; [ [P,A,F,U] // xsec 0 (again, counter-clockwise)
    ;; , [Q,B,G,V] // xsec 1
    ;; , [R,C,H,W]
    ;; , [S,D,I,X]
    ;; , [T,E,J,Y]
    ;; ] 
    ;;
    ;; By converting xsec to outin, we can use the faces
    ;; that is generated by faces() function:
    ;;
    ;;  faces=faces('tube', sides=5); // this uses outin style
    ;;  polyhedron( points = joinarr(pts_xsec_converted_to_outin)
    ;;             , faces=faces ); 
    "
    ];
} // convPolyPts



//##################################################

chainPts=["chainPts","pts,nr","pts", "Geom",
"Return arr of slices in which a slice is an arr of pts.
;;
;; nr: sides of each crosec.
;;
"
];    
    
//######################################### 
/*
  chainPts(pts, r=1, nside=6 )
  chainPts(pts, opt="r=1;nside=6" )
  chainPts(pts, opt=["r",1,"nside",6] )
  chainPts(pts, opt=["r=1","nside",6] )
  

Ref:

1. This article describes sweeping pts across space curve, 
in a way that is not as easy as chainPts() uses:
http://webhome.cs.uvic.ca/~blob/courses/305/notes/pdf/ref-frames.pdf

2. trigs
http://www.ibiblio.org/e-notes/Splines/tree/maple.htm

3. Frenet-Serret_formulas
https://en.wikipedia.org/wiki/Frenet%E2%80%93Serret_formulas



*/
           

//========================================================

//   lineCrossPt( P, midPt(Q,R), Q, midPt(P,R) );  


//========================================================
{ // chainBoneData
function chainBoneData(
       n //=3, 
     , a //=[-360,360]
     , a2 //=[-360,360]
     , seglen //= [0.5,3] // rand len btw two values
     , lens //=undef
     , seed //=undef // base pqr
     , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
                     //   , "closed",false,details=false]
     , opt=[]
     , _rtn=[] // for noUturn
     , _smsegs=[] // segments of smoothed pts
     ,_i=0 // for noUturn
     ,_debug
    )
=(
//  !n? _red(str( "ERROR: the n(# of pts) given to randchain() is invalid: ",n ))
//  :(
    _i==0? //####################################### i==0
    
    let( 
        //=========[ initiation ] =======

          opt = popt(opt) // handle sopt
        , n = n>1?n: hash(opt,"n",3)
        , a = und(a, hash(opt,"a", [170,190])) // = aPQR
        , a2= und(a2,hash(opt,"a2",[-45,45])) // = aRQN
        , seglen= or(seglen, hash(opt, "seglen"))
        , _lens = or(lens, hash(opt, "lens"))
        , seed = or(seed, hash(opt, "seed", randPts(3)))
        , smooth= und(smooth, hash(opt, "smooth"))

        //==============================

        ,lens = // If lens < n-1, repeat lens        
           _lens && isarr(_lens) && len(_lens)< n-1?
             repeat(_lens, ceil((n-1)/len(_lens)))
            : _lens
            
//        , L = isarr(lens)? lens[_i-1]
//            : isarr(seglen)? rand(seglen[0],seglen[1]) 
//            : isnum(seglen)? seglen
//            : rand(0.5,3)
        , pt= seed? seed[0]:randPt() //d,x,y,z)
        , newrtn = [pt]  
              
     )//let @_i==0
     
     chainBoneData(n=n,a=a,a2=a2,seglen=seglen, lens=lens
              , seed = seed
              //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1
              , smooth=smooth, _debug=_debug)   
              
   :let(  // ###################################### i > 0
   
     isend= _i==n-1
     ,_a = isarr(a)? rand(a[0],a[1]):a
     ,_a2= isarr(a2)? rand(a2[0],a2[1]):a2
     ,L = isarr(lens)? lens[_i-1]
          : isarr(seglen)? rand(seglen[0],seglen[1]) 
          : isnum(seglen)? seglen
          : rand(0.5,3)
     ,pt= _i==1? (seed? onlinePt( p01(seed), len=L)
                      :onlinePt( [last(_rtn)
                               , randPt()],len=L)
                  )    
          :  let( P = get(_rtn,-2)
                  , Q = get(_rtn,-1)
                  , Rf = _i==2 && seed? seed[2] //onlinePt( p12(seed), len=L)
                         : iscoline( get(_rtn, [-3,-2,-1]))
                            ?randOfflinePt( get(_rtn,[-2,-1]))
                           : get(_rtn,-3)
                  )
               anglePt( [P,Q,Rf], a=_a, a2=_a2, len=L)   
                       
     ,newrtn = app(_rtn, pt ) 
     , smpts = isend?
              ( smooth==true? smoothPts(newrtn)
                :smooth? smoothPts(newrtn, opt=smooth) 
//                   smoothPts( newrtn, n=hash(smooth, "n")
//                            , aH=hash(smooth,"aH")
//                            , dH=hash(smooth,"dH")
//                            , closed= hash(smooth, "closed")
//                            , details= hash(smooth, "details")
//                       )
                       
                   //n=10, aH=0.5, dH=0.3, closed=false, details=false
                  :newrtn
              ):[]
          
//     ,_debug= str(_debug, "<br/>"
//                 ,"<br/>, <b>_i</b>= ", _i
//                 ,"<br/>, <b>len</b>= ", len
//                 ,"<br/>, <b>L</b>= ", L
//                 ,"<br/>, <b>_rtn</b>= ", _rtn
//                 ,"<br/>, <b>newrtn</b>= ", newrtn
//                 ,"<br/>, <b>actual len</b>= ", dist( get(newrtn,[-1,-2]))
//                 ,"<br/>, <b>pt</b>= ", pt
//                 )
     )//let

  //isend? (smooth? [newrtn,smpts]: newrtn ) 
  isend? 
    [ "pts", smpts
    , "unsmoothed", newrtn
    , "smooth", smooth
    , "seed", seed
    , "lens",lens
    , "seglen", seglen
    , "L",L
    , "a",a
    , "a2", a2
    , "debug",_debug
    ]
    // (smooth? [newrtn,smpts]: newrtn ) 
  
  : chainBoneData(n=n,a=a,a2=a2,seglen=seglen, lens=lens, seed = seed
             //, d=d,x=x,y=y,z=z
              , _rtn=newrtn, _i=_i+1, smooth=smooth, _debug=_debug)
  //)// if_n
);
} // chainBoneData



//==============================================
{ // chainlen
function chainlen(pts,i,j)= // ret the sum of lens between i and j
(                           // default: i=0, j = last
    let( _i= fidx(pts, und(i,0))
       , _j= fidx(pts, und(j,len(pts)-1))
       , i = _i>_j? _j:_i
       , j = _i>_j? _i:_j
       )
    sum( [for(k = [i:j-1]) dist( pts[k],pts[k+1] ) ])
);
} // chainlen


//function closestPtInfo(pts,Rf)= //  the [i,pt,dist] of the pt cloest to ref (2018.9.16)
//(
//   //sortArrs( [ for(i=range(pts)) [ i, pts[i], abs(dist(pts[i],ref)) ] ], by=2  
//   //        ) [0]
//   sortByDist(pts,Rf,isAbs=1)[0]        
//);


//=========================================
{ // coordPts
function coordPts(pqr, len,x,y,z)= 
    let( P=pqr[0], Q=pqr[1], R=pqr[2]
       , len= len==undef?dist([P,Q]):len
       , x = x==undef?len:x
       , y = y==undef?len:y
       , z = z==undef?len:z
       //, N=N(pqr,len=z), NN=N( [N, Q(pqr),P(pqr)] ,len=y) 
       )
(  [ Q
   , onlinePt( [Q,P], len=x )
   , N2(pqr,len=y)
   , N(pqr,len=z) 
   ]
);

    coordPts=["coordPts", "pqr,len,x,y,z","pts","Geo",
    ""
    ];

    function coordPts_test( mode=MODE, opt=[] )=
    (
        doctest( coordPts, 
        [ 
          
        ], mode=mode, opt=opt
        )
    );
} // coordPts


       
//========================================================


//========================================================
{ // cornerPt
function cornerPt(pqr)=
(
  //onlinePt( [pqr[1],midPt	([pqr[0],pqr[2]])], ratio=2)
   pqr[2]+pqr[0]-pqr[1]
);

    cornerPt=[ "cornerPt", "pqr", "pt", "Point",
    "
     Given a 3-pointer (pqr), return a pt that's on the opposite side
    ;; of Q across PR line.
    ;;
    ;;        Q-----R  Q--------R
    ;;       /   _//    ^.      |^.
    ;;      /  _/ /       ^.    |  ^.
    ;;     /_/   /          ^.  |    ^.
    ;;    //    /             ^.|      ^. 
    ;;   P     pt               P        pt
    "];
     
     
    function cornerPt_test( mode=MODE, opt=[] )=
    (
        doctest( cornerPt, 
        [ 
          [ cornerPt([ [2,0,4],ORIGIN,[1,0,2]  ]),  [3,0,6]
                ,"[ [2,0,4],ORIGIN,[1,0,2]]"
          ]
        ], mode=mode, opt=opt
        )
    );
} // cornerPt

//========================================================
//{ // cubePts
  // Ref: Cube( size, center, color, frame, coord, actions, markPts, dim, opt=[] )

// function cubePts( pqr, center=false, p=undef, r=undef, h=1,shift)=
//(
// isnum(pqr)?
// ( let(center= center==true||center==1?[1,1,1]:center
//      ,pts = [ [pqr,0,0],ORIGIN, [0,pqr,0],  [pqr,pqr,0]
//             , [pqr,0,pqr],[0,0,pqr],  [0,pqr,pqr], [pqr,pqr,pqr]
//             ]
//      )
//   center? transPts(pts, actions=["transl", [center[0]?-pqr/2:0
//                                        ,center[1]?-pqr/2:0
//                                        ,center[2]?-pqr/2:0]
//                             ])
//         : pts   
// )
// :ispt(pqr)? 
// (
//   let(center= center==true||center==1?[1,1,1]:center
//      ,pts = [ [pqr[0],0,0]     , ORIGIN      , [0,pqr[1],0], [pqr[0],pqr[1],0]
//             , [pqr[0],0,pqr[2]], [0,0,pqr[2]], [0,pqr[1],pqr[2]]
//             ,  [pqr[0],pqr[1],pqr[2]]
//             ]
//      )
//   center? transPts(pts, actions=["transl", [center[0]?-pqr[0]/2:0
//                                        ,center[1]?-pqr[1]/2:0
//                                        ,center[2]?-pqr[2]/2:0]
//                             ])
//         : pts  
// )
// :let( _pqr= pqr?pqr:randPts(3)
//     , Q = _pqr[1]
//     , h_vector = normalPt(_pqr, len=h)-Q
//     , shiftvec = onlinePt( [N(_pqr), Q], len=-shift )-Q
//     , pqr = isnum(shift)? [ for (p=_pqr) p+shiftvec ] 
//			 :_pqr 
//     , pts = squarePts( pqr, p=p, r=r )
//     //, pts = [_pts[1], _pts[0], _pts[3], _pts[2]] 
//          // we flip the pts from    // <================== NOT A GOOD IDEA
//          //     1_------2_   
//          //       '-_     '-_
//          //          '0-------3 
//          // to 
//          //     0_------3_   
//          //       '-_     '-_
//          //          '1-------2 
//          // to be consistent with the 2 cases above (pqr=num and pqr=pt)                 
//                                                            			 
//     )	concat( pts
//	      , [for (pt=pts) pt+h_vector ] //sign(h)>0?(pt-h_vector):(pt+h_vector) ]
//	      )		
//);
//}// cubePts

//========================================================
{ // cubePts2
  // Ref: Cube( size, center, color, frame, coord, actions, markPts, dim, opt=[])
  
 function cubePts( size, center, site, actions, opt)= // opt: ["coord"..."actions"...]
(
  //hash( ptsHandler( cubeSize = size, center=center
                     //, site=site, actions=actions, opt=opt)
      //, "pts"
      //)
  hash( getTransformData( size = size
                         , center=center
			 , site=site
			 , actions=actions
			 , opt=opt)
      , "pts"
      )
); 

    cubePts=["cubePts", "pqr,p=undef,r=undef,h=1", "points", "Point",
    " Like boxPts but is not limited to axes-parallel boxes. That is, 
    ;; it produces 8 points for a cube of any size at any location along 
    ;; any direction. 
    ;; 
    ;; The input pqr = [P,Q,R0] is used to make the square PQRS that is
    ;; coplanar with R0. It is then added the h to make TUVW by finding 
    ;; the normal of pqr. Optional p,r,h: numbers indicating lengths as 
    ;; shown belo.
    ;;
    ;;     U------------------V_ 
    ;;     |'-_     R0        | '-_
    ;;     |   '-_ -''-_      |    '-_
    ;;     |   _. '-T---'-------------W
    ;;     |_-'     |      '-_|       |
    ;;     Q_-------|---------R_      | h
    ;;       '-_    |    r      '-_   |
    ;;       p  '-_ |              '-_|
    ;;             'P-----------------S
    ;;                        r
    ;; Return a 8-pointer: [P,Q,R,S,T,U,V,W] with order: // pt order changed 2017.10.9
    ;;
    ;;     5----------6_ 
    ;;     | '-_      | '-_
    ;;     1_---'-_---2_   '-_
    ;;       '-_   '4---------7
    ;;          '-_ |      '-_|
    ;;             '0---------3
    ;;
    ;; Use rodfaces(4) to generate the faces needed for polyhedron. 
    ;; 
    ;; New 20141003: shift 
    ;;
    ;;     U------------------V_ 
    ;;     |'-_               | '-_
    ;;     |   '-_            |    '-_
    ;;    Q|------'-T-----------------W
    ;;     | '-_    |  r      |       |
    ;;     |_---'-_-|----------_      | 
    ;;       '-_   'P           '-_   | ---
    ;;          '-_ |              '-_|   -shift
    ;;             'X------------------ ---
    ;;
    ;; New 2015.3.7: pqr is optional. If not given, make it with randPts(3)
    ;;
    ;; New 2017.10.9: 
    ;;  (1) Make its arguments competible with the built-in cube()
    ;;      by adding center and allowing pqr to be a number or a vector
    ;;  (2) Extend the center to accept (other than true/false) a vector
    ;;  (3) Rearrange the pts order:
    ;;
    ;;     4----------7_ 
    ;;     | '-_      | '-_
    ;;     0_---'-_---3_   '-_
    ;;       '-_   '5---------6
    ;;          '-_ |      '-_|
    ;;             '1---------2
    ;;     to be consistent with that generated in pqr=num and pqr=pt cases.
    ;;
    ;; Note 2018.1.9:
    ;; Point (3) doesn't seem to be carried through for some reason.  
    "];

    function cubePts_test( mode=MODE, opt=[] )=
    (
        doctest(cubePts,mode=mode, opt=opt )
    );
} // cubePts2

//========================================================
{ // cubePts3
  // Ref: a cubePts version relies solely the objArgHandler
  //     (other than size) to provide arguments. 20180104
  
 function cubePts3( opt )= // opt: ["site"..."actions"...]
(
 let( size= isnum(size)? [size,size,size]
             : ispt(size)? size
             : randPts(3, d=[0,3]) 
             
    , center = hash( opt, "center")
    , pts0= [ [size.x,0,0], ORIGIN, [0,size.y,0],[size.x, size.y,0] 
              , [size.x,0,size.z], [0,0,size.z], [0,size.y,size.z]
              , [size.x, size.y,size.z] 
              ]  
    , pts1= center? transPts(pts0, actions=["t", [center[0]?-size[0]/2:0
                                                ,center[1]?-size[1]/2:0
                                                ,center[2]?-size[2]/2:0]
                                          ])
           : pts0                
    , actions = hash( opt, "actions" )
    , pts2= actions==false? pts1: transPts(pts1, actions=actions2)
    
    , site = hash( opt, "site" )
    ) 
    site? movePts( pts2, to=site ): pts2         
); 

    cubePts=["cubePts", "pqr,p=undef,r=undef,h=1", "points", "Point",
    " Like boxPts but is not limited to axes-parallel boxes. That is, 
    ;; it produces 8 points for a cube of any size at any location along 
    ;; any direction. 
    ;; 
    ;; The input pqr = [P,Q,R0] is used to make the square PQRS that is
    ;; coplanar with R0. It is then added the h to make TUVW by finding 
    ;; the normal of pqr. Optional p,r,h: numbers indicating lengths as 
    ;; shown belo.
    ;;
    ;;     U------------------V_ 
    ;;     |'-_     R0        | '-_
    ;;     |   '-_ -''-_      |    '-_
    ;;     |   _. '-T---'-------------W
    ;;     |_-'     |      '-_|       |
    ;;     Q_-------|---------R_      | h
    ;;       '-_    |    r      '-_   |
    ;;       p  '-_ |              '-_|
    ;;             'P-----------------S
    ;;                        r
    ;; Return a 8-pointer: [P,Q,R,S,T,U,V,W] with order: // pt order changed 2017.10.9
    ;;
    ;;     5----------6_ 
    ;;     | '-_      | '-_
    ;;     1_---'-_---2_   '-_
    ;;       '-_   '4---------7
    ;;          '-_ |      '-_|
    ;;             '0---------3
    ;;
    ;; Use rodfaces(4) to generate the faces needed for polyhedron. 
    ;; 
    ;; New 20141003: shift 
    ;;
    ;;     U------------------V_ 
    ;;     |'-_               | '-_
    ;;     |   '-_            |    '-_
    ;;    Q|------'-T-----------------W
    ;;     | '-_    |  r      |       |
    ;;     |_---'-_-|----------_      | 
    ;;       '-_   'P           '-_   | ---
    ;;          '-_ |              '-_|   -shift
    ;;             'X------------------ ---
    ;;
    ;; New 2015.3.7: pqr is optional. If not given, make it with randPts(3)
    ;;
    ;; New 2017.10.9: 
    ;;  (1) Make its arguments competible with the built-in cube()
    ;;      by adding center and allowing pqr to be a number or a vector
    ;;  (2) Extend the center to accept (other than true/false) a vector
    ;;  (3) Rearrange the pts order:
    ;;
    ;;     4----------7_ 
    ;;     | '-_      | '-_
    ;;     0_---'-_---3_   '-_
    ;;       '-_   '5---------6
    ;;          '-_ |      '-_|
    ;;             '1---------2
    ;;     to be consistent with that generated in pqr=num and pqr=pt cases.
    "];

    function cubePts_test( mode=MODE, opt=[] )=
    (
        doctest(cubePts,mode=mode, opt=opt )
    );
} // cubePts3

//function __get_arg( name, a, opt, df)=  // used in cylinderPts()
//(
//  a?a: hash(opt, name, df)
//);

function cutHolePts(pts,holePts)=  // 2018.5.2
(
   /*
     2 +--------------+ 3
       |            .'|   
       |  5.------4'  |   
       |   |      |   |   
       |   |      |   |   
       |   |      |   |
       |  6'------'7  |   
     1 '--------------' 0
   
     Make a hole (pis = [5,6,7,8]) out of plane [0,1,2,3]
     return pts,  indexed [0,1,2,3,4,5,6,7]
   
     Note: pts must be clockwise
           holePts must be counter-clockwise 
           
     Note 2018.5.3: 
     
          This function doesn't seem to be needed. To form a face that
          has hole, simply add indices of the hole counter-clockwise.
          It doesn't matter which index starts the count. For example, 
          any of the following works:
          
          [ 0,1,2,3,0,  4,5,6,7,4 ]
          [ 0,1,2,3,0,  6,7,4,5,6 ]
          [ 0,1,2,3,0,  7,4,5,6,7 ]
          
          So there's no need to seek a closest pt or roll
          the indices.
   */
   3
//   let( closestPi= closestPi( holePts,pts[0])
//      , _holePts = closestPi!=0? roll( holePts, -closestPi ): holePts            
//      , rtn = concat( pts, _holePts)
//      )
//   //echo( closestPi = closestPi )   
//   rtn
);



function cylinderPts( h,r,r1,r2,center,d,d1,d2,$fn, $fa, $fs
                    , actions, site, opt=[])= 
   // opt: ["site"..."actions"..."fn"]
( // argument priority: d1,d2 > r1,r2 > d > r
   //echo("in_cylinderPts: ", r=r, r1=r1, r2=r2)
   //echo("in_cylinderPts: ", d=d, d1=d1, d2=d2)
  
   hash( getTransformData( shape="cylinder"
                         , h=h
                         , r=r
                         , r1=r1
                         , r2=r2
                         , center=center
			 , d=d
                         , d1=d1
                         , d2=d2
                         , $fn=$fn
                         , $fa=$fa
                         , $fs=$fs
                         , site=site
			 , actions=actions
			 , opt=opt
			 )
      , "pts"
      )
        
); 


//========================================================
//. d, e

//module decorate(pts, size, color, Frame, MarkPts, Dim, actions, site, opt=[] )
//{
   //_color= colorArg(color, opt);
   //_Frame= subprop(Frame, opt, "Frame",["color", _color] );
   //_MarkPts= subprop(MarkPts, opt, "MarkPts",["color", _color] );
   //_Dim= subprop(Dim, opt, "Dim",["color", _color] );
   //
   //if(pts)
   //{
     //if(_Frame) Frame( pts, opt=_Frame);
   //
     //if(_MarkPts)
     //{ MarkPts( pts, opt=_MarkPts );  
     //}     
     //if(_Dim) Dims(pts= pts, pqrs= hash(_Dims,"pqrs"), opt=_Dim);
   //}
   //
   //Color( color )
   //TransformObj(opt=ptsData)     
    //children();
//}



{ // dist
function dist(a,b,ij=[])=
(
    // Note 2015.4.11:
    // dist could be made available for dist of dist(a,b)
    //                a       b   
    //   pt-pt      P       Q
    //   pt-ln      P       [Q,R] 
    //   ln-pt      [Q,R]   P
    //   ln-ln      [P,Q]   [R,S]       
    //   pt-pl      P       [R,S,T]
    //   pl-pt      [R,S,T] P
    //   ln-pl      [P,Q]   [R,S,T]
    //   pl,ln      [R,S,T] [P,Q]
    //   pl,pl      [P,Q,R] [S,T,W]

    // http://math.harvard.edu/~ytzeng/worksheet/distance.pdf
    let( c= b==undef?(ij?a:a[0]):a
       , d= b==undef?a[1]:b
       , t= str(gtype(c), gtype(d)) 
       )
     //echo( log_b("dist()", 5) )
     //echo( log_(["a",a,"b",b, "c",c, "d", d, "t",t], 5))     
     //echo( log_e("dist()", 5) )
       
     ij? dist( get(a,ij) )  
    :d=="x"? c[1][0]-c[0][0]
    :d=="y"? c[1][1]-c[0][1]
    :d=="z"? c[1][2]-c[0][2]
    :t=="ptpt"? norm(c-d)
     
    :t=="ptln"? norm(c- othoPt( [d[0],c,d[1]] ) )
    :t=="lnpt"? norm(d- othoPt( [c[0],d,c[1]] ) )
    
    :t=="lnln"? uv(c)==uv(d)? // parallel
                  norm( othoPt( [c[0],d[0],c[1]]) - d[0])
                // skew lines, use unit vector of c to make a pl
                // with d (pl=[ d[0],d[1], d[0]+uv_vc, which is
                // parallel to c, then calc dist btw c and pl  
                : abs(distPtPl( c[1], [d[0],d[1], d[1]+ uv(c)] ))
                            
    :t=="ptpl"? distPtPl( c, d )
    :t=="plpt"? distPtPl( d, c )
    :t=="lnpl"? distPtPl( c[0], d)
    :t=="plln"? distPtPl( d[0], c)
    :t=="plpl"? undef // need this
    :undef
);

    dist=["dist","[a,b];a,b","number","len",
    "The distance between a,b, both could be pt, line or plane.
    ;;
    ;; Or, when a is [P,Q] and b is one of str 'x','y','z', return
    ;; the dist of Q.x-Px, Q.y-P.y, Q.z-P.z, respectively. These
    ;; values could be negative. 
    ;;
    ;; Those involving in plane could be negative as well, such
    ;; that it can tell which side of the plane the target is.
    ;;
    ;; Given a plane [P,Q,R], pt N is on the positive side:
    ;;
    ;;       N
    ;;       |
    ;;       Q 
    ;;      / '-._
    ;;     /      'R
    ;;    P 
    "
    ];

    function dist_test( mode=MODE, opt=[] )=
    (
        let(
        pqrs = randPts(4)
        ,P = pqrs[0]
        ,Q = pqrs[1]
        ,R = pqrs[2]
        ,S = pqrs[3]
        ,L1= [ [2,0,0], [2,2,0] ]
        ,L2= [ [0,0,0], [0,2,0] ]
        ,L3= [ [0,0,0], [0,2,2] ]
        ,dPQ = sqrt( pow(P.x-Q.x,2)
               + pow(P.y-Q.y,2)+ pow(P.z-Q.z,2) )
        )

        doctest( dist, 
        [   
         _b("pt-pt")
        ,""
        , "var P", "var Q"
        , [ dist(P,Q),  dPQ, "P,Q" ]
        , [ dist([P,Q]),dPQ, "[P,Q]" ]

        ,"" //--------------------------------------------
        ,_b("pt-line")
        ,"// Any of the following work:"
        ,"//    (pt,ln), ([pt,ln]), (ln,pt), ([ln,pt])"
        ,""
        , "var L1", "var L2","var L3"

        , [ dist( L1, L2[1] ), 2, "L1, L2[1]" ]
        , [ dist( [L3[1], L1] ), longside(2,2), "[ L3[1],L1 ]" ]
        , [ dist( [L3[1], L3] ), 0, "[ L3[1],L3 ]" ]
            
        ,"" //--------------------------------------------
        ,_b("pt-plane")
        ,"// Any of the following work:"
        ,"//    (pt,pl), ([pt,pl]), (pl,pt), ([pl,pt])"
        ,""
        , [ dist( L3[1], app(L1,ORIGIN) ), -2, str("L3[1],", app(L1,ORIGIN)) ]
        
        ,"" //--------------------------------------------
        ,_b("line-line")
        ,"// Line-line dist: (ln,ln), ([ln,ln])"
        ,""
        , [ dist([L1,L2]), 2, "[L1,L2]", ["//","parallel"] ]
        , [ dist(L1,L3), 2, "L1,L3" ]
        , [ dist([L2,L3]), 0, "[L2,L3]", ["//","intersect"] ]
        ,"// https://www.youtube.com/watch?v=t8Ziil0954U"
        , [ dist([[[1,1,3],[0,2,2]],[[2,1,0],[3,-1,1]]])
            , 2*pow(2,0.5),[[ [1,1,3], [0,2,2]], [[2,1,3],[3,-1,1]]]]
        ,"// https://www.youtube.com/watch?v=sbvvIcw77VY"
        , [ dist([[[-2,1,0],[-1,0,1]],[[0,1,0],[1,2,2]]])
            , 6/pow(14,0.5),[[[-2,1,0],[-1,0,1]],[[0,1,0],[1,2,2]]]]


        ,"" //--------------------------------------------
        ,_b("line-plane")
        ,"// Line-plane dist: "
        ,"// (ln,pl), (ln,pl), ([pl,ln]), ([ln,pl])"
        ,""
        , [ dist( L1, app(L2,L3[1]) ), -2
            , str("L1,",app(L2,L3[1])) ]

        , [ dist( L1, [L2[0],L3[1],L2[1]] ), 2
            , str("L1,",[L2[0],L3[1],L2[1]]) ]

        
        ,"" //----------------------------------------------
        ,_b("b='x','y','z':")
        
        , [ dist(L1,"x"), 0, "L1,'x'"]
        , [ dist(L2,"y"), 2, "L2,'y'"]
        , [ dist([L1,"z"]), 0, "[L1,'z']"]
        ]	
        ,mode=mode, opt= opt //update( opt, ["showscope",true])
        ,scope=[ "P",P, "Q",Q,"R",R,"S",S
               , "L1",L1,"L2",L2, "L3",L3
               ]
        )
    );

    //echo( dist_test(mode=112)[1] );
} // dist


//==================================================
distx = ["distx", "pq", "number", "len",
" Given a 2-pointer pq, return difference of x between pq. i.e., 
;;
;; distx(pq) = pq[1][0]-pq[0][0]
"];

function distx_test( mode=MODE, opt=[] )=
(
	let( pq = [[0,3,1],[4,5,-1]]
	   , pq2= reverse(pq)
	   )       
	doctest( distx, 
    [ 
      [ distx(pq), 4,"pq" ]			   		
    , [ distx(pq2), -4,"pq2" ]			   		
    ]
    , mode=mode, opt=opt, scope=["pq", pq, "pq2",pq2]
    )
);

//==================================================
disty= ["disty", "pq", "number", "len",
" Given a 2-pointer pq, return difference of y between pq. i.e.,
;;
;; disty(pq) = pq[1][1]-pq[0][1]
"];

function disty_test( mode=MODE, opt=[] )=
( 
	let( pq = [[0,3,1],[4,5,-1]]
	   , pq2= reverse(pq)
       )	
	doctest( disty,
    [ 
      [ disty(pq), 2,"pq" ]			   		
    , [ disty(pq2), -2,"pq2" ]			   		
    ]
    , mode=mode, scope=["pq", pq, "pq2",pq2]
    )
);

//==================================================
distz= ["distz", "pq", "number", "len",
" Given a 2-pointer pq, return difference of z between pq. i.e.,
;;
;; distz(pq) = pq[1][2]-pq[0][2]
"];

function distz_test( mode=MODE, opt=[] )=
(
	let( pq = [[0,3,1],[4,5,-1]]
	   , pq2= reverse(pq)
       )
	doctest( distz,
    [ 
      [ distz(pq), -2,"pq" ]			   		
    , [ distz(pq2), 2,"pq2" ]			   		
    ]
    ,mode=mode, opt=opt, scope=["pq", pq, "pq2",pq2]
	)
);

//========================================================
{ // distRatios             
function distRatios(pts, isAccum=1) = 
(
   let( lens= [ for(i=[1:len(pts)-1])
                dist( pts, ij=[i-1,i]) ]
       , lensum= sum(lens)
       , accumlens= accum(lens)
       )
   isAccum? 
     concat([0],[ for( d=accumlens) d/lensum ])
   : [for( d=lens ) d/lensum ]   // new 2018.9.22
 );

    distRatios=["distRatios", "pts", "array", "Geometry",
    " For pts= [p0,p1,p2...pn] of pts (the bone),
    ;; if isAccum:
    ;;   ratio of di/d where di= sum( dists prior to i ),
    ;;   d= dist(0,n), eg: [ 0, 0.3, 0.45,0.78,1]
    ;; else:
    ;;   => [0.3, 0.15, 0.33, 0.22 ]  (with sum = 1)
    ;;
    "];      
} // distRatios

//========================================================

// UNDER CONSTRUCT
//
//function distPts( ln1, ln2 )= 
//(
//    // Given 2 lines, return 2 pts,A,B, one on each line, 
//    // that the dist_AB is the dist between the 2 lines
//    // (shortest distance)
//    // NOTE: have to watch if any func used in term requires this
//    //       func, which creates endless loop. Funcs we use all 
//    //       trace back to intsec(ln,ln), which is fine.
//   let( R = ln2[0]
//      , S = ln2[1]
//      , Jr = projPt( R, ln1)
//      , Js = projPt( S, ln1)
//      , dr = dist( Jr,R )
//      , ds = dist( Js,S )
//      , B = onlinePt( [R,S], ratio = dr/(dr+ds) )
//      , A = projPt( B, ln1 )
//      //, A = onlinePt( [Jr,Js], ratio=dr/(dr+ds) ) 
//      //, B = projPt( A, ln2) //onlinePt( [R,S], ratio=dr/(dr+ds) ) 
//      ) [A,B, Jr, Js]
//      
//    // Approach:
//    //   let ln1= [P,Q], ln2= [R,S]
//    //   pl = [P,Q,R]
//    //   J  = projPt( S,pl)
//    //   A = intsec( [P,Q], [R,J] )
//    //   B = onlinePt( [R,S], ratio= dist( R,A) / dist(R,J) )
//    // 
////   let( R = ln2[0]
////      , S = ln2[1]
////      , pl= [ ln1[0],ln1[1],R] 
////      , J = projPt( S, pl )
////      , A = intsec( ln1, [R,J] )
////      , B = projPt( A, ln2) //onlinePt( [R,S], ratio= dist( R,A) / dist(R,J) )    
////      ) [A,B, J]
//);


//========================================================
{ // distPtPl
function distPtPl(pt,pqr)=
(
	( planecoefs(pqr, has_k=false)* pt  + planecoefs(pqr)[3]) 
	/ norm(	planecoefs(pqr, has_k=false) )
);

    distPtPl=["distPtPl", "pt,pqr", "number", "len" ,
     " Given a pt and a 3-pointer (pqr) , return the signed distance between
    ;; the plane formed by pqr and the point.
    "];
        
    /*
        Pt = [x0, y0, z0 ]
        Pl = Ax + By + Cz + D = 0;

              Ax0 + By0 + Cz0 + D
        d= --------------------------
            sqrt( A^2 + B^2 + C^2 ) = norm([A,B,C])

        pc = planecoefs( pqr ) = [A,B,C,D]

        Ax0 + By0 + Cz0 + D=  [pc[0],pc[1],pc[2]] * Pt  + pc[3] = 

    */
    function distPtPl_test( mode=MODE, opt=[] )=
    (
        let( pt= [1,2,0]
           , pqr = [ [-1,0,2], [1,0,-3], [2,0,4]]
           )
        doctest( distPtPl,
        [
          "var pt"
        , "var pqr"
        , [distPtPl(pt,pqr), 2, "pt,pqr" ]
        ]
        , mode=mode, opt=opt
        , scope=[ "pt", pt
                , "pqr", pqr ]	
        )
    );
} // distPtPl


function divPts(pts, divider, _i=0)=
(
  /* divider: a pt : [x,y] or [x,y,z]
              a line : 2d-line or 3d-line
              a plane : 3d-plane   
  */
  3
//  ispt(divider, dim=2)?
//  (  
//     
//  
//  ) 
//  :ispt(divider, dim=3)?
//   (
//   
//   
//   )
//  :len(divider)==2?
//  (
//  
//  ) 
//  :(
//  
//  )
);


function decoPolyEdge_1804( pts, faces, pis,dir, template= [ [0,1], [1,0] ] )=
(
   /*
     To make an edge-decoration on edge [4,7]:
     
     edgingPts( pts, faces
              , pis = [5,4,0]
              , dir = 7
              , template= template
              ) 
   
            6---------------7
          .'    [2]       .'|
        .'  A     x     .'  |
       5----+._-------4'    |
       |       '-._   | [5] |
       |           'B | y   |
       |             \|     |
       |     [3]      + C   .3
       |              |   .'
       |              | .'
       1--------------0'
       
     template is an array of 2d pts on XY plane: [A,B,C]
        
        Y
        |     x
       A+._- - - -+ 
        |  '-._   :
        |      'B : y
        |        \: 
       -+---------+-- X
        0         C
                      
                    
            6-----D._= 7  pts[4],[7] are re-assigned to A and D
          .'[2] .'   '-._  
        .'    .'         .E
       5----A._        .'  \
       |  = 4  '-._  .'    .F
       |           'B    .' |
       |             \ .'   |
       |     [3]      C [5] .3
       |              |   .'
       |              | .'
       1--------------0'

   //  CEFs@4= 
   // 
   //  [ 5,["L",3, "R",2, "subface",[21,20,0,3]]
   //  , 7,["L",2, "R",5, "subface",[3,2,9,8]]
   //  , 0,["L",5, "R",3, "subface",[8,11,22,21]]
   //  ] 
   */
   let( 
      //-----[ set new pts ] -----
      
        x= last(template)[0]
      , y= template[0][1]
      , P = pts[pis[0]]
      , Q = pts[pis[1]]
      , R = pts[pis[2]]
      , S = pts[dir]
      , tmPts = to3d(template) //[for(p=template)[p[0],p[1],0] ]
      , Pb= onlinePt( [Q,P], len=x)
      , Pe= onlinePt( [Q,R], len=y)

      //---------------------------------------------------- 
      // Generate 2 Rf (reference pts) for angle calc. 
      // The len below is arbitrarily choice in attempt to prevent
      // negative angles.       
      //, Rf_tmp = onlinePt( [[x,y,0], ORIGIN], len= max(x,y)*3 )
      //, Rf = onlinePt( [ Q, Pe+Pb-Q], len=max(x,y)*3  ) 
      , Rf_tmp =[x-dist(P,Q),y-dist(Q,R),0]
      //, Rf = onlinePt( [ Q, Pe+Pb-Q], len=max(x,y)*3  ) 
      , newPts1= len(tmPts)==2? [Pe] 
                  : [ each [for(i=[1:len(tmPts)-2])
                             //anglePt( [Rf, Pb,Pe]
                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
                                    //, len=dist( tmPts[0], tmPts[i])   
                                    //)
                             anglePt( [ R, P+R-Q, P] 
                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
                                    , a= angle(
                                          [ Rf_tmp+[1,0,0]
                                          , Rf_tmp
                                          , tmPts[i]
                                          ])
                                    , len=dist( Rf_tmp //[y-dist(Q,R),x-dist(P,Q),0]   
                                              , tmPts[i])   
                                    )
                           ]                
                    , Pe 
                    ]        
      , newPts2= [ for(pt=newPts1)
                   pt+ S-Q
                 ]  
      , oldPts = [ for(i=range(pts))
                   ///  pts[4],[7] are re-assigned to A and D
                   i== pis[1] ? Pb
                   : i== dir ? (Pb+ S-Q) 
                   : pts[i]
                  ]
      , newPts = concat( oldPts, newPts1, newPts2)  
      )
//      echo( "---------------------- calc pts")
//      echo(pts=pts)
//      echo( x=x,y=y )
//      echo( tmPts = tmPts )
//      echo( Pb=Pb )
//      echo( Pe=Pe )
//      //echo(Rf_tmp=Rf_tmp, Rf=Rf)
//      echo( oldPts=oldPts)
//      echo( newPts1=newPts1 )
//      echo( newPts2=newPts2 )
//      echo( newPts = newPts)
      
    //-----[ set new faces ] -----
    let( addedfaces= [ for(i=[0:len(template)-2])  
                       /// when i=0, = indices for [A, D, E, B]
                      [ i==0? pis[1]: len(pts)+i-1 
                      , i==0? dir :len(pts)+i+len(template)-2
                      , len(pts)+i+len(template)-1
                      , len(pts)+i 
                      ]
                   ]
      
      //-----[ set front and back faces ] -----
      
      , dir_eg = quicksort( [pis[1], dir] )
      , front_eg1= quicksort( [pis[0], pis[1]] )
      , front_eg2= quicksort( [pis[2], pis[1]] )
      
      , a_sort_egs= get_face_edges( faces, isSortedPis=true )
      
      , front_fi = get_fis_have_edges( faces
                                     , [front_eg1,front_eg2] 
                                     )[0]
      , left_fi = get_fis_have_edges( faces
                                     , [front_eg1,dir_eg] 
                                     )[0]
      , right_fi = get_fis_have_edges( faces
                                     , [dir_eg,front_eg2] 
                                     )[0]
      , front_pis = [ for(pi = faces[front_fi] )
                      each pi== pis[1]? 
                         concat( [pi]
                              , range(len(pts),len(pts)+len(tmPts)-1)
                              )
                      : pi
                    ]      
                           
      , back_eg1 = [ for( eg = a_sort_egs[ left_fi ] )
                     if( has(eg, dir) && !has(eg, pis[1] ) )
                     eg
                   ][0]
      , back_eg2 = [ for( eg = a_sort_egs[ right_fi ] )
                     if( has(eg, dir) && !has(eg, pis[1] ) )
                     eg
                   ][0]
                   
      , back_fi = get_fis_have_edges( faces
                                     , [back_eg1,back_eg2] 
                                     )[0]
                                     
      , back_pis = [ for(pi = faces[back_fi] )
                      //echo("---", pi=pi) 
                      each pi== dir? 
                         concat( range(len(pts)+len(tmPts)*2-3
                                     ,len(pts)+len(tmPts)-2)
                              , [pi]
                              )
                      : pi
                    ] 
      , right_pis= [ for(pi = faces[right_fi] )
                      pi== pis[1]? ( len(pts)+len(tmPts)-2 )
                      : pi==dir? ( len(pts)+len(tmPts)*2-3 )
                      : pi
                    ]                                                  
       )
//       echo( "---------------------- calc faces")
//       echo(faces=faces)
//       echo(addedfaces=addedfaces)
//       echo( dir_eg = dir_eg )
//       echo(front_eg1=front_eg1)
//       echo(front_eg2=front_eg2)
//       echo(front_fi=front_fi)
//       echo(front_pis=front_pis)
//       echo(left_fi=left_fi)
//       echo(back_eg1=back_eg1)
//       echo(back_fi=back_fi)
//       echo(back_pis=back_pis)
//       echo(right_pis=right_pis)
       //.....................................                   
     let( oldfaces = [ for(fi=range(faces))
                        fi==front_fi? front_pis
                        : fi==back_fi? back_pis
                        : fi==right_fi? right_pis
                        : faces[fi] 
                     ]            
       , newfaces = concat( oldfaces, addedfaces ) 
                     
      )
      //echo(oldfaces=oldfaces)
      [ newPts, newfaces ]
);



function decoPolyEdge_180522_1( pts, faces
                     , pis //, dir
                     , template= [ [-1,0], [0,-1] ] )=
(
   /*
      180522_1: 
        previous version, decoPolyEdge_1804, has good algorithm
        but user hard to figure out the template direction and
        outcome. Also, arg *dir* seems to be (1) on a negative 
        normal of pts (2) in fact not needed when pis is not given 
   
        Achieved: template is now drawn on the [-x,-y] region
                  Works nicely but pis order still not derizable
                  ( for example, [5,4,0] but not [0,4,5] )
   
     To make an edge-decoration on edge [4,7]:
     
     edgingPts( pts, faces
              , pis = [0,4,5]
              , template= template
              ) 
   
            6---------------7
          .'    [2]       .'|
        .'  A     x     .'  |
       5----+._-------4'    |
       |       '-._   | [5] |
       |           'B | y   |
       |             \|     |
       |     [3]      + C   .3
       |              |   .'
       |              | .'
       1--------------0'
       
     template is an array of 2d pts on XY plane: [A,B,C]
        
                  y
                  : 
                  :
       A+._- - - -+ - - - x 
           '-._   :
               'B : 
                 \: 
                  +
                  C
                      
                    
            6-----D._= 7  pts[4],[7] are re-assigned to A and D
          .'[2] .'   '-._  
        .'    .'         .E
       5----A._        .'  \
       |  = 4  '-._  .'    .F
       |           'B    .' |
       |             \ .'   |
       |     [3]      C [5] .3
       |              |   .'
       |              | .'
       1--------------0'

   //  CEFs@4= 
   // 
   //  [ 5,["L",3, "R",2, "subface",[21,20,0,3]]
   //  , 7,["L",2, "R",5, "subface",[3,2,9,8]]
   //  , 0,["L",5, "R",3, "subface",[8,11,22,21]]
   //  ] 
   */
   let( 
      //-----[ set new pts ] -----
      
        x= -template[0].x
      , y= -last(template).y
      , tmPts = to3d(template) 
      
      /* PQR = pts at pis
         
       R----Pe_-------Q
       |       '-._   |
       |           'B |
       |             \|
       |     [3]      Pb
       |              |
       1--------------P
      */
      , P = pts[pis[0]]
      , Q = pts[pis[1]]
      , R = pts[pis[2]]
      , Pb= onlinePt( [Q,P], len=x)
      , Pe= onlinePt( [Q,R], len=y)
      
     // , S = pts[dir]
      
      
      //, Rf = onlinePt( [ Q, Pe+Pb-Q], len=max(x,y)*3  ) 
//      , newPts1= len(tmPts)==2? [Pe] 
//                  : [ each [for(i=[1:len(tmPts)-2])
//                             //anglePt( [Rf, Pb,Pe]
//                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
//                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
//                                    //, len=dist( tmPts[0], tmPts[i])   
//                                    //)
//                             anglePt( [ R, P+R-Q, P] 
//                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
//                                    , a= angle(
//                                          [ Rf_tmp+[1,0,0]
//                                          , Rf_tmp
//                                          , tmPts[i]
//                                          ])
//                                    , len=dist( Rf_tmp //[y-dist(Q,R),x-dist(P,Q),0]   
//                                              , tmPts[i])   
//                                    )
//                           ]                
//                    , Pe 
//                    ]        

      )
//      echo( "---------------------- calc pts")
//      echo(pts=pts)
//	echo( pis = pis )
//      echo( x=x,y=y )
//      echo( tmPts = tmPts )
//      echo( Pb=Pb )
//      echo( Pe=Pe )
//      //echo(Rf_tmp=Rf_tmp, Rf=Rf)
//      echo( oldPts=oldPts)
//      echo( newPts1=newPts1 )
//      echo( newPts2=newPts2 )
//      echo( newPts = newPts)
      
    //-----[ set new faces ] -----
    let( 
      
      //-----[ set front and back faces ] -----
      
       front_eg1= quicksort( [pis[2], pis[1]] )
      , front_eg2= quicksort( [pis[0], pis[1]] )
      
      
      , a_sort_egs= get_face_edges( faces, isSortedPis=true ) 
                    // arr of sorted edges, each item represents a face
      
      , dir_eg = quicksort(                   // the edge representing the direction
                  [ for(eg=get_edges(faces)) 
                    if(has(eg,pis[1]) && !has(eg,pis[0])
                       && !has(eg.pis[2]) ) eg ][0] 
                 )
      , dir = del( dir_eg, pis[1])[0] 
      
      , front_fi  = get_fis_have_edges( faces, [front_eg1,front_eg2] )[0]
      , left_fi   = get_fis_have_edges( faces, [front_eg2,dir_eg] )[0]
      , right_fi  = get_fis_have_edges( faces, [dir_eg,front_eg1] )[0]
      , front_pis = [ for(pi = faces[front_fi] )
                      each pi== pis[1]? 
                         concat( [pi] 
                              , range(len(pts),len(pts)+len(tmPts)-1)
                              )
                      : pi
                    ]      
                           
      , back_eg2 = [ for( eg = a_sort_egs[ left_fi ] )
                     if( has(eg, dir) && !has(eg, pis[1] ) ) eg
                   ][0]
                   
      , back_eg1 = [ for( eg = a_sort_egs[ right_fi ] )
                     if( has(eg, dir) && !has(eg, pis[1] ) ) eg
                   ][0]
                   
      , back_fi = get_fis_have_edges( faces , [back_eg1,back_eg2] )[0]
                                     
      , back_pis = [ for(pi = faces[back_fi] )
                      //echo("---", pi=pi) 
                      each pi== dir? 
                         concat( range(len(pts)+len(tmPts)*2-3
                                     ,len(pts)+len(tmPts)-2)
                              , [pi]
                              )
                      : pi
                    ] 
      , right_pis= [ for(pi = faces[right_fi] )
                      pi== pis[1]? ( len(pts)+len(tmPts)-2 )
                      : pi==dir? ( len(pts)+len(tmPts)*2-3 )
                      : pi
                    ]  
                      
      , S = pts[ dir ] 
      , addedfaces= [ for(i=[0:len(template)-2])  
                       /// when i=0, = indices for [A, D, E, B]
                      [ i==0? pis[1]: len(pts)+i-1 
                      , i==0? dir :len(pts)+i+len(template)-2
                      , len(pts)+i+len(template)-1
                      , len(pts)+i 
                      ]
                   ]             
      , newPts1 = len(tmPts)==2? [Pe] 
                  : movePts( slice(tmPts,1)
                           , from= ORIGIN_SITE
                           , to= [2*Q-P,Q,2*Q-R] )
      //---------------------------------------------------- 
      // Generate 2 Rf (reference pts) for angle calc. 
      // The len below is arbitrarily choice in attempt to prevent
      // negative angles.       
      , Rf_tmp =[x+dist(R,Q),y+dist(Q,P),0]
      
//      , newPts1= len(tmPts)==2? [Pe] 
//                  : [ each [for(i=[1:len(tmPts)-2])
//                             //anglePt( [Rf, Pb,Pe]
//                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
//                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
//                                    //, len=dist( tmPts[0], tmPts[i])   
//                                    //)
//                             anglePt( [ R, Q, P] 
//                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
//                                    , a= angle(
//                                          [ Rf_tmp+[1,0,0]
//                                          , Rf_tmp
//                                          , tmPts[i]
//                                          ])
//                                    , len=dist( Rf_tmp //[y-dist(Q,R),x-dist(P,Q),0]   
//                                              , tmPts[i])   
//                                    )
//                           ]                
//                    , Pe 
//                    ]       
      , newPts2= [ for(pt=newPts1) pt+ S-Q ]  
      , oldPts = [ for(i=range(pts))
                   ///  pts[4],[7] are re-assigned to A and D
                   i== pis[1] ? Pb
                   : i== dir ? (Pb+ S-Q) 
                   : pts[i]
                  ]
      , newPts = concat( oldPts, newPts1, newPts2)  
                 
                                                                  
       )
//       echo( "---------------------- calc faces")
//       echo(faces=faces)
//       echo(addedfaces=addedfaces)
       //echo(front_eg1=front_eg1)
       //echo(front_eg2=front_eg2)
       //echo(front_fi=front_fi)
       //echo(left_fi=left_fi)
       //echo(a_sort_egs = a_sort_egs)
       //echo( dir_eg = dir_eg )
       //echo( dir = dir )
       //echo(back_eg1=back_eg1)
       //echo(back_eg2=back_eg2)
       //echo(back_fi=back_fi)
//
       //echo(front_pis=front_pis)
       //echo(back_pis=back_pis)
       //echo(right_pis=right_pis)
       //
       //echo(oldPts = oldPts)
       //echo(newPts1=newPts1)
       //echo(newPts2=newPts2)
       //echo(newPts=newPts)
       //.....................................                   
     let( oldfaces = [ for(fi=range(faces))
                        fi==front_fi? front_pis
                        : fi==back_fi? back_pis
                        : fi==right_fi? right_pis
                        : faces[fi] 
                     ]            
       , newfaces = concat( oldfaces, addedfaces ) 
                     
      )
      //echo(oldfaces=oldfaces)
      [ newPts, newfaces ]
);
function decoPolyEdge_180523( pts, faces
                     , pis //, dir
                     , template= [ [-1,0], [0,-1] ] 
                     , debug=0 )=
(
   /*
      current: trying to make pis order right 
      
      180522_1: 
        previous version, decoPolyEdge_1804, has good algorithm
        but user hard to figure out the template direction and
        outcome. Also, arg *dir* seems to be (1) on a negative 
        normal of pts (2) in fact not needed when pis is not given 
   
        Achieved: template is now drawn on the [-x,-y] region
                  Works nicely but pis order still not derizable
                  ( for example, [5,4,0] but not [0,4,5] )     To make an edge-decoration on edge [4,7]:
     
     edgingPts( pts, faces
              , pis = [0,4,5]
              , template= template
              ) 
   
            6---------------7
          .'    [2]       .'|
        .'  A     x     .'  |
       5----+._-------4'    |
       |       '-._   | [5] |
       |           'B | y   |
       |             \|     |
       |     [3]      + C   .3
       |              |   .'
       |              | .'
       1--------------0'
       
     template is an array of 2d pts on XY plane: [A,B,C]
        
                  y
                  : 
                  :
       A+._- - - -+ - - - x 
           '-._   :
               'B : 
                 \: 
                  +
                  C
                      
                    
            6-----D._= 7  pts[4],[7] are re-assigned to A and D
          .'[2] .'   '-._  
        .'    .'         .E
       5----A._        .'  \
       |  = 4  '-._  .'    .F
       |           'B    .' |
       |             \ .'   |
       |     [3]      C [5] .3
       |              |   .'
       |              | .'
       1--------------0'

   //  CEFs@4= 
   // 
   //  [ 5,["L",3, "R",2, "subface",[21,20,0,3]]
   //  , 7,["L",2, "R",5, "subface",[3,2,9,8]]
   //  , 0,["L",5, "R",3, "subface",[8,11,22,21]]
   //  ] 
   */
   let( 
      //-----[ set new pts ] -----
      
        x= -template[0].x
      , y= -last(template).y
      , tmPts = to3d(template) 
      
      /* PQR = pts at pis
         
       R----Pe_-------Q
       |       '-._   |
       |           'B |
       |             \|
       |     [3]      Pb
       |              |
       1--------------P
      */
      , P = pts[pis[0]]
      , Q = pts[pis[1]]
      , R = pts[pis[2]]
      , Pb= onlinePt( [Q,P], len=y)
      , Pe= onlinePt( [Q,R], len=x)
      
     // , S = pts[dir]
      
      
      //, Rf = onlinePt( [ Q, Pe+Pb-Q], len=max(x,y)*3  ) 
//      , newPts1= len(tmPts)==2? [Pe] 
//                  : [ each [for(i=[1:len(tmPts)-2])
//                             //anglePt( [Rf, Pb,Pe]
//                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
//                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
//                                    //, len=dist( tmPts[0], tmPts[i])   
//                                    //)
//                             anglePt( [ R, P+R-Q, P] 
//                                    //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
//                                    , a= angle(
//                                          [ Rf_tmp+[1,0,0]
//                                          , Rf_tmp
//                                          , tmPts[i]
//                                          ])
//                                    , len=dist( Rf_tmp //[y-dist(Q,R),x-dist(P,Q),0]   
//                                              , tmPts[i])   
//                                    )
//                           ]                
//                    , Pe 
//                    ]        

      )
//      echo( "---------------------- calc pts")
      
    //-----[ set new faces ] -----
    let( 
      
      //-----[ set front and back faces ] -----
      
       front_eg1= quicksort( [pis[0], pis[1]] )
      , front_eg2= quicksort( [pis[2], pis[1]] )
      
      
      , a_sort_egs= get_face_edges( faces, isSortedPis=true ) 
                    // arr of sorted edges, each item represents a face
      
      , dir_eg = quicksort(                   // the edge representing the direction
                  [ for(eg=get_edges(faces)) 
                    if(has(eg,pis[1]) && !has(eg,pis[0])
                       && !has(eg,pis[2]) ) eg ][0] 
                 )
      , dir = del( dir_eg, pis[1])[0] 
      
      , front_fi  = get_fis_have_edges( faces, [front_eg1,front_eg2] )[0]
      , left_fi   = get_fis_have_edges( faces, [front_eg2,dir_eg] )[0]
      , right_fi  = get_fis_have_edges( faces, [dir_eg,front_eg1] )[0]
      , front_pis = [ for(pi = faces[front_fi] )
                      each pi== pis[1]? 
                         concat( [pi] 
                              , range(len(pts),len(pts)+len(tmPts)-1)
                              )
                      : pi
                    ]      
                           
      , back_eg2 = [ for( eg = a_sort_egs[ left_fi ] )
                     if( has(eg, dir) && !has(eg, pis[1] ) ) eg
                   ][0]
                   
      , back_eg1 = [ for( eg = a_sort_egs[ right_fi ] )
                     if( has(eg, dir) && !has(eg, pis[1] ) ) eg
                   ][0]
                   
      , back_fi = get_fis_have_edges( faces , [back_eg1,back_eg2] )[0]
                                     
      , back_pis = [ for(pi = faces[back_fi] )
                      //echo("---", pi=pi) 
                      each pi== dir? 
                         concat( range(len(pts)+len(tmPts)*2-3
                                     ,len(pts)+len(tmPts)-2)
                              , [pi]
                              )
                      : pi
                    ] 
      , right_pis= [ for(pi = faces[right_fi] )
                      pi== pis[1]? ( len(pts)+len(tmPts)-2 )
                      : pi==dir? ( len(pts)+len(tmPts)*2-3 )
                      : pi
                    ]  
                      
      , S = pts[ dir ] 
      , addedfaces= [ for(i=[0:len(template)-2])  
                       /// when i=0, = indices for [A, D, E, B]
                      [ i==0? pis[1]: len(pts)+i-1 
                      , i==0? dir :len(pts)+i+len(template)-2
                      , len(pts)+i+len(template)-1
                      , len(pts)+i 
                      ]
                   ] 
                   
                               
      //, newPts1 = len(tmPts)==2? [Pb] 
                  //: movePts( slice(tmPts,1)
                           //, from= ORIGIN_SITE
                           //, to= [2*Q-R,Q,2*Q-P ] )
                           //
                           
                           
                           
      //---------------------------------------------------- 
      // Generate 2 Rf (reference pts) for angle calc. 
      // The len below is arbitrarily choice in attempt to prevent
      // negative angles.       
      , Rf_old =[ max(maxx(tmPts),0)+0.5
                , max(maxy(tmPts),0)+0.5 ,0 ]
      , Rf_new = onlinePt([Q,R],len=-Rf_old.x)
                + onlinePt([Q,P],len=-Rf_old.y)
                - Q  
                 
      , newPts1= len(tmPts)==2? [Pb] 
                  : [ each 
                      [for(i=[1:len(tmPts)-2])
                         //anglePt( [Rf, Pb,Pe]
                                //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
                                //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
                                //, len=dist( tmPts[0], tmPts[i])   
                                //)
                         anglePt( [ Pe, Rf_new, Pb] 
                                //, a= angle([Rf_tmp,tmPts[0], tmPts[i]])
                                , a= angle(
                                      [ tmPts[0] //Rf_tmp+[1,0,0]
                                      , Rf_old
                                      , tmPts[i]
                                      ])
                                , len=dist( Rf_old //[y-dist(Q,R),x-dist(P,Q),0]   
                                          , tmPts[i])   
                                )
                       ]                
                    , Pb 
                    ]       
      , newPts2= [ for(pt=newPts1) pt+ S-Q ]  
      , oldPts = [ for(i=range(pts))
                   ///  pts[4],[7] are re-assigned to A and D
                   i== pis[1] ? Pe
                   : i== dir ? (Pe+ S-Q) 
                   : pts[i]
                  ]
      , newPts = concat( oldPts, newPts1, newPts2)  
      , oldfaces = [ for(fi=range(faces))
                        fi==front_fi? front_pis
                        : fi==back_fi? back_pis
                        : fi==right_fi? right_pis
                        : faces[fi] 
                     ]            
       , newfaces = concat( oldfaces, addedfaces ) 
                     
      )           
      debug?                                                                  

       echo( "---------------------- calc faces")
       echo(pts=pts)
       echo( pis = pis )
      echo( x=x,y=y )
      echo( tmPts = tmPts )
        echo( Pb=Pb )
        echo( Pe=Pe )
      echo(Rf_old=Rf_old)
      echo(Rf_new=Rf_new)
      echo( old_site = [ tmPts[0], Rf_old, last(tmPts) ])
      echo( new_site = [ Pe, Rf_new, Pb])
      echo( oldPts=oldPts)
      echo( newPts1=newPts1 )
      echo( newPts2=newPts2 )
      echo( newPts = newPts)
       
       echo(faces=faces)
       echo(addedfaces=addedfaces)
       echo(front_eg1=front_eg1)
       echo(front_eg2=front_eg2)
       echo(front_fi=front_fi)
       echo(left_fi=left_fi)
       echo(a_sort_egs = a_sort_egs)
       echo( dir_eg = dir_eg )
       echo( dir = dir )
       echo(back_eg1=back_eg1)
       echo(back_eg2=back_eg2)
       echo(back_fi=back_fi)

       echo(front_pis=front_pis)
       echo(back_pis=back_pis)
       echo(right_pis=right_pis)
       
       echo( new_site = [2*Q-R,Q,2*Q-P] )
       echo(oldPts = oldPts)
       echo(newPts1=newPts1)
       echo(newPts2=newPts2)
       echo(newPts=newPts)
       //.....................................                   
      //echo(oldfaces=oldfaces)
       [ newPts, newfaces ]
      :[ newPts, newfaces ]
);
function decoPolyEdge( pts, faces
                     , edge
                     , template //= [ [-1,0], [0,-1] ]
                     , opt=[] 
                     , debug=0 )=
(
   /* 
      Decorate an edge of a poly (currently only cube poly)
      based on template, return [newpts, newfaces].
      
      edge: [pi,pj] where pi, pj is the indices of pts 
      template: 2D-pts. 
      
      
      current: trying to use "edge" instead of "pis" for easier 
               usage. And, try to use one data structure but not
               calc every var independently
               
      180523: pis in right order (the target edge is the normal
              of pis)
      180522_1: 
        previous version, decoPolyEdge_1804, has good algorithm
        but user hard to figure out the template direction and
        outcome. Also, arg *dir* seems to be (1) on a negative 
        normal of pts (2) in fact not needed when pis is not given 
   
        Achieved: template is now drawn on the [-x,-y] region
                  Works nicely but pis order still not derizable
                  ( for example, [5,4,0] but not [0,4,5] )     
         
    //----------------              
    To make an edge-decoration on edge [4,7]:
     
     edgingPts( pts, faces
              , edge = [4,7]
              , template= template
              ) 
   
            6---------------7
          .'    [2]       .'|
        .'  A     x     .'  |
       5----+._-------4'    |
       |       '-._   | [5] |
       |           'B | y   |
       |             \|     |
       |     [3]      + C   .3
       |              |   .'
       |              | .'
       1--------------0'
       
     template is an array of 2d pts on XY plane: [A,B,C]
        
                  y
                  : 
                  :
       A+._- - - -+ - - - x 
           '-._   :
               'B : 
                 \: 
                  +
                  C
                      
                    
            6-----D._= 7  pts[4],[7] are re-assigned to A and D
          .'[2] .'   '-._  
        .'    .'         .E
       5----A._        .'  \
       |  = 4  '-._  .'    .F
       |           'B    .' |
       |             \ .'   |
       |     [3]      C [5] .3
       |              |   .'
       |              | .'
       1--------------0'

   //  CEFs@4= 
   // 
   //  [ 5,["L",3, "R",2, "subface",[21,20,0,3]]
   //  , 7,["L",2, "R",5, "subface",[3,2,9,8]]
   //  , 0,["L",5, "R",3, "subface",[8,11,22,21]]
   //  ] 
   */
   let(
   
      // Process template 
        pts = arg(pts, opt, "pts")
      , faces= arg(faces, opt, "faces", CUBEFACES)
      , edge = arg(edge, opt, "edge", [0,1])
      , template= arg(template, opt, "template") 
      , x= -template[0].x
      , y= -last(template).y
      , tmPts = to3d(template) 
      , tmPts_x_len = (maxx(tmPts)-minx(tmPts))*3
      , tmPts_y_len= (maxy(tmPts)-miny(tmPts))*3 
      
      // Locate faces and edges
                
      , e0 = edge[0]
      , e1 = edge[1]
      , e0CEF = get_clocked_EFs_at_pi(faces, e0)  
      , e1CEF = get_clocked_EFs_at_pi(faces, e1)  
               // A clocked_EFs of a pt @ pi is defined as a hash of {edge_end:hFis}
               // like:
               //
               //  [ 3,["L",5, "R",0, "subface",[21,20,0,3]]
               //  , 1,["L",0, "R",2, "subface",[3,2,9,8]]
               //  , 4,["L",2, "R",5, "subface",[8,11,22,21]]
               //  ]   
               //
               // where the keys, 3,1,4 means: 
               // -- there are 3 edges connected to pi: [pi,3], [pi,1], [pi,4]
               // -- thses 3 edges are arranged in a clock-wize order
               //
               // The value is another hash containing the face-indices of rise and left faces,
               // and the subface of the corresponding edge.
      , iNearPis = keys(e0CEF) 
      , jNearPis = keys(e1CEF)
  
      //        U .---------------. T   T= pts[e1]
      //        .'    [2]       .'|      
      //      .'  Pe    x     .'  |
      //    R'----+._-------Q'    |     Q= pts[e0]
      //     |   A   '-._   | [5] |
      //     |           'B | y   |
      //     |             \|     |
      //     |     [3]    C + Pb  . S
      //     |              |   .'
      //     |              | .'
      //     1--------------P'              
          
      , iR = prev( iNearPis, e1)
      , iP = next( iNearPis, e1)
      , iS = prev( jNearPis, e0) 
      
      , Lfi = hashs( e0CEF, [e1,"L"] )  // Lfi= 2 in above case 
      , Rfi = hashs( e0CEF, [e1,"R"] )  // Lfi= 5 in above case
      , Bfi = hashs( e0CEF, [iR,"L"] )  // Lfi= 3 in above case
      , Ffi = hashs( e1CEF, [iS,"L"] )  
      
      // Locate pts
      
      , P = pts[iP]
      , Q = pts[e0]
      , R = pts[iR]
      , S = pts[iS]
      , T = pts[e1]    
        
      , Pb= onlinePt( [Q,P], len=y) // Where the template begins on obj
      , Pe= onlinePt( [Q,R], len=x) // Where the template ends on obj     
  
      // Make new faces
      
      , new_Bpis = [ for(pi = faces[Bfi] ) // new pis for backward face 
                      each pi== e0?         
                           concat( [pi] 
                              , range(len(pts),len(pts)+len(tmPts)-1)
                              )
                      : pi
                    ]  
                       
      , new_Fpis = [ for(pi = faces[Ffi] ) // new pis for forward face
                      each pi== e1? 
                         concat( range(len(pts)+len(tmPts)*2-3
                                     ,len(pts)+len(tmPts)-2)
                              , [pi]
                              )
                      : pi
                    ]   
      , new_Rpis= [ for(pi = faces[Rfi] ) // new pis for right face
                pi== e0? ( len(pts)+len(tmPts)-2 )
                : pi==e1? ( len(pts)+len(tmPts)*2-3 )
                : pi
              ]
      , facesToAdd= [ for(i=[0:len(template)-2])  
                       /// when i=0, = indices for [A, D, E, B]
                      [ i==0? e0: len(pts)+i-1 
                      , i==0? e1 :len(pts)+i+len(template)-2
                      , len(pts)+i+len(template)-1
                      , len(pts)+i 
                      ]
                   ]                   
      , oldfaces = [ for(fi=range(faces))
                        fi==Bfi? new_Bpis
                        : fi==Ffi? new_Fpis
                        : fi==Rfi? new_Rpis
                        : faces[fi] 
                     ]
      , newfaces = concat( oldfaces, facesToAdd )
            
      // Make new pts
      
      // Generate 2 Rf (reference pts) for angle calc. 
      // The len below is chosen in attempt to prevent
      // negative angles.
        
      , Rf_old =[ tmPts_x_len
                , tmPts_y_len ,0 ]
      , old_Rf_site= [ [ tmPts_x_len,0,0]
                     , Rf_old
                     ,  ORIGIN 
                     ]            
      , Rf_new = onlinePt([Q,R],len=-Rf_old.x)
                + onlinePt([Q,P],len=-Rf_old.y)
                - Q  
      , new_Rf_site = [ projPt([Q,R], Rf_new) 
                      , Rf_new, Q
                      ]           
      , newPts1= len(tmPts)==2? [Pb] 
                  : [ each 
                      [ for(i=[1:len(tmPts)-2])
                         anglePt( new_Rf_site 
                                , a= angle( [ old_Rf_site[0]
                                            , old_Rf_site[1] 
                                            , tmPts[i]     
                                            ]
                                          )
                                , len= dist( Rf_old, tmPts[i])   
                                )
                       ]                
                    , Pb 
                    ] 
     , newPts2= [ for(pt=newPts1) pt+ T-Q ]
       
     , oldPts = [ for(i=range(pts))
                     ///  pts[4],[7] are re-assigned to A and D
                     i== e0 ? Pe
                     : i== e1 ? (Pe+ T-Q) 
                     : pts[i]
                    ]   
                               
      , newPts = concat( oldPts, newPts1, newPts2 )
        
      )           
     debug? 
                                                                       
     echo( _bcolor("------ [ decoPolyEdge debugging ] ------","orange"))
     echo( _red( "Input paras:") )
     echo( pts=pts )
     echo( faces=faces )
     echo( edge= edge )
     echo( template=template )
      
     echo( _red( "Process template:") )
     echo( x= x )
     echo( y= y )
     echo( tmPts = tmPts) 
     echo( tmPts_x_len = tmPts_x_len) 
     echo( tmPts_y_len = tmPts_y_len) 

     echo( _red( "Locate needed faces and edges :") )
     echo( e0CEF = e0CEF )  
     echo( e1CEF = e1CEF )  
     echo( iNearPis = iNearPis ) 
     echo( jNearPis = jNearPis )
     echo( iR = iR )
     echo( iP = iP )
     echo( iS = iS )
     echo( Lfi = Lfi )
     echo( Rfi = Rfi )
     echo( Bfi = Bfi )     
     echo( Ffi = Ffi )
                
     echo( _red( "Locate needed pts :") )
     echo( P = P )
     echo( Q = Q )
     echo( R = R )
     echo( S = S )
     echo( T = T )      
     echo( Pb= Pb)
     echo( Pe= Pe)     

     echo( _red( "Make new faces:") )
     echo( new_Bpis = new_Bpis )
     echo( new_Fpis = new_Fpis )
     echo( new_Rpis = new_Rpis )
     echo( oldfaces= oldfaces) 
     echo( facesToAdd= facesToAdd) 
     echo(newfaces = newfaces)          
     
     echo( _red( "Make new pts:") )
     echo( Rf_old = Rf_old )
     echo( old_Rf_site = old_Rf_site )  
     echo( Rf_new = Rf_new )
     echo( new_Rf_site = new_Rf_site )
     echo(newPts1=newPts1)
     echo(newPts2=newPts2)
     echo(oldPts = oldPts)
     echo(newPts=newPts)

     [ newPts, newfaces ]
     :[ newPts, newfaces ]
);
//color("purple")
//{MarkPts(   [[0, 0, 0], [0, 3, 0], [0, 0, 0]]);
//MarkPts(    [[3, 1, 3], [3, 1, 6], [3, 1, 3]] );
//}

function decoPolyEdges( pts, faces, decos, opt=[], _i=-1, debug=0)=
( // decos = [ [<edge>,<template1>, debug]
  //         , [<edge>,<template2>, debug], ... 
  //         , [<edge>,debug], ... 
  //         , [<edge>], ... 
  //]
  /*
   If template of an item x is not given, use the very last template before x.
   For example, the 3rd and 4th items in the 4 items above will use 
   template2.
    
   decoPolyEdges( decos=[ [[4,7],tmp], [[0,3]] ]
                 , opt=[ "pts", pts, "faces",faces ]
                , debug= debug
                )
  */                       
  _i==-1?
  let( pts = arg(pts, opt, "pts")
     , faces= arg(faces, opt, "faces", CUBEFACES)
     , decos = arg(decos, opt, "decos", []) 
     )
     debug?
       //echo("decoPolyEdges() init")
       //echo( decos = decos ) 
       decoPolyEdges( pts=pts, faces=faces, decos=decos, _i=0, debug=debug)
     :decoPolyEdges( pts=pts, faces=faces, decos=decos, _i=0, debug=debug)
  : _i==len(decos)?
   [pts,faces]
  : 
   //echo("decoPolyEdges, _i=", _i)
   //echo( pts= pts )
   //echo( faces = faces )
   //echo( decos_i = decos[_i] )
   let( deco = decos[_i]
      , edge = isnum(deco[0])? deco: deco[0]
      , pf = decoPolyEdge( 
              pts=pts, faces=faces
            , edge= edge
            , template= isarr(deco[1])?deco[1] 
                        : last( [ for(i=[0:_i-1])
                                  if(isarr(decos[i][1])) decos[i][1] ] )
            //, template= decos[ !isarr(decos[_i][1])?0:_i ][1]
                        // If deco doesn't have template, use the 
                        // template of the first deco
            , debug= 0 //decos[_i][2]
             ) 
       )                       
   decoPolyEdges( pts=pf[0], faces=pf[1], decos=decos, _i=_i+1, debug=debug)
);

//function endPtsInfo(pts,ref)= //  the [i,pt,dist] of the 2 end pts from ref (2018.9.16)
//(
//   let( sorted=  sortArrs( 
//                  [ for(i=range(pts)) [ i, pts[i], dist(pts[i],ref) ] ], by=2  
//                 )
//      )
//   [sorted[0], last(sorted)]               
//);
//

function endPtsInfoAlongLine(pts,line)= // Return two ends along line direction
(                               // = [ [ i0, pt0, dist0], [i1, pt1, dist1] ]
                                // If both dists have same sign, then these 2 pts
                                //   are separated by abs(dist0-dist1). Otherwise,
                                //   dist0+dist1 
                                // (2018.9.16) 
   let( lv=1
      , Q = (line[0]+line[1])/2
      , Rf= randPt()
      , N = N([line[0], Q, Rf])
      , B = N([N,Q,line[0]])
      , sorted = sortByDist( pts, [B,Q,N], isAbs=0)
      )
    //echo( log_b("endPtsInfoAlongLine()", lv) )
    //echo( log_(["line",line], lv) )
    //echo( log_(["N",N,"B",B], lv) )
    //echo( log_(["sorted", sorted], lv) )
    //echo( log_e("", lv) )
    [ sorted[0], last(sorted)]  
);

//========================================================
{ // expandPts
function expandPts(pts,dist=0.5, cycle=true, _i_=0)= 
(	
	[ for(i=range(pts))
        let( p3 = get3(pts,i-1))
		onlinePt( [pts[i], angleBisectPt( p3 )]
				, len=-dist/sin(angle(p3)/2)
			    )
	]
);

    expandPts=["expandPts", "pts,dist=0.5,cycle=true", "points", "Point",
    " Given an array of points (pts), return an array of points that *expand* every 
    ;; point outward as indicated by Q ==> Q2 in the graph below:
    ;;
    ;; d = dist
    ;;
    ;; ---------------------Q2
    ;;                     /
    ;;                    /
    ;; P----------Q_     /
    ;;           /  '-_ /
    ;;          /   d  :
    ;;         /      /
    ;;        /      /
    ;;       R      /
    "
    ]; 

    function expandPts_test( mode=MODE, opt=[] )=
    (
        doctest( expandPts, mode=mode, opt=opt )
    );
	
} // expandPts 

//========================================
//{ // extPt
//function extPt(pq)= // Return a Pt that is 20 unit of distance
//                    // away from Q along the pq line. This is 
//                    // a torn down version of onlinePt, for the
//                    // simple purpose of getting a pt beyond Q.
//                    // So how far away from Q is not important.
//(
//   onlinePt( [pq[1],pq[0]], -20 )
//);
//} // extPt

{ // extendPts --- renamed from linePts on 2018.10.23
function extendPts(pq,len=undef, ratio=[1,1])= 
 let( len=isnum(len)?[len,len]:len
	, ratio=isnum(ratio)?[ratio,ratio]:ratio
	)
( 
   [ onlinePt( pq,  len= -len[0], ratio=-ratio[0])
   , onlinePt( p10(pq), len= -len[1], ratio=-ratio[1])
   ]
);		

    extendPts=["extendPts","pq, len, ratio", "pq", "Point, Line",
     " Extend/Shrink line. Given a 2-pointer (pq) and len or ratio , return 
    ;; a new 2-pointer for a line with extended/shortened lengths. 
    ;;
    ;; Both len and ratio can be positive|negative. Positive= extend, negative=shrink.
    ;; 
    ;; They can be either 2-item array (like [1,0]) or a number. If a number, it's the
    ;; same as [number,number].
    ;;
    ;; If array [a,b], a,b for p, q side, respectively. For example, 
    ;;
    ;;   extendPts( pq, len=-1): both sides shrink by 1
    ;;   extendPts( pq, len=[1,1]): both sides extends by 1

    ;; Compared to onlinePt, this function:
    ;; (1) is able to extend both ends. 
    ;; (2) return 2-pointers.
    ;;
    ;; extendPts(pq, len=1) = [ onlinePt( pq, len=-1)
    ;;                      , onlinePt( p10(pq), len=-1 ]
    ;; 
    ;; application: tanglnPt
    ;;
    ;; NOTE: renamed from linePts on 2018.10.23
    "];

    function extendPts_test( mode=MODE, opt=[] )=
    (
        doctest( extendPts,mode=mode, opt=opt )
    );

}// extendPts

//. f,g

 
////========================================================
//          
//
//========================================================
//function farthestPtInfo(pts,Rf)= // [i, pt, dist] of the pt farthest to ref (2018.9.16)
//(
//   last( sortByDist(pts,Rf,isAbs=1) )    
//);
//

{ // getBezierPtsAt
function getBezierPtsAt(  // Return Cubic Bezier pts from 
                          // pts[i] to pts[i+1].
   pts
   , i //=1 
   , n //=6 // extra pts to be added 
   , closed //=false
   , ends  //= [true,true] to include both end pts
   , aH //= undef //[1,1]
   , dH  //= undef //[0.5,0.5]
   , opt=[]
   )=
(
 /*
 When pts is 4 pts:
    
 1) Make an abi pt, A1, on the PQR below
 2) From there, make a tangent pt, G, which
    is the HCP(Handle Control Point) of Q  
 3) Do the same HCP H on R. 
 4) Do a Cubic Bezier Curve calc on Q,R
    with G,H as their HCP, resp. 
     
            G--...__
    dH1=   :        ``H
     dQR  :   _.---._  : dH2 = dQR (default)
         :_-``       `-_:
        Q`---------------R       
       : \`'-_      _-'`/ :
      :   \   A1  A2   /   :
           \          /     : 
          X=P        S

   When pts is a 3-pointer, we insert a X
   as the 1st pt, which is a mirror of S. 
   
 The shape of resultant Bezier curve depends
 on aH and dH: 
 
 aH (angle of handler, aGQR and aQRH).
    aH is entered as a ratio of the default aGQR 
    or aQRH shown above. aH=1: default. Set aH 
    an array to set aH1 and aH2 separately.
    Deafult: [0.4,1]  ( len(pts)==3 )
             [     ]  ( len(pts)>3 )
             
 dH dQR. Again, enter as a ratio of dQR.
    dH = 0.5 (default) means, both dQG and dRH
    are half of dQR. dH= 
    Or a single ratio
    to set both the same
    Deafult: [0.5,0.3]  ( len(pts)==3 )
             [       ]  ( len(pts)>3 )
             
*/
    let(
    
     //------------------------------------------------
     
       opt = popt(opt) // handle sopt
       
     , pts = or(pts, hash( opt, "pts")) 
     , n   = or(n  , hash( opt, "n", 6))
     , i   = fidx(pts, isnum(i)?i:hash(opt,"i",i)) 
      , _aH = und( aH, hash(opt,"aH", 0.5) )
      , aH  = isarr(_aH)?_aH:[_aH,_aH]
      , _dH = und( dH, hash(opt,"dH", 0.38) )
      , dH  = isarr(_dH)?_dH:[_dH,_dH]
      , closed= und( closed, hash( opt, "closed", false) )
     
//     , _aH = aH==undef? hash(opt,"aH", 0.5):aH
//     , aH  = isarr(_aH)?_aH:[_aH,_aH]
//     , _dH = dH==undef? hash(opt,"dH", 0.38):dH
//     , dH  = isarr(_dH)?_dH:[_dH,_dH]
//     , closed= closed==undef? closed:hash( opt, "closed", false)
     , ends= or(ends, hash( opt, "ends", [true,true]))
     
     //------------------------------------------------
     
    , isend= i==len(pts)-1 
     
    , pqrs= 

        i==0?
             //   Q-----------R     at= 0, a1 be set = a2       
             //     a1     a2 |         
             //               S       
             [ for(i=[0])
                let( 
                     Q = pts[0]
                   , R = pts[1]
                   , S = pts[2] 
                   , P = closed? last(pts)
                         :anglePt( [ R,Q,S],a=angle([Q,R,S]))
                   )
                [P,Q,R,S]
            ][0] 
             
       : isend?
            // Q-----------R(0)   at= last angle 
            //  \ a1    a2  |     possible only when closed
            //   p         S(1)      
             
            // [ for(ii=[0])
                let( ii= len(pts)-1
                   , P = pts[ii-1]
                   , Q = pts[ii]
                   , R = pts[0] 
                   , S = pts[1]
                   )
                [P,Q,R,S]
            //][0]
       : i== len(pts)-2?
             
            //  Q-----------R   
            //   \ a1    a2     
            //    p         S(0)      
             
             //[ for(ii=[0])
                let( 
                     P = pts[i-1]
                   , Q = pts[i]
                   , R = pts[i+1] 
                   , S = closed? pts[0]
                         : anglePt( [ Q,R,P],a=angle([P,Q,R]))
                                
                   )
                [P,Q,R,S]
           // ][0]
       : get(pts, [i-1,i,i+1,i+2])
    
    ,P = pqrs[0]
    ,Q = pqrs[1]
    ,R = pqrs[2]
    ,S = pqrs[3]
             
    ,dQR= dist(Q,R)
    ,Xq = onlinePt( [Q,P], len=-1)         
    ,Xr = onlinePt( [R,S], len=-1)         
    ,Hi = anglePt( [R,Q,Xq], a= (180-angle([R,Q,P]))*aH[0]
                          , len= dQR* dH[0] )
    ,Hj = anglePt( [Q,R,Xr], a= (180-angle([Q,R,S]))*aH[1]
                          , len= dQR* dH[1] )
             
   , bzpts = [ for (ii=range(n+2))
                let( r= ii/(n+1)
                  , P1= onlinePt( [Q,Hi], ratio= r )
                  , P2= onlinePt( [Hi,Hj], ratio= r ) 
                  , P3= onlinePt( [Hj,R], ratio= r )
                  , PP1= onlinePt( [P1,P2], ratio= r ) 
                  , PP2= onlinePt( [P2,P3], ratio= r ) 
                  )
                onlinePt( [PP1,PP2], ratio=r)
             ]
   ) // let   
/*
       Hi--...__
       :        ``Hj
   dH1:   _.---._  : dH2 = dRS/2 (default)
     :_-``       `-_:
    Q`---------------R       
   : \`'-_      _-'`/ :
  :   \   A1  A2   /   :
       \          /     : 
        X=P      S
*/
   ["bzpts", ends[0]&&ends[1]? bzpts
             :ends[0]? slice(bzpts,0,-1)
             :ends[1]? slice(bzpts,1)
             : slice(bzpts,1,-1)
   ,"pts", pts
   ,"Hi", Hi
   ,"Hj", Hj 
   ,"aH", aH
   ,"dH", dH
   ,"i",i
   ,"n",n
   ,"pqrs", [P,Q,R,S]
   ] 
   
//   [ //bzpts, 
//     ends[0]&&ends[1]? bzpts
//     :ends[0]? slice(bzpts,0,-1)
//     :ends[1]? slice(bzpts,1)
//     : slice(bzpts,1,-1)
//     
//   ,[P,Q,R,S]
//   ,[ [Hq, aH,dH], [Hr,aH,dH]]
//   ] 
);
} // getBezierPtsAt
//============================================
{ // getBezierPtsAt_150629_working
function getBezierPtsAt_150629_working( 
   pts
   , at=1 
   , n=6 // extra pts to be added 
   , closed=false
   , ends= [true,true]
   , aH= undef //[1,1]
   , dH= undef //[0.5,0.5]
   )=
(
 /*
 When pts is 4 pts:
    
 1) Make an abi pt, A1, on the PQR below
 2) From there, make a tangent pt, G, which
    is the HCP(Handle Control Point) of Q  
 3) Do the same HCP H on R. 
 4) Do a Cubic Bezier Curve calc on Q,R
    with G,H as their HCP, resp. 
     
            G--...__
    dH1=   :        ``H
     dQR  :   _.---._  : dH2 = dQR (default)
         :_-``       `-_:
        Q`---------------R       
       : \`'-_      _-'`/ :
      :   \   A1  A2   /   :
           \          /     : 
          X=P        S

   When pts is a 3-pointer, we insert a X
   as the 1st pt, which is a mirror of S. 
   
 The shape of resultant Bezier curve depends
 on aH and dH: 
 
 aH (angle of handler, aGQR and aQRH).
    aH is entered as a ratio of the default aGQR 
    or aQRH shown above. aH=1: default. Set aH 
    an array to set aH1 and aH2 separately.
    Deafult: [0.4,1]  ( len(pts)==3 )
             [     ]  ( len(pts)>3 )
             
 dH dQR. Again, enter as a ratio of dQR.
    dH = 0.5 (default) means, both dQG and dRH
    are half of dQR. dH= 
    Or a single ratio
    to set both the same
    Deafult: [0.5,0.3]  ( len(pts)==3 )
             [       ]  ( len(pts)>3 )
             
*/
    let( 
     // Make a temp X if pts is 3-ptr. This X is a
     // mirror of pts[2] and will be pts[0]
     //isL3 = len(pts)==3

     //,aQRS = angleAt( pts, isbeg==0? 1:2 )
     at = fidx(pts,at)
     , atend= at==len(pts)-1 
//       , aH= aH==undef?( at==0?[0.6,0.95]:atend?[0.95,0.6]:[0.95,0.95] )
//             :isarr(aH)?aH:[aH,aH]
//       , dH= dH==undef?( at==0?[0.45,0.3]:atend?[0.3,0.45]:[0.35,0.35] )
//             :isarr(dH)?dH:[dH,dH]
       , aH= aH==undef?[0.5,0.5]
             :isarr(aH)?aH:[aH,aH]
       , dH= dH==undef?[0.38,0.38]
             :isarr(dH)?dH:[dH,dH]
     
     , pqrs= 

        at==0?
             //   Q-----------R     at= 0, a1 be set = a2       
             //     a1     a2 |         
             //               S       
             [ for(i=[0])
                let( 
                     Q = pts[0]
                   , R = pts[1]
                   , S = pts[2] 
                   , P = closed? last(pts)
                         :anglePt( [ R,Q,S],a=angle([Q,R,S]))
                   )
                [P,Q,R,S]
            ][0] 
             
       : atend?
            // Q-----------R(0)   at= last angle 
            //  \ a1    a2  |     possible only when closed
            //   p         S(1)      
             
             [ for(i=[0])
                let( 
                     P = pts[at-1]
                   , Q = pts[at]
                   , R = pts[0] 
                   , S = pts[1]
                   )
                [P,Q,R,S]
            ][0]
       : at== len(pts)-2?
             
            //  Q-----------R   
            //   \ a1    a2     
            //    p         S(0)      
             
             [ for(i=[0])
                let( 
                     P = pts[at-1]
                   , Q = pts[at]
                   , R = pts[at+1] 
                   , S = closed? pts[0]
                         : anglePt( [ Q,R,P],a=angle([P,Q,R]))
                                
                   )
                [P,Q,R,S]
            ][0]
       : get(pts, [at-1,at,at+1,at+2])
    
    ,P = pqrs[0]
    ,Q = pqrs[1]
    ,R = pqrs[2]
    ,S = pqrs[3]
             
    ,dQR= dist(Q,R)
    ,Xq = onlinePt( [Q,P], len=-1)         
    ,Xr = onlinePt( [R,S], len=-1)         
    ,Hq = anglePt( [R,Q,Xq], a= (180-angle([R,Q,P]))*aH[0]
                          , len= dQR* dH[0] )
    ,Hr = anglePt( [Q,R,Xr], a= (180-angle([Q,R,S]))*aH[1]
                          , len= dQR* dH[1] )
             
   , bzpts = [ for (i=range(n+2))
                let( r= i/(n+1)
                  , P1= onlinePt( [Q,Hq], ratio= r )
                  , P2= onlinePt( [Hq,Hr], ratio= r ) 
                  , P3= onlinePt( [Hr,R], ratio= r )
                  , PP1= onlinePt( [P1,P2], ratio= r ) 
                  , PP2= onlinePt( [P2,P3], ratio= r ) 
                  )
                onlinePt( [PP1,PP2], ratio=r)
             ]
   ) // let   
/*
        G--...__
       :        ``H
   dH1:   _.---._  : dH2 = dRS/2 (default)
     :_-``       `-_:
    Q`---------------R       
   : \`'-_      _-'`/ :
  :   \   A1  A2   /   :
       \          /     : 
        X=P      S
*/
    //[Hp,Hq]  
    //pqrs 
   [ //bzpts, 
     ends[0]&&ends[1]? bzpts
     :ends[0]? slice(bzpts,0,-1)
     :ends[1]? slice(bzpts,1)
     : slice(bzpts,1,-1)
     
   ,[P,Q,R,S]
   ,[ [Hq, aH,dH], [Hr,aH,dH]]
   ] 


);
} // getBezierPtsAt_150629_working

//========================================================
{ // getBezier_simple
function getBezier_simple( pq,rs, n=6)=
(
   /* Return a list of pts between P and Q (include P,Q)
      representing the Cubic Bezier Curve. 
      The R,S in st are the Handle Control Points
      for P,Q, respectively. 
      
      n: number of points filled in between P and Q
      Return: [P, p1,p2...pn, Q]
      
          R--.._
         :      ``'-S
        :   _.---.  |  
       :_-``      `.|
      P`            Q
      
      Ref: 
      
      1. Cubic Bezier Curves - Under the Hood
      https://vimeo.com/106757336
      
      2. A Primer on Bezier Curves
      http://pomax.github.io/bezierinfo 
   */
   let( pqrs= rs==undef? 
               ( len(pq)==3? [pq[0],pq[2],pq[1],pq[1]]
                 : pq )
              //: typeplp(pq)=="pl"? app(pq,rs)
              : gtype(pq)=="pl"? app(pq,rs)
              : concat( pq,rs)
      , P=pqrs[0]   
      , Q=pqrs[1]
      , R=pqrs[2]
      , S=pqrs[3]
      , bz= [for (i=range(n+2))
               let( r= i/(n+1)
                  , P1= onlinePt( [P,R], ratio= r )
                  , P2= onlinePt( [R,S], ratio= r ) 
                  , P3= onlinePt( [S,Q], ratio= r )
                  , PP1= onlinePt( [P1,P2], ratio= r ) 
                  , PP2= onlinePt( [P2,P3], ratio= r ) 
                  )
               onlinePt( [PP1,PP2], ratio=r)
           ]
      )
   bz
);
} // getBezier_simple

{ // getbonedata 
function getbonedata( pts, Rf )=
(  /*
       Return a bone data representing a bone (chain pts )
   
       bonedata: array of [ ["seed",, "len", ]
                          , [ "len", n  (len of seg AFTER first node) 
                            , "a",   a  (1st angle of node)
                            , "a2",  a  (2nd angle of node)
                            , "refpl", pl (pl for a,a2 to calc pt[i+1] ) 
                            , "J"    , J (projPt of pt[i+1] on refpl)
                            ]
                          , [ ...]
                          ]
                          
      These info is sufficient to duplicate pts at any
      location.      
   */
   
   // Return: [ [a1,a2], [a1,a2]....] ( total of len(pts)-2) items
   // Represent the turning angles of each node in pts
   let( Rf= und(Rf, pts[2])
      , seed= [pts[0], pts[1], Rf]
      )
   concat( [[ "seed", seed
           , "len", dist( p01(seed) ) 
           ]]
     , [ // For each i, decides a,a2 at i by taking i-1,i,i+1
         for( i= [1:len(pts)-2] )
           
           let( refpl = i==1? seed: get(pts,[i-1,i,i-2])
                , J = projPt( pts[i+1], refpl )
                , PQJ = [pts[i-1], pts[i], J]
                , JQR = [J, pts[i], pts[i+1]]
                , a= angle( PQJ, Rf= i==1?Rf:N(refpl))
                , a2= angle( JQR, Rf= i==1?Rf:N(JQR))
                , len = dist( pts, ij=[i,i+1])
                )
           [ "a", i==1?abs(a):a
           , "a2", a2 
           , "len" , len
           , "refpl", refpl
           , "J", J
           ] 
       ]
     )    
);
    getbonedata = ["getbonedata", "pts,Rf", "array", "Geometry"
    ," Return a bone data representing a bone (chain pts )
    ;;
    ;;   bonedata: array of [ ['seed',, 'len', ]
    ;;                      , [ 'len', n  (len of seg AFTER first node) 
    ;;                        , 'a',   a  (1st angle of node)
    ;;                        , 'a2',  a  (2nd angle of node)
    ;;                        , 'refpl', pl (pl for a,a2 to calc pt[i+1] ) 
    ;;                        , 'J'    , J (projPt of pt[i+1] on refpl)
    ;;                        ]
    ;;                      , [...] // for 2nd node 
    ;;                      , ...
    ;;                      ]
    ;;                      
    ;;  These info is sufficient to duplicate pts at any location.
    ;;  See how it is done inside trsfbonePts()
    ;;
    ;; Dev Note: it might be unnecessary to go thru this 
    ;;  getbonedata() => trsfbonePts( which is a recursion)
    ;; See trsfPts()
    "
    ,"trsfPts"
    ];
         
} //getbonedata


//========================================================   
{ // getBoxData
function getBoxData(pts, pqr=[ [1,0,0],[0,0,0],[0,1,0]] )=
(    
    let( P=pqr[0], Q=pqr[1], R=pqr[2], N=N(pqr), B=B(pqr)
       , pqb = [ P,Q,B ]  
       , nqp = [ N,Q,P ]
       , bqn = [ B,Q,N ]
       , rng = range(pts)
       
       // Sorted [dist,pt,i]
       , dpis_bqn = sort([ for(i=rng) [ dist(bqn,pts[i]), pts[i],i ] 
                         ])       
       , dpis_nqp = sort([ for(i=rng) [ dist(nqp,pts[i]), pts[i],i ] 
                         ])       
       , dpis_pqb = sort([ for(i=rng) [ dist(pqb,pts[i]), pts[i],i ] 
                         ])
       
       , pmin = dpis_bqn[0]
       , pmax = last(dpis_bqn)
       , bmin = dpis_nqp[0]
       , bmax = last(dpis_nqp)
       , nmin = dpis_pqb[0]
       , nmax = last(dpis_pqb)       

       // Pts on the box surfaces (i.e., outer limit in terms of pqr coord ) 
       , Pmin = dpis_bqn[0][1]
       , Pmax = last(dpis_bqn)[1]
       , Bmin = dpis_nqp[0][1]
       , Bmax = last(dpis_nqp)[1]
       , Nmin = dpis_pqb[0][1]
       , Nmax = last(dpis_pqb)[1]
       
       // Box surfaces 
       , bqnMin = mvPts( bqn, Pmin-projPt( Pmin, bqn) )    
       , bqnMax = mvPts( bqn, Pmax-projPt( Pmax, bqn) )    
       , nqpMin = mvPts( nqp, Bmin-projPt( Bmin, nqp) )    
       , nqpMax = mvPts( nqp, Bmax-projPt( Bmax, nqp) )    
       , pqbMin = mvPts( pqb, Nmin-projPt( Nmin, pqb) )    
       , pqbMax = mvPts( pqb, Nmax-projPt( Nmax, pqb) )
       
       // Calc the box pts
       
       /*
         pt on nqp pl_1, X, proj to pqb_1 is J, then proj to bqn_2 to get H:       
                          N 
                   K______|_________ J
                   /:     |        /|  
                  / :     +       / |
                 /  :     |      /  |
               G/______________F/   |
               | X  :_____|____|____' I
               | :  /L    Q----|-+-----  B
               | : /     /     |   /
               | :/     /      |  o <= J_Bmax_pqbMin
               | /J    +       | / 
               |/_____/________|/
               H     /         E
                    P        
       */
       , J_Bmax_pqbMin = projPt( Bmax, pqbMin )
       , E = projPt( J_Bmax_pqbMin, bqnMax )
       , I = projPt( J_Bmax_pqbMin, bqnMin )
       , H = projPt( E, nqpMin )
       , L = projPt( I, nqpMin )
       , F = projPt( E, pqbMax )
       , G = projPt( H, pqbMax )
       , K = projPt( L, pqbMax )
       , J = projPt( I, pqbMax )
       , boxpts = [ E,F,G,H,I,J,K,L ]
       , center = midPt([E,K])
       )//let
       [ 
         "outerPts", [Pmin,Pmax,Bmin,Bmax,Nmin,Nmax]
       , "outerPtIndices", [ pmin[2],pmax[2],bmin[2],bmax[2],nmin[2],nmax[2] ]
       , "Pmin", Pmin
       , "Pmax", pmax
       , "Bmin", Bmin
       , "Bmax", Bmax
       , "Nmin", Nmin
       , "Nmax", Nmax
       
       , "boxpts", boxpts
       , "center", center
       ] 
);

    getBoxData = ["getBoxData","pts,[pqr]", "hash","Geometry"
    , "Given pts and/or optional pqr, return a hash for info of
    ;; the mininum box containing all pts in the pqr coordinate.
    ;;
    ;;                  N 
    ;;           K______|_________ J
    ;;           /:     |        /|  
    ;;          / :     +       / |
    ;;         /  :     |      /  |
    ;;       G/______________F/   |
    ;;       |    :_____|____|____| I
    ;;       |    /L    Q----|-+-----  B
    ;;       |   /     / `'-.|   /
    ;;       |  /     /      |`'-._
    ;;       | /     +       | /   `R 
    ;;       |/_____/________|/
    ;;       H     /         E
    ;;            P 
    ;; Return:
    ;;     [   'outerPts', [Pmin,Pmax,Bmin,Bmax,Nmin,Nmax]
    ;;       , 'outerPtIndices', outerPtIndices // Indices corresponding to outperPts 
    ;;       , 'Pmin', Pmin // The pt in pts on the surface of ijkl 
    ;;       , 'Pmax', Pmax //                                 efgh 
    ;;       , 'Bmin', Bmin 
    ;;       , 'Bmax', Bmax
    ;;       , 'Nmin', Nmin
    ;;       , 'Nmax', Nmax
    ;;       , 'boxpts', [E,F,G,H,I,J,K,L]
    ;;       , 'center', center
    ;;       ] 
    ;;
    ;; To make a polyhedron:
    ;;
    ;;   polyhedron( points = boxpts, faces=faces('rod',nside=4))
    "];       
    function getBoxData_test( mode=12, opt=[] )=
    (
        doctest( getBoxData, [], mode=mode, opt=opt )
    );

} // getBoxData 


//========================================================
function getHShapePts( w, len, h, gap, gap2, midw, opt=[] )=
(
   /*
            leg1 W       gap1        leg2 W  
          |---------|------------|-----------|
                               19____________ 18   ___
       23 _________ 22          /:          /|     /    h 
         /:       /|          7/___________6 |   _/_   
      11/______10/ |           | :         | |    | 
        | :     |  |           | :         | |    |
        | :     |  |_21________|_: 20      | |    |  len, leg2L
        | :     |  /           | /         | |    |    
        | :     |/_____________|/          | |   _|_ 
        | :     9               8          | |    |
        | :8                               | |    | 
      0 | :      14 ..............15       | |    |  midW (LW)   
        | :      .:             .:         | |    |
        | :    2_______________: :         | |   _|_
        | :     |  |          3| :         | |
        | :.....|..|13         | :16_______|.| 17      
        |/ 12   | /            |/          | /
       0/_______1/            4|___________|/5     -----> x-axis   

          leg3 W       gap2        leg4 W  
        |---------|------------|-----------|   

      
     /
    y-axis

   */
   
   let( df_opt= [ "w", und(w,2) // define all widths (midbar width and 4 leg widths)
                 , "len", und(len,4) // define all 4 leg len
                 , "h", und(h,2)     // thickness
                 , "gap", und(gap,2) 
                 , "gap2", undef
                 , "midw", undef //und(midw,2)
                 , "leg1", true // Always need to have a leg1opt, so set it to true
                 , "leg2", true // Always need to have a leg2opt, so set it to true
                 , "leg3", true // Always need to have a leg1opt, so set it to true
                 , "leg4", true // Always need to have a leg2opt, so set it to true
                 ]
      
      , opt= update( df_opt, opt )
      , W= hash(opt,"w")
      , L= hash(opt,"len")
      , H= hash(opt,"h")
	    , gap = hash(opt, "gap")
      , gap1 = gap
      , gap2 = und(gap2, hash(opt, "gap2", gap))
      , midw = und(midw, hash( opt, "midw", W)) //und(midw, W)
      
      , leg1opt = subopt1( df_opt= df_opt
                     , u_opt = opt 
                     , subname = "leg1"
                     , df_subopt= [ "w", W, "len", L ]
                     , com_keys=["w","len","h"] 
                     )
	    , leg2opt = subopt1( df_opt= df_opt
                     , u_opt = opt 
                     , subname = "leg2"
                     , df_subopt= [ "w", W, "len", L ]
                     , com_keys=["w","len","h"] 
                     )
	    , leg3opt = subopt1( df_opt= df_opt
                     , u_opt = opt 
                     , subname = "leg3"
                     , df_subopt= [ "w", W, "len", L ]
                     , com_keys=["w","len","h"] 
                     )	    
     , leg4opt = subopt1( df_opt= df_opt
                     , u_opt = opt 
                     , subname = "leg4"
                     , df_subopt= [ "w", W, "len", L ]
                     , com_keys=["w","len","h"] 
                     )
     , L1L = hash(leg1opt, "len")
     , L1W = hash(leg1opt, "w")
     , L2L = hash(leg2opt, "len")
     , L2W = hash(leg2opt, "w")
     , L3L = hash(leg3opt, "len")
     , L3W = hash(leg3opt, "w")
     , L4L = hash(leg4opt, "len")
     , L4W = hash(leg4opt, "w")
     , grow_steps = ["x", L3W, "z", L3L, "x", gap2, "z", -L4L, "x", L4W
                    ,"t",[ L1W+gap+L2W-(L3W+gap2+L4W), 0, L4L+midw+L2L]
                    ,"x", -L2W, "z", -L2L,"x",-gap1, "z", L1L, "x",-L1W]
     , pts1= growPts( ORIGIN, grow_steps)
     , pts2 = [ for(p=pts1) p+[0, H,0]]
     )// let
     //leg1opt
     //["L1L",L1L, "L1W", L1W, "L2L", L2L, "L2W", L2W]
     //["leg1opt", leg1opt, "leg2opt", leg2opt ]
     //grow_steps //
     //["midw", midw]
     //opt
     concat( pts1, pts2 )
);
//========================================================

//function getLShapePts(w, len, h, opt=[] )=
//(
//  //                                 vleg W    
//  //                               |---------|
//  //                            13 _________ 12         
//  //                              /:       /|           
//  //           ___             5 /_______4/ |                     
//  //            |                | :     |  |                    
//  //            |     15_________|_:14   |  |_11__________ 10    
//  //       vleg len   / |        |/      |  /            /|      
//  //           _|_  7____________|6      3/____________2/ |      
//  //            |    |  :..............................|..| 9     ...
//  //  w, hleg w |    | / 8                             | /        /  h
//  //           _|_  0 /________________________________/ 1     _/_    -----> x-axis
//  //                  
//  //                  |------|--------------|
//  //            /       L1 W    L1 len
//  //          y-axis
//
//   let( df_opt= [ "w", und(w,2)
//                 , "len", und(len,4)
//                 , "h", und(h,2)
//                 , "leg1", true // Always need to have a leg1opt, so set it to true
//                 , "leg2", true // Always need to have a leg2opt, so set it to true
//                 ]
//      
//      , opt= update( df_opt, opt )
//      , LW= hash(opt,"w")
//      , LL= hash(opt,"len")
//      , LH= hash(opt,"h")
//	    , leg1opt = subopt1( df_opt= df_opt
//                     , u_opt = opt //hash(opt,"leg1",[])
//                     , subname = "leg1"
//                     , df_subopt= [ "w", LW, "len", LL ]
//                     , com_keys=["w","len","h"] 
//                     )
//	    , leg2opt = subopt1( df_opt= df_opt
//                     , u_opt = opt //hash(opt,"leg2",[])
//                     , subname = "leg2"
//                     , df_subopt= [ "w", LW, "len", LL ]
//                     , com_keys=["w","len","h"] 
//                     )
//     , L1L = hash(leg1opt, "len")
//     , L1W = hash(leg1opt, "w")
//     , L2L = hash(leg2opt, "len")
//     , L2W = hash(leg2opt, "w")
//     , grow_steps = [ "x", L1W+L2L, "z", L2W, "x", -L2L, "z", L1L
//                    , "x", -L1W ]
//                    //["x", 6, "z", 4, "x", -2, "z", -3, "x", -2, "z", 2, "x", -2]
//     , pts1= growPts( ORIGIN, grow_steps)
//     , pts2 = [ for(p=pts1) p+[0, LH,0]]
//     )// let
//     //leg1opt
//     //["L1L",L1L, "L1W", L1W, "L2L", L2L, "L2W", L2W]
//     //["leg1opt", leg1opt, "leg2opt", leg2opt ]
//     //grow_steps //
//     concat( pts1, pts2 )
//);

//========================================================

//function getTShapePts(w, len, h, gap, opt=[] )=
//(
//  //                      L1 W    
//  //                    |---------|
//  //                  8 _________ 9         
//  //                   /:       /|           
//  //           ___  5 /_______4/ |                     
//  //            |     | :     |  |                    
//  //            |     | :     |  |_10__________ 11    
//  //         L1 len   | :     |  /            /|      
//  //           _|_    | :     3/____________2/ |      
//  //            |     | :...................|..| 6    ....
//  //    w, L2 w |     |/ 7                  | /        /  h
//  //           _|_  0 /______________________/ 1     _/_    -----> x-axis
//  //                  
//  //                  |------|--------------|
//  //            /       L1 W    L1 len
//  //          y-axis
//
//   let( df_opt= [ "w", und(w,2)
//                 , "len", und(len,4)
//                 , "h", und(h,2)
//                 , "leg1", true // Always need to have a leg1opt, so set it to true
//                 , "leg2", true // Always need to have a leg2opt, so set it to true
//                 ]
//      
//      , opt= update( df_opt, opt )
//      , LW= hash(opt,"w")
//      , LL= hash(opt,"len")
//      , LH= hash(opt,"h")
//	    , leg1opt = subopt1( df_opt= df_opt
//                     , u_opt = opt //hash(opt,"leg1",[])
//                     , subname = "leg1"
//                     , df_subopt= [ "w", LW, "len", LL ]
//                     , com_keys=["w","len","h"] 
//                     )
//	    , leg2opt = subopt1( df_opt= df_opt
//                     , u_opt = opt //hash(opt,"leg2",[])
//                     , subname = "leg2"
//                     , df_subopt= [ "w", LW, "len", LL ]
//                     , com_keys=["w","len","h"] 
//                     )
//     , L1L = hash(leg1opt, "len")
//     , L1W = hash(leg1opt, "w")
//     , L2L = hash(leg2opt, "len")
//     , L2W = hash(leg2opt, "w")
//     , grow_steps = [ "x", L1W+L2L, "z", L2W, "x", -L2L, "z", L1L
//                    , "x", -L1W ]
//                    //["x", 6, "z", 4, "x", -2, "z", -3, "x", -2, "z", 2, "x", -2]
//     , pts1= growPts( ORIGIN, grow_steps)
//     , pts2 = [ for(p=pts1) p+[0, LH,0]]
//     )// let
//     //leg1opt
//     //["L1L",L1L, "L1W", L1W, "L2L", L2L, "L2W", L2W]
//     //["leg1opt", leg1opt, "leg2opt", leg2opt ]
//     //grow_steps //
//     concat( pts1, pts2 )
//);
     
//========================================================
//function getUShapePts( w, len, h, gap, opt=[] )=
//(
//   /*
//            leg1 W       gap         leg2 W  
//          |---------|------------|-----------|
//                               11____________ 10       ___
//       15 _________ 14          /:          /|         /    h 
//         /:       /|           3___________2 |       _/_   
//      7 /_______6/ |           | :         | |        | 
//        | :     |  |           | :         | |        |
//        | :     |  |_13________|_: 12      | |        |  len, leg2L
//        | :     |  /           | /         | |        |    
//        | :     5/_____________4/          | |       _|_ 
//        | :................................|.. 9      |
//        |/ 8                               |/         | w
//      0 /__________________________________/ 1       _|_   -----> x-axis
//      
//     /
//    y-axis
//
//   */
//   let( df_opt= [ "w", und(w,2)
//                 , "len", und(len,4)
//                 , "h", und(h,2)
//                 , "gap", und(gap,2)
//                 , "leg1", true // Always need to have a leg1opt, so set it to true
//                 , "leg2", true // Always need to have a leg2opt, so set it to true
//                 ]
//      
//      , opt= update( df_opt, opt )
//      , LW= hash(opt,"w")
//      , LL= hash(opt,"len")
//      , LH= hash(opt,"h")
//	    , gap = hash(opt, "gap")
//      , leg1opt = subopt1( df_opt= df_opt
//                     , u_opt = opt //hash(opt,"leg1",[])
//                     , subname = "leg1"
//                     , df_subopt= [ "w", LW, "len", LL ]
//                     , com_keys=["w","len","h"] 
//                     )
//	    , leg2opt = subopt1( df_opt= df_opt
//                     , u_opt = opt //hash(opt,"leg2",[])
//                     , subname = "leg2"
//                     , df_subopt= [ "w", LW, "len", LL ]
//                     , com_keys=["w","len","h"] 
//                     )
//     , L1L = hash(leg1opt, "len")
//     , L1W = hash(leg1opt, "w")
//     , L2L = hash(leg2opt, "len")
//     , L2W = hash(leg2opt, "w")
//     , grow_steps = [ "x", L1W+gap+L2W, "z", L2L+LW, "x", -L2W, "z", -L2L
//                    , "x", -gap, "z", L1L, "x", -L1W ]
//                    //["x", 6, "z", 4, "x", -2, "z", -3, "x", -2, "z", 2, "x", -2]
//     , pts1= growPts( ORIGIN, grow_steps)
//     , pts2 = [ for(p=pts1) p+[0, LH,0]]
//     )// let
//     //leg1opt
//     //["L1L",L1L, "L1W", L1W, "L2L", L2L, "L2W", L2W]
//     //["leg1opt", leg1opt, "leg2opt", leg2opt ]
//     //grow_steps //
//     concat( pts1, pts2 )
//);
  

//===========================================================
{ // getLoftData_dev_20150920
function getLoftData_dev_20150920( pts1, pts2, n=6, rot=0, C=undef
                        , _pts1=[], _pts2=[]
                        , _i=0
                        , _n2=6 // _n2 is the actual final # of pts 'cos 
                                // we gonna add pts to n during process
                        , _i1=0, _i2=0
                        , _a1=0, _a2=0
                        , _P1taken=0, _P2taken=0
                        , _debug="" 
                        )=       
( /* 

      2015.9.20
      
      Clone the getPairingData2_dev and rename to loftData(). Keep
      getPairingData2_dev for future ref.

  */
         /* 
                  pts2
                      `.
                        `.1
                        .'%
          pts1 ...1   .'   %
                 / `.'      % 
                / .' `.      % 
               /.'     `.     %
              o----------0-----0 

       When _i advances, a new a is calc.
         

*/  
   let( C = C?C:centerPt(pts1)
      , pqr = [ pts1[0], C, pts1[1] ]
      , N = N( pqr )
      , rad1 = dist( C, pts1[0] )
      , rad2 = dist( C, pts2[0] )
      , da1 = num(str( 360/len(pts1) ))
      , da2 = num(str( 360/len(pts2) ))
      , da = num(str( 360/n ))
      , a = num(str( _i * 360/n ))
            // NOTE: When getting angle, we have to typecast it to str,
            //       then back to num, 'cos sometimes it's tricky that
            //       that two angles both should be 120, and they
            //       do display as 120, but internally one could be
            //       120.0000001. Note: OpenSCAD displays number this way:
            //
            //         Internally    displayed 
            //         120.00004  :  120.00004
            //         120.000004 :  120
            //         120.00005  :  120.00005
            //         120.000005 :  120.00001
            //      deci:  123456 
            //        
            //       For 120.0000001, if we str(a) it, it will be "120", it 
            //       will then have actual value as 120 when num(str(a))
      )   
   _i==0? getLoftData_dev_20150920(pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                        , _pts1= [ pts1[0] ], _pts2=[ pts2[0] ]
                        , _i=1, _n2=n
                        , _i1= 0
                        , _i2= 0
                        , _P1taken=1, _P2taken=1
                        //, _justpass1=1, _justpass2=1
                        , _debug = str( "<b>getPairingData2</b>"
                                      , "<br/>_i=0"
                                      )
                        )
            
   //:a>=360?
   : _i>5? 
          [ "pts", concat( _pts1, _pts2 )
          , "pts1", _pts1
          , "pts2", _pts2
          , "pts1origin", pts1
          , "pts2origin", pts2    
          , "C", C
          , "faces", let(f=range(_pts1))[ for(i= [0:f-1])
                                          [ i
                                          , f+i
                                          , i==f-1?f:i+1+f
                                          , i==f-1?0:i+1 
                                          ]
                                     ]
          , "debug", _debug 
          ]
   : 
     let( P1i = pts1[_i1]
        , P1i1= pts1[_i1+1]
        , P2i = pts2[_i2]
        , P2i1= pts2[_i2+1]
        , pair1= [ P1i, P1i1 ] //get( pts1,[_i1,_i1+1] )
        , pair2= [ P2i, P2i1 ] //get( pts2,[_i2,_i2+1] )
        , pair1next= [ P1i1, pts1[_i1+2] ] //get( pts1,[_i1+1,_i1+2] )
        , pair2next= [ P2i1, pts2[_i2+2] ] //get( pts2,[_i2+1,_i2+2] )
        
        , a1 = _i1*da1 //_a1 + da1
        , a2 = _i2*da2 //_a2 + da2
        , angPt = anglePt( pqr, a=a, Rf=N ) // Any pt such that a_Pts1[0]_C_angPt= a
                                      // (and a_Pts2[0]_C_angPt= a )
        , ln_a = [ C, angPt ] // The line radiates from C, at angle a 
        , X1= intsec( ln_a, pair1)
        , X2= intsec( ln_a, pair2)
        
        , ln1i = [ C, P1i ]
        , ln2i = [ C, P2i ]
        , P1iOn2 = intsec( ln1i, pair2 )
        , P2iOn1 = intsec( ln2i, pair1 )
        , P1iOn2next = intsec( ln1i, pair2next )
        , P2iOn1next = intsec( ln2i, pair1next )
        
        , issamept1 = isSamePt(P1i1, last(_pts1))
        , issamept2 = isSamePt(P2i1, last(_pts2))
        
//        , ln1 = [ C, pts[_i] ]
//        , ln2 = [ C, pts[_2] ]
//        , ln1 = [ C, pts[_i] ]
//        , ln2 = [ C, pts[_2] ]
//          
        )
        //, a1 = num(str( angle( [ pts1[0], C1, pts1[_i+1] ], Rf=N )  ))
        //, a2 = num(str( angle( [ pts2[0], C2, pts2[_i+1] ], Rf=N )  ))
          /*
            12 conditions:
          
           // group 1 ---
           
            a < a1 < a2
            a < a2 < a1
            a < a1 = a2
          
                 a = a1 < a2
            a2 < a = a1

                 a = a2 < a1
            a1 < a = a2
            
            a1 < a < a2
            a2 < a < a1
            a1 < a2 < a
            a2 < a1 < a
            
                a1 = a2 < a
          */ 
         
//        a < a1 && a < a2 ?  // group 1
//             /*             
//                           
//                        _-`     _.-'`         
//                     i1`   _.i2`      
//                  _-` _.-'`   __..--A``
//               _-`.-'`..--''``      
//             C           
//                           i2
//                        _-`     _.-'`         
//                     _-`   _.-'`      
//                  _-` _.i1`   __..--A``
//               _-`.-'`..--''``      
//             C           
//                           i2
//                        _-`     _A
//                     i1`   _.-'`      
//                  _-` _.-'` 
//               _-`.-'`
//             C           
//                             
//             */          
//          getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//                        , _pts1= app( _pts1, intsec( ln_a, pair1) )
//                        , _pts2= app( _pts2, intsec( ln_a, pair2) )
//                        , _i = _i+1
//                        , _i1 = _i1
//                        , _i2 = _i2
//                        , _a1 = a1  
//                        , _a2 = a2
//                        , _P1taken=0, _P2taken=0
//                        //, _justpass1=0
//                        //, _justpass2=0
//                        , _debug = str("<br>", _debug
//                                  , "<br>_i= ", _i, ", a &lt; a1 && a &lt; a2"
//                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
//                        )          
//
//         //-------------------------------------------------------
//       
//        : a==a1 && a==a2 ?
//               /*              A       
//                            _-`   
//                         _i2   
//                      _-` 
//                   _i1
//                  C  
//              */
//              getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//                    , _pts1= app(_pts1, pts1[_i1+1] )
//                    , _pts2= app(_pts2, pts1[_i2+1] )
//                    , _i  = _i+1
//                    , _i1 = _i1+1
//                    , _i2 = _i2+2
//                    , _a1 = a1  
//                    , _a2 = a2
//                    , _P1taken=0, _P2taken=0
//                        //, _justpass1=0
//                    //, _justpass2=0
//                    , _debug = str("<br>", _debug
//                                  , "<br>_i= ", _i, ", a==a1 && a==a2"
//                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
//                    )    
//         //-------------------------------------------------------
//       
//        : a==a1 ?
//          ( a < a2 ?
//               /*                 _      
//                               _i2       
//                            _-`    _.-'` 
//                         _-`   _.a` 
//                      _-` _i1'`
//                   _-`.-'`
//                  C  
//              */
//              getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//                    , _pts1= app(_pts1, pts1[_i1+1]  )
//                    , _pts2= app(_pts2, intsec( ln_a, pair2) )
//                    , _i  = _i+1
//                    , _i1 = _i1+1
//                    , _i2 = _i2
//                    , _a1 = a1  
//                    , _a2 = a2
//                    , _P1taken=0, _P2taken=0
//                        //, _justpass1=0
//                    //, _justpass2=0
//                    , _debug = str("<br>", _debug
//                                  , "<br>_i= ", _i, ", a==a1 &lt; a2 "
//                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" ) 
//                    )          
//          : // a2 < a ?
//             /*               _      
//                           _a`       
//                        _-`     _.-'` 
//                     i1`   _.-i2 
//                  _-` _.-'`
//               _-`.-'`
//            */          
//            getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//                    , _pts1= concat(_pts1, [ intsec( ln_a, pair1), pts1[_i1+1] ] )
//                    , _pts2= concat(_pts2, [ pts2[_i2+1], intsec( ln_a, pair2next) ] )
//                    , _i  = _i+1
//                    , _i1 = _i1+( a> (_i+1)* da1 ?1:0) 
//                    , _i2 = _i2+1
//                    , _a1 = a1  
//                    , _a2 = a2
//                    , _P1taken=0, _P2taken=0
//                        //, _justpass1=0
//                    //, _justpass2=0
//                    , _debug = str("<br>", _debug
//                                  , "<br>_i= ", _i, ", a2 &lt; a==a1" 
//                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
//                    )          
//         ) // a==a1
//         
//         //-------------------------------------------------------
//         
//        : a==a2 ?
//          ( a < a1 ?
//               /*                 _      
//                               _i1       
//                            _-`    _.-'` 
//                         _-`   _.a` 
//                      _-` _i2'`
//                   _-`.-'`
//                  C  
//              */
//              getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//                    , _pts1= app( _pts1, intsec( ln_a, pair1) )
//                    , _pts2= app( _pts2, pts2[_i2+1] )
//                    , _i  = _i+1
//                    , _i1 = _i1
//                    , _i2 = _i2+1
//                    , _a1 = a1  
//                    , _a2 = a2
//                    , _P1taken=0, _P2taken=0
//                    , _debug = str("<br>", _debug
//                                  , "<br>_i= ", _i, ", a==a2 &lt; a1"
//                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
//                    )          
//          : // a1 < a ?
//             /*               _      
//                           _a`       
//                        _-`     _.-'` 
//                     i2`   _.-i1 
//                  _-` _.-'`
//               _-`.-'`
//            */          
//            getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//                    , _pts1= concat(_pts1, [ pts1[_i1+1], intsec( ln_a, pair1next) ] )
//                    , _pts2= concat(_pts2, [ intsec( ln_a, pair2), pts2[_i2+1] ] )
//                    , _i  = _i+1
//                    , _i1 = _i1+1
//                    , _i2 = _i2+1
//                    , _a1 = a1  
//                    , _a2 = a2
//                    , _P1taken=0, _P2taken=0
//                    , _debug = str("<br>", _debug
//                                  , "<br>_i= ", _i, ", a1 &lt; a==a2 "
//                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
//                    )          
//          ) // a==a2
//
//        //-------------------------------------------------------
//        : a1 < a && a < a2? 
//          /*
//                        _-`    _.-'`         
//                     i2`   _.A`      
//                  _-` _.-'`   __..i1'``
//               _-`.-'`..--''``      
//             C               
//         */ 
//            getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//                    , _pts1= concat(_pts1, [ pts1[_i1+1], intsec( ln_a, pair1next) ] )
//                    , _pts2= concat(_pts2, [ intsec( ln_a, pair2 ) ] )
//                    , _i  = _i+1
//                    , _i1 = _i1+1
//                    , _i2 = _i2
//                    , _a1 = a1  
//                    , _a2 = a2
//                    , _P1taken=0, _P2taken=0
//                    , _debug = str("<br>", _debug
//                                  , "<br>_i= ", _i, ", i1 &lt; a &lt; i2"
//                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
//                    )             
//        //-------------------------------------------------------
//        : a2 < a && a < a1? 
//          /*
//                        _-`    _.-'`         
//                     i1`   _.A`      
//                  _-` _.-'`   __..i2'``
//               _-`.-'`..--''``      
//             C               
//         */ 
//            getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//                    , _pts1= concat(_pts1, [ intsec( ln_a, pair1) ] )
//                    , _pts2= concat(_pts2, [ pts2[_i2+1], intsec( ln_a, pair2next ) ] )
//                    , _i  = _i+1
//                    , _i1 = _i1
//                    , _i2 = _i2+1
//                    , _a1 = a1  
//                    , _a2 = a2
//                    , _P1taken=0, _P2taken=0
//                    , _debug = str("<br>", _debug
//                                  , "<br>_i= ", _i, ", i2 &lt; a &lt; i1" 
//                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
//                    )             
//        //-------------------------------------------------------
//        : 
          //     a1 < a2 < a
          //     a2 < a1 < a
          /*
                        _-`    _.-'`         
                     a`   _.i1`      
                  _-` _.-'`   __..i2'``
               _-`.-'`..--''``      
             C               
                        _-`    _.-'`         
                     a`   _.i2`      
                  _-` _.-'`   __..i1'``
               _-`.-'`..--''``      
             C               

          */ 
          let( // Chk how many i1/i2 the new angle (a) covers:
               cover_i1= floor(a/da1)  
             , cover_i2= floor(a/da2)
             
             // Array of [ angle
             //          , pts1_or_pts2
             //          , index corresponding to angle ( i1 or i2)
             //          ] 
             // For pts1: [ [72, 1, 1],[144, 1, 2] ] 
             , a1s = _i1>0? // If we are in segment 1, where _i1==0, 
                       [ for(i= [ max(_i1,_i-1):cover_i1]) 
                         if(i*da1<a)[ i*da1, 1, i] 
                       ]:[]
             // For pts2: [ [90, 2, 1],[180, 2, 2] ] 
             , a2s = _i2>0?
                       [ for(i= [ max(_i2,_i-1):cover_i2]) 
                         if(i*da2<a)[ i*da2, 2, i] ]
                       :[]
                         
             // Merge them and sort: 
             // [ [72, 1, 1], [90, 2, 1], [144, 1, 2], [180, 2, 2] ] 
             , _aipairs = sort( concat( a1s,a2s) )   
                         
             // If the very first angle is the same as the previous angle (=a-da),
             // then we skip that aipair, 'cos it should be already handled in
             // previous step            
             , aipairs = _aipairs[0][0] == a-da ? 
                         slice( _aipairs,1)
                         : _aipairs 
                         
             // New pts to be added to _pts1 or _pts2            
             , newpts1s= 
                 [ for(i = range(aipairs)) 
                     let( aipair=aipairs[i] // Each aipair could be either pts1 or pts2 
                        , ai = aipair[0]    // angle 
                        , is1 = aipair[1]==1 // Is this pts1 ?
                        )
                     
                     is1 ? pts1[ aipair[2] ] // If yes, simply add the pt
                         
                     : // If it's pts2, project it to the last seg of pts1.
                       intsec( [C, pts2[ aipair[2]] ]
                             , get2( pts1
                                   , floor(
                                       aipair[0] // angle made by pts2[ aipair[2] ] 
                                       / da1 )       
                                   )
                             )               
                 ]                           
             , newpts2s= 
                 [ for(i = range(aipairs))
                     let( aipair=aipairs[i] // [angle, 1 or 2, _i1 or _i2]
                        , ai = aipair[0] 
                        , is2 = aipair[1]==2
                        )
                     is2 ? pts2[ aipair[2] ]
                     : intsec( [C, pts1[ aipair[2]] ]
                             , get2( pts2
                                   , floor(
                                       aipair[0] // angle made by pts1[ aipair[2] ] 
                                       / da2 )       
                                   )
                             )               
                 ]   

              )
           getLoftData_dev_20150920( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                                             
            , _pts1= concat( _pts1, newpts1s
                                            
                           // Add pt where ln_a intsec w/ current seg of pts1
                           , _i==1?[ intsec(ln_a, p01(pts1)) ]
                              :  [ intsec( ln_a
                                         , get2( pts1, cover_i1)                              
                                         ) ] 
                            )              
            , _pts2= concat( _pts2, newpts2s 
                                            
                           // Add pt where ln_a intsec w/ current seg of pts2
                           , _i==1?[ intsec(ln_a, p01(pts2)) ]
                              :  [intsec( ln_a
                                        , get2( pts2, cover_i2) //a2s?last(a2s)[2]:_i2
                                        )]                      
                           )

            , _i  = _i+1
            , _i1 = _i1+( a+da> (_i1+1)* da1 ?1:0)
            , _i2 = _i2+( a+da> (_i2+1)* da2 ?1:0)
            , _a1 = a1  
            , _a2 = a2
            , _P1taken=0, _P2taken=0
            , _debug = str("<br>", _debug
                          , "<br><b>_i</b>=", _i, ", _i1=", _i1, "_i2=", _i2 
                          , ", a1&lt;a2&lt;a, a2&lt;a1&lt;a"
                          , ",<br/>--- a=", a, ", a1=", a1, ", a2=", a2
                          //,     ", da1=",da1, ", da2=", da2
                          ,     ", a1s=",a1s, ", a2s=", a2s
                          ,  ", aipairs=", aipairs
                          , "<br/>---  last(a1s)= ", last(a1s)
                          , "<br/>---  get2(pts1, last(a1s)[2])= ", get2(pts1, last(a1s))[2]
                          , "<br/>---  intsec= ", intsec( ln_a, get2(pts1, last(a1s)[2]))
                          , "<br/>---  get2(pts1, last(a1s)[2])= ", get2(pts1, last(a1s))[2]
                          , "<br/>---  intsec= ", intsec( ln_a, get2(pts1, last(a1s)[2]))
                          , "<br/>--- newpts1s = ", newpts1s
                          , "<br/>--- newpts2s = ", newpts2s
                          , "<br/>--- a2s?last(a2s)[2]:_i2 = ", a2s?last(a2s)[2]:_i2
                         //,     ", p1taken=", _p1taken, ", p2taken=", p2taken
                          , "<br/>" )
            )
//           let( ln1 = [ C,pts1[_i1] ]
//              , ln2 = [ C,pts2[_i2] ] 
//              , p1taken = _i==1 || (a-a1)>=da1
//              , p2taken = _i==1 || (a-a2)>=da2
//              , newpt1s = concat( 
//                                  a2<a1 && !p2taken ? 
//                                    [intsec( ln2, get2( pts1, _i1-1) )]:[]
//                                    
//                                , !p1taken? [P1i]:[]
//                                
//                                , a2>a1 && !p2taken ? 
//                                    [intsec( ln2, get2( pts1, _i1) )]:[]
//                                    
//                                , [ intsec( ln_a, get2( pts1, _i1) ) ]
//                                )  
//              
//              , newpt2s = concat( 
//                                  a1<a2 && !p1taken ? 
//                                    [intsec( ln1, get2( pts2, _i2-1) )]:[]
//                                    
//                                , !p2taken? [P2i]:[]
//                                
//                                , a1>a2 && !p1taken ? 
//                                    [intsec( ln1, get2( pts2, _i2) )]:[]
//                                    
//                                , [ intsec( ln_a, get2( pts2, _i2) ) ]
//                                ) 
//              )
//           getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//            , _pts1= concat( _pts1, newpt1s)                                     
//            , _pts2= concat( _pts2, newpt2s)
//
//            , _i  = _i+1
//            , _i1 = _i1+( a+da> (_i1+1)* da1 ?1:0)
//            , _i2 = _i2+( a+da> (_i2+1)* da2 ?1:0)
//            , _a1 = a1  
//            , _a2 = a2
//            , _P1taken=0, _P2taken=0
//            , _debug = str("<br>", _debug
//                          , "<br>_i=", _i, ", _i1=", _i1, "_i2=", _i2 
//                          , ", a1&lt;a2&lt;a, a2&lt;a1&lt;a"
//                          , ", ( a=", a, ", a1=", a1, ", a2=", a2
//                          ,     ", da1=",da1, ", da2=", da2
//                          ,     ", p1taken=", p1taken, ", p2taken=", p2taken
//                          , " )" )
//            )             
        //-------------------------------------------------------
        
  
);  // getLoftData_dev_20150920()

} // getLoftData_dev_20150920

//===========================================================
//{ // getLoftData
//function getLoftData( 
//
//      pts1     // pts1 and pts2 are coplaner, circular pts
//    , pts2     //   with different sides and rads
//    , n=6      // resolution 
//    , rot=0    // rot of pts2 (to be implanted)
//    //, h      = 0 // height if pts2 are to raised up to make column
//    //, nlayer = 5 // default layer when h >0
//    , C=undef  // center pt for both pts1 and pts2
//    , debug=false // debug or step-specific debug: 
//                  //   true : debug for all range over 0~n-1
//                  //   i    : debug for step i 
//                  //   [i,j,...] : debug for steps i,j ...
//    
//        , _i=0 // step index going through resolution (n) 
//        , _pts1=[], _pts2=[]
//        , _debug="" 
//    )=       
//( 
//         /* 
//                  pts2
//                      `.
//                        `.1
//                        .'%
//          pts1 ...1   .'   %
//                 / `.'      % 
//                / .' `.      % 
//               /.'     `.     %
//              o----------0-----0 
//   
//        line C-Pa, C-P1_i1, C-P2_i2 are called "radlines"
//          
//                        (Pa)     (P1_i1)
//                       _-`    _.-'`         
//                     a`   _.i1`        (P2_i2)
//                  _-` _.-'`   __..i2'``
//               _-`.-'`..--''``      
//             C               
//                        _-`    _.-'`         
//                     a`   _.i2`      
//                  _-` _.-'`   __..i1'``
//               _-`.-'`..--''``      
//             C               
//
//        When _i advances, a new ai is calc.
//   
//        */  
//    let(  _C = C?C:centerPt(pts1)
//        , pqr = [ pts1[0], _C, pts1[1] ]
//        , N = N( pqr ) // line CN is from the center goint toward you
//        , da1 = num(str( 360/len(pts1) )) // angle interval for pts1
//        , da2 = num(str( 360/len(pts2) )) // angle interval for pts2
//        , da = num(str( 360/n ))          // angle interval for resolution n
//        )
//        
//   _i==0? getLoftData(pts1=pts1, pts2=pts2, n=n, rot=rot, C=_C
//                        , debug = debug
//                        , _pts1= [ pts1[0] ], _pts2=[ pts2[0] ]
//                        , _i=1
////                        , _debug = debug? str(                        
////                        "<hr/>"
////                        , "<b>getLoftData()</b> running at debug mode ( isdebug=true )"
////                        ,"<br/> <u>Arguments</u>:"
////                        ,"<br/> <b>n1</b> (1st ring nside)= ", len(pts1)
////                        ,"<br/> <b>n2</b> (2nd ring nside)= ", len(pts1)
////                        ,"<br/> <b>n</b> (resolution)= ", n
////                        ,"<br/> <b>C</b> (center)=",C
////                        ,"<br/> <b>debug</b>= ", debug
////                        ,"<br/> <b>pts1</b>= ", pts1
////                        ,"<br/> <b>pts2</b>= ", pts2
////                        
////                        
////                        
////                          , "<br/><u>Calculated</u>:"
////                          , "<br/> <b>C</b> (center)=",_C
////                          //, "<br/> <b>N</b> (line CN: from C going toward you)=",N
////                          , "<br/> <b>da1</b> (angle interval for pts1)=",da1
////                          , "<br/> <b>da2</b> (angle interval for pts2)=",da2 
////                          , "<br/> <b>da</b> (angle interval for resolution)=",da
////                       ,"<hr/>"
////                         , "<b>_i</b>=0"
////                            , "<br/><b>newpts1s</b>=",[ for(ps=[ pts1[0] ])[for(p=ps)is0(p)?0:p]]
////                          , "<br/><b>newpts2s</b>=",[ for(ps=[ pts2[0] ])[for(p=ps)is0(p)?0:p]]
////                          , "<br/>"
////                          ):"" // debug
//                        )          
//    :_i>n?[ "pts", concat( _pts1, _pts2 )
//          , "pts1", _pts1
//          , "pts2", _pts2
//          , "pts1origin", pts1
//          , "pts2origin", pts2    
//          , "C", C
//          , "faces", let(f=range(_pts1))[ for(i= [0:f-1])
//                                          [ i
//                                          , f+i
//                                          , i==f-1?f:i+1+f
//                                          , i==f-1?0:i+1 
//                                          ]
//                                     ]
//          , "debug", _debug 
//          ]
//   :   
//        // NOTE: When getting angle, we have to typecast it to str,
//        //       then back to num, 'cos sometimes it's tricky 
//        //       that two angles both should be 120, and they
//        //       do display as 120, but internally one could be
//        //       120.0000001. Note: OpenSCAD displays number this way:
//        //
//        //         Internally    displayed 
//        //         120.00004  :  120.00004
//        //         120.000004 :  120
//        //         120.00005  :  120.00005
//        //         120.000005 :  120.00001
//        //      deci:  123456 
//        //        
//        //       For 120.0000001, if we str(a) it, it will be "120", it 
//        //       will then have actual value as 120 when num(str(a))
//    let ( ai = num(str( _i * 360/n ))      // Current angle for n 
//        , angPt = anglePt( pqr, a=ai, Rf=N ) // Any pt such that a_Pts1[0]_C_angPt= a
//                                            // (and a_Pts2[0]_C_angPt= a )
//                                            // I.e., the projection of radline onto
//                                            // pts1 ring or pts2 ring
//        , ln_a = [ C, angPt ] // The line radiates from C, at angle a (radline @a)
//        // Chk how many i1/i2 the new angle (a) covers:
//        , cover_i1= floor(num(str(ai/da1)))  
//        , cover_i2= floor(num(str(ai/da2)))
//        , lastcover_i1= _i==0?0:floor(num(str((_i-1)*da/da1)))  
//        , lastcover_i2= _i==0?0:floor(num(str((_i-1)*da/da2)))
//
//        ///////////////////////////////////////////////////////////
//        // Array of [ angle
//        //          , pts1_or_pts2
//        //          , index corresponding to angle ( i1 or i2)
//        //          ] 
//        // For pts1: [ [72, 1, 1],[144, 1, 2] ] 
//        // These two arrays are generated for each step of n
//        //  
//        // 20151001: In case pts1[ cover_i1 ] or pts2[ cover_i2 ]
//        //           co-angle with one of the step pt, for example,
//        //           when n=6, n1=6, n2=14, then those pts below will
//        //           have same angle of 180:
//        //           pts1[3], pts2[7], pts of resolution [3] 
//        //           So we need to avoid the duplicates by introducing
//        //           num(str(i*da1))!=a) and num(str(i*da2))!=a) here
//        , a1s =lastcover_i1==cover_i1 ?[]:
//               [ for(i= [ lastcover_i1+1:cover_i1]) 
//                   if(i*da1<360 
//                       && num(str(i*da1))!=ai)  // see note above (20151001)
//                   [ i*da1, 1, i ]                 
//               ]
//                   
//        // 20151001: a1angs: angles for pts1, needed for avoiding duplicate
//        //           in getting a2s           
//        , a1angs = [ for(api1=a1s) api1[0] ]
//            
//        // For pts2: [ [90, 2, 1],[180, 2, 2] ] 
//        , a2s =lastcover_i2==cover_i2?[]:
//               [ for(i= [ lastcover_i2+1:cover_i2]) 
//                   if(i*da2<360 
//                       && num(str(i*da2))!=ai // See note above (20151001)
//                       && !has(a1angs,i*da2) // When not already in pts1's angles
//                     )
//                   [ i*da2, 2, i ]
//               ]
//                 
//        // Merge them and sort: 
//        // [ [72, 1, 1], [90, 2, 1], [144, 1, 2], [180, 2, 2] ] 
//        , _aipairs = sort( concat( a1s,a2s) )
//        
//        //, _aipairs = [ for(pair=__aipairs) if(pair[0]!=a) pair ]
//            
//        // If the very first angle is the same as the previous angle (=a-da),
//        // then we skip that aipair, 'cos it should be already handled in
//        // previous step            
//        , aipairs= 
//                 _aipairs[0][0] == ai-da ? 
//                 slice( _aipairs,1)
//                 : _aipairs                         
//           
//        //////////////////////////////////////////////////////////           
//        // New pts to be added to _pts1 or _pts2            
//        , newpts1s= 
//                 
//             !aipairs?[]
//             :[ for(i = range(aipairs)) 
//                 let( aipair=aipairs[i] // Each aipair could be either pts1 or pts2 
//                    //, ai = aipair[0]    // angle 
//                    , is1 = aipair[1]==1 // Is this pts1 ?
//                    )
//                 is1 ? pts1[ aipair[2] ] // If yes, simply add the pt
//                     
//                 : // If it's pts2, project it to the last seg of pts1.
//                   intsec( [C, pts2[ aipair[2]] ]
//                         , get2( pts1
//                               , floor(
//                                   aipair[0] // angle made by pts2[ aipair[2] ] 
//                                   / da1 )       
//                               )
//                         )               
//             ] // for
//         
//        , newpts2s= 
//         
//             !aipairs?[]
//             :[ for(i = range(aipairs))
//                 let( aipair=aipairs[i] // [angle, 1 or 2, _i1 or _i2]
//                    //, ai = aipair[0] 
//                    , is2 = aipair[1]==2
//                    )
//                 is2 ? pts2[ aipair[2] ]
//                 : intsec( [C, pts1[ aipair[2]] ]
//                         , get2( pts2
//                               , floor(
//                                   aipair[0] // angle made by pts1[ aipair[2] ] 
//                                   / da2 )       
//                               )
//                         )               
//             ] //for 
// 
//        , _pts1= concat( _pts1, newpts1s
//                                        
//                       // Add pt where ln_a intsec w/ current seg of pts1
//                       , ai>=360 
//                         || ai== last(aipairs)[0] ?[] // The a might be the same as 
//                                                     // the last a1 or a2, so we 
//                                                     // don't need to add it twice
//                                                     // For example, if n=4 and n1=4,
//                                                     // all a's will be the same as a1's
//                            :[ intsec( ln_a
//                             , get2( pts1, cover_i1)                              
//                             ) ] 
//                        )                  
//      
//        , _pts2= concat( _pts2, newpts2s 
//                                        
//                       // Add pt where ln_a intsec w/ current seg of pts2
//                       , ai>=360 || ai== last(aipairs)[0] ?[]
//                            :[intsec( ln_a
//                                    , get2( pts2, cover_i2) //a2s?last(a2s)[2]:_i2
//                                    )]
//                       )             
//        )//let
//                
//           getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//            , debug = debug
//                                             
//            , _pts1= _pts1                  
//          
//            , _pts2= _pts2
//
//            , _i  = _i+1
//            
//            , _debug = (debug==true || debug==_i || has(debug,_i))? str(
//                    "<br>", _debug
//                  , "<br><b>_i</b>=", _i , ", <b>ai</b> (angle @ step _i)=", ai
//                  , _s("<br/><u>Calculate the indices of pts1,pts2 at step {_})</u>:",_i)
//                  , "<br/>--- (ai/da1)=",(ai/da1), ", (ai/da2)=", (ai/da2)
//                  , ", <b>cover_i1</b>=",cover_i1, ", <b>cover_i2</b>=", cover_i2
//                  , ",<br/>--- (_i-1)*da/da1=",(_i-1)*da/da1, ", (_i-1)*da/da2=", (_i-1)*da/da2,
//                  , ", <b>lastcover_i1</b>=",lastcover_i1, ", <b>lastcover_i2</b>=", lastcover_i2
//                  , ",<br/>--- cover_i1*da1=",cover_i1*da1, ", cover_i2*da2=", cover_i2*da2
//
//                  , ",<br/>--- <b>a1s</b>=",a1s, ", <b>a2s</b>=", a2s
//                  ,  ", <b>aipairs</b>=", aipairs
//
//                  , _s("<br/><u><b>newpts1s or ~2s</b> are pts to be added in step {_} as a result of pts1 or pts2 (means, excluding pts caused by resolution, n)</u>:",_i)
//                  , "<br/>--- <b>len(newpts1s)</b>=",len(newpts1s)
//                            , ", <b>len(newpts2s)</b>=",len(newpts2s) 
//                  , "<br/>--- <b>newpts1s</b> = ", [ for(ps=newpts1s)[for(p=ps)is0(p)?0:p]]
//                  , "<br/>--- <b>newpts2s</b> = ", [ for(ps=newpts2s)[for(p=ps)is0(p)?0:p]]
//                  ,_s("<br/><u><b>_pts1</b> and <b>_pts1</b> are pts obtained after step {_}</u>:",_i)
//                  , "<br/> <b>_pts1</b>= ",len(_pts1),": ", [ for(ps=_pts1)[for(p=ps)is0(p)?0:p]]
//                  , "<br/> <b>_pts2</b>= ",len(_pts2),": ", [ for(ps=_pts2)[for(p=ps)is0(p)?0:p]]
//                  , "<br/>" ):_debug
//            )
//           
////        -------------------------------------------------------
//        
//  
//);  // getLoftData()           
//
//} // getLoftData


//===========================================================
//{ // getLoftData
//function getLoftData( 
//
//      pts1     // pts1 and pts2 are coplaner, circular pts
//    , pts2     //   with different sides and rads
//    , n=6      // resolution 
//    , rot=0    // rot of pts2 (to be implanted)
//    , h      = 0 // height if pts2 are to raised up to make column
//    , nlayer = 5 // default layer when h >0
//    , C=undef  // center pt for both pts1 and pts2
//    , debug=false // debug or step-specific debug: 
//                     true : debug for all range over 0~n-1
//                     i    : debug for step i 
//                     [i,j,...] : debug for steps i,j ...
//    
//        , _i=0 // step index going through resolution (n) 
//        , _pts1=[], _pts2=[]
//        , _debug="" 
//    )=       
//( 
//         /* 
//                  pts2
//                      `.
//                        `.1
//                        .'%
//          pts1 ...1   .'   %
//                 / `.'      % 
//                / .' `.      % 
//               /.'     `.     %
//              o----------0-----0 
//   
//        line C-Pa, C-P1_i1, C-P2_i2 are called "radlines"
//          
//                        (Pa)     (P1_i1)
//                       _-`    _.-'`         
//                     a`   _.i1`        (P2_i2)
//                  _-` _.-'`   __..i2'``
//               _-`.-'`..--''``      
//             C               
//                        _-`    _.-'`         
//                     a`   _.i2`      
//                  _-` _.-'`   __..i1'``
//               _-`.-'`..--''``      
//             C               
//
//        When _i advances, a new ai is calc.
//   
//        */  
//    let(  _C = C?C:centerPt(pts1)
//        , pqr = [ pts1[0], _C, pts1[1] ]
//        , N = N( pqr ) // line CN is from the center goint toward you
//        , da1 = num(str( 360/len(pts1) )) // angle interval for pts1
//        , da2 = num(str( 360/len(pts2) )) // angle interval for pts2
//        , da = num(str( 360/n ))          // angle interval for resolution n
//        )
//        
//   _i==0? getLoftData(pts1=pts1, pts2=pts2, n=n, rot=rot, C=_C
//                        , debug = debug
//                        , _pts1= [ pts1[0] ], _pts2=[ pts2[0] ]
//                        , _i=1
//                        , _debug = debug? str(                        
//                        "<hr/>"
//                        , "<b>getLoftData()</b> running at debug mode ( isdebug=true )"
//                        ,"<br/> <u>Arguments</u>:"
//                        ,"<br/> <b>n1</b> (1st ring nside)= ", len(pts1)
//                        ,"<br/> <b>n2</b> (2nd ring nside)= ", len(pts1)
//                        ,"<br/> <b>n</b> (resolution)= ", n
//                        ,"<br/> <b>C</b> (center)=",C
//                        ,"<br/> <b>debug</b>= ", debug
//                        ,"<br/> <b>pts1</b>= ", pts1
//                        ,"<br/> <b>pts2</b>= ", pts2
//                        
//                        
//                        
//                          , "<br/><u>Calculated</u>:"
//                          , "<br/> <b>C</b> (center)=",_C
//                          //, "<br/> <b>N</b> (line CN: from C going toward you)=",N
//                          , "<br/> <b>da1</b> (angle interval for pts1)=",da1
//                          , "<br/> <b>da2</b> (angle interval for pts2)=",da2 
//                          , "<br/> <b>da</b> (angle interval for resolution)=",da
//                       ,"<hr/>"
//                         , "<b>_i</b>=0"
//                            , "<br/><b>newpts1s</b>=",[ for(ps=[ pts1[0] ])[for(p=ps)is0(p)?0:p]]
//                          , "<br/><b>newpts2s</b>=",[ for(ps=[ pts2[0] ])[for(p=ps)is0(p)?0:p]]
//                          , "<br/>"
//                          ):"" // debug
//                        )          
//    :_i>n?[ "pts", concat( _pts1, _pts2 )
//          , "pts1", _pts1
//          , "pts2", _pts2
//          , "pts1origin", pts1
//          , "pts2origin", pts2    
//          , "C", C
//          , "faces", let(f=range(_pts1))[ for(i= [0:f-1])
//                                          [ i
//                                          , f+i
//                                          , i==f-1?f:i+1+f
//                                          , i==f-1?0:i+1 
//                                          ]
//                                     ]
//          , "debug", _debug 
//          ]
//   :   
//         NOTE: When getting angle, we have to typecast it to str,
//               then back to num, 'cos sometimes it's tricky 
//               that two angles both should be 120, and they
//               do display as 120, but internally one could be
//               120.0000001. Note: OpenSCAD displays number this way:
//        
//                 Internally    displayed 
//                 120.00004  :  120.00004
//                 120.000004 :  120
//                 120.00005  :  120.00005
//                 120.000005 :  120.00001
//              deci:  123456 
//                
//               For 120.0000001, if we str(a) it, it will be "120", it 
//               will then have actual value as 120 when num(str(a))
//    let ( ai = num(str( _i * 360/n ))      // Current angle for n 
//        , angPt = anglePt( pqr, a=ai, Rf=N ) // Any pt such that a_Pts1[0]_C_angPt= a
//                                             (and a_Pts2[0]_C_angPt= a )
//                                             I.e., the projection of radline onto
//                                             pts1 ring or pts2 ring
//        , ln_a = [ C, angPt ] // The line radiates from C, at angle a (radline @a)
//         Chk how many i1/i2 the new angle (a) covers:
//        , cover_i1= floor(num(str(ai/da1)))  
//        , cover_i2= floor(num(str(ai/da2)))
//        , lastcover_i1= _i==0?0:floor(num(str((_i-1)*da/da1)))  
//        , lastcover_i2= _i==0?0:floor(num(str((_i-1)*da/da2)))
//
//        /////////////////////////////////////////////////////////
//         Array of [ angle
//                  , pts1_or_pts2
//                  , index corresponding to angle ( i1 or i2)
//                  ] 
//         For pts1: [ [72, 1, 1],[144, 1, 2] ] 
//         These two arrays are generated for each step of n
//          
//         20151001: In case pts1[ cover_i1 ] or pts2[ cover_i2 ]
//                   co-angle with one of the step pt, for example,
//                   when n=6, n1=6, n2=14, then those pts below will
//                   have same angle of 180:
//                   pts1[3], pts2[7], pts of resolution [3] 
//                   So we need to avoid the duplicates by introducing
//                   num(str(i*da1))!=a) and num(str(i*da2))!=a) here
//        , a1s =lastcover_i1==cover_i1 ?[]:
//               [ for(i= [ lastcover_i1+1:cover_i1]) 
//                   if(i*da1<360 
//                       && num(str(i*da1))!=ai)  // see note above (20151001)
//                   [ i*da1, 1, i ]                 
//               ]
//                   
//         20151001: a1angs: angles for pts1, needed for avoiding duplicate
//                   in getting a2s           
//        , a1angs = [ for(api1=a1s) api1[0] ]
//            
//         For pts2: [ [90, 2, 1],[180, 2, 2] ] 
//        , a2s =lastcover_i2==cover_i2?[]:
//               [ for(i= [ lastcover_i2+1:cover_i2]) 
//                   if(i*da2<360 
//                       && num(str(i*da2))!=ai // See note above (20151001)
//                       && !has(a1angs,i*da2) // When not already in pts1's angles
//                     )
//                   [ i*da2, 2, i ]
//               ]
//                 
//         Merge them and sort: 
//         [ [72, 1, 1], [90, 2, 1], [144, 1, 2], [180, 2, 2] ] 
//        , _aipairs = sort( concat( a1s,a2s) )
//        
//        , _aipairs = [ for(pair=__aipairs) if(pair[0]!=a) pair ]
//            
//         If the very first angle is the same as the previous angle (=a-da),
//         then we skip that aipair, 'cos it should be already handled in
//         previous step            
//        , aipairs= 
//                 _aipairs[0][0] == ai-da ? 
//                 slice( _aipairs,1)
//                 : _aipairs                         
//           
//        ////////////////////////////////////////////////////////           
//         New pts to be added to _pts1 or _pts2            
//        , newpts1s= 
//                 
//             !aipairs?[]
//             :[ for(i = range(aipairs)) 
//                 let( aipair=aipairs[i] // Each aipair could be either pts1 or pts2 
//                    , ai = aipair[0]    // angle 
//                    , is1 = aipair[1]==1 // Is this pts1 ?
//                    )
//                 is1 ? pts1[ aipair[2] ] // If yes, simply add the pt
//                     
//                 : // If it's pts2, project it to the last seg of pts1.
//                   intsec( [C, pts2[ aipair[2]] ]
//                         , get2( pts1
//                               , floor(
//                                   aipair[0] // angle made by pts2[ aipair[2] ] 
//                                   / da1 )       
//                               )
//                         )               
//             ] // for
//         
//        , newpts2s= 
//         
//             !aipairs?[]
//             :[ for(i = range(aipairs))
//                 let( aipair=aipairs[i] // [angle, 1 or 2, _i1 or _i2]
//                    , ai = aipair[0] 
//                    , is2 = aipair[1]==2
//                    )
//                 is2 ? pts2[ aipair[2] ]
//                 : intsec( [C, pts1[ aipair[2]] ]
//                         , get2( pts2
//                               , floor(
//                                   aipair[0] // angle made by pts1[ aipair[2] ] 
//                                   / da2 )       
//                               )
//                         )               
//             ] //for 
// 
//        , _pts1= concat( _pts1, newpts1s
//                                        
//                        Add pt where ln_a intsec w/ current seg of pts1
//                       , ai>=360 
//                         || ai== last(aipairs)[0] ?[] // The a might be the same as 
//                                                      the last a1 or a2, so we 
//                                                      don't need to add it twice
//                                                      For example, if n=4 and n1=4,
//                                                      all a's will be the same as a1's
//                            :[ intsec( ln_a
//                             , get2( pts1, cover_i1)                              
//                             ) ] 
//                        )                  
//      
//        , _pts2= concat( _pts2, newpts2s 
//                                        
//                        Add pt where ln_a intsec w/ current seg of pts2
//                       , ai>=360 || ai== last(aipairs)[0] ?[]
//                            :[intsec( ln_a
//                                    , get2( pts2, cover_i2) //a2s?last(a2s)[2]:_i2
//                                    )]
//                       )             
//        )//let
//                 
//           getLoftData( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
//            , debug = debug
//                                             
//            , _pts1= _pts1                  
//          
//            , _pts2= _pts2
//
//            , _i  = _i+1
//            , _debug = (debug==true || debug==_i || has(debug,_i))? str(
//                    "<br>", _debug
//                  , "<br><b>_i</b>=", _i , ", <b>ai</b> (angle @ step _i)=", ai
//                  , _s("<br/><u>Calculate the indices of pts1,pts2 at step {_})</u>:",_i)
//                  , "<br/>--- (ai/da1)=",(ai/da1), ", (ai/da2)=", (ai/da2)
//                  , ", <b>cover_i1</b>=",cover_i1, ", <b>cover_i2</b>=", cover_i2
//                  , ",<br/>--- (_i-1)*da/da1=",(_i-1)*da/da1, ", (_i-1)*da/da2=", (_i-1)*da/da2,
//                  , ", <b>lastcover_i1</b>=",lastcover_i1, ", <b>lastcover_i2</b>=", lastcover_i2
//                  , ",<br/>--- cover_i1*da1=",cover_i1*da1, ", cover_i2*da2=", cover_i2*da2
//
//                  , ",<br/>--- <b>a1s</b>=",a1s, ", <b>a2s</b>=", a2s
//                  ,  ", <b>aipairs</b>=", aipairs
//
//                  , _s("<br/><u><b>newpts1s or ~2s</b> are pts to be added in step {_} as a result of pts1 or pts2 (means, excluding pts caused by resolution, n)</u>:",_i)
//                  , "<br/>--- <b>len(newpts1s)</b>=",len(newpts1s)
//                            , ", <b>len(newpts2s)</b>=",len(newpts2s) 
//                  , "<br/>--- <b>newpts1s</b> = ", [ for(ps=newpts1s)[for(p=ps)is0(p)?0:p]]
//                  , "<br/>--- <b>newpts2s</b> = ", [ for(ps=newpts2s)[for(p=ps)is0(p)?0:p]]
//                  ,_s("<br/><u><b>_pts1</b> and <b>_pts1</b> are pts obtained after step {_}</u>:",_i)
//                  , "<br/> <b>_pts1</b>= ",len(_pts1),": ", [ for(ps=_pts1)[for(p=ps)is0(p)?0:p]]
//                  , "<br/> <b>_pts2</b>= ",len(_pts2),": ", [ for(ps=_pts2)[for(p=ps)is0(p)?0:p]]
//                  , "<br/>" ):_debug
//            )
//           
//        -------------------------------------------------------
//        
//  
//);  // getLoftData()           
//
//} // getLoftData

//========================================================
{ // getMoveData
function getMoveData(from,to, debug=0)=
(   
    // Return a hash as move action data for either pts or obj.
    //  
    //   [ "from", [[2, 4, 4], [2, 3, 4], [0, 3, 4]]
    //   , "to", [[1, 0, 0], [0, 0, 0], [0, 1, 0]]
    //   , "rotaxis", [[0, 0, 0], [0, 0, -1]]
    //   , "rota", -90
    //   , "rotaxis2", [[0, 0, 0], [1, 0, 0]]
    //   , "rota2", 0
    //   ]
    //
    // 2018.6.8: Revisited to fix issues when from is aligned with axes
    //           Checked out dev_movePts_more_checkups_move_to_ORIGIN_1868()
    //           in demo_Move_movePts.scad
    //
    // Pts of *from*: [P,Q,R]
    // Pts of *to*  : [S,T,U]
    let( S = to[0], T = to[1], U = to[2]
       , from = und(from, ORIGIN_SITE)
       
       // Translate 'form' to 'to' ==> form2
       
       , from2 = [for(p=from)p-from[1]+T]
       , isParallel = iscoline( from2[0], [S,T]) 
       , isOpposite = isParallel &&
                      !isequal( from2[0]
                       , onlinePt( [T,S], len=dist(from[0],from[1]) )
                       )
                       
       // Rot from2[0] to [S,T] line ==> form_rot
           
       , pl_rot1 = isOpposite? 
                     [from2[0], T, N(to)]
                   :isParallel? from2                       
                   : [ from2[0],T,S ] // the pl where rot1 is on
       , N = N( pl_rot1 )     
       , rotaxis = [T, N]
       , rota = isOpposite?180:isParallel?0:-angle( pl_rot1, Rf=N )
       , from_rot = isOpposite? rotPts( from2, axis= rotaxis, a = rota)
                    : isParallel? from2
                    : rotPts( from2, axis=rotaxis, a = rota)
       
       // Rot from_rot[2] to [S,T,U] plane
       
       , Rrot = from_rot[2]
       , Jst = projPt( Rrot, [S,T] )
       , Jstu= anglePt( [S,Jst,U], a=90, len = dist( Rrot, Jst))
       
       , rotaxis2 = [T,S] 
       , rota2 = angle( [Rrot,Jst,Jstu]  
                ,Rf= onlinePt( [T,S] 
                     , len=-2*dist(Jst,S))// x2: ensure correct angle
                )
       , rtn = [ "from", from
               , "to", to
               , "rotaxis", rotaxis
               , "rota", rota
               , "rotaxis2", rotaxis2
               , "rota2", rota2
               ]
       )
   !debug? rtn
   : echo( _bcolor("====== DEBUG getMoveData ====== ","lightblue"))
     echo(Nto=N(to), onlinept= onlinePt( [T,S], len=dist(from[0],from[1]) )
                                , from2_0= from2[0]
                                 )
     echo( from=from )
     echo( to=to, _cmt("// to= [S,T,U]" ))
     
     echo( _blue("// Translate 'form' to 'to' ==> form2" ))
     echo(from2= from2, _cmt("// 'from' translated to 'to'" ))
     echo(isParallel= isParallel, _cmt("// Is from2[2] coline w/ [S,T]" ))
     echo(isOpposite= isOpposite, _cmt("// Is from2[2] opposite dir of [T,S]" ))
     
     echo( _blue("// Rot from2[0] to [S,T] line ==> form_rot" ))
     echo(pl_rot1= pl_rot1, _cmt("// The pl where rot1 is on = [ from2[0],T,S ]" ))
     echo(N= N, _cmt("// N for the axis of rot1" ))
     echo( rotaxis = rotaxis, _cmt("// = [T,N]" ))
     echo( rota= rota, _cmt("// = -angle( plrot1, Rf=N)" ))
     echo( from_rot= from_rot, _cmt("// 'form' after 1st rot" ))

     echo( _blue("// Rot from_rot[2] to [S,T,U] plane" ))
     echo(Rrot= Rrot, _cmt("// = from_rot[2]") )
     echo(Jst= Jst, _cmt("// Rrot proj to ST" ) )
     echo(Jstu= Jstu, _cmt("// anglePt([S,Jst,U]..., len = dist( Rrot, Jst) " ) )
     echo( rotaxis2 = rotaxis2, _cmt("// = [T,S]" ) )
     echo( rota2 = rota2)
     rtn
);

function getMoveData_1867(from,to, debug=0)=
(   // dev: Refer to scadx_obj.scad:move_dev5,6()
    // Pts of *from*: [P,Q,R]
    // Pts of *to*  : [S,T,U]
    let( S = to[0], T = to[1], U = to[2]
       , from = und(from, ORIGIN_SITE)
       , from_translated = [for(p=from)p-from[1]+T]
       , isParallel = iscoline( from_translated[0], [S,T]) //p01(to))    
       , pl_rot1 = isParallel? from_translated
                   : [ from_translated[0],T,S ] // the pl where rot1 is on
       , N = N( pl_rot1 )     
       , rotaxis = [T, N]
       , rota = isParallel?0:-angle( pl_rot1, Rf=N )
       
       , from_rot = rotPts( from_translated, axis=rotaxis, a = rota)
       , Rrot = from_rot[2]
       , Jst = projPt( Rrot, [S,T] ) //p01(to))
       , Jstu= anglePt( [S,Jst,U], a=90, len = dist( Rrot, Jst))
       
       , rotaxis2 = [T,S] //p10(to)
       , rota2 = angle( [Rrot,Jst,Jstu]  
                ,Rf= onlinePt( [T,S] //p10(to)
                     , len=-2*dist(Jst,S))// x2: ensure correct angle
                )
       , rtn = [ "from", from
               , "to", to
               , "rotaxis", rotaxis
               , "rota", rota
               , "rotaxis2", rotaxis2
               , "rota2", rota2
               ]
       )
   debug?
     echo( "====== DEBUG getMoveData ====== ")
     echo( from=from )
     echo( to=to )
     
     echo(from_translated_to_to= from_translated)
     echo(isParallel= isParallel)
     
     echo( rotaxis = rotaxis )
     echo( rota= rota )
     echo( rotaxis2 = rotaxis2 )
     echo( rota2 = rota2)
     echo(pl_rot1= pl_rot1)
     echo(N= N)
     echo(from_rot= from_rot)
     echo(Rrot= Rrot)
     echo(Jst= Jst)
     echo(Jstu= Jstu)
     rtn
   : rtn
);

function getMoveData_new_try_1867(from,to)=
(   // dev: Refer to scadx_obj.scad:move_dev5,6()
    // Pts of *from*: [P,Q,R]
    // Pts of *to*  : [S,T,U]
    let( S = to[0], T = to[1], U = to[2]
       , from = und(from, ORIGIN_SITE)
       , from_translated = [for(p=from)p-from[1]+T]
       //, isParallel = iscoline( from_translated[0], [S,T]) //p01(to))    
       //, pl_rot1 = isParallel? from_translated
       //            : [ from_translated[0],T,S ] // the pl where rot1 is on
       , pl_rot1 = isequal( from_translated[0], to[0]) ? from_translated
                   : [ from_translated[0],T,S ] // the pl where rot1 is on
       , N = N( pl_rot1 )     
       , rotaxis = [T, N]
       , rota = isParallel?0:-angle( pl_rot1, Rf=N )
       
       , from_rot = rotPts( from_translated, axis=rotaxis, a = rota)
       , Rrot = from_rot[2]
       , Jst = projPt( Rrot, [S,T]) //p01(to))
       , Jstu= anglePt( [S,Jst,U], a=90, len = dist( Rrot, Jst))
       
       , rotaxis2 = [T,S] //p10(to)
       , rota2 = angle( [Rrot,Jst,Jstu]  
                ,Rf= onlinePt( [T,S] //p10(to)
                     , len=-2*dist(Jst,S))// x2: ensure correct angle
                )
       )
   [ "from", from
   , "to", to
   , "rotaxis", rotaxis
   , "rota", rota
   , "rotaxis2", rotaxis2
   , "rota2", rota2
   // debug:
   , "<b>DEBUG:</b>","===>"
   , "from_translated", from_translated
   //, "isParallel", isParallel
   , "pl_rot1", pl_rot1
   , "N", N
   , "from_rot", from_rot
   , "Rrot", Rrot
   , "Jst", Jst
   , "Jstu", Jstu
   ]
);

    getMoveData=["getMoveData","from,to","arr","Geometry"
    , "Get two sets of axes and rotation angles for moving pts or obj
    ;; from *from* to *to*, both are 3-pointer. Return:
    ;;
    ;; [ 'rotaxis', rotaxis
    ;; , 'rota', rota
    ;; , 'rotaxis2', rotaxis2
    ;; , 'rota2', rota2
    ;; ]
    ;;
    ;; This is used in movePts() to move polyhedra, and Move() to 
    ;; move shapes. Note the capital 'M' indicates it's object-related.
    "];

} // // getMoveData

{ // getDecoData 

function getDecoData( color, Frame, MarkPts, Dim, opt=[] )=
(           
  //let( _Frame = und_opt(Frame, opt,"Frame", 0.01)
     //, Frame = isnum(_Frame)? ["r", _Frame]: _Frame
     //) // let
//
  //[ "color", getColorArg(color, opt) // Assure color is [name, transp]
  //, "Frame", update( Frame, ["shape", hash(opt, "FrameShape","rod")] )
  //, "MarkPts", subprop(MarkPts, opt, "MarkPts", []) 
  //, "Dim", concat( hash(opt,"Dim",[]), Dim?Dim:[] )
  //]
  //

  [ "color", ColorArg(color, opt) // Assure color is [name, transp]
  , "Frame", subprop( Frame, opt, "Frame"
                    , dfPropName="r"
                    , dfPropType="num"
                    , df=["r",0.01, "FrameShape", "rod"]
                    )
                    //, df=["shape", hash(opt, "FrameShape","rod")] )
  , "MarkPts", subprop(MarkPts, opt, "MarkPts", []) 
  , "Dim", subprop(Dim, opt, "Dim",[]) 
  ]
  
);
}

function getNewCoord(pt, newCoordVecs, oldCoordVecs=[X,Y,Z])= // newCoordVecs: pq or pqr
(
  len(newCoordVecs)==2?
     solve2( transpose(newCoordVecs),pt*(oldCoordVecs==undef?[X,Y]:oldCoordVecs) )
  :  solve3( transpose(newCoordVecs),pt*oldCoordVecs)
);


//========================================================
{ //  getPairingData
function getPairingData(pts1, pts2,C1,C2, rot=0)=
(
    /* FOR: punch hole, grow something, connect 
    */
    
     let(C1= C1?C1:centerPt(pts1)
       , C2= C2?C2:centerPt(pts2)
       //, 
       ,_a = 0, _f=1, _i=2, _p = 3

       // afip: [ang,face,i,p] where face = 1 or 2       
       , afipsdata= getPairingData_sorted_afips(
                   pts1, pts2,C1,C2)
       , afips = hash( afipsdata, "afips" )
       , afips1 = hash( afipsdata, "afips1" ) // [ [a,face,i,p], ... ]
       , afips2 = hash( afipsdata, "afips2" )
       //, afips = sort( concat( afips1, afips2),0,1)
       , asegs1 = getPairingData_ang_seg(pts1) //[ [a0,[P0,P1]], [a1,[P1,P2]]..
       , asegs2 = getPairingData_ang_seg(pts2)
              
       , lasti = len( afips )-1
            
       , pts1new= [ 
       
           for(i=[0:lasti] ) 
            /* 
                Looping through these:
           
                [[0, 1, 0, [2.4716, -5.72261, -1.19268]]
                ,[0, 2, 0, [0.804891, -3.30039, 0.705988]]
                ,[60, 2, 1, [-0.626286, -2.96622, 0.405886]]
                ,[120, 1, 1, [-4.68, -1.1484, 0.519362]]
                ,[120, 2, 2, [-1.34059, -1.92813, 1.2196]]
                ,[180, 2, 3, [-0.623719, -1.22421, 2.33342]]
                ,[240, 1, 2, [2.48016, 0.0840993, 5.23243]]
                ,[240, 2, 4, [0.807458, -1.55838, 2.63352]]
                ,[300, 2, 5, [1.52176, -2.59647, 1.81981]]
                ]
           */
            // Skip items that have angle already appeared :
            if(  str(afips[i][_a])!= str(afips[i-1][_a])
              )
                
            let( 
                 afip = afips[i]    // [ [a,face,i,p], ... ]
                , a  = afip[_a] 
                , p  = afip[_p]
                , pt = afip[_f]==1? // afip[_f]=1: this afip
                       p            // is from afips1 so 
                                    // we give its p back
                  : // afip[_f]=2, like [97, 2, 2, [3.94,-2.06,-0.79]]:
                    // The point, afip[_p], is from pts2, means we need 
                    // to create a new pt on afips1 using this afip[_p]
                    [for(xi=range(asegs1))
                        let(aseg = asegs1[xi] // [[90,180],[p,q]]
                           , as= aseg[0]     // [90,180]
                           //, pq= aseg[1]
                           )
                         if(as[0]< a && a < as[1]) 
                             let( ang = a-as[0] 
                                , pg = aseg[1]
                                , p3 = [ pts1[xi], C1
                                       , get(pts1,xi+1, cycle=true)
                                         //   , pts1[fidx(pts1,xi+1)]
                                        ]
                                , p_at_ang = anglePt(p3, a=ang) 
                                )
                         //[p3, p_at_ang ]
                         projPt( C1, pg, [C1,p_at_ang] )
                     ][0]  
                )

               pt                

           ]                 

       , pts2new= [ 
       
           for(i=[0:lasti] ) 
            // Skip items that have same angle as their next one:
            if(  str(afips[i][_a])!= str(afips[i+1][_a]) )
              let( 
                 afip = afips[i]    // [ [a,face,i,p], ... ]
                , a  = afip[_a] 
                , p  = afip[_p]
                , pt = afip[_f]==2? // afip[_f]=1: this afip
                       p            // is from afips1 so 
                                    // we give its p back
                  : // afip[_f]=1, like [97, 1, 2, [3.94,-2.06,-0.79]]:
                    // The point, afip[_p], is from pts1, means we need 
                    // to create a new pt on afips2 using this afip[_p]
                    [for(xi=range(asegs2))
                        let(aseg = asegs2[xi] // [[90,180],[p,q]]
                           , as= aseg[0]     // [90,180]
                           //, pq= aseg[1]
                           )
                         if(as[0]< a && a < as[1]) 
                             let( ang = a-as[0] 
                                , pg = aseg[1]
                                , p3 = [ pts2[xi], C2
                                            , get(pts2,xi+1,cycle=true)]
                                , p_at_ang = anglePt( p3, a=ang) 
                                )
                         projPt( C2, pg, [C2,p_at_ang] )
                    ][0]  
               )         
                             
           pt                

           ]
       , n= len(pts1new)
       , 2n = n*2
           
        )// let                     
      [ "pts", concat( pts1new, pts2new )
      , "pts1", pts1new
      , "pts2", pts2new
      , "pts1origin", pts1
      , "pts2origin", pts2    
      , "rdiffs", [ for(i=range(pts1new))
                      let(r1= dist( pts1new[i],C1)
                         ,r2= dist( pts2new[i],C2)
                         , d= r2-r1 )
                      [ is0(d)?0:d, r1, r2]
                  ]     
      , "afips", afips
      , "afips1", afips1
      , "afips2", afips2
      , "angles", hash(afipsdata,"angles")
      , "C1", C1
      , "C2", C2
      , "asegs1", asegs1
      , "asegs2", asegs2
      , "faces", [ for(i= [0:n-1])
                      [ i
                      , n+i
                      , i==n-1?n:i+1+n
                      , i==n-1?0:i+1 
                      ]
                 ]     
      ] 
);    

    getPairingData= ["pairPts","pts1, pts2,C1,C2, rot=0","hash","Geometry",
     "Given two sets of co-center pts, pts1 and pts2 (having center  
    ;;  C), on the the same plane,     
    ;;        
    ;;      N-_
    ;;         `-_
    ;;    ---B    M
    ;;        `.   :
    ;;    C-----A---L---P0   
    ;;        pts1  pts2
    ;;       
    ;;  scanning through pts by angle a, add pts to pts1 if pts2[a] 
    ;;  is found, or to pts2 if pts1[a] is found:  
    ;;    
    ;;       N B2
    ;;         `-_
    ;;    ---B    M
    ;;        `M1  :
    ;;    C-----A---L'---P0   
    ;;        pts1  pts2
    ;;
    ;; The rot is the offset given to all pts of pts2
    ;;
    ;; Return a hash:
    ;;   ['afips', afips
    ;;    ,'afips1', afips1
    ;;    ,'afips2', afips2
    ;;    ,'angles', uniq( [for(afip=afips) afip[0]] )
    ;;    ,'pts1',pts1
    ;;    ,'pts2',pts2
    ;;    ,'C1', C1
    ;;    ,'C2', C2
    ;;    ]
    ;; 
    ;; In which when scanning angle from 0 to 360, pts in both pts1
    ;; and pts2 appear in pairs.
    ;;
    ;; This is useful for some situations:
    ;; 1) Lofting
    ;; 2) Connecting two chains
    ;; 3) Punching a hole
    "];
           
    function getPairingData_test( mode=MODE, opt=[] )=
    (
        doctest( getPairingData, [], mode=mode, opt=opt )
    ); 

} // getPairingData

//========================================================
{ // getPairingData_sorted_afips
function getPairingData_sorted_afips(pts1, pts2,C1,C2)=
(   
    /* 2015.7.23
    
    This is to be called inside getPairingData()

    afips: [angle, face, i, pt]
        where face = 1 or 2 for pts1, pts2, resp.

    Usage:

    pqr =  [[2.02391, 0.977828, 2.76615], [2.51219, -1.8059, -1.15628], [1.7283, -2.1596, 0.479486]];
    pl1= arcPts( pqr, rad=3, a=90, a2= 360/4*3, n=4);
    pl2= arcPts( pqr, rad=1.5, a=90, a2= 360/5*4, n=5);
    afips = pairPts_sorted_afips( pl1,pl2,C1=pqr[1] );
    for(x=afips[0]) echo(x); 

    ECHO: [0, 1, 0, [1.09593, -4.04264, 0.254821]]
    ECHO: [0, 2, 0, [1.80406, -2.92427, -0.450729]]
    ECHO: [72, 2, 1, [3.54269, -2.63013, -0.443046]]
    ECHO: [90, 1, 1, [5.13943, -2.81244, -0.114897]]
    ECHO: [144, 2, 2, [3.85721, -1.19693, -1.42103]]
    ECHO: [180, 1, 2, [3.92845, 0.43084, -2.56738]]
    ECHO: [216, 2, 3, [2.31295, -0.605306, -2.03314]]
    ECHO: [270, 1, 3, [-0.115054, -0.799365, -2.19766]]
    ECHO: [288, 2, 4, [1.04404, -1.67286, -1.43346]]
    */
     let( C1=C1?C1:centerPt(pts1)
        , C2= C2?C2:centerPt(pts2)
        , _a = 0, _f=1, _i=2, _p = 3

        , tmpfactor= 1 //10e2
        , rot=0
        
        , Rf1 = N( [pts1[0], C1, pts1[1]] )
        , Rf2 = N( [pts2[0], C2, pts2[1]] )
        //
        // afip: [ang,face,i,p] where face = 1 or 2
        // NOTE: When getting angle, we have to typecast it to str,
        //       then back to num, 'cos sometimes it's tricky that
        //       that two angles that both should be 120, and they
        //       do display as 120, but internally one could be
        //       120.0000001. Note: OpenSCAD displays number this way:
        //
        //         Internally    displayed 
        //         120.00004  :  120.00004
        //         120.000004 :  120
        //         120.00005  :  120.00005
        //         120.000005 :  120.00001
        //       deci:  123456 
        //        
        //       For 120.0000001, if we str(a) it, it will be "120", it 
        //       will then have actual value as 120 when num(str(a))
        
        , afips1= [ for(i=range(pts1)) let(p=pts1[i])
                    let( _aa= angle( [pts1[0], C1, p], Rf=Rf1 ) 
                       , a= round(100*(is0(_aa)?0:_aa))/100
                       )
                    [ num(str((i==0?0:((a<0?360:0)+a))*tmpfactor)), 1, i,p ]
                  //  [ num(str((i==0?0:((a<0?360:0)+a))*tmpfactor)), 1, i,p ]
                  ]
       , afips2= [ for(i=range(pts2)) let(p=pts2[i])
                   let( _aa= angle( [pts2[0], C2, p], Rf=Rf2 ) 
                      , a= round(100*( is0(_aa)?0:_aa))/100
                      )
                  [ (i==0?0:((a<0?360:0)+a))+rot, 2, i,p ]
//                  [ num(str(((i==0?0:((a<0?360:0)+a))+rot)*tmpfactor)), 2, i,p ]
                 ]         
       //
       // sort first by angle (at i=0) then by face( at i=1):
       // 
       , afips= sort( concat( afips1, afips2 ),0,1)
//       , afips = [ for(x=_afips) app(x, type(x[0]))]
//       , _afips= sort( concat( afips1, afips2 ),0)
//       , angs= uniq([ for(x=_afips) x[_a] ])
//       , anghash = [ for(a=angs) [a,[]] ]
//       , _afips2= [ for(ah = anghash) ah
////                      for( afip= _afips)
////                         if( afip[_a]==ah[0]) 
////                            update( anghash, [ah[0], app( anghash[1], afip)])
//                  ]
//       , afips = angs //_afips2 //last( _afips2)                  
        )                     
    //[afips, afips1, afips2, C1, C2] 
    ["afips", afips
    ,"afips1", afips1
    ,"afips2", afips2
    ,"angles", uniq( [for(afip=afips) afip[0]] )
    ,"pts1",pts1
    ,"pts2",pts2
    ,"C1", C1
    ,"C2", C2
    ]   
);
    getPairingData_sorted_afips=["getPairingData_sorted_afips"
    ];
    
    function getPairingData_sorted_afips_test( mode=MODE, opt=[] )=
    (
        doctest( getPairingData_sorted_afips, [], mode=mode, opt=opt )
    ); 
 
} // getPairingData_sorted_afips

//========================================================
{ // getPairingData_ang_seg    
function getPairingData_ang_seg( pts, C, _N, _P0, _rtn=[], _i=0,_debug=[] )=
(
    // 2015.7.24 for use in getPairingData   
    //           Return [ [ [aP,aQ],[P,Q]], [ [aP2,aQ2],[P2,Q2]] ... ]
    //                  for each seg
    // c: center (optional)   
    let ( C= C==undef?centerPt(pts):C
        , P = pts[_i]
        , Q = get(pts, _i+1, cycle=true)
        , _N= _N==undef?N([pts[0],C, pts[1]]):_N
        , _P0= _P0!=undef?_P0: pts[0]
                  //rot? rotPt( pts[0], [_N,C], rot)
                  // pts[0]
        ,_aP = num(str(angle( [ _P0,C,P], Rf= _N ) ))
        ,_aQ = num(str(angle( [ _P0,C,Q], Rf= _N ) ))
        , aP = _i==0? _aP
              : (_aP< last(_rtn)[0][0])? (_aP+360):_aP
        , aQ = _i==0? _aQ
              : (_aQ< last(_rtn)[0][1])? (_aQ+360):_aQ
        ,rtn = app( _rtn, [ [ round(100*aP)/100, round(100*aQ)/100 ],[P,Q] ] ) 
//        ,_debug= str( _debug
//                 , "<br/><b style='color:red'>"
//                 ,   "getPairingData_ang_seg()</b>, _i= </br>", _i
//                 , "<br/><b>pts</b>= ", pts
//                 , "<br/><b>P</b>= ", P
//                 , "<br/><b>Q</b>= ", Q
//                 , "<br/><b>C</b>= ", C
//                 , "<br/><b>_N</b>= ", _N
//                 , "<br/><b>_P0</b>= ", _P0
//                 , "<br/><b>a</b>= ", a
//                 , "<br/><b>rtn</b>= ", _colorPts(rtn)
//                 )
        )
    //_i==len(pts)-1? _debug  
    _i==len(pts)-1? rtn
    : getPairingData_ang_seg( pts=pts, C=C, _N=_N
                        , _P0=_P0, _rtn=rtn, _i=_i+1, _debug=_debug )    
);       
              
//a= ["a","b","c"];
//echo( get= get(a, 3, cycle=true));
} // getPairingData_ang_seg    

//========================================================
{ // getPairingData2_dev
function getPairingData2_dev( pts1, pts2, n=6, rot=0, C=undef
                        , _pts1=[], _pts2=[]
                        , _i=0
                        , _n2=6 // _n2 is the actual final # of pts 'cos 
                                // we gonna add pts to n during process
                        , _i1=0, _i2=0
                        , _a1=0, _a2=0
                        , _P1taken=0, _P2taken=0
                        , _debug="" 
                        )=       
( /* 
      2015.9.18
      
      The first version, getPairingData(), works well but slow.
      Attempting a new approach using recursion.
      
      Also, add n=24 to indicate
      how many pts wanted around the circle, for case when
      len(pts1) and/or len(pts2) are small, which created 
      unsmooth surface.
      
      2015.9.20
      
      This code works pretty well, but not perfect yet. We stop
      it here to leave it for future reference. In the mean time,
      copy this code and rename to loftPts() for further dev. 
      

  */
         /* 
                  pts2
                      `.
                        `.1
                        .'%
          pts1 ...1   .'   %
                 / `.'      % 
                / .' `.      % 
               /.'     `.     %
              o----------0-----0 

       When _i advances, a new a is calc.
         

*/  
   let( C = C?C:centerPt(pts1)
      , pqr = [ pts1[0], C, pts1[1] ]
      , N = N( pqr )
      , rad1 = dist( C, pts1[0] )
      , rad2 = dist( C, pts2[0] )
      , da1 = num(str( 360/len(pts1) ))
      , da2 = num(str( 360/len(pts2) ))
      , da = num(str( 360/n ))
      , a = num(str( _i * 360/n ))
            // NOTE: When getting angle, we have to typecast it to str,
            //       then back to num, 'cos sometimes it's tricky that
            //       that two angles both should be 120, and they
            //       do display as 120, but internally one could be
            //       120.0000001. Note: OpenSCAD displays number this way:
            //
            //         Internally    displayed 
            //         120.00004  :  120.00004
            //         120.000004 :  120
            //         120.00005  :  120.00005
            //         120.000005 :  120.00001
            //      deci:  123456 
            //        
            //       For 120.0000001, if we str(a) it, it will be "120", it 
            //       will then have actual value as 120 when num(str(a))
      )   
   _i==0? getPairingData2(pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                        , _pts1= [ pts1[0] ], _pts2=[ pts2[0] ]
                        , _i=1, _n2=n
                        , _i1= 0
                        , _i2= 0
                        , _P1taken=1, _P2taken=1
                        //, _justpass1=1, _justpass2=1
                        , _debug = str( "<b>getPairingData2</b>"
                                      , "<br/>_i=0"
                                      )
                        )
            
   //:a>=360?
   : _i>4? 
          [ "pts", concat( _pts1, _pts2 )
          , "pts1", _pts1
          , "pts2", _pts2
          , "pts1origin", pts1
          , "pts2origin", pts2    
          , "C", C
          , "faces", let(f=range(_pts1))[ for(i= [0:f-1])
                                          [ i
                                          , f+i
                                          , i==f-1?f:i+1+f
                                          , i==f-1?0:i+1 
                                          ]
                                     ]
          , "debug", _debug 
          ]
   : 
     let( P1i = pts1[_i1]
        , P1i1= pts1[_i1+1]
        , P2i = pts2[_i2]
        , P2i1= pts2[_i2+1]
        , pair1= [ P1i, P1i1 ] //get( pts1,[_i1,_i1+1] )
        , pair2= [ P2i, P2i1 ] //get( pts2,[_i2,_i2+1] )
        , pair1next= [ P1i1, pts1[_i1+2] ] //get( pts1,[_i1+1,_i1+2] )
        , pair2next= [ P2i1, pts2[_i2+2] ] //get( pts2,[_i2+1,_i2+2] )
        
        , a1 = _i1*da1 //_a1 + da1
        , a2 = _i2*da2 //_a2 + da2
        , angPt = anglePt( pqr, a=a, Rf=N ) // Any pt such that a_Pts1[0]_C_angPt= a
                                      // (and a_Pts2[0]_C_angPt= a )
        , ln_a = [ C, angPt ] // The line radiates from C, at angle a 
        , X1= intsec( ln_a, pair1)
        , X2= intsec( ln_a, pair2)
        
        , ln1i = [ C, P1i ]
        , ln2i = [ C, P2i ]
        , P1iOn2 = intsec( ln1i, pair2 )
        , P2iOn1 = intsec( ln2i, pair1 )
        , P1iOn2next = intsec( ln1i, pair2next )
        , P2iOn1next = intsec( ln2i, pair1next )
        
        , issamept1 = isSamePt(P1i1, last(_pts1))
        , issamept2 = isSamePt(P2i1, last(_pts2))
        
//        , ln1 = [ C, pts[_i] ]
//        , ln2 = [ C, pts[_2] ]
//        , ln1 = [ C, pts[_i] ]
//        , ln2 = [ C, pts[_2] ]
//          
        )
        //, a1 = num(str( angle( [ pts1[0], C1, pts1[_i+1] ], Rf=N )  ))
        //, a2 = num(str( angle( [ pts2[0], C2, pts2[_i+1] ], Rf=N )  ))
          /*
            12 conditions:
          
           // group 1 ---
           
            a < a1 < a2
            a < a2 < a1
            a < a1 = a2
          
                 a = a1 < a2
            a2 < a = a1

                 a = a2 < a1
            a1 < a = a2
            
            a1 < a < a2
            a2 < a < a1
            a1 < a2 < a
            a2 < a1 < a
            
                a1 = a2 < a
          */ 
         
        a < a1 && a < a2 ?  // group 1
             /*             
                           
                        _-`     _.-'`         
                     i1`   _.i2`      
                  _-` _.-'`   __..--A``
               _-`.-'`..--''``      
             C           
                           i2
                        _-`     _.-'`         
                     _-`   _.-'`      
                  _-` _.i1`   __..--A``
               _-`.-'`..--''``      
             C           
                           i2
                        _-`     _A
                     i1`   _.-'`      
                  _-` _.-'` 
               _-`.-'`
             C           
                             
             */          
          getPairingData2( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                        , _pts1= app( _pts1, intsec( ln_a, pair1) )
                        , _pts2= app( _pts2, intsec( ln_a, pair2) )
                        , _i = _i+1
                        , _i1 = _i1
                        , _i2 = _i2
                        , _a1 = a1  
                        , _a2 = a2
                        , _P1taken=0, _P2taken=0
                        //, _justpass1=0
                        //, _justpass2=0
                        , _debug = str("<br>", _debug
                                  , "<br>_i= ", _i, ", a &lt; a1 && a &lt; a2"
                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
                        )          

         //-------------------------------------------------------
       
        : a==a1 && a==a2 ?
               /*              A       
                            _-`   
                         _i2   
                      _-` 
                   _i1
                  C  
              */
              getPairingData2( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                    , _pts1= app(_pts1, pts1[_i1+1] )
                    , _pts2= app(_pts2, pts1[_i2+1] )
                    , _i  = _i+1
                    , _i1 = _i1+1
                    , _i2 = _i2+2
                    , _a1 = a1  
                    , _a2 = a2
                    , _P1taken=0, _P2taken=0
                        //, _justpass1=0
                    //, _justpass2=0
                    , _debug = str("<br>", _debug
                                  , "<br>_i= ", _i, ", a==a1 && a==a2"
                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
                    )    
         //-------------------------------------------------------
       
        : a==a1 ?
          ( a < a2 ?
               /*                 _      
                               _i2       
                            _-`    _.-'` 
                         _-`   _.a` 
                      _-` _i1'`
                   _-`.-'`
                  C  
              */
              getPairingData2( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                    , _pts1= app(_pts1, pts1[_i1+1]  )
                    , _pts2= app(_pts2, intsec( ln_a, pair2) )
                    , _i  = _i+1
                    , _i1 = _i1+1
                    , _i2 = _i2
                    , _a1 = a1  
                    , _a2 = a2
                    , _P1taken=0, _P2taken=0
                        //, _justpass1=0
                    //, _justpass2=0
                    , _debug = str("<br>", _debug
                                  , "<br>_i= ", _i, ", a==a1 &lt; a2 "
                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" ) 
                    )          
          : // a2 < a ?
             /*               _      
                           _a`       
                        _-`     _.-'` 
                     i1`   _.-i2 
                  _-` _.-'`
               _-`.-'`
            */          
            getPairingData2( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                    , _pts1= concat(_pts1, [ intsec( ln_a, pair1), pts1[_i1+1] ] )
                    , _pts2= concat(_pts2, [ pts2[_i2+1], intsec( ln_a, pair2next) ] )
                    , _i  = _i+1
                    , _i1 = _i1+( a> (_i+1)* da1 ?1:0) 
                    , _i2 = _i2+1
                    , _a1 = a1  
                    , _a2 = a2
                    , _P1taken=0, _P2taken=0
                        //, _justpass1=0
                    //, _justpass2=0
                    , _debug = str("<br>", _debug
                                  , "<br>_i= ", _i, ", a2 &lt; a==a1" 
                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
                    )          
         ) // a==a1
         
         //-------------------------------------------------------
         
        : a==a2 ?
          ( a < a1 ?
               /*                 _      
                               _i1       
                            _-`    _.-'` 
                         _-`   _.a` 
                      _-` _i2'`
                   _-`.-'`
                  C  
              */
              getPairingData2( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                    , _pts1= app( _pts1, intsec( ln_a, pair1) )
                    , _pts2= app( _pts2, pts2[_i2+1] )
                    , _i  = _i+1
                    , _i1 = _i1
                    , _i2 = _i2+1
                    , _a1 = a1  
                    , _a2 = a2
                    , _P1taken=0, _P2taken=0
                    , _debug = str("<br>", _debug
                                  , "<br>_i= ", _i, ", a==a2 &lt; a1"
                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
                    )          
          : // a1 < a ?
             /*               _      
                           _a`       
                        _-`     _.-'` 
                     i2`   _.-i1 
                  _-` _.-'`
               _-`.-'`
            */          
            getPairingData2( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                    , _pts1= concat(_pts1, [ pts1[_i1+1], intsec( ln_a, pair1next) ] )
                    , _pts2= concat(_pts2, [ intsec( ln_a, pair2), pts2[_i2+1] ] )
                    , _i  = _i+1
                    , _i1 = _i1+1
                    , _i2 = _i2+1
                    , _a1 = a1  
                    , _a2 = a2
                    , _P1taken=0, _P2taken=0
                    , _debug = str("<br>", _debug
                                  , "<br>_i= ", _i, ", a1 &lt; a==a2 "
                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
                    )          
          ) // a==a2

        //-------------------------------------------------------
        : a1 < a && a < a2? 
          /*
                        _-`    _.-'`         
                     i2`   _.A`      
                  _-` _.-'`   __..i1'``
               _-`.-'`..--''``      
             C               
         */ 
            getPairingData2( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                    , _pts1= concat(_pts1, [ pts1[_i1+1], intsec( ln_a, pair1next) ] )
                    , _pts2= concat(_pts2, [ intsec( ln_a, pair2 ) ] )
                    , _i  = _i+1
                    , _i1 = _i1+1
                    , _i2 = _i2
                    , _a1 = a1  
                    , _a2 = a2
                    , _P1taken=0, _P2taken=0
                    , _debug = str("<br>", _debug
                                  , "<br>_i= ", _i, ", i1 &lt; a &lt; i2"
                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
                    )             
        //-------------------------------------------------------
        : a2 < a && a < a1? 
          /*
                        _-`    _.-'`         
                     i1`   _.A`      
                  _-` _.-'`   __..i2'``
               _-`.-'`..--''``      
             C               
         */ 
            getPairingData2( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
                    , _pts1= concat(_pts1, [ intsec( ln_a, pair1) ] )
                    , _pts2= concat(_pts2, [ pts2[_i2+1], intsec( ln_a, pair2next ) ] )
                    , _i  = _i+1
                    , _i1 = _i1
                    , _i2 = _i2+1
                    , _a1 = a1  
                    , _a2 = a2
                    , _P1taken=0, _P2taken=0
                    , _debug = str("<br>", _debug
                                  , "<br>_i= ", _i, ", i2 &lt; a &lt; i1" 
                                  , ", ( a=", a, ", a1=", a1, ", a2=", a2, " )" )
                    )             
        //-------------------------------------------------------
        : 
          //     a1 < a2 < a
          //     a2 < a1 < a
          /*
                        _-`    _.-'`         
                     a`   _.i1`      
                  _-` _.-'`   __..i2'``
               _-`.-'`..--''``      
             C               
                        _-`    _.-'`         
                     a`   _.i2`      
                  _-` _.-'`   __..i1'``
               _-`.-'`..--''``      
             C               

          */ 
           let( ln1 = [ C,pts1[_i1] ]
              , ln2 = [ C,pts2[_i2] ] 
              , p1taken = _i==1 || (a-a1)>=da1
              , p2taken = _i==1 || (a-a2)>=da2
              , newpt1s = concat( 
                                  a2<a1 && !p2taken ? 
                                    [intsec( ln2, get2( pts1, _i1-1) )]:[]
                                    
                                , !p1taken? [P1i]:[]
                                
                                , a2>a1 && !p2taken ? 
                                    [intsec( ln2, get2( pts1, _i1) )]:[]
                                    
                                , [ intsec( ln_a, get2( pts1, _i1) ) ]
                                )  
              
              , newpt2s = concat( 
                                  a1<a2 && !p1taken ? 
                                    [intsec( ln1, get2( pts2, _i2-1) )]:[]
                                    
                                , !p2taken? [P2i]:[]
                                
                                , a1>a2 && !p1taken ? 
                                    [intsec( ln1, get2( pts2, _i2) )]:[]
                                    
                                , [ intsec( ln_a, get2( pts2, _i2) ) ]
                                ) 
              )
           getPairingData2( pts1=pts1, pts2=pts2, n=n, rot=rot, C=C
            , _pts1= concat( _pts1, newpt1s)                                     
            , _pts2= concat( _pts2, newpt2s)

            , _i  = _i+1
            , _i1 = _i1+( a+da> (_i1+1)* da1 ?1:0)
            , _i2 = _i2+( a+da> (_i2+1)* da2 ?1:0)
            , _a1 = a1  
            , _a2 = a2
            , _P1taken=0, _P2taken=0
            , _debug = str("<br>", _debug
                          , "<br>_i=", _i, ", _i1=", _i1, "_i2=", _i2 
                          , ", a1&lt;a2&lt;a, a2&lt;a1&lt;a"
                          , ", ( a=", a, ", a1=", a1, ", a2=", a2
                          ,     ", da1=",da1, ", da2=", da2
                          ,     ", p1taken=", p1taken, ", p2taken=", p2taken
                          , " )" )
            )             
        //-------------------------------------------------------
);  // getPairingData2_dev()

} // getPairingData2_dev

//========================================================
function getPlaneByNormalLine(pq)=
(
  // Create a plane that is normal to pq and 
  // pass through pq[0] 
  let( op = randOfflinePt(pq)
     , N = N( [pq[1], pq[0], op] )
     , B = B( [pq[1], pq[0], op] )
     //, B = N2( [pq[1], pq[0], N] )
     )
  [pq[0], N, B]
);

    module getPlaneByNormalLine_demo()
    {
      	pq = randPts(2);
        pl = getPlaneByNormalLine( pq );

        Line(pq);
        echo(pq=pq, pl=pl);
        MarkPts(pq);
        MarkPts(pl, ["plane", ["color",["blue", .5]]]);
        Mark90( [pq[1], pq[0], pl[1]] );
        Mark90( [pq[1], pq[0], pl[2]] );
   }

//========================================================
function gmedian( pts
                , prec=0.01
                , nMaxSteps = 20 // <== just for safeguard
                , d= 10  // Intv for each search. For [x,y], chk 4 pts: 
                         //   [x+d,y+d],[x+d,y-d], [x-d,y+d], [x-d,y-d] 
                         // If, say, [x+d,y-d] has smaller SOD (sum of dist)
                         // than [x,y], use it as the next [x,y] and 
                         // repeat search. If none is smaller, than half
                         // the d to repeat search.
                         // Search stops when d < prec 
                , _pt 
                , _sod   // sum of dist
                , _i=-1)=
(  // Geometrical Median. 
   // https://stackoverflow.com/questions/12934213/how-to-find-out-geometric-median
   // Note: a gmedian tends to be close to where pts cluster
   
   _i==-1?
     //echo(log_b(["gmedian() init: _i",_i]))
     let( _pt= sum(pts)/len(pts)  
        , _sod = sum_dist( pts, _pt ) 
        )
     //echo( log_(["pts", pts] ))
     //echo( log_(["sum_pts",sum(pts)] ))
     //echo( log_(["sum_pts/n",sum(pts)/len(pts)] ))
     //echo( log_(["_pt",_pt] ))
     //echo( log_(["_sod",_sod] ))
     //echo( log_e() )
        gmedian( pts=pts, prec=prec, nMaxSteps=nMaxSteps, d=d, _pt=sum(pts)/len(pts)
               , _sod=sum_dist( pts, sum(pts)/len(pts) ), _i=0)   
   
  :_i>nMaxSteps || d< prec? 
     //echo( log_b( _s("Existing gmedian, _i={_}, prec={_}, d={_}", [_i,prec,d] )))
     //echo( log_(["rtn(=_pt)", _pt, "_sod", _sod]))
     //echo( log_e())
     _pt
     
  : let( x=_pt[0] //.x
       , y=_pt[1] //.y
       , 4pts = [[x+d,y+d,0],[x+d,y-d,0], [x-d,y+d,0], [x-d,y-d,0]]
       , sods= [for(pt=4pts) 
                  //echo(log_b( "Looping 4pts",2) )
                  //echo(log_(["4pts",4pts],2))
                  //echo(log_(["pt",pt, "sod", sum_dist(pts,pt)],2))
                  //echo(log_e( "sod= ",2) )
                  sum_dist(pts,pt) ]
       , check= sortArrs( [ for(i=[0:3]) //range(4)) 
                            if(sods[i]<_sod) [4pts[i], sods[i]]
                          ], by=1 )[0] 
       , next_pt = check?check[0]:_pt
       , next_sod= check?check[1]:_sod
       , next_d = check?d:d/2         
       )
       //echo(log_b(["gmedian(): _i",_i]))
       //echo( log_(["pts",pts] ))
       //echo( log_(["d",d, "x",x, "y",y] ))
       //echo( log_(["_sod",_sod] ))
       //echo( log_(["_pt",_pt] ))
       //echo( log_(["4pts", 4pts] ))
       //echo( log_(["sods", sods] ) )
       //echo( log_(["sum0", sum_dist(pts,4pts[0]) ] ) )
       //echo( log_(["check", check] ))
       //echo( log_(["new_pt", next_pt] ))
       //echo( log_(["new_sod", next_sod] ))
       //echo( log_(["new_d", next_d] ))
       //echo(log_e())
       gmedian( pts=pts, prec=prec, nMaxSteps=nMaxSteps
              , d=next_d, _pt=next_pt, _sod=next_sod, _i=_i+1)   
);

//color("test")cube();
//ISLOG=1;
//echo( to3d([2,3]) );
//gmedian_check();
module gmedian_check()
{
   //pts = [-X,X*3,X,X*2];// randPts(3, x=0); //pqr();
   pts = [X*10,O,Y*5, X*10+Y*5];


    xx=5; 
    yy=1;
    //pts = [X*10, X*2, Y*1+X*2, Y*1, Y*5, X*10+Y*5]; // 2.75
    pts = 
          //let(xx=4,yy=.5) // 0.188 
          //let(xx=4,yy=1) // 0.158 
          let(xx=.5,yy=4) // 0.501 
          //let(xx=1,yy=4) // 0.507 
          //let(xx=1.5,yy=4) // 0.499 
          //let(xx=1,yy=.5) // .477
          //let(xx=.11,yy=4) // .488
          //let(xx=2,yy=1) // .394
          //let(xx=4,yy=2) // .104
          //let(xx=6,yy=3) // .192
          //let(xx=8,yy=4) // .57
          //let(xx=0,yy=0) // .57
          [X*10, X*xx, Y*yy+X*xx, Y*yy, Y*5, X*10+Y*5]; 
          //[X*10, O, Y*5, X*10+Y*5];  // 1 

    C= sum(pts)/len(pts);
    N = N([pts[0],C,pts[1]]);
    MarkPts(pts, Label=false, Line=["closed",1,"r",0.02], Ball=0);
    MarkPt(C,"C");
    //MarkPt(N,"N");
    
    M= gmedian(pts);
    echo(C=C, M=M);
    MarkPt(M,"M");

    //for(p=pts) Dim([p,C,N] );
    echo(roundness = roundness(pts) );

    //pts = randPts(5);
    //echo(pts=pts);
    //echo(pts_ = roundPts(pts[0],3));
    //echo(pts0= roundPts(pts,0) );
    //echo(pts1= roundPts(pts,1) );
    //echo(pts2= roundPts(pts,2) );
    //echo(pts2= roundPts(pts,8) );
}

//========================================================
function growPts( pts,steps=[] ,_rtn=[], _i=0, _debug=[] )=
(
  let( keys= keys(steps)
     , vals= vals(steps) 
     , L = len(pts)
     )
  _i==0?  
     ( 
     
//      let( steps = haskey(steps, "repeat")?
//                    (
//                      slice( hashkvs(steps), 
//                    
//                    )  
     
     
       len(keys)==0
       ? (ispt(pts)?[pts]:pts)
       : let( newpts = ispt(pts)?[pts]:pts)
         growPts( newpts, steps, newpts, _i+1 ) 
     )
  :_i>len(keys)? _rtn //arrprn(hashkvs(_debug)) 
  : let( key = get(split(keys[_i-1],":"), -1) 
             /* New 2020.7.16: allow any key to have a prefix for 
                // comment, seperated by ":". For example, 
                // "x" ==> "1:x"
                // "Turn left:x" 
                // Very useful for identifying pts. 
                // See usage in  Frontbar() in EndJointForLandScape.scad
                pts = growPts( Y*FRONTBAR_L
                      ,[ "1:z", LEN4, "2:y", -(FRONTBAR_L-LEN4), "3:z", -LARGE_SPACER
                       , "4:y", -LEN4, "5:z", -TB_H, "6:y", LEN4, "7:z", -LARGE_SPACER   

                       , "Next half_8:t", [-LEN4, FRONTBAR_L-LEN4,0]
                       , "9:z", LEN4, "10:y", -(FRONTBAR_L-LEN4), "11:z", -SMALL_SPACER
                       , "12:y", -LEN4, "13:z", -TF_H, "14:y", LEN4, "15:z", -SMALL_SPACER   
                       ]
               ); */    
                  
       , val = vals[_i-1] // Note: can't use hash(steps, key) 'cos there might be
                          // duplicate keys : ["x",3, "y",2 ,"x",-1]
       , p = last(_rtn)                   
       , newPts= (key=="pt")? val
              :key=="x"? addx(p, val) 
              :key=="y"? addy(p, val) 
              :key=="z"? addz(p, val) 
              :key=="t"? (p+ val)
              :(key=="N" && L>2)? p+ uv(pts[1],N(pts))*val // L_to_QP 
              :(key=="B" && L>2)? p+ uv(pts[1],B(pts))*val // L_to_QP on PQR plane
              :(key=="R" && L>2)? p+ uv(pts[1],pts[2])*val 
              :(key=="P" && L>2)? p+ uv(pts[1],pts[0])*val
              :(key=="L" && L>2)? p+ uv(pts[1],N([pts[2],pts[1],N(pts)]))*val // L_to_RQ on PQR plane
              :(key=="a" && L>2)? p+ uv(pts[1]
                                       ,anglePt( [2*pts[1]-pts[2],pts[1],pts[0]]
                                               , a=h(val,"a"), a2=h(val,"a2")
                                               )
                                       )*h(val,"len",1)  
              
              :key=="repeat" ? (
                let( partial_steps = slice(steps, val[0]*2, (val[1]+1)*2) 
                   )
                //echo( len_rtn = len( _rtn), len_steps = len(steps), len_partial=len(partial_steps) )
                //echo(partial_steps= partial_steps)   
                slice(growPts( last(_rtn), partial_steps ),1)
                ) 
                                             
              :key=="arc"? 
                 // An arc key accompies a val containing arcPts's arg name:
                 // ["n",6, "rad",undef, "a",undef, "a2",undef, "ratio",1
                 // and optional pqr as instruction for direction:
                 // , "pqr", undef ]
                 // If pqr not given, use [ [1,0,0], ORIGIN, [0,1,0]]
                 //   when len(pts)<3, or get(pts,[0,1,2]) when len(pts)>=3
                  
                 let( rad = hash( val, "rad"
                                  , len(_rtn)<3? 2
                                    : undef
                                  ) 
                     , a2  = hash( val, "a2")             
                     , pqr =  hash( val, "pqr"
                                  //, len(_rtn)<3?  [ p,addx(p,2),addy(p,2)]
                                  , len(_rtn)<3?  [ p,addx(p,rad),addy(p,rad)]
                                    : let( B= B( get(_rtn, [-1,-2,-3]) ) 
                                         //, B=_B
                                         //, B = a2? rotPt(_B, slice(_rtn, -2), a2)
                                         //      :_B
                                         , rad = und(rad, dist(B, last(_rtn)))
                                         )
                                       [ last(_rtn) //get(_rtn, -1)
                                       , onlinePt( [last(_rtn), B+ last(_rtn)-get(_rtn,-2)]
                                                 , rad )
                                       , onlinePt( get(_rtn,[-1,-2]), -1) 
                                       ]
                                  )
                     , arcPts= arcPts( pqr= a2? rotPts(pqr, get(_rtn, [-2,-1]), a2)
                                                // [ pqr[0]
                                                //, anglePt([pqr[1],pqr[0],pqr[2]],a2)
                                                //, pqr[1]
                                               // ]
                                              : pqr
                                    , n = hash(val,"n")+1
                                    , rad= rad //hash(val, "rad")
                                    , a = hash(val, "a",90)
                                    //, a2=a2
                                    //, a2 = hash(val, "a2")
                                    , ratio = hash(val, "ratio")
                                    )   
                     )
                  //["val", val, "n", hash(val,"n") ]
                  //echo(val=val, a2=a2)
                  slice(arcPts,1) //["pqr", pqr, "arcPts", arcPts]
              :p
                    
       , _rtn = concat( _rtn, ispt(newPts)?[newPts]:newPts) //[for(p=newpts) if(ispt(p))p else each p ] )
       ,_debug = concat( _debug, [ "_i", _i, "key",key, "val",val, "newPts", newPts, "_rtn",_rtn ] )
       ) 
    //_debug
    //echo(_i=_i, pts=pts, val=val)
    growPts( pts=pts, steps=steps, _rtn=_rtn,  _i=_i+1, _debug=_debug)
);


  growPts=["growPts", "pt,steps=[]", "pts", "pts"
  ,"
     Given a single pt and multiple steps, grow new points for 
  ;; each single step, return all pts.
  ;;   
  ;; steps example: [ \"x\",3, \"y\",-2, \"t\",[2,3,0] ]
  ;; Each step generate a pt     
  ;;
  ;; newpts = growPts( pt , [\"x\",2, \"y\",3, \"t\",[-1,-1,1],\"z\",2] )
  ;; 
  ;; Available steps options:
  ;; 
  ;; name value   
  ;;  x  num -- along x axis
  ;;  y  num -- along y axis
  ;;  z  num -- along z axis  
  ;;  t  pt  -- translate by t   
  ;;  N  num -- along the Normal to PQR (L to PQ,QR)
  ;;  B  num -- along the Binormal to PQR (L to PQ on pqr plane)
  ;;  L  num -- L to RQ on pqr plane
 
  ;;  R  num -- along the QR dir 
  ;;  P  num -- along the QP dir
  ;;  arc hash: [ \"pqr\", 3-pts
  ;;              , \"n\" , num
  ;;              , \"rad\", num
  ;;              , \"a\"  , num
  ;;              , \"a2\" , num
  ;;              ]  // see the arcPts()
  ;;
  ;; New 2020.7.16: allow any key to have a prefix as 
  ;; notation, seperated by ':'. For example, 
  ;; 'x' ==> '1:x'
  ;; 'Turn left:x' 
  ;;
  ;; These notations will be shown on display but won't be taken into
  ;;   account in actions. 
  ;; Very useful for identifying pts. 
  ;; See usage in  Frontbar() in EndJointForLandScape.scad
  ;;
  ;; New 2020.7.17: 2 new keys: pt, repeat
  ;;   use of repeat will fail if the previous steps contains absolute pt
  ;;   like the val of the pt key, and the 'pqr' value of 'arc' key. 
  ;;   See the demo_growPts_repeat() in demo_growPts().
  
  "];
     
    function growPts_test( mode=MODE, opt=[] )=
    (
        doctest( growPts,
        [
        ], mode=mode, opt=opt
        )
    );

//========================================================
{ // getSideFaces
function getSideFaces(topIndices
                    , botIndices
                    , clockwise=true)=
let( ts= topIndices
   , bs= botIndices
   , tlast = last(ts)
   , blast = last(bs)
   )
(
  clockwise
  ? [for (i=range(ts))
      i==0
      ? [ ts[0], tlast, blast, bs[0] ]
      : [ ts[i], ts[i-1], bs[i-1], bs[i] ]
     ]
  : [for (i=range(ts))
      i<len(ts)-1
      ? [ ts[i], ts[i+1], bs[i+1], bs[i] ]
      : [ tlast, ts[0], bs[0], blast ]
      ]
);
    getSideFaces=["getSideFaces","topIndices, botIndices, isclockwise=true","array", "Faces"
    , "Given topIndices and botIndices (both are array
    ;; of indices), return an array of indices for the 
    ;; side faces arguments for the polyhedron() function
    ;; to create a column object (note that the top
    ;; and bottom faces are not included).
    ;; isclockwise: true if looking down from top to 
    ;; bottom, the order of indices is clockwise. Default
    ;; is true.
    ;; 
    ;;  clockwise (default)
    ;;       _2-_
    ;;    _-' |  '-_
    ;;   1_   |    '-3
    ;;   | '-_|   _-'| 
    ;;   |    '-0'   |
    ;;   |   _6_|    |
    ;;   |_-'   |'-_ | 
    ;;   5 _    |    7
    ;;     '-_  | _-'
    ;;        '-4' 
    ;;
    ;;  => [ [0,3,7,4], [1,0,4,5], [2,1,5,6],[3,2,6,7]]
    ;;
    ;;  clockwise = false
    ;;       _2-_
    ;;    _-' |  '-_
    ;;   3_   |    '-1
    ;;   | '-_|   _-'| 
    ;;   |    '-0'   |
    ;;   |   _6_|    |
    ;;   |_-'   |'-_ | 
    ;;   7 _    |   '5
    ;;     '-_  | _-'
    ;;        '-4' 
    ;;
    ;;  => [ [3,0,4,7], [2,3,7,6], [1,2,5,6],[0,1,5,4] ]
    ;; [[0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]] 
    "];
    function getSideFaces_test( mode=MODE, opt=[] )=
    (
        let( d1=[ [0,1,2,3],[4,5,6,7]]
           , d2=[ [6,7,8],[9,10,11]] 
           )
        
        doctest( getSideFaces,
        [ 
          [getSideFaces(d1[0],d1[1]),
            [[0,3,7,4], [1,0,4,5], [2,1,5,6], [3,2,6,7]]
            ,"d1[0], d1[1]", ["nl",true]
          ]
         ,[getSideFaces(d1[0],d1[1],false),
            [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
            , "d1[0], d1[1], clockwise=false", ["nl",true]
          ]
         ,[getSideFaces(d2[0],d2[1]),
            [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
            , "d2[0], d2[1]", ["nl",true]
          ]
         ,[ getSideFaces(d2[0],d2[1],false),
            [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
           , "d2[0], d2[1], clockwise=false", ["nl",true]
          ]
        ]
        , mode=mode, opt=opt
        , scope=["d1", d1, "d2",d2]
        )
    );

} //getSideFaces

{ // getTransformData
function getTransformData( shape // cube|cylinder|text // When text, pts and size doesn't matter 
                         , pts
                         , size  // for cube 
                         , h, r,r1,r2,center,d,d1,d2,$fn, $fa, $fs // for cylinder | tube
                         , th // for tube wall thickness
                         , center, actions, site, opt=[] )=
(           
  let( 
     _pts = arg( pts, opt, "pts", [])
   , _shape = arg(shape, opt, "shape", "cube")
   //------------------------------------------------                      
   , _size = sizeArg(size, opt, "size", _shape=="cube"?[1,1,1]:undef)
   //------------------------------------------------                      
   , _h = arg(h, opt,"h",1)
   , _r = arg(r, opt, "r", 1)
   , _d = arg(d, opt, "d", _r*2)
   
   , _r1= arg(r1, opt, "r1", _d/2)
   , _r2= arg(r2, opt, "r2", _d/2)
   
   , _d1= arg(d1, opt, "d1", _r1*2)
   , _d2= arg(d2, opt, "d2", _r2*2)
   
   , _th= arg(th, opt, "th", min(_d1,_d2)/8) // tube wall thickness
   , $fn = arg($fn, opt, "$fn",12)
   , $fa = arg($fa, opt, "$fn", 0.01)
   , $fs = arg($fs, opt, "$fn",0.01)
     
   //---------------------------------------------------                
   , _center = centerArg( center, opt
                , df=_shape=="cylinder"||_shape=="tube"?[1,1,0]:[0,0,0] )
   , centerMove= _shape=="cube"?
                      ["t", [ _center[0]?-_size[0]/2:0
                            , _center[1]?-_size[1]/2:0
                            , _center[2]?-_size[2]/2:0 
                            ]
                      ]
                 : _shape=="cylinder" || _shape=="tube"?
                     ["t",  [ _center[0]?0:(_r1+_r2)/2
                            , _center[1]?0:(_r1+_r2)/2
                            , _center[2]?-_h/2:0
                            ] 
                     ]
                 :[] 
                            
   , _actions0 = argx(actions,opt,"actions",[])           
   , _actions = _center? concat( centerMove, _actions0 )
                       : _actions0
                        
   //---------------------------------------------------                

   , pts0 = _pts? _pts
	     : _shape=="cube"?
  	     	[ [_size.x,0,0], ORIGIN
                , [0,_size.y,0],[_size.x, _size.y,0] 
                , [_size.x,0,_size.z], [0,0,_size.z]
                , [0,_size.y,_size.z], _size  
                ]
             : _shape=="cylinder"?
               
               // 18.8.6: Rewrite circlePts to use the new version
               
               let(pts_down = is0(_d1)? [[0,0,0]]
                      :circlePts( pqr= [ B([ [0,0,-_h*2], O,X])
                                       , O
                                       , N([ [0,0,-_h*2], O,X])
                                       ]
                                , rad=_d1/2, n= $fn )
                                
                  , pts_top = is0(_d2)? [[0,0,_h]]
                      :circlePts( pqr=[ B([ [0,0,-_h*2], [0,0,_h],[1,0,_h]])
                                       , [0,0,_h]
                                       , N([ [0,0,-_h*2], [0,0,_h],[1,0,_h]])
                                       ]
                                , rad=_d2/2, n= $fn )
                  ) 
               //let(pts_top = is0(_d1)? [[0,0,0]]
                      //:circlePts( pqr=[ [0,0,-_h*2], [0,0,0],[1,0,0]], rad=_d1/2, n= $fn )
                  //, pts_down = is0(_d2)? [[0,0,_h]]
                      //:circlePts( pqr=[ [0,0,-_h*2], [0,0,_h],[1,0,_h]], rad=_d2/2, n= $fn )
                  //) 
               concat(pts_down,pts_top)
               
             : _shape=="tube"?
                let( __th = _d1==_d2? _th
                                    : let(a= (_d1/2-_d2/2)*_th/_h)
                                      sqrt(pow(a,2)+pow(_th,2)) 
                   , pts_out_down= circlePts( pqr= [ B([ [0,0,-_h*2], O,X])
                                                   , O
                                                   , N([ [0,0,-_h*2], O,X])
                                                   ]
                                            , rad=_d1/2, n= $fn )
                   , pts_out_top = circlePts( pqr=[ B([ [0,0,-_h*2], [0,0,_h],[1,0,_h]])
                                                   , [0,0,_h]
                                                   , N([ [0,0,-_h*2], [0,0,_h],[1,0,_h]])
                                                   ]
                                            , rad=_d2/2, n= $fn )
                   , pts_in_down = pts_out_down*[ (1-__th/_r1)*X, (1-__th/_r1)*Y,Z]
                   , pts_in_top = pts_out_top*[ (1-__th/_r2)*X, (1-__th/_r2)*Y,Z] 
                   )
                //echo(_d1=_d1, _d2=_d2, _th=_th, __th=__th)   
                concat( pts_out_down, pts_out_top, pts_in_down, pts_in_top)   
             : []
               
    , pts2= pts0 && _actions? transPts(pts0, actions=_actions): pts0 
    
    , _site = arg(site, opt, "site",[])
    , from = hash(opt, "from", ORIGIN_SITE)
    , pts = pts0 && _site? movePts( pts2, from=from
                               , to=_site ): pts2
   
  ) // let
  // Return a hash including not only the pts but those data needed 
  // to get to these pts, for the purpose that if this function is called
  // to get the "pts" of a solid cube, these data is needed to re-position
  // that solid cube into new position. 
  //echo(in_getTransformData_actions = actions, _actions0=_actions0, _center=_center,centerMove=centerMove, _actions=_actions)
  //echo("in_getTransformData: ", r=r, r1=r1, r2=r2, _r=_r, _r1=_r1, _r2=_r2)
  //echo("in_getTransformData: ", d=d, d1=d1, d2=d2, _d=_d, _d1=_d1, _d2=_d2)
  _shape=="cube"?
  [ "shape", shape
  , "_shape", _shape
  , "_pts", _pts
  , "pts0", pts0
  , "pts", pts
  , "size", _size
  , "center", _center
  , "centerMove", centerMove
  , "actions", _actions
  , "site", _site
  , "opt", opt
  ]
  :_shape=="cylinder"?
    [ 
    "shape", shape
    , "_shape", _shape
    , "_pts", _pts
    , "pts0", pts0
    , 
    "pts", pts
    , "h", _h
    , "d1", _d1
    , "d2", _d2
    , "center", _center
    , "actions", _actions
    , "site", _site
    , "$fn", $fn
    , "$fs", $fs
    , "$fa", $fa
    ]  
  :_shape=="tube"?
    [ 
    "shape", shape
    , "_shape", _shape
    , "_pts", _pts
    , "pts0", pts0
    , "pts", pts
    , "h", _h
    , "d1", _d1
    , "d2", _d2
    , "th", _th
    , "center", _center
    , "actions", _actions
    , "site", _site
    , "$fn", $fn
    , "$fs", $fs
    , "$fa", $fa
    ]  
  : _shape=="text"?
  [ "center", _center
  , "actions", _actions
  , "site", site
  , "_site", _site
  //, "opt", opt
  ]
  : _red(_s("Error in getTransformData(): the given shape(={_}) is not recognized"
        , _shape))
);

}





//========================================================
{ // getTubeSideFaces
function getTubeSideFaces(
         indexSlices, clockwise=true)=  
  let( ii = indexSlices )
( 
    //mergeA( 
      [ for (i=range(len(ii)-1))
        getSideFaces( topIndices= ii[i]
                    , botIndices= ii[i+1]   
                    , clockwise=clockwise
                    )
      ]
    //)
);
      
    getTubeSideFaces=["getTubeSideFaces","indexSlices, clockwise=true","array", "faces"
    , "Given an array of indexSlices (an indexSlice
    ;; is a list of indices representing the order 
    ;; of points for a circle), return an array of 
    ;; faces for running polyhedon(). Typically
    ;; usage is in a column or tube of multiple
    ;; segments. For example, CurvedCone.
    ;;
    ;; See getSideFaces that is for tube of only
    ;; one segment. 
    "];       
    function getTubeSideFaces_test( mode=MODE, opt=[] )=
    ( 
        let( d1=[ [6,7,8,9],[10,11,12,13],[14,15,16,17] ] )
        
        doctest(getTubeSideFaces,
        [ 
          "var d1"
        , [ getTubeSideFaces(d1),
            [[[ 6, 9,13,10], [ 7, 6,10,11]
             ,[ 8, 7,11,12], [ 9, 8,12,13]]
            ,[[10,13,17,14], [11,10,14,15]
             ,[12,11,15,16], [13,12,16,17]]
            ], "d1", ["ml",2]
          ]
    //     ,["d1[0], d1[1], clockwise=false"
    //        , getSideFaces(d1[0],d1[1],false),
    //        [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
    //      ]
    //     ,["d2[0], d2[1]", getSideFaces(d2[0],d2[1]),
    //        [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
    //      ]
    //     ,["d2[0], d2[1], clockwise=false"
    //        , getSideFaces(d2[0],d2[1],false),
    //        [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
    //      ]
        ]
        ,mode=mode, opt=opt, scope=["d1", d1])
    );
} // getTubeSideFaces


//========================================================
{ // getAnglePtData
function getAnglePtData(pqr,X)=
(
   let( P=pqr[0], Q=pqr[1], R=pqr[2]
      , len = dist(Q,X)
      )
   ispl(pqr)? // means pqr is not on the same ln
      let( J = projPt( X, pqr )
         , N = N(pqr)
         , a = angle([P,Q,J],Rf=N)
         , a2= angle([J,Q,X],Rf=onlinePt([Q,P],len=sign(a)))
         ) [a,a2,len,J]
    :isequal(P,Q) || isequal(Q,R) || isequal(P,R) ? undef
    :// iscoline:
        ( isinline( R,[P,Q])? [0,0,len,R]
        : [180,0,len, R]
        )         
);
    getAnglePtData=["getAnglePtData","pqr,X","arr","Geometry"
    ,"Return [a,a2,len,J] where a,a2,dist are variables needed
    ;; to get to X from pqr using anglePt(pqr, a,a2,len=len).
    ;; That is, this is a reversed action of anglePt. Hint: 
    ;; polar coordinates. "
    ,"anglePt" 
    ];
} // getAnglePtData

//========================================================
function groupByQuadrant(pts, Rf=O, _rtn=[], _i=0)=
(
    /*                      
                    
   */
  let( lv= 1)
  
  _i==0 ?
     let( quad= quadrant(pts[0], Rf=Rf, isinclude=1)  //==> 0~3 //1~4
        //, _rtn= [for(i=[1:4]) i==quad?[pts[0]]:[]]
        , _rtn= [for(i=[0:3]) i==quad?[pts[0]]:[]]
        )
        //echo( log_b("groupByQuadrant()", lv))
        //echo( log_(["pts",pts],lv )) 
        //echo( log_(["site",site],lv )) 
        //
        //echo( log_b( "_i==0", lv+1) )
        //echo( log_( ["octant", octant], lv+1) )
        //echo( log_( ["octi", octi], lv+1) )
        //echo( log_( ["_rtn", _rtn], lv+1) )
        //echo( log_e( "", lv+1) )
        groupByQuadrant(pts=pts, Rf=Rf
                     , _rtn=_rtn , _i=_i+1)
                     
        
  :_i<len(pts)? 
     let( quad= quadrant(pts[_i],Rf=Rf, isinclude=1)  //==> 1~4
        , new_rtn = replace(_rtn, quad, concat( _rtn[quad], [pts[_i]] ))
        )
        //echo( log_b( ["_i",_i], lv+1) )
        //echo( log_( ["pts[_i]", pts[_i]], lv+1) )
        //echo( log_( ["octant(pts[_i])", octant(pts[_i]) ], lv+1) )
        //echo( log_( ["quad", quad], lv+1) )
        //echo( log_( ["current @ octi (_rtn[octi])", _rtn[octi]], lv+1) )
        //echo( log_( ["_rtn", _rtn], lv+1) )
        //echo( log_( ["new_rtn", new_rtn], lv+1) )
        //echo( log_e( "", lv+1) )        
        groupByQuadrant(pts=pts, Rf=Rf
                     , _rtn= new_rtn , _i=_i+1)
                     
  : 
  
  //echo( log_(["len_rtn",len(_rtn)],lv))
  //echo( log_(["Exiting, _rtn",_rtn],lv))
  //echo( log_e("groupByQuadrant",lv))
  _rtn
);  

//========================================================
function groupByOctant(pts, Rf, _rtn, _i=-1)=
(
    /*                      
                             |   .
                             |  /|
                  1          | / |       0
                             |/  | /    
                .------------+------------.
                |   .-------/|---+--------|---.
                |  /       / |  /|        |  /
                | / 2     /  | / |    3   | /
                |/       .   |/  |        |/
           -----+--------|---+------------+-----
               /|        |  /|   '       /|
         5    / |        | / |  /       / |   4
             /  |        |/  | /       /  |
            '------------+------------'   |
                '-------/|---+------------'
                       / |  /|
               6         | / |      7
                         |/
                         '                       
   */
  let( lv= 1)
  
  _i==-1 ?
     let( //_octvals = [ 111, 11, 1, 101, 110, 10, 0, 100 ] 
        //, 
         octant= octant(pts[0],Rf)  //==> 0~7
        //, octi= idx( _octvals, octant )  //==> 0~7
        , Rf = Rf==undef?O:Rf
        , _rtn= [for(i=[0:7]) i==octant?[pts[0]]:[]]
        //, _rtn= [for(i=[0:7]) i==octant?[pts[0]]:[]]
        )
        //echo( log_b("groupByOctant()", lv))
        //echo( log_(["pts",pts],lv )) 
        //echo( log_(["Rf",Rf],lv )) 
        //echo( log_( ["octant", octant], lv+1) )
        //echo( log_( ["_rtn", _rtn], lv+1) )
        //echo( log_e( "", lv+1) )
        groupByOctant(pts=pts, Rf=Rf
                     , _rtn=_rtn //[[],[],[],[],[],[],[],[]] 
                     , _i=_i+1)
        
  :_i<len(pts)? 
     let( //octi= idx( _octvals, octant(pts[_i], Rf) )  //==> 0~7
         octant= octant(pts[_i],Rf)
        , new_rtn = octant==undef? _rtn
                    : replace(_rtn, octant, concat( _rtn[octant], [pts[_i]] ))
        )
        //echo( log_b( ["_i",_i], lv+1) )
        //echo( log_( ["pts[_i]", pts[_i]], lv+1) )
        //echo( log_( ["octant", octant], lv+1) )
        //echo( log_( ["_rtn", _rtn], lv+1) )
        //echo( log_e( "", lv+1) )        
        groupByOctant(pts=pts, Rf=Rf
                     , _rtn= new_rtn , _i=_i+1)
                     
  : 
  
  //echo( log_(["len_rtn",len(_rtn)],lv))
  //echo( log_(["Exiting, _rtn",_rtn],lv))
  //echo( log_e("groupByOctant",lv))
  _rtn
);  
  
////========================================================
//getTurnDataAtQ= ["getTurnDataAtQ","pqr,[Rf]", "array","Geometry"
//,"Given pqr and optional Rf, return [a,a2,dist,J] for pt at Q.
//;; where Rfn is a pt that`s not co-planar with pqr (hint: treat
//;; Rfn as on the normal direction of pqr), and must be there to 
//;; determine the sign of a and a2. 
//;;
//;; The pqr plane is used as the refpl (reference plane), so a2
//;; is always 0. But if Rf is given, [ P,Q,Rf ] will be the refpl, 
//;; in which case a2!=0.     
//;;
//;; Return [a,a2,dist,J] that could be used as arguments by 
//;; anglePt(pqr,a,a2,len) to make the pt pqr[2], meaning that 
//;; with Rfn, Rf and [a,a2,dist,J], it`s possible to make and an exact 
//;; turn at any pqr.
//;;
//;; J is the projection of R onto refpl.
//"
//];
//function getTurnDataAtQ( pqr, Rf )=      
//(
//    let( d = dist(p12(pqr))
//       , P=pqr[0], Q=pqr[1], R=pqr[2]
//       , Rf= und(Rf, pqr[2])
//       , refpl= [P,Q,Rf]
//       )   
//    ispl(pqr) ? // means pqr is not on the same ln
//        let( J = projPt( R, refpl )
//           , N = N(refpl)
//           , a = angle( [ P,Q,J ],Rf=R) // R here is critical
//           , a2= angle( [ J,Q,R ],Rf=P) // P here is critical 
//           ) [a,a2,d,J]
//    :isequal(P,Q) || isequal(Q,R) || isequal(P,R) ? undef
//    :// iscoline:
//        ( isinline( R,[P,Q])? [0,0,d]
//        : [180,0,d, R]
//        )
//);


//. i,l



function icosahedronPF(r, site, actions, debug=0, opt=[])= 
(  // 2018.5.26 
   // https://en.wikipedia.org/wiki/Regular_icosahedron
   
   /*
     dPR = dQR = dPQ   and OR = OP = OQ
        
                  R _     r     
            el  .'|\ ''--._     
             .'   | \      ''--. O (ORIGIN)
       Q  .'      |  \ el  _.-'/ 
        ':._      |   \.-'`   /  
            '-._..J'`  \     / r
              M '-._    \   /
           el       '-._ \ / 
                        '-'P                  
   */
   
   let( r = arg(r,opt,"r",2)  // dist from center to one corner
      //, site = arg(site, opt, "site", []) 
      //, actions = argx(actions,opt,"actions",[])  
      , el = r/ sin(72)                    // edge length 
      , dMR = sqrt( el*el- (el/2)*(el/2) )
      , dMO = sqrt( r*r- (el/2)*(el/2) )
      , aPOQ = anglebylen( r,el,r )
      , aROM = anglebylen( r, dMR, dMO )  
      , P0 = [0,0,r]
      , pts0 = [ for(i=[0:4]) 
                 anglePt( [ [1,0,0], ORIGIN, [0,1,0] ]
                        , a= 360/5*i, a2= 90-aPOQ, len=r )
               ]
      , pts1 = [ for(i=[0:4]) 
                 anglePt( [ [1,0,0], ORIGIN, [0,1,0] ]
                        , a= 360/5*i-180/5, a2= 90-2*aROM, len=r )
               ]     
      , pts = concat( [P0], pts0, pts1, [addz(P0, -2*r)] )
      , faces= [ [0,2,1],[0,3,2],[0,4,3],[0,5,4],[0,1,5]
               , [1,2,7],[2,3,8],[3,4,9],[4,5,10],[5,6,1]
               , [1,7,6],[2,8,7],[3,9,8],[4,10,9],[5,6,10]
               , [6,7,11],[7,8,11],[8,9,11],[9,10,11],[10,6,11]
               ]
      ) 
    debug?
       echo(r=r)
       echo(el=el)
       echo( aPOQ= aPOQ )
       echo( aROM= aROM )
       echo( a=a)
       echo( P0=P0)
       [pts,faces]    
    : [pts,faces]
);


{ // ibend
// Return the first i that the angleAt(i) is not 180 deg
function ibend(pts, _i=1)=
(
   let( a = angle( get(pts,[_i-1,_i,_i+1]) )
      )
   !is0(a-180) ? _i
   : _i== len(pts)-2? undef
   : ibend(pts, _i+1)
);
//pts= [[1,0,0],[2,0,0],[3,1,1]];
//echo( ibend(pts)); // 1
//
//pts1= [[1,0,0],[2,0,0],[3,0,0],[4,0,0],[10,1,1]];
//echo( ibend(pts1)); // 3
//
//pts2= [[1,0,0],[2,0,0],[3,0,0],[4,0,0],[5,0,0]];
//echo( ibend(pts2)); // undef           
} // ibend 
      
//========================================================
{ // incenterPt
function incenterPt(pqr)=
(
	intsec( [ pqr[0], angleBisectPt( get(pqr, [1,0,2]) ) ]
	      , [ pqr[1], angleBisectPt( pqr ) ]
		  )
//	lineCrossPt( pqr[0], angleBisectPt( rearr(pqr, [1,0,2]) )
//			  , pqr[1], angleBisectPt( pqr )
//			  )
);

    incenterPt=["incenterPt", "pqr", "point", "Angle, Point"
    ," Return the incenter point (all 3 angle bisector lines meet)."
    ];

    function incenterPt_test(mode=MODE, opt=[] )=
    (
        doctest(incenterPt, mode=mode, opt=opt )
    );
} //incenterPt

//  ln-ln: http://math.stackexchange.com/questions/270767/find-intersection-of-two-3d-lines

//========================================================
{ // intsec
    
function intsec( a,b, debug=0 )= // intersection, return: pt|ln|undef
(    
   let( 
         c= b==undef?a[0]:a
       , d= b==undef?a[1]:b
       , t= str(gtype(c), gtype(d)) //typeplp(c),typeplp(d))  
            /* 
              t==lnln
                      K    J        _-`
                    --+----+-------M`-----R----S
                      |    |   _-` 
                      |    |_-` m 
                      |  _-P   
                      Q-`
                 m/PQ =  PJ/(QK-PJ)
                 m  = PQ*PJ/(QK-PJ)
                 m=  dPQ*dPrs/( dQrs-dPrs)
                 M = onlinePt( [P,Q], len=-m) 
                 
                                   Q
                      J         _-`|
                    --+-------M`---K---R----S
                      |   _-` 
                      |_-` m 
                    _-P
                     
                 m = PQ*PJ/(QK+PJ)    
            */
       , dPrs= t=="lnln"? dist( c[0], d ):undef
       , dQrs= t=="lnln"? dist( c[1], d ):undef
       , lnPtSign = t=="lnln"?
                   (isSameSide( c
                   , [d[0],d[1], N( concat(d,[c[0]]))] )?-1:1
                   ) : undef
       , m= t=="lnln"? dist(c)*dPrs/( dQrs+ lnPtSign*dPrs) :undef
       , rtn=  isparal(c,d)? undef
              :t=="lnln"? (!is0(dist(c,d))?undef: onlinePt( c, len= lnPtSign*m))
              :t=="lnpl"? intsec_lnpl( c, d )
              :t=="plln"? intsec_lnpl( d, c )
              :t=="plpl"? intsec_plpl( c, d ) // 2018.6.6
              : undef  
   )
   debug?
   echo("=== Debug intsec ===")
   echo(a=a)
   echo(b=b)
   echo(c=c)
   echo(t=t)
   echo(dPrs = dPrs)
   echo(dQrs = dQrs)
   echo(lnPtSign = lnPtSign)
   echo(m=m)
   echo(rtn = rtn)
   rtn
   :rtn 
   
);
} // intsec

//========================================================
{ // intsec_lnpl
function intsec_lnpl(pq, tuv)=
(
  let( L= dist(pq)
     , dp = distPtPl(pq[0], tuv)
     , dq = distPtPl(pq[1], tuv)
     , sameside = sign(dp)==sign(dq)
     , ratio = sameside ? dp/(dp-dq)
 	               : abs(dp)/(abs(dp)+abs(dq))
     )
  isequal(dp,dq)? undef
                : onlinePt( pq, ratio = ratio)
);
} // intsec_lnpl

//========================================================
function intsec_cirpl(pqr,pl)= // 2018.6.5
(
   // intsec between a circle defined by pqr, and a plane pl
   /*
            _.--+--._
       A .'`    |    `'. B
      --+`------J-------+-------- intsec btw circle and pl
       : ''-_   |        :
       +   r ''-C        +
       :                 :
        \               /
         `:_         _:`
            `'--+--'`
   */    
   
     
   let( C = circumCenterPt(pqr) // circle center
      , intline = intsec(pqr,pl)
      , dCJ = dist(C, intline)
      , r = dist(C,pqr[0])
      , dAJ = shortside( dCJ, r )
      , rtn = onlinePts( intline, lens=[ dAJ, -dAJ ] )       
      ) 
   rtn   
);

//========================================================
function intsec_plpl(pqr,stu, debug=0)= // 2018.6.6
(
   // intsec between 2 planes
   let( A = intsec([pqr[0],pqr[1]],stu, debug=0)
      , B = intsec([pqr[1],pqr[2]],stu)
      , rtn= A && B && ( !isequal(A,B)) ? [A,B]
             : let( C = intsec([pqr[2],pqr[0]],stu) )
               A && C? [A,C]
               :B && C? [B,C]
               : undef    
      )
      
     debug?
      echo("=== ndebugging intsec_plpl ===")
      echo(pqr=pqr)
      echo(stu=stu)
      echo(A=A)
      echo(B=B)
      echo(C=C)
      echo(rtn=rtn)
     rtn
     :rtn 
);

//function intsec_plpl_180605(pqr,stu, debug=0)= // 
//(
//   // intsec between 2 planes
//   // Ref: function by Nassim Khaled
//   // https://www.mathworks.com/matlabcentral/fileexchange/17618-plane-intersection
//   //
//   // We convert scadx friendly parameters to Khaled's versions 
//   //
//   // 2018.6.5 -- doesn't seem to work. Suspected reason:
//   //             the function N([N1,O,N2]) may not be a replacement of 
//   //             cross(N1,N2) in the article
//   
//   let( A1 = pqr[0]
//      , N1 = N(pqr)-pqr[0]
//      , A2 = stu[0]
//      , N2 = N(stu)-stu[0]
//      , N = N([N1,O,N2]) 
//      , hasAns = !is0( norm(N) ) // not parallel nor coincide
//      )
//   hasAns?
//     let( absN = [ for(p=N) abs(p)] 
//        , maxc = index( absN, max( absN ) )
//        , d1 = -N1*A1
//        , d2 = -N2*A2
//        , P= maxc==0?
//              [ 0,  (d2*N1.z - d1*N2.z)/N.x,  (d1*N2.y- d2*N1.y)/N.x  ]
//            : maxc==1?
//              [ (d1*N2.z- d2*N1.z)/N.y, 0, (d2*N1.x - d1*N2.x)/N.y ]
//            : [ (d2*N1.y- d1*N2.y)/N.z, (d1*N2.x- d2*N1.x)/ N.z, 0]
//        )
//     debug?
//      echo("debugging intsec_plpl")
//      echo(A1=A1)
//      echo(N1=N1)
//      echo(A2=A2)
//      echo(N2=N2)
//      echo(N=N)
//      echo(hasAns = hasAns)
//      echo( absN = absN )
//      echo( maxc = maxc )
//      echo( d1 = d1 )
//      echo( d2 = d2 )
//      echo( P = P, O=O )  
//     [ P, N([P,O,N]) ]      
//     :[ P, N([P,O,N]) ]      
//   : undef     
//     
//);


//========================================================

//function lineCrossPt( P,Q,M,N )=
//    let( pqmn = N? [P,Q,M,N]      // lineCrossPt( P,Q,M,N )
//                :Q? concat( P,Q ) // lineCrossPt( pq, mn )
//                :len(P)==2?[ P[0][0],P[0][1],P[1][0],P[1][1] ]
//                :P               // lineCrossPt( pqmn )
//       , P = pqmn[0]
//       , Q = pqmn[1]
//       , M = pqmn[2]
//       , N = pqmn[3]
//       , err= !isOnPlane( [P,Q,M],N) 
//       )
//(
//// Ref: Paul Burke tutorial 
////   http://paulbourke.net/geometry/pointlineplane/
//// and his code in c :
////   http://paulbourke.net/geometry/pointlineplane/lineline.c
//                   
////	P + (Q-P) * ( ((M-P)*(M-N))* ((M-N)*(P-Q)) - ((M-P)*(P-Q))*((M-N)*(M-N)))
//// 	 		 / ( ((P-Q)*(P-Q))* ((M-N)*(M-N)) - ((M-N)*(P-Q))*((M-N)*(P-Q)))
//               
//	err?_red(str( "Error in lineCrossPt: 2 lines are not on same plane. line1: "
//           , [P,Q], ", line2: ", [M,N] ))
//    :P + (Q-P) * ( dot(M-P,M-N)* dot(M-N,P-Q) - dot(M-P,P-Q)* dot(M-N,M-N) )
// 	 		 / ( dot(P-Q,P-Q)* dot(M-N,M-N) - dot(M-N,P-Q)* dot(M-N,P-Q) )
//);


function lineBridge(a,b)=
(  
   let( _b= b==undef?a[1]:b//und(b,a[1])
      , _a= b==undef?a[0]:a //und(b,a[0])
      , A0=_a[0], A1=_a[1]
      , B0=_b[0], B1=_b[1]
      , C = B1+(A1-A0)     // vector of A0->A1 extended from B1
                           // B0,B1,C is a plane containing b and parallel to a
      , plb =[B0,B1,C]
      , pla =[A0,A1,A1+(B1-B0)]
      , Xa = intsec( _a, projPt( _b, pla) )                   
      , Xb = intsec( _b, projPt( _a, plb) )                   
      )
   [Xa, Xb]   
);

   lineBridge=["lineBridge","[a,b]|a,b", "2-pts", "Point, Line",
     " Return the bridge (a line segment) between two lines a and b.
    ;;
    ;; lineBridge(a,b)
    ;; lineBridge([a,b])
    ;;
    ;; If a,b crosses, return []
    ;; Else, return [x,y]
    ;; where len between X,Y = dist(a,b)
    "];

   

//function lncoef(P,Q)=
//(
//    // good site: http://geomalgorithms.com/a02-_lines.html
//    
//    let( pq = Q==undef?P:[P,Q]
//       , P = pq[0], Q= pq[1]
//       )
//    [ Q.
//
//); 

//======================================
lineRotation=[ "lineRotation", "pq,p2", "?", "Angle"
, " The rotation needed for a line
;; on X-axis to align with line-p1-p2
"];

// The rotation needed for a line
// on X-axis to align with line-p1-p2
// 
function lineRotation(p1,p2)=
(
 [ 0,-sign(p2[2]-p1[2])*atan( abs((p2[2]-p1[2])
		                    / slopelen((p2[0]-p1[0]),(p2[1]-p1[1]))))
    , p2[0]==p1[0]?0
	  :sign(p2[1]-p1[1])*atan( abs((p2[1]-p1[1])/(p2[0]-p1[0])))]
);
 
 
{ 
// linePts -- return a series of somewhat predefined (semi random) pts
//            or a hash of the line's detailed properties 
// 2018.10.14:
// linePts: new/ under construct: pts representing the trace of a chain
//               (stripped down from randchain / chainBoneData)
// randchain = chainBoneData : a hash 
// simpleChainData: a stripped down of randchain but still a hash
// chaindata: : pts representing the wrapping pts alone the
//              trace of chain (i.e., linePts)
// Chain: draw chaindata 

function linePts( 
       site   //=undef // base pqr
     , nseg  
     , a     //=[-360,360]
     , a2    //=[-360,360]
     , len //= [0.5,3] // rand len btw two values. Could be just a num
     
     , as // an arr to give predefined as
     , a2s  // an arr to give predefined a2s
     , lens   // An arr to give predefined lens   
     , isReturnHash // if true, return pts. Else, a hash.
     , opt=[]
     , _rtn=[] // for noUturn
     ,_i= 1    // for noUturn
     ,_debug=""
    )=
(

  //let(lv=1)
  //echo(log_b(_s( _i==1?"linePts:init, _i (i_seg)={_}":"linePts(), _i (i_seg)={_}",_i)        
  //          , _i==1?lv:lv+1))
  
  _i==1?
    let( _nseg   = arg(nseg, opt, "nseg", 2)
       , _a      = arg(a, opt, "a", [150,210])
       , _a2     = arg(a2, opt, "a2", 0) 
       , _len    = arg(len, opt, "len", [1,2.5])
       
       , _lens   = arg(lens, opt, "lens")
       , _as     = arg(as, opt, "as")
       , _a2s    = arg(a2s, opt, "a2s") 
       
       , L   = _lens? _lens[0]
               : isarr(_len)? rand( _len[0], _len[1] ) 
               : _len // when _len= num
               
       , _site= arg(site, opt, "site", pqr() )
       , _isReturnHash= arg(isReturnHash, opt, "isReturnHash")
       , newrtn = [ _site[0], onlinePt(_site, L) ]
       //, _debug= str(_debug, "<br/>"
                 //,"<br/>, <b>_i</b>= ", _i
                 //,"<br/>, <b>len</b>= ", len
                 //,"<br/>, <b>L</b>= ", L
                 //,"<br/>, <b>_rtn</b>= ", _rtn
                 //,"<br/>, <b>newrtn</b>= ", newrtn
                 //,"<br/>, <b>actual len</b>= ", dist( get(newrtn,[-1,-2]))
                 //,"<br/>, <b>pt</b>= ", pt
                 //)
       )   
   //echo(log_( ["site", site, "_nseg", _nseg], lv )) 
   //echo(log_( ["nseg", nseg, "_nseg", _nseg], lv )) 
   //echo(log_( ["a", a, "_a", _a], lv )) 
   //echo(log_( ["as", as, "_as", _as], lv )) 
   //echo(log_( ["a2", a2, "_a2", _a2], lv )) 
   //echo(log_( ["len", len, "_len", _len], lv )) 
   //echo(log_( ["lens", lens, "_lens", _lens, "L", L], lv )) 
   //echo(log_( ["isReturnHash", isReturnHash, "_isReturnHash", _isReturnHash], lv )) 
   //echo(log_( ["newrtn", newrtn], lv ))
   
   //echo(log_e("linePts:init", lv))   
   linePts( site   = _site
          , nseg = _nseg>=1? _nseg: 2
          , a = _a
          , a2 = _a2
          , len = _len
          , as = _as
          , a2s = _a2s
          , lens= lens
          , isReturnHash = _isReturnHash 
          , _rtn   = newrtn
          , _i     = _i+1
          , _debug = _debug
          ) 
              
 :_i<nseg+1? // ###################################### i >= 0
   
    let(   
         //_a = isarr(a)? rand(a[0],a[1]):a
       //, _a2= isarr(a2)? rand(a2[0],a2[1]):a2
       
         _a = as? as[_i-2] 
              : isarr(a)? rand(a[0],a[1]):a
              
       , _a2= a2s?a2s[_i-2]
               : isarr(a2)? rand(a2[0],a2[1]):a2
       
       , L  = lens? lens[_i-1]
               : isarr(len)? rand( len[0], len[1] ) 
               : len // when len= num
               
       , pt= let( P= last(_rtn,2)
                , Q= last(_rtn)
                , Rf = _i==2 && site? site[2] //onlinePt( p12(seed), len=L)
                       : iscoline( get(_rtn, [-3,-2,-1]) )
                         ? randOfflinePt( [P,Q] )
                         : last(_rtn,3)
                  )
               anglePt( [P,Q,Rf], a=_a, a2=_a2, len=L)   
                       
       , newrtn = app(_rtn, pt ) 
       
     )//let
     //echo(log_( ["lens", lens, "L", L] )) 
     //echo(log_( ["L", L] )) 
     //echo(log_(["_i", _i], lv+1 ))
     //echo(log_(["a",a, "a2", a2], lv+1 ))
     //echo(log_(["as",as, "a2s", a2s], lv+1 ))
     //echo(log_(["_a",_a, "_a2", _a2], lv+1 ))
     //echo(log_e("", lv+1))	
     linePts( site   = site
            , nseg = nseg
            , a = a
            , a2 = a2
            , len = len
            , lens= lens
            , as = as
            , a2s = a2s
            , isReturnHash = isReturnHash 
            , _rtn   = newrtn
            , _i     = _i+1
            , _debug = _debug
            )
            
 : let( rtn= isReturnHash? 
              [ "pts", _rtn
              , "site", site
              , "a",a
              , "a2", a2
              , "len", len
              , "as",as
              , "a2s", a2s
              , "lens",lens
              , "debug",_debug
              ]
	      :  _rtn
	)
	
  //echo(log_e("", lv+1))	
  rtn        
 
);

} // linePts   
/*
       site   //=undef // base pqr
     , nseg  
     , a     //=[-360,360]
     , a2    //=[-360,360]
     , len //= [0.5,3] // rand len btw two values. Could be just a num
     
     , as // an arr to give predefined as
     , a2s  // an arr to give predefined a2s
     , lens   // An arr to give predefined lens   
     , isReturnHash // if true, return pts. Else, a hash.
     , opt=[]
     */  
      
    linePts=["linePts", " site,nseg,a,a2,len,as,a2s,lens,isReturnHash,opt", "hash|pts","Geometry"
    , "Generate a predefined pts, return either the pts or a hash containing pts and other info.
    ;; 
    ;;  site: a 3-pointer, as the PQR in the graph below. 
    ;;  nseg: count of segments
    ;;  a,a2: determine the 2 angles as those in anglePt():
    ;;
    ;;        a = aPQS, a2= aSQT
    ;;
    ;;             N      _T
    ;;             |   _-:   a2= aSQT
    ;;            /|_-'   :
    ;;           | Q------'. --------- M 
    ;;           |/:'-._/  :
    ;;           /__:__/'-.|
    ;;          /    :      '-._
    ;;         /      R    _-'   S
    ;;        P ... a= aPQS
    ;; 
    ;;  len: length of segment(s)
    ;; 
    ;;  All a,a2 and len have corresponding plural forms: as, a2s, lens
    ;;  and work the same way:
    ;;
    ;;  --- len = num: each segment len = len   (fixed len)
    ;;  --- len =[a,b]: each segment len varying between a and b (varying len)
    ;;  --- lens defined: predefined len
    ;; 
    ;;  isReturnHash:
    ;;    false: return pts 
    ;;    true: return a hash containing keys:
    ;;           ['pts', 'site', 'a', 'a2', 'len', 'as', 'a2s', 'lens'] 
    "];

    function linePts_test( mode=MODE, opt=[] )=
    (
        doctest(linePts
        , [
            [linePts(),"","linePts()", ["notest",1] ]
          ]
        , mode=mode, opt=opt 
        )
    );
 
//========================================================
{ // longside
// This is the hypotenuse of a right tria
function longside(a,b)= sqrt(pow(a,2)+pow(b,2));

    longside=["longside","a,b","number","Triangle",
     " Given two numbers (a,b) as the lengths of two short sides of a 
    ;; right triangle, return the long side c ( c^2= a^2 + b^2 )."
    ,"shortside"
    ];
    function longside_test( mode=MODE, opt=[] )=
    (
        doctest( longside, 
        [ [ longside(3,4), 5, "3,4" ]
        ], mode=mode, opt=opt
        )
    );
} // longside

//. m,n,o
//========================================================

function meshPts(size=[3,3], xs, ys, noise=[], opt, site=undef)=
(                                          // noise: check randPt for noise definition
             
   let(_size = sizeArg(size, opt, "size", [3,3,0])
      ,_xs = arg(xs, opt, "xs", range(_size[0])) 
      ,_ys = arg(ys, opt, "ys", range(_size[1]))
      ,_noise = update(["x",0, "y",0,"z",0],arg(noise, opt, [] ) )
      , pts = _noise?
                 [ for(x=_xs) for(y=_ys) [x,y,0]+ randPt(opt=_noise) ]
               : [ for(x=_xs) for(y=_ys) [x,y,0] ]
      )
//   echo("Inside meshPts --------")
//   echo(_size=_size)
//   echo(_xs = _xs )
//   echo(_ys = _ys ) 
//   echo(_noise= _noise)
//   echo(pts=pts)  
   pts
);



{ // midPt
function midPt(PQ)= (PQ[1]-PQ[0])/2 + PQ[0];
    
    midPt=[ "midPt", "PQ", "pt", "Point" ,
    "Given a 2-pointer PQ, return the pt that's in the middle of P and Q
    ;; 
    ;; Note: = (P+Q)/2
    "];

    function midPt_test( mode=MODE, opt=[] )=
    (
        let( pq= [ [2,0,4],[1,0,2] ] )
        doctest( midPt, 
        [ 
          [ midPt( pq ), [1.5,0,3], pq ]
        ], mode=mode, opt=opt, scope=["pq",pq]
     )
    );

} // midPt




////==================================================
//minPt=[ "minPts", "pts", "array", "Point, Math",
//"Given an array of points (pts), return a pt that
//;; has the min x then min y then min z.
//"];
// 
//function minPt_test( mode=MODE, opt=[] )=
//(	
//    let( pts1= [[2,1,3],[0,2,1] ]
//	   , pts2= [[2,1,3],[0,3,1],[0,2,0], [0,2,1]]
//       )
//	
//	doctest( minPt,
//	[
//      "var pts1", "var pts2"
//    , [ minPts( pts1 ), [0,2,1], "pts1" ]
//    , [ minPts( pts2 ), [0,2,0], "pts2" ]
//    ]
//    , mode=mode, opt=opt, scope=["pts1", pts1, "pts2", pts2]
//	)
//);
//
////==================================================
//function minxPts_test( mode=MODE, opt=[] )=(//======================
//	
//    let( pts1= [[2,1,3],[0,2,1] ]
//	   , pts2= [[2,1,3],[0,3,1],[0,2,0]]
//       )
//	
//	doctest( minxPts,
//	[
//      "var pts1", "var pts2"
//    , [ minxPts( pts1 ), [[0,2,1]], "pts1" ]
//    , [ minxPts( pts2 ), [[0,3,1],[0,2,0]], "pts2" ]
//    ]
//    , mode=mode, opt=opt, scope= ["pts1", pts1, "pts2", pts2]
//	)
//);
//
////==================================================
//function minyPts_test( mode=MODE, opt=[] )=(//==================
//	
//    let( pts1= [[2,1,3],[0,2,1],[3,3,0]]
//	   , pts2= [[2,1,3],[0,3,1],[3,1,0]]
//	   )
//       
//	doctest( minyPts, 
//    [
//      "var pts1", "var pts2"
//    , [ minyPts( pts1 ), [[2,1,3]], "pts1" ]
//    , [ minyPts( pts2 ), [[2,1,3],[3,1,0]], "pts2" ]
//    ]
//    ,mode=mode, opt=opt, scope=["pts1", pts1, "pts2", pts2]
//	)
//);
//
////==================================================
//function minzPts_test( mode=MODE, opt=[] )=(//==================
//	
//    let( pts1= [[2,1,3],[0,2,1]]
//	   , pts2= [[2,1,3],[0,3,1],[0,2,1]]
//	   )
//	doctest( minzPts,
//    [
//      "var pts1", "var pts2"
//    , [ minzPts( pts1 ), [[0,2,1]], "pts1"  ]
//    , [ minzPts( pts2 ), [[0,3,1],[0,2,1]], "pts2" ]
//    ]
//    , mode=mode, opt=opt, scope=["pts1", pts1, "pts2", pts2]
//	)
//);
		

//========================================================

{ // mirrorPt
function mirrorPt( pt, pl )=
(
  gtype(pl)=="pl"
  ? let( int= projPt( pt, pl ) )
    int-(pt-int)
  : gtype(pl)=="ln"
    ? let( int= projPt( pt, getPlaneByNormalLine(pl)) ) 
      int-(pt-int) 
  : gtype(pl)=="pt"
    ? //pl + pl - pt
      let( int= projPt( pt, getPlaneByNormalLine( [ORIGIN, pl])) ) 
      int-(pt-int) 
  : _red(str("Error in mirrorPt(pt,pl): pl should be a plane, a line or a pt. "
           , "You gave: ", pl))  
);  
}


{
function mirrorPts( pts, pl )=
  [ for(p=pts) mirrorPt(p,pl) ];
}
  
   
//========================================================
//{ // movePts
//function movePts(pts,to,from)=
//(  // dev: refer to getMoveData_test_poly()
//    let( from = from?from:p012(pts)
//       , mvdata= getMoveData( from, to )
//       , got1 = [for(p=pts)p+to[1]-from[1]]
//       , got2= rotPts( got1
//                , hash(mvdata, "rotaxis")
//                , hash(mvdata, "rota")
//                ) 
//       , got3= rotPts( got2
//                , hash(mvdata, "rotaxis2")
//                , hash(mvdata, "rota2")
//                )
//       ) got3 
//);
//       
//    movePts=["movePts", "pts,to,from","pts","Geometry"
//    , "Move pts from *from* to *to* (both are 3-pointer)"
//    ];
//} // // movePts

//========================================================
{ // movePts
function movePts(pts, from, to)=            // Updated 2017.10.15 to keep the relation
(  // dev: refer to getMoveData_test_poly()
    let( hasfrom= from              // Save the *from* info 
       , from = from?from:p012(pts) // If *from* not given, use the first 3 of pts
       , mvdata= getMoveData( from, to ) 
       , pts = hasfrom                           // Tricky part here: add the 1st 3 pts
                ? concat( slice(from,0,3), pts)  // of *from* to the begin of data
                : pts                            // This is needed to keep the relation
                                                 // between pts and *from* unchanged.       
       , got1 = [for(p=pts)p+to[1]-from[1]]  
       , got2= rotPts( got1
                , hash(mvdata, "rotaxis")
                , hash(mvdata, "rota")
                ) 
       , got3= rotPts( got2
                , hash(mvdata, "rotaxis2")
                , hash(mvdata, "rota2")
                )
                
       ) hasfrom? slice( got3, 3)   // Remove the *from* from the data 
                : got3 
);
       
    movePts=["movePts", "pts,to,from","pts","Geometry"
    , "Move pts from *from* to *to* (both are 3-pointer)"
    ];
} // // movePts


function movePts2(pts, from, to)= // try approach of coord transition (2018.21.1)  
(                                 // See coord_trans() in study_vectors.scad
   let( _from = from?from: [X,O,Y]
      , _to = to?to:[X,O,Y]
      , _coordfrom = len(_from)==4?_from: [_from[1], _from[0], N2(_from), N(_from)]
      , _coordto = len(_to)==4?_to: [_to[1], _to[0], N2(_to), N(_to)]
      , Of = _coordfrom[0]
      , Ot = _coordto[0]
      , uvf= [for(i=[1:3]) uv(_coordfrom[i]-Of)]
      , uvt= [for(i=[1:3]) uv(_coordto[i]-Ot)]
      , rtn = [ for(pt=pts) solve3(transpose(uvt), (pt-Of)*uvf)+Ot ] 
   )
   echo(_from=_from, _to=_to)
   echo(_coordfrom=_coordfrom, _coordto=_coordto)
   echo(Of = Of, Ot=Ot)
   echo(movePts=rtn)
   rtn
);
       
    movePts=["movePts", "pts,to,from","pts","Geometry"
    , "Move pts from *from* to *to* (both are 3-pointer)"
    ];


//========================================================
{ // normal
function normal(pqr, len=undef, debug=false)=
(  
//// 2015.8.14: add debug. Not sure if it works yet
//
//    let( pqr= [pqr[0],pqr[1],pqr[2]] 
//       , err= debug? 
//                (let( msg= "pqr in normal() should")
//                !ispts(pts)? err( msg, "be pts", gtype(pqr) )
//                : ( isequal( pqr[0], pqr[1])                           
//                  || isequal( pqr[2], pqr[1])
//                  || isequal( pqr[0], pqr[2]) )?
//                  err( msg, "have at least 3 unique pts", "2 are the same pt")
//                : iscol(pqr)? err( msg, "not be colinear", "colinear")
//                : undef
//                )
//             : undef 
//      )
//   err? err:
    let( nv = cross( pqr[0]-pqr[1], pqr[2]-pqr[1] ) )
    len? onlinePt( [ORIGIN, nv], len=len) : nv

);

    normal= ["normal","pqr","pt","math",
    "Given a 3-pointer pqr, return the normal vector Nv (a vector) to a plane
    ;; formed by [P-Q,O,R-Q]. The line [Nv,O] will be L to PQ and RQ.  
    
    
          |
          |
          |        _.
          |    _.-'
          |_.-'
          O._
             '-._
                 '-._
                     '-.      
                 
    "];
    function normal_test( mode=MODE, opt=[] )=
    (
        let( pqr = [[-1, -2, 2], [1, -4, -1], [2, 3, 3]] )
        
        doctest( normal, 
        [
          [ normal(pqr), [-13, 11, -16], pqr ]
        ]
        , mode=mode, opt=opt
        )
    );
} // normal 

//========================================================
{ // normalLn
//function normalLn( pqr, len=undef, sign=1)=
//  let( Q=pqr[1], N=N(pqr) 
//     )
//(
//   sign==1 && len==undef? [Q,N] : onlinePt( [Q,N], len=sign*(len?len:norm(N-Q)) ) 
//);   
function normalLn( pqr, len=undef)= // Recode 2018.5.11
(  let( N=N(pqr) )
   //echo("in normalLn", pqr=pqr, N=N)
   [ pqr[1], onlinePt( [ pqr[1],N], len= len==undef? norm(N-pqr[1]): len )] 
);   
    normalLn=["normalLn","pqr,len=undef, sign=1", "2-pointer", "Geometry",
    " Return a 2-pointer, [Q,N], where Q is the mid pt of pqr and N is 
    ;; the normal pt. Set len to determine the length. len could be <0"
    ];

    function normalLn_test( mode=MODE, opt=[] )=
    (
        doctest( normalLn,
        []
        , mode=mode, opt=opt
        )
    );
} // normalLn

//========================================================
{ // normalPt
function normalPt(pqr, len=1, reversed=false)=
(
	len==undef? (reversed?-1:1)*normal(pqr)+pqr[1]
			  :onlinePt( [ pqr[1]
                      , (reversed?-1:1)*normal(pqr)+pqr[1]
                       ]
                      , len= len*mm )

/*        
        
  pqr=pqr();
  Q = pqr[1];
  qp = pqr[0]-pqr[1];
  qr = pqr[2]-pqr[1];

  Np = cross( qp,qr );
  Np2 = cross( qr,qp );

  MarkPts( pqr, "PQR");
  N = Np + Q;
  N2= Np2 + Q;
  MarkPts( [N,Q,N2]
   , Label=["text",["N","","N2"]] );
 */
                      
                      
);
    normalPt=["normalPt", "pqr,len,reversed", "point", "Point",
     " Given a 3-pointer (pqr), optional len and reversed, return a 
    ;; point representing the normal of pqr."
    ];
function normalPt_test( mode=MODE, opt=[] )=
(
	doctest( normalPt,
	[]
	, mode=mode, opt=opt
	)
);
} // normalPt		


//========================================================
{ // normalPts
function normalPts(pts, len=1)=
 let(tripts= subarr(pts,  cycle=true)
	)
( 
	[for(pqr=tripts) normalPt(pqr,len=len)]
);

    normalPts=["normalPts", "pts,len=1", "pts", "Geo",
    "Return normal pts of each pt in pts"
    ];
    function normalPts_test( mode=MODE, opt=[] )=
    (
        doctest( normalPts, [], mode=mode, opt=opt )
    );
} // normalPts

//========================================================
{ // objArgHandler

function objDecoHandler( color, Frame,MarkPts, Dim, opt=[] )=
(           
  //let( _Frame = und_opt(Frame, opt,"Frame", 0.01)
     //, Frame = isnum(_Frame)? ["r", _Frame]: _Frame
     //) // let
//
  //[ "color", getColorArg(color, opt) // Assure color is [name, transp]
  //, "Frame", update( Frame, ["shape", hash(opt, "FrameShape","rod")] )
  //, "MarkPts", subprop(MarkPts, opt, "MarkPts", []) 
  //, "Dim", concat( hash(opt,"Dim",[]), Dim?Dim:[] )
  //]
  //

  [ "color", getColorArg(color, opt) // Assure color is [name, transp]
  , "Frame", subprop( Frame, opt, "Frame"
                    , dfPropName="r"
                    , dfPropType="num"
                    , df=["r",0.01, "FrameShape", "rod"]
                    )
                    //, df=["shape", hash(opt, "FrameShape","rod")] )
  , "MarkPts", subprop(MarkPts, opt, "MarkPts", []) 
  , "Dim", concat( hash(opt,"Dim",[]), Dim?Dim:[] )
  ]
  
);

//function objArgHandler( pts
//                      , cubeSize 
//                      , center, color                  // simple args
//                      , frame, site, actions, markPts, dim  // compound args
//                      , opt=[]
//           )=
//(           
//  let( 
//   //----------------------------------- simple args
//   
//     
//   _pts = und_opt( pts, opt, "pts", [])
//   , _cubeSize = und_opt(cubeSize, opt, "cubeSize", [2,2,2])
//   , cubeSize = _pts? []
//                : isnum(_cubeSize)  
//                   ? [_cubeSize,_cubeSize,_cubeSize]
//                   : _cubeSize
//                //: objArgHandler_getSizes(  // It sets cubSize default if it's not given
//                //   cubeSize=cubeSize, opt=opt)
//              
//   // Assure center is [a,b,c]              
//   , _center = und_opt( center, opt, "center", [] )
//   , center = isarr(_center)? _center : _center?[1,1,1]:[]
//   
//   // Assure color is [name, transp]   
//   , color= assurePair( und_opt(color, opt, "color", ["gold",0.9]), 1) 
//   
//   , site = und_opt(site, opt, "site",[])
//   
//   //----------------------------------- compound args
//   // Frame    
//   , _frame = und_opt(frame, opt,"frame", true)
//   , frameOpt = isnum(_frame)? ["r", _frame]: _frame
//   
//   // actions // In Cube(actions,opt), we have actions and opt.actions
//              // actions will be appended to opt.actions, unless 
//              // actions= false that disable any actions
//   , _actions = concat( hash(opt,"actions",[]), actions?actions:[] )           
//   , actions = center&& cubeSize
//               ? concat( ["t", [ center[0]?-cubeSize[0]/2:0
//                               , center[1]?-cubeSize[1]/2:0
//                               , center[2]?-cubeSize[2]/2:0 ]
//                         ], _actions )
//               : _actions           
//   
//   // markPts
//   , markPtsOpt= und_opt(markPts, opt, "markPts", [])
//   
//   // dim 
//   , dimOpt = concat( hash(opt,"dim",[]), dim?dim:[] )
//              
//   , pts0 = _pts? _pts
//	     :[ [cubeSize.x,0,0], ORIGIN
//              , [0,cubeSize.y,0],[cubeSize.x, cubeSize.y,0] 
//              , [cubeSize.x,0,cubeSize.z], [0,0,cubeSize.z]
//              , [0,cubeSize.y,cubeSize.z], cubeSize  
//             ]                         
//    , pts2= actions? transPts(pts0, actions=actions): pts0 
//    , pts = site? movePts( pts2, site, [[1,0,0], ORIGIN, [-1,1,0]] ): pts2
//
//         
//   //--------------------------------------------------
//   
//  ) // let
//
//  [ "pts", pts
//  , "cubeSize", cubeSize
//  , "center", center, "color", color, "site", site
//  , "actions", actions
//  , "frame", frameOpt
//  , "markPts", markPtsOpt 
//  , "dim", dimOpt
//  ]
//  
//);
}

    objArgHandler=[ 
     "objArgHandler"
     , "pts, cubeSize,center,color,frame,site,actions,markPts,dim,opt=[]"
     , "hash", "Args",
    " Merge args with opt.args and return a hash containing proper arg values.
    ;;
    ;; Intended to serve as a common arg handler for Cube, Cylinder, 
    ;; cubePts, cylinderPts, etc. 20171229.
    ;;
    ;; 2 main features:
    ;; 
    ;; (1) The args are of two types: those set as plain args (pts, cubeSize, etc) 
    ;; those set in opt ( ['pts', pts, 'cubeSize', cubeSize, ...] ). This function
    ;; merge both types and return a single hash.
    ;;
    ;; (2) It handles size definition and returns the processed pts as an item 
    ;; ['pts',pts] for the intended object. The 'processed' means all the 
    ;; center/actions/site are already appied to the pts.  
    ;;
    ;; Note: this might slow down the execution 'cos too many vars in let(...).
    ;;
    ;; 2018.1.9:
    ;; This function is now the core of at least cubePts, Cube and PolyCube
    ;; 
    "];
    


//========================================================
function octant(pt, Rf=O)= 
( // we present octant as 100, 10 etc... instead of 
  // "+--", "-+-", etc, imagining future need for calc
  // 2018.6.6. 
  //100*((pt-O).x>0?1:0)+ 10*((pt-O).y>0?1:0)+ ((pt-O).z>0?1:0)
  //
  // to-do (2018.10.30): 
  // Generalize it to be available to customized coordate system
  // (Check out dev_inTrianglePt_cone_2() in dev_189p_vectors.scad)
  // 
  
  let(d=pt-Rf)
  d.x>0? (
           d.y>0? (d.z>0? 0:4)  
                : (d.z>0? 3:7)  
         )
       : (
           d.y>0? (d.z>0? 1:5)  
                : (d.z>0? 2:6)  
         )
  
  /*
                             |   .
                             |  /|
                  1          | / |       0
                             |/  | /    
                .------------+------------.
                |   .-------/|---+--------|---.
                |  /       / |  /|        |  /
                | / 2     /  | / |    3   | /
                |/       .   |/  |        |/
           -----+--------|---+------------+-----
               /|        |  /|   '       /|
         5    / |        | / |  /       / |   4
             /  |        |/  | /       /  |
            '------------+------------'   |
                '-------/|---+------------'
                       / |  /|
               6         | / |      7
                         |/
                         '   
                         
//                             |   .
//                             |  /|
//                  2 (11)     | / |       1 (111)
//                             |/  | /    
//                .------------+------------.
//                |   .-------/|---+--------|---.
//                |  /       / |  /|        |  /
//                | / 3 (1) /  | / | 4 (101)| /
//                |/       .   |/  |        |/
//           -----+--------|---+------------+-----
//       (10)    /|        |  /|   '       /|
//         6    / |        | / |  /       / |   5 (110)
//             /  |        |/  | /       /  |
//            '------------+------------'   |
//                '-------/|---+------------'
//               7       / |  /|
//              (0)        | / |   8 (100)
//                         |/
//                         '   
                    
   */
);

{ // onlinePt
function onlinePt(pq, len=undef, ratio=0.5)=
(  let( ratio = len==undef? ratio: len/dist(pq) )
   (1-ratio)*pq[0]+ ratio*pq[1]
);
//(   len==undef?
//    let( pq = [pq[0],pq[1]]
//       , L = isnum(len)?mm*len/dist(pq):ratio)
//	[ L*dist(pq,"x")+pq[0].x
//	, L*dist(pq,"y")+pq[0].y 
//	, L*dist(pq,"z")+pq[0].z ]
//    : (1-ratio)*pq[0]+ ratio*pq[1]	 
//);
    onlinePt=[ "onlinePt", "PQ,len=false,ratio=0.5", "pt", "Point",
    " Given a 2-pointer (PQ), return a point (pt) that lies along the
    ;; line between P and Q, with its distance away from P defined by 
    ;; *len* or by *ratio* if len is not given. When ratio=1, return Q.
    ;; Both *len* and *ratio* can be negative.
    ;;
    ;; pq: a 2-pointer. But can be len(pq)>2, in which the 2st 2 pts are used.
    "];
    function onlinePt_test( mode=MODE, opt=[] )=
    (
       /*
       
         +
         |
         +  +---+---P          Q 
         |.'|       |          |
         +  +  +---+|--+---+---+ 
         |  |.'     |        .' 
         +  +---+---+       + 
         |.'      .'      .'
         +---+---+---+---+---+---+---+
         
       */
    
        let( pq= [[2,1,2], [4,2,1]]
           , d1pt= onlinePt( pq, len=1 )
           , r3pt= onlinePt( pq, ratio=0.3)
           , distpq = dist(pq)
           )
     
        doctest( onlinePt, 
        [
          "var pq"
        , str("dist= ", dist(pq))  
        , [ onlinePt(pq,0), pq[0] , "pq,0"]
        , [ onlinePt(pq,ratio=1), pq[1] , "pq,ratio=1"]
        , [ onlinePt(pq,ratio=.5), [3,1.5,1.5] , "pq,ratio=.5"]
        , [ onlinePt(pq,1), [2.8165, 1.40825, 1.59175], "pq,1", ["asstr",true]]

        //, [ norm(pq[0]- onlinePt(pq,len=1))
            //, 1, "pq,len=1"
            //, ["myfunc", "norm( pq[0]- onlinePt{_} )"]]
        //
        //, [ norm(pq[1]- onlinePt(pq,len=-2))
            //, distpq+2, "pq,len=-2"
            //, ["myfunc", "norm( onlinePt{_}-pq[1] )"]]
//
        //, [ norm(pq[0]- onlinePt(pq,ratio=0.3))
            //, distpq*0.3, "pq,ratio=0.3"
            //, ["myfunc", "norm( pq[0]- onlinePt{_} )"]]
            //, ["funcwrap", "norm({_}-pq[0])"]]
            //
        //, [ norm(pq[1]- onlinePt(pq,ratio=-0.3))
            //, distpq*1.3, "pq,ratio=-0.3"
            //, ["myfunc", "norm( pq[1]- onlinePt{_} )"]]
            //, ["funcwrap", "norm({_}-pq[1])"]]	 
        ]

        , mode=mode, opt=opt, scope=["pq", pq, "distpq", distpq, 
                            "d1pt", d1pt, "r3pt", r3pt]
        )
    );

    //doc(onlinePt);
} // onlinePt

//========================================================

{ // onlinePts
function onlinePts(pq, lens=false, ratios=false)=
(
	ratios==false?
	( len(lens)==0?[]
	  :	[for(l=lens) onlinePt( pq, len=l)]
	):
	( len(ratios)==0?[]
      : [for(r=ratios) onlinePt(pq,ratio=r)]
	)
);
      
    onlinePts=["onlinePts","PQ,lens=false,ratios=false","path","Point",
    "
     Given a 2-pointer (PQ) and a numeric array assigned to either lens
    ;; or ratios, return an array containing pts along the PQ line with 
    ;; distances (from P to each pt) defined in the given array. Refer to
    ;; onlinePt().
    ;; This is good for getting multiple points on pq line. For example, to
    ;; get 4 points that divide pq line evenly into 5 parts:
    ;;
    ;;   onlinePts( pq, ratios=[ 0.2, 0.4, 0.6, 0.8 ] )
    ;;
    ;; This ratios can be produced this way: range(0.2,0.2,1)
    "];

    function onlinePts_test( mode=MODE, opt=[] )=
    (
        let( pq= [[0,1,0],[10,1,0]] //randPts(2);
           , lens= [-1,2]
           , ratios= [0.2, 0.4, 0.6]
           , distpq = dist(pq)
           )
        doctest( onlinePts, 
        [
          "var pq"
        , [ onlinePts(pq,lens), [[-1, 1, 0], [2, 1, 0]], "pq,[-1,2]" ]
        , [ onlinePts(pq,ratios=ratios)
                     , [[2, 1, 0], [4, 1, 0], [6, 1, 0]], "pq,ratios=ratios"]
    //    ,"// Case below produces exact same result, but why test fails ? " 
    //    ,  [ "pq,ratios=range(0.2,0.2,0.7)", onlinePts(pq,ratios=range(0.2,0.2,0.7))
    //                                        , [[2, 1, 0], [4, 1, 0], [6, 1, 0]]]
         ]
        , mode=mode, opt=opt, scope=["pq", pq
               ,"lens", lens
               ,"ratios",ratios
               ,"distpq",distpq] 
                )
    );

    //echo( onlinePts_test(mode=12)[1] );

}// onlinePts


//========================================================
{ //othoPt

function othoPt(pqr, tip=1)=
(
          //is= i==0?[2,0,1]:i==1?[0,1,2]:[1,2,0];
     
   //  tip=0 (default)        tip=1            tip=2
   //
   //          P                Q                R  
   //        .'|:             .'|:             .'|:    
   //      .'  | :          .'  | :          .'  | :
   //    .'    |  :       .'    |  :       .'    |  : 
   //  R'------J---Q    P'------J---R    Q'------J---P
   //          B                B                B  
   //        .'|:             .'|:             .'|:    
   //      .'  | :          .'  | :          .'  | :
   //    .'    |  :       .'    |  :       .'    |  : 
   //  A'------J---C    A'------J---C    A'------J---C

   //let( is= tip==0?[1,0,2]:tip==1?[2,1,0]:[0,2,1]
   let( is= tip==0?[2,0,1]:tip==1?[0,1,2]:[1,2,0]
                       //  0  1  2
      , A= pqr[is[0]]  //  R  P  Q  
      , B= pqr[is[1]]  //  P  Q  R 
      , C= pqr[is[2]]  //  Q  R  P
      
      // B is the tip, get othoPt on AC
      , d= (B-A)*uv(C-A) // dot prod of AB and uv(AC)  
                         // = length of projPt(B,AC)
      , r = d/norm(C-A)
      )
   r*C+(1-r)*A
);


//function othoPt( pqr )= 
//(
///*  Return [x,y] of D
//           Q          ___
//           ^\          :
//          /: \         :   
//       a / :  \_  b    : h
//        /  :    \_     :
//       /   :-+    \    :    
//     P-----D-!------R ---
//
//	|------ c ------|  
//        
//	a^2 = h^2+ PD^2
//	b^2 = h^2+ DR^2  
//	a^2- PD^2 = b^2-(c-PD)^2
//			  = b^2-( c^2- 2cPD+PD^2 )
//			  = b^2- c^2 + 2cPD -PD^2
//	a^2 = b^2-c^2+ 2cPD
//	PD = (a^2-b^2+c^2)/2c
//	ratio = PD/c = (a^2-b^2+c^2)/2c^2    
//*/
//    let( c2= pw(d02(pqr)) )
//	onlinePt( [pqr[0],pqr[2]]
//	        //, ratio=( d01pow2(pqr) - d12pow2(pqr) + d02pow2(pqr))/2/d02pow2(pqr) 
//	        , ratio=(   pw(d01(pqr)) - pw(d12(pqr)) + c2)/2/c2            
//	)
//);

     othoPt= [ "othoPt", "pqr, tip=0", "pt", "Point",
     "Given a 3-pointer (pqr), return the coordinate of the point J
     ;; such that line QD is at 90 degree with line PR.
     ;;
     ;; tip desices which side the J is.
     ;;
     ;;  tip=0 (default)        tip=1            tip=2
     ;;
     ;;          P                Q                R  
     ;;        .'|:             .'|:             .'|:    
     ;;      .'  | :          .'  | :          .'  | :
     ;;    .'    |  :       .'    |  :       .'    |  : 
     ;;  R'------J---Q    P'------J---R    Q'------J---P
     "];

     
    function othoPt_test( mode=MODE, opt=[] )=
    (
        let( pqr= randPts(3)
           , P = pqr[0]
           , Q = pqr[1]
           , R = pqr[2]
           )
        doctest( othoPt,
        [ 
          "var pqr"   
 
        , [ angle([P,othoPt(pqr),Q]), 90,""
          ,  [ "fname"
             , "angle([P,othoPt(pqr,tip=0),Q])"
             , "//", "default"
             ] 
          ]
        , [ angle([P,othoPt(pqr),R]), 90,""
          ,  [ "fname"
             , "angle([P,othoPt(pqr,tip=0),R])"
             , "//", "default"
             ] 
          ]
        ,""  
        , [ angle([Q,othoPt(pqr,1),R]), 90,""
          ,  [ "fname"
             , "angle([Q,othoPt(pqr,tip=1),R])"
             ] 
          ]
        , [ angle([Q,othoPt(pqr,1),P]), 90,""
          ,  [ "fname"
             , "angle([Q,othoPt(pqr,tip=1),P])"
             ] 
          ]
        ,""  
        , [ angle([P,othoPt(pqr,2),R]), 90,""
          ,  [ "fname"
             , "angle([R,othoPt(pqr,tip=2),P])"
             ] 
          ]
        , [ angle([Q,othoPt(pqr,2),R]), 90,""
          ,  [ "fname"
             , "angle([R,othoPt(pqr,tip=2),Q])"
             ] 
          ]
        ]
        , mode=mode, opt=opt, scope=["pqr",pqr, "P",P,"Q",Q,"R",R]
        )
    );

    //echo( othoPt_test()[1] );
} // othoPt 

//. p

//========================================================
{ // planecoefs
function planecoefs(pqr, has_k=true)=
(
	/*
	https://answers.yahoo.com/question/index?qid=20110121141727AAncAu4

	Using normal vector <5, 6, -3> and point (2, 4, -3), we get equation of plane: 
	5 (x-2) + 6 (y-4) - 3 (z+3) = 0 
	5x - 10 + 6y - 24 - 3z - 9 = 0 
	5x + 6y - 3z = 43

	normal = [a,b,c]
	k = -(ax+by+c)

	*/	
	concat( normal(pqr) 
		  , has_k?[-( normal(pqr)[0]* pqr[0][0]
			 + normal(pqr)[1]* pqr[0][1]
			 + normal(pqr)[2]* pqr[0][2]
             )]:[]			 
		)	
);
    planecoefs=["planecoefs", "pqr, has_k=true", "arr", "Geometry" ,
    "
     Given 3-pointer pqr, return [a,b,c,k] for the plane ax+by+cz+k=0.
    "];
     
    function planecoefs_test( mode=MODE, opt=[] )=
    (
        let( pqr = randPts(3)
        //pcs = planecoefs(pqr); // =[ a,b,c,k ]
        //a= pcs[0];
        //b= pcs[1];
        //c= pcs[2];
        //k= pcs[3];
           , othopt = othoPt( pqr )
        //abpt= angleBisectPt( pqr );
        //mcpt= cornerPt( pqr );
           )
        doctest( planecoefs, 
        [
             [ pqr, pqr,"",["myfunc", "pqr", "ml",1, "notest",true]]
            //,str("pqr = ", arrbr( pqr ) )
            , [ othopt, othopt,"pqr",["myfunc", "othoPt", "notest",true]]
            , [ isOnPlane( planecoefs(pqr),othopt ), true ,"pqr", 
            , ["myfunc","isOnPlane( planecoefs{_}, othopt )"]
            //, ["funcwrap","isOnPlane( othopt, planecoefs= {_} )"]
            ]
            ,"// Refer to isOnPlane_test() for more tests."

        ], mode=mode, opt=opt//, scope=["pqr", pqr, "othopt", othopt] 
        )
    );
} // planecoefs

//========================================================
{ // planeDataAt
/*  
  ["pts", [ P, Q, R, S, T ...]
  , "i", 4
  , "p3", [ P0, P1, P2]
  , "closed", undef
  , "pl", [ Q0, Q1, Q2]
  , "Args", " ==> "
  , "pqr", [ P, Q, R]
  , "Rf", [-0.175325, -2.48801, -8.14195]
  , "rot", undef
  , "tilt", 90
  , "tiltforward", undef
  , "posttilt_rot", undef
  , "Calculated", " ==> "
  , "aPQR", 47.557
  , "iscoline", false
  , "A_af_rot", [-0.175325, -2.48801, -8.14195]
  , "A_af_tilt", [-1.02912, -1.47111, -9.99711]
  , "posttilt_axisP", [0.718954, -1.81898, -7.70697]
  , "A_af_rot2", [-1.02912, -1.47111, -9.99711]]
  ]
*/
function planeDataAt( 

        pts
        , i
        , Rf  // Ref pt to decides the first pt of pl (pl[0]) starts
        , rot // Rot about seg (i-1,i)
        , tilt      // = angle [ pts[i-1], pts[i], Rf ]
        , tiltforward // = angle [ pts[i+1], pts[i], Rf ]
                 //   This is the same as a2R in planeDataAtQ()
                 //   If this is defined, a is ignored. 
        , posttilt_rot   // The 2nd rot to rot the pl (after angle is applied)
                 //   about pl's normal 
        , closed
        )=
(
    let( //Rf = ispt(Rf)? Rf 
         //     : len(pts)==2? randOfflinePt( pts) : undef
       //, ai = angle( p3 )
       //, 
       
       pldata= 
       
            //--------------------------------------------------
            i==0?
                /* For i==0, a2next and rot2 are ignored, and a and rot
                   are assigned to a2R and rot2, resp, in planeDataAtQ:
                   
                    a  closed |  a2R for planeDataAtQ()
                    ----------+---------
                    -    -    |  90
                    num  -    |  a 
                    -    true |  und
                    num  true |  und 
                */
                let( p3= len(pts)==2 || rot? 
                            [ onlinePt(pts, -1), pts[0], pts[1] ] 
                            : closed? get(pts,[-1,0,1]):get(pts,[2,0,1])
                   , a= und(tilt, closed? angle(p3)/2: 90) 
                   , adjdir= angle( p201(pts))<a?-1:1

                   , pldata= planeDataAtQ( 
                                  pqr= p3
                                , rot=rot
                                , tiltforward= a 
                                , posttilt_rot= adjdir*posttilt_rot
                                , Rf= und(Rf, closed? last(pts):pts[2]) 
                                )
                   )                 
               concat( ["pts",pts,"i",i,"p3",p3,"closed",closed ], pldata  )            
                 
            //--------------------------------------------------
            :0<i && i < len(pts)-1 ?
            
                let( p3 = get3(pts, i-1) )
                concat( ["pts", pts,"i",i, "p3", p3, "closed",closed ] 
                      ,  planeDataAtQ( 
                                  pqr=p3
                                , rot=rot
                                , tilt = tilt
                                , tiltforward= tiltforward 
                                , posttilt_rot= posttilt_rot
                                , Rf=Rf )
//                                planeDataAtQ( pqr = p3
//                                    , rot=rot, a=a, a2R=a2next
//                                    , rot2=rot2, Rf=Rf )
                                    
                      )            

            //-------------------------------------------- i = last
            :   /* For i==last, a2next is ignored. 
                
                    a  closed |  a for planeDataAtQ()
                    ----------+-----------
                    -    -    |  90
                    num  -    |  a 
                    -    true |  und
                    num  true |  a 
                */    
                let( p3= len(pts)==2? 
                            [ pts[0], pts[1], onlinePt( p10(pts), -1) ] 
                            : get3(pts,i-1)
                   , a= und(tilt, closed? angle(p3)/2: 90) 
                   //, a= isnum(tilt)? tilt
                   //     : !closed? 90:undef
                   , Rf= Rf? Rf 
                         : closed?undef
                         : iscoline( get3(pts, -3))?  
                           randOfflinePt( get(pts, [-1,-2]))
                           : get(pts, -3)
                   , pldata= planeDataAtQ( 
                                  pqr= p3
                                , rot=rot
                                , tilt= a 
                                , posttilt_rot= posttilt_rot
                                , Rf=Rf )
                   
//                   planeDataAtQ( p3
//                                         , rot=rot, a=a, rot2=rot2
//                                         , Rf=Rf )
                   )
                   concat( ["pts",pts,"i",i,"p3",p3,"closed",closed], pldata  )     
    )// let
  
    pldata//concat( pldata, ["pts", pts,"i",i, "p3", p3 ] )
  
);


//function planeDataAt( 
//
//        pts
//        , i
//        , Rf  // Ref pt to decides the first pt of pl (pl[0]) starts
//        , rot // Rot about seg (i-1,i)
//        , a      // = angle [ pts[i-1], pts[i], Rf ]
//        , a2next // = angle [ pts[i+1], pts[i], Rf ]
//                 //   This is the same as a2R in planeDataAtQ()
//                 //   If this is defined, a is ignored. 
//        , rot2   // The 2nd rot to rot the pl (after angle is applied)
//                 //   about pl's normal 
//        , closed
//        )=
//(
//    let( //Rf = ispt(Rf)? Rf 
//         //     : len(pts)==2? randOfflinePt( pts) : undef
//       //, ai = angle( p3 )
//       //, 
//       pldata= 
//       
//            //--------------------------------------------------
//            i==0?
//                /* For i==0, a2next and rot2 are ignored, and a and rot
//                   are assigned to a2R and rot2, resp, in planeDataAtQ:
//                   
//                    a  closed |  a2R for planeDataAtQ()
//                    ----------+---------
//                    -    -    |  90
//                    num  -    |  a 
//                    -    true |  und
//                    num  true |  und 
//                */
//                let( p3= len(pts)==2? 
//                            [ onlinePt( pts, -1), pts[0], pts[1] ] 
//                            : get(pts,[-1,0,1])//PQ(pts, last(pts)) //get3(pts,0)
//                   , a= closed? undef: isnum(a)? a:90
////                   , a= isnum(a)? a
////                        : !closed? 90:undef
//                   , pldata= planeDataAtQ( p3
//                                        , rot=rot //(closed?1:-1)*rot
//                                        , a2R= a 
//                                        , rot2= rot2 //(closed?1:-1)*rot//2
//                                        , Rf=Rf )
//                   )                 
//               concat( pldata, ["pts", pts,"i",i, "p3", p3 ] )            
//                 
//            //--------------------------------------------------
//            :0<i && i < len(pts)-1 ?
//            
//                let( p3 = get3(pts, i-1) )
//                concat( 
//                        planeDataAtQ( pqr = p3
//                                    , rot=rot, a=a, a2R=a2next
//                                    , rot2=rot2, Rf=Rf )
//                                    
//                      , ["pts", pts,"i",i, "p3", p3 ] 
//                      )            
//
//            //-------------------------------------------- i = last
//            :   /* For i==last, a2next is ignored. 
//                
//                    a  closed |  a for planeDataAtQ()
//                    ----------+-----------
//                    -    -    |  90
//                    num  -    |  a 
//                    -    true |  und
//                    num  true |  a 
//                */    
//                let( p3= len(pts)==2? 
//                            [ pts[0], pts[1], onlinePt( p10(pts), -1) ] 
//                            : get3(pts,i-1)
//                   , a= isnum(a)? a
//                        : !closed? 90:undef
//                   , Rf= Rf? Rf 
//                         : closed?undef
//                         : iscoline( get3(pts, -3))?  
//                           randOfflinePt( get(pts, [-1,-2]))
//                           : get(pts, -3)
//                   , pldata= planeDataAtQ( p3
//                                         , rot=rot, a=a, rot2=rot2
//                                         , Rf=Rf )
//                   )
//                   concat( pldata, ["pts", pts,"i",i, "p3", p3 ] )     
//    )// let
//  
//    pldata//concat( pldata, ["pts", pts,"i",i, "p3", p3 ] )
//  
//);

} // planeDataAt

//========================================================
{ // planeDataAtQ
function planeDataAtQ( // Rtn a pl (3-pointer), named [A,B,C] in
                       // the code, that Q is the midPt of BC 
          pqr 
         ,rot=0   // angle of pl rot about PQ (PQ is normal to pl) 
         ,tilt =undef   // angle of pl bending toward P
         ,tiltforward=undef   // angle of pl bending toward R.
         ,posttilt_rot=0  // Rot about normal of newly created pl (i.e.,
                          //    after tilt)
         ,len =1          // len from Q to B and C. (but not QA so 
                          //    we get a tria that can point to a dir
         ,Rf = undef      // Ref Pt to set the starting pos of pl (i.e., A)

         )= 
(   
       /* 
          rot = aRJRx
          
                      N
                      |     _ Rx
                      |  _-`.`
              - - - - Q`- .:- - - - M 
                     / `.:    
                    / .:   `-_ 
                   /.:        `-_
                  J- - - - - - -R    
                 /
                P 
          
         Move Rx to Rxx such that a_PQRxx = a
         
                Rx
                /     Rxx
               /    _-`
              /  _-`
             /_-` a
            Q`-----------------P
       
        So, if rot=a=90, the result is a plane 90d to PQ
        
       */
       
    !ispts(pqr)? assert( unit="planeDataAtQ"
                      , var="pqr"
                      , val= pqr
                      , check= "gtype"
                      , want= "pts"
                      , pass = false
                      )          
    :( // validated             
    
    let( aPQR= angle(pqr)
       , iscoline = isequal(aPQR,180) || isequal(aPQR,0)
       
       , tilt = // if tilt not given, set to either
                  //    abi (when no rot) or 
                  //    90  (when rot is set)
                  isnum(tiltforward)? undef
                  : isnum(tilt)? tilt
                  : rot? 90: aPQR/2 
                  
       , P = pqr[0], Q = pqr[1], R = pqr[2]
       , PQ = p01(pqr)
       , QR = p12(pqr)
       , boneseg = tiltforward? [Q,R]:[P,Q]
       
       , Rf = Rf?Rf: iscoline? randOfflinePt( PQ )
                           : tiltforward?P:R 
                           
       , PQRf = [pqr[0], pqr[1], Rf ] // For iscoline
       
       , A_af_rot = rot? rotPt( Rf, boneseg, rot ): Rf
      
       , A_af_tilt= tilt!=undef? anglePt( [P,Q,A_af_rot], a=tilt )
                    : tiltforward? 
                       anglePt( [R,Q,A_af_rot]
                              , a= tiltforward
                              , Rf=N(pqr) )            
                    : A_af_rot 
                    
       , posttilt_axisP= anglePt([tilt==0?Rf:A_af_tilt,Q,P],a=90)
       
       , A_af_rot2= posttilt_rot?
                    rotPt( A_af_tilt
                         , [posttilt_axisP,Q], posttilt_rot )
                    : A_af_tilt
                    
       , A = A_af_rot2
       
       , B = N( [posttilt_axisP,Q,A]
              , len= ( (tiltforward>aPQR)?-1
                       : tilt==0? (angle(PQRf)>90?-1:1)
                       :1
                     )*len)
       , C = onlinePt( [Q,B], len= -len) 
       , pl= [A,B,C]

       )
    [ "pl", pl
    , "<b>Args</b>"," ==> " 
    , "pqr", pqr
    , "Rf",Rf
    , "rot",rot
    , "tilt",tilt
    , "tiltforward",tiltforward
    , "posttilt_rot", posttilt_rot
    , "<b>Calculated</b>"," ==> "
    , "aPQR", aPQR
    , "iscoline", iscoline
    , "A_af_rot", A_af_rot
    , "A_af_tilt", A_af_tilt
    , "posttilt_axisP", posttilt_axisP
    , "A_af_rot2",A_af_rot2
    ]
    )//___validated

);

    function planeDataAt_testing_get_gots( pldata )= 
    (   //======================================== 
        // Internal use for planeDataAtQ_testing
        //========================================
        let( 
             pts = hash( pldata, "pts") 
           , closed= hash( pldata, "closed")
           , i   = hash( pldata, "i")   
           , abc = hash( pldata, "pl")
           , lasti= len(pts)-1
           , pqr = get3(pts, (i==0?1:i==lasti?-2:i)-1)
           , Rf  = hash( pldata, "Rf", pts[0] )
           , P=pqr[0], Q=pqr[1], R=pqr[2]
           , A=abc[0], B=abc[1], C=abc[2]
           )

        i==0 ? 
        let  ( RfPQ= [ und(Rf,R),P,Q] 
             , N= N(RfPQ) 
             , Aj_pqr= projPt( Rf?RfPQ:pqr,A)
             , Bi = angleBisectPt( get(pts,[-1,0, 1]) )
             , abipl= [ P,Bi, N([Q,P,Bi])]             
             , gots= ["aQPA", angle([Q,P,A])
                     ,"aQPB", angle([Q,P,B])
                     ,"aRQA", angle([R,Q,A])
                     ,"aBPN", angle([B,P,N])
    //                 ,"aNPBj_pqr", angle([N,P, projPt(Rf?RfPQ:pqr,B])
    //                 ,"aNPBj_pqr", angle([N,P, projPt(Rf?RfPQ:pqr,B])
                     ,"aPQAj_pqr", angle([P,Q,Aj_pqr])
                     ,"aRQAj_pqr", angle([R,Q,Aj_pqr])
                     ,"A_rotaway_pqr", angle([Aj_pqr,P,A]) //, Rf=P)
                     ,"pl_on_abipl", all([for(p=abc) isOnPlane( abipl, p)])
                     ]

             ) gots
        :i==lasti ? 
        let  ( QRRf= [ Q,R, und(Rf,R)] 
             , N= N(QRRf) 
             , Aj_pqr= projPt( Rf?QRRf:pqr,A)
             , Bi = angleBisectPt( get3(pts,-2) ) //get(pts,[-1,0, 1]) )
             , abipl= [ R,Bi, N([Q,R,Bi])]             
             , gots= ["aQRA", angle([Q,R,A])
                     ,"aQRB", angle([Q,R,B])
                     ,"aRQA", angle([R,Q,A])
                     ,"aBRN", angle([B,R,N])
    //                 ,"aNPBj_pqr", angle([N,P, projPt(Rf?RfPQ:pqr,B])
    //                 ,"aNPBj_pqr", angle([N,P, projPt(Rf?RfPQ:pqr,B])
                    // ,"aPQAj_pqr", angle([P,Q,Aj_pqr])
                     ,"aRQAj_pqr", angle([R,Q,Aj_pqr])
                     ,"A_rotaway_pqr", angle([Aj_pqr,P,A]) //, Rf=P)
                     
                     ,"pl_on_abipl", all([for(p=abc) isOnPlane( abipl, p)])
                     ]

             ) gots
       : undef
       /* This section doesn't seem to be needed, 'cos it's in the 
          region between i=0 and i=lasti, so when testing, simply
          treat get3(pts,i-1) as pqr then use planeDataAtQ_testing()
          instead.
          let( pqr = get3( pts, (i==0?1:i==len(pts)-1?-2:i)-1)
             , P=pqr[0], Q=pqr[1], R=pqr[2]
             , PQRf= [pqr[0],pqr[1],Rf]
             , N=Rf?N(PQRf):N(pqr), 
             , A=abc[0], B=abc[1], C=abc[2]
             , Aj_pqr= projPt(Rf?PQRf:pqr,A)
             , Bi = angleBisectPt(pqr)
             , abipl= [Q,Bi, N([P,Q,Bi])]             
             , gots= [ "aPQR", angle(pqr)
                     ,"aPQA", angle([P,Q,A])
                     ,"aRQA", angle([R,Q,A])
                     ,"aBQN", angle([B,Q,N])
                     ,"aQPB", angle([Q,B,P])
                     ,"aPQAj_pqr", angle([P,Q,Aj_pqr])
                     ,"aRQAj_pqr", angle([R,Q,Aj_pqr])
                     ,"A_rotaway_pqr", angle([Aj_pqr,Q,A], Rf=P)
                     ,"pl_on_abipl", all([for(p=abc) isOnPlane( abipl, p)])
                     ]
             ) gots
        */
    );

} // planeDataAtQ

  
    module planeDataAt_testing_Draw( pldata, color){   
        echom("planeDataAt_testing_Draw");
        
        echo( pldata = pldata );
        pts = hash( pldata, "pts");
        abc = hash( pldata, "pl");
        i   = hash( pldata, "i");
        lasti= len(pts)-1;
        pqr = get3( pts, i==0?1:i==len(pts)-1?-2:i);
        Rf  = hash( pldata, "Rf");
        closed = hash( pldata, "closed");
        
        MarkPts( pts, "ch=[r,0.05]");
        if(closed) {
            MarkPts( get(pts,[0,-1]), "ch=[r,0.006,color,green];ball=0");
            MarkPts( pqr, "ball=less;l=PQR");
        } else {
            MarkPts( pqr, "ball=less;pl=[transp,0.1];l=PQR");
            }
        MarkPts(abc, ["l=ABC","plane",["color",color]]);
        
        P=pqr[0]; Q=pqr[1]; R=pqr[2];
        PQRf= [ pqr[0],pqr[1],Rf ];
        QRRf= [ pqr[1],pqr[2],Rf ];    
        RfPQ= [ Rf,pqr[0],pqr[1]];
        N= Rf?(i==lasti? N(QRRf):N(RfPQ))
             :N(pqr); 
        MarkPt(N,"l=N;cl=yellow");
        //echo(Rf=Rf);
        if(Rf) {
            MarkPt(Rf,"l=Rf;cl=darkcyan");
            MarkPts([Rf,Q], "ch;ball=false");
        }    
        A= abc[0]; B=abc[1]; C=abc[2];
        Aj_pq= projPt([P,Q],A);
        Aj_qr= projPt([R,Q],A);
        Aj_pqr= projPt(Rf?(i==lasti?QRRf:PQRf):pqr,A);
        
        Bi = angleBisectPt(pqr);
        abipl= [Q,Bi, N([P,Q,Bi])];             
        
        Mark90( [Q,P,B] );
        Mark90( [A,Q,P] );
        Mark90( [B,Q,P] );
        
        if( i==lasti ){
           //MarkPt( Aj_pqr, "l=AjPQR"); 
           Mark90( [Q,R,A] );
           Mark90( [Q,R,B] );
           Mark90( [A, Aj_pqr,Q],"ch");
           Mark90( [A, Aj_qr,Q],"ch");
           Mark90( [A, Aj_pqr,R],"ch");
           Mark90( [A, Aj_qr,R],"ch");
        }
        else{
            Mark90( [A,Q,R] );
            Mark90( [B,Q,R] );
            Mark90( [A, Aj_pqr,Q],"ch");
            Mark90( [A, Aj_pq,Q],"ch");
        }
        //echo( A_Aj_pqr_Q=  [A, Aj_pqr,Q] );
    }  

module planeDataAt_testing( label, settings, isdetails, isdraw ){
       echom(str(
    "planeDataAt_testing(<b>\"", label,"\"</b>,settings=[...]"));
    
        /*
        Usage: see planeDataAt_demo_at_0()
    
        settings=[ setting_1, setting_2 ...]
        
        Each setting_i is a hash containg *wants* :

         setting42= let(pqr= get3(pts42,i-1), apqr = angle(pqr)) 
                ["label","42: tiltforward=30"
                ,"color","blue"
                ,"pqr", pqr
                ,"pldata", pl42
                ,"wants", [
                      "aRQA", 30
                     //,"aRQA", 
                     ,"aBQN", 0
                     ,"aRQAj_pqr", 30
                     //,"aRQAj_pqr", 0
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
                ];       
       
        and is for one arg_setting. Output looks something like:
        
            ECHO: "2: tilt=90"            //<======= a setting
            ECHO: "__| aPQA== 90 ? pass"  //<------- a test
            ECHO: "__| aBQN== 0 ? pass"
            ECHO: "__| aPQAj_pqr== 90 ? pass"
            ECHO: "__| A_rotaway_pqr== 0 ? pass"
            ECHO: "__| pl_on_abipl== false ? pass"
            
            ECHO: "3: tiltforward=90"     //<======= a setting
            ECHO: "__| aRQA== 90 ? pass"  //<------- a test
            ECHO: "__| aBQN== 0 ? pass"
            ECHO: "__| aRQAj_pqr== 90 ? pass"
            ECHO: "__| A_rotaway_pqr== 0 ? pass"
            ECHO: "__| pl_on_abipl== false ? pass"
        */
 
//        echo( setting = settings[0] );
        //echo( pldata= hash(settings[0], "pldata"));
        
        if(settings)
        {    
        results = [ 
           for( setting=settings )
              let( pldata= hash( setting, "pldata")
                 , pts = hash( pldata, "pts") 
                 , i   = hash( pldata, "i")   
                 , abc = hash( pldata, "abc")
                 , lasti= len(pts)-1
                 , pqr = get3(pts, (i==0?1:i==lasti?-2:i)-1)
                 , Rf  = hash( setting, "Rf" )
                 , wants= hash( setting, "wants")
                 , wantvars= keys(wants)
                 , gots= planeDataAt_testing_get_gots( pldata ) 
                    //pts,i, abc, Rf )
                 
                 , res = [ for( wantvar=wantvars )
                            let( want = hash( wants, wantvar )
                               , got  = hash( gots, wantvar )
                               , fail = isequal( want, got )?0:1
                               , msg= str( "__|<b>"
                                           , fail?_red(wantvar)
                                                 : wantvar
                                           , "</b>== "
                                         , want, " ? "
                                         , fail? _red( got )
                                               : _green("pass")
                                         )
                               )
                            [ fail, msg ]
                         ]    
                 , test_count_this_setting= len(wantvars)
                 , test_fail_this_setting= sum( [for(re=res) re[0]] )
                 , label = _color( hash( setting, "label")
                                  , hash( setting, "color" ) 
                                  )
                 , msg = str( label,"<br/>"
                            , join( [for(re=res)re[1]],"<br/>")
                            )
                 )//..let
                 [ test_count_this_setting
                 , test_fail_this_setting, msg ]
        ]; //.. result
                            

       for( i=range(settings) ){
            setting= settings[i];
            if(isdraw || hash(setting,"isdraw")){                 
                planeDataAt_testing_Draw( 
                    pldata = hash( setting, "pldata")
                   ,color= hash( setting, "color")
                );
            }    
            if(isdetails || hash(setting,"isdetails"))
                echo( results[i][2] );
       }  
           
        //echo("<br/>");
        nTests = sum( [for(cfm= results ) cfm[0] ] );
        nFails = sum( [for(cfm= results ) cfm[1] ] ); 
        echo( str( 
            "<span style='font-size:14px;background:lightyellow'><u><b>"
            , label, " </u></b> "
        
             , _bcolor(str(
                    "<u>--- tests "
                    ," (in ", len(settings), " settings) : "
                    , _blue(nTests)
                    , ", failed: ", _color( nFails, nFails?"red":"green")
                    , "</u>&nbsp;</span>&nbsp;"
               ),"lightyellow") 
             , " <br/>(settings: "
                , join([ for(setting=settings)  
                   _color( hash(setting, "label"), hash( setting, "color"))
                 ],", ")
             , ")"
             )
            );

        } //..if(settings)
        
    }//..testing


function planeDataAtQ_testing_get_gots( pqr, abc, Rf )=
(   //======================================== 
    // Internal use for planeDataAtQ_testing
    //   abc = plane generated by planeDataAtQ
    // Rf is for situation that pqr is coln
    //======================================== 
    let  ( P=pqr[0], Q=pqr[1], R=pqr[2]
         , PQRf= [pqr[0],pqr[1],Rf]
         , N=Rf?N(PQRf):N(pqr), 
         , A=abc[0], B=abc[1], C=abc[2]
         , Aj_pqr= projPt(Rf?PQRf:pqr,A)
         , Bi = angleBisectPt(pqr)
         , abipl= [Q,Bi, N([P,Q,Bi])]             
         , gots= [ "aPQR", angle(pqr)
                 ,"aPQA", angle([P,Q,A])
                 ,"aRQA", angle([R,Q,A])
                 ,"aBQN", angle([B,Q,N])
                 ,"aPQAj_pqr", angle([P,Q,Aj_pqr])
                 ,"aRQAj_pqr", angle([R,Q,Aj_pqr])
                 ,"A_rotaway_pqr", angle([Aj_pqr,Q,A], Rf=P)
                 ,"pl_on_abipl", all([for(p=abc) isOnPlane( abipl, p)])
                 ]

         ) gots
    //[]
);
                 
module planeDataAtQ_testing_Draw( pqr, abc, color,Rf, pts, i ){   
    
    pqr = und(pts,pqr);
    MarkPts( pqr, "ch=[r,0.05];l=PQR");
    
    MarkPts(abc, ["l=ABC","plane",["color",color]]);
    
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
    Rf = und(Rf,R); 
    PQRf= [ pqr[0],pqr[1],Rf ];
    RfPQ= [ Rf,pqr[0],pqr[1]];
    N= pts?N(RfPQ):N(PQRf); //
    N= Rf?N(pts?RfPQ:PQRf):N(pqr); 
    MarkPt(N,"l=N;cl=yellow");
    //echo(Rf=Rf);
    if(Rf) {
        MarkPt(Rf,"l=Rf;cl=darkcyan");
        MarkPts([Rf,Q], "ch;ball=false");
    }    
    A= abc[0]; B=abc[1]; C=abc[2];
    Aj_pq= projPt([P,Q],A);
    //Aj_pqr= projPt(pqr,A);
    Aj_pqr= projPt(Rf?PQRf:pqr,A);
    Bi = angleBisectPt(pqr);
    abipl= [Q,Bi, N([P,Q,Bi])];             
    
    Mark90( [Q,P,B] );
    Mark90( [A,Q,P] );
    Mark90( [B,Q,P] );
    Mark90( [A,Q,R] );
    Mark90( [B,Q,R] );
    Mark90( [A, Aj_pqr,Q],"ch");
    Mark90( [A, Aj_pq,Q],"ch");
    //echo( A_Aj_pqr_Q=  [A, Aj_pqr,Q] );
}       
module planeDataAtQ_testing( label, settings, isdetails, isdraw ){
       echom(str("planeDataAtQ_testing(<b>\"", label,"\"</b>,tests=[...]"));
    
        /*
        Usage: see planeDataAt_demo_at_0()
    
        settings=[ setting_1, setting_2 ...]
        
        Each setting_i is a hash containg *wants* :

         setting42= let(pqr= get3(pts42,i-1), apqr = angle(pqr)) 
                ["label","42: tiltforward=30"
                ,"color","blue"
                ,"pqr", pqr
                ,"pldata", pl42
                ,"wants", [
                      "aRQA", 30
                     //,"aRQA", 
                     ,"aBQN", 0
                     ,"aRQAj_pqr", 30
                     //,"aRQAj_pqr", 0
                     ,"A_rotaway_pqr", 0
                     ,"pl_on_abipl", false
                     ]
                ];       
       
        and is for one arg_setting. Output looks something like:
        
            ECHO: "2: tilt=90"            //<======= a setting
            ECHO: "__| aPQA== 90 ? pass"  //<------- a test
            ECHO: "__| aBQN== 0 ? pass"
            ECHO: "__| aPQAj_pqr== 90 ? pass"
            ECHO: "__| A_rotaway_pqr== 0 ? pass"
            ECHO: "__| pl_on_abipl== false ? pass"
            
            ECHO: "3: tiltforward=90"     //<======= a setting
            ECHO: "__| aRQA== 90 ? pass"  //<------- a test
            ECHO: "__| aBQN== 0 ? pass"
            ECHO: "__| aRQAj_pqr== 90 ? pass"
            ECHO: "__| A_rotaway_pqr== 0 ? pass"
            ECHO: "__| pl_on_abipl== false ? pass"
        */
 
        //echo( setting = settings[0] );
        
        if(settings)
        {    
        results = [ 
           for( setting=settings )
              let( pqr = hash( setting, "pqr") // for testing planeDataAtQ
           
                 , pts = hash( setting, "pts") // for testing planeDataAt
                 , i   = hash( setting, "i")   // for testing planeDataAt
           
                 , Rf  = hash( setting, "Rf" )
                 , abc = hash( setting, "abc")
                 , wants= hash( setting, "wants")
                 , wantvars= keys(wants)
                 , gots= planeDataAtQ_testing_get_gots( pqr, abc, Rf, pts,i )
                 //, gots= planeDataAtQ_testing_get_gots( 
                 //         pts?get3(pts,i-1):pqr, abc, Rf) //, pts,i )
                 
                 , res = [ for( wantvar=wantvars )
                            let( want = hash( wants, wantvar )
                               , got  = hash( gots, wantvar )
                               , fail = isequal( want, got )?0:1
                               , msg= str( "__|<b>"
                                           , fail?_red(wantvar)
                                                 : wantvar
                                           , "</b>== "
                                         , want, " ? "
                                         , fail? _red( got )
                                               : _green("pass")
                                         )
                               )
                            [ fail, msg ]
                         ]    
                 , test_count_this_setting= len(wantvars)
                 , test_fail_this_setting= sum( [for(re=res) re[0]] )
                 , label = _color( hash( setting, "label")
                                  , hash( setting, "color" ) 
                                  )
                 , msg = str( label,"<br/>"
                            , join( [for(re=res)re[1]],"<br/>")
                            )
                 )//..let
                 [ test_count_this_setting
                 , test_fail_this_setting, msg ]
        ]; //.. result
                            

       for( i=range(settings) ){
            setting= settings[i];
            if(isdraw || hash(setting,"isdraw"))
                planeDataAtQ_testing_Draw( 
                    pqr = hash( setting, "pqr")
                   ,abc= hash( setting, "abc")
                   ,color= hash( setting, "color")
                   , Rf= hash( setting, "Rf" )
                   , pts= hash( setting, "pts")
                );
            if(isdetails || hash(setting,"isdetails"))
                echo( results[i][2] );
       }  
           
        //echo("<br/>");
        nTests = sum( [for(cfm= results ) cfm[0] ] );
        nFails = sum( [for(cfm= results ) cfm[1] ] ); 
        echo( str( 
            "<span style='font-size:14px;background:lightyellow'><u><b>"
            , label, " </u></b> "
        
             , _bcolor(str(
                    "<u>--- tests "
                    ," (in ", len(settings), " settings) : "
                    , _blue(nTests)
                    , ", failed: ", _color( nFails, nFails?"red":"green")
                    , "</u>&nbsp;</span>&nbsp;"
               ),"lightyellow") 
             , " <br/>(settings: "
                , join([ for(setting=settings)  
                   _color( hash(setting, "label"), hash( setting, "color"))
                 ],", ")
             , ")"
             )
            );

        } //..if(settings)
        
    }//..testing

//======================================
function dualMassPts(pts, div)=
(
  /*         
     For pts on a plane (=x), 
     
         x  x|   x     
        x x  |     x
          A  |  x B x 
         x   |   x
  
     return [A,B] represents the gravity (centroid pts) of
     the pts on the left and right, respectively
  
  */
  
  3
  

);


//========================================================
{ // plat     
function plat(pts,i)= // plane at i that is 90deg to axis before i
(
    let( Rf = randPt()
       , Ni = i==0? N( [pts[1],pts[0],Rf] )
              : N( [pts[0],pts[1],Rf] ) 
       , Mi = i==0? N2( [pts[1],pts[0],Rf] )
              : N2( [pts[0],pts[1],Rf] ) 
       )
    [pts[i],Ni,Mi]
);
} // plat


function platonicPts(type, r, dir, site, actions, debug=0, opt=[])= 
(  // 2018.5.25
   // https://en.wikipedia.org/wiki/Platonic_solid
   let( type = arg(type,opt,"type","tetra") // tetra, octa, dodeca, icosa
      , r = arg(r,opt,"r",2)                // dist from center to one corner
      , dir = arg(dir,opt,"dir",1)          // 1|2. See wiki
      , site = arg(site, opt, "site", []) 
      , actions = argx(actions,opt,"actions",[])  
      ) 
   type=="icosa"? // https://en.wikipedia.org/wiki/Regular_icosahedron
                  //  el* sin(2*PI/5) = r 
   (
     let( el = r/ sin(72)  // edge length 
        , a  = anglebylen( r,el,r )
        , P0 = [r,0,0]
        , P1 = anglePt( ORIGIN_SITE, a=a, len=r )
        , P1j= projPt(P1, [ORIGIN,P0]) 
        , distj =  dist([P1j,P1])
        , nP1j= addx( P1j, -2*P1j.x)
        
        , P2345= [ for(i=[1:4]) 
                    anglePt( [P1,P1j,addz(P1j,1)], a= 360/5*i        
                           , len= distj ) ]
        , P6_11= [ for(i=[1:5]) 
                     anglePt( [P0, nP1j, addy(nP1j,1)]
                              , a2= 360/5*i-180/5-360/5
                               , len= distj) ]            
        , pts = concat( [P0,P1], P2345, P6_11, [ addx(P0, -2*P0.x) ] )
        , faces= [ [0,2,1],[0,3,2],[0,4,3],[0,5,4],[0,1,5]
                 , [1,2,7],[2,3,8],[3,4,9],[4,5,10],[5,6,1]
                 , [1,7,6],[2,8,7],[3,9,8],[4,10,9],[5,6,10]
                 , [6,7,11],[7,8,11],[8,9,11],[9,10,11],[10,6,11]
                 ]
        ) 
     debug?
       echo(r=r)
       echo(el=el)
       echo(a=a)
       echo(P0=P0)
       echo(P1=P1)
       echo(P1j=P1j)
       echo(nP1j=nP1j)
       echo(P2345=P2345)
       [pts,faces]    
     : [pts,faces]
   )       
   :3 //type=="tetra"?
      
);

//function popPtFaces( pts, faces, template, site, dist )=
function popPF( pts,faces
               , template=2
               , dest=[4,5,6]
               , offset=undef
               , depth )=
(
  /* Carve a hole (defined by a 2d template) on the dest of an obj 
     defined by pts and faces.
      
     Return [newpts, newfaces]
     
     template: a collection of 2D pts arranged CCW. Could be defined as:
     
       (1) number: Size of a square
       (2) [x,y]: Size of a rectangular hole. 
       (3) 2D pts: An array of 2d pts on XY plane (counter CW)
                   
     dest: [i,j,k] where i,j,k are indices on a face, like [4,5,6]

                   6-----------7
                .'          .' |
              .'          .'   |
             5----------4'     |
             |          |      3
             |          |    .'
             |          | .'
             1----------0'      
           
           In this case (dest= [4,5,6]), the ORIGIN of template will be 
           anchored on the pts[5]:
     
             y |                   6--------------+
               |                   |              |
               |  +.               |  +.          |
               |  | '.       ===>  |  | '.        |
               |  |   '.           |  |   '.      |
               |  +-----+          |  +-----+     |
               O-------------      5--------------4
                         x

     offset: default: undef, num, or [a,b]
     
        If undef, template is centered
        If num, [num,num] 
        if [a,b], then: 
        
            if template is a num or [x,y], then the square/rectangular 
            is placed with [a,b] as the low-bottom pt on the target face:       
               
               6---------------7
               |               |
               |   +-----+     |
               |   |     |     |
            -- + - +-----+     |
             b |   :           |
            -- 5---+-----------4
               | a |
               
            if template is a list of pts, then the template be placed
            by adding offset to them:
     
             y |                 6--------------+
               |                 |              |
               |   +.            |      +.      |
               |   | '.    ===>  |      | '.    |
               |   |   '.        |      |   '.  |
               |   +-----+       |      +-----+ |
               O-----------      5--------------4
               | x |             | x+ a | 
                                 offset = [a,0] 
     
       if [a,undef] ==> auto centered on y side
       if [undef,b] ==> auto centered on x side
       
    To do:
     
    dirline= p2 = a pair of pts showing the direction of cutting, 
           like [ pts[5], pts[1] ]
           If not given, cut alone the normal line of the dest
           
    depth: how deep down. If not given, cut through. 
        
   */ 
   /*  let target site= [P,Q,R ] 
   
            R---------S
          .'        .'|
         Q--------P'  |
         |        |   3
         |        | .'
         1--------0'      
   */
   let( P=pts[dest[0]], Q=pts[dest[1]], R=pts[dest[2]]
      , S = P-Q+R          
      , C = (P+Q+R+S)/4
      , dPQ = dist(P,Q)
      , dQR = dist(Q,R)
      , offset= isnum(offset)?[offset,offset]:offset
      , tmp=  isnum(template)?
              let( offx= offset? und(offset[0], (dPQ-template)/2 ) 
                               : (dPQ-template)/2
                 , offy= offset? und( offset[1], (dQR-template)/2 ) 
                                : (dQR-template)/2 )  
              [ [ offx+template, offy,0]
              , [ offx+template, offy+template,0]
              , [ offx, offy+template,0]
              , [ offx, offy,0]
              ]
           : len(template)==2?
                let( offx= offset?offset[0]: (dPQ-template[0])/2
                   , offy= offset?offset[1]: (dQR-template[1])/2 )  
              [ [ offx+template[0], offy,0]
              , [ offx+template[0], offy+template[1],0]
              , [ offx, offy+template[1],0]
              , [ offx, offy,0]
              ]
           : to3d( offset==[0,0,0]? template
                   : offset? [for(p= template) p+offset ]
                   : // centering when offset==undef
                     let( tmp_C= sum(template)/len(template) 
                        , offxy= [dPQ/2,dQR/2]- tmp_C
                        )
                     [for(p=template) p+offxy ]
                 )   
      , topHolePts = movePts( tmp
                            , from= ORIGIN_SITE
                            , to= [P,Q,R] 
                            )
      , hEdgefis = get_edgefis(faces)
                     // h{edge:hFis}:   
                     // [ [2,3], ["L",3,"R",6,"F",[4], "B",[0,1]] 
                     // , ...
                     // ]
      , fi= hash(edgefis_get( hEdgefis
                         , [dest[0],dest[1]] 
                         ), "R" )
      , nearfi= hash(edgefis_get( hEdgefis
                         , [dest[0],dest[1]] 
                         ), "L" )
      , normalPi = prev( faces[nearfi], dest[1]  )
      , oppofi = get_near_fi(faces, nearfi
                            , dels( faces[nearfi], dest) 
                            )
      , normalEdge = [ dest[1], normalPt ]  
      , botHolePts= /// NOTE: this is for cube only. Other shapes will likely fail 
                    [ for(p=topHolePts) 
                      p+ pts[ normalPi ]- Q
                    ] 
      , newPts = concat( topHolePts, botHolePts)
      , finalPts = concat( pts, newPts )      
      
      , oldfis = dels( range(faces), [fi,oppofi] )                   
      , oldfaces = [for(fj=oldfis) 
                     faces[fj]] 
                   
      , newsidefaces= [ for(i=range(tmp))
                        [ len(pts)+ i
                        , len(pts)+ (i==0? (len(tmp)-1): i-1)
                        , len(pts)+ (i==0? (len(tmp)-1): i-1)+len(tmp) 
                        , len(pts)+ i+ len(tmp)
                        ]  
                      ]
      , topfaces = concat( faces[fi], faces[fi][0], range(8,8+len(tmp)), 8 )
      , botfaces = concat( faces[oppofi], faces[oppofi][0]
                          , range( len(finalPts)-1
                                 , len(finalPts)-1-len(tmp)
                                 ), len(finalPts)-1 )
      , finalfaces= concat( 
            [ for( fj=range(faces) )
              if((fj!=fi)&&(fj!=oppofi)) faces[fj]
            ] 
          , newsidefaces
          , [topfaces]  
          , [botfaces]   
          )                        
      )  
   //echo(pts=pts)
   //echo(faces=faces)   
   //echo(template=template)
   //echo(offset=offset)
   //echo(tmp=tmp)
   //echo(dest=dest)
   //echo(C=C)
   //echo(P=P,Q=Q,R=R,S=S,C=C)
   //echo( topHolePts = topHolePts )
   //echo(hClockedEFs = hClockedEFs)
   //echo(hEdgefis=hEdgefis)
   //echo(edges_in_hEdgefis = keys(hEdgefis))
   //echo(normalPi = normalPi)
   //echo( botHolePts = botHolePts ) 
   //echo(fi=fi, nearfi=nearfi, oppofi = oppofi )
   //echo(oldfis=oldfis)
   //echo(oldfaces=oldfaces)
   //echo(newsidefaces = newsidefaces)
   //echo(finalfaces=finalfaces)
   [finalPts, finalfaces]
);



function precPts(pts,d=2)=
(
  isnum(pts[0])? [for(n=pts) round(n* pow(10,d))/pow(10,d) ]      
  : [ for(p=pts) [for(n=p) round(n* pow(10,d))/pow(10,d) ] ]
);

    precPts=["precPts","pts, d=2","arr","Geometry"
    ,"Return pts with adjusted precision d (default=2).
    ;; pts could be a pt, or pts. 
    "];

    function precPts_test( mode=MODE, opt=[] )=
    (
        doctest( precPts,
        [
          [precPts( [1.2345,-0.9876, 2.0012] )
                  , [1.23, -0.99, 2]
                  , [1.2345,-0.9876, 2.0012]] 
       ,  [precPts( [[1.2345,-0.9876, 2.0012],[1.2345,-0.9876, 2.0012]] )
                  , [[1.23, -0.99, 2],[1.23, -0.99, 2]]
                  , [[1.2345,-0.9876, 2.0012],[1.2345,-0.9876, 2.0012]]
          ] 
        ]
        , mode=mode, opt=opt
        )
    );    

//========================================================
{ // projPl
function projPl(pl1,pl2,along,newr,twist)=
(
        /*
           P = pts[i-1]
           Q = pts[i]
           pl_APB = lastcut 
           
                  .B=pxi
                 /| r
           -----P-J-------------Q
               /               pts[_i]
              A   
                   
          1) Change r according to r    
          2) If rot, rotate B about PQ to its new location
          3) Project to pl_i
        */
        
        [ for( xi = range( pl1 ) )
          let( prePxi= pl1[xi]
             , P = along[0]
             , Q = along[1]
             , prePxi_r_adj = newr? // Go thru r-adjust only when newr is given
                        let( J=projPt( prePxi, along ))
                        onlinePt( [J,prePxi], newr )
                        : prePxi
             , prePxi_rot= twist? 
                           rotPt( prePxi_r_adj, along, twist )
                          : prePxi_r_adj
             )
             projPt( prePxi_rot, pl2, along )
        ]
);        
    projPl=["projPl","pl1,pl2,along,newr,twist","arr","Geometry"
    ,"Project plane pl1 onto pl2, with the possibilities of assigning
    ;; twist and new r, which is the distance between pts_on_pl1 and
    ;; their projection on line *along*.
    ;; 
    ;; along: [P,Q] where P,Q is center of pl1, pl2, resp. 
    ;;
    ;; Note: This mainly for internal use of chaindata. 
    ;;       Similar to projPts() but has limitation(i.e., *along*
    ;;       is limited to loci of pl1 and pl2) and new features
    ;;       (has newr and twist)
    "];
} // projPl


//========================================================
{ // projPt
function projPt( a,b, along )=
(    
   let( c= b==undef?a[0]:a
   , d= b==undef?a[1]:b
   , t= str(gtype(c),gtype(d))  // ptln,lnpt, plpt,ptpl, lnpl,plln 
   , v= isline(along)?(along[1]-along[0]):undef
   )
   v?
   (
       t=="ptln"? intsec( [c,c+v], d )
      :t=="lnpt"? intsec( c, [d,d+v] )
      :t=="ptpl"? intsec( [c,c+v], d )
      :t=="plpt"? intsec( c, [d,d+v] )
      :t=="lnpl"? [ intsec( [ c[0], c[0]+v ], d )
                  , intsec( [ c[1], c[1]+v ], d ) ]
      :t=="plln"? [ intsec( c, [ d[0], d[0]+v ] )
                  , intsec( c, [ d[1], d[1]+v ] ) ] 
      : undef      
   
   ):(
   
       t=="ptln"? othoPt( [d[0],c,d[1]] )
      :t=="lnpt"? othoPt( [c[0],d,c[1]] )
      :t=="ptpl"? c- dot( c-d[1], (normal(d)/norm(normal(d)))
                        )*(normal(d)/norm(normal(d)))
      :t=="plpt"? d- dot( d-c[1], (normal(c)/norm(normal(c)))
                        )*(normal(c)/norm(normal(c)))
      :t=="lnpl"? [projPt( c[0], d), projPt( c[1], d) ]
      :t=="plln"? [projPt( d[0], c), projPt( d[1], c) ]
      : undef
  
  ));
  
	/*    http://stackoverflow.com/questions/8942950/how-do-i-find-the-orthogonal-projection-of-a-point-onto-a-plane

    The projection of a point q = (x, y, z) onto a plane 
       given by a point p = (a, b, c) and a normal n = (d, e, f) is

    q_proj = q - dot(q - p, n) * n

	n = (normal(pqr)/norm(normal(pqr)))
	
	pt - dot(pt-pqr[1], (normal(pqr)/norm(normal(pqr))))*(normal(pqr)/norm(normal(pqr)))
   */ 
   projPt=["projPt", "a,b", "point", "Point",
    "Return the point where a projects on b or b projects on a.
    ;; a, b could be: pt, line or plane. They can be entered
    ;; in either projPt(a,b) or projPt( [a,b] ) form. 
    ;;
    "];

    function projPt_test( mode=MODE, opt=[] )=(
        doctest( projPt,
        []
        ,mode=mode, opt=opt )
    );   
} // projPt

//========================================================
{ // projPts
function projPts( pts,target, along )=  // pts proj to target 
(
   [for(p=pts) projPt(p,target,along) ]   
);
} // projPts


//function ptsHandler( pts, cubeSize, center, actions, site, opt=[] )=
//(           
//  let( 
//   _pts = arg( pts, opt, "pts", [])
//   , _cubeSize = arg(cubeSize, opt, "cubeSize", [1,1,1])
//   , cubeSize = _pts? []
//                : isnum(_cubeSize)  
//                   ? [_cubeSize,_cubeSize,_cubeSize]
//                   : _cubeSize
//               
//   // Assure center is [a,b,c], which is then merged into actions              
//   , _center = arg( center, opt, "center", [] )
//   , center = isarr(_center)? _center : _center?[1,1,1]:[]
//   
//   , _actions = actions==false?[]:concat( hash(opt,"actions",[]), actions?actions:[] )           
//   , actions = center&& cubeSize
//               ? concat( ["t", [ center[0]?-cubeSize[0]/2:0
//                               , center[1]?-cubeSize[1]/2:0
//                               , center[2]?-cubeSize[2]/2:0 ]
//                         ], _actions )
//               : _actions           
//              
//   , pts0 = _pts? _pts
//	     :[ [cubeSize.x,0,0], ORIGIN
//              , [0,cubeSize.y,0],[cubeSize.x, cubeSize.y,0] 
//              , [cubeSize.x,0,cubeSize.z], [0,0,cubeSize.z]
//              , [0,cubeSize.y,cubeSize.z], cubeSize  
//             ]                         
//    , pts2= actions? transPts(pts0, actions=actions): pts0 
//    
//    , site = arg(site, opt, "site",[])
//    , from = hash(opt, "from", ORIGIN_SITE)
//    , pts = site? movePts( pts2, from=from
//                               , to=site ): pts2
//   
//  ) // let
//  // Return a hash including not only the pts but those data needed 
//  // to get to these pts, for the purpose that if this function is called
//  // to get the "pts" of a solid cube, these data is needed to re-position
//  // that solid cube into new position. 
//  [ "pts", pts
//  , "cubeSize", cubeSize
//  , "center", center
//  , "actions", actions
//  , "site", site
//  ]
//  
//);


//. q,r
//========================================================
{ // quadrant
//function quadrant(pt, Rf=O, isinclude=1)= 
//(
//  //(pt-Rf).x>0?
//    //((pt-Rf).y>0? 1: 4)
//  // : ((pt-Rf).y>0? 2: 3)
//  let(d=pt-Rf)
//  isinclude==1?
//  (  d.x >0  && d.y>=0? 0
//    :d.x <=0 && d.y>0? 1
//    :d.x <0 && d.y<=0? 2
//    :( isequal(d,[0,0]) || isequal(d,[0,0,0])) ? 0
//    :3
//   )  
//  :isinclude==0? 
//   ( d.x >0  && d.y>0? 0
//    :d.x <0 && d.y>0? 1
//    :d.x <0 && d.y<0? 2
//    :d.x >0 && d.y<0? 3
//    : undef
//   )
//   :( d.x==0 ?
//       (d.y>0?Y:-1*Y)
//      : d.y==0 ?
//       (d.x>0?X:-1*X)
//      : O 
//    )  
//);

     
    
    function quadrant_test( mode=MODE, opt=[] )=
    (
        doctest( quadrant,
        mode=mode, opt=opt
        )
    );  

function quadrant(pt, site=[X,O,Y], includes=undef)=
(
   /*
      Return quadrant of pt on a coord system defined by pqr
      
                 R
        1       /     0
         -+    /   ++ 
              /
      - - -  Q---------- P
      2     :       
       --  :    +-   3
          :   
     
    We convert pt to iP+jQ where [i,j]= ij_D = the
    scalars of pt in the coord system {P-Q, R-Q}
     
    2D:  
         [i,j]_D*pqr = [a,b]_C
         ij = solve2( transpose(pq), ab) 
    
    3D: 
	[i,j,k]_D*pqr = [a,b,c]_C
        ijk = solve3( transpose(pqr), abc) 
               
    includes: if given, a 4-item str or array, each item is 0~3, 
              determining how a pt falls onto one of the 4 arms 
              to be categorized.
              
       true or "0123" or [0,1,2,3]
                  
                  R               pt on:  quadrant
              1   1    0          [P,Q]      0    
                  |               [R,Q]      1
          ----2---Q----0--P       [-P,Q]     2
                  |               [-R,Q]     3 
              2   3    3
                  |
 
       "00" or [0,0] or [0,0,undef,undef] 
       
                  R               pt on:  quadrant
              1   0    0          [P,Q]      0    
                  |               [R,Q]      0
          ----x---Q----0--P       [-P,Q]     undef
                  |               [-R,Q]     undef 
              2   x    3
                  |
           This setting allows for detecting if pt falls within the PQR triangle
                      
                  
   */
    //let( P=site[0], Q=site[1], R=site[2]
       //, ij = solve2( transpose([P-Q,R-Q]), (pt-Q))
       //) ( ij[0]>0 ? ( ij[1]>0? 0:3): ij[1]>0?1:2 )
    let( ij = solve2( transpose([site[0]-site[1], site[2]-site[1]]), (pt-site[1]))
       , i=ij[0]
       , j=ij[1]
       , incl = includes==true?[0,1,2,3]:includes
       , excl = ( i>0 ? ( j>0? 0:3): j>0?1:2 )
       )
    incl?
      ( j==0? // pt on either =Q or on [P,Q] or [-P,Q] 
              (i>0? incl[0]: i<0?incl[2]:Q)
      : i==0? (j>0? incl[1]: incl[3])
      : excl      
      )  
    : excl
);
  
      quadrant=["quadrant", "pt,site", "int", "Geometry",
      " 
        Given a pt on the plane of 'site', return the quadrant of pt 
        relative to site, which is defaulted to [X,O,Y].
        
        Return 0~3.
        
        Default site: [X,O,Y]
       
             Y=[0,1,0]
             |
             |-+
             |_|___ X =[1,0,0]
            O=[0,0,0]
        
        The 'site' can be any 3 pts : [P,Q,R]
        
                   R
                 .'
               .'
              Q------ P
        
        Note-1: If the site is [X,O,Y], or a translated version of it, 
                then this func can also determine quadrant SPACE (but not
                just AREA) of a pt, including those NOT on the site plane. 
        
        
                             N   .
                             |  /|
                  1          | / |  R     0
                             |/  | /    
                .------------+------------.
                |           /|   +        |
                |          / |  /|   3    |  
            2   |         /  | / |        | 
                |        .   |/  |        |
           -----+--------|---Q------------+---- P 
                |        |  /|   '        |
                |        | / |  /         |   
                |        |/  | /          |
                |        +   |            |
                '-------/|---+------------'
                       / |  /|
                         | / |       
                         |/
                         '          
        //------------------------------------
        
    includes: if given, a 4-item str or array, each item is 0~3, 
              determining how a pt falls onto one of the 4 arms 
              to be categorized.
              
       true or \"0123\" or [0,1,2,3]
                  
                  R               pt on:  quadrant
              1   1    0          [P,Q]      0    
                  |               [R,Q]      1
          ----2---Q----0--P       [-P,Q]     2
                  |               [-R,Q]     3 
              2   3    3
                  |
 
       \"0\" or [0,0] or [0,0,undef,undef] 
       
                  R               pt on:  quadrant
              1   0    0          [P,Q]      0    
                  |               [R,Q]      0
          ----x---Q----0--P       [-P,Q]     undef
                  |               [-R,Q]     undef 
              2   x    3
                  |
           This setting allows for detecting if pt falls within the PQR triangle  
                  
      "
    , ["groupByQuardrant", "octant", "groupByOctant"]  
    ];     

    function quadrant_test( mode=MODE, opt=[] )=
    (
        doctest( quadrant,
        mode=mode, opt=opt
        )
    );
} // quadrant

function quadCentroidPts(pts, Rf=O)=
(
  [for(g = groupByQuadrant(pts, Rf)) g?centroidPt(g):undef ]
);

   quadCentroidPts=["quadCentroidPts", "pts,Rf=O", "arr", "Geometry",
      " 
        Given pts, return 4 centroidPts for pts located in
        each quadrant.
      "
    , ["quadrant", "groupByQuardrant", "octant", "groupByOctant"]  
    ];   
  
   

 
//===================================
{ // RM

function RM(a3)=
(
	a3.z==0?
	IM*RMy(a3.y) * RMx(a3.x) 
	:RMz(a3.z) * RMy(a3.y) * RMx(a3.x) 

//  No idea why the following doesn't work with [0,y,0]:
//  Note: Run demo_reverse( "On y", p0y0(randPt()), "lightgreen");
//        in rotangles_p2z_demo();
//	(a3.z==0?IM:RMz(a3.z)) * 
//	(a3.y==0?IM:RMy(a3.y)) *
//	(a3.x==0?IM:RMy(a3.x)) 
	
);
     RM=[ "RM","a3","matrix", "Geometry",
    "
    ;; Given a rotation array (a3=[ax,ay,az] containing rotation angles about
    ;; x,y,z axes), return a rotation matrix. Example:
    ;;
    ;;  p1= [2,3,6]; 
    ;;  a3=[90,45,30]; 
    ;;  p2= RM(a3) * p1;  // p2= coordinate after rotation
    ;;
    ;; In OpenScad the same rotation is carried out this way:
    ;;
    ;;   rotate( a3 )
    ;;   translate( p1 )
    ;;   sphere(r=1); 
    ;;
    ;; This rotates a point marked by a sphere from p1 to p2
    ;; but the info of p2 is lost. RM(a3) fills this hole, thus
    ;; provides a possibility of retrieving coordinates of
    ;; rotated shapes.
    ;;
    ;; The reversed rotation from p2 to p1 can be achieved by simply applying
    ;; a transpose :
    ;;
    ;;  p3 = transpose(RM(a3)) * p2 = p1
    "];
    
    function RM_test( mode=MODE, opt=[] )=
    (
        doctest(RM,[],mode=mode, opt=opt )
    );

    //RM_test( [["mode",22]] );
    //doc(RM);
    //function RM_demo()
    //{
    //	a3 = randPt(r=1)*360;
    //	echo("in RM, a3= ",a3);
    //	rm = RM(a3);
    //	echo(" rm = ", rm);
    //}
    ////RM_demo();

    //RM_test( [["mode",22]] );
    //doc(RM);

    module RM_demo()
    {
        a3 = randPt(r=1)*360;
        echo("in RM, a3= ",a3);
        rm = RM(a3);
        echo(" rm = ", rm);
    }
    //RM_demo();

    function rotMx(a)=[ [1,0,0], 
                        [0,cos(a),-sin(a)], 
                        [0,sin(a),cos(a)]
                      ]; 

    function rotMy(a)=[ [cos(a),0,sin(a)], 
                        [0,1,0], 
                        [-sin(a),0,cos(a)]
                      ]; 

    function rotMz(a)=[ [cos(a),-sin(a),0], 
                        [sin(a),cos(a),0],
                        [0,0,1] 
                      ]; 

} // RM


//========================================================
{ // rotM
function rotM(axis,a) = 
(   /* 
       Rotate about any arbitrary axis pq.
       
       http://www.fastgraph.com/makegames/3drotation/
   */   
   let( 
//       qp = axis //reverse(axis)
//        /* if we move pq by moving p to ORIGIN, p=>q will be looking
//           outward from the ORIGIN. This will result in wrong direction
//           of rotation. So we need to reverse it.
//        */
//      //, a=-a
      , uv= onlinePt( reverse(axis) ,len=1)- axis[1]   // unit vector
      , x=uv[0]
      , y=uv[1]
      , z=uv[2]
      , c=cos(a)
      , s=sin(a)
      , t=1-c
      , m= [ [ t*(x*x)+c,   t*(x*y)-s*z, t*(x*z)+s*y ]
           , [ t*(x*y)+s*z, t*(y*y)+c,   t*(y*z)-s*x ]
           , [ t*(x*z)-s*y, t*(y*z)+s*x, t*(z*z)+c   ]
           ]     
//       , _d= chkbugs([],
//                [  "pq", pq
//               , "x", x  
//               , "y", y 
//               , "z", z  
//               , "c",c
//               , "s",s
//               , "t",t
//               , "m",m
//               ]
//               )
       ) 
 // _dt
   m
);
    rotM= ["rotM","axis,a", "matrix", "Geometry",
    " Rotation matrix for rotating angle a about line axis. 
    ;;
    ;; To rotate 30 degreen about x-axis:
    ;;
    ;;    x_axis= [ [1,0,0], [0,0,0] ]
    ;;    m = rotM( x_axis, 30 ) 
    ;;    pt2 = m*(pt- x_axis[1]) + x_axis[1]  
    ;;
    ;; Mainly used in in rotPt() and rotPts(). Most likely the rotPts()
    ;; is the one to go for rotation, in which the translation ( +/-
    ;; x_axis[1]) part is taken care of. So the above example usage can be:
    ;;
    ;;    pt2= rotPt( pt, x_axis, 30 )
    ;;    
    ;; Check out rotPt_demo() (in file scadx_demo.scad)
    ;; 
    ;; To apply this to OpenScad object (non-polyh):
    ;;
    ;;    translate( axis[1] ) 
    ;;    multmatrix(m= rotM( axis,a) ) 
    ;;    {
    ;;       //translate( loc-axis[1] ) // loc: where obj is in xyz coord (do we need this?)
    ;;       cube( [3,2,1] );
    ;;    }
    ;;        
    ;; Note: rotation of pts about any axis can also be achieved through
    ;; anglePt(). However, anglePt() can't be used on the OpenScad
    ;; made, non-polyhedron objects for such a task. In that case, 
    ;; rotM is needed. 
    ;; http://forum.openscad.org/Transformation-matrix-function-library-td5154.html
    "]; 
    
    // Note-1:
    //
    // To re-locate a cylinder (on the z axis) to PQ:
    // ( see module Cylinder() (w/ capital C) in scadx_obj ):
    // 
    // v = Q-P;
    // z = [0,0,1]; 
    // voz = [ v, ORIGIN, z];
    // translate( P ) 
    // multmatrix(m= rotM( [ORIGIN, N(voz)],angle(voz) )) 
    // {
    //   cylinder(h = dist(PQ));
    // } 
    
    // Note-2:
    //
    // This method is to move pts or obj from xyz-coord to any location.
    // 
    // To relocate pts from ANY pqr to ANY new pqr, use: movePts( pts, from, to )
    // To relocate an obj from ANY pqr to ANY new pqr, use: Move( from, to ) obj
    // where from and to are a 'site'
  //}
    
    
} // rotM

//========================================================
{ // rotMx              
function rotMx(a)=[[1,0,0,0], 
                 [0,cos(-a),-sin(-a),0], 
                 [0,sin(-a),cos(-a),0], 
                 [0,0,0,1]]; 

    rotMx=["rotMx", "a", "matrix", "Math",
    ,""
    ];
        
    module rotMx_test(ops=["mode",13])
    {
        doctest( rotMx,
        [
          [ 90, slice(rotMx(90)*[2,1,-2,1],0,-1)
                , [2, 2,1]
                , ["funcwrap", "slice({_}*[2,1,-2,1])"] 
          ]
        
        ]
        ,ops);
    }
    //angle=90;
    //M= [ [cos(angle), -sin(angle), 0, 0],
    //                [sin(angle), cos(angle), 0, 0],
    //                [0, 0, 1, 1],
    //                [0, 0, 0,  1]
    //              ];
    //echo( Mpt= M*[0,2,1,1] );
} // rotMx

//========================================================
{ // rotpqr
function rotpqr(pqr,a)=
(   let( P=pqr[0]
       , Q=pqr[1]
       , R=pqr[2]
       , J=projPt( R,[P,Q] )
       , L=dist(J,R)
       , dJQ = dist(J,Q)
       , dPQ = dist(P,Q)
       , Px= onlinePt( [Q,P], len=2*(dJQ+dPQ)) // extended P
       )
    [ P
    , Q
    , anglePt( [ Px // Make sure a2 rot to right direction
               , J // othoPt([P,R,Q])
               , R
               ], a=90, a2=a, len=L ) 
    ]
);
    rotpqr=["rotpqr","pqr,a","pqr","Geom",
    "Rotate R in pqr by a."
    ];
        // (1)
        //      R_             PQ > TQ
        //      | '-._
        //	p---J-----'Q
        //
        // (2)           R     PQ > TQ     
        //              /|
        //     P-------Q J
        //   
        // (3)   R_            PQ < TQ, Need extended P
        //       |  '-._
        //       |       '-._
        //       J  p-------'Q
} // rotpqr

//========================================================
{ // rotx
function rotx(pt,a)=
    let( pt = rotMx(a)* [ pt.x,pt.y,pt.z,1] )
    [pt.x,pt.y,pt.x]; 

    rotx=["rotx", "pt,a", "matrix", "Math",
    ,""
    ];module rotx_test(ops=["mode",13])
    {
        doctest( rotx,
        [
          [ "[2,1,-1], 90", rotx([2,1,-1], 90)
                , [2, 1,1]
          ]
          , [ "[1,2,3], 90", rotx([1,2,3], 90)
                , [1,-3,2]
          ]
          , [ "[2,1,-1], 90", RMx(90)*([2,1,-1])
                , [2, 1,1]
          ]
          , [ "[1,2,3], 90", RMx(90)*([1,2,3])
                , [1,-3,2]
          ]
        ]
        ,ops);
    }
} // rotx

//========================================================
{ // RMx
function RMx(ax)= [
	[1,0,0]
	,[0, cos(-ax), sin(-ax) ]
	,[0,-sin(-ax), cos(-ax) ]
];

    RMx=["RMx","ax","matrix", "Geometry",
    "
     Given an angle (ax), return the rotation matrix for
    ;; rotating a pt about x-axis by ax. Example:
    ;;
    ;;  pt = [2,3,4]; 
    ;;  pt2= RMx(30)*pt;  
    ;;
    ;; In openscad, you can rotate the pt to pt2 by thiss:
    ;;
    ;;  rotate( [30,0,0 ])
    ;;  translate( pt )
    ;;  sphere(r=1); 
    ;;
    ;; However,  the coordinate info of pt2 is lost. RMx
    ;; (and RMy, RMz) provides a way to retrieve it. This
    ;; is a work around solution to an often asked question:
    ;; how to obtain the coordinates of a shape.
    "];	

    function RMx_test( mode=MODE, opt=[] )=
    (
        doctest( RMx
        ,[
        
        ],mode=mode, opt=opt )
    );
    //RMx_test( ["mode",22] );
    //doc(RMx);

    //RMz_test( ["mode",22] );
    //doc(RMz);
    //RMx_test( ["mode",22] );
    //doc(RMx);

} // RMx


////========================================================
{ // RMy
function RMy(ay)=[
	 [cos(-ay), 0, -sin(-ay) ]
	,[0,        1,        0  ]
	,[sin(-ay), 0,  cos(-ay) ]
];
    RMy=["RMy","ay","matrix", "Geometry",
    "
     Given an angle (ay), return the rotation matrix for
    ;; rotating a pt about y-axis by ay. See RMx for details.
    "];	

    function RMy_test( mode=MODE, opt=[] )=
    (
        doctest( RMy
        ,[
        
        ],mode=mode, opt=opt )
    );
    //RMy_test( ["mode",22] );
} // RMy

//========================================================
{ // RMz
function RMz(az)=[
	 [ cos(-az), sin(-az), 0 ]
	,[-sin(-az), cos(-az), 0 ]
	,[       0,         0, 1 ]
];
    
    RMz=["RMz","az","matrix", "Geometry",
    "
     Given an angle (az), return the rotation matrix for
    ;; rotating a pt about z-axis by az. See RMx for details.
    "];

    function RMz_test( mode=MODE, opt=[] )=
    (
        doctest( RMz
        ,[
        
        ],mode=mode, opt=opt )
    );
    //RMz_test( ["mode",22] );
    //doc(RMz);

} // RMz

//========================================================
{ // RMs
function RMs( angles, pts, _i_=0 )=
(
	_i_<len(pts)? concat( [ RM(angles)*pts[_i_] ]
                         , RMs( angles, pts, _i_+1)
					  ):[]  
);

    RMs=[[]];
    function RMs_test( mode=MODE, opt=[] )=
    (
        doctest( RMs,
        [
        ]
        ,
        mode=mode, opt=opt
        )
    );
} // RMs

//========================================================
{ // RM2z
function RM2z(pt,keepPositive=true)=
(
	/*pt.z==0
	?RM( rotangles_p2z(pt,keepPositive=true) ) 
	:RMy((keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/noRM(pt)))
	*RMx( sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z)) )
	*/
	RM( rotangles_p2z(pt,keepPositive=true) )
);
    RM2z=["RM2z","pt,keepPositive=true", "matrix", "Geometry", 
    "
     Given a pt, return the rotation matrix that rotates
    ;; the pt to z-axis. If keepPositive is true, the matrix will
    ;; always rotate pt to positive z. The xyz of the new pt on z-axis, ;;
    ;; pt2, is thus:
    ;;
    ;;  pt2 = RM2z(pt)*pt
    ;;
    ;; The reversed rotation, i.e., rotate a point on z back to pt,
    ;; can be achieved as:
    ;;
    ;;  pt3 = transpose( RM2z(pt) )*pt2  = pt
    "];
    
    function RM2z_test( mode=MODE, opt=[] )=
    (
        doctest( RM2z,
        [
        ]
        ,
        mode=mode, opt=opt
        )
    );
} // RM2z

//========================================================

/*function RMz2p(pt)=
(
3
//ax = -sign(pt.z)*asin( pt.y/L );  // rotate@y to xz-plane
//	ay = pt.z==0?90:atan(pt.x/pt.z); 	// rotate from xz-plane to target 	
	
);

    RMz2p=[ "RMz2p","pt","array", "Geometry",
    "
     Given a pt, return the rotation matrix that rotates the pt to
    ;; z-axis. The xyz of the new pt, pn, is:
    ;;
    ;;  pn = RM2z(pt)*pt
    "];

//echo("a<span style='font-size:20px'><i>this is a&nbsp;test</i></span>b");
*/

//========================================================
{ // rodfaces
function rodfaces(sides=6,hashole=false)=
 let( sidefaces = rodsidefaces(sides)
	, holefaces = hashole? 
				  [ for(f=sidefaces) 
					 reverse( [for(i=f) [i+sides*2] ])
				  ]:[]
	)
(
	hashole
	? concat( range(sides-1, -1)
			, range(sides, sides*2)
			, sidefaces
			) 	
	: concat( [ range(sides-1, -1) ]
			, [ range(sides, sides*2) ]
			, sidefaces
			)
);

//	p_faces = reverse( range(sides) );
//	// faces need to be clock-wise (when viewing at the object) to avoid 
//	// the error surfaces when viewing "Thrown together". See
//	// http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
//	q_faces = range(sides, sides*2); 
//concat( ops("has_p_faces")?[p_faces]:[]
//				 , ops("has_q_faces")?[q_faces]:[]
//				 , rodsidefaces(sides)
//				 , ops("addfaces")
//				 );

    rodfaces=["rodfaces","sides=6, hashole","array", "Faces",
     " Given count of sides (sides) of a rod( column, cylinder), return the 
    ;; faces required for the polyhedron() function to render the sides of
    ;; that rod. For a rod with sides=4 below: 
    "];

    function rodfaces_test( mode=MODE, opt=[] )=
    (
        doctest( rodfaces,[],mode=mode, opt=opt )
    );                     
} // rodfaces


//========================================================
{ // rodPts
function rodPts( pqr=randPts(3), opt=[] )=
 let( 
     df_opt = 
      [ "r", 0.5
      , "rP", undef
      , "rQ", undef
      , "nside",     6
      
      , "len", d01(pqr)
      , "ext", 0  // Extend outward. A # for len or "r#" for ratio
                  //   Could be array [extP,extQ] 
      , "extP",0  // extend outward from P pt
      , "extQ",0  // extend outward from Q pt
        
	  , "ptsP",undef  // pts on the P end
	  , "ptsQ",undef  // pts on the q end      
      , "xsecpts", undef // cross-section pts. These are 2D
                         // pts ref. to ORIGIN.
      
	  , "aP",90   // cutting angle on p end
	  , "aQ",90   // cutting angle on q end
	  , "rotate",0  // rotate about the PQ line, away from xz plane
	  , "rotQ", 0 // see twistangle()
      , "rotB4cutA",true // to determine if cutting angle 
                              // first or rotate first. 
	  

	  ]
    //--------------------------------------------------
    ,_opt = isstr(opt)? sopt(opt):opt
	,opt  = update( df_opt, _opt ) //concat( opt, _opt )
    
	,r    = hash(opt, "r")
    ,rP   = or( hash(opt,"rP"), r)
    ,rQ   = or( hash(opt,"rQ"), r)
    
    ,xsecpts= hash(opt, "xsecpts")
	,nside= or(len(xsecpts),hash(opt,"nside"))
    
    //------------------------------------------------
    ,L    = hash(opt, "len")
    ,ext  = hash(opt, "ext")
    ,_extP = or(hash(opt, "extP"), len(ext)==2? ext[0]:ext)
    ,_extQ = or(hash(opt, "extQ"), len(ext)==2? ext[1]:ext)
    ,extP  = (begwith(_extP,"r")
                ?num(slice(_extP,1))*L: _extP)*mm 
    ,extQ = (begwith(_extQ,"r")
                ?num(slice(_extQ,1))*L: _extQ)*mm 
    ,Po   = pqr[0]
    ,Qo   = onlinePt( pqr, len=L)
    ,Ro   = pqr[2]
    
    ,P    = onlinePt( [Po,Qo], len=-extP) 
    ,Q    = onlinePt( [Qo,Po], len=-extQ) 
    ,R    = rotpqr( pqr, hash(opt,"rotate",0 ))[2]
    ,J    = projPt( [P,Q],R )
    ,Px   = onlinePt( [P,Q]  // extend P outward to ensure direction
            , len=-2*(dist(J,Q)+dist(P,Q)))
    ,uvN   = uvN( [Px,P,R] )
//    ,Np   = N([Px,P,R])
//    ,Nq   = N([Px,Q,R])
    ,Mp   = N2([Px,P,R])
    ,Mq   = N2([Px,Q,R])
    //------------------------------------------------
    ,ptsP = xsecpts? // pts to be on pl [Px, P, Mp]
            [ for(i=range(xsecpts)) 
               let( pt = xsecpts[i]  
                  , ptx= onlinePt( [P,Mp], len= pt.x)
                  , pty= ptx+ uvN*pt.y
                  )
               pty                
            ]
            :  arcPts( [Px,P,R], a=90, a2= 360*(nside-1)/nside, n=nside,rad=r )
            
    ,ptsQ = xsecpts? // pts to be on pl [P, Q, Mq]
            [ for(i=range(xsecpts)) 
               let( pt = xsecpts[i]  
                  , ptx= onlinePt( [Q,Mq], len= pt.x)
                  , pty= ptx+ uvN*pt.y
                  )
               pty                
            ]
            : arcPts( [Px,Q,R], a=90, a2= 360*(nside-1)/nside, n=nside,rad=r )
            
    )//let
( 
	//[Px,Q,R, Mp,Mq] 
    [ptsP,ptsQ]
    // [[Px,P,R],[Px,Q,R] ]
            
);    
            
    rodPts=["rodPts","","pts","Point",
     " Given 
    ;;
    "];
    function rodPts_test( mode=MODE, opt=[] )=
    (
        doctest( rodPts,[],mode=mode, opt=opt )
    ); 

} // rodPts 


//========================================================
{ // rodsidefaces
function rodsidefaces( sides=6, twist=0) =
 let( top= twist
		   ? roll( range(sides,2*sides), count=twist) 
		   : range(sides,2*sides)
	)
(
	[ for(i = range(sides) )
	  [ i
	  , i==sides-1?0:i+1
	  , i==sides-1? top[0]: top[i+1]
	  , top[i]
//	  , i + 1 + (i- twist==sides-1?0:sides) -twist
//	  , i+sides - twist +( i==0&&twist>0?sides:0)
	  ]
	]
);
    rodsidefaces=["rodsidefaces","sides=6,twist=0","array", "Faces",
     " Given count of sides (sides) of a rod( column, cylinder), return the 
    ;; faces required for the polyhedron() function to render the sides of
    ;; that rod. For a rod with sides=4 below: 
    ;;         
    ;;       _6-_
    ;;    _-' |  '-_
    ;; 5 '_   |     '-7
    ;;   | '-_|   _-'| 
    ;;   |    '(4)   |
    ;;   |   _-_|    |
    ;;   |_-' 2 |'-_ | 
    ;;  1-_     |   '-3
    ;;     '-_  | _-'
    ;;        '-.'
    ;;          0
    ;; 
    ;; twist=0
    ;;
    ;;   [0,1,5,4]
    ;; , [1,2,6,5]
    ;; , [2,3,7,6]
    ;; , [3,0,4,7]
    ;;
    ;; twist = 1:
    ;;
    ;;       _5-_
    ;;    _-' |  '-_
    ;;(4)'_   |     '-6
    ;;   | '-_|   _-'| 
    ;;   |    '-7'   |
    ;;   |   _-_|    |
    ;;   |_-' 2 |'-_ | 
    ;;  1-_     |   '-3
    ;;     '-_  | _-'
    ;;        '-.'
    ;;          0
    ;;
    ;;   [0,1,4,7]
    ;; , [1,2,5,4]
    ;; , [2,3,6,5]
    ;; , [3,0,7,6]
    ;;
    ;;
    ;; twist = 2:
    ;;       (4)_
    ;;    _-' |  '-_
    ;; 7 '_   |     '-5
    ;;   | '-_|   _-'| 
    ;;   |    '-6'   |
    ;;   |   _-_|    |
    ;;   |_-' 2 |'-_ | 
    ;;  1-_     |   '-3
    ;;     '-_  | _-'
    ;;        '-.'
    ;;          0
    ;;
    ;;   [0,1,7,6]
    ;; , [1,2,4,7]
    ;; , [2,3,5,4]
    ;; , [3,0,6,5]
    ;;
    ;; Note that the top and bottom faces are NOT included. 
    "];
    function rodsidefaces_test( mode=MODE, opt=[] )=
    (
        doctest( rodsidefaces,
        [
         [ rodsidefaces(4), 
                [ [0,1,5,4]
                , [1,2,6,5]
                , [2,3,7,6]
                , [3,0,4,7]
                ], "4"
        ]
        ,[ rodsidefaces(5), 
                [ [0, 1, 6, 5]
                , [1, 2, 7, 6]
                , [2, 3, 8, 7]
                , [3, 4, 9, 8]
                , [4, 0, 5, 9]
                ], "5"
         ]
        ,[ rodsidefaces(4,twist=1), 
                [ [0,1,4,7]
                , [1,2,5,4]
                , [2,3,6,5]
                , [3,0,7,6]
                ], "4,twist=1"
        ]
        ,[ rodsidefaces(4,twist=-1), 
                [ [0,1,6,5]
                , [1,2,7,6]
                , [2,3,4,7]
                , [3,0,5,4]
                ], "4,twist=-1"
        ]
        ,[ rodsidefaces(4,twist=2), 
                [ [0,1,7,6]
                , [1,2,4,7]
                , [2,3,5,4]
                , [3,0,6,5]
                ], "4,twist=2"
         ]

        ], mode=mode, opt=opt
        )
    );
} //rodsidefaces
//========================================================
{ // rotangles_p2z
    
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);
    rotangles_p2z=["rotangles_p2z","pt,keepPositive=true","Array","Geometry",
    "
     Given a 3D pt, return the rotation array that
    ;; would rotate pt to a point on z-axis, ptz= [0,0,L],
    ;; where L is the len of pt to the ORIGIN.
    ;; Two ways to rotate pt to ptz using the rotation
    ;; array (RA) is obtained:
    ;;
    ;; 1. Scadex way: (has the target coordinate)
    ;;
    ;;    RA = rotangles_p2z( pt ); 
    ;;    ptz = RM( RA )*pt; 
    ;;    translate( ptz )
    ;;    sphere9 r=1 ); 
    ;;
    ;; 2. OpenScad way: (lost the target coordinate)
    ;;
    ;;    RA = rotangles_p2z( pt ); 
    ;;    rotate( RA )
    ;;    translate( pt )
    ;;    sphere( r=1 ); 
    "
    ];
      
    function rotangles_p2z_test( mode=MODE, opt=[] )=
    ( 
        doctest( rotangles_p2z,
        []
        , mode=mode, opt=opt 
        )
    );
    //rotangles_p2z_test( ["mode",22] );

    //rotangles_p2z_demo();
    //doc(rotangles_p2z);

} // rotangles_p2z



//========================================================
{ //rotangles_z2p
/*
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);
*/
function rotangles_z2p(pt)=
(
	//// 2014.5.20: add 
	////   (pt.x<0?-1:1)
	//// to cover the case of : [x,0,0] when x<0
	////
	(pt.z==0&&pt.y!=0)
	?[ -sign(pt.y)*90, 0, -sign(pt.x)*sign(pt.y)*abs(atan(pt.x/pt.y))]  
	:[-sign(pt.z)*asin( pt.y/ norm(pt) )  // rotate@y to xz-plane
	 , pt.z==0?((pt.x<0?-1:1)*90):atan(pt.x/pt.z) 	      // rotate from xz-plane to target 	
	 , 0]

);	
    rotangles_z2p=["rotangles_z2p","pt","Array","Geometry",
    "
     Given a 3D pt, return the rotation array that
    ;; would rotate a pt on z, ptz (=[0,0,L] where L is
    ;; the distance between pt and the origin), to pt
    ;; Usage:
    ;;
    ;;    RA= rotangles_z2p( pt ); 
    ;;    rotate( RA )
    ;;    translate( ptz )
    ;;    sphere( r=1 ); 
    "
    ];
    function rotangles_z2p_test( mode=MODE, opt=[] )=
    ( doctest( rotangles_z2p, mode=mode, opt=opt )
    );

    //rotangles_z2p_test( ["mode",22] );
    //rotangles_z2p_demo();
} // rotangles_z2p

//========================================================
{ // rotPt
function rotPt(pt,axis,a)=
   let( m=rotM(axis,a)
      )
(
   m*(pt-axis[0]) + axis[0] 
);

    rotPt=["rotPt","pt,axis, a","pt", "Geometry",
    " Rotate pt for angle a about line pq.
    "
    ];
    function rotPt_test( mode=MODE, opt=[] )=
    ( 
        doctest( rotPt,
        []
        , mode=mode, opt=opt 
        )
    );
} // rotPt

//========================================================
{// rotPts
function rotPts(pts,axis,a)=
   let( m=rotM(axis,a)
      )
(
   [for(pt=pts) m*(pt-axis[0]) + axis[0] ] 
);

    rotPts=["rotPts","pt,axis, a","pt", "Geometry",
    " Rotate pt for angle a about line pq.
    "
    ];
    function rotPts_test( mode=MODE, opt=[] )=
    ( 
        doctest( rotPts,
        []
        , mode=mode, opt=opt 
        )
    );

}// rotPts

//function roundPts(pts, d=2)=
//(
   //let(power= pow(10,d))
   //isnum(pts[0])? [ for(x=pts) round(x*power)/power]
   //: [ for(p=pts) roundPts(p,d) ]
//);

function roundness(pts)= 
( 
  // how close pts to a circle or sphere 2018.8.1
  // It's a value normalized to 1, a circle or sphere=1
  // The closer the return to 1, the better the pts represents a circle/sphere
  // Note: triangle doesn't necessarily means they are close to look loke circle.
  //       The more skewer it is, the less it looks like a circle.
  // Example:
  //
  //  A. +-----------------+   B. +-----------------+
  //     |          a      |      |         a       | 
  //     +---+             |      |                 | 
  //       c |             |b     |  d              |b 
  //         |d            |      +------+          | 
  //         |             |             | c        |
  //         +-------------+             +----------+
  //
  //  Let a>b and d>c, then, the A has better (smaller) roundness than B (larger) 
  /*
     NOTE: this approach, based on the gravity of pts ( sum(pts)/n ), has issue:
     
      C. +-----------------+   
         |          a      |   
         +-------------+   |    
                    G  |   |b   
                       |   |    
                       |   |         
                       +---+            
     
     Based on this approach, C has high value of roundness , which is not true
  
  
  */
  /*
      G: gravity = centroid pt
      
      If we use only dist to G as a measure, the following graph:
         
         +-----------------+   
         |                 |   
       A +-------------B   |    
                    G  |   |    
                       |   |    
                       |   |         
                       C---+            
  
       would have a high roundness, which is not true. So we also need to
       take angle into considerations. 
       
      Lets say, roundness, 
      
          R = 1- (aSnAD + aSnDD)
          
      The larger R, the more roundness.     
      
      D: deviation
      DD: dist deviation (against avg dist)
      nDD: normalized DD  = (D_i-avg_D)/avg_D
      SnDD: sum of nDD    = Sum[i=1~n]( nDD )
      aSnDD: average SnDD = SnDD/n = Sum[i=1~n]( nDD ) / n 
      
      AD: angle deviation
      nAD: normalized AD 
           avg_A = 360/n
           nAD = ( A_i- 360/n) / (360/n) = n(A_i-360/n)/360
      SnAD: sum of nAD = Sum[i=1~n]( nAD )      
      aSnAD =  Sum[i=1~n]( nAD ) 
      
      
           D----------------------C
          /       N              /
         /        |             /
        E---------|----F       / 
                  |   /       /
                  G  /       /
                    /       / 
                   /       /
                  A-------B   
        
      N is a normal pt on top of surface. The calc of angles uses N
      as a reference: 
      
         A_i = angle( [Pt_i,G,Pt_i+1], Rf=N )
  
      This would make aEGF either >180 or <0 while all others are 0<a<180.
      If it's <0, we do: a= 360+a to make it remain large deviation. 
     
   Triangles tested look ok.
   
   4-pointer: not quite:   
   Not-quite-right cases:
   
   pts = [[2, 6.47917, 0], [1.3125, 7.27083, 0], [0, 0, 0], [1.9375, 3.0625, 0]];
   R = 0.009 /// too small    
  
   [[0.255639, 1.84962, 0], [0, 1.36842, 0], [1.06015, 0, 0], [2, 4.18045, 0]]
   R = 0.05 too small 
   
  */
   //echo( log_b("roundness()") )
   //echo( log_(["pts",pts] ))    
   let( n = len(pts)
      , G = sum(pts)/n 
      , dists = [ for(p=pts) dist(p,G) ]
      , avg_dist= sum( dists )/n 
      , nDD = [for(d=dists) abs(d-avg_dist)/avg_dist] 
      , SnDD = sum(nDD)
      , aSnDD = SnDD/n
      //, SnDD = sum( [for(d=dists) abs(d-avg_dist)/avg_dist] )
      //, aSnDD = sum( [for(d=dists) abs(d-avg_dist)/avg_dist] )/n
      
      //, N = N( [pts[0], G, pts[1] ] )
      , N = N( get3(pts,1) )
      //, as = [ for(i= [0:n-1])
                //angle( [ pts[i],G,pts[i==n-1?0:i+1]], Rf=N ) 
             //]
      , as = [ for(i= [0:n-1]) angle( get3(pts,i), Rf=N ) 
             ]
      , as2 = [ for(a=as) a<0?(360+a):a ]
      , avg_a = (n-2)*180/n //360/n
      , nAD = [for(a=as2) abs(a-avg_a)/avg_a] 
      , SnAD = sum( nAD )
      , aSnAD =  SnAD/n
      //, _rtn = aSnAD 
      , _rtn = (.2*aSnDD + .8*aSnAD)     
      //, _rtn = (.5*aSnDD + .5*aSnAD)     
      , __rtn = _rtn<0?0:(1-_rtn)
      , rtn = round(__rtn*1000)/1000     
      )
      
   //echo( log_(["G",G,"N",N, "avg_dist", avg_dist, "avg_a", avg_a]) )
   //echo( log_( ["dists", dists]) )
   //echo( log_( ["nDD", nDD]) )
   //echo( log_( ["aSnDD", _red(aSnDD)]) )
   //echo( log_( ["as", as]) )
   //echo( log_( ["as2", as2]) )
   //echo( log_( ["nAD", nAD]) )
   //echo( log_( ["SnAD", SnAD]) )
   //echo( log_( ["aSnAD", _red(aSnAD)]) )
   //echo( log_( ["rtn", _red(rtn)]) )
   //
   //echo( log_e())
   rtn
   
    
   //echo("roundness")   
   //echo(pts=pts)   
   //echo(sum = sum(pts), C=C)
   //echo(dists=dists)
   //echo(avg=avg)
   //rtn<0?0:(1-rtn)
);


function roundness_use_intercepts(pts, prec=0.4)= 
( 
  // how close pts to a circle or sphere 2018.8.1
  // It's a value normalized to 1, a circle or sphere=1
  // The closer the return to 1, the better the pts represents a circle/sphere
  // Note: triangle doesn't necessarily means they are close to look loke circle.
  //       The more skewer it is, the less it looks like a circle.
  // Example:
  //
  //  A. +-----------------+   B. +-----------------+
  //     |          a      |      |         a       | 
  //     +---+             |      |                 | 
  //       c |             |b     |  d              |b 
  //         |d            |      +------+          | 
  //         |             |             | c        |
  //         +-------------+             +----------+
  //
  //  Let a>b and d>c, then, the A has better (smaller) roundness than B (larger) 
  /*
     NOTE: this approach, based on the gravity of pts ( sum(pts)/n ), has issue:
     
      C. +-----------------+   
         |          a      |   
         +-------------+   |    
                    G  |   |b   
                       |   |    
                       |   |         
                       +---+            
     
     Based on this approach, C has high value of roundness , which is not true
  
  
  */
  /*
      G: gravity = centroid pt
      
      If we use only dist to G as a measure, the following graph:
         
         +-----------------+   
         |                 |   
       A +-------------B   |    
                    G  |   |    
                       |   |    
                       |   |         
                       C---+            
  
       would have a high roundness, which is not true. So we also need to
       take angle into considerations. 
       
      Lets say, roundness, 
      
          R = 1- (aSnAD + aSnDD)
          
      The larger R, the more roundness.     
      
      D: deviation
      DD: dist deviation (against avg dist)
      nDD: normalized DD  = (D_i-avg_D)/avg_D
      SnDD: sum of nDD    = Sum[i=1~n]( nDD )
      aSnDD: average SnDD = SnDD/n = Sum[i=1~n]( nDD ) / n 
      
      AD: angle deviation
      nAD: normalized AD 
           avg_A = 360/n
           nAD = ( A_i- 360/n) / (360/n) = n(A_i-360/n)/360
      SnAD: sum of nAD = Sum[i=1~n]( nAD )      
      aSnAD =  Sum[i=1~n]( nAD ) 
      
      
           D----------------------C
          /       N              /
         /        |             /
        E---------|----F       / 
                  |   /       /
                  G  /       /
                    /       / 
                   /       /
                  A-------B   
        
      N is a normal pt on top of surface. The calc of angles uses N
      as a reference: 
      
         A_i = angle( [Pt_i,G,Pt_i+1], Rf=N )
  
      This would make aEGF either >180 or <0 while all others are 0<a<180.
      If it's <0, we do: a= 360+a to make it remain large deviation. 
     
   Triangles tested look ok.
   
   4-pointer: not quite:   
   Not-quite-right cases:
   
   pts = [[2, 6.47917, 0], [1.3125, 7.27083, 0], [0, 0, 0], [1.9375, 3.0625, 0]];
   R = 0.009 /// too small    
  
   [[0.255639, 1.84962, 0], [0, 1.36842, 0], [1.06015, 0, 0], [2, 4.18045, 0]]
   R = 0.05 too small 
   
  */
   //echo( log_b("roundness()") )
   //echo( log_(["pts",pts] ))    
   let( n = len(pts)
      , _pts= [ for(i= range(concat( pts, [pts[0]]) ))
                 let( d = dist( pts[i], pts[i+1] )
                    )
                 each [ for( j = range(floor( d/prec)) )
                        onlinePt( [pts[i], pts[i+1]], len= prec*j ) 
                      ]   
               ]
      , _n = len(_pts)         
      , G = sum(_pts)/_n 
      , dists = [ for(p=_pts) dist(p,G) ]
      , avg_dist= sum( dists )/_n 
      , nDD = [for(d=dists) abs(d-avg_dist)/avg_dist] 
      , SnDD = sum(nDD)
      , aSnDD = SnDD/ _n
      //, SnDD = sum( [for(d=dists) abs(d-avg_dist)/avg_dist] )
      //, aSnDD = sum( [for(d=dists) abs(d-avg_dist)/avg_dist] )/n
      
      //, N = N( [pts[0], G, pts[1] ] )
      , N = N( get3(_pts,1) )
      //, as = [ for(i= [0:n-1])
                //angle( [ pts[i],G,pts[i==n-1?0:i+1]], Rf=N ) 
             //]
      , as = [ for(i= [0:_n-1]) angle( get3(_pts,i), Rf=N ) 
             ]
      , as2 = [ for(a=as) a<0?(360+a):a ]
      , avg_a = (n-2)*180/n //360/n
      , nAD = [for(a=as2) abs(a-avg_a)/avg_a] 
      , SnAD = sum( nAD )
      , aSnAD =  SnAD/_n
      //, _rtn = aSnAD 
      , _rtn = aSnDD //(.2*aSnDD + .8*aSnAD)     
      //, _rtn = (.5*aSnDD + .5*aSnAD)     
      , __rtn = _rtn<0?0:(1-_rtn)
      , rtn = round(__rtn*1000)/1000     
      )
      
   echo( log_(["G",G,"N",N, "avg_dist", avg_dist, "avg_a", avg_a]) )
   echo( log_( ["_pts", _pts]) )
   echo( log_( ["len(_pts)", len(_pts)]) )
   echo( log_( ["dists", dists]) )
   echo( log_( ["nDD", nDD]) )
   echo( log_( ["aSnDD", _red(aSnDD)]) )
   //echo( log_( ["as", as]) )
   //echo( log_( ["as2", as2]) )
   //echo( log_( ["nAD", nAD]) )
   //echo( log_( ["SnAD", SnAD]) )
   //echo( log_( ["aSnAD", _red(aSnAD)]) )
   echo( log_( ["rtn", _red(rtn)]) )
   
   echo( log_e())
   rtn
   
    
   //echo("roundness")   
   //echo(pts=pts)   
   //echo(sum = sum(pts), C=C)
   //echo(dists=dists)
   //echo(avg=avg)
   //rtn<0?0:(1-rtn)
);
//function roundness(pts)= 
//( 
//  // how close pts to a circle or sphere 2018.8.1
//  // It's a value normalized to 1, a circle or sphere=1
//  // The closer the return to 1, the better the pts represents a circle/sphere
//  // Note: triangle doesn't necessarily means they are close to look loke circle.
//  //       The more skewer it is, the less it looks like a circle.
//  // Example:
//  //
//  //  A. +-----------------+   B. +-----------------+
//  //     |          a      |      |         a       | 
//  //     +---+             |      |                 | 
//  //       c |             |b     |  d              |b 
//  //         |d            |      +------+          | 
//  //         |             |             | c        |
//  //         +-------------+             +----------+
//  //
//  //  Let a>b and d>c, then, the A has better (smaller) roundness than B (larger) 
//  /*
//     NOTE: this approach, based on the gravity of pts ( sum(pts)/n ), has issue:
//     
//      C. +-----------------+   
//         |          a      |   
//         +-------------+   |    
//                       |   |b   
//                       |   |    
//                       |   |         
//                       +---+            
//     
//     Based on this approach, C has high value of roundness , which is not true
//  
//  
//  */
//   
//   let( C = sum(pts)/len(pts) 
//      , dists= [ for(p=pts) dist(C,p) ]
//      , avg= sum(dists)/len(pts)
//      , _rtn = sum( [for(d=dists) 
//                      //d-avg 
//                      pow( d-avg, 2) 
//                    ] )/avg /len(pts)
//      , rtn = is0(_rtn)?0:_rtn
//      )
//   //echo("roundness")   
//   //echo(pts=pts)   
//   //echo(sum = sum(pts), C=C)
//   //echo(dists=dists)
//   //echo(avg=avg)
//   rtn<0?0:(1-rtn)
//);

//roundness_test();


//. s
//========================================================
{ // scaleM

function scaleM(v)=[[v[0],0,0,0], 
                   [0,v[1],0,0], 
                   [0,0,v[2],0], 
                   [0,0,0,1]]; 

    scaleM=["scaleM", "v", "matrix", "Math",
    "scale matrix
    ;;  ???
    "
    ];
    module scaleM_test(ops=["mode",13])
    {
        doctest( scaleM,
        [ 
            [ [2,3,1], slice(scaleM([2,1,1])*[2,1,-2,0],0,-1)
                , [], ["funcwrap", "slice({_}*[2,1,-2,0])"] ]
        
        ]
        ,ops);
    }       
} // scaleM

//onlinePt(pq, len=undef, ratio=0.5)
    
function scalePt(pt, base=ORIGIN, len=undef, ratio=0.5)=
(
   isequal(pt,base)? pt
   :ispt(base)?
     onlinePt( len?[pt,base]:[base,pt] 
             , len?-len:undef, ratio ) 
   :isline(base) ||
    ispts(base)&&len(base)>2 ?
     onlinePt( len? [pt, projPt( pt,base)]:[projPt( pt,base), pt] 
              , len?-len:undef, ratio ) 
   :undef  
);

    scalePt=["scalePt", "pt,base=ORING,len=undef,ratio=0.5"
    , "Scale *pt* by either len or ratio based on *base*.
    ;; 
    ;; base: pt|line|plane
    ;;
    ;; If len given, extend the pt by that distance
    ;; Otherwise, take ratio.
    ;;
    ;; Both len and ratio could be negative
    ;;
    ;; 2018.1.17.
    "
    ,"onlinePt" 
    ];
    
function scalePts(pts, base, len, ratio, opt=[], _i=0)=
(
   
   let( base = und_opt(base, opt, "base", ORIGIN)
      , len  = und_opt(len, opt, "len")
      , ratio= und_opt(ratio, opt, "ratio", 0.5)
      ) 
   [ for(p=pts) scalePt(p, base, len, ratio)]
);

    scalePts=["scalePts", "pts,base,len,ratio,opt=[]" 
    ];

function setPtsDecimal(pts,d=2)= // 2018.5.17
(  
   let(pw=pow(10,d))
   [for(p=pts) [ for(n=p) round(n*pw)/pw] ] 
);

//========================================
//
// shapeInfo2d: developed with 2_4_shape2d_info() in dev_188a_shape_descriptor.scad 
//
function shapeInfo2d( 
      pts     // 3d pts on a plane
    , prec= 3 // minimum angle jump
    , nMaxSteps = 20 // <== just for safeguard
    , da= 45  // Intv for each search. Given line [I,B] where I 
              // is the intsec of two quadlines, make [I,A] and
              // [I,C] where angle([B,I,A])= -da and angle([B,I,C])=da.
              // Calc the aSSD (avg sum of squared dist) on IA,IB,IC.   
              // Find one that has the smallest aSSD. If the IB (the original
              // one) has the smallest, half the da and repeat. If the picked
              // line is IC, then pick IC and repeat with +da. If IA, 
              // pick IA and repeat with -da 
              // Search stops when da < prec or nSteps=nMaxSteps 
    , _longax 
    , _longarm
    //, _A
    //, _C
    , _starting_aSSD
    , _aSSD
    , _dir // -1|0|1 for -da, da/2, and da, respectively
           // _dir=0 means need to check both ends with +-da/2
           // _dir=-1 means, 
           /*
              _dir=-1: Use A as next longarm and check only -da
                                  
                      C  
                     /
                    B                               
                   / 
                  A

              _dir=0: Keep B as next longarm, check +-da/2
                          
                  A.       C
                    `.   .`
                      `B'

              _dir=1: Use C as next longarm and check only +da
                          
                  A.
                    `B.
                       `C

           */
                        
    , _i=-1)=
(  
   let(lv=1)
   _i==-1?
     echo(log_b(["2_4_shape2d_info() init: _i",_i], lv))
     let( Cent= centroidPt(pts) 
        , _pts= [for(p=pts)p-Cent] 
        , qcs = quadCentroidPts(_pts)
        , ax02= [qcs[0], qcs[2]]   
        , ax13= [qcs[1], qcs[3]]
        , _longax= dist(ax02)>dist(ax13)? ax02:ax13
        , B = _longax[0]
        , I = intsec(ax02,ax13)
        , N = N( [B,I,_pts[0]] ) 
        , _starting_aSSD= avg_SSD(_pts, _longax)  
        )
     //echo( log_(["pts", pts] ))
     echo( log_(["Cent", Cent] , lv))
     echo( log_(["I", I, "B",B], lv ))
     //echo( log_(["sum_pts",sum(pts)] , lv))
     //echo( log_(["sum_pts/n",sum(pts)/len(pts)] , lv))
     //echo( log_(["_pts",_pts], lv ))
     echo( log_(["_starting_aSSD",_starting_aSSD] , lv))
     echo( log_e("", lv) )
     2_4_shape2d_info( 
                  pts=_pts
                , prec= prec 
                , nMaxSteps = nMaxSteps 
                , da= da 
                , _ax02= ax02
                , _ax13= ax13
                , _longax = _longax
                , _longarm = [I,B] 
                , _starting_aSSD=_starting_aSSD
                , _aSSD=_starting_aSSD
                , _dir=0
                , _i=_i+1)   
   
  : !( _i>nMaxSteps || abs(da)< prec/2 )? 
     
    let( B= _longarm[1]
       , I= _longarm[0]
       , A= _dir==1? undef:anglePt( [B,I,Y], a=-da, Rf=Z ) // _dir=1, forward, don't need A 
       , C= _dir==-1? undef:anglePt( [B,I,Y], a=da, Rf=Z ) // _dir=-1, backward,don't need C 
       , aSSD_A= _dir==1? _aSSD*2  // _dir=1, forward, don't need A, *2 to make it larger
                        : avg_SSD( pts, [I,A])
       , aSSD_B= _aSSD 
       , aSSD_C= _dir==-1? _aSSD*2  // _dir=-1, backward, don't need C, *2 to make it larger
                         : avg_SSD( pts, [I,C]) 
       , sorted_aSSDs = sortArrs([ [ aSSD_A, -1] 
                          , [ aSSD_B, 0] 
                          , [ aSSD_C, 1] 
                          ], by=0)
       , nextdir= sorted_aSSDs[0][1] //[-1,0,1][ smallest ]                   
       , _aSSD= sorted_aSSDs[0][0]                   
       )
     echo( log_b( _s("_i={_}, nMaxSteps={_}, prec={_}, da={_}"
                 ,[_i,nMaxSteps,prec,da]),lv ))
     //echo( log_( ["pts", pts]))
     echo( log_( ["_longarm", _longarm], lv))
     echo( log_( ["IA", [I,A]], lv))
     echo( log_( ["IB", [I,B]], lv))
     echo( log_( ["IC", [I,C]], lv))
     echo( log_( ["aSSD_A", aSSD_A], lv))
     echo( log_( ["aSSD_B", aSSD_B], lv))
     echo( log_( ["aSSD_C", aSSD_C], lv))
     echo( log_( ["aSSDs", [[aSSD_A,-1],[aSSD_B,0],[aSSD_C,1]]], lv))
     echo( log_( ["sorted_aSSDs", sorted_aSSDs], lv))
     //echo( log_( ["i of lowest aSSDs:", aSSDs[0][1]] , lv))
     echo( log_( ["_dir", _dir], lv))
     echo( log_( ["next_dir", nextdir], lv))
     echo( log_( ["_aSSD", _red(_aSSD)], lv))
     echo( log_e("", lv))
     2_4_shape2d_info( 
                  pts=pts
                , prec= prec 
                , nMaxSteps = nMaxSteps  
                , da= nextdir==-1 ?-da // The one with -da has the smallest aSSD
                        :nextdir==0 ? da/2
                        : da 
                , _ax02= _ax02
                , _ax13= _ax13
                , _longax = _longax
                , _longarm = nextdir==-1?[I,A]
                             :nextdir==0 ?[I,B]:[I,C]
                , _starting_aSSD= _starting_aSSD
                , _aSSD= _aSSD
                , _dir = nextdir
                , _i=_i+1) 
                
   : //----------------- exiting ------------------ 
       
     echo( log_b( _s("Existing 2_4_shape2d_info, _i={_}, prec={_}, da={_}"       
                 , [_i,prec,da] ), lv))
     //echo( log_(["I",I]))
     //echo( log_(["_pts",_pts]))
     let( len_2ndArm= dist(_longax)-dist(_longarm)
        , longAx=[ _longarm[1], onlinePt(_longarm, len=dist(_longarm)- dist(_longax))] 
        , longAx2= [ onlinePt(_longarm, ratio=1.5)
                 , onlinePt(_longarm, len=-len_2ndArm*1.5)
                 ] 
     , 
        , half_width= sqrt(_aSSD,2)
                      // Suppose there's a rectangler frame boxing all pts, 
                      // and all pts are on the two frame sides parallel to
                      // the long Ax, then they will give a _aSSD the same
                      // as calculated _aSSD. So we got the calc. _aSSD, reverse
                      // it back to "avg distance" a pt is to the longax to get
                      // the half_width
        , shortAx= [anglePt( [_longarm[1], _longarm[0], X], a=90, len= half_width )
                   , anglePt( [_longarm[1], _longarm[0], X], a=-90, len= half_width )
                   ]
        , shortLongRatio= dist(shortAx)/dist(longAx2) 
        //-------------
        , longEndPtsInfo= endPtsInfoAlongLine(pts,longAx)
                         ///: [ [ i0, pt0, dist0], [i1, pt1, dist1] ]
                         
        , longAx_Js = [ projPt(longEndPtsInfo[0][1],longAx)
                      , projPt(longEndPtsInfo[1][1],longAx)]
                      // 2 end pts on the longAx 
        , longAx_midPt = sum(longAx_Js)/2
        , longAx_bysect_line = [ longAx_midPt
                               , anglePt( [longAx_Js[0], longAx_midPt, X]
                                        , a=90, len= half_width 
                                        ) 
                               ]
        , sortByDist_against_longAx = sortByDist( pts
                                       , [longAx[0], longAx[1]
                                         , N([longAx[0], longAx_midPt,shortAx[0]])
                                         ] // Use plane 'cos we need negative dist
                                       , isAbs=0)
                         ///: [ [ i0, pt0, dist0], [i1, pt1, dist1], ... ]
        
        , shortAx_Js = [ projPt(sortByDist_against_longAx[0][1],longAx_bysect_line) 
                       , projPt(last(sortByDist_against_longAx)[1],longAx_bysect_line)
                       ]
                       
        , boxPts = [ longAx_Js[0]+ shortAx_Js[0]-longAx_midPt
                   , longAx_Js[0]+ shortAx_Js[1]-longAx_midPt
                   , longAx_Js[1]+ shortAx_Js[1]-longAx_midPt
                   , longAx_Js[1]+ shortAx_Js[0]-longAx_midPt
                   ]  
        , roundedness= dist(shortAx_Js)/dist(longAx_Js)                                           
        )
     /*   
     function SSD(pts, target)= // Sum of Squared Dist. Target could be pt, line or plane. 2018.8.12
(
  sum( [for(p=pts) pow( dist(p,target),2) ] ) 
);

function avg_SSD(pts, target)= // averaged Sum of Squared Dist. 2018.8.12
(
  sum( [for(p=pts) pow( dist(p,target),2) ] ) / len(pts) 
);

     */   
        
     echo( log_e("", lv))
     ["longarm",_longarm
//     , "longAx", [ _longarm[1], onlinePt(_longarm, len=dist(_longarm)- dist(_longax))] 
//     , "longAx2", [ onlinePt(_longarm, ratio=1.5)
//                 , onlinePt(_longarm, len=-len_2ndArm*1.5)
//                 ] 
     , "longAx", longAx
     , "longAx_Js", longAx_Js
     , "shortAx_Js", shortAx_Js
     , "boxPts", boxPts
     , "roundedness", roundedness
     , "half_width", half_width            
     , "shortAx", shortAx
     , "shortlongratio", shortLongRatio            
     , "aSSD",_aSSD
     , "quadAx02", _ax02
     , "quadAx13", _ax13
     , "center", _longarm[0]
     , "Note", "longAx2= longAx extended on each side by 50%; quadAx02=[quadCentroidPts[0],quadCentroidPts[1]]; center=longarm[0]= intsec of quadaxes" 
     ]
);


//========================================================
{ // shortside
function shortside(b,c)= sqrt(abs(pow(c,2)-pow(b,2)));

    shortside=["shortside","b,c","number","Geometry",
     " Given two numbers (b,c) as the lengths of the longest side and one of 
    ;; the shorter sides a right triangle, return the the other short side a.
    ;; ( c^2= a^2 + b^2 )."
    ,"longside"
    ];
    function shortside_test( mode=MODE, opt=[] )=
    ( doctest( shortside, 
        [
          [ shortside(5,4), 3, "5,4" ]
        , [ shortside(5,3), 4, "5,3" ]
        ], mode=mode, opt=opt
        )
    );
} // shortside

//######################################### 
{ // simpleChainData
function simpleChainData( 
    /* A stripped-down version of chainData for:
       1) used in marking like MarkPt, Mark90, etc
       2) as a backup function when chainData is needed 
          for bug fixing or upgrading
          
       It can do tapering, varying r's, and basecut, but
       NOT twisting or lofting    
    */      
    
        
    pts
    , nside 
    , r      // chain radius. Could be array. Items of array
             // could be arrays, too:
             //    r = 1
             //    r = [1,2] 
             //    r = [0,[2,1],1] 
    //, lastr  // for tapering
    
    , closed // The chain is closed (head connects to tail)
    , rot   // The entire chain rotate about its bone
            // away from the seed plane, which
            // is [ p0, p1, Rf ]

    //-------------------------------------------------
    // The following are used to generate data using 
    // randchain( data,n,a,a2... ) when data is a pts.
    // They are ignored if data is (2) or (3) 

    //         , n      //=3, 
    //         , a      //=[-360,360]
    //         , a2     //=[-360,360]
    //         , seglen //= [0.5,3] // rand len btw two values
    //         , lens   //=undef
    //         , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
    //                         //   , "clos
    , seed   //=undef // base pqr
    , Rf // Reference pt to define the starting
         // seed plane from where the crosec 
         // starts 1st pt. Seed Plane=[p0,p1,Rf]
            
    //-------------------------------------------------
    , basecut // Define cross-section. This would become
             // the first cross-section (i.e., chain head)
             // When defined, the r,rlast, and rs are ignored
    , tilt    
    , lasttilt   /* [rot,tilt,posttilt_rot]
                 tilt angle at first face( tilt) or last face
                 
                 x is a number btw 0~360, representing
                 pos on a clock face. Line XO will be the
                 axis about which the pl_PQRS will rot
                 and tilt by angle a to create the tilt.
                 
                          Q     .X 
                      .:'`|`':.'
                    :`    | .' `:  x
                   :______|'_____:____
                   :     O|      :P
                    '_    |    _'
                      '-._|_.-' 

                 
                 
                            /  
                           / Z
                       /  / /    /
                      / _Q_/    /
                     _-`  /`-_ /
                    R    O----P
                     `-_   _-`
                        `S`
                        
                 Let Z the last pt of bone. 
                 
                 Default x is 0, which means the tilt is
                 to tilt toward the first pt of xsec. i.e,
                 aPOZ = a. 
                 
                 If x is 90, toward the Q-direction on
                 the above graph, i.e., aQOZ=a 
                 
                 htilt could be a number a, in that case
                 default OQ is the axis. i.e., x=0       
              */
    , shape // default: chain
    //===============================================
    , _pl90at0 // The pl90 ( pts on the first 90-deg plane.
               // It is kept over the entire length of chain
               // till the last for the possible need of loft
    , opt=[] 
    , _rs=[]    
    , _cuts=[]
    ,_i=0
    , _debug)=
(   
   //echo(_red("--- simpleChainData()"), _i=_i)
   //echo(r=r)
   //echo(pts = pts)
   //echo(rot=rot)
   //echo( basecut = basecut)
   _i==0?
   
   let(//###################################### i = 0 
   
      //=========================================
      //    param initiation
        
      //  opt = popt(opt) // handle sopt (Removed 2018.5.16)
      //, 
      pts = ispt(pts[0])? pts 
              : haskey( pts, "pts")? hash(pts, "pts")
              : or(seed, hash(opt,"seed"))? seed
              : pqr()      
    
      //, rot    = und(rot, hash(opt,"rot",0 ))
      , rot    = arg(rot,opt,"rot",df=0)
      , basecut= or(basecut, hash(opt,"basecut", undef))
      //0, Rf0    = or(Rf, hash(opt,"Rf", undef))       
      , Rf0    = arg(Rf,opt,"Rf",undef)        
      
      , closed = len(pts)==2?false
                 : und(closed, hash(opt,"closed",false))
      
      , _tilt    = und( tilt, hash(opt,"tilt"))
      , _lasttilt= und( lasttilt, hash(opt,"lasttilt"))
      , tilt     = isnum(_tilt)?[0,_tilt]:_tilt
      , lasttilt = isnum(_lasttilt)?[0,_lasttilt]:_lasttilt
      
      , _nside = or( nside, hash(opt,"nside",4))
      , nside  = basecut? len(basecut):_nside
      
      , L = len(pts) 
      , rng= [0:L-1] 
      , r  = arg(r, opt, "r", 0.5) //und(r, hash(opt,"r", 0.5))
      //, lastr= und(r, hash(opt,"lastr", undef))
      
      , rs= let(L=len(pts))
            basecut? undef
            :isarr(r)?
             (len(r)< L?  // If rs < pts, repeat rs
               repeat(r, ceil( L /len(r)))
               :r
             )
            :repeat( [r], L )  
     
      //=========================================
      
      )
      //echo(rot    = rot)
      //echo(basecut= basecut)
      //echo(Rf0    = Rf0)
      //echo(closed = closed)
      //
      //echo(_tilt    = _tilt)
      //echo(_lasttilt= _lasttilt)
      //echo(tilt     = tilt)
      //echo(lasttilt = lasttilt)
      //
      //echo(_nside = _nside)
      //echo(nside  = nside)
        //
      //echo(rot    = rot)
      //echo(basecut= basecut)
      //echo(Rf0    = Rf0)
      //echo(closed = closed)
      //
      //echo(_tilt    = _tilt)
      //echo(_lasttilt= _lasttilt)
      //echo(tilt     = tilt)
      //echo(lasttilt = lasttilt)
      //
      //echo(_nside = _nside)
      //echo(nside  = nside)
      //echo(r  = r) 
      //echo(rs=rs)
      let(  
        coln_101= iscoline( get3(pts,-1) )
         )
      //echo(coln_101= coln_101 )
      let(  
         coln012 = iscoline( p012(pts))   
         )
      //echo(coln012 = coln012)
      let(
        // Note 2018.5.17: Changed from   
        //   coln123 = iscoline( p123(pts))
        // This fixed the WARNING invalid type for cross()
        //   seen in Arrow(), SimpleChain() and Dim() when 
        //   len(pts)=2.  
        coln123 = L<=3?false: iscoline( p123(pts)) 
        
      )
      //echo( p123 = p123(pts) ) 
      //echo(pts = pts )  
      //echo( chk = iscoline( p123(pts)))
      //echo(coln123 = coln123) 
     let(  
       _Rf = Rf0?Rf0
             : len(pts)==2? randOfflinePt( pts )
             : closed ? 
                ( coln012 ?   //   x . . .0---1
                              //  / 
                  ( coln123?  //   x . .0---1---2
                              //  /
                    randOfflinePt(p01(pts))
                    :pts[3]   //    x . . 0---1
                  )           //   /          | 
                : pts[2]      //   x . . 0
                ) 
             : 
               ( coln012?                     //   0---1---2
                  ( coln123 || len(pts)==3?   //   0--1--2--3
                    randOfflinePt(p01(pts))
                    : pts[3]                  //   0--1--2
                  )                           //        /   
               : pts[2]       //   0--1 
               )              //     /
               
      , Rf = rot? rotPt( _Rf, p01(pts), rot):_Rf 

      , newr= rs[0]
       
      , preP = onlinePt( p01(pts), -1)
      
      //====================================================             
      // Data pts are created on pl90, then projected to pl
      // to form cut:
                   
      // pl90 is the actual data pts created on a plane that is
      // 90d to the line p01. They will soon be projected to pl
      // if either tilt is defined or closed is true            
      , pl90= ispts(basecut) || ispts( basecut,2) ? 
               let( // Translate pts in the given basecut to the pl90
                    R90 = anglePt( [ preP, pts[0], Rf], a=90 )
                  , uvX = uv( pts[0], R90 )
                  , uvY = uv( pts[0], N( [preP,pts[0],R90] ) )        
                  ) 
                  [ for(P=basecut) pts[0]+ uvX*P.x + uvY*P.y ] 
                  
             // Rewrite to use new circlePts (18.8.6))     
             : circlePts( [ B([preP, pts[0], Rf])
                          , pts[0]
                          , N([preP, pts[0], Rf])
                          ], n=nside
                        , rad= isarr(newr)?newr[0]:newr        
                        )
             //: circlePts( [preP, pts[0], Rf], n=nside
                        //, rad= isarr(newr)?newr[0]:newr        
                        //)
                  
      , pl90_xtra= isarr(newr)?              
                 // Create xtra pl on the same _i 
                 arcPts( [preP, pts[0], Rf]  
                      , a=90                
                      , a2= 360*(nside-1)/nside
                      ,n=nside, rad= newr[1]
                      )
                 :undef
      )      
   
      //echo(_Rf = _Rf )              
      //echo(Rf = Rf) 
      //echo(newr= newr)     
      //echo(preP = preP )
      //echo(pl90= pl90)             
      //echo(pl90_xtra= pl90_xtra)
        
    let(  
      // pl is the actual plane onto which the pts placed 
      // on pl90 will be projected to. Since it is just a
      // pl to hold data, number and order of pts that form
      // this pl are not important.  
      // As a result, the posttilt_rot has no effect
      , pl = closed?
               ( let( pldata = planeDataAt(pts,0
                                      ,closed=true) )
                  hash( pldata,"pl")
               )
              :tilt[1]?
               ( let( pldata = planeDataAt( //pts,0, Rf=Rf 
                                        [pts[0],pts[1],Rf],0, Rf=Rf
                                      , rot= tilt[0]
                                      , tilt= tilt[1]==90?undef:tilt[1]
                                      ,closed=false) )
                  hash( pldata,"pl")
               ) 
               : pl90  
               
      // cut contains the actual data pts (on the plane pl)         
      , cut= closed|| tilt[1] ? 
                projPts( pl90, pl, along=p01(pts) ) 
                : pl90
      , cut_xtra= isarr(newr)?
                  (closed || tilt[1]? 
                    projPts( pl90_xtra, pl, along=p01(pts) ) 
                    : pl90_xtra        
                  ):undef  
      
      // _cuts will be carried to next pt 
      , _cuts= isarr(newr)? [ cut, cut_xtra]: [cut]
    
//       , _debug=str(_debug, "<hr/>"
//          ,_red("<br/>, <b>chaindata debug, _i</b>= "), _i 
//          ,"<br/>, <b>pts</b>= ", pts
//          ,"<br/>, <b>nside</b>= ", nside
//          ,"<br/>, <b>closed</b>= ", closed
//          ,"<br/>, <b>r</b>= ", r
//          ,"<br/>, <b>tilt</b>= ", tilt 
//          ,"<br/>, <b>Rf</b>= ", Rf
//          ,"<br/>, <b>rs</b>= ", rs
//          ,"<br/>, <b>newr</b>= ", newr
//          ,"<br/>, <b>preP</b>= ", preP
//          ,"<br/>, <b>pl90</b>= ", pl90
//          ,"<br/>, <b>pl90_xtra</b>= ", pl90_xtra          
//          ,"<br/>, <b>pl</b>= ", pl
//          ,"<br/>, <b>basecut</b>= ", arrprn(basecut, dep=2)
//          ,"<br/>, <b>cut</b>= ", cut
//          ,"<br/>, <b>pts_i</b>= ", arrprn(pts[_i],dep=2)
//          ,"<br/>, <b>_cuts</b>= ", _cuts
//        )//_debug
        
      )// let @ i=0
      
      //echo(pl = pl)            
      //echo(cut= cut)
      //echo(cut_xtra= cut_xtra)  
      //echo(_cuts= _cuts)
      
      //_debug
      simpleChainData( pts
               , nside=nside
               , closed=closed
               , rot=rot
               , Rf= Rf 
               , tilt=tilt
               , lasttilt=lasttilt 
               , _rs = rs              
               , _cuts=_cuts
               , _pl90at0 = pl90
               ,_i=_i+1
               ,_debug=_debug
               )                 
 
   : 0<_i && _i <len(pts)-1?
    //echo(_i=_i)
    let( //#################################### 0 < i < last 
   
       // dtwi = twist? twist/(len(pts)-1) :0
        prevcut = last(_cuts)
       // ,
        , boneseg = get( pts, [_i-1,_i] )
        , rs = _rs

        // The pl where previous data will project to
        //, pldataAt = planeDataAt( pts,_i)
        , pl_i = hash( planeDataAt( pts,_i), "pl")
      
        , newr = rs[_i] 
        
        , cut = projPl( pl1=prevcut
                      , pl2=pl_i
                      , along= boneseg
                      , newr= isarr(newr)?newr[0]:newr
                      //, twist=twist
                      )
                      
        , cut_xtra = isarr(newr)?
                        projPl( pl1= prevcut
                          , pl2=pl_i
                          , along= boneseg
                          , newr= newr[1]
                          //, twist=twist
                          )
                    : undef 
        
        ,_cuts= isarr(newr)? concat( _cuts,[cut,cut_xtra])
                              : app(_cuts,cut)                   

//       , _debug=str(_debug, "<hr/>"
//          ,_red("<br/>, <b>chaindata debug, _i</b>= "), _i 
//          ,"<br/>, <b>pts</b>= ", pts
//          ,"<br/>, <b>nside</b>= ", nside
//          ,"<br/>, <b>closed</b>= ", closed
//          ,"<br/>, <b>r</b>= ", r
//          ,"<br/>, <b>tilt</b>= ", tilt 
//          ,"<br/>, <b>Rf</b>= ", Rf
//          ,"<br/>, <b>rs</b>= ", rs
//          ,"<br/>, <b>newr</b>= ", newr
//          ,"<br/>, <b>pl_i</b>= ", pl_i
//          ,"<br/>, <b>basecut</b>= ", arrprn(basecut, dep=2)
//          ,"<br/>, <b>cut</b>= ", cut
//          ,"<br/>, <b>pts_i</b>= ", arrprn(pts[_i],dep=2)
//          ,"<br/>, <b>_cuts</b>= ", _cuts
//        )//_debug
        
        )// let @ i>0
      //echo(_i=_i)
      //echo(rs=rs)
      //echo( cut_xtra= cut_xtra )
      //echo(pts=pts) 
      simpleChainData( pts
               , nside=nside
              // , rs=rs
               , closed=closed
               , rot=rot
               , Rf= Rf 
               , tilt=tilt
               , lasttilt=lasttilt 
               , _rs = rs              
               , _cuts=_cuts
               //, _pl90at0 = pl90
               ,_i=_i+1
               ,_debug=_debug
               )                   
        
      : let( //#################################### i = last
          rs = _rs
        , newr = _rs[ len(pts)-1 ] // DONOT use last(_rs) 'cos _rs could 
                                   // be a repeat of shorter [r0,r1...] 
                                   // and might be longer than pts, so
                                   // last(_rs) will give wrong value

        , prevcut = last(_cuts)
        , boneseg = get(pts, [-2,-1])
        
        , pldata= closed?
                   planeDataAtQ( get3(pts,-2) ) 
                 : lasttilt && lasttilt[1]?
                   planeDataAt( pts,_i,
                        , rot= lasttilt[0] 
                        , tilt=lasttilt[1] 
                        , closed=closed
                        , Rf= last(_cuts)[0]) 
                   :planeDataAt(pts,_i,a=90)
                   
          // pl: where to project pts         
        , pl_i = hash( pldata, "pl")
        
        , cut = isarr(r) && last(r)==0? [last(pts)]
                :projPl( pl1= prevcut
                      , pl2=pl_i
                      , along= boneseg
                      , newr= isarr(newr)?newr[0]:newr
                      //, twist=twist
                      )
                      
        , cut_xtra = isarr(newr)?
                        projPl( pl1=prevcut
                          , pl2=pl_i
                          , along= boneseg
                          , newr= newr[1]
                         // , twist=twist
                          )
                    : undef 
        
        ,_cuts= isarr(newr)? concat( _cuts,[cut,cut_xtra])
                              : app(_cuts,cut)      
  
        // Adjust for cone, conedown, spindle, arrow:
        , _cuts_cone = rs[0]==0? concat( [[pts[0]]], slice( _cuts,1)):_cuts
        , cuts= last(rs)==0? 
                      concat( slice( _cuts_cone,0,-1), [[last(pts)]])
                      :_cuts_cone
   
//       , _debug=str(_debug, "<hr/>"
//          ,"<b>_i</b>= ", _i 
//          ,"<br/>, <b>nside</b>= ", nside
//          ,"<br/>, <b>cut</b>= ", cut
//          ,"<br/>, <b>closed</b>= ", closed
//          ,"<br/>, <b>pl_i</b>= ", pl_i
//          ,"<br/>, <b>pldata</b>= ", pldata
//          ,"<br/>, <b>lasttilt</b>= ", lasttilt 
//          ,"<br/>, <b>newr</b>= ", newr
//          ,"<br/>, <b>pts</b>= ", pts
//          ,"<br/>, <b>rs</b>= ", rs
//          ,"<br/>, <b>cut_xtra</b>= ", cut_xtra
//          ,"<br/>, <b>cuts</b>= ", arrprn(cuts, dep=2)
//          )//_debug      

        )
      //_debug
      
      //echo(_i=_i, _red("Prepare to leave SimpleChainData()") )
      //echo(r=r, rs=rs)
      //echo(len_cuts = len(cuts))
      //echo(_fmth(["_cuts",_cuts]), len(_cuts))
      //echo(_fmth(["_cuts_cone", _cuts_cone]), len(_cuts_cone))
      //echo(_fmth(["cuts", cuts ]), len(cuts))
      //echo(_i=_i, _red("Leaving SimpleChainData()") )
      
      ["cuts", cuts
        ,"rs", rs
         ,"Rf", Rf 
         
         ,"__<b>Args</b>__","==>"
         ,"bone", pts
         ,"nside",nside
         ,"r",r
         ,"tilt", tilt //[tilt,tiltax]
         ,"lasttilt", lasttilt //[lasttilt,lasttiltax]
         ,"closed", closed
         ,"rot", rot
         ,"basecut", basecut
         ,"prevcut", prevcut

         ,"seed", seed
         //,"loftpower", loftpower 
         ,"__<b>debug</b>__","==>"  
         ,"debug", _debug
         ]    
                
);  // simpleChainData 

} // simpleChainData

//========================================================
{ // sinpqr
function sinpqr( pqr )=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, T= othoPt( [P,R,Q] )
	, RT= dist([R,T])
	, RQ= dist([R,Q])
	, onQside = norm(T-P) > norm(Q-P)?1:-1
	)
(
	sign(onQside)* sin( angle([R,Q,T]) )
);

    sinpqr= ["sinpqr","pqr","number","Math,Geometry",
     " Given a 3-pointer (pqr), return the len= sin( RT/QR ) where T is 
    ;; the R's projection on PQ line (T=othoPt([P,R,Q])).
    ;;
    ;;           R
    ;;          /| 
    ;;         / |.'
    ;;        / .'T
    ;;       /.'
    ;;  ----Q------
    ;;    .'
    ;;   P
    "]; 

    function sinpqr_test( mode=MODE, opt=[] )=
    (
        doctest( sinpqr,[],mode=mode, opt=opt )
    );
       
} // sinpqr

//========================================================
{ // slope 
function slope(p,q=undef)= q!=undef?
						((q[1]-p[1])/(q[0]-p[0]))
						:((p[1][1]-p[0][1])/(p[1][0]-p[0][0]));

    slope= [ "slope", "p,q=undef", "num",  "Math,Geometry" ,
    " Return slope. slope(p,q) where p,q are 2-D points or
    ;; slope(p) where p = [p1,p2]\\
    "
    ];
    function slope_test( mode=MODE, opt=[] )=
    (	
        doctest( slope, 
         [ 
            [ slope([0,0],[4,2]), 0.5,[[0,0],[4,2]] ]
         ,  [ slope( [[0,0],[4,2]] ), 0.5,[ [[0,0],[4,2]] ] ]
         ,  [ slope( [[0,0],[4,2]] ), 0.5,[ [[1,1],[4,2]] ] ]
         ],mode=mode, opt=opt
        // , [["echo_level", 1]]	
        )
    );

    //slope_test( ["mode",22] );
} // slope

//========================================================
{ //smoothPts
function smoothPts( 
          pts
        , n  //=6
        , aH // =0.5
        , dH // =0.38
        , closed //=false
        //, sameintv=false // Add pts to make intvs more or 
                         // less the same as smallest one
                         // Note that this means the # of
                         // pts is unpredictable
        , details //=false  // When false, ret pts; When true,
                         // ret [ bzAt, bzAt ....] where
                         // bzAt is the ret of getBezierPtsAt
                         // at each pt, a hash w/ following keys:
                         //  bzpts,pts,Hi,Hj,aH,dH,i,n,pqrs
        , rng // indices where smoothing applied to. 
              // default: range(pts) when closed
              //          range(0,len(pts)-1) when open
        , opt=[] 
        )=
/*
   Assume we have 4 pts: 0~3 (PQRS)
   To add pts between 0~1 or 2~3, we need to add extra pts,
   B and E, resp., to generate HCP (Handler Control Pts) 
   
                                      
                                     E
           _-P-----Q-_              : 
        _-`           `-_     __..-S 
      B-                 `R-``        
                         
                         
   getBezierPtsAt( 
   pts
   , at=1 
   , n=6 // extra pts to be added 
   , closed=false
   , ends= [true,true]
   , aH= undef //[1,1]
   , dH= undef //[0.5,0.5]
   , opt=[]
   )                
                   
*/
(
   let(
   
      //-----------------------------------------
      
        opt= popt(opt) // handle sopt
        
      , pts= or(pts, hash(opt,"pts") )
      , n  = n>0? n: hash(opt,"n", 6 )
      , _aH = und( aH, hash(opt,"aH", 0.5) )
      , aH  = isarr(_aH)?_aH:[_aH,_aH]
      , _dH = und( dH, hash(opt,"dH", 0.38) )
      , dH  = isarr(_dH)?_dH:[_dH,_dH]
      , closed= und(closed,hash( opt, "closed", false))
      
      //-----------------------------------------

      , rtn= [ for(i=[ 0
                      : len(pts)-(closed?1:2)])
                let ( bzrtn = getBezierPtsAt( pts
                              , i=i, n=n
                              , aH=aH, dH=dH
                              , closed=closed
                              , ends=[ i==0
                                     , closed && (i==len(pts)-1)
                                          ?false:true] 
                              )
                    )
                details? bzrtn:hash(bzrtn,"bzpts")
//                 : i==0? bzrtn[0]
//                 : pts[1]==last(bzrtn[0]) // last seg when closed
//                     ? slice(bzrtn[0],1,-1)
//                 : slice(bzrtn[0],1)
            ]
      )
   details? rtn:joinarr(rtn)
);   
} // smoothPts
 


//========================================================
{ // smoothPts_new
function smoothPts_new( 
        pts
        , n=6
        , aH=0.5
        , dH=0.38
        , closed=false
        , details=false
        , opt=[]
        )=
/*
   Assume we have 4 pts: 0~3 (PQRS)
   To add pts between 0~1 or 2~3, we need to add extra pts,
   B and E, resp., to generate HCP (Handler Control Pts) 
   
                                      
                                     E
           _-P-----Q-_              : 
        _-`           `-_     __..-S 
      B-                 `R-``        
                         
                         
   getBezierPtsAt( 
   pts
   , at=1 
   , n=6 // extra pts to be added 
   , closed=false
   , ends= [true,true]
   , aH= undef //[1,1]
   , dH= undef //[0.5,0.5]
   )                
                   
*/
(
   let( 
        //n=n==undef?6:n
//      , aH= aH==undef?0.5:aH
//      , dH= dH==undef?0.38:dH
        n = hash(opt,"n", n==undef?6:n )
      , aH= hash(opt,"aH", aH==undef?0.5:aH )
      , dH= hash(opt,"dH", dH==undef?0.38:dH )
      , closed= hash(opt,"closed",closed)
      , details= hash(opt,"details",details)
      
      , rtn= [ for(i=[ 0
                      : len(pts)-(closed?1:2)])
                let ( bzrtn = getBezierPtsAt( pts
                              , at=i, n=n
                              , aH=aH, dH=dH
                              , closed=closed
                              , ends=[ i==0
                                     , closed && (i==len(pts)-1)
                                          ?false:true] 
                              )
                    )
                details? bzrtn:bzrtn[0]
//                 : i==0? bzrtn[0]
//                 : pts[1]==last(bzrtn[0]) // last seg when closed
//                     ? slice(bzrtn[0],1,-1)
//                 : slice(bzrtn[0],1)
            ]
      )
   details? rtn:joinarr(rtn)
);   
} // smoothPts_new

function sortByDist(pts, Rf, isAbs=0)=  // 2018.917
(
   // sort pts by dist to Rf
   // Return array of [i, pt, dist]
   // Rf could be pt, line or plane
   // The dist could be <0 (when Rf is a plane). Set isAbs=1 to 
   //    make all dist positive. This allows detection of the
   //    pt cloest to Rf
    
   isAbs?
   sortArrs(
    [ for(i=range(pts)) [ i, pts[i], abs(dist(pts[i],Rf)) ] ], by=2  
    )
   : sortArrs(
    [ for(i=range(pts)) [ i, pts[i], dist(pts[i],Rf) ] ], by=2  
   )
); 

//function sortPisByDist(pts, Rf)= // 2018.5.10
//(  
//   // Return indices of pts indicating the order of sorted distances between pt and ref
//   // Ref: sortByDist 
//   echo("sortPisByDist()") 
//   let( dpis = [ for(pi= [0:len(pts)-1]) [dist( pts[pi], Rf), pi] ] 
//      )
//      
//   echo( Rf = Rf )
//   echo( dpis = dpis )/// quicksort(spts))   
//   [ for(dpi= quicksort(dpis)) dpi[1] ]
//);  

//function _spherePts_get3rd(pq)= // 181011: what is this for?
//(
//  /*  Return R that:
//  
//        dPR = dQR = dPQ   and OR = OP = OQ
//        
//                  R           
//            d   .'|     
//             .'   |\            O (ORIGIN)
//       Q  .'      | \ d   _.-' 
//        ':._      |  \.-'`  
//            '-.D..J'` \
//             d   '-._  \
//                     '-.\ P
//  */
//  let( //d= dist(pq)
//     //, dOR = norm(P)
//     //, dRD = d*sqrt(3)/2
//     //, 
//     D = (pq[0]+pq[1])/2
//     , aDOR = anglebylen( norm(pq[0])      // = dOR 
//                        , dist(pq)*sqrt(3)/2  // = dRD
//                        , dist(ORIGIN,D) )
//     )
//     anglePt( [ ORIGIN, D, normalPt([ORIGIN,D,pq[1]]) ]
//            , a=aDOR, len=norm(pq[0]) 
//            )
//);
 
function spherePF(r=1,n=1,_pts=[],_fcs=[])=
(  
   // Modified from berkenb's excellent ssphere:
   // http://forum.openscad.org/Coating-a-sphere-with-bumps-golf-ball-ish-for-wheel-treads-tp24090p24094.html
   // return [pt, faces]

  _pts==[]?
    let(
        C0 = (1+sqrt(5))/4 
      , pts = [ [0.5,0,C0],[0.5,0,-C0],[-0.5,0,C0],[-0.5,0,-C0]
              , [C0,0.5,0],[C0,-0.5,0],[-C0,0.5,0],[-C0,-0.5,0]
              , [0,C0,0.5],[0,C0,-0.5],[0,-C0,0.5],[0,-C0,-0.5]] 
      , fcs = [ [10,2,0],[5,10,0],[4,5,0],[8,4,0],[2,8,0]
              , [11,1,3],[7,11,3],[6,7,3],[9,6,3],[1,9,3]
              , [7,6,2],[10,7,2],[11,7,10],[5,11,10],[1,11,5]
              , [4,1,5],[9,1,4],[8,9,4],[6,9,8],[2,6,8]]       
      , npts = [for (v=pts) v/norm(v) ]
    )
    ( n==0?[npts, fcs]
      : spherePF(r=r,n=n-1,_pts=npts,_fcs=fcs)
    )
  : let(
      spts = concat(_pts
                    , [ each for (f=_fcs) 
                       [(_pts[f[0]]+_pts[f[1]])
                       ,(_pts[f[1]]+_pts[f[2]])
                       ,(_pts[f[2]]+_pts[f[0]])
                       ]
                      ]
                    )
    , nsfcs = [each for (i=[0:len(_fcs)-1]) 
               [ [_fcs[i][0], len(_pts)+3*i+0, len(_pts)+3*i+2]
               , [len(_pts)+3*i+0, len(_pts)+3*i+1, len(_pts)+3*i+2]
               , [len(_pts)+3*i+0, _fcs[i][1], len(_pts)+3*i+1]
               , [len(_pts)+3*i+2, len(_pts)+3*i+1, _fcs[i][2]]
               ]
              ]
    , nspts = [for (v=spts) r*v/norm(v)] 
    )
    n==0? [nspts, nsfcs]  
    : spherePF(r=r,n=n-1,_pts=nspts,_fcs=nsfcs) 
); 
 
function spherePts_185q(r, site, actions, debug=0, opt=[])= 
(  // 2018.5.26 
   // https://en.wikipedia.org/wiki/Regular_icosahedron
   
   /*
     dPR = dQR = dPQ   and OR = OP = OQ
        
                  R           
            el  .'|\     
             .'   | \            O (ORIGIN)
       Q  .'      |  \ el  _.-' 
        ':._      |   \.-'`  
            '-._..J'`  \
              M '-._    \
           el       '-._ \ 
                        '-'P                  
   */
   
   let( r = arg(r,opt,"r",2)                // dist from center to one corner
      , site = arg(site, opt, "site", []) 
      , actions = argx(actions,opt,"actions",[])  
      , el = r/ sin(72)  // edge length 
      , dMR = sqrt( el*el- (el/2)*(el/2) )
      , dMO = sqrt( r*r- (el/2)*(el/2) )
      , aPOQ = anglebylen( r,el,r )
      , aROM = anglebylen( r, dMR, dMO )  
        , P0 = [0,0,r]
        , pts0 = [ for(i=[0:4]) 
                    anglePt( [ [1,0,0], ORIGIN, [0,1,0] ]
                           , a= 360/5*i, a2= 90-aPOQ, len=r )
                 ]
        , pts1 = [ for(i=[0:4]) 
                    anglePt( [ [1,0,0], ORIGIN, [0,1,0] ]
                           , a= 360/5*i-180/5, a2= 90-2*aROM, len=r )
                 ]     
        , pts = concat( [P0], pts0, pts1, [addz(P0, -2*r)] )
        //, faces=[]
        , faces= [ [0,2,1],[0,3,2],[0,4,3],[0,5,4],[0,1,5]
                 , [1,2,7],[2,3,8],[3,4,9],[4,5,10],[5,6,1]
                 , [1,7,6],[2,8,7],[3,9,8],[4,10,9],[5,6,10]
                 , [6,7,11],[7,8,11],[8,9,11],[9,10,11],[10,6,11]
                 ]
        ) 
     debug?
       echo(r=r)
       echo(el=el)
       echo( aPOQ= aPOQ )
       echo( aROM= aROM )
       echo(a=a)
       //echo(a2=a2)
       echo(P0=P0)
       //echo(P1=P1)
       //echo(P1j=P1j)
       //echo(nP1j=nP1j)
       //echo(P2345=P2345)
       [pts,faces]    
     : [pts,faces]
   
);
 
function spreadActions(objlen, spreadlen, n=3, dir="x", offset=O)=
(  /* Designed for woodworking (2018.6.28).
      2018.7.30: extended to 2d and 3d.
      
      Return a list of actions for an obj of objlen(=ol) to duplicate and
      spread across a full spreadlen(=sl) along the *dir* where *dir* is 
      "x"|"y"|"z". n = total # of objs  
     
     |------------ sl ------------|
     +----------------------------+   n = 3 
     |    |      |    |      |    |
     |____|      |____|      |____|
       ol
     
     The return list of actions contains n # of "actions" hashes. 
     For the above example, 
     
       sas = spreadActions(ol,sl,n=3, offset=pt)
       
     returns an array of [<dir>,<actions>], something like: 
     
       sas = [ ["x", 0, "t", pt]
             , ["x", 3, "t", pt]
             , ["x", 6, "t", pt]
             ]
     
     The sas can then be used in one of the 2 ways:
     
       1) for(act=sas) Obj(actions=act)
       2) for(act=sas) Transforms(act) Obj()  
            
     dir: 1d: "x"|"y"|"z"
          2d: "x"(leave it to indicate default "xy") |"xy"|"xz"|"yz"
          3d: no use
     
     offset: how far away from the origin, like: [2,3,1] 
             It's a translation applied to the actions. 
      
     Usage:
     (in module Legs() of planter_bench.scad )
   
      sas = spreadActions( objlen=LEN4, spreadlen=outer_l,n=3);
      echo(sas = sas);
      for(act=sas) Obj(actions=act); 
      
      For 2d:
      for(act = 
           spreadActions( objlen=[LEN2,LEN2]
                        , spreadlen=[L, W]
                        , n=[3,2]
                        , dir= "yz"
                        , offset=[1,1,0]
                        );
          ) Transforms(act) Obj();
      
   */
   isnum(objlen)?
   
     let( intv_w = (spreadlen- objlen*n)/(n-1))
     [for(i=[0:n-1]) [dir, (intv_w+objlen)*i, "t", offset] ]
     
   :len(objlen)==2?
     //echo( "2d sas" )
     /// For 2d, dir could be: xy, x(=xy), yz, xz  
     let( i1= dir=="x"||dir=="xy"||dir=="xz"?0:1
        , i2= dir=="xz"||dir=="yz"?2:1
        , xyz= [ dir=="x"|| dir[0]=="x"? 1:0
               , dir=="x"|| dir=="xy" || dir=="yz"? 1:0
               , dir=="xz" || dir=="yz"? 1:0 
               ]
        , intv_1 = (spreadlen[0]- objlen[0]*n[0])/(n[0]-1)
        , intv_2 = (spreadlen[1]- objlen[1]*n[1])/(n[1]-1)
        , rtn=[ for(j=[0:n[1]-1])
                 for (i=[0:n[0]-1])  
                   let(t= 
                   ["t", [ xyz[0]*(intv_1+objlen[0])*i
                           
                           // xyz= [1,1,0] or [0,1,1]  
                         , xyz.x==0?  
                             xyz[1]*(intv_1+objlen[0])*i
                           : xyz[1]*(intv_2+objlen[1])*j
                         
                         , xyz[2]*(intv_2+objlen[1])*j
                         ]+offset
                   ])
                   //echo(i=i,j=j,t=t)
                   t       
              ]
        )
     //echo(intv_w = intv_w)
     //echo(i1=i1, i2=i2, xyz=xyz)
     //echo(rtn=rtn)
     rtn 
      
   : let( intv_x = (spreadlen[0]- objlen[0]*n[0])/(n[0]-1)
        , intv_y = (spreadlen[1]- objlen[1]*n[1])/(n[1]-1)
        , intv_z = (spreadlen[2]- objlen[2]*n[2])/(n[2]-1)
        )
     //echo(intv_w = intv_w)   
     [ for(zi=[0:n[2]-1])
        for (yi=[0:n[1]-1])  
         for (xi=[0:n[0]-1])  
           ["t", [ (intv_x+objlen[0])*xi
                 , (intv_y+objlen[1])*yi
                 , (intv_z+objlen[2])*zi
                 ]+offset
           ]
     ]     
);

function spreadCornerActions(site)=
(  /* Designed for woodworking (2018.6.28).
      Return a list of 4 actions for an obj to spread to the 4 corners
      defined by a special *site* (i.e., aPQR = 90) :
      
          site=pqr
      
        R                   R---------S 
        |             ==>   |         |
        |                   |         |
        Q----------P        Q---------P      
      
     R +--+-------------+--+ S
       |  /             \  |
       +''               ''+ 
       |                   | 
       +..               ..+
       |  \             /  | 
     Q +--+-------------+--+ P
      
   Usage:
   (in module Legs() of planter_bench.scad )
   
   for(act=spreadCornerActions([ X*outer_l, ORIGIN, Y*outer_w] ))
      Inleg_corner(actions=act);
      
         
      
   */
   let(P=site[0], Q=site[1],R=site[2]
      )
   [[]
   ,["mirror",[(P+Q)/2,Q]] 
   ,["mirror",[(R+Q)/2,Q]]
   ,["mirror",[(P+Q)/2,Q], "mirror",[(R+Q)/2,Q]]
   ]
);

 
//========================================================
{ // squarePts
//\\
// If size is given:\\
//\\
//         size
//   p---------------n
//   |'-           -'|
//   |  '-       -'  |
//   |    '-   -'    |
//   |      'x'      |
//   |        '-     |
//   |          '-   |
//   |            '- |
//   m---------------q

/*
;; Given [p,q,r], return [P,Q,N,M]
;;   P-------Q
;;   |      /|
;;   |     / |
;;   |    /  |
;;   M---R---N
;;
;;   P------Q
;;   |'-_   |:
;;   |   '-_| :
;;   |      '-_:
;;   |      |  ':
;;   M------N    R
;;
;; New 2014.7.15: new OPTIONAL arg: p, r to set size. 
;;
;;          p  
;;   P---|-----Q
;;   |   |    /| 
;;   |   |   / | r
;;   |   +--/---
;;   |     /   |
;;   '----R----'
*/
function squarePts(pqr=pqr(), p=undef, r=undef)=
(
  p
  ?( r
     ? squarePts( [ anglePt(pqr,a=90, len=r) //anyAnglePt(pqr,a=90, len=r,pl="xy")
			    , Q(pqr)
			    , onlinePt(p10(pqr),len=r) 
			    ] )
     : squarePts( [ onlinePt(p10(pqr),len=p), Q(pqr), R(pqr) ] )	 
   ) 
  : r
    ? squarePts( [ P(pqr)
			    , Q(pqr)
            , anglePt( pqr, a=90, len=r)
			    //, anyAnglePt(pqr,a=90, len=r,pl="xy")	
			    //, onlinePt(p12(pqr),dist=r) 
			    ] )
    : [  pqr[0]
	  , pqr[1]
	  , cornerPt( [pqr[2], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[1]]) 
	  , cornerPt( [pqr[0], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[2]])
	  ]
); 

    squarePts=["squarePts", "pqr,p=undef,r=undef", "pqrs", "Point",
    " Given a 3-pointer pqr, return a 4-pointer that has p,q of the
    ;; given pqr plus two pts, together forms a square passing through
    ;; r or has r lies along one side. This is useful to find the
    ;; square defined by p,q,r.
    ;;
    ;; Given [p,q,r], return [P,Q,N,M]
    ;;   P-------Q
    ;;   |      /|
    ;;   |     / |
    ;;   |    /  |
    ;;   M---R---N
    ;;
    ;;   P------Q
    ;;   |'-_   |:
    ;;   |   '-_| :
    ;;   |      '-_:
    ;;   |      |  ':
    ;;   M------N    R
    ;;
    ;; New 2014.7.15: new OPTIONAL arg: p, r to set size. 
    ;;
    ;;          p  
    ;;   P---|-----Q
    ;;   |   |    /| 
    ;;   |   |   / | r
    ;;   |   +--/---
    ;;   |     /   |
    ;;   '----R----'
    "
    ];

    function squarePts_test( mode=MODE, opt=[] )=
    (
        let( pqr = randPts(3)
           , sqs = squarePts( pqr )
           )
        
        doctest( squarePts, 
        [
          [ is90( [for(i=[0,1,2]) sqs[i] ] ) , true, "pqr"
            , ["myfunc", "is90( [ for(i=[0,1,2]) squarePts{_}[i] ] ) "]
          ]

          ,[ is90( [for(i=[1,2,3]) sqs[i] ] ), true, "pqr", 
            , ["myfunc", "is90( [ for(i=[1,2,3]) squarePts{_}[i] ] )"]
          ]

        ,[ is90( [for(i=[2,3,0]) sqs[i] ] ), true, "pqr", 
            , ["myfunc", "is90( [ for(i=2,3,0]) squarePts{_}[i] ] )"]
          ]
        ], mode=mode
         )
    );

} // squarePts

//========================================================
function sum_dist(pts,pt)= 
( 
  //echo( log_b("sum_dist():", layer=4))
  //echo( log_( ["sum_dist.pts", pts], 4) )
  //echo( log_( ["sum_dist.pt", pt],4) )
  let( sod = sum( [for(p=pts) 
                     //echo( log_b("Summing up in sum_dist()",5 ) )
                     //echo( log_( ["p",p, "pt", pt],5 ) )
                     let(rtn = abs(dist(p,pt)) )
                     //echo( log_e(layer=5 ) )
                     rtn 
                  ] )
     )  
  //echo( log_( ["sod", sod],4) )
  //echo( log_e(layer=4))
  sod
  //sum( [for(p=pts) 
          //abs(dist(to3d(p),to3d(pt))) 
       //] )
);
function sum_dist2(pts,pt)= sum( [for(p=pts) pow(dist(to3d(p),to3d(pt)),2) ] );



function SSD(pts, target)= // Sum of Squared Dist. Target could be pt, line or plane. 2018.8.12
(
  sum( [for(p=pts) pow( dist(p,target),2) ] ) 
);

    SSD=[ "SSD", "pts,target", "array", "Point",
     "Sum of Squared Dist. Target could be a pt, line or plane. 2018.8.12
    "
    ,"sd, avg_SSD"
    ];

    function SSD_test( mode=MODE, opt=[] )=
    (   
        let( pts= [[2,3,4],[5,6,7]] )
        doctest( SSD,
        [ 
        ]
        ,mode=mode, opt=opt
        )
    );



function avg_SSD(pts, target)= // averaged Sum of Squared Dist. 2018.8.12
(
  sum( [for(p=pts) pow( dist(p,target),2) ] ) / len(pts) 
);

    avg_SSD=[ "avg_SSD", "pts,target", "array", "Point",
     "averaged Sum of Squared Dist. 2018.8.12. Target could be a pt, line or plane. 2018.8.12
    "
    ,"sd, SSD"
    ];

    function avg_SSD_test( mode=MODE, opt=[] )=
    (   
        let( pts= [[2,3,4],[5,6,7]] )
        doctest( avg_SSD,
        [ 
        ]
        ,mode=mode, opt=opt
        )
    );
    

//========================================================
{ // symedianPt
function symmedianPt(pqr=pqr())=

   let( P=pqr[0], Q=pqr[1], R=pqr[2] )
(
    onlinePt( [centroidPt(pqr), angleBisectPt(pqr)], len=2)
);    
} //symedianPt

symmedianPt=["symmedianPt","pqr","Point","Point"
, "Return the symmedian pt of pqr, which is the S below:
;; 
;;    C---A---S
;;
;; where C = centroid pt, A= angleBisectPt of pqr.
"];

function symmedianPt_test( mode=MODE, opt=[] )=
(
    doctest( symmedianPt,[],mode=mode, opt=opt )
);

//. t
//========================================================
{ // tanglnPt_P
function tanglnPt_P(pqr, len)=
(  /*
       tangent line pt at P

               D    T
       Q ---R--+----+-
          `-_  |  -`
             `-|-`
               P 
       For pqr, the tangent line, PT, to the circle centering
       Q and passing thru P, can be obtained knowing:
       
       PD^2 = QD * DT
       
       T can also be obtained with anglePt( QPR, a=90)
       
    */
  anglePt( p102(pqr),a=90,len=len)
//   let( P=P(pqr), Q=Q(pqr), R=R(pqr), D=othoPt( [R,P,Q] )
//      , T= onlinePt( [D,Q], len= -pow( dist(P,D),2)/ dist(Q,D) )
//      )
//   len?onlinePt([P,T],len=len) :T
);
} // tanglnPt_P

//========================================================
{ // tangLn
function tangLn(pqr, len)= // Return: [A,B,abi]
(  // tangent line 90-deg to angle bisect line of pqr 
   // len: int (extend to P and Q sides dir by len)
   //      arr (extend to P by len[0], Q by len[1]
   /*
      tangent line is the line passing Q and 90d to Q-Abi
      where abi is the angle bisect point.

              abi    
       P -     +    R
          `-_  |  -`
             `-|-`
          A----Q---B
               
      len=[a,b] where a= dAQ, b= dBQ. 
      or =c to set both the same 
      or if not set, = [ dPQ/2, dQR/2] 
   */    
   
   let( len= len==undef? [ d01(pqr)/2, d12(pqr)/2 ]
             :isarr(len)? len
             : [len,len]
      , abi = angleBisectPt( pqr )
      , tPt = anglePt( [abi,pqr[1],pqr[2]], a=90 )
      )
   [ onlinePt( [pqr[1], tPt], len=-len[0] )
   , onlinePt( [pqr[1], tPt], len=len[1] )
   , abi
   ]
);
} // tangLn

//========================================================
function textPts()=
(
3
   // https://www.microsoft.com/en-us/Typography/default.aspx
   // http://nodebox.github.io/opentype.js

);

//module move(pqr,stu){  // move an obj from pqr to stu. 
//                       // Note this is used for obj.
//    
//   P=pqr[0]; Q=pqr[1]; R=pqr[2];
//   S=stu[0]; T=stu[1]; u=stu[2];
//    
//    
//    
//}    

{ // tocoord
function tocoord(pt, coord)= 
    !ispt(pt)? [for(p=pt) tocoord(p, coord) ]
    :let( pqr= len(coord)==4? [for(i=[1,2,3]) coord[i]-coord[0] ]:coord
       , ijk = solve3( transpose( pqr), pt- (len(coord)==4?coord[0]:O) )
       )
       //echo( "in_tocoord,", pqr=pqr)
(  ijk 
);
} // tocoord

//========================================================
function transPts(pts,actions=[], keep=false, _newpts=[], _i=0
                 , _debug="--- DEBUG: transPts() ---")=
(
   // See its counter part for Premitives: Transform()

  // Allow "x","y","z", "rotx","roty","rotz" for opt key. 
  // Update Transform() accordingly (2017.7.6)
  
   // fix bug (when opt=[]) and major recode to let code clearer and easier 
   //  to debug (2017.5.19)
   
   actions==[]? pts
   : _i>= len(actions)? (keep? concat( pts, _newpts): _newpts)
   : _i%2==0 ? transPts(pts=pts,actions=actions
                       , keep=keep,_newpts= _i==0?pts:_newpts
                       ,_i=_i+1, _debug=_debug)
   : let(
     _newpts= actions[_i-1] == "rot"?
                     let( x_ax = [[1,0,0],ORIGIN]
                         , y_ax = [[0,1,0],ORIGIN ]
                         , z_ax = [[0,0,1],ORIGIN]
                         )
                     rotPts(rotPts(rotPts(_newpts,x_ax, actions[_i][0])
                                  ,y_ax, actions[_i][1])
                            ,z_ax, actions[_i][2])
             : actions[_i-1] == "rotx"? rotPts(_newpts, [[1,0,0],ORIGIN], actions[_i] )
             : actions[_i-1] == "roty"? rotPts(_newpts, [[0,1,0],ORIGIN], actions[_i] )
             : actions[_i-1] == "rotz"? rotPts(_newpts, [[0,0,1],ORIGIN], actions[_i] ) 
             : actions[_i-1] == "rota"? rotPts(_newpts, actions[_i][0], actions[_i][1] )
             : actions[_i-1] == "x" ? [ for(p=_newpts) p+ [actions[_i],0,0] ]
             : actions[_i-1] == "y" ? [ for(p=_newpts) p+ [0,actions[_i],0] ]
             : actions[_i-1] == "z" ? [ for(p=_newpts) p+ [0,0,actions[_i]] ]
             : actions[_i-1] == "transl" 
               || actions[_i-1] == "tsl" 
               || actions[_i-1] == "t"? [ for(p=_newpts) p+ actions[_i] ]
             : actions[_i-1] == "mirror"
               || actions[_i-1] == "m" ? mirrorPts( _newpts, actions[_i] ) //2017.7.14  
             : _newpts
     , _debug = str( _debug
               , ", _i = ", _i
               , ", act= ", actions[_i-1]
               , ", val= ", actions[_i]
               , ", pts= ", pts
               , ", _newpts= ", _newpts
               )    
    )
    transPts(pts=pts,actions=actions, keep=keep,_newpts=_newpts,_i=_i+1,_debug=_debug)   
);
 
function translatePts(pts,pt)= [for(p=pts)p+pt];
 
 //a=[1,2,3];
 //echo( transPts([a], ["x", 3,"y", 1] ) );
           
//                   let( x_ax = [[1,0,0],ORIGIN]
//                       , y_ax = [[0,1,0],ORIGIN ]
//                       , z_ax = [[0,0,1],ORIGIN]
//                       )
//                   rotPts(rotPts(rotPts(__newpts,x_ax, opt[_i][0])
//                                ,y_ax, opt[_i][1])
//                          ,z_ax, opt[_i][2])
//                   
//                       
//                    : has( ["transl","tsl","t","x","y","z"], act)?
//                    //act=="transl" || act=="tsl" || act=="t" ?
//                    [ for(p=__newpts) p+val ]
//                    :pts
//      
//     let( __newpts= _i==1? pts: _newpts
//        , act = opt[_i-1]
//        
//        
//        , val = opt[_i]? ( act=="x"? [val,0,0]
//                         : act=="y"? [0,val,0]
//                         : act=="z"? [0,0,val] 
//                         : opt[_i]
//                         )
//                : [0,0,0]
//        , _newpts = act=="rot"?
//                      let( x_ax = [[1,0,0],ORIGIN]
//                       , y_ax = [[0,1,0],ORIGIN ]
//                       , z_ax = [[0,0,1],ORIGIN]
//                       )
//                       rotPts(rotPts(rotPts(__newpts,x_ax,val[0]),y_ax,val[1]),z_ax,val[2])
//                    : has( ["transl","tsl","t","x","y","z"], act)?
//                    //act=="transl" || act=="tsl" || act=="t" ?
//                    [ for(p=__newpts) p+val ]
//                    :pts
//       , _debug = str( _debug
//                     , ", _i = ", _i
//                     , ", act= ", act
//                     , ", val= ", val
//                     , ", pts= ", pts
//                     , ", _newpts= ", _newpts
//                     )             
//       ) transPts(pts=pts,opt=opt, keep=keep,_newpts=_newpts,_i=_i+1,_debug=_debug)                      
//);
     
// function transPts(pts,opt, keep=false, _newpts=[], _i=0
//                 , _debug="--- DEBUG: transPts() ---")=
//(
//   // fix bug (when opt=[]) and major recode to let code clearer and easier 
//   //  to debug (2017.5.19)
//   
//   opt==[]? pts
//   : _i>= len(opt)? (keep? concat( pts, _newpts): _newpts)
//   : _i%2==0 ? transPts(pts=pts,opt=opt, keep=keep,_newpts=_newpts,_i=_i+1, _debug=_debug)
//   : let( __newpts= _i==1? pts: _newpts
//        , act = opt[_i-1]
//        , val = opt[_i]? ( act=="x"? [val,0,0]
//                         : act=="y"? [0,val,0]
//                         : act=="z"? [0,0,val] 
//                         : opt[_i]
//                         )
//                : [0,0,0]
//        , _newpts = act=="rot"?
//                      let( x_ax = [[1,0,0],ORIGIN]
//                       , y_ax = [[0,1,0],ORIGIN ]
//                       , z_ax = [[0,0,1],ORIGIN]
//                       )
//                       rotPts(rotPts(rotPts(__newpts,x_ax,val[0]),y_ax,val[1]),z_ax,val[2])
//                    : has( ["transl","tsl","t","x","y","z"], act)?
//                    //act=="transl" || act=="tsl" || act=="t" ?
//                    [ for(p=__newpts) p+val ]
//                    :pts
//       , _debug = str( _debug
//                     , ", _i = ", _i
//                     , ", act= ", act
//                     , ", val= ", val
//                     , ", pts= ", pts
//                     , ", _newpts= ", _newpts
//                     )             
//       ) transPts(pts=pts,opt=opt, keep=keep,_newpts=_newpts,_i=_i+1,_debug=_debug)                      
//);    
                    
//function transPts(pts,actions, keep=false, _src=[])=
//(
//   actions==[]?(keep? concat( pts,_src): _src)
//   : let( //op = actions[0]
//        //, 
//          act = keys(actions)[0] //op[0]
//        , val = hash( actions, act ) //op[1]
//        , _src = _src==[]?pts:_src
//        , newops = slice(actions,2)
//        , newpts =  act=="transl" || act=="tsl" || act=="t" ?
//                    [ for(p=_src) p+val ]
//                  : act=="rot" ? 
//                    let( x_ax = [[1,0,0],ORIGIN]
//                       , y_ax = [[0,1,0],ORIGIN ]
//                       , z_ax = [[0,0,1],ORIGIN]
//                       )
//                    rotPts(rotPts(rotPts(_src,x_ax,val[0]),y_ax,val[1]),z_ax,val[2])
//                  :pts
//        ) newops==[]? (keep? concat( pts,newpts): newpts)
//                    : transPts( pts, newops, keep, _src=newpts )            
//);

//========================================================
function translAB(pts, d=1, keep=false)=  // Translate along the angle-bisect line
(  
   let( t= uv( angleBisectPt(get(pts,[0,1,2])), pts[1] )*d )
   concat( keep?pts:[], [ for(p=pts) p+ t ] )
);  
   
//========================================================
function translB(pts, d=1, keep=false)=
(  
   let( t= uvB( get(pts,[0,1,2]))*d )
   concat( keep?pts:[], [ for(p=pts) p+ t ] )
);    
  
//=====================================================
//function trsform( pts1, seed2, seed1=undef )=
//(
//   let( pts1 = addz(pts1,0)  // make sure it's 3D
//      , seed1 = seed1==undef? 
//                 ( len(pts1)==2? app(pts1,randPt())
//                               : pts1) : seed1
//      , N1 = N(pts1)
//      , N2 = N(seed2)
//      )
//)   

module Transforms( actions, _i=0)
{
  //
  // Usage: Transforms( ["rot", [0,90,0], "transl", [5,0,10]] ) cube( [ 10, 4, 2 ] );
  //
  // -- Allow actions to have "x","y","z","rotx","roty","rotz" as keys (2017.7.6)
  //
  // See its counter part for poly: transPts()
  
  //echo(_s("Transforms({_})",str(actions)));
  if(_i<len(actions))
  { 
    act = actions[_i];
    data = actions[_i+1];
    if( act == "rot" )
    {

      axis_x = [[1,0,0],[0,0,0]];
      axis_y = [[0,1,0],[0,0,0]];
      axis_z = [[0,0,1],[0,0,0]];      
      
      Transforms( actions, _i=_i+2)
      multmatrix(m = rotM( axis_z, data[2]))
      multmatrix(m = rotM( axis_y, data[1]))
      multmatrix(m = rotM( axis_x, data[0]))
      children();

    } 
    else if ( act == "rotx" )
    {
      Transforms( actions, _i=_i+2)
      multmatrix(m = rotM( [[1,0,0],[0,0,0]], data))
      children();
    } 
    else if ( act == "roty" )
    {
      Transforms( actions, _i=_i+2)
      multmatrix(m = rotM( [[0,1,0],[0,0,0]], data))
      children();
    } 
    else if ( act == "rotz" )
    {
      Transforms( actions, _i=_i+2)
      multmatrix(m = rotM( [[0,0,1],[0,0,0]], data))
      children();
    } 
    else if (act=="x"||act=="y"||act=="z")
    {
      //echo("transl");
      data2=  act=="x"? [data,0,0]
            : act=="y"? [0,data,0]
            : act=="z"? [0,0,data]
            : data;
        
      Transforms( actions, _i=_i+2)
       translate( data2 ) children();
    } 
    else if (act=="transl" || act=="trl" || act=="t" )
    {
      //echo("transl");
      Transforms( actions, _i=_i+2)
       translate( data ) children();
    }
    else if (act=="scalex"||act=="scaley"||act=="scalez")
    {
      //echo("transl");
      data2=  act=="scalex"? [data,1,1]
            : act=="scaley"? [1,data,1]
            : act=="scalez"? [1,1,data]
            : data;
      Transforms( actions, _i=_i+2)
       scale( data2 ) children();
    }            
    else if (act=="scale" )
    {
      //echo("transl");
      Transforms( actions, _i=_i+2)
       scale( data ) children();
    }
    else if (act=="mirror" || act=="m" )
    {
      // data is a pt, line or plane
      
      // If a pt, obj A will be mirrored thru a plane that (1) passes
      //    through ORIGIN (2) has dir defined by normal vec [ORIGIN, pt]
      
      // If a line:
      // The line should be the normal vector to the mirror plane.
      // To mirror an obj A on thru a plane pqr, require the dist 
      // between A and pqr, and use that to make a vector normal 
      // to pqr, pointing to A
        
      //echo(_red(_s("act=mirror in Transforms = {_}, pq={_}", [str(data),str(pq)])));
      
      //pq = len(data)==2?data:isarr(data[0])?normalLn(data):[data, ORIGIN];
      Transforms( actions, _i=_i+2)
      {
        if(ispt(data))
          mirror(data) children();
        else
        {
           pq = len(data)==3? normalLn(data)
                            : data;
           /*
              To mirror obj-A against a mirror normal to PQ to generate obj-B 
              
                         B  
                C.     .'
                  '. .'   .Q
                   .J.  .'
                 .'   'P
               A'       '.    
                          D
           
              We need to move the whole thing to align J to the ORIGIN
              
              translate( P )
              mirror( Q-P )
              translate( ORIGIN-P ) A
              
              Usage: 
              
                 Transforms( actions=["mirror", [R,J]] ) where R is where A is  
                 
           */                 
           translate( pq[0] )
           mirror( pq[1]-pq[0] )
           translate( ORIGIN-pq[0] ) 
           children();
        }
      }    
    } 
      
  } 
  else { children();}  
  //echo("Leaving Transforms()");
}  
   
   
 
//=====================================================


//=========================================


//=================================
{ // Transform         
module Transform( 

    /* check out the dev: explore_multmatrix_150830 [dev.scad] */

      mvx=0
    , mvy=0
    , mvz=0 /* move */
    , expx=1
    , expy=1
    , expz=1 /* expend(stretch) ratio. Note that when set
                 to negative, a reflection+expand is performed: 
                 expx=-1:  mirror on yz plane, no expansion
                 expx=-2:  mirror on yz plane, expand twise as big
                 expx=-1, expy=-1: mirror on pl{ [1,-1,0],z-axis }
                                   (or pl{ [-1,1,0],z-axis } )
                                   Or, easier, mirror on yx then on xz
                 expx=expy=expz=-1: mirror thru [0,0,0].
                 */
    , xoy=0,xoz=0   
    , yox=0,yoz=0
    , zox=0,zoy=0 /* The ?o? is a tilting angle where o is the 
                     origin ([0,0,0]). 
                     xoy means: xoz surface remans unchanged, but
                     yoz surface tilts angle xoy (=angle POU below)
                     toward x:   
                                 y
                                 |
                                 R___U____S___T
                                 |  /     |  /
                                 | /      | /
                                 |/       |/
                                 O--------P------x
                  */   
    , exp=1  /* exp ratio. This expends along all x,y,z directions 
                on top of expx, expy, expz
              */
    ){
        
    function al(ang)= 
        /* al(ang): Convert angle to len  
           When doing tilting, the argument m to multmatrix takes 
           len, but not angle:   
            
             m = [ [ expx, xoy,  xoz, addx ]   
                 , [ yox , expy, yoz, addy ]
                 , [ zox , zoy,  expz, addz ]
                 , [ 0   , 0  ,  0   , 1/exp ] 
                 ];
           where xoy is the ratio RU/RO in the figure: 
                 y
                 |
                 R___U____S___T
                 |  /     |  /
                 | /      | /
                 |/       |/
                 O--------P------x
           transform(), takes angles, so need conversion. 
        */
        ang?tan(ang):0;
    
      
    m = [ [ expx*exp   , al(xoy),  al(xoz) ]   
        , [ al(yox), expy*exp   ,  al(yoz) ]
        , [ al(zox), al(zoy),  expz*exp    ]
        ];   
    if( mvx || mvy || mvz ){        
        translate( [mvx,mvy,mvz] )
        multmatrix( m = m ){ children();}  
    }else{
        multmatrix( m = m ){ children();}  
    }    
  }
} // Transform
   

//module TransformObj( actions, site, opt=[])
//{
   //actions= argx(actions, opt, "actions",[]);
   //site= arg(site, opt, "site",[]);
   //
   //if(site)
     //Move( to=site )
     //Transforms( actions )
     //children();  
   //else
     //Transforms( actions )
     //children();
//}

//module TransformObj( color, actions, site, opt=[])
//{
//   color= und_opt(color, opt, "color",[]);
//   actions= und_opt(actions, opt, "actions",[]);
//   site= und_opt(site, opt, "site",[]);
//   
//   color(color[0], color[1])
//   if(site)
//     Move( to=site )
//     Transforms( actions )
//     children();  
//   else
//     Transforms( actions )
//     children();
//}
   
//========================================================
{ // tria_props
tria_props= ["tria_props", "pqr","array","Geometry"
,"Return [ [len1,a1], [len2,a2], [len3,a3]] of the triangle
;; pqr, where len1=dPQ, a1= aPQR"
];
function tria_props(pqr)=
(
  let( P=pqr[0], Q=pqr[1], R=pqr[2]
     , aQ= angle(pqr)
     , aR= angle([Q,R,P]) 
     )    
  [ [ dist(P,Q), aQ]
  , [ dist(Q,R), aR]
  , [ dist(R,P), 180-aQ-aR]
  ]    
);
} // tria_props
   
//========================================================
   
function triangleArcPlate(pqr,r,n)=
(
   /*
     dPR = dQR = dPQ   and OR = OP = OQ
        
                  R _     r     
            p  .'|\ ''--._     
             .'   | \      ''--. O (ORIGIN)
       Q  .'      |  \ q  _.-'/ 
        ':._      |   \.-'`   /  
            '-._..J'`  \     / r
              M '-._    \   /
           r       '-._ \ / 
                        '-'P  
     
           R           
          /'.          
         /   ^.           
        /______^.  
       /'.     .'^.    
      /   '. .'    ^.       
     Q------'--------' P 
                                     
   */

  3
);

   
function trslN(pts, d=1, keep=false)=
(  // Translate pts along the normal dir of the first 3 pts
   //    by distance d
   // trslN( [2,3,4], 1)
   // trslN( [[2,3,4],[5,6,7]], -2 )
   let( t= uvN( get(pts,[0,1,2]))*d )
   concat( keep?pts:[], [ for(p=pts) p+ t ] )
);    



function transCoordPts(pts, pqr=ORIGIN_SITE)=
(
   //echo(_coord=_coord)   
   //[for(pt=pts) pt*pqr]
   pts*pqr
);

    transCoordPts= ["transCoordPts", "pts,pqr", "pts", "Geometry",
    " Transfer pts to a new coord system defined by pqr where
    ;; P,Q,R are unit vectors of the new coordinate.
    "
    ];
    
    function transCoordPts_test( mode=MODE, opt=[] )=
    (
        doctest( transCoordPts, [], mode=mode, opt=opt )
    );
    
    

function transN(pts, d=1, keep=false)=
(  // Translate pts along the normal dir of the first 3 pts
   //    by distance d
   // trslN( [2,3,4], 1)
   // trslN( [[2,3,4],[5,6,7]], -2 )
   let( t= uvN( get(pts,[0,1,2]))*d )
   concat( keep?pts:[], [ for(p=pts) p+ t ] )
);   
 
function transP(pts, d=1, keep=false)=
(  // Translate pts along the dir of Q->P (= pts[1]->pts[0]) 
   //    by distance d

   let( t= onlinePt( pts, len=-d) -pts[0] )
   concat( keep?pts:[], [ for(p=pts) p+ t ] )
);    

//=====================================================

function trslN2(pts, d=1)=
(  // Translate pts along the N2 dir of the first 3 pts
   //    by distance d
   // trslN( [2,3,4], 1)
   // trslN( [[2,3,4],[5,6,7]], -2 )
   let( t= uv( pts[1], N2( get(pts,[0,1,2]) ) )*d )
   [ for(p=pts) p+ t ]
);

function transN2(pts, d=1)=
(  // Translate pts along the N2 dir of the first 3 pts
   //    by distance d
   // trslN( [2,3,4], 1)
   // trslN( [[2,3,4],[5,6,7]], -2 )
   let( t= uv( pts[1], N2( get(pts,[0,1,2]) ) )*d )
   [ for(p=pts) p+ t ]
);


//================================================
{ // trslP
function trslP(pts, d=1)=
(  // Translate pts along the P dir of the first 3 pts
   //    by distance d
   // trslN( [2,3,4], 1)
   // trslN( [[2,3,4],[5,6,7]], -2 )
   let( t= uv( pts[0],pts[1] )*d )
   [ for(p=pts) p+ t ]
);    

//function transP(pts, d=1)=
//(  // Translate pts along the P dir of the first 3 pts
//   //    by distance d
//   // trslN( [2,3,4], 1)
//   // trslN( [[2,3,4],[5,6,7]], -2 )
//   let( t= uv( pts[0],pts[1] )*d )
//   [ for(p=pts) p+ t ]
//);   
 
//function transPts(pts, d=1)=
//(  // Translate pts along the P dir of the first 3 pts
//   //    by distance d
//   // trslN( [2,3,4], 1)
//   // trslN( [[2,3,4],[5,6,7]], -2 )
//   let( t= uv( pts[0],pts[1] )*d )
//   [ for(p=pts) p+ t ]
//);    
} // trslP

//================================================
{ // trsfbonePts   
function trsfbonePts( pts, seed, _nodes=[], _newpts=[], _i=0, _debug="" )=
    (
        _i==0?
        
            let( seed = und(seed, pts)
               , _nodes= getbonedata( pts )
               , len0= hash(_nodes[0],"len")
               , pq = [ onlinePt( p10(seed), len0), seed[1] ]
               , _newpts= pq  //p01(seed)
               , _debug= str( 
                   _red("<br><hr>trslocPts debug, _i=0")
                   , "<br><b>_nodes</b> = ", _nodes
                   , "<br><b>_newpts</b> = ", _newpts
                   )
               )
            trsfbonePts( pts,seed,_nodes
                     , _newpts=_newpts, _i=_i+1, _debug=_debug)
            
        : 0<_i && _i< len(pts)-1?
        
            let( refpl = _i==1? seed: get( _newpts, [-2,-1,-3] )
               , node = _nodes[_i]
               
               , newpt= anglePt( refpl, a= hash( node, "a")
                                      , a2=hash(node, "a2")
                                      , len=hash(node,"len")
                               )
               , _newpts= app(_newpts, newpt)
               , _debug= str( _debug
                   , _red("<hr>trslocPts debug, _i="), _i
                   , "<br><b>_nodes</b> = ", _nodes //_colorPts(_nodes)
                   , "<br><b>refpl</b> = ", refpl
                   , "<br><b>newpt</b> = ", newpt
                   , "<br><b>_newpts</b> = ", _newpts
                   , "<hr>"
                   )
               )
            trsfbonePts( pts,seed,_nodes
                     , _newpts= _newpts
                     , _i=_i+1
                     , _debug=_debug)
        
       //: _debug
       :_newpts
     );//_trsfbonePts()
     
     trsfbonePts= ["trsfbonePts", "pts,[seed]", "array", "Geometry",
    " Transform bone pts to where the seed is by aligning the 
    ;; first 2 pts of pts with those of seed "
    ];
} // trsfbonePts
  
//========================================================
//{  // trfPts
//// Not done 2015.8.6 (see demo)   
//function trsfPts(pts, pqr)=
//(
//   let( newP = pqr[0]
//      , newQ = pqr[1]
//      , newR = pqr[2]
//      
//      , v = newQ - pts[1]  // translation vector
//      
//      // Translate pts to newQ
//      , pl_moved= [ for(p=pts)p+v]
//      
//      // plane for pl_moved to travel during next rot  
//      // in order to align p01(pl_moved) with [newP,newQ]     
//      , rotpl_1 = [ pl_moved[0], newQ, newP ]
//      // axis of rotation
//      , axis1 = [ N(rotpl_1), newQ ]
//      // angle of rotation
//      , rota1 = angle( rotpl_1)
//      // pts after 1st rotation
//      , pl_rot1 = rotPts(pl_moved, axis1, rota1)
//      
//      , axis2 = p01( pl_rot1)
//      , rota2= twistangle( pl_rot1[2], pl_rot1[0], pl_rot1[1], newR )
//      
//      , newpts = rotPts( pl_rot1, axis2, rota2 )
//      )
//   newpts            
//); 
//    trfPts=["trfPts", "pqr,pqr2", "n/a", "Geometry",
//    " A module to transform an obj from a coordnate (defined by pqr)
//    ;; to another (pqr2). Args pqr, pqr2 are any 3-pointer.
//    ;; 
//    "];
//
//    function trfPts_test( mode=MODE, opt=[] )=
//    (
//        doctest( trfPts,[],mode=mode, opt=opt )
//    );
//} // trfPts
//========================================================
// Not done 2015.8.6 (see demo)   
//function trsfPts( pts, pqr )= // Transform pts to align with pqr
//(let( P= pts[0]
//    , Q= pts[1]
//    , pqr0 = p012(pts)
//    //, pqr_x= trslN(pqr0,2)
//    , newP = onlinePt( p10(pqr), dist(p01(pts)))
//    , pqr = [ newP, pqr[1], pqr[2] ]
//    , newpts = 
//        concat(  [newP, pqr[1]]
//        , [ for(i=[2:len(pts)-1])
//            let( ap = getAnglePtData( pqr0, pts[i])  //: [a,a2,dist,J]
//               , newpt= anglePt( pqr, a=ap[0], a2=ap[1], len=ap[2] )
//               )
//            newpt
//          ]
//        )
//    ) newpts //trslN( newpts, -2)    
//);   
     
   
//function tubePts(size, nside=4, th, incline )=
//(
  //             _6-_
  //          _-' |  '-_
  //       _-'   14-_   '-_
  //    _-'    _-'| _-15   '-_
  //   5_  13-'  _-'  |      .7
  //   | '-_ |'12_2-_ |   _-' | 
  //   |    '-_'|    '|_-'    |
  //   |   _-| '-_10_-|'-_    |
  //   |_-'  |_-| 4' _-11 '-_ | 
  //   1_   9-  | |-'       _-3
  //     '-_  '-8'|      _-'    
  //        '-_   |   _-'
  //           '-_|_-'
  /*
  
     incline=0 | [0,0]
      +-.---.-+
      | |   | |  
      | |   | |  
      | |   | |  
      | |   | |  
      | |   | |  
      +-.---.-+
      
     incline = 45
                     _ _ _ _ _ _ _ 
      |\.____/|     |'.  45
      | |   | |     |  '.
      | |   | |     |    '.
      | |   | |     |     |
      | |___| |     |     |
      |/    '\|
      
     incline = [-45,45]
         ___   
       /|   '\           .'|
      | |   | |        .'  | -45  
      | |   | |      .'_ _ | _ _ _ ___ 
      | |   | |     |      |
      | |___| |     |      |
      |/    '\|     |      |
  */
function tubePts( h,r,r1,r2,center,d,d1,d2, th, $fn, $fa, $fs
                    , actions, site, opt=[])= 
   // opt: ["site"..."actions"..."fn"]
( // argument priority: d1,d2 > r1,r2 > d > r
 
   let(h =getTransformData( shape="tube"
                         , h=h
                         , r=r
                         , r1=r1
                         , r2=r2
                         , center=center
			 , d=d
                         , d1=d1
                         , d2=d2
                         , th=th
                         , $fn=$fn
                         , $fa=$fa
                         , $fs=$fs
                         , site=site
			 , actions=actions
			 , opt=opt
			 )
      )
   //echo("in tubePts, h=", h)   
   h(h, "pts")
        
);  
   
//========================================================
{ // twistangle
function twistangle( p,q,r,s )=
(
  let( P = r==undef? p[0]:p
     , Q = r==undef? p[1]:q
     , R = q==undef? p[2]: r==undef? q[0]:r
     , S = q==undef? p[3]: r==undef? q[1]:s
     , Jp = projPt( P, [Q,R] )
     , Js = projPt( S, [Q,R] )
     , d = Js-Jp
     )
  angle( [ P+d, Js, S], Rf=Jp )    
    
//  let( P = r==undef? p[0]:p
//     , Q = r==undef? p[1]:q
//     , R = q==undef? p[2]: r==undef? q[0]:r
//     , S = q==undef? p[3]: r==undef? q[1]:s
//
//     , plrtn= planeDataAtQ( [Q,R,S], a=90, a2=90) 
//     , pl = hash(plrtn, "pl")
//     , Jp= projPt( pl, P) 
//     , Js= projPt( pl, S)
//     , _a = angle( [Jp,R,Js], Rf=Q ) 
//     , a = isnan(_a)|| abs(_a)<1e-5?0:_a // Escape the nan value
//     )     
//     a
       
     //[P,Q,R,S,"<br/>",pl,Jp,Js]
     //[Jp, Js]
     
);

    twistangle=["twistangle","pqrs", "number", "Angle",
     " Given a 4-pointer (pqrs), looking down from Q to R (using QR
    ;; as the axis), return the angle representing how much the line_RS
    ;; twists away from line_PQ. 
    ;;
    ;; The following graphs looks down from Q to R: 

    ;;  +-----:-----+   a: 0~90
    ;;  :     :  S  :  
    ;;  :     : /   : 
    ;;  :     :R    :
    ;;  ------Q-----P 

    ;;  +-----:-----+   a:90~180
    ;;  : S   :     :   
    ;;  :  '. :     : 
    ;;  :    R:     :
    ;;  ------Q-----P 

    ;;  ------Q-----P  a: 0~ -90
    ;;  :     :R    :
    ;;  :     : '.  :
    ;;  :     :   S :
    ;;  +-----:-----+ 

    ;;  ------Q-----P  a: -90~ -180
    ;;  :    R:     :
    ;;  :   / :     :
    ;;  :  S  :     :
    ;;  +-----:-----+ 
    ;;
    ;;  a_PAB + a_DCS      D   S
    ;;                     :  /|
    ;;                     : / |
    ;;                F    :/  |   
    ;;                :    /   |  .'
    ;;           E    :   /:  .|'
    ;;           :    :  / :.' |
    ;;  P    B   :    : / .C---:
    ;;  |'.  |   :    :/.'_.-''
    ;;  |  '.| --:----R--'------
    ;;  |    |.  :  .'
    ;;  |    | '.:.'
    ;; -|----|-_.Q -------    
    ;;  |.'_.|.'
    ;;  '----A'
    ;;     .'

    "];

    function twistangle_test(mode=MODE, opt=[] )=
    (
        let ( Q = [2,0,0]
            , R = [-2,0,0]
            )
        doctest( twistangle
        ,[ 
           "", "var Q", "var R"
         , [twistangle( [[3,1,1],Q],[R,[-3,-1,1]] ), 90
            , "[[3,1,1],Q],[R,[-3,-1,1]]"]
         , [twistangle( [3,1,1],Q,R,[-3,1,1] ), 0
            , "[3,1,1],Q,R,[-3,1,1]", ["prec",6]]
         , [twistangle( [[3,0,1],Q,R,[-3,-1,1]] ), 45
            , "[[3,0,1],Q,R,[-3,-1,1]]"]
         , [twistangle( [3,-1,1],Q,R,[-3,1,1] ), -90
            , "[3,-1,1],Q,R,[-3,1,1]"]
         ]
        , mode=mode, opt=opt, scope=["Q",Q,"R",R]
        )
    );

    //echo( twistangle_test( mode=12 )[1] );

    //module twistangle_demo()
    //{	
    //	pqrs = randPts(4);
    //	P=P(pqrs); Q=Q(pqrs); R=R(pqrs);
    //	S=pqrs[3];
    //	
    //	MarkPts([P,Q,R,S],["labels","PQRS"]);
    //	Chain( [P,Q,R,S ] );
    //	//Line0( [Q,S] );
    //
    //	echo( "quad:", quadrant( [Q,R,P],S), "twistangle:", twistangle( [P,Q,R, S] ));
    //
    //}
    //twistangle_demo();

    module twistangle_chk(){
        
      pts= [ [6.77079, 4.14925, 6.00621]
           , [6.7593, 4.21674, 5.98853]
           , [6.15651, 2.98036, 5.65985]
           , [6.21062, 3.00029, 5.70077]];   
     echo( twistangle = twistangle( pts ) );   
        
    }    

//twistangle_chk();   
} // twistangle


//========================================================
{ // uv
function uv(p,q)=             // uv( [2,3,4],[5,6,7] )
(   let( pq= q==undef?        // uv( [ [2,3,4],[5,6,7] ])
             (isarr(p[0])?p:[ORIGIN,p])  // uv( [2,3,4] )
             :[p,q] )
    onlinePt( pq ,len=1)- pq[0] 
    /*
      = (q-p)/norm(q-p) 
       
    */
);

    uv=["uv", "pq", "pt", "Geometry",
    " Return the unit vector (len = 1 ) of pq "
    ];

    function uv_test( mode=MODE, opt=[] )=
    (
        doctest( uv,
        [
        ], mode=mode, opt=opt
        )
    );
} // uv

//========================================================
{ // uvB
function uvB( pqr )= // unit vector of pqr normal
( 
    uv( pqr[1], B(pqr) )  
);
} // uvB

//========================================================
{ // uvN
function uvN( pqr )= // unit vector of pqr normal
( 
    uv( pqr[1], N(pqr) )  
);
} // uvN

/*arcPts(
pqr 
                , n=6
                , rad= undef
                , a = undef
                , a2 = undef
                , ratio= 1
*/

    
function vecOp( refPts,  indices=[], _pt,_debug=[], _i=0)= // cmd: hash: ["+",1,"-",-2 ]
(
   _i>=len(indices)? _pt
   :  let( ii=indices[_i]
         , L = len(refPts)
         , _pt = (_pt?_pt:ORIGIN) +
                 ( isint(ii)?    // ------------------ [3, ...]
                   refPts[ (ii<0?L:0)+ii]           
                 : len(ii)==2?   // ------------------ [ ... [i,j], ...]
                   let( 
                        P = isint( ii[0] ) ?
                                refPts[ (ii[0]<0?L:0)+ii[0] ]: ii[0]
                      , Q = isint( ii[1] )? 
                                refPts[ (ii[1]<0?L:0)+ii[1] ]: ii[1]
                      )
                   _i==0? Q // When the indices[0] is an array [x,y], x is ignore
                        : Q-P
                 : ii           // ------------------- [, P, ...]
                 )                                      
        ) 
       vecOp( refPts = refPts
            , indices = indices
            , _pt= _pt
            , _i = _i+1 
            , _debug = concat( _debug,
                       [["refPts", refPts, "indices", indices, "ii",ii
                        ,"_i", _i, "len(ii)", len(ii)
                        , "_pt", _pt]] )
            )   
);

    vecOp= [ "vecOp", "srcPts, indices", "pt", "Point",
     " Perform a series of vector operations (+/- on pt), by indices
    ;;  referring to the srcPts
    ;;
    ;;   refPts: a series of pts to be used as ref
    ;;   indices: two form: 
    ;;
    ;;   (1)  [ i, [<i>,<j>], [<i>,<j>]]  // when pt is undef
    ;;   (2)  [ [<i>,<j>], [<i>,<j>]]
    ;;
    ;;  where i,j could be negative (counted from the back)
    ;;
    ;;  When it is form (2), the <i> for the first pair will be ignored.
    ;; 
    ;;  It takes the pt, add a vector of srcPts[j]-srcPts[i] for each
    ;;  pair of [i,j] from the indices. 
    ;;
    ;;  When pt is NOT given, assign it to the srcPts[i] where i is 
    ;;  the first i in indices (either the i in [i, [i1,j1]...] or the 
    ;;  i0 in [[i0,j0],[i1,j1]...])
    ;;  
    ;; Examples:
    ;;
    ;; vecOp( refPts= pts, indices=[2,[1,0]] )= pts[2]+ pts[0]-pts[1]
    ;; vecOp( refPts= pts, indices=[0,[1,2],[1,2]] ) = pts[0]+ 2*(pts[2]-pts[1)
    ;; vecOp( refPts= pts, indices=[ORIGIN,[1,2],[1,2]] )
    ;;   = ORIGIN+ 2*(pts[2]-pts[1)
    ;; vecOp( refPts= pts, indices=[[1,2],[0,1]] ) = pts[2]-pts[1]+pts[1]-pts[0]
    
    "];

     
    function vecOp_test( mode=MODE, opt=[] )=
    (
        let( pqr= [[0.61, -0.56, 0.74], [-0.17, -0.38, 0.02], [0.18, -0.67, -0.59]]
           )
        doctest( vecOp,
        [ 
          "var pqr"
        , [vecOp( refPts=pqr,indices=[2,[1,0]] ),  [0.96, -0.85, 0.13]
                   , "refPts=pqr,indices=[2,[1,0]] ", ["asstr",true]]
                   
        , [vecOp( refPts=pqr,indices=[0,[1,2],[1,2]] ), [1.31, -1.14, -0.48]
                 , "refPts=pqr,indices=[0,[1,2],[1,2]]", ["asstr",true]]
                 
        , [vecOp( refPts=pqr,indices=[ORIGIN,[1,2],[1,2]]), [0.7, -0.58, -1.22]
                 , "pt=ORIGIN,refPts=pqr,indices=[ORIGIN,[1,2],[1,2]] ", ["asstr",true]]
                 
        , [vecOp( refPts=pqr,indices=[[1,-1],[0,1]] ), [-0.6, -0.49, -1.31]
                    , "refPts=pqr,indices=[[1,-1],[0,1]]", ["asstr",true]]
        ]
        , mode=mode,pt=opt, scope=["pqr",pqr]
        )
    );
    
function x(pts,x)= isnum(pts[0]) ? [pts[0]+x,pts[1], pts[2] ]
                   : [ for ( p = pts ) [p[0]+x, p[1], p[2] ] ] ;
function y(pts,y)= isnum(pts[0]) ? [pts[0],pts[1]+y, pts[2] ]
                   : [ for ( p = pts ) [p[0], p[1]+y, p[2] ] ] ;
function z(pts,z)= isnum(pts[0]) ? [pts[0],pts[1], pts[2]+z ]
                   : [ for ( p = pts ) [p[0], p[1], p[2]+z ] ] ;
function t(pts,xyz)= isnum(pts[0]) ? [pts[0]+xyz.x,pts[1]+xyz.y, pts[2]+xyz.z ]
                   : [ for ( p = pts ) [p[0]+xyz.x, p[1]+xyz.y, p[2]+xyz.z ] ] ;

module X(n){ translate( [n,0,0] ) children(); }
module Y(n){ translate( [0,n,0] ) children(); }
module Z(n){ translate( [0,0,n] ) children(); }
module T(a){ translate( a ) children(); }

module chaindata_testing( label, settings, isdetails, isdraw ){
    echom("chaindata_testing");
    
    //##################################################
    //##################################################
    
    module chaindata_testing_Draw( chdata, color, transp=1){

        cuts = hash(chdata, "cuts");
        pts = joinarr( cuts );
        bone = hash(chdata,"bone");
        
        nseg = len(cuts)-1;
        nside = len(cuts[0]);
        
        MarkPts( hash(chdata, "bone" ),"ch");
        MarkPts( p012(bone), "pl=[transp,0.2];ball=0");
        MarkPts( [for(cut=cuts)cut[0]], "ch=[color,blue,r,0.01];ball=0");
        
        if(len(cuts[0])>1)
            MarkPts( cuts[0], "ch=[transp,1,color,red, r,0.008];ball=0");
        if(len(last(cuts))>1)
            MarkPts( last(cuts)
                    , "ch=[transp,1,color,green,r,0.008];ball=0");
        
        P=bone[0]; Q=bone[1]; R=bone[2];
        N0 = N( [Q,P,R] );
        B0 = B( [Q,P,R] );
        pl00 = cuts[0][0];
        pl01 = cuts[0][1];
        
//        if( hash(chdata,"tiltlast")){
//            last= get(bone,-1);
//            last2= get(bone,-2);
//
//            lastpl = last(cuts);
//            lastpl0= lastpl[0];
//            lastpl1= lastpl[1];
//
//            Mark90([ last2, last ,lastpl0],"ch" );
//            Mark90([ last2, last ,lastpl1],"ch" );
//            Mark90([lastpl0, projPt(lastpl0, [last2,last]), last2], "ch" );
//            Mark90([lastpl1, projPt(lastpl1, [last2,last]), last2], "ch" );
//            
//        } else {    
//            Mark90( [Q,P,pl00],"ch" );
//            Mark90( [Q,P,pl01],"ch" );
//            Mark90( [ pl00, projPt( pl00, [P,Q]), P], "ch" );
//            Mark90( [ pl01, projPt( pl01, [P,Q]), P], "ch" );
//        }
        
        shape = hash(chdata,"shape", "chain");
        faces = faces( shape= shape //hash(chdata,"shape", "chain")
                     , nside=nside, nseg= nseg  );
        //echo(faces=faces); 
        //MarkPts( pts, "l");
        color(color,transp)
        polyhedron(points = pts, faces=faces );     
        
    }    
    //##################################################
    //##################################################
    function get_gots( chdata )=
    (
        //======================================== 
        // Internal use for chaindata_testing
        //========================================
        let(
             cd = chdata
           //, seed= hash(cd,"seed")
           , cuts= hash(cd,"cuts")
           , bone= hash(cd,"bone")
    
           , cut00 = cuts[0][0]
           , cut01 = cuts[0][1]
           , lastcut0 = last(cuts)[0]
           , lastcut1 = last(cuts)[1]
    
           , lastbone = get(bone,-1)
           , lastbone2= get(bone,-2)
    
           , tilt= hash(cd,"tilt")
           , tiltlast= hash(cd,"tiltlast")
           , closed = hash(cd, "closed")
           , rs = hash(cd,"rs")
           , Rf = hash(cd, "Rf")
           , AjPQ= projPt(cut00,p01(bone))
           , AjPQR= len(bone)==2?undef:projPt(cut00,p012(bone))
           , BjPQ= projPt(cut01,p01(bone))
           , BjPQR= len(bone)==2?undef:projPt(cut01,p012(bone))
                
           , got=let( a_per_side= angle( [cuts[0][0], bone[0],cuts[0][1]])
                    )
                concat(
                       
                [
                 "first_r",  dist(bone[0], cut00)
                ,"rs", [for( i=range(cuts) )
                        dist( bone[i], cuts[i][0] ) ]
                ,"twist_seg0", twistangle( cut00, bone[0], bone[1],
                                      cuts[1][0] )
                ,"a_per_side", a_per_side
                
               // ,"rot", len(bone)>2? // choice of this var is questionable
               //         -twistangle( cut00, bone[0],bone[1], cuts[1][0]) 
               //         : a_per_side
               ]
               , !tiltlast? 
                [
                 "tilt",     angle( [bone[1],bone[0],cut00] )
                ,"a_cut00_bone", angle( [ bone[1],bone[0],cut00] )
                ,"a_cut01_bone", angle( [ bone[1],bone[0],cut01] )
                ,"a_cut00_pqr", len(bone)==2?undef
                                :isequal(AjPQ,AjPQR)?0
                                :angle( [cut00, AjPQ,AjPQR ] )
                ,"a_cut01_pqr", len(bone)==2?undef
                                :isequal(BjPQ,BjPQR)?0
                                :angle( [cut01, BjPQ,BjPQR ] )
                ]
                :[
                 "tiltlast",  [ lastbone2,lastbone
                                    , lastcut0
                                    ]                         
                ,"a_lastcut0_bone", angle( [ lastbone2,lastbone
                                            ,lastcut0] ) 
                ,"a_lastcut1_bone", angle( [ lastbone2,lastbone
                                            ,lastcut1] )  
                ]
              )//..concat  
           )//..let
           got 
    );    
    //##################################################
    //##################################################

    function module_testing_get_result_of_setting( setting, gots )=
    (
       let( 
            chdata= hash( setting, "chdata")
           , cuts = hash( chdata, "cuts") 
           , wants= hash( setting, "wants")
           , wantvars= keys(wants)
           //, gots = get_gots(chdata)
           , res= [ for( wantvar=wantvars )
                    let( want = hash(wants, wantvar)
                       , got  = hash(gots,  wantvar)
                       , fail = isequal( want,got )?0:1
                       , msg= str( "__|<b>"
                                , fail?_red(wantvar)
                                     : wantvar
                                , "</b>== "
                                , want, " ? "
                                , fail? _red( got )
                                   : _green("pass")
                                )
                       )
                       [ fail, msg ]
                  ]      
            , test_count_this_setting= len(wantvars)
            , test_fail_this_setting= sum( [for(re=res) re[0]] )
            , label = _color( hash( setting, "label")
                          , hash( setting, "color" ) 
                          )
            , msg = str( label,"<br/>"
                    , join( [for(re=res)re[1]],"<br/>")
                    )
            )//..let
           [ test_count_this_setting
           , test_fail_this_setting, msg ]
                    
    );//..module_testing_get_result_of_setting()

    module module_testing_echo( settings, results ){

        nTests = sum( [for(tfm= results ) tfm[0] ] );
        nFails = sum( [for(tfm= results ) tfm[1] ] ); 
        echo( str( 
            "<span style='font-size:14px;background:lightyellow'><u><b>"
            , label, " </u></b> "

             , _bcolor(str(
                    "<u>--- tests "
                    ," (in ", len(settings), " settings) : "
                    , _blue(nTests)
                    , ", failed: ", _color( nFails, nFails?"red":"green")
                    , "</u>&nbsp;</span>&nbsp;"
               ),"lightyellow") 
             , " <br/>(settings: "
                , join([ for(i = range(settings))
                            let(setting=settings[i]
                               ,label = hash(setting,"label")
                               , fail = results[i][1]
                               )   
                           _color( str( label
                                      , fail? str("<b style='color:red'>="
                                                 , fail, " fail(s)</b>"):""
                                      )
                                 , hash( setting, "color"))
                       ],", ")
             , ")"
             )
            ); 
    }
    
    //##################################################
    //##################################################
    //gots = get_gots( hash( settings[0], "chdata"));
    //echo(gots = gots);
    
    if(settings)
    {            
        results=[
                  for( setting=settings )
                     module_testing_get_result_of_setting( 
                        setting 
                      , gots = get_gots( hash(setting,"chdata"))
                     )
                ]; //.. result
        //results=[];            
        //echo( results = results );       
        for( i=range(settings) ){
            setting= settings[i];
            //echo(i=i, setting=setting);
            if(isdraw || hash(setting,"isdraw")){                 
                chaindata_testing_Draw( 
                    chdata = hash( setting, "chdata")
                   ,color= hash( setting, "color")
                   ,transp= hash(setting, "transp")
                );
            }    
            if(isdetails || hash(setting,"isdetails"))
                echo( results[i][2] );
        }       
           
        module_testing_echo( settings, results ); 
               
    }//..if(settings)
    
}//..chaindata_testing 


//. TESTS 
/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_geometry_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_geometry.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_geometry_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_geometry", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      RM_test( mode, opt )
    , RM2z_test( mode, opt )
    , RMs_test( mode, opt )
    , RMx_test( mode, opt )
    , RMy_test( mode, opt )
    , RMz_test( mode, opt )
    , SSD_test( mode, opt ) // to-be-coded
    , addx_test( mode, opt )
    , addy_test( mode, opt )
    , addz_test( mode, opt )
    //, al_test( mode, opt ) // internal func
    , angle_test( mode, opt )
    , angleAfter2Rots_test( mode, opt )
    , angleAt_test( mode, opt )
    , angleBisectPt_test( mode, opt )
    , anglePt_test( mode, opt )
    , anglebylen_test( mode, opt )
    , arcPts_test( mode, opt )
    , avg_SSD_test( mode, opt ) // to-be-coded
    , cavePF_test( mode, opt ) // to-be-coded
    //, cavePF2_test( mode, opt ) // to-be-coded
    , centerPt_test( mode, opt )
    , centroidPt_test( mode, opt )
    //, chainBoneData_test( mode, opt ) // to-be-coded
    //, chainlen_test( mode, opt ) // to-be-coded
    , cirIntsecPts_test( mode, opt ) // to-be-coded
    , circlePts_test( mode, opt )
    , circlePtsOnPQR_test( mode, opt ) // to-be-coded
    , circumCenterPt_test( mode, opt ) // to-be-coded
    , cirfrags_test( mode, opt )
    //, convPolyPts_test( mode, opt ) // to-be-coded
    , coordPts_test( mode, opt )
    , cornerPt_test( mode, opt )
    , cubePts_test( mode, opt )
    //, cubePts3_test( mode, opt ) // to-be-coded
    //, cutHolePts_test( mode, opt ) // to-be-coded
    //, cylinderPts_test( mode, opt ) // to-be-coded
    //, decoPolyEdge_test( mode, opt ) // to-be-coded
    //, decoPolyEdge_1804_test( mode, opt ) // to-be-coded
    //, decoPolyEdge_180522_1_test( mode, opt ) // to-be-coded
    //, decoPolyEdge_180523_test( mode, opt ) // to-be-coded
    //, decoPolyEdges_test( mode, opt ) // to-be-coded
    , dist_test( mode, opt )
    , distPtPl_test( mode, opt )
    //, distRatios_test( mode, opt ) // to-be-coded
    //, divPts_test( mode, opt ) // to-be-coded
    //, dualMassPts_test( mode, opt ) // to-be-coded
    //, endPtsInfoAlongLine_test( mode, opt ) // to-be-coded
    , expandPts_test( mode, opt )
    , extendPts_test( mode, opt )
    //, extPt_test( mode, opt ) // to-be-coded
    //, getAnglePtData_test( mode, opt ) // to-be-coded
    //, getBezierPtsAt_test( mode, opt ) // to-be-coded
    //, getBezierPtsAt_150629_working_test( mode, opt ) // to-be-coded
    //, getBezier_simple_test( mode, opt ) // to-be-coded
    , getBoxData_test( mode, opt )
    //, getDecoData_test( mode, opt ) // to-be-coded
    //, getHShapePts_test( mode, opt ) // to-be-coded
    //, getLoftData_dev_20150920_test( mode, opt ) // to-be-coded
    //, getMoveData_test( mode, opt ) // to-be-coded
    //, getMoveData_1867_test( mode, opt ) // to-be-coded
    //, getMoveData_new_try_1867_test( mode, opt ) // to-be-coded
    , getPairingData_test( mode, opt )
    //, getPairingData2_dev_test( mode, opt ) // to-be-coded
    //, getPairingData_ang_seg_test( mode, opt ) // to-be-coded
    , getPairingData_sorted_afips_test( mode, opt )
    //, getPlaneByNormalLine_test( mode, opt ) // to-be-coded
    , getSideFaces_test( mode, opt )
    //, getTransformData_test( mode, opt ) // to-be-coded
    , getTubeSideFaces_test( mode, opt )
    //, get_gots_test( mode, opt ) // to-be-coded
    //, getbonedata_test( mode, opt ) // to-be-coded
    //, gmedian_test( mode, opt ) // to-be-coded
    //, groupByOctant_test( mode, opt ) // to-be-coded
    //, groupByQuadrant_test( mode, opt ) // to-be-coded
    , growPts_test( mode, opt )
    //, ibend_test( mode, opt ) // to-be-coded
    //, icosahedronPF_test( mode, opt ) // to-be-coded
    , incenterPt_test( mode, opt )
    //, intsec_test( mode, opt ) // to-be-coded
    //, intsec_cirpl_test( mode, opt ) // to-be-coded
    //, intsec_lnpl_test( mode, opt ) // to-be-coded
    //, intsec_plpl_test( mode, opt ) // to-be-coded
    //, lineBridge_test( mode, opt ) // to-be-coded
    //, lineRotation_test( mode, opt ) // to-be-coded
    , longside_test( mode, opt )
    //, meshPts_test( mode, opt ) // to-be-coded
    , midPt_test( mode, opt )
    //, mirrorPt_test( mode, opt ) // to-be-coded
    //, mirrorPts_test( mode, opt ) // to-be-coded
    //, module_testing_get_result_of_setting_test( mode, opt ) // to-be-coded
    //, movePts_test( mode, opt ) // to-be-coded
    , normal_test( mode, opt )
    , normalLn_test( mode, opt )
    , normalPt_test( mode, opt )
    , normalPts_test( mode, opt )
    //, objDecoHandler_test( mode, opt ) // to-be-coded
    //, octant_test( mode, opt ) // to-be-coded
    , onlinePt_test( mode, opt )
    , onlinePts_test( mode, opt )
    , othoPt_test( mode, opt )
    //, planeDataAt_test( mode, opt ) // to-be-coded
    //, planeDataAtQ_test( mode, opt ) // to-be-coded
    //, planeDataAtQ_testing_get_gots_test( mode, opt ) // to-be-coded
    //, planeDataAt_testing_get_gots_test( mode, opt ) // to-be-coded
    , planecoefs_test( mode, opt )
    //, plat_test( mode, opt ) // to-be-coded
    //, platonicPts_test( mode, opt ) // to-be-coded
    //, popPF_test( mode, opt ) // to-be-coded
    , precPts_test( mode, opt ) 
    //, projPl_test( mode, opt ) // to-be-coded
    , projPt_test( mode, opt )
    //, projPts_test( mode, opt ) // to-be-coded
    //, quadCentroidPts_test( mode, opt ) // to-be-coded
    , quadrant_test( mode, opt )
    , rodPts_test( mode, opt )
    , rodfaces_test( mode, opt )
    , rodsidefaces_test( mode, opt )
    //, rotM_test( mode, opt ) // to-be-coded
    //, rotMx_test( mode, opt ) // to-be-coded
    //, rotMx_test( mode, opt ) // to-be-coded
    //, rotMy_test( mode, opt ) // to-be-coded
    //, rotMz_test( mode, opt ) // to-be-coded
    , rotPt_test( mode, opt )
    , rotPts_test( mode, opt )
    , rotangles_p2z_test( mode, opt )
    , rotangles_p2z_test( mode, opt )
    , rotangles_z2p_test( mode, opt )
    //, rotpqr_test( mode, opt ) // to-be-coded
    //, rotx_test( mode, opt ) // to-be-coded
    //, roundness_test( mode, opt ) // to-be-coded
    //, roundness_use_intercepts_test( mode, opt ) // to-be-coded
    //, scaleM_test( mode, opt ) // to-be-coded
    //, scalePt_test( mode, opt ) // to-be-coded
    //, scalePts_test( mode, opt ) // to-be-coded
    //, setPtsDecimal_test( mode, opt ) // to-be-coded
    //, shapeInfo2d_test( mode, opt ) // to-be-coded
    , shortside_test( mode, opt )
    //, simpleChainData_test( mode, opt ) // to-be-coded
    , sinpqr_test( mode, opt )
    , slope_test( mode, opt )
    //, smoothPts_test( mode, opt ) // to-be-coded
    //, smoothPts_new_test( mode, opt ) // to-be-coded
    //, sortByDist_test( mode, opt ) // to-be-coded
    //, spherePF_test( mode, opt ) // to-be-coded
    //, spherePts_185q_test( mode, opt ) // to-be-coded
    //, spreadActions_test( mode, opt ) // to-be-coded
    //, spreadCornerActions_test( mode, opt ) // to-be-coded
    , squarePts_test( mode, opt )
    //, sum_dist_test( mode, opt ) // to-be-coded
    //, sum_dist2_test( mode, opt ) // to-be-coded
    , symmedianPt_test( mode, opt )
    //, tangLn_test( mode, opt ) // to-be-coded
    //, tanglnPt_P_test( mode, opt ) // to-be-coded
    //, textPts_test( mode, opt ) // to-be-coded
    //, textWidth_test( mode, opt ) // to-be-coded
    //, tocoord_test( mode, opt ) // to-be-coded
    , transCoordPts_test( mode, opt )
    //, transN_test( mode, opt ) // to-be-coded
    //, transN2_test( mode, opt ) // to-be-coded
    //, transP_test( mode, opt ) // to-be-coded
    //, transPts_test( mode, opt ) // to-be-coded
    //, translAB_test( mode, opt ) // to-be-coded
    //, translB_test( mode, opt ) // to-be-coded
    //, tria_props_test( mode, opt ) // to-be-coded
    //, triangleArcPlate_test( mode, opt ) // to-be-coded
    //, trsfbonePts_test( mode, opt ) // to-be-coded
    //, trslN_test( mode, opt ) // to-be-coded
    //, trslN2_test( mode, opt ) // to-be-coded
    //, trslP_test( mode, opt ) // to-be-coded
    , twistangle_test( mode, opt )
    , uv_test( mode, opt )
    //, uvB_test( mode, opt ) // to-be-coded
    //, uvN_test( mode, opt ) // to-be-coded
    , vecOp_test( mode, opt )
    ]
);



//========================================================
{ // textWidth
function textWidth( txt, size=10, scale=1 )=
(   // A wild guess of what the total width of txt is.
    // Used in Write() and Dim()

    // symbol width ratios in reference to average char
    // width that is decided by "size"
    let(symbol_w_ratios  = 
    [  
         "P", 0.906, "Q", 1.055 , "R", 0.98, ".", 0.3768, "1", 0.654
       , "2", 0.7542, "3", 0.7542, "4", 0.7542, "5", 0.7542, "6", 0.7542
       , "7", 0.7542, "8", 0.7542, "9", 0.7542, "S", 0.905, "T", 0.828
       , "U", 0.98 , "V", 0.906, "W", 1.282, "X", 0.906, "Y", 0.906
       , "Z", 0.828, "0", 0.754, "A", 0.903, "B", 0.905, "C", 0.98
       , "D", 0.98, "E", 0.909, "F", 0.83, "G", 1.055, "H", 0.98
       , "I", 0.38, "J", 0.676, "K", 0.905, "L", 0.755, "M", 1.13
       , "N", 0.98, "O", 1.055 , "/", 0.3768, " ", 0.3768, "+", 0.792
       , "-", 0.452, "*", 0.5277, "(", 0.452, ")", 0.452, "a", 0.755
       , "b", 0.755, "c", 0.678, "d", 0.754, "e", 0.754, "f", 0.35
       , "g", 0.754, "h", 0.754, "i", 0.301, "j", 0.301, "k", 0.679
       , "l", 0.301, "m", 1.13, "n", 0.754, "o", 0.754 , "p", 0.754
       , "q", 0.754 , "r", 0.452, "s", 0.678, "t", 0.377, "u", 0.754 
       , "v", 0.678, "w", 0.9795, "x", 0.6785, "y", 0.678, "z", 0.678
       
       , "!", 0.3767, "\"", 0.4815, "#", 0.7543, "$", 0.7543, "%", 1.206
       , "&", 0.904 , "'", 0.259, ",", 0.3768, ":", 0.3768, ";", 0.3768
       , "<", 0.7921, "=", 0.7921, ">", 0.7921, "?", 0.7542, "@", 1.3766
       , "[", 0.3768, "]", 0.3768, "\\", 0.3768, "^", 0.6365, "`", 0.4518
       , "_", 0.75, "|", 0.3522, "~", 0.7921, "{", 0.3625, "}", 0.3625
       
       , chDEG, 0.5423

       ]
        , sz= isnum(size)?size:size[0]//-1
        , sc= isnum(scale)?scale:scale[0] 
        , expwidth = sum( [ for(i=range(txt))
                              hash( symbol_w_ratios, txt[i],0.6
                                  )*sz*sc  //, sz*sc)
                           ] )
        )
    expwidth
);
} // textWidth

