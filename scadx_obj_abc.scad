
/*
scadx_obj_abc.scad (started 2018.12.5)
 
  Collection of alphabet-like polyhedron objects

  Although they might be used to represent customized fonts, the main objective 
  is to provide shapes that minics the alphabets. For example, Obj_O is
  a cube with a hole. 

Ref:

  https://playtype.com/about/typefaces/glossary

*/

/* Representing shapes:

Obj_A

      toff
      |--|                     | tw | 
      .  :     |'.        /|   .----. ----
      . : \    |  \      / |   |    |  bh
       :---\   |---\    /__|   |----| ----
      :     \  |    \  /   |   |    | 

toff   0.5      0        1      0   (top-offset)
tw      0       0        0      1   (top width) 
bh     0.6     0.6      0.7    0.55 (bar height) 
corner  skip    (1)     (2)    (3)  

(1) 
       
*/
ABC_x = 0.8;
ABC_y = 1;
ABC_z = 0.2;
ABC_th= 0.2;
ABC_tile= 0; // tilt angle
ABC_corner=1;
          
ABC_defines=
[ 
   "A", [ "toff", 0.5
        , "tw", 0
        , "bh", 0.6
        , "corner", undef 
        ]
];

//function obj_A_pts() 