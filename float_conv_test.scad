include <doctest.scad>

str=["str","x","str","str"
,"Convert x to string"];

module testfloatconv(fname="str", recs=recs)
{
    recs = fname=="str"?
               [ for (rec=recs) 
                   dt_type(rec)=="str"
                    ?rec
                    :[ str( dt_type(rec[0])=="str"?dt_num(rec[0]):rec[0])
                     ,rec[1],rec[0] ] ]
               :recs;
    result= fname=="str"? doctest(str, recs, ["prec", 10]):recs;
    echo( result[1] );
    echo( dt_red(dt_s("Function {_}() tested: {_}, failed: {_}",[fname, result[2],result[3]] )));
               
};

recs= [ "Integer:"
      , ""
      , ["123456", "123456" ]
      , ["1234560", "1.23456e+06"]  // Sci.Notation 
      , ["1234564", "1.23456e+06" ] // 4 gets cut 
      , ["1234565", "1.23457e+06"]  // Why not 1.23457 ? 
      , ["1234566", "1.23457e+06" ] // 6 advances
      , ""
      , "Float ( > 1 ) -- Rounded up normally to 6 significant digits: cut 4, advance 5"
      , ""
      , ["1.123456", "1.12346"]   
      , ["1.0123456", "1.01235"]   
      , ["1.00123456", "1.00123" ]  
      , ["1.000123456", "1.00012" ]  
      , ["1.0000123456", "1.00001"]        
      , ""
      , "Float ( &lt; 1 ) -- 5 sometimes cut, sometimes advances:"
      , ""
      , ["0.1234565", "0.123457"]  
      , ["0.01234565", "0.0123457"]  
      , ["0.001234565", "0.00123457"]  
      , ["0.0001234565", "0.000123457"]   
      , ["0.00001234565", "1.23457e-05"] 

      ,""
      , "III. This section is to test 6 digits + a 5, like 0.1111115, 0.2222225, etc, and multiply them with 0.1 repeatedly. It appears that the extra-5 is treated in an irregular manner: "
      ,""
      , ["0.1111115", "0.111112"]   
      , ["0.01111115", "0.0111112"]   
      , ["0.001111115", "0.00111112"]  // Why is 5 cut? 
      , ["0.0001111115", "0.000111112"]  // Why is 5 cut? 
      , ["0.00001111115", "1.11112e-05"]  // Why is 5 cut?
      ,""
      , ["0.2222225", "0.222223"]  // Why is 5 cut? 
      , ["0.02222225", "0.0222223"]  // Why is 5 cut? 
      , ["0.002222225", "0.00222223"]  // Why is 5 cut? 
      , ["0.0002222225", "0.000222223"]   
      , ["0.00002222225", "2.22223e-05"]  // Why is 5 cut?
      ,""
      , ["0.3333335", "0.333334"]   
      , ["0.03333335", "0.0333334"]  // Why is 5 cut? 
      , ["0.003333335", "0.00333334"]   
      , ["0.0003333335", "0.000333334"]   
      , ["0.00003333335", "3.33334e-05"]  // Why is 5 cut?
      ,""
      , ["0.4444445", "0.444445"]   
      , ["0.04444445", "0.0444445"]   
      , ["0.004444445", "0.00444445"]  // Why is 5 cut? 
      , ["0.0004444445", "0.000444445"]   
      , ["0.00004444445", "4.44445e-05"]  // Why is 5 cut?
      ,""
      , ["0.5555555", "0.555556"]  // Why is 5 cut? 
      , ["0.05555555", "0.0555556"]   
      , ["0.005555555", "0.00555556"]   
      , ["0.0005555555", "0.000555556"]  // Why is 5 cut? 
      , ["0.00005555555", "5.55556e-05"] // Why is 5 cut?
      ,""
      , ["0.6666665", "0.666667"]   
      , ["0.06666665", "0.0666667"]  // Why is 5 cut? 
      , ["0.006666665", "0.00666667"]  // Why is 5 cut? 
      , ["0.0006666665", "0.000666667"]  // Why is 5 cut? 
      , ["0.00006666665", "6.66667e-05"]  
      ,""
      , ["0.7777775", "0.777778"]   
      , ["0.07777775", "0.0777778"]   
      , ["0.007777775", "0.00777778"]  // Why is 5 cut? 
      , ["0.0007777775", "0.000777778"]  // Why is 5 cut? 
      , ["0.00007777775", "7.77778e-05"]  // Why is 5 cut?
      ,""
      , ["0.8888885", "0.888889"]  // Why is 5 cut? 
      , ["0.08888885", "0.0888889"]   
      , ["0.008888885", "0.00888889"]  // Why is 5 cut? 
      , ["0.0008888885", "0.000888889"]  // Why is 5 cut? 
      , ["0.00008888885", "8.88889e-05"]  // Why is 5 cut?
      ,""
      , ["0.9999995", "1"]   
      , ["0.09999995", "0.1"]   
      , ["0.009999995", "0.00999999"]  // Why is 5 cut? 
      , ["0.0009999995", "0.001"]   
      , ["0.00009999995", "0.0001"] 

      ];          
  
//echo( dt_num_test() [1] ); 
testfloatconv();
               
/*

III. This section is to test 6 digits + a 5, like 0.1111115, 0.2222225, etc, and multiply them with 0.1 repeatedly. It appears that the extra-5 is treated in an irregular manner: 
 str(0.1111115)     = 0.111112   
 str(0.01111115)    = 0.0111112   
 str(0.001111115)   = 0.00111111  // Why is 5 cut? 
 str(0.0001111115)  = 0.000111111  // Why is 5 cut? 
 str(0.00001111115) = 1.11111e-05  // Why is 5 cut?
 str(0.2222225)     = 0.222222  // Why is 5 cut? 
 str(0.02222225)    = 0.0222222  // Why is 5 cut? 
 str(0.002222225)   = 0.00222222  // Why is 5 cut? 
 str(0.0002222225)  = 0.000222223   
 str(0.00002222225) = 2.22222e-05  // Why is 5 cut?
 str(0.3333335)     = 0.333334   
 str(0.03333335)    = 0.0333333  // Why is 5 cut? 
 str(0.003333335)   = 0.00333334   
 str(0.0003333335)  = 0.000333334   
 str(0.00003333335) = 3.33333e-05  // Why is 5 cut?
 str(0.4444445)     = 0.444445   
 str(0.04444445)    = 0.0444445   
 str(0.004444445)   = 0.00444444  // Why is 5 cut? 
 str(0.0004444445)  = 0.000444445   
 str(0.00004444445) = 4.44445e-05  // Why is 5 cut?
 str(0.5555555)     = 0.555555  // Why is 5 cut? 
 str(0.05555555)    = 0.0555556   
 str(0.005555555)   = 0.00555556   
 str(0.0005555555)  = 0.000555555  // Why is 5 cut? 
 str(0.00005555555) = 5.55555e-05  // Why is 5 cut?
 str(0.6666665)     = 0.666667   
 str(0.06666665)    = 0.0666666  // Why is 5 cut? 
 str(0.006666665)   = 0.00666666  // Why is 5 cut? 
 str(0.0006666665)  = 0.000666666  // Why is 5 cut? 
 str(0.00006666665) = 6.66667e-05  
 str(0.7777775)     = 0.777778   
 str(0.07777775)    = 0.0777778   
 str(0.007777775)   = 0.00777777  // Why is 5 cut? 
 str(0.0007777775)  = 0.000777777  // Why is 5 cut? 
 str(0.00007777775) = 7.77777e-05  // Why is 5 cut?
 str(0.8888885)     = 0.888888  // Why is 5 cut? 
 str(0.08888885)    = 0.0888889   
 str(0.008888885)   = 0.00888888  // Why is 5 cut? 
 str(0.0008888885)  = 0.000888888  // Why is 5 cut? 
 str(0.00008888885) = 8.88888e-05  // Why is 5 cut?
 str(0.9999995)     = 1   
 str(0.09999995)    = 0.1   
 str(0.009999995)   = 0.00999999  // Why is 5 cut? 
 str(0.0009999995)  = 0.001   
 str(0.00009999995) = 0.0001  
IV. In this observation, 7 digits of 5 (5555555) is multiplied by pow(10,n). Again, this shows the extra-5 is treated unpredictably: 
 str(5555555000)   = 5.55556e+09   
 str(555555500)    = 5.55556e+08   
 str(55555550)     = 5.55556e+07   
 str(5555555)      = 5.55556e+06   
 str(5.555555)     = 5.55556   
 str(0.5555555)    = 0.555555  // Why is 5 cut? 
 str(0.05555555)   = 0.0555556   
 str(0.005555555)  = 0.00555556   
 str(0.0005555555) = 0.000555555  // Why is 5 cut? 
 str(0.00005555555)= 5.55555e-05  // Why is 5 cut?
*/ 