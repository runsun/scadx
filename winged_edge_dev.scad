include <scadx.scad>

  
/*

 ------Q---------
       |
       |
  1    |    2
       |
       |
       |
-------P------------

*/


pts=  growPts( ORIGIN, [ "x", 5
                       , "y", 5
                       , "x",-5
                       , "t",[0, -5,5]
                      // , "y",-5
                      // , "z", 8
                       
                       , "x", 5
                       , "y", 5
                       , "x",-5
                       , "t",[0, -5,5]
                       
                       , "x", 5
                       , "y", 5
                       , "x",-5
                       , "t",[0, -5,5]
                       
                       , "x", 5
                       , "y", 5
                       , "x",-5
                      // , "y",-5
                       ] );

echo(pts=pts);
MarkPts( pts, "l=[size,2]" );
for(i=[0:3]) Line( slice(pts, i*4,(i+1)*4) );
for(i=[0:3]) Line( [pts[i],pts[i+12]] );
for(i=[0:3]) Line( [pts[i*4],pts[i*4+3]] );

//polyhedron( pts, faces("chain") );


//w= [ 0, ["pq",[P,Q], "faces",[ 1,2], "left", ["b","a"], "right",["e","c"]]
//];


