


//========================================================
{ // type
function type(x)= 
(
	x==undef?undef:
    floor(x)==x? "int"
	:( abs(x)+1>abs(x)?"float"
//	( abs(x)+1>abs(x)?
//	floor(x)==x?"int":"float"
	: str(x)==x?"str"
	: str(x)=="false"||str(x)=="true"?"bool"
	: (x[0]==x[0])&&len(x)!=undef? "arr" // range below doesn't have len
    : let( s=str(x) 
         , s2= split(slice(s,1,-1)," : ")
         ) 
         s[0]=="[" && s[len(s)-1]=="]" 
       //  && all( [ for(x=s2) type(x)=="int" ] )?"range"
         && all( [ for(x=s2) isint(int(x)) ] )?"range"
    :"unknown"
	)
);
    type=[ "type", "x", "str", "Type, Inspect" ,
    " Given x, return type of x as a string.
    "
    ];
    function type_test( mode=MODE, opt=[] )=
    (
    doctest( type,
    [  
    [ type([2,3]), "arr" , [2,3]]
    , [ type([]), "arr" , "[]"]
    , [ type(-4), "int", "-4"]
    , [ type( 0 ), "int", "0"]
    , [ type( 0.3 ), "float", "0.3"]
    , [ type( -0.3 ), "float", "-0.3"]
    , [ type(2e-6), "float", "2e-6"]
    , [ type(2e3), "int", "2e3"]
    , [ type(2e-06), "float", "2e-06"]
    , [ type(2e+03), "int", "2e+03"]
    , [ type("a"), "str", "'a'"]
    , [ type("10"), "str", "'10'"]
    , [ type(true), "bool", "true"]
    , [ type(false), "bool", "false"]
    , [ type([1:2:10]), "range", "[1:2:10]", ["//","New 2015.9.10"]]
    , [ type([1:1.5:10]), "range", "[1:1.5:10]", ["//","New 2015.9.10"]]
    , [ type(undef), undef, "undef"]
    , "// 2015.11.17 "
    , [ type(12345678901234500), "int", "12345678901234500", ["//","Bugfix 2015.11.17"]]

    ],mode=mode, opt=opt

    )
    );
} // type 

//========================================================
{ // type
function xtype(x)= 
(
    x==[]?"arr"
    :ishash(x)? "hash"
    :isnums(x)? "nums"
    : type(x)
);
    xtype=[ "xtype", "x", "str", "Type, Inspect" ,
    " Given x, return type of x as a string w/ some new features than type().
    - 2018.2.1
    "
    ];
    function xtype_test( mode=MODE, opt=[] )=
    (
    doctest( xtype,
    [  
      "// xtype's new features comparing to type():"
       
    , [ xtype([2,3]), "nums" , [2,3]]
    , [ xtype([1,2,3]), "nums" , [1,2,3]]
    , [ xtype(["2",3]), "hash" , "['2',3]"]
    , [ xtype(["2",3,4]), "arr" , "['2',3,4]"]
    , ""
    , "// Same as type:"
    
    , [ xtype([]), "arr" , "[]"]
    , [ xtype(-4), "int", "-4"]
    , [ xtype( 0 ), "int", "0"]
    , [ xtype( 0.3 ), "float", "0.3"]
    , [ xtype( -0.3 ), "float", "-0.3"]
    , [ xtype(2e-6), "float", "2e-6"]
    , [ xtype(2e3), "int", "2e3"]
    , [ xtype(2e-06), "float", "2e-06"]
    , [ xtype(2e+03), "int", "2e+03"]
    , [ xtype("a"), "str", "'a'"]
    , [ xtype("10"), "str", "'10'"]
    , [ xtype(true), "bool", "true"]
    , [ xtype(false), "bool", "false"]
    , [ xtype([1:2:10]), "range", "[1:2:10]", ["//","New 2015.9.10"]]
    , [ xtype([1:1.5:10]), "range", "[1:1.5:10]", ["//","New 2015.9.10"]]
    , [ xtype(undef), undef, "undef"]
    , "// 2015.11.17 "
    , [ xtype(12345678901234500), "int", "12345678901234500", ["//","Bugfix 2015.11.17"]]

    ],mode=mode, opt=opt

    )
    );
} // type 

//========================================================
{ // gtype 
    
function gtype(x, dim=3)=
(
    ispt(x, dim)?"pt"
    :isline(x, dim)?"ln"
    :ispl(x, dim)?"pl"
    :iscir(x,dim)?"cir"
    :isball(x,dim)?"ball"
    :undef
);
    gtype=["gtype","x,dim=3","pt|ln|pl|cir|ball|false", "Type,Inspect",
    "Return what type of geometry shape x is. It could be one
    ;; of ('pt','ln','pl','cir','ball'), indicating that x is
    ;; a point, line, plane, circle, ball, resp. If none of them,
    ;; return undef. 
    ;;
    ;; They are determined by the criteria below:
    ;;
    ;;  pt: [i,j,k]
    ;;  ln: [p,q] and p!=q
    ;;  pl: [p,q,r] and p!=q!=r and not co-linear 
    ;;  cir: [ ln,r] or [r,ln] in 3D
    ;;       or [pt,r] or [r,pt] in 2D
    ;;  ball: [ pt,r] or [r,pt] in 3D
    ;;  
    ;; dim : dimension. Default=3 
    "];

    function gtype_test( mode=MODE, opt=[] )=
    (
        let( P=[2,3,4]
           , Q=[5,6,7]
           , R=[3,5,7]
           )
        
        doctest( gtype,
        [
          [gtype( P ), "pt", "P"]
        , [gtype( [P,Q] ), "ln", "[P,Q]"]
        , [gtype( [P,Q,R] ), "pl", "[P,Q,R]"]
        , [gtype( "a" ), undef, "'a'"]
        , [gtype( [P,Q,R,P] ), undef, "[P,Q,R,P]"]
        , [gtype( [[P,Q],1] ), "cir", "[[P,Q],1]"]
        , [gtype( [2,[P,Q]] ), "cir", "[2,[P,Q]]"]
        , [gtype( [P,1] ), "ball", "[P,1]"]
        , [gtype( [0.5,P] ), "ball", "[0.5,P]"]
        
        ], mode=mode, opt=opt, scope=["P",P,"Q",Q,"R",R]
        )
    );
    //echo( gtype_test(mode=112)[1] );
} // gtype
                     
//========================================================
{ // begmath          
function begmatch( s, ptns )= // ptn = [ [name, w0,w] ... ]
(
    let( rtn=[ for(p=ptns) 
               let( m= begword(s, w0=p[1], w=p[2], we=p[3]))
               if(m) [p[0],m]
             ]
       )
   rtn?rtn[0]:undef
);
        
    begmatch=["begmatch","s,ptns", "arr|undef", "Core, Inspect"
    , "Check what pattern matches beginning of s. Return [pattern_name,matched_word] or undef.
    ;; 
    ;; ptns = list of [pattern_name, w0, w] 
    ;;        (see begword for the definition of w0 and w)
    ;; 
    ;; Example:
    ;; 
    ;; begmatch( 'a+b/2'
    ;;         , [ ['op','+-*/']
    ;;           , ['word']  // use default begword settings
    ;;           , ['num', '.0123456789']
    ;;           ] )
    "
    ,"begwith,begword"
    ];

    function begmatch_test( mode=MODE, opt=[] )=
    (
        let(ptns= [ ["op","+-*/",""]
              , ["word"]  // use default begword settings
              , ["num", ".0123456789",".0123456789"]
              ])
        doctest( begmatch,
        [
          "var ptns"
        , ""
        , [ begmatch( "ab+2.3/cd", ptns ), ["word","ab"],"'ab+2.3/cd',ptns"]
        , [ begmatch( "+2.3/cd", ptns ), ["op","+"],"'+2.3/cd',ptns"]
        , [ begmatch( "2.3/cd", ptns ), ["num","2.3"],"'2.3/cd',ptns"]
        , [ begmatch( "/cd", ptns ), ["op","/"],"'/cd',ptns"]
        , 
        ]
        , mode=mode, opt=opt, scope=["ptns",ptns]
        )
    );               
} // begmath

//========================================================
{ // begwith
function begwith(o,x)= 
(
	x==""||x==[]
	? undef
	: isarr(x)
	  ? [ for(i=range(x)) if(index(o,x[i])==0) x[i] ] [0]
	  : index(o,x)==0
);

    begwith=["begwith","o,x","true|false", "String,Array,Inspect"
    , "Check if o (str|arr) starts with x.
    ;; 
    ;; begwith('abcdef', 'abc')= true
    ;; begwith('abcdef', '')= undef
    ;; begwith(['ab','cd','ef'], 'ab')= true
    ;; 
    ;; New 2014.8.11:
    ;; 
    ;; x can be an array, return true if o begins with any item in x.
    ;; 
    ;; New 2015.2.17
    ;; 
    ;;  If x is array, instead of returning T/F, return the m or undef
    ;;  where m is the item in x that matches.
    ;; 
    ;; begwith('abcdef',['abc','ghi'])= 'abc' 
    ;; begwith(['ab','cd','ef'],['gh','ab'])= 'ab'
    "    
    ,"begword,endwith"
    ];

    function begwith_test( mode=MODE, opt=[] )=
    (
        doctest( begwith, 
        [ 
          [ begwith("abcdef","def"), false, "'abcdef', 'def'" ]
        , [ begwith("abcdef","abc"), true, "'abcdef', 'abc'" ]
        , [ begwith("abcdef", ""), undef, "'abcdef', '" ]
        , [ begwith(["ab","cd","ef"], "ab"), true, "['ab','cd','ef'], 'ab'"]
        , [ begwith([], "ab"), false, "[], 'ab'"]
        , [ begwith("", "ab"), false, "', 'ab'"]
        , "// New 2014.8.11"
        , [ begwith("abcdef",["abc","ghi"]),"abc", "'abcdef',['abc','ghi']"] 
        , [ begwith("ghidef",["abc","ghi"]),"ghi", "'ghidef',['abc','ghi']"] 
        , [ begwith("aaadef",["abc","ghi"]),undef, "'aaadef',['abc','ghi']"] 
        , [ begwith(["ab","cd","ef"], ["gh","ab"])
             , "ab", "['ab','cd','ef'], ['gh','ab']"]
        ]
        , mode=mode, opt=opt )
    );
} // begwith

// ========================================================
{ //begword_
function begword_(
      s
    , isnum = false
    , wb= "" //str(a_z, A_Z, "_") // df letter for s[0]
    , w = "" //str(a_z, A_Z, "_", "1234567890") // df letters
    , we= "" 
    , dwb= str(a_z, A_Z, "_")
    , dw= str(a_z, A_Z, "_", "1234567890")  
    ) =
   let( _w0= isstr(wb)
      , _w = w==undef?str(a_z, A_Z, "_", "1234567890"):w // df chars
      //, _we= we //we==undef?str(a_z, A_Z, "_", "1234567890"):we // df chars
      , w0 = isnum? "1234567890.": _w0 
      , w  = isnum? "1234567890.": _w 
      //, w  = isnum? "": _w 
      , s0 = has(w0, s[0])?s[0]:false
      , _i = [for(i=range(s)) // index of the 1st unmatch
                let( m= has( i==0?w0:w, s[i]) )
                if((!m) && ( we==undef? true
                             : has(we,s[i])
                           )
                  ) i ][0] 
      ,rtn = _i==0?"": !_i? s
             :( !s0?"" 
                : _i==undef? s0
                : slice( s, 0, we&&_i?(_i+1):_i) 
              )
//      ,_debug= _fmth([ "s=",s
//                , "_i=",_i
//                , "w0",w0
//                , "w", w
//                , "we",we
//                , "rtn=", rtn ])     
      )
(
   //_debug //join(_debug ,"<br/>")
   rtn 
);
} // begword_       
                
                
                
//=============================================================
{ // begword    
function begword(
      s
    , isnum = false
    , w0= undef //str(a_z, A_Z, "_") // df letter for s[0]
    , w = undef //str(a_z, A_Z, "_", "1234567890") // df letters
    , we= undef 
    ) =
   let( _w0= w0==undef?str(a_z, A_Z, "_"):w0 // df char for s[0]
      , _w = w==undef?str(a_z, A_Z, "_", "1234567890"):w // df chars
      //, _we= we //we==undef?str(a_z, A_Z, "_", "1234567890"):we // df chars
      , w0 = isnum? "1234567890.": _w0 
      , w  = isnum? "1234567890.": _w 
      //, w  = isnum? "": _w 
      , s0 = has(w0, s[0])?s[0]:false
      , _i = [for(i=range(s)) // index of the 1st unmatch
                let( m= has( i==0?w0:w, s[i]) )
                if((!m) && ( we==undef? true
                             : has(we,s[i])
                           )
                  ) i ][0] 
      ,rtn = _i==0?"": !_i? s
             :( !s0?"" 
                : _i==undef? s0
                : slice( s, 0, we&&_i?(_i+1):_i) 
              )
//      ,_debug= _fmth([ "s=",s
//                , "_i=",_i
//                , "w0",w0
//                , "w", w
//                , "we",we
//                , "rtn=", rtn ])     
      )
(
   //_debug //join(_debug ,"<br/>")
   rtn 
);
                
    begword=["begword","s,isnum=false,w0=...,w=...","str","String"
    //begword=["begword","s,w,w0,isnum=false,dfw0=..., dfw=...","str","String"
    ,"Match beginning of s to word defined by w0 & w. Return the word matched or ''.
    ;; 
    ;; The pattern is defined as chars-allowed in w0 and w. 
    ;; It will first check if the 1st char can be found in w0, 
    ;; followed by checking if each char can be found in w 
    ;; until it fails. 
    ;; 
    ;; default:
    ;; 
    ;;   w0= str(a_z, A_Z, '_')  // df letter for s[0]
    ;;   w = str(a_z, A_Z, '_', '1234567890') // df letters
    ;; 
    ;; where a_z = 'abcdefghijklmnopqrstuvwxyz'
    ;; 
    ;; Set isnum=true to return the beginning digits
    ;; 
    ;;   begword( 'abc123,def' ) => 'abc123'
    ;;   begword( '123abc,def' ) => ''
    ;;   begword( '123abc,def',isnum=true ) => '123'
    "
    ,"begwith,endwith"];

    function begword_test( mode=MODE, opt=[] )=
    (
        doctest( begword
        , [
            [ begword("abc+123"), "abc","'abc+123'" ]
           ,[ begword("123"), "", "'123'"]
           ,[ begword("b123"), "b123", "'b123'"]
           ,[ begword("ab_c123"), "ab_c123", "'ab_c123'" ]
           ,[ begword("ab_c123",w="bc"), "ab", "'ab_c123',w='bc'"
                    ]
           ,[ begword("x01+123"), "x01", "'x01+123'"]
           
           ,[ begword("abc123+5"), "abc123", "'abc123+5'" ]
           ,[ begword("_x123+5"), "_x123", "'_x123+5'" ]
           ,""
           ,"// Return the leading spaces: "
           ,[ begword("   abc",w0=" ",w=" "), "   "
                 , "'   abc',w0=' ',w=' '"]
           ,""
           ,"// Match single char: "
           ,[ begword("+abc",w0="+-*",w=""), "+"
                 , "'+abc',w0='+-*',w=''"]
           ,[ begword("*-abc",w0="+-*",w=""), "*"
                 , "'*-abc',w0='+-*',w=''"]
           ,""
           ,"// Find leading numbers: "      
           ,[ begword("12.3+abc",isnum=true), "12.3", "'12.3+abc',isnum=true"]
           ,[ begword(".123+abc",isnum=true), ".123", "'.123+abc',isnum=true"]
           ,[ begword("0.5+abc",isnum=true), "0.5","'0.5+abc',isnum=true"]
           ,[ begword("12.3",isnum=true), "12.3","'12.3',isnum=true"]
           ,[ begword("3+a",isnum=true), "3","'3+a',isnum=true"]
           ,[ begword("(3+a)",isnum=true), "","'(3+a)',isnum=true"]
           ,[ begword("(3+a)"), "","'(3+a)'"]
           
           ,""
           ,"// new: we (word end)"
           ,[ begword("{abc}+123",w0="{",we="}"), "{abc}",
                    "'{abc}+123',w0='{',we='}'" ]
           ,[ begword("sin(2+3)/2",w0="nis"
                      ,w=str("(",a_z,A_Z,"+-*/","0123456789")
                      , we=")")
             , "sin(2+3)"
             ,"'sin(2+3)/2',w0='nis',w=str('(',a_z,A_Z,'+-*/','0123456789') , we=')'" ]
         
    //       ,"", "// It's too expensive to solve the problem below, so we leave it:"
    //       ,""
    //       ,str("> begword(\".12.3+abc\",isnum=true)= ", "\"\""
    //            , str("\"",_red( begword(".12.3+abc",isnum=true) ) )
    //           )
    //       , [ begword(".12.3+abc",isnum=true), ".12.3", "'.12.3+abc',isnum=true"] 
           
        ], mode=mode, opt=opt )
    );
} // begword 

// ========================================================
{ // between 
    
function between( low, n, high, include=0 )=
(
  (
	(include==1 || include[0]) ? 
		( low<=n) : (low<n )
  )&&
  (
	(include==1 || include[1]) ? 
		( n<=high) : (n<high )
  )
);

    between=["between", "low,n,high,include=0", "T|F", "Inspect"
    , "Check if n is between low/high bound.
    ;; 
    ;; include: decides if the bounds be included. Set it to 0 
    ;; (open, not included) or 1 (closed, included), or [0,1] 
    ;; or [1,0] to set them differently.
    ;; 
    ;;   between( 3,a,8 )= true
    ;;   between( 3,a,5 )= false
    ;;   between( 3,a,5,[0,1] )= true // Include right end
    ;;   between( 3,a,5,1 )= true // Include both ends
    "];

    function between_test( mode=MODE, opt=[] )=
    (
       let(a=5)
       doctest( between,
        [
           "var a",""
          ,[ between(3,a,8),true, "3,a,8" ]
          ,[ between(3,a,5),false, "3,a,5" ]
          ,[ between(3,a,5,[0,1]),true, "3,a,5,[0,1]", ["//","Include right end"] ]
          ,[ between(3,a,5,1),true, "3,a,5,1",  ["//","Include both ends"] ]
          ,""	
          ,[ between(5,a,8),false, "5,a,8" ]
          ,[ between(5,a,8,[0,1]),false, "5,a,8,[0,1]",  ["//","Include right end"] ]
          ,[ between(5,a,8),false,"5,a,8" ]
          ,[ between(5,a,8,[1,0]),true, "5,a,8,[1,0]", ["//","Include left end"] ]
          ,[ between(5,a,8,1),true, "5,a,8,1", ["//","Include both ends"] ]
        ], mode=mode, scope=["a",a], opt=opt )
    );
} // between

//========================================================
function count(s, arr, _i=0, _rtn=0)=  // 2017.4.19
(	
    _i>=len(arr)?_rtn
    : let( _rtn= _rtn+ (s==arr[_i]?1:0) )
      count( s, arr, _i=_i+1, _rtn=_rtn )
);


function countAll(arr, _i=0, _collected=[], _rtn=[])=  // 2017.4.19
(
    _i>=len(arr)?_rtn
    : let( s= arr[_i])
      _i==0
      ? countAll(arr, _i=1, _collected=[s], _rtn=[s,1] )
      : has( _collected, s) 
        ? countAll( arr, _i=_i+1
                   , _collected= _collected
                   , _rtn= update( _rtn, [s, hash(_rtn, s)+1] )
                   )  
        : countAll( arr, _i=_i+1
                   , _collected= concat( _collected, [s] )
                   , _rtn= update( _rtn, [s, 1] )
                   )
);


//========================================================
{ // countType
    
function countType(arr, typ, _i_=0)=
(	
    typ=="num"
    ?len( [ for(x=arr) let(t=type(x)) if( t=="float"||t=="int") 1] )
    :len( [ for(x=arr) let(t=type(x)) if( t==typ) 1] )
);
    
    countType= ["countType", "arr,typ", "int", "Array, Inspect, Type",
    " Given array (arr) and type name (typ), count the items of type typ in arr.\\
    "];

    function countType_test( mode=MODE, opt=[] )=
    (
        doctest( countType,
        [
          [ countType(["x",2,[1,2],5.1],"str"),1
            , "['x',2,[1,2],5.1], 'str'" ]
        , [ countType(["x",2,[1,2],5.1,3], "int"),2
            , "['x',2,[1,2],5.1,3], 'int'" ]
        , [ countType(["x",2,[1,2],5.1,3] ,"float"),1
            , "['x',2,[1,2],5.1,3], 'float'" ]
        ] 
        , mode=mode, opt=opt )
    ); 
} // countType

//========================================================
{ // countArr
function countArr(arr)= countType(arr, "arr");

    countArr= ["countArr", "arr", "int", "Array, Inspect, Type",
    " Given array (arr), count the number of arrays in arr.
    "];

    function countArr_test( mode=MODE, opt=[] )=
    (
        doctest( countArr,
        [
          [ countArr(["x",2,[1,2],5.1]),1, ["x",2,[1,2],5.1]]
        ]
        , mode=mode, opt=opt
        )
    );
} // countArr

//========================================================
{ //countInt
function countInt(arr)= countType(arr, "int");

    countInt=["countInt", "arr", "int", "Array, Inspect, Type",
    " Given array (arr), count the number of integers in arr.
    "];

    function countInt_test( mode=MODE, opt=[] )=
    (
        doctest( countInt, 
        [
          [ countInt(["x",2,"3",5.3,4]),2, "['x',2,'3',5.3,4]"]
        ]
        , mode=mode, opt=opt
        )
    );
} // countInt

//========================================================
{ // countNum
function countNum(arr)= countType(arr, "num");

    countNum= ["countNum", "arr", "int", "Array, Inspect, Type",
    " Given array (arr), count the number of numbers in arr.
    "];

    function countNum_test( mode=MODE, opt=[] )=
    (
        doctest( countNum,
        [
          [ countNum(["x",2,"3",5.3,4]),3, "['x',2,'3',5.3,4]" ]
        ]
        , mode=mode, opt=opt
        )
    );
} // countNum

//========================================================
{ // countStr
function countStr(arr)= countType(arr, "str") ;    
    
    countStr =  ["countStr", "arr", "int", "Array, Inspect, Type",
    " Given array (arr), count the number of strings in arr.
    "];

    function countStr_test( mode=MODE, opt=[] )=
    (
        doctest( countStr,
        [
          [ countStr(["x",2,"3",5]),2, "['x',2,'3',5]" ]
        , [ countStr(["x",2,5]),1, "['x',2, 5]" ]
        ]
        , mode=mode, opt=opt
        )
    ); // countStr_test();
} // countStr

//========================================================
{ // endwith
function endwith(o,x)=   
( 
    isarr(o)?
        isstr(x)
            ? last(o)==x
            :[for(y=x) if(last(o)==y) y][0]
    : isstr(x)
            ? slice(o,-len(x))==x
            :[for(y=x) if(slice(o,-len(y))==y) y][0] 
);
 
    endwith=[ "endwith", "o,x", "T/F",  "String, Array, Inspect",
    "
     Given a str|arr (o) and x, return true if o ends with x.
    ;;
    ;; New 2014.8.11:
    ;;
    ;;  x can be an array, return true if o ends with any item in x.
    ;;
    ;; New 2015.2.17
    ;;
    ;;  If x is array, instead of returning T/F, return the m or undef
    ;;  where m is the item in x that matches.
    ;;
    ;; Ref: begwith()
    "];

    function endwith_test( mode=MODE, opt=[] )=
    (
        doctest( endwith, 
        [ 
        [ endwith("abcdef", "def"), true, "'abcdef', 'def'" ]
        , [ endwith("abcdef", "abc"), false, "'abcdef', 'def'" ]
        , [ endwith(["ab","cd","ef"], "ef"),true, "['ab','cd','ef'], 'ef'" ]
        , [ endwith(["ab","cd","ef"], "ab"),false, "['ab','cd','ef'], 'ab'" ]
        , [ endwith(33, "abc"), false, "33, 'def'" ]
        , "// New 2014.8.11"
        , [ endwith("abcdef", ["def","ghi"]), "def", "'abcdef', ['def','ghi']" ] 
        , [ endwith("abcghi", ["def","ghi"]),  "ghi", "'abcghi', ['def','ghi']" ] 
        , [ endwith("aaaddd", ["def","ghi"]), undef, "'abcddd', ['def','ghi']" ] 
        , [ endwith(["ab","cd","ef"], ["gh","ef"]),"ef", "['ab','cd','ef'], ['gh','ef']"]	
        ]
        , mode=mode, opt=opt
        )
    );            
} // endwith  
////========================================================
{ // endword 
    
function endword(  
      s
     //, isnum = false
    //, w0= str(a_z, A_Z, "_") // df letter for s[0]
    //, w = str(a_z, A_Z, "_", "1234567890") // df letters
    ) =
(
    let( m = match_at_rev(s, len(s)-1, str(A_z,"_"))
       )
    //echo(s=s, m=m)   
    m && (m[1]==len(s))? m[2]: ""   
);

    endword=["endword","s","str","String",
      "Check if s ends with word."
    //,"Match beginning of s to word defined by w0 & w. Return the word matched or ''.
    //;; 
    //;; The pattern is defined as chars-allowed in w0 and w. 
    //;; It will first check if the 1st char can be found in w0, 
    //;; followed by checking if each char can be found in w 
    //;; until it fails. 
    //;; 
    //;; default:
    //;; 
    //;;   w0= str(a_z, A_Z, '_')  // df letter for s[0]
    //;;   w = str(a_z, A_Z, '_', '1234567890') // df letters
    //;; 
    //;; where a_z = 'abcdefghijklmnopqrstuvwxyz'
    //;; 
    //;; Set isnum=true to return the beginning digits
    //;; 
    //;;   endword( 'abc123,def' ) => 'abc123'
    //;;   endword( '123abc,def' ) => ''
    //;;   endword( '123abc,def',isnum=true ) => '123'
    //"
    ,"begwith,endwith"];

        function endword_test( mode=MODE, opt=[] )=
        (
          //doctest( endword, mode=mode, opt=opt )
           doctest( endword
          , [
              [ endword("123+abc"), "abc","'123+abc'" ]
             ,[ endword("123"), "", "'123'"]
             ,[ endword("a123b"), "b", "'a123b'"]
             ,[ endword("ab_c123"), "", "'ab_c123'" ]          
             ,[ endword("   abc"), "abc", "'   abc'"]
             ,[ endword("3+a"), "a","'3+a'"]               
             ,[ endword("3+_a_"), "_a_","'3+_a_'"]               
             ,[ endword("3+a."), "","'3+a.'"]               
             ,""
             //,"// new: we (word end)"
             //,[ endword("{abc}+123",w0="{",we="}"), "{abc}",
                      //"'{abc}+123',w0='{',we='}'" ]
             //,[ endword("sin(2+3)/2",w0="nis"
                        //,w=str("(",a_z,A_Z,"+-*/","0123456789")
                        //, we=")")
               //, "sin(2+3)"
               //,"'sin(2+3)/2',w0='nis',w=str('(',a_z,A_Z,'+-*/','0123456789') , we=')'" ]
               
            ], mode=mode, opt=opt )
        );
} // endword  
      


{ // has
function has(o,x)= 
    !(isarr(o)||isstr(o))?false // new 2015.3.11
    //:isarr(x)? len([ for(a=x) if (index(o,a)>-1) 1])>0
    :isarr(x)? ( max( [for(oo=o) if(oo==x) 1])>0 )
    
    :index(o,x)>-1;

    has=["has","o,x","T|F","String, Array, Inspect",
    " Given an array or string (o) and x, return true if o 
    ;; contains x. 
    ;;
    ;;   has( 'abcdef','d' )= true
    ;;   has( 'abcdef','m' )= false
    ;;   has( [2,3,4,5],3 )= true
    ;;   has( 'abcdef',['d','y'] )= true 
    ;;
    ;; 2015.3.11: non-arr and non-str gets false
    ;;
    ;;   has(false,...) = false   
    ;;   has(3,...) = false   
    "];

    function has_test( mode=MODE, opt=[] )=
    (
        doctest( has,
        [ 
          [has("abcdef","d"),true, "'abcdef','d'"]
        , [has("abcdef","m"),false, "'abcdef','m'"]
        , [has([2,3,4,5],3),true, "[2,3,4,5],3"]	
        , [has([2,3,4,5],6),false, "[2,3,4,5],6"]	
        , [has([2,3,4,5],undef),false, "[2,3,4,5],undef"]
        , ""
        , "// New 2015.2.16: x could be an array, and return true if has any"
        , "// Revise 2018.3.29: if x is AoA, o must be an AoA"
        , ""
        , [has("abcdef",["d","y"]),false, "'abcdef',['d','y']"]
        , [has("abcdef",["m","y"]),false,"'abcdef',['m','y']"]
        , [has([2,3,4,5],[3,9]),false,"[2,3,4,5],[3,9]"]	
        , [has([2,3,4,5],[6,9]),false,"[2,3,4,5],[6,9]"]	
        , [has([2,[3,4],5],[3,4]),true,"[2,[3,4],5],[3,4]"]	
        
        ]
        ,mode=mode, opt=opt )
    );	
} // has


//========================================================
{ // is0
    
function is0(n) = abs(n)<ZERO;

    is0=["is0","n","T|F","Inspect",
    str( " Given a number, check if it is 'equivalent' to zero. The 
    ;; decision is made by comparing it to the constant, ZERO=", ZERO )
    ,"isequal"
    ];

    function is0_test( mode=MODE, opt=[] )=
    (
        doctest( is0,
        [
          "// A value, 1e-15, is NOT zero:"
        , str("//   1e-15==0: ", 1e-15==0 )
        , "// but it's so small that it is equivalent to zero:"
        , [ is0(1e-15), true, "1e-15" ]
        ]
        , mode=mode, opt=opt
        )
    );
} // is0

//========================================================
{ // is90
function is90(pq1, pq2)=
(    
   let( pqr = pq2==undef? pq1
               : [pq1[0],pq1[1], pq1[1]+p2v(pq2)]
      )
   is0(  (pqr[0]-pqr[1])*(pqr[2]-pqr[1]) )   
 
//    let( pqr = pq2==undef? pq1
//               : [pq1[0],pq1[1], pq1[1]+p2v(pq2)]
//       , P=pqr[0], Q=pqr[1], R=pqr[2]
//       , pq2 = dist_2(P,Q)
//       , qr2 = dist_2(Q,R)
//       , pr2 = dist_2(P,R)
//       )
//    is0( pr2-pq2-qr2 )
    
    
);

    is90=["is90", "pq1,pq2", "T|F", "Inspect",
    "Check if two lines form 90 degree angle. Don't have to be co-plane.
    ;;
    ;; New 20140613: can take a single 3-pointer, pqr:
    ;;
    ;;   is90( pqr );
    ;;
    ;; Same as angle( pqr )==90. Refer to : othoPt_test() for testing.
    "];

    function is90_test( mode=MODE, opt=[] )=
    (
        let( pqr = randPts(3)           // 3 random points
           , sqPts = squarePts(pqr)   //  This gives 4 points of a square, must be perpendicular
                                  // The first 2 pts = pq[0], pq[1]. 	

           , pq1 = [ pqr[0], pqr[1]   ]  // Points 0,1 of the square
           , _pq2= [ pqr[1], sqPts[2] ]  // Points 1,2 of the square

        // Make a random translation to pq2x:
           , t = randPt()
           , pq2 = [ _pq2[0]+t, _pq2[1]+t ]
           )
           
        doctest( is90,
        [
          "// A 3-pointer, pqr, is randomly generated. It is then made to generate"
         ,"// a square with sqarePts(pqr), in which side PQ is our pq1, and a side"
         ,"// perpendicular to pq is translated to somewhere randomly and taken as pq2."
        ,  [ is90(pq1,pq2), true, "pq1,pq2"  ]
        ]
        ,mode=mode, opt=opt, scope=["pq1",pq1,"pq2",pq2]  
        )
    );
} // is0

//========================================================

{ //isarr
function isarr(x)= type(x)=="arr";

    isarr=[ "isarr","x", "T|F", "Type, Inspect, Array",
    "
     Given x, return true if x is an array.
    "];
     
    function isarr_test( mode=MODE, opt=[] )=(
        doctest( isarr, 
        [ [ isarr([1,2,3]), true, [1,2,3]]
        , [ isarr("a"), false, "'a'"]
        , [ isarr(true), false, "true"]
        , [ isarr(false), false, "false"]
        , [ isarr(undef), false, "undef"]
        ],mode=mode, opt=opt
        )
    );
 } // isarr     
     
//========================================================
{ // isat 
function isat(s,i,x)= // why need this ? 
(
   isarr(x)? [ for(w=x) if(isat(s,i,w)) w ][0]
   : len(x)>1 ?
     ![ for( j=range(x) )
      if( x[j]!= s[i+j] ) 1]
     : s[i]==x
); 
      
    isat=["isat","s,i,x","T|F","Inspect",
    "Check if x is at index i of str s.
    ;; 
    ;; Compare to index() or idx():
    ;; 
    ;; s='abc+def+def'
    ;; 
    ;; idx(s,'def')=4
    ;; isat( s,4,'def')=true
    ;; isat( s,8,'def')=true : idx can`t reach this
    ;; 
    ;; x could be array:
    ;; isat('ab+cd+cd',4,['b+','d+'])= 'd+'
    "
    ,"idx"
    ];

    //==================================================
    function isat_test( mode=MODE, opt=[] )=
    (
        doctest( isat,
        [
          [ isat("ab+cd+cd", 3, "cd"),true, "'ab+cd+cd',3,'cd'"]
        , [ isat("ab+cd+cd", 6, "cd"),true, "'ab+cd+cd',6,'cd'"]
        , [ isat("ab+cd+cd", 1, "cd"),false, "'ab+cd+cd',1,'cd'"]
        , [ isat("ab+cd+cd", 1, "b+"),true, "'ab+cd+cd',1,'b+'"]
        , [ isat("ab+cd+cd", 1, ["b+","d+"]),"b+", "'ab+cd+cd',1,['b+','d+']"]
        , [ isat("ab+cd+cd", 4, ["b+","d+"]),"d+", "'ab+cd+cd',4,['b+','d+']"]
        , [ isat("ab+cd+cd", 4, ["b","d"]),"d", "'ab+cd+cd',4,['b','d']"]
        , [ isat( "a+cos(2)+sin(5)", 2, ["cos","sin"])
          , "cos"
          , "'a+cos(2)+sin(5)', 2, ['cos','sin']"
          ] 
        ]
        ,mode=mode, opt=opt
        )
    );
    //echo( isat_test()[1] );      
} // isat 

//========================================================
{ // isball
function isball(x)=
(
    len(x)==2 && (  (ispt(x[0]) && isnum(x[1]))
                 || (ispt(x[1]) && isnum(x[0]))
                 )
);
  
    isball=["isball","x","T|F","Inspect",
    "Check if x is a representation for a ball (sphere).
    ;; 
    ;; x is a 2-item array, either [P,r] or [r,P]
    ;; 
    "
    ];
    
    function isball_test( mode=MODE, opt=[] )=
    (
        let( P = randPt() )
           
        doctest( isball,
        [
          str("P= ", _fmt(P))
          ,[ isball([P,2]), true, "[P,2]"] 
          ,[ isball([2,P]), true, "[2,P]"] 
          ,[ isball([P,P]), false, "[P,P]"] 
           
        ]
        ,mode=mode, opt=opt
        )
    );
    
} // isball
      
//========================================================
{ // isbool
function isbool(x)= x==true || x==false; 

    isbool=[ "isbool","x", "T|F", "Type, Inspect",
    "
     Given x, return true if x is a boolean (true|false).
    "];

    function isbool_test( mode=MODE, opt=[] )=(
        doctest( isbool, 
        [ [ isbool([1,2,3]), false, [1,2,3]]
        , [ isbool("a"), false, "'a'"]
        , [ isbool(true), true, "true"]
        , [ isbool(false), true, "false"]
        , [ isbool(undef), false, "undef"]
        ],mode=mode, opt=opt
        )
    );
 } // isbool
 
//========================================================
 { // iscir
function iscir(x,dim=3)= // For 3d cir, has to be [ln,r] or [r,ln]
(                       
    len(x)==2 &&
    ( 
      dim==3? (  (isline(x[0]) && isnum(x[1]))
              || (isline(x[1]) && isnum(x[0]))
              )
     :dim==2? (  (ispt(x[0]) && isnum(x[1]))
              || (ispt(x[1]) && isnum(x[0]))
              ) 
     :false
   )
); 
 } // iscir
 
     iscir=["iscir","x,dim=3","T|F","Inspect",
    "**** { under construction } *****
    ;; Check if x is a representation for a circle.
    ;; 
    ;; x is a 2-item array, either [Line,r] or [r,Line]
    ;; 
    "
    ];
    
    function iscir_test( mode=MODE, opt=[] )=
    (
        let( P = randPt() )
           
        doctest( iscir,
        [
          str("P= ", _fmt(P))
                     
        ]
        ,mode=mode, opt=opt
        )
    );
 
//========================================================
 { // iscoline
function iscoline(a,b,dim=3, _rtn=true, _i=1, _debug=[])= // iscoline( [P,Q],R )
                      //       ( [P,Q,R ... ] )
                      //       ( P, [Q,R ...])
                      //       ( [P,Q], [R,S ...])
(

   _i==1?  // init 
   ( let( pts = concat( ispt(a,dim)?[a]:a?a:[]    // Combine a and b to be pts
                      , ispt(b,dim)?[b]:b?b:[]) )
     len(pts)<=2? true
     : iscoline( pts,b,dim, _rtn=true, _i=2)  // Set a=pts hereafter (for _i>1)
   )
   : _rtn==false? false
   : _i==len(a)? _rtn //replace(join(_debug),"\n","<br/>") //_rtn
   : let( cross = cross( a[_i]-a[0], a[_i]-a[1])
        )
      iscoline( a,b, dim
             , _rtn= _rtn&& (  is0(cross)         // cross() has to be 0 (2d)
                            || is0(norm(cross)))  // or [0,0,0] (3d)
             , _i=_i+1 
//             , _debug= concat(_debug
//                       , ["\n_i=", _i
//                         ,"\nlen(a)=", len(a) 
//                         ,"\ncross( a[_i]-a[0], a[_i]-a[1])= ", cross
//                       ])
             )  
);    
        
    iscoline=[ "iscoline","a,b", "T|F", "Geom, Inspect",
    "Chk if pts in a and pts in b are all on the same line.
    ;; 
    ;; iscoline( [P,Q],R )
    ;;       ( [P,Q,R ... ] )
    ;;       ( P, [Q,R ...])
    ;;       ( [P,Q], [R,S ...])
    " 
    ];

    function iscoline_test( mode=MODE, opt=[] )=(//====================

        let( pts = onlinePts( randPts(2), lens= range(4)) //0,0.2,0.8))
           )
        doctest( iscoline, 
        [ 
         str( ">> pts= <br/>", arrprn( pts, 2))
        , [ iscoline(pts), true, "pts"]
        
        , [ iscoline(slice(pts,2),slice(pts,0,2)), true
            , "slice(pts,2),slice(pts,0,2)"]
        
        , [ iscoline( pts[0], slice(pts,1)), true
            , "pts[0], slice(pts,1)"] 

        , [ iscoline( slice(pts,0,-1), last(pts)), true
            , "slice(pts,0,-1), last(pts)"] 

        , [ iscoline( pts, randPt()), false
            , "pts, randPt()"] 

        , [ iscoline( [[2,3,4],[5,6,7],[3,4,5]]), true, [[2,3,4],[5,6,7],[3,4,5]]]
        , [ iscoline( [[2,3],[6,9],[1,1.5]]), true, [[2,3],[6,9],[1,1.5]] ]
        , [ iscoline( [[2,3],[6,9],[1,1.5]], dim=2), true, "[[2,3],[6,9],[1,1.5]],dim=2"]
        

        ],mode=mode, opt=opt, scope=["pts",pts]
        )
    );

    //echo( iscoline_test( mode=112 )[1] );
} // iscoline

function isConvex_dev(pts, pi, Rf)=
(
   // Check if pts[pi] in pts is a convex pt in reference to a pt Rf
   /*
          |   Rf       |   Rf
          |            |   
          .P          Q.   
        .'              '.
    
     P is convex, Q is not  
     2018.5.11   
     
                 E
                /     
     D-._      /  _. F 
         '-._ /_-' 
             P'             Rf
             
                 E
                /     
     D-._      /  _. F 
         '-._ /_-'   |
             P'------Q-----Rf
                     |  d  |  
             
     --- N = N( [Rf,P,pt] )
     --- pl = [N,Q,Rf]        
     --- Get the pt that has smallest d = dist(Q,Rf) where Q is proj of pt
         onto NPRf. In this case, pt F         
     --- Check if ALL pts (except P and F) are on same side of plane NPF 
         AND ANY pt is on different sides with Rf
         
     --- if Rf not given, extend the center of all pts to P then double the length
         to get one    
   */
   echo("isConvex():")
   let( rf = Rf==undef? onlinePt( [sum(pts)/len(pts),pts[pi]], ratio=10)
                       : Rf
                       
      , dis = [ for(pj=range(pts))
               if(pj!=pi) 
               let( N = N([ rf, pts[pi], pts[pj]] )
                  , J = projPt( [rf,pts[pi],N], pts[pj] )
                  )
               [dist(J,Rf),pj]         
                //[angle( [rf,pts[pi], pts[i] ]), i ]
              ]
      , sortdis = sortArrs( dis,0 )        
      , min_ai = sortArrs( dis,0 )[0][1]  // i of pt that has smallest angle 
      , 2nd_di = sortArrs( dis,0 )[1][1]
      , 3rd_di = sortArrs( dis,0 )[2][1]
       
      , plane = [ pts[ sortdis[0][1] ] 
                //, pts[ pi ]
                , pts[ sortdis[1][1] ] 
                , pts[ sortdis[2][1] ] 
                //, N( [rf, pts[pi], pts[ min_ai ] ])
                ]
      , pts_wo_plane = [ for( i=[0:len(pts)-1] )                   
                                 if( i!=min_ai && i!=pi && i!=2nd_di && i!=3rd_di ) pts[i]
                               ]           
      , isPtsSameSide= isSameSide( pts_wo_plane, plane )
      , isRfSameSide = isSameSide( [pts_wo_plane[0], rf], plane)                       
      , rtn = isPtsSameSide && !isRfSameSide
      )
   //echo(pts = pts)
   echo( pi=pi, min_ai=min_ai, 2nd_di = 2nd_di )
   echo( sortdis=sortdis )
   //echo( ais = ais )
   //echo( quicksort_ais = sortArrs( ais ) )
   //echo( min_angle = sortArrs(ais)[0][0] )
   //echo( Rf=Rf, rf=rf )
   echo( ref_plane = [ pi, min_ai, "N"] )
   //echo( plane = plane )
   //echo( pts_wo_plane = pts_wo_plane )
   //echo( isPtsSameSide = isPtsSameSide )
   //echo( isRfSameSide = isRfSameSide )
   //echo( rtn = rtn )  
   rtn    
);

function isConvex(pts, pi, Rf)=   
(

   /* Works in most cases but not all. Failed case:
   
   pts= [[-0.315754, 2.18321, 0.0165775], [-1.83316, -0.461837, -2.4851], [-0.250761, -0.707988, 1.40556], [-2.71441, 2.61027, 1.81194], [-1.35138, -2.97306, 2.55987], [-2.85625, 2.42117, -2.24881], [-1.8127, 2.11271, -0.169752], [-0.43634, 2.67442, -1.86039]];
      pts=[[0.954102, -2.52069, 2.86141], [-1.58446, 0.731409, 0.831557]
      , [1.14213, -0.814758, 0.481223], [0.326093, 2.92951, -2.28178]
      , [-0.737669, 1.28368, 1.14409], [0.0167644, 1.17775, 1.47647]
      , [1.06618, 2.12602, -0.0956963], [1.09691, 2.20065, -2.15693]
      , [-2.28315, 0.470324, 2.14885], [-0.370817, 1.36549, 0.924967]];
      
   */   
   // Check if pts[pi] in pts is a convex pt in reference to a pt Rf
   /*
          |   Rf       |   Rf
          |            |   
          .P          Q.   
        .'              '.
    
     P is convex, Q is not  
     2018.5.11   
     
                 E
                /     
     D-._      /  _. F 
         '-._ /_-' 
             P'         Rf
             
     --- Get the pt that has smallest angle [Rf,P,pt]. In this case, pt F         
     --- Get the normal pt of [Rf,P,F] = N to create a plane [N,P,F]
     --- Check if ALL pts (except P and F) are on same side of NPF 
         AND ANY pt is on different sides with Rf
         
     --- if Rf not given, extend the center of all pts to P then double the length
         to get one    
   */
   //echo("isConvex():")
   let( rf = Rf==undef? onlinePt( [sum(pts)/len(pts),pts[pi]], ratio=10)
                       : Rf
      , ais = [ for(i=range(pts))
                if(i!=pi) [angle( [rf,pts[pi], pts[i] ]), i ]
              ]
              
      , min_ai = sortArrs( ais,0 )[0][1]  // i of pt that has smallest angle 
      
      , plane = [ pts[ min_ai] 
                , pts[ pi ] 
                , N( [rf, pts[pi], pts[ min_ai ] ])
                ]
      , pts_wo_plane = [ for( i=[0:len(pts)-1] )                   
                                 if( i!=min_ai && i!=pi ) pts[i]
                               ]           
      , isPtsSameSide= isSameSide( pts_wo_plane, plane )
      , isRfSameSide = isSameSide( [pts_wo_plane[0], rf], plane)                       
      , rtn = isPtsSameSide && !isRfSameSide
      )
   //echo(pts = pts)
   echo( pi=pi, min_ai=min_ai )
   //echo( ais = ais )
   //echo( quicksort_ais = sortArrs( ais ) )
   //echo( min_angle = sortArrs(ais)[0][0] )
   //echo( Rf=Rf, rf=rf )
   echo( ref_plane = [ pi, min_ai, "N"] )
   //echo( plane = plane )
   //echo( pts_wo_plane = pts_wo_plane )
   //echo( isPtsSameSide = isPtsSameSide )
   //echo( isRfSameSide = isRfSameSide )
   //echo( rtn = rtn )  
   rtn    
);



function isConvexPlane(pts, precision=5)=
(
  all( [for(a= angle(get3(pts)))
       abs(a-180)<=pow(1,-precision) ] 
       )
);

//========================================================
{ // isequal
    
function isequal(a,b,prec)=
(
  let(ta = type(a)
     ,tb = type(b)
     ,isnum= (ta=="int"||ta=="float")&&(tb=="int"||tb=="float")
     )
  isnum? 
    (abs(a-b)< (prec==undef?ZERO:pow(10,-prec)) )
  : ta!=tb? false
  : ta=="arr" ?
    (
      len(a)!=len(b)? false
      : len([ for(i=[0:len(a)-1]) if(!isequal(a[i],b[i],prec))1])==0
    )
  :a==b  
//  : ispt(a)&&ispt(b)
//    //? ( abs(a.x-b.x)<ZERO && abs(a.y-b.y)<ZERO && abs(a.z-b.z)<ZERO )
//  ? (   isequal(a[0],b[0],prec) 
//       && isequal(a[1],b[1],prec) 
//       && isequal(a[2],b[2],prec) )
//    : isarr(a)&&isarr(b)
//    ? len(a)==len(b) 
//        && all([for(i=len(a)) isequal( a[i],b[i], prec) ] )
//    : a==b
);
        
    isequal=["isequal", "a,b", "T|F", "Inspect",
    str(" Given 2 numbers a and b, return true if they are determined to
        ;; be the same value. This is needed because sometimes a calculation
        ;; that should return 0 ends up with something like 0.0000000000102.
        ;; So it fails to compare with 0.0000000000104 (another shoulda-been
        ;; 0) correctly. This function checks if a-b is smaller than the
        ;; predifined ZERO constant ("
        , ZERO
        ,") and return true if it is.
        ")
    ];

    function isequal_test( mode=MODE, opt=[] )=(//====================
        doctest( isequal, 
        [ str("// ZERO = ",ZERO)
        ,  [ isequal(1e-12,0),true, "1e-12,0"]
        , [ isequal(1e-10,0),true, "1e-10,0"]
        , [ isequal(1e-6,0),true, "1e-6,0"]
        , [ isequal(1e-4,0),false, "1e-4,0"]
        , [ isequal(1e-2,0),false, "1e-2,0"]
        , [ isequal(1e-9,1e-10),true, "1e-9,1e-10"]
        , [ isequal(1e-12,1e-13),true, "1e-12,1e-13"]
        , [ isequal(1e-14,1e-13),true, "1e-14,1e-13"]
        , [ isequal("1e-15",0),false, "'1e-15',0"]
        , [ isequal("1e-15","0"),false, "'1e-15','0'"]
        , [ isequal("abc","abc"),true, "'abc','abc'"]
        , [ isequal(undef,undef),true, "undef,undef"]
        , [ isequal(undef,false),false, "undef,false"]
        , [ isequal(["a",1e-14],["a",1e-13]),true, "[\"a\",1e-14],[\"a\",1e-13]"]
        , [ isequal(["a",1e-14],["a",0.1]),false, "[\"a\",1e-14],[\"a\",0.1]"]
        , [ isequal(["a","b"],["a","a"]),false, "[\"a\",\"a\"],[\"a\",\"b\"]"]
    
        ],mode=mode, opt=opt
        )
    );
} // isequal
        
//========================================================
//function iserr(assert_rtn)= begwith(assert_rtn,"ERROR");

//=========================================
{ // iscoord
function iscoord(pts)=
   len(pts)==4 && is90( get(pts,[1,0]))
               && is90( get(pts,[1,2]))
               && is90( get(pts,[1,3]));
               
    iscoord=["iscoord","pts","T/F", "Geometry",
    " Return true if pts is a 4-pointer representing a coordinate system,
    ;; in which pts[1] is the origin."
    ];

    function iscoord_test( mode=MODE, opt=[] )=
    (
       doctest( iscoord,
       [
       ]
       , mode=mode, opt=opt
       )    
    );   

} // iscoord

//========================================================
{ // isfloat 
function isfloat(x)= type(x)=="float";

    isfloat=[ "isfloat","x", "T|F","Type, Inspect, Number",
    "
     Check if x is a float.
    "];
    function isfloat_test( mode=MODE, opt=[] )=(//====================
        doctest(isfloat, 
        [ [ isfloat(3), false, "3"]
        , [ isfloat(3.2), true, "3.2"]
        , [ isfloat("a"), false, "'a'"]
        ],mode=mode, opt=opt
        )
    );
 } // isfloat

//========================================================
{ // isnums
    
function isfloats(x)= isarr(x)&& all([for(y=x)isfloat(y)]);

    isfloats =[ "isfloats","x", "T|F", "Type,Inspect,Number",
    "
     Check if x is an array of isfloats.
    "];
    function isfloats_test( mode=MODE, opt=[] )=(//=========================
        doctest(isfloats, 
        [ 
          [ isfloats(3), false, "3"]
        , [ isfloats(3.2), false, "3.2"]
        , [ isfloats("a"), false, "'a'"]
        , [ isfloats([1,2,0.3]), false, "[1,2,0.3]"]
        , [ isfloats([1,2,"a"]), false, "[1,2,'a']"]
        , [ isfloats([0.5,1.3,2.4]), true, "[0.5,1.3,2.4]"]
        ]
        ,mode=mode, opt=opt
        )
    );
 } // isfloats  
 

//========================================================
{ // ishash
   
function ishash(x)=  (type(x)=="arr") 
                     && (len(x)%2==0) //( len(x)/2 == floor(len(x)/2))
                     && all( [for(i=[0:2:len(x)-1]) isstr(x[i]) ]);

    ishash = [ "ishash","x", "T|F", "hash,type, Inspect",
    "
     Check if x **could** be a hash, i.e., an array with even number length
    ;; and all even-numbered indices point to strings.
    "]; 
    function ishash_test( mode=MODE, opt=[] )=(//====================
        doctest(ishash, 
        [ [ ishash([1,2,3]), false, "[1,2,3]"]
        , [ ishash([1,2,3,4]), false, "[1,2,3,4]"]
        , [ ishash(["1",2,"3",4]), true, "['1',2,'3',4]"]
        , [ ishash(3), false, "3"]
        , [ ishash(3.2), false, "3.2"]
        , [ ishash("a"), false, "'a'"]
        ],mode=mode, opt=opt
        )
    );

 } // ishash
 
 
function isin(x,o) = hash(o,x); // just a reversed way of has(). 2018.7.7
 
function isInConvexPlane(pts, pt, precision=5)=  // 2018.5.8
(
   //let( sa = sum([for(p2=get2(pts)) angle( [p2[0],pt,p2[1]] )]) )
   //echo(sa = _color(sa, sa>=360?"green":"red"))
   //sa >= 360
   abs(sum([for(p2=get2(pts)) angle( [p2[0],pt,p2[1]] )]) -360)
   <= pow(1,-precision) 
   
   //sum([for(p2=get2(pts)) angle( [p2[0],pt,p2[1]] )]) >= 360
);
 
 
//========================================================
{ // isint
function isint(x)=   type(x)=="int";

    isint = [ "isint","x", "T|F", "Type,Inspect, Number",
    "
     Check if x is an integer.
    "];
    function isint_test( mode=MODE, opt=[] )=(//====================
        doctest(isint, 
        [ [ isint(3), true, "3"]
        , [ isint(3.2), false, "3.2"]
        , [ isint("a"), false, "'a'"]
        ],mode=mode, opt=opt
        )
    );
 } // isint     
//========================================================
{ // isinline
    
function isinline( pt,ln, closed=false )= 
(  
  isonline(pt,ln)?
  (
    let( gt = str(gtype(pt),gtype(ln))
       , R = gt=="ptln"?pt:ln
       , pq= gt=="ptln"?ln:pt
       , rtn = norm(pq[0]-R)+norm(pq[1]-R) == norm(pq[0]-pq[1])
      )
    closed? rtn
    : (rtn && !isSamePt(R, pq[0]) && !isSamePt(R,pq[1]))
  ): false      
);
   
    isinline=["isinline", "(pt,ln|ln,pt),closed=false", "T|F","Inspect",
    "Check if pt is in line segment ln. If closed, both line endpts
    ;; are included, means pt could be one of ln[0] or ln[1]
    "];

    function isinline_test( mode=MODE, opt=[] )=
    (//=========================
        let( P=[1,1,1], Q=[2,2,2], R=[3,3,3], S=[1,2,3] )
        doctest(isinline, 
        [ 
          [ isinline( P, [Q,R] ), false, "P, [Q,R] " ]
        , [ isinline( Q, [Q,R] ), false, "Q, [Q,R] " ]
        , [ isinline( Q, [Q,R], closed=true ), true, "Q, [Q,R] " ]
        , [ isinline( [Q,R], P ), false,"[Q,R], P" ]
        , [ isinline( [R,P], Q ), true, "[R,P], Q" ]
        , [ isinline( Q,[R,P]), true,"Q,[R,P]" ]
        , [ isinline( S,[R,P]), false,"S,[R,P]" ]
        ]
        ,mode=mode, opt=opt, scope=["P",P,"Q",Q,"R",R,"S",S] 
        )
    );  

} // isinline

//================================================================
{ // isline
    
function isline(x,dim=3)=
( 
    //echo("In isline", x=x,    len(x)==2 , ispts(x,dim), x[0]!=x[1]
    //    )
    len(x)==2 && ispts(x,dim) && x[0]!=x[1]
);
    
    isline=["isline","x,dim=3","T|F","Geo, Inspect"
    ,"Return true if x is a line. Default is3d=true"
    ];
     
    function isline_test( mode=MODE, opt=[] )=(//=================
        let( P=[2,3,4]
           , Q=[5,6,7]
           , R=[3,4,5]
           )
        doctest( isline,
        [ 
          str("P= ",str(P))
        , str("Q= ",str(Q))
        , str("R= ",str(R))
        , [isline( P ), false, "P" ]
        , [isline( [P,Q] ), true, "[P,Q]" ]
        , [isline( [P,["a",2,3]] ), false, "[P,['a',2,3]]" ]
        , [isline( [P,P] ), false,  "[P,P]" ]
        , [isline( [P,Q,R] ), false, "[P,Q,R]" ]
        , [isline( [[2,3],[5,6]] ), false, [[2,3],[5,6]]]
        , [isline( [[2,3],[5,6]],2 ), true, "[[2,3],[5,6]],dim=2"]
        ]
        , mode=mode, opt=opt , scope=["P",P,"Q",Q,"R",R]
        )
    );
} // isline

//========================================================
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#Infinities_and_NaNs
//0/0: nan	sin(1/0): nan	asin(1/0): nan	ln(1/0): inf	round(1/0): inf
//-0/0: nan	cos(1/0): nan	acos(1/0): nan	ln(-1/0): nan	round(-1/0): -inf
//0/-0: nan	tan(1/0): nan	atan(1/0): 90	log(1/0): inf	sign(1/0): 1
//1/0: inf	ceil(-1/0): -inf	atan(-1/0): -90	log(-1/0): nan	sign(-1/0): -1
//1/-0: -inf	ceil(1/0): inf	atan2(1/0, -1/0): 135	max(-1/0, 1/0): inf	sqrt(1/0): inf
//-1/0: -inf	floor(-1/0): -inf	exp(1/0): inf	min(-1/0, 1/0): -inf	sqrt(-1/0): nan
//-1/-0: inf	floor(1/0): inf	exp(-1/0): 0	pow(2, 1/0): inf	pow(2, -1/0): 0
// "nan" is the only value in OpenSCAD not equal to itself
function isnan(x)= x!=x;

    isnan =[ "isnan","x", "T|F", "Type,Inspect,Number",
    "
     Check if x is a NaN (not a number). For example, 1/0.
    "];
    function isnan_test( mode=MODE, opt=[] )=(//=========================
        doctest(isnan, 
        [ 
          [ isnan(3), false, "3"]
        , [ isnan("3"), false, "\"3\""]
        , [ isnan(0/0), true, "0/0"]
        , [ isnan(undef), false, "undef"]
        ]
        ,mode=mode, opt=opt
        )
    );    

//========================================================
{ // isnum
    
function isnum(x)=   type(x)=="int" || type(x)=="float";

    isnum =[ "isnum","x", "T|F", "Type,Inspect,Number",
    "
     Check if x is a number.
    "];
    function isnum_test( mode=MODE, opt=[] )=(//=========================
        doctest(isnum, 
        [ 
          [ isnum(3), true, "3"]
        , [ isnum(3.2), true, "3.2"]
        , [ isnum("a"), false, "'a'"]
        , [ isnum("-"), false, "'-'"]
        ]
        ,mode=mode, opt=opt
        )
    );
 } // isnum  
    
//========================================================
{ // isnums
    
function isnums(x)= isarr(x)&& all([for(y=x)isnum(y)]);

    isnums =[ "isnums","x", "T|F", "Type,Inspect,Number",
    "
     Check if x is an array of numbers.
    "];
    function isnums_test( mode=MODE, opt=[] )=(//=========================
        doctest(isnums, 
        [ 
          [ isnums(3), false, "3"]
        , [ isnums(3.2), false, "3.2"]
        , [ isnums("a"), false, "'a'"]
        , [ isnums([1,2,0.3]), true, "[1,2,0.3]"]
        , [ isnums([1,2,"a"]), false, "[1,2,'a']"]
        ]
        ,mode=mode, opt=opt
        )
    );
 } // isnum     
//========================================================
{ // isonline 
function isonline( pt,ln )= 
(
   // If online, the cross() will be [0,0,0]
   let( ptln= ispt(pt)?[pt,ln]:[ln,pt]
      , _pt = ptln[0]
      , _ln = ptln[1])
   //echo(ptln=ptln, pt-ln[0], pt-ln[1], cross( pt-ln[0], pt-ln[1]))
   _pt==_ln[0]||_pt==_ln[1]?true
   :is0(norm( cross( _pt-_ln[0], _pt-_ln[1]) ) )
   
   // An alternative: 2018.11.5 ===> doesn't seem to work. Why ?
   // let ln = [P,Q], pt= aP+bQ
   // If a+b=1, then pt is on PQ line
   // use: solve2(ab,d)
   //echo( pt=pt, ln=ln )
   //ispt(pt)? solve3(concat(ln,[O]),pt)
   //: solve3(pt,concat(ln,[O]))
    
);  
//(  let( gt = str(gtype(pt),gtype(ln))
//      , X = gt=="ptln"?pt:ln
//      //, pq= gt=="ptln"?ln:pt
//      )
//   isequal( X, projPt(X, gt=="ptln"?ln:pt ))
//);

    isonline=["isonline", "(pt,ln|ln,pt)", "T|F","Inspect",
    "Check if pt is on the line ln"
    ];
    function isonline_test( mode=MODE, opt=[] )=
    (//=========================
        let( P=[1,1,1], Q=[2,2,2], R=[3,3,3], S=[1,2,3] )
        doctest(isonline, 
        [ "var P", "var Q", "var R", "var S"
        , [ isonline( P, [Q,R] ), true, "P, [Q,R] " ]
        , [ isonline( [Q,R], P ), true,"[Q,R], P" ]
        , [ isonline( [R,P], Q ), true, "[R,P], Q" ]
        , [ isonline( Q,[R,P]), true,"Q,[R,P]" ]
        , [ isonline( S,[R,P]), false,"S,[R,P]" ]
        ,""
        , [isonline(O,[[-3.66, -2.12, -0.99], [-14.64, -8.48, -3.96]]), true,
              [O,[[-3.66, -2.12, -0.99], [-14.64, -8.48, -3.96]]] 
          ]
        , [isonline(O,[[0.55, -0.54, 1.01], [0.55, -0.54, 1.01]]),true,[O,[[0.55, -0.54, 1.01], [0.55, -0.54, 1.01]]]]      
        , [isonline(O,[[-1.31, -2.41, 2.61], [3.93, 7.23, -7.83]]),true, [O,[[-1.31, -2.41, 2.61], [3.93, 7.23, -7.83]]]]
        ]
        ,mode=mode, opt=opt, scope=["P",P,"Q",Q,"R",R,"S",S] 
        )
    );  
//P=[1,1,1]; Q=[2,2,2]; R=[3,3,3]; S=[1,2,3];
//echo( isonline( P, [Q,R] ) );
//echo( isonline( [Q,R], P ) );
//echo( isonline( [R,P], Q ) );
//echo( isonline( Q,[R,P]));
//echo( isonline( S,[R,P]));
} // isonline

//========================================================
{ // isOnPlane
function isOnPlane( pqr, pt )=
 let( pco = isarr(pqr[0])?planecoefs(pqr):pqr  
	, got =  pco[0]*pt.x
	   + pco[1]*pt.y
	   + pco[2]*pt.z
	   + (len(pco)==3?0:pco[3])
	)
(
	is0(got)
);

    isOnPlane=["isOnPlane", "pqr,pt", "T|F", "Inspect, Plane",
    "
     Given a 3-pointer (pqr) and a point (pt) ( to be converted to planecoefs,
    ;; = [a,b,c,k] or [a,b,c])), check if the pt is on
    ;; the plane:
    ;;
    ;;   a*pt.x + b*pt.y + c*pt.z + k == ZERO
    "];

    function isOnPlane_test( mode=MODE, opt=[] )=( //======================
        
        let( pqr    = randPts(3)
           , pcoefs = planecoefs( pqr )
           , othopt = othoPt( pqr )
           , abipt  = angleBisectPt( pqr )
           , midpt  = midPt( p01(pqr) )
           )
        doctest( isOnPlane,
        [ 
          "// Randomly create pqr, and get its planecoefs, pcoefs."
        , "var pqr", "var pcoefs"
        , ""
        , "// From pqr, create othoPt, angleBisectPt and midPt of p,q."
        , "// They should all be on the same plane."
        , "var othopt", "var abipt", "var midpt"
        , [ isOnPlane( pqr,P(pqr) ), true, "pqr,P(pqr)" ]
        , [ isOnPlane( pqr, othopt ), true,  "pqr, othopt" ]
        , [ isOnPlane( newy(pqr,othopt),  othopt  ), true, "newy(pqr,othopt), othopt" ]
        , [ isOnPlane( pcoefs,othopt),  true, "pcoefs, othopt" ]
        , ""
        , "// angleBisectPt: "
        , [ isOnPlane( newy( pqr,abipt), abipt ),true, "newy(pqr,abipt), abipt" ]
        , [ isOnPlane( pcoefs, abipt ),  true, "pcoefs, abipt" ]
        , ""
        , "// Mixed:"
        , [ isOnPlane( [othopt, abipt, midpt] , pqr[2] ), true, 
           "[othopt, abipt, midpt], pqr[2]" ]
        , ""
        , "// normalPt:"
        , [ isOnPlane( pqr, N(pqr)), false, "pqr,N(pqr)"]
        
        ], mode=mode, opt= opt //update(opt,["showscope",true])
           , scope= ["pqr", pqr
                    , "pcoefs", pcoefs
                    , "othopt", othopt
                    , "abipt", abipt
                    , "midpt", midpt]
        )
    );
    //echo( isOnPlane_test()[1] );
} // isOnPlane


function isOnPQR(pqr,pt)=
(
   let( _pqr_pt= ispt(pt)?[pqr,pt]:[pt,pqr]
      , ijk = tocoord( _pqr_pt[1], _pqr_pt[0]  )
      )
   //echo( sum = sum(ijk) )   
   isequal(sum(ijk),1)
);
 

function isInPQR( pqr, pt
                , includes=["pq","qr","pr"] //"pq" : pt on PQ line => true
                )=
(
   let( _pqr_pt= ispt(pt)?[pqr,pt]:[pt,pqr]
      , ijk = tocoord( _pqr_pt[1], _pqr_pt[0]  )
      , i= ijk[0]
      , j= ijk[1]
      , k= ijk[2]
      , qr= has(includes, "qr")
      , pr= has(includes, "pr")
      , pq= has(includes, "pq")
      )
   isequal(i+j+k,1) &&
   (
      ( i>0 && j>0 && k>0 )||
      (( i==0 && j>0 && k>0 ) && qr) ||   
      (( i>0 && j==0 && k>0 ) && pr) ||   
      (( i>0 && j>0 && k==0 ) && pq) 
   )     
);


//================================================================
{ // isparal
// check gtype( [[-6.66, -0.15, -3.82], [-6.66, -0.15, -3.82]])
// [[-5.64, 0.01, -2.04], [-5.64, 0.01, -2.04]]
//  [[-4.38, 0.16, -4.72], [-4.38, 0.16, -4.72]]
//echo(gt=gtype( [[-6.66, -0.15, -3.82], [-6.66, -0.15, -3.82]]));
//echo(gt=gtype( [[-5.64, 0.01, -2.04], [-5.64, 0.01, -2.04]]));
//echo(gt=gtype( [[-4.38, 0.16, -4.72], [-4.38, 0.16, -4.72]]));
//echo(isline=isline( [[-6.66, -0.15, -3.82], [-6.66, -0.15, -3.82]]));
//echo(isline=isline( [[-5.64, 0.01, -2.04], [-5.64, 0.01, -2.04]]));
//echo(isline=isline( [[-4.38, 0.16, -4.72], [-4.38, 0.16, -4.72]]));

function isparal(a,b)=
(
   let( c= b==undef?a[0]:a
       , d= b==undef?a[1]:b
       , t= str(gtype(c),gtype(d))  // lnln, lnpl, plln, plpl 
       )
   //echo("In isparal,", a=a,b=b,c=c,d=d,gtype_d=gtype(d), t=t)    
    t=="lnln"? isonline(O, [c[0]-c[1], d[0]-d[1]])//uv(c)==uv(d)
   :t=="lnpl"? is90( c, [ d[1], N(d) ] ) 
   :t=="plln"? is90( d, [ c[1], N(c) ] )
   :t=="plpl"? uv( [c[1],N(c)] )==uv( [d[1],N(d)] )
   :undef
);

    isparal=["isparal", "a,b", "T|F", "Geo",
    "Check if a and b are parallel. a, b could be line or plane. 
    ;;
    ;; They can be entered in either isparal(a,b) or 
    ;;  isparal( [a,b] ) form. For example:
    ;;
    ;; L  = [ [P,Q], [R,S] ]; 
    ;; PL = [ [P0,Q0,R0], [P1,Q1,R1], [P2,Q2,R2] ]; 
    ;; 
    ;; isparal( L, PL )  // or (PL,L)
    ;; isparal( [PL,L] ) // or ( [L,PL] )
    ;;
    ;; Note that it returns undef for [[A,B],[C,C]]
    ;;  'cos [C,C] is not a valid line. 
    ;;  In these case, you can check isonline(C, [A,B])
    ;; 
    "];

    function isparal_test( mode=MODE, opt=[] )=
    (
        doctest( isparal, 
        [
        ]
        , mode=mode, opt=opt
        )
    );
} // isparal

//================================================================
{ // ispl 
function ispl(x,dim=3)=
(
    dim==2? !iscoline(x)
    : len(x)==3 
    && ispts(x, dim) 
    && !isequal(x[0],x[1])
    && !isequal(x[0],x[2])
    && !isequal(x[2],x[1])
    && !iscoline(x)

//    len(x)==3 && ispts(x, dim) &&
//    (x[0]!=x[1]) && (x[0]!=x[2]) && (x[1]!=x[2]) 
//    && !iscoline(x)
  
);
    
    ispl=["ispl","x,dim=3","T|F","Geo, Inspect"
    ,"Return true if x is a plane. Default is3d=true"
    ];
    function ispl_test( mode=MODE, opt=[] )=(//=================

        let( P=[2,3,4]
           , Q=[5,6,7]
           , R=[3,4,5]
           )
        doctest( ispl,
        [ 
          [ ispl( P ), false, "P" ]
        , [ ispl( [P,Q] ), false, "[P,Q]" ]
        , [ ispl( [P,Q,R] ), false, "[P,Q,R]"]
        , [ ispl( [P,P,R] ), false, "[P,P,R]"]
        , [ ispl( [P,Q,P] ), false, "[P,Q,P]"]
        , [ ispl( [P,Q,Q] ), false, "[P,Q,Q]"]
        , [ ispl( [[2,3],[5,6],[7,10]] ), false, [[2,3],[5,6],[7,8]]]
        , [ ispl( [[2,3],[5,6],[7,10]],2 ), true, "[[2,3],[5,6],[7,10]],dim=2"]
        
        ]
        , mode=mode, opt=opt , scope=["P",P,"Q",Q,"R",R]
        )
    );
} // ispl
//====================
{ // ispt
function ispt(x,dim=3)= 
(   len(x)==dim&& countNum(x)==dim
);

    ispt = ["ispt","x,dim=3","T|F","Geo, Inspect"
    ,"Return true if x is a point. Default is3d=true"
    ];

    function ispt_test( mode=MODE, opt=[] )=( //===================

        doctest( ispt,
        [ 
          [ ispt([2,3,4]), true, [2,3,4] ]
        , [ ispt([2,3,4,5]), false, [2,3,4,5] ]
        , [ ispt([2,3,"4"]), false, [2,3,"4"] ]
        , [ ispt([2,3]), false, [2,3] ]
        , [ ispt([2,3],2), true, "[2,3],dim=2" ]
        , [ ispt( 234), false, 234]
        , [ ispt( "234"), false, "'234'"]
        ],mode=mode, opt=opt
        )
    );
} // ispt
//====================
{ // ispts
function ispts(o,dim=3)= 
(   
   o && (len([for(x=o) if(!ispt(x,dim))1])==0 )
       
);
    ispts=["ispts", "o,dim=3", "T|F","Inspect",
    "Check if o is an array of points.
    "];

    function ispts_test( mode=MODE, opt=[] )=
    (//=========================
        let( P=[1,1,1], Q=[2,2,2], R=[3,3,3], S=[1,2,3] )
        doctest(ispts, 
        [ 
          [ ispts( [P,Q,R] ), true, "[P,Q,R]" ]
        , [ ispts( [1,2,3] ), false, "[1,2,3]" ]
        , [ ispts( [[1,2],[3,4]] ), false, "[[1,2],[3,4]]" ]
        , [ ispts( [[1,2],[3,4]], dim=2 ), true, "[[1,2],[3,4]],dim=2" ]
        ]
        ,mode=mode, opt=opt, scope=["P",P,"Q",Q,"R",R,"S",S] 
        )
    );     
} // ispts   


function isrange(o, _d, _i=0) =
(
   _i==0?
     ( isnum(o[0]) && isnum(o[1])?
       isrange( o, _d= o[1]-o[0], _i=1)
       : false 
     )   
   : _i<len(o)-1?
     ( isnum(o[_i+1]) && _d != o[_i+1]-o[_i] ? false
     : isrange( o, _d=_d, _i=_i+1)   
     )
   : true
 );
 
    isrange=["isrange", "o", "T/F", "Inspect",
    " Return true if the type of o is 'range'
    "];

    function isrange_test( mode=MODE,ops=[])=
    (
       doctest( isrange,
       [ 
         [ isrange( [2:6] ), true, "[2:6]" ]
       , [ isrange( [2:2:10] ), true, "[2:2:10]" ]
       , [ isrange( [1,2,3] ), true, "[1,2,3]" ]
       , [ isrange( [1,2,3,5] ), false, "[1,2,3,5]" ]
       , [ isrange( "abc" ), false, "'abc'" ]
         
       ], mode=mode, ops=ops )
    ); 

//========================================================
{ // isSamePt
function isSamePt( p,q=undef )=
(
    let( Q = q==undef? p[1]:q
       , P = q==undef? p[0]:p
       //, d = [ is0(Q.x-P.x), is0(Q.y-P.y), is0(Q.z-P.z) ]
       )
    is0(Q.x-P.x) && is0(Q.y-P.y) && is0(Q.z-P.z)
   //d==[true,true,true]
);
    isSamePt=["isSamePt", "p,q=undef", "T/F", "Geometry",
    " Return true if p and q, or p[0] and p[1] in case of
    ;; p = PQ and q not defined, are the same point.
    "];

    function isSamePt_test( mode=MODE,ops=[])=
    (
       let( P = randPt()
          , Q = randPt()
          )
  
       doctest( isSamePt,
       [ 
         str( "P = ", _fmt(P) )
       , str( "Q = ", _fmt(Q) )
       , [ isSamePt( P,Q ), false, "P,Q"]
       , [ isSamePt( P,P ), true, "P,P"]
       , [ isSamePt( [P,P] ), true, "[P,P]"]
       , [ isSamePt( [P,Q] ), false, "[P,Q]"]
         
       ], mode=mode, ops=ops )
    );

} // isSamePt       
//========================================================
{ // isSameSide
function isSameSide( pts, pqr )=
(   let( ss = sum( [ for(p=pts) sign( distPtPl( p,pqr) )] )
      )
   //echo( dists = [ for(p=pts)  dist( p,pqr) ])   
   //echo( signs = [ for(p=pts) sign( distPtPl( p,pqr) )])   
  abs(ss)== len(pts)

  // Also: 
  // https://stackoverflow.com/questions/1560492/how-to-tell-whether-a-point-is-to-the-right-or-left-side-of-a-line/53823213#53823213
); 

    isSameSide=["isSameSide", "pts,pqr", "T/F", "Geometry",
    " Return true if all pts is on the same side of plane pqr."];

    function isSameSide_test( mode=MODE,ops=[])=
    (
       doctest( isSameSide,
       [
          
       ], mode=mode, ops=ops )
    );
} // isSameSide         
   
//========================================================
{ // isstr
function isstr(x)=   type(x)=="str";

    isstr= [ "isstr","x", "T|F", "Type,String,Inspect",
    "
     Check if x is a string.
    "];

    function isstr_test( mode=MODE, opt=[] )=(//=====================

        doctest( isstr, 
        [ 
          [ isstr(3), false, "3"]
        , [ isstr(3.2), false, "3.2"]
        , [ isstr("a"), true, "'a'"]
        ],mode=mode, opt=opt
        )
    );
 } // isstr
 
function contain(main, sub)= // sub is subset of main ( 2018.4.29 )
(
   len( [ for( s=sub ) 
          if( has(main,s) ) 1
        ] ) == len(sub)
);

    contain=["contain","main,sub","T|F","Inspect",
    " Check if sub is a subset of main. main could be arr or str, 
    ;; but sub must be an arr 
    "];

    function contain_test( mode=MODE, opt=[] )=
    (
      doctest( contain,
      [ 
        [contain( [2,3,1], [1,2]), true, "[2,3,1], [1,2]"]
      , [contain( [2,3,1], [1,2,4]), false, "[2,3,1], [1,2,4]"]
      , [contain( [[2,3],[4,1],[5,3]], [[2,3],[4,1]]), true
                   , "[[2,3],[4,1],[5,3]], [[2,3],[4,1]]"]
      , [contain( "abcde", "ac"), false, "'abcde','ac'"]
      , [contain( "abcde", ["a","c"]), true, "'abcde',['a','c']"]
      ]
      ,mode=mode, opt=opt )
    );	 
    

//========================================================
{ // istype

//echo("xtype(33)=", xtype(33));
function istype(o,t)=
(	
    ( (type(o)=="float"||type(o)=="int") && t=="num")? true  //== Added 2018.1.20  
    : let( _t = index(t,",")>=0
    		? split(t,",")
    		:index(t," ")>=0
    		? split(t," "): t 
       , _t2= has(_t,"num")? concat( _t, ["int","float"]): _t
       )
    //echo( o=o, t=t, _t=_t, _t2=_t2 )
    isstr(_t2)? ( _t2=="pt"? ispt(o)   //== Added 2018.6.18
                : _t2=="pts"? ispts(o) //== Added 2018.6.18
                : xtype(o)==_t2 || type(o)==_t2
                )
              : sum( [for(x=_t2) 
                      if(type(o)==x||xtype(o)==x
                        || (x=="pt" && ispt(o))   //== Added 2018.6.18
                        || (x=="pts" && ispts(o)) //== Added 2018.6.18
                        ) 1 ] )? true:false 
);

    istype=["istype","o,t","T|F", "type, inspect",
    "
      Given a variable (o) and a typename (t), return true if 
    ;; o is type t. It can also check if o is one of multiple types
    ;; given, in which case, t could be entered as:
    ;;
    ;; istype(o, ['arr','str'] )
    ;; istype(o, 'arr,str')
    ;; istype(o, 'arr str')
    "];

    function istype_test( mode=MODE, opt=[] )=(//=====================

        doctest( istype,
        [ 
          [ istype("3","str"), true, "'3','str'"]
        , [ istype("3",["str","arr"]), true, "'3',['str','arr']"]
        , [ istype(3,["str","arr"]), false, "3,['str','arr']"]
        , [ istype(3,["str","arr","int"]), true, "3,['str','arr','int']"]
        , [ istype(3,"str,arr,int"), true, "3,'str,arr,int'"]
        , [ istype(3,"str arr int"), true, "3,'str arr int'"]
        , [ istype(3,"str arr bool"), false, "3,'str arr bool'"]
        , [ istype(33, ["str", "int"]),true, "33, [\"str\",\"int\"]"]
        , [ istype(33, ["str", "num"]),true, "33, [\"str\",\"num\"]"]
        , [ istype(33.4, "str num"),true, "33.4, \"str num\""]
        , [ istype(33.4, "str int"),false, "33.4, \"str int\""]
        , ""
        , [ istype(3,"num"),true,"3,'num'"]
        , [ istype(3.3,"num"),true,"3.0,'num'"]
        , [ istype([1,2,3],["nums","arr"]),true,"[1,2,3],['nums','arr']"]
        , [ istype([1,2,3],"nums arr"),true,"[1,2,3],'nums arr'"]
        , [ istype([1,2,3],"nums,arr"),true,"[1,2,3],'nums, arr'"]
        , [ istype([1,2,3],"arr,nums"),true,"[1,2,3],'arr,nums'"]
        , [ istype([1,2,3],"arr,str"),true,"[1,2,3],'arr,str'"]
        , [ istype([0,1,2],"arr str"),true,"[0,1,2],'arr str'"]
        , [ istype([0,1,2],"arr,str"),true,"[0,1,2],'arr str'"]
        
        , ""
        , [ istype("3","str"), true, "'3','str'"]
        , [ istype([1,2,3],"nums"),true,"[1,2,3],'nums'"]
        , [ istype([1,2.2,3.3],"nums"),true,"[1,2.2,3.3],'nums'"]
        , [ istype(["a",1],"hash"),true,"['a',1],'hash'"]
        
        ,""
        , [ istype([1,2,3],"pt"), true, "[1,2,3],'pt'"]
        , [ istype([[1,2,3],[4,5,6]],"pts"), true, "[[1,2,3],[4,5,6]],'pts'"]
        , [ istype([1,2,3],"pt,pts"), true, "[1,2,3],'pt,pts'"]
        , [ istype([[1,2,3],[4,5,6]],"pt,pts"), true, "[[1,2,3],[4,5,6]],'pt,pts'"]
        
        ], mode=mode, opt=opt
        )
    );
} // istype  
 

// ========================================================

//=========================================
{ // scomp
function scomp(a,op,b)= // compare str(a) and str(b). Openscad can't
                        // compare arr but can compare strs. So turning
                        // arr to str make them comparable.
( 
  op=="="? isequal(a,b) //a==b
  :op==">"? str(a)>str(b)
  //op=="<"? 
  :str(a)<str(b)
);

    scomp=["scomp","a,op,b","T|F","String, Array, Inspect",
    " compare str(a) and str(b). Openscad can't
    ;; compare arr but can compare strs. So turning
    ;; arr to str make them comparable.
    ;;
    ;; op could be: \">\", \"=\", \"&lt;\"
    "];

    function scomp_test( mode=MODE, opt=[] )=
    (
        doctest( scomp,
        [ 
          [scomp( [2,3], ">", [1,2]), true, "[2,3],\">\",[1,2]"]
	      , [scomp( [6,"D"], ">", [2, "d"]), true, "[6,'D'], '>', [2, 'd']"]
	      , [scomp( [6,"D"], "<", [2, "d"]), false, "[6,'D'], '&lt;', [2, 'd']"]
        , [scomp( [6,"D"], "=", [6, "D"]), true, "[6,'D'], '=', [6, 'D']"]
        , [scomp( [1,"b"], "<", [2, "d"]), true, "[1,'b'], '&lt;', [2, 'd']"]
        , [scomp( [4,"c"], "<", [2, "d"]), false, "[4,'c'], '&lt;', [2, 'd']"]
        ]
        ,mode=mode, opt=opt )
    );	  
} // scomp
// [[2, "d"], [6, "D"], [1, "b"], [4, "c"]]


//=========================================

/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_inspect_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo scadx -fn scadx_inspect.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages:
    //
    // results = get_scadx_inspect_test_results(...);
    //
    // (1) for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_inspect", results, showEach = false );
    //

    [
      begmatch_test( mode, opt )
    , begwith_test( mode, opt )
    , begword_test( mode, opt )
    //, begword__test( mode, opt ) // to-be-coded
    , between_test( mode, opt )
    //, count_test( mode, opt ) // to-be-coded
    //, countAll_test( mode, opt ) // to-be-coded
    , countArr_test( mode, opt )
    , countInt_test( mode, opt )
    , countNum_test( mode, opt )
    , countStr_test( mode, opt )
    , countType_test( mode, opt )
    , endwith_test( mode, opt )
    , endword_test( mode, opt )
    , gtype_test( mode, opt )
    , has_test( mode, opt )
    , is0_test( mode, opt )
    , is90_test( mode, opt )
    , isOnPlane_test( mode, opt )
    , isSamePt_test( mode, opt )
    , isSameSide_test( mode, opt )
    , isarr_test( mode, opt )
    , isat_test( mode, opt )
    , isball_test( mode, opt )
    , isbool_test( mode, opt )
    , iscir_test( mode, opt )
    , iscoline_test( mode, opt )
    , iscoord_test( mode, opt )
    , isequal_test( mode, opt )
    , isfloat_test( mode, opt )
    , ishash_test( mode, opt )
    , isinline_test( mode, opt )
    , isint_test( mode, opt )
    , isline_test( mode, opt )
    , isnan_test( mode, opt )
    , isnum_test( mode, opt )
    , isonline_test( mode, opt )
    , isparal_test( mode, opt )
    , ispl_test( mode, opt )
    , ispt_test( mode, opt )
    , ispts_test( mode, opt )
    , isstr_test( mode, opt )
    , istype_test( mode, opt )
    , scomp_test( mode, opt )
    , type_test( mode, opt )
    ]
);