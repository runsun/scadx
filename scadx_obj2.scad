include <scadx_obj.scad>

//========================================================
Dim=["Dim"
, "site,color,r,spacer,gap,h,pts,followSlope,Label,Guides,GuideP,GuideQ,Arrows,debug,opt"
, "n/a","Obj"
, "Draw a dimention
  ;; 
  ;; site: either pqr (if pts not given) or ijk (if pts given)
  ;;       Or more than 3 pts/indices for chained dim in which all dims facing the same direction 
  ;;       and all have same properties (for multiple dims in which , check out Dims() )
  ;; 
  ;;
  ;; Dim( [P,Q,R] )
  ;; Dim( [i,j,k], pts=pts )
  ;; 
  ;; For chained dims:
  ;; 
  ;; Dim( [P,Q,R,S,T...] )
  ;; Dim( [i,j,k,l,m...], pts=pts )

"
];

DIM_OPT=[]; // Global dim opt. Set this at global level. 2018.5.29.
            // For example, set DIM_OPT = ["r",0.1] would 
            // scale up the entire set of dim for all dims
   

module Dim(   // 2018.5.22 --- re-name pqr to site 
               //               such that it is not limited to 3-item
         site // [P,Q,R], [i,j,k] (when pts given)
              // [P,Q,R,S ...], [i,j,k,...] (when pts given)
        , color 
        , r   // rad of line
        , spacer // dist btw guide and target. Could be negative   
        , gap    // gap between two arrows
                 
        , h      // A ratio for vertical pos of line.  Could be negative
                    /* h ________      0.5         0
                        |   1    |  |_______|  |       |
                        |        |  |       |  |_______|
                        P        Q  P       Q  P       Q
                    */
        , pts // If pts is given, site could be provided as 
              // indices. For examplem, 
              //    site = [0,4,2] 
              //    site = [2,3, [4.38,5.1,2.1] ]
              // new 2018.5.18
                 
        , followSlope   // Follow the line if it has a slope. See
                        // code for details. Default= true        
        
        , Label=true    // false or hash,  or text
        , Guides  // false or hash
        , GuideP  // false or hash
        , GuideQ  // false or hash
        , Arrows  // false or hash
        , Link  //  new 2018.6.20
                 /*           ___
                        |      |   
                        |      |  stem, default=0  
                    .---+---. ---
                    |       |

                   When set to true, auto reset to 0.2;
                   When set:
                    -- h default = 1
                    -- gap default= 0
                    -- Guides len cut in half
                    -- it uses Arrows properties. If Arrows false, use Guides
                 */
        , debug= 0 
          
        , opt= []
        )
{ 
  //echom("Dim()");
  opt= update(DIM_OPT, opt);
  //echo(DIM_OPT=DIM_OPT, opt=opt);

  if(len(site)<=3)
  {   
    _site   = len(site)>2?site:concat( site, [randPt()]);
    site = pts? [ for(x=_site) isint(x)? pts[x]:x ]: _site;
    
    r     = arg(r, opt, "r", 0.015);  
    color = colorArg(color, opt, "blue");
    //_stem  = arg(stem, opt, "stem", 0);
    //stem = _stem==true?0.2:_stem;
    
    _Link = subprop( Link==undef? hash(opt, "Link", false) :Link   
                 , opt, "Link", dfPropName= "pts", dfPropType="pt,pts"
                  , dfsub=["r",0.01, "color", "blue", "$fn",6, "Arrow", false
                          ,"indent", 0
                          //, "stem", Link==true?0.2:isnum(Link)?Link:false
                          , "stem", isnum(Link)?Link
                                    :Link? 0.2:false
                          ] );  
                          
    //echo(Link=Link, _Link=_Link);  
    
    spacer= arg(spacer, opt, "spacer", 0.1);
    _gap  = arg(gap, opt, "gap", _Link?0:undef);
    h     = arg(h, opt, "h", _Link?1:0.5);
    
    followSlope = arg(followSlope,opt, "followSlope", true);
 	/*  
            followSlope= true:    followSlope= false:      
            R decides the plane   R further decides dir
            & side of dim line     of guides 
                /                   
               /'-._              |_________|
              /     '-._  /       |         |
             P'-._      '/        P'-._     |  
             |    '-._  /         |    '-._ |
             |        'Q          |        'Q 
             |         |          |         |
             |         R          |         R
        */    
    _Label= subprop( Label, opt, "Label"
            , dfPropName="text"
            , dfPropType=["str","num", "nums"]
            , dfsub= ["color",color, "size",1, "scale", r/0.015*0.2
                     , "indent",0.1
                     , "followView", false //true
                     , "valign","center" 
                     , "decimal", 2
                     , "maxdecimal", 2
                     , "prefix", ""
                     , "subfix", ""
                     ] 
           );
  
                           
    // Apply temporary fix described in issue #11
    // https://bitbucket.org/runsun/scadx/issues/11/subprop-of-a-subprop-failed-to-shut-down          
    // Head = subprop( sub= Head==undef? hash(opt, "Head", true) :Head
    //               , ... ); 
    // to allow sub be turned off via opt
     
    Guides = subprop( Guides==undef? hash(opt, "Guides", true) :Guides
                     //Guides
                    , opt, "Guides"
                    , dfPropName="color"
                    , dfPropType="str"
                    , dfsub= [ "r", r, "len", r/0.015*0.4/ (_Link==false?1:2)
                             , "color",color
                             ] 
                   );
    
    GuideP = Guides ?
                  subprop( GuideP==undef? hash(opt, "GuideP", true) :GuideP
                         , opt, "GuideP"
                         , dfPropName="color"
                         , dfPropType="str"
                         , dfsub= Guides
                         )  
             : false; 
             
    GuideQ = Guides ?
                  subprop( GuideQ==undef? hash(opt, "GuideQ", true) :GuideQ
                         , opt, "GuideQ"
                         , dfPropName="color"
                         , dfPropType="str"
                         , dfsub= Guides
                         )  
             : false; 
             
             
//    GuideP = Guides ? 
//             ( GuideP==true? Guides
//               : GuideP? update( Guides, GuideP)
//               : false
//             )          
//             : false; 
//    GuideQ = Guides ? 
//             ( GuideQ==true? Guides
//               : GuideQ? update( Guides, GuideQ)
//               : false
//             )          
//             : false; 
                   
    Arrows = subprop( Arrows==undef? hash(opt, "Arrows", true) :Arrows
                    , opt, "Arrows"
                    , dfPropName="color"
                    , dfPropType="str"
                    , dfsub= [ "r", r
                             , "color",color
                             , "Head", _Link==false?true:false
                             ] 
                   );
    
        
    /* subunit "arrow" composes of 2 Arrow modules, one points to
       hP, one hQ. Seperated by gap. The ops is designed as though
       that both the Arrow modules is one Arrows module, 'cos it's 
       unlikely for the two arrows to be formatted differently:
    
        |                                      |
        |  _='                           '=_   |  
      hP|-'-------------  label  ------------'-|hQ
        | '-_          |<- gap->|         _-'  |
        |    '                           '     |
           <-- spacer                             <-- spacer      
        P--------------------------------------Q 
        
        */
    
    a_pqr = angle(site);
   
    P = site[0];
    Q = site[1];
    Ro= site[2];
    
    /* When followSlope = true, R has to be recalc
       such that new a_PQR = 90

            /  
           /'-._
          /     '-._  /
        P'-._       '/
         |    '-._  /        
         |        'Q
         |        /| 
         |       / Ro
                R
            /                    
           /'-._
          /     '-._  /
        Q'-._       '/
        /|    '-._  /        
       / |        '| P
      R  |         | 
         Ro                                          
    */
    R = followSlope
         ? ( a_pqr==90
             ? Ro 
             : anglePt( site, a=90)
           )  
         : Ro ;  
       
     /*
    P----Q   Need X to make pts along XP line
    |    |
    X----R
    */   
    X= P+R-Q; //cornerPt( [P,Q,R] );   
       
    dPQ = norm(P-Q);
    L = (!followSlope) && a_pqr!=90         // L= the dim to be reported
        ? norm( P- othoPt(p201(site))):dPQ;
    
    //-------------------------------------------- guide 
    /*
            gP2         gQ2  --- 
             |           |    |
          hP +-----------+ hQ | g_len  
             |           |    |
         gP0=gP1         gQ1 ---
    spacer-->            |    |
             Po          |    | gQx_len  
             | '-_       |    |  
             X    '-_   gQ0  ---       
             |       '-_    <--- spacer    
             |          'Qo 
             |           |
             |           Ro     
         
    gPx=0 @ followslope=true, or a_PQR>=90
    gQx=0 @ followslope=true, or a_PQR<=90
    */ 
//    h = opt("h");
    //echo("h",h);
    
    //echo(Guides=Guides, GuideP=GuideP, GuideQ=GuideQ);
    g_len = hash(Guides, "len");
    gP_len = hash( GuideP, "len");
    gQ_len = hash( GuideQ, "len");
    
    // _h: an intrisic height to ensure there's a height
    //     when no guides
    _h = gP_len==undef? (gQ_len==undef? 0.5:gQ_len):gP_len;
    
    // extra len of guides due to slope of PQ
    gPx_len= (!followSlope)&& a_pqr<90? shortside( L, dPQ ): 0;
    gQx_len= (!followSlope)&& a_pqr>90? shortside( L, dPQ ): 0;
    
    gP0 = onlinePt( [P,X], len= -spacer );
    gP1 = onlinePt( [P,X], len= -spacer - gPx_len );    
    //gP2 = onlinePt( [P,X], len= -spacer - gPx_len - (gP_len==undef?g_len:gP_len));    
    gP2 = onlinePt( [P,X], len= -spacer - gPx_len - _h);//(gP_len==undef?g_len:gP_len));    
    
    gQ0 = onlinePt( [Q,R], len= -spacer );
    gQ1 = onlinePt( [Q,R], len= -spacer - gQx_len );    
    gQ2 = onlinePt( [Q,R], len= -spacer - gQx_len - _h);//(gQ_len==undef?g_len:gQ_len) );    
    
    hP  = onlinePt( [gP1,gP2], ratio= h);
    hQ  = onlinePt( [gQ1,gQ2], ratio= h);
    
    //echo( spacer=spacer, gP_len = gP_len, gQ_len = gQ_len, gPx_len = gPx_len, gQx_len = gQx_len, gap=gap);
    //echo( gP0=gP0, gP1=gP1, gP2=gP2, hP=hP, hQ=hQ );
    //MarkPts([gP0,gP1,gP2], Label=["text",["gP0","gP1","gP2"]], Line=false );
    //MarkPts([gQ0,gQ1,gQ2], Label=["text",["gQ0","gQ1","gQ2"]], Line=false );
    
    //echo(GuideP=GuideP);
    //echo(GuideQ=GuideQ);
    if( Guides && GuideP ) Line( [gP0, gP2], opt= GuideP );  
    if( Guides && GuideQ ) Line( [gQ0, gQ2], opt= GuideQ );  

    //echo(_Label= _fmth(_Label));
    //echo(Label= (Label));
    labelstr= isstr(Label)? Label
              : str( hash(_Label,"prefix","")
                , hash(Label, "text"
                      , numstr( L
                               , conv= hash(_Label,"conv")
                               , d=    hash(_Label,"decimal")
                               , dmax= hash(_Label,"maxdecimal")
                               )
                      )         
               , hash(_Label,"subfix","")
               );
               
    //echo(L=L, labelstr=labelstr, conv=hash(_Label,"conv")
    //, dmax=hash(_Label,"maxdecimal"), d=hash(_Label,"decimal"));
    //
    //================================================= line
    //
    
    // hP, hQ: two pts where line intersect guides
    // gapP, gapQ: two pts where the arrow line starts. 
    //             determined by ["line",["gap",0]]   
    /*
         gP2                    gQ2
          |                      |
       hP +-----gapP    gapQ-----+ hQ
          |                      |
         gP1                    gQ1    
    
       But, remember that the real hP needs to take
       into account the len of arrow head
    
             _-'| 
       _hP <----+--------- gapP
             '-_| hP 
    
           |<-->| len of arrow head 
    
    */


    labelsize = hash(_Label,"size") ;
    labelscale= hash(_Label,"scale");
    a=labelsize; 
    gap = _gap!=undef?_gap:
          textWidth( labelstr
                   , size = labelsize
                   , scale= labelscale
                   )+ 1*labelsize*labelscale;
                   
    //echo(labelsize=labelsize, labelscale=labelscale, _gap=_gap, gap=gap );
    
    lineL = (L-gap)/2; // arrow len
    
    //echo( labelsize=labelsize, labelscale=labelscale, gap=gap, lineL=lineL);
    
    begP = onlinePt( [hP,hQ], len=lineL);
    begQ = onlinePt( [hQ,hP], len=lineL);

    //echo(gap=gap, L=L, Arrows=Arrows);
    if( gap != L   //  Don't draw arrow when gap is as big as the dim length
        && Arrows ) 
    {
       Arrow( [ begP, hP], opt=Arrows );
       Arrow( [ begQ, hQ], opt=Arrows );
    }
    
//    if(opt_arrowP) Arrow( [ begP, hP], opt_arrowP );
//    if(opt_arrowQ) Arrow( [ begQ, hQ], opt_arrowQ );
         
    //       
    //================================================= label
    //
    //textsite = [hQ, (begP+begQ)/2, P+Q-R];
    M = (begP+begQ)/2;
    
    _textSite = [hQ, M, spacer+_h<0? (2*P-gP2)
                       : (2*gP2-gP0) ];
                       
    stem_spacer = (len(labelscale)?labelscale[1]:labelscale)
                  *(len(labelsize)?labelsize[1]:labelsize)*.8;  
                  
    //vStemOffset = ;                 
    stem = hash(_Link,"stem");
    textSite = _Link? [for(p=_textSite) p+uv( gP2-gP0)* (stem+stem_spacer) ]
                   : _textSite;
                   
                   
    // ===================== 2018.6.20: Link                          
                           
    _lpts = hash(_Link,"pts");
    __lpts= isnum(_lpts[0])?[_lpts]:(ispt(_lpts)?[_lpts]:_lpts);
    //echo(_Link=_Link);
    //echo(_lpts=_lpts);
    //echo(__lpts=__lpts);
    //echo(arrow_opt= update(hash(_Link,"Arrow",[]), _Link) );
    lpts = concat( stem>0? [(M+ uv( gP2-gP0)*stem)]:[]
                   , __lpts? __lpts:[]
                   );
                   
    if( !(Link==false || Link==undef ) ) // Link is defined in some way
    {   
      //echo(lpts = lpts);
     
      headPt = onlinePt( [textSite[1], lpts[0]],len= hash(_Link,"indent",0));
      headPt = lpts[0];
      //Red() MarkPts(lpts);
      _arrow = hash(_Link,"Arrow",[]);
      //echo(_arrow=_arrow, headPt=headPt);
      //MarkPts( [M,headPt], "MH"); 
      if(_arrow==false )
        { if (headPt) Line( [M, headPt], opt=_Link); }
      else 
      Arrow( [headPt, M]
           , opt= ishash(_arrow)? update(_arrow, _Link):_Link
           );
      if(len(lpts)>1)
      {
         Line( lpts, opt=_Link);
      }     
    }
//    if(Link==true || istype(Link,"num"))
//    {
//      echo("Drawing stem");
//      stem_opt= Arrows?Arrows: Guides;
//      Line( [M, M+ uv( gP2-gP0)*stem], len=stem, opt=stem_opt);
//    } 
//    else if(_lpts) 
//    {        
//        headPt = onlinePt( [textSite[1], lpts[0]],len= hash(_Link,"indent",0));
//        
//        //Red() MarkPts(lpts);
//        _arrow = hash(_Link,"Arrow",[]); 
//        if(_arrow==false)
//        Line( [lpts[0], headPt], opt=_Link);
//        else 
//        Arrow( [lpts[0], headPt]
//             , opt= ishash(_arrow)? update(_arrow, _Link):_Link
//             );
//        if(len(lpts)>1)
//        {
//           Line( lpts, opt=_Link);
//        }     
//    }
    
    //====================================================== end Link
                       
                   
    //echo( hQ=hQ, gP0=gP0, gP2=gP2, _h=_h);             
    //echo(stem=stem, _textSite=_textSite);               
    //echo(textSite=textSite);  
    //echo(labelstr = labelstr);
    //echo(labelscale=labelscale);
    //echo(labelscale_1en=len(labelscale));
    //echo(labelsize=labelsize);
    //echo( gP2=gP2, gP0=gP0);
    //echo( stem_spacer=stem_spacer, stem_offset = stem_offset);
    //echo("uv( gP2-gP0)* (stem+labelsize*labelscale*.8)= ", uv( gP2-gP0)* (stem+labelsize*labelscale*.8));
    //echo(_Label=_fmth(_Label));
//    if(stem)
//    {
//       stem_opt= Arrows?Arrows: Guides;
//       Line( [M, M+ uv( gP2-gP0)*stem], len=stem, opt=stem_opt);
//       //MarkPts( [M,Pt_stem], "MS");
//       //MarkPts( [gP2,gP0], ["gP2","gP0"]);
//    }               
                                      
    //check = ["hQ",hQ, "M",M, "P",P, "Q",Q, "R",R
            //,"gP0",gP0, "gP1",gP1, "gP2", gP2, "2gP2-P",2*gP2-P, "2P-gP2", 2*P-gP2
            //];
    //MarkPts( vals(check), Line=false
    //       , Label=["text",keys(check)] );
           
    //color("red") MarkPts( textSite );       
    //echo(_fmth( check ));       
    
    if(debug)
    {
      echom("Dim");
      echo(site=site);
      echo(opt=opt);
    }
    
    Text(labelstr, opt=_Label    
        //, site=textSite //P+Q-R]
        , site=_lpts? ( [for(p=textSite)p+ last(lpts)-textSite[1]]): textSite
        , halign= hash(_Label,"halign","center")
        , valign= hash(_Label,"valign","center")
        );
 
 } else // len(site)>3 
 {
   /*  
     when len(site)>3, like 
       
     [3,1,5,1], [2,3,4,5,6,7]  (indies when pts given)
     [p0,p1,p2,p3], [p0,p1,p2,p3,p4,p5,p6]  (when pts not given)
      
     The last one is always the reference pt.
   */
   
   
   _sitePts= pts? [for(i=site) pts[i]]: site;
   
   for(i=[0:len(_sitePts)-3]) 
   {
     Rf = i== len(_sitePts)-3? last(_sitePts)
          : last(_sitePts)- last(_sitePts,2) +_sitePts[i];
            
     Dim(   
         site = [ _sitePts[i], _sitePts[i+1], Rf ] // [P,Q,R]
        , color=color
        , r=r 
        , spacer=spacer 
        , gap=gap    
        , h=h   
        , pts= undef
        , followSlope= followSlope   
        , Label= Label    
        , Guides=true  
        , GuideP= i==0?true:false  
        , GuideQ= true  
        , Arrows= Arrows
        , Link= Link
        , debug= debug 
        , opt=opt
        );           
   }
   
 } // len(site)>3       
}// Dim

  
module Dims( pts, opts, debug=0, opt=[]) 
{                      
    // Recode 2018.5.21
    // opts: array of [ site, opt ], each representing a single Dim
    // Dims: for cases of setting different dims on
    //       an given *pts*, each dim could have its own opt.
    //       Good for using dim as sub-obj in other obj
    //REF: see Dim_demo.scad and Cube_demo.scad
   /*
   
   opts= array of site or [site,opt]
   
   
    Used in Cube():
    
      Cube(... dim= [2,3,4] );
      Cube(... dim= [2,3,pt] );
      Cube(... dim= [ [2,3,pt], [4,5,6],... ] );
      Cube(... dim= [ [2,3,pt], ["pqr", [2,3,pt], "color", ], ... ] );
      Cube(... dim= ["pqr", [2,3,pt], "color", ] );
      Cube(... dim= [ "pqrs", [[2,3,pt]], "color", "red" ] );        
      Cube(... dim= [ "pqrs", [2,3,pt], "color", "red" ] );     
   */
   
   if(debug)
   {
     echom(_b(_s("Dims, pts={_}",str(pts))));
     echo(opt=opt);
   }
      
   for( site_opt = opts )
   {
      _opt= isnum( site_opt[0] )?opt 
            : update( opt, site_opt[1] );
            
      site = isnum( site_opt[0] )? site_opt: site_opt[0];                    
      
      if(debug)
      { 
        echo(site_opt=site_opt);
        echo(site=site)
        echo(_opt=_opt);
      }
      
      Dim( site= site
         , pts=pts
         , debug=debug
         , opt= update(_opt, opt) 
         );
   }
   
}








  