//include <scadx_core.scad>
//include <scadx_obj_adv_dev.scad>

ECHO_MOD_TREE= true;  // 2015.5.22
    /* Every single module will have an argument: 
       mti (module tree indent) as its last arg.
       mti is an integer indicating which layer
       the module is in in the current module tree.
       
       module A( ... mti=0){
          if(ECHO_MOD_TREE) echom( "A", mti);
          B( ... mti=mti+1)
       }
    */ 
    
//mm = 0.01;  // millimeter






//. a

function advchaindata( 
        
    pts
    , nside 
    , nsidelast // 2015.7.23: Different nside than the 
                // beginning face, allowing for loft.
           /*  The loft can be done in :
           
               1) Set the 1st cut with either nside or cut0
               2) Set the last cut w/ either nsidelast or cutlast
               
               Note that in these conditions :
               -- twist will NOT twist the cuts, but will rotate 
                    the cutlast for loft
               
                  .------.
                .'        `.
              .`     .      `.
               `.          .'
                 `.______-`

           */ 
    , loftpower // Power of lofting. The real r at pt i, ri, changes
                // its value from r_head to r_tail over i's. The larger 
                // the loftpower is, the faster the ri approaches r_tail. 
                // If =0, the change of r follows a straight line. If 
                // between -1~0, r will remains unchanged until the end 
                // of pts before it starts lofting. If <-1, r could shrink 
                // first before start lofting. 
                 
    , r    // radius at the head (first cut)
    , rlast   // radius at the tail (last cut)
    //, rs     // array of r to be added on top of r 
             //   If rs < pts, repeat rs  
             // Note that this array could contain arrays:
             //   rs = [ 3, [3,3.5], 2,2 ] 
             // indicating multiple r's at that pt
    , closed // If the chain is meant to be closed.
             // The starting and ending crosecs will
             // be modified. 
    //-------------------------------------------------
    , twist // Angle of twist of the last crosec 
            // comparing to the first crosec
    , rot   // The entire chain rotate about its bone
            // away from the seed plane, which
            // is [ p0, p1, Rf ]


    //-------------------------------------------------
    // The following are used to generate data using 
    // randchain( data,n,a,a2... ) when data is a pts.
    // They are ignored if data is (2) or (3) 

    //         , n      //=3, 
    //         , a      //=[-360,360]
    //         , a2     //=[-360,360]
    //         , seglen //= [0.5,3] // rand len btw two values
    //         , lens   //=undef
    //         , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
    //                         //   , "clos
    , seed   //=undef // base pqr
    , Rf // Reference pt to define the starting
         // seed plane from where the crosec 
         // starts 1st pt. Seed Plane=[p0,p1,Rf]
            
    //-------------------------------------------------
    , cut0 // Define cross-section. This would become
             // the first cross-section (i.e., chain head)
             // When defined, the r,rlast, and rs are ignored
    , cutlast // Define cross-section. This would become
             // the last cross-section (i.e., chain tail)
             // When defined, the r,rlast, and rs are ignored
             // Define both cut0 and cutlast to loft.
             
    
    , bevel0

    , tilt    
    , tiltlast   /* [rot,tilt,posttilt_rot]
                 tilt angle at first face( tilt) or last face
                 
                 x is a number btw 0~360, representing
                 pos on a clock face. Line XO will be the
                 axis about which the pl_PQRS will rot
                 and tilt by angle a to create the tilt.
                 
                            /  
                           / Z
                       /  / /    /
                      / _Q_/    /
                     _-`  /`-_ /
                    R    O----P
                     `-_   _-`
                        `S`
                        
                 Let Z the last pt of bone. 
                 
                 Default x is 0, which means the tilt is
                 to tilt toward the first pt of xsec. i.e,
                 aPOZ = a. 
                 
                 If x is 90, toward the Q-direction on
                 the above graph, i.e., aQOZ=a 
                 
                 htilt could be a number a, in that case
                 default OQ is the axis. i.e., x=0       
              */
    , shape // default: chain
    //===============================================
    , _pl90at0 // The pl90 ( pts on the first 90-deg plane.
               // It is kept over the entire length of chain
               // till the last for the possible need of loft
    , opt=[] 
    , _rs=[]    
    , _cuts=[]
    ,_i=0
    , _debug)=
(   
   _i==0?
   
   let(//###################################### i = 0 
   
      //------------------------------
      //    param initiation
        
        opt = popt(opt) // handle sopt
      , pts = ispt(pts[0])? pts 
              : haskey( pts, "pts")? hash(pts, "pts")
              : or(seed, hash(opt,"seed"))? seed
              : pqr()
      
    
      , rot= und(rot, hash(opt,"rot",0 )) 

      //, xpts= or(xpts, hash(opt,"xpts",undef))
      , cut0= or(cut0, hash(opt,"cut0", undef))
      
      , Rf0= or(Rf, hash(opt,"Rf", undef))       
      , twist= und(twist, hash(opt,"twist",0))
      , closed= len(pts)==2?false
                : und(closed, hash(opt,"closed",false))
                
      //, _hbevel= or( hbevel, hash(opt,"hbevel"))
      //, hbevel= _hbevel==true? ["dir",-1, "r",0.5, "n", 
      
      , _tilt= und( tilt, hash(opt,"tilt"))
      , _tiltlast= und( tiltlast, hash(opt,"tiltlast"))
      , tilt = isnum(_tilt)?[0,_tilt]:_tilt
      , tiltlast = isnum(_tiltlast)?[0,_tiltlast]:_tiltlast
      
      , _nside= or( nside, hash(opt,"nside",4))
      , nside = cut0? len(cut0):_nside
      , nsidelast= und( nsidelast, hash(opt,"nsidelast"))
      , loftpower= und( loftpower,hash(opt,"loftpower",0))
 
      //
      // --- Set r. One of the most critical steps ---
      //
      // This will convert r to an array rs. All r's required
      // through the entire pts are calced here. 
      //
      // What could alter r: 
      //   rlast ==> create a tapering effect 
      //   distRatios ==> if rs[0]!= rs[-1], then distRatios
      //                  will alter rs[_i]
      //   nsidelast ==> create a lofting effect 
      //   loftpower ==> if nsidelast, a loftpower will alter r[_i]
      // 
      // rlast is disabled WHEN an array is given to r, 'cos 
      // it doesn't make sense to set rlast and r to array at the same time
      // 
      // At this moment, we make loft a "post-processing" step, means
      // re-calc everything at the last pt for lofting. This is not idea
      // but lets not complicate things for now. 
      // 
      , L = len(pts) 
      , rng= [0:L-1] 
      
      , r  = und(r, hash(opt,"r", 0.5))
      , rlast  = isarr(r)? undef: und(rlast,hash(opt,"rlast"))
      , distRatios = distRatios(pts)
      , rs= cut0? undef
            :rlast?  
               let( d = rlast-r )
               [ for( i=rng ) r+ d*distRatios[i] ] 
            :isarr(r)?
             (len(r)< L?  // If rs < pts, repeat rs
               repeat(r, ceil( L /len(r)))
               :r
             ): repeat( [r], L )  
                   
      //, rs= rlast? [ for( i=rng ) distRatios[i]* rs_fulllen[i] ]
      //           : rs_fulllen
      //, rs= rs_fulllen      
      //=========================================
      
      , coln_101= iscoline( p_101(pts))   
      , coln012 = iscoline( p012(pts))   
      , coln123 = iscoline( p123(pts))   
      , _Rf = Rf0?Rf0
             : len(pts)==2? randOfflinePt( pts )
             : closed ? 
                ( coln012 ?   //   x . . .0---1
                              //  / 
                  ( coln123?  //   x . .0---1---2
                              //  /
                    randOfflinePt(p01(pts))
                    :pts[3]   //    x . . 0---1
                  )           //   /          | 
                : pts[2]      //   x . . 0
                ) 
             : 
               ( coln012?                     //   0---1---2
                  ( coln123 || len(pts)==3?   //   0--1--2--3
                    randOfflinePt(p01(pts))
                    : pts[3]                  //   0--1--2
                  )                           //        /   
               : pts[2]       //   0--1 
               )              //     /
               
      , Rf = rot? rotPt( _Rf, p01(pts), rot):_Rf 

      , newr= rs[0]
//      , _newr = isnum(rlast)? (_i*(rlast-r)/(len(pts)-1)+r)
//                  : r
//         
//      , newr = rs ? 
//               (  // rs[_i] could be array !!
//                 rs[0] && isarr( rs[0] ) ? 
//                   [ for(_r=rs[0]) _r+_newr]
//                 :isnum(rs[0])? (_newr + rs[0] )
//                 :_newr
//              )     
//              : _newr
       
      , preP = onlinePt( p01(pts), -1)
      
      //====================================================             
      // Data pts are created on pl90, then projected to pl
      // to form cut:
                   
      // pl90 is the actual data pts created on a plane that is
      // 90d to the line p01. They will soon be projected to pl
      // if either tilt is defined or closed is true            
      , pl90= ispts(cut0) || ispts( cut0,2) ? 
               let( // Translate pts in given cut0 to the pl90
                    //preP= onlinePt( p01(pts), -1)
                   R90 = anglePt( [ preP, pts[0], Rf], a=90 )
                  , uvX = uv( pts[0], R90 )
                  , uvY = uv( pts[0], N( [preP,pts[0],R90] ) )        
                  ) 
                  [ for(P=cut0) pts[0]+ uvX*P.x + uvY*P.y ] 
             
             // Rewrite to use new circlePts 2018.8.6      
             : circlePts( [ B( [preP, pts[0], Rf] )
                          , pts[0]
                          , N( [preP, pts[0], Rf] )
                          ] , n=nside
                        , rad= isarr(newr)?newr[0]:newr        
                        )
             //: circlePts( [preP, pts[0], Rf], n=nside
                        //, rad= isarr(newr)?newr[0]:newr        
                        //)
                  
      , pl90_xtra= isarr(newr)?              
                 // Create xtra pl on the same _i 
                 arcPts( [preP, pts[0], Rf]  
                      , a=90                
                      , a2= 360*(nside-1)/nside
                      ,n=nside, rad= newr[1]
                      )
                 :undef
      
      // pl is the actual plane onto which the pts placed 
      // on pl90 will be projected to. Since it is just a
      // pl to hold data, number and order of pts that form
      // this pl are not important.  
      // As a result, the posttilt_rot has no effect
      , pl = closed?
               ( let( pldata = planeDataAt(pts,0
                                      ,closed=true) )
                  hash( pldata,"pl")
               )
              :tilt[1]?
               ( let( pldata = planeDataAt( //pts,0, Rf=Rf 
                                        [pts[0],pts[1],Rf],0, Rf=Rf
                                      , rot= tilt[0]
                                      , tilt= tilt[1]==90?undef:tilt[1]
                                      //, posttilt_rot= tilt[2]
                                      //tilt?tilt[2]:undef
                                      ,closed=false) )
                  hash( pldata,"pl")
               ) 
               : pl90  
               
      // cut contains the actual data pts (on the plane pl)         
      , cut= closed|| tilt[1] ? 
                projPts( pl90, pl, along=p01(pts) ) 
                : pl90
      , cut_xtra= isarr(newr)?
                  (closed || tilt[1]? 
                    projPts( pl90_xtra, pl, along=p01(pts) ) 
                    : pl90_xtra        
                  ):undef  
      
      // _cuts will be carried to next pt 
      , _cuts= isarr(newr)? [ cut, cut_xtra]: [cut]
    
       , _debug=str(_debug, "<hr/>"
          ,_red("<br/>, <b>chaindata debug, _i</b>= "), _i 
          ,"<br/>, <b>r</b>= ", r
          ,"<br/>, <b>rlast</b>= ", rlast
          //,"<br/>, <b>rs_fulllen</b>= ", rs_fulllen
          ,"<br/>, <b>rs</b>= ", rs
          ,"<br/>, <b>pts</b>= ", pts
          ,"<br/>, <b>nside</b>= ", nside
          ,"<br/>, <b>closed</b>= ", closed
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>preP</b>= ", preP
          ,"<br/>, <b>pl90</b>= ", pl90
          ,"<br/>, <b>pl90_xtra</b>= ", pl90_xtra
          
          ,"<br/>, <b>pl</b>= ", pl
          ,"<br/>, <b>Rf</b>= ", Rf
          ,"<br/>, <b>cut0</b>= ", arrprn(cut0, dep=2)
          ,"<br/>, <b>cut</b>= ", cut
          ,"<br/>, <b>pts_i</b>= ", arrprn(pts[_i],dep=2)
          ,"<br/>, <b>, tilt</b>= ", tilt 
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>_cuts</b>= ", _cuts
//          ,"<br/>, <b>xpts_i</b>= ", arrprn(xpts_i, dep=2)
          //,"<br/>, <b>newrtn</b>= ", arrprn(newrtn, dep=2)
        )//_debug
        
      )// let @ i=0
      //_debug
      chaindata( pts
               , nside=nside
               , nsidelast=nsidelast
               , loftpower=loftpower
               , r=r, rlast=rlast, rs=rs
               , closed=closed
               , twist=twist, rot=rot
               , Rf= Rf //cut0[0]
               , cutlast= cutlast
               , tilt=tilt,tiltlast=tiltlast 
               , _rs = rs              
               , _cuts=_cuts
               , _pl90at0 = pl90
               ,_i=_i+1
               ,_debug=_debug
               )                 
 
   : 0<_i && _i <len(pts)-1?
   
    let( //#################################### 0 < i < last 
   
        dtwi = twist? twist/(len(pts)-1) :0
        ,lastcut = last(_cuts)
        ,boneseg = get( pts, [_i-1,_i] )
        , rs = _rs

        // The pl where previous data will project to
        , pldataAt = planeDataAt( pts,_i)
        , pl_i = hash( pldataAt, "pl")
      
        , newr = rs[_i] 
        
        , cut = projPl( pl1=lastcut
                      , pl2=pl_i
                      , along= boneseg
                      , newr= isarr(newr)?newr[0]:newr
                      , twist=twist
                      )
                      
        , cut_xtra = isarr(newr)?
                        projPl( pl1=lastcut
                          , pl2=pl_i
                          , along= boneseg
                          , newr= newr[1]
                          , twist=twist
                          )
                    : undef 
        
        ,newcuts= isarr(newr)? concat( _cuts,[cut,cut_xtra])
                              : app(_cuts,cut)                   
   
       , _debug=str(_debug, "<hr/>"
          ,_red("<br/>, <b>chaindata debug, _i</b>= "), _i 
//          ,"<br/>, <b>r</b>= ", r
          ,"<br/>, <b>lastcut</b>= ", lastcut
          //,"<br/>, <b>pl_i</b>= ", keys(pldataAt)
          ,"<br/>, <b>pl_i</b>= ", pl_i
          ,"<br/>, <b>rlast</b>= ", rlast
          ,"<br/>, <b>rs</b>= ", rs
          ,"<br/>, <b>pts</b>= ", pts
          ,"<br/>, <b>nside</b>= ", nside
          ,"<br/>, <b>closed</b>= ", closed
          ,"<br/>, <b>newr</b>= ", newr
          
//          ,"<br/>, <b>pl</b>= ", pl
//          ,"<br/>, <b>pl90</b>= ", pl90
//          ,"<br/>, <b>cut90</b>= ", cut90
//          ,"<br/>, <b>cut90_twisted</b>= ", cut90_twisted
//          ,"<br/>, <b>cut90_r_adjusted</b>= ", cut90_r_adjusted
//          ,"<br/>, <b>cut90_r_adjusted_xtra</b>= ", cut90_r_adjusted_xtra
          ,"<br/>, <b>Rf</b>= ", Rf
//          ,"<br/>, <b>lastcut_r_adjusted</b>= ", lastcut_r_adjusted
//          ,"<br/>, <b>cut_projected</b>= ", cut_projected
          ,"<br/>, <b>cut</b>= ", cut
          ,"<br/>, <b>cut_xtra</b>= ", cut_xtra
          ,"<br/>, <b>pts_i</b>= ", arrprn(pts[_i],dep=2)
          ,"<br/>, <b>, tilt</b>= ", tilt 
          ,"<br/>, <b>cut0</b>= ", arrprn(cut0, dep=2)
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>_cuts</b>= ", _cuts
          ,"<br/>, <b>newcuts</b>= ", newcuts
          )
        )// let @ i>0

      //_i==len(pts)-2? _debug:  
      chaindata( pts
               , nside=nside
               , nsidelast=nsidelast
               , loftpower=loftpower
               , r=r, rlast=rlast, rs=rs
               , closed=closed
               , twist=twist, rot=rot
               , Rf= Rf //cut0[0]
               , cutlast= cutlast
               , tiltlast=tiltlast
               , _cuts= newcuts
               , _rs = _rs
               , _pl90at0 = _pl90at0
               ,_i=_i+1
               ,_debug=_debug
               )  
               
//       chaindata( pts, r=r, rlast=rlast, rs=rs
//                , nside=nside, rot=rot, Rf=xsec[0]
//                , tilt=tilt,tiltlast=tiltlast, twist=twist
//                , closed=closed, _xsecs=newxsecs
//                ,_i=_i+1,_debug=_debug)
                
                
      : //_i==len(pts)-1? 
   
     let( //#################################### i = last
          dtwi = twist? twist/(len(pts)-1) :0
        , rs = _rs
        , newr = last(_rs) 

        , lastcut = last(_cuts)
        , boneseg = get(pts, [-2,-1])
        
        , pldata= closed?
                   planeDataAtQ( get3(pts,-2) ) 
                 : tiltlast && tiltlast[1]?
                   planeDataAt( pts,_i,
                        , rot= tiltlast[0] //tiltlast? tiltlast[0]:undef
                        , tilt=tiltlast[1] //tiltlast? tiltlast[1]:undef
                        , closed=closed
                        , Rf= last(_cuts)[0]) 
                   :planeDataAt(pts,_i,a=90)
                   //planeDataAt( pts,_i,
                        //, Rf=Rf
                        //, closed=false
                   //) 
          // pl: where to project pts         
        , pl_i = hash( pldata, "pl")
        
        , cut = isarr(r) && last(r)==0? [last(pts)]
                :projPl( pl1=lastcut
                      , pl2=pl_i
                      , along= boneseg
                      , newr= isarr(newr)?newr[0]:newr
                      , twist=twist
                      )
                      
        , cut_xtra = isarr(newr)?
                        projPl( pl1=lastcut
                          , pl2=pl_i
                          , along= boneseg
                          , newr= newr[1]
                          , twist=twist
                          )
                    : undef 
        
        ,newcuts= isarr(newr)? concat( _cuts,[cut,cut_xtra])
                              : app(_cuts,cut) 
              
//        , cut_r =  projPts( pre_cut, pl
//                           , along=get(pts,[_i-1,_i] )
//                           )
//        ,_cut = isnum(rlast)&&rlast!=r || rs? [ for(p= cut_r)
//                        onlinePt( [ pts[_i], p], isarr(newr)?newr[0]:newr)]
//                   : cut_r 
//        
//        , cut = newr==0 && !cut0?  // cone
//                 [_cut[0]]:_cut
//        
//        , cut_xtra = isarr(newr)?
//                      ( isnum(rlast) || rs? [ for(p= cut_r)
//                        onlinePt( [ pts[_i], p], newr[1])]
//                       : cut_r 
//                      ):undef   
//        
//        ,_newcuts= isarr(newr)? concat( _cuts,[cut,cut_xtra])
//                              : app(_cuts,cut) 
//        , newcuts=is0(r + (rs?rs[0]:0))? // for conedown
//                    replace( _newcuts,0,[pts[0]]) 
//                   : _newcuts 
        /*
          lofting
            
          When nsidelast is set, Lofting is handled AFTER all other
          processes are done. This is not the opitimized way,
          'cos it will do many steps that are not needed for the 
          lofting, so it's a waste of time. However, this is a 
          complicated process and could mess up other process easily, 
          so mixing them up is not a good idea at this moment. 2015.7.25

          Lofting is done by first calc the "paring data" using 
          the first and last crossections(taken from newcuts) with
          getParingData(), converting to two xsecs (called lofthead, 
          lofttail) that have same # of pts.
          
          Then loop thru pts, in each step i, 
          (1) translocate the lofthead to the pl that's 90d to the 
          bone at pts[i] (call it pl90). Lets call it loft_i; 
          (2) Loop thru loft_i, in each step xi, adjust each r (dist 
          between loft_i[xi] and bone) according to rdiffs (r 
          differences) obtained w/ getParingData(). Call the result 
          loft_i_r 
          (3) Project loft_i_r onto the pl at i (pl_i).   
          
        */    
        , cuts = nsidelast>2? // lofting
                  let( 
                       pts1 = newcuts[0] 
                     , lastcut= last(newcuts)
                     , pqr= [ get(pts,-2), get(pts,-1), lastcut[0]] 
                     , pts2 = circlePts( 
                                //pqr= [ get(pts,-2)
                                     //, get(pts,-1)
                                     //, lastcut[0]] 
                                // Rewrite to use new circlePts 2018.8.6      
                                 pqr= [ B(pqr), pqr[1], N(pqr)] 
                                , rad= dist( lastcut[0], last(pts))
                                , n = nsidelast
                             )
                             
                     , pairdata =  getPairingData(
                                       pts1= pts1
                                      , pts2= pts2
                                      )
                     , lofthead = hash( pairdata, "pts1") // head face
                     , lofttail = hash( pairdata, "pts2") // tail face
                     , angles = // Angles of each pt (in lofthead or 
                                // lofttail) spanning from pt [0]. These
                                // angles are the same for lofthead/lofttail)  
                                hash(pairdata, "angles")
                     , rdiffs = // radius and their differences:
                                // [ [rdiff0,r_head0,r_tail0]
                                // , [rdiff1,r_head1,r_tail1]
                                // ]
                                hash( pairdata, "rdiffs" ) 

                     , lenratio = // For [p0,p1,p2...pn] of pts (the bone),
                                  // ratio of di/d where di= dist(0,i),
                                  // d= dist(0,n), eg: [ 0, 0.3, 0.45,0.78,1]
                            let( lens= [ for(i=[1:len(pts)-1])
                                        dist( get(pts, [i-1,i])) ]
                               , lensum= sum(lens)
                               , accumlens= accum(lens)
                               )
                            concat([0],[ for( d=accumlens) d/lensum ])
                     )
                            
                 [ for( i=range(pts) )
                      i==0 ? lofthead
                      : i== len(pts)-1? lofttail
                      : let( pts_unloft = newcuts[i]
                           , lofthead_at90i = 
                                // this is lofthead relocated to pl 90d to (i-1,i)
                                [for(xi= range(lofthead))
                                    
                                  anglePt( [ pts[i-1], pts[i], pts_unloft[0] ]
                                         , a=90, a2=angles[xi]
                                         , len= rdiffs[xi][1]
                                         )    
                               ]
                            , lofttail_at90i = 
                                // this is lofttail relocated to pl 90d to (i-1,i)
                                [for(xi= range(lofttail))
                                   
                                  anglePt( [ pts[i-1], pts[i], pts_unloft[0] ]
                                         , a=90, a2= angles[xi]
                                         , len= rdiffs[xi][2]
                                         )      
                                ] 
                             , projected_loft_i = 
                                [for(xi= range(lofttail))
                                  let( phead= lofthead_at90i[xi]
                                     , ptail= lofttail_at90i[xi]
                                     , dr = rdiffs[xi][0] // diff of r
                                     , dri_ratioed= // dist of phead~ptail when 
                                                // lofthead and lofttail are
                                                // placed on the same plane.
                                                // It could be <0
                                                dr* lenratio[i] 
                                    , dri_factored =
                                            loftpower>=0?  
                                            ( dri_ratioed
                                             +(dr-dri_ratioed)
                                              *(1-exp(-loftpower*lenratio[i]))
                                            ):                                
                                            ( dri_ratioed
                                              -(dr-dri_ratioed)
                                              *(1-exp(loftpower*lenratio[i]))
                                            )   

                                     , p90=  onlinePt( [phead,pts[i]]
                                                     , len= -dri_factored)
                                     )
                                projPt( p90, p012( pts_unloft ),get(pts,[i-1,i]) )
                                ]
                             ) 
                      projected_loft_i 
                ]
                : newcuts      
                      
                      
//        , newxsecs = rlast==0 && !cut0?  // cone
//                     [_newxsecs[0]]
//                     : _newxsecs  
                    
       , _debug=str(_debug, "<hr/>"
          ,"<b>_i</b>= ", _i 
          ,"<br/>, <b>nside</b>= ", nside
          ,"<br/>, <b>cut</b>= ", cut
          ,"<br/>, <b>closed</b>= ", closed
          ,"<br/>, <b>pl_i</b>= ", pl_i
          ,"<br/>, <b>pldata</b>= ", pldata
          ,"<br/>, <b>tiltlast</b>= ", tiltlast 
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>twist</b>= ", twist
          ,"<br/>, <b>dtwi</b>= ", dtwi
         /// ,"<br/>, <b>last_xpts</b>= ", arrprn(last(_xpts),2)
          ,"<br/>, <b>pts</b>= ", pts
          ,"<br/>, <b>rs</b>= ", rs
          ,"<br/>, <b>cut_xtra</b>= ", cut_xtra
          ,"<br/>, <b>newcuts</b>= ", arrprn(newcuts, dep=2)
          )//_debug      

        )
      //_debug
     
      ["cuts", isarr(r) && r[0]==0? concat( [[pts[0]]], slice( cuts,1))
               :cuts
      //,"newcuts",newcuts
        ,"rs", rs
         ,"Rf", Rf 
         
         ,"__<b>Args</b>__","==>"
         ,"bone", pts
         ,"nside",nside
         ,"nsidelast",nsidelast
         ,"r",r
         ,"rlast", rlast
         ,"tilt", tilt //[tilt,tiltax]
         ,"tiltlast", tiltlast //[tiltlast,tiltlastax]
         ,"closed", closed
         ,"twist", twist
         ,"rot", rot
         ,"cut0", cut0
         ,"cutlast", cutlast

         ,"seed", seed
         //,"loftpower", loftpower 
         ,"__<b>debug</b>__","==>"  
         ,"debug", _debug
         ]    
                
);  // advchaindata 
    
    
//############################################################
    

//========================================================
Arc=[[],
"

 angle not given:  draw arc:RS

                     _R 
                   _:  :
                  :     : 
                 -       :
                :         : 
               -           : 
       p ------S-----------'Q


 angle given: draw arc:DS

                     _R 
                   _:  :
                D :     : 
                 -'-_    :
                :    '-_  : 
               -     a  '-_: 
       p ------S-----------'Q


	 
"];

  
module Arc(pqr,opt=[]){

    //echom("Arc","");
    /*
      Given pqr, draw a curve c_PS: 
    
                       _R 
                  S _-`  
                 _-`:      
              _-`    :       
           _-`        :          
        _-`           :            
      Q---------------: P
             rad (radius of curve= dPQ if not given)

                       _R 
                  S _-`  
                 _:`: :    
              _-`  : : :     
           _-`      : : :        
        _-`         : : :           
      Q-------------:-P-:
      |..... rad .....|-|  
                       r  (base radius of the curve line PS)
    
                     _._       C: center of xsec 
                   -` | `-     rss: radius around C 
     Q------------|---C---|            
                   -  |  -`  
                    ``-``
    */
    df_opt=[ 
        "a", angle(pqr)  // angle determining cone length
      , "nseg", $fn==0?6:$fn //# of slices 
      , "fn", 6 //$fn==0?6:$fn    //# of points along bottom circle
                                   // if not given, use a_pqr
      , "rad", d01(pqr)  // rad of the curve. 
      , "rads", undef
      , "radpts", undef // Instead defining pts by dist to Q,
                         // radpts give 3D pt coords in ref. to Q. 
      
      , "r", 0.3         // Set rad of crossection on P and R ends at once
      , "rP", undef      // rad of the circle at P
	  , "rR", undef      // rad of the circle at R  
      
      
      // rs, rss, xsecpts: only one is used:
      , "rs", undef      // rads along the curve line PS 
      , "rss", undef     // r around the xsec center C. It sets 
                         //  a symmetric rs pattern for each xsec
      , "xsecpts", undef // [ slice1, slice2 ...] where slice?=[p1,p2...]
      
      , "rotate",0       
      
      , "frame", false // mark the curve line
      , "markxsec", false // true | i | [i1,i2 ...] mark xsec(s)
      , "xsecopt", []  // add to markxsec opt to customize xsec marking
      
      , "frame", false // draw lines of edges 
      , "showxsec2d", false // show xsec on xy plane
//      , "showobj", true // false to hide shape to debug (by viewing markers)
      , "hide",false
      
      , "closed", false  // close the arc to make it a circle
      , "sealed", false  // true: seal the 2 pts of xsecpts that's closet 
                         // to the Q. Works only when closed=true.
                         // Or, [i,j] where i,j are adjacent indices
                         // of the two pts on xsecpts.
      , "echopts", false // echo pts. Can copy/paste them and do a
                         // MarkPts( joinarr(pts),["chain",true] );
                        // example: 
                        // pts=[ [ [-0.3141, 1.06765, 0.97702]
                        //       , [-2.0401, 1.19704, 0.91436]
                        //       , [-1.1965, 0.27633, -0.2859]
                        //       ]
                        //     , [ [-0.3206, 0.85794, 1.1333]
                        //       , [-2.0515, 0.83005, 1.18784]
                        //       , [-1.2080, -0.0906, -0.01244]
                        //       ]
                        //     ... 
                        //     ]
                        //    ];
      
      //, "debug", false   // set to true to show pt indices
      ];//df_opt
      
	_opt= update( df_opt, opt);

    //echo(_opt = _opt);
    
    function opt(k,nf)=hash(_opt,k,nf);

    rad = opt("rad");
    r   = opt("r");
    rP  = or(opt("rP"), r);
    rR  = or(opt("rR"), r);
    radpts= opt("radpts");
    nseg= radpts?len(radpts):opt("nseg");
    rot = opt("rotate");
    fn  = opt("fn");
    rss = opt("rss");       
    markxsec=opt("markxsec");
    //showobj = opt("showobj");
    
    xsecpts= opt("xsecpts");
    nside = xsecpts?len(xsecpts):fn;
    
    //echo("rP,rR", rP, rR);
    
    a = opt("a");
    P = pqr[0]; 
    Q = pqr[1]; 
    N = N(pqr);    
    R= anglePt(pqr, a, len=rad);  // The end pt of arc
    //echo(a=a);
    //MarkPts( [P,Q,N,R], "pl;ch");
    //newpqr = [P,Q,R];
	
    /*
        +..__
     rP |    ``''--..
        |           | rR
        +-----------+
        P           R
    */
    _rs = opt("rs");
    rs = _rs? _rs: 
          [ for( i= range(nseg+1) ) rP-i*(rP-rR)/nseg  ] ;
         
    _rads= opt("rads");      
    rads= _rads? _rads : 
            [ for( i= range(nseg+1) ) rad  ] ;
                
    


    //================================================
    // Dealing with rotation when xsecpts are used
    //
    Cs = radpts? 
         [ for( p=addz(radpts,0)) 
             p==ORIGIN?Q: anglePt( pqr
                    , a= p.x==0? 90: atan( p.y/p.x )  
                    , len= norm( p ))
                                  
         ]  
         :[ for( i= range(nseg+1) )  // center of each slice      
            anglePt( pqr, i*a/(nseg), len=rads[i] ) 
         ];
    
         
         
    // Create T (tanglnPt_P) and Q2 ( Q after rotation)
    // when xsecpts are given with rotation
    /* 
       Q2 is the new Q after rotation          
                       _-`
         Q-----P-----C`-----
                 _-`
              _-`
            Q2
       T is a pt on the line normal to Q-C-Q2
         
    */      
    tq2s= xsecpts && rot?
          [ for( i= range(nseg+1) )
             let( ai= i*a/(nseg) 
              , C = Cs[i]
              , nextC= Cs[i+1]?Cs[i+1]:anglePt( pqr, a+10 )
              , T = tanglnPt_P( [C,Q,nextC],len=-2 ) //: aQCT=90
              , Q2= anglePt( [T,C,Q],a=90,a2=rot)
              , qcn= [Q,C,N]
              )
              [T,Q2]
         ]:[];
         
//    tq2s= xsecpts && rot?
//          [ for( i= range(nseg+1) )
//             let( ai= i*a/(nseg) 
//              , C = Cs[i]
//              , nextC= Cs[i+1]?Cs[i+1]:anglePt( pqr, a+10 )
//              , T = tanglnPt_P( [C,Q,nextC],len=-2 ) //: aQCT=90
//              , Q2= anglePt( [T,C,Q],a=90,a2=rot)
//              , qcn= [Q,C,N]
//              )
//              [T,Q2]
//         ]:[];
          
    /*
        // Draw the [T,C,Q2] triangle for debugging:
          
        for( i= range(tq2s) ){
            echo( i, tq2s[i] );
            MarkPts( [tq2s[i][0], Cs[i], tq2s[i][1]]
                   , ["chain",true,"label","TCQ","plane",true]);
        }
    */
    //===============================================
          
    pts= [ for( i= range( radpts?nseg:(nseg+1)) )
             let( ai= i*a/(nseg) 
              , C = Cs[i]
              , nextC= Cs[i+1]?Cs[i+1]:anglePt( pqr, a+10 )
              , T = tq2s[i][0] 
              , Q2= tq2s[i][1] 
              , qcn= [Q,C,N]
              )
              xsecpts? // xsecpts : [ [2,1],[3,5] ...]
              (
                rot?
                [ for( j= range(xsecpts) )
                       let( pt = xsecpts[j]  
                          , ptx= onlinePt( [C, Q2], len= -pt.x)
                          , pty= ptx+ uvN([Q2,C,T]) *pt.y
                          //, pty= ptx+ uv(  C, N([Q2,C,T]) )*pt.y
                          )
                       pty
                ]
                :[ for( j= range(xsecpts) )
                       let( pt = xsecpts[j]  
                          , ptx= onlinePt( [C, Q], len= -pt.x)
                          , pty= ptx+ uvN(pqr)*pt.y
                          //, pty= ptx+ uv( Q,N(pqr) )*pt.y
                          )
                       pty
                ]
              )
              :rss?
              [ for( j= range(fn) )
                  anglePt( qcn, a= j*360/fn+ rot, len=rss[i][j])
              ]
              :[ for( j= range(fn) )
                  anglePt( qcn, a= j*360/fn+ rot, len=rs[i])
              ]
         ];
    if(opt("echopts")) 
        echo( lenpts = len(pts), str( "pts to make Arc:<br/>", arrprn(pts,dep=2)) );     
    //MarkPts( get(pts,-1), "r=0.1;trp=0.5");
    
    //=======================================================
    //  markxsec: marking the cross-section points.
    //          
    //   Arc( ... opt=["markxsec",true] ) // mark the first xsec          
    //   Arc( ... opt=["markxsec",3] )    // mark the i=3 xsec       
    //   Arc( ... opt=["markxsec",[2,-1] ] ) // mark i=2 and the last xsec
    //           
    markxsecopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markxsec"
                     , df_subopt= update(
                                  ["r",0.06,"chain"
                                    ,["color","black","r",0.02
                                     , "closed",true]
                                  ], opt("xsecopt"))
                     , com_keys=[]
                     ); 
    //echo(markxsecopt= markxsecopt );          
    module Markxsec(pts, i, markxsecopt)
    {
       _i = fidx( pts, i );
        c = xsecpts?len(xsecpts):fn;
        _opt= getv(markxsecopt,"label")?
               concat( ["label", ["text", range(c*_i, c*(_i+1))]] 
                    , markxsecopt
                    ):markxsecopt;        
        /* ##### DO_NOT use update() here #####
           _opt= update( markxsecopt
                       , ["label", ["text", range(c*_i, c*(_i+1))]] 
                       );
           The markxsec on the root level, Arc(...opt=["markxsec",val]),
           other than taking either true/false or an opt, could also
           take other stuff like val=int or arr_of_int. Since it is
           an arr that looks like a hash, for example, [-3,2,-1],
           it will be added to markxsecopt directly. If we update label:
               ["r",0.06, -3,2,-1, "label",["text", "some"]]
                          ^^^^^^^
           hash() will fail to retrieve label opt. So we put the label
           up front using concat:  
               ["label",["text", "some"], "r",0.06, -3,2,-1 ]
        */
       // echo(_i=_i, c=c, _opt=_opt);
       MarkPts( pts[_i], _opt);
    }
    
    if( isarr(markxsec) ){ 
        for( i= markxsec ) Markxsec(pts,i,markxsecopt);            
    } else if( isint(markxsec)) { Markxsec(pts,i,markxsecopt);
    } else if( markxsec)  
        for( i= range(pts) ) Markxsec(pts,i,markxsecopt);            
        
    //=======================================================
    showxsecopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "showxsec2d"
                     , df_subopt=["chain",["color","black", "closed",true]
                                 ,"label", range(xsecpts)]
                     , com_keys=[]
                     );
    
    //echo( xsecpts = addz(xsecpts,0), showxsecopt=showxsecopt  );
    // Why the following show error coord when is called with
    // "label", ["isxyz", true]
    //echo(showxsecopt=showxsecopt);
    if( xsecpts && showxsecopt ) 
        MarkPts( addz(xsecpts,0), showxsecopt);
    //=======================================================
    frameopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "frame"
                     //, df_subopt= ["label",true]
                     , df_subopt=["chain"
                                  ,["color","green","transp",0.3,"r",0.015]
                                   ,"ball",false
                                 ]
                     , com_keys=[]
                     );

    frameopt2 = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "frame"
                     , df_subopt=["chain"
                                  ,["closed",true
                                   ,"color","green","transp",0.3,"r",0.015]
                                   ,"ball",false
                                 ]
                     , com_keys=[]
                     );
//    echo( frame= opt("frame"));
    //echo( frameopt= frameopt);
//    echo( frameopt2= frameopt2);
    
    
    if( frameopt ){
        t_pts= transpose(pts);
        for( i=range(nside) ){
           MarkPts( t_pts[i], opt("closed")?frameopt2:frameopt);
        }
        for( i=range(pts) ){
           MarkPts( pts[i], frameopt2);
        }
    }
    
    //================================================
    
//    frameopt = subopt1( df_opt= df_opt
//                     , u_opt = opt
//                     , subname = "frame"
//                     , df_subopt=["color","black","r",0.06
//                                 ,"chain",["color","black","r",0.02]
//                                 ,"label",range(nseg)
//                                 ]
//            
//                     , com_keys=[]
//                     );
//    if( frameopt ) MarkPts(Cs,frameopt);             
    
//    echo(pts= str("<br/>",arrprn(pts,dep=2)));
    
//    for( sl= pts ){
//        MarkPts( sl, ["chain",true, "label",range(sl), "markpts",true] );
//    }
//    if(xsecpts){ MarkPts( addz(xsecpts,0)
//            , ["chain",true,"label",range(xsecpts)] ); }
//
        
        faces = faces( opt("closed")?"ring":"chain"
                     , nside=nside, nseg=nseg );
        
        
               function findunwantedfaces(sealed,_rtn=[],_i=0)=
               (
                  let( found = [ for(f=faces) 
                                 if([f[0],f[1]]==sealed) f
                                ] //: first run: [5,6,46,45], [5,6,16,15]   
                       , next = [ get(found,-1)[3], get(found,-1)[2]] // [15,16]
                     )
                  _i< nseg?
                     findunwantedfaces( next,_rtn=concat(_rtn,found),_i=_i+1)
                  :_rtn
               );     
                                 
    if(!opt("hide")){
        

        if( opt("closed")){
           
           //faces= faces("ring", nside=nside, count= nseg);
            
            
           sealed = opt("sealed");
           //echo(sealed = sealed);
           if(sealed ){ //: sealed = [5,6]
                uwfaces = findunwantedfaces(sealed);
                //echo( uwfaces=uwfaces);
                     // [[5,6,46,45],[5,6,16,15],[15,16,26,25]
                     // ,[25,26,36,35], [35, 36, 46, 45]]
                _wfaces = [ for(f=faces) if(idx(uwfaces,f)<0) f] ; 
               
                // Want to add 2 faces: [5,15,25,35,45],[6,16,26,36,46]
                tuwfaces = transpose(uwfaces);
                // transpose uwfaces:
                // [ [5,5,15,25,35], [6,6,16,26,36]
                // , [46,16,26,36,46], [45,15,25,35,45]]
                newfaces= [app( slice(tuwfaces[0],1), tuwfaces[3][0])
                          ,app( slice(tuwfaces[1],1), tuwfaces[2][0])
                          ];
               
    //           echo( uwfaces= uwfaces );
    //           echo( transposed= transpose(uwfaces));
    //           echo( newfaces= newfaces );
               
               //echo( wfaces= arrprn(wfaces,dep=2) );    
               wfaces = concat( _wfaces, newfaces); //concat( _wfaces,
               color( opt("color"), opt("transp") )    
               polyhedron( points= joinarr(  pts ) //pts
                          , faces = wfaces
                          );            
           } 
           else{ //wfaces=faces; }    
            
               
           
           color( opt("color"), opt("transp") )    
           polyhedron( points= joinarr(  pts ) //pts
                      , faces = faces
                      );                   
           }
	} else {
            //faces= faces("chain",nside=nside, count= nseg);      
            //echo(faces=faces);
//            echo(lenfaces = len(faces), faces= arrprn(faces,dep=2));
//            MarkPts( get(pts,-1), "r=0.1;trp=0.5");
//           
            color( opt("color"), opt("transp") )    
            polyhedron( points= joinarr(  pts ) //pts
                      , faces = faces
                      );              
        } 
 
  }//if_showobj   
    

}    



//========================================================
Arrow=["Arrow","pq, ops", "n/a", "geometry"
, "



"];


module Arrow_old( pts, opt=[] )
{
    //echom("Arrow");
  
    //================================================== pt data opt
    df_opt= [ "r", 0.02
            , "nside", 4
            , "cones", false
            , "coneP", false
            , "coneQ", true
            , "rot", 45
            ];
    
    u_opt = popt(opt); 
    //echo( u_opt = u_opt );
    opt = update( df_opt, u_opt );
    function opt(k,df)= hash( opt, k, df );
    r = opt("r");

    //-------------------------------------

        // Automatically decide cone r and len (based on line r)
        // when they are not set by user:
        function auto_cone_r( r )=
            (4*exp(-0.8*r)) *r; // Must be >r, but with certain ratio
                                // that is large ( 3x ) @ small r but
                                // smaller @ large r. So we use a 
                                // exponential function 
        function auto_cone_l(r,L)=
            min(10*exp(-0.5*r)*r, 2*L/5); // 6*r, but can't be too large 
        
        L0  = dist(pts);              // First seg len
        Llast= dist( pts,ij=[-2,-1]); // Last seg len
    
        cone_r = auto_cone_r( r );
        cone_l = auto_cone_l( r, L0 );
        
        df_cones= ["r", cone_r, "len", cone_l ];
        df_coneP= [] ;
        df_coneQ= ["len", auto_cone_l( r, Llast ) ]; 
        
    //================================================ cone opt
        
    function subopt(sub_dfs)=
           getsubops( 
                  u_ops= u_opt  
                , df_ops = df_opt
                , com_keys= [] //"transp","color"]
                , sub_dfs= sub_dfs
                );
    coneP_opt = subopt( ["cones", df_cones, "coneP", df_coneP] );
    coneQ_opt = subopt( ["cones", df_cones, "coneQ", df_coneQ] );
    
    //echo( coneP_opt= coneP_opt );
    //echo( coneQ_opt= coneQ_opt );
    
    /*================================================ 
      Insert A, B for the cone(s):
    
     P------Q-------R------S
         .             .
      _-`|             |`-_    
     P---A--Q-------R--B---S
      `-_|             |_-`
         `             ` 
    */
    
    pts_p_handled = coneP_opt?
                    let( _conelen=hash( coneP_opt, "len")
                       , conelen= _conelen>=L0? L0*0.9: _conelen
                       )
                    insert( pts, onlinePt(pts, conelen), 1 )
                    : pts;
                    
    pts_q_handled = coneQ_opt?
                    let( _conelen=hash( coneQ_opt, "len")
                       , conelen= _conelen>=L0? L0*0.9: _conelen
                       )
                    insert( pts_p_handled
                          , onlinePt( get(pts_p_handled,[-1,-2]), conelen)
                          , -1 
                          )
                    : pts;
                    
    pts_final= pts_q_handled;
    
    //echo( len=len(pts_final), pts_final= pts_final );
    /*================================================         
       rs:  let c = cone_r
       
              len     len      len    
                 .             
              _-`|            
             P---A----Q--R--------S
              `-_|             
                 `
         rs= [0,[c,r],r, r,       r ]                          ` 
                               .
                               |`-_    
             P--------Q--R-----B---S
                               |_-`
                               '
         rs= [r      ,r, r, [r,c],0 ]

                 .             .
              _-`|             |`-_    
             P---A----Q--R-----B---S
              `-_|             |_-`
                 `             ` 
         rs= [0,[c,r],r, r,  [c,r],0]
     
    */
    n = len( pts_final );
    rs_ln = repeat( [r], n-( coneP_opt? ( coneQ_opt? 3:2)
                           : (coneQ_opt?2:0)   
                           ));
    rs = concat( coneP_opt? [ 0, [ hash(coneP_opt,"r"), r]]:[]
               , rs_ln   
               , coneQ_opt? [ [ r, hash(coneQ_opt,"r")], 0 ]:[]
               );
    //echo(rs = rs );           
    //================================================= 
    //Line (pts_final, r=rs, opt=opt );
               
    SimpleChain( simpleChainData( pts_final, r=rs, opt=opt )
               , shape="cone", opt=opt ); 
    
    //chdata=simpleChainData( pts_final, r=rs, opt=opt );
    //SimpleChain( chdata, shape="cone", opt=opt ); 
    
    //    The following 2 approaches are good, too, as of 2015.8.26:
    //
    //    chdata=chaindata( pts_final, r=rs, opt=opt );
    //    echo( chdata= arrprn(chdata));
    //    color( hash(opt,"color"), hash(opt, "transp"))
    //    Chain( chdata); //pts_final, r=0.1, rs=rs, opt=opt );  
    //
    //    chdata=simpleChainData( pts_final, r=rs, opt=opt );
    //    //echo( chdata= arrprn(chdata));
    //    cuts = hash(chdata, "cuts");
    //    pts = joinarr(cuts);
    //    faces = faces( "cone", nside=len( cuts[1]), nseg= len(cuts)-1);
    //    color( hash(opt,"color"), hash(opt, "transp"))
    //    polyhedron( points= pts, faces=faces );
    
}//_Arrow()    


module Arrow( pts, r, color, Head, $fn, opt=[] )
{
  // 2018.12.14:
  //   [1] Modify Arrow() to make it take pts: Arrow(pts) and the arrow head
  //       is placed on the end of pts
  //   [2] Remove the len arg
  //
  // This is a simplifier version of previous Arrow(), dealing with only
  // one-side arrow and use polyhedron. The old one is renamed to Arrow2()
  // 
  //echom("Arrow");
  //echo(arrow_opt = opt);
  r    = arg(r,opt,"r", SCALE/5); 
  $fn  = arg($fn,opt,"$fn", 4); 
  color= colorArg(color,opt); 
  P = pts[ len(pts)-2 ];
  Q = pts[ len(pts)-1 ];
          
  pqlen= norm(P-Q); //dist(P,Q);
   
    // Automatically decide cone's r and len (based on line r)
    // when they are not set by user:
    function auto_cone_r( r )=
        (4*exp(-0.8*r)) *r; // Must be >r, but with certain ratio
                            // that is large ( 3x ) @ small r but
                            // smaller @ large r. So we use a 
                            // exponential function 
    function auto_cone_l(r,L)=
        min(10*exp(-0.5*r)*r, 2*L/5); // 6*r, but can't be too large             
        
    cone_r = auto_cone_r( r );    
    df_h_len = auto_cone_l(r, pqlen);
    
    Head = subprop( sub= Head==undef? hash(opt, "Head", true) :Head
                  , opt=opt
                  , subname="Head"  
                  , dfsub=["r",cone_r, "len", df_h_len, "color", color, "nside", $fn]
                  , dfPropName="len"
                  , dfPropType="num"
                  , debug=0 );

    //echo(opt = opt, Head= Head );  
        
    if(Head)
    {
    
      conelen= min( hash( Head, "len"), pqlen*0.9);
      headBaseCenter = onlinePt( [Q,P], conelen); 
    
      Rf = N([Q,P,randPt()]); 
      
      // Rewrite to use new circlePts (2018.8.6)
            
      pts_cut_0 = circlePts( [ B([2*Q-P,P,Rf]), P, N([2*Q-P,P,Rf])]
                           , rad=r, n=$fn);
      pts_cut_1 = circlePts( [ B([2*Q-P,headBaseCenter,Rf])
                             , headBaseCenter
                             , N([2*Q-P,headBaseCenter,Rf])
                             ]
                           , rad=r, n=$fn);
      pts_cut_2 = circlePts( [ B( [2*Q-P,headBaseCenter,Rf] )
                             , headBaseCenter
                             , N( [2*Q-P,headBaseCenter,Rf] )
                             ]        
                           , rad= hash(Head,"r"), n=$fn);
      //pts_cut_0 = circlePts( [2*Q-P,P,Rf], rad=r, n=$fn);
      //pts_cut_1 = circlePts( [2*Q-P,headBaseCenter,Rf], rad=r, n=$fn);
      //pts_cut_2 = circlePts( [2*Q-P,headBaseCenter,Rf], rad= hash(Head,"r"), n=$fn);
    
      arrowPts = concat( pts_cut_0  // pts around P
                       , pts_cut_1  // pts centering head base
                       , pts_cut_2  // pts around head base
                       , [Q]        // head tip
                       );
                     
      faces = faces(shape="cone", nside=$fn, nseg=3 );               
      Color(color)
      polyhedron( points = arrowPts, faces= faces);
    }
    else 
    {
      Line(pts, $fn=$fn, r=r, color=color, opt=opt);
    }  
       
    if(len(pts)>2) Line( slice(pts, 0,-1), $fn=$fn, r=r, color=color, opt=opt);     
    
}//_Arrow()    
//module Arrow( pq, r, color, len, Head, $fn, opt=[] )
//{
//  // This is a simplifier version of previous Arrow(), dealing with only
//  // one-side arrow and use polyhedron. The old one is renamed to Arrow2()
//  // 
//  //echom("Arrow");
//  //echo(arrow_opt = opt);
//  r    = arg(r,opt,"r", SCALE/5); 
//  $fn  = arg($fn,opt,"$fn", 4); 
//  color= colorArg(color,opt); 
//  len  = arg(len, opt, "len");
//  P = pq[0];
//  Q = len?( isnum?onlinePt( pq, len=len )
//                 :onlinePt( pq, ratio=len[0] )
//          ):pq[1];
//          
//  pqlen= dist(P,Q);
//   
//    // Automatically decide cone's r and len (based on line r)
//    // when they are not set by user:
//    function auto_cone_r( r )=
//        (4*exp(-0.8*r)) *r; // Must be >r, but with certain ratio
//                            // that is large ( 3x ) @ small r but
//                            // smaller @ large r. So we use a 
//                            // exponential function 
//    function auto_cone_l(r,L)=
//        min(10*exp(-0.5*r)*r, 2*L/5); // 6*r, but can't be too large             
//        
//    cone_r = auto_cone_r( r );    
//    df_h_len = auto_cone_l(r, pqlen);
//    
//    Head = subprop( sub= Head==undef? hash(opt, "Head", true) :Head
//                  , opt=opt
//                  , subname="Head"  
//                  , dfsub=["r",cone_r, "len", df_h_len, "color", color, "nside", $fn]
//                  , dfPropName="len"
//                  , dfPropType="num"
//                  , debug=0 );
//
//    //echo(opt = opt, Head= Head );
//  
//        
//    if(Head)
//    {
//    
//      conelen= min( hash( Head, "len"), pqlen*0.9);
//      headBaseCenter = onlinePt( [Q,P], conelen); 
//    
//      Rf = N([Q,P,randPt()]); 
//      
//      // Rewrite to use new circlePts (2018.8.6)
//            
//      pts_cut_0 = circlePts( [ B([2*Q-P,P,Rf]), P, N([2*Q-P,P,Rf])]
//                           , rad=r, n=$fn);
//      pts_cut_1 = circlePts( [ B([2*Q-P,headBaseCenter,Rf])
//                             , headBaseCenter
//                             , N([2*Q-P,headBaseCenter,Rf])
//                             ]
//                           , rad=r, n=$fn);
//      pts_cut_2 = circlePts( [ B( [2*Q-P,headBaseCenter,Rf] )
//                             , headBaseCenter
//                             , N( [2*Q-P,headBaseCenter,Rf] )
//                             ]        
//                           , rad= hash(Head,"r"), n=$fn);
//      //pts_cut_0 = circlePts( [2*Q-P,P,Rf], rad=r, n=$fn);
//      //pts_cut_1 = circlePts( [2*Q-P,headBaseCenter,Rf], rad=r, n=$fn);
//      //pts_cut_2 = circlePts( [2*Q-P,headBaseCenter,Rf], rad= hash(Head,"r"), n=$fn);
//    
//      arrowPts = concat( pts_cut_0  // pts around P
//                       , pts_cut_1  // pts centering head base
//                       , pts_cut_2  // pts around head base
//                       , [Q]        // head tip
//                       );
//                     
//      faces = faces(shape="cone", nside=$fn, nseg=3 );               
//      Color(color)
//      polyhedron( points = arrowPts, faces= faces);
//    }
//    else 
//    {
//      Line([P,Q], $fn=$fn, opt=opt);
//    }  
//       
//         
//    
//}//_Arrow()    
//
module Arrow2( pts, r, color, Head=true, Tail, $fn, opt=[] )
{
  //echom("Arrow");
  
  r    = arg(r,opt,"r", SCALE/5); 
  $fn  = arg($fn,opt,"$fn", 4); 
  color  = colorArg(color,opt); 
  //echo(r=r, $fn=$fn, color=color);
  opt = update(opt, ["nside", $fn] );
  
    //================================================== pt data opt
    //df_opt= [ "r", 0.02
            //, "nside", 4
            //, "cones", false
            //, "coneP", false
            //, "coneQ", true
            //, "rot", 45
            //];
    //
    //u_opt = popt(opt); 
    //echo( u_opt = u_opt );
    //opt = update( df_opt, u_opt );
    //function opt(k,df)= hash( opt, k, df );
    //r = opt("r");

    //-------------------------------------

        // Automatically decide cone r and len (based on line r)
        // when they are not set by user:
        function auto_cone_r( r )=
            (4*exp(-0.8*r)) *r; // Must be >r, but with certain ratio
                                // that is large ( 3x ) @ small r but
                                // smaller @ large r. So we use a 
                                // exponential function 
        function auto_cone_l(r,L)=
            min(10*exp(-0.5*r)*r, 2*L/5); // 6*r, but can't be too large 
        
        L0  = dist(pts);              // First seg len
        Llast= dist( pts,ij=[-2,-1]); // Last seg len
    
        cone_r = auto_cone_r( r );
        cone_l = auto_cone_l( r, L0 );
        
        //df_cones= ["r", cone_r, "len", cone_l ];
        //df_coneP= [] ;
        //df_coneQ= ["len", auto_cone_l( r, Llast ) ]; 
        //
    //================================================ cone opt
    df_h_len = auto_cone_l(r, Llast);
    df_t_len = auto_cone_l(r, L0);
    //echo(L0=L0, Llast = Llast, df_h_len=df_h_len, df_t_len=df_t_len);
    
    Head = subprop( sub=Head, opt=opt, subname="Head"  // Head: arrow head in the last pt
                  , dfsub=["r",cone_r, "len", df_h_len, "color", color, "nside", $fn]
                  , dfPropName="len"
                  , dfPropType="num"
                  , debug=0 );
                  
    Tail = subprop( sub=Tail, opt=opt, subname="Tail"  // Tail: arrow tail in the first pt
                  , dfsub=["r",cone_r
                          , "len", df_t_len
                          , "color", color
                          , "nside", $fn
                          ]
                  , dfPropName="len"
                  , dfPropType="num"
                  , debug=0 );
    //Tail = false;
                        
//    function subopt(sub_dfs)=
//           getsubops( 
//                  u_ops= u_opt  
//                , df_ops = df_opt
//                , com_keys= [] //"transp","color"]
//                , sub_dfs= sub_dfs
//                );
//    coneP_opt = subopt( ["cones", df_cones, "coneP", df_coneP] );
//    coneQ_opt = subopt( ["cones", df_cones, "coneQ", df_coneQ] );
    
    //echo( coneP_opt= coneP_opt );
    //echo( coneQ_opt= coneQ_opt );
    
    /*================================================ 
      Insert A, B for the cone(s):
    
     Tail               Head 
     P------Q-------R------S
         .             .
      _-`|             |`-_    
     P---A--Q-------R--B---S
      `-_|             |_-`
         `             ` 
    */
    coneP_opt = Tail;
    coneQ_opt = Head; 
    //echo(Head = Head);
    //echo(Tail = Tail);
//    pts_p_handled = coneP_opt?
//                    let( _conelen=hash( coneP_opt, "len")
//                       , conelen= _conelen>=L0? L0*0.9: _conelen
//                       )
//                    insert( pts, onlinePt(pts, conelen), 1 )
//                    : pts;
//                    
//    pts_q_handled = coneQ_opt?
//                    let( _conelen=hash( coneQ_opt, "len")
//                       , conelen= _conelen>=L0? L0*0.9: _conelen
//                       )
//                    insert( pts_p_handled
//                          , onlinePt( get(pts_p_handled,[-1,-2]), conelen)
//                          , -1 
//                          )
//                    : pts;
    pts_w_tail = Tail?
                let( _conelen=hash( Tail, "len")
                   , conelen= _conelen>=L0? L0*0.9: _conelen
                   )
                insert( pts, onlinePt(pts, conelen), 1 )
                : pts;
                    
    pts_w_head = Head?
                let( _conelen=hash( Head, "len")
                   //, conelen= _conelen>=L0? L0*0.9: _conelen
                   , conelen= min( _conelen, dist(pts_w_tail, ij=[-2,-1])*0.9) 
                   //_conelen>=L0? L0*0.9: _conelen
                   )
                insert( pts_w_tail
                      , onlinePt( get(pts_w_tail,[-1,-2]), conelen)
                      , -1 
                      )
                : pts_w_tail;
    //echo(pts=pts);                
    //echo(pts_w_tail=pts_w_tail);                
    //echo(pts_w_head=pts_w_head);                
    pts_final= pts_w_head; //pts_q_handled;
    
    //echo( len=len(pts_final), pts_final= pts_final );
    /*================================================         
       rs:  let c = cone_r
       
              len     len      len    
                 .             
              _-`|            
             P---A----Q--R--------S
              `-_|             
                 `
         rs= [0,[c,r],r, r,       r ]                          ` 
                               .
                               |`-_    
             P--------Q--R-----B---S
                               |_-`
                               '
         rs= [r      ,r, r, [r,c],0 ]

                 .             .
              _-`|             |`-_    
             P---A----Q--R-----B---S
              `-_|             |_-`
                 `             ` 
         rs= [0,[c,r],r, r,  [c,r],0]
     
    */
    n = len( pts_final );
    //rs_ln = repeat( [r], n-( coneP_opt? ( coneQ_opt? 3:2)
                           //: (coneQ_opt?2:0)   
                           //));
    //rs = concat( coneP_opt? [ 0, [ hash(coneP_opt,"r"), r]]:[]
               //, rs_ln   
               //, coneQ_opt? [ [ r, hash(coneQ_opt,"r")], 0 ]:[]
               //);
    rs_ln = repeat( [r], n-( Tail? ( Head? 4:2)
                           : (Head?2:0)   
                           ));
    rs = concat( Tail? [ 0, [ hash(Tail,"r"), r]]:[]
               , rs_ln   
               , Head? [ [ r, hash(Head,"r")], 0 ]:[]
               );
    
    //echo(n=n);
    //echo(rs = rs );           
    //echo(pts_final=_red(pts_final));
    //echo(Head=Head);
    //echo(Tail=Tail);
    //================================================= 
    //Line (pts_final, r=rs, opt=opt );
    
    data = simpleChainData( pts_final, r=rs, opt=opt );           
    //echo(data = _fmth(data) );           
    SimpleChain( data
               , shape= Head?( Tail?"spindle":"cone")
                        : "conedown"
               , color=color, opt=opt ); 
    
    //chdata=simpleChainData( pts_final, r=rs, opt=opt );
    //SimpleChain( chdata, shape="cone", opt=opt ); 
    
    //    The following 2 approaches are good, too, as of 2015.8.26:
    //
    //    chdata=chaindata( pts_final, r=rs, opt=opt );
    //    echo( chdata= arrprn(chdata));
    //    color( hash(opt,"color"), hash(opt, "transp"))
    //    Chain( chdata); //pts_final, r=0.1, rs=rs, opt=opt );  
    //
    //    chdata=simpleChainData( pts_final, r=rs, opt=opt );
    //    //echo( chdata= arrprn(chdata));
    //    cuts = hash(chdata, "cuts");
    //    pts = joinarr(cuts);
    //    faces = faces( "cone", nside=len( cuts[1]), nseg= len(cuts)-1);
    //    color( hash(opt,"color"), hash(opt, "transp"))
    //    polyhedron( points= pts, faces=faces );
    
}//_Arrow()    



module ArcArrow( pqr, rad=2, n=8, opt=[]){
    
    opt = popt(opt);
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
    J = midPt([P,Q]);

    dPQ= dist(pqr);
    rad = rad<=dPQ/2? dPQ*1.5: rad;
    
    M = anglePt( [ P,J,R], a=90, len= shortside( dPQ/2,rad));
    aM = angle([P,M,Q]); 
    a = 180-aM/2-90;
    
    //Mark90( [M,J,pqr[0]],"ch");
    //echo(rad=rad, a=a, aM=aM, aPQM = angle( [pqr[0],pqr[1],M]), apqr = angle(pqr), dPM = dist(pqr[0],M)
    //    , dQM = dist( pqr[1],M ));
    //MarkPt(M,"l=M");
    newpqr = [pqr[0],M,pqr[1]];
    pts = arcPts( newpqr, rad=rad, a=aM, n=n);
    Arrow2(pts, opt=opt); 
    
    
    //echo(opt=opt);
    chain = hash(opt,"chain");
    if(chain) { 
        chainext = hash(opt,"chainext", 0);
        SimpleChain( [ chainext?onlinePt( [P,M], len= -chainext):P
                     , M
                     , chainext?onlinePt( [Q,M], len= -chainext):Q
                     ], opt=opt); 
    }
}

////========================================================
//Block=[ "Block","pq,ops", "n/a", "3D, shape",
// " Given a 2-pointer pq, make a block that has its one edge lies on
//;; or parallel to the pq line. The block size is determined by pq,
//;; width and depth. It can rotate by an angle defined by ''rotate''
//;; about the pq-line. Default ops:
//;;
//;;  [ ''color'' , ''yellow''
//;;  , ''transp'', 1
//;;  , ''rotate'', 0           // rotate angle alout the line
//;;  , ''shift'' , ''corner''  //corner|corner2|mid1|mid2|center
//;;  , ''width'' , 1
//;;  , ''depth'' , 6
//;;  ]
//;;
//;; The shift is a translation decides where on the block it attaches
//;; itself to pq. It can be any [x,y,z], or one of the following strings:
//;;
//;;    corner       mid1          mid2         center
//;;    q-----+      +-----+      +-----+      +-----+
//;;   / :     :    p :     :    / q     :    / :     :
//;;  p   +-----+  +   +-----+  +   +-----+  + p +-----+
//;;   : /     /    : q     /    p /     /    : /     /
//;;    +-----+      +-----+      +-----+      +-----+
//;;
//;;    corner2
//;;    +-----+
//;;   / :     :
//;;  +   q-----+
//;;   : /     /
//;;    p-----+
//"];
//
//module Block(pq,ops){
//	Line( pq, update(ops, ["style", "block"]) );
//}
//
//module Block_test(ops){ doctest(Block, ops=ops);}
//
//module Block_demo_1()
//{
//	echom("Block_demo_1");
//	pq= randPts(2);
//	//pq = [ [2,1,3], [4,3,2]];
//	MarkPts(pq);
//	Block(pq);
//}
//
//module Block_demo_2()
//{
//	echom("Block_demo_2");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]], ["width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], update(ops, ["color","red"]));//
//	Block([[x-w,0,0], [x-w,0,z]], update(ops, ["color","green"]) );
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//	Block( [ [x, 0, z], [0, 0, z+1]], update(ops, ["color","blue", "rotate",360*$t] ) );
//	
//}
//
//
//module Block_demo_3()
//{
//	echom("Block_demo_3");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]]
//					   , [ "width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift","corner2" // <====
//					  ]) );
//
//
//}
//
//module Block_demo_4()
//{
//	echom("Block_demo_4");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]]
//					   ,[ "width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift","mid1" // <====
//					  ]) );
//
//}
//
//module Block_demo_5()
//{
//	echom("Block_demo_5");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]]
//					   , ["width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift","mid2" // <====
//					  ]) );
//}
//
//
//module Block_demo_6()
//{
//	echom("Block_demo_6");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]],[ "width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift","center" // <====
//					  ]) );
//}
//
//
//module Block_demo_7()
//{
//	echom("Block_demo_7");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]]
//					   , ["width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift",[2*w, 0,w] // <====
//					  ]) );
//}
//
//
//module Block_demo()
//{
//	//Block_demo_1();
//	//Block_demo_2();
//	Block_demo_3();
//	//Block_demo_4();
//	//Block_demo_5();
//	//Block_demo_6();
//	//Block_demo_7();
//}
////Block_test( ["mode",22] );
////Block_demo();
////doc(Block);
//
//



//// shape========================================================

//. c


function chaindata( 
        
    pts
    , nside 
    , nsidelast // 2015.7.23: Different nside than the 
                // beginning face, allowing for loft.
           /*  The loft can be done in :
           
               1) Set the 1st cut with either nside or cut0
               2) Set the last cut w/ either nsidelast or cutlast
               
               Note that in these conditions :
               -- twist will NOT twist the cuts, but will rotate 
                    the cutlast for loft
               
                  .------.
                .'        `.
              .`     .      `.
               `.          .'
                 `.______-`

           */ 
    , loftpower // Power of lofting. The real r at pt i, ri, changes
                // its value from r_head to r_tail over i's. The larger 
                // the loftpower is, the faster the ri approaches r_tail. 
                // If =0, the change of r follows a straight line. If 
                // between -1~0, r will remains unchanged until the end 
                // of pts before it starts lofting. If <-1, r could shrink 
                // first before start lofting. 
                 
    , r    // radius at the head (first cut)
    , rlast   // radius at the tail (last cut)
    //, rs     // array of r to be added on top of r 
             //   If rs < pts, repeat rs  
             // Note that this array could contain arrays:
             //   rs = [ 3, [3,3.5], 2,2 ] 
             // indicating multiple r's at that pt
    , closed // If the chain is meant to be closed.
             // The starting and ending crosecs will
             // be modified. 
    //-------------------------------------------------
    , twist // Angle of twist of the last crosec 
            // comparing to the first crosec
    , rot   // The entire chain rotate about its bone
            // away from the seed plane, which
            // is [ p0, p1, Rf ]


    //-------------------------------------------------
    // The following are used to generate data using 
    // randchain( data,n,a,a2... ) when data is a pts.
    // They are ignored if data is (2) or (3) 

    //         , n      //=3, 
    //         , a      //=[-360,360]
    //         , a2     //=[-360,360]
    //         , seglen //= [0.5,3] // rand len btw two values
    //         , lens   //=undef
    //         , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
    //                         //   , "clos
    , seed   //=undef // base pqr
    , Rf // Reference pt to define the starting
         // seed plane from where the crosec 
         // starts 1st pt. Seed Plane=[p0,p1,Rf]
            
    //-------------------------------------------------
    , cut0 // Define cross-section. This would become
             // the first cross-section (i.e., chain head)
             // When defined, the r,rlast, and rs are ignored
    , cutlast // Define cross-section. This would become
             // the last cross-section (i.e., chain tail)
             // When defined, the r,rlast, and rs are ignored
             // Define both cut0 and cutlast to loft.
             
    
    , bevel0

    , tilt    
    , tiltlast   /* [rot,tilt,posttilt_rot]
                 tilt angle at first face( tilt) or last face
                 
                 x is a number btw 0~360, representing
                 pos on a clock face. Line XO will be the
                 axis about which the pl_PQRS will rot
                 and tilt by angle a to create the tilt.
                 
                            /  
                           / Z
                       /  / /    /
                      / _Q_/    /
                     _-`  /`-_ /
                    R    O----P
                     `-_   _-`
                        `S`
                        
                 Let Z the last pt of bone. 
                 
                 Default x is 0, which means the tilt is
                 to tilt toward the first pt of xsec. i.e,
                 aPOZ = a. 
                 
                 If x is 90, toward the Q-direction on
                 the above graph, i.e., aQOZ=a 
                 
                 htilt could be a number a, in that case
                 default OQ is the axis. i.e., x=0       
              */
    , shape // default: chain
    //===============================================
    , _pl90at0 // The pl90 ( pts on the first 90-deg plane.
               // It is kept over the entire length of chain
               // till the last for the possible need of loft
    , opt=[] 
    , _rs=[]    
    , _cuts=[]
    ,_i=0
    , _debug)=
(   
   _i==0?
   
   let(//###################################### i = 0 
   
      //------------------------------
      //    param initiation
        
        opt = popt(opt) // handle sopt
      , pts = ispt(pts[0])? pts 
              : haskey( pts, "pts")? hash(pts, "pts")
              : or(seed, hash(opt,"seed"))? seed
              : pqr()
      
    
      , rot= und(rot, hash(opt,"rot",0 )) 

      //, xpts= or(xpts, hash(opt,"xpts",undef))
      , cut0= or(cut0, hash(opt,"cut0", undef))
      
      , Rf0= or(Rf, hash(opt,"Rf", undef))       
      , twist= und(twist, hash(opt,"twist",0))
      , closed= len(pts)==2?false
                : und(closed, hash(opt,"closed",false))
                
      //, _hbevel= or( hbevel, hash(opt,"hbevel"))
      //, hbevel= _hbevel==true? ["dir",-1, "r",0.5, "n", 
      
      , _tilt= und( tilt, hash(opt,"tilt"))
      , _tiltlast= und( tiltlast, hash(opt,"tiltlast"))
      , tilt = isnum(_tilt)?[0,_tilt]:_tilt
      , tiltlast = isnum(_tiltlast)?[0,_tiltlast]:_tiltlast
      
      , _nside= or( nside, hash(opt,"nside",4))
      , nside = cut0? len(cut0):_nside
      , nsidelast= und( nsidelast, hash(opt,"nsidelast"))
      , loftpower= und( loftpower,hash(opt,"loftpower",0))
 
      //
      // --- Set r. One of the most critical steps ---
      //
      // This will convert r to an array rs. All r's required
      // through the entire pts are calced here. 
      //
      // What could alter r: 
      //   rlast ==> create a tapering effect 
      //   distRatios ==> if rs[0]!= rs[-1], then distRatios
      //                  will alter rs[_i]
      //   nsidelast ==> create a lofting effect 
      //   loftpower ==> if nsidelast, a loftpower will alter r[_i]
      // 
      // rlast is disabled WHEN an array is given to r, 'cos 
      // it doesn't make sense to set rlast and r to array at the same time
      // 
      // At this moment, we make loft a "post-processing" step, means
      // re-calc everything at the last pt for lofting. This is not idea
      // but lets not complicate things for now. 
      // 
      , L = len(pts) 
      , rng= [0:L-1] 
      
      , r  = und(r, hash(opt,"r", 0.5))
      , rlast  = isarr(r)? undef: und(rlast,hash(opt,"rlast"))
      , distRatios = distRatios(pts)
      , rs= cut0? undef
            :rlast?  
               let( d = rlast-r )
               [ for( i=rng ) r+ d*distRatios[i] ] 
            :isarr(r)?
             (len(r)< L?  // If rs < pts, repeat rs
               repeat(r, ceil( L /len(r)))
               :r
             ): repeat( [r], L )  
                   
      //, rs= rlast? [ for( i=rng ) distRatios[i]* rs_fulllen[i] ]
      //           : rs_fulllen
      //, rs= rs_fulllen      
      //=========================================
      
      , coln_101= iscoline( get(pts,[1,0,1])) //p_101(pts))   
      , coln012 = iscoline( p012(pts))   
      , coln123 = iscoline( p123(pts))   
      , _Rf = Rf0?Rf0
             : len(pts)==2? randOfflinePt( pts )
             : closed ? 
                ( coln012 ?   //   x . . .0---1
                              //  / 
                  ( coln123?  //   x . .0---1---2
                              //  /
                    randOfflinePt(p01(pts))
                    :pts[3]   //    x . . 0---1
                  )           //   /          | 
                : pts[2]      //   x . . 0
                ) 
             : 
               ( coln012?                     //   0---1---2
                  ( coln123 || len(pts)==3?   //   0--1--2--3
                    randOfflinePt(p01(pts))
                    : pts[3]                  //   0--1--2
                  )                           //        /   
               : pts[2]       //   0--1 
               )              //     /
               
      , Rf = rot? rotPt( _Rf, p01(pts), rot):_Rf 

      , newr= rs[0]
//      , _newr = isnum(rlast)? (_i*(rlast-r)/(len(pts)-1)+r)
//                  : r
//         
//      , newr = rs ? 
//               (  // rs[_i] could be array !!
//                 rs[0] && isarr( rs[0] ) ? 
//                   [ for(_r=rs[0]) _r+_newr]
//                 :isnum(rs[0])? (_newr + rs[0] )
//                 :_newr
//              )     
//              : _newr
       
      , preP = onlinePt( p01(pts), -1)
      
      //====================================================             
      // Data pts are created on pl90, then projected to pl
      // to form cut:
                   
      // pl90 is the actual data pts created on a plane that is
      // 90d to the line p01. They will soon be projected to pl
      // if either tilt is defined or closed is true            
      , pl90= ispts(cut0) || ispts( cut0,2) ? 
               let( // Translate pts in given cut0 to the pl90
                    //preP= onlinePt( p01(pts), -1)
                   R90 = anglePt( [ preP, pts[0], Rf], a=90 )
                  , uvX = uv( pts[0], R90 )
                  , uvY = uv( pts[0], N( [preP,pts[0],R90] ) )        
                  ) 
                  [ for(P=cut0) pts[0]+ uvX*P.x + uvY*P.y ]
                  
             // Rewrite to use new circlePts (2018.8.6)
            
             : circlePts( [ B([preP, pts[0], Rf])
                          , pts[0]
                          , N([preP, pts[0], Rf])
                          ]
                        , n=nside
                        , rad= isarr(newr)?newr[0]:newr        
                        )
             //: circlePts( [preP, pts[0], Rf], n=nside
                        //, rad= isarr(newr)?newr[0]:newr        
                        //)
                  
      , pl90_xtra= isarr(newr)?              
                 // Create xtra pl on the same _i 
                 arcPts( [preP, pts[0], Rf]  
                      , a=90                
                      , a2= 360*(nside-1)/nside
                      ,n=nside, rad= newr[1]
                      )
                 :undef
      
      // pl is the actual plane onto which the pts placed 
      // on pl90 will be projected to. Since it is just a
      // pl to hold data, number and order of pts that form
      // this pl are not important.  
      // As a result, the posttilt_rot has no effect
      , pl = closed?
               ( let( pldata = planeDataAt(pts,0
                                      ,closed=true) )
                  hash( pldata,"pl")
               )
              :tilt[1]?
               ( let( pldata = planeDataAt( //pts,0, Rf=Rf 
                                        [pts[0],pts[1],Rf],0, Rf=Rf
                                      , rot= tilt[0]
                                      , tilt= tilt[1]==90?undef:tilt[1]
                                      //, posttilt_rot= tilt[2]
                                      //tilt?tilt[2]:undef
                                      ,closed=false) )
                  hash( pldata,"pl")
               ) 
               : pl90  
               
      // cut contains the actual data pts (on the plane pl)         
      , cut= closed|| tilt[1] ? 
                projPts( pl90, pl, along=p01(pts) ) 
                : pl90
      , cut_xtra= isarr(newr)?
                  (closed || tilt[1]? 
                    projPts( pl90_xtra, pl, along=p01(pts) ) 
                    : pl90_xtra        
                  ):undef  
      
      // _cuts will be carried to next pt 
      , _cuts= isarr(newr)? [ cut, cut_xtra]: [cut]
    
       , _debug=str(_debug, "<hr/>"
          ,_red("<br/>, <b>chaindata debug, _i</b>= "), _i 
          ,"<br/>, <b>r</b>= ", r
          ,"<br/>, <b>rlast</b>= ", rlast
          //,"<br/>, <b>rs_fulllen</b>= ", rs_fulllen
          ,"<br/>, <b>rs</b>= ", rs
          ,"<br/>, <b>pts</b>= ", pts
          ,"<br/>, <b>nside</b>= ", nside
          ,"<br/>, <b>closed</b>= ", closed
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>preP</b>= ", preP
          ,"<br/>, <b>pl90</b>= ", pl90
          ,"<br/>, <b>pl90_xtra</b>= ", pl90_xtra
          
          ,"<br/>, <b>pl</b>= ", pl
          ,"<br/>, <b>Rf</b>= ", Rf
          ,"<br/>, <b>cut0</b>= ", arrprn(cut0, dep=2)
          ,"<br/>, <b>cut</b>= ", cut
          ,"<br/>, <b>pts_i</b>= ", arrprn(pts[_i],dep=2)
          ,"<br/>, <b>, tilt</b>= ", tilt 
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>_cuts</b>= ", _cuts
//          ,"<br/>, <b>xpts_i</b>= ", arrprn(xpts_i, dep=2)
          //,"<br/>, <b>newrtn</b>= ", arrprn(newrtn, dep=2)
        )//_debug
        
      )// let @ i=0
      //_debug
      chaindata( pts
               , nside=nside
               , nsidelast=nsidelast
               , loftpower=loftpower
               , r=r, rlast=rlast, rs=rs
               , closed=closed
               , twist=twist, rot=rot
               , Rf= Rf //cut0[0]
               , cutlast= cutlast
               , tilt=tilt,tiltlast=tiltlast 
               , _rs = rs              
               , _cuts=_cuts
               , _pl90at0 = pl90
               ,_i=_i+1
               ,_debug=_debug
               )                 
 
   : 0<_i && _i <len(pts)-1?
   
    let( //#################################### 0 < i < last 
   
        dtwi = twist? twist/(len(pts)-1) :0
        ,lastcut = last(_cuts)
        ,boneseg = get( pts, [_i-1,_i] )
        , rs = _rs

        // The pl where previous data will project to
        , pldataAt = planeDataAt( pts,_i)
        , pl_i = hash( pldataAt, "pl")
      
        , newr = rs[_i] 
        
        , cut = projPl( pl1=lastcut
                      , pl2=pl_i
                      , along= boneseg
                      , newr= isarr(newr)?newr[0]:newr
                      , twist=twist
                      )
                      
        , cut_xtra = isarr(newr)?
                        projPl( pl1=lastcut
                          , pl2=pl_i
                          , along= boneseg
                          , newr= newr[1]
                          , twist=twist
                          )
                    : undef 
        
        ,newcuts= isarr(newr)? concat( _cuts,[cut,cut_xtra])
                              : app(_cuts,cut)                   
   
       , _debug=str(_debug, "<hr/>"
          ,_red("<br/>, <b>chaindata debug, _i</b>= "), _i 
//          ,"<br/>, <b>r</b>= ", r
          ,"<br/>, <b>lastcut</b>= ", lastcut
          //,"<br/>, <b>pl_i</b>= ", keys(pldataAt)
          ,"<br/>, <b>pl_i</b>= ", pl_i
          ,"<br/>, <b>rlast</b>= ", rlast
          ,"<br/>, <b>rs</b>= ", rs
          ,"<br/>, <b>pts</b>= ", pts
          ,"<br/>, <b>nside</b>= ", nside
          ,"<br/>, <b>closed</b>= ", closed
          ,"<br/>, <b>newr</b>= ", newr
          
//          ,"<br/>, <b>pl</b>= ", pl
//          ,"<br/>, <b>pl90</b>= ", pl90
//          ,"<br/>, <b>cut90</b>= ", cut90
//          ,"<br/>, <b>cut90_twisted</b>= ", cut90_twisted
//          ,"<br/>, <b>cut90_r_adjusted</b>= ", cut90_r_adjusted
//          ,"<br/>, <b>cut90_r_adjusted_xtra</b>= ", cut90_r_adjusted_xtra
          ,"<br/>, <b>Rf</b>= ", Rf
//          ,"<br/>, <b>lastcut_r_adjusted</b>= ", lastcut_r_adjusted
//          ,"<br/>, <b>cut_projected</b>= ", cut_projected
          ,"<br/>, <b>cut</b>= ", cut
          ,"<br/>, <b>cut_xtra</b>= ", cut_xtra
          ,"<br/>, <b>pts_i</b>= ", arrprn(pts[_i],dep=2)
          ,"<br/>, <b>, tilt</b>= ", tilt 
          ,"<br/>, <b>cut0</b>= ", arrprn(cut0, dep=2)
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>_cuts</b>= ", _cuts
          ,"<br/>, <b>newcuts</b>= ", newcuts
          )
        )// let @ i>0

      //_i==len(pts)-2? _debug:  
      chaindata( pts
               , nside=nside
               , nsidelast=nsidelast
               , loftpower=loftpower
               , r=r, rlast=rlast, rs=rs
               , closed=closed
               , twist=twist, rot=rot
               , Rf= Rf //cut0[0]
               , cutlast= cutlast
               , tiltlast=tiltlast
               , _cuts= newcuts
               , _rs = _rs
               , _pl90at0 = _pl90at0
               ,_i=_i+1
               ,_debug=_debug
               )  
               
//       chaindata( pts, r=r, rlast=rlast, rs=rs
//                , nside=nside, rot=rot, Rf=xsec[0]
//                , tilt=tilt,tiltlast=tiltlast, twist=twist
//                , closed=closed, _xsecs=newxsecs
//                ,_i=_i+1,_debug=_debug)
                
                
      : //_i==len(pts)-1? 
   
     let( //#################################### i = last
          dtwi = twist? twist/(len(pts)-1) :0
        , rs = _rs
        , newr = last(_rs) 

        , lastcut = last(_cuts)
        , boneseg = get(pts, [-2,-1])
        
        , pldata= closed?
                   planeDataAtQ( get3(pts,-2) ) 
                 : tiltlast && tiltlast[1]?
                   planeDataAt( pts,_i,
                        , rot= tiltlast[0] //tiltlast? tiltlast[0]:undef
                        , tilt=tiltlast[1] //tiltlast? tiltlast[1]:undef
                        , closed=closed
                        , Rf= last(_cuts)[0]) 
                   :planeDataAt(pts,_i,a=90)
                   //planeDataAt( pts,_i,
                        //, Rf=Rf
                        //, closed=false
                   //) 
          // pl: where to project pts         
        , pl_i = hash( pldata, "pl")
        
        , cut = isarr(r) && last(r)==0? [last(pts)]
                :projPl( pl1=lastcut
                      , pl2=pl_i
                      , along= boneseg
                      , newr= isarr(newr)?newr[0]:newr
                      , twist=twist
                      )
                      
        , cut_xtra = isarr(newr)?
                        projPl( pl1=lastcut
                          , pl2=pl_i
                          , along= boneseg
                          , newr= newr[1]
                          , twist=twist
                          )
                    : undef 
        
        ,newcuts= isarr(newr)? concat( _cuts,[cut,cut_xtra])
                              : app(_cuts,cut) 
              
//        , cut_r =  projPts( pre_cut, pl
//                           , along=get(pts,[_i-1,_i] )
//                           )
//        ,_cut = isnum(rlast)&&rlast!=r || rs? [ for(p= cut_r)
//                        onlinePt( [ pts[_i], p], isarr(newr)?newr[0]:newr)]
//                   : cut_r 
//        
//        , cut = newr==0 && !cut0?  // cone
//                 [_cut[0]]:_cut
//        
//        , cut_xtra = isarr(newr)?
//                      ( isnum(rlast) || rs? [ for(p= cut_r)
//                        onlinePt( [ pts[_i], p], newr[1])]
//                       : cut_r 
//                      ):undef   
//        
//        ,_newcuts= isarr(newr)? concat( _cuts,[cut,cut_xtra])
//                              : app(_cuts,cut) 
//        , newcuts=is0(r + (rs?rs[0]:0))? // for conedown
//                    replace( _newcuts,0,[pts[0]]) 
//                   : _newcuts 
        /*
          lofting
            
          When nsidelast is set, Lofting is handled AFTER all other
          processes are done. This is not the opitimized way,
          'cos it will do many steps that are not needed for the 
          lofting, so it's a waste of time. However, this is a 
          complicated process and could mess up other process easily, 
          so mixing them up is not a good idea at this moment. 2015.7.25

          Lofting is done by first calc the "paring data" using 
          the first and last crossections(taken from newcuts) with
          getParingData(), converting to two xsecs (called lofthead, 
          lofttail) that have same # of pts.
          
          Then loop thru pts, in each step i, 
          (1) translocate the lofthead to the pl that's 90d to the 
          bone at pts[i] (call it pl90). Lets call it loft_i; 
          (2) Loop thru loft_i, in each step xi, adjust each r (dist 
          between loft_i[xi] and bone) according to rdiffs (r 
          differences) obtained w/ getParingData(). Call the result 
          loft_i_r 
          (3) Project loft_i_r onto the pl at i (pl_i).   
          
        */    
        , cuts = nsidelast>2? // lofting
                  let( 
                       pts1 = newcuts[0] 
                     , lastcut= last(newcuts)
                     , pqr = [ get(pts,-2)
                             , get(pts,-1)
                             , lastcut[0]
                             ] 
                     , pts2 = circlePts( 
                                pqr= [ B(pqr)
                                     , pqr[1]
                                     , N(pqr)
                                     ] 
                                // Rewrite to use new circlePts (2018.8.6)
                                //pqr= [ get(pts,-2)
                                     //, get(pts,-1)
                                     //, lastcut[0]
                                     //] 
                                , rad= dist( lastcut[0], last(pts))
                                , n = nsidelast
                             )
                             
                     , pairdata =  getPairingData(
                                       pts1= pts1
                                      , pts2= pts2
                                      )
                     , lofthead = hash( pairdata, "pts1") // head face
                     , lofttail = hash( pairdata, "pts2") // tail face
                     , angles = // Angles of each pt (in lofthead or 
                                // lofttail) spanning from pt [0]. These
                                // angles are the same for lofthead/lofttail)  
                                hash(pairdata, "angles")
                     , rdiffs = // radius and their differences:
                                // [ [rdiff0,r_head0,r_tail0]
                                // , [rdiff1,r_head1,r_tail1]
                                // ]
                                hash( pairdata, "rdiffs" ) 

                     , lenratio = // For [p0,p1,p2...pn] of pts (the bone),
                                  // ratio of di/d where di= dist(0,i),
                                  // d= dist(0,n), eg: [ 0, 0.3, 0.45,0.78,1]
                            let( lens= [ for(i=[1:len(pts)-1])
                                        dist( get(pts, [i-1,i])) ]
                               , lensum= sum(lens)
                               , accumlens= accum(lens)
                               )
                            concat([0],[ for( d=accumlens) d/lensum ])
                     )
                            
                 [ for( i=range(pts) )
                      i==0 ? lofthead
                      : i== len(pts)-1? lofttail
                      : let( pts_unloft = newcuts[i]
                           , lofthead_at90i = 
                                // this is lofthead relocated to pl 90d to (i-1,i)
                                [for(xi= range(lofthead))
                                    
                                  anglePt( [ pts[i-1], pts[i], pts_unloft[0] ]
                                         , a=90, a2=angles[xi]
                                         , len= rdiffs[xi][1]
                                         )    
                               ]
                            , lofttail_at90i = 
                                // this is lofttail relocated to pl 90d to (i-1,i)
                                [for(xi= range(lofttail))
                                   
                                  anglePt( [ pts[i-1], pts[i], pts_unloft[0] ]
                                         , a=90, a2= angles[xi]
                                         , len= rdiffs[xi][2]
                                         )      
                                ] 
                             , projected_loft_i = 
                                [for(xi= range(lofttail))
                                  let( phead= lofthead_at90i[xi]
                                     , ptail= lofttail_at90i[xi]
                                     , dr = rdiffs[xi][0] // diff of r
                                     , dri_ratioed= // dist of phead~ptail when 
                                                // lofthead and lofttail are
                                                // placed on the same plane.
                                                // It could be <0
                                                dr* lenratio[i] 
                                    , dri_factored =
                                            loftpower>=0?  
                                            ( dri_ratioed
                                             +(dr-dri_ratioed)
                                              *(1-exp(-loftpower*lenratio[i]))
                                            ):                                
                                            ( dri_ratioed
                                              -(dr-dri_ratioed)
                                              *(1-exp(loftpower*lenratio[i]))
                                            )   

                                     , p90=  onlinePt( [phead,pts[i]]
                                                     , len= -dri_factored)
                                     )
                                projPt( p90, p012( pts_unloft ),get(pts,[i-1,i]) )
                                ]
                             ) 
                      projected_loft_i 
                ]
                : newcuts      
                      
                      
//        , newxsecs = rlast==0 && !cut0?  // cone
//                     [_newxsecs[0]]
//                     : _newxsecs  
                    
       , _debug=str(_debug, "<hr/>"
          ,"<b>_i</b>= ", _i 
          ,"<br/>, <b>nside</b>= ", nside
          ,"<br/>, <b>cut</b>= ", cut
          ,"<br/>, <b>closed</b>= ", closed
          ,"<br/>, <b>pl_i</b>= ", pl_i
          ,"<br/>, <b>pldata</b>= ", pldata
          ,"<br/>, <b>tiltlast</b>= ", tiltlast 
          ,"<br/>, <b>newr</b>= ", newr
          ,"<br/>, <b>twist</b>= ", twist
          ,"<br/>, <b>dtwi</b>= ", dtwi
         /// ,"<br/>, <b>last_xpts</b>= ", arrprn(last(_xpts),2)
          ,"<br/>, <b>pts</b>= ", pts
          ,"<br/>, <b>rs</b>= ", rs
          ,"<br/>, <b>cut_xtra</b>= ", cut_xtra
          ,"<br/>, <b>newcuts</b>= ", arrprn(newcuts, dep=2)
          )//_debug      

        )
      //_debug
     
      ["cuts", isarr(r) && r[0]==0? concat( [[pts[0]]], slice( cuts,1))
               :cuts
      //,"newcuts",newcuts
        ,"rs", rs
         ,"Rf", Rf 
         
         ,"__<b>Args</b>__","==>"
         ,"bone", pts
         ,"nside",nside
         ,"nsidelast",nsidelast
         ,"r",r
         ,"rlast", rlast
         ,"tilt", tilt //[tilt,tiltax]
         ,"tiltlast", tiltlast //[tiltlast,tiltlastax]
         ,"closed", closed
         ,"twist", twist
         ,"rot", rot
         ,"cut0", cut0
         ,"cutlast", cutlast

         ,"seed", seed
         //,"loftpower", loftpower 
         ,"__<b>debug</b>__","==>"  
         ,"debug", _debug
         ]    
                
);  // chaindata 


Chain=["Chain","pts,ops", "n/a", "3D, Shape",
 " Given array of pts (pts) and ops, make a chain of line
;; segments to connect points. ops:
;;
;;  [ ''r''     , 0.05
;;  , ''closed'',true
;;  , ''color'' ,false
;;  , ''transp'',false
;;  , ''usespherenode'',true
;;  ]
"];	




module Chain_test(ops){ doctest(Chain,ops=ops);}



//module Chain0( pts, opt=[])
//{
//    echom("Chain0");
//    
//    df_opt = [ "r", 0.008
//             , "nside", 4
//             , "closed",false
//             , "Rf",undef
//             ];
//    opt = update( df_opt, opt );
//
//    chrtn = chainPts0( pts=pts, opt=opt );
//    xsecs = hash(chrtn, "xsecs");
//    
//    //echofh(chrtn);
//    //echo(debug = hash( chrtn, "debug"));
//    
//    //echo( xsecs = arrprn( xsecs ) );
//    
//    Rf0 = hash(chrtn, "Rf0");
//    //echo(Rf0 = Rf0);
//    //MarkPt(Rf0, "r=0.3;trp=0.3" );
//    
//    
//    
//    //for( xs = xsecs ) MarkPts( xs ); //Chain( xs , opt="closed");
//        
//    faces = faces( shape= hash(opt,"closed")? "ring":"chain"
//                 , nside= hash(opt,"nside")
//                 , nseg = len(pts)-1
//                 );
//    //echo( faces = faces );
//    
//    color( hash( opt, "color")
//         , hash( opt, "transp")
//         )    
//    polyhedron( points= joinarr(xsecs)             
//              , faces=faces );
//}
//
//module Chain0_demo(){
//    
//   echom("Chain0_demo"); 
//   pqr= randPts(5); //pqr();
//   //pqr = [[2.3226, -2.58512, -0.910189], [-2.53233, -2.73718, -0.367593], [-1.73259, -0.215232, -2.55716]];
//   echo( pqr = pqr );  
//   Chain0( pqr ); 
//   //MarkPts( pqr, "r=0.2" ); 
//    
//   //plat1rtn = planeDataAt( pqr, 1 );
//   //plat1 = hash(plat1rtn,"pl"); 
//   //echo(plat1=plat1);
//   //MarkPts(plat1, "pl");
//    
//   //MarkPt( [2.65243, -0.343472, 2.42548] );
//    
//   //Chain0( pqr(), ["color","red", "r", 0.05] ); 
//    
//}    
////Chain0_demo();

//========================================
// Ruler: a quick draft for used in FontWindthCheck [scadx_obj2]. 2015.8.29
//
module Ruler( pqr /* R decides which side the scale ticks and labels on */
            //, scaletype  /* len, ratio ( future: log )*/
            , startvalue = 0
            , majorIntv =1 /* major interval */
            //, minorIntvPerMajor
            //, rMajor 
            //, rMinor
            //, line
            , showindex = false /*if true, show indices instead of value*/
            , opt=[] 
            )
{   echom("Ruler");
    
    pqr = len(pqr)==2?app(pqr,randPt()):pqr;
    
    df_opt= 
        [ "r"     , 0.01  // rad of line
        , "color" , "darkcyan"  
        , "transp", 1    
        
        // Subunit settings:
        // Set to true to use default ops. false to hide. hash
        // to customize. 
        , "ticks"  , true // false or hash
        , "majorticks", true 
        , "minorticks", true
        ];  
   
    df_ticks= ["len", 0.5];
    df_majorticks= [ "len", 0.5 
                   ];
    
    df_label=
        [ "size",5
        , "scale",0.05
        , "prefix",""
        , "suffix",""
        , "conv", undef
        , "decimal", undef
        , "maxdecimal", 2
        , "text", undef // set to string replace df value 
        , "color", undef
        , "transp", 1
        ];    
    _opt = update( df_opt, opt); // df_ops updated w/ user_ops
    function opt(k,df) = hash(_opt,k,df);
    r = opt("r");

    //==========================================================
    function _getsubops_( sub_dfs )=
        getsubops( opt, df_ops= df_opt
        , com_keys= ["r", "color", "transp"]
        , sub_dfs= sub_dfs
        );
        
    opt_majorticks= _getsubops_( ["ticks", df_ticks, "majorticks", [] ] );
    opt_minorticks= _getsubops_( ["ticks", df_ticks, "minorticks", [] ] );

    _label = opt("label");
    opt_label = isstr(_label)
            ? update(df_label, ["text", _label]) //concat(["text", _label], df_label)   
            : ishash(_label)
            ? update( df_label, _label) //concat(_label, df_label )
            : df_label;

    function opt_label(k, df)= hash( opt_label, k, df); 
    
    pq = p01(pqr);
    P = pq[0]; Q=pqr[1]; R=pqr[2];
    MarkPts( pq, ["chain",_opt,"ball",0, "r",r]);
    
    // Major ticks 
    nMaj = floor(dist(pq) / majorIntv);
//    echo( opt_majorticks = opt_majorticks
//        , distpq=dist(pq)
//        , majorIntv=majorIntv,nMaj=nMaj
//        , opt_label = opt_label
//        );
    if( opt_majorticks ){
       ticklen = hash( opt_majorticks, "len", 0.2);
        
       for( i=[0:nMaj] ){
           P0= i==0?P:onlinePt( pq, i*majorIntv);
           P1= i==0?anglePt([Q,P,R] ,a=90,len= ticklen/2)
                   :anglePt([P,P0,R],a=90,len= ticklen/2 );
           P2= onlinePt( [P0,P1], len=-ticklen/2); 
           //echo( i=i, p12= [P1,P2], labelpqr= labelpqr);
           MarkPts( [P0,P1], ["ball=0","color","darkcyan", "chain",["r",r] ] );
           //labelpqr = [ extPt([P0,P1]),P1,R];
           label_margin = 0.1;
           labelanchor = onlinePt( [P0,P2], label_margin);
           
           //labelpqr = [ extPt([P0,P2]),labelanchor,R];
           labelpqr = [ -4*P0+5*P2  // get a far pt on the P2 side
                      , labelanchor,R];
           Write( startvalue+ (showindex?i:i*majorIntv)
                , labelpqr
                , valign="center"
                , scale=0.025
                , opt= opt_label
                ); //, scale=1, size=10); //, opt=_opt );
       }
        
    }    
    
}


module Ruler_demo(){
    echo("Ruler_demo");
    
    pqr = pqr();
    pqr = [[-0.97857, 2.53666, -2.37992], [0.810818, -2.11543, 1.02358], [2.82844, -1.29005, 1.5751]];
    echo(pqr=pqr);
    
    MarkPts( pqr, "ch;l=PQR;ball=0" );
    //Ruler( pqr );
    
    //Ruler( pqr(), opt=["majorticks", ["len",5]] );
    
}
//Ruler_demo();
//

ISLOG = 0; 
     
module Chain( // 2018.10.22 add logging 

     data     /* data could be: 
                   (1) pts
                   (2) randchain return (has "pts") or 
                   (3) chainPts return (has "xsecs") 
                */
     , opt
     
    //===============================================     
    // The following are used to generate data using 
    // randchain( data,n,a,a2... ) when data is a pts.
    // They are ignored if data is (2) or (3) 
    
     , n      //=3, nSegments =n-1
     , a      //=[-360,360]
     , a2     //=[-360,360]
     , seglen //= [0.5,3] // rand len btw two values
     , lens   //=undef
     , seed   //=undef // base pqr
     , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
                     //   , "clos
                     
    //===============================================     
    // The following are used to generate data using
    // chaindata( data,n,a,a2... ) when data is a randchain
    // return. They are ignored if the data is already 
    // chainPts return. 

     , r //=1
     , rlast //=undef
     , rs //=[] // to be added to r 
     , nside //=6
     , nsidelast
     , loftpower
     
     , rot //=0   // The entire chain rotate about its bone
               // away from the seed plane, which
               // is [ p0, p1, Rf ]
     
     , cut0
     , cutlast
     
     , xpts //=undef  // Define crosec. the r,r2, and rs
                   // will be ignored, unless, see below
     , xptratios //= undef //  ratio of r|r2|rs
     
     , xptadds //= false // arr of nums added to r|r2|rs
                      // AND/OR xpts|xptratios
     
     
     , Rf //=undef  // Reference pt to define the starting
                 // seed plane from where the crosec 
                 // starts 1st pt. Seed Plane=[p0,p1,Rf]
                 
     , twist //=0 // Angle of twist of the last crosec 
               // comparing to the first crosec
     
     , closed //=false // If the chain is meant to be closed.
                    // The starting and ending crosecs will
                    // be modified. 
     
     , tilt //=undef//=[0,0]
     , tiltlast //=undef//=[0,0] 
     
     //===============================================     
     // The following are Chain-specific, used for display
  
     , shape= "chain"
     
     , color
     , transp
     
     , body
     , markpts
     , bone
     , frame
     , edges
     , markxpts
     , pads
     
     )
{
    // Top layer of logging level for this module
    lv = 0;  
    
    log_b("Chain()", lv);
    //echom("Chain");

    //=====================================================     
    // arg reset
    //-------------------------------------
    // Convert string opt to pure opt:
    _u_opt = popt(opt); 
    //echo( _u_opt = _u_opt );
    
    function setargv( name, plainvar, df) = 
        und(plainvar, hash( _u_opt, name, df ));
    //-------------------------------------
    // args for 
    // randchain( data,n,a,a2... ) when data is a pts.
    n= setargv("n", n, 3 );
    a= setargv("a", a ); 
    a2= setargv("a2", a2 );
    seglen= setargv( "seglen", seglen);  
    lens= setargv("lens", lens);
    seed= setargv("seed", seed);
    smooth= setargv("smooth", smooth);    
    //-------------------------------------
    // args for
    // chaindata( data,n,a,a2... ) when data is a randchain
    r= setargv("r",r,0.5);
    rlast= setargv("rlast", rlast);
    //rs= setargv("rs", rs);
    _nside= setargv("nside",nside );
    nsidelast= setargv("nsidelast", nsidelast);
    loftpower= setargv("loftpower",loftpower,0 );
    rot= setargv("rot",rot, 0 );
    cut0= setargv("cut0", cut0);
    Rf= setargv("RF",Rf );
    twist= setargv("twist",twist,0 );
    closed= setargv("closed",closed,false );
    tilt= setargv("tilt",tilt );
    tiltlast= setargv("tiltlast", tiltlast);
    //-------------------------------------
    //color= setargv("color",color );
    color= colorArg(color, opt, ["gold",1]);
    //echo(color= color, _color=_color);
    //transp= setargv("transp",transp,1 );
    _shape= setargv("shape",shape,"chain");
    //=====================================================     

    //echo(closed_at_start = closed );
    //
    
    log_b("Prep user opt",lv+1);
    // Prepare user opt
    //
    //
    // Make display opt available in both simple form
    // (body=false, etc) AND hash form ( either "body=false"
    // or ["body",false] )
    //
    // Note 2018.10.22: these should be re-written using the subopt
    //
    _disp_opt= [ "body",body
               , "markpts", markpts
               , "bone", bone
               , "frame", frame
               , "edges", edges
               , "markxpts", markxpts
               , "pads", pads
               ];
    disp_opt= joinarr( [ for(kv= hashkvs(_disp_opt))
                         if(kv[1]!=undef) kv ] ); 
    u_opt = update( _u_opt, disp_opt );
    //echo( u_opt = u_opt ); 
    log_(["_u_opt",_u_opt], lv+1);
    
    log_(["u_opt",u_opt], lv+1);
    log_(["_disp_opt",_disp_opt], lv+1);
    log_(["disp_opt",disp_opt], lv+1);
    log_e("",lv+1);
    
    
    log_b("Prep default opt",lv+1);                    
    //
    // Prepare defult opt                     
    //
    df_opt = [ "closed",false
             , "body", true
             ];
    opt = update( df_opt, u_opt );
    function opt(k,nf)= hash(opt, k, nf);
    //echo(opt=opt);
                         
    seed=  or(seed, hash(opt, "seed")); //app( p01(bone), hash(chrtn,"Rf"));
    //data= or(data, or(seed, randPts(3)));                     
                         
    //closed = und(closed, opt("closed"));
    //nside  = opt("nside");
    log_(["opt",opt], lv+1);
    log_e("",lv+1);
       

  
    log_b("Handle data",lv+1);                    
    // Handle data, which could be:
    // 
    // (1) undef ==> give seed to grow from seed 
    // (2) pts ==> will get chain pts using chainPts
    // (3) randchain return => get chain pts w/ chainPts
    // (4) chainPts return => extract pts 
    // 
    chdata_opt= [
        "n", n
       ,"a", a 
       ,"a2", a2
       ,"seglen", seglen 
       ,"lens", lens
       ,"seed", seed
       , "smooth", smooth 
        //=======================
        //"pts", pts
        , "nside",_nside 
        , "nsidelast", nsidelast
        , "loftpower", loftpower
        , "r", r    // radius at the head (first cut)
        , "rlast", rlast   // radius at the tail (last cut)
        , "rs", rs     // array of r to be added on top of r              
        , "closed", closed // If the chain is meant to be closed.             
        //-------------------------------------------------
        , "twist", twist
        , "rot", rot
        //-------------------------------------------------
        , "seed", seed   //=undef // base pqr
        , "Rf", Rf // Reference pt to define the starting            
        //-------------------------------------------------
        , "cut0", cut0 // Define cross-section. This would become
        , "cutlast", cutlast // Define cross-section. This would become
        //, "bevel0", bevel0

        , "tilt", tilt    
        , "tiltlast", tiltlast 
    ];   
    
    log_(["chdata_opt", _fmth(chdata_opt)], lv+2);
     
    //echo(chdata_opt= chdata_opt);
    //echo(data=data, chdata_opt= chdata_opt);
    //echo( "verify: ", has_cuts= haskey( data, "cuts")
    //    , has_pts = haskey( data, "pts")
    //    , is_pts = ispt(data[0])
    //    );
    _chrtn = haskey( data, "cuts")? data 
            : haskey( data, "pts")? 
                chaindata( hash( data, "pts"), opt=chdata_opt )
            : ispt(data[0])? 
                chaindata( data, opt=chdata_opt )
            : chaindata( hash( randchain( n=n, a=a, a2=a2
                                 , seglen=seglen
                                 , lens=lens, seed=seed, smooth=smooth  
                                 , opt=opt
                                 ), "pts" ), opt=chdata_opt )
            ;
            
    //echo( _chrtn= arrprn(_chrtn,dep=3, nl=true) );
    //log_(["_chrtn", _chrtn], lv+2);
    
    cuts = hash(_chrtn, "cuts");
    //echo( cuts = arrprn( cuts ) );
    bone = hash(_chrtn, "bone");
    //echo( bone = bone );
    frmlines = transpose( cuts );
    pts = joinarr( cuts );
    //echo( pts = arrprn(pts,3) );
    nseg = len( cuts )-1; 
    //nside= len( cuts[0] );<== this will cause error if shape is conedown
    nside = und(_nside, len(cuts[0])==1?4:len(cuts[0]));
    
    log_(["cuts",cuts, "bone",bone, "frmlines", frmlines], lv+2);
    log_(["pts",pts], lv+2);
    log_(["nseg",nseg,"nside",nside], lv+2);
    //echo( nside = nside);
    //echo(nseg=nseg);
//    echo( str("<br/><b> cuts= </b>", cuts
//             ,"<br/><b> bone= </b>", bone
//             ,"<br/><b> frmlines= </b>", frmlines
//             ,"<br/><b> pts= </b>", pts
//             ,"<br/><b> seed= </b>", seed
//             ,"<br/><b> nseg= </b>", nseg
//             )); 
    //echo( cuts = arrprn(cuts,3,nl=true) );
    // -------------------------------------------    
    //MarkPts(seed, "pl=[trp,0.1];ball=false");
    
    log_e("",lv+1);
    
    log_b("Set subopts",lv+1);                    
    
    subopts = subopts( 
    
          df_opt=df_opt
        , u_opt= u_opt
//        , touse =  [ "markpts"
//                    , "labels" 
//                    , "plane"  
//                    , "markxpts"   
//                    ]
        , dfs=[ "markpts", ["rng", range(pts)]
                           
              , "bone", ["r",0.06,"rng",range(bone)
                        ,"chain"
                         ,["r",0.02,"closed",closed]]
              , "seed", ["r",0.06,"rng",range(bone)
                        ,"chain",["r",0.02]]             
              , "frame", ["r",0.01, "color","green"
                         ,"rng", range(frmlines)
                         ]
              , "edges", ["r",0.01, "color","green"
                         ,"rng", range(frmlines)
                         ]
              , "markxpts", ["r",0.04
                          //, "closed",true
                          ,  "chain",["closed",true] 
                         ,"rng", range(cuts)
                         ]
              , "pads", ["r",0.1, "color",undef
                         ,"rng", range(frmlines)
                         ]           
              ]
        , com_keys=[] 
       );
       
    function sub(subname)= hash(subopts, subname);
    
    //echo( subopts = arrprn(subopts,dep=3) );

    
    // -------------------------------------------
    log_b("markpts", lv+2);    
    o_mpts = sub("markpts");
    if(pts && o_mpts)  {
        //echo(o_mpts=o_mpts);
        MarkPt( get(pts,hash(o_mpts,"rng"))
                    , o_mpts );
    }    
    //MarkPts( pts, "r=0.03"); 
    
    // Temperarorily insert current MarkPts feature:
    //if(MarkPts==true) MarkPts(pts);
    //else if(MarkPts) MarkPts(pts, opt=MarkPts);
      
    log_e("",lv+2);
    
    // -------------------------------------------  
      
    log_b("bone", lv+2);
    o_bone = sub("bone");
    if(bone && o_bone)  {
        //echo(o_bone=o_bone, "<br/>",bone=bone, "<br/>"
        //    , selbone = get(bone,hash(o_bone,"rng")));
        
        MarkPts( get(bone,hash(o_bone,"rng"))
                    , o_bone );
    }    
    log_e("",lv+2);
    
    // -------------------------------------------  
    
    log_b("seed", lv+2);   
    o_seed = sub("seed");
    if(seed && o_seed)  {
        //echo(o_seed=o_seed, "<br/>",seed=seed);
        
        MarkPts( get(seed,hash(o_seed,"rng"))
                    , o_seed );
    }       
    log_e("",lv+2);
    
    //-------------------------------------------
    
    log_b("markxpts", lv+2);
    o_xpts = sub("markxpts");
    //echo( o_xpts = o_xpts );
    if( cuts && o_xpts ){
        
       //echo( rng = or(hash( o_xpts, "rng"), range(cuts)) );
      //echo( o_xpts = o_xpts );

         for( i= hash( o_xpts, "rng", range(cuts))){
           //echo( str("<b>",i, "</b>"
           //         , arrprn(get(cuts,i),nl=true)
           //         , "<br/> opt=", delkey(o_xpts,"rng"))
           //    );
           MarkPts( get(cuts,i), delkey(o_xpts,"rng"));
         } 
    }    
    log_e("",lv+2);
    
    //-------------------------------------------
    
    log_b("edges",lv+2);
    o_edges = sub("edges");
    if( frmlines && o_edges ){
       //echo("edging, o_edges= ", o_edges);
       for( i= hash(o_edges, "rng", range(frmlines))){
           Chain( get(frmlines,i), o_edges);
       }         
    }    
    log_e("",lv+2);
    
    //-------------------------------------------
       
    log_b("frame",lv+2);   
    o_frame = sub("frame");
    //echo(o_frame = o_frame);
    if(o_frame){
        //echo(o_frame = o_frame);
        if( frmlines ) { 
           for( fl= frmlines){ 
               Chain( fl, o_frame);
           }         
        }//if frmlines    
        
       if( cuts ){
//           echo("cuting ...");
//           echo( hash(o_frame,"rot"), hash(o_frame,"nside"), hash(opt,"nside"));
           for( cut= cuts){ //cut= cuts ) {
               //echo( i=i, cut = cut );
               // chaining of cuts will be rotated by the amount
               // of 180/nside (say, 45 for nside=4) to match the
               // angle of cuts and that of frmlines               
               Chain( cut
                    
                    , opt=concat( ["closed",true], o_frame
                            , ["rot", hash(o_frame,"rot", 0)
                               +180/hash(o_frame,"nside"
                                   ,hash(opt,"nside", nside))
                              ] 
                            )
                    );
           }    
       }// if cuts  
    }//if_o_frame
    log_e("",lv+2);
    
     closinglink= [  cuts[0][0]
                    , bone[0]
                    , last(bone)
                    , last(cuts)[0]
                  ];  
                  
     //echo( closinglink = closinglink);             
                  
     /* tailroll
    
        tailroll is the amount(int) of pts for the tailface
        to roll in order to match the tail face to the head
        face to overcome twisting in cases of closed chain
    
        twistangle 
    
     */
                    
     twistangle = len(cuts[0])>1 && len(last(cuts))>1?
                   twistangle(closinglink): undef;     
     tailroll =  -floor( twistangle/ ( 360/ nside)); 
     //echo( len(cuts[0]), len(last(cuts))
     //    , twistangle= twistangle, tailroll=tailroll);
     
     L0= len(cuts[0]);
     Llast= len(last(cuts));
     
     shape= closed && tailroll? "ring"
            : L0==1? 
                ( Llast==1? "spindle":"conedown")
            : Llast==1? "cone"
            : closed? "ring"
            : _shape; //or(shape, opt( "shape", "chain"));
     //echo( closed=closed, shape = shape );
     
     log_b("Actual drawing",lv+2);
     faces = faces( shape=shape
                  , nside=nside 
                  , nseg= nseg 
                  , tailroll = tailroll
                  );
      
     //echo(color, transp);             
     if(pts && opt("body"))
     {    
         Color(color)   
         polyhedron( points= pts, faces=faces);  
     }    
     log_e("",lv+2);
//     
  log_e("Chain:end", lv);
    
 }//_Chain()
 
module chain_dummy_end(){} // this is for quick access on editor to reach end of Chain 
  
//module Chain( 
//
//     data     /* data could be: 
//                   (1) pts
//                   (2) randchain return (has "pts") or 
//                   (3) chainPts return (has "xsecs") 
//                */
//     , opt
//     
//    //===============================================     
//    // The following are used to generate data using 
//    // randchain( data,n,a,a2... ) when data is a pts.
//    // They are ignored if data is (2) or (3) 
//    
//     , n      //=3, nSegments =n-1
//     , a      //=[-360,360]
//     , a2     //=[-360,360]
//     , seglen //= [0.5,3] // rand len btw two values
//     , lens   //=undef
//     , seed   //=undef // base pqr
//     , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
//                     //   , "clos
//                     
//    //===============================================     
//    // The following are used to generate data using
//    // chaindata( data,n,a,a2... ) when data is a randchain
//    // return. They are ignored if the data is already 
//    // chainPts return. 
//
//     , r //=1
//     , rlast //=undef
//     , rs //=[] // to be added to r 
//     , nside //=6
//     , nsidelast
//     , loftpower
//     
//     , rot //=0   // The entire chain rotate about its bone
//               // away from the seed plane, which
//               // is [ p0, p1, Rf ]
//     
//     , cut0
//     , cutlast
//     
//     , xpts //=undef  // Define crosec. the r,r2, and rs
//                   // will be ignored, unless, see below
//     , xptratios //= undef //  ratio of r|r2|rs
//     
//     , xptadds //= false // arr of nums added to r|r2|rs
//                      // AND/OR xpts|xptratios
//     
//     
//     , Rf //=undef  // Reference pt to define the starting
//                 // seed plane from where the crosec 
//                 // starts 1st pt. Seed Plane=[p0,p1,Rf]
//                 
//     , twist //=0 // Angle of twist of the last crosec 
//               // comparing to the first crosec
//     
//     , closed //=false // If the chain is meant to be closed.
//                    // The starting and ending crosecs will
//                    // be modified. 
//     
//     , tilt //=undef//=[0,0]
//     , tiltlast //=undef//=[0,0] 
//     
//     //===============================================     
//     // The following are Chain-specific, used for display
//  
//     , shape= "chain"
//     
//     , color
//     , transp
//     
//     , body
//     , markpts
//     , bone
//     , frame
//     , edges
//     , markxpts
//     , pads
//     
//     )
//{
//    //echom("Chain");
//
//    //=====================================================     
//    // arg reset
//    //-------------------------------------
//    // Convert string opt to pure opt:
//    _u_opt = popt(opt); 
//    //echo( _u_opt = _u_opt );
//    function setargv( name, plainvar, df) = 
//        und(plainvar, hash( _u_opt, name, df ));
//    //-------------------------------------
//    // args for 
//    // randchain( data,n,a,a2... ) when data is a pts.
//    n= setargv("n", n, 3 );
//    a= setargv("a", a ); 
//    a2= setargv("a2", a2 );
//    seglen= setargv( "seglen", seglen);  
//    lens= setargv("lens", lens);
//    seed= setargv("seed", seed);
//    smooth= setargv("smooth", smooth);    
//    //-------------------------------------
//    // args for
//    // chaindata( data,n,a,a2... ) when data is a randchain
//    r= setargv("r",r,0.5);
//    rlast= setargv("rlast", rlast);
//    //rs= setargv("rs", rs);
//    _nside= setargv("nside",nside );
//    nsidelast= setargv("nsidelast", nsidelast);
//    loftpower= setargv("loftpower",loftpower,0 );
//    rot= setargv("rot",rot, 0 );
//    cut0= setargv("cut0", cut0);
//    Rf= setargv("RF",Rf );
//    twist= setargv("twist",twist,0 );
//    closed= setargv("closed",closed,false );
//    tilt= setargv("tilt",tilt );
//    tiltlast= setargv("tiltlast", tiltlast);
//    //-------------------------------------
//    color= setargv("color",color );
//    transp= setargv("transp",transp,1 );
//    _shape= setargv("shape",shape,"chain");
//    //=====================================================     
//
//    //echo(closed_at_start = closed );
//    //
//    // Prepare user opt
//    //
//    //
//    // Make display opt available in both simple form
//    // (body=false, etc) AND hash form ( either "body=false"
//    // or ["body",false] )
//    _disp_opt= [ "body",body
//               , "markpts", markpts
//               , "bone", bone
//               , "frame", frame
//               , "edges", edges
//               , "markxpts", markxpts
//               , "pads", pads
//               ];
//    disp_opt= joinarr( [ for(kv= hashkvs(_disp_opt))
//                         if(kv[1]!=undef) kv ] ); 
//    u_opt = update( _u_opt, disp_opt );
//    //echo( u_opt = u_opt );                     
//    //
//    // Prepare defult opt                     
//    //
//    df_opt = [ "closed",false
//             , "body", true
//             ];
//    opt = update( df_opt, u_opt );
//    function opt(k,nf)= hash(opt, k, nf);
//    //echo(opt=opt);
//                         
//    seed=  or(seed, hash(opt, "seed")); //app( p01(bone), hash(chrtn,"Rf"));
//    //data= or(data, or(seed, randPts(3)));                     
//                         
//    //closed = und(closed, opt("closed"));
//    //nside  = opt("nside");
//    
//
//
//    // Handle data, which could be:
//    // 
//    // (1) undef ==> give seed to grow from seed 
//    // (2) pts ==> will get chain pts using chainPts
//    // (3) randchain return => get chain pts w/ chainPts
//    // (4) chainPts return => extract pts 
//    // 
//    chdata_opt= [
//        "n", n
//       ,"a", a 
//       ,"a2", a2
//       ,"seglen", seglen 
//       ,"lens", lens
//       ,"seed", seed
//       , "smooth", smooth 
//        //=======================
//        //"pts", pts
//        , "nside",_nside 
//        , "nsidelast", nsidelast
//        , "loftpower", loftpower
//        , "r", r    // radius at the head (first cut)
//        , "rlast", rlast   // radius at the tail (last cut)
//        , "rs", rs     // array of r to be added on top of r              
//        , "closed", closed // If the chain is meant to be closed.             
//        //-------------------------------------------------
//        , "twist", twist
//        , "rot", rot
//        //-------------------------------------------------
//        , "seed", seed   //=undef // base pqr
//        , "Rf", Rf // Reference pt to define the starting            
//        //-------------------------------------------------
//        , "cut0", cut0 // Define cross-section. This would become
//        , "cutlast", cutlast // Define cross-section. This would become
//        //, "bevel0", bevel0
//
//        , "tilt", tilt    
//        , "tiltlast", tiltlast 
//    ];    
//    //echo(chdata_opt= chdata_opt);
//    //echo(data=data, chdata_opt= chdata_opt);
//    //echo( "verify: ", has_cuts= haskey( data, "cuts")
//    //    , has_pts = haskey( data, "pts")
//    //    , is_pts = ispt(data[0])
//    //    );
//    _chrtn = haskey( data, "cuts")? data 
//            : haskey( data, "pts")? 
//                chaindata( hash( data, "pts"), opt=chdata_opt )
//            : ispt(data[0])? 
//                chaindata( data, opt=chdata_opt )
//            : chaindata( hash( randchain( n=n, a=a, a2=a2
//                                 , seglen=seglen
//                                 , lens=lens, seed=seed, smooth=smooth  
//                                 , opt=opt
//                                 ), "pts" ), opt=chdata_opt )
//            ;
//            
//    //echo( _chrtn= arrprn(_chrtn,dep=3, nl=true) );
//    
//    cuts = hash(_chrtn, "cuts");
//    //echo( cuts = arrprn( cuts ) );
//    bone = hash(_chrtn, "bone");
//    //echo( bone = bone );
//    frmlines = transpose( cuts );
//    pts = joinarr( cuts );
//    //echo( pts = arrprn(pts,3) );
//    nseg = len( cuts )-1; 
//    //nside= len( cuts[0] );<== this will cause error if shape is conedown
//    nside = und(_nside, len(cuts[0])==1?4:len(cuts[0]));
//    //echo( nside = nside);
//    //echo(nseg=nseg);
////    echo( str("<br/><b> cuts= </b>", cuts
////             ,"<br/><b> bone= </b>", bone
////             ,"<br/><b> frmlines= </b>", frmlines
////             ,"<br/><b> pts= </b>", pts
////             ,"<br/><b> seed= </b>", seed
////             ,"<br/><b> nseg= </b>", nseg
////             )); 
//    //echo( cuts = arrprn(cuts,3,nl=true) );
//    // -------------------------------------------    
//    //MarkPts(seed, "pl=[trp,0.1];ball=false");
//    
//    subopts = subopts( 
//    
//          df_opt=df_opt
//        , u_opt= u_opt
////        , touse =  [ "markpts"
////                    , "labels" 
////                    , "plane"  
////                    , "markxpts"   
////                    ]
//        , dfs=[ "markpts", ["rng", range(pts)]
//                           
//              , "bone", ["r",0.06,"rng",range(bone)
//                        ,"chain"
//                         ,["r",0.02,"closed",closed]]
//              , "seed", ["r",0.06,"rng",range(bone)
//                        ,"chain",["r",0.02]]             
//              , "frame", ["r",0.01, "color","green"
//                         ,"rng", range(frmlines)
//                         ]
//              , "edges", ["r",0.01, "color","green"
//                         ,"rng", range(frmlines)
//                         ]
//              , "markxpts", ["r",0.04
//                          //, "closed",true
//                          ,  "chain",["closed",true] 
//                         ,"rng", range(cuts)
//                         ]
//              , "pads", ["r",0.1, "color",undef
//                         ,"rng", range(frmlines)
//                         ]           
//              ]
//        , com_keys=[] 
//       );
//       
//    function sub(subname)= hash(subopts, subname);
//    
//    //echo( subopts = arrprn(subopts,dep=3) );
//
//    
//    // -------------------------------------------    
//    o_mpts = sub("markpts");
//    if(pts && o_mpts)  {
//        //echo(o_mpts=o_mpts);
//        MarkPt( get(pts,hash(o_mpts,"rng"))
//                    , o_mpts );
//    }    
//    //MarkPts( pts, "r=0.03");   
//   
//    // -------------------------------------------    
//    o_bone = sub("bone");
//    if(bone && o_bone)  {
//        //echo(o_bone=o_bone, "<br/>",bone=bone, "<br/>"
//        //    , selbone = get(bone,hash(o_bone,"rng")));
//        
//        MarkPts( get(bone,hash(o_bone,"rng"))
//                    , o_bone );
//    }    
//
//    // -------------------------------------------    
//    o_seed = sub("seed");
//    if(seed && o_seed)  {
//        //echo(o_seed=o_seed, "<br/>",seed=seed);
//        
//        MarkPts( get(seed,hash(o_seed,"rng"))
//                    , o_seed );
//    }       
//    //-------------------------------------------
//    o_xpts = sub("markxpts");
//    //echo( o_xpts = o_xpts );
//    if( cuts && o_xpts ){
//        
//       //echo( rng = or(hash( o_xpts, "rng"), range(cuts)) );
//      //echo( o_xpts = o_xpts );
//
//         for( i= hash( o_xpts, "rng", range(cuts))){
//           //echo( str("<b>",i, "</b>"
//           //         , arrprn(get(cuts,i),nl=true)
//           //         , "<br/> opt=", delkey(o_xpts,"rng"))
//           //    );
//           MarkPts( get(cuts,i), delkey(o_xpts,"rng"));
//         } 
//    }    
//    
//    //-------------------------------------------
//    o_edges = sub("edges");
//    if( frmlines && o_edges ){
//       //echo("edging, o_edges= ", o_edges);
//       for( i= hash(o_edges, "rng", range(frmlines))){
//           Chain( get(frmlines,i), o_edges);
//       }         
//    }    
//    
//    //-------------------------------------------
//       
//    o_frame = sub("frame");
//    //echo(o_frame = o_frame);
//    if(o_frame){
//        //echo(o_frame = o_frame);
//        if( frmlines ) { 
//           for( fl= frmlines){ 
//               Chain( fl, o_frame);
//           }         
//        }//if frmlines    
//        
//       if( cuts ){
////           echo("cuting ...");
////           echo( hash(o_frame,"rot"), hash(o_frame,"nside"), hash(opt,"nside"));
//           for( cut= cuts){ //cut= cuts ) {
//               //echo( i=i, cut = cut );
//               // chaining of cuts will be rotated by the amount
//               // of 180/nside (say, 45 for nside=4) to match the
//               // angle of cuts and that of frmlines               
//               Chain( cut
//                    
//                    , opt=concat( ["closed",true], o_frame
//                            , ["rot", hash(o_frame,"rot", 0)
//                               +180/hash(o_frame,"nside"
//                                   ,hash(opt,"nside", nside))
//                              ] 
//                            )
//                    );
//           }    
//       }// if cuts  
//    }//if_o_frame
//    
//     closinglink= [  cuts[0][0]
//                    , bone[0]
//                    , last(bone)
//                    , last(cuts)[0]
//                  ];  
//                  
//     //echo( closinglink = closinglink);             
//                  
//     /* tailroll
//    
//        tailroll is the amount(int) of pts for the tailface
//        to roll in order to match the tail face to the head
//        face to overcome twisting in cases of closed chain
//    
//        twistangle 
//    
//     */
//                    
//     twistangle = len(cuts[0])>1 && len(last(cuts))>1?
//                   twistangle(closinglink): undef;     
//     tailroll =  -floor( twistangle/ ( 360/ nside)); 
//     //echo( len(cuts[0]), len(last(cuts))
//     //    , twistangle= twistangle, tailroll=tailroll);
//     
//     L0= len(cuts[0]);
//     Llast= len(last(cuts));
//     
//     shape= closed && tailroll? "ring"
//            : L0==1? 
//                ( Llast==1? "spindle":"conedown")
//            : Llast==1? "cone"
//            : closed? "ring"
//            : _shape; //or(shape, opt( "shape", "chain"));
//     //echo( closed=closed, shape = shape );
//     
//     faces = faces( shape=shape
//                  , nside=nside 
//                  , nseg= nseg 
//                  , tailroll = tailroll
//                  );
//                  
//     if(pts && opt("body"))
//     {    
//         color(color,transp)   
//         polyhedron( points= pts, faces=faces);  
//     }    
//
// }//_Chain()
// 


//module ChainByHull( pts, opt=[]) // for debug use
//{
//    for( i = [1:len(pts)-1] )
//        Line0( [pts[i-1], pts[i]], opt=opt );
//}    


module Color(colorArg,opt=[],df=undef)
{  // Allow color be entered as one of the following: 
   //
   //  Color("red")
   //  Color(["red",0.5])
   //  Color( ["color","red"] )
   //  Color( ["color",["red",0.5]])
   //  
   //  Usage:
   //         Color( opt ) cube();
   //         Color( color_arg, opt ) cube();
   //
   //         module Obj(color,opt=[])
   //         {  Color(color,opt) Cube(); }
   //
   // Color("red") cube();
   // Color(["green",0.7]) cube();
   //
   // Color( opt=["color", "blue"] ) cube();
   // Color( opt=["color", ["blue",0.5] ] ) cube();
   //
   // Color( undef, ["color", "blue"] ) cube();
   // Color( undef, ["color", ["blue",0.5] ] ) cube();
   // Color( "teal", ["color", "blue"] )  cube();  
   // Color( "teal", ["color", ["blue",0.5] ]] )  cube();  
   //   
   //  2018.1.19
   
   //color= getColorArg( und_opt(colorArg, opt,"color",df) ); //hash(data, "color", data));
   color = colorArg(colorArg,opt,df);
   //echo(colorArg=colorArg,opt=opt, color=color);
   color( color[0], color[1] )
      children();
}


//========================================================
ColorAxes=["ColorAxes", "ops", "n/a", "Shape",
 " Paint colors on axes. 
;;  [
;;   ''r'',0.012
;;  ,''len'', 4.5
;;  ,''xr'',-1
;;  ,''yr'',-1
;;  ,''zr'',-1
;;  ,''xops'', [''color'',''red''  , ''transp'',0.3]
;;  ,''yops'', [''color'',''green'', ''transp'',0.3]
;;  ,''zops'', [''color'',''blue'' , ''transp'',0.3]
;;  ]
"];

module ColorAxes( ops=[] )
{
	ops = concat( ops,
	[
		"r",0.012
		,"len", 4.5
		,"xr",-1
		,"yr",-1
		,"zr",-1
		,"fontscale", 1
		,"xops", ["color","red", "transp",0.3]
		,"yops", ["color","green", "transp",0.3]
		,"zops", ["color","blue", "transp",0.3]
	]);
	function ops(k)= hash(ops,k);
	r = ops("r");
	l = ops("len");

	fscale= ops("fontscale");
	//echo( "fscale" , fscale);
	//include <../others/TextGenerator.scad>
	
	xr= hash( ops, "xr", if_v=-1, then_v=r) ;
	yr= hash( ops, "yr", r, if_v=-1, then_v=r);
	zr= hash( ops, "zr", r, if_v=-1, then_v=r);
	
	echo( "r",r, "xr", xr);

	Line( [[ l,0,0],[-l,0,0]], concat( ["r", xr], ops("xops") ));
	Line( [[ 0, l,0],[0, -l,0]], concat( ["r", yr], ops("yops") ));
	Line( [[ 0,0,l],[0,0,-l]], concat( ["r", zr], ops("zops") ));

	xc = hash(ops("xops"), "color");
	yc = hash(ops("yops"), "color");
	zc = hash(ops("zops"), "color");
	translate( [l+0.2, -6*r,0]) 
		rotate($vpr) scale( fscale/15 ) 
		color(xc) text( "x" );
	translate( [6*r, l+0.2, 0]) 
		rotate($vpr) scale( fscale/15 ) 
		color(yc) text( "y" );
	translate( [0, 0, l+0.2]) 
		rotate($vpr) scale( fscale/15 ) 
		color(zc) text( "z" );
} 
module ColorAxes_test(ops){ doctest( ColorAxes ,ops=ops);}


//========================================================
Cone=["Cone","pq, ops=[]", "n/a", "Geometry"
, "Given a 2 or 3-pointer pq and ops, make a cone along the PQ
;; line. If pq a 3-pointer, the points forming the cone bottom
;; will start from plane of the 3 points and circling around 
;; axis PQ in clockwise manner. Default ops:
;;
;; [ ''r'' , 3  // rad of the cone bottom
;; , ''len'', 0.3  // height of the cone
;; , ''fn'', $fn==0?6:$fn // # of segments along the cone curve,
;; ]
;;
;; New feature: You can mark points @ run-time:
;; 
;; Cone(pq, [''markpts'', true] )
;; Cone(pq, [''markpts'', [''labels'',''ABCDEF''] )
;; 
"];
module Cone_test(ops=[]){
    doctest( Cone, [], ops); }
    
module Cone( pq, ops=[] ){
    //_mdoc("Cone","");
                     
	ops= concat( ops
    , [ "r" , 1            // rad of the curve this obj follows
	  , "len", undef       // default to len_PQ. Could be negative
      , "fn", $fn==0?6:$fn // # of segments along the cone curve,
                           // = # of slices or segments
      , "markpts", false   // true or hash
      , "twist", 0
      ], OPS);

    /*      Q
           _|_ 
      _--'' | ''--_
    -{------P------}----- 
      ''-.__|__.-''
            |      | 
            |<---->|
                r    
    */
    
    function ops(k,df)=hash(ops,k,df);
    
    //echo("ops= ", ops);
    
    r = ops("r");
    fn= ops("fn");
    
    P = pq[0]; Q=pq[1]; 
    R= len(pq)==3? pq[2]: anglePt( [Q,P, randPt()], a=90, r=r);
    
    pqr = [P,Q,R];
    
    //     0
    //     | 
    //  _4-|-3_
    // 5_  +  _2
    //   -6-1-
    //
    // faces= [ [0,2,1]
    //        , [0,3,2] 
    //        , ... 
    //        , [0,6,5]
    //        , [0,1,6]
    //        ]
    
    faces= concat(
            [ for( i =range(fn) ) [0, i>fn-2?1:i+2, i+1] ]
            , [range(1, fn+1)]    
      );  

    //echo("faces", faces);
       
    arcpts = arcPts( [Q,P,R]
                    , pl="yz"
                    , a=360
                    , count=fn
                    , r=r
                    , twist= ops("twist")
                    ); 
//    echo( "arcpts", arcpts);   
            
    df_markpts= ["samesize",true, "labels", range(1, fn+1)];       
    m = ops("markpts");
	if(m) { MarkPts(arcpts , concat( isarr(m)?m:[], df_markpts));}
    
    color( ops("color"), ops("transp"))        
    polyhedron( points= concat( [Q], arcpts )
              , faces = faces );               
}

module Cone_demo_1(){
 _mdoc("Cone_demo_1"
      ," yellow: Cone( randPts(2) )
      ;; green: Cone( randPts(3) , [ ''fn'',6
      ;;                , ''color'',''green''
      ;;                , ''transp'',0.5
      ;;                , ''markpts'',true  // <=== new feature in scadex
      ;;                ] 
       ");    

    pq = randPts(2);
    MarkPts( pq, ["labels","PQ"]);
    Cone( pq );
    LabelPt( pq[1], "  Cone(pq)");

    pqr = randPts(3);
    Plane(pqr, ["mode",3]);
    MarkPts( pqr, ["labels","PQR"]);
    Cone( pqr , [ "fn",6
                , "color","green"
                , "transp",0.5
                , "markpts",true  // <=== new feature in scadex
                ]);
    LabelPt( pqr[1], "  Cone(pqr,[\"fn\",6,\"markpts\",true...])"
            , ["color","green"]);
                            
    pqr2 = randPts(3);
    Plane(pqr2, ["mode",3]);
    MarkPts( pqr2, ["labels","PQR"]);
    Cone( pqr2 , [ "fn",6
                , "color","blue"
                , "transp",0.5
                , "markpts",true  // <=== new feature in scadex
                , "twist", 30 // <=== new 2015.1.25
                ]);
     LabelPt( pqr2[1]
     , "  Cone(pqr,[\"fn\",6,\"markpts\",true,\"twist\",30...])"
            , ["color","blue"]);
            
}    

//Cone_demo_1();

//========================================================
//Coord=["Coord", "pqr,ops", 

module Coord( range, scale, color, Majortick=true, Minortick=true, Label, r, site
              , X,Y=true,Z
              , opt=[]
            ) //randCoordPts() )
{
  mj= subprop(Majortick, opt, "Majortic",["scalelen",2]);
  mi= subprop(Minortick, opt, "Minortic",["scalelen",1]);
  
  common = ["range", arg(range, opt, "range", [-5,5])
           ,"scale", scaleArg(scale,opt, mi? hash(mj,"scalelen")/hash(mi,"scalelen")
                                           : hash(mj,"scalelen")
                                           )
           , "Majortick", mj
           , "Minortick", mi
           , "r", arg(r,opt,"r")
           , "show0", false
           ];
  
  _X = subprop(X, opt, "X",  update(common, ["color",["red",0.5]] ));
  _Y = subprop(Y, opt, "Y",  update(common, ["site", [ [0,1,0], ORIGIN, [-1,-1,0]], "color",["green",0.5] ]));
  _Z = subprop(Y, opt, "Z",  dfsub=update(common, ["site", [ [0,0,1], ORIGIN, [-1,0,-1]], "color",["blue",0.5]  ]));
  echo(X=X,_X=_X);
  echo(Y=Y,_Y=_Y);
  echo(Z=Z,_Z=_Z);
  module _draw()
  {
    Ruler( opt=_X); 
    Ruler( opt=_Y); 
    Ruler( opt=_Z); 
  }
  
  _site=arg(site,opt,"site");          
  
  if(_site) Move(to=_site)_draw();
  else _draw();
    
}

module Coord_old(pqr, ops=[])
{
    /*
       Set labels default. We want to allows for :
       Coord( pqr, ["labels","xyz"] )
       That is, setting it to a string, but not like the Multi 
       Complex Arguments pattern that only takes 
       true|false|style_hash. 
       We set all to false, but it can be either a string or a style_hash 
    */
    lbs = hash( ops, "labels", false ); 
    lbx = hash( ops, "labelx", false );  
    lby = hash( ops, "labely", false );
    lbz = hash( ops, "labelz", false );
    lbo = hash( ops, "labelo", false );
    
    // If it's a string, convert to ["text",label]  
    u_ops = update(ops,
            ["labels", isstr(lbs)?["text",lbs]:lbs
            ,"labelx", isstr(lbx)?["text",lbx]:lbx
            ,"labely", isstr(lby)?["text",lby]:lby
            ,"labelz", isstr(lbz)?["text",lbz]:lbz
            ,"labelo", isstr(lbo)?["text",lbo]:lbo
            ]);       
    df_ops=[ "r", 0.01
           , "len",5           
           //---------------
           , "axes", true
           , "x", true
           , "y", true
           , "z", true
           //---------------
//           , "labels", false
//           , "labelx", false
//           , "labely", false
//           , "labelz", false
//           , "labelo", false
           ];
    com_keys= ["transp","color","r", "len"];
    df_axes=[];
    df_lbs= ["labels","xyz", "color","black", "scale",0.03];
    
    _ops = update( df_ops, u_ops);
    function ops(k,df)=hash(_ops,k,df);
    
    //echo( ["ops",_fmth(ops), "u_ops",_fmth(u_ops),"_ops",_fmth(_ops)] );
    //===============================================
    function subops(sub_dfs)=
          getsubops(  u_ops = u_ops  
                    , df_ops = df_ops
                    , com_keys= com_keys
                    , sub_dfs= sub_dfs
                    );
    //===============================================
    
    pqr= or(pqr, pqr());
    //MarkPts(pqr);
    pts= coordPts( get(pqr,[0,1,2])
                 , len=ops("len")
                 );
    
    //MarkPts(pts, ["r",0.2, "transp",0.3] );
    
    //=============================================== Axes
    ops_x= subops( ["axes",df_axes, "x", ["color","red"]] );
    ops_y= subops( ["axes",df_axes, "y", ["color","green"]] );
    ops_z= subops( ["axes",df_axes, "z", ["color","blue"]] );  
//    echo(ops_x= ops_x );  
//    echo( str("set ops_x= ", getsubops_debug( u_ops=u_ops, df_ops=df_ops
//                    , com_keys= com_keys
//                    , sub_dfs= ["axes", df_axes, "x", ["color","red"] ]
//                 ) ));   
    //echofh( ["ops_x",ops_x, "ops_y",ops_y, "ops_z",ops_z] );
    if( ops_x ) Line( get(pts,[1,0]), len=hash(ops_x,"len"), ops= ops_x);    
    if( ops_y ) Line( get(pts,[1,2]), len=hash(ops_x,"len"), ops= ops_y);    
    if( ops_z ) Line( get(pts,[1,3]), len=hash(ops_x,"len"), ops= ops_z);    
    //=============================================== Labels
//    labels = istype(labels,"str","arr")? labels
//             
//    function getaxislabels( )=
//        let( labels = ops("labels")
//           , isstar = istype(labels,"str","arr")
////           , lbs = istype(labels,"str","arr")?labels[i]
////                   :labels==true? axname :""
//           ) //lbs
//        
//        :labels==true? 
//        [ for(i=range(4))
//            let( name= "xoyz"[i] )
//            isstar? labels[i]:labels==true?"
    
    ops_lbx= subops( ["labels",df_lbs, "labelx", ["text","x"]] );
    ops_lby= subops( ["labels",df_lbs, "labely", ["text","y"]] );
    ops_lbz= subops( ["labels",df_lbs, "labelz", ["text","z"]] );
    ops_lbo= subops( ["labels",df_lbs, "labelo", ["text","o"]] );
    if(ops_lbx) LabelPt( pts[0], hash(ops_lbx,"text",""), ops_lbx ); 
    if(ops_lbo) LabelPt( pts[1], hash(ops_lbo,"text",""), ops_lbo ); 
    if(ops_lby) LabelPt( pts[2], hash(ops_lby,"text",""), ops_lbx ); 
    if(ops_lbz) LabelPt( pts[3], hash(ops_lbz,"text",""), ops_lbx ); 
    
//    echofh( ["ops_lbx",ops_lbx, "ops_lby",ops_lby
//            , "ops_lbz",ops_lbz, "ops_lbo",ops_lbo] );
    echo( str("set ops_lbo= ", getsubops_debug( u_ops=u_ops, df_ops=df_ops
                    , com_keys= com_keys
                    , sub_dfs= ["labels",df_lbs, "labelo", ["text","o"]]
                 ) ));  
          
    echo( str("set ops_y= ", getsubops_debug( u_ops=u_ops, df_ops=df_ops
                    , com_keys= com_keys
                    , sub_dfs= ["axes",df_axes, "y", ["color","green"]]
                 ) ));        

}

//
//

//========================================================
//Cube=["Cube","size,center=false,opt=[]", "n/a", "Obj"
//, "An extended version of the built-in cube(). New features
//;; defined in <i>opt</i>. 
//;;
//;; size= < num | pt >
//;; opt:  [ \"color\", \"gold\"
//;;       , \"frame\", true
//;;       , \"coord\", undef
//;;       , \"markPts\", undef            
//;;       , \"actions\", undef
//;;       , ]
//;; 
//;;       color: <col_name> | [<col_name>, <transparancy> ]
//;;              Set color of the solid obj. 
//;;       frame: T/F | num | <frame_opt>
//;;              where <frame_opt> is [\"r\",num, \"color\",...]
//;;              Refer to module Frame()
//;;       coord: pqr 
//;;              Set the new coordinate of the obj. The obj will
//;;              be placed upon it.
//;;       actions: a hash defining the actions like translate, etc.
//;;              See transPts. The actions will be perfromed based
//;;              on the new coord.
//;;       markPts: a hash defining the corner pts labeling. See
//;;              module MarkPts.  
//;;  
//"
//,"cube, cubePts"
//];

//
  /*
    Cube(3)
    Cube( [4,2,1], center=true )
    Cube( [4,2,1], center=true, opt=["coord",pqr()] )
    
    Cube(3, opt=["color","red"] )
    Cube(3, opt=["color",["red",0.5]] )
    
    Cube(3, opt=["actions",["x",5] ] )  // ref transPts() or Transforms() for all actions
    Cube(3, opt=["actions",["x",5, "coord",pqr()] ] ) // Make a cube on coord, and 
                                                      // move it by x=5 where x is 
                                                      // the QP axis
                                                       
    Cube(3, opt=["frame", true] );  // default
    Cube(3, opt=["frame", false] );
    Cube(3, opt=["frame", 0.1] );
    Cube(3, opt=["frame", ["color","blue"
                          ,"r",0.05
                          ,"joint", false
                          ] 
                ]);    
    
    
    dim:
    
    ["dim", [0,1,2]] // dim=indices
    ["dim", [6,[2,3,0],2] ]); // Use a pt [2,3,-2] instead of index. This allows to dim any place.
    ["dim", ["pqr", [6,4,2], "color","red"]]); // More options
    ["dims", [ [6,4,2]          // Use "dims" for multiple dims
             , [6,[2,3,0],4]
             , ["pqr",[3,0,1]]
             , ["pqr",[1,5,3], "color","black"]
             ]
    ] 
  */

//. Cube


module Cube( size, center, color                  // simple args
           , Frame=true
           , echoPt=0
           , site, actions, MarkPts, Dim  // compound args
           , opt=[]
           )
{
   //echom(_red("Cube()"));
   
   //---------------------------------------------------
   // getTransformData():
   //
   // Getting the data for transformations (actions,size,etc)
   //
   // This would be used in transforming the obj in the TransformObj()
   // Ideally we could have skip this and do them all in the
   // TransformObj(). However, we also need this to create pts for
   // framing, pt tracking and pt decorations. So we use getTransformData 
   // as a bridge.  
   _transdata = getTransformData( shape="cube"
                               , size = size
                               , center=center
           	               , site=site
           	               , actions=actions
           	               , opt=opt) ;
   pts = hash( _transdata, "pts" );
   transdata= update(_transdata, ["echoPt", arg(echoPt,opt,"echoPt",-1)]);
   
   //echoPt = arg(echoPt,opt,"echoPt",0);
   //if( echoPt>0 )
   //{  echo(pts = pts);
      //if   
   //}   
   //echo(transdata = transdata);
   //echo(transdata = transdata, size=hash(transdata, "size"), actions=hash(transdata, "actions"));
   _color= colorArg(color, opt);
   Color( _color )
   TransformObj(transdata )     
       cube(size=hash(transdata, "size"));

   //---------------------------------------------------
   // Now we do the pt decorations:
   //
   DecoratePts( pts //, color=color
              , Frame=Frame
              , Frame_df = [ "r", 0.005, "color", "black"     
                           , "Joint", ["shape","corner","$fn",4]]
              , MarkPts=MarkPts, MarkPts_df= ["color", _color]
              , Dim=Dim, Dim_df=["color", _color]
              , opt=opt );
}

module Cube_b4_objArgHandler( size, center, color                  // simple args
           , frame, site, actions, markPts, dim  // compound args
           , opt=[]
           )
{
   //echom("Cube()");
   
   //----------------------------------- simple args
   //// Assure size is [a,b,c]   
   _size = und(size, hash(opt, "size", 2) );
   size= isnum(_size)? [_size,_size,_size]
             : ispt(_size)? _size
             : randPts(3, d=[0,3]);

   //// Assure center is [a,b,c]              
   _center = und(center, hash(opt, "center", false) );
   center = isarr(_center)? _center : _center?[1,1,1]:false;
   
   //// Assure color is [name, transp]   
   cl = assurePair( und(color, hash(opt, "color", ["gold",0.9])), 0.9) ;
   site = und(site, hash(opt, "site",[]));
   
   //----------------------------------- compound args
   
   //// Frame    
   _frame = und(frame, hash(opt,"frame", true));
   frame = isnum(_frame)? ["r", _frame]: _frame;
   if(frame) Frame( pts, opt=frame);
   
   //// actions // In Cube(actions,opt), we have actions and opt.actions
                // actions will be appended to opt.actions, unless 
                // actions= false that disable any actions
   _actions = update( hash(opt,"actions",[]), actions?actions:[] );           
   
   //-----------------------------------   
   //// Create pts
   
   pts0= cubePts( size, center=center );
   _pts = actions==false?pts0: transPts( pts0, _actions ) ;
   pts = site? movePts(_pts, to=site): _pts;
      
   //// markPts
   markPtsOpt= und(markPts, hash(opt, "markPts", []));
   if(markPtsOpt)
   { indices = hash(markPtsOpt, "indices", undef);
     MarkPts( indices?get(pts,indices):pts, markPtsOpt);  
   }     

   //// dim 
   dims = concat( hash(opt,"dim",[]), dim?dim:[] );           
   if(dims) Dims(pts= pts, pqrs= hash(dims,"pqrs"), opt=dim);

   //-----------------------------------   
   // The builtin cube(... center) can't handle partial centering
   // So the *center* is dissolved into actions and handled by 
   // Transforms()
   actions2= concat( ["transl", [ center[0]? -size[0]/2 :0
                                 , center[1]? -size[1]/2 :0
                                 , center[2]? -size[2]/2 :0
                                 ]
                      ], actions==false?[]:_actions 
                   );
                      
   color(cl[0], cl[1])
   if(site)
     Move( to=site )
     Transforms( actions2 )
     cube(size);
   else
     Transforms( actions2 )
     cube(size);   
}

//

//. c-continued

//========================================================
CurvedCone=["CurvedCone","pqr, ops=[]"
   , "n/a", "Geometry"
, "Given a 3-pointer pqr and ops, make a curved 
;; cone, where ops is:
;;
;; [ ''r'' , 3  // rad of the curve
;; , ''r2'', 0.3  // rad of the bottom of the cone
;; , ''a'', undef // angle determining cone length
;;              // if not given, use a_pqr
;; , ''fn'', $fn==0?6:$fn // # of segments along the cone curve,
;; , ''fn2'', $fn==0?6:$fn // # of points along bottom circle 
;; ]
"];
module CurvedCone_test(ops=[]){
    doctest( CurvedCone, [], ops); }
    
module CurvedCone( pqr, ops=[] )
{
/*          	. R  						 	       _
             _-' -      
          _-'     :
       _-'         :     
    _-'            : 
  Q+---------------+ P



                R  |N (N is on pl_pqr, and NP perp to PQ )
             _-' : | 
          _-'     || 
       _-'        _|_ 
    _-'      _--'' | ''--_
  Q+--------{------P------}----- 
             ''-.__|__.-''
                   |      | 
   |<------------->|<---->|
           r           r2 
    
*/
    //_mdoc("CurvedCone","");
                     
	ops= concat( ops
    , [ "r" , undef  // rad of the curve this obj follows
	  , "r2", 0.5  // rad of the bottom of the obj 
      , "a", undef // angle determining cone length
                   // if not given, use a_pqr
      , "fn", $fn==0?6:$fn // # of segments along the cone curve,
                    // = # of slices or segments
      , "fn2", $fn==0?6:$fn // # of points along bottom circle
      ], OPS);

    function ops(k,df)=hash(ops,k,df);
    

    r= ops("r")==undef?d01(pqr):ops("r"); 
    echo("r", r, "d01(pqr)", d01(pqr));
    
    a = ops("a")? ops("a"): angle(pqr);
    P = pqr[0]; Q=pqr[1]; R= anglePt(pqr, a, len=r);
    
    r2= ops("r2");
    fn= ops("fn");
    fn2=ops("fn2");
    
    pqr = [P,Q,R];
   
    function getPtsByLayer(i)=
    (   let( // X is a point along the R->P curve
             X = anglePt( pqr
                  , a=(fn-i-1)*a/(fn)
                  , len=r )
           , N = N([Q,X,R])
           ) 
        arcPts( [Q,X,N]
                    , pl="xy"
                    , a=360
                    , count=fn2
                    , r=r2 /fn*(i+1)
                    )
    );

    // This for loop is for debug only. It displays
    // lines thru circling pts of each layer, and also
    // label pts
    //    for( i = range(fn) ){
    //       pts = getPtsByLayer(i); 
    //       Chain( pts, ["r",0.01]);
    //       MarkPts( pts, ["samesized",true, "r", 0.02] );
    //       LabelPts( pts, range( i*fn2+1, (i+1)*fn2+1 )
    //                , ["scale",0.02] ); 
    //    }    
    
    
    // pts: [ [p10,p11,p12...p16]  <== layer 1
    //      , [p20,p21,p22...p26]
    //      ...
    //      , [pi0,pi1,pi2...pi6] ] <==layer i    
    pts_layers = [ for( i = range(fn) )
                   getPtsByLayer(i) ];
    //echo("");
    //echo("pts_layers: ", pts_layers);
    //echo( "pts_layers = ", pts_layers );
        
    pts = [ for(a=pts_layers, b=a) b] ; 
       
    //echo("");
    //echo("pts: ", pts);
    //echo("pts = ", pts); 
        
    ptsAll = concat( [R], pts );
    //echo("");
    //echo("with R: ptsAll: ", ptsAll);
    
    /*
    faces of a raised pantagon :
    
    faces = [ [1,0,5,6]
            , [2,1,6,7]
            , [3,2,7,8]
            , [4,3,8,9]
            , [0,4,9,5]
            , [0,1,2,3,4]
            , [5,6,7,8,9]
            ];
            
    fn = 4
    fn2= 6
    layer i = range( fn)
    faces= 
    [ [1,6,12,7]    // i = 1  
   , [2,1, 7,8]                    
   , [3,2, 8,9]
   , [4,3, 9,10]
   , [5,4,10,11]
   , [6,5,11,12]
   
   , [7,12,18,13]  // i = 2, j= range(fn2)  
          [(i-1)*fn2+1, fn2*2, fn2*3, i*fn2+1) 
   , [8,7, 13,14] 
          [(i-1)*fn2+j+1, (i-1)*fn2+j, i*fn2+j, i*fn2+j+1) 
   , [9,8,14,15]     
   , [10,9,15,16]
   , [11,10,16,17]
   , [12,11,17,18]
   
   , [13,18,24,19] // layer = 3
   , [14,13,19,20]
   , [15,14,20,21]
   , [16,15,21,22]
   , [17,16,22,23]
   , [18,17,23,24] 
   ]
            
    Last 2 are easy to make. Prior to that,         
    we make a "preface" first then transpose it:
       
       [ [1,2,3,4,0]  : concat( range(1,5), [0])
       , [0,1,2,3,4]  : range( 0, 5 )
       , [5,6,7,8,9]  : range( 5, 5+5 )
       , [6,7,8,9,5]  : concat( range(6,5+5), [5]) 
       ]
    ---------------------   
    If there are more than one layer, the above
    is the layer 0. Each layer will inc by 5:
    
    i=1:
       [ [5,6,7,8,9]  : range( 5, 5+5 )
       , [10,11,12,13,14]: range( 5+5, 10+5 )
       , [11,12,13,14,10]: concat( range(5+6,10+5), [10]) 
       , [6,7,8,9,5]  : concat( range(5+1,10), [5]) 
       ]
    
    layer i, fn:
    
        [ concat( range( i*fn+1, (i+1)*fn ), [fn] )
        , range( i*fn, (i+1)*fn )
        , range( (i+1)*fn, (i+2)*fn )
        , concat( range((i+1)*fn+1,(i+2)*fn),[i*fn] )
        ]
    */    
    
    // Faces w/o the first layer (which contains
    // triangles) and the bottom layer
//    function getSideFacesByLayer(i, fn, hasnext=false)=
//      transpose(
//        [ concat( range( i*fn+1, (i+1)*fn ), [fn] )
//        , range( i*fn, (i+1)*fn )
//        , range( (i+1)*fn, (i+2)*fn )
//        , hasnext? range((i+1)*fn+1,(i+2)*fn+1) 
//          : concat( range((i+1)*fn+1,(i+2)*fn),[i*fn] )
//        ]
//      )
//      ;
    
    // Layer0 contains triangles
    // 
    //     0
    //     | 
    //  _4-|-3_
    // 5_  +  _2
    //   -6-1-
    //
    // faces= [ [0,2,1]
    //        , [0,3,2] 
    //        , ... 
    //        , [0,6,5]
    //        , [0,1,6]
    //        ]
    
    function getLayer0Faces(fn)=
      [ for( i =range(fn-1) )
        [ 0, i>fn-3?1:i+2, i+1]
      ];  

    
    sidefaces = getTubeSideFaces
      ( [ for (i=range(1, fn+1))
          [ for (j=range(fn2)) (i-1)*fn2+j+1 ]
        ]      
      , isclockwise=true 
      );
    //echo("");      
    //echo("\nsidefaces = ",sidefaces);
                
    faces = mergeA( 
            [getLayer0Faces(fn2+1)
            , mergeA( sidefaces )  
            ,  [range((fn-1)*fn2+1, fn*fn2+1)] 
            ]);                
    //echo("");            
    //echo( "faces: ", faces);
    //echo("");
       
    //color("green", 0.8)            
    polyhedron( points= ptsAll
              , faces = faces );               
}

module CurvedCone_demo_1(){
  _mdoc("CurvedCone_demo_1"
  ," yellow: CurvedCone( randPts(3) )
  ;; red  : a=60, r=8, r2=1,  fn= 4, fn2=6 
  ;; green: a=30, r=8, r2=0.5, fn=10, fn2=20 
  ;; blue : a=120,r=3, r2=1  , fn= 9, fn2=4 
   ");    

    pqr = randPts(3);
    MarkPts( pqr, ["labels","PQR"] ); Chain(pqr, ["closed",false]); 
    
    CurvedCone( pqr );
    LabelPt( midPt(p12(pqr)), "CurvedCone(pqr)");
    
    color("red", 0.6)
    CurvedCone( randPts(3), ["a",60
                     , "r", 8
                     , "r2", 1
                     , "fn",4 
                     , "fn2",6 
                     ] );
                     
    color("green", 0.6)
    CurvedCone( randPts(3), ["a",30
                     , "r", 8
                     , "r2", 0.5
                     , "fn",10
                     , "fn2",20
                     ] );
    
    color("blue", 0.6)
    CurvedCone( randPts(3), ["a",120
                     , "r", 3
                     , "r2", 1
                     , "fn",9
                     , "fn2",4
                     ] );    
}    

//CurvedCone_demo_1();


//========================================================
//cycleRange=["cycleRange","pts/i, 


//. Cylinder

module Cylinder( h,r,r1,r2,center,d,d1,d2,$fn, $fa, $fs
               , color, echoPts
               , site, actions
               , Frame=true, MarkPts, Dim, opt=[] )
{
   echom(_red("Cylinder()"));
   
   //---------------------------------------------------
   // getTransformData():
   // Getting the data for transformations (actions,size,etc)
   //
   // This would be used in transforming the obj in the TransformObj()
   // Ideally we could have skip this and do all the setting handlings
   // in TransformObj(). However, we also need this to create pts for
   // framing, pt tracking and pt decorations. So we use getTransformData 
   // as a bridge.  
   _transdata= getTransformData( shape="cylinder"
                         , h=h
                         , r=r
                         , r1=r1
                         , r2=r2
                         , center= centerArg(center, opt, df=110)
			 , d=d
                         , d1=d1
                         , d2=d1
                         , $fn=$fn
                         , $fa=$fa
                         , $fs=$fs
                         , site=site
			 , actions=actions
			 , opt=opt
			 );
   //echo(transdata=transdata);			               	               
   pts = hash( _transdata, "pts" );
   //echo(pts = pts);
   //MarkPts(pts);
   
   //echo(transdata = _fmth( transdata ) );
   //echo(transdata = transdata, size=hash(transdata, "size"), actions=hash(transdata, "actions"));

   
   _color= colorArg(color, opt);
   
   // We need to set these values in the module scope 
   // to prevent WARNING: $fs too small - clamping to 0.010000             
   $fn = hash( _transdata, "$fn" );
   $fs = hash( _transdata, "$fs" );
   $fa = hash( _transdata, "$fa" );
   
   //echo( center = hash(transdata, "center") );
   //echo( center = centerArg(center, opt) );
   //echo( opt=opt );
   
   transdata= update(_transdata, ["echoPts", arg(echoPts,opt,"echoPts",-1)]);
   
   Color( _color )
   TransformObj( transdata ) 
     cylinder( h=hash( transdata, "h") 
             , d1=hash( transdata, "d1")
             , d2=hash( transdata, "d2") 
             , $fa=$fa 
             , $fs=$fs 
             , $fn=$fn 
             );
             

   //---------------------------------------------------
   // Now we do the pt decorations:
   //
   DecoratePts( pts 
              , Frame= Frame==true?["shape","rod"]
                       : Frame? update( Frame, ["shape","rod"])
                       : false
              , Frame_df = [ "r", 0.005, "color", "black"     
                           , "Joint", ["shape","ball","$fn",4]
                           , "shape",  is0( hash(transdata,"d2"))?"cone"
                                      :is0( hash(transdata,"d1") )?"conedown":undef
                           ]
              , MarkPts=MarkPts, MarkPts_df= ["color", _color]
              , Dim=Dim, Dim_df=["color", _color]
              , opt=opt 
              );
          

  /*
   //----------------------------------- simple args
               
   ptsData= cylinderPtsHandler( h,r,r1,r2,center,d,d1,d2,$fn, $fa, $fs
                              , center, actions, site, opt, debug=false);
   pts = hash( ptsData, "pts" );                             
   
   decoData = objDecoHandler( color=color
                     , frame= frame
                     , markPts=markPts
                     , dim=dim
                     , opt = update( opt
                                   , [ "frameShape"
                                     , is0( hash(ptsData,"d2"))?"cone"
                                       :is0( hash(ptsData,"d1") )?"conedown":undef
                                     ]
                                   )
                     ) ; 
   //echo(ptsData=ptsData); //pts=pts);//, decoData=decoData);
   
   decoratePts( pts, opt=decoData );
       
   // We need to set these values in the module scope 
   // to prevent WARNING: $fs too small - clamping to 0.010000             
   $fn = hash( ptsData, "$fn" );
   $fs = hash( ptsData, "$fs" );
   $fa = hash( ptsData, "$fa" );
   
   Color( decoData )
   TransformObj( opt= ptsData ) //actions=actions,site=site)
     cylinder( h=hash( ptsData, "h") 
             , d1=hash( ptsData, "d1")
             , d2=hash( ptsData, "d2") 
             , $fa=$fa //hash( ptsData, "$fa", undef) // using hash to grab here gets "too small" error
             , $fs=$fs //hash( ptsData, "$fs", undef)
             , $fn=$fn //hash( ptsData, "$fn", undef)
             );
       
   //echo(h=h,r=r,r1=r1,r2=r2,d=d,d1=d1,d2=d2,center=center,actions=actions);
   */
}

//module _Cylinder(h,r,r1,r2,center,d,d1,d2,$fn, $fa, $fs
//               , color, site, actions, frame, markPts, dim, opt=[] )
//{
//  opt= update( [ "h", 1
//               , "r", 1
//               , "center", false
//               , "$fa", 0.01
//               , "$fs", 0.01
//               , "$fn", 8
//               
//               , "color", "gold"
//               
//               , "frame", true    // true/false | 1 | <frame opt>
//               , "site", undef   // pqr
//               , "actions", undef
//               , "markPts", undef // <markPts opt>
//               , "dim", undef     // dim=[ "pqr", <3indices|3pts> 
//                                  //       , <rest of dim opt>   ]
//                                  // dims: dim | [dim, dim ...]      
//               ]
//               , opt
//               );
//  
//   //----------------------------------- simple args
//               
//   h = h!=undef?h: hash(opt,"h");
//   
//   // argument priority: d1,d2 > r1,r2 > d > r
//   _r = r!=undef?r: hash(opt, "r");
//   d = d!=undef?d: hash(opt, "d", _r*2);
//   _r1= r1!=undef?r1: hash(opt, "r1", d/2);
//   _r2= r2!=undef?r2: hash(opt, "r2", d/2);
//   d1= d1!=undef?d1: hash(opt, "d1", _r1*2);
//   d2= d2!=undef?d2: hash(opt, "d2", _r2*2);
//   
//   r= d==undef?r:undef;
//   r1= d1==undef?r1:undef;
//   r2= d2==undef?r2:undef;
//   
//   center = center!=undef?center:hash(opt,"center", false);
//                 
//   $fn= $fn!=undef?$fn:hash(opt, "$fn");
//   $fs= $fs!=undef?$fs:hash(opt, "$fs");
//   $fa= $fa!=undef?$fa:hash(opt, "$fa");
//
//   //// Assure color is [name, transp]
//   cl = assurePair( und(color, hash(opt, "color", ["gold",0.9])), 0.9) ;
//   //echo(color=color, cl=cl);
//   site = und(site, hash(opt, "site",[ [1,0,0], ORIGIN, [-1,1,0]]));
//   //v = [ site[1], N(site) ];
//    
//   //// actions // We have actions and opt.actions
//                // actions will be appended to opt.actions, unless 
//                // actions= false that disable any actions
//   _actions = actions==false?[]:update( hash(opt,"actions",[]), actions?actions:[] );           
//    
//   //----------------------------------- compound args
//   
//   //// Frame
//   _frame = und(frame, hash(opt,"frame"));
//   frame = isnum(_frame)? ["r", _frame]: _frame;
//   if(frame) 
//      Frame( pts, shape= is0(d2)?"cone":is0(d1)?"conedown":undef
//           , opt=frame);
//   
//   //// markPts
//   markPtsOpt= und(markPts, hash(opt, "markPts", []));
//   
//   
//   //-----------------------------------
//   //echo(actions=actions, _actions=_actions);
//   //// Create pts
//   pts0= cylinderPts( h=h,r=r,r1=r1,r2=r2,center=center
//                    , d=d,d1=d1,d2=d2,$fn=$fn, $fa=$fa, $fs=$fs
//                    , opt= update(opt, ["actions", _actions])
//                    );
//   pts = site? movePts(pts0, to=site): pts0;
//   
//   
//   //// MarkPts
//   if(markPtsOpt)
//   { indices = hash(markPtsOpt, "indices", undef);
//     MarkPts( indices?get(pts,indices):pts, markPtsOpt);  
//   }  
//   
//   color(cl[0], cl[1])
//   if(site)
//     Move( to=site )
//     Transforms( _actions )
//     cylinder(h=h, r=r, r1=r1, r2=r2,d=d,d1=d1,d2=d2,center=center
//                      ,$fa=$fa,$fs=$fs,$fn=$fn);  
//   else
//     Transforms( _actions )
//     cylinder(h=h, r=r, r1=r1, r2=r2,d=d,d1=d1,d2=d2,center=center
//                      ,$fa=$fa,$fs=$fs,$fn=$fn);  
//   
//   dims = concat( hash(opt,"dim",[]), dim?dim:[] );           
//   if(dims) Dims(pts= pts, pqrs= hash(dims,"pqrs"), opt=dim);   
//       
//   //echo(h=h,r=r,r1=r1,r2=r2,d=d,d1=d1,d2=d2,center=center,actions=actions);
//}


//. d, f

//========================================================
DashBox=[ "DashBox", "pq,ops", "n/a", "3D, Shape", 
 " Given a 2-pointer (pq) and ops, make a box with dash lines parallel
;; to axes. The default ops is:
;;
;;   [ ''r'',0.005
;;   , ''dash'', 0.05
;;   , ''space'', 0.05
;;   , ''bySize'', true
;;   ]
;;
;; where r= radius of dash line, dash= the length of dash, space= space
;; between dashes. The p,q are the opposite corners of the box if bySize=
;; false. If bySize=true, p is a corner, and q contains size of box on
;; x,y,z direction.
"];  

module DashBox(pq, ops)
{
	ops=update(
		[ "r",0.005
		, "dash", 0.1
		//, "space", 0.05
		, "bySize", true
		], ops );
	function ops(k)= hash(ops,k);

	echo( ops("bySize"));

	pts = boxPts( pq, bySize= ops("bySize") );

	r= ops("r");

	color("blue"){
		Line( [ pts[0], pts[4] ], ops);
		DashLine( [ pts[1], pts[5] ], ops );
		DashLine( [ pts[2], pts[6] ], ops );
		DashLine( [ pts[3], pts[7] ], ops );
	}
	color("red"){
		DashLine( [ pts[0], pts[1] ], ops);
		DashLine( [ pts[2], pts[3] ], ops );
		DashLine( [ pts[4], pts[5] ], ops );
		DashLine( [ pts[6], pts[7] ], ops );
	}
	color("lightgreen"){
		DashLine( [ pts[0], pts[3] ], ops);
		DashLine( [ pts[1], pts[2] ], ops );
		DashLine( [ pts[4], pts[7] ], ops );
		DashLine( [ pts[5], pts[6] ], ops );
	}
}


module DashBox_test( ops ) {	doctest( DashBox, ops=ops); }

module DashBox_demo(pq, bySize=true)
{
	pq = [ [2,1,-1],[3,2,1]];
	DashBox( pq , [["bySize", bySize]]);

	//boxPts_demo( pq, bySize );
	MarkPts( boxPts( pq ) );

}
//DashBox_demo(bySize=true);
//doc(DashBox);


//========================================================
DashLine=["DashLine","pq,ops", "n/a", "3D, Shape", 
 " Given two-pointers(pq) and ops, draw a dash-line between pq.
;; Default ops:
;;
;;   [ ''r''     , 0.02     // radius
;;   , ''dash''  , 0.5      // dash length
;;   , ''space'' , undef    // space between dashes, default=dash/3
;;   , ''color'' , false
;;   , ''transp'', false
;;   , ''fn''    , $fn
;;   ]
"];

module DashLine(pts, r, dash, space, color, opt=[])
{
    //echom("DashLine:");
    r= arg(r, opt, "r", 0.01);
    dash= arg(dash, opt, "dash", 0.025);
    space= arg(space, opt, "space", 0.025);
    color= colorArg(color, opt, ["black",.5]);
    //echo(color=color);
    module DrawDashSegment( _i=0, residule=0 )
    {
       pq= [pts[_i], pts[_i+1]];
       s = dash+space;
       L = dist( pq );
       n = floor((L-residule)/s)+1;
       _pts_this_seg = [ for(i=range(n))
                         onlinePt( pq, len= residule+i*s )
                       ] ; 
       for(i= [0:2:len(_pts_this_seg)-1])
       {
         //echo(i=i, [_pts_this_seg[i], _pts_this_seg[i+1]]);
         if( _pts_this_seg[i+1])
          Line( [_pts_this_seg[i], _pts_this_seg[i+1]], r=r, color=color);
       }
       if((L-residule)%s>0)
       {
          //echo(residule = (L-residule)%s);
          //echo(last2= [ onlinePt( get(_pts_this_seg,[-1,-2]), len=(L-residule)%s)
          //      , last(_pts_this_seg)
          //      ]);
          Line( [ onlinePt( get(_pts_this_seg,[-1,-2]), len=(L-residule)%s)
                , last(_pts_this_seg)
                ], r=r, color=color);
       }
    }	
 
    for( i = range(len(pts)-1) ) DrawDashSegment(i);
}


module DashLine_test(ops){ doctest(DashLine,ops=ops); }

module DashLine_demo(){

	pts= randPts(5);
	DashLine( pts); 
	MarkPts(pts, Line=false);
	
	pts2 = randPts(2);
	DashLine( pts2, ops=["dash",0.02, "space",0.06] );
	//MarkPts(pts2);
}
//DashLine_demo();
//DashLine_test();
//doc(DashLine);

//module DecorateObj( color, Frame,MarkPts, Dim, opt=[] )
//{
//
//
  //children();
//}

/*
function objDecoHandler( color, Frame,MarkPts, Dim, opt=[] )=
(           
  //let( _Frame = und_opt(Frame, opt,"Frame", 0.01)
     //, Frame = isnum(_Frame)? ["r", _Frame]: _Frame
     //) // let
//
  //[ "color", getColorArg(color, opt) // Assure color is [name, transp]
  //, "Frame", update( Frame, ["shape", hash(opt, "FrameShape","rod")] )
  //, "MarkPts", subprop(MarkPts, opt, "MarkPts", []) 
  //, "Dim", concat( hash(opt,"Dim",[]), Dim?Dim:[] )
  //]
  //

  [ "color", getColorArg(color, opt) // Assure color is [name, transp]
  , "Frame", subprop( Frame, opt, "Frame"
                    , dfPropName="r"
                    , dfPropType="num"
                    , df=["r",0.01, "FrameShape", "rod"]
                    )
                    //, df=["shape", hash(opt, "FrameShape","rod")] )
  , "MarkPts", subprop(MarkPts, opt, "MarkPts", []) 
  , "Dim", concat( hash(opt,"Dim",[]), Dim?Dim:[] )
  ]
  
);
*/

module DecoratePts(pts, color
                  , faces 
                  , Frame, MarkPts, Dim
                  , Frame_df, MarkPts_df, Dim_df
                  , opt=[] )
{
   //echom("DecoratePts()");
   _color= colorArg(color, opt);
   _Frame= subprop(Frame, opt, "Frame", dfPropName="r", dfPropType="num", Frame_df );
   _MarkPts= subprop(MarkPts, opt, "MarkPts", MarkPts_df );
   _Dim= subprop(Dim, opt, "Dim", Dim_df );
   
   //echo(_Frame=_Frame, _MarkPts=_MarkPts, _Dim=_Dim);
   //MarkPts(pts);
   //Color(color, opt=opt)
   //{
     if(_Frame) Frame( pts, opt=_Frame);
     //if(_Frame) DrawEdges( [pts, faces], opt=_Frame);
   
     if(_MarkPts) {
        //echo(_MarkPts = _MarkPts);
        MarkPts( pts, opt=_MarkPts );
     }     
     if(_Dim) Dims(pts= pts, pqrs= hash(_Dims,"pqrs"), opt=_Dim);
   //}
}


DRAWEDGES_OPT=[];


module DrawEdges(pf, pts,faces,r, color, $fn, issort=false, uniq=false, opt=[])
{
  // 2018.5.23:
  // -- To replace Frame in the future
  // -- Different between DrawEdges(pts,faces) and MarkPts(pts, Line=...) 
  //    ** DrawEdges needs faces, draw only edges
  //    ** MarkPts-Line doesn't need faces, draw depends on order of pts
  // 2018.5.27:
  // -- DrawEdges(pf, pts,faces) instead of DrawEdges(pts,faces)
  // -- Also note that get_edges() is now defaulted to have duplicated
  //    and unsorted edges (for the purpose of speed)
  opt = update(DRAWEDGES_OPT,opt);
  pts=pts?pts:pf[0];
  faces=faces?faces:pf[1];
  r = arg(r, opt, "r", 0.01);
  
  color= colorArg(color,opt,"black");
  
  egs = get_edges(faces, issort=false, uniq=false );
  //echo(_red(str("In DrawEdges, egs= ", str(egs), ", r=", r)));
  for(eg=egs) 
  {
     Line( pts=get(pts,eg), r=r,color=color,$fn=$fn,opt=opt );
  }
}



module Frame_failed_180112(pts, shape, r, color, closed, grid, joint, $fn, opt=[]) 
{
    // 2018.1.12: rewrite using new und_opt()
    //
    //  2017.10.18: Excellent re-write, showing:
    //  (1) how easy it is when argumtns are packed in an "opt"
    //  (2) how default arguments are displayed in the code,
    //      which could serve as a standard pattern
    
    // joint: the sphere obj in the corners
    // grid : the vertical lines in the graph below
    //
    //           _6-_
    //        _-' |  '-_
    //     5 '_   |     '-7
    //       | '-_|   _-'| 
    //       |    |-4'   |
    //       |   _-_|    |
    //       |_-' 2 |'-_ | 
    //      1-_     |   '-3
    //         '-_  | _-'
    //            '-.'
    //              0
//    opt= update(  [ "shape", "rod"
//                  , "r",0.01
//                  , "$fn", 16
//                  , "color","black"
//                  , "closed", true
//        
//                  , "grid", true
//                  , "joint", true   //// The setting of "joint" is taken care of in Line()
//                  ] 
//                , opt==true?[]
//                  : isnum(opt)?["r", opt]
//                  : opt
//                );
    pts= und_opt(pts, opt, "pts",[]);
    shape= und_opt(shape, opt, "shape","rod");
    
    opt= update( opt, [ "r", und_opt(r, opt, "r",0.01)
         , "$fn", und_opt( $fn, opt, " $fn")
         , "color", getColorArg(color, opt, "black")
         , "closed", und_opt(closed, opt, "closed",true)
         , "joint", und_opt(joint, opt, "joint",true)
           // The setting of "joint" is taken care of in Line()
         ]
         );
    _grid = [grid, hash(opt,"grid")];
    grid = _grid==[undef,undef]? opt
           : _grid[0]==false || _grid==[undef,false] ? false
           : _grid==[undef,true]? opt
           : updates( opt, hash(opt,"grid",[]), grid==true?[]:grid );      
    //_grid= und_opt( grid, opt, "grid", opt); // grid=und, opt.grid=und: use opt
    //grid = update()
    echo(_grid=_grid, grid=grid);
    
    //grid!=false && hash(opt,"grid")!=false ?
    //      updates( opt, [hash(opt, "grid", []), grid?grid:[]] )
    //      : false ;
                                     
                                     
    
//    r= und_opt(r, opt, "r",0.01);
//    closed= ;
//    joint= und_opt(joint, opt, "joint",true);
//     $fn= ;
                
    //echo("in Frame, opt=", opt);
    //r = r!=undef?r: hash( opt, "r" );
    //col = assurePair( color!=undef?color:hash(opt, "color"),1);
    //closed = und(closed, hash(opt, "closed" ) );
    //shape = und(shape, hash(opt, "shape"));
    //echo(color=color);
    
    
    L = len(pts);
    
//    _grid= hash(opt,"grid");
//    df_grid= [ "r", hash(opt,"r")
//             , "$fn", hash(opt, "$fn")
//             , "color", assurePair(hash(opt, "color"),1)
//             ];
//    grid = isarr(_grid)? update(df_grid,_grid): df_grid;
//    
    //echo(shape=shape);
    
    if(shape=="rod")
    {
      Line( slice(pts,0,L/2), opt= opt );
      Line( slice(pts,L/2), opt=opt );
    
       if( grid )
       {
         for(i = [0:L/2-1] ) 
          Line( [pts[i], pts[i+L/2]], opt=grid);
       }  
    }
    else if (shape=="cone")
    {
      Line( slice(pts,0, L-1), opt=opt );
    
        if( grid )
       {
          for(i = [0:L-2] ) 
          Line( [pts[i], last(pts)], opt=grid);
       }    
    }
    else if (shape=="conedown")
    {
      Line( slice(pts,1, L), opt=opt );
    
        if( grid )
       {
          for(i = [1:L-1] ) 
          Line( [pts[i], pts[0]], opt=grid);
       }    
    }
       
}

module Frame(pts, shape, r, color, indices, Joint, $fn, opt=[]) 
{
    // joint: the sphere obj in the corners
    // grid : the vertical lines in the graph below
    //
    //           _6-_
    //        _-' |  '-_
    //     5 '_   |     '-7
    //       | '-_|   _-'| 
    //       |    |-4'   |
    //       |   _-_|    |
    //       |_-' 2 |'-_ | 
    //      1-_     |   '-3
    //         '-_  | _-'
    //            '-.'
    //              0

    //echom("Frame()");
    
    L = len(pts);
    
    shape= arg(shape, opt, "shape", "rod");
    r= arg(r, opt, "r", LINE_R/3);
    indices = arg(indices, opt, "indices");
    opt = haskey(opt,"closed")?opt:update(opt,["closed",true]);
    _color= colorArg(color,opt, df="black");
    
    //$fn = arg($fn, opt, "$fn", 4);
    //col = colorArg(color, opt);
    //Joint = subprop(Joint, opt, "Joint"); 
    
    //echo(shape=shape);
    
       _Joint= subprop(Joint, opt, "Joint", ["color",_color, "r", r]);
    
    module _Line(pts)
    {
       Line(pts, color=_color
           , r=r, $fn=$fn, Joint=_Joint, opt=opt );
    }
    
    if(indices)
    {
      for(x=indices) _Line( get(pts,x) );
    }
    else if(shape=="rod")
    {
      _Line( slice(pts,0,L/2) );
      _Line( slice(pts,L/2) );
    
      for(i = [0:L/2-1] ) 
          _Line( [pts[i], pts[i+L/2]] );
 
    }
    else if (shape=="cone")
    {
      _Line( slice(pts,0, L-1) );
      for(i = [0:L-2] ) 
          _Line( [pts[i], last(pts)]);
   
    }
    else if (shape=="conedown")
    {
      _Line( slice(pts,1, L) );
      for(i = [1:L-1] ) 
          _Line( [pts[i], pts[0]]);
    }
    else if (shape=="tube")
    {
      //echo("shape=tube");
      nside= len(pts)/4;
      
      for(i=[0:4])
      { 
         //echo("------------", i=i);
          _Line( [for(j=[0:nside-1]) //echo( i= nside*i+j)    
                                    pts[nside*i+j] ] ); 
      }
      for(i=[0:nside-1]) 
      { _Line( [pts[i], pts[i+nside]]);
        _Line( [pts[i+nside*2], pts[i+nside*3]]);
      }
    }
       
}

module Frame_old(pts, shape, r, color, closed, grid, joint, $fn, opt=[]) 
{
    //  2017.10.18: Excellent re-write, showing:
    //  (1) how easy it is when argumtns are packed in an "opt"
    //  (2) how default arguments are displayed in the code,
    //      which could serve as a standard pattern
    
    // joint: the sphere obj in the corners
    // grid : the vertical lines in the graph below
    //
    //           _6-_
    //        _-' |  '-_
    //     5 '_   |     '-7
    //       | '-_|   _-'| 
    //       |    |-4'   |
    //       |   _-_|    |
    //       |_-' 2 |'-_ | 
    //      1-_     |   '-3
    //         '-_  | _-'
    //            '-.'
    //              0
    opt= update(  [ "shape", "rod"
                  , "r",0.01
                  , "$fn", 16
                  , "color","black"
                  , "closed", true
        
                  , "grid", true
                  , "joint", true   //// The setting of "joint" is taken care of in Line()
                  ] 
                , opt==true?[]
                  : isnum(opt)?["r", opt]
                  : opt
                );
                
    //echo("in Frame, opt=", opt);
    r = r!=undef?r: hash( opt, "r" );
    col = assurePair( color!=undef?color:hash(opt, "color"),1);
    closed = und(closed, hash(opt, "closed" ) );
    shape = und(shape, hash(opt, "shape"));
    
    L = len(pts);
    
    _grid= hash(opt,"grid");
    df_grid= [ "r", hash(opt,"r")
             , "$fn", hash(opt, "$fn")
             , "color", assurePair(hash(opt, "color"),1)
             ];
    grid = isarr(_grid)? update(df_grid,_grid): df_grid;
    
    //echo(shape=shape);
    
    if(shape=="rod")
    {
      Line( slice(pts,0,L/2), opt= opt );
      Line( slice(pts,L/2), opt=opt );
    
       if( _grid )
       {
         for(i = [0:L/2-1] ) 
          Line( [pts[i], pts[i+L/2]], opt=grid);
       }  
    }
    else if (shape=="cone")
    {
      Line( slice(pts,0, L-1), opt=opt );
    
        if( _grid )
       {
          for(i = [0:L-2] ) 
          Line( [pts[i], last(pts)], opt=grid);
       }    
    }
    else if (shape=="conedown")
    {
      Line( slice(pts,1, L), opt=opt );
    
        if( _grid )
       {
          for(i = [1:L-1] ) 
          Line( [pts[i], pts[0]], opt=grid);
       }    
    }
       
}



//. L

//========================================================
LabelPt=["LabelPt", "pt, text, indent, fmt 
              , size,font,halign,valign,spacing,direction,language,script,$fn
              , scale, color, actions, site, opt=[]", "n/a", "Geometry, Doc",
 " Write the given text next to the point pt. If text not given, the pt is shown.
;;  
;; The text is written according to the viewpoint ($vpr) such that it is always 
;; facing the viewer. 
;; 
;; indent: a num as the distance between the pt and text.
;; fmt: format (see _f and _s2)
;;
;; Other properties of text are the same as that of Text() 
"];

module LabelPt_test( opt=["mode",13] ) { doctest( LabelPt, ops=ops ); }

 
module LabelPt(pt, text, indent, fmt, followView
              // args below are for Text   
              , size,font,halign,valign,spacing,direction,language,script,$fn
              , scale, color, actions, site, opt) 
{
   /*
     2020.6.7: 
                Serious error: LabelPt cannot label at the pt. 
     
     Should we retire this and use MarkPt and/or Text instead ? 
     Both MarkPt and LabelPt use Text()
          
      Compare LabelPt, Text and MarkPt:

      args:  LabelPt  Text MarkPt
      =============================
      pt          y    -      y
      text        y    y    Label
      indent      y    -    (packed)
      format     fmt format  
      followView  y    -    (packed)       

      size       y     y 
      font       y     y
      halign     y     y
      valign     y     y 
      spacing    y     y
      direction  y     y
      language   y     y
      script     y     y
      $fn        y     y
      scale      y     y
      color      y     y
      actions    y     y 
      site       y     y  
      opt        y     y    opt


     
     
   */  

   
    echom("LabelPt()");
    
    pt = arg(pt, opt, "pt");
    echo( "pt= ", pt);
    indent= arg(indent, popt(opt), "indent", 0.05);
    
    followView= arg( followView, opt, "followView", true);
    
    _actions= updates( [ "x", indent]
                       , [ followView? ["rot",$vpr] : argx(actions,opt,"actions",[])
                         , ["t", pt]
                         ] 
                     );
    
    //opt=updatekey(opt,"actions", ["x", indent, "rot",$vpr, "t",pt], newfirst=true);
    _text0 = subprop( text, opt, "text", dfPropName="text", dfPropType=["str","nums"]
           // , dfsub= ["color",_col] //, "fmt", "{*`a|d2|aj=,}"]
           );
    textOpt = ishash(_text0)? 
            updatekey( _text0, "actions"
                     , updates( [ "x", indent]
                                , [ followView?["rot",$vpr]:argx(actions,opt,"actions",[])
                                  , ["t", pt]
                                  ]
                              )
                     ) :_text0;
    _text= hash(textOpt,"text", pt);                             
    //_text0 = subprop(text, opt, "text", df=pt);
    //_text = _text0==true?pt:_text0;
    //echo(scale=scale, _scale=_scale, opt=opt);
    fmt = arg(fmt, opt, "fmt", ispt(_text)?"{*`a|d2|aj=,}":"");
    label = fmt?_s2(fmt,_text):str(_text);
    echo(_actions=_actions, text=text,   _text=_text, textOpt=textOpt); // label=label);
    
    _scale = scaleArg(scale,opt,1)*0.2;  // Text's scale is SCALE=0.1. We set the default
                                      // scale of LabelPt to 0.25 so the final scale 
                                      // for the output text is 0.1*0.25= 0.025
    _opt = delkeys(opt, ["scale","actions"]); // NEED to del "scale" from opt otherwise the scaling
                                 // set in the opt will be applied twice.
    
    //echo(text=text, fmt=fmt, label=label, _scale=_scale, _actions=_actions, _opt=_opt);
    //Text(label, opt=textOpt);
    Text(label, size,font,halign,valign,spacing,direction,language,script,$fn
         , _scale, color, _actions, site, opt=delkeys(textOpt,["scale","actions"])); 
    echo( _s("Label={_}, _actions={_}", [label, _actions] ) );      
         
}

module LabelPt_old(pt,text="", opt=[]) 
{
    //if(ECHO_MOD_TREE) echom("LabelPt", mti);
    
    df_opt= [ "scale", 0.03
            , "tmpl", "" //{_`a|d2|aj=,}"
            , "isxyz", undef // label pt with coordinate
            , "shift", undef  // could be: 2.5, [3,1,0] 
            ];
    
    opt = update( df_opt, opt
                , df=["sc","scale","sh","shift"]);
    
    //echo( opt=opt);
    isxyz = hash( opt, "isxyz");
    tmpl = or( hash( opt,"tmpl")
             , isxyz?"{*`a|d2|aj=,}":"");
	  scale = hash(opt,"scale")*mm;
    _label= isxyz? pt
            :hash(opt, "text", text);
    label = tmpl?_s2(tmpl,_label):str(_label);
    
    _shift = or(hash(opt, "shift"), [1,1,1]*scale*3 );
	  shift = isarr(_shift)?_shift:_shift*[1,1,1] ;

    //echo(pt=pt, label=label, tmpl=tmpl, shift=shift);
    
//	echo("_shift", _shift, "shift", shift, "scale", scale);
//	echo("pt",pt,"label: ", label);
//	echo("LabelPt opt", opt);
	translate( pt+ shift ) 
		rotate($vpr) 
        translate( [-scale*4,-scale*5,0])
		scale( scale ) 
		color( hash(opt,"color"), hash(opt,"transp") ) 
		text( label
            , font="Liberation Mono" );
		//drawtext( str(label) );	
}
//echo($vpr=$vpr);
//translate([-0.03*4,-.15,0]) scale(0.03) text("0");
//LabelPt( [2,3,5], opt=["label","x"] );

//translate( [2,3,5] ) text("test"); 


////========================================================
//LabelPts=["LabelPts", "pts,labels,opt=[]", "n/a", "Geometry, Doc",
// " Given points (pts), labels (array of strings or a string) and option
//;; (opt), write the
//;; label next to the point. The label is written according to the 
//;; viewpoint ($vpr) such that it is always facing the viewer. Other
//;; properties of label are decided by opt. 
//;;
//;; New args 2014.8.16: 
//;;
//;;   prefix, suffix, begnum, usexyz
//;;
//"];
//
//module LabelPts_test( opt=["mode",13] ) { doctest( LabelPts, opt=opt ); }
//
//
//module LabelPts( pts, labels="", opt=[], mti=0 )
//{
//	echom("LabelPts", mti);
//    
//	opt = update(
//      [ "color", [0.4,0.4,0.4]
//	  //, "colors", COLORS
//	  , "isxyz",false
//      , "tmpl", ""
//      ], opt );
//	function opt(k,nf)= hash(opt, k, nf);
//    isxyz  = opt("isxyz");
//    color  = opt("color"); 
//    colors = opt("colors");
//    labels = opt("labels", labels);
//    //tmpl   = opt("tmpl");
//    
//	circle =concat([get(pts,-1)], pts, [pts[0]]);
//
//    for (i=[0:len(pts)-1])	{
//    
//        pt = len(pts)<3? pts[i]
//             : onlinePt( [ pts[i]
//                         , angleBisectPt( 
//                            [ circle[i], circle[i+1], circle[i+2] ])
//                         ], len=0
//                       );
//             
//        echo(pts_i=pts[i], pt=pt);           
//        LabelPt( pts[i]
//            , labels[i] //getlabel(i) //labels?labels[i]: getlabel(i)
//            , opt= update( opt, ["color", or(opt("colors")[i], color)]
//                         )
//            , mti=mti+1 ); 
//	}//for
//}
//
//
//module LabelPts_demo_1()
//{
//    echom("LabelPts_demo_1");
//    
//	pqr = randPts(3);
//    echo(pqr=pqr);
//	MarkPts( pqr, ["r", 0.05] );
//	LabelPts( pqr, "PQR" );
//	Plane( pqr, ["mode",3] );
//    
//    MarkPt( P(pqr) );
//    LabelPt( P(pqr), ["isxyz",true]);
//    
//    
////	lmn = randPts(3);
////	MarkPts( lmn, ["r",0.05] );
////	Plane( lmn, ["mode",3] );
////	LabelPts( lmn, ops=["color","red"]);
//}
////LabelPts_demo_1();
//
//module LabelPts_demo_1_2()
//{
//    echom("LabelPts_demo_1_2");
//    
//	fghi = squarePts(randPts(3));
//	MarkPts( fghi, ["r",0.05] );
//	Plane( fghi, ["mode",4] );
//	LabelPts( fghi, "FGHI", ["color","green"] );
//
//	pts = randPts(5);
//	MarkPts( pts, ["r",0.05, "samesize",true, "sametransp",true] );
//	LabelPts( pts, range(pts), ["color","purple"] );
//	Chain(pts, ["r", 0.02 ] );
//}
////LabelPts_demo_1_2();
//
//module LabelPts_demo_2_scale()
//{
//	echom("LabelPts_demo_2_scale");
//    
//    pqr = randPts(3);
//	MarkPts( pqr, ["r", 0.05] );
//	LabelPts( pqr, "PQR" );
//	Chain( pqr, ["r",0.02, "closed", false, "color","red"] ); 
//	LabelPt( pqr[1], "scale=0.03 (default)",["color","red", "shift",0.5] );
//
//	lmn = randPts(3);
//	MarkPts( lmn, ["r",0.05] );
//	Chain( lmn, ["r",0.02, "closed", false, "color","green"] ); 
//	LabelPts( lmn, "lmn", opt=["scale",0.06]);
//	LabelPt( lmn[1], "scale=0.06", ["color","green", "shift",0.5] );
////
//	fghi = randPts(4);
//	MarkPts( fghi, ["r",0.05] );
//	Chain( fghi, ["r",0.02, "closed", false, "color","blue"] ); 
//	LabelPts( fghi, "FGHI", ["scale",0.02,"color","red"] );
//	LabelPt( fghi[0], "scale=0.02", ["color","blue"] );
//}
////LabelPts_demo_2_scale();
//
//
//module LabelPts_demo_3_prefix_begnum_etc()
//{
//    echom("module LabelPts_demo_3_prefix_begnum_etc()");
//	// New 2014.8.16:
//	// prefix, suffix, begnum, usexyz
//
//	// Testing prefix, suffix with given labels 
//	pqr = randPts(3, x=[2,5]);
//	MarkPts( pqr, ["r", 0.05] );
//	LabelPts( pqr, range(pqr),["color","red", "prefix","p[", "suffix","]"] );
//	Chain( pqr, ["r",0.02, "closed", false, "color","red"] ); 
//	LabelPt( pqr[0], "[\"prefix\",\"p[\", \"suffix\",\"]\"]", ["color","red","shift",0.6] );
//
//	// Testing begnum=10 
//	lmn = randPts(3,y=[3,6]);
//	MarkPts( lmn, ["r",0.05] );
//	Chain( lmn, ["r",0.02, "closed", false, "color","green"] ); 
//	LabelPts( lmn, [10,11,12], ops=["scale",0.06]);
////    LabelPt( lmn[0], "[\"begnum\",10]", ["color","green","shift",0.6] );
//
//
//	fghi = randPts(2,z=[3,6]);
//	MarkPts( fghi, ["r",0.05] );
//	Chain( fghi, ["r",0.02, "closed", false, "color","blue"] ); 
//	LabelPts( fghi, fghi, opt=["scale",0.02,"color","blue"] );
//	//LabelPt( fghi[0], "scale=0.02", ["color","blue"] );
//}
//
////LabelPts_demo_3_prefix_begnum_etc();
//
//module LabelPts_demo()
//{
//	//LabelPts_demo_1();
//	//LabelPts_demo_2_scale();
//	LabelPts_demo_3_prefix_begnum_etc();
//}
////LabelPts_demo();


//module Line0(pq,opt=[])
//{   
//    //echom("Line0");
//    r = hash( opt, "r", 0.015 );
//    fn = hash( opt, "fn", 6); 
//    
////    pqr= len(pq)==2?app(pq,randPt()):pq;
////    qpr= get(pqr, [1,0,2]);
////    
////    ptsP = arcPts( pqr, a=90, a2=360*(fn-1)/fn, rad=r, n=3 );
////    ptsQ = reverse( arcPts( qpr, a=90, a2=360*(fn-1)/fn, rad=r, n=3 ));
////    
////    pts = concat( ptsP, ptsQ );
////
////    faces = faces("rod", nside=fn); 
////    color( hash(opt,"color"), hash(opt,"transp"))
////    polyhedron( points = pts, faces= faces );
////    
//    color( hash(opt, "color"), hash(opt, "transp",1) )
//    hull()
//    {   
//       translate( pq[0])
//       sphere( r= r*mm ); 
//               
//       translate( pq[1])
//       sphere( r= r*mm);
//
//    }        
//   
//}

module Line0(pts, r=0.1, $fn=4, closed=false) { // a version of Line that uses the polyline approach
                                  // of Ronaldo's
    if(len(pts)>1 && len(pts[0])>1 && r>0) { 
            pts  = concat([for(p=pts) addz(p)], 
                        closed? [addz(pts[0])] : []); 
            for(i=[0:len(pts)-2]) { 
                v = pts[i+1]-pts[i]; 
                L = norm(v); 
                if( L > 1e-12) 
                    translate(pts[i]) 
                        rotate([0, acos(v[2]/L), atan2(v[1],v[0])]) 
                            rotate(45) cylinder(h=L, r=r, $fn=4); 
            } 
     } 
}

////========================================================
//Line =["Line", "pq,ops", "n/a", "3D, Shape",
//"
// Given a 2-pointer pq and ops, draw a line between p,q in pq. The
//;; default ops:


//module Line(pts, r, closed, color, joint, $fn, opt=[])
//{
//  //r     = isnum(r)?r: hash(opt, "r",0.05);
//  //r      = r?r: hash(opt, "r",0.05);
//  r      = und_opt(r,opt,"r",0.05); 
//  //closed = closed==undef?hash(opt, "closed", closed): closed;
//  closed = und_opt(closed, opt, "closed");
//  color  = assurePair( und(color, hash(opt, "color", "gold"), 1));
//  _joint = und(joint, hash( opt, "joint", false));
//  joint  = isnum(_joint)?["r",_joint]:_joint;
//  $fn    = $fn?$fn:hash( opt, "$fn", 4);
//  
//  //echo("in Line, joint= ", joint);
//  if( joint )
//  {
//     j_r = hash( joint, "r", r);
//     j_fn= hash( joint, "$fn", $fn);
//     j_col=assurePair(hash( joint, "color", color),1);
//     //echo("in Line, j_col= ", j_col);
//     for(i=[0:len(pts)- 1]) 
//     { 
//       translate(pts[i]) 
//       color(j_col[0],j_col[1]) 
//       sphere(r=j_r, $fn=j_fn);
//     }   
//  }
// 
//  // The following code based on Ronaldo's Polyline: 
//  // http://forum.openscad.org/Can-you-sweep-a-object-with-fingers-tp19057p19330.html
//  // 2016.11.22 
//  if(len(pts)>1 && len(pts[0])>1 && r>0) { 
//            pts  = concat([for(p=pts) addz(p)], 
//                        closed? [addz(pts[0])] : []); 
//            color(color[0],color[1])
//            for(i=[0:len(pts)-2]) { 
//                v = pts[i+1]-pts[i]; 
//                L = norm(v); 
//                if( L > 1e-12) 
//                    translate(pts[i]) 
//                        rotate([0, acos(v[2]/L), atan2(v[1],v[0])]) 
//                            rotate(45) cylinder(h=L, r=r, $fn=$fn); 
//            } 
//     } 
//}
module Line(pts, r, closed, color, Joint, $fn, opt)
{
  r      = arg(r,opt,"r", SCALE/5); 
  closed = arg(closed, opt, "closed");
  color  = colorArg(color,opt); 
  
  _Joint = subprop( sub=Joint, opt=opt, subname="Joint", dfsub=["r",r, "color", color]
                , dfPropName="r"
                , dfPropType="num", canDisable=true, debug=0 );
  $fn = arg($fn,opt,"$fn",4);
  
  //echo("in Line, Joint= ", Joint);
  //echo("in Line, opt= ", opt);
  //echo("in Line, _Joint= ", _Joint);

  if( _Joint )
  {
     shape = hash(_Joint,"shape","ball");
     j_r = hash( _Joint, "r", r);
     j_fn= hash( _Joint, "$fn", 16);
       
     if(shape=="ball")
     {
       //j_col= colorArg(undef, Joint); //assurePair(hash( Joint, "color", color),1);
       //echo("in Line, j_col= ", j_col);
       //echo("in Line, r=", r, ", Joint_r= ", j_r);
       for(i=[0:len(pts)- 1]) 
       { 
         translate(pts[i]) 
         Color(undef, _Joint) //color(j_col[0],j_col[1]) 
         sphere(r=j_r, $fn=j_fn);
       }
     }  
     else if( shape=="corner" )
     {
       jr = r*sqrt(2); //sqrt(2*j_r)/2;		
       //echo(r=r,jr=jr);

       for(i=[0:len(pts)- 1]) 
       { 
         translate(pts[i]) 
         Color(undef, _Joint) //color(j_col[0],j_col[1]) 
          cube(size= jr,center=true );
       }
     
     }
     //else if( shape=="cube" )
     //{
       //_Joint2= update( ["size", j_r, "center",true, "Frame", false], _Joint ); 
       //echo("In Line, _Joint2=", _Joint2, j_r=j_r);
       //_pts = concat( [ last(pts)], pts, [pts[0]] );
       //
       //Cube(site=[ _pts[2], _pts[1], _pts[0] ], opt=_Joint2 );
       //for(i=[1:len(_pts)- 3]) 
       //{ 
         //Cube( site= [ _pts[i+1], _pts[i], _pts[i-1] ] , opt=_Joint2);
       //}
       //L = len(_pts);
       //Cube( color="blue", site= closed?
                   //[ _pts[L-1],_pts[L-2], _pts[L-3] ]
                   //: [ 2*_pts[L-2]-_pts[L-3], _pts[L-2], _pts[L-1] ], opt=_Joint2);
                   //
       //MarkPt(2*_pts[L-2]-_pts[L-3]);             
     //}     
  }
 
  // The following code based on Ronaldo's Polyline: 
  // http://forum.openscad.org/Can-you-sweep-a-object-with-fingers-tp19057p19330.html
  // 2016.11.22 
  if(len(pts)>1 && len(pts[0])>1 && r>0) { 
            pts  = concat([for(p=pts) addz(p)], 
                        closed? [addz(pts[0])] : []); 
            color(color[0],color[1])
            for(i=[0:len(pts)-2]) { 
                v = pts[i+1]-pts[i]; 
                L = norm(v); 
                if( L > 1e-12) 
                    translate(pts[i]) 
                        rotate([0, acos(v[2]/L), atan2(v[1],v[0])]) 
                            rotate(45) cylinder(h=L, r=r, $fn=$fn); 
            } 
     } 
}



//========================================================
//{ // Line2 block
//
//Line2 =[["Line2", "pq,ops", "n/a", "3D, Shape"]
//	  ,["Given a 2-pointer pq and ops, draw a line between p,q in pq."
//	   ,"The default ops:"
//		,"                                           " 
//		," [[''r'', 0.05]                   // radius of the line    "
//		," ,[''style'',''solid'']       // solid | dash | block      "
//		," ,[''color'', ''yellow'']                                  "
//		," ,[''transp'', 1]                       // transparancy    "
//		," ,[''fn'', $fn]                                            "
//		," ,[''rotate'', 0]             // rotate alout the line     "
//		,", [''markpts'', false]      // Run MarkPts if true"
//	    ," ,[''shift'', ''corner''] //corner|corner2|mid1|mid2|center   "
//		,"  //................. style-block settings                 "
//		," ,[''width'', 1]                                           "
//		," ,[''depth'', 6]                                           "
//		," ,[''touch'', false ] // a pt where the block will rotate  "
//		,"                      // until the block touches the pt.   "
//		,"  //................. style-dash settings                  "
//		," ,[''dash'', 0.4]     // len of dash                       "
//		," ,[''space'', false]  // space between dashes              "
//		,"  //................. head/tail                            "
//		," ,[''head'',head_ops ]                                     "
//		," ,[''tail'',tail_ops ]                                     "
//		," ]                                                         "
//		,"where head_ops and tail_ops both are:                      "
//		,"    [[''style'',false]   // false|cone|sphere              "
//		,"    ,[''len'',line_r*6]       // length                    "
//		,"    ,[''r'',line_r*3]         // radius                    "    
//		,"    ,[''r0'',0]          // smaller radius                 "
//		,"    ,[''fn'',ops(''fn'')]                                  "
//		,"    ,[''rotate'',0]      // rotate about the line          "  
//		,"    ,[''color'', ops(''color'')]                           " 
//		,"    ,[''transp'',ops(''transp'')]                          "
//		,"    ]                                                      "
//		]
//		];
//
//module Line2( pq, ops){
//
//	
//	//echom("Line");
//	//echo("In Line, original pq: ", pq);
//
//	ops= updates( OPS,
//		[ "r", 0.05			// radius of the line
//		, "style","solid"   // solid | dash | block
//		, "markpts", false
//		, "shift", "corner" //corner|corner2|mid1|mid2|center
//		//................. style-block settings	
//		, "w", 1  //,"width", 1
//		, "depth", 6
//		, "touch", false  // a pt where the block will rotate 
//					    // until the block touches the pt.  
//		//................. style-dash settings	
//		, "dash", 0.4     // len of dash
//		, "space", false  // space between dashes
//		//................. head and tail extension 2014.5.18
//		, "extension0", 0  
//		, "extension1", 0  
//		//....................
//		, "head", false
//		, "tail", false
//
//		], ops); function ops(k)= hash(ops,k);
//	
//	if(ops("markpts")) MarkPts(pq);	
//
//	style = ops("style");
//	r 	  = ops("r");
//	w     = ops("w");
//	headops  = ops("head");
//	tailops  = ops("tail");
//	depth = ops("depth");
//	extension0 = ops("extension0");
//	extension1 = ops("extension1");
//
//	//echo("style",style, "rotate", ops("rotate"));
//
//	// Re-sort p,q to make p has the min xyz. This is to 
//	// simplify the calc of rotation later.
//	p = minyPts(minxPts(minzPts(pq)))[0];
//	switched = p!=pq[0];
//	//echo("switched? ", switched);
//	q = switched?pq[0]:pq[1]; //:pq[0]; 
//	L = norm(p-q);
//	
//	x = q[0]-p[0];
//	y = q[1]-p[1];
//	z = q[2]-p[2];
//
//	rot = rotangles_z2p( q-p ); 
//	//echo_("rot={_}",[rot]);
//
//	/*  
//	KEEP THIS SECTION for debugging style=block
//	//============================
//	pz = [0,0,L];
//	pm = rmx( rot[0] )* pz;
//	Line( [O, pz]);
//	Line( [O, pm] );
//	
//	pn = rmy( rot[1] ) * pm + p;
//	Line( [O+p, pn], r=0.2, color="blue", transp=0.3);
//	MarkPts( [pz,pm,pn], [["transp",0.3],["r",0.3]] );
//	
//	//============================
//	*/
//
//
//	// The shift is a translation [x,y,z] determines how (or, where 
//	// on the line/block) the line/block attaches itself to pq. It
//	// can be any 3-pointer. If style=block, one of the following 
//	// preset values also works:
//	//
//	//    corner			mid1			mid2			center 
//	//    q-----+      +-----+      +-----+      +-----+                      
//	//   / \     \    p \     \    / q     \    / \     \     
//	//  p   +-----+  +   +-----+  +   +-----+  + p +-----+
//	//   \ /     /    \ q     /    p /     /    \ /     / 
// 	//    +-----+      +-----+      +-----+      +-----+  
//
//	//    corner2
//	//    +-----+   
//	//   / \     \  
//	//  +   q-----+ 
//	//   \ /     /  
// 	//    p-----+   
//	//
//
//	shift_ = ops("shift");
//	shift =  isarr(shift_)? shift_  // if shift_ is something like [2,-1,1]
//	:hash(                          // if shift_ is one of the following  
//		[ "corner",  [0,0,0] 
//		, "corner2", [ -w, 0, 0] 
//		, "mid1",    [ -w/2, 0 ,0] 
//		, "mid2",    [ 0, -depth/2,0] 
//		, "center" , [ -w/2, -depth/2, 0 ] 
//		], shift_
//	);
//    		
//
//	// head/tail settings:
//	//
//	// Each has a "len" for the length and a "r" for the radius. 
//	// Also a r0 if needed depending on head/tail style (for
//	// example, cone). Possible head/tail styles: 
//	// 	
//	// | sphere 
//	// | cone : this is in fact a cylinder. In this case, the 
//	// |        r and r0 values are assigned to the r1, r2 
//	// |        arguments of the cylinder. It's default setting
//	// |		   is to make an arrow.
//
//	head0= updates( ops, 
//			[ "style",false // false|cone|sphere
//			, "len",r*6
//			, "r",r*3
//			, "r0",0
//			], isarr( headops )?headops:[] );
//
//	tail0= updates( ops,
//			[ "style",false
//			, "len",r*6
//			, "r",r*3
//			, "r0",0
//			], isarr( tailops )?tailops:[] );
//
//	// We need to chk if head/tail should be reversed
//	head = switched?tail0:head0;
//	tail = switched?head0:tail0;	
//
//	function head(k)= hash(head,k);
//	function tail(k)= hash(tail,k);
//	
//	hstyle = head("style");
//	tstyle = tail("style");
//
//	hl 	   = hstyle?head("len"):0;
//	tl 	   = tstyle?tail("len"):0;
//
//
//	// Rotation of head/tail around the line. 
//	// If not defined, follow the line rotation
//	//hrot   = hasht(head, "rotate", ops("rotate"));
//	//trot   = hasht(tail, "rotate", ops("rotate"));	
//	hrot   = hash(head, "rotate", ops("rotate"));
//	trot   = hash(tail, "rotate", ops("rotate"));
//	
//	// headcut/tailcut: the length that is required to cut away
//	// from the line to accomodate the head/tail. This determines
//	// where exactly the head/tail is gonna be placed. For example, 
//	// a cone (or arrow) starts from the exact p or q location
//	// and extends its length inward toward the center of line.
//	// A sphere however is placed at exactly the q or q, so half
//	// of it will be extended outside of the p-q line. 
//	headcut = hash( [ "cone", hl-0.0001 
//					,"sphere", 0
//					,false, 0
//					], hstyle );
//	tailcut = hash( [ "cone", tl-0.0001 
//					,"sphere", 0
//					,false, 0
//					], tstyle );	
//
//
//	// LL is the actual length that is shorter or equal to L
//	// depending on the styles of head and tail. 
//	//LL = L- headcut - tailcut;
//	LL = L- headcut - tailcut + extension0 + extension1;
//
//	
//	// The point to start/end the line taking into consideration 
//	// of headcut/tailcut. onlinePt is a function returning the
//	// xyz of a pt lies along a line specified by pq.
//	ph= head("style")? onlinePt([p,q], ratio = (L-headcut)/L):q; 
//	pt= tail("style")? onlinePt([p,q], ratio = tailcut/L):p; 
//	
//
//	/*	
//	------------ Calc the angle to "rotate to touch pt" in style_block
// 
//	Let: pqr the 3-pointer on the plane of block surface
//		 pt : the touch pt
//
//	        Pt
//    		    /|
//    		d1 /	 | 			
//    		  /  | d2
//    		 /a  |
//		p----+------q-----r
//
//	If we have r (a 3rd pt on block surface other than pq), we can calc:
//
//		d1 = norm( pt-pq[0])
//		d2 = distPtPl( pt, pqr )
//	
//	BUT, how do we find that 3rd pt r ?
//
//	We know that the block is first placed on yz-plane when created.
//	So any pt on yz-plane, like [0,1,1] serves the purpose. 
//
//	We know that the transformation of style-block is:
//
//		translate( tail("style")?pt:p )	 
//		rotate(rot)
//		rotate( [0, 0, ops("rotate")])
//		translate( shift )
//		cube( ... )
//
//	By applying "this process" to [0,1,1] (skipping the last 3 lines),
//	we should find the 3rd r pt we want. 
//
//	So, how to come up with the forumla for "this process" ?
//
//	We should already have all tools for this (see the following), 
//	but for some reason it hasn't worked out yet. Has to come back.
//
//	// First a unit pt on yz surface:
//	puz = [0,1,1];
//
//	// This pt is shifted following the shift of the block:
//	puz_shift = puz + shift ;
//
//	// It then rotates about z-axis following the block's ops("rotate"):
//	puz_opsrot = rmz( ops("rotate"))* puz_shift ;
//
//	// Then the real rotation, first about x, then y:
//	puz_rx = rmx( rot.x )*puz_opsrot;
//	puz_ry = rmy(rot.y)*puz_rx;
//
//	// It then needs to translocate with the block:
//	p3rd = puz_ry +(tail("style")?pt:p);	
//
//	This p3rd is on the surface of the block.
//
//	echo("ops[rotate]", ops("rotate"));
//	echo("shift ", shift);
//	echo("puz_opsrot ", puz_opsrot);
//	echo("puz_rx ", puz_rx);
//	echo("puz_ry ", puz_ry);
//	echo("p3rd ", p3rd );
//	
//	//MarkPts([ puz, puz_opsrot, puz_rx, puz_ry, p3rd]
//	//	,[["r",0.2],["transp",0.4]] );
//
//	MarkPts([puz, p3rd],[["r",0.2],["transp",0.4]]);
//
//	
//	touch = ops("touch");
//	//echo("touch ", touch);
//	p3rd= rmy(rot.y)*
//			rmx(rot.x)*
//				rmz( ops("rotate"))* ([0,1,1]+shift)
//			 + (tail("style")?pt:p);
//	p3rd= rmy(rot.y)*
//			rmx(rot.x)*
//				rmz( ops("rotate"))* ([0,1,1]+shift)
//			 + (tail("style")?pt:p);
//	//MarkPts( [ p3rd ], [["r",0.2],["colors",["blue"]],["transp",0.4]] );
//	d1 = norm( touch-p);
//	d2 = distPtPl( touch, [p,q,p3rd] );
//	rotang = -asin(d2/d1);
//	//MarkPts([p3rd],[["r",0.2],["transp",0.4]]);
//	echo("d1 = ", d1);
//	echo("d2 = ", d2 );
//	echo("rotang ", rotang);
//	
//*/
//	touch = ops("touch");
//	prerot =touch? rotang: ops("rotate");
//	//echo("prerot ", prerot);
//			
//
//	//echo("Before drawing line, rot = ", rot);
//
//	// ------------------------------------           The line itself :	
//	if(style== "dash"){
//
//		color(ops("color"), ops("transp"))
//		DashLine([pt,ph], ops);
//
//	} else if (style=="block") {    // block
//
//		translate( tail("style")?pt:p )	 
//		rotate(rot)
//		rotate( [0,0, prerot]) //ops("rotate")])
//		color(ops("color"), ops("transp"))
//		translate( shift+ [0,0,switched?-extension1:-extension0] )
//    		cube( [ops("width"), ops("depth"), LL ]
//			, $fn=ops("fn"));
//
//	} else {                       // solid
//
//		translate( tail("style")?pt:p) 
//		rotate(rot)
//		rotate( [0,0, ops("rotate")])
//    		color(ops("color"), ops("transp"))
//		translate( shift+ [0,0,switched?-extension1:-extension0] )
//    		cylinder( h=LL, r=r , $fn=ops("fn"));
//	} 
// 
//
//
//	// ---------------------------------- The tail :
//	//		
//	if(tstyle=="cone")
//	{
//		translate( p )  
//		rotate( rot)
//		rotate( [0,0,trot] )
//		color(tail("color"), tail("transp"))
//		translate( shift )
//    		cylinder( h=tl, r1=tail("r0"), r2=tail("r"), $fn=tail("fn"));
//
//	} else 
//
//	if (tstyle=="sphere")
//	{
//		translate( switched?pt:p ) 
//		rotate( rot)
//		rotate( [0,0,trot] )
//		color(tail("color"), tail("transp"), $fn=tail("fn"))
//		translate( shift )
//    		sphere( r=tail("r"));
//	} 
//
//	// ----------------------------------- The head :
//	// 
//	if(hstyle=="cone")
//	{
//		translate( ph )  
//		rotate( rot)
//		rotate( [0,0,hrot] )
//		color(head("color"), head("transp"))
//		translate( shift )
//    		cylinder( h=hl, r2=head("r0"), r1=head("r"), $fn=head("fn"));
//
//	} else
//
//	if (hstyle=="sphere")
//	{
//		translate( q ) 
//		rotate( rot)
//		rotate( [0,0,hrot] )
//		color( head("color"), head("transp"), $fn=head("fn"))
//		translate( shift )
//    		sphere( r=head("r"));
//	} 
//	
//} // Line2 
//
//module Line2_test( ops ){doctest (Line, ops=ops); }
//
//
//
//} // Line2 block 

//========================================================
//LineByHull=["LineByHull", "pq,r=0.05,fn=15", "n/a", "3D,Shape",
//"
// Draw a line as thick as 2*r between the two points in pq using
//;; hull(). This approach doesn't need to calc rotation/translation,
//;; so the faithfulness of outcome is guaranteed. But is much slower
//;; so not good for routine use.
//"];
//
//module LineByHull( pq,r=0.05, fn=15 )
//{
//	hull(){
//		translate(pq[0])
//		sphere(r=r, $fn=fn);
//		translate(pq[1])
//		sphere(r=r, $fn=fn);
//	}
//}
//
//module LineByHull_test( ops ){ doctest(LineByHull, ops=ops); }
//
//module LineByHull_demo()
//{
//	LineByHull( randPts(2) );
//}
//LineByHull_demo();
//doc(LineByHull);



//. m


//========================================================
Mark90=["Mark90", "pqr,opt", "n/a", "Grid, Math, Geometry, 3D, Shape",
" Given a 3-pointer pqr and an opt, make a L-shape at the middle
;; angle if it is 90-degree. The default opt, other than usual
;; r, color and transp, has ''len''= 0.4
;;
;; ops: [ ''len'', 0.4
;;      , ''color'', false
;;      , ''transp'', 0.5
;;      , ''r'', 0.01
;;      ]
;;
;; default arg: Mark90(pqr,0.5) means r=0.5
"];
	
module Mark90(pqr, opt=[]) // default arg: Mark90(pqr,0.5) means r=0.5
{
    
    df= [ "r", 0.01 
        , "len", 0.4
        , "color","green"
        , "transp", 0.5
        ];
     
    _opt = popt( isnum(opt)?["r",opt]:opt
               );
    opt=update(  df, _opt );

    pqr = get3(pqr,0);
    //echo(angle=angle(pqr));
    if( ispl(pqr) && !isequal(pqr[0],pqr[1])
      && !isequal(pqr[2],pqr[1])
      && !isequal(pqr[0],pqr[2])
      )
    {        
	      p = pqr[0];
        q = pqr[1];
        r = pqr[2];
        
        if (is90( [p,q], [q,r] ) )
        {            
          //echo("is90");
          len = hash(opt,"len");
          m = min( 0.4*norm(p-q), 0.4*norm(r-q), len);
          
          PP = onlinePt( [q,p], len=m); 
          RR = onlinePt( [q,r], len=m); 
          Line( [PP, cornerPt([PP,q,RR]), RR], opt= opt ); 
          Line( pqr, opt=opt);
        }
    }//_ispl     
}//_Mark90


module Mark90_test(opt){ doctest( Mark90, opt=opt); }
//Mark90_demo();
//Mark90_test();
//doc(Mark90);



//========================================================
module MarkBezierPtsAt( bzrtn, opt=[] )                        
{     
   /*
   bzrtn: [bzpts,pqrs,param] = 
   [ bzpts, 
   , [P,Q,R,S]
   ,[["T",app(tlnQ,abiQ), "aT",aPQR, "aH",aH[0], "dH",dH[0], "H",Hq ]
    ,["T",app(tlnR,abiR), "aT",aQRS, "aH",aH[1], "dH",dH[1], "H",Hr ]
    ]
   ] 
   */ 
    echom("MarkBezierPtsAt");
    
    df_opt=[
          "showends",[true,true]
        , "bzpts", true
        , "markT", true
        , "markH", true
    ];
   _opt = update( df_opt, opt );
   function opt(k,nf)=hash(_opt, k,nf); 
   
   _showends= opt("showends");
   showends= !_showends? [0,0]
             : isarr(_showends)? _showends
             : [ _showends, _showends ];
   showhead = showends[0];
   showend  = showends[1];
   bzpts = (showhead && showend)? bzrtn[0]
           : showhead? slice(bzrtn[0], 0, -1)
           : showend ? slice(bzrtn[0], 1)
           : slice(bzrtn[0], 1,-1);
   pqrs  = bzrtn[1];
   param = bzrtn[2];
   
   bzptsopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "bzpts"
                     , df_subopt= "cl=red;chain;r=0.05"
                     , com_keys=["color","transp"]
                     ); 
                     
   markTopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markT"
                     , df_subopt=["show",[true,true], "r",0.015
                                 ,"chain",["r",0.015],"color","green"
                                 ,"ball",false,"transp",0.1,"label",true]
                     , com_keys=["color","transp"]
                     ); 
                     
   markHopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markH"
                     , df_subopt=["show",[true,true]
                                 ,"chain",["r",0.015],"r",0.05
                                 ,"color","gray"
                                 ,"transp",0.5,"label",["text",["","H"]]]
                     , com_keys=["color","transp"]
                     );  
   echo( bzptsopt= bzptsopt );
   echo( markTopt = markTopt );
   echo( markHopt = markHopt );
   
   //--------------------------------------
   // Given pqrs and generated Bezier curve pts
   if( bzptsopt )
   { MarkPts( bzpts, bzptsopt ); }

   //--------------------------------------
   // abi and tangent lines  
   /*
   param = 
   ,[["T",app(tlnQ,abiQ), "aT",aPQR, "aH",aH[0], "dH",dH[0], "H",Hq ]
    ,["T",app(tlnR,abiR), "aT",aQRS, "aH",aH[1], "dH",dH[1], "H",Hr ]
    ]
   */
   for( i=[0,1] )
   {
       if( markTopt && hash( markTopt,"show")[i] )
       {
           T = hash( param[i],"T");
           abi = T[2];
           Chain( [pqrs[1+i], abi], markTopt); 
           Mark90( [ T[0],pqrs[1+i], abi], markTopt );
           MarkPts( p01(T), markTopt); 
       }    
   }   
 
   for( i=[0,1] )
   {
       if( markHopt && hash( markHopt,"show")[i] )
       {
           H = hash( param[i],"H");
           MarkPts( [pqrs[1+i],H] 
                  , markHopt); 
       }    
   }   
}//MarkBezierPts
  
module MarkBezierPtsAt_demo(){
  
  n=5;
    
  pqr=pqr();
  MarkPts( pqr, "ch=[r,0.025];l=PQR");
  bzrtn= getBezierPtsAt( pqr, at=0, n=n);
         
  echo( bzrtn = arrprn( bzrtn, dep=2));  
  //MarkBezierPtsAt( bzrtn );
    
  /*  
  pqr2 = trslN(pqr, 4);
  MarkPts( pqr2, "ch=[r,0.025];cl=red;l=PQR");
  bzrtn2= getBezierPtsAt( pqr2, at=0, n=n);
  echo( bzrtn2 = arrprn( bzrtn2, dep=2));  
  MarkBezierPtsAt( bzrtn2, ["markT",["show",[1,0]]
                         , "markH",["show",[1,0]]
                         ]
                );
    
  pqr3 = trslN(pqr, 8);
  
  MarkPts( pqr3, "ch=[r,0.025];cl=green;l=PQR");
  
  bzrtn3= getBezierPtsAt( pqr3, at=0, n=n);
  echo( bzrtn3 = arrprn( bzrtn3, dep=2));  
  MarkBezierPtsAt( bzrtn3); 
//  , ["markT",["show",[1,0]]
//                         , "markH",["show",[1,0]]
//                         ]
//                );
                
  bzrtn32= getBezierPtsAt( pqr3,at=1, n=n, btw=[1,2]);
  echo( bzrtn32 = arrprn( bzrtn32, dep=2));  
  MarkBezierPtsAt( bzrtn32, ["markT",["show",[0,1]]
                         , "markH",["show",[0,1]]
                         ]
                ); 
  */               
}    
//MarkBezierPtsAt_demo();


module MarkBezierPtsAt_demo_chain(){
  
  n=10;
  
    
  pts= randchain(8, d=[-5,5], a=[45,135], a2=[0,45]);
//  pts =  [[2.12448, 3.3809, 3.13628], [1.70918, 2.70776, 2.63994], [0.776025, 3.09614, 4.13017], [1.49755, 3.3722, 5.68667], [2.40417, 2.75162, 5.48191], [2.0055, 2.36655, 4.81182], [0.416727, 2.38473, 6.83724], [1.89168, 2.62708, 7.14462]];  
  echo( pts= pts );
    
  MarkPts(pts,"ch=[color,blue,r,0.01];r=0.03");

  module smoothPts( pts, opt=[] ){
      
      for( i=[0:len(pts)-2] )
        { 
            bzrtn = getBezierPtsAt( pts, i, n=10, aH=.95, dH=0.4 );
            MarkBezierPtsAt( bzrtn, ["bzpts",["r",0.02], "markT",false, "markH",false] );
        }
  }   

  smoothPts( pts );
  
  pts2 = [ [4,0,0],[4,4,0],[0,4,0],[0,0,0],[4,0,0]];
  MarkPts(pts2,"ch=[color,blue,r,0.01];r=0.03");

  MarkBezierPts( pts2 );
  
}    
//MarkBezierPtsAt_demo_chain();



module Move(from, to=[] ){
   // Move obj from *from* to *to*, both are 3-pointer
   // The main action is to move an obj from the ORIGIN_SITE
   //   to the *to* site. The purpose of *from* is to move
   //   it to the ORIGIN_SITE.
   // 
   //echom("Move");
   //echo(from=from, to = to );
   to=to?to:ORIGIN_SITE;
   
   if(from!=to)
   {
     mvdata = getMoveData( from=from, to=to, debug=0 );
     //translate( isequal(to[1],O)?O:to[1] )
     translate( to[1] )
     multmatrix( m= rotM( hash(mvdata, "rotaxis2")
                        , hash(mvdata, "rota2")
                        )
               ){ 
         multmatrix( m= rotM( hash(mvdata, "rotaxis")
                            , hash(mvdata, "rota")
                            )
                   )
                   { 
                       translate( -from[1] )
                       children();
                   }
     }
   }
}    

module Move2( from, to ) // Alternative approach to Move() 2018.6.15
{   
   P=from[0]; Q=from[1]; R=from[2]; 
   S=to[0]; T=to[1]; U=to[2];
      
   V= P+T-Q;  W= R+T-Q; // translating from from to to => VTW
     
   Wr= rotPt( W    // The new W after rot from TV to TS
            , axis= [T,N([S,T,V])]
            , a= angle([S,T,V]) 
            );
              
   B_STWr= B([S,T,Wr]); // Binormal pt of [S,T,Wr]
   B_to=  B([S,T,U]);  // Binormal pt of [S,T,U]
     
   RotObjFromTo( [B_STWr,T,B_to] ) 
   RotObjFromTo( [V,T,S] )   // Rot from TA to TS  
   translate(T-Q)            // Moved from from to to => atb
   children();
}

//module Move2( pqr, stu ) // Alternative approach to Move() 2018.6.15
//{   
//   P=pqr[0]; Q=pqr[1]; R=pqr[2]; 
//   S=stu[0]; T=stu[1]; U=stu[2];
//      
//   V= P+T-Q;  W= R+T-Q; // translating from pqr to stu => VTW
//     
//   Wr= rotPt( W    // The new W after rot from TV to TS
//            , axis= [T,N([S,T,V])]
//            , a= angle([S,T,V]) 
//            );
//              
//   B_STWr= B([S,T,Wr]); // Binormal pt of [S,T,Wr]
//   B_STU=  B([S,T,U]);  // Binormal pt of [S,T,U]
//     
//   RotObjFromTo( [B_STWr,T,B_STU] ) 
//   RotObjFromTo( [V,T,S] )   // Rot from TA to TS  
//   translate(T-Q)            // Moved from pqr to stu => atb
//   children();
//}

//module Move(from, to=[] ){
//   // Move obj from *from* to *to*, both are 3-pointer
//   // 
//   //echom("Move");
//   //echo(to = to );
//   if(to)
//   {
//     mvdata = getMoveData( from=from, to=to );
//     //echo( mvdata = mvdata ); 
//     translate( to[1] )
//     //children();
//     multmatrix( m= rotM( hash(mvdata, "rotaxis2")
//                        , hash(mvdata, "rota2")
//                        )
//               ){ 
//         multmatrix( m= rotM( hash(mvdata, "rotaxis")
//                        , hash(mvdata, "rota")
//                        )
//                   ){ 
//                       if(from)
//                       {
//                         translate(-1*from) children();
//                       }
//                       else
//                       children();
//                   }
//     }
//   }
//}    



module MoveTo(site=[] )  // 2018.6.5.
{
   // Move obj from ORIGIN_SITE to *to*

   if(site)
   {
     mvdata = getMoveData( from=undef, to=site );
     m1 = rotM( hash(mvdata, "rotaxis")
              , hash(mvdata, "rota")
              );
     m2 = rotM( hash(mvdata, "rotaxis2")
              , hash(mvdata, "rota2")
              );
              
     translate( site[1] )
     multmatrix( m= m2 ) 
     multmatrix( m= m1 )   
     children();
   }
   else children();
} 

module MoveToOrigin(site=[] ) // 2018.6.5
{
   // Move obj from ORIGIN_SITE to *to*
   echom(_s("demo_MoveToOrigin(site={_})",str(site)));
   if(site)
   {
   
     // Move site to O
     site0 = [ for(p=site) p-site[1] ];
     
     Red() MarkPts( site0 );
     
     // Rot site0[0] (about [ site0[1], site0[2] ] )
     //  to XZ plane
     Gold(0.1) Plane( expandPts([X*5,Z*5,X*-5,Z*-5],3) );
     Green(0.1) Plane( expandPts([ site0[2],O,Z],2) );
     J0 = projPt( site0[0], [X,O,Z] );
     Mark90( [site0[0], J0,O] );
     
     
     
     //rotate( )
     
//     mvdata = getMoveData( from=site, to=ORIGIN_SITE );
//     m1 = rotM( hash(mvdata, "rotaxis")
//              , -hash(mvdata, "rota")
//              );
//     m2 = rotM( hash(mvdata, "rotaxis2")
//              , -hash(mvdata, "rota2")
//              );
//     echo(mvdata= _fmth(mvdata));
//     echo(m1= m1);
//     echo(m2= m2);         
//     translate( site[1] )
//     multmatrix( m= m2 ) 
//     multmatrix( m= m1 )   
//     children();
   }
  // else children();
} 



//. MarkPt

module MarkPt(pt, Label, r, color, Ball, Grid, Link, opt=[]) 
{
    //echom(str("MarkPt() --", pt));
    //echo(Label=Label, opt=opt);
    
    //loglevel=3;
    //echo(log_b("MarkPt",loglevel));
    
    pt = arg(pt, opt, "pt");
    _r = arg(r, opt, "r", 3*LINE_R);
    _col = colorArg(color, opt);
    
    //echo(pt=pt, Ball=Ball, Label=Label, opt_text=hash(opt,"text"), _Label=_Label, Grid=Grid);
    
    //-----------------------------------------------
    //Ball = Ball==undef && hash(opt,"Ball")==undef? true: Ball;
    Ball = Ball==undef? hash(opt,"Ball", true): Ball;
    _Ball = subprop( Ball, opt, "Ball", dfPropName= "r", dfPropType="num"
                   , dfsub=["r",_r, "color", _col, "$fn",12] );
                   
    _Ball_r = hash(_Ball,"r", _r);
                   
    //echo(Label=Label, r=r, _r=_r, _Ball=_Ball);                
    if(_Ball)
    {  
       //b_col = assureColor(hash(_Ball,"color", _col));
               //? colorArg(undef,_Ball)
               //: _col;
       //echo(pt=pt, Ball=Ball, _Ball=_Ball, b_col=hash(_Ball,"color"));        
       
       Color( hash(_Ball,"color") )
       translate( pt )
        sphere( r = _Ball_r, $fn= hash(_Ball, "$fn") );
    }

    //-----------------------------------------------
    _Grid = subprop( Grid 
            , opt, "Grid", dfPropName= "mode", dfPropType="num", dfsub=["color",_col, "mode",2] );
    if(_Grid) 
    {
       //echo(_Grid=_Grid);
       PtGrid(pt, opt=_Grid);
    }
           
    //-----------------------------------------------
    _Label = subprop( Label //= Label==undef? hash(opt, "Label", true) :Label
                     , opt, "Label", dfPropName="text"
            , dfPropType=["str","num", "nums"]
            , dfsub= ["color",_col//, "scale", 0.2
                     , "indent",0.1
                     , "followView",true
                     , "valign","center" 
                     , "text", pt
                     ] 
           );   
           
    //echo( subprop_debug= subprop( Label, opt, "Label", dfPropName="text"
            //, dfPropType=["str","num", "nums"]
            //, dfsub= ["color",_col//, "scale", 0.2
                     //, "indent",0.05
                     //, "followView",true
                     //, "valign","center" 
                     //, "text", pt
                     //] 
           //, debug=true) )  
    //echo(_Label = _Label, "type of", type(_Label));                 
    if(_Label) 
    {
       //echo("In MarkPt, if(_Label), _Label=", _Label);
       
       df_scale = 0.2;
       _scale = df_scale * scaleArg(undef, _Label,1);
       
       indent= hash(popt(_Label), "indent", 0.05);
       followView= hash( _Label, "followView", true);
       _actions= updates( [ "x", indent]
                        , [ followView? ["rot",$vpr] : hash(_Label,"actions",[])
                          , ["t", pt]
                          ] 
                        );

       _text0 = hash(_Label,"text");   
       _text = _text0==true?pt:_text0;           
       _Label2= isfloats(_text)&& hash(_Label,"format")==undef?
                 update( _Label, ["format","{*`a|d2}"] )
                 : _Label; 
       //echo(opt=opt);          
       //echo(_Label=_Label);
       //echo(_Label2=_Label2);                    
       //echo("Before Text()", indent=indent, _text0=_text0, _text=_text
                           //,_actions = _actions);  
                           
    // 2018.6.18: Link                          
                           
    _Link = subprop( Link==undef? hash(opt, "Link", true) :Link   
                  , opt, "Link", dfPropName= "pts", dfPropType="pt,pts"
                  , dfsub=[ "pts",[]    // If pts given, use it 
                          , "steps",[]  // If steps given, use it in growPts() to generate pts  
                          , "r",0.01, "color", ["gray",.5], "$fn",6, "Arrow", true
                          ,"indent", _Ball?_Ball_r:0
                          ] );
    
    _linkSteps = hash(_Link, "steps");
    _lpts= _linkSteps?slice(growPts( pt, _linkSteps ),1)
           :hash(_Link,"pts", []);
    lpts= isnum(_lpts[0])?[_lpts]:_lpts;
    
    //echo(_linkSteps=_linkSteps);
    //echo(_Link=_Link);
    //echo(_lpts=_lpts);
    //echo(lpts=lpts);
    //echo(arrow_opt= update(hash(_Link,"Arrow",[]), _Link) );
    
    if(_lpts) 
    {        
        headPt = onlinePt( [pt, lpts[0]],len= hash(_Link,"indent",0));
        _arrow = hash(_Link,"Arrow",[]); 
        //echo(_arrow=_arrow, [pt, lpts[0]],len= hash(_Link,"indent",0), [lpts[0], headPt]);
        if(_arrow==false)
        Line( [lpts[0], headPt], opt=_Link);
        else 
        Arrow( [lpts[0], headPt]
             , opt= ishash(_arrow)? update(_arrow, _Link):_Link
             );
        if(len(lpts)>1)
        {
           Line( lpts, opt=_Link);
        }     
    }
    
                         
    Text(text = _text
       , color=color 
       , scale = _scale
       , actions= _lpts? update(_actions, ["t",last(lpts)]): _actions
       , opt= delkeys(_Label2, ["scale","actions"]) 
       ); 
           
    } 
    
    //echo(log_e("MarkPt",loglevel));

}
    MarkPt=["MarkPt", "pt,Label,r,color,Ball=true,Grid,opt=[]"
           , "n/a", "3D, Shape, Point",
    "An obj to mark a pt with optional subobjs: Ball, Label and/or Grid.
    ;;
    ;; Args r and color applied to all subobjs, but can be specified
    ;; in each subobj.
    ;;
    ;; Ball: default true. Use builtin sphere to draw a sphere. 
    ;; Grid: default undef. Use PtGrid to draw a pt-grid. 
    ;; Label: default undef. Use Text() to write text.
    ;;
    ;; For Label:
    ;; 
    ;; 1) Label=true will show the coordiates of the pt
    ;; 2) All the followings are same:
    ;;
    ;;    1. MarkPt(pt, 'TEST',       opt=['Label',['color','red']] );
    ;;    2. MarkPt(pt, Label='TEST', opt=['Label',['color','red']]  );
    ;;    3. MarkPt(pt, Label=['text', 'TEST', 'color','red'] );
    ;;    4. MarkPt(pt, opt=['Label',['text','TEST','color','red']] );
    ;;  
    ;; Refer to Text() for more settings like scale, actions, followView, etc 
    "];

//echo("testing _f: ", _f("3", "{|d2}"));


module MarkCenter(pts, Label, r, color, Ball, Grid, opt=[])
{
  MarkPt( sum(pts)/len(pts)
        , Label=Label, r=r, color=color, Ball=Ball, Grid=Grid
        , opt= update( [ "Label", [ "followView", true
                                  , "halign","center"
                                  , "valign","center"
                                  , "color", "purple"
                                  , "indent", 0
                                  ]
                       , "Ball", false           
                       ], opt)           
        );
}


module MarkFaces(pts,faces, Label, r, color, Ball, Grid, opt=[])
{
  for(fi = range(faces) )
  {
     fpts = get(pts,faces[fi]);
     MarkCenter( pts=fpts
        , Label=["text",fi,"indent",0]
        , r=r, color=color, Ball=false, Grid=Grid, opt=opt );
  }
}


//. MarkPts

//========================================================
MarkPts=["MarkPts", "pts,opt", "n/a", "3D, Shape, Point",
"
 Given a series of points (pts) and ops, make a sphere at each pt.
;; Default ops:
;;
;;   [ ''r'',0.1
;;   , ''colors'',[''red'',''green'',''blue'',''purple'',''khaki'']
;;   , ''transp'',1
;;   , ''grid'',false
;;   , ''echo'',false
;;   ]
;;
;; The colors follow what's in colors. If len(pts)>len(colors),
;; colors recycles from the the 1st one and the transparancy and
;; radius reduced by ratio accordingly by a ratio each cycle. If
;; grid is true, draw PtGrid for each point; if echo is true,
;; echo each point.
"];

module MarkPts(pts, Label
              , r, color, countbeg
              , Line
              , Ball
              , Grid
              , indices
              , opt=[])
{
  //echom("MarkPts()");
  //echo(opt=opt);  
  _r = arg(r, opt, "r", LINE_R);
  _col = arg(color,opt,"color"); // This _col hasn't been verified by assureColor
                                 // or colorArg, so no garantee it'd be a [c,t]
  //echo(_r=_r, _col=_col);
    
    //-------------------------------------------
    
  _indices = arg(indices, opt, "indices",range(pts));
  _indices2 = [ for(is= [for(i=_indices) 
                             isarr(i)?i
                             :istype(i,"range")?[for(j=i)j]
                             :[i] 
                        ]
                   ) each is ];
                        
  // echo(Label=Label, _Label=_Label); //, range_3=range(3), all([for(x=_Label)istype(x,"num")]));
  
  //-------------------------------------------
  // The reason we need a _Ball here is to set varying colors and
  // decreasing r. Otherwise we don't need it.    
  _Ball = subprop( Ball==undef? hash(opt, "Ball", true) :Ball 
                 , opt, "Ball"
                  , dfPropName = "r"
                  , dfPropType = "num"
                  , dfsub=[//"color",_col 
                          //, 
                          "r",_r*4
                          , "dec_r", 0.02   // % of r reduction over i's
                          , "dec_t", 0.04 // % of transp reduction over i's
                          ]); 
  has_Ball_color= haskey(_Ball,"color");
  //echo(_Ball=_Ball, b_col=b_col);  
  LC = len(COLORS); 

  dec_trp = hash(_Ball, "dec_t", 0);
  
  
  
  //-------------------------------------------
  //
  // New 20200718:countbeg --- allows for counting starts at a number other than 0
  //
  // Example usage: SideBarGuide() in woodworking/EndjointForLandScape.scad
  //
  countbeg = countbeg? countbeg: 0;
   newrange= range(countbeg, countbeg+len(pts));
  _Label = subprop( Label==undef||Label==true? newrange: Label //range(pts): Label
                   , opt, "Label", dfPropName="text", dfPropType=["str","nums"]
                 ,dfsub=["color",_col, "countbeg", 0] 
                 );
  _text = hash(_Label,"text", false)  ;   
  //echo(Label=Label, _Label=_Label,_text=_text, type(_text)
  //       , istype(_text, "arr"), type(_text)=="arr");
  
  //-------------------------------------------
  for(i = range(pts)) 
  {
     //echo(_indices2=_indices2, i=i, "has(_indices2,i)", has(_indices2,i));
     if(has(_indices2,i))
     {
       col = get(COLORS,i+1,cycle=true) ;
       b_col = !has_Ball_color? ["color",col]:[];
       transp = 1-dec_trp*i;  
       _Ball2= _Ball?
               //updates(_Ball, [ !hash(_Ball, "color")?["color", [col,transp]]:[] 
                               //,[ "r", hash(_Ball, "r")*(1-i*hash(_Ball, "dec_r"))]
                              //])
               updates(_Ball, [ b_col
                               ,[ "r", hash(_Ball, "r")*( 
                                  h(_Ball, "samesize")?1:(1-i*hash(_Ball, "dec_r"))
                                  )
                               
                                ]
                              ])
               :_Ball;
       //echo(col=col, transp=transp, _col=_col, b_col=b_col, _Ball2=_Ball2);
       //echo(col=col, _col=_col, b_col=b_col, _Ball=_Ball, _Ball2=_Ball2);
       //echo(_Ball2=_Ball2);
       t = hash(_Label, "Label"); //isnums( )
       //opt_Label= update( _Label, ["Label", isarr(t)||isstr(t)?t[i]
                                                     //:true?_pts[i]
                                                     //:t] );
       //echo(opt_Label=opt_Label); 
     
       //echo( "update2(opt, _Label)=", update2(opt, _Label));
       //echo("type(_text)=", type(_text), "istype(_text, 'arr str')= ",istype(_text, "arr str"));
       label = istype(_text, "arr str")
                               ? str(_text[i]): _text;
       //echo(_text=_text, label=label);  
       //MarkPt(pt, Label, r, color, Ball, Grid, opt=[])                                                                   
       
       MarkPt(pts[i], Label = istype(_text, "arr str")
                               ? str(_text[i]): _text
                     , r=r
                     , Ball= _Ball2
                     , Grid=Grid
                     , opt= update(opt, ["Label", delkey(_Label,"text")] )
                   
                     );
     }// if(has ..)                
  }// for
  
  //-------------------------------------------
  
  _Line = subprop( Line==undef? hash(opt, "Line", true) :Line   
                 , opt, "Line", dfPropName= "r", dfPropType="num"
                  , dfsub=["r",_r*.75, "color", _col, "$fn",6] );
  if(_Line) Line(pts, Joint=false, opt=_Line); 
  
}

module MarkPts_old(pts, opt=[], mti=0) 
{
    //if(ECHO_MOD_TREE) echom("MarkPts", mti);
    echom("MarkPts", mti);
    echo(MarkPts_opt = _fmth(opt) );
        
    df_opt= [ "r",0.1
            , "transp",1
            , "color",""
        	, "colors", COLORS
			, "samesize", false
			, "sametransp", false
            , "rng",undef
    
	    , "ball", true
            , "grid", false
            , "label", false
            , "chain", false
            , "plane", false
            ];
    //opt= update( df_opt, opt );
    opt= update( df_opt, opt
           , df=["g","grid","l","label"
                ,"ch","chain","pl","plane"
                ,"b","ball"]
           );    
	function opt(k,nf)= hash(opt,k,nf);
    label = opt("label");
    
    gridopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "grid"
                     , df_subopt= ["mode",2]
                     , com_keys=["color","transp"]
                     );
    labelopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "label"
                     , df_subopt= ["text", range(pts), "color","brown"]
                     , com_keys=["color","transp"]
                     , mainprop="text"
                     ); 
    //echo(label=label, "<br/>", labelopt=labelopt);
    chainopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "chain"
                     , df_subopt= ["color",undef]
                     , com_keys=["color","transp"]
                     );                      
    planeopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "plane"
                     , df_subopt= ["color",undef]
                     , com_keys=["color","transp"]
                     );          
                     
    color  = opt("color");           
	colors = opt("colors");
	transp = opt("transp");
	samesize = opt("samesize");
	sametransp=opt("sametransp");
	label = opt("label");
     cL = len(colors);
    _rng = opt("rng"); 
    rng = isint(_rng)?[_rng]
              :isarr(_rng)? _rng: range(pts);    
    
  //echo(label = label, labelopt=labelopt );
	// color cycle. The more ccycle, the smaller the sphere
	// and the more transparent they are.  
	function ccycle(i) = floor(i/cL); 
    //echo( indices=indices);
    if(opt("ball"))
	for (j=range(rng)){
        i = rng[j];
		translate(get(pts,i))
		color( or( color
                 , opt("colors")[j-cL*ccycle(j)])   // cycling thru colors
			 , transp* ( sametransp?1:(1-0.3*ccycle(j))) // transp level
			 ) 
		sphere(r=opt("r")*mm*(samesize?1:(1-0.15*ccycle(j)))
			  );
	}

    if( gridopt ){
        for (j=range(rng)){
            i = rng[j]; 
             PtGrid( get(pts,i), opt=gridopt); 
        }    
    }
    if( planeopt ){ Plane(pts, opt=planeopt); }
    
    if( opt("echopts") ) echo( 
                  join( [for(i=range(pts))
                           str( "pts[",i,"]=", pts[i])
                         ]
                      ,"<br/>"));
    
                  
    labeltext = hash(labelopt,"text");
	if(labelopt){ 
        // pts for label. If to label the Q in pqr, the label
        // is placed on a pt outside of a_pqr. This is to make
        // sure that a polygon is not crowded by text.         
        labelpts= len(pts)>2? 
                  let( p3s = concat( [last(pts)], pts, [pts[0]] )
                     ) 
                  [ for (i = [1:len(p3s)-2]) 
                     onlinePt( [ p3s[i]
                               , angleBisectPt( [p3s[i-1], p3s[i], p3s[i+1]]) 
                               ], len= -20*hash( labelopt,"scale",0.01 ))
                  ]
                    
//                  [ for( p3= ranges( pts, cover=[1,1]
//                            , returnitems=true) )
//                     onlinePt( [p3[1], angleBisectPt(p3)], len= -20*hash( labelopt,"scale",0.01 ))
////                     onlinePt( [p3[1], angleBisectPt(p3)], len= -0.3 )
//                  ]
                  
                  :len(pts)==2? extendPts(pts, len=0.3)
                  :pts;
                  
        for( j= range(rng) ){
            i = rng[j];
            LabelPt( get(labelpts,i)
                   , opt= update( labelopt, ["text"
                                            ,get(labeltext,j)] )
                   , mti=mti+1 );
        }    
    }
//    if(chainopt) Chain0(pts, opt=chainopt);
    
    // ChainByHull is for debug only:
    //if(chainopt) ChainByHull(pts, opt=chainopt);
    if(chainopt) {
        //echo(chainopt=chainopt);
        //echo(pts = pts );
        //SimpleChain(pts, opt=chainopt);
        Line(pts, opt=chainopt);
      
    }    
}
//MarkPts(pqr()), ["chain",["r",0.02]] );

//. N

//========================================================
Normal=["Normal", "pqr,ops","n/a","3D, Line, Geometry",
 " Given a 3-pointer (pqr) and ops, draw the normal of plane made up by
;; pqr. The normal will be made passed through Q (pqr[1]). Its length
;; is determined by L/3 where L the shortest length of 3 nside. The
;; default ops:
;;
;; [ ''r''      , 0.03
;; , ''transp'' , 0.5
;; , ''mark90p'', true
;; , ''mark90r'', true
;; , ''len''    , L/3
;; ]
"];
 
module Normal_test( ops=["mode",13] ){ doctest( Normal, ops=ops); }

module Normal( pqr, ops )
{
	L = min( d01(pqr), d02(pqr), d12(pqr));
	ops = update( [
			  "r"     , 0.03
			  ,"len"  , L/3
			, "transp", 1
			, "mark90p", false
			, "mark90r", true
			, "reverse", false
			, "label", false
			], ops);
	function ops(k)= hash(ops,k);
	echo(ops);
	mark90p = ops("mark90p");
	mark90r = ops("mark90r");

	Nrm = (ops("reverse")?-1:1)*normal( pqr )/3;
	_N = Nrm + pqr[1];
	N = onlinePt( [ORIGIN+ pqr[1],_N]
				, len=ops("len") );

	MarkPts([N]);

	if( mark90p ) { Mark90( [ N,pqr[1],pqr[0] ], mark90p ); } 
	if( mark90r ) { Mark90( [ N,pqr[1],pqr[2] ], mark90r ); } 

	Line0([ ORIGIN+ pqr[1], N ], concat( ["head",["style","cone"]], ops) );
	
}

module Normal_demo_1()
{
	pqr = randPts(3);
	MarkPts( pqr, ["grid",["mode",2]] );
	Chain( pqr );
	Normal( pqr );

	color("red") Normal(pqr, ["reverse",true]);
}

module Normal_demo()
{
	Normal_demo_1();
}
//Normal_demo();



//========================================================
//. p

Pie=[[]];

module Pie( angle, r, ops=[] )
{
	
	ops = concat( ops
		// ,["h",1
		//  ]
		 ,OPS);
	
	function ops(k) = hash( ops, k);
	
	arcpts = concat( [[0,0]], arcPts( a=angle, r = r, count = angle) );
	//echo( "arcpts: ", arcpts);

	paths = [range(len(arcpts))];

	color(ops("color"), ops("transp"))
	polygon ( points=arcpts, paths=paths); 



}

module Pie_demo()
{

	Pie(angle=60, r=3);

	ColorAxes();
	
	translate( [0,0,-1])
	Pie( angle=80, r=4 );

	scale([1,1,0.5])
	translate( [0,0,-2])
	Pie( angle=280, r=4 );
	
	translate( [0,0,-3])
	Pie( angle=360, r=5 ); //<==== note this

}
//Pie_demo();



//========================================================
Plane=[ "Plane", "pqr,opt", "n/a", "2D, Geometry, Shape", 
" 
 Given a 3-pointer pqr and opt, draw a plane in space based on
;; settings in opt:
;;
;;  [ 'mode', 1 // mode = 1~4
;;  , 'color', 'brown'
;;  , 'transp', .4
;;  , 'frame', false
;;  , 'r', 0.005
;;  , 'th', 0 // thickness
;;  ]
;;
;; # mode=1: rect parallel to axes planes
;;
;;  The 1st pt is the anchor(starting) pt
;;  The 2nd pt is treated like sizes:
;;     [ x,y,* ] = parallel to xy-plane (* = any)
;;     [ x,0,z ] = parallel to xz-plane
;;     [ 0,y,z ] = parallel to yz-plane
;;  The 3rd pt, r, is ignored.
;;  Plane( [ [1,2,3],[4,-5,6]], [['mode',1]] )
;;     draw a plane of size 4 on x, 5 on -y
;;     starting from [1,2,3]
;;
;; # mode=2 rectangle
;;
;;   line p1-p2 is a side of rectangle.
;;   p3 is on the side line parallel to line p1~p2
;;
;; # mode=3 triangle
;;
;; # mode=4 parallelogram
;;
;; # frame: default= false
;;
;;   Draw a frame around using Chain if frame is true or is
;;    a hash as the opt option for Chain. For example
;;
;;    Plane( pqr, [['frame', ['color', 'blue'] ]] )
;;
;;    draw a blue frame.
;;
;; NEW 2014.6.19: len(pqr) can be >3. But all faces connect to pqr[0].
"];

//doc(Plane);
//. Plane 

module Plane(pts, mode, h, color, closed, solid_color=false, actions, opt=[])
{
   //echom("Plane()");
   
   _pts  = arg(pts,opt,"pts");
   _h    = arg(h, opt, "h",0.01);
   _mode = arg(mode, opt, "mode", 1);
   _closed = arg(closed, opt, "closed", 1);
   _actions= argx(actions,opt,"actions",[]);
   _color= colorArg( color, opt, ["gold", 0.3]
                   , separate=!solid_color // setting separate=true 
                                           // allows to keep transparancy
                   );
   //echo(_mode=_mode);                
   
   module _Poly(pts,faces, site)
   {
     pts=transN( pts, _h, keep=true);
     
     TransformObj( ["pts", pts, "actions", _actions, "site", site] )
     { 
       Color( _color )
       polyhedron( points=pts, faces= faces );
     }  
   }
      
     if(_mode==1) // make all pts as a plane
     {
         pts0= movePts(_pts, to=ORIGIN_SITE);            
        _Poly( pts0, faces= rodfaces(len(_pts)), site=get(_pts,[0,1,2]) );
     }
     
     else if(_mode==2) // parallelogram
     {
       pts2= concat(_pts, [_pts[0]]);       
       
       for(i=[0:len(pts2)-3] )
       {
         p3= get(pts2,[i,i+1,i+2] );
         p30= movePts(p3, to=ORIGIN_SITE);
         p40= concat(p30, [p30[2]+p30[0]-p30[1]]);
         _Poly( p40, faces= rodfaces(4), site=p3 );         
       }        
     }

     else if(_mode==3) // triangle plane
     {
       pts3= concat(_pts, [ closed? [_pts[0]]
                                  : [ vecOp(_pts, [-3, [-1,-2], [-1,-2]])] 
                          ] );       
       pts3=_pts;
       for(i=[0:len(pts3)-3] )
       {
         p3= get(pts3,[i,i+1,i+2] );
         p30= movePts(p3, to=ORIGIN_SITE);
         _Poly( p30, faces= rodfaces(3), site=p3 );
       }
     }

     else if(_mode==4) // square
     {
       pts4= concat(_pts, _closed? [_pts[0]]
                                 : [ vecOp(_pts, [-3, [-1,-2], [-1,-2]])]
                    );       
       for(i=[0:len(pts4)-3] )
       {
         p3= get(pts4,[i,i+1,i+2] );
         //echo(i=i, p3=p3);
         p30= movePts(p3, to=ORIGIN_SITE);
         p40= squarePts(p30); 
          
         _Poly( p40, faces= rodfaces(4), site=p3 );
         
       }
     }

}

module Plot(fml, x, y, xys, Plane=true, Line, MarkPts, opt)
{
   //
   // 1919: xys = [ xs, ys ]
   //       if xys given, fml, x,y are ignored 
   //
   
   echom("Plot()");
   x = type(x)=="range"?[for(i=x)i]:x;
   y = type(y)=="range"?[for(i=y)i]:y;
   pts= xys? to3d(
                   zip(xys[0], xys[1])
                 ): 
        isstr(fml)? func(fml, y?["x", x, "y",y]:["x",x], returnWithVars=1)
                  : fml ;
   echo(pts=pts);
   //echo(pts=pts);

//  _Line = subprop( Line==undef? hash(opt, "Line", true) :Line   
//                 , opt, "Line", dfPropName= "r", dfPropType="num"
//                  , dfsub=["r",_r*.75, "color", _col, "$fn",6] );
//
//  _Plane = subprop( Plane==undef? hash(opt, "Plane", true) :Plane  
//                 , opt, "Line", dfPropName= "r", dfPropType="num"
//                  , dfsub=["r",_r*.75, "color", _col, "$fn",6] ); 
   
   if(Plane)
   { faces = faces(shape="mesh"
                  , nx=len(x) //isarr(x)?x:[for(i=x)i])
                  , ny=len(y) //isarr(y)?y:[for(i=y)i])
             );
     polyhedron( pts, faces ); //for(f=faces) Plane( get(pts,f) );
   }
   
   if(MarkPts)
   for( p = pts )
     MarkPt( p, opt=MarkPts );
     
   if(Line) Line(pts, opt=Line);  
}

module PlotMesh( mesh, nx, color="gold", Plane=true
               , Line, Line_x, Line_y, MarkPts, opt)
{
   // mesh could be: pts : [ [x,y,z] ....] ===> need nx, or will treat it as a square 
   //             or ptss: [ [ [x,y,z] ....], [ [x,y,z] ....], ...]
   
   //echom("PlotMesh()");
   //pts= [[0, 0, -0.21], [0, 1, -0.28], [0, 2, 0.04], [0, 3, 0.26], [1, 0, -0.29], [1, 1, 0.18], [1, 2, -0.08], [1, 3, -0.11], [2, 0, 0.03], [2, 1, -0.21], [2, 2, -0.12], [2, 3, 0.09], [3, 0, 0.21], [3, 1, -0.27], [3, 2, -0.11], [3, 3, 0.19], [4, 0, 0.1], [4, 1, 0.07], [4, 2, 0.25], [4, 3, 0.21]];
   
   //echom("PoltMesh()");
   
   is_mesh_pts= isnum(mesh[0][0]);
   pts = is_mesh_pts? mesh : [for(pts=mesh) each pts];
   ptss= is_mesh_pts? 
         let( nx = isnum(nx)? nx: floor(sqrt( len(mesh), 2)) 
            , ny = floor(len(mesh)/nx)
            )
            [ for(x=range(nx)) 
                 [for(y=range(ny)) mesh[ x*nx + y] ] 
            ]   
         : mesh;
                          
   nx = len(ptss);
   ny = len(ptss[0]);
   faces= faces( shape="mesh", nx=nx, ny=ny);
   
   //---------------------------------------------
   
   _col = colorArg(color, opt);
   _Plane= subprop( Plane 
                  , opt, "Plane"
                  , dfPropName="color"
                  , dfPropType=["str","arr", "float"]
                  , dfsub= ["color", ["gold",1] 
                           ] 
                 ); 
                 
   _Line= subprop( Line 
                  , opt, "Line"
                  , dfPropName="r"
                  , dfPropType=["num"]
                  , dfsub= [ "r", 0.0075
                           , "color", "purple" 
                           ] 
                 ); 
                 
   _Line_x= Line_x==undef? _Line
            : subprop( Line_x 
                  , opt, "Line_x"
                  , dfPropName="r"
                  , dfPropType=["num"]
                  , dfsub= update( [ "r", 0.0075
                                 , "color", "purple" 
                                 ], isarr(_Line)?_Line:[]
                                 )
                 ); 
                 
   _Line_y= Line_y==undef? _Line
            : subprop( Line_y 
                  , opt, "Line_y"
                  , dfPropName="r"
                  , dfPropType=["num"]
                  , dfsub= update( [ "r", 0.0075
                                 , "color", "purple" 
                                 ], isarr(_Line)?_Line:[]
                                 )
                 ); 
                 
   _MarkPts= subprop( MarkPts 
                  , opt, "MarkPts"
                  , dfPropName="r"
                  , dfPropType=["num"]
                  , dfsub= [ "r", 0.05
                           ] 
                 ); 
   
   if(_MarkPts)
   for( p = pts )
     MarkPt( p, opt=_MarkPts );
   
   //echo( Line= Line );  
   //echo( _Line= _Line );  
   //echo( Line_x= Line_x );  
   //echo( _Line_x= _Line_x );  
   //echo( Line_y= Line_y );  
   //echo( _Line_y= _Line_y );  
   
   if( _Line_x ) // ||(_Line && _Line_x!=undef))
   {  //echo(opt=_Line_x==undef?_Line:_Line_x);
      for(yi=range(ny))
        Line( [for(xi=range(nx)) ptss[xi][yi]]
            , opt=_Line_x); //==undef?_Line:_Line_x );    
   }
   
   if( _Line_y ) //||(_Line && _Line_y!=undef))
      for( ps=ptss) Line(ps, opt=_Line_y);
      
   if(_Plane)
   {  
      //echo(color=h(_Plane,"color"));
      //echo(_Plane=_Plane);
      //echo(_Line_y=_Line_y);
      Color( h(_Plane,"color"))  polyhedron( pts, faces ); 
   }
        
}


//x = range(0,.5,4);
//y = range(-4,.5,4);
//Plot("(x^2-y^2)/4", x=x, y=y);

module Poly(pf, pts, faces, center, color, frame, site, actions, markPts, dim
           , opt=[]
           )
{
   //echom("Poly()");
   // A polyhedron  
//   pts = hash(pf, "pts", pts);   
//   faces = hash(pf, "faces",faces); 
     
   pts= arg(pts, opt, "pts", pf[0]);
   faces= arg(faces, opt, "faces", pf[1]);    
   Color(color)
   //echo(pts = pts);
   //echo(faces=faces);
   polyhedron( points=pts, faces = faces );
} 
      
module PolyCube( size, center, color, frame, site, actions, markPts, dim
           , opt=[]
           )
{
   // A polyhedron Cube 

   decoData = objDecoHandler( color=color
                       , frame=frame, markPts=markPts
                       , dim=dim, opt=opt) ;

   ptsData = ptsHandler( cubeSize = size, center=center
           		, site=site, actions=actions, opt=opt) ;
   pts = hash( ptsData, "pts" );
   
   
   decoData = objDecoHandler( color=color
                       , frame=frame, markPts=markPts
                       , dim=dim, opt=opt) ;

   decoratePts( pts, opt=decoData );
   
   Color( decoData )
   polyhedron( points=pts, faces = rodfaces(4) );
}       
    
module PolyCylinder( h,r,r1,r2,center,d,d1,d2,$fn, $fa, $fs
               , color, site, actions, frame, markPts, dim, opt=[]
               )
{
   // A polyhedron Cylinder 

   ptsData= cylinderPtsHandler( h,r,r1,r2,center,d,d1,d2,$fn, $fa, $fs
                              , center, actions, site, opt, debug=false);
   pts = hash( ptsData, "pts" );                             
   
   // We need to set these values in the module scope 
   // to prevent WARNING: $fs too small - clamping to 0.010000             
   $fn = hash( ptsData, "$fn" );
   $fs = hash( ptsData, "$fs" );
   $fa = hash( ptsData, "$fa" );

   shape = is0( hash(ptsData,"d2"))?"cone"
           :is0( hash(ptsData,"d1") )?"conedown":"rod";
                                       
   decoData = objDecoHandler( color=color
                     , frame= frame
                     , markPts=markPts
                     , dim=dim
                     , opt = update( opt, ["frameShape", shape] )
                     ) ; 
   faces = shape=="cone"?
           concat( [range($fn), [ 0, $fn-1, $fn] ]
                   , [ for(i=range($fn-1)) [i,i+1, $fn] ]
                   )  
           : shape=="conedown"?
           concat( [range(1,$fn+1), [ 0, 1, $fn] ]
                   , [ for(i=range($fn-1)) [i,i+1, 0] ]
                   )  
           : faces( shape=shape, nside=$fn);
   
   decoratePts( pts, opt=decoData );
         
   Color( decoData )
   polyhedron( points= pts
             , faces = faces );
             
}           

           

module Primitive(name, data)
{
  function data( para,df)= hash(data, para, df);
  
  if(name=="cube")
  {
      cube( size= data( "size", 1)
          , center= data( "center", false)
          );
  }
  else if(name=="Cube")
  {
      Cube( size= data( "size", 1), opt=data );
          // size= data( "size", 1)
          //, center= data( "center", false)
          //);
  }
  else if(name=="sphere")
  {
    r  = data( "r",1);
    sphere( d  = data( "d", r*2)
          , $fa= data( "$fa", $fa )
          , $fs= data( "$fs", $fs )
          , $fn= data( "$fn", $fn )
          );      
  }  
  else if(name=="cylinder")
  {  
     r= data( "r", 1);
    r1= data( "r1", r);
    r2= data( "r2", r);
     
      cylinder( h= data("h")
              , d1= data("d1", r1*2)
              , d2= data("d2", r2*2)
              , center= data( "center", false)
              , $fa= data( "$fa", $fa )
              , $fs= data( "$fs", $fs )
              , $fn= data( "$fn", $fn )
              );               
  }
  else if(name=="Cylinder")
  {  
     r= data( "r", 1);
    r1= data( "r1", r);
    r2= data( "r2", r);
     
      Cylinder( h= data("h")
              , d1= data("d1", r1*2)
              , d2= data("d2", r2*2)
              , center= data( "center", false)
              , $fa= data( "$fa", $fa )
              , $fs= data( "$fs", $fs )
              , $fn= data( "$fn", $fn )
              );               
  }
  
}


//========================================================

PtGrid=["PtGrid", "pt,opt", "n/a","3D, Point, Line, Geometry" ,
"
 Given a pt and opt, draw thin cylinders from pt to axes or axis planes
;; for the purpose of displaying the pt location in 3D. Default opt:
;;
;;  [ 'highlightaxes', true
;;  , 'r',0.007    // line radius
;;  , 'projection', false, // draw projection on axis planes.
;;  ,                      // 'xy'|'yz'|'xy,yz'|...
;;  , 'mark90', 'auto' // draw a L-shape to mark the 90-degree angle
;;  , 'mode',2 // 0: none
;;             // 1: 2lines: pt~p_xy, p@xy~Origin
;;             // 2: plane and line: pt~p@xy, p@xy~x, p@xy~y
;;             // 3: block
;;      
;;         |
;;         |     |      mode 1
;;     ----+-----|-.---
;;        /  '-_ |
;;       :      ':
;;         |
;;         |     |       mode 2
;;     ----+-----|-.---
;;        /      |/
;;       :-------/
;;      / 
;;         |_______    mode 3
;;        /|      /|
;;       /-------+ |
;;    ---|-|-----|-+---
;;       |/      |/
;;       :-----'-/
;;      / 
"];
/*
    module Obj( opt=[], transl=[0,0,0] )
    {
        opt= update( ["arm", true]
                    ,opt);
        function opt(k,df)= hash(opt,k,df);

        df_arm=["len",3];
        
        opt_arm= getsubopt( opt
                    , com_keys= ["transp"]
                    , sub_dfs= ["arm", df_arm ]
                    );
        function opt_arm(k,df) = hash(opt_arm,k,df);
        
        echo("opt_arm",opt_arm);
        
        translate(transl) color(opt("color"), opt("transp"))
        cube( size=[0.5,1,2] );
        if ( opt_arm )
        {   translate( transl+[0,1, 1]) 
            color(opt_arm("color"), opt_arm("transp"))
            cube( size=[0.5,opt_arm("len"),0.5] );
        }   
    }
    
    
    module obj( opt=[] )
    {
        u_opt = opt;
        df_opt= [ "r",0.02
                , "heads",true
                , "headP",true
                , "headQ",true
                ];
        df_heads= ["r", 0.5, "len", 1];  
        df_headP= [];
        df_headQ= [];
                
        opt = update( df_opt, u_opt );
        function opt(k,df)= hash(opt,k,df);
        
        com_keys = [ "color","transp","fn"];
        
        opt_headP= getsubopt( opt=u_opt, df_opt=df_opt
                    , com_keys= com_keys;
                    , sub_dfs= ["heads", df_heads, "headP", df_headP ]
                    );
        opt_headQ= getsubopt( opt=u_opt, df_opt=df_opt
                    , com_keys=com_keys;
                    , sub_dfs= ["heads", df_heads, "headQ", df_headQ ]
                    );
            
        function opt_headP(k) = hash( opt_headP,k );
        function opt_headQ(k) = hash( opt_headQ,k ); 
    }
    
    
    PtGrid.line
    PtGrid.mark90
    
*/



//module PtGrid(pt, opt, mti=0)
//{
//    //if(ECHO_MOD_TREE) echom("PtGrid", mti);   
//    
//    df_proj  = []; // sub obj df
//    //df_mark90= []; // sub obj df     
//    df_axes  = ["r",0.02,"transp",0.5]; // sub obj df
//    com_keys = ["color","transp","r"]; // propt also be decided on root level
//    
//    df_opt = 
//        [ "r",0.005
//        , "transp", 0.4
//        , "color", undef
//        , "coord", [[1,0,0],[0,0,0],[0,1,0],[0,0,1]]
//        , "mode", 2 // 0: none
//                    // 1: line // pt:p_xy, p_xy:Origin
//                    // 2: plane and line //pt:p_xy, p_xy:x, p_xy:y
//                    // 3: block
//        
//        // -------sub obj-------
//        , "proj"  , false //"xy"|"yz"|"xy,yz"|"xy,yz,xz"| ...
//        , "mark90", false
//        , "axes"  , true
//        ]; 
//       
//    u_opt = opt;         // user opt. Set this so it's clear later
//    opt = update( df_opt, u_opt );
//    
//    //echo( opt = opt );
//    
//    function opt(k,df)= hash(opt,k,df);
//	
//    lineopt= hashex(opt,["color","transp","r"]);
//    //echo( lineopt = lineopt );
//    r= opt("r");
//	mode = opt("mode");
//	
//    proj = opt("proj");
//    axes = opt("axes");
//    //mark90 = opt("mark90");
//    
//    //============== sub obj =======================
//    function getsub( sub_dfs )=
//        subopt( opt=u_opt
//              , df_opt=df_opt
//              , com_keys= com_keys
//              , sub_dfs= sub_dfs
//              );
//    opt_axisx = getsub( ["axes", df_axes, "axisx", ["color","red"]] );
//    opt_axisy = getsub( ["axes", df_axes, "axisy", ["color","green"] ] );
//    opt_axisz = getsub( ["axes", df_axes, "axisz", ["color","blue"] ] );
//    opt_projxy= getsub( ["proj", df_proj, "projxy",["color","blue"] ] );
//    opt_projxz= getsub( ["proj", df_proj, "projxz",["color","green"] ] );
//    opt_projyz= getsub( ["proj", df_proj, "projyz",["color","red"] ] );     
//    //opt_mark90= getsub( ["mark90", df_mark90] );
//
//    //============== setup coord =====================
//
//    _coord = opt("coord");
//    coord = len(_coord)==3? coordPts(_coord): _coord;
//    //Coord(coord);
//    
//    aO= coord[1]; // points defining axis
//    aX= coord[0];
//    aY= coord[2];
//    aZ= coord[3];
//    // Frequently used point:
//    Pxy = projPt( pt, [aX,aO,aY]);
//    Pyz = projPt( pt, [aY,aO,aZ]);
//    Pxz = projPt( pt, [aX,aO,aZ]);
//            
//    module L_line( projPt, Pt3=aO ) // Draw an L line from pt to projPt 
//    {                            // to Pt3 (df=aO that is the ORIGIN)
//        pqr = [pt, projPt, Pt3 ];
//        Line( pqr, opt=lineopt);
//        //SimpleChain( pqr, lineopt );
//        //if(mark90) Mark90( pqr, opt=opt_mark90 );  
//    }    
//    
//    // =========== pt-plane Proj lines 
//    
//    if(proj)
//    {        
//        if (has(proj,"xy")) L_line( Pxy );
//        if (has(proj,"yz")) L_line( Pyz );
//        if (has(proj,"xz")) L_line( Pxz );
//    }    
//      
////    echo( _fmth(
////       ["proj",proj] , "has(proj,\"xy\")",haskey(proj, "xy") ]
////    ));
//    // ============= Grid lines 
//    
//    if(mode==1 && !proj && !has(proj,"xy"))  L_line( Pxy ) ;
//    else
//    {
//        Px =  othoPt( [ aO,pt,aX] ); 
//        Py =  othoPt( [ aO,pt,aY] ); 
//
//        L_line( Pxy, Px );  // pt - Pxy - Px
//        
//        if (mode==2)
//        {
//            //Line0( [Pxy,Py], opt=lineopt );
//            Line( [Pxy,Py], opt=lineopt);
//            //SimpleChain( [Pxy,Py], opt=lineopt );
////            if(mark90) {
////                Mark90( [pt,Pxy,Px], opt=opt_mark90);
////                Mark90( [pt,Pxy,Py], opt=opt_mark90);
////            }
//        }    
//        else if (mode==3)
//        {
//            Pz =  othoPt( [ aO,pt,aZ] ); 
//            Line([ Pyz, Pz, Pxz, Px, Pxy
//					       , Py, Pyz, pt, Pxy ],opt=lineopt);
//			      Line( [pt, Pxz], opt=lineopt); 
////            SimpleChain([ Pyz, Pz, Pxz, Px, Pxy
////					  , Py, Pyz, pt, Pxy ],opt=lineopt);
////			SimpleChain( [pt, Pxz], opt=lineopt); 
//            
////            Chain([ Pyz, Pz, Pxz, Px, Pxy
////					  , Py, Pyz, pt, Pxy ],opt=lineopt);
////			Line0( [pt, Pxz], opt=lineopt); 
//            
////            if(mark90) {
////                Mark90( [pt,Pxy,Px], opt=opt_mark90);
////                Mark90( [pt,Pxy,Py], opt=opt_mark90);
////            }
//        }    
//    }    
// 
//}
module PtGrid(pt, r, mode, color, Mark90, site, opt=[])
{
    //echom("PtGrid()");
    
    _r   = arg(r, opt, "r", LINE_R/5);
    _color= colorArg(color, opt, ["gold",0.5]);
    _mode= arg(mode, opt, "mode", 2);
    _site= arg(site, opt, "site", ORIGIN_SITE);
    _Mark90 = arg(Mark90, opt, "Mark90", true); 
    
    ptxy = projPt( pt, _site );
    //echo(_r=_r, _color=_color, _mode=_mode, _site=_site, _mark90=_mark90);
    
    module _Line(pt2) { Line(pt2, r=_r,color=_color); }
      
    if(_mode)
    {
      _Line([pt, ptxy]);
      
      if(_mode==1) _Line([ptxy,_site[1]]);
       
      if(_mode>=2)
      {
        B= B(_site); // binormal pt
        //echo(B=B);
        //translate(B) sphere(0.1);
        
        ptx = projPt( ptxy, [_site[0], _site[1]] );
        pty = projPt( ptxy, [B, _site[1]] );
        
        _Line([ptx, ptxy]);
        _Line([pty, ptxy]);
      
        if(_mode==3)
        {
           N = N(_site); // normal pt
           
           ptxz = projPt(pt, [_site[0], _site[1], N] );
           ptyz = projPt(pt, [N,B,_site[1]] );
           ptz  = projPt(pt, [N,_site[1]]);
           
           //echo(ptxz=ptxz);
           //translate( ptxz) sphere(r=0.02);
           
           pts=[ ptyz,pt,ptxz, ptz, ptyz
                  , pty, _site[1], ptx, ptxz,pt ];
           
           _Line( pts );
           _Line([ ptz, _site[1]] );
            
        } // _mode==3      
      } // _mode>=2
    } //_mode  
      
} // PtGrid


//. R

//========================================================
Rod=["Rod", "pqr,ops", "n/a", "Geometry,Line",
 " Given a 2- or 3-pointer (pqr), draw a line (cylinder) from point P
;; to Q using polyhedron. The 3rd point, R, is to make a plane (of pqr), 
;; from that the angle of drawn points start in a anti-clock-wise manner 
;; (when looking from P-to-Q direction). So R is to determine where the
;; points start. If it is not important, use 2-pointer for pqr and R will
;; be randomly set. 
;;
;; Imagine pqr as a new coordinate system where P is on the x axis, Q is 
;; the ORIGIN and R is on xy plane: 
;; 
;;       N (z)
;;       |
;;       |________
;;      /|'-_     |
;;     / |   '-_  |
;;    |  |      : |
;;    |  Q------|---- M (y)
;;    | / '-._/ |
;;    |/_____/'.|
;;    /          '-R
;;   /
;;  P (x)
;;
;; In the graph below, P,Q,R and 0,4 are co-planar. 
;;
;;       _7-_
;;    _-' |  '-_
;; 6 :_   |Q.   '-4
;;   | '-_|  '_-'| 
;;   |    '-5' '-|
;;   |    | |    '-_
;;   |  p_-_|    |  'R
;;   |_-' 3 |'-_ | 
;;  2-_     |   '-0
;;     '-_  | _-'
;;        '-.'
;;          1
;;
;; headangle/tailangle are 90, but customizable.
;;
;;                         R
;;                        :
;; |       .--------.    :  |
;; |     .'          '. :   |  
;; |   P'--------------Q.   | 
;; | .' pa            qa '. |
;; |------------------------|
"];

//========================================================
 
//module Rod_b4155t( pqr=randPts(3), ops=[]) //r=0.6, count=6, showindex=true)
//{
//	// A Rod making use of rodPts
//
//	//echom("Rod");
//
//	//------------------------------------------
//	ops = concat
//	( ops,
//	, [ 
//	  //------ To be sent to rodPts()
//		"r", 0.05
//	  , "nside",     6
//	  , "label", "" 
//	  , "p_pts",undef  // pts on the P end
//	  , "q_pts",undef  // pts on the q end
//	  , "p_angle",90   // cutting angle on p end
//	  , "q_angle",90   // cutting angle on q end
//	  //, "twist_angle", 0 // see twistangle()
//	  , "rotate",0  	  // rotate about the PQ line, away from xz plane
//	  , "cutAngleAfterRotate",true // to determine if cutting first, or
//								  // rotate first. 
//	  //------ The above are to sent to rodPts()
//	
//	  , "has_p_faces", true
//	  , "has_q_faces", true
//	  , "addfaces",[]
//	  , "addpts",[]
//
//	  , "echodata",false
//	  , "markrange",false // Set to true or some range to mark points
//						 //if true, full range
//	  , "markOps",  []    // [markpt setting, label setting] 
//	  , "labelrange",false
//	  , "labelOps", []
//	  ]
//	, OPS
//	);
//	function ops(k)=hash(ops,k);
//	nside = ops("nside");
//
//	rodpts = rodPts( pqr, ops);
//    echo(rodpts = rodpts);
//	p_pts = rodpts[0];
//	q_pts = rodpts[1];
//
//	// Check if angles are correct:
//	//	echo( "angle p:", angle( [Q(pqr),P(pqr), p_pts[0]] ));
//	//	echo( "angle q:", angle( [P(pqr),Q(pqr), q_pts[0]] ));
//
//	allpts = concat( p_pts, q_pts, ops("addpts") );
//	//echo("Rod.allpts: ", allpts);
//
//	_label = ops("label");
//	label= _label==true?"PQ":_label;
//
//	//if(label) LabelPts( p01(pqr), label ) ;
//
////	p_faces = reverse( range(nside) );
////	// faces need to be clock-wise (when viewing at the object) to avoid 
////	// the error surfaces when viewing "Thrown together". See
////	// http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
////	q_faces = range(nside, nside*2); 
////	faces = concat( ops("has_p_faces")?[p_faces]:[]
////				 , ops("has_q_faces")?[q_faces]:[]
////				 , rodsidefaces(nside)
////				 , ops("addfaces")
////				 );
////	faces = rodfaces(nside);
//	faces = faces("rod",nside);
//	
//	echo("Rod.faces:", faces);
//
//	//=========== marker and label =============
//	allrange= range(allpts);
//	markrange = hash(ops, "markrange" , if_v=true, then_v=allrange);
//	labelrange= hash(ops, "labelrange", if_v=true, then_v=allrange);
//	markops = or(hash(ops, "markops"),[]); 
//	labelops= or(hash(ops, "labelops"),["labels",""]); 
//	pts_to_mark = [ for(i=markrange)	allpts[i] ];
//	pts_to_label= [ for(i=labelrange)	allpts[i] ];
//
//	// We make them BEFORE the polyhedron so they are always visible
//	if(markrange){ MarkPts( pts_to_mark, markops); }
//	if(labelrange){ LabelPts( pts_to_label, hash(labelops, "labels"), labelops); }
//	// =========================================
//
//	color( ops("color"), ops("transp") )
//	polyhedron( points = allpts 
//			  , faces = faces
//			 );
//
//	//==========================================
//	if( ops("echodata"))
//	{
//		echo(
//			 "<b>Rod()</b>:<br/><b>ops</b> = ", ops
//			,join([""
//			,str("<b>q_pts</b> = ", q_pts )
//			,str("<b>p_pts</b> = ", p_pts )
//			,str("<b>faces</b> = ", faces )
//			],";<br/>"));
//	}
//}
////========================================================

module _Rod_dev( pqr=randPts(3), ops=[]) //r=0.6, count=6, showindex=true)
{
	// This is the original version before rodPts() was built. After we 
	// have rodPts, the rod point calc was taken out so the current Rod()  
	// simply makes use of rodPts();

	//echom("Rod");

	//------------------------------------------
	ops = concat
	( ops,
	, [ "r", 0.05
	  , "nside",     6
	  , "label", "" 
	  , "p_pts",undef  // pts on the P end
	  , "q_pts",undef  // pts on the q end
	  , "p_angle",90   // cutting angle on p end
	  , "q_angle",90   // cutting angle on q end
	  , "rotate",0  // rotate about the PQ line, away from xz plane
	  , "cutAngleAfterRotate",true // to determine if cutting first, or
						  // rotate first. 
	  , "echodata",false

	  , "markrange",false // Set to true or some range to mark points
						 //if true, full range
	  , "markOps",  []    // [markpt setting, label setting] 
	  , "labelrange",false
	  , "labelOps", []
	  ]
	, OPS
	);

	function ops(k) = hash( ops, k);

	//echo("Rod.ops:",ops);
	//echo("Rod, p-, q-angle: ", [p_angle, q_angle] );

	r = ops("r");
	rot = ops("rotate");
	nside = ops("nside");
	p_angle = ops("p_angle");
	q_angle = ops("q_angle");
	cutAngleAfterRotate= ops("cutAngleAfterRotate");

	//echo("nside", nside);
	//------------------------------------------
	// pqr0: original pqr before rotate
	//
	pqr0= pqr==undef
		 ? randPts(3)
		 : len(pqr)==2
		   ? concat( pqr, [randPt()] )
		   : pqr ;

	//pq = p01(pqr0);
	P = pqr0[0];
	Q = pqr0[1];
	R1= pqr0[2];
	pq= [P,Q];
	N = N(pqr0);
	M = N2(pqr0);

	// Before rotation: coplanar: P Q A B 0 4 R1
	//
	//                          _-5
	//                       _-'.' '.
	//                    _-' .'     '.
	//                 _-'  .'         '.
	//              _-'   _6    _-Q----_-4--------------A 
	//            1'   _-'  '_-'   '_-'.'            _-'
	//          .' '.-'   _-' '. _-' .'           _-'
	//        .' _-' '. -'    _-'. .' '.       _-'
	//      .'_-'   _-''.  _-'   _-7    R1  _-'
	//	  2-'     P-----0-'----------------B     
	//      '.         .'  _-'
	//        '.     .' _-'
	//          '. .'_-'
	//            3-'
	//                     
	// After rotation by 45 degree: coplanar: P Q 4 0 R2 (R2 not shown)  
	//                 
	//	      5-------4
	//	     /|      /|
	//	    / |  Q'-/----------A
	//	   /  |  /\/  |       /
	//	  /   6-/-/---7      /
	//   /   / / /  \/      /
	//	1-------0   /\  	  /
	//	|  / /  |  /  R1  /
	//	| / P.--|-/------B
	//	|/    '.|/          
	//  2-------3          
	//           
	// The pl_PQ40 rotates (swings like a door) to make 0-4 move away 
	// from pl_PQAB. So [P,Q,R2] will be the new pqr to work with.

	// So, if rotation, we make a R2 that is on the pl_PQ40 to make a new 
	// pqr: [P,Q,R2] 

	// We use the othoPt T as the rotate center to carry the rotation:
	//
	//	       N (z)
	//	       |
	//	       |
	//	      /|
	//	     / |       R2  
	//	    |  |     .' 
	//	    |  Q---_------- M (y)
	//	    | / '-:_
	//	    |/_-'   '._
	//	  T /----------'-R1
	//	   /
	//	  P (x)
	//   /
	//  /
	// P2

	// Calc R2 = post-rotation R using T

	T = othoPt( [ P,R1,Q ] );

	// Since we need to get R2 by rotating an angle on the PTR1 plane, 
	// of which its normal is decided by the order of P,T, R1, the location 
	// of T is critical that it could get the rotation to the wrong side.
	// 
	// (1)
	//      R1_             PQ > TQ
	//      |  '-._
	//	p---T------'Q
	//
	// (2)           R1     PQ > TQ     
	//              /|
	//     P-------Q T
	//   
	// (3)   R1_            PQ < TQ
	//       |  '-._
	//       |       '-._
	//       T  p-------'Q
	// (4)
	//              R1      PQ < TQ
	//            .'|
	//          .'  |
	//        .'    |
	//   P---Q      T
	//   
	// Shown above, case (3) will cause PTR1 to go to wrong direction. 
	// So we make a new p1_for_rot as P to ensure that it is always on 
	// the far left:

	p1_for_rot= angle(pqr0)>=90
				? P
				: onlinePt([norm(T-Q)>=norm(P-Q)? T:P,Q], len=-1 );

	R2 = anyAnglePt( [ p1_for_rot, T, R1]     
                   , a= rot 
                   , pl="yz"
                   , len = dist([T,R1]) 
				  );

	P2 = onlinePt( [P,Q], len=-1);  // Extended P: P2----P----Q

	norot_pqr_for_p = [P2,P, R1];
	norot_pqr_for_q = [P, Q, R1];
	rot_pqr_for_p   = [P2,P, R2];
	rot_pqr_for_q   = [P, Q, R2];

	pqr_for_p = cutAngleAfterRotate&&rot>0
				? rot_pqr_for_p
				: norot_pqr_for_p;
	pqr_for_q = cutAngleAfterRotate&&rot>0
				? rot_pqr_for_q
				: norot_pqr_for_q;		

	// p_pts90 = [p0,p1,p2,p3]
	// q_pts90 = [p5,p6,p7,p8]
	// 
	// If p_angle=0, and p_pts not given, p_pts90 will
	// be the final points on p end for polyhedron 

	// p_pts90 and q_pts90 decides the points of side lines. 
	// It doesn't matter if cutAngleAfterRotate is true, 'cos 
	// the position of side lines is independent of cut-angle.
	// So we use rot_pqr_for_p. It will be = rot_pqr_for_p
	// If rotate=0 (then R2=R1).
	// 
	// Later we will project these lines to p_plane and q_plane,
	// Those planes are what decides the final sideline pts. 
	p_pts90= arcPts( rot_pqr_for_p
				  , r=r, a=360, pl="yz", count=nside );

	q_pts90= arcPts( rot_pqr_for_q
				   , r=r, a=360, pl="yz", count=nside );

	//Line( [P, p_pts90[0] ],["r",0.05] );
	//MarkPts( [P2],["colors",["purple"]] );
	//MarkPts( p_pts90 );
	//Chain( p_pts90 );

	//-------------------------------------------------
	// Cut an angle from P end. 
	//
	// If cutAngleAfterRotate, we want to use the pqr 
	// BEFORE rotate.
	Ap = anyAnglePt( cutAngleAfterRotate
					? [P2,P,R1]
					: [P2,P,R2]
					//rot_pqr_for_p 
					// cutAngleAfterRotate
					//? norot_pqr_for_p
					// : rot_pqr_for_p 
				  , a=p_angle+(90-p_angle)*2
				  , pl="xy"); 
	Np = N([ Ap, P, Q ]);  // Normal point at P
	//echo("Nh= ", Nh);
	//MarkPts( [Ah], ["labels",["Ah"]] );
	p_plane = [Ap, Np, P ];  // The head surface 
	//Plane( headplane, ["mode",4] );
	p_pts=or( ops("p_pts")
			  , p_angle==0
			   ? p_pts90
			   : 	[ for(i=range(nside)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], p_plane ) ]
			  );

	//-------------------------------------------------
	// Cut an angle from Q end. 
	Aq = anyAnglePt( cutAngleAfterRotate
					? [P,Q,R1]
					: [P,Q,R2]
					//rot_pqr_for_q
					// cutAngleAfterRotate
					//? rot_pqr_for_q
					// : norot_pqr_for_q 
				  , a=q_angle //90-q_angle
				  , pl="xy"); // Angle point for tail
	Nq = N( cutAngleAfterRotate
					? norot_pqr_for_q
					: rot_pqr_for_q );  // Normal point at Q
	//echo("Nt= ", Nt);
	//MarkPts( [At], ["labels",["At"]] );

	// q_plane is the surface of the final pts landed. Final pts
	// are determined by extending the side lines to meet the 
	// q_plane where the intersections are those final pts are. 
	//
	// These side lines will be the same no matter it's rotate-cut_angle
	// or cut_angle-then-rotate. 
	//
	// The q_plane, however, will be different (cut_angle before or
	// after rotate). 

	q_plane = [Aq, Nq, Q ];//:pqr0;  // The head surface 
	//Plane( headplane, ["mode",4] );
	q_pts=or( ops("q_pts")
			  , q_angle==0
			    ? q_pts90
			    : [ for(i=range(nside)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], q_plane ) ]
			  );


//	echo("rot = ", rot);
//	echo("angle( [ p_pts[0], P, Q] )= ", angle( [ p_pts[0], P, Q] ));
//	echo("angle( [ q_pts[0], Q, P] )= ", angle( [ q_pts[0], Q, P] ));

//	if( tailangle==90 ){ tailpts = tailpts90
//	}

	allpts = concat( p_pts, q_pts );

	_label = ops("label");
	label= _label==true?"PQ":_label;

	//if(ops("showsideindex")) LabelPts( allpts, range(allpts) ) ;
	if(label) LabelPts( pq, label ) ;

	p_faces =  range(nside); 
	q_faces = range(nside, nside*2); 
	faces = concat( [p_faces], [q_faces], rodsidefaces(nside) );
	//echo("faces: ", faces);


	//=========== marker and label =============
	allrange= range(allpts);
	markrange = hash(ops, "markrange" , if_v=true, then_v=allrange);
	labelrange= hash(ops, "labelrange", if_v=true, then_v=allrange);
	markops = or(hash(ops, "markops"),[]); 
	labelops= or(hash(ops, "labelops"),["labels",""]); 
	pts_to_mark = [ for(i=markrange)	allpts[i] ];
	pts_to_label= [ for(i=labelrange)	allpts[i] ];

	// We make them BEFORE the polyhedron so they are always visible
	if(markrange){ MarkPts( pts_to_mark, markops); }
	if(labelrange){ LabelPts( pts_to_label, hash(labelops, "labels"), labelops); }
	// =========================================

	color( ops("color"), ops("transp") )
	polyhedron( points = allpts 
			  , faces = faces
			 );

	// We make marker AFTER the polyhedron so they are hidden behind
	// the object
	
//	echo_( "markrange={_}, markops={_}",[markrange,markops]);
//	echo_( "labelrange={_}, labelops={_}",[labelrange,labelops]);

	//
	// To check if the p-, q-angle work:
	//
//	echo_("Rod: cutAngleAfterRotate:{_}, p_angle={_}, angle( [Q, P, allpts[0]] )={_} "
//		, [cutAngleAfterRotate, p_angle, angle( [Q, P, allpts[0]] )] 
//		);
//	echo_("Rod: cutAngleAfterRotate:{_}, q_angle={_}, angle( [P, Q, allpts[{_}]] )={_} "
//		, [cutAngleAfterRotate, q_angle, nside, angle( [P,Q, allpts[nside]] ) ]
//		);
  


	//==========================================
	if( ops("echodata"))
	{
		echo(
			 "<b>Rod()</b>:<br/><b>ops</b> = ", ops
			,join([""
			,str("<b>q_pts</b> = ", q_pts )
			,str("<b>p_pts</b> = ", p_pts )
			,str("<b>faces</b> = ", faces )
			],";<br/>"));
	}
}


//========================================================
//module Obj(opt=[]){
//    
//    df_opt=[ 
//             "bone",[]
//           , "seed", []
//           , "xpts", []
//    
//           //, "markpts", false
//           , "plane", false
//           , "labels", false
//           , "frame",false
//           ];
//    
//    
//}  

//========================================================

//


module Rod( pqr=pqr(), opt=[] ){
    
    echom("Rod");
    
   // MarkPts( pqr, ["chain",true,"label",false,"plane",true]);
    //["text",["Po","Qo","Ro"]]] );
    
    df_opt= 
    [
      "nside",6
    , "transp",1 
    
    , "dim", false
    , "bone", false
    , "markxsec", false
    , "frame", false
    , "radlines", false
    , "hide", false 
    , "echopts", false
    ];
    
    //echo(pqr=pqr);
    _opt= isstr(opt)?sopt(opt):opt;
    opt = update( df_opt, _opt );
    function opt(k,nf)= hash(opt,k,nf);
    //echo(opt=opt);
    
    //----------------------------------------------
    df_markxsec= ["r",0.06,"chain"
                  ,["color","black","r",0.02, "closed",true]
                  ] ;
    markxsecopt= subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markxsec"
                     , df_subopt= df_markxsec
                     , com_keys=[]
                     );
    //----------------------------------------------
    
    nside= or(len( opt("xsecpts")),opt("nside"));
    pqr = len(pqr)==2?app(pqr, randPt()):pqr;
    //echo(pqr=pqr);

    ptsPnQ = rodPts( pqr, opt );
    allpts = joinarr( get(ptsPnQ,[0,1]) );
    
    if(opt("echopts")) {
        echo(pqr = pqr );
        echo( str( "ptsPnQ=<br/>",
        arrprn( ptsPnQ, dep=2)));
    }    
    //----------------------------------------------
    df_markpts=  ["r",0.06,"chain",["r",0.02,"closed",true]
                 ,"label",true] ;
        
    if( opt("markpts") ) {
        
        markptsopt= subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markpts"
                     , df_subopt= df_markpts
                     , com_keys=[]
                     );
        
        //Label has to be treated as a whole for both p-face
        // and q-face. chain, however, has to be treated 
        // seperatedly. 
        if( hash(markptsopt,"label"))
            MarkPts( allpts, update( markptsopt, ["chain",false]));
        
        nolabel = update( markptsopt, ["label",false]);
        MarkPts( ptsPnQ[0], nolabel);                 
        MarkPts( ptsPnQ[1], nolabel);                 
    }
    //----------------------------------------------
    if( opt("frame") ) {        

        frameopt= subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "frame"
                     , df_subopt= [ "markpts",false, "label",false
                                  , "chain",[ "r",0.01,"color","green"
                                            , "closed",true] 
                                  ]
                     , com_keys=[]
                     );
                   
        for(i=range(nside)){
            Line0( [allpts[i], allpts[i+nside]]
                  , opt= hash(frameopt,"chain") );
        }
        
        mp_opt= update( df_markpts, frameopt );
        mp_label = hash( frameopt, "label" );
        MarkPts( ptsPnQ[0], mp_opt );                 
        MarkPts( ptsPnQ[1]
           , update( mp_opt
                   , mp_label==true?
                        ["label",["text", range(nside,2*nside)]
                     ]:mp_label?mp_label:[]
                   )
               );
    }
    //----------------------------------------------
    radlinesopt= subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "radlines"
                     , df_subopt= ["color","green","r",0.01]
                     , com_keys=[]
                     );
    if(radlinesopt){
        for(i=range(nside)){
              Line0( [pqr[0], allpts[i]], opt= radlinesopt );   
              Line0( [pqr[1], allpts[i+nside]], opt= radlinesopt );   
        }
    }    
    //----------------------------------------------
    boneopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "bone"
                     , df_subopt=["color","black","r",0.06
                                 ,"chain",["color","black","r",0.02]
                                 ,"label","PQ"
                                 ]
            
                     , com_keys=[]
                     );
    if( boneopt ) MarkPts( p01(pqr),boneopt); 
    //----------------------------------------------
    
    
//    MarkPts( get(ptsPnQ,[0,1,2])
//           , ["chain",true,"label",["text","PQR","shift",1,"color","red"]] );
    
//    MarkPts( [ ptsPnQ[2],P(pqr)], ["chain",true
//              , "label",["text",["Mp",""]]] );
//    MarkPts( [ ptsPnQ[3],Q(pqr)], ["chain",true
//              , "label",["text",["Mq",""]]] );
    
    //echo(nside=nside);
    
    //echo( ptsPnQ = arrprn(ptsPnQ, dep=2) );
//    MarkPts( ptsPnQ[0], ["chain",true,"label",true] );
//    MarkPts( ptsPnQ[1], ["chain",true,"label",true] );
    
    //MarkPts( ptsPnQ[1], ["chain",["color","red"],"color","red"] );
    //MarkPts( addz(opt("xsecpts"),0), "cl");
    
	if( !opt("hide")){
        faces = faces("rod",nside );
        color( opt("color"), opt("transp") )
        polyhedron( points = allpts 
                  , faces = faces
                 );
    }//if
}//Rod 

//

//Rod( randPts(3, d=[1,6]),opt=["rotate",30, "xsecpts",
//[[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]] ]
//);

//pts= [[-1.01092, 2.59624, -0.851656], [2.5989, -2.61717, -0.630292], [-1.81425, -1.01677, -1.92352], [-8.23054, 13.0231, -1.29439], [-1.01092, 2.59624, -0.851656]];
//MarkPts( pts, ["chain",true,"label",["text",["Po","Qo","Ro", "Px","P"]]]);

//pqr= [[3.68999, 3.18951, 2.36557], [1.56907, 1.60905, 1.90884], [1.23829, 1.73406, 2.36217]];
//echo( rodPts(pqr ) );
//MarkPts( pqr, "c,p,l");
//Mark90( pqr );
//echo( angle( pqr ) );
//
//echo( rotpqr( pqr, 0) ) ;
 

module RotFromTo(pb,pe) 
{   // Rotate obj from one direction pb to another pe (no worry about angle) 
    // about an axis passing [0,0,0]. pb, pe: vector
    // by Ronaldo
    // http://forum.openscad.org/rotate-45-45-0-tp24013p24015.html
 
    if( norm(pb-pe)==0 || norm(pb)==0 || norm(pe)==0 ) 
        children();
    else 
        // Note: pe/norm(pe)+pb/norm(pb) is an angleBysectPt 
        mirror(pe/norm(pe)+pb/norm(pb)) mirror(pb) children();
}

//========================================================
{ // rotObj
module RotObj( axis
             , a 
             , center=[0,0,0] // = translation of obj when it is built.
             )
{
    translate( axis[1] ) 
    multmatrix(m= rotM( axis,a) ) 
    {
       translate( center-axis[1] ) // center: where obj is in xyz coord
       children();
    }           
}    

    RotObj= ["RotObj", "axis,a,center=[0,0,0]", "n/a","Geometry",
    " Module to rotate obj @center about axis for angle a.
    ;;
    ;; center is the translation when the obj is created.
    ;;
    ;;    RotObj( axis, a, center=loc )
    ;;        color(clr, 0.4) 
    ;;        cube( lens ); 
    "
    ];
} // RotObj


module RotObjFromTo( pqr ) // rot obj from QP to QR 2018.6.11
{   
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
    P0 = P-Q; R0=R-Q;
    
    if( norm(P0-R0)==0 || norm(P0)==0 || norm(R0)==0 ) 
        children();
    else 
    {
      translate( Q )
      RotFromTo( P0, R0 )
      translate(-Q )
      children();
    }
}    

    RotObj= ["RotObj", "axis,a,center=[0,0,0]", "n/a","Geometry",
    " Module to rotate obj @center about axis for angle a.
    ;;
    ;; center is the translation when the obj is created.
    ;;
    ;;    RotObj( axis, a, center=loc )
    ;;        color(clr, 0.4) 
    ;;        cube( lens ); 
    "
    ];


//. Ruler
 
module Ruler( range
            , scale
            , color
            , show0 // display the first tick and label 0. Can be turn off 
                    // for Coord()
            , Majortick=true
            , Minortick=true
            , negative=false
            , Label, r, site, opt=[])
{
   //echom("Ruler()");
   range= arg(range, opt, "range", [0,5]); 
   r  = arg(r, opt, "r", LINE_R/5);
   _col = colorArg(color, opt, ["darkblue",1]);
   negative = arg(negative, opt, "negative", false);
   //ruler_ends=  [ORIGIN, [len,0,0]]; 
   //echo(opt=opt);
   
   mj = subprop(Majortick, opt, "Majortick", dfsub=[ "scalelen", 1
                                                   , "ticklen", 0.2
                                                   //, "color", _col
                                                   , "r", r
                                                   ]);
   mi = subprop(Minortick, opt, "Minortick", dfsub=[ "scalelen", 0.2
                                                   , "ticklen", 0.1
                                                  // , "color", _col
                                                   , "r", r
                                                   ]);
                                                   
   mj_scale= hash(mj,"scalelen");
   mi_scale= hash(mi,"scalelen");
   
   mj_len= hash(mj,"ticklen");
   mi_len= hash(mi,"ticklen");
   //echo(mj_len=mj_len, mi_len=mi_len);
   
   len = range[1]-range[0];
   pts = onlinePts(  [ORIGIN, [len,0,0]]
                  , lens=mi? [ for(i=[0:floor(len/mi_scale)]) i*mi_scale+range[0]] 
                           : [ for(i=[0:floor(len/mj_scale)]) i*mj_scale+range[0]] );
   
   scale = scaleArg(scale, opt, mi? (mj_scale/mi_scale): mj_scale );
   
   //echo( range=range, site=site, len=len, mj=mj, mi=mi, mi_scale=mi_scale    
   //    , lens=[ for(i=[0:floor(len/mi_scale)]) mi_scale]); //pts=pts);
   //echo(pts=pts);
        
   _show0 = arg(show0, opt, "show0", true);
   
   module _draw()
   {    
     MarkPts(pts, Label=false, Line=["r",r, "color", _col]
          , Ball=false //["color","black","r",0.015, "dec_r", 0]
          );
     for( i= range(pts) )
     {
        val = i/scale[0]*mj_scale+range[0];
              
        ismj = isint(val/mj_scale); // => if i = a major tick

        //echo("<hr/>", i=i, val=val, ismj=ismj, _show0=_show0);
        
        if( ismj )
        {  
           if((val==0&&_show0)||val!=0)
           {
            //echo("Drawing");
            
            Line( [pts[i], pts[i]+[0,mj_len,0]], opt=mj );
            MarkPt( pts[i]+[0,mj_len+0.08,0]
                  , Label=[ "text", val
                         , "halign","center", "scale",0.7   
                         , "followView",false, "indent", 0]
                 , Ball=false
                 );
           }      
        }   
        else if(mi)
           Line( [pts[i], pts[i]+[0,mi_len,0]], opt=mi );
       
     }    
   }
   
   site = arg(site,opt,"site", ORIGIN_SITE);
   Color( _col) //colorArg(color, opt,"darkblue"))
   if(site)
     Move(to=site)_draw();
   else
     _draw();  
   //MarkPts(site);
   
}   


//. S,T

module SimpleChain(
        chdata
        , opt
        , color
        //, transp
        , shape
        //, faces // Use this if defined. Otherwise, determed automatically using shape
        , closed
        )
{   //echom(_bcolor("SimpleChain()","green"));
    //echo(opt = opt );
    //echo(chdata = _fmth(chdata) );
    
    opt = popt(opt);
    function opt(k,df)=hash(opt, k, df);
    color  = colorArg(color, opt);
    //transp = und( transp, opt("transp",1));
    closed = und( chdata, opt("closed"));    
    
    _chdata = und( chdata, opt("chdata"));
    //echo("\n",_chdata = _chdata );
    chdata = ispt(_chdata[0])?
             let( df_opt = [ "nside", 4, "r",0.01 ]
                , bone = _chdata
                ) simpleChainData( bone, opt=update( df_opt,opt ) )
             : _chdata;
    //echo("\n",chdata = chdata );
    //echo( opt = opt );
    function ch(k,df) = hash( chdata, k, df );
    closed = und(closed, opt("closed", ch( "closed" ))) ;
    //echo(closed = closed );
    bone   = ch( "bone" );
    cuts   = ch( "cuts" );
    pts    = joinarr( cuts );
    
    shape  = und( shape, opt("shape", closed?"ring":"chain" )); 
//    _shape  = und( shape, opt("shape"));
//    shape = ch( _shape, closed?"ring":"chain" ); 
    n = len( bone );
    nseg = len(cuts)-1; //n-1;
    nside= len( cuts[1] );
    faces = faces(shape, nside=nside, nseg = nseg );
    
    //echo(shape=shape, nside=nside, nseg=nseg);
    //echo(faces= _fmth(["faces",faces]));
    //echo(pts=_fmth(["pts",pts]));
    //MarkPts(pts);
    Color(color)    
    polyhedron( points = pts, faces = faces );
}    
//SimpleChain(pqr());

//========================================================
Text=["subarr","arr, cover=[1,1], cycle=true", "array", "Array",
 " Given an array (arr) and range of cover (cover), which is in the form
;; of [m,n], i.e., [1,1] means items i-1,i,i+1, return an array of items.
;;
;; Set cycle=true (default) to go around the beginning or end and resume 
;; from the other side. 
"];

/*
text 
String. The text to generate.
size 
Decimal. The generated text will have approximately an ascent of the given value (height above the baseline). Default is 10.
Note that specific fonts will vary somewhat and may not fill the size specified exactly, usually slightly smaller.
font 
String. The name of the font that should be used. This is not the name of the font file, but the logical font name (internally handled by the fontconfig library). This can also include a style parameter, see below. A list of installed fonts & styles can be obtained using the font list dialog (Help -> Font List).
halign 
String. The horizontal alignment for the text. Possible values are "left", "center" and "right". Default is "left".
valign 
String. The vertical alignment for the text. Possible values are "top", "center", "baseline" and "bottom". Default is "baseline".
spacing 
Decimal. Factor to increase/decrease the character spacing. The default value of 1 will result in the normal spacing for the font, giving a value greater than 1 will cause the letters to be spaced further apart.
direction
String. Direction of the text flow. Possible values are "ltr" (left-to-right), "rtl" (right-to-left), "ttb" (top-to-bottom) and "btt" (bottom-to-top). Default is "ltr".
language
String. The language of the text. Default is "en".
script
String. The script of the text. Default is "latin".
$fn
used for subdividing the curved path segments provided by freetype
Example
*/

module Sweeping_by_Marko()
{
  // The following code is by Marko (berkenb)
  // http://forum.openscad.org/Sweeping-polygon-over-irregular-shape-tp24801p24880.html
  
  function flatten(vec) = [for (v=vec) for(e=v) e]; 
  function Q_im(q) = [q[1], q[2], q[3]]; 
  function Q_conj(q) = [q[0], -q[1], -q[2], -q[3]]; 
  function Q_mult(q,p) = 
  [(q[0]*p[0]-q[1]*p[1]-q[2]*p[2]-q[3]*p[3]),(q[1]*p[0]+q[0]*p[1]+q[2]*p[3]-q[3]*p[2]),(q[2]*p[0]+q[0]*p[2]-q[1]*p[3]+q[3]*p[1]),(q[3]*p[0]+q[0]*p[3]+q[1]*p[2]-q[2]*p[1])];                       
  function rotQ(q, a, n) = Q_mult(flatten([cos(a/2),n*sin(a/2)]),q); 
  function poly_rotQ(list, q) = [for (v=list) 
  Q_im(Q_mult(q,Q_mult([0,v.x,v.y,v.z],Q_conj(q))))]; 
  function poly_rot2d(list, a) = [for (x=list) [cos(a)*x[0]+sin(a)*x[1], 
  -sin(a)*x[0]+cos(a)*x[1]]]; 
  function poly_translate(list, d) = [for (v=list) v+d]; 
  function interp_lists(l1, w1, l2, w2) = [for (i=[0:len(l1)-1]) 
  w1*l1[i]+w2*l2[i]]; 

  function poly_loft_faces (N_z, N_x, closed=false) = flatten([ 
       (closed ? ([for (i=[0:N_x-1]) [(N_z-1)*N_x+i, (N_z-1)*N_x+(i+1)%N_x, 
  i], 
         for (i=[0:N_x-1]) [(i+1)%N_x, i, (N_z-1)*N_x+(i+1)%N_x]]) 
        : concat([[for (i=[0:N_x-1]) N_x-1-i]], [[for (i=[0:N_x-1]) 
  (N_z-1)*N_x+i]])), // caps 
         for (i=[0:N_z-2],j=[0:N_x-1]) [[(i+1)*N_x+j, i*N_x+j, 
  i*N_x+((j+1)%N_x)],[i*N_x+((j+1)%N_x), (i+1)*N_x+((j+1)%N_x), 
  (i+1)*N_x+j]]]); 


  // extrude a cross section linearly interpolated between cross sections cr1 
  // and cr2 along path 'path', 
  // with optional tangential twist linearly increasing along path 
  module loft (path, cr1, cr2, twist=0) { 
    p = flatten([path, [2*path[len(path)-1]-path[len(path)-2]]]); 
    pts = flatten([ 
      for (i=1, d=p[1]-p[0], u=cross([0,0,1], d), un=norm(u), dn=norm(d), 
  a=asin(un/dn), 
           q=un>0?rotQ([1,0,0,0],a,u/un) : [1,0,0,0], n=d/dn, cr=cr1; 
           i<len(p); 
           d=p[i]-path[i-1], u=cross(n, d), un=norm(u), dn=norm(d), 
  a=asin(un/dn), 
           n=d/dn,q=un>0?rotQ(q,a,u/un):q, 
  cr=interp_lists(cr1,1-(i-1)/(len(p)-1),cr2,(i-1)/(len(p)-1)), i=i+1) 
        poly_translate(poly_rotQ(twist!=0?[for(v=poly_rot2d([for (v=cr) 
  [v.x,v.y,0]],i*twist/(len(p)-1))) [v.x,v.y,0]]:[for (v=cr) [v.x,v.y,0]], q), 
  p[i-1]) 
    ]); 
    fcs = poly_loft_faces(len(path), len(cr1)); 
    polyhedron(pts, fcs, convexity=8); 
  } 

  pH = [[-1, 1], [-0.8,1], [-0.8, 0.1], [0.8, 0.1], [0.8, 1], [1, 1], 
        [1, -1], [0.8, -1], [0.8, -0.1], [-0.8, -0.1], [-0.8, -1], [-1, -1]]; 
  pH2 = [for (v=pH) 2*v]; 
  phelix = [for (i=[0:6:3*360]) 5*[cos(i), sin(i), i/360]]; 

  //loft(phelix, pH, pH2, -170); 
  //----------------------------
  pts = 24; 
  l = 2; 
  psquare =  [for (i=[0:pts/4-1]) [-l/2, l/2-i/pts*4*l], 
     for (i=[pts/4:pts/2-1]) [-l/2+(i-pts/4)/pts*4*l, -l/2], 
     for (i=[pts/2:3*pts/4-1]) [l/2, -l/2+(i-pts/2)/pts*4*l], 
     for (i=[3*pts/4:pts-1]) [l/2-(i-3*pts/4)/pts*4*l, l/2] 
   ]; 
  pcircle = [for (i=[0:pts-1]) [sin(-i*360/pts), cos(-i*360/pts)]]; 
  phelix = [for (i=[0:6:3*360]) 5*[cos(i), sin(i), i/360]]; 
  phelix = [for (i=[0:1:180]) 5*[cos(i), sin(i), i/360]]; 

  loft(phelix, pcircle, psquare); 
}

module Text( text, size,font,halign,valign,spacing,direction,language,script,$fn
           , scale, color, format, template, actions, site, opt)
{

  /* 2018.5.29:

     If len(site)==4, then use site as the box, find the center
    
      R-------C-------S
      |       |       |
      |       B-------A    
      |               | 
      Q---------------P
      
     site= [P,Q,R,S] will be converted to [A,B,C] and both 
     valign and halign will be set to "center" by default
  
  */
  
  //echom("Text()");
  //
  //echo( text= arg(text,opt,"text") 
      //, size= sizeArg(size,opt) 
      //, font= arg(font,opt,"font") 
      //, halign= arg(halign,opt,"halign") 
      //, valign= arg(valign,opt,"valign") 
      //, spacing= arg(spacing,opt,"spacing") 
      //, direction= arg(direction,opt,"direction")
      //, language= arg(language,opt,"language")
      //, script= arg(script,opt,"script")
      //, $fn= arg($fn,opt,"$fn", 12)
      //, scale= scaleArg(scale,opt,0.1)
      //, actions= argx(actions,opt,"actions")
      //, site= arg(site,opt,"site")
                //
      //);
  //echom(_s("Text(\"{_}\")",text));   
  _scale= scaleArg(scale,opt, 1)*SCALE; //* 0.1;
  //echo("In Text", scale=scale, opt=opt, _scale=_scale);   
  //_actions= argx(actions, opt, "actions");
  //_site= arg(site,opt,"site", ORIGIN_SITE);
  //
  _fmt = arg(format,opt,"format");
  _text0= arg(text,opt,"text");
  _fmttext = _fmt? _f(_text0,_fmt):_text0; 
  
  _tmpl = arg(template,opt,"tmpl");
  _text = str(_tmpl? _s2(_tmpl,_fmttext):_fmttext); 
    
  _site = arg(site, opt, "site");
  site2 = len(_site)==4? 
           [ (_site[0]+_site[3])/2
           , sum(_site)/4
           , (_site[2]+_site[3])/2 
           ]
         : _site;  
   
   valign = arg(valign, opt, "valign", len(_site)==4? "center":undef ); 
   halign = arg(halign, opt, "halign", len(_site)==4? "center":undef ); 
   
   //echo("In Text, before getTransformData");  
   transdata=  getTransformData( shape= "text" 
           	               , size = undef
                               , center=false
           	               , site=site2
           	               , actions=actions
           	               , opt=opt) ; 
   //echo(transdata = transdata); 
   //echo("In Text, after getTransformData"); 
   
//  echo("In Text", text=text
//      , _fmt=_fmt
//      , _text0=_text0
//      , _fmttext=_fmttext
//      , _tmpl=_tmpl
//      , _text=_text, opt=opt
//        , _actions=_actions, _site=_site);
//  //echo(_actions=_actions);
//  echo("In Text, _scale=", _scale);      
  Color( colorArg(color,opt) )
  //TransformObj( actions= _actions, site=_site, opt=opt )     
  TransformObj( transdata )     
  scale( _scale )
  text( text= _text  
      , size= sizeArg(size,opt) 
      , font= arg(font,opt,"font") 
      , halign= arg(halign,opt,"halign")  
      , valign= arg(valign,opt,"valign")  
      , spacing= arg(spacing,opt,"spacing") 
      , direction= arg(direction,opt,"direction")
      , language= arg(language,opt,"language")
      , script= arg(script,opt,"script")
      , $fn= arg($fn,opt,"$fn")
      );

}           


module Text_old(pqr, txt){
    
  	module transform(followVP=ops("vp"))
	{
		translate( midPt( [L0,L1] ) )
	     color( ops("color"), ops("transp") )
		if(followVP){ rotate( $vpr ) children(); }
		else { rotate( rotangles_z2p( Q-P ) )
			  rotate( [ 90, -90, 0] )
			  children();
			}
	}
	conv = ops("conv");
	//echo("conv",conv, "L", L, "numstr(L, uconv=uconv)", numstr(L, conv=conv));
	transform()
	scale(0.04) text(str(ops("prefix")
						  //, L, "="
						  , numstr(L
								, conv=ops("conv")
								, d=ops("decimal")
								, maxd=ops("maxdecimal")
								)
						  , ops("suffix") ));
    
  
}

module Text_dev(){

  module AxisLines( oxyz
      , lnops=[]
      , plops=[]
    )
  {
      ln_ops= concat( lnops, ["r",0.03, "transp", 0.3]);
      pl_ops= concat( lnops, ["color","blue", "mode",3, "transp",0.2]); 
      
      Line( p01( oxyz), concat(["color", "red"], ln_ops) );
      Line( p02( oxyz), concat(["color", "green"], ln_ops) );
      Line( p03( oxyz), concat(["color", "blue"], ln_ops) );
      
      color( "green", hash(pl_ops,"transp") )
      { Plane( p123( oxyz), pl_ops );
        
      }    
      
//      color( "green", hash(pl_ops,"transp") )
//      { Plane( p012( oxyz), pl_ops );
//        Plane( p023( oxyz), pl_ops );
//        Plane( p013( oxyz), pl_ops );
//      }    
  }    
  
  pqr = randPts(3);
  //pqr = [[-1.8992, -2.12299, -1.62216], [1.25559, -0.48364, -1.36525], [-1.10772, -1.4157, 0.591315]];
//  pqr = [[-2.84926, -0.433825, 1.42911], [0.0140438, 1.59454, 1.47263], [0.313024, 0.256869, 0.98651]]; 
  // ERROR:
  // pqr= [[0.982085, -0.443367, -2.85914], [2.66226, 2.26935, 0.904825], [2.38218, 2.66048, 0.311629]]
  echo("pqr", pqr);
  
  P= pqr[0]; 
  Q= pqr[1];
  R= pqr[2];
  N= onlinePt( [Q, N(pqr)]  , len= 3 );
  M= onlinePt( [Q, N([N,Q,P])], len= 3 );

 
  O = Q;
  X = P;
  Y = M;
  Z = N;
  // New coordinate: 0:Q, x:P, y:M, z:N
  MarkPts( [O,X,Y,Z], ["labels","OPQR"] );
  
  
  // ==============================
    
  xyzo_0 = [O,X,Y,Z];

  //AxisLines( xyzo_0 );

  // ==============================

  xyzo_1 = [ for(p=xyzo_0) p+(ORIGIN-O)];
      
  AxisLines( xyzo_1,plops=["color","green"]);
  
  MarkPts( xyzo_1, ["labels","OXYZ"] );
  
  Arrow2( [ O, xyzo_1[0] ]
       , ["color","red", "arrow",["r",0.15,"len",0.4], "arrowP",false ] );
  
  //========================    

  A = [ norm(xyzo_1[1]), 0, 0];
  MarkPts([A], ["labels","A"] );
  
  apts = arcPts( [A,ORIGIN, xyzo_1[1]],r=A.x, count=20 ); 
  Line( [ORIGIN,A] , ["r",0.005]);
  Chain( apts, ["closed",false, "r",0.01] );
    
  
  D = N( [A, ORIGIN, xyzo_1[1]], len=3);  
  YY = xyzo_1[2];
  ZZ = xyzo_1[3];
  
  T = othoPt( [ORIGIN, YY, D]);
  Tz= othoPt ( [ORIGIN, ZZ, D]);
  
  Line( [YY,T], ["r",0.03] );
  Mark90( [YY,T,ORIGIN] ); 
  MarkPts( [D,T], ["labels", "DT"] );
  Line( extendPts( [ORIGIN, D], len=[5,5]), ["r", 0.03,"color","red"]); 
  
  //---------------------------------------
  a_xoa = angle([ xyzo_1[1], ORIGIN, A]);
  
  B= anyAnglePt( [ORIGIN, T, YY]
                , a=a_xoa, len= norm(T-YY), pl="yz"
                ); 
  MarkPts( [B], ["labels","B"] );              
  Chain( [ORIGIN, B,T], ["closed",false, "r",0.03] );              

  Line( [ORIGIN,B] , ["r",0.01]);
  aptsY = arcPts( [B,T, YY],r= norm(B-T) ); 
  echo("aptsY", aptsY);
  Chain( aptsY, ["closed",false,"r",0.01] );
  
//---------------------------------------
  a_xoa = angle([ xyzo_1[1], ORIGIN, A]);
  
  C= anyAnglePt( [ORIGIN, Tz, ZZ]
                , a=-a_xoa, len= norm(Tz-ZZ), pl="yz"
                ); 
  MarkPts( [C], ["labels","C"] );              
  Chain( [ORIGIN, C,Tz], ["closed",false, "r",0.03] );              

  AxisLines( [ORIGIN, A,B,C] );
}


module Tube( h,r,r1,r2,center,d,d1,d2, th, $fn, $fa, $fs
           , color
           , pts // pre-defined pts
           , Frame=true
           , echoPt=0
           , site, actions, MarkPts, Dim // compound args
           , opt=[]
           )
{
   //echom(_red("Tube()"));
   
   //---------------------------------------------------
   // getTransformData():
   //
   // Getting the data for transformations (actions,size,etc)
   //
   // This would be used in transforming the obj in the TransformObj()
   // Ideally we could have skip this and do them all in the
   // TransformObj(). However, we also need this to create pts for
   // framing, pt tracking and pt decorations. So we use getTransformData 
   // as a bridge.  
   _transdata = getTransformData( shape="tube"
                         , h=h
                         , r=r
                         , r1=r1
                         , r2=r2
                         , center=center
			 , d=d
                         , d1=d1
                         , d2=d1
                         , th=th
                         , $fn=$fn
                         , $fa=$fa
                         , $fs=$fs
                         , site=site
			 , actions=actions
			 , opt=opt
			 );
   //echo(actions=actions);			 
   //echo(_transdata= _fmth(_transdata));			  
   pts = pts?pts: hash( _transdata, "pts" );
   transdata= update(_transdata, ["echoPt", arg(echoPt,opt,"echoPt",-1)]);
   
   //MarkPts(pts);
   // Need to declare public $fs/$fa otherwise it might give warning of
   // WARNING: $fs too small - clamping to 0.010000
   $fs=h(transdata, "$fs");
   $fa=h(transdata, "$fa");
   $fn=h(transdata, "$fn");
   
   //echoPt = arg(echoPt,opt,"echoPt",0);
   //if( echoPt>0 )
   //{  echo(pts = pts);
      //if   
   //}   
   //echo(transdata = transdata);
   //echo(transdata = transdata, size=hash(transdata, "size"), actions=hash(transdata, "actions"));
   _color= colorArg(color, opt);
   Color( _color )
   TransformObj( delkey( transdata,"actions") /// Need to del actions 'cos they
                                              /// already been handled. This is 
                                              /// different than Cube and Cylinder.
                                              /// (We use built-in solids for Cube
                                              ///  and Cylinder but poly for Tube)  
               )  
     polyhedron( points=pts, faces=faces(shape="tube", nside=$fn ));
//
   //---------------------------------------------------
   // Now we do the pt decorations:
   //
   DecoratePts( pts //, color=color
              , Frame= Frame==true?["shape","tube"]
                       : Frame? update( Frame, ["shape","tube"])
                       : false
              , faces = faces("tube", nside=$fn, nseg=1)
              //, faces = 
              
              , Frame_df = [ "r", 0.005, "color", "black"     
                           , "Joint", ["shape","corner","$fn",4]]
              , MarkPts=MarkPts, MarkPts_df= ["color", _color]
              , Dim=Dim, Dim_df=["color", _color]
              , opt=opt );
}



module TransformObj( transdata=[]) //pts, size, center, scale, actions, site, opt=[])
{
   /*
     transData is the return of getTransformData()
   */  
   //echom("TransformObj()");
 
   site = hash(transdata, "site");
   //echo(site=site);
   actions = hash(transdata, "actions"); 
   //echo(site=site, actions=actions,  transdata=transdata );
   
   echoPts= hash(transdata, "echoPts",-1);
   if(echoPts>-1)
   { 
     pts= hash(transdata, "pts");
     echo( pts = roundPts(pts, d=echoPts) );
   }
   
   if(site)
     Move( to=site )
     Transforms( actions )
     children();  
   else
     Transforms( actions )
     children();
}

module UnitCoord(pqr, opt=[])
{
   // given pqr, draw a unit coord using arrows (2018.12.2)
   echom("UnitCoord()");
   cpts = coordPts(pqr,len=1);
   Red() Arrow( [cpts[0], cpts[1]], opt=opt );
   Green() Arrow( [cpts[0], cpts[2]], opt=opt );
   Blue() Arrow( [cpts[0], cpts[3]], opt=opt );
   //Mark90([_X,Q,_Y]);
   //Mark90([_X,Q,_Z]);
   //Mark90([_Z,Q,_Y]);
}


//. w




//========================================================
module Write(
    text
  , at // a pqr as the destination
  //------ the following are taken from doc, except size changed from 10 to 4 -----
  , size /* Decimal. The generated text will have approximately an ascent of the given value (height above the baseline). Default is 4.
    Note that specific fonts will vary somewhat and may not fill the size specified exactly, usually slightly smaller.*/
  , font /* String. The name of the font that should be used. This is not the name of the font file, but the logical font name (internally handled by the fontconfig library). A list of installed fonts can be obtained using the font list dialog (Help -> Font List).*/
  , halign /* String. The horizontal alignment for the text. Possible values are "left", "center" and "right". Default is "left".*/
  , valign /* String. The vertical alignment for the text. Possible values are "top", "center", "baseline" and "bottom". Default is "baseline".*/
  , spacing /* Decimal. Factor to increase/decrease the character spacing. The default value of 1 will result in the normal spacing for the font, giving a value greater than 1 will cause the letters to be spaced further apart.*/
  , direction /* String. Direction of the text flow. Possible values are "ltr" (left-to-right), "rtl" (right-to-left), "ttb" (top-to-bottom) and "btt" (bottom-to-top). Default is "ltr".*/
  , language /* String. The language of the text. Default is "en".*/
  , script /* String. The script of the text. Default is "latin".*/
  , $fn /* used for subdividing the curved path segments provided by freetype*/
  //-------------------------------------------
  , color
  , transp
  , scale /*default =0.1*/
  , shift /* a 3-pointer, xyz, for shifting toward x,y and z BEFORE move*/
  , rot /* [rota, roty, rotz] */
  , th /* thickness of text , df=1*/
  , childcare /* "interaction", "difference", "-difference" */
  , opt=[] 
    ){
        
    opt = popt(opt);
    function opt(k,df)= hash(opt,k,df);
    //echom(str("Write, opt= ", opt));
    //echo( write_opt= opt );
        
    size= und(size, opt("size",4));
    font= und(font, opt("font"));
    halign= und(halign, opt("halign","left"));
    valign= und(valign, opt("valign","baseline"));
                           /*top|center|baseline|bottom*/
    spacing= und(spacing, opt("spacing",1)); /* Decimal*/
    direction=und(direction,opt("direction")); /*ltr|rtl|ttb|btt*/
    language= und(language,opt("language","en"));
    script= und(script,opt("script","latin")); 
    $fn= und($fn,opt("$fn"));
        
    color= und(color, opt("color"));
    transp= und(transp,opt("transp"));
    scale = und(scale, opt("scale",1));
   
    //P=pqr[0]; Q=pqr[1]; N=N(pqr); B=B(pqr);
    shift = und(shift, opt("shift",[0,0,0]));
    childcare= und( childcare, opt("childcare") );
    
    //echo(shift=shift);
    //echo( Write_color=color, transp=transp); 
    //echo( size=size, scale = scale, shift=shift );
   
    rot = und( rot, opt("rot",[0,0,0]) );
    th = und( th, opt("th",1) );
   
   module TextObj(){
       //echom("TextObj");
       //echo( color=color, text=text);
       if(text)
           color(color,transp)
           translate( shift ) 
           scale(scale)
           rotate( rot ) //und(rot,[0,0,0]) )
           linear_extrude( th )
           text( str(text)
               , size=size
               , font=font
               , halign=halign
               , valign=valign
               , spacing=spacing
               , direction=direction
               , language=language
               , script=script
               , $fn=$fn
               );
   }   
  
   module Childcare(cc){
        //echom("Childcare");
        //echo( childcare = cc, , text=text );
        if(cc=="difference"){
            difference() {TextObj(); children();}
        }
        else if(cc=="-difference"){            
            difference() {children();TextObj(); }
        }
        else if(cc=="intersection"){            
            intersection() {children();TextObj(); }
        }
        else { //echo("no childcare", text=text); 
              TextObj(); children();}
   }    
   
   if(at) Move( to=at )  Childcare( childcare )children();  
   else  Childcare( childcare )children();
   
}
