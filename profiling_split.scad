/*
 This file compares the speed of 3 versions of hash, one is coded with
 list comprehension (hash_list), the 2nd and 3rd with the built-in search()
 (hash_search, hash_search2).
 
 data type for hash_list    : [ k0,v0, k1,v1, ...]
 data type for hash_search  : [ [k0,v0], [k1,v1], ...]
 data type for hash_search2 : [ k0,v0, k1,v1, ...]   // same as hash_list
 
 Two factors: data-size (how many k-v pairs) and run-count.
 
 ref :
 http://forum.openscad.org/Digging-into-search-td12421.html
 
*/

//include <../doctest/doctest.scad>
/*
 Change the DATA_SIZE, RUN_COUNT, then select hash version. Run and write down the time it takes:
 
==========================================
DATA   RUN         time (seconds)  
size  Count     list   search search2
==========================================
1000    200       0       0      0
1000   1000       1       0      0
1000   2000       3       0      0
1000   5000       6       0      1
1000  10000      12       1      1

2000    500       1       0      0
2000   1000       2       0      0
2000   3000       8       1      1
2000   5000      14       2      2
==========================================



*/

DATA_SIZE=  1000;
RUN_COUNT=  1000;

RECUR = 1;
SEARCH=2;

choice= RECUR;
choice= SEARCH;

//==================================================================
//
//  Unmark this section to run profiling
//
if( choice==LIST ) speedtest_recur();
else if( choice==SEARCH ) speedtest_search();
  
//==================================================================

strunit = "abcde";

function makedata(_rtn="", _i=0)=
(
   if(i==DATA_SIZE) ? _rtn
     : makedata( str(_rtn,strunit), _i+1)
);

data = makedata();
   

function split_reur(s, sp="a", _rtn=[], _bf="", _i=0)= //,notfound=undef)=
(
  	_i==len(s)? _rtn
   : s[_i]==sp?
     split_reur(s=s, sp=sp, _rtn=concat(_rtn,[_bf]),  _bf="", _i=_i+1)
   : split_reur(s=s, sp=sp, _rtn=_rtn,  _bf=str(_bf,s[_i]), _i=_i+1)
);
   
//function split_reur(s, sp="a", _rtn=[], _bf="", _i=0)= //,notfound=undef)=
//(
//   s==""? empty
//    :sp==""? [s]
//    : s==sp?(keep? (keepempty?["",s,""]:[s])
//            :(keepempty?["",""]:[])
//            )    
//    :i==0? concat( empty, sp2, split( slice(s,1), sp, keep, keepempty)  )
//    :i==len(s)-1?  concat( [slice(s,0,-1)],sp2, empty)
//    :i<0 ? [s]
//	   : concat(
//		   [ slice(s, 0, i) ]
//         , sp2
//		 , split( slice(s, i+len(sp)), sp, keep, keepempty ) 
//    )
//);

function split_search(s, sp="a", _rtn=[], _js=[], _i=-1)=
(
   _js==[]? 
      split_search(s, sp, _rtn=[], _js=search( [sp], s, 0), _i=0)
   : _js==[]? [s]
     //: split_search(s, sp, _rtn=concat(_rtn, , _js=_js, _i=_i+1)
   : len(_js)==_i? _rtn
     //split_search(s=s, sp=sp, _rtn=concat(_rtn,[_bf]),  _bf="", _i=_i+1)
   : split_search(s=s, sp=sp, _rtn=_rtn,  _bf=str(_bf,s[_i]), _i=_i+1)
);    

);   
 
 
function hash_search(h,k)= 
(
   h[ search([k],h)[0]][1]   
);          

function hash_search2(h,k)= 
(
  h[search([k], [for(i=[0:2:len(h)-2])h[i]])[0]*2+1]   
); 
      
function hash(h,k)= 
(         
    hash_choice== LIST
    ? hash_list(h,k)
    :hash_choice== SEARCH
    ? hash_search(h,k)
    : hash_search2(h,k)   
);           


module speedtest_list()
{
    echo("speedtest_list, DATA_SIZE=", DATA_SIZE, ", RUN_COUNT= ", RUN_COUNT);
    rtn = 
     [ for(k=keys_to_use) hash_list( data, k )] ;
    echo("speedtest_list --- done");
}

module speedtest_search()
{
    echo("speedtest_search, DATA_SIZE=", DATA_SIZE, ", RUN_COUNT= ", RUN_COUNT);
    rtn = 
     [ for(k=keys_to_use) hash_search( data, k )] ;
    echo("speedtest_search --- done");
         
}

module speedtest_search2()
{
    echo("speedtest_search2, DATA_SIZE=", DATA_SIZE, ", RUN_COUNT= ", RUN_COUNT);
    rtn = 
     [ for(k=keys_to_use) hash_search( data, k )] ;
    echo("speedtest_search2 --- done");
         
}

//==================================================================
//
//  Unmark this section to run profiling
//

//if( hash_choice==LIST ) speedtest_list();
//else if( hash_choice==SEARCH ) speedtest_search();
//else  speedtest_search2();
  
//==================================================================
hash = ["hash", "h,k,notfound"];
module hash_test( mode=112 ){ 
	h= 	hash_choice== SEARCH?
      [ [-50, 20 ]
			, ["xy", [0,1] ]
			, [2.3, 5 ]
			, [[0,1],10]
			, [true, 1 ]
			, [1, false]
			, [-5]
			]
     : [ -50, 20 
			, "xy", [0,1] 
			, 2.3, 5 
      , 2.3, 6
			,[0,1],10
			, true, 1 
			, 1, false
			,-5
			];

	rtn=doctest( hash,  
    [ str("> h= ", h) //_fmth(h, eq=", ", iskeyfmt=true))
    , ""
    , [ hash(h, -50), 20, "h, -50" ]
    , [ hash(h, "xy"), [0,1], "h,'xy'"  ]
    , [ hash(h, 2.3), 5, "h,2.3" ]
    , [ hash(h, [0,1]), 10, "h,[0,1]" ]
    , [ hash(h, true), 1, "h,true" ]
    , [ hash(h, 1), false, "h,1" ]
    , [ hash(h, "xx"), undef, "h,'xx'" ]
    ,""
    , "## key found but it's in the last item without value:"
    ,""
    , [ hash(h, -5), undef, "h, -5" ]
//    , "## notfound is given: "
//    , [ hash(h, -5,"df"), "df", "h,-5,'df'", ["//", "value not found"] ]
//    , [ hash(h, -50,"df"), 20, "h,-50,'df'" ]
//    , [ hash(h, -100,"df"), "df", "h,-100,'df'" ]
    ,""
    , "## Other cases:"
    ,""
    , [ hash([], -100), undef, "[],-100" ]
    , [ hash(true, -100), undef, "true,-100" ]
    , [ hash([ [undef,10]], undef), undef, "[[undef,10]],undef" ]
    , " "
//    , "## Note below: you can ask for a default upon a non-hash !!"
//    , [ hash(true, -100, "df"), "df", "true,-100,'df'"]
//    , [ hash(true, -100), undef, "true,-100 "]
//    , [ hash(3, -100, "df"), "df", "3,-100,'df'"]
//    , [ hash("test", -100, "df"), "df", "'test',-100,'df'"]
//    , [ hash(false, 5, "df"), "df", "false,5,'df'"]
//    , [ hash(0, 5, "df"), "df", "0,5,'df'" ]
//    , [ hash(undef, 5, "df"), "df", "undef,5,'df'" ]
//    , "  "
    ], mode=mode //,["h",h]
    );
    echo( rtn );
}    
//hash_test();  