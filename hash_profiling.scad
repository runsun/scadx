/*
 This file compares the speed of 3 versions of hash, one is coded with
 list comprehension (hash_list), the 2nd and 3rd with the built-in search()
 (hash_search, hash_search2).
 
 data type for hash_list    : [ k0,v0, k1,v1, ...]
 data type for hash_search  : [ [k0,v0], [k1,v1], ...]
 data type for hash_search2 : [ k0,v0, k1,v1, ...]   // same as hash_list
 
 Two factors: data-size (how many k-v pairs) and run-count.
 
 ref :
 http://forum.openscad.org/Digging-into-search-td12421.html
 
*/

//include <../doctest/doctest.scad>
/*
 Change the DATA_SIZE, RUN_COUNT, then select hash version. Run and write down the time it takes:
 
==========================================
DATA   RUN         time (seconds)  
size  Count     list   search search2
==========================================
1000    200       0       0      0
1000   1000       1       0      0
1000   2000       3       0      0
1000   5000       6       0      1
1000  10000      12       1      1

2000    500       1       0      0
2000   1000       2       0      0
2000   3000       8       1      1
2000   5000      14       2      2
==========================================



*/

DATA_SIZE=  1000;
RUN_COUNT=  1000;

LIST = 1;
SEARCH=2;
SEARCH2=3;

hash_choice= LIST;
//hash_choice= SEARCH;
//hash_choice= SEARCH2;

//==================================================================
//
//  Unmark this section to run profiling
//

if( hash_choice==LIST ) speedtest_list();
else if( hash_choice==SEARCH ) speedtest_search();
else  speedtest_search2();
  
//==================================================================

// Return a list of keys, each key is 4-char in length 
function makekeys( count=DATA_SIZE )= 
(
   [ for(i=[0:count-1]) 
       str( chr( rands(65,65+52,1)[0] )
          , chr( rands(65,65+52,1)[0] )
          , chr( rands(65,65+52,1)[0] )
          , chr( rands(65,65+52,1)[0] )
          )
         //dt_join( [for(x=rands(65,65+52,4)) chr(x) ], "" )
    ]
);
        
// randomly make DATA_SIZE keys
keys = makekeys( DATA_SIZE ); 
         
// make keys into [key,i] pairs         
pairs= [ for(i=[0:len(keys)-1])[ keys[i],i ] ]; 
   

// Create data. If hash_choice=List, create: [ key0,0, key1,1, ... ]
// if hash_choice=SEARCH, create: [ [key0,0], [key1,1], ...]
data = hash_choice==SEARCH?
       [ for( i=[0:len(keys)-1] )
            [keys[i],i] ]
       : [ for( i=[0:len(keys)*2-1] )
           i%2==0? keys[i/2]: (i-1)/2 ] ;
       
//echo(keys);
//echo(data);

// keys to use in profiling run (total of RUN_COUNT). They could have
// repeats.
keys_to_use = [ for (i= rands(0, len(keys)-1, RUN_COUNT)) keys[i] ];


function hash_list(h,k)= //,notfound=undef)=
(
  [ for(i=[0:2:len(h)-2])
      if( h[i]==k ) h[i+1] ][0]
);
 
 
function hash_search(h,k)= 
(
   h[ search([k],h)[0]][1]   
);          

function hash_search2(h,k)= 
(
  h[search([k], [for(i=[0:2:len(h)-2])h[i]])[0]*2+1]   
); 
      
function hash(h,k)= 
(         
    hash_choice== LIST
    ? hash_list(h,k)
    :hash_choice== SEARCH
    ? hash_search(h,k)
    : hash_search2(h,k)   
);           


module speedtest_list()
{
    echo("speedtest_list, DATA_SIZE=", DATA_SIZE, ", RUN_COUNT= ", RUN_COUNT);
    rtn = 
     [ for(k=keys_to_use) hash_list( data, k )] ;
    echo("speedtest_list --- done");
}

module speedtest_search()
{
    echo("speedtest_search, DATA_SIZE=", DATA_SIZE, ", RUN_COUNT= ", RUN_COUNT);
    rtn = 
     [ for(k=keys_to_use) hash_search( data, k )] ;
    echo("speedtest_search --- done");
         
}

module speedtest_search2()
{
    echo("speedtest_search2, DATA_SIZE=", DATA_SIZE, ", RUN_COUNT= ", RUN_COUNT);
    rtn = 
     [ for(k=keys_to_use) hash_search( data, k )] ;
    echo("speedtest_search2 --- done");
         
}

//==================================================================
//
//  Unmark this section to run profiling
//

//if( hash_choice==LIST ) speedtest_list();
//else if( hash_choice==SEARCH ) speedtest_search();
//else  speedtest_search2();
  
//==================================================================
hash = ["hash", "h,k,notfound"];
module hash_test( mode=112 ){ 
	h= 	hash_choice== SEARCH?
      [ [-50, 20 ]
			, ["xy", [0,1] ]
			, [2.3, 5 ]
			, [[0,1],10]
			, [true, 1 ]
			, [1, false]
			, [-5]
			]
     : [ -50, 20 
			, "xy", [0,1] 
			, 2.3, 5 
      , 2.3, 6
			,[0,1],10
			, true, 1 
			, 1, false
			,-5
			];

	rtn=doctest( hash,  
    [ str("> h= ", h) //_fmth(h, eq=", ", iskeyfmt=true))
    , ""
    , [ hash(h, -50), 20, "h, -50" ]
    , [ hash(h, "xy"), [0,1], "h,'xy'"  ]
    , [ hash(h, 2.3), 5, "h,2.3" ]
    , [ hash(h, [0,1]), 10, "h,[0,1]" ]
    , [ hash(h, true), 1, "h,true" ]
    , [ hash(h, 1), false, "h,1" ]
    , [ hash(h, "xx"), undef, "h,'xx'" ]
    ,""
    , "## key found but it's in the last item without value:"
    ,""
    , [ hash(h, -5), undef, "h, -5" ]
//    , "## notfound is given: "
//    , [ hash(h, -5,"df"), "df", "h,-5,'df'", ["//", "value not found"] ]
//    , [ hash(h, -50,"df"), 20, "h,-50,'df'" ]
//    , [ hash(h, -100,"df"), "df", "h,-100,'df'" ]
    ,""
    , "## Other cases:"
    ,""
    , [ hash([], -100), undef, "[],-100" ]
    , [ hash(true, -100), undef, "true,-100" ]
    , [ hash([ [undef,10]], undef), undef, "[[undef,10]],undef" ]
    , " "
//    , "## Note below: you can ask for a default upon a non-hash !!"
//    , [ hash(true, -100, "df"), "df", "true,-100,'df'"]
//    , [ hash(true, -100), undef, "true,-100 "]
//    , [ hash(3, -100, "df"), "df", "3,-100,'df'"]
//    , [ hash("test", -100, "df"), "df", "'test',-100,'df'"]
//    , [ hash(false, 5, "df"), "df", "false,5,'df'"]
//    , [ hash(0, 5, "df"), "df", "0,5,'df'" ]
//    , [ hash(undef, 5, "df"), "df", "undef,5,'df'" ]
//    , "  "
    ], mode=mode //,["h",h]
    );
    echo( rtn );
}    
//hash_test();  