include <scadx.scad>

/*

	scadx_Make2.scad
	
	-- Based on scadx_Make.scad 2018.1.17
	-- Rename all def to opt
	-- Rename ops to actions



*/


//include <../doctest/doctest.scad> 
 
/*
  scadx_make.scad
  
  Building OpenSCAD objects in an OOP (Object-Oriented Programming) way.
  
  * An object is represented by a data structure <obj>, which is a hash 
    containing even number of items, representing a set of key:val pairs.
  
    <obj>: [ "units", <shape> || [<shape>,...] ||  <part> || [<part>,...]  
           , <mod>
           , "copies", [<mod>,...]
           , "cutoffs", [<obj>,...]
           ]
         
    <shape>: [ "shape", "cube"|"cylinder"|"Cube"|"Cylinder"|"sphere"|"poly"
             , "opt", <param>
             , <mod> 
             , "copies", [<mod>,...]
             , "cutoffs", [<obj>,...]
    	     ]
    	     
    	     <param>: type hash, parameters for building shape
    	     
    <part> : [ "part", <partname>
             , <mod> 
             , "copies", [<mod>,...]
             , "cutoffs", [<obj>,...]
    	     ]  	     
    	     
    	     <partname>: string as name of part template

    <mod> : a optional modifier, an embedded hash containing key:val to modify 
            the properties of its parent.
            
            , "color", <color>
            , "actions", <actions>
            , "islogging", 0|1
            , "visible", 0|1
            
            <color>: type string ( "red" ) or array ( ["red", 0.5] )
                
            <actions>: type hash for operations:
            
                  [ "t"|"transl", [x,y,z]
                  , "x"|"y"|"z", n
                  , "rot", [x,y,z]
                  , "rotx"|"roty"|"rotz", n
                  , "mirror", pt|line|plane
                  ]
       
*/

/*

   possible markPts value:
 
      "g" 
      "l;cl=green" 
      "b=false;g;l;cl=blue"
       ["cl=green", "grid",true]
      "cl=blue;g=[color,gold,r,0.05]" );
      "g=[color,blue]"
      "cl=red;l=test" );
      "cl=green;l=[isxyz,true]" );
      ["cl=blue;r=0.2"
      ,"label",["scale=0.015"
               ,"isxyz",true
               ,"tmpl","{*`a|d3|wr=,in}"
               ]
      ]
      ["cl=purple;ball=false"
      ,"label",["scale=0.05"
               ,"isxyz",true
               ,"tmpl","{*`a|d3|wr=,in}"
               ]
      ]
      
    MarkPts( trslN(pqr,10), "ch;cl=teal;rng=1;l=AB"); 
    MarkPts( trslN(pqr,12), "ch;cl=purple;rng=[0,-1];l=BC"); 
    MarkPts( trslN(pqr,14), "ch;cl=black;rng=1;l=[isxyz,true]"); 
  "l;ch;echopts"
  ["samesize",true]
  "samesize,label||color=red"
  "g;c;ch")
  
 */
  


    //LOG_PARAMS= ["LOG_LEVEL",10]; 
    //
    //LEVEL_ASSEM= 1;
    //LEVEL_COPY= 2;
    //LEVEL_PART= 3;
    //LEVEL_CUTOFF= 4;
    //LEVEL_MARKPT= 5;
    //LEVEL_DIM = 5; 
    
   //RESERVED_SHAPE_NAMES=["cube","sphere","cylinder","poly"];

   ISLOGGING= true;
   LOGLEVEL = 0;
   
  
//module Draw_a_bare_shape( shtype, shname, opt, pts=[], mod=[], loglevel=LOGLEVEL, loglayer=4 )
//{
//  /* A bare shape doesn't take into account of "copies" and "cutoffs"
//  
//     <shname>  : cube|sphere|cylinder|poly
//     <opt> : list of parameters
//     <pts>     : optional
//     <mod>     : [ "actions", [...]
//                 , "color", 
//                 ]
//  */
//  module _log(s,mode){ log_(s, loglayer, mode);} 
//  module _log_b(s,mode){ log_b(s, loglayer, mode);} 
//  module _log_e(s,mode){ log_e(s, loglayer, mode);} 
//  
//  islogging = hash(mod, "islogging",0) || loglayer <= loglevel ;
//   
//  //echo(shape_islogging = hash(mod, "islogging")
//      //, loglayer=loglayer, LOGLEVEL=LOGLEVEL, islogging=islogging ); 
//  //pts = pts? pts: hash( opt, "pts");    
//  if(islogging) 
//  {
//  _log_b(_s("Draw shape {_}:<b>{_}</b> with Draw_a_bare_shape()"
//            , [shtype, _Make_get_name_and_colorMark(shname, color[0])]
//            ));
//   
//   _log(["shtype", shtype, "shname", shname, "opt", opt, "pts", pts] );
//   _log(["added mod", mod]);
//  }
//   
//   actions = hash(mod, "actions",[]);
//   newpts = transPts( pts, actions );
//   _color = hash( mod, "color", ["gold",1] );
//   color = isstr(_color)?[_color,1]:_color;
//   
//   //visible = hash( mod, "visible", 1);
//    
//   //if(visible)
//   //{
//     color( color[0], color[1] )
//     if(shtype=="poly") 
//     {
//        faces = hash( opt, "faces", faces(4));
//      
//        if( islogging )
//        {
//        _log( "<u>Drawing a <b>\"poly\"</b></u> using polyhedron()", loglayer );
//        _log( ["pts", pts ], loglayer);
//        _log( ["newpts", newpts ], loglayer);
//        _log( ["faces", faces ], loglayer );
//        } 
//        
//        polyhedron( pts, faces ) ; 
//     }
//     else
//     { 
//             
//       if( islogging )
//       {
//       _log( _s("<u>Drawing <b>{_}</b>(a \"{_}\") using Primitive()</u>"
//               , [shname,shtype]));
//       _log( ["opt: ", opt] );
//       
//       }
//             
//       Transforms( actions )
//       Primitive( shtype, opt ); 
//     }
//   //}  
//   //else{
//   //
//    //_log("shape.visible is false, skip making"); 	
//   //}  
//   
//   if(islogging) _log_e("Leaving Draw_a_bare_shape()");
//
//}

//module Draw_a_bare_shape( shtype, shname, opt, pts=[], mod=[], loglevel=LOGLEVEL, loglayer=4 )
module Draw_a_bare_shape( shape, mod=[], loglevel=LOGLEVEL, loglayer=4 )
{
  /* A bare shape doesn't take into account of "copies" and "cutoffs"
  
     <shape>: [ "name", <str>
              , "shape", cube|sphere|cylinder|poly
              , "opt", ["pts", <array> (optional)
                       ]
              ]

     <mod>  :                          modifier
              [ "actions", [...]
              , "color", <str|array>
              ]
              
  */
  //echo("Draw_a_bare_shape: ", shape=shape);
  
  module _log(s,mode){ log_(s, loglayer, mode);} 
  module _log_b(s,mode){ log_b(s, loglayer, mode);} 
  module _log_e(s,mode){ log_e(s, loglayer, mode);} 
  
  islogging = hash(mod, "islogging",0) || loglayer <= loglevel ;
  
  shname = hash( shape, "name", "");
  shtype = hash( shape, "shape", "Cube" );  
  opt = hash( shape, "opt" );
   
  //echo(shape_islogging = hash(mod, "islogging")
      //, loglayer=loglayer, LOGLEVEL=LOGLEVEL, islogging=islogging ); 
  //pts = pts? pts: hash( opt, "pts");    
  if(islogging) 
  {
  _log_b(_s("Draw shape {_}:<b>{_}</b> with Draw_a_bare_shape()"
            , [shtype, _Make_get_name_and_colorMark(shname, color[0])]
            ));
   
   _log(["shtype", shtype, "shname", shname, "opt", opt] );
   _log(["added mod", mod]);
  }
   
   actions = hash(mod, "actions",[]);
   _color = hash( mod, "color", ["gold",1] );
   color = isstr(_color)?[_color,1]:_color;
   
   _pts = hash( opt, "pts", _Make_get_shape_pts(shape) );
   pts = transPts( _pts, actions );
   
     if(shtype=="poly") 
     {
        faces = hash( opt, "faces", rodfaces(4));
        if( islogging )
        {
        _log( "<u>Drawing a <b>\"poly\"</b></u> using polyhedron()", loglayer );
        _log( ["pts", pts ], loglayer);
        _log( ["newpts", newpts ], loglayer);
        _log( ["faces", faces ], loglayer );
        } 
        
        polyhedron( pts, faces ) ; 
     }
     else
     { 
             
       if( islogging )
       {
       _log( _s("<u>Drawing <b>{_}</b>(a \"{_}\") using Primitive()</u>"
               , [shname,shtype]));
       _log( ["opt: ", opt] );
       
       }  
       
       frameOpt = update( hash(opt, "Frame", []), ["r",0.03] );
       _opt = update(opt, ["Frame",frameOpt]);
       
       // Incorporate color into opt so it won't overwrite 
       // the Frame color (Cube or Cylinder can take care of color))
       if(shtype=="Cube"||shtype=="Cylinder")
       {
         
         Transforms( actions )
         Primitive( shtype, update(_opt, ["color", color] )); 
   	 
       } else {
       
         Color(color)
         Transforms( actions )
         Primitive( shtype, opt ); 
   	 if(pts) Frame(pts, opt=frameOpt);  
       
       }
       
     }

   if(islogging) _log_e("Leaving Draw_a_bare_shape()");

}
     
module Draw_a_shape( objname, shape, mod=[], partlib = []
                   , loglevel=LOGLEVEL, loglayer=3)
{
  /* <shape>:
  
   [ "shape", <shName>
   , "opt", [...]
   , "actions", [...]
   , "copies", [...]
   , "cutoffs", <obj>
   ]   
  */
  module _log(s,mode){ log_(s, loglayer, mode);} 
  module _log_b(s,mode){ log_b(s, loglayer, mode);} 
  module _log_e(s,mode){ log_e(s, loglayer, mode);} 
  
  islogging = hash(shape, "islogging",0) || loglayer <= loglevel ;
  
  //echo(shape_islogging = hash(shape, "islogging")
      //, loglayer=loglayer, LOGLEVEL=LOGLEVEL, islogging=islogging ); 
      
  if(islogging)
  {
   _log_b(_s("Draw shape <b>{_}</b> with Draw_a_shape(), opt={_}"
           ,[_Make_get_name_and_colorMark(objname
                     , hash(mod,"color", hash(shmod, "color", "gold")))
            ,opt]));

  _log(["shape",shape], mode=0);
  _log(["added mod to this shape", mod], mode=0);
  }
  
  //echo(loglevel=loglevel, loglayer=loglayer, islogging=islogging);
  
  shmod  = delkeys(shape,["shape","parts","copies", "opt"]);
  copies = hash( shape, "copies",[[]] );
  _cutoffs= hash( shape, "cutoffs");
    
    
  //_shname = hash( shape, "shape", "cube" );  
  //shname2 = haskey(shape, "name")? str(_shname,"/",hash(shape, "name")): _shname;
  shname = hash(shape, "name", "");
  shtype = hash( shape, "shape", "Cube" );  
  
  // 2017.9.17: while implenting "poly", we need to send pts and faces
  // to Draw_a_bare_shape(). But current design of Draw_a_bare_shape()
  // doesn't take <shape> as an argument (pts and faces are defined in 
  // <shape>). As a temporary measure, we transfer faces to <opt> 
  opt= hash( shape, "opt" );
          
  shpts  = hash( shape, "pts", _Make_get_shape_pts( shape ) );
  shapevisible = hash( shape, "visible", 1);
  
  if(islogging) _log(_s("Loop thru [{_}] copies of shape", len(copies)));  
  
      for( ci = [0:len(copies)-1] )
      {
        difference()
        {
          newmod = _Make_update_parts( [shmod, copies[ci], mod] );
          
          if(islogging)
          { _log(_s("===shape #{_}<b>{_}</b>===",[ci,shname]));
            _log(["newmod", newmod],mode=0);
          } 
          if( hash(newmod,"visible",1) )
          {
            Draw_a_bare_shape( shape, newmod 
                             , loglevel=loglevel, loglayer = loglayer+1
                         );
            //Draw_a_bare_shape( shtype, shname, opt, shpts, newmod 
                             //, loglevel=loglevel, loglayer = loglayer+1
                         //);
                         
          } else {
            if(islogging) 
            _log( _color("==> This copy's visible=0. Skip drawing", "orange"));
          }   
        
        
        if(_cutoffs)
          {
            if(islogging)_log("Drawing cutoffs:");
            cutoffs = _cutoffs[0]=="units"?[_cutoffs]:_cutoffs;
            for( coi = [0:len(cutoffs)-1] )
            {
              _cutoff = cutoffs[coi];
              //newmod2 = delkeys(_Make_update_parts( [shmod, copies[ci], mod] ), ["shape","cutoffs"]);
      	
            	// We want a cutoff's visibility=1 when either (1) shape.visible=1 
            	// (so it will be a cutoff as we want) or (2) shape.visible=0 AND 
            	// cutoff.visible=1 (means, we want to see how a cutoff is displaced
            	// so in this case a cutoff is drawn without its parent shape)
            	cutoffvisible = shapevisible?1:
            	                 hash( _cutoff, "visible", 0);
            	if(islogging)
            	{
            	_log( ["_cutoff", _cutoff, "visible", hash(_cutoff, "visible") ]);
            	_log(["shapevisible", shapevisible, "cutoffvisible", cutoffvisible]);
      	        _log(["newmod", newmod]);
                }
            	cutoff = update(_cutoff, ["visible", cutoffvisible, "color", "gray"]);                  
            	//newmod = update( _newmod, ["visible", cutoffvisible]);
              
              Draw_copies_of_an_obj( objname = str( objname, ".cutoff*", coi)
            	           , obj = cutoff
            	           , mod = delkey(newmod , "visible")
            	           , partlib= partlib
            	           , loglevel=loglevel
            	           , loglayer = loglayer+1
                         );
            }              
          }
        
        }  
                    
      } 
  if(islogging) _log_e( _s("Leaving Draw_a_shape( {_} )", objname));

} 

module Draw_a_part( objname, part, fullpartname, mod=[], partlib = []
                   , loglevel=LOGLEVEL, loglayer=3)
{
  /* <part>: 
  
    [ "part", <partname> ]
    
    [ "part", <partname>
     , "actions", [...]
     , "copies", [...]
     , "cutoffs", <obj>
     ]
     
   Each part is defined in partlib as:
   
   "partlib", [ <partname1>, <obj1>
              , <partname2>, <obj2>
              , ... 
              ]      
  */  
  
  module _log(s,mode){ log_(s, loglayer, mode);} 
  module _log_b(s,mode){ log_b(s, loglayer, mode);} 
  module _log_e(s,mode){ log_e(s, loglayer, mode);} 
  
  islogging = hash(part, "islogging",0) || loglayer <= loglevel ;
  partmod  = delkeys(part,["shape","parts","copies", "opt"]);
  
  //echo(shape_islogging = hash(shape, "islogging")
      //, loglayer=loglayer, LOGLEVEL=LOGLEVEL, islogging=islogging ); 
  partname = hash(part, "part");
  coloredPartName= _Make_get_name_and_colorMark(fullpartname
                     , hash(mod,"color", hash(partmod, "color", "gold")));     
  if(islogging)
  {
   _log_b(_s("Draw <b>{_}</b> with Draw_a_part()"
           ,coloredPartName 
           ));

  _log(["partlib",partlib]);
  _log(["part",part]);
  _log(["added mod to this part", mod]);
  }
  
  copies = hash( part, "copies",[[]] );
  _cutoffs= hash( part, "cutoffs");
    
  partvisible = hash( part, "visible", 1);
  
  if(!haskey(partlib, partname))
  {
    echo(_red(
    _s("Error! Requested part named \"<b>{_}</b>\" of obj \"<b>{_}</b>\" is not found in *partlib*", [partname, objname])
    ));
  }
  else
  {
    // the obj represented by partname
    partobj = hash( partlib, partname );
  
    if(islogging) _log(_s("Loop thru {_} copies of <b>{_}</b>"
                        , [len(copies), coloredPartName] ));  
  
        for( ci = [0:len(copies)-1] )
        {
          difference()
          {
            newmod = _Make_update_parts( [partmod, copies[ci], mod] );
          
            if(islogging)
            { _log(_s("===<b>{_}.copy#{_}</b>===",[fullpartname,ci]));
              _log(["newmod", newmod],mode=0);
            } 
            if( hash(newmod,"visible",1) )
            {
          
              Draw_copies_of_an_obj( fullpartname, //str(objname,".",partname)
                                   , partobj, mod=newmod, partlib=partlib
                              , loglevel=loglevel, loglayer=loglayer+1);
                            
            } else {
              if(islogging) 
              _log( _color("==> This copy's visible=0. Skip drawing", "orange"));
            }   
        
        
          if(_cutoffs)
            {
              if(islogging)
              {
                _log("Drawing cutoffs:");
                _log(["newmod", newmod]);
              }
              cutoffs = _cutoffs[0]=="units"?[_cutoffs]:_cutoffs;
              shapevisible = hash( part, "visible", 1);
              
              for( coi = [0:len(cutoffs)-1] )
              {
                _cutoff = cutoffs[coi];
              
                //newmod2 = delkeys(_Make_update_parts( [shmod, copies[ci], mod] ), ["shape","cutoffs"]);
      	
              	// We want a cutoff's visibility=1 when either (1) shape.visible=1 
              	// (so it will be a cutoff as we want) or (2) shape.visible=0 AND 
              	// cutoff.visible=1 (means, we want to see how a cutoff is displaced
              	// so in this case a cutoff is drawn without its parent shape)
              	cutoffvisible = shapevisible?1:
              	                 hash( _cutoff, "visible", 0);
              	if(islogging)
              	{
              	_log( ["_cutoff", _cutoff, "visible", hash(_cutoff, "visible") ]);
              	_log(["shapevisible", shapevisible, "cutoffvisible", cutoffvisible]);
        	        _log(["newmod", newmod]);
                  }
              	cutoff = update(_cutoff, ["visible", cutoffvisible, "color", "gray"]);                  
              	//newmod = update( _newmod, ["visible", cutoffvisible]);
              
                Draw_copies_of_an_obj( objname = str( objname, " cutoff[", coi, "]")
              	           , obj = cutoff
              	           , mod = delkey(newmod , "visible")
              	           , partlib= partlib
              	           , loglevel=loglevel
              	           , loglayer = loglayer+1
                           );
              }              
            }
        
          }  
                    
        } 
  }      
  if(islogging) _log_e( _s("Leaving Draw_a_part( obj:{_}, part:{_} )", [objname, part[1]]));

} 

module Draw_units( objname, units, mod=[], partlib = []
                  , loglevel=LOGLEVEL, loglayer=2)
{

  /* <units>: a list of units 
  
   [ [ "shape", <shName>
     , "opt", [...]
     , "actions", [...]
     , "copies", [...]
     , "cutoffs", <obj>
     ]
   , [ "part", <partname> ]
   , [ "part", <partname>
     , "actions", [...]
     , "copies", [...]
     , "cutoffs", <obj>
     ]
   , 
   ] 
   
   Each part is defined in partlib as:
   
   "partlib", [ <partname1>, <obj1>
              , <partname2>, <obj2>
              , ... 
              ] 
   
  */
  module _log(s,mode,add_ind){ log_(s, loglayer, mode,add_ind=add_ind);} 
  module _log_b(s,mode,add_ind){ log_b(s, loglayer, mode,add_ind=add_ind);} 
  module _log_e(s,mode,add_ind){ log_e(s, loglayer, mode,add_ind=add_ind);} 
  
  islogging = hash(mod, "islogging",0) || loglayer <= loglevel ;
  
  units= units[0]=="part"
        || units[0]=="shape" ?[units]:units;
  
  if(islogging)
  {
    _log_b(_s("Draw <b>{_}.units:{_}</b> using <b>Draw_units()</b>: "
             ,[ objname, [for(p=units)p[1]]]));
    _log(["units", units]);
    _log(["objMod to be added to units", mod]);
  }
 
  for( ui = [0:len(units)-1] )
  {
    unit = units[ui];
    unitname = hash( unit, "part", hash(unit, "shape"));
    fullunitname= str(objname,".unit*", ui, ":", unitname);
    if(islogging)
    //_log(_s("Looping thru {_} <b>{_}.parts</b>: <b>{_}.parts[{_}]:{_}</b>:"
    //     , [len(parts),objname, objname, pi,partname] ));
    _log(str("Ready to draw <b>", fullunitname,"</b>"));
    if( haskey(unit, "part"))
    Draw_a_part( objname, unit, 
                , fullunitname
                , mod
                , loglevel=loglevel
                , partlib=partlib
                , loglevel=loglevel, loglayer = loglayer+1);
    if( haskey(unit, "shape"))
        Draw_a_shape( fullunitname, unit, mod
                , loglevel=loglevel, loglayer = loglayer+1);
  }
  
  if(islogging)
  _log_e(_s("Leaving Draw_units(), {_}.{_}"
           ,[objname, [for(p=units) p[1]]]));
}

module Draw_copies_of_an_obj( objname, obj, mod=[], partlib=[]
                            , loglevel=LOGLEVEL, loglayer=1)
{
  /*
  <shName>: cube, sphere, cylinder, poly 
  
  <obj>, [ "shapes", [ [ "shape", <shName>
                       , "opt", "actions", [...]
                       , "copies", [...]
                       , "cutoffs", <obj>
                       ]
                     , [ "shape", <shName>
                       , "opt", "actions", [...]
                       , "copies", [...]
                       , "cutoffs", <obj>
                       ]
                     ]   
          , "parts", [ [ "part", <partName>
                       ]
                     , [ "part", <partName>
                       , "actions", [...]
                       , "copies", [...]
                       , "cutoffs", <obj>
                       ]
                     ]                                  
          , "actions", []
          , "parts" , [ ]
          , "copies", [ [], [] ]
          , "color", ""
          , "cutoffs", <obj>
          ]
  */

  module _log(s,mode,add_ind){ log_(s, loglayer, mode,add_ind=add_ind);} 
  module _log_b(s,mode,add_ind){ log_b(s, loglayer, mode,add_ind=add_ind);} 
  module _log_e(s,mode,add_ind){ log_e(s, loglayer, mode,add_ind=add_ind);} 
  
  islogging = hash(obj, "islogging",0) || loglayer <= loglevel ;
  
  if(islogging)
  {
    _log_b(_s("Draw <b>{_}</b> with Draw_copies_of_an_obj()"
             , _Make_get_name_and_colorMark(objname, hash(obj,"color","gold"))));
    _log(["obj", obj]);         
    _log(["external mod to this obj", mod]);
  }
  //shapes = hash( obj, "shapes",[]);
  //parts = hash( obj, "parts",[]);
  units = hash( obj, "units",[]);
  
  if(!units)
  {
    _log(_red(_s("obj <b>{_}</b>: {_}", [objname, obj]) )); 
    _log(_red(
        _s("Error!! obj <b>{_}</b> doesn't have <b>units</b>"
        , objname)));
  }
  else
  {
    
    objMod = delkeys(obj,["units","copies"]);
    
    copies = hash( obj, "copies",[[]] );
    
    if(islogging){
     _log( ["objMod (internal mod)", objMod] );
     _log(_s("Looping through {_} copies of {_}",[len(copies), objname]));
    }
    
    difference()
    {
      for( ci = [0:len(copies)-1] )
      {
        newmod = _Make_update_parts( [objMod, copies[ci], mod] );
        fullobjname = str(objname,".copy#",ci);
        coloredname = _Make_get_name_and_colorMark( 
                                      _b(fullobjname)
                                         , hash(newmod,"color","gold")
                                         );
                  
        if(islogging)
        {
         _log_b( str("Drawing ", coloredname),add_ind=1);
          //_log( ["mod", mod], add_ind=1 );
          _log( ["newmod", newmod], add_ind=1 );
        }
        if( hash( newmod, "visible", 1))
        {
          Draw_units( fullobjname
                   , isarr(units[0])? units:[units]  //<== issue#3
                   , newmod
                   , partlib = partlib
                   , loglevel= loglevel
                   , loglayer= loglayer+1
                   ) ;
          //if(parts)         
          //Draw_parts( fullobjname
                   //, parts
                   //, newmod
                   //, partlib = partlib
                   //, loglevel= loglevel
                   //, loglayer= loglayer+1
                   //) ;
                            
          //if(islogging)
          //_log_e(_s("Done drawing <b>{_}</b>'s copies[{_}]",[objname,ci]),add_ind=1);
        
        } else {
          if(islogging)
          {
           _log( _color("==> This copy's visible=0. Skip drawing", "orange"),add_ind=1);
           //_log_e(_s("Leaving Draw_copies_of_an_obj(objname=<b>\"{_}\"</b>)",objname,add_ind=1));
          }
        }
        
        if(islogging)
        _log_e(str( "Done drawing ", coloredname),add_ind=1);
      }    
                   
      _cutoffs = hash(obj, "cutoffs");
      cutoffs = _cutoffs[0]=="units"?[_cutoffs]:_cutoffs;
      
      if(cutoffs)
      { 
        if(islogging)
        _log(_s("Looping throu {_}.cutoffs: {_}", [objname, cutoffs]));
        
        for( coi = [0:len(cutoffs)-1] )
        { 
      	Draw_copies_of_an_obj( objname = str( objname, ".cutoff*", coi)
      	           , obj = cutoffs[ coi ]
      	           , mod = update(_Make_update_part(objMod,mod), ["color","gray"])  
      	           , partlib = partlib
      	           , loglevel = loglevel
      	           , loglayer = loglayer+1
                   );
        }           
      }             
    }
  }
  if(islogging)
  _log_e(_s("Leaving Draw_copies_of_an_obj(<b>\"{_}\"</b>)",objname,add_ind=1));

}
  
//. Make   
module Make ( data
            , title= undef
            , loglevel = LOGLEVEL
            , _partlib = []
            , _assems  = []
            ) 
{
  /*
  
  <obj>, [ "shapes", [ <shName>, ["opt", ...]
                                  , <shName>, ["opt", ...]
                                  ]
          , "parts" , [ ]
          , "copies", [ [], [] ]
          , "color", ""
          , "actions", []
          , "cutoffs", <obj>
          ]
          
                      
    data: [
            "partlib", []
          , "objs"
            , [
                "obj1", [ "shapes", [ <shName>, ["opt", ...]
                                    , <shName>, ["opt", ...]
                                    ]
                        , "parts" , [ ]
                        , "copies", [ [], [] ]
                        , "color", ""
                        , "actions", []
                        , "cutoffs", []
                        ]
              ]  ...
              
          ] //_data    
  */
  if(loglevel)
  echo( _mono(_b("--------------------------- Start Make() ---------------------------" )));

  title = und(_span( _b( hash(data, "title"), "font-size:20px")) );
  _note = hash(data, "note");
  echo(title);  
       
  //if(loglevel)
  //{
    if(_note)
    {
      note= isarr(_note)? join(_note, str(_mono("<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"), "// "))
                       : _note;  
      echo( _green(str("// ",note)));
    }    
  //}
  	
  objs = hash(data, "objs", []);
  partlib = hash( data, "partlib",[]);
  
  //objs = haskey(_objs,"copies")?_objs:update(_objs, ["copies",[[]]]);
  if(loglevel)
  {
    log_main( ["partlib", partlib] );
    log_main( ["partlib.keys:", keys(partlib) ] );
    log_main( ["data", data] );
    log_main( ["objs.keys:", keys(objs) ] );
    log_main( ["objs", objs] );
    
  }
  if(objs)
  {
    for( i=[1:2:len(objs)-1] )
    {
       //echo(i=i);
       objname = objs[i-1];
       obj = objs[i];
       Draw_copies_of_an_obj(objname, obj, mod=[]
                           , partlib = partlib, loglevel=loglevel);
    }
  }
  else { echo(_red("Error! no *objs* in data. Nothing to make")); }

  if(loglevel)
  {
    echo( und(_span( str("Leaving ", _b( hash(data, "title"))), "font-size:20px")) );      
    echo( _mono(_b("--------------------------- Done Make() ---------------------------" )));
  }
} //__Make
    
        
//------------------------------------------------------------

//. == Make tools ===============  

module log_main(s,mode,add_ind=0,debug=0) 
                  { log_(s, 0, mode, add_ind, debug=debug);}
                  
                  
//function _Make_getCubePts(xyz)=
//   (
//     //[ for (p= cubePts( [[xyz.x,0,0], ORIGIN, [0,xyz.y,0]], h=xyz.z ) )
//       //p+[0,0,xyz.z]
//     //]
//     cubePts(xyz)
//   );
                     
function _Make_get_shape_pts( shape )=
   (
     // Given a shape, resolve the source of pts (either pts or size)
     // NOTE: currently this applies to only shape = cube or poly 
     
     //  cube or poly: Return pts or undef
     //  sphere or cylinder: return undef
     
     /*
     A shape could be:
     
     	 [ "shape", "cube"   <== cube | cylinder | sphere
         , "opt", ["size",[10,1,4]]
         , <mod>
         ]
         
     or just :
     
         [ "shape", "poly"
         , "pts", [ ... ]
         , <mod> 
         ] 
        
     */    
     let( shapename = hash( shape, "shape")
        ,  _pts = hash( shape, "pts")  // if "pts" is given , use it. Otherwise, 
                                         // get it from the size
        , size = hashs( shape, ["opt","size"])
        , no_pts =  !_pts && !size 
        )
     no_pts? undef
     :  _pts?_pts: (shapename=="cube"||shapename=="Cube"? cubePts(size) //_Make_getCubePts( size)
                                     :undef)
     );    

function _Make_update_part( part1, part2 )=
(
  let( // If part1 have opt ("shape","opt" and "pts"), we keep them; 
       // This prevents the opt section of a part from being overwritten
       // by later parts. 
       // If not exist, we get them from part2 and make sure they are placed
       // in the beginning of a part. 
       shape= hash(part1, "shape", hash(part2,"shape")) 
     , opt= hash(part1, "opt", hash(part2,"opt")) 
     , pts= hash(part1, "pts", hash(part2,"pts")) 
     , core1= hashex( part1, ["shape","opt","pts"])
     , core2= hashex( part2, ["shape","opt","pts"])
     //, core1= [ shape?["shape", shape]:[]  
              //, opt?["opt", opt]:[] 
              //, pts?["pts",pts]:[] 
              //]
     //, opt = core1? [for( x= core1 ) each x]:[]
     , _def = core1 ? core1: core2
     , part1 = concat( _def, delkeys(part1, ["shape","opt","pts"] ) )
     
     // Turn part2 into a mod (modifier). Items in a mod are updated 
     // differently
     // color: the last value
     // visible: both true
     // debug: any is true
     // markPts: any is true
     // dims: any is true
     // cutoffs: concat (w/ special treatment)
     // actions: concat 
     , mod2 = 
  [ haskey(part2,"color")?["color", vals_of_key([part2,part1], "color")[0]]: []
  , haskey(part2,"visible")?["visible", all_in_hashs( [part2,part1], "visible")]:[]
  , haskey(part2,"islogging")?["islogging"  , any_in_hashs( [part2,part1], "islogging")]:[]
  , haskey(part2,"markPts")?let( _m1= hash(part1, "markPts")
                               , _m2= hash(part2, "markPts")
                               , s1= isstr(_m1)?_m1:len(_m1)%2?_m1[0]:""
                               , s2= isstr(_m2)?_m2:len(_m2)%2?_m2[0]:""
                               , m1= isstr(_m1)?[]:len(_m1)%2?slice(_m1,1):_m1
                               , m2= isstr(_m2)?[]:len(_m2)%2?slice(_m2,1):_m2
                               )
                            concat( [join([s1,s2],";")], m1,m2)   
                            :[]
  , haskey(part2,"dims")?["dims"   , hash(part2,"dims")]:[]
  
  // cutoffs could be: (1) ["shape",...] or (2) [["shape",...]]. We want to 
  // make sure (1) is [["shape",...]] before a concat is made 
  , haskey(part2,"cutoffs")?let( c1=hash( part1,"cutoffs",[])
                               , c2=hash( part2,"cutoffs",[])
                               , cos= ["cutoffs"
                                      , concat( isstr(c1)||c1[0]=="shape"?[c1]:c1
                                              , isstr(c2)||c2[0]=="shape"?[c2]:c2 
                                              )
                                      ]
                               )
                              cos 
                            : [] 
                            
  , haskey(part2,"actions")?["actions", concat( hash( part1,"actions",[])
                        , hash( part2,"actions",[]) )]:[]
  ]
  )
  update( part1, [for(x=mod2) each x] )  
);

    _Make_update_part=[ "_Make_update_part", "part1,part2"
     , "array", "Make" 
     ,"Update part1 with part2. A *part* is a hash having two sections:
     ;; &lt;opt> and &lt;mod> (=modifier) 
     ;; 
     ;; &lt;opt> defines basic obj parameters ('shape','opt','pts')
     ;; &lt;mod> defines modifications ('color', 'actions', 'visible', ...)
     ;; 
     ;; At least one of part1, part2 must have opt.
     ;;
     ;; Items in a mod are not updated in the same way:
     ;; 
     ;; // color: the last value
     ;; // visible: both true
     ;; // debug: any is true
     ;; // markPts: any is true
     ;; // dims: any is true
     ;; // cutoffs: concat
     ;; // actions: concat
     "
     ,"_Make_update_parts"
    ];

    function _Make_update_part_test( mode=MODE, opt=[] )=
    (
      let( c1= ["shape","cube","color","gold" ]   
         , c2= ["color", "blue"]
         , c3= ["shape","poly","color", "red"]
         
         , v1= ["shape","cube","visible",1 ]   
         , v2= ["visible", 1]
         , v3= ["visible", 0]
         
         , d1= ["shape","cube","debug",0 ]   
         , d2= ["debug", 1]
         , d3= ["debug", 0]
         
         , a1= ["shape","cube","actions", ["rot",[90,0,0] ] ]   
         , a2= ["actions", ["transl",[1,2,3] ]]
         
         )
      doctest( _Make_update_part,
      [
//       _h3("color") 
//      ,_h4("color") Regency#77901
//      ,_h5("color") 
//      ,_h6("color") 
//      ,
       _span(_u("color"), "font-size:14px")
       ,"" 
      , "var c1", "var c2", "var c3", ""
      , [_Make_update_part(c1,c2),["shape", "cube", "color", "blue"], "c1,c2"]
      
      , ""
      , "// Note the shape is taken from c1 and placed in the beginning of c2:"
      , [_Make_update_part(c2,c1),["shape", "cube", "color", "gold"], "c2,c1"]
      , ""
      , "// Note that <b>shape</b> is NOT replaced by new value:"
      , [_Make_update_part(c1,c3),["shape", "cube", "color", "red"], "c1,c3"]
        
      ,"" 
      ,_span(_u("visible"), "font-size:14px")
      ,"" 
      , "var v1", "var v2", "var v3", ""
      , [_Make_update_part(v1,v2),["shape", "cube", "visible", 1], "v1,v2"]
      , [_Make_update_part(v1,v3),["shape", "cube", "visible", 0], "v1,v3"]
       
      ,"" 
      ,_span(_u("debug"), "font-size:14px")
      ,"" 
      ,"var d1", "var d2", "var d3", ""
      , [_Make_update_part(d1,d2),["shape", "cube", "debug", 1], "d1,d2"]
      , [_Make_update_part(d1,d3),["shape", "cube", "debug", 0], "d1,d3"]
      
       
      ,"" 
      ,_span(_u("actions"), "font-size:14px")
      ,"" 
      , "var a1", "var a2"
      , [_Make_update_part(a1,a2),["shape", "cube", "actions", ["rot", [90, 0, 0], "transl", [1, 2, 3]]], "a1,a2"]
      , [_Make_update_part(a2,a1),["shape", "cube", "actions", ["transl", [1, 2, 3], "rot", [90, 0, 0]]], "a2,a1"]
      
      ]
      , mode=mode, opt=opt, scope=[ "c1", c1, "c2", c2, "c3", c3
                                  , "v1", v1, "v2", v2, "v3", v3 
                                  , "d1", d1, "d2", d2, "d3", d3 
                                  , "a1", a1, "a2", a2
                                  ]
      )
    );   
    
// -----------------------------------------
function _Make_update_parts( parts, _rtn=[], _i=0 )=
(
   _i==0? _Make_update_parts( parts, _rtn= parts[_i], _i=_i+1)
   :_i>=len( parts )? _rtn
   : _Make_update_parts( parts
                      , _rtn= _Make_update_part(_rtn, parts[_i]), _i=_i+1)
);

     _Make_update_parts=[ "_Make_update_parts", "parts"
     , "array", "Make" 
     ,"Update all parts to parts[0]. See _Make_update_part().
     "
     ,"_Make_update_part"
    ];

    function _Make_update_parts_test( mode=MODE, opt=[] )=
    (
      let( c1= ["shape","cube","color","gold" ]   
         , c2= ["color", "blue"]
         , c3= ["shape","poly","color", "red"]
         
         , v1= ["shape","cube","visible",1 ]   
         , v2= ["visible", 1]
         , v3= ["visible", 0]
         
         , d1= ["shape","cube","debug",0 ]   
         , d2= ["debug", 1]
         , d3= ["debug", 0] 
         
         , a1= ["shape","cube","actions", ["rot",[90,0,0] ] ]   
         , a2= ["actions", ["transl",[1,2,3] ]]
         
         , x1= [ "shape","cube"
               , "color","gold","visible",1,"debug",0,"actions",["rot",[90,0,0]] ]
         , x2= [ "color","gray","visible",1,"debug",0,"actions",["transl",[1,2,3]]]
         , x3= [ "color","blue","visible",1,"debug",0]
         , x4= [ "color","red" ,"visible",0,"debug",1,"actions",["transl",[5,5,5]]]
         )
      doctest( _Make_update_parts,
      [
       _span(_u("color"), "font-size:14px")
       ,"" 
      , "var c1", "var c2", "var c3", ""
      , [ _Make_update_parts([c1,c2,c3])
        , ["shape", "cube", "color", "red"]
        , "[c1,c2,c3]"
        ]
      ,"" 
      , "var v1", "var v2", "var v3", ""
      , [_Make_update_parts([v2,c2,c3])
           , ["shape", "poly", "visible", 1, "color", "red"]
           , "[v2,c2,c3]"
        ]
      ,"" 
      ,"var x1", "var x2", "var x3", "var x4", ""
      , [_Make_update_parts([x1,x2,x3])
           , ["shape", "cube", "color", "blue", "visible", 1, "debug", 0, "actions", ["rot", [90, 0, 0], "transl", [1, 2, 3]]]
           , "[x1,x2,x3]"
        ]
      , [_Make_update_parts([x1,x2,x4])
           , ["shape", "cube", "color", "red", "visible", 0, "debug", 1, "actions", ["rot", [90, 0, 0], "transl", [1, 2, 3], "transl", [5, 5, 5]]]
           , "[x1,x2,x4]"
        ]
         
      ]
      , mode=mode, opt=opt, scope=[ "c1", c1, "c2", c2, "c3", c3
                                  , "v1", v1, "v2", v2, "v3", v3 
                                  , "d1", d1, "d2", d2, "d3", d3 
                                  , "a1", a1, "a2", a2
                                  , "x1", x1, "x2", x2, "x3", x3, "x4", x4
                                  ]
      )
    );   

function _Make_get_name_and_colorMark(name,color)=
(
   let( color=isstr(color)?color:color==undef?"gold":color[0]
      , colorMark = _span( "&nbsp;&nbsp;&nbsp;", s= str("background:",color))
      )
   str( name, " ", colorMark)
   //str( colorMark, " ", name, " ", colorMark)
);
    
    _Make_get_name_and_colorMark=[ "_Make_get_name_and_colorMark"
      , "name,color", "str", "Make" 
    ,"Return a str containing *name* flanked by colored mark."
    ,""
    ];

    function _Make_get_name_and_colorMark_test( mode=MODE, opt=[] )=
    (
      let(rtn= "<span style=\"background:teal\">&nbsp;&nbsp;&nbsp;</span> test <span style=\"background:teal\">&nbsp;&nbsp;&nbsp;</span>"
         )
        doctest( _Make_get_name_and_colorMark,
        [ 
          str("_Make_get_name_and_colorMark(\"test\",\"teal\")= "
             , rtn)
        ]
        , mode=mode, opt=opt 
        )
    );  
    
  function _Make_get_materials(data)=
  (
    // Return a list of materials with size and copy info.
    // Use: 
    //       mat = _Make_get_materials(data); 
    //       for(x=mat)
    //         echo( _mono(_fmth( x )) ); 
    //
    // to print out like:
    //
    // ECHO: "[name="leg_y:leg_y", size=[0.625, 8.5, 4.9483], copies=2]"
    // ECHO: "[name="leg_x:leg_x_top", size=[26, 0.625, 0.625], copies=2]"
    // ECHO: "[name="leg_x:leg_x_guard", size=[26, 0.625, 0.9375], copies=2]"
    // ECHO: "[name="leg_x:leg_x", size=[26, 0.625, 4.25], copies=2]"
    // ECHO: "[name="top:top", size=[30, 11.25, 0.625], copies=1]"

    let( objs = hash(data, "objs")
       , objnames = keys(objs)
       , obj_shape_n_copies = 
         [ for(objname=objnames)
           let( obj = hash(objs, objname)
              , _shapes = hash(obj,"units")
              , shapes = isarr( _shapes[0] )? _shapes:[_shapes ]  
              //, shapes = isarr( _shapes[0] )? _shapes:[_shapes ]  
              , nObjCopies = len(hash(obj, "copies", [0]))
              , shape_n_copies =
                [ for(shape=shapes)
                  let( shname = hash(shape,"name", hash(shape,"shape"))
                     , nShCopies = len(hash(shape, "copies", [0]))
                     , size= hashs(shape, ["opt","size"])
                     )
                  [ "name", str(objname,"/", shname)
                  , "size", reverse(quicksort(size))
                  //, "shapeCopies", nShCopies
                  //, "objCopies", nObjCopies
                 // , "totalCopies", nShCopies * nObjCopies
                  , "copies", nShCopies * nObjCopies
                  ]   
                ]
              )
             each shape_n_copies
         ]
      )              
    obj_shape_n_copies
  ); 
  

