
      
//========================================================
{ // _assert
/*
,"_assert that a value or the type of a var is as expected. If pass, 
;; return true. Else, return an error msg like:
;;
;; [rangeError]:
;; => var i in get() should be  0&lt;=i&lt;=3. Got 4.(var= [10, 11, 12, 13])
;; 
;; [typeError]:
;; => var o in join() should be type arr. Got str.(o= 'abc')
;; 
;; unit : Name of function or module where the target var in
;; var  : Target var nbame
;; val  : The var value
;; check: What to check, like 'type','range', etc. When fails,
;;        the returned str shows (check)Error. For example,
;;        check= 'type' ==> 'typeError'
;; range: For range check only. [i,j] to indicate inclusive val 
;;        range. If i>j, means target value x must be x>=i and 
;;        j>=x. If val is an array, range sets to [0,len(val)-1].
;; want : The expected. Could be a value,type,etc, depending on 'check'
;; rel  : A str to describe the relationship. Usually automatically
;;        set, but can be customized. See CHECK TYPE example below. 
;; got  : If not given, set to val
;; pass : True|False. Automatically decided, but can be customized.
;; showfullval: Default false. The (val=...) in the end of errmsg
;;        could be very long so it is cut to 30 chars. If set this
;;        to true, entire val will be shown.

New 2015.8.6: when *check*=type or gtype, *want* can be array, in such
          cases, *val* will be checked against any item in *want*

Usage:

    In planeAt():
    
        function planeAt( pts ...)=
        ( 
            !ispts(pts)? _assert( unit="planeAt"
                              , var="pts"
                              , val= pts
                              , check= "gtype"
                              , want= "pts"
                              , pass = false
                              )
            :( //code if passed 
               let( ... )
               return
            ) 
        );    
        
        plrtn = planeAt( pts, i=i ); 
        if( isstr(plrtn) && begwith(plrtn, "ERROR:") )
            echo(plrtn); 
        else
            dosomething( plrtn );
        
    Or you can just: 

        plrtn = planeAt( pts, i=i ); 
        dosomething( plrtn );

    and when error, go back to check echo(plrtn)

*/


function _assert( check= "type"
               , unit="func_or_mod"   // unit name
               , var ="var"           // var name
               , val = undef
               , want = ""
               , range= undef
               , rel = ""
               , got = undef
               , pass= undef
               , showfullval=false
               )=
(
    let( val = val==undef?val
                :showfullval?val
                :(len(val)>30? str( slice(val,0, 30),"..."): val)
       , rng = und(range, [0,len(val)-1] )           
       , LE = "&lt;=" 
       , range= isarr(rng)? 
                  ( rng[0]<rng[1]? str( rng[0], LE, var, LE, rng[1])
                    : str( var,LE,rng[1], "||",rng[0],LE,var)
                  ):rng       
               
        , rel = or( rel,
                    hash( [ "type", "should be type"
                     , "gtype", "should be gtype"
                     , "range", "should be "
                     , "len", "should have len"
                     , "value", "should be"
                     ], check )
              )
       , got = und( got,
                    hash( [ "type", type(val)
                     , "gtype", gtype(val)
                     , "range", range( range[0],range[1]-1)
                     , "len", len(val)
                     , "value", val
                     ], check )
              )   
       , pass = und( pass, check=="range"? has(got, val)
                          :check=="type" && isarr(want)? any(want,got)
                          :check=="gtype" && isarr(want)? any(want,got)
                          :got==want
                   )  
       , want = check=="range"? range:want
       )              
    pass? true
    : str("ERROR: ", _red( str( 
           , "<br><b><u>[", check, "Error]:</u></b><br/>"
           , "=> var <b><u>", var, "</u></b> in <b>", unit, "()</b> "
           , rel, " <u>", want, "</u>. "
           , "Got <u>", got, "</u>."       
           , val==undef?""
             :str("(", check=="range"?"var":var, "=<u>", _fmt(val), "</u>)")
           ))
      )// str     
);          

    _assert=["_assert","unit,var,check,[val,want,range,rel,got,pass,showfullval]"
           ,"str|true","Core,Debug"
    ,"_assert that a value or the type of a var is as expected. If pass, 
    ;; return true. Else, return an error msg like:
    ;;
    ;; ERROR:
    ;; [rangeError]:
    ;; => var i in get() should be  0&lt;=i&lt;=3. Got 4.(var= [10, 11, 12, 13])
    ;; 
    ;; ERROR:
    ;; [typeError]:
    ;; => var o in join() should be type arr. Got str.(o= 'abc')
    ;;
    ;; unit : Name of function or module where the target var in
    ;; var  : Target var nbame
    ;; val  : The var value
    ;; check: What to check, like 'type','range', etc. When fails,
    ;;        the returned str shows (check)Error. For example,
    ;;        check= 'type' ==> 'typeError'
    ;; range: For range check only. [i,j] to indicate inclusive val 
    ;;        range. If i>j, means target value x must be x>=i and 
    ;;        j>=x. If val is an array, range sets to [0,len(val)-1].
    ;; want : The expected. Could be a value,type,etc, depending on 'check'
    ;; rel  : A str to describe the relationship. Usually automatically
    ;;        set, but can be customized. See CHECK TYPE example below. 
    ;; got  : If not given, set to val
    ;; pass : True|False. Automatically decided, but can be customized.
    ;; showfullval: Default false. The (val=...) in the end of errmsg
    ;;        could be very long so it is cut to 30 chars. If set this
    ;;        to true, entire val will be shown.
    "
    ,"errmsg"
    ];

    function _assert_test( mode=MODE, opt=[] )=
    (
       let(  
        opt= ["highlight",true, "notest",true, "nl",false]
       )
        
        doctest( _assert,
        [
        
        ""
        , _b("** CHECK VALUE **")
        , ""
         
        , [ //replace( 
            _assert( check="value"
                  , unit="join"
                  , var = "x"
                  , val =  4
                  , want= 5
                  )//, "<", "&lt;")
           ,""
           ,"unit='join',var='x',val=4,check='value',want=5"
           , opt]
        , ""   
        ,   _b("** CHECK RANGE **")
        , ""
        , [ _assert( check = "range"
                  , unit="get"
                  , var="i"
                  , val= [10,11,12,13]
                  , got= 4
                  )
          ,""
          ,"unit='get',var='i',check='range',val=[10,11,12,13],got=4"
          , opt
          ] 
        ,""
        , [ _assert( check = "range"
                  , unit="get"
                  , var="i"
                  , range=[10,2]
                  , got=4
                  )
          , ""
          , "unit='get',var='i',check='range',range=[10,2], got=4"
          ,  opt
          ] 
           
        ,""
        ,  _b("** CHECK TYPE **")
        , ""
        
        , [ _assert( check="type"
                  , unit="join"
                  , var = "o"
                  , val = "abc"
                  , want="arr"
                  )
           , ""
           , "unit='join',var='o',val='abc',check='type',want='arr'"
           , opt
           ]
        ,""      
        , [ _assert( check="type"
                  , unit="join"
                  , var = "o"
                  , val = [1,2,[3,4]]
                  , want= "arr"
                  , rel = "shouldn't have item type"
                  , pass = countArr([1,2,[3,4]])==0
                  )
          , ""
          , "unit='join',var='o',val=[1,2,[3,4]],check='type',rel='shouldn`t have item type',want='arr',pass=countArr(o)==0"
          , opt]   
       
            
        ] 
         ,  mode=mode, opt=opt )
    );    

    //echo( _assert_test(mode=112)[1] );
} // _assert


/*
=================================================================
               Error Handling
=================================================================
*/
{ // errmsg 
    
ERRMSG = [
  "rangeError"
    , "<br/> => Index {iname}(={got}) out of range of {vname} (len={len})"
 ,"typeError"
    , "<br/> => {vname}'s type {rel} {want}{unwant}. A {got} is given (value={val}) "
 ,"arrItemTypeError"
    , "<br/> => Array {vname} {rel} item of type {want}{unwant}. ({vname}: {val})"
,"gTypeError"
    , "<br/> => {vname}'s gtype {rel} {want}{unwant}. A {got} is given (value={val}) "
,"lenError"    
    , "<br/> => {vname}'s len {rel} {want}{unwant}. A {got} is given (value={val}) "
];

function errmsg(errname,data, errmsg=ERRMSG, showfullval=false)=
(
    let ( _val= str(hash(data,"val"))
        , val = showfullval?_val
              :(len(_val)>30? str( slice(_val,0, 30),"..."): _val)
        , df=["want",""
             ,"unwant",""
             ,"rel","should be" ]
//        , df=["fname","?"
//             ,"vname","?"
//             ,"iname","?"
//             ,"rel","should be"
//             ,"want",""
//             ,"unwant",""
//             ,"got",""
//             ,"len",""
//             ,"val",v]
                 
        , _data = update( df, data )
        , 
        , data = [ for(i=range(_data))
                    
                    isint(i/2)? _data[i]
                    : let(k= _data[i-1], v= _data[i])
                      (k=="rel"? v
                      :str("<u>", k=="val"?val:v, "</u>") 
                      )
                 ] 
            
        )
  _red(
         str( 
              "[<b><u>", errname, _h("</u></b>] in {fname}(): ",data)
            , _h( hash(errmsg, errname), data)
            )
      )
);
    errmsg=["errmsg","errname,data,errmsg,showfulval","str","Core,Debug",
      "Return a preformatted error msg. 
      
      " 
    ,""];

    function errmsg_test( mode=MODE, opt=[] )=
    (
        let( err1= [ "fname","get"
                , "iname","i"
                , "vname","arr"
                , "len", 10
                , "got", 11
                ]
           , err2= [ "fname","join"
                , "vname","o"
                , "rel", "should be"
                , "want","arr"
                , "got", "str"
                , "val", "abc"
                ] 
           , err3= [ "fname","capital"
                , "vname","o"
                , "rel", "should NOT be"
                , "unwant","arr"
                , "got", "arr"
                , "val", [1,2,3]
                ]      
           , opt= ["highlight",false,"notest",true, "nl",true]
           )
        
        doctest( errmsg, 
        [
            [ errmsg("rangeError" , err1
                    ) //,"<","&lt;")
            ,"<span style='color:red'>[<u><b>rangeError</b></u>] in <u>get</u>(): <br/> => Index <u>i</u>(=<u>11</u>) out of range of <u>arr</u> (len=<u>10</u>)</span>"
            ,err1
            ,opt
            ]  
            
        ,   [ errmsg("typeError" , err2
                    )// ,"<","&lt;")
            ,"<span style='color:red'>[<u><b>typeError</b></u>] in <u>join</u>(): <br/> => <u>o</u>'s type should be <u>arr</u>. A <u>str</u> is given (value=abc) </span>"
            ,err2
            ,opt
            ]  
            
        ,   [ replace(errmsg("typeError" , err3
                    ) ,"<","&lt;")
            ,"<span style='color:red'>[<b><u>typeError</u></b>] in <u>capital</u>(): <br/> => <u>o</u>'s type should NOT be <u></u><u>arr</u>. A <u>arr</u> is given (value=<u>[1, 2, 3]</u>) </span>"
            ,err3
            ,opt
            ]           
        ]
        , mode=mode, opt=opt )
    );
    //echo( errmsg_test( mode=112)[1] );
    
} // errmsg
        
//=========================================================    
function gTypeErr(data)= errmsg("gTypeError", data);      
//=========================================================    
function typeErr(data)= errmsg("typeError", data);      
//=========================================================    
function rangeErr(data)= errmsg("typeError", data);      
//=========================================================    
function arrItemTypeErr(data)= errmsg("arrItemTypeError", data);      
//=========================================================    
function err( msg, want, got )=
    _red( str( "<b>[ERROR]: </b>"
             , msg
             , "<b>", want
             , "</b>, got: <b>"
             , got
             , "</b>"
             ));
//echo( err( "len of pqr in normal() should >= ", 3,2));
//=========================================================    

/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_error_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_error.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_error_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_error", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      _assert_test( mode, opt )
    //, arrItemTypeErr_test( mode, opt ) // to-be-coded
    //, err_test( mode, opt ) // to-be-coded
    , errmsg_test( mode, opt )
    //, gTypeErr_test( mode, opt ) // to-be-coded
    //, planeAt_test( mode, opt ) // to-be-coded
    //, rangeErr_test( mode, opt ) // to-be-coded
    //, typeErr_test( mode, opt ) // to-be-coded
    ]
);
    
    