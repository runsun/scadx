include <scadx_obj.scad>

//========================================================
Dim=["Dim"
, "site,color,r,spacer,gap,h,pts,followSlope,Label,Guides,GuideP,GuideQ,Arrows,debug,opt"
, "n/a","Obj"
, "Draw a dimention
  ;; 
  ;; site: either pqr (if pts not given) or ijk (if pts given)
  ;;       Or more than 3 pts/indices for chained dim in which all dims facing the same direction 
  ;;       and all have same properties (for multiple dims in which , check out Dims() )
  ;; 
  ;;
  ;; Dim( [P,Q,R] )
  ;; Dim( [i,j,k], pts=pts )
  ;; 
  ;; For chained dims:
  ;; 
  ;; Dim( [P,Q,R,S,T...] )
  ;; Dim( [i,j,k,l,m...], pts=pts )

"
];

DIM_OPT=[]; // Global dim opt. Set this at global level. 2018.5.29.
            // For example, set DIM_OPT = ["r",0.1] would 
            // scale up the entire set of dim for all dims
   

module Dim(   // 2018.5.22 --- re-name pqr to site 
               //               such that it is not limited to 3-item
         site // [P,Q,R], [i,j,k] (when pts given)
              // [P,Q,R,S ...], [i,j,k,...] (when pts given)
        , color 
        , r   // rad of line
        , spacer // dist btw guide and target. Could be negative   
        , gap    // gap between two arrows
                 
        , h      // A ratio for vertical pos of line.  Could be negative
                    /* h ________      0.5         0
                        |   1    |  |_______|  |       |
                        |        |  |       |  |_______|
                        P        Q  P       Q  P       Q
                    */
        , pts // If pts is given, site could be provided as 
              // indices. For examplem, 
              //    site = [0,4,2] 
              //    site = [2,3, [4,5,-2] ]
              // new 2018.5.18
                 
        , followSlope   // Follow the line if it has a slope. See
                        // code for details. Default= true        
        
        , Label=true    // false or hash,  or text
        , Guides  // false or hash
        , GuideP  // false or hash
        , GuideQ  // false or hash
        , Arrows  // false or hash
        , Link  //  new 2018.6.20
                 /*           ___
                        |      |   
                        |      |  stem, default=0  
                    .---+---. ---
                    |       |

                   When Link set to true, auto reset stem to 0.2;
                   When set:
                    -- h default = 1
                    -- gap default= 0
                    -- Guides len cut in half
                    -- it uses Arrows properties. If Arrows false, use Guides
                 */
        , debug= 0 
          
        , opt= []
        )
{ 
  //echom("Dim()");
  opt= update(DIM_OPT, opt);
  //echo(DIM_OPT=DIM_OPT, opt=opt);
  //echo(Label=Label);
  //echo(opt=opt);
  
  r     = arg(r, opt, "r", 0.015);  
    
  _Label= subprop( Label, opt, "Label"
            , dfPropName="text"
            , dfPropType=["str","num", "nums"]
            , dfsub= ["color",color, "size",1, "scale", r/0.015*0.2
                     , "indent",0.1
                     , "followView", false //true
                     , "valign","center" 
                     , "decimal", 2
                     , "maxdecimal", 2
                     , "prefix", ""
                     , "subfix", ""
                     , "chainRatio", false // new 2018.9.26
                                           // Use in chain dim (when len(site)>3):
                                           //
                                           // If false (default):
                                           //        10     4   6
                                           //    |-------|---|----|
                                           // 
                                           // If true, show ratio:
                                           //        0.5  0.2  0.3
                                           //    |-------|---|----|
                     ] 
           );
  if(len(site)<=3)
  {   
    // Make sure it's 3-item array:
    i3   = len(site)>2?site:concat( site, [randPt()]); 
    
    site = pts? [ for(x=i3) len(x)>1?x:pts[x] ]
              : i3;
        
              
    //r     = arg(r, opt, "r", 0.015);  
    color = colorArg(color, opt, "blue");
    
    _Link = undef; // <=============== declare _Link. It's value would be set later
                   //                  but we need the var _Link here now.   
    isLink = !(_Link==false ||_Link==undef );
    
    spacer= arg(spacer, opt, "spacer", 0.1);
    _gap  = arg(gap, opt, "gap", isLink?0:undef);
    h     = arg(h, opt, "h", isLink?1:0.5);    
    followSlope = arg(followSlope,opt, "followSlope", true);
 	/*  
            followSlope= true:    followSlope= false:      
            R decides the plane   R further decides dir
            & side of dim line     of guides 
                /                   
               /'-._              |_________|
              /     '-._  /       |         |
             P'-._      '/        P'-._     |  
             |    '-._  /         |    '-._ |
             |        'Q          |        'Q 
             |         |          |         |
             |         R          |         R
        */    
//    _Label= subprop( Label, opt, "Label"
//            , dfPropName="text"
//            , dfPropType=["str","num", "nums"]
//            , dfsub= ["color",color, "size",1, "scale", r/0.015*0.2
//                     , "indent",0.1
//                     , "followView", false //true
//                     , "valign","center" 
//                     , "decimal", 2
//                     , "maxdecimal", 2
//                     , "prefix", ""
//                     , "subfix", ""
//                     , "chainRatio", false // new 2018.9.26
//                                           // Use in chain dim (when len(site)>3):
//                                           //
//                                           // If false (default):
//                                           //        10     4   6
//                                           //    |-------|---|----|
//                                           // 
//                                           // If true, show ratio:
//                                           //        0.5  0.2  0.3
//                                           //    |-------|---|----|
//                     ] 
//           );
    
    //echo(_Label=_Label);
                           
    // Apply temporary fix described in issue #11
    // https://bitbucket.org/runsun/scadx/issues/11/subprop-of-a-subprop-failed-to-shut-down          
    // Head = subprop( sub= Head==undef? hash(opt, "Head", true) :Head
    //               , ... ); 
    // to allow sub be turned off via opt
     
    Guides = subprop( Guides==undef? hash(opt, "Guides", true) :Guides
                     //Guides
                    , opt, "Guides"
                    , dfPropName="color"
                    , dfPropType="str"
                    , dfsub= [ "r", r, "len", r/0.015*0.4/ (isLink?2:1)
                             , "color",color
                             ] 
                   );
    
    GuideP = Guides ?
                  subprop( GuideP==undef? hash(opt, "GuideP", true) :GuideP
                         , opt, "GuideP"
                         , dfPropName="color"
                         , dfPropType="str"
                         , dfsub= Guides
                         )  
             : false; 
             
    GuideQ = Guides ?
                  subprop( GuideQ==undef? hash(opt, "GuideQ", true) :GuideQ
                         , opt, "GuideQ"
                         , dfPropName="color"
                         , dfPropType="str"
                         , dfsub= Guides
                         )  
             : false; 
                   
    Arrows = subprop( Arrows==undef? hash(opt, "Arrows", true) :Arrows
                    , opt, "Arrows"
                    , dfPropName="color"
                    , dfPropType="str"
                    , dfsub= [ "r", r
                             , "color",color
                             , "Head", isLink?false:true
                             ] 
                   );
    
        
    /* subunit "arrow" composes of 2 Arrow modules, one points to
       hP, one hQ. Seperated by gap. The ops is designed as though
       that both the Arrow modules is one Arrows module, 'cos it's 
       unlikely for the two arrows to be formatted differently:
    
        |                                      |
        |  _='                           '=_   |  
      hP|-'-------------  label  ------------'-|hQ
        | '-_          |<- gap->|         _-'  |
        |    '                           '     |
           <-- spacer                             <-- spacer      
        P--------------------------------------Q 
        
        */
    
    a_pqr = angle(site);
   
    P = site[0];
    Q = site[1];
    Ro= site[2];
    
    /* When followSlope = true, R has to be recalc
       such that new a_PQR = 90

            /  
           /'-._
          /     '-._  /
        P'-._       '/
         |    '-._  /        
         |        'Q
         |        /| 
         |       / Ro
                R
            /                    
           /'-._
          /     '-._  /
        Q'-._       '/
        /|    '-._  /        
       / |        '| P
      R  |         | 
         Ro                                          
    */
    R = followSlope
         ? ( a_pqr==90
             ? Ro 
             : anglePt( site, a=90)
           )  
         : Ro ;  
       
     /*
    P----Q   Need X to make pts along XP line
    |    |
    X----R
    */   
    X= P+R-Q; //cornerPt( [P,Q,R] );   
       
    dPQ = norm(P-Q);
    L = (!followSlope) && a_pqr!=90         // L= the dim to be reported
        ? norm( P- othoPt(p201(site))):dPQ;
    
    //-------------------------------------------- guide 
    //: Guides
    
    /*
            gP2         gQ2  --- 
             |           |    |
          hP +-----------+ hQ | g_len  
             |           |    |
         gP0=gP1         gQ1 ---
    spacer-->            |    |
             Po          |    | gQx_len  
             | '-_       |    |  
             X    '-_   gQ0  ---       
             |       '-_    <--- spacer    
             |          'Qo 
             |           |
             |           Ro     
         
    gPx=0 @ followslope=true, or a_PQR>=90
    gQx=0 @ followslope=true, or a_PQR<=90
    */ 
//    h = opt("h");
    //echo("h",h);
    
    //echo(Guides=Guides, GuideP=GuideP, GuideQ=GuideQ);
    g_len = hash(Guides, "len");
    gP_len = hash( GuideP, "len");
    gQ_len = hash( GuideQ, "len");
    
    // _h: an intrisic height to ensure there's a height
    //     when no guides
    _h = gP_len==undef? (gQ_len==undef? 0.5:gQ_len):gP_len;
    
    // extra len of guides due to slope of PQ
    gPx_len= (!followSlope)&& a_pqr<90? shortside( L, dPQ ): 0;
    gQx_len= (!followSlope)&& a_pqr>90? shortside( L, dPQ ): 0;
    
    gP0 = onlinePt( [P,X], len= -spacer );
    gP1 = onlinePt( [P,X], len= -spacer - gPx_len );    
    //gP2 = onlinePt( [P,X], len= -spacer - gPx_len - (gP_len==undef?g_len:gP_len));    
    gP2 = onlinePt( [P,X], len= -spacer - gPx_len - _h);//(gP_len==undef?g_len:gP_len));    
    
    gQ0 = onlinePt( [Q,R], len= -spacer );
    gQ1 = onlinePt( [Q,R], len= -spacer - gQx_len );    
    gQ2 = onlinePt( [Q,R], len= -spacer - gQx_len - _h);//(gQ_len==undef?g_len:gQ_len) );    
    
    hP  = onlinePt( [gP1,gP2], ratio= h);
    hQ  = onlinePt( [gQ1,gQ2], ratio= h);
    
    //echo( spacer=spacer, gP_len = gP_len, gQ_len = gQ_len, gPx_len = gPx_len, gQx_len = gQx_len, gap=gap);
    //echo( gP0=gP0, gP1=gP1, gP2=gP2, hP=hP, hQ=hQ );
    //MarkPts([gP0,gP1,gP2], Label=["text",["gP0","gP1","gP2"]], Line=false );
    //MarkPts([gQ0,gQ1,gQ2], Label=["text",["gQ0","gQ1","gQ2"]], Line=false );
    
    //echo(GuideP=GuideP);
    //echo(GuideQ=GuideQ);
    if( Guides && GuideP ) Line( [gP0, gP2], opt= GuideP );  
    if( Guides && GuideQ ) Line( [gQ0, gQ2], opt= GuideQ );  

    //echo(_Label= _fmth(_Label));
    //echo(Label= (Label));
    labelstr= isstr(Label)? Label
              : str( hash(_Label,"prefix","")
                , hash(Label, "text"
                      , numstr( L
                               , conv= hash(_Label,"conv")
                               , d=    hash(_Label,"decimal")
                               , dmax= hash(_Label,"maxdecimal")
                               )
                      )         
               , hash(_Label,"subfix","")
               );
               
    //echo(L=L, labelstr=labelstr, conv=hash(_Label,"conv")
    //, dmax=hash(_Label,"maxdecimal"), d=hash(_Label,"decimal"));
    //
    //================================================= line
    //
    
    // hP, hQ: two pts where line intersect guides
    // gapP, gapQ: two pts where the arrow line starts. 
    //             determined by ["line",["gap",0]]   
    /*
         gP2                    gQ2
          |                      |
       hP +-----gapP    gapQ-----+ hQ
          |                      |
         gP1                    gQ1    
    
       But, remember that the real hP needs to take
       into account the len of arrow head
    
             _-'| 
       _hP <----+--------- gapP
             '-_| hP 
    
           |<-->| len of arrow head 
    
    */


    labelsize = hash(_Label,"size") ;
    labelscale= hash(_Label,"scale");
    a=labelsize; 
    gap = _gap!=undef?_gap:
          textWidth( labelstr
                   , size = labelsize
                   , scale= labelscale
                   )+ 1*labelsize*labelscale;
                   
    //echo(labelsize=labelsize, labelscale=labelscale, _gap=_gap, gap=gap );
    
    lineL = gap>L? -0.3: (L-gap)/2; // arrow len
    
    //echo( labelsize=labelsize, labelscale=labelscale
    //     , L=L, gap=gap, lineL=lineL, labelstr=labelstr);
    
    begP = onlinePt( [hP,hQ], len=lineL);
    begQ = onlinePt( [hQ,hP], len=lineL);

    //echo(gap=gap, L=L, Arrows=Arrows);
    if( gap != L   //  Don't draw arrow when gap is as big as the dim length
        && Arrows ) 
    {
       Arrow( [ begP, hP], opt=Arrows );
       Arrow( [ begQ, hQ], opt=Arrows );
    }
    
//    if(opt_arrowP) Arrow( [ begP, hP], opt_arrowP );
//    if(opt_arrowQ) Arrow( [ begQ, hQ], opt_arrowQ );
         
    //       
    //================================================= label
    //: Label
    
    //textsite = [hQ, (begP+begQ)/2, P+Q-R];
    M = (begP+begQ)/2;
    
    textSite = [hQ, M, spacer+_h<0? (2*P-gP2)
                       : (2*gP2-gP0) ];
                       
                   
                   
    // ================================================= 2018.6.20: Link                          
    //: Link

    _Link = subprop( sub= Link==undef? hash(opt, "Link", false) :Link   
                   , opt= opt
                   , subname= "Link"
                   , dfPropName= "pts", dfPropType="pt,pts"
                   , dfsub=[ "pts",[]   // If pts given, use it 
                          , "steps",[]  // If steps given, use it in growPts() to generate pts  
                          , "r", r //hash("Guides","r")
                          , "color", color //hash("Guides","color") //"blue"
                          , "$fn",6
                          , "Arrow", false
                          , "spacer", 0.15
                          , "stem", isnum(Link)?Link
                                    :Link? 0.2:0
                          ] 
                   );
                   
    //echo(opt=opt);
                   
    //echo(Link=Link);               
    //echo(_Link=_Link);               
    //link_spacer = (len(labelscale)?labelscale[1]:labelscale)
    //              *(len(labelsize)?labelsize[1]:labelsize)*.8;  
                  
    //vStemOffset = ;                 
    //stem = hash(_Link,"stem");
    //textSite = _Link? [for(p=_textSite) p+uv( gP2-gP0)* (stem+link_spacer) ]
    //               : _textSite;               
    if(_Link) 
    {
      //echo(if_link="yes");
      _Dim_LinkedText(gP0, gP2, M, _Link, _Label, labelstr, textSite);
    
    }else{
                
      Text(labelstr, opt=_Label    
          , site= textSite
          , halign= hash(_Label,"halign","center")
          , valign= hash(_Label,"valign","center")
          );
     }
 } else // len(site)>3 
 {
   /*  
     when len(site)>3, like 
       
     [3,1,5,1], [2,3,4,5,6,7]  (indies when pts given)
     [p0,p1,p2,p3], [p0,p1,p2,p3,p4,p5,p6]  (when pts not given)
      
     The last one is always the reference pt.
   */
   
   //_sitePts= pts? [ for(i=site) len(i)==1?pts[i]:i ]
   //               : site;
    
   _sitePts= [ for(i=site) isint(i)?pts[i]:i ]; 
                
//    echo(site=site, _sitePts = _sitePts);
        { 
         _chainRatio= h(_Label, "chainRatio");
         chainRatio= _chainRatio?
                     ( isarr(_chainRatio)
                       ? _chainRatio
                       : let(rs= distRatios(slice(_sitePts,0,-1), isAccum=0)) //==> [ 0.5, 0.2, 0.3 ]
                          [ for(r=rs) round(r*100)/100 ] 
                     )
                     : _chainRatio;  
        } 
   
   for(i=[0:len(_sitePts)-3]) 
   {
     Rf = i== len(_sitePts)-3? last(_sitePts)
          : last(_sitePts)- last(_sitePts,2) +_sitePts[i];
     //echo(i=i, Rf=Rf, _sitePts = _sitePts);
	//================================================
	//================================================

     //echo(i=i);
     //echo(_chainRatio=_chainRatio, chainRatio=chainRatio);
     //echo(Label= Label);
     //echo(distRatios = distRatios(_sitePts, isAccum=0));
                 
     Dim(   
         site = [ _sitePts[i], _sitePts[i+1], Rf ] // [P,Q,R]
        , color=color
        , r=r 
        , spacer=spacer 
        , gap=gap    
        , h=h   
        , pts= undef
        , followSlope= followSlope   
        , Label= _chainRatio? 
                  update( isarr(Label)?Label:[]
                        , ["text", chainRatio[i]] 
                        )
                 : Label    
        , Guides=true  
        , GuideP= i==0?true:false  
        , GuideQ= true  
        , Arrows= Arrows
        , Link= Link
        , debug= debug 
        , opt=opt
        );           
   }
   
 } // len(site)>3 
 
   module _Dim_LinkedText(gP0, gP2, M
                         , _Link, _Label, labelstr
                         , textSite)
   {     
      //lv=2;                
      //log_b("_Dim_LinkedText()", lv);
      //log_(_green("Draw Link text"), lv);
      //log_(["_Link", _Link]);
      //log_(["_Label", _Label]);
      //log_(["labelstr", labelstr]);
        
      /*  Link:

                         Label <------- @ LabelPt (LP)  
                               <------- spacer 
                          s    <------- @ stemHead (SH)
                          |    <------- stem
                   +------M------+
                   |             |
           
    
       Default: These are the same :

       Link = true
       Link = 0.2
       Link = ["stem", 0.2]

                         Label <------- LabelPt (LP)  
                               <------- spacer = 0.1
                          SH    <------- stemHead  (SH)
                          |    <------- stem = 0.2
                   +------M------+
                   |             |

                   --+
                     |
                     M-- Label 
                     |
                   --+   

       Link = num: Set stem = num; 
            = pt : | stem=0? lpts = [M, SH, pt as LP]  
                   | stem!=0? lpts = [M, pts ] (last(pts))
                   
                         Label <------- LabelPt (LP) = pt 
                               <------- link.spacer = 0.1
                          x    
                          |    --------> Draw a line based on [SH, pt]  
                         SH   <------- stemHead  (SH)
                         /
                  .._   /     <------- stem  
                 /    'M._                   
                /'-._     '-.          
               /     '-._  /      
                        '-/        
                         /                       
                         
           = pts: | stem=0? lpts = [M]+pts  
                  | stem!=0? lpts = [M,SH] + pts 
                  
       Link = [ "steps", steps ] : 
              | stem=0? lpts = growPts(M, steps) 
              | stem!=0? lpts = [M] + growPts(SH, steps) 
                   
    */       
      stem = hash(_Link,"stem");
      SH= stem==0? M: (M+ uv( gP2-gP0)*stem); // Stem Head Pt
      //log_(["stem",stem,"SH",SH] );
      
      __linkSteps = hash(_Link, "steps");
      __lpts = hash(_Link,"pts");
      
      lpts = __linkSteps ?
                growPts( SH, __linkSteps )
             : __lpts ?
                let( _lpts = isnum(__lpts[0])?[__lpts]:__lpts )
                concat( [SH], _lpts)
             : undef;

      link_spacer= hash(_Link, "spacer");
      LP= lpts? 
            onlinePt(get(lpts, [-1,-2]), len=-link_spacer)
          : (stem==0 ?M:SH)+ uv( gP2-gP0)*link_spacer;
                       
      _arrow = hash(_Link,"Arrow",[]);

      //echo(Link=Link);
      //echo(_Link=_Link);
      //echo(M=M, SH=SH);
      //echo(stem= stem );
      //echo(__linkSteps = __linkSteps );
      //echo(__lpts=__lpts);
      //echo(_arrow=_arrow);
      //echo(lpts=lpts);
      //echo(LP=LP);
      
      if(_arrow==false )
        { if (SH) Line( [M, SH], opt=_Link); 
        }
      else 
      Arrow( stem?[SH, M]:get(lpts, [1,0])
             //[SH, M]
           , opt= ishash(_arrow)? update(_arrow, _Link):_Link
           );
      if(len(lpts)>1)
      {
         Line( lpts, opt=_Link);
      } 
      Text(labelstr, opt=_Label    
        , site= [for(p=textSite)p+ LP-textSite[1]]
        , halign= hash(_Label,"halign","center")
        , valign= hash(_Label,"valign","center")
        ); 
      
      //log_e("",lv);     
   }      
}// Dim

  
module Dims( pts, opts, debug=0, opt=[]) 
{                      
    // Recode 2018.5.21
    // opts: array of [ site, opt ], each representing a single Dim
    // Dims: for cases of setting different dims on
    //       an given *pts*, each dim could have its own opt.
    //       Good for using dim as sub-obj in other obj
    //REF: see Dim_demo.scad and Cube_demo.scad
   /*
   
   opts= array of site or [site,opt]
   
   
    Used in Cube():
    
      Cube(... dim= [2,3,4] );
      Cube(... dim= [2,3,pt] );
      Cube(... dim= [ [2,3,pt], [4,5,6],... ] );
      Cube(... dim= [ [2,3,pt], ["pqr", [2,3,pt], "color", ], ... ] );
      Cube(... dim= ["pqr", [2,3,pt], "color", ] );
      Cube(... dim= [ "pqrs", [[2,3,pt]], "color", "red" ] );        
      Cube(... dim= [ "pqrs", [2,3,pt], "color", "red" ] );     
   */
   
   if(debug)
   {
     echom(_b(_s("Dims, pts={_}",str(pts))));
     echo(opt=opt);
   }
      
   for( site_opt = opts )
   {
      _opt= isnum( site_opt[0] )?opt 
            : update( opt, site_opt[1] );
            
      site = isnum( site_opt[0] )? site_opt: site_opt[0];                    
      
      if(debug)
      { 
        echo(site_opt=site_opt);
        echo(site=site)
        echo(_opt=_opt);
      }
      
      Dim( site= site
         , pts=pts
         , debug=debug
         , opt= update(_opt, opt) 
         );
   }
   
}








  