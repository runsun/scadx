include <scadx.scad>

module simpleChainData_testing( label, settings, isdetails, isdraw ){
    /*
        isdetails: 0/1: show test details
        isdraw: 0: no draw
                1: draw main obj only
                2: draw detailed marking lines (Mark90, etc)S
    */
    echom("simpleChainData_testing");
    
    //##################################################
    //##################################################
    
    module simpleChainData_testing_Draw( chdata, color, transp=1, isdraw){
        echom(str("simpleChainData_testing_Draw, isdraw=", isdraw));
        
        //echo(chdata=chdata);
        cuts = hash( chdata, "cuts");
        nseg = len(cuts)-1;
        nside = len(cuts[0]);
        pts  = joinarr( cuts );
            
        if(isdraw==2){
            
            bone = hash( chdata,"bone");
            Rf   = hash( chdata,"Rf");
            is2pt= len(bone)==2;
            
             
            MarkPts( hash(chdata, "bone" ),"ch");
            MarkPts( [for(cut=cuts)cut[0]]
                   , "ch=[color,blue,r,0.01];ball=0");
            
            if(!is2pt)
                MarkPts( p012(bone)
                        , "pl=[transp,0.2];ball=0");
            if(len(cuts[0])>1)
                MarkPts( cuts[0]
                        , "ch=[transp,1,color,red, r,0.008];ball=0");
            if(len(last(cuts))>1)
                MarkPts( last(cuts)
                        , "ch=[transp,1,color,green,r,0.008];ball=0");
            
            P=bone[0]; Q=bone[1]; R=is2pt?Rf:bone[2];
            pl00 = cuts[0][0];
            pl01 = cuts[0][1];
            N0 = N( [Q,P,R] );
            B0 = B( [Q,P,R] );
            
            if( hash(chdata,"lasttilt")){
                last= get(bone,-1);
                last2= get(bone,-2);

                lastpl = last(cuts);
                lastpl0= lastpl[0];
                lastpl1= lastpl[1];

                Mark90([ last2, last ,lastpl0],"ch" );
                Mark90([ last2, last ,lastpl1],"ch" );
                Mark90( [lastpl0
                      , projPt(lastpl0, [last2,last]), last2], "ch" );
                Mark90([ lastpl1
                      , projPt(lastpl1, [last2,last]), last2], "ch" );
                
            } else {    
                Mark90( [Q,P,pl00],"ch" );
                Mark90( [Q,P,pl01],"ch" );
                Mark90( [ pl00, projPt( pl00, [P,Q]), P], "ch" );
                Mark90( [ pl01, projPt( pl01, [P,Q]), P], "ch" );
            }
        }//.. isdraw=2
        
        shape = hash(chdata,"shape", "chain");
        faces = faces( shape= shape //hash(chdata,"shape", "chain")
                     , nside=nside, nseg= nseg  );

        color(color,transp)
        polyhedron(points = pts, faces=faces );     
        
    }    
    //##################################################
    //##################################################
    function get_gots( chdata )=
    (
        //======================================== 
        // Internal use for simpleChainData_testing
        //========================================
        let(
             cd = chdata
           //, seed= hash(cd,"seed")
           , cuts= hash(cd,"cuts")
           , bone= hash(cd,"bone")
    
           , basecut0 = cuts[0][0]
           , basecut1 = cuts[0][1]
           , lastcut0 = last(cuts)[0]
           , lastcut1 = last(cuts)[1]
    
           , lastbone = get(bone,-1)
           , lastbone2= get(bone,-2)
    
           , tilt= hash(cd,"tilt")
           , lasttilt= hash(cd,"lasttilt")
           , closed = hash(cd, "closed")
           , rs = hash(cd,"rs")
           , Rf = hash(cd, "Rf")
           , AjPQ= projPt(basecut0,p01(bone))
           , AjPQR= len(bone)==2?undef:projPt(basecut0,p012(bone))
           , BjPQ= projPt(basecut1,p01(bone))
           , BjPQR= len(bone)==2?undef:projPt(basecut1,p012(bone))
                
           , got=let( a_per_side= angle( [cuts[0][0], bone[0],cuts[0][1]])
                    )
                concat(
                       
                [
                 "first_r",  dist(bone[0], basecut0)
                ,"rs", [for( i=range(cuts) )
                        dist( bone[i], cuts[i][0] ) ]
                ,"twist_seg0", twistangle( basecut0, bone[0], bone[1],
                                      cuts[1][0] )
                ,"a_per_side", a_per_side
                
               // ,"rot", len(bone)>2? // choice of this var is questionable
               //         -twistangle( basecut0, bone[0],bone[1], cuts[1][0]) 
               //         : a_per_side
               ]
               , !lasttilt? 
                [
                 "tilt",     angle( [bone[1],bone[0],basecut0] )
                ,"a_basecut0_bone", angle( [ bone[1],bone[0],basecut0] )
                ,"a_basecut1_bone", angle( [ bone[1],bone[0],basecut1] )
                ,"a_basecut0_pqr", len(bone)==2?undef
                                :isequal(AjPQ,AjPQR)?0
                                :angle( [basecut0, AjPQ,AjPQR ] )
                ,"a_basecut1_pqr", len(bone)==2?undef
                                :isequal(BjPQ,BjPQR)?0
                                :angle( [basecut1, BjPQ,BjPQR ] )
                ]
                :[
                 "lasttilt",  [ lastbone2,lastbone
                                    , lastcut0
                                    ]                         
                ,"a_lastcut0_bone", angle( [ lastbone2,lastbone
                                            ,lastcut0] )
                ,"a_lastcut1_bone", angle( [ lastbone2,lastbone
                                            ,lastcut1] )  
                ]
              )//..concat  
           )//..let
           got 
    );    
    //##################################################
    //##################################################

    function module_testing_get_result_of_setting( setting, gots )=
    (
       let( 
            chdata= hash( setting, "chdata")
           , cuts = hash( chdata, "cuts") 
           , wants= hash( setting, "wants")
           , wantvars= keys(wants)
           , res= [ for( wantvar=wantvars )
                    let( want = hash(wants, wantvar)
                       , got  = hash(gots,  wantvar)
                       , fail = isequal( want,got )?0:1
                       , msg= str( "__|<b>"
                                , fail?_red(wantvar)
                                     : wantvar
                                , "</b>== "
                                , want, " ? "
                                , fail? _red( got )
                                   : _green("pass")
                                )
                       )
                       [ fail, msg ]
                  ]      
            , test_count_this_setting= len(wantvars)
            , test_fail_this_setting= sum( [for(re=res) re[0]] )
            , label = _color( hash( setting, "label")
                          , hash( setting, "color" ) 
                          )
            , msg = str( label,"<br/>"
                    , join( [for(re=res)re[1]],"<br/>")
                    )
            )//..let
           [ test_count_this_setting
           , test_fail_this_setting, msg ]
                    
    );//..module_testing_get_result_of_setting()

    module module_testing_echo( settings, results ){

        nTests = sum( [for(tfm= results ) tfm[0] ] );
        nFails = sum( [for(tfm= results ) tfm[1] ] ); 
        echo( str( 
            "<span style='font-size:14px;background:lightyellow'><u><b>"
            , label, " </u></b> "

             , _bcolor(str(
                    "<u>--- tests "
                    ," (in ", len(settings), " settings) : "
                    , _blue(nTests)
                    , ", failed: ", _color( nFails, nFails?"red":"green")
                    , "</u>&nbsp;</span>&nbsp;"
               ),"lightyellow") 
             , " <br/>(settings: "
                , join([ for(i = range(settings))
                    let(setting=settings[i]
                       ,label = hash(setting,"label")
                       , fail = results[i][1]
                       )   
                   _color( str( label
                              , fail? str("<b style='color:red'>="
                                         , fail, " fail(s)</b>"):""
                              )
                         , hash( setting, "color"))
                       ],", ")
             , ")"
             )
            ); 
    }
    
    //##################################################
    //##################################################
    //gots = get_gots( hash( settings[0], "chdata"));
    //echo(gots = gots);
    
    if(settings)
    {            
        results=[
                  for( setting=settings )
                     module_testing_get_result_of_setting( 
                        setting 
                      , gots = get_gots( hash(setting,"chdata"))
                     )
                ]; //.. result
        //results=[];            
        //echo( results = results );       
        for( i=range(settings) ){
            setting= settings[i];
            //echo(i=i, setting=setting);
            if(isdraw || hash(setting,"isdraw")){                 
                simpleChainData_testing_Draw( 
                    chdata = hash( setting, "chdata")
                   ,color= hash( setting, "color")
                   ,transp= hash(setting, "transp")
                   ,isdraw=isdraw
                );
            }    
            if(isdetails || hash(setting,"isdetails"))
                echo( results[i][2] );
        }       
           
        module_testing_echo( settings, results ); 
               
    }//..if(settings)
    
}//..simpleChainData_testing 
//simpleChainData_testing(isdraw=1);

module simpleChainData_testing_2pointer(isdetails=0, isdraw=0){
    
    //echom("simpleChainData_testing_2pointer",
    //     , "simpleChainData( pq, r=0.8 )" );
    
    //=====================================================
    pq= randPts(2);
    chdata= simpleChainData(pq, r=0.8);
    function chdata(k,df)=hash(chdata,k,df);
    cuts = chdata("cuts");
    setting1= [ "label", "1) 2-pointer"
              , "color", "olive"
              , "transp" , 0.6
              , "chdata", chdata
              , "wants", [ "a_basecut0_bone",90
                         , "a_basecut1_bone",90
                         , "twist_seg0", 0
                         , "a_per_side", 90
                        // , "a_basecut0_pqr",0
                         ]
              , "isdetails", isdetails
              , "isdraw", isdraw
              ];
    //=====================================================
    settings= [setting1
              ];
    simpleChainData_testing( label="simpleChainData_testing_2pointer"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw
    );    
}   
//simpleChainData_testing_2pointer(isdetails=1, isdraw=1);

module simpleChainData_testing_rot(isdetails, isdraw){  
    //echom("simpleChainData_testing_rot");

    nseg = 4;
    nside= 4;
    r = 0.6;

    rchrtn = chainBoneData( nseg+1, a=[ 130,170], a2=[-60,60] ); 
    bone1 = hash(rchrtn, "pts");

    //=====================================================
    chdata1 = simpleChainData( bone1, r=r, nside=nside );
    cuts1 = hash( chdata1, "cuts" );
    setting1= [ "label", "1) nside=4"
              , "color", "olive", "transp",0.8
              , "chdata", chdata1
              //, "shape", "chain"
              , "wants", [ 
                       "a_basecut0_bone",90
                     , "a_basecut1_bone",90
                     , "a_basecut0_pqr",0
                     , "a_basecut1_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              , "isdetails", isdetails
              , "isdraw", isdraw
              ];
    //=====================================================
    chdata2 = simpleChainData( trslN(bone1,3), r=r, nside=nside, rot=45 );
    cuts2 = hash( chdata2, "cuts" );
    setting2= [ "label", "2) nside=4,rot=45"
              , "color", "red", "transp",0.5
              , "chdata", chdata2
              //, "shape", "chain"
              , "wants", [ 
                       "a_basecut0_bone",90
                     , "a_basecut1_bone",90
                     , "a_basecut0_pqr", 45
                     , "a_basecut1_pqr",45
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              , "isdetails", isdetails
              , "isdraw", isdraw
              ];
    
    //=====================================================
    settings= [setting1
              ,setting2
              ];
    simpleChainData_testing( label="simpleChainData_testing_rot"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw
                     ); 
    
}
//simpleChainData_testing_rot(isdetails=1, isdraw=1); //15"

module simpleChainData_testing_tilt(isdetails, isdraw){  
    //echom("simpleChainData_testing_tilt");

    nseg = 3;
    nside= 4;
    r = 0.6;

    rchrtn = chainBoneData( nseg+1, a=[ 130,170], a2=[-60,60] ); 
    bone1 = hash(rchrtn, "pts");

    //=====================================================
    chdata1 = simpleChainData( bone1, r=r, nside=nside );
    cuts1 = hash( chdata1, "cuts" );
    setting1= [ "label", "1) No tilt"
              , "color", "olive", "transp",0.5
              , "chdata", chdata1
              //, "shape", "chain"
              , "wants", [ 
                       "a_basecut0_bone",90
                     , "a_basecut1_bone",90
                     , "a_basecut0_pqr",0
                     , "a_basecut1_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
             // , "isdraw", 1
              ];
    //=====================================================
    chdata2 = simpleChainData( trslN(bone1,2), r=r, nside=nside, tilt=60 );
    cuts2 = hash( chdata2, "cuts" );
    setting2= [ "label", "2) tilt=60"
              , "color", "red", "transp",0.3
              , "chdata", chdata2
              //, "shape", "chain"
              , "wants", [ 
                       "a_basecut0_bone",60
                     , "a_basecut1_bone",90
                     , "a_basecut0_pqr",0
                     , "a_basecut1_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
             //, "isdraw", 1
              ];

//    //=====================================================
//    chdata21 = simpleChainData( trslN2(bone1,2), r=r, nside=nside
//                                                , tilt=[0,45,30] );
//    cuts21 = hash( chdata21, "cuts" );
//    setting21= [ "label", "21) tilt=[0,45,30]"
//              , "color", "red", "transp",0.3
//              , "chdata", chdata21
//              //, "shape", "chain"
//              , "wants", [ 
//                       "a_basecut0_bone",60
//                     , "a_basecut1_bone",90
//                     , "a_basecut0_pqr",0
//                     , "a_basecut1_pqr",0
//                     , "twist_seg0", 0
//                     , "a_per_side", 90 
//                     ]
//              , "isdetails", 1
//             , "isdraw", 1
//              ];
              
    //=====================================================
    chdata3 = simpleChainData( trslN(bone1,4), r=r, nside=nside, rot=60 );
    cuts3 = hash( chdata3, "cuts" );
    setting3= [ "label", "3) rot=60"
              , "color", "green", "transp",0.3
              , "chdata", chdata3
              //, "shape", "chain"
              , "wants", [ 
                       "a_basecut0_bone",90
                     , "a_basecut1_bone",90
                     , "a_basecut0_pqr",60
                     , "a_basecut1_pqr",30
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];

    //=====================================================
    /* :::::NOTE:::::
       The first num in tilt, 90, is an indication of which direction
       the tilt is to operate. This does NOT mean that the data points
       are rotated by 90. Note that the pl pts starts at a pt the same
       as default.
       To rotate the data pts, use rot=n. See next: setting5
    */ 
    chdata4 = simpleChainData( trslN(bone1,6), r=r, nside=nside, tilt=[90,45] );
    cuts4 = hash( chdata4, "cuts" );
    setting4= [ "label", "4) tilt=[90,45]"
              , "color", "blue", "transp",0.3
              , "chdata", chdata4
              //, "shape", "chain"
              , "wants", [ 
                       "a_basecut0_bone",90
                     , "a_basecut1_bone",45
                     , "a_basecut0_pqr",0
                     , "a_basecut1_pqr",0
                     , "twist_seg0", 0
                     
                    // , "a_per_side",  
                     ]
             // , "isdetails", 1
             // , "isdraw", 1
              ];
              
    //=====================================================
    
    chdata5 = simpleChainData( trslN(bone1,8), r=r,nside=nside, rot=90, tilt=45);//,tilt=90);
    cuts5 = hash( chdata5, "cuts" );
    
//    chdata51 = simpleChainData( trslN(bone1,8), r=r,nside=nside,tilt=[90,45]);
//    cuts51 = hash( chdata51, "cuts" );
//    MarkPts( cuts51[0], "ch=[r,0.005];pl=[transp,0.1];ball=false");
//
//    Rf= hash( chdata5, "Rf");
//    echo( Rf=Rf );
//    MarkPt( Rf, "l=[color,darkcyan,text,Rf]" );
    
    setting5= [ "label", "5) rot=90, tilt=45"
              , "color", "purple", "transp",0.3
              , "chdata", chdata5
              //, "shape", "chain"
              , "wants", [ 
                       "a_basecut0_bone",45
                     , "a_basecut1_bone",90
                     , "a_basecut0_pqr",0
                     , "a_basecut1_pqr",0
                     , "twist_seg0", 0
                     
                     //, "a_per_side",  
                     ]
              //, "isdetails", 1
             // , "isdraw", 1
              ];

              
    //=====================================================
    settings= [setting1
              ,setting2
              //,setting21
              ,setting3
              ,setting4
              ,setting5
              ];
    simpleChainData_testing( label="simpleChainData_testing_tilt"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw 
                     ); 
    
}
//simpleChainData_testing_tilt(isdetails=0, isdraw=0); //3~37"

module simpleChainData_testing_lasttilt(isdetails, isdraw){  
    //echom("simpleChainData_testing_lasttilt");

    nseg = 3;
    nside= 4;
    r = 0.6;

    rchrtn = chainBoneData( nseg+1, a=[ 130,170], a2=[-60,60] ); 
    bone1 = hash(rchrtn, "pts");

    //=====================================================
    chdata1 = simpleChainData( bone1, r=r, nside=nside, lasttilt=90 );
    cuts1 = hash( chdata1, "cuts" );
    setting1= [ "label", "1) No tilt"
              , "color", "olive", "transp",0.5
              , "chdata", chdata1
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                     //, "tilt", 90
                      "a_lastcut0_bone",90
                     , "a_lastcut1_bone",90
                     //, "lasttilt", 90
                     //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
    //=====================================================
    chdata2 = simpleChainData( trslN(bone1,2), r=r, nside=nside
                , lasttilt=60 );
    //echo(chdata2=chdata2);
    cuts2 = hash( chdata2, "cuts" );
    setting2= [ "label", "2) lasttilt=60"
              , "color", "red", "transp",0.3
              , "chdata", chdata2
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                      //"tilt", 90
                      "a_lastcut0_bone",60
                     , "a_lastcut1_bone",90
                     //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];

    //=====================================================
    chdata3 = simpleChainData( trslN(bone1,4), r=r, nside=nside, rot=90 );
    cuts3 = hash( chdata3, "cuts" );
    setting3= [ "label", "3) rot=90"
              , "color", "green", "transp",0.3
              , "chdata", chdata3
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                       "a_basecut0_bone",90
                     , "a_basecut1_bone",90
                    //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
    //=====================================================
    chdata4 = simpleChainData( trslN(bone1,6), r=r, nside=nside
                                            , lasttilt=[90,45] );
    cuts4 = hash( chdata4, "cuts" );
    setting4= [ "label", "4) lasttilt=[90,45]"
              , "color", "blue", "transp",0.3
              , "chdata", chdata4
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                     "a_lastcut0_bone",90
                     , "a_lastcut1_bone",45
                     //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                      ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
              
    //=====================================================
    chdata5 = simpleChainData( trslN(bone1,8), r=r, nside=nside
                                            , rot=90, lasttilt=45 );
    cuts5 = hash( chdata5, "cuts" );
    setting5= [ "label", "5) rot=90,lasttilt=45"
              , "color", "purple", "transp",0.3
              , "chdata", chdata5
              //, "shape", "chain"
              , "wants", [ 
                       //"first_r", 0.6 
                     "a_lastcut0_bone",45
                     , "a_lastcut1_bone",90
                     //, "rs", chdata("rs")
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                      ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
              
    //=====================================================
    settings= [setting1
              ,setting2
              ,setting3
              ,setting4
              ,setting5
              ];
    simpleChainData_testing( label="simpleChainData_testing_lasttilt"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw 
                     ); 
    
}
//simpleChainData_testing_lasttilt(isdetails=0, isdraw=1); //2", 46"

module simpleChainData_testing_closed(isdetails, isdraw){  
    //echom("simpleChainData_testing_closed");

    nseg = 6;
    nside= 4;
    r = 0.8;

    rchrtn = chainBoneData( nseg+1, seglen=2,a=[ 120,150], a2=[0,30] ); 
    bone1 = hash(rchrtn, "pts");

    //=====================================================
    chdata1 = simpleChainData( bone1, r=r, nside=nside );
    cuts1 = hash( chdata1, "cuts" );
    setting1= [ "label", "1) nside=4"
              , "color", "olive", "transp",0.9
              , "chdata", chdata1
              //, "shape", "chain"
              , "wants", [ 
                       "a_basecut0_bone",90
                     , "a_basecut1_bone",90
                     , "a_basecut0_pqr",0
                     , "a_basecut1_pqr",0
                     , "twist_seg0", 0
                     , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
 
    //=====================================================
    chdata2 = simpleChainData( trslN(bone1,4), r=r, nside=nside, closed=true );
    cuts2 = hash( chdata2, "cuts" );
    setting2= [ "label", "2) closed=true"
              , "color", "red", "transp",0.9
              , "chdata", update(chdata2, ["shape","ring"])
              //, "shape", "ring"
              , "wants", let( bone= hash(chdata2,"bone")
                            , p3_0 = get3( bone, -1 )
                            , p3_last= get3( bone,-2)
                            )
                     [  //"a_basecut0_bone",angle(p3_0)/2
                     //, "a_basecut1_bone",90
                      "a_basecut0_pqr", 0
                     , "a_basecut1_pqr",0
                     , "twist_seg0", 0
                    // , "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
              
    //=====================================================
    chdata3 = simpleChainData( trslN(bone1,8), r=r, nside=nside 
                                          , closed=true, rot=45 );
    cuts3 = hash( chdata3, "cuts" );
    setting3= [ "label", "3) closed=true, rot=45"
              , "color", "green", "transp",0.9
              , "chdata", update(chdata3, ["shape","ring"])
              //, "shape", "ring"
              , "wants", [ 
                      // "a_basecut0_bone",90
                    // , "a_basecut1_bone",90
                      "a_basecut0_pqr", 45
                     , "a_basecut1_pqr",45
                     , "twist_seg0", 0
                     //, "a_per_side", 90 
                     ]
              //, "isdetails", 1
              //, "isdraw", 1
              ];
    
    //=====================================================
    settings= [setting1
              ,setting2
              ,setting3
              ];
    simpleChainData_testing( label="simpleChainData_testing_closed"
                     , settings=settings
                     , isdetails=isdetails
                     , isdraw =isdraw
                     ); 
    
}
//simpleChainData_testing_closed(isdetails=1, isdraw=2);//3", 26"

module simpleChainData_demo_arrow_0(){
    //echom("simpleChainData_demo_arrow_0");
    
    chbone = chainBoneData( seed=pqr(), seglen=4, n=3, a=180,a2=0);
    bone1 = hash(chbone,"pts");
   
    //-------------------------------------------------
    chdata = simpleChainData( bone1, r=[ 1,[1,1.5],0], nside=4 );
    //echo(chdata=chdata);
    pts = joinarr( hash(chdata,"cuts"));
    //echo(pts = pts );
    //MarkPts( pts, "ch;ball=0");
    faces = faces("cone", nside=4, nseg=3);
    //echo(faces = faces );
    polyhedron(points = pts, faces=faces );
    
    
    //-------------------------------------------------
    bone2 = [ for(p=bone1) p+ [0,5,0]];
    //MarkPts(bone2,"ch;l");
    chdata2 = simpleChainData( bone2, r=[ 0,[1.5,1],1], nside=3 );
    //echo(chdata2=chdata2);
    pts2 = joinarr( hash(chdata2,"cuts"));
    //echo(pts2 = pts2 );
    //MarkPts( pts2, "ch;ball=0");
    faces2 = faces("conedown", nside=3, nseg=3);
    //echo(faces2 = faces2 );
    color("red",0.8)
    polyhedron(points = pts2, faces=faces2 );

    
    //-------------------------------------------------
    _bone3 = [ for(p=bone2) p+ bone2[0]-bone1[0] ];
    bone3 = concat( [onlinePt( p01(_bone3),-1)], _bone3);
    //echo(bone3=bone3);
    //MarkPts(bone3,"ch;l");
    chdata3 = simpleChainData( bone3, r=[ 0,[1.5,1],[1,2],0], nside=24 );
    //echo(chdata3=chdata3);
    pts3 = joinarr( hash(chdata3,"cuts"));
    //echo(pts3 = pts3 );
    //MarkPts( pts3, "ch;ball=0");
    faces3 = faces("spindle", nside=24, nseg=5);
    //echo(faces3 = faces3 );
    color("darkcyan",1)
    polyhedron(points = pts3, faces=faces3 );
        
    
    //-------------------------------------------------
    _bone4 = [ for(p=bone2) p+ bone3[0]-bone1[0] ];
     bone4= concat( _bone4
                  , [anglePt( [ _bone4[0],last(_bone4), bone3[0]]
                     , a=90, len=-4)] 
                 );
    Mark90( get3(bone4,-3));
    //echo(bone4=bone4);
    //MarkPts(bone4,"ch;l=PQRST");
    chdata4 = simpleChainData( bone4, r=[ 0,[0.5,0.2],0.2,0.2], nside=12 );
    //echo(chdata4=chdata4);
    pts4 = joinarr( hash(chdata4,"cuts"));
    //echo(pts4 = pts4 );
    //MarkPts( pts4, "ch;ball=0");
    faces4 = faces("conedown", nside=12, nseg=4);
    //echo(faces3 = faces3 );
    color("darkcyan",0.8)
    polyhedron(points = pts4, faces=faces4 );    
    
    
}
//simpleChainData_demo_arrow_0();

module simpleChainData_tests(isdetails=0, isdraw=0){
    
    simpleChainData_testing_2pointer( isdetails=isdetails, isdraw=isdraw );
    simpleChainData_testing_rot( isdetails=isdetails, isdraw=isdraw );
    simpleChainData_testing_tilt( isdetails=isdetails, isdraw=isdraw );
    simpleChainData_testing_lasttilt( isdetails=isdetails, isdraw=isdraw );
    simpleChainData_testing_closed( isdetails=isdetails, isdraw=isdraw );
}    
//simpleChainData_tests(); // 9"

module simpleChainData_demo_curved_arrow(){
    echom("simpleChainData_demo_curved_arrow");
    
    //n= 3;
    chbone = chainBoneData( seed=pqr(),seglen=5
             , n=5, a=[100,120],a2=[-60,60]);
    bone0 = smoothPts( hash(chbone,"pts"), n=6 );
    MarkPts(bone0, "ch;r=0.05");
    arrow_r = 1;
    r = 0.4;
    
    //-------------------------------------------------
    bone1= bone0;
    nArrow1= 5;// How many pts taken to be the arrow head
    lenArrow1= sum( [for(i=[0:nArrow1-2]) dist( bone1[i], bone1[i+1]) ]);
    nNonArrow1 = len(bone1)-nArrow1;
    _arrow_head_rs= [ for(i=range(nArrow1))
                      arrow_r * dist( bone1[i],bone1[0])/lenArrow1 ]; 
    arrow_head_rs = app(slice(_arrow_head_rs,0,-1), [arrow_r, r]);
    rs1 = concat( arrow_head_rs
                , repeat([r],nNonArrow1)
                );
    //echo(rs1=rs1);
    chdata = simpleChainData( bone1, r=rs1, nside=6 );
    //echo(chdata=chdata);
    cuts = hash( chdata, "cuts");
    //echo( cuts= arrprn(cuts) );
    faces = faces("conedown", nside= len(get(cuts,-1))
                            , nseg= len(bone1));
    //echo(faces = faces );
    pts = joinarr(cuts);
    //MarkPts( pts, "ch;ball=0");
    polyhedron(points = pts, faces=faces );
    
    //---------------------------------------------
    bone2= trslN(bone0, 5);
    r2 = r;
    arrow2_r = arrow_r;
    nArrow2= 5;// How many pts taken to be the arrow head
    lenArrow2= sum( [for(i=[0:nArrow2-2]) 
                    dist( bone2[i], bone2[i+1]) ]);
    nNonArrow2 = len(bone2)-nArrow2;
     _arrow2_tail_rs= [ for(i=range(nArrow2))
                      arrow2_r * dist( bone2[len(bone2)-i-1],last(bone2))/lenArrow2 ]; 
    arrow2_tail_rs = concat( [[r2, arrow2_r]]
                          , slice(reverse(_arrow2_tail_rs),1) );

    rs2 = concat( repeat([r2], nNonArrow2)
                , arrow2_tail_rs
                );
    //echo(rs2=rs2);
    chdata2 = simpleChainData( bone2, r=rs2, nside=3 );
    //echo(chdata=chdata2);
    cuts2 = hash( chdata2, "cuts");
    //echo( cuts= arrprn(cuts2) );
    faces2 = faces("cone", nside= len(cuts2[0])
                            , nseg= len(bone2));
    //echo(faces2 = faces2 );
    pts2 = joinarr(cuts2);
    //MarkPts( pts2, "ch;ball=0");
    color("red")
    polyhedron(points = pts2, faces=faces2 );    
    
    //---------------------------------------------
    bone3= trslN(bone0, 10);
    L3=len(bone3);
    r3 = r;
    arrow3_r = arrow_r;
    arrow3_r2= arrow3_r*1.5;
    nArrow3= 4;// How many pts taken to be the arrow head
    nArrow32= 6;// How many pts taken to be the arrow head
    headlen3 = chainlen( bone3, 0,nArrow3-1);
    taillen3 = chainlen( bone3, i=L3-nArrow32-2); 

    nNonArrow3 = L3-nArrow3-nArrow32;
    
    _arrow3_head_rs= [ for(i=range(nArrow3))
                      arrow3_r * dist( bone3[i],bone3[0])/headlen3 ]; 
    arrow3_head_rs = app(slice(_arrow3_head_rs,0,-1), [arrow3_r, r3]);
  
    _arrow3_tail_rs= [ for(i=range(nArrow32))
                      arrow3_r2 * dist( bone3[len(bone3)-i-1]
                                      ,last(bone3))/taillen3 ]; 
    arrow3_tail_rs = concat( [[r3, arrow3_r]]
                          , slice(reverse(_arrow3_tail_rs),1) );

    rs3 = concat( arrow3_head_rs
                , repeat([r3], nNonArrow3)
                , arrow3_tail_rs
                );
    echo(rs3=rs3);
    chdata3 = simpleChainData( bone3, r=rs3, nside=6 );
    //echo(chdata=chdata2);
    cuts3 = hash( chdata3, "cuts");
    //echo( cuts= arrprn(cuts2) );
    faces3 = faces("spindle", nside= len(cuts3[ floor(len(cuts3)/2) ])
                            , nseg= len(bone3)+1);
    //echo(faces3 = faces3 );
    pts3 = joinarr(cuts3);
    //MarkPts( pts3, "ch;ball=0");
    color("green")
    polyhedron(points = pts3, faces=faces3 );    
    
}
//simpleChainData_demo_curved_arrow();

module simpleChainData_demo_r(){
    echom("simpleChainData_demo_r");
    
    //n= 3;
    chbone = chainBoneData( seed=pqr(), seglen=3
             , n=8, a=[130,180],a2=[0,60]);
    bone0 = hash(chbone,"pts"); //smoothPts( hash(chbone,"pts"), n=4 );
    //MarkPts(bone0, "ch;r=0.05");
    arrow_r = 1;
    r = 0.4;
    
    //-------------------------------------------------
    bone1= smoothPts(bone0,n=2);
    MarkPts(bone1,"ch");
//    rs1=[1,1,1,2,2];
//    //echo(rs1=rs1);
//    chdata = simpleChainData( bone1, r=rs1, nside=4 );
//    //echo(chdata=chdata);
//    cuts = hash( chdata, "cuts");
//    //echo( cuts= arrprn(cuts) );
//    pts = joinarr(cuts);
//    //MarkPts( pts, "ch;ball=0;color=black");
//    //for(cut=cuts) MarkPts(cut,"ch;ball=0");
//    //for(cut=cuts) MarkPts(cut,"pl=[trp,0.1];ball=0");
//        
//    faces = faces(shape="chain", nside= len(get(cuts,-1))
//                            , nseg= len(bone1)-1);
//    
//    //echo(faces = faces );
//    color(undef, 0.9)
//    polyhedron(points = pts, faces=faces );
    
    //-------------------------------------------------
    bone2= trslN( bone1, 10);
    MarkPts(bone2,"ch");
//    rs2=[1,1,[1,2],[2,1]];
//    //echo(len=len(rs2), rs2=rs2);
//    chdata2 = simpleChainData( bone2, r=rs2, nside=4 );
//    //echo(chdata2=chdata2);
//    cuts2 = hash( chdata2, "cuts");
//    //echo( cuts2= arrprn(cuts2) );
//    pts2 = joinarr(cuts2);
//    //MarkPts( pts2, "ch;ball=0;color=black");
//    //for(cut=cuts2) MarkPts(cut,"ch;ball=0");
//    //for(cut=cuts2) MarkPts(cut,"pl=[trp,0.1];ball=0");
//        
//    faces2 = faces(shape="chain", nside= len(get(cuts2,-1))
//                            , nseg= len(cuts2)-1);
//    
//    //echo(faces = faces );
//    color("red", 0.9)
//    polyhedron(points = pts2, faces=faces2 );
//    
    
    //-------------------------------------------------
    bone3= trslN( bone1, 20);
    MarkPts(bone3,"ch");
    chainlen = chainlen(bone3); 
    r0= 2; rlast=0; 
    slope = (rlast-r0)/chainlen;

//    rs3= func2d("line", ybeg=r0, yend=rlast, xpts=bone3);
//    echo(lenbone3=len(bone3), len=len(rs3), rs3=rs3);
//    chdata3 = simpleChainData( bone3, r=rs3, nside=4 );
//    //echo(chdata3=chdata3);
//    cuts3 = hash( chdata3, "cuts");
//    //echo( cuts3= arrprn(cuts3) );
//    pts3 = joinarr(cuts3);
//    //MarkPts( pts3, "ch;ball=0;color=black");
//    //for(cut=cuts3) MarkPts(cut,"ch;ball=0");
//    //for(cut=cuts3) MarkPts(cut,"pl=[trp,0.1];ball=0");
//        
//    faces3 = faces(shape="cone", nside= len(get(cuts3,1))
//                            , nseg= len(cuts3)-1);
//    
//    //echo(faces = faces );
//    color("green", 0.9)
//    polyhedron(points = pts3, faces=faces3 );
    
    
}
//simpleChainData_demo_r();

module simpleChainData_demo_basecut(){
    echom("simpleChainData_demo_basecut");
    
    //n= 3;
    chbone = chainBoneData( seed=pqr(), seglen=8
             , n=5, a=[100,160],a2=0);
    bone0 = hash(chbone,"pts"); //smoothPts( hash(chbone,"pts"), n=4 );
    //MarkPts(bone0, "ch");
    arrow_r = 1;
    r = 0.4;
    
    //-------------------------------------------------
    bone1= smoothPts(bone0,n=2);

//    echo(bone1=bone1);
//    MarkPts(bone1,"ch=[r,0.05,color,blue]");
//    //echo(rs1=rs1);
    basecut1 = [ [0,0], [-2,0],[-1,1],[-1.5,2],[-0.8,1.5]
               , [-0.7,2],[-0.5,2.35],[-0.25,2.5],[-0,2.6]
                ];
    chdata = simpleChainData( bone1, basecut=basecut1, shape="chain" );
    //echo(chdata=chdata);
    //cuts = hash( chdata, "cuts");
    //echo( cuts= arrprn(cuts) );
    //pts = joinarr(cuts);
    //MarkPts( pts, "ch;ball=0;color=black");
    //for(cut=cuts) MarkPts(cut,"ch;ball=0");
    //for(cut=cuts) MarkPts(cut,"pl=[trp,0.1];ball=0");

    SimpleChain( chdata );
//    faces = faces(shape="chain", nside= len(basecut1)
//                            , nseg= len(bone1)-1);
//    
//    //echo(faces = faces );
//    color(undef, 1)
//    polyhedron(points = pts, faces=faces );

    //-------------------------------------------------
    bone2= trslN(bone1,10);
    //MarkPts(bone2,"ch=[r,0.05,color,blue];ball=0");
    basecut2 = basecut1;
    chdata2 = simpleChainData( bone2, basecut=basecut2
    , closed=true );
    SimpleChain( chdata2, color="darkcyan", shape="ring" );
    //echo(chdata2=chdata2);
//    cuts2 = hash( chdata2, "cuts");
//    //echo( cuts= arrprn(cuts) );
//    pts2 = joinarr(cuts2);
//    //MarkPts( pts, "ch;ball=0;color=black");
//    //for(cut=cuts) MarkPts(cut,"ch;ball=0");
//    //for(cut=cuts) MarkPts(cut,"pl=[trp,0.1];ball=0");
//        
//    faces2 = faces(shape="ring", nside= len(basecut2)
//                            , nseg= len(bone2)-1);
//    
//    //echo(faces = faces2 );
//    color("darkcyan", 1)
//    polyhedron(points = pts2, faces=faces2 );
    
    
    
}
//simpleChainData_demo_basecut();


