include <scadx.scad>
include <woodworking/scadx_woodworking.scad>

//
// _sandbox.scad
//
// Template to try out scadx 
//

Cube(actions=["scale", [1.5,1,1]]
    , color=["gray",0.5]
    , MarkPts=["Label",["scale",2, "color","red"]]);