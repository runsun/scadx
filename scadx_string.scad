
module __echo(x, cmt="") // All ' in x will be replaced by "
{  
   if(cmt)
     echo( str(replace(x, "'", "\""), _color(replace(cmt, "'", "\""), "darkgreen")));
   else
     echo(replace(x, "'", "\""));
}

module _echo(s,arr, cmt="")
{
   __echo( _s(s,arr), cmt);
}

module echoblock(arr, indent="  ", v="| ", t="-", b="-", lt=".", lb="'")
{
	lines = arrblock(arr, indent=indent, v=v,t=t,b=b, lt=lt, lb=lb);

	_Div( join(lines,"<br/>")
		, "font-family:Droid Sans Mono"
		);  
}

    echoblock=[ "echoblock", "arr", "", "Doc, Console" , 
    " Given an array of strings, echo them as a block. Arguments:
    ;;
    ;;   indent: prefix each line
    ;;   v : vertical left edge, default '| '
    ;;   h : horizontal line on top and bottom, default '-'
    ;;   lt: left-top symbol, default '.'
    ;;   lb: left-bottom symbol, default \"'\"
    ;;
    ;; See arrblock()
    "];

    function echoblock_test( mode=MODE, opt=[] )=
    (
        doctest( echoblock, mode=mode, opt=opt )
    );
 
module echome(){                  // echo name of the module this module is in. 2019.8.21
  echo(_span(_b( str("=== ",parent_module(),"() ===")
    ),"color:blue;font-size:12px"));   
}

module echopts(pts,label="pts", d=2) // 2018.5.13
{ 
  pw = pow(10,d);
  //pts = [ for(p=pts) replace( str([ for(n=p) round(n*pw)/pw]), ", ",",") ];
  pts = [ for(i=range(pts)) 
          replace( str([ for(n=pts[i]) round(n*pw)/pw]
                 , (i+1)%4==0?"           \n":""), ", ",",") 
        ];
  echo( str(label, "=[ ", join(pts,", "), "];") ); 
} 

//========================================================
function asc(c, n = 0) = 
( // from nophead
  // http://forum.openscad.org/How-to-detect-chr-0-td12351.html#a12418
  c == chr(n) ? n : asc(c, n + 1)
);

    asc=["asc","c,n=0","int","core,str",
    "Reversal of chr()"
    ];

    function asc_test(mode=MODE, opt=[] )=
    (
        doctest( asc, 
        [
        ]
        , mode=mode, opt=opt )
    );
    
//=========================================
function _colorPts(pts)=
(
    let( c=[0,.3], bc=[0.85,1]
       )

        str("["
           , join( [for( a = pts )
                      let(cl=  randc(bc,bc,bc) )    
                      _color(str(
                          "["
                          , join([ for( b= a ) 
                                    _bcolor(b, cl)
                                 ],",")
                          ,"]"
                      ), randc(c,c,c))    
                   ], ","
                 )
          , "]"
          )
);
                  
//========================================================

module echo_(s, arr=undef)
{
	if(arr==undef){ echo(s); }
	else if (isarr(arr)){
		echo( _s(s, arr));
	}else { echo( str(s, arr) ); }
}

    echo_=["echo_", "s,arr=undef", "", "Console",
    " Given a string containing one or more '{_}' as blanks to fill,
    ;; and an array (arr) as the data to fill, echo a new s with all
    ;; blanks replaced by arr items one by one. If the arr is given as 
    ;; a string, it is the same as echo( str(s,arr) ).
    ;;
    ;; Example usage:
    ;;
    ;; echo_( '{_}.scad, version={_}', ['scadex', '20140531-1'] );
    "];

    function echo__test( mode=MODE, opt=[] )=
    (
        doctest( echo_, mode=mode, opt=opt )
    );


//========================================================

// echoblock(arr, indent="  ", v="| ", t="-", b="-", lt=".", lb="'")
//module echoblock(arr, indent="  ", v="| ", t='-', b="-", lt=".", lb="'")

//========================================================

module echoh(s, h, showkey) 
{  echo( _h(s, h, showkey=showkey));
}

    echoh=["echoh","s,h,showkey","", "Hash, Console",
    " Given a string template (s) containing '{key}' where key is a key of
    ;; a hash, and a hash (h), return a new string in which all keys in {??}
    ;; are replaced by their corresponding values in h. If showkey is true, 
    ;; replaced by key=val.
    "]; 


    function echoh_test( mode=MODE, opt=[] )=
    (
        doctest( echoh, mode=mode, opt=opt )
    );

//========================================================

module echofh(h, pairbreak=" , "
			  , keywrap="<b>,</b>"
			  , valwrap="<i>,</i>"
              , eq = "="
              , iskeyfmt= false
              ) 
{  echo( _fmth(h, pairbreak=pairbreak
			  , keywrap=keywrap
			  , valwrap=valwrap
              , eq = eq
              , iskeyfmt= iskeyfmt
    ));
}

    echofh=["echofh","h","", "Hash, Console",
    " Given a string template (s) containing '{key}' where key is a key of
    ;; a hash, and a hash (h), return a new string in which all keys in {??}
    ;; are replaced by their corresponding values in h. If showkey is true, 
    ;; replaced by key=val.
    "]; 

    function echofh_test( mode=MODE, opt=[] )=
    (
        doctest( echofh, mode=mode, opt=opt )
    );


//========================================================

module echom(mname, remark)
{
    //indent = isint(mti)?repeat("&nbsp;",2*mti):"";
    
//    echo( str( "<span style='color:navy'><code>"
//             , indent //mti==undef? "": repeat("&nbsp;",2*mti)
//             //, "", mname, " ---------------------</code></span>"
//             , "&lt;", mname, "&gt;:"
//             , isint(mti)?mti:"", "---------------------</code></span>"
//             ) ); 
//                    
//    if(remark)  echo( _blue(remark));
//        
    echo( 
       _code( 
             str( "&lt;", mname, "&gt;:----------"
                , remark?"<br>:: ":""
                , remark?remark:""
                )
              , s="color:navy" 
              ) 
        );
}


//echom_demo();
//accum_test(["mode",2]);
    
      
function _esc(s, target="<")=
(
  let( _s= has(target," ")? replace(str(s), " ", "&nbsp;"):str(s) )
  has(target,"<")? replace(_s, "<", "&lt;"): s
);      
      
//========================================================

function _fmth(h
              , wrap=["",""]
              , pairbreak="\n|"
              , keywrap= ["<b>","</b>"]
              , valwrap= ["<i>","</i>"]
//              , pairwrap=","
//              , eq = "="
//              , iskeyfmt= false
//              // New 2015.3.16: if key is sectionhead or comment, convert pair
//              // to section head and comment, respectively
//              , sectionhead= "###"
//              , comment= "///"
              , _i=0
              , _rtn=""
              
			  )=
(  
    let( 
         newrtn = str( _rtn
                     //, pairbreak
                     //, "<b>", h[_i],"</b>="
                     //, keywrap
                     , keywrap[0], h[_i], keywrap[1]
                     , "="
                     
                           , _fmt( h[_i+1])
//                                 , wrap
//                                 , pairbreak
//                                 , keywrap
//                                 , valwrap
                                 , _i<len(h)-2?", ":"")
       , newi = _i+2                    
       )
    _i>len(h)-2? str("[", _rtn,"]")
    : _fmth( h
           , wrap
           , pairbreak
           , keywrap
           , valwrap
           , _i=newi, _rtn=newrtn)    

);     
      
    _fmth=["_fmth" ,"h,pairbreak=' , ',keywrap='<b>,</b>',valwrap='<u>,</u>', pairwrap=',',eq='=',iskeyfmt=false,sectionhead='####',comment='////'" 
         ,"string","String, Doc"
    ,"Return a formatted hash. Usually used with echo().
    ;;
    ;;  keywrap,valwrap,pairwrap: strs wrap around each key,val or pair,
    ;;     respectively.
    ;;  eq: symbol between key and val
    ;;  iskeyfmt: format key if true
    ;;  sectionhead, comment: if key is a sectionhead or comment, convert
    ;;     the pair to section head or comment, respectively
    ;;
    ;; The return is a single line of string. Setting pairbreak to &lt;br/&gt;
    ;; will break the line to lines of k-v pairs. 
    ;;
    "];

    function _fmth_test( mode=MODE, opt=[] )=
    (
      let( h = ["num",23,"str","test", "arr", [2,"a"] ]
         , h2= ["pqr", [2,3,4]]
         , h3= ["pqr", [[0,1,2],undef] ]
         )

      doctest(_fmth,
      [
       "var h"
        ,str("_fmth(h): ", _fmth(h))
        , "var h2"
      ,str("_fmth(h2): ", _fmth(h2))
        , "var h3"
      ,str("_fmth(h3): ", _fmth(h3))
        ,str("_fmth(h3,iskeyfmt=true): ", _fmth(h3, iskeyfmt=true))
        ,str("_fmth(h,pairbreak='&lt;br/>'): ", str("<br/>",_fmth(h,pairbreak="<br/>")))
    //	,[ "", _fmth(h,pairbreak="<br/>")
    //     ,"h,pairbreak='&lt;br/>'", ["notest", true, "nl",true]
    //          ]
        
    //	  ["['a',23]", _fmth(["a",23]), "[\"a\",23]" ]
    //	, ["h", _fmth(h ),""]
    //	, ["h2", _fmth( h2),""]
    //	, ["h3", _fmth( h3),""]

      ],  mode=mode, opt=opt, ,scope=["h",h, "h2",h2,"h3",h3]
      )
    );

//========================================================
function _f(x, fmt, opt=[])= 
     // New formatting func using hash as style. The original 
     // _f's design makes it hard to combine/update multiple 
     // formattings   
     //-- 2018.11.7
(  
 //echo("---------_f():",fmt=fmt)
 let( _fmt = subprop( isstr(fmt)?sopt(fmt):fmt
                    , opt, "fmt"
                    , dfsub=[] 
                    )
     //, pad= arg(pad, opt, "pad", " ")
     
     // Set %
     , pctg = has(_fmt,"%")
     , sn = num(x) * ( pctg?100:1 )  // 
     , isnum= isnum(x)
     , conv = hash(_fmt,"x")
       
     // Set d (decimal point) and unit conversion
     , _s = isnum? numstr( sn
                     , d= num( hash( _fmt, "d") )
                     , conv= pctg?undef: replace(conv,">",":") // disable uconv
                     )                       // if pctg
            : x
       
     // width
     , _w = hash( _fmt, "w" )
     , _wn = num(_w)
     , w= _w? (isnum(_wn)? _wn
                          : asc(_w)-87)
            : undef
     
     , al = h( _fmt, "al","r")
     , _pad= hash( opt, "p", h(_fmt, "p", " " )) // padding
     , dw = w-len(_s)-(pctg?1:0)                 // diff of w and len(s)
     
     ///------------------------------------------------
     // Padding - Note that a micro-adjustment is made using (dw%2&&x<0?1:0)
     // to turn  
     //            "__-3.48_" (L:R = 3:2) 
     //  to 
     //            "_-3.48__" (L:R = 2:3)
     //
     // [2018.11.27] --- changed to dw%2?(dw/2-(x<0?-.5:.5)): dw/2
     // 
     , spad= dw<0? str(slice( _s,0,w-(pctg?1:0)), pctg?"%":"")
          : dw>0? 
             str( al=="r"? repeat( _pad, dw)
                  :al=="c"? repeat( _pad, dw%2?(dw/2+(x<0?-.5:.5))
                                              : dw/2
                                  )
                  :""          
                , _s
                , pctg?"%":""
                ,  al=="l"? repeat( _pad, dw)
                  :al=="c"? repeat( _pad, dw%2?(dw/2-(x<0?-.5:.5))
                                              : dw/2
                                  )
                  :""
                )
          : str( _s, pctg?"%":"")
          
           
     , wrap= hash( _fmt, "wr")
     , sw= wrap? replace( wrap,",",spad):spad
       
     , srtn= sw
     , stya= hash( _fmt, "aw")
     , styn= hash( _fmt, "nw")
     , stys= hash( _fmt, "sw")
     , rtn = styn && isnum? replace(styn,",",srtn)
             : stys && isstr(x) ?replace(stys,",",srtn)
             : stya && isarr(x) ?replace(stya,",",srtn):srtn
     )
// echo(_s=_s, len_s=len(_s), w=w, dw=dw
//     , nPad=[dw%2?(dw/2+(x<0?-.5:.5))
//                                              : dw/2
//            ,dw%2?(dw/2-(x<0?-.5:.5))
//                                              : dw/2
//            ]
//     , spad = spad)    
 //echo( isnum=isnum, _s=_s, spad = spad )
 //echo(fmt=fmt, _fmt=_fmt)
 //echo(ss= replace(ss,"<","&lt;"))
 //echo(_fmt=_fmt, stya=stya, styn=styn, stys=stys, stys= replace(stys,"<","&lt;"), rtn= replace(rtn,"<","&lt;"))
 //echo(ss= ss)
 isarr(x)&& haskey(_fmt,"ar")?
        let( _fmt2= delkey(_fmt, "ar") )
   	[ for(item=x) _f( item,fmt= _fmt2)  ]
 :rtn 
);



    _f=["_f", "x,fmt,opt=[]' '", "str", "String, Doc, Console" 
    ,"Format x with format fmt. Note that it handles non-style formatting, and 
    ;; _hl() handles both non-style (by calling _f) and style formatting. 
    ;; _f and _hl have same arg signature.
    ;;
    ;; fmt could be a str (to be coverted to hash by sopt()) or a hash.
    ;; If a str, multiple formats are separated with ';' 
    ;; 
    ;;   _ff(s, 'w=10;d=2')     ==> fmt in str       
    ;;   _ff(s, ['w',10,'d',2] )  ==> fmt in hash
    ;; 
    ;; Boolean-format:
    ;;
    ;;    %: percentage  
    ;;    ar:  applied to items of array, return arr ===> NOTE THIS --
    ;;
    ;; Value-format:
    ;;
    ;;    w:  w=9 set width=9. 
    ;;    d:  d=2  decimal(precision) position=2  
    ;;    al: al=l,c,r align left, center, or right 
    ;;    p:  p=x pads the spaces with x to fulfill width.  
    ;;    aj: aj=',' jointer for array items  
    ;;    x:  x=a>b convert x from unit a to unit b   
    ;;        (see uconv(), numstr() and num())
    ;;
    ;; Conditional-wrapping:
    ;; 
    ;;    wr: w={,} wraps with { and }
    ;;    aw: aw={,} wrap with { and } if x is array 
    ;;    sw: sw={,} wrap with { and } if x is str
    ;;    nw: nw={,} wrap with { and } if x is number
    
    "
    ];

    function _f_test( mode=MODE, opt=[] )=
    (
        doctest( _f,
                [
          "// Integer: "
        , [ _f(5), "5", "5"]
        , [ _f("5"), "5", "'5'"]
        
        ,""
        ,"//Flags: <b>b,i,u,^,v,%, ar</b>"
        ,""
        
        , [ _f(0.5,"%"), "50%", "0.5,'%'", ["//","%"]]
        , [ _f(1.4,"%"), "140%", "1.4,'%'", ["//","%"]]
        , [ _f(.5,"%;d=2"), "50.00%", ".5,'%;d=2'", ["//","% and d"]]
         ,""
        , "// flag 'ar' to apply the fmt to each item of an array "
        , "//  _f( [2,3,4], 'ar;d=1') = '[2.0, 3.0, 4.0]'" 
        , ""
        , [ _f( [2,3,4], "ar;d=1"), ["2.0", "3.0", "4.0"]
             , "[2,3,4], 'ar:d=1'"
         ]     
        ,[ _f( [2,3,4], "ar;w=5;al=c;wr={,}"), ["{  2  }", "{  3  }", "{  4  }"]
             , "[2,3,4], 'ar;w=5;al=c;wr={,}'"
         ]     
        ,[ _f( [0.2,0.3,0.4], "ar;%"), ["20%", "30%", "40%"]
             , "[0.2,0.3,0.4], 'ar;%'"
         ]         
        ,""
        ,"// Use either a str or a hash:"
        ,""
         , [ _f(0.3,["%",true]), "30%", "0.3,['%',true]"]
         , [ _f(0.3,["w",5, "p","&npsp;"])
               , "&npsp;&npsp;0.3", "0.3,[\"w\",5, \"p\",\"&npsp;\"]"
               , ["//", "Use npsp here but not nbsp to show it's effect"]
               ]
        
        ,""
        ,"//<b>w,d</b>: width and decimal" 
        ,""    
        , [ _f(3.8, ["d",2]), "3.80", "3.8, ['d',2]"]
        , [ _f(3, "d=2"), "3.00", "3, 'd=2'"]
        //, [ _f(1, ["w",6,"d",2,"al","c"] ), " 1.00 ", "1, ['w',6,'d',2,'al','c']"]
        //, [ _f(1, "w=6;d=2;al=c" ), " 1.00 ", "1, 'w=6;d=2;al=c'"]
        , [ _f(3.839, "d=2"), "3.84", "3.839, 'd=2'"]
        , [ _f(3.839, "d=0"), "4", "3.839, 'd=0'"]
        , [ _f(3.839, "w=8"), "   3.839", "3.839, 'w=8'"]
        , [ _f(-3.839,"w=8"), "  -3.839", "-3.839, 'w=8'"]
        , [ _f(3.839, "w=8;d=2"), "    3.84", "3.839,'w=8;d=2'"]
        , [ _f(3.839, "w=8;al=l" ), "3.839   ", "3.839,'w=8;al=l'"]

        , [ _f(3.839, "w=8;al=c"), "  3.839 ", "3.839,'w=8;al=c'"]
        , [ _f(3.839, "w=8;d=2;al=c"), "  3.84  ", "3.839,'w=8;d=2;al=c' "]
        , [ _f(-3.839,"w=8;d=2;al=c"), " -3.84  ", "-3.839,'w=8;d=2;al=c'"
              ,["//", "Note that if x<0 and len(pads) is odd, it tries to balance both sides"]
              ]
        , [ _f(3.839, "w=4"), "3.83", "3.839,'w=4'"
             ,["//","w too small, get cut"]
          ]
        , [ _f(3.839, ["w",8,"al","c","p","#"]), "##3.839#", "3.839,['w',8,'al','c','p','#']"]
        , [ _f("abcdefghij", "w=12"), "  abcdefghij", "'abcdefghij','w=12'"
          ]
        , [ _f("3.8","d=2"), "3.8", "'3.8','d=2'"
                , ["//", "Can't format decimal on str"]]
        , [ _f("3.8","w=6"), "   3.8", "'3.8','w=6'"]
        , [ _f("3.8","w=6;d=2"), "   3.8", "'3.8','w=6;d=2'"]
        , [ _f(0.837,"d=2"), "0.84", "'3.8','d=2'"]
        , [ _f(0.837,"%;d=2"), "83.70%", "'3.8','%;d=2'"]
          
        ,""
        ,"// <b>al</b>: l,c,r  --- align " 
        ,""    
        , [ _f(1.4685,"%;d=1"), "146.9%", "1.4685,'%;d=1'", ["//","% & d2"]]
        , [ _f(0.4685,"%;d=0;w=9;p=-;al=c"), "---47%---", "0.4685,'%;d=0;w=9;p=-;al=c'", ["//","%"]]
        , [ _f(1.685,"%;w=9;al=l"), "168.5%   ", "1.685,'%;w=9;al=l'"]
        , [ _f(1.685,"%;w=9;al=c"), "  168.5% ", "1.685,'%;w=9;al=c'"]
        , [ _f(1.685,"%;w=6"), "168.5%", "1.685,'%;w=6'"]
        , [ _f(1.685,"%;w=4"), "168%", "1.685,'%;w=4'"]
        
        ,""
        , "// <b>x</b>: unit conversion"
        ,""
        , [ _f(96, "x=in>ftin"), "8'", "96, 'x=in>ftin'", ["//","Unit convert"] ]
        , [ _f(100, "x=in>ftin"), "8'4\"", "100, 'x=in>ftin'", ["//","Unit convert"] ]
        , [ _f(3.5, "x=ft>ftin"), "3'6\"", "3.5, 'x=ft>ftin'" ]
        , [ _f(3.5, "x=ft>in"), "42", "3.5, 'x=ft>in'" ]
        , [ _f(1, "x=in>cm"), "2.54", "1, 'x=in>cm'" ]
        , [ _f(96, "wr={,};x=in>ftin"), "{8'}"
                    , "96, 'wr={,};x=in>ftin'" ]
        ,""
        , [ _f(1.4685, "%;w=11;d=2;al=c;p=x;wr={,}")
            ,"{xx146.85%xx}", "1.4685, '%;w=11;d=2;al=c;p=x;wr={,}'" ]
                
        ,""
        ,"// <b>wr</b>: wrap; <b>aw,nw, sw</b>: wrap when array, num or str, resp.:"
        ,""
        , [ _f(30, "wr={,}"), "{30}", "30, 'wr={,}'", ["//","wrap"]]
        , [ _f(30, "aw={,}"), "30", "30, 'aw={,}'", ["//","wrap if arr"] ]
        , [ _f([2,3], "aw={,}"), "{[2, 3]}", "[2,3], 'aw={,}'", ["//","wrap if arr"] ]
        , [ _f(30, "nw=/,/"), "/30/", "30, 'nw=|,|'",["//","wrap if num"] ]
        , [ _f([2,3], "aw={,}"), "{[2, 3]}", "[2,3], 'aw={,}'" ]
        , [ _f([2,3], "sw={,}"), "[2, 3]", "[2,3], 'sw={,}'" ]
        , [ _f("[2,3]", "sw={,}"), "{[2,3]}", "'[2,3]', 'sw={,}'" ]
 
        ,""
        ,"// <b>ar</b>: fmt applied to items of array; return an arr"
        ,""
        , [ _f( [2,3,4], "wr=_,_"), "_[2, 3, 4]_", "[2,3,4], 'wr=_,_'"]
        , [ _f( [2,3,4], "ar;wr=_,_"), ["_2_", "_3_", "_4_"], "[2,3,4], 'ar;wr=_,_'"]
        //, [ _f( [2,3,4], "ar;aj=|;wr=_,_"), "[_2_|_3_|_4_]", "[2,3,4], 'ar;aj=|;wr=_,_'"]
        , [ _f( [2,3,4], "aw=_,_"), "_[2, 3, 4]_", "[2,3,4], 'aw=_,_'"]
        , [ _f( [2,3,4], "ar;aw=_,_"), ["2", "3", "4"], "[2,3,4], 'ar;aw=_,_'"]
        , [ _f( [2,[3],4], "ar;aw=_,_"), ["2", "_[3]_", "4"], "[2,[3],4], 'ar;aw=_,_'"]
        , [ _f( [2,[3],"4"], "ar;nw=#,#;sw=`,`;aw=_,_")
            , ["#2#", "_[3]_", "`4`"], "[2,[3],4], 'ar;nw=#,#;sw=`,`;aw=_,_'"]

                           
        ,""  
        , "// The following are hard to test, but we can show it:"
          , str( "_f(", _arg("30, 's=color:red'"), ") = ",
               _f(30, "s=color:red")
               ) 
          , str( "_f(", _arg("30, 's=font-size:20px'"), ") = ",
               _f(30, "s=font-size:20px")
               ) 
          , str( "_f(", _arg("30, 'b;fs=20'"), ") = ",
               _f(30, "b;fs=20;c=blue ")
               ) 
        , str( "_f(",_arg("96, 'w=6;al=c;wr=<code>{,}</code>;c=cyan;bc=black', pad='&nbsp;'"),") = "
            , _f(96, "w=6;al=c;wr=<code>{,}</code>;c=cyan;bc=black", pad="&nbsp;") )
   
   
        ]//doctest  
        , mode=mode, opt=opt
        )
    );
//} // _f

//echo( replace( _f(30, "s=color:red"), "<", "&lt;") );

//
//function _ff(x, fmt, opt=[])= 
//     // New formatting func using hash as style. The original 
//     // _f's design makes it hard to combine/update multiple 
//     // formattings   
//     //-- 2018.11.7
//(  
// //echo(fmt=fmt)
// let( _fmt = subprop( isstr(fmt)?sopt(fmt):fmt
//                    , opt, "fmt"
//                    , dfsub=[] 
//                    )
//     //, pad= arg(pad, opt, "pad", " ")
//     
//     // Set %
//     , pctg = has(_fmt,"%")
//     , sn = num(x) * ( pctg?100:1 )  // 
//     , isnum= isnum(x)
//     , conv = hash(_fmt,"x")
//       
//     // Set d (decimal point) and unit conversion
//     , _s = isnum? numstr( sn
//                     , d= num( hash( _fmt, "d") )
//                     , conv= pctg?undef: replace(conv,">",":") // disable uconv
//                     )                       // if pctg
//            : x
//       
//     // width
//     , _w = hash( _fmt, "w" )
//     , _wn = num(_w)
//     , w= _w? (isnum(_wn)? _wn
//                          : asc(_w)-87)
//            : undef
//     
//     , al = h( _fmt, "al","r")
//     , _pad= hash( _fmt, "p", " " ) // padding
//     , dw = w-len(_s)-(pctg?1:0)
//     , spad= dw<0? str(slice( _s,0,w-(pctg?1:0)), pctg?"%":"")
//          : dw>0? 
//             str( al=="r"? repeat( _pad, dw)
//                  :al=="c"? repeat( _pad, dw/2):""         
//                , _s
//                , pctg?"%":""
//                ,  al=="l"? repeat( _pad, dw)
//                  :al=="c"? repeat( _pad, dw/2-(dw%2?1:0)):""              )
//          : str( _s, pctg?"%":"")
//     
//     , sb= has(_fmt,"b")?_b(spad):spad//ctg
//     , su= has(_fmt,"u")?_u(sb):sb
//     , si= has(_fmt,"i")?_i(su):su
//     , s_sup= has(_fmt,"^")? str("<sup>",si,"</sup>")
//              :has(_fmt,"v")? str("<sub>",si,"</sub>"):si
//                
//     , wrap= hash( _fmt, "wr")
//     , sw= wrap? replace( wrap,",",s_sup):s_sup
//       
//     , color= hash( _fmt, "c")
//     , sc= color? _color(sw,color): sw 
//       
//     , bc= hash( _fmt, "bc")
//     , sbc= bc? _bcolor(sc,bc): sc 
//       
//     , fs = hash( _fmt, "fs")
//     , sf = fs? _span( sbc, s=str("font-size:",fs,"px;") ): sbc
//     , sty = hash( _fmt, "s")
//     , ss  = sty? _span( sbc, s=sty): sf //sbc
//        
//     , srtn= ss //sbc //ss //bc //sf
//     , stya= hash( _fmt, "aw")
//     , styn= hash( _fmt, "nw")
//     , stys= hash( _fmt, "sw")
//     , rtn = styn && isnum? replace(styn,",",srtn)
//             : stys && isstr(x) ?replace(stys,",",srtn)
//             : stya && isarr(x) ?replace(stya,",",srtn):srtn
//     )
// //echo( isnum=isnum, _s=_s, spad = spad )
// //echo(fmt=fmt, _fmt=_fmt)
// //echo(ss= replace(ss,"<","&lt;"))
// //echo(_fmt=_fmt, stya=stya, styn=styn, stys=stys, stys= replace(stys,"<","&lt;"), rtn= replace(rtn,"<","&lt;"))
// //echo(ss= ss)
// isarr(x)&& haskey(_fmt,"ar")?
//        let( _fmt2= delkey(_fmt, "ar") )
//   	[ for(item=x) _ff( item,fmt= _fmt2)  ]
// :rtn 
//);
///*
//*/    
//
//    _ff=["_ff", "x,fmt,opt=[]", "str", "String, Doc, Console" 
//    ,"Format x with format fmt.
//    ;;
//    ;; fmt could be a str (to be coverted to hash by sopt()) or a hash.
//    ;; If a str, multiple formats are separated with ';' 
//    ;; 
//    ;;   _ff(s, 'b;d=2')     ==> fmt in str       
//    ;;   _ff(s, ['b',true, 'd',2] )  ==> fmt in hash
//    ;; 
//    ;; Boolean-format:
//    ;;
//    ;;    b: boldface 
//    ;;    i: italic
//    ;;    u: underscore 
//    ;;    %: percentage  --
//    ;;    v: subscript
//    ;;    ^: superscript
//    ;;    ar:  applied to items of array, return arr ===> NOTE THIS --
//    ;;
//    ;; Value-format:
//    ;;
//    ;;    w:  w=9 set width=9. --
//    ;;    d:  d=2  decimal(precision) position=2  --
//    ;;    al: al=l,c,r align left, center, or right --
//    ;;    p:  p=x pads the spaces with x to fulfill width.  --
//    ;;    aj: aj=',' jointer for array items  --
//    ;; 
//    ;;    c:  c=red make it red 
//    ;;    bc: bc=red paints the background red
//    ;;    fs: fs=20 sets font-size to 20px
//    ;; 
//    ;;    x:  x=a>b convert x from unit a to unit b  -- 
//    ;;        (see uconv(), numstr() and num())
//    ;;
//    ;; Conditional-wrapping:
//    ;; 
//    ;;    wr: w={,} wraps with { and }
//    ;;    aw: aw={,} wrap with { and } if x is array 
//    ;;    sw: sw={,} wrap with { and } if x is str
//    ;;    nw: nw={,} wrap with { and } if x is number
//    ;;
//    ;; Style-format:
//    ;; 
//    ;;    s: 's=font-size:20px' sets font size.
//    ;;
//    "
//    ];
//
//    function _ff_test( mode=MODE, opt=[] )=
//    (
//        doctest( _ff,
//        [
//          "// Integer: "
//        , [ _ff(5), "5", "5"]
//        , [ _ff("5"), "5", "'5'"]
//        
//        ,""
//        ,"//Flags: <b>b,i,u,^,v,%, ar</b>"
//        ,""
//        , [ _ff(30,"b"), "<b>30</b>", "30,'b'", ["//","b"]]
//        , [ _ff(30,"i"), "<i>30</i>", "30,'i'", ["//","i"]]
//        , [ _ff(30,"u"), "<u>30</u>", "30,'u'", ["//","u"]]
//        , [ _ff(30,"u;i"), "<i><u>30</u></i>", "30,'u;i'", ["//","ui"]]
//        , [ _ff(30,"^"), "<sup>30</sup>", "30,'^'"
//                      , ["//","^: superscript"]]
//        , [ _ff(30,"v"), "<sub>30</sub>", "30,'v'"
//                      , ["//","v: subscript"]]
//        , [ _ff(0.5,"%"), "50%", "0.5,'%'", ["//","%"]]
//        , [ _ff(1.4,"%"), "140%", "1.4,'%'", ["//","%"]]
//        , [ _ff(.5,"%;d=2"), "50.00%", ".5,'%;d=2'", ["//","% and d"]]
//        , [ _ff(0.3,"%;b;u"), "<u><b>30%</b></u>", "0.3,'%;b;u'", ["//","multiple "]]
//         ,""
//        , "// flag 'ar' to apply the fmt to each item of an array "
//        , "//  _ff( [2,3,4], 'ar;d=1') = '[2.0, 3.0, 4.0]'" 
//        , ""
//        , [ _ff( [2,3,4], "ar;d=1"), ["2.0", "3.0", "4.0"]
//             , "[2,3,4], 'ar:d=1'"
//         ]     
//        ,[ _ff( [2,3,4], "ar;w=5;al=c;wr={,}"), ["{  2  }", "{  3  }", "{  4  }"]
//             , "[2,3,4], 'ar;w=5;al=c;wr={,}'"
//         ]     
//        ,[ _ff( [0.2,0.3,0.4], "ar;%"), ["20%", "30%", "40%"]
//             , "[0.2,0.3,0.4], 'ar;%'"
//         ]         
//        ,""
//        ,"// Use either a str or a hash:"
//        ,""
//         , [ _ff(0.3,["%",true,"b",true,"u",true]), "<u><b>30%</b></u>"
//                     , "0.3,['%',true,'b',true,'u',true]"]
//        
//        ,""
//        ,"//<b>w,d</b>: width and decimal" 
//        ,""    
//        , [ _ff(3.8, ["d",2]), "3.80", "3.8, ['d',2]"]
//        , [ _ff(3, "d=2"), "3.00", "3, 'd=2'"]
//        //, [ _ff(1, ["w",6,"d",2,"al","c"] ), " 1.00 ", "1, ['w',6,'d',2,'al','c']"]
//        //, [ _ff(1, "w=6;d=2;al=c" ), " 1.00 ", "1, 'w=6;d=2;al=c'"]
//        , [ _ff(3.839, "d=2"), "3.84", "3.839, 'd=2'"]
//        , [ _ff(3.839, "d=0"), "4", "3.839, 'd=0'"]
//        , [ _ff(3.839, "w=8"), "   3.839", "3.839, 'w=8'"]
//        , [ _ff(-3.839,"w=8"), "  -3.839", "-3.839, 'w=8'"]
//        , [ _ff(3.839, "w=8;d=2"), "    3.84", "3.839,'w=8;d=2'"]
//        , [ _ff(3.839, "w=8;al=l" ), "3.839   ", "3.839,'w=8;al=l'"]
//
//        , [ _ff(3.839, "w=8;al=c"), "  3.839 ", "3.839,'w=8;al=c'"]
//        , [ _ff(3.839, "w=8;d=2;al=c"), "  3.84  ", "3.839,'w=8;d=2;al=c'"]
//        , [ _ff(-3.839,"w=8;d=2;al=c"), "  -3.84 ", "-3.839,'w=8;d=2;al=c'"]
//        , [ _ff(3.839, "w=4"), "3.83", "3.839,'w=4'"
//             ,["//","w too small, get cut"]
//          ]
//        , [ _ff(3.839, ["w",8,"al","c","p","#"]), "##3.839#", "3.839,['w',8,'al','c','p','#']"]
//        , [ _ff("abcdefghij", "w=12"), "  abcdefghij", "'abcdefghij','w=12'"
//          ]
//        , [ _ff("3.8","d=2"), "3.8", "'3.8','d=2'"
//                , ["//", "Can't format decimal on str"]]
//        , [ _ff("3.8","w=6"), "   3.8", "'3.8','w=6'"]
//        , [ _ff("3.8","w=6;d=2"), "   3.8", "'3.8','w=6;d=2'"]
//        , [ _ff(0.837,"d=2"), "0.84", "'3.8','d=2'"]
//        , [ _ff(0.837,"%;d=2"), "83.70%", "'3.8','%;d=2'"]
//          
//        ,""
//        ,"// <b>al</b>: l,c,r  --- align " 
//        ,""    
//        , [ _ff(1.4685,"%;d=1"), "146.9%", "1.4685,'%;d=1'", ["//","% & d2"]]
//        , [ _ff(0.4685,"b;%;d=0;w=9;p=-;al=c"), "<b>---47%---</b>", "146.85,'b;%;d=0;w=9;p=-;al=c'", ["//","%"]]
//        , [ _ff(1.685,"%;w=9;al=l"), "168.5%   ", "1.685,'%;w=9;al=l'"]
//        , [ _ff(1.685,"%;w=9;al=c"), "  168.5% ", "1.685,'%;w=9;al=c'"]
//        , [ _ff(1.685,"%;w=6"), "168.5%", "1.685,'%;w=6'"]
//        , [ _ff(1.685,"%;w=4"), "168%", "1.685,'%;w=4'"]
//        
//        ,""
//        , "// <b>x</b>: unit conversion"
//        ,""
//        , [ _ff(96, "x=in>ftin"), "8'", "96, 'x=in>ftin'", ["//","Unit convert"] ]
//        , [ _ff(100, "x=in>ftin"), "8'4\"", "100, 'x=in>ftin'", ["//","Unit convert"] ]
//        , [ _ff(3.5, "x=ft>ftin"), "3'6\"", "3.5, 'x=ft>ftin'" ]
//        , [ _ff(3.5, "x=ft>in"), "42", "3.5, 'x=ft>in'" ]
//        , [ _ff(1, "x=in>cm"), "2.54", "1, 'x=in>cm'" ]
//        , [ _ff(96, "wr={,};x=in>ftin"), "{8'}"
//                    , "96, 'wr={,};x=in>ftin'" ]
//        ,""
//        , [ _ff(1.4685, "%;b;w=11;d=2;al=c;p=x;wr={,}")
//            ,"{<b>xx146.85%xx</b>}", "1.4685, '%;b;w=11;d=2;al=c;p=x;wr={,}'" ]
//                
//        ,""
//        ,"// <b>wr</b>: wrap; <b>aw,nw, sw</b>: wrap when array, num or str, resp.:"
//        ,""
//        , [ _ff(30, "wr={,}"), "{30}", "30, 'wr={,}'", ["//","wrap"]]
//        , [ _ff(30, "aw={,}"), "30", "30, 'aw={,}'", ["//","wrap if arr"] ]
//        , [ _ff([2,3], "aw={,}"), "{[2, 3]}", "[2,3], 'aw={,}'", ["//","wrap if arr"] ]
//        , [ _ff(30, "nw=/,/"), "/30/", "30, 'nw=|,|'",["//","wrap if num"] ]
//        , [ _ff([2,3], "aw={,}"), "{[2, 3]}", "[2,3], 'aw={,}'" ]
//        , [ _ff([2,3], "sw={,}"), "[2, 3]", "[2,3], 'sw={,}'" ]
//        , [ _ff("[2,3]", "sw={,}"), "{[2,3]}", "'[2,3]', 'sw={,}'" ]
// 
//        ,""
//        ,"// <b>ar</b>: fmt applied to items of array; return an arr"
//        ,""
//        , [ _ff( [2,3,4], "wr=_,_"), "_[2, 3, 4]_", "[2,3,4], 'wr=_,_'"]
//        , [ _ff( [2,3,4], "ar;wr=_,_"), ["_2_", "_3_", "_4_"], "[2,3,4], 'ar;wr=_,_'"]
//        //, [ _ff( [2,3,4], "ar;aj=|;wr=_,_"), "[_2_|_3_|_4_]", "[2,3,4], 'ar;aj=|;wr=_,_'"]
//        , [ _ff( [2,3,4], "aw=_,_"), "_[2, 3, 4]_", "[2,3,4], 'aw=_,_'"]
//        , [ _ff( [2,3,4], "ar;aw=_,_"), ["2", "3", "4"], "[2,3,4], 'ar;aw=_,_'"]
//        , [ _ff( [2,[3],4], "ar;aw=_,_"), ["2", "_[3]_", "4"], "[2,[3],4], 'ar;aw=_,_'"]
//        , [ _ff( [2,[3],"4"], "ar;nw=#,#;sw=`,`;aw=_,_"), ["#2#", "_[3]_", "`4`"], "[2,[3],4], 'ar;nw=#,#;sw=`,`;'"]
//
//                           
//        ,""  
//        , "// The following are hard to test, but we can show it:"
//          , str( "_ff(", _arg("30, 's=color:teal'"), ") = ",
//               _ff(30, "s=color:teal")
//               ) 
//          , str( "_ff(", _arg("30, 's=font-size:20px'"), ") = ",
//               _ff(30, "s=font-size:20px")
//               ) 
//          , str( "_ff(", _arg("30, 'b;fs=20'"), ") = ",
//               _ff(30, "b;fs=20;c=blue ")
//               ) 
//        , str( "_ff(",_arg("96, 'w=6;al=c;wr=<code>{,}</code>;c=cyan;bc=black', pad='&nbsp;'"),") = "
//            , _ff(96, "w=6;al=c;wr=<code>{,}</code>;c=cyan;bc=black", pad="&nbsp;") )
//   
//   
//        ]//doctest 
//        , mode=mode, opt=opt
//        )
//    );
// _ff

//========================================================

function _fmt(x
             , s=_span("&quot;,&quot;", "color:purple") 
	     , n=_span(",", "color:brown") //magenta")//blueviolet")//brown")
	     , a=_span(",", "color:navy")//midnightblue")//cadetblue")//mediumblue")//steelblue") //#993300") //#cc0060")  
	     , ud=_span(",", "color:brown") //orange") 
	      //, level=2
	     , fixedspace= true
		   )=
(
	x==undef? replace(ud, ",", x) 
	:isarr(x)? replace(a, ",", replace(str(x),"\"","&quot;")) 
	: isstr(x)? replace(s,",",fixedspace?replace(str(x)," ", "&nbsp;"):str(x)) 
		:isnum(x)||x==true||x==false?replace(n,",",str(x))
				:x
);

    _fmt=["_fmt", "x,[s,n,a,ud,fixedspace]", "string", "String, Doc, Console" 
    ," Given an variable (x), and format settings for string (s), number (n),
    ;; array (a) or undef (ud), return a formatted string of x. If fixedspace
    ;; is set to true, replace spaces with html space. The formatting is
    ;; entered in '(prefix),(suffix)' pattern. For example, a '{,}' converts
    ;; x to '{x}'.
    "
    ];

    function _fmt_test( mode=MODE, opt=[] )=(//====================

      doctest(
       _fmt
      ,[
         str("_fmt(3.45): ", _fmt(3.45))
         , str("_fmt(3.45, n='{,}'): ", _fmt(3.45, n="{,}"))
         ,  str("_fmt(['a',[3,'b'], 4]): ", _fmt(["a",[3,"b"], 4] ))
         ,  str("_fmt(['a',[3,'b'], 4], n='{,}'): ", _fmt(["a",[3,"b"], 4],n="{,}" ))
         ,  str("_fmt(['a',[3,'b'], 4], a='&lt;u>,&lt;/u>'): ", _fmt(["a",[3,"b"], 4],a="<u>,</u>" ))
         ,  str("_fmt('a string with     5 spaces'): ", _fmt("a string with     5 spaces"))			, str("_fmt(false): ", _fmt(false))

      ],  mode=mode, opt=opt
      )
    );


//n = 1.23;
////n = 123456789;
//p = floor(log(n));
//f = n*pow(10,-p); //p>0? n/pow(10, p): n*pow(10,p);
//n0 = floor(f);
//nextn= p<0?( f-n0) 
//       : n - n0*pow(10,p);
//echo(n=n);
//echo( p=p );
//echo(f=f);
//echo(n0 = n0);
//echo(nextn = nextn);

function _formula( data, s="", a=""
                 , w=20 // Need to set this every time, unfortunally
                 )=
(
   _tag("table"
       , _tr( 
           join( 
             [ for(x=data) 
                _td( 
                
                    str("<table style='padding:0px'>"
                        ,"<tr style='padding:0px'>"
                        ,"<td style='padding:0px;align-text:center'>"
                        , replace(x, _BR, "</td></tr><tr><td>")
                        ,"</td></tr></table>"
                        )
                   , a="valign=middle"
                   , s=str("text-align:center;display:block;padding:0px;",s)
                   ) 
             ]
             //, "&nbsp;&nbsp;" 
             )
            )             
       ,s=str("font-family:Courier New;background:#FEF9E7;",s)
       ,a=a //str(a, " width =", w)
       )
); 

module _formula(data, s="", a="border=0", w)//=20)
{
  //echo("In _formula module, a=", a);
  echo( _formula( data, s=s, a=a, w=w ));
}
 
//s= _formula( [ _vector([1,2,3])
//             , " x "
//             , _matrix([[1,2,3],[4,5,6],[7,8,9]] , cellfmt="d=0;w=3")
//             , "="
//             , _vector( [1,2,3]* [[1,2,3],[4,5,6],[7,8,9]] )
//             ] 
//        , w=190 //< ======= need to seet this manually
//        ); 
//echo(s);
//echo(_esc(s));

//========================================================
function _h(s,h, showkey=false, wrap="{,}",isfmt=true, _i_=0)= 
( 
	len(h)<2?s
	:_i_>len(h)?s
	  :_h(
		  replace(s, replace(wrap,",",h[_i_])
				, str( showkey?h[_i_]:"", showkey?"=":""
					, h[_i_+1] //isfmt?_fmt(h[_i_+1]):h[_i_+1] 
					)
				)
		 , h
		 , wrap= wrap
		 , showkey= showkey
		 , _i_=_i_+2
		 )
);
    _h= [ "_h", "s,h, showkey=false, wrap='{,}'", "string", "Hash, Doc",
    "
     Given a string (s), a hash (h) and a wrapper string like {,},
    ;; replace the {key} found in s with the val where [key,val] is
    ;; in h. If showkey=true, replace {key} with key=val. This
    ;; is useful when echoing a hash:
    ;;
    ;; _h( '{w},{h}', ['h',2,'w',3])=> '3,2'
    ;; _h( '{w},{h}', ['h',2,'w',3], true)=> 'w=3,h=2'
    "];

    function _h_test( mode=MODE, opt=[] )=
    (
      let( data=["m",3,"d",6]
           , tmpl= "2014.{m}.{d}"
           )
      doctest( _h, 
        [
          "var data"
        , [ _h("2014.{m}.{d}",  data)
            , "2014.3.6"
            ,str( tmpl, ", ", data )
          ]
        , "// showkey=true :"
        , [
            _h("2014.[m].[d]",  data, true, "[,]")
            , "2014.m=3.d=6"
            , "'2014.[m].[d]', data, showkey=true, wrap='[,]'"
          ]
        , "// hash has less key:val " 
        , [
            _h("2014.{m}.{d}", ["m",3])
            , "2014.3.{d}"
            , "tmpl, ['m',3]"
          ]
        , "// hash has more key:val " 
        , [
            _h("2014.{m}.{d}", ["a",1,"m",3,"d",6] )
            , "2014.3.6"
            , "tmpl, ['a',1,'m',3,'d',6]"
          ]	
        ], mode=mode, opt=opt , scope=["tmpl", tmpl, "data", data]
      )
    );


function _hl(x, fmt, opt=[], _do_f=1)= 
     // highlight x (2018.11.9) --- html style related
     //
     // _do_f: internal use to provide a way to disable calling _f to avoid 
     //        dublicate style settings 
     // 
(  
 //echo("in _hl, ", x=x,  fmt=fmt, opt=opt)
 let( _fmt = subprop( isstr(fmt)?sopt(fmt):fmt
                    , opt, "fmt"
                    , dfsub=[] 
                    )
     , x= _do_f?_f(x, _fmt, opt=opt):x
     
     // Set d (decimal point) and unit conversion
     , sb= has(_fmt,"b")?_b(x):str(x)//ctg
     , su= has(_fmt,"u")?_u(sb):sb
     , si= has(_fmt,"i")?_i(su):su
     , s_sup= has(_fmt,"^")? str("<sup>",si,"</sup>")
              :has(_fmt,"v")? str("<sub>",si,"</sub>"):si
                
     , color= hash( _fmt, "c")
     , sc= color? _color(s_sup,color): s_sup 
       
     , bc= hash( _fmt, "bc")
     , sbc= bc? _bcolor(sc,bc): sc 
       
     , ff= hash( _fmt, "ff")
     , sff = ff? _span( sbc, s=str("font-family:",ff) ): sbc
     
     , fs = hash( _fmt, "fs")
     , sf = fs? _span( sff, s=str("font-size:",fs,"px;") ): sff
     , sty = hash( _fmt, "s")
     , rtn  = sty? _span( sbc, s=sty): sf //sbc
        
     )
 //echo( isnum=isnum, _s=_s, spad = spad )
 //echo(fmt=fmt, _fmt=_fmt)
 //echo(ss= replace(ss,"<","&lt;"))
 //echo(_fmt=_fmt, stya=stya, styn=styn, stys=stys, stys= replace(stys,"<","&lt;"), rtn= replace(rtn,"<","&lt;"))
 //echo(ss= ss)
 isarr(x)&& haskey(_fmt,"ar")?
        let( _fmt2= delkey(_fmt, "ar") )
   	[ for(item=x) _hl( item,fmt= _fmt2, _do_f=0)  ]
 :rtn 
);


/*
*/    

    _hl=["_hl", "x,fmt,opt=[]", "str", "String, Doc, Console" 
    ,"Highlight x with format fmt. It is extended from _f and handles 
    ;; style-formatting in addition to non-style formatting in _f. 
    ;; _f and _hl have same arg signature.
    ;; 
    ;; fmt could be a str (to be coverted to hash by sopt()) or a hash.
    ;; If a str, multiple formats are separated with ';' 
    ;; 
    ;;   _hl(s, 'b;c=red')     ==> fmt in str       
    ;;   _hl(s, ['b',true,'c','red'] )  ==> fmt in hash
    ;; 
    ;; _f formats: 
    ;;
    ;;    b: boldface 
    ;;    i: italic
    ;;    u: underscore 
    ;;    %: percentage  --
    ;;    x:  x=a>b convert x from unit a to unit b  -- 
    ;;        (see uconv(), numstr() and num())
    ;;    ar:  applied to items of array, return arr ===> NOTE THIS --
    ;;    w:  w=9 set width=9. --
    ;;    d:  d=2  decimal(precision) position=2  --
    ;;    al: al=l,c,r align left, center, or right --
    ;;    p:  p=x pads the spaces with x to fulfill width.  --
    ;;    aj: aj=',' jointer for array items  --
    ;;
    ;;    wr: w={,} wraps with { and }
    ;;    aw: aw={,} wrap with { and } if x is array 
    ;;    sw: sw={,} wrap with { and } if x is str
    ;;    nw: nw={,} wrap with { and } if x is number
    ;;
    ;; _hl formats:
    ;; 
    ;;    c:  c=red make it red 
    ;;    bc: bc=red paints the background red
    ;;    fs: fs=20 sets font-size to 20px
    ;;    v: subscript
    ;;    ^: superscript
    ;;    s: 's=font-size:20px' sets font size.
    "
    ];

    function _hl_test( mode=MODE, opt=[] )=
    (
        doctest( _hl,
        [
          "// Integer: "
        , [ _hl(5), "5", "5"]
        , [ _hl("5"), "5", "'5'"]
        , [ _hl(0), "0", "0"]
        , [ _hl("0"), "0", "'0'"]
        
        ,""
        ,"//Flags: <b>b,i,u,^,v,%, ar</b>"
        ,""
        , [ _hl(30,"b"), "<b>30</b>", "30,'b'", ["//","b"]]
        , [ _hl(30,"i"), "<i>30</i>", "30,'i'", ["//","i"]]
        , [ _hl(0,"i"), "<i>0</i>", "0,'i'", ["//","i"]]
        , [ _hl(30,"u"), "<u>30</u>", "30,'u'", ["//","u"]]
        , [ _hl(30,"u;i"), "<i><u>30</u></i>", "30,'u;i'", ["//","ui"]]
        , [ _hl(30,"^"), "<sup>30</sup>", "30,'^'"
                      , ["//","^: superscript"]]
        , [ _hl(30,"v"), "<sub>30</sub>", "30,'v'"
                      , ["//","v: subscript"]]
        , [ _hl(0.5,"%"), "50%", "0.5,'%'", ["//","%"]]
        , [ _hl(1.4,"%"), "140%", "1.4,'%'", ["//","%"]]
        , [ _hl(.5,"%;d=2"), "50.00%", ".5,'%;d=2'", ["//","% and d"]]
        , [ _hl(0.3,"%;b;u"), "<u><b>30%</b></u>", "0.3,'%;b;u'", ["//","multiple "]]
         ,""
        , "// flag 'ar' to apply the fmt to each item of an array "
        , "//  _hl( [2,3,4], 'ar;d=1') = '[2.0, 3.0, 4.0]'" 
        , ""
        , [ _hl( [2,3,4], "ar;d=1"), ["2.0", "3.0", "4.0"]
             , "[2,3,4], 'ar:d=1'"
         ]     
        ,[ _hl( [2,3,4], "ar;w=5;al=c;wr={,}"), ["{  2  }", "{  3  }", "{  4  }"]
             , "[2,3,4], 'ar;w=5;al=c;wr={,}'"
         ]     
        ,[ _hl( [0.2,0.3,0.4], "ar;%"), ["20%", "30%", "40%"]
             , "[0.2,0.3,0.4], 'ar;%'"
         ]         
        ,""
        ,"// Use either a str or a hash:"
        ,""
         , [ _hl(0.3,["%",true,"b",true,"u",true]), "<u><b>30%</b></u>"
                     , "0.3,['%',true,'b',true,'u',true]"]
        
        ,""
        ,"//<b>w,d</b>: width and decimal" 
        ,""    
        , [ _hl(3.8, ["d",2]), "3.80", "3.8, ['d',2]"]
        , [ _hl(3, "d=2"), "3.00", "3, 'd=2'"]
        //, [ _hl(1, ["w",6,"d",2,"al","c"] ), " 1.00 ", "1, ['w',6,'d',2,'al','c']"]
        //, [ _hl(1, "w=6;d=2;al=c" ), " 1.00 ", "1, 'w=6;d=2;al=c'"]
        , [ _hl(3.839, "d=2"), "3.84", "3.839, 'd=2'"]
        , [ _hl(3.839, "d=0"), "4", "3.839, 'd=0'"]
        , [ _hl(3.839, "w=8"), "   3.839", "3.839, 'w=8'"]
        , [ _hl(-3.839,"w=8"), "  -3.839", "-3.839, 'w=8'"]
        , [ _hl(3.839, "w=8;d=2"), "    3.84", "3.839,'w=8;d=2'"]
        , [ _hl(3.839, "w=8;al=l" ), "3.839   ", "3.839,'w=8;al=l'"]

        , [ _hl(3.839, "w=8;al=c"), "  3.839 ", "3.839,'w=8;al=c'"]
        , [ _hl(3.839, "w=8;d=2;al=c"), "  3.84  ", "3.839,'w=8;d=2;al=c'"]
        , [ _hl(-3.839,"w=8;d=2;al=c"), " -3.84  ", "-3.839,'w=8;d=2;al=c'"]
        , [ _hl(3.839, "w=4"), "3.83", "3.839,'w=4'"
             ,["//","w too small, get cut"]
          ]
        , [ _hl(3.839, ["w",8,"al","c","p","#"]), "##3.839#", "3.839,['w',8,'al','c','p','#']"]
        , [ _hl("abcdefghij", "w=12"), "  abcdefghij", "'abcdefghij','w=12'"
          ]
        , [ _hl("3.8","d=2"), "3.8", "'3.8','d=2'"
                , ["//", "Can't format decimal on str"]]
        , [ _hl("3.8","w=6"), "   3.8", "'3.8','w=6'"]
        , [ _hl("3.8","w=6;d=2"), "   3.8", "'3.8','w=6;d=2'"]
        , [ _hl(0.837,"d=2"), "0.84", "'3.8','d=2'"]
        , [ _hl(0.837,"%;d=2"), "83.70%", "'3.8','%;d=2'"]
          
        ,""
        ,"// <b>al</b>: l,c,r  --- align " 
        ,""    
        , [ _hl(1.4685,"%;d=1"), "146.9%", "1.4685,'%;d=1'", ["//","% & d2"]]
        , [ _hl(0.4685,"b;%;d=0;w=9;p=-;al=c"), "<b>---47%---</b>", "146.85,'b;%;d=0;w=9;p=-;al=c'", ["//","%"]]
        , [ _hl(1.685,"%;w=9;al=l"), "168.5%   ", "1.685,'%;w=9;al=l'"]
        , [ _hl(1.685,"%;w=9;al=c"), "  168.5% ", "1.685,'%;w=9;al=c'"]
        , [ _hl(1.685,"%;w=6"), "168.5%", "1.685,'%;w=6'"]
        , [ _hl(1.685,"%;w=4"), "168%", "1.685,'%;w=4'"]
        
        ,""
        , "// <b>x</b>: unit conversion"
        ,""
        , [ _hl(96, "x=in>ftin"), "8'", "96, 'x=in>ftin'", ["//","Unit convert"] ]
        , [ _hl(100, "x=in>ftin"), "8'4\"", "100, 'x=in>ftin'", ["//","Unit convert"] ]
        , [ _hl(3.5, "x=ft>ftin"), "3'6\"", "3.5, 'x=ft>ftin'" ]
        , [ _hl(3.5, "x=ft>in"), "42", "3.5, 'x=ft>in'" ]
        , [ _hl(1, "x=in>cm"), "2.54", "1, 'x=in>cm'" ]
        , [ _hl(96, "wr={,};x=in>ftin"), "{8'}"
                    , "96, 'wr={,};x=in>ftin'" ]
        ,""
        , [ _hl(1.4685, "%;b;w=11;d=2;al=c;p=x;wr={,}")
            ,"<b>{xx146.85%xx}</b>", "1.4685, '%;b;w=11;d=2;al=c;p=x;wr={,}'" ]
                
        ,""
        ,"// <b>wr</b>: wrap; <b>aw,nw, sw</b>: wrap when array, num or str, resp.:"
        ,""
        , [ _hl(30, "wr={,}"), "{30}", "30, 'wr={,}'", ["//","wrap"]]
        , [ _hl(30, "aw={,}"), "30", "30, 'aw={,}'", ["//","wrap if arr"] ]
        , [ _hl([2,3], "aw={,}"), "{[2, 3]}", "[2,3], 'aw={,}'", ["//","wrap if arr"] ]
        , [ _hl(30, "nw=/,/"), "/30/", "30, 'nw=|,|'",["//","wrap if num"] ]
        , [ _hl([2,3], "aw={,}"), "{[2, 3]}", "[2,3], 'aw={,}'" ]
        , [ _hl([2,3], "sw={,}"), "[2, 3]", "[2,3], 'sw={,}'" ]
        , [ _hl("[2,3]", "sw={,}"), "{[2,3]}", "'[2,3]', 'sw={,}'" ]
 
        ,""
        ,"// <b>ar</b>: fmt applied to items of array; return an arr"
        ,""
        , [ _hl( [2,3,4], "wr=_,_"), "_[2, 3, 4]_", "[2,3,4], 'wr=_,_'"]
        , [ _hl( [2,3,4], "ar;wr=_,_"), ["_2_", "_3_", "_4_"], "[2,3,4], 'ar;wr=_,_'"]
        //, [ _hl( [2,3,4], "ar;aj=|;wr=_,_"), "[_2_|_3_|_4_]", "[2,3,4], 'ar;aj=|;wr=_,_'"]
        , [ _hl( [2,3,4], "aw=_,_"), "_[2, 3, 4]_", "[2,3,4], 'aw=_,_'"]
        , [ _hl( [2,3,4], "ar;aw=_,_"), ["2", "3", "4"], "[2,3,4], 'ar;aw=_,_'"]
        , [ _hl( [2,[3],4], "ar;aw=_,_"), ["2", "_[3]_", "4"], "[2,[3],4], 'ar;aw=_,_'"]
        , [ _hl( [2,[3],"4"], "ar;nw=#,#;sw=`,`;aw=_,_"), ["#2#", "_[3]_", "`4`"], "[2,[3],4], 'ar;nw=#,#;sw=`,`;'"]

                           
        ,""  
        , "// The following are hard to test, but we can show it:"
          , str( "_hl(", _arg("30, 's=color:teal'"), ") = ",
               _hl(30, "s=color:teal")
               ) 
          , str( "_hl(", _arg("30, 's=font-size:20px'"), ") = ",
               _hl(30, "s=font-size:20px")
               ) 
          , str( "_hl(", _arg("30, 'b;fs=20'"), ") = ",
               _hl(30, "b;fs=20;c=blue ")
               ) 
        , str( "_hl(",_arg("96, 'w=6;al=c;wr=<code>{,}</code>;c=cyan;bc=black', pad='&nbsp;'"),") = "
            , _hl(96, "w=6;al=c;wr=<code>{,}</code>;c=cyan;bc=black", pad="&nbsp;") )
   
   
        ]//doctest 
        , mode=mode, opt=opt
        )
    );


function ichar(s,c)= search(c, s, 0)[0][0]; // char index 2018.5.1

//========================================================

function lcase(s, _rtn="", _i=0)=
(
    let( i= idx(A_Z,s[_i])
       , _rtn= str( _rtn, i==undef?s[_i]:a_z[i] )
       )
    _i<len(s)-1? lcase( s,_rtn,_i+1) : _rtn
);

    lcase=["lcase", "s","str","String",
     " Convert all characters in the given string (s) to lower case. "
    ];
    function lcase_test( mode=MODE, opt=[] )=(//=======================
      doctest( lcase,
      [ 
          [lcase("aBC,Xyz"), "abc,xyz", "'aBCC,Xyz'"]
      ]
        , mode=mode, opt=opt
      )
    );

//========================================================

//function _matrix(data)=
//(
//  let( rows = _tablist(data, cellj="", rowj=0 )
//     , m = [ str(BOX_TL, _SP, rows[0], _SP ,BOX_TR) 
//           , for(i=[1:len(rows)-2] )
//               str(BOX_V, _SP, rows[i], _SP ,BOX_V) 
//           , str(BOX_BL, _SP, last(rows), _SP ,BOX_BR) 
//           ]
//     )
//  join(m,_BR)   
//);

function _matrix( data
                 , cellfmt    // common format -- Refer to _f for fmt patterns
                 , sup=""
                 , sub=""
                 , headerfmt  // common header format
                 , headerfmts               
                 , colfmts                
                 //, cellj // if given join cells of a row w/ it
                 , rowj   // if given, join cells of a row w/ cellj, and rows w/ rowj
                 //, rowwraps 
                 , escHTML                   
                 , opt 
                 )=
(
  let( rows= _tablist( data     = data
                   , cellfmt  = cellfmt
                   , headerfmt =headerfmt
                   , headerfmts=headerfmts                
                   , colfmts = colfmts                
                   , cellj= ""
                   , rowj=  rowj
                   , rowwraps="" 
                   , escHTML=escHTML                   
                   , opt=       opt
                   ) 
     , suplen = len(str(sup))
     , sublen = len(str(sub))
     , su_len = max( sublen, suplen)               
     , _sup = str( sup, repeat(_SP, su_len-suplen))              
     , _sub = str( sub, repeat(_SP, su_len-sublen))              
     , m = len(rows)==1? [ str("<b>[</b>"
                         , rows[0]
                         , "<b>]</b>" 
                         , sup?_sup(sup):""
                         , sub?_sub(sub):""
                         ) ]
       :len(rows)==2?
         //echo(sup = sup)
         [ str(BOX_TL, rows[0], BOX_TR, _sup) 
         , str(BOX_BL, rows[1], BOX_BR, _sub) 
         ]
       : [ str(BOX_TL, rows[0], BOX_TR, _sup) 
         , for(i=[1:len(rows)-2] )
             str(BOX_V, rows[i], BOX_V) 
         , str(BOX_BL, last(rows), BOX_BR,_sub) 
         ]
     )
  //echo("<hr/>")   
  //echo(in_matrix_data=data) 
  //echo(in_matrix_rows=rows)     
  join(m, _BR)   
);  

//_matrix( [[1,2,3]] );
//_matrix( [[1],[2]] );
//_matrix( [[1,2,3],[4,5,6]] );
//_matrix( [[1,2,3],[4,5,6],[7,8,9]] );
//_m = _matrix( [[1,2,3]] );
 
//_table([[ 3 ]]);
//_table([[ "abc"]]);

//_matrix( [[1],[2]] );
//_matrix( [[1,2,3],[4,5,6]] );
//_matrix( [[1,2,3],[4,5,6],[7,8,9]] );

//_matrix( [[1,2]], sup="T" );
//_matrix( [[1,2]], sup=30000,sub="t=1" );
//_matrix( [[1],[2]], sup="T" );
//_matrix( [[1],[2]], sup=30000,sub="t=1" );
////_matrix( [[1,2,3],[4,5,6]] );
//_matrix( [[1,2,3],[4,5,6],[7,8,9]], sup="T" );
//_matrix( [[1,2,3],[4,5,6],[7,8,9]], sup=30000,sub="t=1" );


module _matrix( data
             , cellfmt    // common format -- Refer to _f for fmt patterns
             , sup=""
             , sub=""
             , headerfmt  // common header format
             , headerfmts               
             , colfmts                
             , cellj // if given join cells of a row w/ it
             , rowj   // if given, join cells of a row w/ cellj, and rows w/ rowj
             , rowwraps 
             , escHTML                   
             , opt 
             )
{
  echo(str(_BR,_matrix( data     = data
                   , cellfmt  = cellfmt
                   , sup=sup
                   , sub=sub
                   , headerfmt =headerfmt
                   , headerfmts=headerfmts                
                   , colfmts = colfmts                
                   , cellj= ""
                   , rowj=  rowj
                   , rowwraps="" 
                   , escHTML=escHTML                   
                   , opt=       opt 
                   )
       )            
  );
}

//_matrix( [ [1,2,3],[4,5,6], [7,8,9] ] );
//_matrix( [ [1,2,3],[4,5,6], [7,8,9] ], "d=0;w=3" );



module _mdoc(mname, s){ 
	echo(_pre(_code(
			str("<br/><b>"
			, mname
			, "()</b><br/><br/>"
			, parsedoc(s)
			)
			,s="color:blue"
		)));
}				  

    _mdoc=["_mdoc", "mname,s", "n/a", "Doc",
     " Given a module name (mname) and a doc string (s), parse s (using 
    ;; parsedoc()) and echo to the console.
    "];
    function _mdoc_test( mode=MODE, opt=[] )=
    (
      doctest( _mdoc, mode=mode, opt=opt )
    );

//========================================================

//========================================================

function parsedoc( s
                 , split="//"
                 , prefix ="|"
		 , indent =""
		 , replace=[]
		 , linenum
		 )=
 let( s   = split(s, split)	)
(
	
  join(
	[for( i=range(s) )
		 str( indent
			, prefix
			, isnum(linenum)?str((linenum+i),". "):"" 
			, replace
			  (
				replace
				( 
				  replace( s[i], "''", "&quot;" )
				, "<", "&lt;"
				)
			  , ">", "&gt;"
			  )
			)
	],"<br/>")									
);

    parsedoc=["parsedoc","string","string", "Doc",
     " Given a string for documentation, return formatted string.
    "];

    function parsedoc_test( mode=MODE, opt=[] )=
    (
      doctest( parsedoc, mode=mode, opt=opt )
    );
 
 //========================================================
function replace( o,x,new="" )=
( 
  isarr(o)
  ? ( let(x= fidx(o,x))
      inrange(o,x)
	    ? concat( slice(o, 0, x), [new], x==len(o)-1?[]:slice(o,x+1))
      : o 
	  )
  : ( index(o,x)<0? o 
      : join( split( o, x), new ) // <= Applied search()-based split (16.12.20)
    )                             //    which also makes it tail-recursion
);

//( 
//    let( isa = isarr(o)
//       , x= isa?fidx(o,x):x)
//  isa
//  ? ( inrange(o,x)
//	  ? concat( slice(o, 0, x), [new], x==len(o)-1?[]:slice(o,x+1)): o 
//	  )
//  : ( index(o,x)<0? o 
//	    :str( slice(o,0, index(o,x))
//			    , new 
//			    , (index(o,x)==len(o)-len(x)?""
//				    :replace( slice(o, index(o,x)+len(x)), x, new) )
//		      )
//    )
//);    
    replace =[ "replace", "o,x,new=\"\"", "str|arr", "String, Array",
    "
     Given a str|arr (o), x and optional new. Replace x in o with new when
    ;; o is a str; replace the item x (an index that could be negitave) 
    ;; with new when o is an array.
    ;;
    ;; 2016.12.20: use search()-based split() and tail-recursion
    ;;
    " ];

    function replace_test( mode=MODE, opt=[] )=
    (
      doctest( replace, 
      [
        [	replace("a_text", "e"), "a_txt", "'a_text', 'e'"]
      , [	replace("a_text", ""), "a_text", "'a_text', '"]
        , [ replace("a_text", "a_t", "A_T"), 	"A_Text", "'a_text', 'a_t', 'A_T'"]
      , [	replace("a_text", "ext", "EXT"), "a_tEXT", "'a_text', 'ext', 'EXT'"]
      , [	replace("a_text", "abc", "EXT"), "a_text", "'a_text', 'abc', 'EXT'"]
      , [	replace("a_text", "t", 	"{t}"), "a_{t}ex{t}", "'a_text', 't',   '{t}'"]
        , [ replace("a_text", "t", 	4), 		"a_4ex4", "'a_text', 't', 4"]
        , [ replace("a_text", "a_text","EXT"), "EXT", "'a_text', 'a_text', 'EXT'"]
        , [	replace("", "e"), "",  "', 'e'"]
      , "# Array # "
        , [ replace([2,3,5], 0, 4), [4,3,5] , "[2,3,5], 0, 4"]
        , [ replace([2,3,5], 1, 4), [2,4,5] , "[2,3,5], 1, 4"]
        , [ replace([2,3,5], 2, 4), [2,3,4] , "[2,3,5], 2, 4"]
        , [ replace([2,3,5], 3, 4), [2,3,5] , "[2,3,5], 3, 4"]
        , [ replace([2,3,5], -1, 4), [2,3,4] , "[2,3,5], -1, 4"]

       ], mode=mode, opt=opt	
      )
    );	
    	
 //========================================================
function replaces( o,subs, _i=0 )=
( 
  //echo(o=o, subs=subs, ", replacing ", subs[_i]," with ", subs[_i+1], _i=_i)
  _i>=len(subs)/2+2? o
  : replaces( o=replace(o, subs[_i], subs[_i+1]), subs=subs, _i=_i+2)
);
    replaces =[ "replaces", "o,subs", "str|arr", "String, Array",
    "
     Given a str|arr (o) and a hash subs=h{k,v}, replace each k in o with v when
    ;; o is a str; replace the item x (an index that could be negitave) 
    ;; with new when o is an array.
    " ];

    function replaces_test( mode=MODE, opt=[] )=
    (
      doctest( replaces, 
      [
       [replaces("a_text", ["e","E","t","T","_","|"]), "a|TExT", "'a_text', [\"e\",\"E\",\"t\",\"T\",\"_\",\"|\"]"]
//      , [replaces("a_text", ["t","T", "e","E"]), "a_txt", "'a_text', 'e'"]
//      ,[replaces("a_text", ""), "a_text", "'a_text', '"]
//      ,[replaces("a_text", "a_t", "A_T"), 	"A_Text", "'a_text', 'a_t', 'A_T'"]
//      ,[replaces("a_text", "ext", "EXT"), "a_tEXT", "'a_text', 'ext', 'EXT'"]
//      ,[replaces("a_text", "abc", "EXT"), "a_text", "'a_text', 'abc', 'EXT'"]
//      ,[replaces("a_text", "t", 	"{t}"), "a_{t}ex{t}", "'a_text', 't',   '{t}'"]
//      ,[replaces("a_text", "t", 	4), 		"a_4ex4", "'a_text', 't', 4"]
//      ,[replaces("a_text", "a_text","EXT"), "EXT", "'a_text', 'a_text', 'EXT'"]
//      ,[replaces("", "e"), "",  "', 'e'"]
      ,"# Array # "
      ,[ replaces([2,3,5], [0, 4,1,6]), [4,6,5] , "[2,3,5], [0,4,1,6]"]
//      ,[ replaces([2,3,5], 1, 4), [2,4,5] , "[2,3,5], 1, 4"]
//      ,[ replaces([2,3,5], 2, 4), [2,3,4] , "[2,3,5], 2, 4"]
//      ,[ replaces([2,3,5], 3, 4), [2,3,5] , "[2,3,5], 3, 4"]
//      ,[ replaces([2,3,5], -1, 4), [2,3,4] , "[2,3,5], -1, 4"]

      ], mode=mode, opt=opt	
      )
    );		

//========================================================
function repeat(x,n=2)=
(
    let(n=round(n))
	isarr(x)
    ?
	 ( n>0? concat(x, n>1?repeat(x,n-1):[]): [] )
	:
     ( n>0? str(x, n>1?repeat(x,n-1):""): "") 
);

    repeat=["repeat", "x,n=2", "str|arr", "Array,String" ,
    "
     Given x and n, duplicate x (a str|int|arr) n times.
    "];

    function repeat_test( mode=MODE, opt=[] )=
    (
      doctest( repeat, 
        [ 
          [ repeat("d"), "dd" , "'d'"]
        , [ repeat("d",5), "ddddd" , "'d',5'"]
        , [ repeat("d",0), "" , "'d',0'"]
        , [ repeat([2,3]), [2,3,2,3] , "[2,3]"]
        , [ repeat([2,3],3), [2,3,2,3,2,3] , "[2,3],3"]
        , [ repeat([[2,3]],3), [[2,3],[2,3],[2,3]] , "[[2,3]],3"]
        , [ repeat([2,3],0), [] , "[2,3],0"]
        , [ repeat(3), "33" , "3"]
        , [ repeat(3,5), "33333", "3,5", ["//","return a string"] ]
      ], mode=mode, opt=opt

      )
    );



function _s_ruler(s, _rtn="", _i=0)= // debug tool to check what s[_i] is
(  
  
  _i<len(s)?
   let(c= _mono(s[_i]==" "?"_": s[_i]))
   _s_ruler( s 
           , str( _rtn, _i%10==0? _red(c)
                         :_i%5==0? _green(c)
                         : c
                )         
           , _i+1
           )
  : let( counter= join( [ for(i=[0:floor(len(s)/10)])
                          _span( str(_red(_bcolor(i,"gold"))
                                    ,"1234"
                                    ,_green("5")
                                    , "6789")
                               ,s = "font-family:lucida console"
                               )      
                          //_mono(str( _red(str(i)), _mono("123456789")))
                        ] )  
       ) 
  _span( str("<br/>", counter, "<br/>",_rtn), s="font-size:14px")
);

    _s_ruler= [ "_s_ruler", "s" , "str", "String",
    "
      A debug tool to check what s[_i] is. It prints out two lines,
      first being a ruler, 2nd is s, with highlighted color to 
      identify character location easily
      
      012345678911234567892123456789
      {car}_car_{seat}_seats?
      
      '{car}_car_{seat}_seats'[20]='t'
    "
    ] ;
//echo(_s_ruler("{car} car {seat} seats?"));
//echo( test="{car} car {seat} seats?"[20]);



function _s( s, data= [] )=
(
   // Revised 2018.11.8 --- by simplying _s2()
   
   (!data)||(!s)?s 
   :let( data= isarr(data)?data:[str(data)]
       , ms = match(s,ps= ["{_}"]) /// arr of [2,4,"{_}"]
              // 1918: bug of match found:
              // match( "test_date={_}/{_})", ps=["{_}"] )
              // should have given: 
              //     [[10, 13, "{_}", ["{_}"]], [14, 17, "{_}", ["{_}"]]]
              // but gives instead:
              //    [[4, 5, "_", ["_"]], [10, 13, "{_}", ["{_}"]], [14, 17, "{_}", ["{_}"]]]
              
              
       , ss= ms?
             let( ms = [ for(m=ms) if(m[2]=="{_}")m ] ) // <=== temporary fix 
             //echo(ms=ms)
             join( 
                   [ for( mi = range(ms) ) /// each ms[mi] is: [2,4,"{_}"]
                      str( slice( s,mi==0?0:(ms[mi-1][1]), ms[mi][0] )
                         , data[mi] 
                         , mi==len(ms)-1? slice(s,ms[mi][1]):""
                         )
                   ]
                 )
             :s
       )
     //echo(ms=ms)  
     ss  
);  



function _s_b4_18b7(s,arr, sp="{_}", _rtn="", _i=0, _j=0)= 
( 

    //echo("_s(....................)")
    let( arr= !isarr(arr)?[str(arr)]:arr
       , m= match_at( s, _i, ["{",str(A_zu,0_9d),"}"] )  // false | [2,4,"{_}"]
       //, m= match_at( s, _i, str("{",str(A_zu,0_9d),"}") )  // false | [2,4,"{_}"]
       , add= m? (_j<len(arr)?arr[_j]:sp):s[_i]
       , _rtn = str( _rtn, add)
       , _i = _i+ (m? len(sp):1)
       , _j = _j+ (m? 1:0)
       )
    echo(s=s, m=m, mm=mm)   
    !s?s
    :_i>=len(s)? _rtn
    : _s( s, arr, sp, _rtn, _i,_j )
);

    _s= [ "_s", "s,arr,sp=''{_}''" , "str", "String",
    "
     Given a string (s) and an array (arr), replace the sp in s with
    ;; items of arr one by one. This serves as a string template.
    "
    ] ;

    function _s_test( mode=MODE, opt=[] )=
    (
        doctest( _s,
        [ 
          [ _s("2014.{_}.{_}", [3,6]), "2014.3.6", "'2014.{_}.{_}', [3,6]"]
        , [ _s("", [3,6]), "", "'', [3,6]"]
        , [ _s("2014.{_}.{_}", []), "2014.{_}.{_}", "'2014.{_}.{_}', []"]
        , [ _s("2014.{_}.{_}", [3]), "2014.3.undef", "'2014.{_}.{_}', [3]"]
        , [ _s("2014.3.6", [4,5]), "2014.3.6", "'2014.3.6', [4,5]"]
        , [ _s("{_} car {_} seats?", ["red","blue"]), "red car blue seats?"
              , "'{_} car {_} seats?', ['red','blue']"
          ]
        , 
        [ _s("test_date={_}/{_}", [12,3]), "test_date=12/3"
              , "\"test_date={_}/{_}\", [12,3]"
          ]
        ], mode=mode, opt=opt

      )
    );

//module _s(s,arr, sp="{_}")  // 2018.7.9
//{
//  echo(_s(s,arr, sp=sp));
//}


function _s2( s
    , data= []
    , beg="{"
    , df_fmtchars= str(A_zu,0_9d,"-*`|=%/,:")
    , morefmtchars=""
    , end="}"
    , show=undef
    
//    , _debug=[]
    )=
(
    let( data= isarr(data)?data:[str(data)]
       , ps= [ beg
             , str(df_fmtchars, morefmtchars)
             , end
             ]
       , m = match(s,ps=ps) 
             //: [ [2,4,  "{2}", ["{","2","}"]]
             //  , [9,11, "{5}", ["{","5","}"]] ]
       , ss= m?
             join( 
               [for( mi = range(m) )
                   
                let( mx= m[mi] //:[2,4,"{2}",["{","2","}"]]
                   , i = mx[0] 
                   , j = mx[1]
                   , ms= mx[2] //: ms:entire matched string "{2}"
                   , mcs= split(mx[3][1],"`") 
                               //: mcs:content between { and }
                               
                   , mc= mcs[0] //: _, *, int, name
                   , fmt= mcs[1]
                   , nmc= num(mc)
                   , x = isnum(nmc)?
                            (inrange(data, nmc)? 
                                get(data, nmc): ms)
                         : mc=="_"? or(data[mi], ms)
                         : mc=="*"? data
                         : hash(data, mc,ms)
                   )
                str( slice( s,mi==0?0:(m[mi-1][1]),i )
                   //, "["
                   , fmt ? _f(x,fmt):x
                  // , "]"
                   , mi==len(m)-1? slice(s,j):""
                   )
               ]//_for
             )//_join
             :s
       )//_let
    show? hash( [ "df_fmtchars", df_fmtchars
                , "patterns", ps
                , "matched", m
                ], show )
    :ss
);  

//========================================================
_s2= [ "_s2", "s,data,beg,df_fmtchars,morefmtchars,end,show" , "str", "String",
"Replace the d in {d`f} in s with data using optional format f. 
;; 
;; d: data selection (_,*,int,name)
;; f: format (see _f()) 
;; 
;; W/o format:
;;  _s2('2015.{_}.{_}', [5,24]) => 2015.5.24 
;;  _s2('2015.{*}.{*}', [5,24]) => 2015.[5, 24].[5, 24]
;;  _s2('2015.{-1}.{0}', [5,24])=> 2015.24.5
;;  _s2('2015.{m}.{d}', ['m',5,'d',24])  => 2015.5.24 
;;  
;; W format:
;;  _s2('2015.{_`fmt}.{_`fmt}', [5,24]) 
;;  _s2('2015.{1`fmt}.{0`fmt}', [24,5])
;;  _s2('2015.{m`fmt}.{d`fmt}', ['m',5,'d',24])
;;
;; beg,end: beginning/end patterns for the search. 
;;          
;; df_fmtchars: default chars for defining fmt. This is the
;;          chars for the patterns between beg and end.
;;              
;; morefmtchars: add your own chars for search pattern. For
;;          example, if your search term is:{.*..!..} where * 
;;          and ! are not in df_fmtchars, you can set
;;          morefmtchars='*!'
;;
;; Refer to match() for details of the above pattern settings.
;;    
;; show: for displaying property. For example, 
;;         _s2(... show='df_fmtchars')
;; 
;; Refer to _f() for what fmt could be. 
", "_s,_f,_h,_fmt,_fmth"
] ;

function _s2_test( mode=MODE, opt=[] )=
(
	let( data=["m",3,"d",6]
       , tmpl= "2014.{m}.{d}"
       )
    doctest( _s2,
    [ 
     "// {_}"
    , [ _s2("2014.{_}.{_}", [3,6]), "2014.3.6", "'2014.{_}.{_}', [3,6]"]
    , [ _s2("", [3,6]), "", "'', [3,6]"]
    , [ _s2("2014.{_}.{_}", []), "2014.{_}.{_}", "'2014.{_}.{_}', []"]
    , [ _s2("2014.{_}.{_}", [3]), "2014.3.{_}", "'2014.{_}.{_}', [3]"]
    , [ _s2("2014.3.6", [4,5]), "2014.3.6", "'2014.3.6', [4,5]"]
	, [ _s2("{_} car {_} seats?", ["red","blue"]), "red car blue seats?"
        , "'{_} car {_} seats?', ['red','blue']"
		]
    , "","//==============="
    , "// {i}, i could be negative"
    , [ _s2("2014.{0}.{1}", [3,6]), "2014.3.6", "'2014.{0}.{1}', [3,6]"]
    , [ _s2("2014.{1}.{0}", [3,6]), "2014.6.3", "'2014.{1}.{0}', [3,6]"]
    , [ _s2("2014.{-1}.{-2}", [3,6]), "2014.6.3", "'2014.{-1}.{-2}', [3,6]"]
    , [ _s2("2014.{-1}.{-1}", [3,6]), "2014.6.6", "'2014.{-1}.{-1}', [3,6]"]
    , [ _s2("2014.{1}.{2}", [3,6]), "2014.6.{2}", "'2014.{1}.{2}', [3,6]"]
    , [ _s2("2014.{-1}.{2}", [3,6]), "2014.6.{2}", "'2014.{-1}.{2}', [3,6]"]
    , "","//==============="
    , "// {*}"
    , [ _s2("At pt {_} & {_}", [2,3,4]), "At pt 2 & 3"
           ,"'At pt {_} & {_}', [2,3,4]"
      ]
    , [ _s2("At pt {*} & {*}", [2,3,4]), "At pt [2, 3, 4] & [2, 3, 4]"
           ,"'At pt {*} & {*}', [2,3,4]"
      ]
    , ""
    , [ _s2("Px={_}, P={*}, Py={-2}", [2,3,4]), "Px=2, P=[2, 3, 4], Py=3"
           ,"'Px={_}, P={*}, Py={-2}', [2,3,4]"
      ]
    
    , "","//==============="
    , "// {name}"
    , [ _s2("2014.{m}.{d}",  data)
        , "2014.3.6"
        ,str( tmpl, ", ", data )
      ]
//    , "// showkey=true :" {abc`/}
//    , [
//        _s2("2014.[m].[d]",  h=data)
//        , "2014.m=3.d=6"
//        , "'2014.[m].[d]', h=data, showkey=true, wrap='[,]'"
//      ]
    , "// hash has less key:val " 
    , [
        _s2("2014.{m}.{d}", ["m",3])
        , "2014.3.{d}"
        , "tmpl, ['m',3]"
      ]
    , "// hash has more key:val " 
    , [
        _s2("2014.{m}.{d}", ["a",1,"m",3,"d",6] )
        , "2014.3.6"
        , "tmpl, ['a',1,'m',3,'d',6]"
      ]	
    , "","//==============="
    , [
        _s2("2014.{m}.{d}", ["a",1,"m",3,"d",6] )
        , "2014.3.6"
        , "'2014.{m}.{d}', ['a',1,'m',3,'d',6]"
      ]  

    ,"// The following case uses [] that are not in the"
    ,"// default fmt chars:"
    , [ _s2("2014.{m`%||wr=/,/}.{d`|d2|wr=[,]}"
            , ["a",1,"m",3,"d",6] 
            , morefmtchars="[]"
            )
        , "2014./300%/.[6.00]"
        , "'2014.{m`%||wr=/,/}.{d`|d2|wr=[,]}',
 ['a',1,'m',3,'d',6], morefmtchars='[]'"
        , ["nl",true]
      ]
    ,"// if not use morefmtchars:"  
    , [ _s2("2014.{m`%||wr=/,/}.{d`|d2|wr=[,]}"
            , ["a",1,"m",3,"d",6] 
            )
        , "2014./300%/.{d`|d2|wr=[,]}"
        , "'2014.{m`%||wr=/,/}.{d`|d2|wr=[,]}',
 ['a',1,'m',3,'d',6]"
        , ["nl",true]
      ]
      
      , [ _s2("% of {0}= {0`%}",[0.96]), "% of 0.96= 96%"
            ,"'% of {0}= {0`%}',[0.96]"]  
    , [ _s2("% of {0`u}= {0`%b}",[0.96]), "% of <u>0.96</u>= <b>96%</b>"
            ,"'% of {0`u}= {0`%b}',[96]"
            ,["nl",true]
      ]  
    , [ _s2("{0} to ftin:{0`||x=in:ftin}",[42])
            , "42 to ftin:3'6\""
            ,"'{0} to ftin:{0`||x=in:ftin}',[42]"
            ,["nl",true]
      ]
    , [ _s2("Take 2 decimal of {0}: {0`|d2}", [14.369])
            ,"Take 2 decimal of 14.369: 14.37"
            ,"'Take 2 decimal of {0}: {0`|d2}', [14.369]"
            ,["nl",true]
      ]  
    , [ _s2("W={W`||x=in:ftin}, L={L`||x=in:cm}cm"
            ,["L",1, "W",30])
            ,"W=2'6\", L=2.54cm"
            ,"'W={W`||x=in:ftin},L={L`||x=in:cm}cm', ['L',1, 'W',30]"
            ,["nl",true]
      ]
    ,""
    ,"// Change { } to something else:"
    ,[ _s2("2014.[m].(d)",["m",3,"d",6]
           , beg="[(", end="])")
       , "2014.3.6"
       , "'2014.[m].(d)',['m',3,'d',6], beg='[(', end='])'"
     ]      
    ,[ _s2("2014.[m].(d)",["m",3,"d",6]
           , beg="[(", end="])")
       , "2014.3.6"
       , "'2014.[m].(d)',['m',3,'d',6], beg='[(', end='])'"
     ]      
    ,""
    ,"// format items of array"
    ,""
    ,[ _s2( "At pt {_`a|d1}"
          , [ [0.2351, -9.72087, 1.5621]]
          )
          , "At pt [0.2, -9.7, 1.6]"
          , "'At pt {_`a|d1}',[[0.2351,-9.72087,1.5621]]"
          , ["nl",true]
      ] 
     ,""
    ,"// show: expose internal property. Avaliable: df_fmtchars,patterns, matched"
    ,[ _s2("2014.[m].(d)",["m",3,"d",6]
           , beg="[(", end="])", show="matched")
       , [[5, 8, "[m]", ["[", "m", "]"]], [9, 12, "(d)", ["(", "d", ")"]]]
       , "'2014.[m].(d)',['m',3,'d',6], beg='[(', end='])', show='matched'"
       , ["nl",true]
     ]      
    ], mode=mode, opt=opt, scope=["data",data,"tmpl",tmpl]

	)
);



function _ss( s, data= [], fmt="", opt=[] )=
(
   //echo("In _ss,", s=s, data=data, opt=opt)
   (!data)||(!s)?s 
   :let( data= isarr(data)?data:[str(data)]
       , ps= [ "{"
             , str(A_zu,0_9d,"-*%;:=>")
               //str(df_fmtchars, morefmtchars)
             , "}" 
             ]
       , ms = match(s,ps=ps) 
             ///: [ [2,4,  "{2}", ["{","2","}"]]
             ///  , [9,11, "{5}", ["{","5","}"]] ]
       , ss= ms?
             join( 
               [for( mi = range(ms) )
                   
                let( m= ms[mi] //:[2,4,"{2}",["{","2","}"]]
                   , i = m[0] 
                   , j = m[1]
                   , mstr= m[2] //: mstr:entire matched string "{2}"
                   , mcs= split(m[3][1],":") 
                               /// m[3][1]:content between { and }
                               /// 
                               
                   , mc= mcs[0] //: _, *, int, name
                   , fmt= update(fmt, mcs[1])
                   , nmc= num(mc)
                   , x = isnum(nmc)? 
                            (inrange(data, nmc)? 
                                get(data, nmc): mstr)
                         : mc=="_"? //or(data[mi], mstr) <=== this skips 0, not good
                                    (data[mi]==undef?mstr:data[mi])   // 2018.11.29
                         : mc=="*"? data
                         : hash(data, mc,mstr)
                   )
                //echo("----------", mi=mi, mc=mc, nmc=nmc, data_i = data[mi], x=x)   
		//echo(_hl(x=x,fmt=fmt, opt=opt))
		//echo( mc=mc )   
		//echo( mc=mc, nmc = nmc ) 
		//echo("Inside _ss, ", x=x, fmt=fmt, opt=opt)  
                str( slice( s,mi==0?0:(ms[mi-1][1]),i )
                   //, "["
                   , fmt ? _hl(x=x,fmt=fmt, opt=opt):x
                  // , "]"
                   , mi==len(ms)-1? slice(s,j):""
                   )
               ]//_for
             )//_join
             :s
       )//_let
     ss  
//    show? hash( [ "patterns", ps
//                , "matched", ms
//                ], show )
//    :ss
);  

//========================================================
_ss= [ "_ss", "s,data=[]" , "str", "String",
"Given a string s and a data, replace substrings in s with data . 
;; Note: _s() only replaces '{_}'. _ss() provides much more features. 
;; 
;; Format of substrings to be replaced: {x:f}  
;;
;;   x: _  --- corresponding positional item in data 
;;      *  --- entire-data-array
;;      int --- indexed item of data
;;      name --- data as a hash
;;   f: format (see _ff()) 
;;
;; W/o format:
;;  _ss('2015.{_}.{_}', [5,24]) => 2015.5.24 
;;  _ss('2015.{*}.{*}', [5,24]) => 2015.[5, 24].[5, 24]
;;  _ss('2015.{-1}.{0}', [5,24])=> 2015.24.5
;;  _ss('2015.{m}.{d}', ['m',5,'d',24])  => 2015.5.24 
;;  
;; W format:
;;  _ss('2015.{_:fmt}.{_:fmt}', [5,24]) 
;;  _ss('2015.{1:fmt}.{0:fmt}', [24,5])
;;  _ss('2015.{m:fmt}.{d:fmt}', ['m',5,'d',24])
;;
;; Refer to _ff() for what fmt could be. 
"
, "_s,_f,_ff, _h,_fmt,_fmth"
] ;

function _ss_test( mode=MODE, opt=[] )=
(
	let( data=["m",3,"d",6]
       , tmpl= "2014.{m}.{d}"
       )
    doctest( _ss,
    [ 
     "// {_}"
    , [ _ss("2014.{_}.{_}", 3), "2014.3.{_}", "'2014.{_}.{_}', 3"]
    , [ _ss("2014.{_}.{_}", [3,6]), "2014.3.6", "'2014.{_}.{_}', [3,6]"]
    , [ _ss("", [3,6]), "", "'', [3,6]"]
    , [ _ss("2014.{_}.{_}", []), "2014.{_}.{_}", "'2014.{_}.{_}', []"]
    , [ _ss("2014.{_}.{_}", [3]), "2014.3.{_}", "'2014.{_}.{_}', [3]"]
    , [ _ss("2014.3.6", [4,5]), "2014.3.6", "'2014.3.6', [4,5]"]
	, [ _ss("{_} car {_} seats?", ["red","blue"]), "red car blue seats?"
        , "'{_} car {_} seats?', ['red','blue']"
		]
    , "","//==============="
    , "// {i}, i could be negative"
    , [ _ss("2014.{0}.{1}", [3,6]), "2014.3.6", "'2014.{0}.{1}', [3,6]"]
    , [ _ss("2014.{1}.{0}", [3,6]), "2014.6.3", "'2014.{1}.{0}', [3,6]"]
    , [ _ss("2014.{-1}.{-2}", [3,6]), "2014.6.3", "'2014.{-1}.{-2}', [3,6]"]
    , [ _ss("2014.{-1}.{-1}", [3,6]), "2014.6.6", "'2014.{-1}.{-1}', [3,6]"]
    , [ _ss("2014.{1}.{2}", [3,6]), "2014.6.{2}", "'2014.{1}.{2}', [3,6]"]
    , [ _ss("2014.{-1}.{2}", [3,6]), "2014.6.{2}", "'2014.{-1}.{2}', [3,6]"]
    , "","//==============="
    , "// {*}"
    , [ _ss("At pt {_} & {_}", [2,3,4]), "At pt 2 & 3"
           ,"'At pt {_} & {_}', [2,3,4]"
      ]
    , [ _ss("At pt {*} & {*}", [2,3,4]), "At pt [2, 3, 4] & [2, 3, 4]"
           ,"'At pt {*} & {*}', [2,3,4]"
      ]
    , [ _ss("Px={_}, P={*}, Py={-2}", [2,3,4]), "Px=2, P=[2, 3, 4], Py=3"
           ,"'Px={_}, P={*}, Py={-2}', [2,3,4]"
      ]
    
    , "","//==============="
    , "// {name}"
    , [ _ss("2014.{m}.{d}",  data), "2014.3.6",str( tmpl, ", ", data )]
      
    , "// hash has less key:val " 
    , [ _ss("2014.{m}.{d}", ["m",3]), "2014.3.{d}", "tmpl, ['m',3]"]
    
    , "// hash has more key:val " 
    , [ _ss("2014.{m}.{d}", ["a",1,"m",3,"d",6] ), "2014.3.6"
          , "tmpl, ['a',1,'m',3,'d',6]" ]	

    , ""
    , "//============= formatting ==========="
    , "// Check _ff() for formatting patterns"
    , ""
     
    , [ _ss("% of {0}= {0:%}",[0.96]), "% of 0.96= 96%"
            ,"'% of {0}= {0:%}',[0.96]"]  
    , [ _ss("% of {_}= {0:%}",[0.96]), "% of 0.96= 96%"
            ,"'% of {_}= {0:%}',[0.96]"]  
            
    , [ _ss("% of {0:u}= {0:%;b}",[0.96]), "% of <u>0.96</u>= <b>96%</b>"
            ,"'% of {0:u}= {0:%;b}',[96]"
      ]  
    
    , [ _ss("2014.{_:d=1}.{_:d=2}", [3,6]), "2014.3.0.6.00", "'2014.{_:d=1}.{_:d=2}', [3,6]"]
    
    , [ _ss("{0} to ftin:{0:x=in>ftin}",[42])
            , "42 to ftin:3'6\""
            ,"'{0} to ftin:{0:x=in>ftin}',[42]"
      ]
    , [ _ss("Take 2 decimal of {0}: {0:d=2}", [14.369])
            ,"Take 2 decimal of 14.369: 14.37"
            ,"'Take 2 decimal of {0}: {0:d=2}', [14.369]"
      ]  
    , [ _ss("W={W:x=in>ftin}, L={L:x=in>cm}cm"
            ,["L",1, "W",30])
            ,"W=2'6\", L=2.54cm"
            ,"'W={W:x=in>ftin},L={L:x=in>cm}cm', ['L',1, 'W',30]"
      ]
  
    ,""
    ,"// format items of array"
    ,""
    ,[ _ss( "At pt {_:ar;d=1}"
          , [ [0.2351, -9.72087, 1.5621]]
          )
          , "At pt [\"0.2\", \"-9.7\", \"1.6\"]"
          , "'At pt {_:ar;d=1}',[[0.2351,-9.72087,1.5621]]"
      ] 
     
    ], mode=mode, opt=opt, scope=["data",data,"tmpl",tmpl]

	)
);


//========================================================
{sci=["sci","n","str","str"
,"Return the str representation of n in scientific notation.
"];
function sci(n)=
(
   let( p = floor(log(n))  // power
      , i = floor(n*pow(10,-p))  // integer part
      , f = (n*pow(10,-p)-i) // fraction part
      , pp= str( i+f, "e" 
               , p>=0? "+":"-"
               , abs(p)<10?"0":""
               , abs(p)
               ) 
      )
      pp //[p,i,f] //pp
);
     function sci_test( mode=12, opt=[] )=
     (
         doctest( sci
         , [
             [ sci(3), "3e+00", 3]
           , [ sci(1234), "1.234e+03", 1234 ]
           , [ sci(123.4), "1.234e+02", 123.4 ]
           , [ sci(1.234), "1.234e+00", 1.234 ]
           , [ sci(0.1234), "1.234e-01", 0.1234 ]
           , [ sci(0.12345), "1.2345e-01", 0.12345 ]
           , [ sci(0.0001234), "1.234e-04", "0.0001234" ]
           , [ sci(1.0001234), "1.00012e+00", "1.0001234" ]
           
           ], mode=mode,opt=opt
         )
     );        
               
}//sci


//========================================================
{slice=[ "slice", "s,i,j", "str|arr", "String,Array" ,
 " Slice s (str|arr) from i to j-1. 
;;
;; - If j not given or too large, slice from i to the end. 
;; - Both could be negative/positive.
;; - If i >j, do a reversed slicing
;;
;;   slice( ''012345'',2 )= ''2345''
;;   slice( ''012345'',2,4 )= ''23''
;;   slice( ''012345'',-2 )= ''45''
;;   slice( [10,11,12,13,14,15],2,20 )= [12, 13, 14, 15] // When out of range
;;
;; Reversed slicing:  
;;
;;   slice( [10,11,12,13,14,15], 5,2 )= [15, 14, 13]
;;   slice( ''012345'', 5,2 )= ''543''
;;   slice( ''012345'', -3,-5 )= ''32''
;;
;; Note that this func doesn't do cycling (i.e., when it is
;; out of range, it doesn't continue from the other end). 
;; Use *range()* for that purpose.
;;
;; New 2020.7.17: can take slice(o, [i,j])
"
];

function slice(o,i,j)= 
// let( i= i<-(len(o))?0:fidx(o,i)
//    , j=j==undef||j>len(o)-1? len(o): fidx(o, j)
//	  , L=len(o)
//	  , ret= i<j? [for(k=[i:j-1])o[k]]
//			     :i>j? [for(k=[L-i:L-j-1])o[L-k]]
//		       :[]
//	)
 let( L=len(o)
    , _i = j==undef? (isint(i)?i:i[0]):i  // New 2020.7.17: can take slice(o,[i,j])
    , _j = j==undef? (isint(i)?L:i[1]):j  // 
    , i= _i<-L?0:fidx(o,_i)
    , j= _j==undef||_j>len(o)-1? L: fidx(o, _j)
    , ret= i<j? [for(k=[i:j-1])o[k]]
		       :i>j? [for(k=[L-i:L-j-1])o[L-k]]
		   :[]
	)
(
	isarr(o)? ret: join(ret,"")
);
 
    function slice_test( mode=MODE, opt=[] )=
    (
        let( 
        s="012345"
        ,a=[10,11,12,13,14,15]
        )
        doctest( slice, 
        [
          "## slice a string ##"
          
        , [ slice(s,2),"2345", "'012345',2"]		 
        , [ slice(s,2,4),"23", "'012345',2,4"]
        , [ slice(s,2,len(s)),"2345", "'012345',2,len(s)"]
        , [ slice(s,0,2),"01", "'012345',0,2"]		 
        , [ slice(s,-2),"45","'012345',-2"]		 
        , [ slice(s,1,-1),"1234","'012345',1,-1"]		 
        , [ slice(s,-3,-1),"34","'012345',-3,-1"]		 
        , [ slice(s,3,3),"", "'012345',3,3"]		 
        , [ slice(s,0,0),"", "'012345',0,0"]		 
        , [ slice("",3),"","',3"]		 
          
        , "## slice an array ##" 	
        , [ slice(a, 2), [12,13,14,15], "[10,11,12,13,14,15],2"]
        , [ slice(a, 2,5), [12,13,14], "[10,11,12,13,14,15],2,5"]
        , [ slice(a, 2,20), [12,13,14,15], "[10,11,12,13,14,15],2,20"
                , ["//", "When out of range"]]
        , [ slice(a, -3), [13,14,15], "[10,11,12,13,14,15],-3"]
        , [ slice(a, -20, -3), [10,11,12], "[10,11,12,13,14,15],-20,-3"
                , ["//", "When out of range"]]
        , [ slice(a, -8, -3), [10,11,12], "[10,11,12,13,14,15],-8,-3"
                , , ["//", "When out of range"]]
        , [ slice([[1,2],[3,4],[5,6]], 2), [[5,6]],"[[1,2],[3,4],[5,6]],2"]
        , [ slice([[1,2],[3,4],[5,6]], 1,1), [], "[[1,2],[3,4],[5,6]], 1,1"]
        , [ slice([], 1), [], "[], 1"]
        , [ slice([9], 1), [], "[9], 1"]

        ,""
        ,"// Reversed slicing:"
        ,""
        , [ slice(a, 2,5), [12,13,14], "[10,11,12,13,14,15], 2,5"]
        , [ slice(a, 5,2), [15,14,13], "[10,11,12,13,14,15], 5,2"]
        , [ slice(s, 5,2), "543", "'012345', 5,2"]
        , [ slice(s, 3,1), "32", "'012345', 3,1"]
          
        , [ slice(s, -3,-5), "32", "'012345', -3,-5"]
        
        , ""
        , "// New 2020.7.17: can take slice(o, [i,j]) "
        , ""
        , [ slice(a,[2,5]), [12,13,14], "[10,11,12,13,14,15], [2,5]"] 
        
        ] , mode=mode, opt=opt
        )
    );
    //echo( slice_test()[1] );
}

//========================================================
//function split(s,sp=" ", keep=false,keepempty=true)=
// let(i = index(s,sp) 
//    ,sp2= keep?[sp]:[]
//    ,empty= keepempty?[""]:[]
//	)	
//(
//	s==""? empty
//    :sp==""? [s]
//    : s==sp?(keep? (keepempty?["",s,""]:[s])
//            :(keepempty?["",""]:[])
//            )    
//    :i==0? concat( empty, sp2, split( slice(s,1), sp, keep, keepempty)  )
//    :i==len(s)-1?  concat( [slice(s,0,-1)],sp2, empty)
//    :i<0 ? [s]
//	   : concat(
//		   [ slice(s, 0, i) ]
//         , sp2
//		 , split( slice(s, i+len(sp)), sp, keep, keepempty ) 
//    )
//);

       
function split( s, sp=" ", keep=false,keepempty=true, _bf="", _rtn=[], _i=0)=
(
  !isstr(s)||!isstr(sp)?undef
  :s==""?""
  :sp==""?[for(i=range(s)) s[i]] // New 2020.7.16 (replacing [s])
  :_i>=len(s)
  ? concat( _rtn, _bf||keepempty?[_bf]:[] )
  : s[_i]==sp[0]                       // If sp[0] found
    && sp==slice(s, _i, _i+len(sp))    // Then we check if entire sp is found
    ? split( s, sp, keep,keepempty     // A match of sp is found
           , _bf=""                    // Reset _bf
           , _rtn= concat( _rtn        
                        , _bf||keepempty?[_bf]:[] // _bf is added to _rtn if it is not ""
                                                  // or if keepempty=true
                        , keep?[sp]:[]  // sp is added back to _rtn if keep=true
                        )
           , _i=_i+len(sp)
           )
    :  split( s, sp, keep,keepempty
            , _bf=str(_bf, s[_i])
            , _rtn=_rtn
            , _i=_i+1)
);


  split= [ "split", "s, sp=\" \", keep=false, keepempty=true", "arr", "String" ,
  "
   Split a string into an array. 
  ;; 
  ;; 2015.2.11: (1) arg keep, set true to keep the sp. Default false: 
  ;; 
  ;; split('abc' 'b',false)=> ['a','c']  // keep = false
  ;; split('abc','b',true) => ['a','b','c'] // keep = true
  ;;
  ;; 2015.3.2: new arg: keepempty=true
  ;; 
  ;;  split( ',t',',')= ['','t']
  ;;  split( ',t',',', keepempty=false )= ['t']
  ;;
  ;; 2016.12.19: new search()-based and tail-recursion.
  ;; This should make it much faster. 
  ;; 2020.7.16: new: when sp='', now split entire string into single-char array
  "
  ];

  function split_test( mode=MODE, opt=[] )=
  (
    doctest( split,
      [ 
        [ split("this is it"), ["this","is","it"], "'this is it'"]
      , [ split("this is it","i"), ["th","s ","s ","t"], "'this is it','i'"]
      , [ split("this is it","t"), ["","his is i",""], "'this is it','t'"]
      , [ split("this is it","is"), ["th"," "," it"], "'this is it','is'"]
      , [ split("this is it",","), ["this is it"], "'this is it',','"]
      , [ split("Scadx",","), ["Scadx"], "'Scadx',','"]
      , [ split("Scadx",""), ["S", "c", "a", "d", "x"], "'Scadx',''"]
      , [ split("Scadx","Scadx"), ["",""], "'Scadx','Scadx'"]
      , [ split(undef,"x"), undef, "undef,'x'"]
      , [ split("Scadx",3), undef, "'Scadx',3"]
      , ""
    , "// keep: keep the sp"
      , ""
      , [ split("abcabc","b"), ["a","ca","c"], "'abcabc','b'"]
      , [ split("abcabc","a",true), ["", "a","bc","a","bc"], "'abcabc','a', keep=true"]
      
  //    , [ split("abcabc","a",false), ["a","bc","a","bc"], "'abcabc','a',keepempty=false"]
  //    , [ split("abcabc","c",true), ["ab","c","ab","c"], "'abcabc','c',keepempty=true"]
      , ""
      , "// keepempty: keep the empty string. Default:true "
      , ""
      , [ split("tt","t"), ["","",""] , "'ttt','t'"]
      , [ split("tt","t", keepempty=false), [] , "'tt','t' keepempty=false"]
      , [ split(",t",","), ["","t"] , "',t',','"]
      , [ split(",t",",", keepempty=false), ["t"] , "',t',',', keepempty=false"]
      ,""
      , [ split("abcabc","a",false,false), ["bc","bc"], "'abcabc','a', false,false"]
      //, [ split("",",", keepempty=false), [], "'',','"]
      //, [ split("",","), [""], "'',',', keepempty=true"]
      
    ], mode=mode, opt=opt
    )
  );


//========================================================
function splits(s,sp=[], keep=false, keepempty=false, _i=0)=
(
  let(s = isarr(s)?s:[s] )

//    joinarr([ for(x=s) split(x, sp=sp[_i], keep=keep) ])
	_i<len(sp)
	?  splits( joinarr(
                [ for(x=s) split(x, sp=sp[_i], keep=keep, keepempty=keepempty) ])
             , sp=sp, keep=keep, keepempty=keepempty, _i=_i+1)
	:s	
);

  splits= [ "splits", "s, sp=[], keep=false", "arr", "String",
   " Split a string into an array based on the item in array sp . 
  ;;
  ;; splits(''abcdef'',[''b'',''e'']) => [''a'',''cd'',''f'']
  ;;
  ;; Developer note: 
  ;;
  ;; Most other recursions in this file go through items in a list
  ;; with a pattern:
  ;;
  ;;   _I_&lt;len(arr)
  ;;   ? ...
  ;;   : []           // Note this
  ;; 
  ;; This code, however, demos a rare case of recursion into layers 
  ;; of a tree:
  ;;
  ;;   _I_&lt;len(arr)
  ;;   ? ...
  ;;   : arr          // major difference
  ;;
  ;; 2015.2.11: New arg: keep=false
  "];
  function splits_test( mode=MODE, opt=[] )=
  (
    //s="this is it";
    let( s="abc_def_ghi" )
    doctest( splits,
    [ 
        "var s"
      , [ splits(s,["b"]), ["a", "c_def_ghi"], "s,['b']"]
    , [ splits(s,["b","_"]), ["a", "c","def", "ghi"],"s1,['b','_']"]	
    , [ splits(s,["_","b"]), ["a", "c","def", "ghi"], "s1,['_','b']"]	
      , [ splits(s,["b","_","e"]), ["a", "c","d","f", "ghi"], "s1,['b','_','e']"]
      , "// New 2015.2.11: argument keep"
      , [ splits("abcabc",["b","c"]), ["a","a"], "'abcabc',['b','c']"]
      , [ splits("abcabc",["c","b"],true)
              , ["a","b","c","a","b","c"], "'abcabc',['c','b'],keep=true"]
      , [ splits("abcabc",["b","c"],true)
              , ["a","b","c","a","b","c"], "'abcabc',['b','c'],keep=true"]
      , "// New 2015.3.2: argument keepempty"
      , [ splits("abcabc",["b","c"],keepempty=true), ["a","","a","",""], 
              "'abcabc',['b','c'],keepempty=true"
         ]
  //    , [ splits( "[3,[a,4],[5,7]]", ["[",",","]"],keep=true)
  //          ["[",3,
         
    ], mode=mode, opt=opt, scope=["s",s]

    )
  );
  
  
//========================================================
// function chari(s,c)= search(c, s, 0)[0][0]; // char index 2018.5.1

//echo("///", search( "_the", "I_am_the_findw", 1) );
//function stri(s,x)=
//(  
//   let(is=search( x,s,1 ))// [0,1,2]
//   //echo( s=s, x=x, is=is )
//   (is[len(is)-1]-is[0]+1)==len(x)?is[0]:undef 
//);
//
//    echo( si = stri("abcdef", "f") );
//
//    stri=["stri", "s,x", "int", "Index,String",
//    "Return the index of x in string s. Return undef if not found.
//    ;; 
//    ;; Compare to index:
//    ;; 
//    ;;  Unlike index, this does`t use slice() and join() for each 
//    ;;  possible char check in s. Should be faster than index when 
//    ;;  x is a word but not a single char.
//    "
//    ,"index"
//    ];
//
//    function stri_test( mode=MODE, opt=[] )=
//    (
//      let( s= "I_am_the_findw"
//         , arr=["xy","xz","yz"]
//         ) 
//
//      doctest(stri,     
//    	[ "var s"
//    	, [ stri( "I_am_the_findw","a"),  2, "s, 'a'" ]
//        , [ stri(  "abcdef","f"),  5, "'abcdef','f'" ]
//        , [ stri( "findw","n"),  2, "'findw','n'" ]
//        , [ stri( "I_am_the_findw","am"),  2, "s, 'am'",  ]
//        , [ stri( "I_am_the_findw","_the"),  4, "s, '_the'",  ]
//        , [ stri( "I_am_the_findw","find"),  9, "s, 'find'" ]
//        , [ stri( "I_am_the_findw","the"),  5,"s, 'the'",  ]
//        , [ stri("I_am_the_ffindw","findw"),  10,"s, 'findw'" ]
//        , [ stri("findw", "findw"),  0, "'findw', 'findw'" ]
//        , [ stri( "findw","dwi"),  undef, "'findw', 'dwi'" ]
//        , [ stri("I_am_the_findw","The"),  undef,"s, 'The'" ]
//        , [ stri( "replace__text","tex"),  9, "s, 'tex'" ]
//        , [ stri("replace__ttext","tex"), 10, "'replace__ttext', 'tex'" ]
//        , [ stri( "replace_t_text","text"),  10, "'replace_t_text','tex'"]
//        , [ stri("abc", "abcdef"),  undef, "'abc', 'abcdef'" ]
//        , [ stri( "","abcdef"),  undef,"'', 'abcdef'" ]
//        , [ stri( "abc",""),  undef, "'abc',''" ]
//  	, [ stri([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
//        
//        ]
//        ,mode=mode, opt=opt, scope=[ "s",s, "arr",arr]
//      )
//    );      


////========================================================

//function _tablist( header,data,sep=1, indent=0
//               , dfHeaderFmt="u|ac|bc=khaki"
//               , dfCellFmt="|w6d2ac"
//               , lineWrap=""  
//               , lineWrapTop= ""
//               , lineWrapBottom=""                    
//               , opt=[]  // [ "sep_is", indices to insert separator (like [3:3:12])
//                         //             (To sep every other 2 lines, use: [2:2:len])
//                         // , "rowstyle", [ [row is], "color:red"
//                         //             , [row is], "background:black;color:green"
//                         //             , ... ]
//                         // ]
//               )=
//(  // header: ["name",<format>, "sex", <format>, "tel",<format> ]
//   // where <format> is that for _f()
//   //let( _header = _span( join([ for(hv=hashkvs(header)) _f(hv[0], hv[1])])
//   //                     , s="background:yellow" )
//   let( header= [for(h=header)h?h:dfCellFmt]
//      , sep = isstr(sep)?sep:repeat("&nbsp;", sep)
//      , ind = isstr(indent)?indent:repeat("&nbsp;", indent)
//      , fmts = vals(header) 
//      , dfh = split(dfHeaderFmt,"|")
//      , _header =  str( ind
//                      , (join([ for(kv=hashkvs(header))       
//                              _u(_f(kv[0]
//                                   , join( [ for(i=[0:2])
//                                             str( und(dfh[i],"")
//                                                , und(split(kv[1],"|")[i],"")
//                                                )
//                                           ], "|"
//                                         )        
//                                         //  str(dfh[0], kv[1])
//                                   , pad="&nbsp;"
//                                   ))
//                            ]
//                       , sep)
//                       )
//                      )         
//      , sep_is = [for(i=h(opt, "sep_is"))i]
//      , _rowstyle= h(opt, "rowstyle")
//      , rowstyle = [ for(i=range(data)) 
//                        [ for( is= keys( _rowstyle ) )
//                           if( has(is,i) ) h(_rowstyle, is)  
//                        ][0] 
//                    ]
//      , _data = [ each
//                  for( i=range(data))
//                  let( line = data[i]
//                     , _linestr= //replace(replace(
//                                 join( [ for(i=range(line))
//                                       i<=len(fmts)? _f(line[i], fmts[i], pad="&nbsp;")
//                                                   : line[i]
//                                       ] 
//                                     , sep     
//                                     )
//                                // , "<","&lt;"
//                                //  )
//                               //, "&nbsp;","^"
//                               //)       
//                     , __linestr= rowstyle[i]?_span(_linestr,rowstyle[i]): _linestr
//                     , linestr= i==0? ( lineWrapTop? 
//                                         str(lineWrapTop[0], __linestr, lineWrapTop[1] )
//                                        : __linestr
//                                      )
//                              : i==len(data)-1? 
//                                      ( lineWrapBottom? 
//                                         str(lineWrapBottom[0], __linestr, lineWrapBottom[1] )
//                                        : __linestr
//                                      )
//                              :( lineWrap? 
//                                         str(lineWrap[0], __linestr, lineWrap[1] )
//                                        : __linestr
//                               )
//                                
//                     )
//                  //echo(sep_is, i,isin(i,sep_is), has(sep_is,i))   
//                  has(sep_is,i)? [ "", linestr ]
//                                : [ linestr ]
//                ] 
//      , rtn= join( concat( [_header], _data)
//                 , str("<br/>", ind)
//                 )
//      )     
//   //echo(header=header) 
//   //echo(_header=_header) 
//   
//   //echo(rowstyle=rowstyle)                          
//   //replace(
//    //replace( 
//    //_mono(str("<br/>",rtn,"<br/>")), "<", "&lt;")
//    //, "&nbsp;", "^" )
//   _span( _mono(str("<br/>",rtn,"<br/>"))
//       , "background:lightcyan"
//       )
//);
function _tablist( data
               , header // set true to set the first line of data as header
               
               , cellfmt    // common format -- Refer to _f for fmt patterns
               , headerfmt  // common header format
               
               /// The following 2 are h{idx:fmt} for target-specific fmt 
               //   [ 0, "b;c=red", 4, "bc=black;c=green", ... ]
               , headerfmts=[]                
               , colfmts = []                
              // , rowfmts // [ rs, fmt, rs, fmt]
               
               , cellj="|" // if given join cells of a row w/ it
               , rowj="<br/>"   // if given, join cells of a row w/ cellj, and rows w/ rowj
                // 3 different returns:
                //
                // cellj rowj  
                //   F    F -- [ ["xx","yy","zz"], ["xx","yy","zz"] ]
                //   T    F -- [ "xx,yy,zz", "xx,yy,zz" ]
                //  T/F   T -- "xx,yy,zz ; xx,yy,zz"
                
               , rowwraps="||" 
               , escHTML                   
               , opt=[]  
               )=
( // 3 return modes decided by cellj/rowj  
  // cellj rowj                              cellfmt rowfmt
  //   F    F -- [ ["xx","yy"], ["xx","yy"] ]   y      -
  //   T    F -- [ "xx,yy", "xx,yy" ]           y      y 
  //  T/F   T -- "xx,yy ; xx,yy"                y      y
   let( _rowj = arg(rowj, opt, "rowj")
      , _cellj= arg(cellj, opt, "cellj", _rowj?"|":undef)
      , _rowwraps = arg( rowwraps, opt, "rowwraps", "||" )
      
      , cis = range(data[0]) 
                 
      , hCellfmt = argu(cellfmt, opt, "cellfmt", "w=6;d=2;al=c")//#FEF9E7") // common format for ALL cells
      , hColfmts = argu(colfmts, opt, "colfomts",[]) // [ 2,fmt, 3, fmt ]
//      , aColfmts = [ for(ci= cis)                // col-specific cell formats
//                       h( hColfmts,ci,[]) 
//                     //update( hCellfmt, h( hColfmts,ci,[]) )
//                   ]    
      
      , hHeaderfmt= argu(headerfmt, opt, "headerfmt", update(hCellfmt, "u"))
      , hHeaderfmts= argu(headerfmts, opt, "headerfmts",[]) // [ 2,fmt, 3, fmt ]
      , aHeaderfmts = [ for(ci= cis)                // col-specific header formats
                        //echo(_____________ci= ci)
                        //echo(hCellfmt= hCellfmt)
                        //echo(aColfmts_ci= aColfmts[ci])
                        //echo(hHeaderfmt= hHeaderfmt)
                        //echo(hHeaderfmt_ci=h( hHeaderfmts,ci,[]))
                        //echo(__aHfmsi= updates( hHeaderfmt 
                               //, [ aColfmts[ci]
                                 //, h( hHeaderfmts,ci,[]) 
                                 //]
                               //))
                        updates(  hHeaderfmt
                                 , [ h(hColfmts,ci) //hCellfmt 
                                   , h( hHeaderfmts,ci) 
                                 ]
                               )
                      ]    
              
      //------------------------------------------------ 
      , __header= arg(header, opt, "header")
      , headerTexts = __header==true || __header==1? data[0] : header
      //, headers = _header? [ for(ci=cis) _hl( _header[ci], _headerfmts[ci] )]
      //                   : []
      //------------------------------------------------ 
      , __data = __header==true || __header==1 ? slice(data,1) : data

      , rtn = isstr(_rowj)? //_rowj!=undef && _rowj!=false? 
//        echo(__header=__header)
//        echo(headerTexts=headerTexts, [ for(ci=cis) _hl( headerTexts, _headerfmts[ci]) ])
//        echo(_headerfmt=_headerfmt) 
//        echo(_headerfmts=_headerfmts) 
        //echo(_colfmts=_colfmts, _cellj=_cellj) 
               
              join( 
                concat( !__header?[]: 
                        [ str( _rowwraps?_rowwraps[0]:""
                             , join(
                                 [ for(ci=cis) 
                                    _hl( headerTexts[ci]
                                       , aHeaderfmts[ci]
                                       , opt=["p", "&nbsp;"]
                                       ) 
                                 ]
                                 , _cellj
                                 )
                            , _rowwraps?_rowwraps[1]:""
                            )
                        ]
                      , [ for (ri = range(__data))
                           str( _rowwraps?_rowwraps[0]:""
                              , join( [ for(ci = cis)
                                    _hl( __data[ri][ci]
                                       , update( hCellfmt, h(hColfmts,ci))
                                       , opt=["p", "&nbsp;"]    
                                       )
                                  ]
                                , _cellj
                                )
                              , _rowwraps?_rowwraps[1]:""
                              )   
                        ]
                     )   
                 , _rowj
                 )
                      
              : isstr(_cellj)?
              //: _cellj!=undef && _cellj!=false?  // only cellj given,
                                                 // return array of row strings
                  //echo(no_rowj_has_cellj=1)
                  //echo(is_cellj_str = isstr(_cellj), _rowwraps=rowwraps)      
                  concat( !__header?[]: 
                          [ str( _rowwraps?_rowwraps[0]:""
                              , join(
                                     [ for(ci=cis) 
                                        _hl( headerTexts[ci]
                                           , aHeaderfmts[ci]
                                           , opt=["p", "&nbsp;"]
                                           )  
                                     ]
                                     , _cellj
                                     )
                               , _rowwraps?_rowwraps[1]:""
                              )        
                          ]
                        , [ for (ri = range(__data))
                             str( _rowwraps?_rowwraps[0]:""
                                , join( [ for(ci = cis)
                                          //echo(ci=ci, x=__data[ri][ci]
                                              //, style=update( hCellfmt,h(hColfmts,ci))
                                              //)
                                         _hl( __data[ri][ci]
                                            , update( hCellfmt,h(hColfmts,ci))
                                            , opt=["p", "&nbsp;"]    
                                            )
                                      ]
                                    , _cellj
                                    ) 
                                , _rowwraps?_rowwraps[1]:""
                              )    
                          ]
                       ) 
                         
              : concat( !__header?[]: 
                          [  [ for(ci=cis) 
                                _hl( headerTexts[ci]
                                   , aHeaderfmts[ci]
                                   , opt=["p", "&nbsp;"]
                                   )  
                             ]
                          ]
                        , [ for (ri = range(__data))
                             [ for(ci = cis)
                               _hl( __data[ri][ci]
                                  , update( hCellfmt, h(hColfmts,ci))
                                  , opt=["p", "&nbsp;"]    
                                  )
                            ]
                          ]
                       )     
                             
      )     
   //echo( _cellj=_cellj, _rowj=_rowj ) 
   //echo(header=header) 
   //echo(__header=__header) 
   //echo(_header=_header) 
   //echo(headers=headers) 
//   echo(aColfmts=aColfmts) 
//   echo(headerfmt=headerfmt) 
//   echo(hHeaderfmt= hHeaderfmt) 
//   echo(hHeaderfmts=hHeaderfmts) 
   //echo(headers=headers) 
   //echo(data = data)
   //echo(__data = __data)
   //echo(cols = cols)
   //echo(_data = _data)
   //echo(_cellfmt=_cellfmt)
   //echo(_colfmt=_colfmt)
   //echo(_colfmts=_colfmts)
   //echo(_cfmts=_cfmts)
   //echo(rowfmt=rowfmt)                          
   //replace(
    //replace( 
    //_mono(str("<br/>",rtn,"<br/>")), "<", "&lt;")
    //, "&nbsp;", "^" )
   //_span( _mono(str("<br/>",rtn,"<br/>"))
       //, "background:lightcyan"
       //)
//   escHTML && rowj!=undef? replace( 
//                             replace(
//                              replace( rtn, "<br", "||br"), "<","&lt;"
//                                    ), "||br", "<br/>&lt;br"
//                                  )
   //echo(rtn = rtn, lens = [for(x=rtn)len(x)])
   escHTML ? replace( 
                   replace(
                    replace( str(rtn), "<br", "||br"), "<","&lt;"
                          ), "||br", "<br/>&lt;br"
                   )
   : rtn //_code(_mono(rtn, s="background-color:#FEF9E7"))
   //: rtn                                       
   
   
);

    _tablist=["_tablist", "header,data,indent=0", "str", "Array, String",
        , "Given header which is a hash:
        ;; 
        ;;  header: ['name',/format/, 'sex', /format/, 'tel',/format/ ]
        ;;
        ;; Return a string representing the data in a table format
        ;;
        ;; -- indent could be an int (1,2,3...) or a str like '---'
        "];    
    
    function _tablist_test( mode=MODE, opt=[] )=
    (
        //let( mode= 100*(mode>99?1:0)+ mode%10 ) // disable testing 
        //echo(mode=mode)   
        let( a1= transpose( [ range(1,5), [for(i=range(5))i/3]] )
           , a2= transpose( [ range(1,5), [for(i=range(5))i/3], [for(i=range(5)) sqrt(i)]] )
           )
        doctest( _tablist,
        [ "var a1"
        , "var a2"
	, ""
	, str( "> _tablist(a1)<br/>", _tablist( a1 ) )
	, ""
	, str( "> _tablist(a2)<br/>", _tablist( a2 ) )
        
        , ""  
        , "// Header"
        , "// header could be 1,true, or a set of data"
        , ""
        , str( "> _tablist( a1, header=1 )<br/>", _tablist( a1, header=1))
        , ""
        , str( "> _tablist( a1, header=[\"a\",\"cd\"])<br/>"
             , _tablist( a1, header=["a","cd"] ))
          
        , ""
        , "// <u>cellfmt</u>: set common fmt" 
        , ""
        , str( "> _tablist( a1, header=1, cellfmt='d=1;w=8' )<br/>"
              , _tablist( a1, header=1, cellfmt="d=1;w=8") )
              
        , ""
        , "// <u>colfmts</u>: set fmt(s) of specific cols" 
        , ""
        , str( "> _tablist( a2, header=1, cellfmt='d=1;w=8', colfmts=[1,'w=4;d=0', 1, '%;d=0', 2,'w=15'] )<br/>"
              , _tablist( a2, header=1, cellfmt="d=1;w=8", colfmts=[0,"w=4;d=0",1, "%;d=0", 2,"w=15"]) )
              
              
        , ""      
        , "// <u>headerfmt</u>: set header fmt, which overwrites cellfmt" 
        , ""
        , str( "> _tablist( a1, header=1, cellfmt='d=1;w=8', headerfmt='d=0' )<br/>"
              , _tablist( a1, header=1, cellfmt="d=1;w=8", headerfmt="d=0") )
              
        , ""
        , str( "> _tablist( a1, header=1, cellfmt='d=1;w=8', headerfmt='bc=khaki' )<br/>"
              , _tablist( a1, header=1, cellfmt="d=1;w=8", escHTML=0) )
        , ""
        , "// <u>headermts</u>: set fmt(s) of specific headers" 
        , ""
        , str( "> _tablist( a2, header=1, cellfmt='d=1;w=8', colfmts=[1,'w=4;d=0', 2,'w=15'], headerfmts=[2,'d=5'] )<br/>"
              , _tablist( a2, header=1, cellfmt="d=1;w=8", colfmts=[0,"w=4;d=0",2,"w=15"], headerfmts=[2,"d=5"]) )
        , ""
        , ""
        , "// <u>rowwraps</u>: default: '||'" 
        , ""
        , str( "> _tablist( a1, header=1, rowwraps='[]')<br/>"
        , _tablist( a1, header=1, rowwraps="[]" ))
         , ""
        , "//=========================================="
        , "// No rowj, return arr of row string:"
        , "//=========================================="
        , ""
        , str( "> _tablist(a1, rowj=0)<br/>", _tablist( a1, rowj=0 ) )
        , str( "> _tablist(a1, header=1, rowj=0)<br/>", _tablist( a1, header=1, rowj=0 ) )
          
        , ""
        , "//=========================================="
        , "// No rowj and cellj, return arr of cell string arrays:"
        , "//=========================================="
        , ""
        , str( "> _tablist(a1, rowj=0, cellj=0)<br/>", _tablist( a1, rowj=0, cellj=0 ) )
        , str( "> _tablist(a1, header=1, rowj=0, cellj=0)<br/>", _tablist( a1, header=1, rowj=0, cellj=0 ) )
          
        ],mode=mode, opt=opt, scope=["a1",a1, "a2",a2] )
    ); 

module _tablist( data
             , header // set true to set the first line of data as header
             , cellfmt    // common format -- Refer to _f for fmt patterns
             , headerfmt  // common header format
             , headerfmts=[]                
             , colfmts = []                
             , cellj="|" // if given join cells of a row w/ it
             , rowj="<br/>"   // if given, join cells of a row w/ cellj, and rows w/ rowj
             , rowwraps="||" 
             , escHTML                   
             , opt=[] 
             )
{
  echo(_tablist( data     = data
             , header   = header 
             , cellfmt  = cellfmt
             , headerfmt =headerfmt
             , headerfmts=headerfmts                
             , colfmts = colfmts                
             , cellj= cellj
             , rowj=  rowj
             , rowwraps=rowwraps 
             , escHTML=escHTML                   
             , opt=       opt 
             )
      );
}               

//t=_tablist( [["3+2"],[5]], cellfmt="d=0;w=7;s=font-size:20px"
//       , header=1, cellj="", rowwraps=""  );
//echo( str(_BR, t));
//echo( _mono( _tag("table"
//                 ,_tr( 
//                        str( _td(_span(t)), _td(" X ")
//                           , _td(_matrix( [ [1,2,3],[4,5,6], [7,8,9] ],"d=0;w=3" ))
//                           )
//                      )
//    ))) ;
//echo(t);
//_tablist( [[_span(t)], [3]] );
//=========================================
function toHtmlColor(colorArray)= // Convert return of randc 
( 
  let( ca = [ for(c=colorArray) int(c*255) ] )                          
  _s("rgb({_},{_},{_})",ca)
);


//========================================================
                
function trim(o, l=" ", r=" ")=
(
    isarr(o)? [for(x=o) trim(x, l=l,r=r) ]
    : isstr(o)?
        index(l, o[0])>=0? trim( slice(o,1), l=l,r=r)
        : index(r, last(o))>=0? trim( slice(o, 0,-1), l=l, r=r )
        : o
    :o
);  
  
    trim=["trim", "o, l=' ', r=' '", "arr/str", "Array, String",
    , "Given o as a str/arry, trim the string or strings in arr on either
    ;; or both left and right. The characters to be trimmed away are 
    ;; defined in l or r. If defined as l='abc', all the chars a,b,c
    ;; on the far left will be trimmed: 
    ;;
    ;; l = 'abc'
    ;; 'abcxyz' => 'xyz'
    ;; 'bcaxyz' => 'xyz'
    ;; 'cbaxyz' => 'xyz'

    "];    
  
    function trim_test( mode=MODE, opt=[] )=
    (
        doctest( trim,
        [
         [ trim("abcxyz",l="abc"), "xyz", "'abcxyz',l='abc'"]
        , [ trim("bcaxyz",l="abc"), "xyz", "'bcaxyz',l='abc'"]
        , [ trim("cabxyz",l="abc"), "xyz", "'cabxyz',l='abc'"]
        , [ trim("  abc "), "abc", "'  abc '"]
        , [ trim(",abc!", l=",", r="!" ), "abc", "',abc!',l=',',r='!'"]
        , [ trim(", abc! . ", l=" ,", r="!. " ), "abc", "', abc! . ', l=' ,', r='!. '"]
        , [ trim([" a, ", ", b"], l=" ,.", r=" ,."), ["a","b"], "['a,',',b'], l=',.', r=' ,.'" ] 
        , [ trim(["# a, ", 3, undef, true], l=" #", r=", ."), ["a",3,undef,true]
            , "['# a, ', 3, undef, true], l=' #', r=', .'"] 
        ],mode=mode, opt=opt )
    ); 


//========================================================
ucase=["ucase", "s","str","String",
 " Convert all characters in the given string (s) to upper case. "
];
//_alphabetHash2= split("a,A,b,B,c,C,d,D,e,E,f,F,g,G,h,H,i,I,j,J,k,K,
//l,L,m,M,n,N,o,O,p,P,q,Q,r,R,s,S,t,T,u,U,v,V,w,W,x,X,y,Y,z,Z",",");
//
//function ucase(s, _I_=0)=
//(
//  	_I_<len(s)
//	? str( hash( _alphabetHash2, s[_I_], notfound=s[_I_] )
//		, ucase(s,_I_+1)
//		)
//	:""
//);
function ucase(s, _rtn="", _i=0)=
(
    let( i= idx(a_z,s[_i])
       , _rtn= str( _rtn, i==undef?s[_i]:A_Z[i] )
       )
    _i<len(s)-1? ucase( s,_rtn,_i+1) : _rtn
);    


function ucase_test( mode=MODE, opt=[] )=
(
	doctest( ucase,
	[
		[ucase("abc,Xyz"), "ABC,XYZ", "'abc,Xyz'", ]
	], mode=mode, opt=opt
	)
);


//function updateStyle(s1,s2, isRtnHash=0)=
//(
//  let( _s1= [ for(kv=split(s1,";"))
//              if(trim(kv)) each split(kv,":") ]
//     , _s2= [ for(kv=split(s2,";"))
//              if(trim(kv)) each split(kv,":") ]
//     , s= update(_s1,_s2)         
//     )
//  //echo("in updateStyle")
//  //echo(_s1=_s1, _s2=_s2)             
//  isRtnHash? s
//  : join( [ for( i= [0:len(s)-1] )
//             str(s[i], i%2?";":":")
//          ]
//        )
//);
//
//    updateStyle=["updateStyle", "s1,s2", "arr/str", "Array, String",
//    , "Update style
//    "];    
//  
//    function updateStyle_test( mode=MODE, opt=[] )=
//    (
//        doctest( updateStyle,
//        [
//         [updateStyle("a:b;c:d", "c:x"), "a:b;c:x;", "'a:b;c:d', 'c:x'" ]   
//        ,[updateStyle("a:b;c:d", "a:3"), "a:3;c:d;", "'a:b;c:d', 'a:3'" ]   
//        ,[updateStyle("a:b;c:d;", ";a:3;;"), "a:3;c:d;", "'a:b;c:d;', ';a:3;;'" ]   
//        ,[updateStyle("a:b;; ;c:d;", "a:3"), "a:3;c:d;", "'a:b;; ;c:d;', 'a:3'" ]   
//          
//        ],mode=mode, opt=opt )
//    ); 



function _vector( arr
                 , cellfmt    // common format -- Refer to _f for fmt patterns
                 , sup=""
                 , sub=""
                 , headerfmt  // common header format
                 , headerfmts               
                 , colfmts                
                 //, cellj // if given join cells of a row w/ it
                 , rowj   // if given, join cells of a row w/ cellj, and rows w/ rowj
                 //, rowwraps 
                 , escHTML                   
                 , opt 
                 )=
( // return a str for printing [1,2,3 ] as
  //   1 
  //   2 
  //   3 
  //echo(_vector_arr= arr)
  //
  // arr could be a str: "P,Q,R"  (2018.11.28)
  
  
  let( arr = isstr(arr)? [for(x=split(arr,","))x] // Allows _vector("P,Q,R")
                     : arr
     , digit= max( [ for(x=arr)
                     floor(x)==x?0
                     :floor(10*x)==10*x?1
                     :isnum(x)? 2:0 ] 
                 )
     , w_int= max( [ for(x=arr)
                     //echo(x=x, len(str(x)))
                     isnum(x)? //let(n= x<0?1:0)
                               len(str(floor(x)))//-n
                             : len(str(x)) 
                   ] 
                 )            
     , m= _matrix( data     = [for(x=arr)[x]]
                 , cellfmt  = update( ["d",digit
                                      , "w", w_int+(digit==0?0:1)+digit + 2
                                      ]
                                    , cellfmt?cellfmt:[] )
                 , sup=sup
                 , sub=sub                   
                 , headerfmt =headerfmt
                 , headerfmts=headerfmts                
                 , colfmts = colfmts                
                 , cellj= ""
                 , rowj= rowj
                 , rowwraps="" 
                 , escHTML=escHTML                   
                 , opt=       opt 
                 )
     )
  //echo(arr=arr, digit=digit, w_int=w_int)   
     //echo(digit=digit, data=[for(x=arr)[x]], cellfmt=update( ["d",digit], cellfmt?cellfmt:[] ))
  //echo(ms= replace(ms, "&nbsp;", "_"))
  //join( [ for(m=ms) _mono(m) ] , _BR ) 
  //_mono(m)
  //echo(digit=digit, w_int = w_int, w=w_int+(digit==0?0:1)+digit + 2)
  m
);  
//echo( _vector = str(_BR, _vector([1,2,3])) );
//_vector([1,2,3]) ;
//_vector( [-10,0,17] );
//_vector( [-10,20,17], sup="T", sub="t=1" );

module _vector( arr
             , cellfmt    // common format -- Refer to _f for fmt patterns
             , sup=""
             , sub=""
             , headerfmt  // common header format
             , headerfmts               
             , colfmts                
             , cellj // if given join cells of a row w/ it
             , rowj   // if given, join cells of a row w/ cellj, and rows w/ rowj
             , rowwraps 
             , escHTML                   
             , opt 
             )
{
  echo(str(_BR,_vector( arr     = arr
                   , cellfmt  = cellfmt
                   , sup=sup
                   , sub=sub
                   , headerfmt =headerfmt
                   , headerfmts=headerfmts                
                   , colfmts = colfmts                
                   , cellj= ""
                   , rowj=  rowj
                   , rowwraps="" 
                   , escHTML=escHTML                   
                   , opt=       opt 
                   )
       )            
  );
}


//===========================================
//
//. Console html tools 
//
//===========================================

_SP  = "&nbsp;";
_BR  = "<br/>"; 

function _table(arr, a="" //s="", a=""
               , header // set true to set the first line of data as header
               
               , cellfmt    // common format -- Refer to _f for fmt patterns
               , Colfmts // [ 2,fmt, 3, fmt ]
               
               , headerfmt  // common header format
               
               /// The following 2 are h{idx:fmt} for target-specific fmt 
               //   [ 0, "b;c=red", 4, "bc=black;c=green", ... ]
               , headerfmts=[]                
               , colfmts = []                
              // , rowfmts // [ rs, fmt, rs, fmt]
               
               , cellj="|" // if given join cells of a row w/ it
               , rowj="<br/>"   // if given, join cells of a row w/ cellj, and rows w/ rowj
                // 3 different returns:
                //
                // cellj rowj  
                //   F    F -- [ ["xx","yy","zz"], ["xx","yy","zz"] ]
                //   T    F -- [ "xx,yy,zz", "xx,yy,zz" ]
                //  T/F   T -- "xx,yy,zz ; xx,yy,zz"
                
               , rowwraps="||" 
               
               , escHTML                   
               , opt=[]  
               )=
(
   let( hCellfmt = argu(cellfmt, opt, "cellfmt", "w=6;d=2;al=c")//#FEF9E7") // common format for ALL cells
      , hColfmts = argu(colfmts, opt, "colfomts",[]) // [ 2,fmt, 3, fmt ]
      , cis = range(arr[0])
      , hHeaderfmt= argu(headerfmt, opt, "headerfmt", update(hCellfmt, "u"))
      , hHeaderfmts= argu(headerfmts, opt, "headerfmts",[]) // [ 2,fmt, 3, fmt ]
      , aHeaderfmts = [ for(ci= cis)                // col-specific header formats
                        updates(  hHeaderfmt
                                 , [ h(hColfmts,ci) //hCellfmt 
                                   , h( hHeaderfmts,ci) 
                                 ]
                               )
                      ]    
      //------------------------------------------------ 
      , __header= arg(header, opt, "header")
      , headerTexts = __header==true || __header==1? arr[0] : header

     , data = __header==true || __header==1 ? slice(arr,1) : arr
      
     , headerstr= !__header?"":
                  _tr(join( [ for(ci=cis) 
                              _tag("th"
                                  ,_hl( headerTexts[ci]
                                      , aHeaderfmts[ci]
                                      , opt=["p", "&nbsp;"]
                                      )
                                  )       
                            ]
                      ))   
     , datastr= join( [ for(row=data) 
                       _tr( join(
                           [ for(ci = cis)
                             _td(_hl( row[ci]
                                    , update( hCellfmt, h(hColfmts,ci))
                                    , opt=["p", "&nbsp;"]    
                                ))
                           ]
                         ))
                      ])
     , allstr = str(headerstr,datastr)              
     )  
     
  _tag("table", allstr, a=a, s="font-family:Courier new;border:solid 1px black;")
);

    _table=["_table", "header,data,indent=0", "str", "Array, String",
        , "Given header which is a hash:
        ;; 
        ;;  header: ['name',/format/, 'sex', /format/, 'tel',/format/ ]
        ;;
        ;; Return a string representing the data in a table format
        ;;
        ;; -- indent could be an int (1,2,3...) or a str like '---'
        "];    
    
    function _table_test( mode=MODE, opt=[] )=
    (
        //let( mode= 100*(mode>99?1:0)+ mode%10 ) // disable testing 
        //echo(mode=mode)   
        let( a1= transpose( [ range(1,5), [for(i=range(5))i/3]] )
           , a2= transpose( [ range(1,5), [for(i=range(5))i/3], [for(i=range(5)) sqrt(i)]] )
           )
        doctest( _table,
        [ "var a1"
        , "var a2"
	, ""
	, str( "> _table(a1)<br/>", _table( a1, a="border=0") )
	, ""
	, str( "> _table(a2)<br/>", _table( a2 ) )
        
        , ""  
        , "// Header"
        , "// header could be 1,true, or a set of data"
        , ""
        , str( "> _table( a1, header=1 )<br/>", _table( a1, header=1))
        , ""
        , str( "> _table( a1, header=[\"a\",\"cd\"])<br/>"
             , _table( a1, header=["a","cd"] ))
          
        , ""
        , "// <u>cellfmt</u>: set common fmt" 
        , ""
        , str( "> _table( a1, header=1, cellfmt='d=1;w=8;bc=lightblue' )<br/>"
              , _table( a1, header=1, cellfmt="d=1;w=8;bc=lightblue") )
              
        , ""
        , "// <u>colfmts</u>: set fmt(s) of specific cols" 
        , ""
        , str( "> _table( a2, header=1, cellfmt='d=1;w=8', colfmts=[1,'w=3;d=0', 1, '%;d=0', 2,'w=15'] )<br/>"
              , _table( a2, header=1, cellfmt="d=1;w=8", colfmts=[0,"w=3;d=0",1, "%;d=0", 2,"w=15"]) )
              
              
        , ""      
        , "// <u>headerfmt</u>: set header fmt, which overwrites cellfmt" 
        , ""
        , str( "> _table( a1, header=1, cellfmt='d=1;w=8', headerfmt='d=0' )<br/>"
              , _table( a1, header=1, cellfmt="d=1;w=8", headerfmt="d=0") )
              
        , ""
        , str( "> _table( a1, header=1, cellfmt='d=1;w=8', headerfmt='bc=khaki' )<br/>"
              , _table( a1, header=1, cellfmt="d=1;w=8",  headerfmt="bc=khaki", escHTML=0) )
        , ""
        , "// <u>headermts</u>: set fmt(s) of specific headers" 
        , ""
        , str( "> _table( a2, header=1, cellfmt='d=1;w=8', colfmts=[1,'w=4;d=0', 2,'w=15'], headerfmts=[2,'d=5'] )<br/>"
              , _table( a2, header=1, cellfmt="d=1;w=8", colfmts=[0,"w=4;d=0",2,"w=15"], headerfmts=[2,"d=5"]) )

        //-------------------------------------------
        
        
        ],mode=mode, opt=opt, scope=["a1",a1, "a2",a2] )
        
    ); 

module _table( arr
             , header // set true to set the first line of data as header
             , cellfmt    // common format -- Refer to _f for fmt patterns
             , headerfmt  // common header format
             , headerfmts=[]                
             , colfmts = []                
             //, cellj="|" // if given join cells of a row w/ it
             //, rowj="<br/>"   // if given, join cells of a row w/ cellj, and rows w/ rowj
             , rowwraps="||" 
             , escHTML                   
             , opt=[] 
             )
{
  echo(_table( arr     = arr
             , header   = header 
             , cellfmt  = cellfmt
             , headerfmt =headerfmt
             , headerfmts=headerfmts                
             , colfmts = colfmts                
            // , cellj= cellj
            // , rowj=  rowj
             , rowwraps=rowwraps 
             , escHTML=escHTML                   
             , opt=       opt 
             )
      );
}  

//echo( str( "<table>"
//         , "<tr><td style='border-width:1px;'>abc</td><td>def</td></tr>"
//         , "<tr><td>ghi</td><td>jkl</td></tr>"
//         , "</table>"
//         ));

function _tag(tagname, t="", s="", a="")=
(
  //echo(_s("In tag, tagname=\"{_}\", s=\"{_}\", a=\"{_}\"", [tagname, s,a]) )
  str( "<", tagname
      , a?str(" ", a," "):""
      , s==""?"":str(" style='", s, "'")
      , ">", t, "</", tagname, ">")
);

function _div (t, s="", a="")=_tag("div", t, s, a);
function _span(t, s="", a="")=_tag("span", t, s, a);
//function _tablist(t, s="", a="")=_tag("table", t, s, a);
function _pre(t, s="", a="") =_tag("pre", t
                  , str(
                      "font-family:Courier New;margin-top:5px;margin-bottom:0px;"
                    , s)
                  , a);
function _code (t, s="", a="")=_tag("code", t, s, a);
function _tr   (t, s="", a="")=_tag("tr", t, s, a);
function _td   (t, s="", a="")=_tag("td", t, s, a);
function _b   (t, s="", a="")=_tag("b", t, s, a);
function _i   (t, s="", a="")=_tag("i", t, s, a);
function _u   (t, s="", a="")=_tag("u", t, s, a);
function _sup (t, s="", a="")=_tag("sup", t, s, a);
function _sub (t, s="", a="")=_tag("sub", t, s, a);
function _ssup (base, base_s="", t, s="", a="")=_span(str(base, _tag("sup", t, s, a)),s=base_s);
function _ssub (base, base_s="", t, s="", a="")=_span(str(base, _tag("sub", t, s, a)),s=base_s);
function _h1  (t, s="", a="")= _tag("h1", t, str("margin-top:0px;margin-bottom:0px;",s), a); 
function _h2  (t, s="", a="")= _tag("h2", t, str("margin-top:0px;margin-bottom:0px;",s), a); 
function _h3  (t, s="", a="")= _tag("h3", t, str("margin-top:0px;margin-bottom:0px;",s), a); 
function _h4  (t, s="", a="")= _tag("h4", t, str("margin-top:0px;margin-bottom:0px;",s), a);  
function ______(x="",label="-",s="",a="")= 
              _mono(str(repeat( label,20) ," ", _b(x), " "
                       , repeat( label,20))
                   , s=str("font-size:18px;",s));

module _div (t, s="", a=""){ echo(_tag("div", t, s, a));}
module _span(t, s="", a=""){ echo(_tag("span", t, s, a));}
//module _tablist(t, s="", a=""){ echo(_tag("table", t, s, a));}
module _code (t, s="", a=""){ echo(_tag("code", t, s, a));}
module _tr   (t, s="", a=""){ echo(_tag("tr", t, s, a));}
module _td   (t, s="", a=""){ echo(_tag("td", t, s, a));}
module _b   (t, s="", a=""){ echo(_tag("b", t, s, a));}
module _i   (t, s="", a=""){ echo(_tag("i", t, s, a));}
module _u   (t, s="", a=""){ echo(_tag("u", t, s, a));}
module _sup (t, s="", a=""){ echo(_tag("sup", t, s, a));}
module _sub (t, s="", a=""){ echo(_tag("sub", t, s, a));}
module _ssup (base,base_s="", t, s="", a=""){ echo(_sup(base,base_s, t, s, a));}
module _ssub (base,base_s="", t, s="", a=""){ echo(_sub(base,base_s, t, s, a));}
module _h1  (t, s="", a=""){ echo( _h1(t, s, a));} 
module _h2  (t, s="", a=""){ echo( _h2(t, s, a));} 
module _h3  (t, s="", a=""){ echo( _h3(t, s, a));} 
module _h4  (t, s="", a=""){ echo( _h4(t, s, a));}  
module _hr(){ echo("<hr/>");}
module ______(x="", label="-", s="",a=""){ echo(______(x,label));}


function _color(t,c,s="") = _span(t, s=str("color:",isarr(c)?toHtmlColor(c):c,";",s));
function _bcolor(t,c,s="") = _span(t, s=str("background-color:"
                                            ,isarr(c)?toHtmlColor(c):c
                                            ,";", s));
function _red  (t,s="") = _span(t, s=str("color:red;",s));
function _green(t,s="") = _span(t, s=str("color:green;",s));
function _blue (t,s="") = _span(t, s=str("color:blue;",s));
function _gray (t,s="") = _span(t, s=str("color:gray;",s));
function _orange (t,s="") = _span(t, s=str("color:orange;",s));
function _darkorange (t,s="") = _span(t, s=str("color:darkorange;",s));
function _purple (t,s="") = _span(t, s=str("color:purple;",s));
function _teal (t,s="") = _span(t, s=str("color:teal;",s));
function _violet (t,s="") = _span(t, s=str("color:violet;",s));
function _olive (t,s="") = _span(t, s=str("color:olive;",s));
function _darkgreen (t,s="") = _span(t, s=str("color:darkgreen;",s));
function _darkkhaki (t,s="") = _span(t, s=str("color:darkkhaki;",s));
function _khaki (t,s="") = _span(t, s=str("color:khaki;",s));
function _magenta (t,s="") = _span(t, s=str("color:magenta;",s));
function _midnightblue (t,s="") = _span(t, s=str("color:midnightblue;",s));
function _brown (t,s="") = _span(t, s=str("color:brown;",s));
function _white (t,s="") = _span(t, s=str("color:white;",s));

//function _tab(t,spaces=4)= replace(t, repeat("&nbsp;",spaces)); // replace tab with spaces
function _cmt(t,s="")= _span(t, s=str("color:darkcyan;",s));   // for comment
function _mono(t,s="")=  _span( t, s=str(s,";font-family:Courier New"));
module Red(t=1){ color("red",t) children(); }
module Green(t=1){ color("green",t) children(); }
module Blue(t=1){ color("blue",t) children(); }
module Darkblue(t=1){ color("Darkblue",t) children(); }
module Purple(t=1){ color("purple",t) children(); }
module Teal(t=1){ color("teal",t) children(); }
module Olive(t=1){ color("Olive",t) children(); }
module Gray(t=1){ color("Gray",t) children(); }
module Orange(t=1){ color("orange",t) children(); }
module Magenta(t=1){ color("Magenta",t) children(); }
module Darkkhaki(t=1){ color("Darkkhaki",t) children(); }
module Khaki(t=1){ color("Khaki",t) children(); }
module Midnightblue(t=1){ color("Midnightblue",t) children(); }
module Brown(t=1){ color("brown",t) children(); }
module Gold(t=1){ color("Gold",t) children(); }
module Black(t=1){ color("Black",t) children(); }
module White(t=1){ color("White",t) children(); }
module Pink(t=1){ color("Pink",t) children(); }
module Lightblue(t=1){ color("Lightblue",t) children(); }
module White(t=1){ color("white",t) children(); }

module _cmt(t,s){ echo(_cmt(t,s));}
module _red(t,s){ echo(_red(t,s));}
module _green(t,s){ echo(_green(t,s));}
module _blue(t,s){ echo(_blue(t,s));}
module _gray(t,s){ echo(_gray(t,s));}
module _orange(t,s){ echo(_orange(t,s));}
module _darkorange(t,s){ echo(_darkorange(t,s));}
module _darkkhaki(t,s){ echo(_darkkhaki(t,s));}
module _khaki(t,s){ echo(_khaki(t,s));}
module _purple(t,s){ echo(_purple(t,s));}
module _teal(t,s){ echo(_teal(t,s));}
module _violet(t,s){ echo(_violet(t,s));}
module _olive(t,s){ echo(_olive(t,s));}
module _darkgreen(t,s){ echo(_darkgreen(t,s));}
module _magenta(t,s){ echo(_magenta(t,s));}
module _midnightblue(t,s){ echo(_midnightblue(t,s));}
module _brown(t,s){ echo(_brown(t,s));}
module _white(t,s){ echo(_white(t,s));}

module _color(t,c,s=""){ echo(_color(t,c,s));}
module _bcolor(t,c,s=""){ echo(_bcolor(t,c,s));}
module _mono(t,s=""){ _color(_mono(t,s));}
// Format the function/module argument
function _arg(s)= //replace(
                  _span( s?str("<i>"
                    ,replace(replace(s,"''","&quot;"),"\"","&quot;")
					,"</i>"):""
					, "color:#444444;font-family:ubuntu mono")
		  //,"<", "&lt;" 			
                  //)
                  ;
          
    _arg=["_arg", "s", "string", "String, Format"
        , "Format the function/module argument
        "];    
  
    
    function _arg_test( mode=MODE, opt=[] )=
    (  //echo(mode=mode<10?mode:mode>20? (100+ mode%10):mode%10)
        //let(mode=102)
        doctest( _arg,
        [
        
          [ _arg("a,b")
          , "<span style='color:#444444;font-family:ubuntu mono'><i>a,b</i></span>"
          , "'a,b'" 
          ]
        
        , "// Note how the the quoting symbols of ''d'' is typed."
        ,   [ _arg("a,b=''d'',c=\"xx\"")
              , "<span style='color:#444444;font-family:ubuntu mono'><i>a,b=\"d\",c=\"xx\"</i></span>"
              //, "<span style='color:#444444;font-family:ubuntu mono'><i>a,b=\"d\",c=\"xx\"</i></span>"
            ,"'a,b=&#39;&#39;d&#39;&#39;,c=\"xx\"'"
            , ["mode", mode<10?mode:mode>20? (100+ mode%10):mode%10 
              , "//", "Difficult to test. Just show result"]
           // , ["showhtml",true,"highlight",false,"//", "showhtml doesn't work?"] 
            ]
        
        ],mode=mode, opt=opt 
        )
    ); 
    
module help(fname)
{
	echo_( " | {_} ({_}) |=> {_} (group: {_} )", fname[0] );	
	
	echoblock(fname[1]);
}
//help(trueor2);

/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_string_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_string.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_string_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_string", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      _arg_test( mode, opt )
    //, _b_test( mode, opt ) // to-be-coded
    //, _bcolor_test( mode, opt ) // to-be-coded
    //, _blue_test( mode, opt ) // to-be-coded
    //, _brown_test( mode, opt ) // to-be-coded
    //, _cmt_test( mode, opt ) // to-be-coded
    //, _code_test( mode, opt ) // to-be-coded
    //, _color_test( mode, opt ) // to-be-coded
    //, _colorPts_test( mode, opt ) // to-be-coded
    //, _darkgreen_test( mode, opt ) // to-be-coded
    //, _darkkhaki_test( mode, opt ) // to-be-coded
    //, _darkorange_test( mode, opt ) // to-be-coded
    //, _div_test( mode, opt ) // to-be-coded
    , _f_test( mode, opt )
    , _fmt_test( mode, opt )
    , _fmth_test( mode, opt )
    //, _gray_test( mode, opt ) // to-be-coded
    //, _green_test( mode, opt ) // to-be-coded
    , _h_test( mode, opt )
    //, _h1_test( mode, opt ) // to-be-coded
    //, _h2_test( mode, opt ) // to-be-coded
    //, _h3_test( mode, opt ) // to-be-coded
    //, _h4_test( mode, opt ) // to-be-coded
    //, _i_test( mode, opt ) // to-be-coded
    //, _khaki_test( mode, opt ) // to-be-coded
    //, _magenta_test( mode, opt ) // to-be-coded
    //, _midnightblue_test( mode, opt ) // to-be-coded
    //, _mono_test( mode, opt ) // to-be-coded
    //, _olive_test( mode, opt ) // to-be-coded
    //, _orange_test( mode, opt ) // to-be-coded
    //, _pre_test( mode, opt ) // to-be-coded
    //, _purple_test( mode, opt ) // to-be-coded
    //, _red_test( mode, opt ) // to-be-coded
    , _s_test( mode, opt )
    , _s2_test( mode, opt )
    //, _span_test( mode, opt ) // to-be-coded
    //, _tab_test( mode, opt ) // to-be-coded
    , _tablist_test( mode, opt )
    //, _tag_test( mode, opt ) // to-be-coded
    //, _td_test( mode, opt ) // to-be-coded
    //, _teal_test( mode, opt ) // to-be-coded
    //, _tr_test( mode, opt ) // to-be-coded
    //, _u_test( mode, opt ) // to-be-coded
    //, _violet_test( mode, opt ) // to-be-coded
    , asc_test( mode, opt )
    //, ichar_test( mode, opt ) // to-be-coded
    , lcase_test( mode, opt )
    , parsedoc_test( mode, opt )
    , repeat_test( mode, opt )
    , replace_test( mode, opt )
    , sci_test( mode, opt )
    , slice_test( mode, opt )
    , split_test( mode, opt )
    , splits_test( mode, opt )
    //, toHtmlColor_test( mode, opt ) // to-be-coded
    , trim_test( mode, opt )
    , ucase_test( mode, opt )
    ]
);


  