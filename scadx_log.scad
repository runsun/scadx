/*
  scadx_log.scad   
  
  A logging system
*/ 

//include <scadx.scad>

ISLOG = 1;

LOG_INDENT_UNIT= 2;
LOG_LAYER= [0, "black"];
LOG_LEVEL= 10;


LOG_PROMPT= _span("&nbsp;","font-family:arial");
LOG_PROMPT_B= "<b>&#9487;</b>";    
LOG_PROMPT_E= "<b>&#9495;</b>";    
// http://graphemica.com/search?q=box+drawing

LOG_LAYERS= [ "1", [1, "darkblue"]   // <layerName>, [ <level>, <color> ]
            , "2", [2, "darkViolet"]
            , "3", [3, "blue"]
            , "4", [4, "teal"]
            , "5", [5, "maroon"]
            , "6", [6, "darkgreen"]
            , "7", [7, "indigo"]
            , "8", [8, "#566573"]
            , "9", [9, "darkkhaki"]
            , "10", [10, "darkblue"]   // <layerName>, [ <level>, <color> ]
            , "11", [11, "darkViolet"]
            , "12", [12, "blue"]
            , "13", [13, "teal"]
            , "14", [14, "maroon"]
            , "15", [15, "darkgreen"]
            , "16", [16, "indigo"]
            , "17", [17, "#566573"]
            , "18", [18, "darkkhaki"]
            ];           
//
// LOG_DEFAULTS could be/ should be re-written at RT
//
LOG_PARAMS_INIT = 
           [ "LOG_LEVEL", LOG_LEVEL  // decide up to which level to log/display
           , "mode", 1
           , "add_ind", 0
           , "prompt", LOG_PROMPT
           , "debug", 0
           , "hLayers", LOG_LAYERS
           ];

function log_( ss, layer, mode, add_ind, prompt, debug, hParams)=
(
   // log_(): a base func to be extended into more practical ones for 
   //         each spacial use case
   // 
   // ss could be str or arr
   // If ss = arr: 
   //   mode=0: no highlight
   //   mode=1: highlight keys
   //   mode=2: highlight vals 
   //
   // layer --- decides which layer (and thus, indent and color) the func/modules extended from 
   //           this log_ would echo. Could be:
   //   -- int = will be converted to str for the keys in the default LOG_LAYERS
   //   -- str = key for data in hParams.layers
   //   -- arr = [level, color]  ( skip hParams entirely and set a run-time level/color )
   // 
   // hParams:
   //
   //   [ "LOG_LEVEL", 10
   //   , "mode", 1
   //   , "add_ind", 0
   //   , "prompt", " "
   //   , "debug", 0
   //   , "hLayers", [ "1", [1, "darkblue"]
   //                , "2", [2, "darkViolet"]
   //                , "3", [3, "blue"]
   //                , "4", [4, "teal"]
   //                , "5", [5, "maroon"]
   //                , "6", [6, "darkgreen"]
   //                , "7", [7, "indigo"]
   //                , "8", [8, "orangered"]
   //                , "9", [9, "darkkhaki"]]
   //                ]
   //   ]
   !ISLOG?"": 
   let( ss= und(ss, "")
      , layer = layer==undef? LOG_LAYER: isint(layer)?str(layer):layer
      , _hParams = hParams? update( LOG_PARAMS_INIT, hParams ): LOG_PARAMS_INIT
      , hParams = update( _hParams
                        , ["hLayers", update( hash( LOG_PARAMS_INIT, "hLayers")
                                            , hash(_hParams,"hLayers") ) ]
                        )
      , lvl_col = isstr(layer)?hashs( hParams, ["hLayers", layer], LOG_LAYER ):layer
      , mode= und(mode, hash(hParams, "mode", 1))
      , add_ind = und(add_ind, hash(hParams, "add_ind", 1) )
      , prompt = und(prompt, hash(hParams, "prompt", LOG_PROMPT))
 
      , level = lvl_col[0]+ add_ind
//       , pre = level<2?""
//         :( [ for( i= [1:len(num(level))-1] ) //range(layer) ) 
//                  _mono( str( repeat("&nbsp;",i)
//                            , _span( LOG_PROMPT// prompt
//                              , prompt==LOG_PROMPT?
//                                 str( "background:", LOG_LAYERS[i*2][1])
//                                 :str("color:",LOG_LAYERS[i*2][1])
//                            )
//                       ))
//               ]
//             )      
      , LOG_LEVEL = hash( hParams, "LOG_LEVEL", 0 )
      , isdebug= und(debug, hash( hParams, "debug", 0))
      , debug = isdebug? [ "ss", ss
                         , "layer", layer
                         , "mode", mode
                         , "add_ind", add_ind
                         , "lvl_col", lvl_col
                         , "level", level
                         , "pre", pre
                         , "LOG_LEVEL", LOG_LEVEL
                         , "hParams", hParams
                         ] : undef
      )
    isdebug? _fmth(debug)  
    :level>LOG_LEVEL? undef
    :(let( color = lvl_col[1]
        , level = lvl_col[0]*LOG_INDENT_UNIT+ add_ind
        , indent = _mono(repeat( "&nbsp;", level))
        , _prompt= _mono(
                         str( prompt==LOG_PROMPT?" ":""
                             , _span(  prompt
                                     , prompt==LOG_PROMPT?
                                               str( "background:", color)
                                               :str("color:",color)
                                     )
                             , " " 
                             )
                       )   
              
       , _s = isarr(ss)?join([ for(i=[1:2:len(ss)])
                              let(k= ss[i-1], v= ss[i])
                              str( mode==1?_b(k):k
                                  , "=", isstr(v)?"\"":""
                                  , mode==2? _b(v):v 
                                  , isstr(v)?"\"":"")
                            ]
                          , ", ")
                      : ss 
       , s= replace( _s, "\n"
                    , str( "<br>"
                         , _mono( repeat("&nbsp;",6)) //=> the spaces of "ECHO: "
                         , indent, _prompt, " " 
                         ) 
                  ) 
       )
      _color(str( //pre
                 indent, _prompt,  prompt==LOG_PROMPT?s:_u(s)), color)
     )  
           
);
         
function log_b( ss, layer, mode, add_ind, prompt, debug, hParams=[] )=
                log_( ss, layer, mode, add_ind, prompt
                , debug, hParams=update( hParams, ["prompt", LOG_PROMPT_B]) );
                
function log_e( ss, layer, mode, add_ind, prompt, debug, hParams=[])=
               log_( ss, layer, mode, add_ind, prompt
               , debug, hParams=update( hParams, ["prompt", LOG_PROMPT_E]) );

//-------------------------------------------------------     

module log_( ss,layer, mode, add_ind, prompt, debug, hParams=[] )
           { 
             s=  log_( ss, layer, mode, add_ind, prompt, debug,  hParams); 
             if(s) echo(s);
           }      
 
module log_b( ss,layer, mode, add_ind, prompt, debug, hParams=[] )
           {  s= log_b( ss, layer, mode, add_ind, prompt, debug, hParams=update( hParams, ["prompt", LOG_PROMPT_B])); 
              if(s) echo(s);}                 

module log_e( ss,layer, mode, add_ind, prompt, debug, hParams=[] )
           {  s= log_e( ss, layer, mode, add_ind, prompt, debug, hParams=update( hParams, ["prompt", LOG_PROMPT_E])); 
              if(s) echo(s);}       

//==========================================================


/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_log_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo scadx -fn scadx_log.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages:
    //
    // results = get_scadx_log_test_results(...);
    //
    // (1) for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_log", results, showEach = false );
    //

    [
      //log__test( mode, opt ) // to-be-coded
    //, log_b_test( mode, opt ) // to-be-coded
    //, log_e_test( mode, opt ) // to-be-coded
    ]
);

function get_scadx_log_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_log.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_log_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_log", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      //log__test( mode, opt ) // to-be-coded
    //, log_b_test( mode, opt ) // to-be-coded
    //, log_e_test( mode, opt ) // to-be-coded
    ]
);



