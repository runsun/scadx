include <../scadx.scad>

ISLOG=1;

// https://en.wikipedia.org/wiki/Vector_space

function _r(n) = n==undef? _mono(repeat(_SP, 9))
                        : _mono(str( _u(str("Rule.", n )), ":"));
  

//. --------- 1onPQ ---------

//1onPQ_on_line_PQ();
module 1onPQ_on_line_PQ()
{
   echom("1onPQ_on_line_PQ");
   AR= R_ARROW;
   _h2("For P,Q, study M = iP + jQ, i+j=n");
   _h2(_red( join(
      [ str(_r("22-1"), " i+j=1", R_ARROW, " M is on the line PQ (like: 1.2P-0.2Q )")
      , ""
      , str(_r("22-2"), " i+j=1 & i,j>0", R_ARROW, " M is inside PQ segment (like: 0.2P+0.8Q)")
      , ""
      , str(_r("22-3"), " i,j are 'pulling force' for P, Q, respectively")
      , str(_r(), R_DBL_ARROW, " If i>1, M goes beyond PQ segment on the P side.")
      ],_BR )));
      
   _h2("In matrix form:");
   _h2(join([
             _formula([ 
                        "M = iP + jQ"
                      , R_ARROW
                      , _matrix( [ ["i","j"]], "w=2" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ])  
 
            ,_formula([ 
                        _vector(["2P-Q","-3P+4Q", "0.8P+0.2Q"],"w=10")
                      , "="  
                      ,  _matrix( [ [2,-1], [-3,4], [.8,.2]], "d=1;w=5" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "R", "S", "T" ],"w=2" ) 
                      //, R_ARROW
                      //, "RS is the center line, and passes thru O"
                      ]) 
           , _formula([R_ARROW, "R,S,T are all on line PQ 'cos i+j=1"])            
           , _formula([R_ARROW, "T is inside PQ segment 'cos i,j>0"])            
       ])
   );    
      
   _h2("In matrix form:");
   _h2(join([
             _formula([ 
                        "M = iP + jQ"
                      , R_ARROW
                      , _matrix( [ ["i","j"]], "w=2" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ])  
 
            ,_formula([ 
                        _vector(["2P-Q","-3P+4Q", "0.8P+0.2Q"],"w=10")
                      , "="  
                      ,  _matrix( [ [2,-1], [-3,4], [.8,.2]], "d=1;w=5" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "R", "S", "T" ],"w=2" ) 
                      //, R_ARROW
                      //, "RS is the center line, and passes thru O"
                      ]) 
           , _formula([R_ARROW, "R,S,T are all on line PQ 'cos i+j=1"])            
           , _formula([R_ARROW, "T is inside PQ segment 'cos i,j>0"])            
       ])
   );    
   
   /*
    i    j      i-j   i+j  (i-j)/(i+j)
   --------- ------------- -----------
   0.5  0.5      0     1      0
   1     0       1     1      1
   0     1       -1    1      -1
   -0.1  1.1   -1.2    1     -1.2
   0.2  0.8     -0.6   1     -0.6
   0.1   0.4    -0.3  0.5    -0.6
   
    
   
   */
   P= [4, 3, 0]; Q=[1, 4, 0];
   
   P= [4, 3, 0]; Q=[1, 4, 0];
   M = 1.5*P-0.5*Q;
   
   R= P/2; S= Q/2;
   
   Red() Dim( [Q,P,R], Label="i+j=1, i,j>=0", Link=true);
   
   Green() Dim( [P, 1.5*P-0.5*Q,R], Label=["text","i+j=1, j<0","halign","left"]
       , spacer=0.5,Link=["steps",["x",0.5]]);
   
   Blue() Dim( [-0.5*P+1.5*Q,Q, R], Label=["text","i+j=1, i<0","halign","left"]
       , spacer=0.5,Link=["steps",["x",0.5]]);
   
   Line([1.5*P-0.5*Q, -0.5*P+1.5*Q]);
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
   Purple() MarkPt(M, "M = 1.5*P-0.5*Q");     
   
   
}


//1onPQ();
module 1onPQ()
{
   echom("1onPQ");
   AR= R_ARROW;
   _h2("For P,Q, study M = iP + jQ, i+j=n");
   //_h2("Case: i+j= n ");
   _h2(_red( join(
      [ str(_r("22-4"), " n= i+j", R_ARROW, " how far away M is from [0,0,0]")
      ,  str(_r(), " w=(i-j)/(i+j)", R_ARROW, " how far away M is from the center line")
      ],_BR )));
 
   
   P= [4, 3, 0]; Q=[1, 4, 0];
   
   P= [4, 3, 0]; Q=[1, 4, 0];
   M = 1.5*P-0.5*Q;
   
   R= P/2; S= Q/2;
   
   Red(){
     Dim( [Q,P,R], Label="i+j=1, i,j>=0", Link=true);
   
     Dim( [P, 1.5*P-0.5*Q,R], Label=["text","i+j=1, j<0","halign","left"]
         , spacer=0.5,Link=["steps",["x",0.5]]);
   
     Dim( [-0.5*P+1.5*Q,Q, R], Label=["text","i+j=1, i<0","halign","left"]
         , spacer=0.5,Link=["steps",["x",0.5]]);
         
                
     MarkPt(M, "M = i*P+(1-i)*Q", Ball=0);   
     Line([1.5*P-0.5*Q, -0.5*P+1.5*Q]);

     MarkPt( 0.8*P+0.2*Q, "0.8*P+0.2*Q, w= (0.8-0.2)/1 = 0.6"
           , Link=["steps",["t",[0.15,-.65,0], "t",[1.5,-.5,0] ]]  );
     
   }  
   
   
   Green(){
     Dim( [-0.5*P/2+1.5*Q/2,Q/2, O], Label=["text","i+j=0.5, i<0","halign","left"]
         , spacer=0.1, Guides=["len",.35], Link=["steps",["t",[0.5,.25,.0]]]);
     
     Dim( [Q/2,P/2,O], Label="i+j=0.5, i,j>=0", Link=true);
   
     Dim( [P/2, 1.5*P/2-0.5*Q/2,O], Label=["text","i+j=0.5, j<0","halign","left"]
         , spacer=0.15,Link=["steps",["t",[0.5,0.25,0]]]);
   
     MarkPts([P,Q]/2, false);   
     Line([1.5*P-0.5*Q, -0.5*P+1.5*Q]/2);
     MarkPt( 0.75*P-0.25*Q, "= i*P+(0.5-i)*Q", Ball=0);
     
     MarkPt( 0.4*P+0.1*Q, "0.4*P+0.1*Q, w= (0.4-0.1)/0.5 = 0.6"
           , Link=["steps",["t",[0.25,-1,0] ]]  );
   
   }  
   
   
   Blue(){
     Dim( [-0.5*P+2*Q, 1.5*Q, O], Label=["text","i+j=1.5, i<0","halign","left"]
         , spacer=0.1, Guides=["len",.35], Link=["steps",["t",[0.5,.25,.0]]]);
     
     Dim( [1.5*Q,1.5*P,O], Label="i+j=1.5, i,j>=0", Link=true);
   
     Dim( [1.5*P, 2*P-0.5*Q,O], Label=["text","i+j=1.5, j<0","halign","left"]
         , spacer=0.2,Link=["steps",["t",[0.5,0.25,0]]]);
   
     MarkPts(1.5*[P,Q], false);   
     Line([ 2*P-0.5*Q, -0.5*P+2*Q]);
     MarkPt( 2*P-0.5*Q, " = i*P+(1.5-i)*Q", Ball=0);

     MarkPt( 1.2*P+0.3*Q, "1.2*P+0.3*Q, w= (1.2-0.3)/1.5 = 0.6"
           , Link=["steps",["t",[0.15,-.65,0], "t",[1.5,-.5,0] ]]  );
   
   }  
   
   Red() { Line( [O, 1.1*(P+Q)] ); 
           MarkPt( (P+Q), "Center Line", Ball=0);}
   Line( [1.75*Q,O,1.75*P] );
   DashLine( [O, 1.4*P+ 1.4*Q/4 ] );
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );

}

                         
//. --------- 2onPQ ---------
//2onPQ_parallel_to_PQ();
module 2onPQ_parallel_to_PQ()
{
   echom("2onPQ_parallel_to_PQ");
   AR= R_ARROW;
   _h2("For P,Q, study 2 pts [R,S]= n*[P,Q] = [nP, nQ]");
   //_h2("Case: i+j= n ");
   _h2(_red( join(
      [ str(_r("21-1"), " Line RS is ||_to PQ")
      , ""
      , str(_r("21-2"), " n = d(O,nP)/d(O,P) = d(O,nQ)/d(O,Q)")
      , str(_r(), R_DBL_ARROW, " n = len_nP/len_P = len_nQ/len_Q")
      ],_BR )));
   
   _h2("In matrix form:");
   _h2(join([
             _formula([ 
                        "n*[P,Q]= [nP,nQ] = [ [n,0]*[P,Q], [0,n]*[P,Q] ]"
                     ])
            ,_formula([ R_ARROW
                      , _matrix( [ ["n","0"], ["0","n"]], "w=2" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ])  
            ,_formula([ _matrix( [ ["n","0"], ["0","n"]], "w=2" )
                      , R_ARROW
                      , "a scalar matrix" 
                      ])  
       ])
   );
   
   
   P= [4, 3, 0]; Q=[1, 4, 0];
   S= -0.6*P+ 1.1*Q;
   R= 0.4*P + 0.1*Q;
   
   Red(){ 
   	MarkPt( P/2, "0.5*P");   
   	MarkPt( Q/2, "0.5*Q");   
   	Line([0.75*P-0.25*Q, -0.25*P+0.75*Q]);
   	MarkPt( 0.75*P-0.25*Q, " = 0.5*[P,Q]", Ball=0);
   	}
   	
   Green(){ 
   	MarkPt( 0.75*P, "0.75*P");   
   	MarkPt( 0.75*Q, "0.75*Q");   
   	Line([1*P-0.25*Q, -0.25*P+1*Q]);
   	MarkPt( 1*P-0.25*Q, " = 0.75*[P,Q]", Ball=0);
   	}
   	
   Blue(){ 
   	MarkPt( 1.25*P, "1.25*P");   
   	MarkPt( 1.25*Q, "1.25*Q");   
   	Line([1.5*P-0.25*Q, -0.25*P+1.5*Q]);
   	MarkPt( 1.5*P-0.25*Q, " = 1.25*[P,Q]", Ball=0);
   	}   
      
   Line( [1.5*Q,O,1.5*P] );
   Line([1.5*P-0.5*Q, -0.5*P+1.5*Q]);
   MarkPt( 1.5*P-0.5*Q, "= [P,Q]", Ball=0);
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
   //Purple() MarkPt(M, "M = 1.5*P-0.5*Q");   
}


//2onPQ_parallel_to_PQ_2();
module 2onPQ_parallel_to_PQ_2()
{
   echom("2onPQ_parallel_to_PQ_2");
   AR= R_ARROW;
   _h2("For P,Q, study 2 pts R= iP+jQ, S=kP+lQ");
   //_h2("Case: i+j= n ");
   _h2(_red( join(
      [ str(_r("21-3"), " i+j = k+l = n", R_ARROW, " Line RS is ||_to PQ")
      , str(_r(), R_DBL_ARROW, " n = len_nP/len_P = len_nQ/len_Q")
      ],_BR )));
   
   _h2( _s("Two easy ways to create PQ-parallel lines:
   <br/> {_} n*[P,Q] = [nP, nQ]
   <br/> {_} [ iP+jQ, jP+iQ ]
     ", [R_DBL_ARROW,R_DBL_ARROW] ));

   
   _h2("In matrix form:");
   _h2(join([
             _formula([ 
                        "[R,S] = [ [i,j]*[P,Q], [k,l]*[P,Q] ]"
                     ])
            ,_formula([ R_ARROW
                      , _matrix( [ ["i","j"], ["k","l"]], "w=2" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "R", "S" ],"w=2" ) 
                      ])  
            ,_formula([ "i+j=k+l"
                      , R_ARROW
                      , "RS || PQ" 
                      ]) 
            ,_formula([ "[ P+3Q, 5P-Q ]=", R_ARROW
                      , _matrix( [ [1,3],[5,-1]], "d=0;w=3" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "R", "S" ],"w=2" ) 
                      ])  
                       
       ])
   );   
   P= [4, 3, 0]; Q=[1, 4, 0];
   S= -0.6*P+ 1.1*Q;
   R= 0.4*P + 0.1*Q;
   
   Green(){ 
   	MarkPt( 0.75*P, "0.75*P");   
   	MarkPt( 0.75*Q, "0.75*Q");   
   	Line([1*P-0.25*Q, -0.25*P+1*Q]);
   	MarkPt( 1*P-0.25*Q, " = 0.75*[P,Q]", Ball=0);
   	MarkPt( -0.25*P+Q, "-0.25*P+1*Q"
   	      , Link=["steps",["t",[0.25,-1,0], "x", 0.2]] );
   	MarkPt( 0.5*P+0.25*Q, "0.5*P+0.25*Q"
   	      , Link=["steps",["y",-1, "x",0.5]] );
   	}
   	
   Blue(){ 
   	MarkPt( 1.25*P, "1.25*P");   
   	MarkPt( 1.25*Q, "1.25*Q");   
   	Line([1.5*P-0.25*Q, -0.25*P+1.5*Q]);
   	MarkPt( 1.5*P-0.25*Q, " = 1.25*[P,Q]", Ball=0);
   	}   
      
   DashLine( [1.5*Q,O,1.5*P] );
   Line([1.5*P-0.5*Q, -0.5*P+1.5*Q]);
   MarkPt( 1.5*P-0.5*Q, "= [P,Q]", Ball=0);
   Purple() {
     MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
     MarkPt(O, Label=["text","O","scale",2, "indent",0.2]);
   }     
}


//2onPQ_center_line();
module 2onPQ_center_line()
{
   echom("2onPQ_center_line");
   _h2("For P,Q, study M= iP+jQ ");
   _h2( str("Case: i=j ", R_DBL_ARROW, " Center Line"));
   _h2(_red( join(
      [ str(_r("23-1"), " i=j", R_ARROW, " i(P+Q)=j(P+Q) is on center line [O,C] (O=origin, C= midpt of PQ = (P+Q)/2)")
      , str(_r("23-2"), " i + j = 2*i = OM/OC")
      ],_BR )));
   
   _h2("In matrix form:");
   _h2(join([
             _formula([ 
                        "[R,S] = [ [i,j]*[P,Q], [k,l]*[P,Q] ]"
                     ])
            ,_formula([ R_ARROW
                      , _matrix( [ ["i","j"], ["k","l"]], "w=2" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "R", "S" ],"w=2" ) 
                      ])  
            ,_formula([ "i=j: R is on center line" 
                      ])  
            ,_formula([ "k=l: S is on center line" 
                      ])  
            ,_formula([ 
                        "[2P+2Q,-P-Q]="
                      ,  _vector( [ "2 2", "-1 -1" ], "w=5" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "R", "S" ],"w=2" ) 
                      , R_ARROW
                      , "RS is the center line, and passes thru O"
                      ])  
       ])
   );   
   
   
   P= [4, 3, 0]; Q=[1, 4, 0];

   C = (P+Q)/2;
   M= 0.2*(P+Q);
   N= 0.75*(P+Q);
       
   Red(){ Line([O, 0.8*(P+Q)]);
          MarkPt( 0.65*(P+Q), Ball=0, Label=["text", " = i(P+Q)=j(P+Q), i=j" ] );
        }
   
   Green(){
        MarkPt(M, " M= 0.2*(P+Q), OM/OC = 0.4 ");
        Dim( [O,M,C,P], Label=["chainRatio",true], Link=true);
        }
   
   Blue(){
        MarkPt(N, " N= 0.75*(P+Q), ON/OC = 1.5");
        Dim( [O,C,N, P], Label=["chainRatio",true], spacer=0.7, Link=true);
        }
   
   Purple() {
          MarkPts( [P,Q], Label=["text","PQ","scale",2
                   , "indent",0.2],r=0.05, Ball=["r",0.1] );
          MarkPt(C, Label=["text", " C = 0.5*(P+Q)", "scale",1.5]); 
          MarkPt(O, "O");
          } 
}


//2onPQ_parallel_to_OQ();
module 2onPQ_parallel_to_OQ()
{
   echom("2onPQ_parallel_to_OQ");
   _h2("For P,Q on XY plane, study M, N in the form of iP+jQ");
   _h2("Case: same i: M= iP+jQ, N= iP+kQ");
   _h2(_red( join(
      [ str(_r("24-1"), " Fixed i", R_ARROW, " [M,N] is ||_to [O,Q]" )
      , str(_r(), " Fixed j", R_ARROW, " [M,N] is ||_to [O,P]" )
      ],_BR )));
   _h2(str(R_ARROW, "Line [ 2P+3Q, 4P+3Q ] is || OP 'cos same j"));          

   _h2("In matrix form:");
   _h2(join([
             _formula([ 
                        "[M,N] = [ [i,j]*[P,Q], [k,l]*[P,Q] ]"
                     ])
            ,_formula([ R_ARROW
                      , _matrix( [ ["i","j"], ["i","k"]], "w=2" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "M", "N" ],"w=2" ) 
                      , R_DBL_ARROW
                      , "Fixed i: MN || OQ "
                      ])  
            ,_formula([ R_DBL_ARROW
                      , "[ 2P+3Q, 2P-Q ]="
                      , _matrix( [ [2,3], [2,-1]], "d=0;w=3" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "M", "N" ],"w=2" ) 
                      , R_DBL_ARROW
                      , "Fixed i: MN || OQ "
                      ])  
            ,_formula([ R_ARROW
                      , _matrix( [ ["i","j"], ["k","j"]], "w=2" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "M", "N" ],"w=2" ) 
                      , R_DBL_ARROW
                      , "Fixed j: MN || OP "
                      ]) 
            ,_formula([ R_DBL_ARROW
                      , "[ 3P-2Q, 2P-2Q ]="
                      , _matrix( [ [3,-2], [2,-2]], "d=0;w=3" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "M", "N" ],"w=2" ) 
                      , R_DBL_ARROW
                      , "Fixed j: MN || OP "
                      ])                        
       ])
   );    
   
   P= [4, 3, 0]; Q=[1, 4, 0];

   C = (P+Q)/2;
   M= 0.2*P+ 0.4*Q;
   N= 0.2*P+ 1*Q;
   K= 0.2*P;
   L= 0.2*P-0.2*Q;
   I= 0.2*P+0.8*Q;
       
   Red(){ Line( [ .2*P + 1.2*Q, .2*P-.2*Q ] );
          MarkPt( .2*P + 1.2*Q, Ball=0, Label=["text", " = 0.2*P + j*Q" ] );
          MarkPt( K, " K = 0.2*P");
          MarkPt( L, " L = 0.2*P- 0.2*Q");
          MarkPt( M, " M = 0.2*P+ 0.4*Q");
          MarkPt( N, " N = 0.2*P+ 1*Q");
          MarkPt( I, Label=" I = 0.2*P+ 0.8*Q", Link=true);
        }
   DashLine( [Q, N] );
   
   Green(){
   
      MarkPt(  -0.2*P+1.85*Q , "= -0.2*P + j*Q");
      DashLine( [ -0.2*P, -0.2*P+ 2*Q] );
   }
   
   
   Purple() {
          MarkPts( [P,Q], Label=["text","PQ","scale",2
                   , "indent",0.2],r=0.05, Ball=["r",0.1] );
          //MarkPt(C, " C = 0.5*(P+Q)"); 
          MarkPt(O, "O");
          }       
  Line([1.85*P, O, 1.85*Q]);
  MarkPt( 1.5*Q, " = 0P + jQ (i=0) ", Ball=0 );
}


//2onPQ_line_passing_O();
module 2onPQ_line_passing_O()
{
   echom("2onPQ_line_passing_O");
   AR= R_ARROW;
   _h2("For P,Q on XY plane, study 2 pts R=iP+jQ, S=kP+lQ");
   //_h2("Case: i+j= n ");
   _h2(_red( join(
      [ str(_r("25-1"), "i/j=k/l", R_ARROW, "R is on line OS")
      , str(_r("25-2"), "j=ni", R_ARROW, "iP+niQ for all n are on a line passing O")
      ],_BR )));
   
   _h2("In matrix form:");
   _h2(join([
             _formula([ 
                        "[M,N] = [ [i,j]*[P,Q], [k,l]*[P,Q] ]"
                     ])
            ,_formula([ R_ARROW
                      , _matrix( [ ["i","j"], ["k"," kj/i"]], "w=5" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "M", "N" ],"w=2" ) 
                      , R_DBL_ARROW
                      , "MN passes thru O "
                      ])  
            ,_formula([ R_DBL_ARROW
                      , "[[3P,6Q],[P,2Q]] =" 
                      , _matrix( [ [3,6], [1,2]], "d=0;w=3" )
                      , _vector( [ "P", "Q" ],"w=2" ) 
                      ,"="
                      , _vector( [ "M", "N" ],"w=2" ) 
                      , R_DBL_ARROW
                      , "MN passes thru O "
                      ])   
       ])
   ); 
   
   P= [4, 3, 0]; Q=[1, 4, 0];
   S= -0.6*P+ 1.1*Q;
   R= 0.4*P + 0.1*Q;
   
   Red(){ 
   	MarkPt( 0.1*P+0.3*Q, "0.1*P+0.3*Q");   
   	MarkPt( 0.2*P+0.6*Q, "0.2*P+0.6*Q");   
   	MarkPt( 0.3*P+0.9*Q, "0.3*P+0.9*Q");   
   	MarkPt( 0.4*P+1.2*Q, Label=["text"," = i*P + 3 i*Q", "scale",1.5], Ball=0);   
   	DashLine([ O, 0.5*P+1.5*Q]);
   	}
   	
   Green(){ 
   	MarkPt( 0.8*P-0.4*Q, "0.0*P-0.4*Q");   
   	MarkPt( 1.2*P-0.6*Q, "1.2*P-0.6*Q");   
   	MarkPt( 1.6*P-0.8*Q, "1.6*P-0.8*Q");   
   	MarkPt( 2.4*P-1.2*Q, "2.4*P-1.2*Q");   
   	MarkPt( 2.8*P-1.4*Q, Label=["text"," = i*P - (i/2)*Q", "scale",1.5], Ball=0);   
   	DashLine([ O, 3*P-1.5*Q]);
   	}
   	
   Line( [1.5*Q,O,1.5*P] );
   Line([1.5*P-0.5*Q, -0.5*P+1.5*Q]);
   Purple() {
      MarkPts( [P,Q], Label=["text","PQ","scale",2
               , "indent",0.2],r=0.05, Ball=["r",0.1] );
      //MarkPt(C, " C = 0.5*(P+Q)"); 
      MarkPt(O,["text","O","scale",2]);
      } 
}


//PQ_new_quadrants();
module PQ_new_quadrants()
{
   echom("PQ_new_quadrants");
   AR= R_ARROW;
   _h2("For P,Q on XY plane, study the sign of i,j in iP+jQ = M");
   //_h2("Case: i+j= n ");
   _h2(_red( join(
      [ str(_r("26-1"), "OP,OQ are basis axes of a new coord D", AR
                     , " D= {P,Q}")
      , str(_r(), "4 new quadrants")
      , str(_r(), str("[i,j]",_sub("D")), AR, "coord of M on basis of D")
      ],_BR )));

   
   P= 4*X+3*Y; Q= X+4*Y;
   R= P/2; S= Q/2;
   T= 2*P; U=2*Q;
    
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
   pl0= [T,O,U];
   Purple() 
   MarkPt(.2*P+1.2*Q, Ball=0, Label=["text","Quad 0, i>0, j>0, ij=++","scale",3]);
   Gold(0.1) Plane( pl0 );
   
   pl1= [ U,O, -1.3*P, -2*P+3.5*Q] ;
   Red() MarkCenter(pl1, Label=["text"," Quad 1, i<0, j>0, ij=-+","scale",3]);
   //MarkPts(pl1);
   Red(0.1) Plane( pl1 );
   
   pl2= [ -1.5*P, O, -1.5*Q] ;
   Green() MarkCenter(pl2, Label=["text","Quad 2, i<0, j<0, ij=--","scale",3]);
   //MarkPts(pl2);
   Green(0.1) Plane( pl2 );

   pl3= [ 1.5*P, 3.25*P-1.75*Q, -2*Q, O] ;
   Blue() MarkCenter(pl3, Label=["text","Quad 3, i>0, j<0, ij=+-","scale",3]);
   //MarkPts(pl3);
   Blue(0.1) Plane( pl3 );
   
   Line([T,O,U],r=0.1);   
   Black() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
}

function markijk(i,j,k)=
(
  k? _ss("{_:c=red}P+{_:c=red}Q+{_:c=red}R", [i,j,k] ) 
  :_ss("{_:c=red}P+{_:c=red}Q", [i,j] )
);

function markijkl(i,j,k,l)=
(
  //_ss("[{_:c=red}P+{_:c=red}Q,{_:red}R+{_:red}S]", [i,j,k,l] ) 
  _s("[{_}P+{_}Q,{_}R+{_}S]", [_red(i),_red(j),_red(k),_red(l)] ) 
);

vPQ = _vector(["P","Q"], "w=1");
vPQR = _vector(["P","Q", "R"], "w=1");
vPQRS = _vector(["P R","Q S"], "w=3");

//PQ_summary();
module PQ_summary()
{
   echom("PQ_summary()");
   AR= R_ARROW;
   _h2("--- For P,Q on XY plane ---<hr/>");
   //======================================================
   _h1( str("S=", markijk("i","j")));
   _h2(join( [
           _formula( [ "S=", markijk("i","j")
                     , "=", _red("[i j]")
                     ,"*[P,Q]="
                     , _red("[i j]"), vPQ
                     , "=", _red("[i j]"), _vector([ "Px Py Pz", "Qx Qy Qz"], "w=8" )
                     , "=", _s( "[{_}Px+{_}Qx,{_}Py+{_}Qy,{_}Pz+{_}Qz]"
                              , [for(x=["i","j","i","j","i","j"])_red(x)] )
                   ])
          
          ,_BR2
          
          , _red(str("Check i,j : i=j ", AR, " S on center line"))         
          ,_formula([ "2P+2Q =",_red("[2 2]")
                  , _vector(["P","Q"], "w=1")
                  ])
          
          , _BR2
          
          , _red(str("Check (i-j)/(i+j) = r ", AR, "Spread horizontally (along PQ dir) <br/>"))         
          
          , _mono(replaces(
                  "      |     |     |
                  /------Q-----C-----P----                
                  /      |     |     |
                  / r&lt;-1 | r&lt;0 | r>0 | r>1 
                  /     r=-1  r=0   r=1      
          	 ", ["/",_BR, " ",_SP]))
          	 
          , _BR2
           
          , _red("Check i+j : i+j=1 ")
          ,_formula([ "2P-Q =",_red("[2 -1]")
                  , _vector(["P","Q"], "w=1")
                   , AR, _red("i+j=1"), AR, "S on PQ line"
                  
                  ])
          ,_formula([ markijk(0.3,0.7)
                   ,"=", _red("[.3 .7]")
                  , _vector(["P","Q"], "w=1")
                   , AR, _red("i+j=1 & i,j>0"), AR, "S in PQ line segment"
                  
                  ])
          
          , _BR2
          
          , _red(str("Check i+j : i+j = n",AR, " Scale vertically (as shown below) :",_BR))                           
          
            , _mono(replaces(
          "   \\           /
          '    \\         /  n>1
          ' ----Q-------P------- n=1                 
          '      \\     /
          '       \\   /   0 &lt;n&lt;1   
          '        \\ / 
          '         O---- n=0
          '        / \\ 
          '       /   \\    n&lt;0
	 ", ["'",_BR, " ",_SP]))
                  
      ]));
   
    //=====================================================
    _h3("<hr/>");
    _h1( str( "M= ", markijk("i","j"), _BR
            , "N= ", markijk("k","l")
            ));        
    _h2(join( [
           _formula( [ _vector(["M","N"],"w=1"),"="
                        , _vector( [ markijk("i","j")
                                   , markijk("k","l")
                                   ] )
                     , "=", _vector( [ "i j", "k l"], cellfmt="c=red;w=3" ), vPQ
                   ])
          ,""
          , _red(str("i+j=k+l =n ", AR, " MN || PQ", AR, "n decides how far MN is from O"))  
          ,_formula( [ _vector(["M","N"],"w=1"),"="
                     , _matrix( [ [0.3,1], [-0.7, 2]], cellfmt="c=red;w=4;d=1" )
                          , vPQ
                   ]) 
          ,""
          , _red(str("i=k", AR, " MN || OQ"))  
          ,_formula( [ _vector(["M","N"],"w=1"),"="
                     , _matrix( [ [3,1], [3, 2]], cellfmt="c=red;w=2;d=0" )
                          , vPQ
                          , "|| OQ"
                     ])  
          , _mono(replaces(
                  "   
                  '    \\ #       /  
                  ' ----Q-#-----P------- 
                  '      \\ #   /
                  '       \\ # /   
                  '        \\ # 
                  '         O #
                  '        / \\ #
                  
        	 ", ["'",_BR, " ",_SP]))
	  ,""             
          , _red(str("j=l", AR, " MN || PO"))  
          ,_formula( [ _vector(["M","N"],"w=1"),
                     , "=", _matrix( [ [1,2], [3, 2]], cellfmt="c=red;w=2;d=0" )
                          , vPQ
                          , "|| OP"
                   ]) 
            , _mono(replaces(
          "   
          '    \\     #   /  
          ' ----Q---#---P-------                  
          '      \\ #   /
          '       #   /  
          '      # \\ / 
          '     #   O
          '    #   / \\ 
          '  
	 ", ["'",_BR, " ",_SP]))                   
	  ,""             
          , _red(str("i/j = k/l", AR, " MN passes O"))  
          ,_formula( [ _vector(["M","N"],"w=1")
                    , "=", _matrix( [ [1,2], [3, 6]], cellfmt="c=red;w=2;d=0" )
                          , vPQ
                   ]) 
            , _mono(replaces(
          "           #
          '    \\     #   /  
          ' ----Q----#--P-------                  
          '      \\   # /
          '       \\ # /  
          '        \\#/ 
          '         O
          '        /#\\ 
          '  
	 ", ["'",_BR, " ",_SP]))     
	               
       ],_BR));
                              
         //=============================================================== 
//    _h3("<hr/>");
//    _h1( str("S=", markijk("i","j","k")));
//   _h2(join( [
//      _formula( [ "S=", markijk("i","j","k")
//                , "= ", _red("[i j k]")
//                , vPQR 
//                ] )
//      ]));
//         //=============================================================== 
//    _h3("<hr/>");
//    _h1( _ss("S= {_}P+{_}Q+{_}R<br/>T= {_}P+{_}Q+{_}R<br/>U= {_}P+{_}Q+{_}R" 
//           , [_red("i"),_red("j"),_red("k")
//             , _red("a"),_red("b"),_red("c")
//             , _red("d"),_red("e"),_red("f")
//             ]
//           ));
//
//    _h2(join( [
//    
//       _formula( [ _vector(["S","T","U"],"w=1"), "=", 
//                 , "=", _vector( [ "i j k","a b c","d e f"], cellfmt="c=red;w=7" )
//                      , vPQR
//                 ]) 
//          
//      ],_BR));
      


}

//. pt2_dev 
module F_2_central_line_xx()
{
   echom("F_2_central_line");
   _h1("M=aP+bQ");
   
   P= 4*X+3*Y; Q= X+4*Y;
   R= P/2; S= Q/2;
   T= 2*P; U=2*Q;
    
   Line([T,O,U]);
   
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
   Red()
   {
   Dim( [Q,P,R], Label="a+b=1, a,b>=0", Link=true);
   
   Line([1.5*P-0.5*Q, -0.5*P+1.5*Q]);
   Dim( [P, 1.5*P-0.5*Q,R], Label=["text","a+b=1, b<0 (1.5P-0.5Q) ","halign","left"]
       , spacer=0.5,Link=["steps",["x",0.5]]);
   Dim( [-0.5*P+1.5*Q,Q, R], Label=["text","a+b=1, a<0 (-0.5P+1.5Q)","halign","left"]
       , spacer=0.5,Link=["steps",["x",0.5]]);
   
   //Line([R,S]);
   //Line([0.9*R,0.9*S]);
   Line([1.5*R-0.5*S,-0.5*R+1.5*S]);
   
   Dim( [S,R,O], Label="a+b=k<1, a,b>=0 (0.25P+0.25Q)", Link=true);
   //R1=  
   //Dim( [R, 1.5*R-0.5*S,P]
   Dim( [R, .75*P-0.25*Q,P]
       , Label=["text","a+b=k, b<0 (0.75P-0.25Q) ","halign","left"
               ,"actions",["rotx",180]
               ]
       , Link=true); //["steps",["t",[0.25,-0.125,0]]]);
   Dim( [-0.25*P+0.75*Q,S, Q]
        , Label=["text","a+b=k, a<0 (-0.25P+0.75Q)","halign","right"
                ,"actions",["rotx",180]
                ]
       , Link=true);
   
   
   }
   
   V= 0.25*Q;
   W= 2*P+ 0.25*Q;
   
   Green()
   {
     Line([V,W]);
     MarkPt( 1.5*P+.25*Q, Link=["steps",["t",[0.3,-0.3,0],"x",1]], Label="?P+0.25Q", Ball=false);
   }
   
   D=(P+Q)/2;
   E=.75*P+.75*Q;
   F= P+Q;
   
   G= 0.25*P+0.5*Q;
   H= 0.5*P + Q;
   I= 0.75*P + 1.5*Q;
   
   Blue()
   {
     MarkPts([O,D,E,F], Label=["text",["","","0.75*P + 0.75Q"," P + Q"]]);
     MarkPt( (E+F)/2, Link=["steps",["t",[0.3,-0.3,0],"x",.2]], Label="aP+aQ", Ball=false);
   
     MarkPts([O,G,H,I], Label=["text",["","","0.5*P + Q"," 0.75P + 1.5Q"]]);
     MarkPt( .2*H+.8*I, Link=["steps",["t",[0.3,-0.3,0],"x",1]], Label="2aP+aQ", Ball=false);
   }

   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
   Purple() MarkPts( [R,S], Label=["text","RS","scale",1, "indent",0.2] );

   }


module dev_onlinePt_summary()
{
   echom("dev_onlinePt_summary");
   _h1("M=aP+bQ");
   
   P= 4*X+3*Y; Q= X+4*Y;
   R= P/2; S= Q/2;
   T= 2*P; U=2*Q;
    
   Line([T,O,U]);
   
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
   Red()
   {
   Dim( [Q,P,R], Label="a+b=1, a,b>=0", Link=true);
   
   Line([1.5*P-0.5*Q, -0.5*P+1.5*Q]);
   Dim( [P, 1.5*P-0.5*Q,R], Label=["text","a+b=1, b<0 (1.5P-0.5Q) ","halign","left"]
       , spacer=0.5,Link=["steps",["x",0.5]]);
   Dim( [-0.5*P+1.5*Q,Q, R], Label=["text","a+b=1, a<0 (-0.5P+1.5Q)","halign","left"]
       , spacer=0.5,Link=["steps",["x",0.5]]);
   
   //Line([R,S]);
   //Line([0.9*R,0.9*S]);
   Line([1.5*R-0.5*S,-0.5*R+1.5*S]);
   
   Dim( [S,R,O], Label="a+b=k<1, a,b>=0 (0.25P+0.25Q)", Link=true);
   //R1=  
   //Dim( [R, 1.5*R-0.5*S,P]
   Dim( [R, .75*P-0.25*Q,P]
       , Label=["text","a+b=k, b<0 (0.75P-0.25Q) ","halign","left"
               ,"actions",["rotx",180]
               ]
       , Link=true); //["steps",["t",[0.25,-0.125,0]]]);
   Dim( [-0.25*P+0.75*Q,S, Q]
        , Label=["text","a+b=k, a<0 (-0.25P+0.75Q)","halign","right"
                ,"actions",["rotx",180]
                ]
       , Link=true);
   
   
   }
   
   V= 0.25*Q;
   W= 2*P+ 0.25*Q;
   
   Green()
   {
     Line([V,W]);
     MarkPt( 1.5*P+.25*Q, Link=["steps",["t",[0.3,-0.3,0],"x",1]], Label="?P+0.25Q", Ball=false);
   }
   
   D=(P+Q)/2;
   E=.75*P+.75*Q;
   F= P+Q;
   
   G= 0.25*P+0.5*Q;
   H= 0.5*P + Q;
   I= 0.75*P + 1.5*Q;
   
   Blue()
   {
     MarkPts([O,D,E,F], Label=["text",["","","0.75*P + 0.75Q"," P + Q"]]);
     MarkPt( (E+F)/2, Link=["steps",["t",[0.3,-0.3,0],"x",.2]], Label="aP+aQ", Ball=false);
   
     MarkPts([O,G,H,I], Label=["text",["","","0.5*P + Q"," 0.75P + 1.5Q"]]);
     MarkPt( .2*H+.8*I, Link=["steps",["t",[0.3,-0.3,0],"x",1]], Label="2aP+aQ", Ball=false);
   }

   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
   Purple() MarkPts( [R,S], Label=["text","RS","scale",1, "indent",0.2] );

   }
//dev_onlinePt_summary();


module dev_onlinePt_summary3()
{
   echom("dev_onlinePt_summary3");
   _h1("M=aP+bQ");
   
   P= [4,3]*[X,Y]; //4*X+3*Y; 
   Q= [1,4]*[X,Y]; //X+4*Y;
   R= P/2; 
   S= Q/2;
   T= 2*P; U=2*Q;
    
   pq=[P,Q]; 
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
   n=50;
   
   pl0= [T,O,U];
   MarkCenter(pl0, Label=["text","Quad 0, a>0, b>0","scale",3]);
   Gold(0.1) Plane( pl0 );
   Black()
     for(i=range(n))
     {
        r2= rands(0,1.2,2);
        MarkPt( r2*[P,Q], Label=false);
     }
   
   pl1= [ U,O, -1.3*P, -1*P+2.5*Q] ;
   MarkCenter(pl1, Label=["text"," Quad 1, a<0, b>0","scale",3]);
   //MarkPts(pl1);
   Red(0.1) Plane( pl1 );
   Red()
     for(i=range(n))
     {
        r2= rands(0,1.5,2);
        MarkPt( [-r2[0], r2[1]]*[P,Q], Label=false);
     }
     
     
   pl2= [ -1.5*P, O, -1.5*Q] ;
   MarkCenter(pl2, Label=["text","Quad 2, a<0, b<0","scale",3]);
   //MarkPts(pl2);
   Green(0.1) Plane( pl2 );
   Green()
     for(i=range(n))
     {
        r2= rands(0,1.2,2);
        MarkPt( -1*r2*[P,Q], Label=false);
     }
     
     

   pl3= [ 1.5*P, 3.25*P-1.75*Q, -2*Q, O] ;
   MarkCenter(pl3, Label=["text","Quad 3, a>0, b<0","scale",3]);
   //MarkPts(pl3);
   Blue(0.1) Plane( pl3 );
   Blue()
     for(i=range(n))
     {
        r2= rands(0,2,2);
        MarkPt( [r2[0], -r2[1]]*[P,Q], Label=false);
     }
     
   
   Line([T,O,U],r=0.1);   
   Black() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.05, Ball=["r",0.1] );
}
//dev_onlinePt_summary3();


module dev_onlinePt()
{
   echom("onlinePt");
   _h2("aP+bQ=M, a&lt;1, b&lt;1, a+b=1");
   
   P= 4*X+4*Y;
   Q= X+4*Y;
   pq = [P,Q];
   
   Arrow( [O,P] );
   Arrow( [O,Q] );

   R= (P+O)/2;
   S= (Q+O)/2;
   MarkPts( [R,S], "RS", Line=false );
   Red(.2) Arrow( [O,R], r=0.05);
   Green(.3) Arrow( [O,S], r=0.05);

   M = (P+Q)/2;
   MarkPt(M, "M");
   Green(.4) Arrow( [R,M], r=0.05);
   
   dim = ["spacer",0.3, "Link",["stem",0.1], "Label", ["chainRatio",true]];            
   
   Gray(.5)
   {
   Dim( [Q,M,P,S], opt=dim); // spacer=0.3, Link=true, Label=["chainRatio",true] );
   Dim( [O,S,Q,M], opt=dim); // spacer=0.3, Link=true, Label=["chainRatio",true] );
   Dim( [O,R,P,M], spacer=0.3, Link=true
        , Label=["chainRatio",true, "actions",["rot",[0,180,180]]] );
   }
   
   echo(_span( "mid pt = M = 0.5*(P-O) + 0.5*(Q-O) = (P+Q)/2", s="font-size:20px;color:red" ));
   _blue(_span("1) (P+Q)/2 is the midpoint between P and Q", s="font-size:20px;" ));
   //------------------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
}
//dev_onlinePt();


module dev_onlinePt2()
{
   echom("dev_onlinePt2");
   _h2("aP+bQ, a+b=1, a&lt;1, b&lt;1");
   
   P= 4*X+4*Y; Q= X+4*Y;
   Line( [P,O,Q] );

   module get_onlinePt( a_ratio=.75, Label="A", color="red")
   {
     A1= P*a_ratio;
     A2= Q*(1-a_ratio);
     MarkPts( [A1,A2], [str(Label,"1"), str(Label,"2")],, Line=false );
     Red(.2) Arrow( [O,A1], r=0.05);
     Green(.3) Arrow( [O,A2], r=0.05);
     
     M = A1+A2;
     Blue(.3) Arrow( [A1,M], r=0.05 );
     Purple(.3) Arrow( [A2,M], r=0.05 );

     MarkPt(M, Label);
     echo(_span(_s("{_} = {_}*P + {_}*Q", [Label, a_ratio, 1-a_ratio])
               , s=str("font-size:20px;color:",color) )); 
   }
   
   Red(.3) get_onlinePt(.75, "A", "red");
   Green(.4) get_onlinePt(.25, "B", "green");

   dim = ["spacer",0.2, "Link",["stem",0], "Label", ["chainRatio",true]];            
   Red(.2)
   {
     Dim( [Q,(.75*P+.25*Q),P,O], opt=dim); 
     Dim( [O, .75*P,P,Q], Label=["chainRatio",true, "actions",["rot",[0,180,180]]]);
   }                  
   Green(.3)
   {
     Dim( [Q,(.25*P+.75*Q),P,O], Link=0, spacer=0.8, Label=["chainRatio",true]); 
     Dim( [O,0.75*Q,Q,P], opt=dim );
   }
   
   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
      
   echo("-----------------------");
   _blue(_span("1) (P+Q)/2 = midpoint between P and Q", s="font-size:20px;color:blue" ));
   echo("-----------------------");
   _blue(_span("2) a,b in 'aP+bQ' are 'Pulling Force'"
               , s="font-size:20px;"));        
   _blue(_span("  ==> 0.75P + 0.25Q' means P has 3 times of Pulling Force than Q has"
               , s="font-size:20px;"));  
   _blue(_span("'3) When a+b=1 and both a,b&lt;1, aP+bQ bounces between P and Q."
               , s="font-size:20px;"));  
                           
}
//dev_onlinePt2();


module dev_onlinePt3()
{
   echom("dev_onlinePt3");
   _h2("aP+bQ, a+b=1, a&lt;1, b&lt;1");
   
   P= 4*X+4*Y; Q= X+4*Y;
   Line( [P,O,Q] );

   module get_onlinePt( a_ratio=.75, Label="A", color="red")
   {
     A1= P*a_ratio;
     A2= Q*(1-a_ratio);
     //MarkPts( [A1,A2], [str(Label,"1"), str(Label,"2")],, Line=false );
     Red(.2) Arrow( [O,A1], r=0.05);
     //Green(.3) Arrow( [O,A2], r=0.05);
     
     M = A1+A2;
     Blue(.3) Arrow( [A1,M], r=0.05 );
     //Purple(.3) Arrow( [A2,M], r=0.05 );

     MarkPt(M, Label);
     echo(_span(_s("{_} = {_}*P + {_}*Q", [Label, a_ratio, 1-a_ratio])
               , s=str("font-size:20px;color:",color) )); 
   }
   
   Red(.3) get_onlinePt(-.25, "M", "red");
   //Green(.4) get_onlinePt(1.25, "B", "green");

   dim = ["spacer",0.2, "Link",["stem",0], "Label", ["chainRatio",true]];            
//   Red(.2)
//   {
//     Dim( [Q,(.75*P+.25*Q),P,O], opt=dim); 
//     Dim( [O, .75*P,P,Q], Label=["chainRatio",true, "actions",["rot",[0,180,180]]]);
//   }                  
   
   M= (-.25*P+1.25*Q);
   Green(.6)
   {
     Dim( [M,Q,O], Link=0, spacer=0.3); 
     Dim( [Q, P,O], Link=0, spacer=0.3); 
   }
   Blue() 
   MarkPt(M
         , Link=["steps", ["y",1.5,"x",1]]
         , Label="-0.25P + 1.25Q"); 
         
   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
      
   echo("-----------------------");
   _blue(_span("1) (P+Q)/2 = midpoint between P and Q", s="font-size:20px;color:blue" ));
   echo("-----------------------");
   _blue(_span("2) a,b in 'aP+bQ' are 'Pulling Force'"
               , s="font-size:20px;"));        
   _blue(_span("  ==> 0.75P + 0.25Q' means P has 3 times of Pulling Force than Q has."
               , s="font-size:20px;"));  
   _blue(_span("3) When a+b=1 and both a,b&lt;1, aP+bQ bounces between P and Q."
               , s="font-size:20px;"));  
   echo("-----------------------");
   _blue(_span("4) When a+b=1 and a&lt;0, a is a 'Pushing force', push M beyond Q."
               , s="font-size:20px;"));  
                           
}
//dev_onlinePt3();


module dev_onlinePt6xx()
{
   echom("dev_onlinePt6");
   _h2("aP+bQ, a+b=1, a&lt;1, b&lt;1");
   
   P= 4*X+3*Y; Q= X+4*Y;
   Line([P,O,Q]);
   
   A= -.5*P + 1.5*Q;
   B= .25*P + .75*Q;
   C= 1.25*P -.25*Q;
   
//   Red(){
//   MarkPt(A,r=0.1, Label=["text","-0.5*P + 1.5*Q","scale",1],Link=["steps",["y",.5, "x",.2]] );
//   MarkPt(B,r=0.1, Label=["text","0.25*P + 0.75*Q","scale",1],Link=["steps",["y",.5, "x",.2]] );
//   MarkPt(C,r=0.1, Label=["text","1.25*P -0.25*Q","scale",1],Link=["steps",["y",.5, "x",.2]] );
//   }
   
   //--------------------------------
   R= P/2;
   S= Q/2;
   
   D= -0.2*P + 0.7*Q;
   E= .25*P + 0.25*Q;
   F= .6*P - 0.1*Q;
   
//   Green(){
//   MarkPt(D,r=0.1, Label=["text","-0.2*P + 0.7*Q","scale",1],Link=["steps",["y",-1.8, "x",1.5]] );
//   MarkPt(E,r=0.1, Label=["text","0.25*P + 0.25*Q","scale",1],Link=["steps",["y",-.8, "x",.5]] );
//   MarkPt(F,r=0.1, Label=["text","0.6*P -0.1*Q","scale",1],Link=["steps",["y",.5, "x",1]] );
//   }
   
   //---------------------------
   Purple() MarkPts( [P,Q], Label=["text","PQ","scale",2, "indent",0.2],r=0.025 );
   Purple() MarkPts( [R,S], Label=["text","RS","scale",2, "indent",0.2],r=0.025 );
      
   echo("-----------------------");
   _blue(_span("1) (P+Q)/2 = midpoint between P and Q", s="font-size:20px;color:blue" ));
   echo("-----------------------");
   _blue(_span("2) a,b in 'aP+bQ' are 'Pulling Force'"
               , s="font-size:20px;"));        
   _blue(_span("  ( 0.75P + 0.25Q' means P has 3 times of Pulling Force than Q has)"
               , s="font-size:20px;"));  
   _blue(_span("  ==> When a+b=1 and both a,b&lt;1, aP+bQ bounces between P and Q."
               , s="font-size:20px;"));  
   echo("-----------------------");
   _blue(_span("3) When a+b=1 and a&lt;0, a is a 'Pushing force' from P, which pushes M beyond Q."
               , s="font-size:20px;")); 
   echo("-----------------------");
   _red(_span("4) When a+b=1, aP+bQ is a pt on the PQ line."
               , s="font-size:20px;"));   
   echo("-----------------------");
   _green(_span("5) For Lines PQ and L=[a1P+b1Q, a2P+b2Q], if a1+b1=a2+b2=k: "
               , s="font-size:20px;"));   
   _green(_span("... 5.1) L is parallel to [P,Q]"
               , s="font-size:20px;"));   
   _green(_span("...... ==> [2P+3Q, 3P+2Q] is a line parallel to PQ, 'cos 2+3=3+2=5 "
               , s="font-size:20px;"));   
   _green(_span("... 5.2) k is proportational to dist between O and PQ"
               , s="font-size:20px;"));   
   _green(_span("...... ==> let a1+b1=a2+b2=0.5, [a1P+b1Q, a2P+b2Q]= a line that is half way from O to PQ"
               , s="font-size:20px;"));   
   _green(_span("... 5.3) a>=1 & b>=1: M bounces between R and S where R,S are on line OP,OQ."
               , s="font-size:20px;"));   
   
   echo("-----------------------");
   _purple(_span("6) With a1+b1=a2+b2, let d1=abs(a1-b1), d2=abs(a2-b2):"
               , s="font-size:20px;"));   
   _purple(_span("... 6.1) if d1=d2= r, then r= dist(L)/dist(PQ)"
               , s="font-size:20px;"));   
               
   d= dist([P,Q]);            
   module parallel_lines(a1,b1,a2,b2, color)
   {
     R= a1*P+b1*Q;
     S= a2*P+b2*Q;
     
     color(color) MarkPts([R,S], Ball=false, r=0.1); 
    _color( _s( "[{_}P+{_}Q, {_}P+{_}Q]=> a1+b1={_}, a2+b2={_} ==> abs(a1-b1)={_}, abs(a2-b2)={_}, dist(L)/dist(PQ) = {_}" 
            , [ _b(a1), _b(b1), _b(a2), _b(b2)
              , _b(a1+b1), _b(a2+b2)
              , _b(abs(a1-b1)), _b(abs(a2-b2)), _b(dist([R,S])/d)
              ] )
            , color);
   }
   parallel_lines(.1,.3,.3,.1,"red");
   parallel_lines(.8,.4,.4,.8,"green");
   parallel_lines(1.5,-0.7,-0.7,1.5,"blue");

   _purple(_span("... 6.2) else,  abs(d1-d2)/2= dist(L)/dist(PQ)"
               , s="font-size:20px;"));   
   parallel_lines(1.8,-0.3,1,0.5,"purple");
   parallel_lines(1.8,-0.3,1,0.5,"purple");

}
//dev_onlinePt6xx();

//. quiz 

//quiz_vec_parallel_line();
module quiz_vec_parallel_line()
{
   echom("quiz_vec_parallel_line");
   _purple( "Given P,Q,R, draw a line TV that is parallel to PQ and <br/>
          the dist between TV~PQ is 1/4 of R~PQ"  );
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   //echo(pqr=pqr);
   Black() MarkPts( pqr, "PQR", Line=["closed",1]);
          
   T = [0.25,1]* [R-Q,P-Q]+Q;
   U = [0.25,0.75]* [R,P];
   V = [0.25,-.5]* [R-Q,P-Q]+Q;
   MarkPts([T,U,V],"TUV", Line=false);
   DashLine([T,V]);   
   _red("T = [0.25,1]* [R-Q,P-Q]+Q;");
   _green("U = [0.25,0.75]* [R,P];");
   _blue("V = [0.25,-.5]* [R-Q,P-Q]+Q;");
}

//quiz_2_vec_inner_triangle();
module quiz_2_vec_inner_triangle()
{
   echom("quiz_2_vec_inner_triangle");
   _red( "Given P,Q,R, draw an internal triangle having all sides parallel to its parent"  );
   _purple("To make sure they are parallel, a+b need to be constant");
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   //echo(pqr=pqr);
   Black() MarkPts( pqr, "PQR", Line=["closed",1]);
        
   Blue() 
   DashLine( [ [.6,.2]*[P-R,Q-R]+R
             , [.6,.2]*[Q-P,R-P]+P
             , [.6,.2]*[R-Q,P-Q]+Q  
             
             , [.6,.2]*[P-R,Q-R]+R
              
             ]);
}


//quiz_3_vec_outer_triangle();
module quiz_3_vec_outer_triangle()
{
   echom("quiz_3_vec_outer_triangle");
   _purple( "Given P,Q,R, draw an outer triangle"  );
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   //echo(pqr=pqr);
   Black() MarkPts( pqr, "PQR", Line=["closed",1]);
          
   Red() 
   DashLine( [ [1.2, -0.1]*[P-R,Q-R]+R
             , [1.2, -0.1]*[Q-P,R-P]+P
             , [1.2, -0.1]*[R-Q,P-Q]+Q  
             
             , [1.2, -0.1]*[P-R,Q-R]+R
              
             ]);
   
   pts2=[ for(i=[0:2]) 
          let( p= next(pqr, P,i)
             , q= next(pqr, P,i+1)
             , r= next(pqr, P,i+2)
             )
          each [[-0.3, 1.3]*[p-r,q-r]+r
               ,[-0.3, 1.3]*[p-q,r-q]+q ]
       ];
   Green() DashLine( range(pts2, cycle=1, returnitems=1), dash=0.005);
   
   Purple(0.1) Line( [[-0.3,1.3]*[Q,P], [-0.3,1.3]*[Q,R] ], r=0.1);
   
}


//quiz_4_vec_intsec_PQ_and_XYPlane();
module quiz_4_vec_intsec_PQ_and_XYPlane()
{
   echom("quiz_4_vec_intsec_PQ_and_XYPlane");
   _h2(_purple( "Given P,Q get the intersection point (pt) between PQ line and XY plane"  ));
   pq = randPts(2);
   //pq = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83]];       
   P= pq[0]; Q=pq[1];
   
   _h3( join( [ "pt= iP + jQ = [iPx+jQx, iPy+jPy, iPz+jQz]" 
              , "i+j=1 // because pt is on line PQ"
              ], _BR));
   _h3( join([ "pt.z = 0 (on XY plane) => iPz+jQz = 0"
             , "iPz = -jQz"
             , "j= -iPz/Qz = 1-i"
             , "i= 1/(1-Pz/Qz) = Qz/(Qz-Pz) "
             , "j= 1-Qz/(Qz-Pz) = -Pz(Qz-Pz)"
             , "pt = Qz/(Qz-Pz) * P  - Pz(Qz-Pz) * Q"
             ], _BR));
   
   i = Q.z/(Q.z-P.z);
   j = -P.z/(Q.z-P.z);
   pt = i*P + j*Q;
   
   
   Red() { MarkPt(pt,"pt");
          DashLine([C,pt]);
         }
         
   //echo(pqr=pqr);
   Black() MarkPts( pq, "PQ");
          
   C= abs(P.z)<abs(Q.z)?P:Q;
   
   _red(str("pt= ", pt));
      
}



//quiz_5_if_two_pts_same_side_of_line_PQ();
module quiz_5_if_two_pts_same_side_of_line_PQ()
{
   echom("quiz_5_if_two_pts_same_side_of_line_PQ");
   _h2(_purple( "Given 2 pts, R,S on plane OPQ, check if they on the same side of line PQ"  ));
   pq = randPts(2);
   Line([P,O,Q]);
   Black()MarkPts(pq,"PQ",r=0.05, Grid=3);
   
   
   //pq = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83]];       
   P= pq[0]; Q=pq[1];
   R= projPt(randPt(),[P,O,Q]);
   S= projPt(randPt(),[P,O,Q]);
    
   _h3( join( [ "R=aP+bQ, S=cP+dQ" 
              , "For a pt= iP+jQ, if i+j<1, pt is on the side where O is"
              ], _BR));
   _h3( join([ "To get a,b and c,d, use getNewCoord():"
             , "ab= getNewCoord( R, pq )"
             , "cd= getNewCoord( S, pq )"
             , "sameSide = sign(sum(ab)-1)==sign(sum(cd)-1)"
             ], _BR));
   
   ab= getNewCoord( R, pq );
   cd= getNewCoord( S, pq );
   sameSide = sign(sum(ab)-1)==sign(sum(cd)-1);
   
   Red(0.2) Plane( [R,P,Q] );
   color( sameSide?"red":"Green", 0.1) Plane( [S,P,Q] );
  
   Color(sameSide?"green":"red") MarkPts([R,S], "RS", Line=false);
      
}




//. --------- 1onPQR ---------

1onPQR_on_plane_PQR();
module 1onPQR_on_plane_PQR()
{
   echom("1onPQR_on_plane_PQR");
   AR= R_ARROW;
   P= [6, -1, 3]; Q=[4,7,4]; R=[1,1.5,5];
   pqr = [P,Q,R];
      
   _h2("For P,Q R, study pt S=iP+jQ+kR = [i,j,k]*[P,Q,R]");
   _h2(_red( join(
      [ 
        str(_r("32-1"), " if i+j+k=1", R_ARROW, " S is on plane PQR")
      , str(_r("32-2"), " if i+j+k=1 & i,j,k>0 ", R_ARROW, " S is inside triangle PQR")
      , str(_r("32-3"), " i,j,k are 'pulling force' of P, Q, R, respectively")
      ],_BR )));
   
//   _h2( _s("Two easy ways to create PQ-parallel lines:
//   <br/> {_} n*[P,Q,R] = [nP, nQ, nR]
//   <br/> {_} [ iP+jQ+kR, jP+iQ+kR, kP+iQ+jR ]
//     ", [R_DBL_ARROW,R_DBL_ARROW] ));
//   _h2(str("Easier to put coefs into a matrix: <br/>"
//          , _matrix( [ [i,j,k], [i,k,j], [k,j,i]], "d=1" )
//          ));  
   
   
   ijk_s = [0.5,0.2,0.3];
   ijk_t = [0.1,0.2,0.7];
   ijk_u = [-0.2, -0.1, 1.3];
   ijk_v = [0.7, 0.6, -0.3];
   
   Red() MarkPt( ijk_s*pqr, str("ijk=",ijk_s) ); 	
   Green() MarkPt( ijk_t*pqr, str("ijk=",ijk_t) ); 	
   Blue() MarkPt( ijk_u*pqr, str("ijk=",ijk_u) ); 	
   Black() MarkPt( ijk_v*pqr, str("ijk=",ijk_v) ); 	
   
   
   Red() DashLine([O,1.5*P] );
   Green() DashLine([O,1.5*Q] );
   Blue() DashLine([O,1.5*R] );
   
   Purple() MarkPts( pqr
            , Label=["text","PQR","scale",2, "indent",0.2],r=0.05
            , Ball=["r",0.1], Line=["closed",1] );
   Purple(0.1) Plane(pqr);
   r=0.8;  
   Blue(0.2) Plane( [ [r+1,-r/2,-r/2]*pqr
                    , [-r/2,r+1,-r/2]*pqr
                    , [-r/2,-r/2,r+1]*pqr
                    ] );  
                    
    _h2("In matrix form:");
   _h2(join([
             _formula([ "S=", markijk("i","j","k")
                      , "=dot prod=", _vector(["i","j","k"],"c=red;w=1", sup="T")
                      , vPQR
                      , "=[", _red("i j k"),"]"
                      , vPQR
                      ])  
 	    , _formula([
 	                markijk( -1,0.5,1.5 )
 	                , "=[", _red("-1 0.5 1.5"),"]"
                      , vPQR, R_ARROW, " on pqr plane"
 	              ])
 	    , _formula([
 	                markijk( 0.2,0.5,0.3 )
 	                , "=[", _red("0.2 0.5 0.3"),"]"
                      , vPQR, R_ARROW, " inside the PQR triangle"
 	              ])
 	                	
            ,_formula([ 
                       _vector( _ss("{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R"
                               , [ -1, 1.5,0.5, 0.5,2,-1.5, .5,.2,.3], fmt="c=red" )
                              )                        
                     , "="  
                      ,  _matrix( [ [-1, 1.5,0.5], [0.5,2,-1.5], [.5,.2,.3]]
                          , "d=1;w=5;c=red;" )
                      , vPQR 
                      ,"="
                      , _vector( [ "R", "S", "T" ],"w=1" ) 
                      //, R_ARROW
                      //, "RS is the center line, and passes thru O"
                      ]) 
           , _formula([R_ARROW, "R,S,T are all on plane PQR 'cos i+j+k=1"])            
           , _formula([R_ARROW, "T is inside PQR triangle 'cos i,j,k>0"]) 
           , _formula(["PQR = ", pqr])
           , _formula(["ijk_s= ", ijk_s, ", pt=", ijk_s*pqr ]) 
           , _formula(["solve3( transpose(pqr), pt) = ", solve3(transpose(pqr), ijk_s*pqr) ]) 
           , _formula(["tocoord( pt, pqr) = ", tocoord(ijk_s*pqr, pqr) ]) 
                       
       ])
   );         
   Red()
   MarkPts( [ [-1, 1.5,0.5], [0.5,2,-1.5], [.5,.2,.3]]*pqr
          , Label=["text", "STU", "scale",3]
          , r=0.05, Line=["closed",1]);
                
   
                
                         
}


//1onPQR_in_plane_PQR_spread(Rf=randPt());
module 1onPQR_in_plane_PQR_spread(Rf=O)
{
   echom("1onPQR_in_plane_PQR_spread");
   _h2("For P,Q R and S=iP+jQ+kR, study spread of S when i+j+k=1, i,j,k>0");
      
   lv=1;
   
   module test(Rf=Rf, ijksum=1, n=50, color="red" )
   {
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
       P= [8, -1, 3];//+Rf;
       Q=[4,5,4];//+Rf; 
       R=[1,1.5,5];//+Rf;
       pqr=[P,Q,R];
       stu=[gpt(1.5,P), gpt(1.5,Q), gpt(1.5,R)];
       S=stu[0];
       T=stu[1];
       U=stu[2];
     
       MarkPt(Rf, "Rf", color="black");
       Line(pqr, closed=1);
       Line( [P,Rf,R] );
       Line( [Q,Rf] );
     
       //log_(["pqr", pqr],lv);
       //log_(["stu",stu],lv );
   
     //n=50;
     pqr2=[for(p=pqr)p-Rf];
   
     for(i=range(n))
       {
          if(i<5)
          _color( str("ijksum=", ijksum,", ijk=",r3), color);
          _r3= rands(0,1,3);
          sum = sum(_r3);   
          r3= _r3*(ijksum/sum); 
          //echo(sum=sum(r3));
          color(color) MarkPt( r3*pqr2+Rf, Label=false);
          //Red() MarkPt( zipx(sign2,r3)*pqr2+Rf, Label=false);
       }
     //echo(ijksum*([1,1,1]*pqr2));  
     
     newpqr = [for(p=ijksum*(pqr-[Rf,Rf,Rf]))p+Rf];
     
     color(color)  
     MarkPt( newpqr[0], Label=["text", str("i,j,k>0, i+j+k=",ijksum), "scale",2]);
     MarkPts( newpqr, Line=["closed",1] );
        
     Black() MarkPts( pqr, Label=["text","PQR","scale",1, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );

   }// test
   
   test( Rf=Rf, ijksum=0.5, color="red",n=30);
   test( Rf=Rf, ijksum=1, color="green");
   test( Rf=Rf, ijksum=1.5, color="blue", n=100);
   
}


//1onPQR_on_plane_PQR_spread(Rf=randPt());
module 1onPQR_on_plane_PQR_spread(Rf=O)
{
   echom("1onPQR_on_plane_PQR_spread");
   _h2("For P,Q R and S=iP+jQ+kR, study spread of S when i+j+k=1 for any i,j,k");
   lv=1;
   
   module test(Rf=Rf, ijksum=1, n=200, color="red" )
   {
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
       P= [8, -1, 3];//+Rf;
       Q=[4,5,4];//+Rf; 
       R=[1,1.5,5];//+Rf;
       pqr=[P,Q,R];
       stu=[gpt(1.5,P), gpt(1.5,Q), gpt(1.5,R)];
       S=stu[0];
       T=stu[1];
       U=stu[2];
     
       MarkPt(Rf, "Rf", color="black");
       Line(pqr, closed=1);
       Line( [P,Rf,R] );
       Line( [Q,Rf] );
     
       //log_(["pqr", pqr],lv);
       //log_(["stu",stu],lv );
   
     //n=50;
     pqr2=[for(p=pqr)p-Rf];
   
     for(i=range(n))
       {
          _r3= rands(-1,1,3);
          factor = sum(_r3)/ijksum;
          //sum = sum(_r3);   
          r3= _r3/factor; //(ijksum/sum); 
          //echo(sum=sum(r3));
          color(color) MarkPt( r3*pqr2+Rf, Label=false);
          //Red() MarkPt( zipx(sign2,r3)*pqr2+Rf, Label=false);
          if(i<5)
           _color( str("ijksum=", ijksum," should = ", sum(r3), ", _r3=", _r3, ", ijk=",r3), color);
          

       }
     //echo(ijksum*([1,1,1]*pqr2));  
     
     newpqr = [for(p=ijksum*(pqr-[Rf,Rf,Rf]))p+Rf];
     
     color(color)  
     {MarkPt( newpqr[0], Label=["text", str("i,j,k>0, i+j+k=",ijksum), "scale",2]);
      MarkPts( newpqr, Line=["closed",1] );
     }   
     Black() MarkPts( pqr, Label=["text","PQR","scale",1, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );

   }// test
   
   test( Rf=Rf, ijksum=0.5, color="red");
   test( Rf=Rf, ijksum=1, color="green");
   test( Rf=Rf, ijksum=1.5, color="blue");
   
}


//1onPQR_on_side_plane();
module 1onPQR_on_side_plane()
{
   echom("1onPQR_on_side_plane");
   AR= R_ARROW;
   P= [6, -1, 3]; Q=[4,7,4]; R=[2,1.5,5];
   pqr = [P,Q,R];
      
   _h2("For P,Q R, study pt S=iP+jQ+kR = [i,j,k]*[P,Q,R]");
   _h2(_red( join(
      [ 
        str(_r("33-1"), " if i=0", R_ARROW, " S is on plane OQR. Same as j,k for S on POR,PQO, respectively.")
      ],_BR )));
   
   ijk_s = [0.5,0.2,0.3];
   ijk_t = [0.1,0.2,0.7];
   ijk_u = [-0.2, -0.1, 1.3];
   ijk_v = [1.1, .2, -0.3];
   
   Red() MarkPt( ijk_s*pqr, str("ijk=",ijk_s) ); 	
   Green() MarkPt( ijk_t*pqr, str("ijk=",ijk_t) ); 	
   Blue() MarkPt( ijk_u*pqr, str("ijk=",ijk_u) ); 	
   Black() MarkPt( ijk_v*pqr, str("ijk=",ijk_v) ); 	
   
   Purple(){   
   
         MarkPt( [-0.8, 0,1.8]*pqr, Label=["text","i < 0","scale",4], Ball=0 );
         MarkPt( [ 0.7,0.7,-0.4]*pqr, Label=["text","i > 0","scale",4], Ball=0 );
   
            MarkPts( pqr
                    , Label=["text","PQR","scale",2, "indent",0.2],r=0.05
                    , Ball=["r",0.1], Line=["closed",1] );
           DashLine( [ 1.6*P-0.6*Q, -0.6*P+1.6*Q] );           
           DashLine( [ 1.6*P-0.6*R, -0.6*P+1.6*R] );           
           DashLine( [ 1.6*R-0.6*Q, -0.6*R+1.6*Q] );           
          }  
                    
                         
   Line([Q,O,R]);                             
   Purple(0.1) Plane(pqr);
   r=0.8;  


  ijkls= [ [0,-.4, 0.2], [0, -0.5, 2.5]
       , [0, 2, -0.75], [0, 2, -2.2] ];
  STUV= ijkls*pqr;
       
  Blue(0.1) Plane( STUV ); 
  Blue() MarkPts( STUV, Label=["text","STUV","scale",2], Line=false);
       
    _h2("In matrix form:");
   _h2(join([
                	
      _formula([ _vector( _ss("{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R"
                         , joinarr(ijkls), fmt="c=red" )
                        )                        
               , "="  
               ,  _matrix( ijkls
                    , "d=1;w=5;", colfmts=[0,"d=0;w=3;c=red"] )
               , vPQR 
               ,"="
               , _vector(  "S,T,U,V","w=1" ) 
               , R_ARROW
               , "STUV on plane OQR"
               ]) 
   ]));    
}



//onPQR_j_eq_k_on_center_plane_spread(Rf=randPt());
module onPQR_j_eq_k_on_center_plane_spread(Rf=O)
{
   echom("onPQR_j_eq_k_on_center_plane_spread");
   _h2(str("For P,Q R and S=iP+jQ+kR"
           , R_ARROW, _red("j=k") 
           , R_ARROW, _red(" S is on [Rf,C,P] plane")
   ));
      
   lv=1;
   
   module test(Rf=Rf, ijksum=1, n=50, color="red" )
   {
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
       P= [8, -1, 3];//+Rf;
       Q=[4,5,4];//+Rf; 
       R=[1,1.5,5];//+Rf;
       pqr=[P,Q,R];
       stu=[gpt(1.5,P), gpt(1.5,Q), gpt(1.5,R)];
       S=stu[0];
       T=stu[1];
       U=stu[2];
     
       MarkPt(Rf, "Rf", color="black");
       Line(pqr, closed=1);
       Line( [P,Rf,R] );
       Line( [Q,Rf] );
     
       //log_(["pqr", pqr],lv);
       //log_(["stu",stu],lv );
   
     //n=50;
     pqr2=[for(p=pqr)p-Rf];
   
     ijks= [for(i=range(n))
             let(jk= rand(-0.5,1) )
             [ rand(-0.5,1),jk,jk]
          ]; 
     pts= [for(pt=ijks*pqr2)pt+Rf]; 
     
     for(pi=range(n))
     {
        color(color) MarkPt( pts[pi], Label=false);
        //if(pi<5) echo(pt=pts[pi]);
        //if(i<5)
        //  _color( str("ijksum=", ijksum,", ijk=",r3), color);
     }
     
     Red() { Line( [Rf,(R+Q)/2, P ] );
              MarkPt((R+Q)/2, "C");
           }
     Red(0.1) Plane([Rf,(R+Q)/2, P ]);         
     
     //newpqr = [for(p=ijksum*(pqr-[Rf,Rf,Rf]))p+Rf];
     
        
     Black() MarkPts( pqr, Label=["text","PQR","scale",1, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );
     
     Blue(0.1)               
     Plane( [for(pt=[ [-0.5, -0.5, -0.5]
                    , [-0.5, 1,1]
                    , [1,1,1]
                    , [1, -0.5,-0.5] 
                    ]*pqr2
                    ) pt+Rf] );    
                    
                               
     ijks2= [for(ijk=slice(ijks,0,4)) 
              [for(x=ijk) floor(x*100)/100] ];
     _h2(_formula([
     
         _vector( _ss("{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R"
                                 , joinarr(ijks2), fmt="c=red" )
                                ) 
         , "="  
          ,  _matrix( ijks2, "d=2;w=6;c=red;", colfmts=[0,"c=black"] )
          , vPQR 
          ,"="
          , _vector( "S,T,U,V","w=1" )         
      ]));
     
      _h2(_formula([     
          _ss("[{_:c=blue},{_:c=red},{_:c=red}]=",["i", "j","k"])
          , R_ARROW, _red("j=k")
          , R_ARROW, _ss("[{_:c=blue},{_:c=red}]",["P","C<sub>QR</sub>"])
          , R_ARROW, " on OPC<sub>QR</sub> plane" 
       ]));
   

   }// test
   
   //test( Rf=Rf, ijksum=0.5, color="red",n=30);
   //test( Rf=Rf, ijksum=1, color="green");
   test( Rf=Rf, ijksum=undef, color="blue", n=200);
}


//onPQR_j_eq_k_on_center_plane();
module onPQR_j_eq_k_on_center_plane()
{
   echom("onPQR_j_eq_k_on_center_plane");
   _h2(str("For P,Q R and S=iP+jQ+kR"
           , R_ARROW, _red("j=k on different i's") 
           //, R_ARROW, _red(" S is on [Rf,C,P] plane")
   ));
      
   P= [8, -1, 3];
   Q= [4, 5, 4];
   R= [1, 1.5, 5];
   pqr= [P,Q,R];
   
   is= [-0.25, 0, 0.5, 1, 1.25];
   js= [-0.25, 0, 0.25, 0.5];
   ijks= [ for(j=js) 
           [for(i=is) [i,j,j]]
         ];
   //echo(ijks=ijks);      
   ptss= [for(row=ijks) [for(ijk=row) ijk*pqr]];          
   
   for(ji= range(ptss) )
   {
      pts= ptss[ji];
      color(COLORS[ji+1])
      { for(ii = range(pts))
           MarkPt(pts[ii], Label=["text",ijks[ji][ii], "scale",1.5]);
        Line( get(pts,[0,-1]), r=ji==0?0.05:0.02 );
      }    
   }
   
   for(ii= range(is))
   {
      p2=[ ptss[0][ii], last(ptss)[ii] ];
      color( ii==len(is)-1?"teal":"gold" )
         Line( p2, r=ii==len(is)-1?0.05:0.02);
   }
   
   //------------------------------------------------------------
   Purple() DashLine( [P,O,R] );
   Purple() DashLine( [Q,O] );
   Line( [P,(Q+R)/2] );
   Teal() Line( [O,(Q+R)/2], r=0.05 );
   Purple(0.1) Plane(pqr);
   Black() MarkPts( pqr, Label=["text","PQR","scale",2.5, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );
   Black() MarkPts( [O,(Q+R)/2], Label=["text","OC","scale",2.5, "halign","right"               
                    ,"indent",-0.2]);
     
   vPQR_template= join( repeat( ["{_}P+{_}Q+{_}R"], 3), ",");

   _h2(_formula([     
          _matrix( slice(ijks[0], 0,3)
                 , str("d=2;w=6;c=", "red") 
                 , colfmts=[0,"c=black"] 
                 )
        , vPQR 
        ,"=", _vector( "S,T,U","w=1" ) 
        , R_ARROW  
        , _tablist([ [str("j=k: in center plane OPC")]
                   , ["~ i: pt moves along OP dir"]
                   , ["j+k: how far away from PC line" ]
                   ], cellfmt="w=30")      
    ]));
    
    ijks2= [ for( ji=range(js)) ijks[ji][0] ];
   _h2(_formula([     
          _matrix( ijks2
                 , str("d=2;w=6;") 
                 , colfmts=[0,"c=teal"] 
                 )
        , vPQR 
        ,"=", _vector( "S,T,U","w=1" ) 
        , R_ARROW        
        , _tablist([ [str("j=k: in center plane OPC")]
                   , ["~ j: pt moves along OC dir"]
                   ], cellfmt="w=30")      
    ]));
    
   _h2(_formula([     
          _ss("[{_:c=blue},{_:c=red},{_:c=red}]=",["i", "j","k"])
          , R_ARROW, _red("j=k")
          , R_ARROW, _ss("[{_:c=blue},{_:c=red}]",["P","C<sub>QR</sub>"])
          , R_ARROW, " on OPC<sub>QR</sub> plane" 
    ]));

   _h2(_formula([     
          R_ARROW, "Changing i", R_ARROW, str(" || O",_blue("P")) 
    ]));

   _h2(_formula([     
          R_ARROW, "Changing j,k", R_ARROW, str(" || O",_red("C<sub>QR</sub>")) 
    ]));
    
}


//onPQR_j_eq_nk_on_center_plane();
module onPQR_j_eq_nk_on_center_plane()
{
   echom("onPQR_j_eq_nk_on_center_plane");
   _h2(str("For P,Q R and S=iP+jQ+kR"
           , R_ARROW, _red("j=nk on different i's") 
           //, R_ARROW, _red(" S is on [Rf,C,P] plane")
   ));
      
   P= [8, -1, 3];
   Q= [4, 5, 4];
   R= [1, 1.5, 5];
   pqr= [P,Q,R];
   
   is= [0.5, 1]; //-0.25, 0, 0.5, 1, 1.25];
   js= [-0.25,0.25];//, 0, 0.25, 0.5];
   ns= [1, 2]; //[-1,0, 1,2];
   ijksss= [ for(n=ns)
           [ for(j=js) 
             [ for(i=is)[i,j,n*j] ]
           ]  
         ];
   //echo(ijks=ijks);      
   ptsss= [for(ijkss=ijksss) 
            [for(ijks=ijkss) 
               [for(ijk=ijks) ijk*pqr]
            ]   
           ]; 
          
   //echo(ijksss=ijksss);
   echo(ijksss= str(_BR,arrprn(ijksss,dep=3)));
   echo(ptsss= str(_BR,arrprn(ptsss,dep=3)));
                   
   for(ni = range(ns))
   { for(ji= range(js))
     { 
       //echo(ptsss[ni], ptsss[ni][ji], ptsss[ni][ji][0]);
       Line( [ ptsss[ni][ji][0], last(ptsss[ni][ji])] );
       for(ii=range(is))
       {
          pt= ptsss[ni][ji][ii];
          echo(ni_ji_ii=[ni,ji,ii], ijk=_color(ijksss[ni][ji][ii],COLORS[ni+1]), pt=pt);
          color(COLORS[ni+1])
          MarkPt(pt, Label=["text",str(ijksss[ni][ji][ii],_s(":k={_}j",ns[ni]))
                                  , "scale",1.5]);
          //Line( get(ptsss,[0,-1]), r=ji==0?0.05:0.02 );
       }    
     }
   }
/*   
   for(ii= range(is))
   {
      p2=[ ptss[0][ii], last(ptss)[ii] ];
      color( ii==len(is)-1?"teal":"gold" )
         Line( p2, r=ii==len(is)-1?0.05:0.02);
   }
*/   
   //------------------------------------------------------------
   Purple() DashLine( [P,O,R] );
   Purple() DashLine( [Q,O] );
   Line( [P,(Q+R)/2] );
   Teal() Line( [O,(Q+R)/2], r=0.05 );
   Purple(0.1) Plane(pqr);
   Black() MarkPts( pqr, Label=["text","PQR","scale",2.5, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );
   Black() MarkPts( [O,(Q+R)/2], Label=["text","OC","scale",2.5, "halign","right"               
                    ,"indent",-0.2]);
     
   vPQR_template= join( repeat( ["{_}P+{_}Q+{_}R"], 3), ",");
/*
   _h2(_formula([     
          _matrix( slice(ijks[0], 0,3)
                 , str("d=2;w=6;c=", "red") 
                 , colfmts=[0,"c=black"] 
                 )
        , vPQR 
        ,"=", _vector( "S,T,U","w=1" ) 
        , R_ARROW  
        , _tablist([ [str("j=k: in center plane OPC")]
                   , ["~ i: pt moves along OP dir"]
                   , ["j+k: how far away from PC line" ]
                   ], cellfmt="w=30")      
    ]));
    
    ijks2= [ for( ji=range(js)) ijks[ji][0] ];
   _h2(_formula([     
          _matrix( ijks2
                 , str("d=2;w=6;") 
                 , colfmts=[0,"c=teal"] 
                 )
        , vPQR 
        ,"=", _vector( "S,T,U","w=1" ) 
        , R_ARROW        
        , _tablist([ [str("j=k: in center plane OPC")]
                   , ["~ j: pt moves along OC dir"]
                   ], cellfmt="w=30")      
    ]));
    
   _h2(_formula([     
          _ss("[{_:c=blue},{_:c=red},{_:c=red}]=",["i", "j","k"])
          , R_ARROW, _red("j=k")
          , R_ARROW, _ss("[{_:c=blue},{_:c=red}]",["P","C<sub>QR</sub>"])
          , R_ARROW, " on OPC<sub>QR</sub> plane" 
    ]));

   _h2(_formula([     
          R_ARROW, "Changing i", R_ARROW, str(" || O",_blue("P")) 
    ]));

   _h2(_formula([     
          R_ARROW, "Changing j,k", R_ARROW, str(" || O",_red("C<sub>QR</sub>")) 
    ]));
*/
}



//1onPQR_sign_on_side_planes();
module 1onPQR_sign_on_side_planes()
{
   echom("1onPQR_sign_on_side_planes");
   AR= R_ARROW;
   P= [6, -1, 3]; Q=[4,7,4]; R=[1,1.5,5];
   pqr = [P,Q,R];
   
   Line( [P,O,Q] );
   Line( [O,R] );   
      
   _h2("For P,Q R, study the sign of [i,j,k] for pt S=iP+jQ+kR = [i,j,k]*[P,Q,R]");
//   _h2(_red( join(
//      [ 
//        str(_r("34-1"), " if i+j+k=1", R_ARROW, " S is on plane PQR")
//      ],_BR )));
   
   ijk_s = [0.5,0.2,0.3];
   ijk_t = [0.1,0.2,0.7];
   ijk_u = [-0.2, -0.1, 1.3];
   ijk_v = [1.1, .2, -0.3];
   
   Red() MarkPt( ijk_s*pqr, str("ijk=",ijk_s) ); 	
   Green() MarkPt( ijk_t*pqr, str("ijk=",ijk_t) ); 	
   Blue() MarkPt( ijk_u*pqr, str("ijk=",ijk_u) ); 	
   Black() MarkPt( ijk_v*pqr, str("ijk=",ijk_v) ); 	
   
   MarkCenter(pqr, Label=["text","+++","scale",3] );
   MarkCenter( [P, [1.5,-0.5,0]*pqr, [1.5,0,-0.5]*pqr, [1.6,-0.3,-0.3]*pqr ]
             , Label=["text","+--","scale",3] );
   MarkCenter( [Q, [-0.5,1.5,0]*pqr, [0,1.5,-0.5]*pqr, [-0.3,1.6,-0.3]*pqr ]
             , Label=["text","-+-","scale",3] );
   MarkCenter( [R, [-0.5,0,1.5]*pqr, [0,-0.5,1.5]*pqr, [-0.3,-0.3,1.6]*pqr ]
             , Label=["text","--+","scale",3] );
   
   
   MarkCenter( [P, Q, [1.5,0, -0.5]*pqr, [0, 1.5,-0.5]*pqr ]
             , Label=["text","++-","scale",3] );
   MarkCenter( [P, R, [0, -0.5,1.5]*pqr, [1.5,-0.5, 0]*pqr ]
             , Label=["text","+-+","scale",3] );
   MarkCenter( [R, Q, [-0.5,0, 1.5]*pqr, [-0.5,1.5,0]*pqr ]
             , Label=["text","-++","scale",3] );
   
   
   Purple(){
            MarkPts( pqr
                    , Label=["text","PQR","scale",2, "indent",0.2],r=0.05
                    , Ball=["r",0.1], Line=["closed",1] );
           DashLine( [ 1.6*P-0.6*Q, -0.6*P+1.6*Q] );           
           DashLine( [ 1.6*P-0.6*R, -0.6*P+1.6*R] );           
           DashLine( [ 1.6*R-0.6*Q, -0.6*R+1.6*Q] ); 
                     
          }            
   
   Red(0.1) Plane( [ O, 1.6*P-0.6*Q, -0.6*P+1.6*Q] );           
   Green(0.1) Plane( [ O, 1.6*P-0.6*R, -0.6*P+1.6*R] );           
   Blue(0.1) Plane( [ O, 1.6*R-0.6*Q, -0.6*R+1.6*Q] );           
   
   Purple(0.1) Plane(pqr);
   r=0.8;  
//   Blue(0.1) Plane( [ [r+1,-r/2,-r/2]*pqr
//                     , [-r/2,r+1,-r/2]*pqr
//                     , [-r/2,-r/2,r+1]*pqr
//                     ] );       
}

//. --------- 2onPQR ---------


//2onPQR_j_eq_k_on_center_line();
module 2onPQR_j_eq_k_on_center_line()
{
   echom("2onPQR_j_eq_k_on_center_line");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, b=c, varying a")));
   lv=1;
   
   P= [5, 2, 3]; Q=[3,6,3]; R=[1,1.5,5];
   pqr=[P,Q,R];
  
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [1/2, 1/4, 1/4], "red" );
   Black() MarkPt( C, Label=["text","ijk=[1/3,1/3,1/3]= centroid Pt"]);
   _Draw( [1/10, 9/20, 9/20], "green");
   _Draw( [0, 1/2, 1/2], "blue");
   _Draw( [-2/5, 7/10, 7/10], "purple");
   _Draw( [1.2, -.1, -.1], "teal");
   _Draw( [1.4, -.2, -.2], "olive");
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
   
   ijks= [[1/2, 1/4, 1/4]
         ,[0, 1/2, 1/2]
         ,[-2/5, 7/10, 7/10]
         ,[1.2, -.1, -.1]
         ];
   _h2("In matrix form:");
   _h2(join([
                	
      _formula([ _vector( _ss("{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R"
                         , joinarr(ijks), fmt="c=red" )
                        )                        
               , "="  
               ,  _matrix( ijks
                    , "d=2;w=5;c=red", colfmts=[0,"d=1;w=4;c=black"] )
               , vPQR 
               ,"="
               , _vector(  "S,T,U,V","w=1" ) 
               , R_ARROW
               , "S,T,U,V on center line"
               ]) 
   ]));      
   
}


//2onPQR_j_eq_k_on_center_plane();
module 2onPQR_j_eq_k_on_center_plane()
{
   echom("2onPQR_j_eq_k_on_center_plane");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, b=c, varying a")));
   lv=1;
   
   P= [5, 2, 3]; Q=[3,6,3]; R=[1,1.5,5];
   pqr=[P,Q,R];
  
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   Black() MarkPt( C, Label=["text","ijk=[1/3,1/3,1/3]= centroid Pt"]);

   ijks= [[2, 1/4, 1/4]
         ,[.5, 1/2, 1/2]
         ,[-3, 7/10, 7/10]
         ,[5, -.1, -.1]
         ];
   for(i=range(ijks))   
     _Draw( ijks[i], COLORS[i+1] );
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
   
   _h2("In matrix form:");
   _h2(join([
                	
      _formula([ _vector( _ss("{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R"
                         , joinarr(ijks), fmt="c=red" )
                        )                        
               , "="  
               ,  _matrix( ijks
                    , "d=2;w=5;c=red", colfmts=[0,"d=1;w=4;c=black"] )
               , vPQR 
               ,"="
               , _vector(  "S,T,U,V","w=1" ) 
               , R_ARROW
               , "S,T,U,V on center line"
               ]) 
   ]));      
   
}


//. --------- 3onPQR ---------

//3onPQR_nxPQR();
module 3onPQR_nxPQR()
{
   echom("3onPQR_nxPQR");
   AR= R_ARROW;
   _h2("For P,Q,R, study 3 pts [S,T,U]= n*[P,Q,R] = [nP, nQ, nR]");
   _h2(_red( join(
      [ str(_r("31-1"), " Plane STU(=nPQR) is ||_to PQR")
      , ""
      , str(_r("31-2"), " n = OS/OP = OT/OQ = OU/OR")
      ],_BR )));
 
     
   P= [8, -1, 3]; Q=[4,5,4]; R=[1,1.5,5];
   pqr = [P,Q,R];
   
   Red(){ 
   	MarkPts( pqr/2, Label=["text", ["P/2","Q/2","R/2"]]
   	       , Line=["closed",1] );  
   	MarkCenter(pqr/2, "0.5*[P,Q,R]");
        Dim([O,R/2,R,P], Label=["chainRatio",1]);
   	}
   Red(0.1) Plane(pqr/2);
     	
   
   Red() DashLine([O,1.5*P] );
   Green() DashLine([O,1.5*Q] );
   Blue() DashLine([O,1.5*R] );
   
   Purple() MarkPts( pqr
            , Label=["text","PQR","scale",2, "indent",0.2],r=0.05
            , Ball=["r",0.1], Line=["closed",1] );
   Purple(0.1) Plane(pqr);         

   	
   Green(){ 
   	MarkPts( 1.25*pqr, Label=["text", ["1.25P","1.25Q","1.25R"]]
   	       , Line=["closed",1] );  
   	MarkCenter(1.25*pqr, Label=["text","1.25*[P,Q,R]","scale",2]);
        Dim([O,P,1.25*P, R], Label=["chainRatio",1]);
   	}
   Green(0.1) Plane(1.25*pqr);
      
}


//3onPQR_parallel_to_plane_PQR_2();
module 3onPQR_parallel_to_plane_PQR_2()
{
   echom("3onPQR_parallel_to_plane_PQR_2");
   AR= R_ARROW;
   P= [8, -1, 3]; Q=[4,5,4]; R=[1,1.5,5];
   pqr = [P,Q,R];
   
   _h2("For P,Q R, study 3 pts S=iP+jQ+kR, T=aP+bQ+cR, U=dP+eQ+fR");
   //_h2("Case: i+j= n ");
   _h2(_red( join(
      [ str(_r("31-3"), " if i+j+k = a+b+c = d+e+f", R_ARROW, " Plane STU is ||_to PQR")
      , str(_r(), R_DBL_ARROW, " n = OS/OP = OT/OQ = OU/OR")
      ],_BR )));
   
        
   _h2( _ss("3 ways to create PQR-parallel planes:
   <br/> {_} n*[P,Q,R] = [nP, nQ, nR] {_}
   <br/> {_} [ iP+jQ+kR, jP+iQ+kR, kP+iQ+jR ] {_}
   <br/> {_} [ i{_:v}P+j{_:v}Q+k{_:v}R, i{_:v}P+j{_:v}Q+k{_:v}R, i{_:v}P+j{_:v}Q+k{_:v}R ] {_}
     ", [R_DBL_ARROW
        , _teal("// xn")
        ,R_DBL_ARROW
        , _teal("// Swap i,j,k") 
        ,R_DBL_ARROW
        , "0","0","0",1,1,1,2,2,2
        , _teal("// make sure sum(ijk) are the same") 
        ]
        ));
     
     
//   _h2(
//       str("Easier to put coefs into a matrix: <br/>"
//          , _matrix( [ [i,j,k], [i,k,j], [k,j,i]], "d=1" )
//          ));  
   
   i=-0.2; j=-.3; k=1.2;
   S= [i,j,k]*pqr;
   T= [i,k,j]*pqr;
   U= [k,j,i]*pqr;
   stu = [S,T,U];

  _h2( _formula(
     [ _vector( _ss("S={_}P+{_}Q+{_}R,T={_}P+{_}Q+{_}R,U={_}P+{_}Q+{_}R"
                     , [i,j,k,i,k,j,k,j,i], fmt="c=red" )
               )  
     , _BR, R_ARROW
      , "i+j+k="
      , _tablist( [[i+j+k], [i+j+k], [i+j+k] ], cellfmt="d=1;w=5;c=red" )
       ]
   ));    	
   Red(){ 
        MarkPts( stu, Label=["text"
                            , [ _s("{_}*pqr", str([i,j,k]))
                              , _s("{_}*pqr", str([i,k,j]))
                              , _s("{_}*pqr", str([k,j,i]))
                              ]
                            ,"scale",1.5]
   	       , Line=["closed",1] );  
   	MarkCenter(stu, Label=["text", str("stu, i+j+k=",i+j+k)]);
        }
   Red(0.1) Plane(stu);
   
   Blue()
   Dim([ O
       , intsec( [ [O,R], stu2] )
       , intsec( [ [O,R], stu ] )
       , R, P]
      , Label=["chainRatio",1]
      );
   	
   //----------------
   ijks = [ [-0.2, 0.3, 0.3 ]
          , [0.5, -0.3, 0.2 ]
          , [0.2, 0.5, -0.3]
          ];
   stu2= [for(ijk=ijks)ijk*pqr ];
   
   _h2(_formula([
        _vector( _ss("S={_}P+{_}Q+{_}R,T={_}P+{_}Q+{_}R,U={_}P+{_}Q+{_}R"
                     , joinarr(ijks), fmt="c=green" )
               )  
      , _BR, R_ARROW
      , "i+j+k="
      , _tablist( [[sum(ijks[0])], [sum(ijks[1])], [sum(ijks[2])] ], cellfmt="d=1;w=5;c=green" )
                
   ]));
   
   Green(){ 
        MarkPts( stu2, Label=["text"
                            , ijks
                            ,"scale",1.5]
   	       , Line=["closed",1] );  
   	MarkCenter(stu2, Label=["text", str("stu2, i+j+k=",sum(ijks[0]))]);
   	}
   Green(0.1) Plane(stu2);
            	
   
   
   
   Red() DashLine([O,1.5*P] );
   Green() DashLine([O,1.5*Q] );
   Blue() DashLine([O,1.5*R] );
   
   Purple() MarkPts( pqr
            , Label=["text","PQR","scale",2, "indent",0.2],r=0.05
            , Ball=["r",0.1], Line=["closed",1] );
   Purple(0.1) Plane(pqr);         
  
}



//PQR_octants();
module PQR_octants()
{
   echom("PQR_octants");
   AR= R_ARROW;
   P= [6, -1, 3]; Q=[3,6,3]; R=[1,1.5,5];
   pqr = [P,Q,R];
      
   _h2("For P,Q R, study the signs of [i,j,k] for pt S=iP+jQ+kR = [i,j,k]*[P,Q,R]");
   _h2(_red( join(
      [ 
        str(_r("34-1"), " New octants for the new coord system, D= {P,Q,R}")
      ],_BR )));
   
   ijk_s = [0.5,0.2,0.3];
   ijk_t = [0.1,0.2,0.7];
   ijk_u = [-0.2, -0.1, 1.3];
   ijk_v = [1.1, .2, -0.3];
   
   
   MarkCenter(pqr, Label=["text","+++","scale",3] );
   MarkCenter(-0.5*pqr, Label=["text","---","scale",3] );
   MarkCenter( [P, [1.5,-0.5,0]*pqr, [1.5,0,-0.5]*pqr, [1.6,-0.3,-0.3]*pqr ]
             , Label=["text","+--","scale",3] );
   MarkCenter( [Q, [-0.5,1.5,0]*pqr, [0,1.5,-0.5]*pqr, [-0.3,1.6,-0.3]*pqr ]
             , Label=["text","-+-","scale",3] );
   MarkCenter( [R, [-0.5,0,1.5]*pqr, [0,-0.5,1.5]*pqr, [-0.3,-0.3,1.6]*pqr ]
             , Label=["text","--+","scale",3] );
   
   
   MarkCenter( [P, Q, [1.5,0, -0.5]*pqr, [0, 1.5,-0.5]*pqr ]
             , Label=["text","++-","scale",3] );
   MarkCenter( [P, R, [0, -0.5,1.5]*pqr, [1.5,-0.5, 0]*pqr ]
             , Label=["text","+-+","scale",3] );
   MarkCenter( [R, Q, [-0.5,0, 1.5]*pqr, [-0.5,1.5,0]*pqr ]
             , Label=["text","-++","scale",3] );
   
   
   Purple(){
            MarkPts( pqr
                    , Label=["text","PQR","scale",2, "indent",0.2],r=0.05
                    , Ball=["r",0.1], Line=["closed",1] );
          }            
   
//   Red(0.1) Plane( [ O, 1.6*P-0.6*Q, -0.6*P+1.6*Q] );           
//   Green(0.1) Plane( [ O, 1.6*P-0.6*R, -0.6*P+1.6*R] );           
//   Blue(0.1) Plane( [ O, 1.6*R-0.6*Q, -0.6*R+1.6*Q] );           
   
   Red() { Arrow( [O,P], r=0.075 ); Line([ -P, 1.5*P ]); }
   Green() {Arrow( [O,Q], r=0.075 );Line([ -Q, 1.5*Q ]); }
   Blue() { Arrow([O,R], r=0.075);Line([ -R, 1.5*R ]); }
   
   pl_QR= [ [0,1.4,-0.2]*pqr, [0,-0.2,1.4]*pqr, [0, -1.1,0.5]*pqr, [0, .5,-1.1]*pqr ];
   Green(.1)Plane(pl_QR);
   pl_RP= [ [-0.2, 0,1.4]*pqr, [1.4, 0,-0.2]*pqr, [.5,0,-1.1]*pqr, [-1.1, 0, .5]*pqr ];
   Green(.1)Plane(pl_RP);
   pl_PQ= [ [1.4,-0.2,0]*pqr, [-0.2,1.4,0]*pqr, [-1.1,0.5,0]*pqr, [.5,-1.1,0]*pqr ];
   Red(.1)Plane(pl_PQ);
   
   Purple(0.1) Plane(pqr);
}


//PQR_summary();
module PQR_summary()
{
   echom("PQR_summary()");
   AR= R_ARROW;
   _h2("--- For P,Q,R ---<hr/>");
   /*
   //======================================================
   _h1( str("S=", markijk("i","j")));
   _h2(join( [
           _formula( [ "S=", markijk("i","j")
                     , "=", _red("[i j]")
                     ,"*[P,Q]="
                     , _red("[i j]"), vPQ
                     , "=", _red("[i j]"), _vector([ "Px Py Pz", "Qx Qy Qz"], "w=8" )
                     , "=", _s( "[{_}Px+{_}Qx,{_}Py+{_}Qy,{_}Pz+{_}Qz]"
                              , [for(x=["i","j","i","j","i","j"])_red(x)] )
                   ])
          
          ,_BR2
          
          , _red(str("Check i,j : i=j ", AR, " S on center line"))         
          ,_formula([ "2P+2Q =",_red("[2 2]")
                  , _vector(["P","Q"], "w=1")
                  ])
          
          , _BR2
          
          , _red(str("Check (i-j)/(i+j) = r ", AR, "Spread horizontally (along PQ dir) <br/>"))         
          
          , _mono(replaces(
                  "      |     |     |
                  /------Q-----C-----P----                
                  /      |     |     |
                  / r&lt;-1 | r&lt;0 | r>0 | r>1 
                  /     r=-1  r=0   r=1      
          	 ", ["/",_BR, " ",_SP]))
          	 
          , _BR2
           
          , _red("Check i+j : i+j=1 ")
          ,_formula([ "2P-Q =",_red("[2 -1]")
                  , _vector(["P","Q"], "w=1")
                   , AR, _red("i+j=1"), AR, "S on PQ line"
                  
                  ])
          ,_formula([ markijk(0.3,0.7)
                   ,"=", _red("[.3 .7]")
                  , _vector(["P","Q"], "w=1")
                   , AR, _red("i+j=1 & i,j>0"), AR, "S in PQ line segment"
                  
                  ])
          
          , _BR2
          
          , _red(str("Check i+j : i+j = n",AR, " Scale vertically (as shown below) :",_BR))                           
          
            , _mono(replaces(
          "   \\           /
          '    \\         /  n>1
          ' ----Q-------P------- n=1                 
          '      \\     /
          '       \\   /   0 &lt;n&lt;1   
          '        \\ / 
          '         O---- n=0
          '        / \\ 
          '       /   \\    n&lt;0
	 ", ["'",_BR, " ",_SP]))
                  
      ]));
   
    //=====================================================
    _h3("<hr/>");
    _h1( str( "M= ", markijk("i","j"), _BR
            , "N= ", markijk("k","l")
            ));        
    _h2(join( [
           _formula( [ _vector(["M","N"],"w=1"),"="
                        , _vector( [ markijk("i","j")
                                   , markijk("k","l")
                                   ] )
                     , "=", _vector( [ "i j", "k l"], cellfmt="c=red;w=3" ), vPQ
                   ])
          ,""
          , _red(str("i+j=k+l =n ", AR, " MN || PQ", AR, "n decides how far MN is from O"))  
          ,_formula( [ _vector(["M","N"],"w=1"),"="
                     , _matrix( [ [0.3,1], [-0.7, 2]], cellfmt="c=red;w=4;d=1" )
                          , vPQ
                   ]) 
          ,""
          , _red(str("i=k", AR, " MN || OQ"))  
          ,_formula( [ _vector(["M","N"],"w=1"),"="
                     , _matrix( [ [3,1], [3, 2]], cellfmt="c=red;w=2;d=0" )
                          , vPQ
                          , "|| OQ"
                     ])  
          , _mono(replaces(
                  "   
                  '    \\ #       /  
                  ' ----Q-#-----P------- 
                  '      \\ #   /
                  '       \\ # /   
                  '        \\ # 
                  '         O #
                  '        / \\ #
                  
        	 ", ["'",_BR, " ",_SP]))
	  ,""             
          , _red(str("j=l", AR, " MN || PO"))  
          ,_formula( [ _vector(["M","N"],"w=1"),
                     , "=", _matrix( [ [1,2], [3, 2]], cellfmt="c=red;w=2;d=0" )
                          , vPQ
                          , "|| OP"
                   ]) 
            , _mono(replaces(
          "   
          '    \\     #   /  
          ' ----Q---#---P-------                  
          '      \\ #   /
          '       #   /  
          '      # \\ / 
          '     #   O
          '    #   / \\ 
          '  
	 ", ["'",_BR, " ",_SP]))                   
	  ,""             
          , _red(str("i/j = k/l", AR, " MN passes O"))  
          ,_formula( [ _vector(["M","N"],"w=1")
                    , "=", _matrix( [ [1,2], [3, 6]], cellfmt="c=red;w=2;d=0" )
                          , vPQ
                   ]) 
            , _mono(replaces(
          "           #
          '    \\     #   /  
          ' ----Q----#--P-------                  
          '      \\   # /
          '       \\ # /  
          '        \\#/ 
          '         O
          '        /#\\ 
          '  
	 ", ["'",_BR, " ",_SP]))     
	               
       ],_BR));
                              
         //=============================================================== 
    _h3("<hr/>");
  */    
    
    _h1( str("S=", markijk("i","j","k")));
   _h2(join( [
         _formula( [ "S=", markijk("i","j","k")
                , "= ", _red("[i j k]")
                , vPQR 
                ] )
      ,   _formula( [ "S="
                     , _red("[i j k]"), vPQR
                     , "=", _red("[i j k]")
                          , _vector([ "Px Py Pz", "Qx Qy Qz", "Rx Ry Rz"], "w=8" )
                     , "=", _s( "[{_}Px+{_}Qx+{_}Rx,{_}Py+{_}Qy+{_}Ry,{_}Pz+{_}Qz+{_}Rz]"
                              , [for(x=["i","j","k","i","j","k","i","j","k"])_red(x)] )
                   ])
      ], _BR));
      
   _h2(_red(str("i=j=k= n ", AR, " S is on the center line")));
   _h2(join([
         _formula( [ "S="
                     , _red("[2 2 2]"), vPQR
                     , "=", markijk(2,2,2)
                     , "= (P+Q+R)*2"
                   ])  
      ],_BR));

   _h2(_red(str("i=j=k=1/3 ", AR, " S is the centroid Pt of PQR")));
   _h2(join([
         _formula( [ "S=", _red("[1/3,1/3,1/3]"),"*[P,Q,R]="
                   , "(P+Q+R)/3 " 
                   ])  
      ],_BR));
      
   _h2(_red(str("i+j+k= 1 ", AR, " S is on the PQR plane")));
   _h2(join([
         _formula( [ "S=", _red("[-0.1,0.8,0.3]"),"*[P,Q,R]="
                     , _red("[-0.1 0.8 0.3]"), vPQR
                     , "=", markijk( -0.1,0.8,0.3)
                   ])  
      ],_BR));
      
      
   _h2(_red(str("i+j+k=1 & i,j,k>0 ", AR, " S is inside the PQR triangle")));
   _h2(join([
         _formula( [ "S=", _red("[0.2,0.5,0.3]"),"*[P,Q,R]="
                     , _red("[0.2 0.5 0.3]"), vPQR
                     , "=", markijk( 0.2,0.5,0.3)
                   ])  
      ],_BR));


    ______("S, T");
    _h1( str("S=", markijk("i","j","k"), ", T=", markijk("a","b","c")));
   
   _h2(_red(str("i+j+k=a+b+c=1 ", AR, " S,T is on a plane PQR<br/>i+j+k=a+b+c ", AR, " S,T is on a plane ||_to PQR")));
   _h2(join([
         _formula( [ _vector(["S","T"],"w=1"),"="
                   , _vector(["-0.3 0.8 0.3","0.2 0.5 0.1"], "w=12;c=red")
                   , vPQR //"*[P,Q,R]="
                   , R_ARROW
                   , "S,T on plane || to PQR"
                   ])  
      ],_BR));   

   _h2(_red(str("i=a", AR, " ST || QR"
               ,_BR,"j=b", AR, " ST || PR"
               ,_BR,"k=c", AR, " ST || PQ"
               )));
   _h2(join([
         _formula( [ _vector(["S","T"],"w=1"),"="
                   , _vector(["-2 1 3","-2 2 1"], "w=6;c=red")
                   , vPQR 
                   , R_ARROW
                   , "ST ||2 QR"
                   ])  
      ],_BR));   
   
   
       
   _h2(join([
         _formula( [ "S=", _red("[0.2,0.5,0.3]"),"*[P,Q,R]="
                     , _red("[0.2 0.5 0.3]"), vPQR
                     , "=", markijk( 0.2,0.5,0.3)
                   ])  
      ],_BR));


         //=============================================================== 
                
         
//    _h3("<hr/>");
//    _h1( _ss("S= {_}P+{_}Q+{_}R<br/>T= {_}P+{_}Q+{_}R<br/>U= {_}P+{_}Q+{_}R" 
//           , [_red("i"),_red("j"),_red("k")
//             , _red("a"),_red("b"),_red("c")
//             , _red("d"),_red("e"),_red("f")
//             ]
//           ));
//
//    _h2(join( [
//    
//       _formula( [ _vector(["S","T","U"],"w=1"), "=", 
//                 , "=", _vector( [ "i j k","a b c","d e f"], cellfmt="c=red;w=7" )
//                      , vPQR
//                 ]) 


//           _formula( [ "[M,N]=", _s("[{_}P+{_}Q,{_}P+{_}Q]", [_red("i"),_red("j"),_red("k"), _red("l")])
//                     , "=", _vector( [ "i j", "k l"], cellfmt="c=red;w=3" ),vPQRS
//                     , "=", _vector( [ "i j", "k l"], cellfmt="c=red;w=3" )
//                          , _vector( [ "P Q","R S"], "w=3", sup="T")
//                   ])
//          ,""
//          , _red(str("i+j=k+l =n ", AR, " MN || PQ", AR, "n decides how far MN is from O"))  
//          ,_formula( [ "[M,N]=", _s("[{_}P+{_}Q,{_}R+{_}S]", [_red("i"),_red("j"),_red("k"), _red("l")])
//                     , "=", _matrix( [ [0.3,1], [-0.7, 2]], cellfmt="c=red;w=4;d=1" )
//                          , _vector( [ "P Q","R S"], "w=3", sup="T")
//                   ]) 
//          ,""
//          , _red(str("i=k", AR, " MN || OQ"))  
//          ,_formula( [ "[M,N]=", _s("[{_}P+{_}Q,{_}R+{_}S]", [_red(3),1,_red(3), 2])
//                     , "=", _matrix( [ [3,1], [3, 2]], cellfmt="c=red;w=2;d=0" )
//                          , _vector( [ "P Q","R S"], "w=3", sup="T")
//                          , "|| OQ"
//                     ])  
//          , _mono(replaces(
//                  "   
//                  '    \\ #       /  
//                  ' ----Q-#-----P------- 
//                  '      \\ #   /
//                  '       \\ # /   
//                  '        \\ # 
//                  '         O #
//                  '        / \\ #
//                  
//        	 ", ["'",_BR, " ",_SP]))
//	  ,""             
//          , _red(str("j=l", AR, " MN || PO"))  
//          ,_formula( [ "[M,N]=", _s("[{_}P+{_}Q,{_}R+{_}S]", [1,_red(2),3, _red(2)])
//                     , "=", _matrix( [ [1,2], [3, 2]], cellfmt="c=red;w=2;d=0" )
//                          , _vector( [ "P Q","R S"], "w=3", sup="T")
//                          , "|| PO"
//                   ]) 
//            , _mono(replaces(
//          "   
//          '    \\     #   /  
//          ' ----Q---#---P-------                  
//          '      \\ #   /
//          '       #   /  
//          '      # \\ / 
//          '     #   O
//          '    #   / \\ 
//          '  
//	 ", ["'",_BR, " ",_SP]))                   
//	  ,""             
//          , _red(str("i/j = k/l", AR, " MN passes O"))  
//          ,_formula( [ "[M,N]=", _s("[{_}P+{_}Q,{_}R+{_}S]", [_red(1),_red(2),_red(3), _red(6)])
//                     , "=", _matrix( [ [1,2], [3, 6]], cellfmt="c=red;w=2;d=0" )
//                          , _vector( [ "P Q","R S"], "w=3", sup="T")
//                   ]) 
//            , _mono(replaces(
//          "           #
//          '    \\     #   /  
//          ' ----Q----#--P-------                  
//          '      \\   # /
//          '       \\ # /  
//          '        \\#/ 
//          '         O
//          '        /#\\ 
//          '  
//	 ", ["'",_BR, " ",_SP]))                   
                              
         //=============================================================== 
//          , _red(str("Check (i-j)/(i+j) = r ", AR, "Spread horizontally (along PQ dir) <br/>"))         
//          
//          , _mono(replaces(
//                  "      |     |     |
//                  /------Q-----C-----P----                
//                  /      |     |     |
//                  / r&lt;-1 | r&lt;0 | r>0 | r>1 
//                  /     r=-1  r=0   r=1      
//          	 ", ["/",_BR, " ",_SP]))
          	 
//          , _BR2
//           
//          , _red("Check i+j : i+j=1 ")
//          ,_formula([ "2P-Q =",_red("[2 -1]")
//                  , _vector(["P","Q"], "w=1")
//                   , AR, _red("i+j=1"), AR, "S on PQ line"
//                  
//                  ])
//          ,_formula([ markijk(0.3,0.7)
//                   ,"=", _red("[.3 .7]")
//                  , _vector(["P","Q"], "w=1")
//                   , AR, _red("i+j=1 & i,j>0"), AR, "S in PQ line segment"
//                  
//                  ])
//          
//          , _BR2
//          
//          , _red(str("Check i+j : i+j = n",AR, " Scale vertically (as shown below) :",_BR))                           
//          
//            , _mono(replaces(
//          "   \\           /
//          '    \\         /  n>1
//          '-----Q-------P------- n=1                 
//          '      \\     /
//          '       \\   /   0 &lt;n&lt;1   
//          '        \\ / 
//          '         O---- n=0
//          '        / \\ 
//          '       /   \\    n&lt;0
//	 ", ["'",_BR, " ",_SP]))
//                  
//      ],_BR));
//   _h2("<hr/>");
//   _h1( str("S=", markijk("i","j"), ", T=", markijk("k","l")));
//   _h2(join( [
//           _formula( [ str("[S,T] = "
//                     , "[", markijk("i","j"), ", ", markijk("k","l"),"] =")
//                     , _vector( [ _red("i j"), _red("k l") ] )
//                     , vPQ
//                   ])
//          , _red("Check i+j, k+l :")         
//          , _formula([ "[", markijk(3,-1), ",", markijk(0.3,1.7),"]="
//                     , _vector( [ _red("3 -1"), _red("0.3 1.7") ] )
//                     , vPQ
//                    , AR, _red("i+j=k+l=2"), AR, "ST || PQ"
//                  
//                  ])
//          ,_formula([ markijk(0.3,0.7)
//                   ,"=", _red("[.3 .7]")
//                  , _vector(["P","Q"], "w=1")
//                   , AR, _red("i+j=1 & i,j>0"), AR, "S in PQ line segment"
//                  
//                  ])
//      ])
//      )
      ;


}
//pqr = pqr();
//MarkPts(pqr, "PQR", Line=["closed",1]);
//Line( [pqr[0],O, pqr[1]] );
//Line( [O, pqr[2]] );
//MarkPts( [[-2,1,3]*pqr,[-2,2,1]*pqr], "ST" );
//Red(0.2)MarkPts( [[-2,1,3],[-2,2,1]]*pqr, "ST", r=0.05 );


//. --------- 1onPQRS ---------

//1onPQR_on_plane_PQR();
module 1onPQR_on_plane_PQRS() // to do
{
   echom("1onPQR_on_plane_PQRS");
   AR= R_ARROW;
   P= [6, -1, 3]; Q=[4,7,4]; R=[1,1.5,5];
   pqr = [P,Q,R];
      
   _h2("For P,Q R, study pt S=iP+jQ+kR = [i,j,k]*[P,Q,R]");
   _h2(_red( join(
      [ 
        str(_r("32-1"), " if i+j+k=1", R_ARROW, " S is on plane PQR")
      , str(_r("32-2"), " if i+j+k=1 & i,j,k>0 ", R_ARROW, " S is inside triangle PQR")
      , str(_r("32-3"), " i,j,k are 'pulling force' of P, Q, R, respectively")
      ],_BR )));
   
//   _h2( _s("Two easy ways to create PQ-parallel lines:
//   <br/> {_} n*[P,Q,R] = [nP, nQ, nR]
//   <br/> {_} [ iP+jQ+kR, jP+iQ+kR, kP+iQ+jR ]
//     ", [R_DBL_ARROW,R_DBL_ARROW] ));
//   _h2(str("Easier to put coefs into a matrix: <br/>"
//          , _matrix( [ [i,j,k], [i,k,j], [k,j,i]], "d=1" )
//          ));  
   
   
   ijk_s = [0.5,0.2,0.3];
   ijk_t = [0.1,0.2,0.7];
   ijk_u = [-0.2, -0.1, 1.3];
   ijk_v = [0.7, 0.6, -0.3];
   
   Red() MarkPt( ijk_s*pqr, str("ijk=",ijk_s) ); 	
   Green() MarkPt( ijk_t*pqr, str("ijk=",ijk_t) ); 	
   Blue() MarkPt( ijk_u*pqr, str("ijk=",ijk_u) ); 	
   Black() MarkPt( ijk_v*pqr, str("ijk=",ijk_v) ); 	
   
   
   Red() DashLine([O,1.5*P] );
   Green() DashLine([O,1.5*Q] );
   Blue() DashLine([O,1.5*R] );
   
   Purple() MarkPts( pqr
            , Label=["text","PQR","scale",2, "indent",0.2],r=0.05
            , Ball=["r",0.1], Line=["closed",1] );
   Purple(0.1) Plane(pqr);
   r=0.8;  
   Blue(0.2) Plane( [ [r+1,-r/2,-r/2]*pqr
                    , [-r/2,r+1,-r/2]*pqr
                    , [-r/2,-r/2,r+1]*pqr
                    ] );  
                    
    _h2("In matrix form:");
   _h2(join([
             _formula([ "S=", markijk("i","j","k")
                      , "=dot prod=", _vector(["i","j","k"],"c=red;w=1", sup="T")
                      , vPQR
                      , "=[", _red("i j k"),"]"
                      , vPQR
                      ])  
 	    , _formula([
 	                markijk( -1,0.5,1.5 )
 	                , "=[", _red("-1 0.5 1.5"),"]"
                      , vPQR, R_ARROW, " on pqr plane"
 	              ])
 	    , _formula([
 	                markijk( 0.2,0.5,0.3 )
 	                , "=[", _red("0.2 0.5 0.3"),"]"
                      , vPQR, R_ARROW, " inside the PQR triangle"
 	              ])
 	                	
            ,_formula([ 
                       _vector( _ss("{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R,{_}P+{_}Q+{_}R"
                               , [ -1, 1.5,0.5, 0.5,2,-1.5, .5,.2,.3], fmt="c=red" )
                              )                        
                     , "="  
                      ,  _matrix( [ [-1, 1.5,0.5], [0.5,2,-1.5], [.5,.2,.3]]
                          , "d=1;w=5;c=red;" )
                      , vPQR 
                      ,"="
                      , _vector( [ "R", "S", "T" ],"w=1" ) 
                      //, R_ARROW
                      //, "RS is the center line, and passes thru O"
                      ]) 
           , _formula([R_ARROW, "R,S,T are all on plane PQR 'cos i+j+k=1"])            
           , _formula([R_ARROW, "T is inside PQR triangle 'cos i,j,k>0"])            
       ])
   );         
   Red()
   MarkPts( [ [-1, 1.5,0.5], [0.5,2,-1.5], [.5,.2,.3]]*pqr
          , Label=["text", "STU", "scale",3]
          , r=0.05, Line=["closed",1]);
                
   
                
                         
}


//. --- othoPt ---------

//dev_othoPt();
module dev_othoPt()
{
   echom("dev_othoPt");
   pqr=pqr(); 
   P=pqr[0]; Q=pqr[1]; R=pqr[2];
   //P= [5, 3, 3]; Q=[4,5,4]; R=[1,1.5,5];
   //pqr = [P,Q,R];
   MarkPts(pqr, "PQR", Line=["closed",1]);
   //MarkPts(pqr, Label=["text",true], Line=["closed",1]);
  
   //dd = (P-Q)*uv(R-Q);
   //r= dd/norm(Q-R);
   //S= r*R+(1-r)*Q;
   //MarkPt(S, "S");
   //Mark90( [P,S,Q] );
   echo(pqr=pqr);
   
   function othopt(pqr, tip=0)=
   (
     let( is= tip==0?[1,0,2]:tip==1?[2,1,0]:[0,2,1]
        , P= pqr[is[0]]     
        , Q= pqr[is[1]]     
        , R= pqr[is[2]]
        , d= (P-Q)*uv(R-Q) // dot prod of A and uv(B)  
                           // = length of projPt(A,B)
        , r = d/norm(Q-R)
        )
     r*R+(1-r)*Q
   );
   
   module othopt(pqr, tip)
   {
     is= tip==0?[1,0,2]:tip==1?[2,1,0]:[0,2,1];
     P= pqr[is[0]];     
     Q= pqr[is[1]];     
     R= pqr[is[2]];
        
     S= othopt(pqr, tip);
     echo(S=S);
     MarkPt(S, "S");
     Mark90( [P,S,Q] );
   }
   Red() othopt(pqr, tip=0);
   //Green() othopt(pqr, tip=1);
   //Blue() othopt(pqr, tip=2);
   
}


//. --- movePts ----


//. pt3_dev




//dev_inTrianglePt_coPlane_01();
module dev_inTrianglePt_coPlane_01()
{
   echom("dev_inTrianglePt_coPlane_01");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, b=4c, Q has stronger pulling")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [1.5, -.4, -.1], "red" );
   _Draw( [0.5, .4, .1], "red" );
   _Draw( [0,   .8, .2], "red" );
   _Draw( [-0.5, 1.2, .3], "red");
   Red() {
     Line( [[1.5,-.4, -.1]*pqr, [-1, 1.6,.4]*pqr] );
     MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}



//dev_inTrianglePt_centroid_1();
module dev_inTrianglePt_centroid_1()
{
   echom("dev_inTrianglePt_centroid_1");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, b=c, fixed a")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
   Arrow( normalLn( pqr ) );  
   Red() Arrow( [O, 2*P] );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   //_Draw( [1/2, 1/4, 1/4], "red", " centroid" );
   Black() MarkPt( C, Label=["text","abc=[1/3,1/3,1/3]= centroid Pt"]);
   
   _Draw( [1, 1/3, 1/3], "red");
   Red()
   { S= [1, 1/3,1/3]*pqr;
     Js = projPt( S, pqr );
     Mark90( [S,Js,C] );
     Line( [Js,C,S]);
   }
   
   _Draw( [2,1/3, 1/3], "green");
   Green(.4)
   { T= [2, 1/3,1/3]*pqr;
     Jt = projPt( T, pqr );
     Mark90( [T,Jt,C] );
     Line( [Jt,C,T]);
   }
   
   _Draw( [2,2/3, 2/3], "blue");
   Blue(.4)
   { U= [2, 2/3,2/3]*pqr;
     Ju = projPt( U, pqr );
     Mark90( [U,Ju,C] );
     Line( [Ju,C,U]);
   }
   
   _Draw( [2,1,1], "purple" );
   Purple(.4)
   { V= [2, 1,1]*pqr;
     Jv = projPt( V, pqr );
     Mark90( [V,Jv,C] );
     Line( [Jv,C,V]);
   }   
   
   _Draw( [2,0,0], "olive" );
   Olive(.4)
   { W= [2, 0,0]*pqr;
     Jw = projPt( W, pqr );
     Mark90( [W,Jw,C] );
     Line( [Jw,C,W]);
   }   
   //_Draw( [0, 1/2, 1/2], "blue");
   //_Draw( [-2/5, 7/10, 7/10], "purple");
   //_Draw( [1.2, -.1, -.1], "teal");
   //_Draw( [1.4, -.2, -.2], "olive");
     //
   MarkPts( [[2, -.3, -.3]*pqr,[2, 1.5, 1.5]*pqr]
          , Label=["text", [[2, -.3, -.3],[2, 1.5, 1.5]]] );
}


//dev_inTrianglePt_cone_1();
module dev_inTrianglePt_cone_1()
{
   echom("dev_inTrianglePt_cone_1");
   _h2(str( "Pt= aP + bQ + cR, ", _red(" If all a,b,c >0, Pt will be inside the cone")));
   _h2(_purple( "===> The cone is the new coordinate plane"));
   _h2(_purple( "===> Every new PQR is a new coordinate system. <br/>
                 ===> A pt [a,b,c] in that system can be converted <br/>
                      back to Cartesien system by: aP+bQ+cR
                      "));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
   Arrow( normalLn( pqr ) );  
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   Olive(.5){
     Plane( [3*P,O,3*Q ]);
     Plane( [3*P,O,3*R ]);
     Plane( [3*R,O,3*Q ]);
   }
   
   pts = randPts(400,d=2);
   
   for(p=pts)
   {
      color= (p[0]>0 && p[1]>0 && p[2]>0)?"green":"red";
      MarkPt( p*pqr, color=color);
   }  
   
}



//dev_inTrianglePt_octants();
module dev_inTrianglePt_octants()
{
   echom("dev_inTrianglePt_octants");
   _h2(str( "Pt= aP + bQ + cR, ", _red(" If all a,b,c >0, Pt will be inside the cone")));
   _h2(_red("which is equivilent to: if all x,y,z>0, [x,y,z] will be in the 1st octant"));
   _h2(_purple( "==> Every new PQR is a new coordinate system. <br/>
                 ==> [a,b,c] is a coordinate for a pt in that system <br/>
                 ==> A pt [a,b,c] in that system can be converted <br/>
                      back to Cartesien system by: aP+bQ+cR <br/>
                 ==> A pt S in the Cartesien system can be converted to a pt [a,b,c] in PQR system by:<br/>
                     [a,b,c] = solve3( PQR, S)       
                "));
   _h2(_red("# Convert any pt [x,y,z] (=[a,b,c]) to PQR coordinate system <br/>
                     (but still displayed in Cartesian (as S here) 'cos that's what we have):<br/>
                  Known a,b,c :  aP + bQ + cR = S => [a,b,c]*[P,Q,R] = S<br/>                "));
   _h2(_red("# Convert any pt [a,b,c] in a PQR system back to Cartesien: <br/>
            Known S:  [a,b,c] = solve3( PQR, S) <br/>"));
   
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
   Arrow( normalLn( pqr ) );  
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   Olive(.4){
     Plane( [3*P,3*Q, -3*P,-3*Q ]);
     Plane( [3*P,3*R, -3*P,-3*R ]);
     Plane( [3*R,3*Q, -3*R,-3*Q ]);
   }
   
   MarkCenter( [P,Q,R]*3, Label=["text","+++","scale",3] );
   MarkCenter( [-P,Q,R]*3, Label=["text","-++","scale",3] );
   MarkCenter( [P,-Q,R]*3, Label=["text","+-+","scale",3] );
   MarkCenter( [P,Q,-R]*3, Label=["text","++-","scale",3] );
   MarkCenter( [-P,-Q,R]*3, Label=["text","--+","scale",3] );
   MarkCenter( [-P,Q,-R]*3, Label=["text","-+-","scale",3] );
   MarkCenter( [P,-Q,-R]*3, Label=["text","+--","scale",3] );
   MarkCenter( [-P,-Q,-R]*3, Label=["text","---","scale",3] );
   
   pts = randPts(400,d=2);
   colors=[ "111", "gold"
          , "-111", "red"
          , "1-11", "green"
          , "11-1", "blue"
          , "-1-11", "teal"
          , "-11-1", "darkkhaki"
          , "-1-11", "purple"
          , "-1-1-1", "black"
          ];
   for(p=pts)
   {
      color= (p[0]>0 && p[1]>0 && p[2]>0)?"green":"red";
      colorcode = str( sign(p[0]), sign(p[1]), sign(p[2]) );
      MarkPt( p*pqr, color= h(colors, colorcode));
   }  
   
}



//dev_inTrianglePt_02();
module dev_inTrianglePt_02()
{
   echom("dev_inTrianglePt_02");
   _h2(str( "M = aP + bQ + cR,   ", _red("b=c, change a")));
   _h2(_red("M is on plane [O,P,C<sub>QR</sub>]"));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
   
   Purple() Line([O,P,(Q+R)/2], closed=1); 
   Purple(0.2)  Plane( [O,P,(Q+R)/2] );
   
   
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
   
   Arrow( normalLn(pqr) );  
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("b+c= ", abc[1]+abc[2], ", abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [0.5, .5, .5], "red" );
   _Draw( [0,   .5, .5], "red" );
   _Draw( [-0.5, .5, .5], "red");
   Red() {
     Line( [[1.5,.5, .5]*pqr, [-1, .5, .5]*pqr] );
    // MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
   }
     
   _Draw( [0.5, .8, .8], "green" );
   _Draw( [0,   .8, .8], "green" );
   _Draw( [-0.5, .8, .8], "green");
   Green() {
     Line( [[1.5,.8, .8]*pqr, [-1, .8, .8]*pqr] );
    // MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
   }
     
   _Draw( [0.5, -.8, -.8], "blue" );
   _Draw( [0,   -.8, -.8], "blue" );
   _Draw( [-0.5, -.8, -.8], "blue");
   Blue() {
     Line( [[1.5,-.8, -.8]*pqr, [-1, -.8, -.8]*pqr] );
    // MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
   }
          
//   _Draw( [0.5, 0, 0], "purple" );
//   _Draw( [0,   0, 0], "purple" );
//   _Draw( [-0.5, 0, 0], "purple");
//   Purple() {
//     Line( [[1.5,0, 0]*pqr, [-1, 0, 0]*pqr] );
//    // MarkPt( [-1,1.6,.4]*pqr, " a+b+c=1, b=4c => Q has stronger pulling");
//   }
//     

   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
   Gold(0.3)
   Plane( [ [-1, -0.8,-0.8]*pqr
          , [1.5, -0.8,-0.8]*pqr
          , [1.5, .8,.8]*pqr
          , [-1, .8,.8]*pqr
          ] );
}


//dev_inTrianglePt_parallel_1();
module dev_inTrianglePt_parallel_1()
{
   echom("dev_inTrianglePt_parallel_1");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, a=0.25")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [0.25, -.25, 1], "red" );
   _Draw( [0.25, 0, .75], "red" );
   _Draw( [0.25, .25, .5], "red" );
   _Draw( [.25, .75, 0], "red");
   _Draw( [.25, 1.25, -.5], "red");
   Red() {
     Line( [[.25, -.5, 1.25]*pqr, [.25,1.5,-.75]*pqr] );
     MarkPt( [.25,1.5,-.75]*pqr, " a+b+c=1, a=0.25 => parallel to RQ");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}


//dev_inTrianglePt_parallel_11();
module dev_inTrianglePt_parallel_11()
{
   echom("dev_inTrianglePt_parallel_11");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1 (on PQR plane), a=0.25 or 0.5")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, label, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str(label, ", abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [0.25, -.05, .8], "S", "red" );
   _Draw( [0.25, 1.25, -0.5], "T", "red" );
   
   _Draw( [0.5, -.25, .75], "U", "green" );
   _Draw( [0.5, .95, -.45], "V", "green" );
//   _Draw( [0.25, 0, .75], "red" );
//   _Draw( [0.5, .25, .5], "red" );
//   _Draw( [.25, .75, 0], "red");
//   _Draw( [.25, 1.25, -.5], "red");
   Red() {
     Line( [[.25, -.5, 1.25]*pqr, [.25,1.5,-.75]*pqr] );
     MarkPt( [.25,1.5,-.75]*pqr, " a=0.25 for S,T");
   }
   Green() {
     Line( [[.5, -.5, 1]*pqr, [.5,1.5,-1]*pqr] );
     MarkPt( [.5,-.5,1]*pqr, " a= 0.5 for both U, V");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}


//dev_inTrianglePt_parallel_12();
module dev_inTrianglePt_parallel_12()
{
   echom("dev_inTrianglePt_parallel_12");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, a=0.25")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   //_Draw( [1, -.25, 1], "red" );
   //_Draw( [1, 0, .75], "red" );
   //_Draw( [1, .25, .5], "red" );
   //_Draw( [1, .75, 0], "red");
   //_Draw( [1, 1.25, -.5], "red");
   Red() {
     Line( [[.25, -.5, 1.25]*pqr, [.25,1.5,-.75]*pqr] );
     MarkPt( [.25,1.5,-.75]*pqr, " a+b+c=1, a=0.25 => on PQR plane");
   }
   Green() {
     Line( [[1.25, -.5, 1.25]*pqr, [1.25,1.5,-.75]*pqr] );
     MarkPt( [1.25,1.5,-.75]*pqr, " a+b+c=2, a=125 => above plane");
   }
   Blue() {
     Line( [[-.5, -.5, 1.25]*pqr, [-.5,1.5,-.75]*pqr] );
     MarkPt( [-.5,1.5,-.75]*pqr, " a+b+c= -.75, a= -0.5 => below plane");
   }
     
   Gold(1)Plane(pqr);  
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
   
   Green(0.3)
   Plane( [ [1.25, -.5, 1.25]*pqr, [1.25,1.5,-.75]*pqr
          , [-.5,1.5,-.75]*pqr, [-.5, -.5, 1.25]*pqr
          ]
        );
}


//dev_inTrianglePt_perpendicular_1();
module dev_inTrianglePt_perpendicular_1_under_construct()
{
   echom("dev_inTrianglePt_perpendicular_1");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1 (on PQR plane), a=0.25 or 0.5")));
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [2.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, label, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str(label, ", abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   m= [0,.5,.5];
   s= [1,1.25,-1.25];  
   j= [1,.75,-.75];
   //t= [0.5, .75, -.75];
   M = m*pqr;
   S = s*pqr;
   J = j*pqr;
   //T = t*pqr;
   
   _Draw( m, "M", "red" );
   _Draw( s, "S", "red" );
   _Draw( j, "J", "blue" );
   
   _Draw( [0.5, 0.25,0.25], "T", "purple");
   //_Draw( t, "T", "purple" );
   Line( [M,J] );
   DashLine( [J,R] );
   
   _h2("Want to find a pt, J, on line SP such that aPJM=90 
   <br/> 1. a+b+1 = 1 (so it's on PQR plane)
   <br/> 2. a = 1 (so it's on line PS)
   <br/> 3. b=-c (so it fullfils #1 above) => n,-n or -n,n
   <br/> 4. JR^2-MR^2= PM^2-PJ^2
   ");
   
   echo(aPJM = angle( [P,J,M] ) ); 
   Mark90( [M,J,P]);
    
   
  // _Draw( [0.5, -.25, .75], "U", "green" );
   //_Draw( [0.5, .95, -.45], "V", "green" );
//   _Draw( [0.25, 0, .75], "red" );
//   _Draw( [0.5, .25, .5], "red" );
//   _Draw( [.25, .75, 0], "red");
//   _Draw( [.25, 1.25, -.5], "red");
   Red() {
     Line( [s*pqr, [1, -.5,.5]*pqr] );
     //MarkPt( [.25,1.5,-.75]*pqr, " a=0.25 for S,T");
   }
   Green() {
     //Line( [[.5, -.5, 1]*pqr, [.5,1.5,-1]*pqr] );
     //MarkPt( [.5,-.5,1]*pqr, " a= 0.5 for both U, V");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}


//dev_inTrianglePt_dist_1();
module dev_inTrianglePt_dist_1()
{
   echom("dev_inTrianglePt_dist_1");
   _h2(str( "M = aP + bQ + cR => study the relationship between a,b,c and absolute dist (among P,Q,R) "
   ));
   lv=1;
   
   pqr=pqr();
   pqr= [[.7, 2.91, 1.01], [-0.9, 1.77, 1.8], [-0.08, -0.15, -0.35] ];
   //pqr = [[-0.91, 0.68, -1.66], [2.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, label, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str(label, ", abc= ",abc, cmt=cmt?cmt:"") );
   }     
     
   s= [1,1.25,-1.25];  
   j= [1,.75,-.75];
   //t= [0.5, .75, -.75];
   
   m= [.2,.4,.4];
   M = m*pqr;
   _Draw( m, "M", "red" );
   abc_m = solve3(pqr,M);
   echo(abc_m = abc_m, M=M, abc_m_pqr = abc_m*pqr);
   _Draw( abc_m, "M'", "green");
   //echo( m*pqr, );
   
   //S = s*pqr;
   //J = j*pqr;
   //T = t*pqr;
   
   //_Draw( s, "S", "red" );
   ////_Draw( j, "J", "blue" );
   
   //_Draw( [0.5, 0.25,0.25], "T", "purple");
   //_Draw( t, "T", "purple" );
   //Line( [M,J] );
   //DashLine( [J,R] );
   
   //echo(aPJM = angle( [P,J,M] ) ); 
   //Mark90( [M,J,P]);
    
   
  // _Draw( [0.5, -.25, .75], "U", "green" );
   //_Draw( [0.5, .95, -.45], "V", "green" );
//   _Draw( [0.25, 0, .75], "red" );
//   _Draw( [0.5, .25, .5], "red" );
//   _Draw( [.25, .75, 0], "red");
//   _Draw( [.25, 1.25, -.5], "red");
   Red() {
     //Line( [s*pqr, [1, -.5,.5]*pqr] );
     //MarkPt( [.25,1.5,-.75]*pqr, " a=0.25 for S,T");
   }
   Green() {
     //Line( [[.5, -.5, 1]*pqr, [.5,1.5,-1]*pqr] );
     //MarkPt( [.5,-.5,1]*pqr, " a= 0.5 for both U, V");
   }
     
   Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
}


/*

P=[p0,p1,p3];
Q=[q0,q1,q2];
dPQ = sqrt[ (p0-q0)^2+ (p1-q1)^2 + (p2-q2)^2 ]

M=[m0,m1,m3];
J=[j0,j1,j2];
J= P+bQ-bR = [p0,p1,p3] + b[q0,q1,q2]-b[r0,r1,r2];
  = [p0,p1,p3] + b[q0,q1,q2]-b[r0,r1,r2];
  =   
dJM = sqrt[ (m0-j0)^2+ (m1-j1)^2 + (m2-j2)^2 ]



*/

//dev_inTrianglePt_1x();
module dev_inTrianglePt_1x()
{
   echom("dev_inTrianglePt_1");
   _h2(str( "M = aP + bQ + cR,   ", _red("a+b+c=1, none the same, b=a+0.1")));
   _h2("a+b+c=1 ensures the point is on the pqr plane!!!");
   lv=1;
   
   pqr = [[-0.91, 0.68, -1.66], [1.82, 1.03, 1.83], [1.63, -0.93, -0.38]];       
   P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
   C = sum(pqr)/3;
     
   log_( ["pqr",pqr], lv );
   Black() MarkPts(pqr, "PQR", Line=["closed",1] );     
     
   module _Draw(abc, color, cmt)
   { 
     Color(color)
     MarkPt( abc*pqr, str("abc= ",abc, cmt=cmt?cmt:"") );
   }
     
   _Draw( [.1, .2, .7], "red" );
   _Draw( [.2, .3, .5], "green");
   _Draw( [.3, .4, .3], "blue");
   _Draw( [.4, .5, .1], "purple");
   _Draw( [-0.3, -0.2, 1.5], "teal");
   _Draw( [.6, .7, -.3], "olive");

   Line( [[-0.5,-0.4, 1.9]*pqr,[.7,.8,-.5]*pqr] );  
}
//dev_inTrianglePt_1();



//dev_inTrianglePt01x();
module dev_inTrianglePt01x()
{
   echom("dev_inTrianglePt01");
   _h2(str( "M = iP + jQ + kR,   ", _red("i+j+k=1, j!=k, varying i")));
   lv=1;
   
   module test( Rf=O)
   {   
     log_b("test",lv);
     
     pqr = [ [1.63, -0.93, -0.38], [-0.91, 0.68, -1.66],[1.82, 1.03, 1.83]];       
     P= pqr[0]; Q=pqr[1]; R=pqr[2];
   
     T= 2*P; 
     U=2*Q;
     C = sum(pqr)/3;
     
     log_( ["pqr",pqr] );
     Black() MarkPts(pqr, "PQR", Line=["closed",1] );
     
     Red() Line( [[1.6, -.3, -.3]*pqr,  [-0.6, .8, .8]*pqr] );
     Red() MarkPt( [1.4, -.2, -.2]*pqr, "i+j+k=1, ijk=[?, j, j] (j=k)", Ball=false);
     Red() MarkPt( [-0.6, .8, .8]*pqr, "i+j+k=1, ijk=[?, j, j] (j=k)", Ball=false);
     
     Green() Line( [[1.5, -.1, -.4]*pqr,[-0.5, .3, 1.2]*pqr] );
     Green() MarkPt( [-0.2, .2, 1]*pqr, "i+j+k=1, ijk=[ ?, j, k=4j ] => R has more pulling force", Ball=false);

     Blue() Line( [[1.25, -.2, -.05]*pqr,[-1,1.6,.4]*pqr] );
     Blue() MarkPt( [-1,1.6,.4]*pqr, "i+j+k=1, ijk=[ ?, j=4k, k ] => Q has more pulling force", Ball=false);
     
     Black() MarkPt( C, "ijk=[1/3,1/3,1/3]= centroid Pt");
     
   log_e("",lv);  
   } //test()
   
   test();
   
   
}


//dev_inTrianglePt02x();
module dev_inTrianglePt02x()
{
   echom("dev_inTrianglePt02x");
   _h2(str( "M = iP + jQ + kR,   ", _red("j=k, varying i on different sum (=i+j+k) ")));
   lv=1;
   
   module test( Rf=O, sum=1, color="red")
   {   
     log_b("test",lv);
     
     P= [2.5,.5]*[X,Y]+Rf; 
     Q= [0.2,2]*[X,Y]+Rf;
     R= [2.2,2.5]*[X,Y]+Rf; 
     pqr=[P,Q,R];
     T= 2*P; 
     U=2*Q;
     C = sum(pqr)/3;
     
     log_( ["pqr",pqr] );
     Black() MarkPts(pqr, "PQR", Line=["closed",1] );
     
     
     Black() MarkPt( C, "centroid");
     log_( _color( str("sum = ", sum), color ));
     //Red() MarkPt( (sum*[1/2, 1/4, 1/4])*pqr, "ijk=[0.5, 0.25, 0.25]");
     //Green() MarkPt( (sum*[1/10, 9/20, 9/20])*pqr, "ijk=[1/10, 9/20, 9/20]");
     //Blue() MarkPt( (sum*[0, 1/2, 1/2])*pqr, "ijk=[0, 0.5, 0.5]");
     //Purple() MarkPt( (sum*[-2/5, 7/10, 7/10])*pqr, "ijk=[-0.4, 0.7, 0.7]");
     //
     //Teal() MarkPt( (sum*[1.2, -.1, -.1])*pqr, "ijk=[1.2, -0.1, -0.1]");
     //Olive() MarkPt( (sum*[1.4, -.2, -.2])*pqr, "ijk=[1.4, -0.2, -0.2]");
     
     //Line( [[1.6, -.3, -.3]*pqr,[-0.6, .8, .8]*pqr] );
     
     Line( [(sum*[1.6, -.3, -.3])*pqr,(sum*[-0.6, .8, .8])*pqr] );
     color(color) MarkPt( (sum*[-0.6, .8, .8])*pqr, str( "sum= ", sum), Ball=false );
     
   log_e("",lv);  
   } //test()
   
   Red() test([4.5,0], sum=1, color="red");
   Green() test([4.5,0], sum=0.95, color="green");
   Blue() test([4.5,0], sum=1.05, color="blue");
   Purple() test([4.5,0], sum=1.25, color="purple");
   
}



//dev_inTrianglePt1();
module dev_inTrianglePt1()
{
   echom("dev_inTrianglePt1");
   
   module test( Rf=O, pair="pq")
   {   
     P= [2.5,.5]*[X,Y]+Rf; 
     Q= [0.2,2]*[X,Y]+Rf;
     R= [2.2,1.5]*[X,Y]+Rf; 
     pqr=[P,Q,R];
     T= 2*P; 
     U=2*Q;
     Line(pqr, closed=1);
     /*
        
     
     
     */
     ab = hash( ["pq",[P,Q], "pr",[P,R], "rq",[R,Q] ], pair);
     A= ab[0];
     B= ab[1];
          
     //A= pair=="qr"? Q: P; // || pair=="pr"?P:Q;
     //B= pair=="pq"? Q: R;
     
     //echo(pqr=pqr);
     //echo(ab = [A,B]);
     
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
     
     Black() 
     { 
       MarkPt(P, "P"); 
       MarkPt(Q, Label=["text", "Q","halign","right", "indent",-0.1]);
       MarkPt(R, "R");
       //Line(pair=="pq"?[P,Q]:"pair"=="pr"?[P,R]:[Q,R], r=0.05);
       Line([A,B], r=0.05);
     }
     //pl0= [1.5*(Q-Rf)+Rf,Rf,1.5*(P-Rf)+Rf];
     pl0= [gpt(1.5,B),Rf,gpt(1.5,A)];
     
     Line( pl0 );
   
     n=10;
   
     Gold(0.1) Plane( pl0 );
     ij= hash( ["pq",["i","j"], "pr",["i","k"], "rq",["k","j"] ], pair);
     
     MarkCenter(pl0, Label=["text",_s("{_}>0, {_}>0",ij),"scale",1]);
     Black()
       for(i=range(n))
       {
          r2= rands(0,1,2);
         // MarkPt( r2*[A-Rf,B-Rf]+Rf, Label=false);
       }
   
     //pl1= [ 1.5*(Q-Rf)+Rf,Rf, -1.3*(P-Rf)+Rf, -1*(P-Rf)+Rf+1.5*(Q-Rf)+Rf] ;
     //pl1= [ gpt(1.5,Q), Rf, gpt(-1.3,P), gpt(-1,P)+gpt(1.5,Q)] ;
     pl1= [ gpt(1.5,B), Rf, gpt(-1.2,A), gpt(-1,A)+gpt(1.5,B)-Rf] ;
     MarkCenter(pl1, Label=["text",_s("{_}<0, {_}>0",ij),"scale",1]);
     Red(0.1) Plane( pl1 );
     Red()
       for(i=range(n))
       {
          r2= rands(0,1.5,2);
          //MarkPt( [-r2[0], r2[1]]*[A-Rf,B-Rf]+Rf, Label=false);
       }

   //pl2= [ -1.2*P, Rf, -1.5*Q] ;
   //pl2= [ gpt(-1.2,P), Rf, gpt(-1.5,Q) ];
   pl2= [ gpt(-1.2,A), Rf, gpt(-1.5,B) ];
   MarkCenter(pl2, Label=["text",_s("{_}<0, {_}<0",ij),"scale",1]);
   //MarkPts(pl2);
   Green(0.1) Plane( pl2 );
   Green()
     for(i=range(n))
     {
        r2= rands(0,1.2,2);
       // MarkPt( -1*r2*[A-Rf,B-Rf]+Rf, Label=false);
     }

   //pl3= [ 1.5*P, 2.25*P-1.75*Q, -1.5*Q, Rf] ;
   //pl3= [ 1.5*A, 2.25*A-1.75*B, -1.5*B, Rf] ;
   pl3= [gpt(1.5,A), gpt(2,A)-gpt(0.5,B)+Rf, gpt(-1.5,B), Rf];
   //echo(pl3=pl3);
   MarkCenter(pl3, Label=["text",_s("{_}>0, {_}<0",ij),"scale",1]);
   //MarkPts(pl3);
   Blue(0.1) Plane( pl3 );
   Blue()
     for(i=range(n))
     {
        r2= rands(0,2,2);
       // MarkPt( [r2[0], -r2[1]]*[A-Rf,B-Rf]+Rf, Label=false);
     }   
     
   } //test()
   
   test([0,6,0], pair="pq");
   test([0,0,0], pair="pr");
   test([7,3,0], pair="rq");
   
}


//dev_inTrianglePt2(Rf=O, pair=["+++","---"]);
//dev_inTrianglePt2(Rf=[0,6,0], pair=["-++","+--"]);
//dev_inTrianglePt2(Rf=[6,6,0], pair=["+-+","-+-"]);
//dev_inTrianglePt2(Rf=[6,0,0], pair=["++-","--+"]);
module dev_inTrianglePt2(Rf=O, pair=["+-+","-+-"], )
{
   echom("dev_inTrianglePt2");
   _h1("M=aP+bQ");
   lv=1;
   
   module test(Rf=Rf)
   {
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
     
       P= [3.1,.5]*[X,Y]+Rf; 
       Q= [0.5,2.5]*[X,Y]+Rf;
       R= [2.8,2.6]*[X,Y]+Rf; 
       pqr=[P,Q,R];
       stu=[gpt(1.5,P), gpt(1.5,Q), gpt(1.5,R)];
       S=stu[0];
       T=stu[1];
       U=stu[2];
     
       MarkPt(Rf, "Rf", color="black");
       Line(pqr, closed=1);
       Line( [P,Rf,R] );
       Line( [Q,Rf] );
     
       log_(["pqr", pqr],lv);
       log_(["stu",stu],lv );
   
     n=50;
     pqr2=[for(p=pqr)p-Rf];
     
     sign1= [for(c=range(pair[0])) pair[0][c]=="+"?1:-1];
     sign2= [for(c=range(pair[1])) pair[1][c]=="+"?1:-1];
     
     echo(sign1=sign1, sign2=sign2);   
     for(i=range(n))
       {
          r3= rands(0,.5,3);
          Green() MarkPt( zipx(sign1, r3)*pqr2+Rf, Label=false);
          Red() MarkPt( zipx(sign2,r3)*pqr2+Rf, Label=false);
       }
   
     planes= [ "+++", [R, 1.5*P, 1.5*Q,Rf] 
             , "---", [-1.5*Q, Rf, -1.5*P]
             , "-++", [R,Rf,2*Rf-P,Q]
             , "+--", [2*Rf-Q,2*Rf-R,Rf,P]
             , "+-+", [P,R,Rf, 2*Rf-Q]
             , "-+-", [2*Rf-R, 2*Rf-P,Q,Rf]
             , "++-", [P,Q,2*Rf-P,2*Rf-Q]
             , "--+", [2*Rf-Q, P,Q,2*Rf-P]
             ];
     pl_pos = h(planes, pair[0]);
     pl_neg = h(planes, pair[1]);
             
             
     Green() MarkPt(pl_pos[0], Label=["text", pair[0],"scale",3], Ball=false );
     Red()   MarkPt(pl_neg[0], Label=["text", pair[1],"scale",3], Ball=false );

     Green(0.1) Plane( to3d(pl_pos) );
     Red(0.1) Plane( to3d(pl_neg) );
 
     Line([P,2*Rf-P]);   
     Line([R,2*Rf-R]);   
     Line([Q,2*Rf-Q]);   
     Black() MarkPts( pqr, Label=["text","PQR","scale",1, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );

   }// test
   
   test( );
   
}



//dev_inTrianglePt3(Rf=O, pair=["+++","---"]);
//dev_inTrianglePt3(Rf=[0,6,0], pair=["-++","+--"]);
//dev_inTrianglePt3(Rf=[6,6,0], pair=["+-+","-+-"]);
//dev_inTrianglePt3(Rf=[6,0,0], pair=["++-","--+"]);
module dev_inTrianglePt3(Rf=O, pair=["+-+","-+-"], )
{
   echom("dev_inTrianglePt3");
   _h1("M=aP+bQ");
   lv=1;
   
   module test(Rf=Rf)
   {
     function gpt(k,pt,Rf=Rf)= k*(pt-Rf)+Rf; 
     
       P= [1,-2]*[X,Y]+Rf; 
       Q= [-2,.5]*[X,Y]+Rf;
       R= [2,1.8]*[X,Y]+Rf; 
       pqr=[P,Q,R];
       stu=[gpt(1.5,P), gpt(1.5,Q), gpt(1.5,R)];
       S=stu[0];
       T=stu[1];
       U=stu[2];
     
       MarkPt(Rf, "Rf", color="black");
       Line(pqr, closed=1);
       Line( [P,Rf,R] );
       Line( [Q,Rf] );
     
       log_(["pqr", pqr],lv);
       log_(["stu",stu],lv );
   
     n=50;
     pqr2=[for(p=pqr)p-Rf];
     
     sign1= [for(c=range(pair[0])) pair[0][c]=="+"?1:-1];
     sign2= [for(c=range(pair[1])) pair[1][c]=="+"?1:-1];
     
     echo(sign1=sign1, sign2=sign2);   
     for(i=range(n))
       {
          r3= rands(0,1,3);
          Green() MarkPt( zipx(sign1, r3)*pqr2+Rf, Label=false);
          Red() MarkPt( zipx(sign2,r3)*pqr2+Rf, Label=false);
       }
   
     planes= [ "+++", [R, 2*Rf-P,1.5*Q,2*Rf-R, 1.5*P,2*Rf-Q] 
             , "---", [1.5*P,2*Rf-Q, R, 2*Rf-P,1.5*Q,2*Rf-R]
             , "-++", [R,Rf,2*Rf-P,Q]
             , "+--", [2*Rf-Q,2*Rf-R,Rf,P]
             , "+-+", [P,R,Rf, 2*Rf-Q]
             , "-+-", [2*Rf-R, 2*Rf-P,Q,Rf]
             , "++-", [P,2*Rf-R,Q, Rf]
             , "--+", [2*Rf-Q, Rf,2*Rf-P,R]
             ];
     pl_pos = h(planes, pair[0]);
     pl_neg = h(planes, pair[1]);
             
             
     Green() MarkPt(pl_pos[0], Label=["text", pair[0],"scale",3], Ball=false );
     Red()   MarkPt(pl_neg[0], Label=["text", pair[1],"scale",3], Ball=false );

     Green(0.1) Plane( to3d(pl_pos) );
     Red(0.1) Plane( to3d(pl_neg) );
 
     Line([P,2*Rf-P]);   
     Line([R,2*Rf-R]);   
     Line([Q,2*Rf-Q]);   
     Black() MarkPts( pqr, Label=["text","PQR","scale",1, "indent",0.2] 
                    ,r=0.05, Ball=["r",0.1], Line=["closed",1] );

   }// test
   
   test( );
   
}





//dev_offTrianglePts();
module dev_offTrianglePts()
{
   function offTrianglePts(pqr,n)=
   (
     [for(i=range(n))
       //let( r3= zipx([1,-1,1], rands(0,.5,3) )) 
       let( r3= rands(0,1,3) ) 
       (r3/sum(r3))*pqr
     ]     
   
   
   );
   
   n = 20;

   //pqr=pqr(); 
   _pqr = [[1.14, 2.55,.5], [0.37, .2, -.75], [-1.55, 1.5, 0]];
   pqr= [ for(p=_pqr) p+ [2,1,0] ];
   echo(pqr=pqr); 
   MarkPts(pqr, "PQR", Line=["closed",1]);
   
   
   pts0 = offTrianglePts(pqr, n);
   Red() MarkPts(pts0, Label=false, Line=false);

   pts= randInPlanePts(pqr, n );
   Green() MarkPts(pts, Label=false, Line=false);

}

//. Solving eq with matrix 

//demo_3variables_eq();
module demo_3variables_eq()
{ 
  echom("demo_3variables_eq()");
  
  P=[4,-3,1]; Q=[2,1,3]; R=[-1,2,-5];
  pqr = [P,Q,R];
  pqr2 = transpose(pqr);
  abc= pqr;
  ds= [-10,0,17];
  xyz = solve3( abc, ds );
  ijk = [1,2,-1];
  S= ijk * pqr;
   
  //xyz= pqr*xyz;
  
  //_abc = _matrix([abc],"d=1");
  _abc = _matrix(abc, "d=0;w=3");
  _pqr2 = _matrix(pqr2, "d=0;w=4");
  _xyz = _vector(xyz);
  _ds= _matrix([ ds ], cellfmt="d=0;w=3");
  _ijk = _matrix([ijk], "d=0;w=4");
  _S = _matrix([S], "d=0;w=4");
  
  $PQR= _vector(["P","Q","R"], "w=3");//, "T<br/> <br/> ");
  _PQR= [ ["Px", "Py", "Pz"]
         , ["Qx", "Qy", "Qz"]
         , ["Rx", "Ry", "Rz"]
         ];
                       
  _mPQR= _matrix(_PQR, "w=3" );
  _mPQRt= _matrix(transpose(_PQR), "w=3" );
  
  echo(join(
  [ 
    _h2("Considering a 3-variable equation solution:")
  , _formula(
         [ join(["a0x+b0y+c0z = d0","a1x+b1y+c1z = d1","a2x+b2y+c2z = d2"],_BR) 
         ]
         , w=170)
  , _formula(
         [ R_ARROW, join([" 4x -3y + 1z =-10"," 2x+ 1y + 3z =  0","-1x+ 2y -5z = 17"],_BR) 
         ]
         , w=170)
  , _h3("where:")
  , _formula( ["abc=", _abc] )
  , _formula( ["ds= ", _ds ])
  , _formula( [ _matrix([ ["a0", "b0", "c0"]
                        , ["a1", "b1", "c1"]
                        , ["a2", "b2", "c2"]
                        ],"w=3" )
             , " x "
             , _vector( ["x","y","z"],"w=1") //_xyz
             , " ="
             , _matrix( [ [" a0<u>x</u> + b0<u>y</u> + c0<u>z</u>"]
                        , ["a1x + b1y + c1z<u></u><u></u><u></u>"]
                        , ["a2x + b2y + c2z<u></u><u></u><u></u>"]
                        ], "w=37" )
             , " ="
             , _vector( abc*xyz )
             , "...... (1)"
             ]
           )      
  , _h3("xyz can be solved using solve3():")
             
  , _formula( ["xyz= solve3( abc,ds )= ", _xyz, " = xyz" ] )
  , _formula( [ _abc, "  x  ", _xyz, " ="
             , _vector( abc*xyz )
             ]
           ) 
           
  , "<hr/>"
  
  , _h2("Now, apply to coordinate transition case:")
  , _h3(  "<br/> iP + jQ + kR = S" )
  , _h3( str("PQR (=abc)="
            , _red(" Unit axes in a new pqr-coordinate system, D ")
            , R_ARROW
            , _red(" D ={ P, Q, R }") 
            ))
  , _h3( str("ijk = "
            , " the scalar of point S in D "
            , " = S coordinate on the basis of D"
            , R_ARROW
            , _red( " [i,j,k]<sub>D</sub>" )
            ))             
  , _h3( str( "S = "
            , _red("the xyz of the new point S in the Cartesian coord")
            , R_ARROW
            , _red( " C = {X,Y,Z} = { [1,0,0], [0,1,0], [0,0,1] }")
            ))         
  , _h3( str( "S: "
            , _red( " [i,j,k]<sub>D</sub>" )
            , L_ARROW, R_ARROW
            , _red( " [x,y,z]<sub>C</sub>" )
            ))         
  
  ,_BR
  
  , _h3( "[i,j,k] * [P,Q,R] = [Sx, Sy, Sz]"  )
  , _formula( [
            "In OpenSCAD, [i,j,k]*[P,Q,R] = "
            , _vector( ["dot prod of [i,j,k] and P"
                       ,"dot prod of [i,j,k] and Q"
                       ,"dot prod of [i,j,k] and R"
                       ]
                     ,"w=27")
           ])          
  , _formula(  [ 
          "[i j k]", vPQR
        , "= [i,j,k]"
        , _matrix(_PQR, "w=3")
        , "="
        , _vector( ["iPx+jQx+kRx", "iPy+jQy+kRy", "iPz+jQz+kRz"], "w=12" )
        , "="
        , _vector( ["Sx", "Sy", "Sz"], "w=4" )
        , "...... (2)"
        ] )
           
  , _h3(str("Re-write (2) in the form of (1) by applying : "
           , _red( "AB = B<sup>T</sup>A")
           ))         
  , _formula(  [ 
         _vector( ["Px Py Pz", "Qx Qy Qz", "Rx Ry Rz"], "w=9", sup="T" )
        , _vector( ["i","j","k"], "w=2" )
        , "="
        , _vector( ["Sx", "Sy", "Sz"], "w=4" )
        , "...... (3)"
        ] )           
    
  , _h3("So, to solve ijk: ")
  , _h2( _red(_formula( ["solve3("
              //, _span("[P,Q,R]<sup>T</sup>", s="font-size:18px")
              , _vector( ["P","Q","R"], "w=2", sup="T")
              , ","
              , _vector( ["Sx", "Sy", "Sz"], "w=4", sub="C" )
              ,")="
              , _vector(["i","j","k"], "w=2", sub="D") ] 
              )))
  
  , _h2(_blue(_formula([ 
              _vector(["i","j","k"], "w=2", sub="D") 
              //_span("[i,j,k]", s="font-size:18px")
              , " x  "
             ,  _vector( [ "P","Q","R"], "w=2")
             , " ="
             , _vector( ["Sx", "Sy", "Sz"], "w=4", sub="C" )
             ]
           ) )) 
  ,"<hr/>"         
  , _h3("For 2 different coordinate system, D={P,Q,R}, E={S,T,U}:")

  , _h2(_formula([ 
              _vector(["i","j","k"], "w=2", sub="D") 
              , "x"
             ,  _vector( [ "P","Q","R"], "w=2")
             , "="
             , _vector( ["Sx", "Sy", "Sz"], "w=4", sub="C" )
             , "="
             , _vector(["l","m","n"], "w=2", sub="E") 
              , "x"
             ,  _vector( [ "S","T","U"], "w=2")
             ]
           ) ) 

  , _h2(_red(_formula([ 
              _vector(["i","j","k"], "w=2", sub="D") 
              , "x"
             ,  _vector( [ "P","Q","R"], "w=2")
             , "="
             , _vector(["l","m","n"], "w=2", sub="E") 
              , "x"
             ,  _vector( [ "S","T","U"], "w=2")
             ]
           ) )) 
  , _h3("Convert from D to E: -- see coord_trans()")
  
  ]));
    
}


//vectors_multiplication();
module vectors_multiplication()
{
 // https://stattrek.com/matrix-algebra/vector-multiplication.aspx?Tutorial=matrix
 
 echom("vectors_multiplication()");
 _h2("Two vectors: A = [i,j,k], B=[c,d,f]");
 
 ijk = ["i","j","k"];
 fgh = ["f","g","h"];
 vijk = _vector(ijk,"w=1");
 vfgh = _vector(fgh,"w=1");
 module span(x){ _span(x, s="font-size:16px");}
 
 span( _formula( ["Two vectors: A =", vijk, ",  B =", vfgh ] ));
 
 _h2("Inner product (= dot product = scalar product = projection product = product of (projection of A onto B) and B = |A||B|cos(a)):");
 
 
 span(_formula([ "A<sup>.</sup>B = ", "A<sup>T</sup>B = [i,j,k]"
               , vfgh
               , "= if+jg+kh = |A||B|cos(a)"
               , R_ARROW
               , "In OpenSCAD:"
               , _blue("A*B")
               //, _b( "= dot product = scalar product")
               ])
     );
      
 _h2("Outer product (= tensor product):");
 span(_formula([ "A",MATH_TENSOR_PROD,"B = ", "AB<sup>T</sup> = "
                , vijk
                , " [f,g,h]="
                , _matrix( [ ["if","ig","ih"]
                           , ["jf","jg","jh"]
                           , ["kf","kg","kh"]
                           ], "w=3")
                , R_ARROW
               , "In OpenSCAD:"
               , _blue("A*transpose(B)")
               ])   
      );
      
 _h2("Cross product (= vector product = area product = normal vector = |A||B|sin *(normal unit)):");
 
 
 span(_formula([ "cross(A,B)= AxB=-BxA=", vijk
               , vfgh
               , "= [jh-kg, -(ih-kf), ig-jf]"
               , R_ARROW
               , "In OpenSCAD:"
               , _blue("cross(A,B)")
               ])
     );
_hr();
 ______(_red("[i,j,k]*P"));
 
span(_formula([ "[i,j,k]*P = "
               , _vector(["i","j","k"], "w=1", sup="T")
               , _vector(["Px","Py","Pz"], "w=2")
               , "= [i,j,k]"
               , _vector(["Px","Py","Pz"], "w=2")
               , "= iPx+jPy+kPz"
               ])   
      );
 
 ______(_red("[i,j,k]*[P,Q,R]"));
 
 
 _h2( str("In case of ", _red("[i,j,k]*[P,Q,R]"), ", which is "
         , _red(" dot products of [i,j,k] with x,y,z components of P,Q,R.")
         , R_ARROW, " 1 pt"
         ));

 span(_formula([ "[i,j,k]*[P,Q,R] = "
               , _vector(["i","j","k"], "w=1", sup="T")
               , vPQR //_vector(["P","Q","R"], "w=2")
               
               , "= [i,j,k]"
               , _matrix( [ ["Px","Py","Pz"],["Qx","Qy","Qz"], ["Rx","Ry","Rz"]]
                        , "w=3" )
                ])   
      );
 span(_formula([ 
               "="
               , _span("[", s="font-size:48px")
                , "[i,j,k]", _vector( ["Px","Qx","Rx"], "w=2")
                , ","
                , "[i,j,k]", _vector( ["Py","Qy","Ry"], "w=2")
                , ","
                , "[i,j,k]", _vector( ["Pz","Qz","Rz"], "w=2")
                , _span("]", s="font-size:48px") 
         
               , "="
                , _vector( [ "iPx+jQx+kRx"
                           , "iPy+jQy+kRy"
                           , "iPz+jQz+kRz"
                           ], "w=13" )
                ])   
      );

 ______(_red(str("[i,j,k]",MATH_TENSOR_PROD,"[P,Q,R]")));
 span(_formula([ "[i,j,k]",MATH_TENSOR_PROD ,"[P,Q,R] = "
               , vijk //_vector(["i","j","k"], "w=2")
               , _vector(["P","Q","R"], "w=1", sup="T")
               , "="
               , vijk //_vector(["i","j","k"], "w=2")
               , "[P Q R]"
               
               , "="
               , _matrix( [ ["iP","iQ","iR"]
                          , ["jP","jQ","jR"]
                          , ["kP","kQ","kR"]]
                        , "w=3" )
               ])   
      );  
 span(_formula( [ "=[ i[P,Q,R],j[P,Q,R],k[P,Q,R]]", R_ARROW, "3 planes || PQR"] ));         
      
 ______(_red(str("[i,j,k]",MATH_TENSOR_PROD,"P")));
 span(_formula([ "[i,j,k]",MATH_TENSOR_PROD ,"P = "
               , vijk //_vector(["i","j","k"], "w=2")
               , _vector(["Px","Py","Pz"], "w=1", sup="T")
               , "="
               , vijk //_vector(["i","j","k"], "w=2")
               , "[Px Py Pz]"
               
               , "="
               , _matrix( [ ["iPx","iPy","iPz"]
                          , ["jPx","jPy","jPz"]
                          , ["kPx","kPy","kPz"]]
                        , "w=4" )
               , R_ARROW
               , " 3 pts along OP line"         
                ])   
      );      
      
 ______("Note this OpenSCAD behavior");
  span(_formula([ _red("n*[P,Q,R]"), "= [nP,nQ,nR]"
               , "="
               , _vector(["nP+0Q+0R","0P+nQ+0R","0P+0Q+nR"], "w=10")
               , "="
               , _vector(["n 0 0","0 n 0","0 0 n"], "w=5")
               , _vector(["P","Q","R"], "w=")
               , R_ARROW
               , " 3 pts"
                ])   
      ); 
  span(_formula([ _red("[i,j,k]*[P,Q,R]"), "= dot product (see above) "
               , "="
               , _vector( [ "iPx+jQx+kRx"
                           , "iPy+jQy+kRy"
                           , "iPz+jQz+kRz"
                           ], "w=13" )
                           
               , R_ARROW
               , " 1 pt"
                ])   
      ); 
  span(_formula([ _red("cross([i,j,k],[P,Q,R])"), "= cross product "
               , "=",_red("undef")
               , R_ARROW, " has to be cross([i,j,k],[a,b,c]])"
                ])   
      ); 
      
}

//trans_coord();
module trans_coord()
{
  // ijk_D * PQR = lmn_E + STU
  // where D = {P,Q,R}, E = {S,T,U}
  echom("trans_coord()");
  _h2( "Coordinate Transition:");
  _hr();
  _h2(join([ _ss("A {_:c=red}= {_:c=red} in the standard Cartessian coord (C={_:c=red})
            <br/> has scalars= {_:c=red} in a coord D={_:c=red}"
           , ["pt","[a,b,c]<sub>C</sub>","{X,Y,Z}"
             , "[i,j,k]<sub>D</sub>","{P,Q,R}"]
             )
          , _BR, R_ARROW, 
          , _red("[i,j,k]<sub>D</sub>*pqr = [a,b,c]<sub>C</sub>")
          ]));
  _h2( _red(_formula( ["solve3("
              //, _span("[P,Q,R]<sup>T</sup>", s="font-size:18px")
              , _vector( ["P","Q","R"], "w=", sup="T")
              , ","
              , _vector( ["a", "b", "c"], "w=1", sub="C" )
              ,")="
              , _vector(["i","j","k"], "w=1", sub="D") ] 
              )));
  _h2( str("3D ", R_ARROW, " "
          ,_red( " ijk = solve3( transpose(pqr), abc) ")
          ));
  _h2( str("2D ", R_ARROW, " "
          ,_red( " ij = solve2( transpose(pq), ab) ")
          ));
  _hr();
  
  _h3( str("Transfer from a coordinate system, "
          , _red("D={P,Q,R}")
          , " to another one, "
          , _red("E={S,T,U}")
          )
     );
  _h2(_red(_formula([ 
              _vector(["i","j","k"], "w=1", sub="D") 
              , "x"
             ,  _vector( [ "P","Q","R"], "w=1")
             , "="
             , _vector(["l","m","n"], "w=1", sub="E") 
              , "x"
             ,  _vector( [ "S","T","U"], "w=1")
             ]
           ) )) ;  
  
  pqr= pqr();
  stu= pqr();
  
  echo(pqr=pqr);
  echo(stu=stu);
  
  ijk = [1,2,-3];
  
  V = ijk*pqr; 
  
  lmn = solve3( transpose(stu), V);
  echo(lmn = lmn);
  
  MarkPts(pqr, "PQR", Line=["closed",1] );
  MarkPts(stu, "STU", Line=["closed",1] );
  
  Red() MarkPt(V, "V");
  MarkPt( lmn*stu, "W", Ball=["r",0.1, "color",["green",0.5]]);
  _h2(_red("solve3( transpose(stu), ijk*pqr ) = lmn"));
  echo( lmn = solve3(transpose(stu),ijk*pqr) );
}  

//dev_trans_coord();
module dev_trans_coord()
{
   echom("dev_trans_coord()");
   //----------------------------------------
   pqr = pqr();
   pqr = [[-1.06, -1.84, -0.91], [1.37, -0.71, -1.25], [-1.12, -0.93, -1.47]];
   echo(pqr=pqr);
   MarkPts( pqr, "PQR" ); //Plane(pqr);
   //----------------------------------------
   stu = [for(p=pqr(p=8,r=6)) p +[d,d,d]] ;
   stu=  [[-0.357855, -2.50991, 4.47961], [4.74, 3.65, 4.22], [0.405952, 1.64818, 0.585619]];
   echo(stu=stu);
   MarkPts( stu, "STU" ); //Plane(stu, color=["red",0.3]);

   coord_pqr= [pqr[1], pqr[0], N2(pqr), N(pqr)];
   coord_stu= [stu[1], stu[0], N2(stu), N(stu)];
   
   coordf =  [[1.37, -0.71, -1.25], [-1.06, -1.84, -0.91], [0.950109, 0.0385334, -1.76321], [1.49047, -1.22451, -2.09898]];
   coordt = [[4.74, 3.65, 4.22], [-0.357855, -2.50991, 4.47961], [4.35224, 3.93395, 3.34306], [5.40602, 3.0786, 3.74049]];
   
   coordf = coordPts(pqr, len=1);
   coordt = coordPts(stu, len=1);
   
   for(i=[1:3])
   {
     color(COLORS[i]){
       Arrow( [ coordf[0], coordf[i]] );
       Arrow( [ coordt[0], coordt[i]] );
     }
   }
   //UnitCoord(pqr);
   //UnitCoord(stu);
   
   cube1 = cubePts( site=pqr );
   //echo(cube1=cube1);
   //MarkPts(cube1, Line=false);
   Cube(site=pqr);
   //color(undef, 0.8)
   //polyhedron( points= cube1, faces=faces("rod", nside=4,nseg=1)); 
 
   //=================================================
   
   /*
      x   x   P              P   
      y = y * Q  = [x y z]_f Q
      zC  zf  R              R 
                 coordf
                 
      x2   x   S              S   
      y2 = y * T  = [x y z]_t T
      z2C  zt  U              U 
                 coordt
  
      Known: x   x   
             y = y     
             zf  zt 
  
      x2             S                PT    S
      y2 = [x y z]_t T  = ( [x y z]_c Q   ) T  
      z2C            U                R     U  
      
            x               S  
       =  ( y * [P,Q,R] ) * T  
            zC              U
       
                 coordt
            
    
   */  
   pqr_ut = transpose(slice(coordf,1));
   stu_u  = slice(coordt,1);    
   cube2 = [ for(pt=cube1)
              ((pt-coordf[0])*pqr_ut)*stu_u+coordt[0]
           ];
   echo(cube2=cube2);
   MarkPts(cube2);
   //Cube(pts=cube2);
   
    
   color("red", 0.2)
   polyhedron( points= cube2, faces=faces("rod", nside=4,nseg=1)); 
   


    function movePts2(pts, from, to)= // try approach of coord transition (2018.21.1)  
    (                                 // See coord_trans() in study_vectors.scad
       let( _from = from?from: [X,O,Y]
          , _to = to?to:[X,O,Y]
          , _coordfrom = len(_from)==4?_from: [_from[1], _from[0], N2(_from), N(_from)]
          , _coordto = len(_to)==4?_to: [_to[1], _to[0], N2(_to), N(_to)]
          , Of = _coordfrom[0]
          , Ot = _coordto[0]
          , uvf= [for(i=[1:3]) uv(_coordfrom[i]-Of)]
          , uvt= [for(i=[1:3]) uv(_coordto[i]-Ot)]
          , rtn = [ for(pt=pts) solve3(transpose(uvt), (pt-Of)*uvf)+Ot ] 
       )
       echo(_from=_from, _to=_to)
       echo(_coordfrom=_coordfrom, _coordto=_coordto)
       echo(Of = Of, Ot=Ot)
       echo(movePts=rtn)
       rtn
    );


}
//  
//P=randPt(); Q=randPt();
//pq = [P,Q];
//MarkPts(pq, "PQ");
//Line([P,O,Q]);
//
//sum= rand(range(-2,3));
//i= rand(range(-2,4));
//j= sum-i;
//a= rand(range(-2,4));
//b= sum-a;
//S= [i,j]*pq;
//T= [a,b]*pq;
//st=[S,T];
//
//
//echo(ij=[i,j],ab=[a,b], ij_eq_ab= _color(str([i,j]==[a,b])
//    , [i,j]==[a,b]?"green":"red"));
//echo(pq=pq, st=st);
//isp=isparal(pq, [S,T]);
//isonl=isonline(O, [Q-P, T-S]);
//echo(isparal= isp?_green(isp):_red(isp));
//echo(isonline= isonl? _green(isonl):_red(isonl));
//Red() Arrow( [O, Q-P],r=0.25 );
//Green() Arrow([O,T-S], r=0.25 );



//MarkPts([S,T],"ST");
//echo(isparallel=isparal(pq, [S,T]));
//Red() Arrow( [O, Q-P],r=0.25 );
//Green() Arrow([O,T-S], r=0.25 );
//echo(isonline=isonline(O, [Q-P, T-S]));
//
//echo(pq=pq, st=st, isonline=isonline(O, [Q-P, T-S]) );
//echo(A=Q-P, B=T-S, is_O_on_AB= isonline(O, [Q-P,T-S]));
//qpst=[Q-P,T-S];
    
// pq = [[2.49, -2.58, 1.23], [2.71, 0.39, 1.06]], st = [[-3.15, -6.33, -0.72], [-2.27, 5.55, -1.4]], isonline = false
// pq = [[0.04, 0.51, -0.64], [2.76, -0.55, 2.95]], st = [[-2.64, 2.08, -4.87], [8.24, -2.16, 9.49]], isonline = false
// pq = [[1.48, 0.57, -0.24], [1.05, -0.53, -0.16]], st = [[2.1, -1.06, -0.32], [1.24, -3.26, -0.16]], isonline = false
// pq = [[1.31, -1.05, -2.96], [0.55, 1.66, -2.79]], st = [[-0.42, 8.74, -5.24], [2.62, -2.1, -5.92]], isonline = true

//echo([2,3,4]*[5,6,7], cross([2, 3, 4], [5, 6, 7]));     // produces ECHO: [-3, 6, -3]
//echo(cross([2, 1, -3], [0, 4, 5]));    // produces ECHO: [17, -10, 8]
