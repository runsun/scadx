//if(IS_SHOW_FILE_LOADING) echom( "scadx_range.scad" );


//==================================================
{ //fidx
function fidx(o,i,cycle=false, fitlen=false)=
(
	/*
	len: 1 2 3 4 5
		 0 1 2 3 4   i>=0
   		-5-4-3-2-1   i <0    

  				   0 1 2 3 4   : legal index
  		-5-4-3-2-1 0 1 2 3 4   : proper index

	When out of range: 

		if cycle=false: return undef
		if cycle=true: 
			cycle through array as many as needed. So if cycle is set
					to true, it never returns false.  

	*/
  let( L=isint(o)?o:len(o), j = cycle? mod(i,L):i )
	-L<=j&&j<L ? ( (j<0? L:0)+j )
             : fitlen==true?(L-1):undef
);

    fidx=["fidx", "o,i,cycle=false,fitlen=false", "int|undef", "Index, Array, String, Inspect",
    " Try to fit index i to range of o (arr or str). If i is negative, convert it 
    ;; to *legal* index value (means, positive int). For o of len L, this would mean 
    j;; -L &lt;= i &lt; L. That is, i should be in range *(-L,L]*. 
    ;; 
    ;; New 2014.8.17: o could be an integer for range 0 ~ o
    ;;
    ;; If i out of this range, return undef (when cycle=false) or:
    ;; -- set fitlen=true to return the last index
    ;; -- set cycle=true to loop from the other end of o. So if cycle=true, a given i 
    ;;     will never go undef, no matter how large or small it is.  
    ;;
    ;; ref:
    ;;
    ;; get(arr,i)
    ;; range(arr)
    "];

    function fidx_test( mode=MODE, opt=[] )=(//==================

        doctest( fidx, 
        [
            "// string "
          , [ fidx("789",2),2, "'789',2" ]
          ,	[ fidx("789",3), undef, "'789',3",  ["//","out of range"] ]
          ,	[ fidx("789",-2),1,"'789',-2" ]
          ,	[ fidx("789",-3),0, "'789',-3" ]
          ,	[ fidx("789",-4),undef, "'789',-4", ["//","out of range"] ]
          ,	[ fidx("",0),undef, "'',0" ]
          ,	[ fidx("789","a"), undef, "'789','a'" ]
          ,	[ fidx("789",true), undef, "'789',true" ]
          ,	[ fidx("789",[3]), undef, "'789',[3]" ]
          ,	[ fidx("789",undef), undef, "'789',undef" ]
          , "// array"
          ,	[ fidx([7,8,9],2), 2, "[7,8,9],2", ]
          ,	[ fidx([7,8,9],3), undef, "[7,8,9],3", ["//","out of range"] ]
          ,	[ fidx([7,8,9],-2), 1, "[7,8,9],-2", 1 ]
          ,	[ fidx([7,8,9],-3), 0, "[7,8,9],-3", 0 ]
          ,	[ fidx([7,8,9],-4), undef, "[7,8,9],-4", ["//","out of range"] ]
          ,	[ fidx([],0), undef,  "[],0" ]
          ,	[ fidx([7,8,9],"a"), undef, "[7,8,9],'a'" ]
          ,	[ fidx([7,8,9],true), undef,"[7,8,9],true" ]
          ,	[ fidx([7,8,9],[3]), undef,"[7,8,9],[3]" ]
          ,	[ fidx([7,8,9],undef), undef,"[7,8,9],undef" ]

          , "// If out of range, return undef, or:"
          , "//  -- Set cycle to go around the other end."
          , "//     (it never goes out of bound) "
          , "//  -- Set fitlen to return the last index."
          , "//"
          ,	[ fidx([7,8,9],3), undef,"[7,8,9],3", ["//","out of range"] ]
          ,	[ fidx([7,8,9],3,fitlen=true), 2, "[7,8,9],3,fitlen=true"
                                   , ["//","fitlen"] ]
          , [ fidx([7,8,9],3,true), 0, "[7,8,9],3, cycle=true" ]
          ,	[ fidx([7,8,9],4,true), 1, "[7,8,9],4, cycle=true" ]
          ,	[ fidx([7,8,9],7,true), 1, "[7,8,9],7, cycle=true" ]
          ,	[ fidx([7,8,9],-4), undef,"[7,8,9],-4",  ["//","out of range"] ]
          ,	[ fidx([7,8,9],-4,true), 2, "[7,8,9],-4, cycle=true"  ]
          ,	[ fidx([7,8,9],-16,true), 2, "[7,8,9],-16, cycle=true" ]
          ,	[ fidx([7,8,9],-5,true), 1, "[7,8,9],-5, cycle=true" ]
          
          ,"//"
          ,"// New 2014.8.17: o can be an integer for range 0~o"
          ,"//"
          ,	[ fidx(3,5), undef,"3,5", ["//","out of range"] ]
          ,	[ fidx(3,5,fitlen=true), 2, "3,5,fitlen=true"
                , ["//","get the last idx"] ]
          ,	[ fidx(3,4,cycle=true), 1, "3,4,cycle=true"
                , ["//","cycle from begin"] ]
          , [ fidx(3,-2), 1, "3,-2" ]
          ,	[ fidx(3,-3), 0, "3,-3" ]
          ,	[ fidx(3,-4), undef,"3,-4", ["//","out of range"] ]
          ,	[ fidx(3,-4,cycle=true), 2, "3,-4,cycle=true",  ["//","cycle=true"] ]
          , "//"	

          ], mode=mode, opt=opt
        )
    );
} // fidx
//echo( fidx_test( mode=112 )[1] );

//==================================================
{//get
 /*
  dev note 2016.12.10:
  
  allowing app and pp might not be good idea. For one it complicates
  things. 2ndly most usage of get doesn't involve them. This func will
  be used very frequently so it has to be very fast. 
  
  Besides, they are troublesome. For example, 
  
    get( o, 3, app=[2,3,4] )
  
  it can't be tell if adding a point of [2,3,4], or 3 values (2,3,4) is 
  intended
  
  
 */ 
  
//function get(o,i, app=[], pp=[], cycle=false)=
//
// The "swizzling" featured in opengl can be performed by get()
// https://www.khronos.org/opengl/wiki/Data_Type_(GLSL)#Swizzling
// 
// Similar to select() example in the OpenSCAD doc:
// https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/List_Comprehensions#Sorting_a_vector
//

function get(o,i)= 
                   // Big change 2018.4.26: 
                   //    total rewrite, much simpler. Remove the flatten feature.
                   // Big change 2018.2.23: 
                   //   Remove feature of cycle and avoid fidx
                   //   because too slow. Still keep the feature
                   //   of -i and ratio_i. This is already much faster.
                   // Note 2018.4.26:
                   //  A simplified version could be:
                   //   get(o,i)= 
                   //     let( i = (0<i && i<1)? floor(len(o)*i) )
                   //     o[ (len(o)+ i %len(o))%len(o)]
(         
   isarr(i)||istype(i,"range") ? // allow array of i's 
     [ for(j=i) get(o,j) ]
   :o[ (
         ( (0<i && i<1)? floor(len(o)*i):i // allow ratio
         ) %len(o)                         // assure negative in range
                                           //   like get([1,2,3], -10) 
         + len(o)                          // assure positive     
       )%len(o)                            // assure in range
     ]
 
//   isarr(i)||istype(i,"range")? 
//   [ for( ii=i
//        , j= (0<ii && ii<1)? floor(len(o)*ii)  // allow ratio
//             : ii<0? len(o)+ii
//             : ii>=len(o)? ii%len(o)  // allow i >= len(o)
//             : ii
//        ) 
//        o[j] 
//   ] 
//   : let( j= (0<i && i<1)? floor(len(o)*i)  // allow ratio
//             : i<0? len(o)+i
//             : i>=len(o)? i%len(o) // allow i >= len(o)
//             : i
//        )     
//       o[j]
      
//function get(o,i, cycle=false)= 
//(
//   isarr(i)||istype(i,"range")? 
//   [ for( ii=i
//        , j= (0<ii && ii<1)? floor(len(o)*ii):ii  // allow ratio
//        ) 
//        o[ fidx(o,j,cycle=cycle)] 
//   ] 
//   : o[ fidx( o
//            , (0<i && i<1)? floor(len(o)*i):i // allow ratio
//            , cycle=cycle ) 
//      ]
       
//    let( L = len(o)
//       , i = (0<i && i<1)? floor(L*i):i  // allow i as ratio
//       , iarr = isarr(i)
//       //, rtn = iarr? [for(j=i) get(o,j, cycle=cycle)] 
//       , rtn = iarr? [ for( ii=i
//                          , j= (0<ii && ii<1)? floor(L*ii):ii  // allow ii as ratio
//                          ) 
//                       o[ fidx(o,j,cycle=cycle)] 
//                     ] 
//                   : o[ fidx(o,i,cycle=cycle) ]
//       , rtn2= app? concat( rtn, ispt(app)?[app]:app )
//                 : rtn 
//       )
//       pp ? concat( ispt(pp)?[pp]:pp, rtn2  )
//                 : rtn2
);
       
    get=["get", "o,i,cycle=false", "item|arr", "Index, Inspect, Array, String" ,
     " Given a str or arr (o), an index (i), get the i-th item. 
    ;; i could be negative.
    ;;
    ;; New argument *cycle*: cycling from the other end if out of range. This is
    ;; useful when looping through the boundary points of a shape.
    ;;
    ;;   arr= [20, 30, 40, 50]; 
    ;;   get( arr, 1 )= 30
    ;;   get( arr, -2 )= 40
    ;;   get( 'abcdef', -2 )= 'e'
    ;;   //New 2014.6.27: cycle
    ;;   get( arr, 4 )= undef
    ;;   get( arr, 4, cycle=true )= 20
    ;;   get( arr, 10 )= undef
    ;;   get( arr, 10, cycle=true )= 40
    ;;   get( arr, -5 )= undef
    ;;   get( arr, -5, cycle=true )= 50
    ;;   // New 2015.6.20: i could be array
    ;;   get(arr,[0,-1])= [20, 50]
    ;;   // New 2015.9.10: i could be float:
    ;;   get(arr,0.5) ==> the the mid item
    ;;   get(arr, [0, 0.5,-1]) => the the 1st, mid, and last items 
    ;;
    
    "];

    //;; Note: this function flattens an index array:
    //;;
    //;;  get( [10,11,12], [[0,1],[1,2]] ) = get( [10,11,12], [0,1,1,2] )
    
    function get_test( mode=MODE, opt=[] )=
    (
        let( arr = [20,30,40,50]
           , arr2= [10,11,12,13,14,15,16,17,18]
           , pts=[ [0,1,2],[3,4,5],[6,7,8]]
           )

        doctest( get,
        [ 
        "//## array ##"
        , "var arr"
        , ""
        , [ get( arr, 1 ), 30, "arr, 1" ]
        , [ get(arr, -1), 50, "arr, -1", ]
        , [ get(arr, -2), 40, "arr, -2" ]
        , [ get(arr, 5), 30, "arr, 5" ]
        , [ get(arr, 13), 30, "arr, 13" ]
        , [ get(arr, true), undef, "arr, true" ]
        , [ get([], 1), undef, "[], 1" ]
        , [ get([[2,3],[4,5],[6,7]],-2), [4,5], "[[2,3],[4,5],[6,7]], -2" ]
        ,
        //, "//!!! NOTE !!!"
        //, "//This flattens an index array:"
        //, ""
        //, [ get( [10,11,12], [[0,1],[1,2]] ), [10,11,11,12], "[10,11,12], [[0,1],[1,2]]"]
        , [ get( [10,11,12], [0,1,1,2] ), [10,11,11,12], "[10,11,12], [0,1,1,2]"]
        
        , "var arr2"
        , [get(arr2,[2:5]), [12,13,14,15], "arr2,[2:5]"]
        
        , "//## string ##"
        , ""
        , [ get("abcdef", -2), "e", "'abcdef', -2" ]
        , [ get("abcdef", 6), "a", "'abcdef', 6" ]
        , [ get("abcdef", false), undef, "'aabcdef', false" ]
        , [ get("abcdef", [1:3]), ["b","c","d"], "'aabcdef', [1:3]" ]
        , [ get("", 1), undef, "'', 1" ]
        
//        , "//New 2014.6.27: cycle"
//        , [ get(arr,4), undef, "arr, 4" ]
//        , [ get(arr,4,cycle=true), 20, "arr, 4, cycle=true" ]
//        , [ get(arr,10), undef, "arr, 10" ]
//        , [ get(arr,10,cycle=true), 40, "arr, 10, cycle=true" ]
//        , [ get(arr,-5), undef, "arr, -5" ]
//        , [ get(arr,-5,cycle=true), 50, "arr, -5, cycle=true" ]
       // ,""
        //, "//New 2015.6.20: i could be an array. This replaces sel()"
        //, [ get(arr, [0,-1]), [20,50], "arr,[0,-1]" ]
        //, [ get(arr, [[0,-1],[1,-2]]), [[20,50],[30,40]], "arr,[[0,-1],[1,-2]]" ]
        ,""
        //, "//New 2015.6.20: app"
        //, [ get(arr, -1,app=100), [50,100], "arr, -1,app=100", ]
        //, [ get(arr, -1,app=[60,70]), [50,60,70], "arr, -1,app=[60,70]", ]
        //, [ get(arr, -1,app=[60,70,80]), [50,60,70,80], "arr, -1,app=[60,70,80]", ]
        //, [ get(arr, [0,-1],app=100), [20,50,100], "arr,[0,-1],app=100" ]
        //, [ get(arr,[0,-1],app=[60,70]),[20,50,60,70],"arr,[0,-1],app=[60,70]" ]
//
        //,"//New: pp"
        //, [ get(arr, -1,pp=100), [100,50], "arr, -1,pp=100", ]
        //, [ get(arr, -1,pp=[60,70]), [60,70,50], "arr, -1,pp=[60,70]", ]
        //, [ get(arr, [0,-1],pp=100), [100, 20,50], "arr,[0,-1],pp=100" ]
        //, [ get(arr,[0,-1],pp=[60,70]),[60,70, 20,50],"arr,[0,-1],pp=[60,70]" ]
        //,""
        //, [ get(arr, -1,pp="a",app="b"),["a",50,"b"],"arr,-1,pp='a',app='b'"]
        //
        //,""
        //,"In the following cases, we get 2 pts, and append new pt(s) :"
        //,""
        //,"var pts"
        //, [get(pts, [0,-1], app=[0,0,1]), [[0,1,2],[6,7,8],[0,0,1]]
           //, "pts, [0,-1], app=[0,0,1]"]
        //, [get(pts, [0,-1], app=[[0,0,1],[0,0,2]])
                //, [[0,1,2],[6,7,8],[0,0,1],[0,0,2]]
           //, "pts, [0,-1], app=[[0,0,1],[0,0,2]]"]
        
       // ,""
        ,"// New: i could be float (0&lt;i&lt;1), i=0.5: get mid point"
        ,"var arr2"
        , str("len(arr2) = ",len(arr2) )
        , [ get(arr, 0.5), 40, "arr,0.5"]
        , [ get(arr2, 0.5), 14, "arr2,0.5"]
        , [ get(arr2, 0.3), 12, "arr2,0.3"]
        , [ get(arr2, [0,0.5,-1]), [10,14,18], "arr2,[0,0.5,-1]"]
        ]
        , mode=mode, opt=opt,  scope=["arr", arr, "arr2",arr2,"pts",pts]
        )
    );
} // get
//echo( get_test( mode=12 )[1] );

//==================================================
{//get2
function get2(o,i)= //,cycle=true)=
(   
    i==undef? [ for(i=[0:len(o)-1]) get2(o,i) ]
    : get(o, [ i, i+1]) //==(len(o)-1)?0:i+1] )    
);  
    get2=["get2", "o,i", "item", "Index, Inspect, Array, String" ,
     " Given a str or arr (o), an index (i), get the i and i+1 items. 
    ;; i could be negative.
    ;;
    "];
    
    get2_tests=[ 
	  "## array ##"
	 , [ [[20,30,40,50], 1], get( [20,30,40,50], 1 ), 301 ]
	 , [ [[20,30,40,50], -1], get([20,30,40,50], -1), 50 ]
	 , [ [[20,30,40,50], -2], get([20,30,40,50], -2), 40 ]
	 , [ [[20,30,40,50], true], get([20,30,40,50], true), undef ]
	 , [ [[], 1], get([], 1), undef ]
	 , [ [[[2,3],[4,5],[6,7]], -2], get([[2,3],[4,5],[6,7]], -2), [4,5] ]
	 ,"## multiarray ##"
	 , [ [[[2,3],[4,5],[6,7]], -2, 0], get([[2,3],[4,5],[6,7]], -2,0), 4 ]
	 , [ [[[2,3],[4,5],[6,7]], -2, -1], get([[2,3],[4,5],[6,7]], -2,-1), 5 ]
	 , "## string ##"
	 , [ ["abcdef", -2], get("abcdef", -2), "e" ]
	 , [ ["abcdef", false], get("abcdef", false), undef ]
	 , [ ["", 1], get("", 1), undef ]
	 ];
    
}// get2
//==================================================
{// get3
function get3(o,i)= //,cycle=true)=
(   
    i==undef? [ for(i=[0:len(o)-1]) get3(o,i) ]
    :get(o, [ i, i+1, i+2] )    
);  
    get3=["get3", "o,i,cycle=false", "item", "Index, Inspect, Array, String" ,
     " Given a str or arr (o), an index (i), get the i-1, i, and i+1 items. 
    ;; i could be negative.
    ;;
    "];
    function get3_test( mode=MODE, opt=[] )=
    (
        let( arr = [20,30,40,50] )
                   //0  1  2  3
        doctest( get3,
        [ 
        "## array ##"
        , "var arr"
        , [ get3( arr ), [[20,30,40],[30,40,50],[40,50,20], [50,20,30]], "arr" ]
        , [ get3( arr, 1 ), [30,40,50], "arr, 1" ]
        , [ get3( arr, 0 ), [20,30,40], "arr, 0" ]
        , [ get3( arr, -1 ), [50,20,30], "arr, -1" ]
        , "## string ##"
        , [ get3( "abcdef", 1 ), ["b","c","d"], "'abcdef', 1" ]
        , [ get3( "abcdef", 0 ), ["a","b","c"], "'abcdef', 0" ]
        , [ get3( "abcdef", -1 ), ["f","a","b"], "'abcdef', -1" ]

        ]
        , mode=mode, opt=opt,  scope=["arr", arr]
        )
    );

    //echo( get3_test( mode=12 )[1] );
}// get3

{// get3m
function get3m(o,i)= //,cycle=true)=
(   
    i==undef? [ for(i=[0:len(o)-1]) get3m(o,i) ]
    :get(o, [ i-1, i, i+1] )    
);  
    get3m=["get3m", "o,i,cycle=false", "item", "Index, Inspect, Array, String" ,
     " Given a str or arr (o), an index (i), get the i-1, i, and i+1 items. 
    ;; i could be negative.
    ;;
    "];
    function get3m_test( mode=MODE, opt=[] )=
    (
        let( arr = [20,30,40,50] )
                   //0  1  2  3
        doctest( get3m,
        [ 
        "## array ##"
        , "var arr"
        , [ get3m( arr ), [[50,20,30],[20,30,40], [30,40,50],[40,50,20]], "arr" ]
        , [ get3m( arr, 1 ), [20,30,40], "arr, 1" ]
        , [ get3m( arr, 0 ), [50,20,30], "arr, 0" ]
        , [ get3m( arr, -1 ), [40,50,20], "arr, -1" ]
        , "## string ##"
        , [ get3m( "abcdef", 1 ), ["a","b","c"], "'abcdef', 1" ]
        , [ get3m( "abcdef", 0 ), ["f","a","b"], "'abcdef', 0" ]
        , [ get3m( "abcdef", -1 ), ["e","f","a"], "'abcdef', -1" ]

        ]
        , mode=mode, opt=opt,  scope=["arr", arr]
        )
    );

    //echo( get3_test( mode=12 )[1] );
}// get3

//========================================================
{//range
function range(i,j,k, cycle, n, returnitems=false)=
(  
  //echo(ijk=[i,j,k])
  
  let ( 
	 obj = isarr(i)||isstr(i)? i :undef
	, k= isnum(n)? i+n*j: k
	 
	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
	,beg= inum==1 
            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
        ,end= inum==1 
		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
		   : inum==2?j:k
		   
	,cycle=cycle==true?1:cycle

	,intv = sign(end-beg)        // when inum=3, the middle one is
			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	

	,residual= or( mod(abs(end-beg), abs(intv)), intv)

	,extraidx= abs(intv)>abs(beg-end) // End points to be skipped, 
	           ? 1                    // depending on the intv. If
	           : residual             // intv too large, set to 1
	,objrange= obj
			   ? range ( len(obj)
						, cycle=cycle ) 
			   : []
	, _rtn=										 

        ( intv==0 || beg==end || (beg==0&&end==undef) 
        || obj==[] || obj=="" )
        ? []
        : obj  // Allowing range(arr) or range(str)
          ? objrange

          // Non obj : =================

          : cycle>0
          ? concat( 
                [ for(i=[beg:intv:end-extraidx]) i ]
                ,[ for(i=[beg:intv:beg+(cycle-1)*intv]) i]
                )
          : cycle<0
            ?  concat( 
                [ for(i=[end+cycle*intv:intv:end-intv]) i]
                ,[ for(i=[beg:intv:end-extraidx]) i ]
                )
          : isarr(cycle)
            ? concat( 
                [ for(i=[end+cycle[0]*intv:intv:end-intv]) i]
                ,[ for(i=[beg:intv:end-extraidx]) i ]
                ,[ for(i=[beg:intv:beg+(cycle[1]-1)*intv]) i]
                )
          : [ for(i=[beg:intv:end-extraidx]) i	]
          
          /// 2018.10.15: 
          /// When n is given and j (=intv) is < 1, it could cause
          /// the loop to stop prematurally due to machine error. 
          /// For example, range( 0.2, 0.05, n=30) should give 30
          /// numbers but only 29. It stops at 1.6 when the last 
          /// item should be 1.065. We make a correction below 
        , rtn= isnum(n)?
               ( n>len(_rtn)? concat( _rtn, [_rtn[n-2]+intv] )
               : _rtn
               ): _rtn  

   )
   //i==1 && j==2?
   //echo( n=n, ijk=[i,j,k], inum=inum, residual=residual, extraidx= extraidx )
   //echo(ijk=[i,j,k], cycle=cycle, n=n)
   //echo(_rtn = _rtn)
   //echo(intv=intv, beg=beg, end=end, extraidx=extraidx, end-extraidx)
   //(returnitems? get(obj,rtn): rtn)
   //:
   (returnitems? get(obj,rtn): rtn)
);

    range=["range", "i,j,k,cycle,returnitems", "array", "index",
     " Given i, optional j,k, return a range or ranges of ordered numbers 
    ;; without the end point. 
    ;;
    ;; (1) basic
    ;;
    ;;   range(3)    => [0,1,2] // from 0 to 3, inc by 1 (= for(n=[0:i-1] )
    ;;   range(3,6)  => [3,4,5] // inc by 1  (= for(n=[i:j-1] )
    ;;   range(3,2,6)=> [3,5]   // inc by 2  (= for(n=[i:j:k-1] )
    ;;   range(6,3)  => [6,5,4]
    ;;   range(6,2,3)=> [6,4]
    ;;
    ;; (2) i= array or string
    ;;
    ;;   for(x=arr)        ... the items
    ;;   for(i=range(arr)) ... the indices 
    ;;
    ;;   range([3,4,5]) => [0,1,2]
    ;;   range( ''345'' )= [0, 1, 2]
    ;;
    ;; (3) cycle= i,-i, true: cycling (New 2014.9.7)
    ;;
    ;;   range( ... cycle= i|-i}true...)  
    ;; 
    ;; cycle= -1/+i: extend the index on the left/right side by 1 count
    ;; cycle= true: same as cycle=1 
    ;; cycle=[-1,1]
    ;;
    ;;   range( 4,cycle=1 )   = [0, 1, 2, 3, 0] // Add first to the right
    ;;   range( 4,cycle=-1 )  = [3, 0, 1, 2, 3] // Add last to the left
    ;;   range( 2,5,cycle=2 ) = [2, 3, 4, 2, 3] // Add 1st,2nd to the right
    ;;   range( 2,5,cycle=-1 )= [4, 2, 3, 4]    // Add last to the left
    ;;   range( 2,1.5,8,cycle=2 )= [2, 3.5, 5, 6.5, 2, 3.5]
    ;;
    ;; range(obj,cycle=i,-1,[-1,1],true...)
    ;;
    ;;   range( [11, 12, 13],cycle=2 )= [0, 1, 2, 0,1]
    ;;
    ;; (4) n to specify total items needed (New 2018.10)
    ;;
    ;;   range(1,3,n=5)  ==> return 5 items
    "];

    function range_test( mode=MODE, opt=[] )=
    (
        let( obj = [10,11,12,13] )
        //echo("in range_test()")
        doctest( range,
        [
        
          "// 1 arg: starting from 0"
        , [ range(5), [0,1,2,3,4], "5"]
        , [ range(-5), [0, -1, -2, -3, -4] , "-5"]
        , [ range(0), [] , "0"]
        , [ range([]), [] , "[]"]

        , ""
        , "// 2 args: "
        , [ range(2,5), [2,3,4], "2,5"]
        , [ range(5,2), [5,4,3], "5,2"]
        , [ range(-2,1), [-2,-1,0], "-2,1"]
        , [ range(-5,-1), [-5,,-4,-3,-2], "-5,-1"]

        ,""
        , "// Note these are the same:"
        , [ range(3), [0,1,2], "3"]
        , [ range(0,3), [0,1,2], "0,3"]
        , [ range(-3), [0,-1,-2], "-3"]
        , [ range(0,-3), [0,-1,-2], "0,-3"]
        , [ range(1),[0] , "1"]
        , [ range(-1),[0] , "-1"]
            
        ,""
        ,"// 3 args, the middle one is interval. Its sign has no effect"
        , [ range(2,0.5, 5), [2,2.5,3,3.5, 4, 4.5], "2,0.5,5"]
        , [ range(2, -0.5, 5), [2,2.5,3,3.5, 4, 4.5], "2,-0.5,5"]
        , [ range(5,-1,0), [5,4,3,2,1], "5,-1,0"]
        , [ range(5,1,2), [5,4,3], "5,1,2"]
        , [ range(8,2,0),[8,6,4,2], "8,2,0"]
        , [ range(0,2,8),[0, 2, 4, 6], "0,2,8"]			
        , [ range(0,3,8),[0, 3, 6], "0,3,8"]
            
        , ""
        , "// Extreme cases: "
            
        , [ range(), [], "", ["//", "Give nothing, get nothing"]]
        , [ range(0),[], "0", ["//", "Count from 0 to 0 gets you nothing"] ]
        , [ range(2,2),[], "2,2", ["//", "2 to 2 gets you nothing, either"] ]
        , [ range(2,0,4),[], "2,0,4", ["//", "No intv gets you nothing, too"] ]
        , [ range(0,1),[0] , "0,1"]
        , [ range(0,1,1),[0] , "0,1,1"]

        , "// When interval > range, count only the first: "
        , [ range(2,5,4),[2] , "2,5,4"]
        , [ range(2,5,-1),[2] , "2,5,-1"]
        , [ range(-2,5,-4),[-2] , "-2,5,-4"]
        , [ range(-2,5,1),[-2] , "-2,5,1"]

        , ""
        , " range( <b>obj</b> )"
        , ""
        , "// range( arr ) or range( str ) to return a range of an array"
        , "// so you don't have to do : range( len(arr) )"
        , ""
        , str("> obj = ",obj)
        , ""
        , [ range(obj),[0,1,2,3], "obj"]
        , [ range([3,4,5]), [0,1,2] , "[3,4,5]"]
        , [ range("345"), [0,1,2] , "'345'"]
        , [ range([]), [] , "[]"]
        , [ range(""), [] , "'"]

        , ""
        , " range( ... <b>cycle= i,-i,true</b>...)"
        , ""
        , "// New 2014.9.7: "
        , "// cycle= +i: extend the index on the right by +i count "
        , "// cycle= -i: extend the index on the right by -i count"
        , "// cycle= true: same as cycle=1 "
        , "" 
        , [ range(4,cycle=1), [0,1,2,3,0], "4,cycle=1", ["//","Add first to the right"] ]
        , [ range(4,cycle=-1), [3,0,1,2,3], "4,cycle=-1", ["//","Add last to the left"] ]

        , [ range(4,cycle=true), [0,1,2,3,0], "4,cycle=true", ["//","true=1"] ]
        
        
        ,""

        , [ range(2,5,cycle=1), [2,3,4,2], "2,5,cycle=1", ["//","Add first to the right"] ]
        , [ range(2,5,cycle=2), [2,3,4,2,3], "2,5,cycle=2", ["//","Add 1st,2nd to the right"] ]
        , [ range(2,5,cycle=-1), [4,2,3,4], "2,5,cycle=-1", ["//","Add last to the left"] ]
        , [ range(2,5,cycle=-2), [3,4,2,3,4], "2,5,cycle=-2"]
        , [ range(2,5,cycle=true), [2,3,4,2], "2,5,cycle=true", ["//","true=1"] ]
            
        , ""
        , [ range(2,1.5,8,cycle=2), [2, 3.5, 5, 6.5, 2, 3.5] , "2,1.5,8,cycle=2"]
        , [ range(2,1.5,8,cycle=-2), [5, 6.5, 2, 3.5, 5, 6.5] , "2,1.5,8,cycle=-2"]
        , [ range(2,1.5,8,cycle=true), [2, 3.5, 5, 6.5, 2] , "2,1.5,8,cycle=true"]
            
        , ""
        , " range(<b>obj</b>,<b>cycle=i,-1,[-1,1],true</b>...)"
        , ""
        , str("> obj = ",obj)
        ,[ range(obj,cycle=1)	,[0,1,2,3,0], "obj,cycle=1"] 
        ,[ range(obj,cycle=[-1,1])	,[3, 0,1,2,3,0], "obj,cycle=[-1,1]"] 
        ,[ range(obj,cycle=2)	,[0,1,2,3,0,1], "obj,cycle=2"] 
        ,[ range(obj,cycle=-1)	,[3,0,1,2,3], "obj,cycle=-1"] 
        ,[ range(obj,cycle=-2)	,[2,3,0,1,2,3], "obj,cycle=-2"] 

        , ""
        , " range( obj, <b>returnitems=true,false</b> )"
        , ""
        , "// When using obj, can set returnitems=true to return items."
        , ""
        , [ range([3,4,5],returnitems=true), [3,4,5] , "[3,4,5],returnitems=true"]
        , [ range(obj,cycle=[-1,1],returnitems=true)	
            ,[13,10,11,12,13,10], "obj,cycle=[-1,1],returnitems=true"
          ]
        , ""
        , "// When j is a float, setting the count might be easier than calc the end value k:"
        , " <b>range(5,1.5, n=10)</b> // ==> return 10 items (new 2018.10.25)"
        , ""
        , [ range(1,2,n=5), [1,3,5,7,9], "1,2,n=5"]  
        , [ range(1,.25,n=5), [1,1.25,1.5,1.75,2], "1,0.25,n=5"]  
        , [ range(2,-.25,n=5), [2,1.75,1.5,1.25,1], "1,-0.25,n=5"]
        , [ range( 0.2, 0.05, n=30)
            ,  [ 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65
               , 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1,   1.05, 1.1, 1.15
               , 1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55, 1.6, 1.65]
            , "0.2, 0.05, n=30"
            , ["asstr",1]
          ]
        , [ range( 0, 0.05, n=30)
            ,  [ 0,   0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45
               , 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95
               , 1,   1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45
               ]
            , "0, 0.05, n=30"
            , ["asstr",1]
          ]
          
        ]
        , mode=mode, opt=opt
        )
    );

    //echo( range_test( mode =12 )[1] );
}//range


function index(x,s)= idx(x,s); 
  // In the future we will replace all "index()" in this file with "idx()"

//========================================================
{// idx
  // Note 18.4.26:
  //   Returning -1 when not found causes a lot of trouble 
  //   'cos -1 is a valid index in many scadx range functions. 
  //   ===> changed it to undef
  //   ===> Would need lots of fixes in many funcs. --- for example, int() 18.10.4 
  //
  // NOTE 16.12.16: 
  // Introduce search()-based indexing. That is, use search() as
  // the core function in an attempt to speed it up. No profiling is
  // done yet but it is inspired by hash_profiling.scad
  // 
  //   idx( array, x) and idx( string, "x") are able to use search(),
  // but
  //   idx( string, "xyz") failed and has to use old approach
  //
function idx( o,x )=
(    
  !(len(o)>0)?undef
  :str(o)!=o ?  // array
   ifthen(search( [x], o)[0],[], undef)
   
  : // string
    //echo("---------",_s("idx({_},{_})",[o,x]))
    len(x)>1  // string and x is word
    ? und( [ for (i=[0:len(o)-1])  // avoid range() 'cos it's slow 2016.12.10
            if( (o[i]==x[0]) &&          // Match the first char 
                (![ for(xi=[1:len(x)-1]) // Match the following 
                     if( o[i+xi]!=x[xi] )1  // len(x)-1 char in x
                  ]) 
              ) i // Return i only when both match        
    	   ][0]
        , undef
        )      
   : und(search( x, o)[0],undef) // searching only one char in a string

);           
 
    idx=["idx", "o,x", "int", "Index,String",
    "Return the index of x in o (str|arr), or undef if not found.
    ;; 
    ;; Compare to index:
    ;; 
    ;;  Unlike index, idx does`t use slice() and join() for each 
    ;;  possible char check in s. Should be faster than index when 
    ;;  x is a word but not a single char.
    "
    ,"index"
    ];

    function idx_test( mode=MODE, opt=[] )=
    (
        let( s= "I_am_the_findw"
           , arr=["xy","xz","yz"]
           ) 

        doctest(idx,  
        [ 
          "var s","var arr",
       // , ""    
       // , 
        "// Array indexing "
        ,""
        , str("arr = ", arr)
        , [ idx(["xy","xz","yz"],"xz"),  1, "arr, 'xz'" ]
        , [ idx(["xy","xz","yz"],"yz"),  2, "arr, 'yz'" ]
        , [ idx(["xy","xz","yz"],"xx"),  undef, "arr, 'xx'" ]
        , [ idx([[1,2],[4,5],[3,2]],[3,2]),2,"[[1,2],[4,5],[3,2]],[3,2]"]

        , [ idx([],"xx"), undef, "[], 'xx'" ]
        ,""
        , "// String indexing"
        ,""
        , str("s= ", s)
        , [ idx( "I_am_the_findw","a"),  2, "s, 'a'" ]
        , [ idx(  "abcdef","f"),  5, "'abcdef','f'" ]
        , [ idx( "findw","n"),  2, "'findw','n'" ]
        , [ idx( "I_am_the_findw","am"),  2, "s, 'am'",  ]
        , [ idx( "I_am_the_findw","_the"),  4, "s, '_the'",  ]
        , [ idx( "I_am_the_findw","find"),  9, "s, 'find'" ]
        , [ idx( "I_am_the_findw","the"),  5,"s, 'the'",  ]
        , [ idx("I_am_the_ffindw","findw"),  10,"s, 'findw'" ]
        , [ idx("findw", "findw"),  0, "'findw', 'findw'" ]
        , [ idx( "findw","dwi"),  undef, "'findw', 'dwi'" ]
        , [ idx("I_am_the_findw","The"),  undef,"s, 'The'" ]
        , [ idx( "replace__text","tex"),  9, "s, 'tex'" ]
        , [ idx("replace__ttext","tex"), 10, "'replace__ttext', 'tex'" ]
        , [ idx( "replace_t_text","text"),  10, "'replace_t_text','tex'"]
        , [ idx("abc", "abcdef"),  undef, "'abc', 'abcdef'" ]
        , [ idx( "","abcdef"),  undef,"'', 'abcdef'" ]
        , [ idx( "abc",""),  undef, "'abc',''" ]
        ,""
        ,"// Others"
        ,""
        , [ idx([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
        
        ]
        ,mode=mode, opt=opt, scope=[ "s",s, "arr",arr]
             
        )
    );

    //echo(idx_test()[1]);
}// idx


//========================================================

/*
    function index(o,x)=  
    (
      let(rtn= x==""? undef
               : isarr(o)? [ for(i=range(o)) if(isequal(o[i],x))i ][0]
               : isstr(o)? [ for(i=range(o)) if( slice(o,i,len(x)+i)==x) i ][0]
               : undef )
       rtn==undef?-1:rtn
    );            

    index=["index", "o,x", "int", "Index,Array,String",
    " Return the index of x in o (a str|arr), or -1 if not found.
    ;;
    ;;   arr= ['xy', 'xz', 'yz']; 
    ;;   index( arr, 'yz' )= 2
    ;;   index( arr, 'xx' )= -1
    ;;
    ;;   index( 'I_am_the_findw', 'am' )= 2
    ","idx"
    ];	 

    function index_test( mode=MODE, opt=[] )=
    (
        //let( arr = ["xy","xz","yz"]
        //   , s= "I_am_the_findw"

        doctest(index,  
        [ 
          "## Array indexing ##"
        , [ index(["xy","xz","yz"],"xz"),  1, "arr, 'xz'" ]
        , [ index(["xy","xz","yz"],"yz"),  2, "arr, 'yz'" ]
        , [ index(["xy","xz","yz"],"xx"),  -1, "arr, 'xx'" ]
        , [ index([],"xx"), -1, "[], 'xx'" ]
        , "## String indexing ##"
        , [ index( "I_am_the_findw","a"),  2, "s, 'a'" ]
        , [ index( "I_am_the_findw","am"),  2, "s, 'am'",  ]
        , [ index(  "abcdef","f"),  5, "'abcdef','f'" ]
        , [ index( "findw","n"),  2, "'findw','n'" ]
        , [ index( "I_am_the_findw","find"),  9, "s, 'find'" ]
        , [ index( "I_am_the_findw","the"),  5,"s, 'the'",  ]
        , [ index("I_am_the_ffindw","findw"),  10,"s, 'findw'" ]
        , [ index("findw", "findw"),  0, "'findw', 'findw'" ]
        , [ index( "findw","dwi"),  -1, "'findw', 'dwi'" ]
        , [ index("I_am_the_findw","The"),  -1,"s, 'The'" ]
        , [ index( "replace__text","tex"),  9, "s, 'tex'" ]
        , [ index("replace__ttext","tex"), 10, "'replace__ttext', 'tex'" ]
        , [ index( "replace_t_text","text"),  10, "'replace_t_text','tex'"]
        , [ index("abc", "abcdef"),  -1, "'abc', 'abcdef'" ]
        , [ index( "","abcdef"),  -1,"'', 'abcdef'" ]
        , [ index( "abc",""),  -1, "'abc',''" ]

        ,"## Others ##"
        , [ index([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
        , [ index([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
        
        ],mode=mode, opt=opt//, scope=["arr",arr, "s",s]
             
        )
    );
 */          
           
////========================================================

//========================================================
{ // inrange
function inrange(o,i)=
(
	/*
	len: 1 2 3 4 5
		 0 1 2 3 4   i>=0
   		-5-4-3-2-1   i <0    
	*/
	
	isint(i)? ( (-len(o)<=i) && ( i<len(o)) )
            : false

);

    inrange=[ "inrange", "o,i", "T|F", "Index, Array, String, Inspect",
    " Check if index i is in range of o (a str|arr). i could be negative.
    ;;
    ;; ## string ##
    ;;   inrange( '789',2 )= true
    ;;   inrange( '789',-2 )= true
    ;;   inrange( '789',-4 )= false
    ;; ## array ##
    ;;   inrange( [7,8,9],2 )= true
    ;;   inrange( [7,8,9],-2 )= true
    ;;   inrange( [7,8,9],-4 )= false
    "];

    function inrange_test( mode=MODE, opt=[] )=
    (
        doctest( inrange,
        [
          "## string ##"
        , [ inrange("789",2), true , "'789',2"]
        , [ inrange("789",3), false , "'789',3"]
        , [ inrange("789",-2), true , "'789',-2"]
        , [ inrange("789",-3), true , "'789',-3"]
        , [ inrange("789",-4), false , "'789',-4"]
        , [ inrange("",0), false , "',0"]
        , [ inrange("789","a"), false , "'789','a'"]
        , [ inrange("789",true), false , "'789',true"]
        , [ inrange("789",[3]), false , "'789',[3]"]
        , [ inrange("789",undef), false , "'789',undef"]
        , "## array ##"
        , [ inrange([7,8,9],2), true , "[7,8,9],2"]
        , [ inrange([7,8,9],3), false , "[7,8,9],3"]
        , [ inrange([7,8,9],-2), true , "[7,8,9],-2"]
        , [ inrange([7,8,9],-3), true , "[7,8,9],-3"]
        , [ inrange([7,8,9],-4), false , "[7,8,9],-4"]
        , [ inrange([],0), false , "[],0"]
        , [ inrange([7,8,9],"a"), false , "[7,8,9],'a'"]
        , [ inrange([7,8,9],true), false , "[7,8,9],true"]
        , [ inrange([7,8,9],[3]), false , "[7,8,9],[3]"]
        , [ inrange([7,8,9],undef), false , "[7,8,9],undef"]
        ]
        , mode=mode, opt=opt
        )
    );

    //echo( inrange_test( mode=112)[1] );
}// inrange

//========================================================
{ // ranges
function ranges(i,j,k, cover=1, cycle=true, returnitems=false, _debug=[])=
let ( 
     cover= cover>0?[0,cover]:cover<0?[-cover,0]:cover // allow +n, -m, or [m,n]
	,obj = isarr(i)||isstr(i)? i :undef
	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
	,beg= inum==1 
            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
    ,end= inum==1 
		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
		   : inum==2?j:k
		   
	,intv = sign(end-beg)        // when inum=3, the middle one is
			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	

    ,sign = sign(end-beg)
    ,baserange= obj?range(obj):range(beg,intv,end) 
    ,left = cover[0]
    ,right= len(baserange)>1?cover[1]:0    
    
    ,newrange = concat( 
                 cycle?range( end-cover[0]*intv, sign*intv, end ):[]
                ,baserange
                ,cycle?range( beg, sign*intv, beg+intv*cover[1] ):[]
                )
//	,_debug= chkbugs( _debug,
//             ["cover",cover
//             ,"left",left
//             ,"right",right
//             , "baserange", baserange
//             , "newrange", newrange
//             ,"beg",beg
//             ,"end",end
//             ,"intv",intv
//             ]
//             )
    ,rtn=        
	( intv==0 || beg==end || (beg==0&&end==undef) || obj==[] || obj=="" )
	? []
	: obj  // Allowing range(arr) or range(str)
	  ? ranges ( len(obj)
				, cover=cover
				, cycle=cycle
				)

	  // Non obj : =================

	  : cover
        //? _debug
	  ?//[ _debug,   // uncomment to debug
           [ for( i=range(left,1, left+len(baserange)) )
               //range( i-left, i+right+1) //
               let( islast = i==left+len(baserange)-1
                  , cycledleft = (i==left&&!cycle?0:left) // When cycle, adj the
                  , cycledright= (islast&&!cycle?0:right) // 1st and last 
                  )
               [for( ii=range( i - cycledleft
                             , i + cycledright + 1 
                             ) ) newrange[ii] 
               ] 
           ]
         // ]      // uncomment to debug

	   // no cover  ---------------
       : cycle
		  ? concat( [for(i=[beg:intv:end])[i] ], [[beg]] )
		  :[ for(i=[beg:intv:end]) [i]	]
)(          
   returnitems? [ for(is=rtn) get(obj,is) ]: rtn        
);
          
     ranges=["ranges", "i,j,k,cover=1,cycle=true,returnitems=false", "array", "index",
     " Similar to range(), but instead of returning an arr of indices,
    ;; this func returns an ''array of *array of indices*'':
    ;;
    ;;   range(3)   = [0,1,2]
    ;;   ranges(3)  = [[0,1], [1,2], [2,0]]  // cover= 1 or [0,1]
    ;;   ranges(3..)= [[2,0], [0,1], [1,2]]  // cover= -1 or [1,0]
    ;;   ranges(3..)= [[0,1,2], [1,2,0], [2,0,1]] //cover= 2 or [0,2]
    ;;
    ;; This is achieved by setting the *cover* : 
    ;;
    ;;   cover=+n: cover n more indices on the right side 
    ;;   cover=-n: cover n more indices on the left side
    ;;   cover=[i,j]: set both sides together
    ;;
    ;;   obj = [10, 11, 12, 13]
    ;;
    ;;   ranges( obj )= [[0, 1], [1, 2], [2, 3], [3, 0]]
    ;;   ranges( 0,2,6,cover=[0,2] )= [[0, 2, 4], [2, 4, 0], [4, 0, 2]]
    ;;
    ;; Usage of the rest of arguments, i,j,k cycle, are the same as in range()
    "];

    function ranges_test( mode=MODE, opt=[] )=
    (
        let( obj = [10,11,12,13] )
        
        doctest( ranges
        ,[
          str( "obj = ", _fmt(obj))
        , ""
        , "// 1 arg: examples show starting from 0 to 2, covering 2 indices each"
        , [ ranges(3), [[0, 1], [1, 2], [2, 0]] , "3"]
        , [ ranges(-3), [[0,-1], [-1,-2], [-2,0]] , "-3"]
        , ""
        , "// 2 args: "
        , [ ranges(2,5), [[2,3], [3,4], [4,2]] , "2,5"]
        , [ ranges(5,2), [[5,4], [4,3], [3,5]] , "5,2"]
        , [ ranges(-2,1), [[-2,-1], [-1,0], [0,-2]], "-2,1"]
        , [ ranges(-5,-1), [[-5,-4], [-4,-3], [-3,-2], [-2,-5]], "-5,-1"]

        , ""
        , "// Note these are the same:"
        , [ ranges(3), [[0, 1], [1, 2], [2, 0]] , "3"]
        , [ ranges(0,3), [[0, 1], [1, 2], [2, 0]], "0,3"]
        , [ ranges(-3), [[0, -1], [-1, -2], [-2, 0]], "-3"]
        , [ ranges(0,-3), [[0, -1], [-1, -2], [-2, 0]], "0,-3"]
        , [ ranges(1),[[0]] , "1"]
        , [ ranges(-1),[[0]] , "-1"]
        
        , ""
        , "// 3 args, the middle one is interval. Its sign has no effect"
        , [ranges(2,0.5,4) ,  [[2, 2.5], [2.5, 3], [3, 3.5], [3.5, 2]], "2,0.5,4"]
        , [ranges(2,-0.5,4) ,  [[2, 2.5], [2.5, 3], [3, 3.5], [3.5, 2]], "2,-0.5,4"]
        , [ranges(2,0.5,4,cover=2) ,  [[2,2.5,3], [2.5,3,3.5], [3,3.5,2], [3.5,2,2.5]],"2,0.5,4, cover=2"]
        , [ranges(2,0.5,4,cover=-1) 
            ,  [[3.5,2], [2,2.5], [2.5, 3], [3, 3.5]], "2,0.5,4, cover=-1"]
        , [ranges(2,0.5,4,cover=-2) ,  [[3, 3.5,2], [3.5, 2,2.5], [2, 2.5, 3], [2.5, 3, 3.5]], "2,0.5,4, cover=-2"]

        , ""
        , [ ranges(4,1,1),[[4, 3], [3, 2], [2, 4]], "4,1,1"]
        , [ ranges(8,2,0),[[8, 6], [6, 4], [4, 2], [2, 8]] , "8,2,0"]
        , [ ranges(0,3,8),[[0, 3], [3, 6], [6, 0]] , "0,3,8"]
        
        , ""
        , [ ranges(4, cover=2), [[0,1,2],[1,2,3],[2,3,0],[3,0,1]], "4,cover=2" ]  
        
        , ""
        , "// Extreme cases: "		
        , [ ranges(), [], "", ["//", "Give nothing, get nothing"]]
        , [ ranges(0),[], "0", ["//", "Count from 0 to 0 gets you nothing"] ]
        , [ ranges(2,2),[], "2,2", ["//", "2 to 2 gets you nothing, either"] ]
        , [ ranges(2,0,4),[], "2,0,4", ["//", "No intv gets you nothing, too"] ]
        , [ ranges(0,1),[[0]] , "0,1"]
        , [ ranges(0,1,1),[[0]] , "0,1,1"]

        , "// When interval > range, count only the first: "
        , [ ranges(2,5,4),[[2]] , "2,5,4"]
        , [ ranges(2,5,-1),[[2]] , "2,5,-1"]
        , [ ranges(-2,5,-4),[[-2]] , "-2,5,-4"]
        , [ ranges(-2,5,1),[[-2]] , "-2,5,1"]

        , ""
        , " ranges( <b>obj</b> )"
        , ""
        , "// ranges( arr ) or ranges( str ) to return a range of an array"
        , "// so you don't have to do : ranges( len(arr) )"
        , ""
        , str("> obj = ",obj)
        , ""
        , [ ranges(obj),[[0, 1], [1, 2], [2, 3], [3, 0]], "obj"]
        , [ ranges([3,4,5]), [[0, 1], [1, 2], [2, 0]], "[3,4,5]"]
        , [ ranges("345"), [[0, 1], [1, 2], [2, 0]]  , "'345'"]
        , [ ranges([]), [] , "[]"]
        , [ ranges(""), [] , "'"]

        , ""
        , " ranges( ... <b>cycle= i,-i,true</b>...)"
        , ""
        , "// cycle= true: extend the index on the right by +i count "
        , "" 
        , [ ranges(4,cycle=false), [[0, 1], [1, 2], [2, 3], [3]], "4,cycle=false"
                    , ["//","Add first to the right"] ]
        , [ ranges(2,5,cycle=false), [[2, 3], [3, 4], [4]] , "2,5,cycle=false"]
        , [ ranges(2,1.5,8,cycle=false),
                    [[2, 3.5], [3.5, 5], [5, 6.5], [6.5]], "2,1.5,8,cycle=false" ]
        
        , ""
        , " ranges(<b>obj</b>,<b>cycle=true</b>...)"
        , ""
        , str("> obj = ",obj)
        , [ ranges(obj,cycle=true),[[0, 1], [1, 2], [2, 3], [3, 0]], "obj,cycle=true"]
        
        , ""
        , " ranges(... <b>cover</b>=[i,j], n, -n )"
        , ""
        , "// cover=[1,2] means, instead of returning i, return"
        , "//[i-1,i,i+1,i+2]. Note that when cover is set, return array of arrays."
        , "// Also note that w/cover, cycle=+i/-i is treated as cycle=true."

        , [ ranges(3), [[0, 1], [1, 2], [2, 0]] , "3"]
        , [ ranges(3,cover=[0,1],cycle=false)
            , [[0, 1], [1, 2], [2]], "3,cover=[0,1]"]
        , [ ranges(3,cover=[0,1],cycle=true)
            , [[0, 1], [1, 2], [2,0]], "3,cover=[0,1],cycle=true"]
            
            
        , ""
        , [ ranges(3,cover=[0,0]), [[0], [1], [2]], "3,cover=[0,0]"]
        , [ ranges(5,8), [[5, 6], [6, 7], [7, 5]], "5,8"]
        , [ ranges(5,8,cover=[0,0]), [[5], [6], [7]], "5,8,cover=[0,0]"]
    //		,["5,8,cover=[1,0]", ranges(5,8,cover=[1,0])
    //						, [[5], [5, 6], [6, 7]]]
    //		,["5,8,cover=[0,1]", ranges(5,8,cover=[0,1])
    //						, [[5, 6], [6, 7], [7]]]
    //		,["5,8,cover=[1,0],cycle=true", ranges(5,8,cover=[1,0],cycle=true)
    //						, [[7,5], [5, 6], [6, 7]]]
    //		,["5,8,cover=[0,2],cycle=true", ranges(5,8,cover=[0,2],cycle=true)
    //						, [[5, 6,7], [6, 7, 5], [7,5,6]]]

        , ""
        , [ ranges(0,2,8,cover=[0,1])
                            ,[[0, 2], [2, 4], [4, 6], [6, 0]]
                            , "0,2,8,cover=[0,1]"]
                            
        , [ ranges(0,3,8,cover=[0,1])
                            ,[[0, 3], [3, 6], [6, 0]]
                            , "0,3,8,cover=[0,1]"
                            ]
        , [ ranges(0,2,6,cover=[0,2])
                ,[[0, 2, 4], [2, 4, 0], [4, 0, 2]]
                , "0,2,6,cover=[0,2]", ]  

            ,""
        , [ ranges(obj),[[0, 1], [1, 2], [2, 3], [3, 0]], "obj"]
        , [ ranges(obj,cover=[1,1])
                ,[[3, 0, 1], [0, 1, 2], [1, 2, 3], [2, 3, 0]]
                , "obj,cover=[1,1]" ] 

        , ""
        , " range( obj, <b>returnitems=true,false</b> )"
        , ""
        , "// When using obj, can set returnitems=true to return items."
        , "// Usage: Plane(w/ thickness)"
        , ""
        , [ ranges(obj,cover=[1,1],returnitems=true)
                ,[[13, 10, 11], [10, 11, 12], [11, 12, 13], [12, 13, 10]]
                , "obj,cover=[1,1],returnitems=true" ] 
        
        
        ]//doctest

        , mode=mode, opt=opt, scope=["obj",obj]
        )
    );

    //echo( ranges_test( mode =12 )[1] );  
}// ranges

// ?50> ranges(obj,cover=[1,1],returnitems=true) want: [[13, 10, 11], [10, 11, 12], [11, 12, 13], [12, 13, 10]] got: [13, 10, 11, 10, 11, 12, 11, 12, 13, 12, 13, 10]


/////////////////////////////////////////////////////////
//
//         TESTING
//
/////////////////////////////////////////////////////////

function get_scadx_range_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_range.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_range_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_range", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      fidx_test( mode, opt )
    , get_test( mode, opt )
    //, get2_test( mode, opt ) // to-be-coded
    , get3_test( mode, opt )
    , get3m_test( mode, opt )
    , idx_test( mode, opt )
    //, index_test( mode, opt )
    //, index_test( mode, opt )
    , inrange_test( mode, opt )
    , range_test( mode, opt )
    , ranges_test( mode, opt )
    ]
);
