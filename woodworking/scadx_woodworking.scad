include <woodworking_params.scad>


//. Constants 
  
// Actual lengths in inches
//LEN1= 0.75;
//LEN2= 1.5;
//LEN4= 3.75;
//LEN3= LEN2+(LEN4-LEN2)/2; 
//LEN6= 5.4215;
//LEN8= 7.25;
//  
//FT2= 24;  
//FT4= 48;  
//FT6= 72;
//FT8= 96;
//FT10= 120;
//
//PLY1= 0.225;
//PLY2= 0.5;
//
//WOODLENS= [undef,LEN1,LEN2,LEN3,LEN4,undef,LEN6,undef,LEN8];
//
//TABLESAW_KERF= 0.111; // inch, close to 7/64
//
//1x2x8 = [LEN1, LEN2, FT8]; 
//2x2x8 = [LEN2, LEN2, FT8]; 
//2x4x8 = [LEN2, LEN4, FT8]; 
//4x4x8 = [LEN4, LEN4, FT8]; 
//2/2x4x8 = [LEN2/2, LEN4, FT8]; 
//2x4/2x8 = [LEN2, LEN4/2, FT8]; 
//  
// Wood definition. To add customized defs, use:
// WOODS = update( _WOODS, <your_wood>)
// Otherwise, set WOOD = _WOOD;
//WOODS = 
//  [ "2", LEN2
//  , "4", LEN4
//  , "6", LEN6
//  , "8", LEN8
//  , "2/2", LEN1
//  , "4/2", LEN4/2
//  , "6/2", LEN6/2
//  , "8/2", LEN8/2
//  , "B1x2x8",[LEN1, LEN2, FT8]
//  , "B2x2x8",[LEN2, LEN2, FT8]
//  , "B2x4x8",[LEN2, LEN4, FT8]
//  , "B4x4x8",[LEN4, LEN4, FT8]
//  , "B2x4/2x8",[LEN2, LEN4/2, FT8]
//  , "B2/2x4x8",[LEN2/2, LEN4, FT8]
//  , "B2/2x4/2x8",[LEN2/2, LEN4/2, FT8]
//  , "P1x4x8", [1, FT4, FT8]
//
//   Notes: Lowes sell 2 brands of cedar pickets, white-tag and blue-tag
//, "cedPkt_1x6_Lb", [0.603, 5.53, FT6-1,"cedar_picket_1x6_Lowes_blue_tag"] // -1: corner cut of picket
//, "cedPkt_1x6_Lw", [0.56, 5.447, FT6-1,"cedar_picket_1x6_Lowes_white_tag"] 
//, "ced_1x2x8_L", [0.7135, 1.5420, FT8,"cedar_1x2x8_Lowes"]   
//
//];
//
// Width of saw blade in inch
//KERF= 0.111; 
//
//PATIO_COLUMN_WIDTH= 16; // patio column width          
//PATIO_FLOOR_WIDTH = 135.5 + PATIO_COLUMN_WIDTH*2; // in inches 
//PATIO_FLOOR_DEPTH = 120;
//PATIO_FLOOR_HEIGHT = 5;

//. parts definition
/*======  Definition of *parts* for DrawCutlist() and DrawPart() ============

 parts: { partname:part } where a part is a hash:
     
    [ "mat" , <matname> like "B2x4x8". Could also be customized [x,y,z]
    
    , "len",  <len of wanted> like 36. Could be undef. see below. 
                       
    , "size", <size of wanted> like [36,LEN4,LEN2] --- the wanted size is determined
                                                       by either the len or size 
                                                       When len given, use it with the
                                                       other 2 measures (y,z) on mat, like 
                                                       [len, LEN4, LEN]. Otherwise, use
                                                       size = [36, LEN4/2, LEN2], which
                                                       means the mat's width, LEN4, is
                                                       cut in halves.   
                                                       
    , "count", 6   --- Count of copies. Note: this is to determine the output of 
                       cutlist, nothing to do w/ how many parts are actually drawn 
                       (which is determined by a drawing module separately).
                           
    , "color", ["teal",isCutlist?.5:1]
    , "actions", ["roty",-90, "y",-os] 
    
                 --- actions to bring the part from mat-position (lying 
                     on the XY plane) to its init-position. Its final 
                     opsition requires additinal actions defined by
                     individual drawing module.
                                                    
                             actions          actions in its
                              here            drawing mod
                     mat-pos ------> init-pos -------------> final-pos
                                            
    , "decos", [.... ]  --- data used by decoPolyEdges() to modify pts.                                                              
    ]
*/    

//. func/modules

function processParts(parts, isdraw, Frame, Label, drawopts)=
(
  // * Add pts and faces to parts if decos is defined; 
  // * Assure defined size and len 
  //
  // Note: actions is not considered. So the pts added are the
  //       part in its original position on the XY plane
  //       It is designed in such a way that the pts can either
  //       be used for drawing the final product or the cutlist
  //       (by applying different actions) 
  //echo(_red("processParts"), Frame=_b(Frame))
  //echo(log_pp_(str("*** processParts(), drawopts = ", drawopts)))
  let(_drawopts= [ each
                for(pn = keys(parts)) 
                [ pn
                , update( [ "isdraw",isdraw
                          , "Frame", isnum(Frame)?0.05:Frame
                                     // Fix_18c4: 
                                     //  This frame is set to 1 (which is too big) at 
                                     //  some time that can't be traced at this moment.
                                     //  As a temp fix, we forced it to 0.05
                          , "Label", Label
                          ]
                        , h(drawopts,pn,[]) 
                        )
                ]  
              ] )
  [ each 
    //for(pn=share(keys(parts),drawparts)) // Use only parts given in drawparts 
    //for(pn= drawopts?share(keys(parts),keys(drawopts)):keys(parts)) // Use only parts given in drawparts 
    for(pn= keys(parts)) // Use only parts given in drawparts 
    let( p = hash(parts,pn) 
       , decos = hash(p, "decos")
       , partmat= hash(p, "mat") //=> <mat_name> || [x,y,z]
       
       //, matsize= reverse( h( WOODS, partmat, reverse(partmat)) ) //=> [x,y,z]
       , _matsize= h( WOODS, partmat ) //=> [H,W,L] or [H,W,L,NOTE]
       , matsize = _matsize? reverse( slice(_matsize,0,3) )  //=> [L,W,H]
                           : partmat                         // = [x,y,z]
                   
       , partsize = hash(p,"size", [ hash(p,"len"), matsize[1], matsize[2] ] )
       , _pts = cubePts( partsize )
       , pf = decos? decoPolyEdges( pts=_pts, faces=CUBEFACES, decos= decos )
                     : [_pts, CUBEFACES] 
       , rtn=[pn, updates( ["name", pn]
                         , [p, [ "pts", pf[0]
                               , "faces", pf[1]
                               , "matsize", matsize
                               , "size", partsize, "len", partsize[0] 
                               , "isdraw", hashs(_drawopts, [pn,"isdraw"], 1 ) 
                               , "Frame", hashs(_drawopts, [pn,"Frame"], 0.05 )
                               , "Label", hashs(_drawopts, [pn,"Label"], 0 )
                               , "Markpts", hashs(_drawopts, [pn,"Markpts"],0 )
                               , "Dims", hashs(drawopts, [pn,"Dims"], [] )
                               ]
                           ]
             ) 
             ]     
       )
    //echo(_red("processParts(): "), _b(pn), Frame=Frame, p=p)   
    ISLOG?   
    echo(log_pp_b(pn))
    echo(log_pp( ["drawopts", drawopts] ))
    //echo(log_pp( ["_drawopts", _drawopts] ))
    echo(log_pp( ["p",p] ) )
    echo(log_pp( ["_matsize",_matsize,"matsize",matsize,"partsize",partsize] ))
    //echo(log_pp_( ["pts",pf[0]] ))
    echo(log_pp( ["rtn",rtn] ))
    echo(log_pp_e())
    rtn
    :rtn
                 
   ]
  
);  


function cutlist_for_1_mat( /// 1D cutlist for a single material

      parts // Definition of parts. It's a return of processParts()
      
    , mat = "B2x4x8" //=> <matname> || [x,y,z]
                     // Process only the parts that use this material.
                     // It could be a string to indicate one predefined in WOODS
                     // from which the size is obtain, or a customized size [x,y,z] 
                          
    , avail_lens= [] //=> [] || [n,n2...]
                     // lens of the avail_mats (available materials). 
                     // If given, these avail_mats will be used first.
                     // New material of full size (defined by mat) are 
                     // added if avail_mats are not enough. For example, let
                     // 
                     //  mat= "B2x4x8", avail_lens= [50,60] 
                     //
                     // means we have "B2x4x8" in 50 and 60 inches. These
                     // 2 pieces will be used first. 
                     //
                     // if we don't have any avail_mat, leave it [].
                                               
    , spacer = 1 // Define the space between two parts on a mat. This should
                 // take into account of saw kerf and material damage, etc.  
        
    , _matsize    // The size of material used, in [x,y,z] form, like 
                  //  [ FT8, LEN4, LEN2] or [60, LEN4,LEN2]  
                  // This is an internal RT argument set at the init stage 
                  // by the code. Its value is defined in part.matsize.  
    , _rtn  = []
    , debug=0
    , _i=-1 
    )=
(  /*
      Find a better cut fit of parts using ONE specific type of material.  
      Mainly for use in DrawCutlist()

   parts: { partname:part } where a part is a hash:
     
    [ "mat" , <matname> like "B2x4x8". Could also be customized [x,y,z]
    
    , "len",  <len of wanted> like 36. Could be undef. see below. 
                       
    , "size", <size of wanted> like [36,LEN4,LEN2] --- the wanted size is determined
                                                       by either the len or size 
                                                       When len given, use it with the
                                                       other 2 measures (y,z) on mat, like 
                                                       [len, LEN4, LEN]. Otherwise, use
                                                       size = [36, LEN4/2, LEN2], which
                                                       means the mat's width, LEN4, is
                                                       cut in halves.   
                                                       
    , "count", 6   --- Count of copies. Note: this is to determine the output of 
                       cutlist, nothing to do w/ how many parts are actually drawn 
                       (which is determined by a drawing module separately).
                           
    , "color", ["teal",isCutlist?.5:1]
    , "actions", ["roty",-90, "y",-os] 
    
                 --- actions to bring the part from mat-position (lying 
                     on the XY plane) to its init-position. Its final 
                     opsition requires additinal actions defined by
                     individual drawing module.
                                                    
                             actions          actions in its
                              here            drawing mod
                     mat-pos ------> init-pos -------------> final-pos
                                            
    , "decos", [.... ]  --- data used to change the pts.                                                               
    ]
    
   Return:
              mat_len 
        mat      |  avail_len
         |       |   |
    [ ["B2x4x8", 96, 9, ["TopBarSide", 21], ["BotBarShort", 18.75]
                      , ["BotBarShort", 18.75], ["BotBarShort", 18.75], ["CBar", 6]]
    , ["B2x4x8", 96, 9, ["TopBarIn", 21], ["TopBarIn", 21], ["TopBarMid", 21]
                      , ["TopBarSide", 21]]
    , ["B2x4x8", 80, 7.25, ["TopBarLong", 72]]
    , ["B2x4x8", 60, 4.5, ["Inleg_corner", 27], ["Inleg_corner", 27]]
    , ...
    ] 
    
   */
    
   _i==-1? 
     let( 
        _parts= echo( log_("Assigning _parts",5))
                //echo( log_b( "in cutlist_for_1_mat, loop thru pn", 5))
                [ for( pn = keys(parts) ) 
                    let( part = hash(parts, pn))
                    //echo( log_(["pn",pn,"part",_fmth(part)],5))
                    //echo( log_(["pn",pn,"part",part],5))
                        
                    if( (h( part,"mat")==mat)  && h(part,"isdraw") ) 
                    [ pn
                    , hash(part,"len")
                    , hash(part,"count", 1)
                    , hash(part,"matsize")  // why do we have a matsize here? [18c5]
                    ]
                    //=> [ <part_name>, <part_len>, <part_count>, <part_mat_size> ]
                  ]
                
                    
        , sorted_parts = reverse( sortArrs(_parts, by=1) ) // sorted by len
        
        , matsize = last(_parts[0])
                             
        , _rtn = avail_lens?        // _mats => ["B2x4x8",60,60] or ["B2x4x8",96,96]   
                     [ for(len=avail_lens)
                        [ mat, len, len ] 
                     ]
                : [[ mat, matsize[0], matsize[0] ]]                                           
        )
     ISLOG?
       echo( log_cl1_b() )
       echo( log_cl1( str("init, _i=",_i)))
       echo( log_cl1( ["Using a material: mat", mat]) )
       //echo( log_cl1( ["drawopts", drawopts] ) )
       echo( log_cl1( ["keys(parts)", keys(parts)] ) )
       echo( log_cl1( ["_parts", _parts] ) )
       echo( log_cl1( ["sorted_parts", sorted_parts] ) )
       echo( log_cl1( ["matsize",  matsize]) )
       echo( log_cl1( ["avail_lens",  avail_lens]) ) 
       echo( log_cl1( ["_rtn",  _rtn]) )        
       cutlist_for_1_mat(parts=sorted_parts, matname=mat, _matsize=matsize, _rtn=_rtn , spacer=spacer,_i=0, debug=debug) 
     
     : cutlist_for_1_mat(parts=sorted_parts, matname=mat, _matsize=matsize, _rtn=_rtn , spacer=spacer,_i=0, debug=debug) 
        
   : _i>=200  // <================ Set max part count in case things go wrong 
     || parts == []?
     //=> sort by leftover lens then by avail_lens, so the shorter mats are
     //   listed in front.  
     let( rtn = sortArrs( sortArrs(_rtn,by=2), by=1) )
     ISLOG? 
       echo(log_cl1( ["Leaving, _i:", _i] ))
       echo(log_cl1( ["parts", parts] ))
       echo(log_cl1_e())
       rtn
     : rtn
      
   : let( part_nlc= parts[0]       //=> [<part_name>, <part_len>, <part_count> ]
        , plen  = part_nlc[1]
        , pcount = part_nlc[2]
        , mat   = _rtn[0]
        , m_stock_len= _matsize[0] // original len
        , m_avail_len = mat[2]     // leftover of the previous mat
        , neednew= (plen+spacer)> m_avail_len
        , iLastFit = len([ for( mi= range(_rtn))          
                           if( (plen+spacer)<= _rtn[mi][2])1 ])-1                                
                    //=> iLastFit = i of the mat w/ the shortest leftover 
                    //   that fits. This mat will be the one to be filled so
                    //   when neednew is not true, it fills this mat first but
                    //   not the one w/ the longest leftover (i.e., _rtn[0]) 
                     
        , __rtn = neednew? 
                 concat( [[ mat[0]                    //=> matname
                          , m_stock_len               
                          , m_stock_len-plen-spacer   //=> avail_len (leftover) for next run 
                          , [part_nlc[0], part_nlc[1]]  
                         ]]
                       , _rtn ) 
                 : replace(_rtn, iLastFit
                          , concat( replace(_rtn[iLastFit], 2
                                          , _rtn[iLastFit][2]-plen-spacer)
                                  , [ [part_nlc[0], part_nlc[1]] ])  
                          )
        , newrtn = len(__rtn)>1? reverse(sortArrs(__rtn,by=2))
                               : __rtn
        , newparts=  [ if( pcount>1 ) 
                           [ part_nlc[0], part_nlc[1], pcount-1]
                     , if( len(parts)>1 )
                         each [ for(i=[1:len(parts)-1]) parts[i] ] 
                     ] 
        )
     ISLOG?
       echo( log_cl1(_red(str("=============== _i= ", _i) )))   
       echo( log_cl1( ["parts", parts ]))
       echo( log_cl1( ["part_nlc", part_nlc]))
       echo( log_cl1( ["plen", plen]))
       echo( log_cl1( ["mat", mat]))
       echo( log_cl1( ["iLastFit", iLastFit ]))
       echo( log_cl1( ["m_stock_len", m_stock_len]))
       echo( log_cl1( ["m_avail_len", m_avail_len]))
       echo( log_cl1( ["neednew", neednew?_red(neednew):_green(neednew) ]))    
       echo( log_cl1( ["_rtn", _rtn ]))   
       echo( log_cl1( ["__rtn", __rtn ]))   
       echo( log_cl1( ["newrtn", newrtn ]))
       echo( log_cl1( ["len_newrtn", len(newrtn) ]) )
       cutlist_for_1_mat(parts=newparts, matname=mat, _matsize=_matsize, _rtn=newrtn, spacer=spacer,_i=_i+1, debug=debug)    
     : cutlist_for_1_mat(parts=newparts, matname=mat, _matsize=_matsize, _rtn=newrtn, spacer=spacer,_i=_i+1, debug=debug)    
         
);

function get_matsize(mat, len)=
(  // Return mat size. mat is either a mat name (one of keys of WOODS) or [x,y,z]
   // Return [Len, width, height] which is [x,y,z]
   // 
   //  get_matsize("B2x4x8")     => [96, LEN4, LEN2]
   //  get_matsize("B2x4x8", 60) => [60, LEN4, LEN2]
   //  get_matsize([70,LEN4,LEN2])  => [70, LEN4, LEN2]
   //  get_matsize("B2x4x8", 80)    => [70, LEN4, LEN2]
   //
   let( _matsize = hash(WOODS,mat)
      , rtn= _matsize? reverse( slice(_matsize,0,3) ): mat
      )
   //echo("in get_matsize()", _matsize = _matsize)
   //echo("in get_matsize()", rtn = rtn)
   len? [len, rtn[1],rtn[2]]: rtn    
);

///. DrawCutlist module

module DrawCutlist( 
      title  // title appears on the bottom side of cutlist drawing
    , parts 
    , avail_mats=[]
         /* avail_mat_lens: Defined only when you have to use pre-cut wood 
         
            a hash of { <matname|matsize>: <len|lens> }
            avail_mats = [ 
                           "B2x4x8",[60,50]
                         , [96,LEN4,LEN2], 80 
                         ]
            => we have 3 pieces wood available:
                  
               "B2x4x8" 50-in              
               "B2x4x8" 60-in              
               [96,LEN4,LEN2] 80-in              
         */
//    , isdraw
//    , Frame     
//    , drawopts          
    , mat_spacer = 1 
    , part_spacer = 1
    , debug=0
    )
{  
   /* 
    parts: { partname:part } where a part is a hash:
     
    [ "mat" , <matname> like "B2x4x8". Could also be customized [x,y,z]
    
    , "len",  <len of wanted> like 36. Could be undef. see below. 
                       
    , "size", <size of wanted> like [36,LEN4,LEN2] --- the wanted size is determined
                                                       by either the len or size 
                                                       When len given, use it with the
                                                       other 2 measures (y,z) on mat, like 
                                                       [len, LEN4, LEN]. Otherwise, use
                                                       size = [36, LEN4/2, LEN2], which
                                                       means the mat's width, LEN4, is
                                                       cut in halves.   
                                                       
    , "count", 6   --- Count of copies. Note: this is to determine the output of 
                       cutlist, nothing to do w/ how many parts are actually drawn 
                       (which is determined by a drawing module separately).
                           
    , "color", ["teal",isCutlist?.5:1]
    , "actions", ["roty",-90, "y",-os] 
    
                 --- actions to bring the part from mat-position lying 
                     on the XY plane) to its init-position. Its final 
                     opsition requires additinal actions defined by
                     individual drawing module.
                                                    
                             actions          actions in its
                              here            drawing mod
                     mat-pos ------> init-pos -------------> final-pos
                                            
    , "decos", [.... ]  --- data used to change the pts.                                                               
    ]
                    
   */
   echom("DrawCutlist()");
   log_dcl_b();
   log_dcl( ["title", str(title)] );
   //log_dcl( ["parts", parts] );
   //log_dcl( ["keys(parts2)", keys(parts2)] );
   //log_dcl( ["drawopts", drawopts] );
   log_dcl( ["part_mats", part_mats] );
   log_dcl( ["mat_spacer", mat_spacer] );
   
  // parts2 = processParts(parts, isdraw, Frame, drawopts);
   //log_dcl( ["parts", str(parts)] );
   //
   // part_mats => the mats requested by parts
   //
   part_mats = uniq([for(p=vals(parts)) hash(p,"mat")] );
   
   // The following causes an error "Recursion detected calling function 'abs' "
   // log_dcl( ["parts2", parts2] );
   //
   
   //echo( parts2= _fmth(parts2) );
      
   cutlist = [ each
               for( m = part_mats )                      // m=> <matname> || [x,y,z]
               let( avail_lens= h( avail_mats, m,[] ))
               cutlist_for_1_mat( parts= parts 
                                , mat  = m   
                                , avail_lens = //avail_lens==undef?undef
                                              len(avail_lens)>=-1?avail_lens:[avail_lens]
                                //, drawopts=drawopts
                                , spacer = part_spacer
                                , debug=debug
                                )
             ];
             //    
             //    
             //                  | 
             //                  |
             // cutlist=> [ [<mat_name||[x,y,z]>,<mat_len>,<mat_leftover>
             //           , part1, part2 ... ]
             //           where part1, part2= [ <part_name>, <part_len> ]
             
   //cutlist = sortArrs( _cutlist, 1);                                          
   //_red("cutlist= ");
   //echo(cutlist = cutlist);
   log_dcl( ["cutlist", cutlist] );
   
   //echo(arrprn(cutlist));
   //echo("");
   //echo( act_added = add_cutlist_actions(cutlist) );
   if(title)
   { Black()
     Text( str(title, ", part_spacer= ", part_spacer)
         , site= [ for(p=ORIGIN_SITE) p+[0,-5,0]]
         , scale=2
         );
   }
   //NAME=0;
   //LEN=1;
   //COUNT=2;
   
   for( i = range(cutlist) )
   {
     //echo(_red( _s("Loop thru cutlist, i={_}",i) ));

     //Blue()

     // Get the starting y of this mat. Note that mats prior to this
     // may have different y values ( =widths ). So we need to calc 
     // them individually as below.
     mat_y = i==0?0
             :sum( [ for(j=[1:i])
                   let( pre_mat= cutlist[j-1]
                      , pre_y= get_matsize( pre_mat[0] ).y 
                      ) 
                   pre_y+ mat_spacer
                 ]);  
     //echo(mat_y=mat_y);
                
     mat = cutlist[i]; //=> mat = [<matname>,<metlen>,<leftlen>, part, part ...]
                       //   part = [ <partname>, <partlen>, <partcount> ]  
                       //=> ["B2x4x8", 96, 1.5, ["TopBarLong", 72, 2], ["TopBarIn", 21, 1]]
     
     //_matsize = hash(WOODS,mat[0], reverse(mat[0])); //=> [ LEN2, LEN4, 96 ];
     //matsize = [ mat[1], _matsize[1], _matsize[0] ]; //=> [ 60, LEN4, LEN2 ];

     matsize = get_matsize( mat[0], mat[1] ); //=> [ 60, LEN4, LEN2 ]
     
     MarkPts( [ [ -.5
                ,  mat_y+matsize[1]/2 //i*(matsize[1]+mat_spacer)+matsize[1]/2
                , LEN2 ]
              , [  matsize[0]
                ,  mat_y+matsize[1]*.75 //i*( matsize[1]+mat_spacer)+matsize[1]*.75
                , matsize[2] ]
              ]
            , Line=0, Ball=0  
           ,  Label=["text", [ str(" (", mat[0], ":", mat[1], ") ", "#",i+1, " " )
                             , str(mat[2], "in")
                             ] 
                   , "halign","right"
                   , "scale",8
                   , "color", COLORS[ index(part_mats, mat[0]) +1 ]
                   , "followView", false ] 
           );

     //echo(i=i, mat0=mat[0], color_i=index(part_mats, mat[0]), color= COLORS[ index(part_mats, mat[0]) ]);
                
     //echo(i=_red(i));
     //echo(mat = mat);
     //echo(_matsize = _matsize);
     //echo(matsize = matsize);
     _parts = slice(mat, 3);
     /*_parts= [ ["Outleg", 24.75, 1 ]
               , ["Inleg_middle", 24.1875, 2 ]
               , ["Inleg_middle", 24.1875, 1 ]
               ]
     */                   
     //echo( i = i, y = i*(LEN4+mat_spacer), leftover= mat[2] ) ;   
     //echo(_green( _s("cutlist[{_}], _parts={_}",[i, _parts])));
     //if(parts)
     //log_b("In DrawCutlist, Looping _parts", layer=5 );
     for( j = range(_parts) )
     {
        //echo(_blue( _s("   parts[{_}]: part= {_}, prelen={_}",[j,part, prelen] )));
        log_b( ["In DrawCutlist, Looping _parts, partname", partname], 6 );
        log_(["part", part],6 );
        log_(["part_pts", part_pts],6 );
        log_(["_ppts", _ppts],6 );
        part = _parts[j]; //=> part = [ <partname>, <partlen>, <partcount> ] 
        partname = part[0]; //hash(part, "partname" );
        plen     = part[1];
        pcount   = part[2];
        //echo(j=j, partname=partname, part=part, matsize= hashs(parts,[partname,"mat"]));
        //echo(j=j, part=part);
        
        part_pts  = hashs( parts, [partname, "pts"] );
        part_faces= hashs( parts, [partname, "faces"] );
        //echo( j=j, partname=part[0], part=hash(parts, part[0]), part_pts=part_pts);
        //echo(i=i, j=j, partname = part[0], prelen=prelen, act=act);
        
        prelen = j==0?0
                  : sum( [ for( pp= slice( _parts,0,j) ) pp[1]+part_spacer ] );
        act = ["x",prelen, "y", mat_y //i*(matsize[1]+mat_spacer)
              ];
        //echo(part_pts=part_pts);
        //echo(act=act);
        _ppts = part_pts? transPts(part_pts, actions=act)
                        : cubePts( [plen, LEN4, LEN2], actions=act );
        ppts = _ppts; //part[4]?part[4]:_ppts;
        //echo(ppts=ppts);          
        
        DrawEdges( pts=ppts, faces=part_pts?part_faces:CUBEFACES, r=0.08 );
        
        Color( hashs(parts, [partname, "color"], "gold" ))
        polyhedron( ppts, part_pts?part_faces:CUBEFACES );
        MarkCenter( get(ppts, [4,5,6,7])
                  , Label=["text", _s("{_} ({_})", [partname, plen])
                          , "scale", 5, "followView",0
                          , "color", "black"
                          ] );
       log_e(layer=6);                   
     }   
     //log_e(layer=5 );
     
     //matsize = hashs(parts, [partname,"mat"]); //(WOODS, mat[0]); 
     //matsize = part_mats[i];
     //echo(matsize = matsize);
     
     matpts = cubePts( matsize //[ mat[1], matsize[1], matsize[0] ]
              , actions=[ "y", mat_y]); //i*(matsize[1]+mat_spacer)] );
     //matpts = cubePts(reverse( quicksort(2x4x8 ))
     //         , actions=[ "y", i*(LEN4+mat_spacer)] );
     
     DrawEdges( pts=matpts, faces=CUBEFACES , r=0.05, color="khaki");
     Gold(0.1)
     polyhedron( points=matpts, faces= CUBEFACES);
   }

}                  


module DrawPart(partname, parts, i, actions, count, debug_decos=0)
//module DrawPart(partname, parts, actions, count, debug_decos=0)
{   
  /* Draw a part
  
  Usage:  
  
      /// Define a DrawParts module:
      module DrawParts( parts, isdraw, Frame, Label, drawopts=[])
      {
        /// Make sure parts are updated:
        parts2= processParts( parts, isdraw, Frame, Label, drawopts );
                      
        Legs();

        /// Define a part module to use DrawPart: 
        module Legs(); 
        {
          act= <actions to place a leg to its final position> 
          DrawPart( "Leg", parts=parts2, actions=act);
      
          act2= <actions to place another leg> 
          DrawPart( "Leg", parts=parts2, actions=act2);
        }
      }   
  */    

  //echom(_ss("DrawPart(\"{_:c=red}\")", partname ));
  log_dp_b(partname);   
  log_dp(["part",part]);
  log_dp(["act",act]);
  //log_dp(["_drawopts",_drawopts] );
  //log_dp(["isdraw",isdraw,"Frame",Frame]);
  
                                        
  part = hash(parts, partname);
  
  //_drawopts = h(drawopts,partname);
  
  //echo(part = _fmth(part) );
  mat  = hash(part, "mat"); //=> <matname>||[x,y,z] like [96,LEN4,LEN2]
  matsize= hash(WOODS, mat, reverse(mat) ); 
          //=> material size like: [LEN2,LEN4,96]
  size = hash(part,"size", [ hash(part,"len"), matsize[1], matsize[0] ] ); 
          //=> stock size like: [LEN2,LEN4,60]
  
  //echo(mat=mat, sizes=sizes, size=size, part=part);

  act = concat(h( part, "actions",[] ), actions?actions:[]) ; //=> act to reach the final position
  //decos = h( part, "decos" );
      
  //_pts =cubePts( size, actions= concat( act?act:[], actions) );  
  //if(markpts)
  //  Red()MarkPts(_pts, Label=["text", range(_pts), "scale", 4]);           
  //echo(_pts=_pts);
//  pf= decos? decoPolyEdges(
//                  pts=_pts
//                , faces = CUBEFACES
//                , decos=decos
//                , debug = debug_decos
//                ): [_pts, CUBEFACES] ;
//  pts = pf[0];
//  faces = pf[1];
  _pts = h(part, "pts");
  pts = transPts( _pts, act); //, concat( act?act:[], actions)) ;
  faces = h(part, "faces");
  
//  ismarkpts=hash(part,"ismarkpts");
//  if(ismarkpts) // && h(ismarkpts,"i")==i)
//  {
//       MarkPts(pts//, Ball=0, Line=0
//              ,Label= update( ["text", range(pts)
//                              , "scale", 3
//                              , "color","blue"]
//                            , isarr(ismarkpts)?ismarkpts:[])
//              ,opt= update( ["Ball",0, "Line",0]
//                          , isarr(ismarkpts)?ismarkpts:[]
//                          )              
//              );   
//  }
  mopt=hash(part,"markpts"); //=> drawopts= [ "markpts", [2,opt, 3,opt] ]
                             //             [ "markpts", opt ]
                             //             [ "markpts", 1 ]  
  log_dp(["_mopt",mopt] );
  
  function _get_opt(optname)=
  (
     /*
        Process drawopts setting for 'markpts', etc
        
        To set markpts opt for a part:
         
        drawopts= [ <partname>, ["markpts",<mopt>] ]
       
        * ["markpts", 1]          //==> mark all copies of part
        * ["markpts", true]          //==> mark all copies of part
        * ["markpts", opt ]       //==> mark all copies w/ opt
        * ["markpts", [1,[]]      //==> mark copy.1
        * ["markpts", [1,[],3,[]] //==> mark copy.1 and copy.3
        * ["markpts", [1,opt1,3,opt2] //==> mark copy.1 .3 w/ opt1,2
        
        CRITICAL:
        
        opt = _get_opt(<optname>)
        if( opt || opt==[] )       <===== use this to check 
        { ... }
        
     */
  
     let( _opt = hash(part, optname)
        , opt = optname=="Frame" && isnum(_opt)?_opt
               : (isnum(_opt)&&_opt!=0)
                 ||_opt==true?[]     // _mopt = 1 || true
               : isnum(_opt[0])? h(_opt,i)    // _mopt = [2,opt, 3,opt]
               : _opt
        )
     //echo("In _get_opt:", optname=optname, _opt=_opt, opt=opt)           
     opt
  );
  
  function _get_boolean_opt(optname)=
  (
     /*
        Process drawopts setting for 'isdraw', etc
        
        To set isdraw opt for a part:
         
        drawopts= [ <partname>, ["isdraw",<mopt>] ]
       
        * ["isdraw", 1]          //==> draw all copies of part
        * ["isdraw", true]       //==> draw all copies of part
        * ["isdraw", [2,3] ]     //==> draw copy.2 and copy.3
        
     */
  
     let( _opt = hash(part, optname)
        , opt = isarr(_opt)? has(_opt, i)
                :_opt 
        )        
     opt
  );  
  
  
  isdraw = _get_boolean_opt("isdraw"); 
  Frame = _get_opt("Frame"); 
  Label = _get_opt("Label"); 
  Dims = _get_opt("Dims");
  Markpts = _get_opt("Markpts");
  
  log_dp(["Frame", Frame]);  
  log_dp(["Label", Label]);  
  log_dp(["mopt", mopt]); 
  log_dp(["Dims", Dims]); 
  
  //echo(Frame=Frame);
  //echo( str(partname,".",i), Dims = Dims);
   
  if(Markpts==[]||Markpts)
  {
       MarkPts(pts
              ,Label= update( ["text", range(pts)
                              , "scale", 3
                              , "color","blue"]
                            , Markpts 
                            )
              ,opt= update( ["Ball",0, "Line",0]
                          , Markpts 
                          )              
              );   
  }
  
  if(Dims)
  {
     Dims( pts, opts= Dims );
  }
               
  if( ( (Frame==[]||Frame) ||isdraw)&& (Label==[]||Label) )
    Transforms( act ) 
    MarkCenter( get( _pts, [4,5,6,7])
              , Label=update( ["text", i==undef?partname:str(partname,".",i)
                            , "scale", 5, "followView",0
                            , "color", "black"
                            ], Label
                            )
              , opt=Label              
              );
  //echo("In DrawPart(): ", Frame=Frame);            
  if( Frame==[]||Frame )
    DrawEdges( [pts, faces], opt=isnum(Frame)?["r",Frame]
                                 :update(["r",0.05],Frame));

  log_dp(["isdraw", isdraw]);  
  if( isdraw )
    Color(hash(part,"color") ) polyhedron( pts, faces );
  
  
    
//  if(count)
//   //module _bom(name, mat, count)  // mat=["2x4x8", [L,LEN4,LEN2]]
//  {
//     echo(
//       str( _b( partname), " : "
//          , mat, " : "
//          , _blue( hash(part, "len" ))
//          , " in"
//          , count? _red(str(" X ", _b(count))):""
//          )
//     );
//  }
   //_bom( partname , size, count);
  log_dp_e(); 
}


//. Template

/* To execute: 
  
  Template( 
  
    isdraw=1
    , Frame=1
    , Label=0
    , ismarkpts=0
    
    , offset=0
    , isCutlist= 1
    , avail_mats = ["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
    , cutlist_part_spacer= 1//0.75
    , drawopts=[    
                 "TopBarLong", ["isdraw",0, "Frame",1] 
               ,  "BarSupport", ["isdraw",0, "Frame",0] 
               ]
  );             
  
*/ 

module Template(
 
     isdraw=1
   , Frame=0.05
   , Label=0 
   , ismarkpts=0
   
   , offset=0    // distance to disassemble parts to show each part
   , avail_mats = ["B2x4x8", [60,70], [96,LEN4/2,LEN2], 75 ]
   
   , isCutlist= 1 // =1: Draw cutlist instead of product
   , cutlist_mat_spacer= 1
   , cutlist_part_spacer=1 
         // cutlist_part_spacer: 
         // The space seperates parts. This allows for considerations
         // for saw-width, bad spot in lumber, etc. It's better to 
         // print out cutlists of different part_spacer values   
   , drawopts  /// opts for individual part 
        =[ "TopBarIn", ["isdraw",1,"Frame",0]
         , "CBar", ["isdraw",1] 
         ]
   )
{
  echom("Planter_Bench_187q()");
  
  ///=====================================================
  /// Define frequently used units
  
  L = FT6;
  W = 24;
  H = 28.5;
  title= _s( "Planter_Bench ({_}x{_}x{_})",[L,W,H]); 
      
  nBar = 14; // # of short-bars on the top. They will be placed onto
             // the 2 sections seperated by TopBarMid 
  bar_w = LEN2/2; // w of the top short bars.
  bar_intv = (L-LEN2*3 - nBar* bar_w)/(nBar+2);
      
  os = offset;
  
  ///=====================================================
  /// Echo Title to console 
  
  echo(_div( _b(title) , s="font-size:24px" ));
  
  ///=====================================================
  /// Main
  
  if(isCutlist) 
   DrawCutlist( title= title 
              , parts=parts                 
              , avail_mats = avail_mats 
              , mat_spacer=cutlist_mat_spacer
              , part_spacer=cutlist_part_spacer 
              , debug= 0
              );                  
  else {
    DrawParts( parts=parts );
  }
  
 
  ///=====================================================
  /// Define parts
  
  /*
    A part is a hash containing the following keys:
     
    [ "mat" , <matname> //=> like "B2x4x8". Could also be customized [x,y,z]
    
    , "len",  <len of wanted> //=> like 36. Could be undef. see below. 
                       
    , "size", <size of wanted> //=> like [36,LEN4,LEN2] 
               --- the wanted size is determined
                   by either the len or size above.
                   When len given, use it as the x-element
                   of size with the other 2 measures (y,z) on 
                   mat, like [len, LEN4, LEN]. Otherwise, use
                   size like: [36, LEN4/2, LEN2] (this case 
                   means the mat's width, LEN4, is cut in halves).   
                                                       
    , "count", 6 //=> Count of copies. Note: this is to determine the 
                      output of cutlist, nothing to do w/ how many parts 
                      are actually drawn (which is determined by a drawing 
                      module separately).
                           
    , "color", ["teal",isCutlist?.5:1]
    
    , "actions", ["roty",-90, "y",-os] 
                 //=> actions to bring the part from mat-position lying 
                     on the XY plane) to its init-position. Its final 
                     opsition requires additinal actions defined by
                     individual drawing module.
                                                    
                             actions          actions in its
                              here            drawing mod
                     mat-pos ------> init-pos -------------> final-pos
                                            
    , "decos", [.... ]  --- data used to change the pts.                                                               
    ]
  
  */   
  _parts=  
  [
    "Outleg", [ "mat" , "B2x4x8"
              , "len", H-LEN4 
              , "count", 6                        
              , "color", ["teal",isCutlist?.5:1]
              , "actions", ["roty",-90, "rotz",-90, "y", -os]
              ]
  , "Inleg_corner",  [ "mat" , "B2x4x8"
                     , "len",  H-LEN2
                     , "decos", [ [ [4,0]
                                  , [ [-LEN2,0] , [-LEN2,-LEN4], [0,-LEN4] ]
                                  ]
                                ]
                     , "count", 4
                     , "color", ["purple",isCutlist?.3:1]
                     , "actions", ["roty",-90, "rotz",-90,"y",LEN2, "z",LEN2]
                     ] 
  , "Inleg_middle",[ "mat" , "B2x4x8"
                   , "len", H-LEN2-LEN4/4*3 
                   , "count", 2
                   , "color", ["purple",isCutlist?.3:1]
                   , "actions", ["rotx", 90, "roty",-90, "t",[L/2-LEN4/2+LEN4,LEN2*2,LEN2] ]
                   ]
                 
                   
  , "TopBarLong", [ "mat", "B2x4x8"
               , "len", L 
               , "count", 2 
               , "actions",["rotx",90, "t",[0,LEN2-os,H-LEN4+os]
                           ] 
               , "color", ["olive", isCutlist?.5:.8]
               ]
               
  , "TopBarSide", [ "mat", "B2x4x8"
            , "len", W-LEN2*2
            , "count", 2 
            , "actions",["rotx",90, "rotz",90, "t", [-os,LEN2, H-LEN4+os]] 
            , "color", ["olive", isCutlist?.5:.8]
            ]  
            
  , "TopBarMid", [ "mat", "B2x4x8"
           , "size", [W-LEN2*2, LEN4/2, LEN2]
           , "actions", [ "rotx",90, "rotz",90, "t",[L/2-LEN2/2, LEN2, H-LEN4/2+os*2] ]
           , "color", ["olive", isCutlist?.5:1]
            ]
  , "BarSupport", [ "mat", [96, LEN4/2, LEN2] //"B2x4x8"
               , "size", [L-LEN4*2, LEN4/2, LEN2]
               , "actions", ["rotx",90, "t", [LEN4, LEN2*2,H-LEN4+os]] 
               , "count", 2 
               , "color", "khaki"
               , "decos"
                  ,[ [ [1,5]
                   , [ [0,0], [0,-(L-LEN4*3)/2]  
                     , [-LEN4/4,-(L-LEN4*3)/2]  
                     , [-LEN4/4,-(L-LEN4*3)/2-LEN4]  
                     , [0,-(L-LEN4*3)/2-LEN4]  
                     ]
                   ]                 
                   ]     
               ]
  , "TopBarIn" , [ "mat", "B2x4x8"
              , "size", [W-LEN2*2, LEN4/2, LEN2/2]
              , "count", 4 
              , "actions", ["rotx",90, "rotz",90, "t", [0, LEN2, H-LEN4/2+os*2] ] 
              , "color", "khaki"
              ]
          
  , "BotBarLong", [ "mat", "B2x4x8"
                   , "len", L 
                   , "count", 1 
                   , "color", ["green",isCutlist?0.5:1]
                   , "actions", ["y", W-LEN4, "z", -os ]
                   , "decos"
                      ,[ [ [3,7]
                         , [ [0,0], [0,-L/2+LEN4/2]
                           , [-LEN2,-L/2+LEN4/2]
                           , [-LEN2,-LEN4-L/2+LEN4/2]
                           , [0,-LEN4-L/2+LEN4/2]
                           ]
                         ]  
                       , [ [6,2]
                         , [ [-LEN2,0]
                           , [-LEN2,-LEN4]
                           , [0,-LEN4]
                           ]
                         ]
                       , [ [3,7] ]
                       ]
                   ]
                   
  , "BotBarShort", [ "mat", //[96, LEN4,LEN2] 
                            "B2x4x8"
                    , "len", W-LEN4-LEN2 
                    , "count", 3 
                    , "actions", ["rotz", 90, "t",[LEN4,LEN2-os/2, -os] ] 
                    , "color", ["green",isCutlist?0.5:1]
                   ]
           
  , "CBar", [ "mat", //[96, LEN4,LEN2]     
                     "B2x4x8"
                 , "size", [6, LEN4/2, LEN2]
                 //, "len", 6 
                 , "count", 7
                 , "decos",[ [ [7,3], [[-LEN4/2,0],[0,-LEN4/2]] ] 
                           , [ [2,6] ]
                           ] 
                 , "color", "khaki"   
                 , "actions",[]        
                 ] 
  ];
  
  ///=====================================================
  /// Update parts w/ other settings
  
  parts= processParts(_parts, isdraw, Frame, Label, drawopts);


  ///=====================================================
  /// Draw parts module. It contains modules to draw
  /// individual parts. 
  
  module DrawParts( parts ) 
  {
    log_dps_b();
    
    Legs();
    TopOutBars();
    TopInBars();
    Bottom_bars();
    Corner_bars();

    module Legs() 
    {
      log_dps2_b("Legs()");
      log_dps2( str("sas=", sas) );
      log_dps2( str("sas2=", sas2) );
      
      sas = spreadActions( objlen=LEN4, spreadlen=L,n=3);
      for(i = range(sas)) 
      { 
        act= sas[i];
        DrawPart( partname="Outleg", parts=parts
                , actions=sas[i], count=i==2?6:0);        
        DrawPart( "Outleg", parts=parts
                , actions=concat( ["mirror", [[0,W/2,0],ORIGIN]], act));
      }
      
      sas2= spreadCornerActions([ X*L, ORIGIN, Y*W] );
      for(i=range(sas2))
        DrawPart( "Inleg_corner", parts=parts
                , actions=sas2[i], count=i==3?4:0);
      
      DrawPart( "Inleg_middle", parts=parts
                , drawopts=drawopts);
      DrawPart( "Inleg_middle", parts=parts
                , actions=["mirror", [[0,W/2,0],ORIGIN]], count=2);
                
      log_dps2_e("Legs()");
    }  
  
    module TopOutBars()
    {
      log_dps2_b("TopOutBars()");
      DrawPart("TopBarLong", parts=parts, count=2);
      DrawPart("TopBarLong", parts=parts, actions=["mirror", [W/2*Y,O]]); 
      DrawPart("TopBarSide", parts=parts, count=2);
      DrawPart("TopBarSide", parts=parts, actions=["mirror", [L/2*X,O]]);
      log_dps2_e("TopOutBars()");
    }  
  
    module TopInBars()
    {
      log_dps2_b("TopInBars()");
      DrawPart("TopBarMid", parts=parts,  count=1);
      DrawPart("BarSupport", parts=parts, count=2, markpts=0);   
      DrawPart("BarSupport", parts=parts, actions=["y",W-LEN2*3]); 
        
      sas1= spreadActions(bar_w, L/2, nBar/2 +2);      
      for(i=[1:len(sas1)-2])
      { 
         DrawPart("TopBarIn", parts=parts, actions= sas1[i], bar_w= bar_w );
         DrawPart("TopBarIn", parts=parts, actions= concat( sas1[i], ["x", L/2])
                  , count=i== nBar/2? nBar:0 
                  , bar_w= bar_w
                  );
      }   
      
      log_dps2( ["bar_intv(in)", bar_intv], layer=LOG_DPS+1);
      log_dps2_e("TopInBars()");
    }  
  
    module Bottom_bars()
    {
      log_dps2_b("Bottom_bars()");
      DrawPart("BotBarLong", parts=parts, count=1);
      sas = spreadActions( LEN4, L,3 ) ;
      for( i=range(sas)) 
        DrawPart("BotBarShort", parts=parts,actions= sas[i], count= i==2? 3:0);     
      log_dps2_e("Bottom_bars()");
    }
  
    module Corner_bars()
    {
      log_dps2_b("Corner_bars()");
      act= [ "rotx", 90, "roty", -45 
             , "t", [ 0, LEN2, H-LEN4-6/sqrt(2) ]
             ];
    
      Front();     
      translate(W*Y) mirror(Y) Front(print_count=14);                                   
    
 
      module Front_left()
      {    
        DrawPart("CBar", parts=parts, actions=concat(act,["x",LEN4,"y",-os]), markpts=0); // "/"
        DrawPart("CBar", parts=parts, actions=concat(act,["rotz",90, "t",[LEN2,LEN2*2,0]])); // "/"
        DrawPart("CBar", parts=parts, actions=concat(act,["mirror"
                                           ,[X*((L-LEN4*3)/4+LEN4/2),O]
                                           , "y",-os
                                           ]));      // "\"
      }
      module Front(print_count)
      {
        Front_left();
        translate(L*X) mirror(X) Front_left();
        DrawPart("CBar", parts=parts, actions=concat(act
                       ,["rotz",90
                        , "t",[LEN2+L/2-LEN2/2, LEN2*2,LEN4/2]])
                       , count=print_count); // "/"
      }
            
      log_dps2_e("Corner_bars()");
    }
    
    log_dps_e();
    
  } //<= DrawParts()

}
  
//Template();

//. Objects

function T_Track_Pts( len= 24, ops=[] )=
(   33 //let( )
 
);

//. logging 
//
// Set logging layer, used for the arg layer: log_(s, layer, ...)
// 
LOG_DPS = 1;  // DrawParts
LOG_DPS2= 2;  // DrawParts
LOG_DP  = 4;  // DrawPart

LOG_CL1 = 5;  // cutlist_for_1_mat
LOG_CL  = 4;  // cutlist
LOG_DCL = 3;  // DrawCutlist

LOG_PP  = 5;  // processParts

// cutlist
function log_cl_b(mode, add_ind, prompt, debug, hParams)=
   log_b( "cutlist()", LOG_CL, add_ind, prompt, debug, hParams); 
function log_cl(ss, mode, add_ind, prompt, debug, hParams)=
   log_(ss, LOG_CL, add_ind, prompt, debug, hParams); 
function log_cl_e(ss, mode, add_ind, prompt, debug, hParams)=
   log_e(ss?ss:"", LOG_CL, add_ind, prompt, debug, hParams); 
   
// cutlist_for_1_mat
function log_cl1_b(mode, add_ind, prompt, debug, hParams)=
   log_b( "cutlist_for_1_mat()", LOG_CL, add_ind, prompt, debug, hParams); 
function log_cl1(ss, mode, add_ind, prompt, debug, hParams)=
   log_(ss, LOG_CL, add_ind, prompt, debug, hParams); 
function log_cl1_e(ss, mode, add_ind, prompt, debug, hParams)=
   log_e(ss?ss:"", LOG_CL, add_ind, prompt, debug, hParams); 

// DrawCutlist
module log_dcl_b(mode, add_ind, prompt, debug, hParams){
   log_b("DrawCutlist", LOG_DCL, add_ind, prompt, debug, hParams); }
module log_dcl(ss, mode, add_ind, prompt, debug, hParams){
   log_(ss, LOG_DCL, add_ind, prompt, debug, hParams); }
module log_dcl_e(mode, add_ind, prompt, debug, hParams){
   log_e("DrawCutlist", LOG_DCL, add_ind, prompt, debug, hParams); }

// DrawPart
module log_dp_b(partname, mode, add_ind, prompt, debug, hParams){
   log_b(str("DrawPart(",_b(partname),")"), LOG_DP, add_ind, prompt, debug, hParams); }
module log_dp(ss, mode, add_ind, prompt, debug, hParams){
   log_(ss, LOG_DP, add_ind, prompt, debug, hParams); }
module log_dp_e(ss, mode, add_ind, prompt, debug, hParams){
   log_e( ss?ss:"", LOG_DP, add_ind, prompt, debug, hParams); }

// DrawParts
module log_dps_b(mode, add_ind, prompt, debug, hParams){
   log_b("DrawParts", LOG_DPS, add_ind, prompt, debug, hParams); }
module log_dps(ss, mode, add_ind, prompt, debug, hParams){
   log_(ss, LOG_DPS, add_ind, prompt, debug, hParams); }
module log_dps_e(ss, mode, add_ind, prompt, debug, hParams){
   log_e("DrawParts", LOG_DPS, add_ind, prompt, debug, hParams); }
   
module log_dps2_b(ss, mode, add_ind, prompt, debug, hParams){
   log_b( str("DrawParts", ss?" -- ":"", ss?_b(ss):"")
        , LOG_DPS2, add_ind, prompt, debug, hParams); }
module log_dps2(ss, mode, add_ind, prompt, debug, hParams){
   log_(ss, LOG_DPS2, add_ind, prompt, debug, hParams); }
module log_dps2_e(ss, mode, add_ind, prompt, debug, hParams){
   log_e( str("DrawParts", ss?" -- ":"", ss?_b(ss):"")
        , LOG_DPS2, add_ind, prompt, debug, hParams); }
// processParts
function log_pp_b(partname, mode, add_ind, prompt, debug, hParams)=
   log_b(str("processParts() -- ", _b(partname)), LOG_PP, add_ind, prompt, debug, hParams); 
function log_pp(ss, mode, add_ind, prompt, debug, hParams)=
   log_(ss, LOG_PP, add_ind, prompt, debug, hParams); 
function log_pp_e(ss,mode, add_ind, prompt, debug, hParams)=
   log_e(ss?ss:"", LOG_PP, add_ind, prompt, debug, hParams); 

