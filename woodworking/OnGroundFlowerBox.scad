include <../scadx.scad>
include <scadx_woodworking.scad>

include <../scadx_faces.scad>

/*To join 4x4 lumbers for an on-the-ground plant box

Top view: 

              back 
      +=======================+ ---
      |                       |  :
 Left |  Planter box          |  W    right
      |                       |  : 
      +=======================+ ---
      |........... L .........|
              front 

Front view:

  +=======================+ ---
  |        4x4            |  : 
  +=======================+  :
  |        4x4            | H (Layers= 3)    
  +=======================+  :
  |        4x4            |  : 
  +=======================+ ---

Left|front view:

  ===========+==============
  ===========+           
             +==============
  ===========+
             +==============
  ===========+                
  ===========+==============
     Left         Front 
     
     
Detailed Left View

:

 ==================================+    +============================+
                                   |    ::::::::::::::::::::::::::::::
      .                            |    :_::::::::::::::::::::::::::::
      |`'''--...__                 |    | '''--...__::::::::::::::::::
      |          -'''--.._         |    |- - - - - -'''--.._::::::::::
      |                    ```---..|    |                    ```---...
      |                                 |_                           |
      |                                 |- - - - - - - - - - - - - - |
      |                                 |                            |
      |                      __...-|    |                      __...-'
      |           __...---'''      |    |- - - - - -__...---'''::::::L
      |_...---''''                 |    |_...---''''::::::::::::::::::
                                   |    ::::::::::::::::::::::::::::::
  =================================+    +============================+
  
  
  

 ==================================+    +============================+
                                   |    ::::::::::::::::::::::::::::::
      .                            |    :_::::::::::::::::::::::::::::
      |`'''--...__                 |    | '''--...__::::::::::::::::::
      |          -'''--.._       a |    |- - - - - -'''--.._::::::::::
      |                    ```---..|    |                    ```---...
      |                                 |                            |
 =====+                                 |- - - - - - - - - - - - - - |
      |                                 |                            |
      |                      __...-|    |                      __...-'
      |           __...---'''      |    |- - - - - -__...---'''::::::L
      |_...---''''                 |    |_...---''''::::::::::::::::::
                                   |    ::::::::::::::::::::::::::::::
                                   |    +============================+
                                   |    ::::::::::::::::::::::::::::::
      .                            |    :_::::::::::::::::::::::::::::
      |`'''--...__                 |    | '''--...__::::::::::::::::::
      |          -'''--.._         |    |- - - - - -'''--.._::::::::::
      |                    ```---..|    |                    ```---...
      |                                 |                            |
 =====+                                 |- - - - - - - - - - - - - - |
      |                                 |                            |
      |                      __...-|    |                      __...-'
      |           __...---'''      |    |- - - - - -__...---'''::::::L
      |_...---''''                 |    |_...---''''::::::::::::::::::
                                   |    ::::::::::::::::::::::::::::::
                                   |    +============================+
                                   |    ::::::::::::::::::::::::::::::
      .                            |    :_::::::::::::::::::::::::::::
      |`'''--...__                 |    | '''--...__::::::::::::::::::
      |          -'''--.._         |    |- - - - - -'''--.._::::::::::
      |                    ```---..|    |                    ```---...    
      |                                 |                            |
 =====+                                 |- - - - - - - - - - - - - - |
      |                                 |                            |
      |                      __...-|    |                      __...-'
      |           __...---'''      |    |- - - - - -__...---'''::::::L
      |_...---''''                 |    |_...---''''::::::::::::::::::
                                   |    ::::::::::::::::::::::::::::::
 ==================================+    +============================+  
  
      |............ d .............|
      

a : angle. Default = 10
d : end length. Default = the 4 in 4x4 
    But could be shortened (to allow a cap cover to prevent rot)
    
One way of cutting is to ignored a value, but simply find specific 
ratio on the cut side:        

RATIO_OF_QUOTER_CUT = 1/3

   ---    +============================+
   1/4    :_::::::::::::::::::::::::::::
          | '''--...__::::::::::::::::::
   ---    |- - - - - -'''--.._:::::::::: -----------
   1/4    |                    ```---... 1/3 of 1/4  
          |                            | 2/3 of 1/4
   ---    |- - - - - - - - - - - - - - | -----------
   1/4    |                            |
          |                      __...-'
   ---    |- - - - - -__...---'''::::::L
   1/4    |_...---''''::::::::::::::::::
          ::::::::::::::::::::::::::::::
   ---    +============================+ 
       
 
  SideBar: 

            <--- back                          front -->       

                     =====+                                
                          |                                 
                          |                      __...-|    
                          |           __...---'''      |    
    ___                   |_...---''''                 |    
     :                  tbh                            |    
   TB_H             (tenon back                        |    
     :                  height)       tenon            |  tfh ( tenon front height )  
    ___       _ _ _ _ _ _ .                            |    
     :           TILL    |`'''--...__                  |    
     :       =============+          -'''--.._         |    
LARGE_SPACER        _ _ _ |                    ```---..|  ___     
     :                    |                                SMALL_SPACER (tf bottom height) 
    ---              =====+   - - - - - - - - - - - - -   ---    
                                   
                sblen2 ---|
                sblen ---------------------------------|        

*/

//. Parameters
/*  ____                                  __                
   / __ \____ __________ _____ ___  ___  / /____  __________
  / /_/ / __ `/ ___/ __ `/ __ `__ \/ _ \/ __/ _ \/ ___/ ___/
 / ____/ /_/ / /  / /_/ / / / / / /  __/ /_/  __/ /  (__  ) 
/_/    \__,_/_/   \__,_/_/ /_/ /_/\___/\__/\___/_/  /____/  

*/                                                            

_TILL_RATIO= 9.5405;
RATIO_OF_QUOTER_CUT = 1/ _TILL_RATIO; 
SIDEBAR_L= 2.5*12;
FRONTBAR_L = 8*12;
TILL= RATIO_OF_QUOTER_CUT*1/4*LEN4;  // How much the slope contact 
                                     // deviated from the 1/4 line
                                     // over the len of LEN4
TILL_SLOPE = 2*TILL / LEN4;

//
// Till angle. For ref only. No need in calc.
//
pqr_for_angle= [X*5,O,X+Y*TILL_SLOPE]; 
TILL_A = angle ( pqr_for_angle ); 
//MarkPts( pqr_for_angle );



//ANGLE = angle( [ X*10, 0, X + Z*TILL ] );
//MarkPts( [ X*10, O, X + Z*TILL ] );
//echo( ANGLE = ANGLE ); 
UNIT_COORD_TILL_FRONT_DOWN= [ 1, 0, -TILL_SLOPE];
UNIT_COORD_TILL_FRONT_UP  = [ 1, 0, TILL_SLOPE];
UNIT_COORD_TILL_BACK_DOWN = [ -1, 0, -TILL_SLOPE];
UNIT_COORD_TILL_BACK_UP   = [ -1, 0, TILL_SLOPE];
                                     
//TILL_FRONT_DOWN = [LEN4, 0, -TILL*2]; // ''--..__  ==>
//TILL_FRONT_UP   = [LEN4, 0,  TILL*2]; // __..--''  ==>
//TILL_BACK_DOWN  = [-LEN4, 0,-TILL*2]; // __..--''  <==
//TILL_BACK_UP    = [-LEN4, 0, TILL*2]; // ''--..__  <==

LARGE_SPACER = LEN4/4 + RATIO_OF_QUOTER_CUT*LEN4/4;
SMALL_SPACER = LARGE_SPACER- TILL*2;

TB_H = LEN4-2*LARGE_SPACER;  // tenon back height
TF_H = LEN4-2*SMALL_SPACER;  // tenon front height

FB_TATIO = TF_H/TB_H;

TEMPLATE_BASE_H = LEN4*2; 
 
module description()
{ 
    // Outpot a styled description of the parameters 
    echo(_span("<br>Design of An On-ground Flower Box", s="color:red;font-size:20px"));
    function _pair(k,v, post="")=
    ( str( "<br>"
         ,_span( k, s="color:blue;font-weight:900px;text-decoration:underline"), " = "
         , _span( v, s="color:red" )
         , " ", post 
         ) 
    );
    echo(
      str( 
      , _pair( "FRONTBAR_L", FRONTBAR_L, "in")
      , _pair( "SIDEBAR_L", SIDEBAR_L, "in")
      , _pair("_TILL_RATIO", _TILL_RATIO)
      //, _pair("_", LEN4/LEN1*_TILL_RATIO)
      , _pair( "Till_SLOPE", TILL_SLOPE)  
      , _pair( "Till_A (= till angle) ", TILL_A, "(unused in calc)" )
      )
    ); 
}
description();

 

//. Main
/*      __  ___      _     
       /  |/  /___ _(_)___ 
      / /|_/ / __ `/ / __ \
     / /  / / /_/ / / / / /
    /_/  /_/\__,_/_/_/ /_/ 
*/                       
 
module SideBar( title=false, dim=false, markpts=false, withguide=false, transparancy=0.2, line_r= 0.01 ){ echom("SideBar");
  
  sblen =SIDEBAR_L;
  
  pts= growPts( -sblen*X
        , [ "x", (sblen-LEN4)
          , "z", LARGE_SPACER
          , "t", UNIT_COORD_TILL_FRONT_DOWN*LEN4
          , "z", TF_H
          , "t", UNIT_COORD_TILL_BACK_DOWN*LEN4
          , "z", LARGE_SPACER
          , "x", -(sblen-LEN4)
          , "t", [0, LEN4, -LEN4]
          , "repeat", [0,6]
          ]  
  );
  
  if(markpts)
  Red() MarkPts( pts, opt=["Label",["scale", 3]] ); 
  
  fs = rodfaces(8); 
  Blue() DrawEdges(pts=pts,faces=fs, r=line_r);  
  
  if(title)
  {
  title_site= [[0,LEN4,LEN4], pts[15], [SIDEBAR_L, 2*LEN4,2*LEN4] ];
  Text("SideBar", site= title_site, color="red", actions=["y",.8, "x",SIDEBAR_L*0.4]); 
  }
  
  if (dim)
  Red()
  { 
    //Dims( opts=[ [0,7,9], [7,15,(pts[8]+pts[9])*0.8]], pts=pts);
    Dim( [0,7,15,(pts[8]+pts[9])*0.8], pts=pts, Label=["scale",2]);
    Dim( [15,14,[0,LEN4,LEN4], (pts[1]+pts[9])/2], pts=pts, Label=["scale",2.5]);
    Dim( [1,2,5,6,3], pts=pts, spacer=.5, Label=["actions",["rotz", -90], "scale",2]);
    Dim( [ [0,LEN4,LEN4], 12,11,Y*LEN4,0],pts=pts, Label=["actions",["rotz", 90], "scale",2]);
  }
  Red(transparancy) 
  polyhedron( pts, rodfaces(8) ); 
  
  if(withguide)
  SideBarGuide();         

}

module SideBar_TopHalf( title=false, markpts=false, transparancy=0.2, line_r= 0.01  ){ echom("SideBar_TopHalf");
  
  sblen =SIDEBAR_L;
  
  pts= growPts( [ -(sblen-LEN4),0,LEN4/2 ]
        , [ "x", sblen
          , "z", TF_H/2
          , "t", UNIT_COORD_TILL_BACK_DOWN*LEN4
          , "z", LARGE_SPACER
          , "x", -(sblen-LEN4)
          , "t", [0, LEN4, -LEN4/2]
          , "repeat", [0,4]
          ]  
  );
  if(markpts)
  Red() MarkPts( pts, opt=["Label",["scale", 3]] ); 
  
  fs= [ [5,4,3,2,1,0], [6,7,8,9,10,11]
      , [4,5,11,10], [1,0,6,7], [2,3,9,8]
      , [0,6,11,5], [3,4,10,9], [1,2,8,7]
      ];
  
  Blue() DrawEdges(pts=pts,faces=fs, r=line_r );  
  
  if(title)
  {
  title_site= [[-SIDEBAR_L, LEN4,LEN4], pts[10], [0, 2*LEN4,2*LEN4] ];
  //echo( title_site = title_site);
  //MarkPts( title_site );
  Text("SideBar_TopHalf", site= title_site, color="red", actions=["roty",180, "y", 1, "x",SIDEBAR_L*0.5]); 
  }
  
  Red( transparancy ) 
  polyhedron( pts, fs );        

}

module SideBar_BottomHalf( title=false, markpts=false, transparancy=0.2, line_r= 0.01 ){ echom("SideBar");
  
  sblen =SIDEBAR_L;
  
  pts= growPts( -sblen*X
        , [ "x", (sblen-LEN4)
          , "z", LARGE_SPACER
          , "t", UNIT_COORD_TILL_FRONT_DOWN*LEN4
          , "z", TF_H/2
          , "x", -sblen
          , "t", [0, LEN4, -LEN4/2]
          , "repeat", [0,4]
          ]  
  );
  
  if(markpts)
  Red() MarkPts( pts, opt=["Label",["scale", 3]] ); 
  
  fs = [ [ 5,4,3,2,1,0], [6,7,8,9,10,11]
       , [6,11,5,0], [1,2,8,7], [3,4,10,9]
       , [4,5,11,10], [9,8,2,3], [0,1,7,6]
       ] ;
  //for( p = slice(fs,[0,1]) ) Plane(get(pts,p));
  //Plane( get(pts,fs[7]) );     
       
  Blue() DrawEdges(pts=pts,faces=fs, r= line_r );  
  
  if(title)
  {
  title_site= [ pts[11], pts[10], [0, 1.5*LEN4,2*LEN4] ];
  Text( "SideBar_BottomHalf", site= title_site, color="red"
      , actions=["roty",180, "y",.4, "x",SIDEBAR_L*0.7]); 
  }
  
  Red( transparancy ) 
  polyhedron( pts, fs);         

}


module FrontBar( title=false, dim=false, markpts=false, withguide=false, transparancy=0.2, line_r=0.01 ){ echom("FrontBar");
/*

  <--- back                          front -->       

           =====+                                
                |                                 
                |                      __...-|    
                |           __...---'''      |    
                |_...---''''                 |    
              tbh                            |    
          (tenon back                        |    
              height)       tenon            |  tfh ( tenon front height )  
    _ _ _ _ _ _ .                            |    
       TILL    |`'''--...__                 |    
   =============+          -'''--.._         |    
          _ _ _ | LARGE_SPACER               ```---..|  ___     
                |                                SMALL_SPACER (tf bottom height) 
           =====+   - - - - - - - - - - - - -   ---    
                                   
      sblen2 ---|
      sblen ---------------------------------|

   ---    +============================+
   1/4    :_::::::::::::::::::::::::::::
          | '''--...__::::::::::::::::::
   ---    |- - - - - -'''--.._:::::::::: -----------
   1/4    |                    ```---... 1/3 of 1/4  
          |                            | 2/3 of 1/4
   ---    |- - - - - - - - - - - - - - | -----------
   1/4    |                            |
          |                      __...-'
   ---    |- - - - - -__...---'''::::::L
   1/4    |_...---''''::::::::::::::::::
          ::::::::::::::::::::::::::::::
   ---    +============================+ 
       


*/
  //FRONTBAR_L = LEN4+2;//LEN4*2;
  rq = RATIO_OF_QUOTER_CUT;
  LARGE_SPACER =  LEN4/4 + rq*LEN4/4;
  SMALL_SPACER =  LEN4/4 - rq*LEN4/4;  
  
  pts = growPts( Y*FRONTBAR_L
    ,[ "1:z", LEN4, "2:y", -(FRONTBAR_L-LEN4), "3:z", -LARGE_SPACER
     , "4:y", -LEN4, "5:z", -TB_H, "6:y", LEN4, "7:z", -LARGE_SPACER   

     , "8(next half):t", [-LEN4, FRONTBAR_L-LEN4,0]
     , "9:z", LEN4, "10:y", -(FRONTBAR_L-LEN4), "11:z", -SMALL_SPACER
     , "12:y", -LEN4
     
     
     , "13:z", -TF_H, "14:y", LEN4, "15:z", -SMALL_SPACER   
     ]
     ); 
  
  if(markpts)
  MarkPts( pts, color=["blue",0.4] );//, Label=false);  
  
  //echo([for(i=range("abc")) "abc"[i]]);
  if(title)
  {
  title_site= [ [-1.1*LEN4, 2*LEN4,1.1*LEN4], [-1.1*LEN4,0,1.1*LEN4], [-1.5*LEN4, 0,2*LEN4]];
  //MarkPts(title_site);
  Text("FrontBar", site= title_site, color="Green", actions=["y",.5, "x",1]); 
  }
  
  if(dim)
  Green()
  { 
    //Dims( opts=[ [0,7,9], [7,15,(pts[8]+pts[9])*0.8]], pts=pts);
    Dim( [[-LEN4,2*LEN4,LEN4],[0,2*LEN4,LEN4], [-LEN4,2*LEN4,0]], Label=["scale",2]);
    Dim( [[0,2*LEN4,LEN4], Y*2*LEN4, [-LEN4,2*LEN4,0]], Label=["scale",2]);
    Dim( [[-LEN4,0, LEN4], 10, 9, 7], pts=pts, Label=["scale",2.5]);
    Dim( [2,3,6,7,13], pts=pts, spacer=.5, Label=["actions",["rotz", 90], "scale",2]);
    Dim( [ [-LEN4,0, 0], 13,12, [-LEN4,0, LEN4] ,7],pts=pts, spacer=0.5
       , Label=["actions",["rotz", -90], "scale",2]);
  }
   
  Green() DrawEdges(pts=pts,faces=rodfaces(8), r=line_r); 
  Green( transparancy ) polyhedron( pts, rodfaces(8) );          
 
  if(withguide)
  FrontBarGuide();
}

guide_fs=[ [11,10,9,8,3,2,1,0]
     , [28,29,30,31,36,37,38,39]
     
     
     // base top and bottom  
     , [39,38,10,11]
     , [0, 1, 29,28]
     
     // inner wall 
     , [20,21,22,23,24,25, 26,27]
     , [12,13,14,15,16,17,18,19]
     , [21,20,19,18]
     , [27,26,13,12]
     
     // frame back
     , [28,39,20,27]
     , [39,11,19,20]
     , [12,19,11,0]
     , [0,28,27,12]
     
     
     // frame front (in the middle of the whole thing) -- top
     , [21,18,10,38]
     , [38,37,22,21]
     , [18,17,9,10]
     
     // frome front -- bottom
     , [29,1,13,26]
     , [1,2,14,13]
     , [26,25,30,29]
     
     // tongue top / bottom 
     , [6,7,8,9,17,16,23,22,37,36,35,34]
     , [2,3,4,5,33,32,31,30,25,24,15,14]
     
     // tongue front
     , [4,3,8,7]
     , [31,32,35,36]
     
     // tongue inner wall (middle separator) 
     , [24,23,16,15 ]
     , [5,6,34,33]
  ];
  
  
module FrontBarGuide( title=false, dim=false, markpts=false )
{
  EXT_BACK = LEN4;
  EXT_FRONT= LEN4;  
  SMALL_SPACER_OUTER = SMALL_SPACER- (UNIT_COORD_TILL_FRONT_UP* LEN1)[2];
  LARGE_SPACER_OUTER = LARGE_SPACER+ (UNIT_COORD_TILL_FRONT_UP* LEN1)[2];
  
  pts= growPts( [-(LEN4+LEN1), (LEN4+EXT_BACK), -LEN1],
       [ 
       
       //
       // side-bottom
        "y", -EXT_BACK
       , "z", SMALL_SPACER_OUTER+LEN1
       , "y", -(LEN4+EXT_FRONT)
       
       // front inner face
       , "t", UNIT_COORD_TILL_FRONT_UP* LEN1
       , "y", EXT_FRONT-LEN1
       , "z", LEN4-2*SMALL_SPACER
       , "y", -(EXT_FRONT-LEN1)
       
       // side-top
       , "t", UNIT_COORD_TILL_BACK_UP* LEN1
       , "y", LEN4+EXT_FRONT
       , "z", SMALL_SPACER_OUTER+LEN1
       , "y", EXT_BACK
       
       // ==========================================
       // Side inner --- back
       , "t", [LEN1, 0, -LEN4-LEN1]
       , "y", -EXT_BACK
       , "z", SMALL_SPACER
       , "y", -LEN4
       , "z", LEN4-2*SMALL_SPACER
       , "y", LEN4
       , "z", SMALL_SPACER
       , "y", EXT_BACK
       
       // Another Side inner --- back
       , "x", LEN4
       , "y", -EXT_BACK
       , "z", -LARGE_SPACER
       , "y", -LEN4
       , "z", -(LEN4-2*LARGE_SPACER)
       , "y", LEN4
       , "z", - LARGE_SPACER
       , "y", EXT_BACK       
       
       // Another Side bottom
       , "t", [LEN1, 0, -LEN1]
       , "y", -EXT_BACK
       , "z", LARGE_SPACER_OUTER+LEN1
       , "y", -(LEN4+EXT_FRONT)
       
       // Another Side front inner face
       , "t", UNIT_COORD_TILL_BACK_DOWN* LEN1
       , "y", EXT_FRONT-LEN1
       , "z", LEN4-2*LARGE_SPACER
       , "y", -(EXT_FRONT-LEN1)
       
       // Another side-top
       , "t", UNIT_COORD_TILL_FRONT_DOWN* LEN1
       , "y", LEN4+EXT_FRONT
       , "z", LARGE_SPACER_OUTER+LEN1
       , "y", EXT_BACK       
       ]
  );
  
  // Check angle:
//  pqr = [ pts[2], pts[30], pts[30]-X*(2*LEN1+LEN4)]; 
//  MarkPts( pqr ); 
//  a= angle( pqr );
//  echo(a = a);
  
  if(markpts)
  { 
  Red() MarkPts( slice(pts, 0, 13) );
  Black()MarkPts( slice(pts, 12, 20), countbeg=12 );
  Blue()MarkPts( slice(pts, 20, 28), countbeg=20 );
  Green()MarkPts( slice(pts, 28), countbeg=28 );
  }
  
  if(title)
  {
  title_site= [ pts[11]
              , onlinePt( [pts[4], pts[8]], len=4)
              , pts[12]
              ];
  //MarkPts(title_site);                            
  Text("FrontBarGuide", site= title_site, scale=0.6,actions=["rotx", 180, "y", -.5, "x",2]); 
  }
  
  if(dim)
  Blue()
  {
  Dim( [ 1,2,9,10,39], pts=pts );
  Dim( [ intsec( [pts[13], pts[14]], [ pts[1], pts[29] ] ), 14,17
       , intsec( [pts[13], pts[14]], [ pts[10], pts[38] ] ), 39 ], pts=pts, spacer= -.5 ); 
  
  Dim( [ intsec( [pts[25], pts[26]], [ pts[1], pts[29] ] ), 25,22
       , intsec( [pts[25], pts[26]], [ pts[10], pts[38] ] ), pts[38]+Y], pts=pts); 
  
  //Dim( [29,30,37,38, onlinePt( [pts[10],pts[38]], ratio=2 )] ,pts=pts, spacer= -.5 );
  Dim( [29,30,37,38, 16] ,pts=pts, Label=["actions",["rotx", 180]]);
  Dim([ 16,17,22], pts=pts);
  
  Dim( [ 10,11,28], pts=pts);
  Dim( [ 8,9,29] , pts=pts, spacer=.75);
  Dim( [ 11,39, 29] , pts=pts);
  Dim( [ 39,28,1] , pts=pts, Label=["actions",["rotz", 180]]);
  Dim( [ 18,21,22] , pts=pts, spacer=1);
  
  Dim( [ 36, 31, 32] , pts=pts, Label=["actions",["rotz", 180]]);
  Dim( [ 35,32,4] , pts=pts, spacer= -0.5, Label=["actions",["rotz", 180]]);
  Dim( [ 7,4,3] , pts=pts, Label=["actions",["rotz", 180]]);
  Dim( [ 3,8,32] , pts=pts);//, Label=["actions",["rotz", 180]] );
  
  }
  
  
  Brown(0.2)DrawEdges( pts=pts, faces= guide_fs, r=0.015 );
  
  Gold(0.4) polyhedron( pts, guide_fs );
  
}

module SideBarGuide( title=false, dim=false, markpts=false )
{
  //-------------------------------------------------------------------
  // This is to replace the templates, which seem to be too complicated
  //-------------------------------------------------------------------
  echom("SideBarGuide()");
  
  EXT_BACK = LEN4;
  EXT_FRONT= LEN4;
  pts= growPts( [ -(EXT_BACK+LEN4), -LEN1, -LEN1] 
       , ["x", LEN4
         , "z", LARGE_SPACER+LEN1
         , "t", UNIT_COORD_TILL_FRONT_DOWN*(LEN4+EXT_FRONT)
        
         , "y", LEN1 //4
         , "t", UNIT_COORD_TILL_BACK_UP*(EXT_BACK-LEN1)
         , "z", 2*TILL_SLOPE*(LEN4+LEN1)+ TB_H //TF_H/TB_H*(LEN4+LEN1)/LEN4
         , "t", UNIT_COORD_TILL_FRONT_UP*(EXT_BACK-LEN1)
         
         , "y", -LEN1
         , "t", UNIT_COORD_TILL_BACK_DOWN*(LEN4+EXT_FRONT)
         , "z", LARGE_SPACER+LEN1
         , "x", -EXT_BACK  //7
       
         , "t", [0,LEN1, -LEN4-LEN1]
         , "x", LEN4
         , "z", LARGE_SPACER
         , "t", UNIT_COORD_TILL_FRONT_DOWN*LEN4
         , "z", TF_H
         , "t", UNIT_COORD_TILL_BACK_DOWN*LEN4
         , "z", LARGE_SPACER // 13
          
         , "x", -EXT_BACK
         , "y", LEN4
         , "x", EXT_BACK 
         , "z", -LARGE_SPACER
         , "t", UNIT_COORD_TILL_FRONT_UP*LEN4
         , "z", -TF_H
         , "t", -UNIT_COORD_TILL_FRONT_DOWN*LEN4
         , "z", -LARGE_SPACER
         , "x", -EXT_BACK
         
         , "t", [0, LEN1, -LEN1]
         , "repeat", [0,2]
         , "y", -LEN1
         , "repeat", [4,6]
         , "y", LEN1
         , "repeat", [8,10]
         
//         , "x", LEN4
//         , "z", LARGE_SPACER+LEN1
//         , "t", UNIT_COORD_TILL_FRONT_DOWN*(LEN4+EXT_FRONT)
//         , "z", TF_H + 2*TILL_SLOPE*EXT_FRONT 
//         , "t", UNIT_COORD_TILL_BACK_DOWN*(LEN4+EXT_FRONT)
//         , "z", LARGE_SPACER+LEN1
//         , "x", -EXT_BACK
         
       ]   
           
  
  
  );
  
  if(markpts)
  {
    Red() MarkPts( slice(pts, 0, 13) );
    Black()MarkPts( slice(pts, 12, 20), countbeg=12 );
    Blue()MarkPts( slice(pts, 20, 28), countbeg=20 );
    Green()MarkPts( slice(pts, 28), countbeg=28 );
  }
  
  if(title)
  {
  title_site= [ onlinePt( [pts[32], pts[36]], len=4)
              , pts[39]
              , pts[32]
              ];
  //MarkPts(title_site);                            
  Text("SideBarGuide", site= title_site, color="blue",
       scale=0.6,actions=["rotx", 180, "y", -.5, "x",2]);   
  }
  
  if(dim)
  Blue()
  {
  Dim( [ 1,2,9,10,38], pts=pts, Label=["actions", ["rotz", 180] ] );
  //Dim( [29,30,37,38, onlinePt( [pts[10],pts[38]], ratio=2 )] ,pts=pts, spacer= -.5 );
  Dim([ 22,23,33], pts=pts);
  Dim( [0,11,29], pts=pts);
  
  Dim( [ 37, 36, 26] , pts=pts);
  Dim( [39, 38, [ EXT_FRONT, LEN4+LEN1, LEN4+LEN1] , 3], pts=pts );
  Dim( [ 11,39, 1] , pts=pts);
  Dim( [ 18,21,22] , pts=pts, spacer=1);
  
  Dim( [ [LEN4, LEN4+LEN1, LEN4+LEN1], 36, 31, [LEN4, LEN4+LEN1, -LEN1], 1] , pts=pts);
  Dim( [ 3,8,32] , pts=pts, Label=["actions",["rotz", 180]] );
  
  }

  Blue(0.3)DrawEdges( pts=pts, faces= guide_fs, r=0.015 );
  
  Teal(.2)polyhedron(pts,guide_fs);
}

module Assembly()
{
  tr= 0.6; 
  r = 0.05;
  
            FrontBar( transparancy=tr, line_r=r );
  Z(LEN4)   FrontBar( transparancy=tr, line_r=r );
  Z(LEN4*2) FrontBar( transparancy=tr, line_r=r );

  Z(-LEN4/2)X(-LEN4)SideBar_TopHalf( transparancy=tr, line_r=r );
  Z(LEN4/2)         SideBar( transparancy=tr, line_r=r );
  Z(LEN4/2+LEN4)    SideBar( transparancy=tr, line_r=r );
  Z(LEN4/2+LEN4*2)  SideBar_BottomHalf( transparancy=tr, line_r=r );

}  
  
Assembly();
//FrontBarGuide();
//SideBarGuide();
//SideBar_TopHalf(title=true);
//SideBar_BottomHalf(title=true);








