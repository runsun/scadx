

  
// Actual lengths in inches
LEN1= 0.75;
LEN2= 1.5;
LEN4= 3.75;
LEN3= LEN2+(LEN4-LEN2)/2; 
LEN6= 5.4215;
LEN8= 7.25;
  
FT6= 72;
FT8= 96;
FT10= 120;

PLY1= 0.225;
PLY2= 0.5;

WOODLENS= [undef,LEN1,LEN2,LEN3,LEN4,undef,LEN6,undef,LEN8];

TABLESAW_KERF= 0.111; // inch, close to 7/64

1x2x8 = [LEN1, LEN2, FT8]; 
2x2x8 = [LEN2, LEN2, FT8]; 
2x4x8 = [LEN2, LEN4, FT8]; 
4x4x8 = [LEN4, LEN4, FT8]; 
//2/2x4x8 = [LEN2/2, LEN4, FT8]; 
//2x4/2x8 = [LEN2, LEN4/2, FT8]; 
  
WOOD_SIZE_HASH = 
  [ "2", LEN2
  , "4", LEN4
  , "6", LEN6
  , "8", LEN8
  , "2/2", LEN1
  , "4/2", LEN4/2
  , "6/2", LEN6/2
  , "8/2", LEN8/2
  , "B1x2x8",[LEN1, LEN2, FT8]
  , "B2x2x8",[LEN2, LEN2, FT8]
  , "B2x4x8",[LEN2, LEN4, FT8]
  , "B4x4x8",[LEN4, LEN4, FT8]
  , "B2x4/2x8",[LEN2, LEN4/2, FT8]
  , "B2/2x4x8",[LEN2/2, LEN4, FT8]
  , "B2/2x4/2x8",[LEN2/2, LEN4/2, FT8]
  ];  
  
wood=
[
  // Notes: Lowes sell 2 brands of cedar pickets, white-tag and blue-tag
  "cedPkt_1x6_Lb", [0.603, 5.53, FT6-1,"cedar_picket_1x6_Lowes_blue_tag"] // -1: corner cut of picket
, "cedPkt_1x6_Lw", [0.56, 5.447, FT6-1,"cedar_picket_1x6_Lowes_white_tag"] 
, "ced_1x2x8_L", [0.7135, 1.5420, FT8,"cedar_1x2x8_Lowes"]   

];
