include <scadx.scad>

d=1.5; // w and h of a 2x2
2d= 2*d;
4d= 4*d;

module 22L(l=5){ cube([d,d,l]);}
module 2L2(l=5){ cube([d,l,d]);}
module L22(l=5){ cube([l,d,d]);}
module 12L(l=5){ cube([d/2,d,l]);}
module 1L2(l=5){ cube([d/2,l,d]);}
module 21L(l=5){ cube([d,d/2,l]);}
module 2L1(l=5){ cube([d,l,d/2]);}
module L12(l=5){ cube([l,d/2,d]);}
module L21(l=5){ cube([l,d,d/2]);}
module 42L(l=5){ cube([2d,d,l]);}
module 4L2(l=5){ cube([2d,l,d]);}
module 24L(l=5){ cube([d,2d,l]);}
module 2L4(l=5){ cube([d,l,2d]);}
module L42(l=5){ cube([l,2d,d]);}
module L24(l=5){ cube([l,d,2d]);}

module AngleAluminum( l=36, placement="in" 
                    , w=1.5, th=1/8, pqr=undef ){
          
  pts1= [ [0,0,0], [0,w,0],[th,w,0]
        , [th,th,0], [w,th,0],[w,0,0]]; 
                        
  pts2 = addz( pts1, l );
                        
  _pts = concat( pts1, pts2 );
  
  pts = placement=="out"? [for(p=_pts)p+[ -th,-th,0]]:_pts;
                        
  //MarkPts( pts, "l");
  faces = faces("rod", nside=6);
  //echo(faces = faces );
            
  color("lightgray",1)                      
  if(pqr)
      Move( to=pqr)                      
      polyhedron( points = pts, faces= faces); 
  else 
      polyhedron( points = pts, faces= faces);  
 
  echo( _color(_s("<b>AngleAluminum</b>: <u>{_} x {_} x {_}</u>"
       ,[w,th,l]),"gray")); 
}    
module AngleAluminum_demo(){
    pqr=pqr();
    MarkPts( pqr, "ch;l=PQR");
    AngleAluminum(pqr=pqr,l=0.5, placement="out");
}
//AngleAluminum_demo();

module L_angle( size=3, w=d, th=1/8, pqr=undef, placement="in" ){
       
  pts1= [ [0,0,0], [0,size,0],[w,size,0]
        , [w,w,0], [size,w,0],[size,0,0]]; 
                        
  pts2 = addz( pts1, th );
                        
  _pts = concat( pts1, pts2 );
  
  pts = placement=="out"? [for(p=_pts)p+[ -w,-w,0]]:_pts;
                        
  //MarkPts( pts, "l");
  faces = faces("rod", nside=6);
  //echo(faces = faces );
            
  color("lightgray",1)                      
  if(pqr)
      Move(to=pqr)                      
      polyhedron( points = pts, faces= faces); 
  else 
      polyhedron( points = pts, faces= faces);
  
  echo( _color(_s("<b>L_angle</b>: <u>{_} x {_} x {_}</u>"
       ,[w,th,size]),"gray")); 
}

module L_angle_demo(){
    pqr=pqr();
    MarkPts( pqr, "ch;l=PQR");
    L_angle(pqr=pqr,placement="out");
}
//L_angle_demo();

module Stud( h=2d, w=4d, l=12, pqr=undef
           , color=undef, transp=0.8){
       
  //h = w2;//min( w, w2);
  //w = max( w, w2);   
  pts1= [ [0,0,0], [0,0,h],[0,w,h], [0,w,0]
        ];               
  pts2 = addx( pts1, l );
                        
  pts = concat( pts1, pts2 );
  
  //pts = placement=="out"? [for(p=_pts)p+[ -w,-w,0]]:_pts;
                        
  //MarkPts( pts, "l");
  faces = faces("rod", nside=4);
  //echo(faces = faces );
            
  color(color, transp)                      
  if(pqr)
      Move(to=pqr)                      
      polyhedron( points = pts, faces= faces); 
  else 
      polyhedron( points = pts, faces= faces);

    echo( _color(_s("<b>Stud</b>: <u>{_} x {_} x {_}</u>"
       ,[ (w==2?w:h)/d,(w==2?h:w)/d,l]),"darkkhaki"));   
}

module Stud_demo(){
    pqr=pqr();
    MarkPts( pqr, "ch;l=PQR");
    Stud(pqr=pqr);
}
//Stud_demo();


module Board( l=5*d, w=4d, th=1/4, pqr=undef
           , color=undef, transp=0.8){
       
  //h = w2;//min( w, w2);
  //w = max( w, w2);   
  pts1= [ [0,0,0], [0,w,0],[0,w,th], [0,0,th]
        ];               
  pts2 = addx( pts1, l );
                        
  pts = concat( pts1, pts2 );
  
  //pts = placement=="out"? [for(p=_pts)p+[ -w,-w,0]]:_pts;
                        
  //MarkPts( pts, "l");
  faces = faces("rod", nside=4);
  //echo(faces = faces );
            
  color(color, transp)                      
  if(pqr)
      Move(to=pqr)                      
      polyhedron( points = pts, faces= faces); 
  else 
      polyhedron( points = pts, faces= faces);

    echo( _color(_s("<b>Board</b>: <u>{_} x {_} x {_}</u>"
       ,[ l,w,th]),"darkkhaki"));   
}





module Saw_guard(){
   
   mv(-8,0,-2d) Stud( 2d,4d,20, transp=0.5);
   mv(-8,0,-4d) Stud( 2d,4d,20, transp=0.5);

   module oneside(){ 
     mvy(4d) rotx(90) AngleAluminum(l=4d ); 
     mvyz(4d,-7*d)  AngleAluminum(l=8*d);
     //mirror([0,1,0]) mvyz(4d,-7*d)  AngleAluminum(l=8*d);
     mirror([0,1,0]) mvz(-7*d)  AngleAluminum(l=8*d);
   }
    
   oneside();
   mvx(-0.1) mirror() oneside(); 
   //mvy(15) mvx(-0.1) rotx(90) rotz(90) AngleAluminum( ); 
   

}
//Saw_guard();
