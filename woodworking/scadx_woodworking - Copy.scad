

///. Constants 
  
// Actual lengths in inches
LEN1= 0.75;
LEN2= 1.5;
LEN4= 3.75;
LEN3= LEN2+(LEN4-LEN2)/2; 
LEN6= 5.4215;
LEN8= 7.25;
  
FT6= 72;
FT8= 96;
FT10= 120;

PLY1= 0.225;
PLY2= 0.5;

WOODLENS= [undef,LEN1,LEN2,LEN3,LEN4,undef,LEN6,undef,LEN8];

TABLESAW_KERF= 0.111; // inch, close to 7/64

1x2x8 = [LEN1, LEN2, FT8]; 
2x2x8 = [LEN2, LEN2, FT8]; 
2x4x8 = [LEN2, LEN4, FT8]; 
4x4x8 = [LEN4, LEN4, FT8]; 
//2/2x4x8 = [LEN2/2, LEN4, FT8]; 
//2x4/2x8 = [LEN2, LEN4/2, FT8]; 
  
WOODS = 
  [ "2", LEN2
  , "4", LEN4
  , "6", LEN6
  , "8", LEN8
  , "2/2", LEN1
  , "4/2", LEN4/2
  , "6/2", LEN6/2
  , "8/2", LEN8/2
  , "B1x2x8",[LEN1, LEN2, FT8]
  , "B2x2x8",[LEN2, LEN2, FT8]
  , "B2x4x8",[LEN2, LEN4, FT8]
  , "B4x4x8",[LEN4, LEN4, FT8]
  , "B2x4/2x8",[LEN2, LEN4/2, FT8]
  , "B2/2x4x8",[LEN2/2, LEN4, FT8]
  , "B2/2x4/2x8",[LEN2/2, LEN4/2, FT8]

  // Notes: Lowes sell 2 brands of cedar pickets, white-tag and blue-tag
, "cedPkt_1x6_Lb", [0.603, 5.53, FT6-1,"cedar_picket_1x6_Lowes_blue_tag"] // -1: corner cut of picket
, "cedPkt_1x6_Lw", [0.56, 5.447, FT6-1,"cedar_picket_1x6_Lowes_white_tag"] 
, "ced_1x2x8_L", [0.7135, 1.5420, FT8,"cedar_1x2x8_Lowes"]   

];


// Width of saw blade in inch
KERF= 0.111; 

PATIO_COLUMN_WIDTH= 16; // patio column width          
PATIO_FLOOR_WIDTH = 135.5 + PATIO_COLUMN_WIDTH*2; // in inches 
PATIO_FLOOR_DEPTH = 120;
PATIO_FLOOR_HEIGHT = 5;


///. Cutlist function
function cutlist_for_1_mat_187h( 
	  parts 
        , mat = "B2x4x8" // Process only the parts that use this material
                         // It could be a string as one of the key in WOODS
                         // from which the size is obtain, or a customized size [x,y,z] 
                          
        , avail_lens= [] // lens of the avail_mats (available materials). 
                         // If given, these avail_mats will be used first.
                         // New material of full size (defined by mat) are 
                         // added if avail_mats are not enough. For example, let
                         // 
                         //  mat= "B2x4x8" and availabled_lens= [50,60] 
                         //
                         // means we have "B2x4x8" in 50 and 60 inches
                         // if we don't have any avail_mat, leave it [].
                         //
                                               
        , spacer = 1 // Define the space between two parts.  
        
        , drawparts   // Names of parts to be drawn. If undef, use the keys in parts.
        
        , _matsize    // get matsize from hash(WOODS, mat); if not 
                      // found, get size [x,y,z] from the mat that is in xxyxz form
                      // or in [x,y,z] form    
        , _rtn  = []
        , debug=0
        , _i=-1 
        )=
(  /*
      Attempt to find a better cut fit of parts using ONE specific type of material.  
      Mainly for use in DrawCutlist()

   parts: { partname:part } where a part is a hash:
     
    [ "mat" , <matname> like "B2x4x8". Could also be customized [x,y,z]
    
    , "len",  <len of wanted> like 36. Could be undef. see below. 
                       
    , "size", <size of wanted> like [36,LEN4,LEN2] --- the wanted size is determined
                                                       by either the len or size 
                                                       When len given, use it with the
                                                       other 2 measures (y,z) on mat, like 
                                                       [len, LEN4, LEN]. Otherwise, use
                                                       size = [36, LEN4/2, LEN2], which
                                                       means the mat's width, LEN4, is
                                                       cut in halves.   
                                                       
    , "count", 6   --- Count of copies. Note: this is to determine the output of 
                       cutlist, nothing to do w/ how many parts are actually drawn 
                       (which is determined by a drawing module separately).
                           
    , "color", ["teal",isCutlist?.5:1]
    , "actions", ["roty",-90, "y",-os] 
    
                 --- actions to bring the part from mat-position lying 
                     on the XY plane) to its init-position. Its final 
                     opsition requires additinal actions defined by
                     individual drawing module.
                                                    
                             actions          actions in its
                              here            drawing mod
                     mat-pos ------> init-pos -------------> final-pos
                                            
    , "decos", [.... ]  --- data used to change the pts.                                                               
    ]
    
   Return:
              mat_len 
        mat      |  avail_len
         |       |   |
    [ ["B2x4x8", 96, 9, ["TopBarSide", 21], ["BotBarShort", 18.75]
                      , ["BotBarShort", 18.75], ["BotBarShort", 18.75], ["CBar", 6]]
    , ["B2x4x8", 96, 9, ["TopBarIn", 21], ["TopBarIn", 21], ["TopBarMid", 21]
                      , ["TopBarSide", 21]]
    , ["B2x4x8", 80, 7.25, ["TopBarLong", 72]]
    , ["B2x4x8", 60, 4.5, ["Inleg_corner", 27], ["Inleg_corner", 27]]
    , ...
    ] 
    
   */

   _i==-1? 
     let( 
        drawparts= drawparts==undef? keys(parts): drawparts
        ,_parts= [ for(kvs= hashkvs(parts))
                    if( (hash(kvs[1],"mat")==mat) ) 
                    [ kvs[0], hash(kvs[1],"len"), hash(kvs[1],"count", 1) ]
                      //=> [ <part_name>, <part_len>, <part_count> ]
                  ]  
        , sorted_parts = reverse( sortArrs(_parts, by=1) )
        
        , _matsize = isarr(mat)?mat
                     : hash(WOODS, mat) 
                     
        , matsize = reverse(                 // matsize => [ 96, LEN4, LEN2 ]   
                      _matsize? _matsize
                      : [for(a=split(mat,"x")) num(a) ]
                    )
                             
        , _mats = avail_lens?        // _mats => ["B2x4x8",60,60] or ["B2x4x8",96,96]   
                     [ for(len=avail_lens)
                        [ mat, len, len ] 
                     ]
                : [[ mat, matsize[0], matsize[0] ]]                                           
        )
     debug? 
       echo("")
       echo(_red("cutlist()=========="), _i=_i)
       echo("init: ")   
       //echo( parts = parts )
       //echo( _parts = _parts )
       //echo( sorted_parts = sorted_parts )
       echo( drawparts = drawparts )
       echo( mate = mat )
       echo( matsize = matsize )
       echo( avail_lens = avail_lens ) 
       echo( _mats = _mats ) 
       echo( _rtn = _mats )        
       cutlist_for_1_mat(parts=sorted_parts, matname=mat, _matsize=matsize, _rtn=_mats , spacer=spacer,_i=0, debug=debug) 
     : cutlist_for_1_mat(parts=sorted_parts, matname=mat, _matsize=matsize, _rtn=_mats , spacer=spacer,_i=0, debug=debug) 
        
   : _i>=200  // <================ Set max part count in case things go wrong 
     || parts == []?
     
     //=> sort by leftover lens then by avail_lens, so the shorter mats are
     //   listed in front.  
     sortArrs( sortArrs(_rtn,by=2), by=1)
     
   : let( part_nlc= parts[0]       //=> [<part_name>, <part_len>, <part_count> ]
        , plen  = part_nlc[1]
        , pcount = part_nlc[2]
        , mat   = _rtn[0]
        , m_stock_len= _matsize[0] // original len
        , m_avail_len = mat[2]     // leftover of the previous mat
        , neednew= (plen+spacer)> m_avail_len
        , __rtn = neednew? 
                 concat( [[ mat[0]                    //=> matname
                          , m_stock_len               
                          , m_stock_len-plen-spacer   //=> avail_len (leftover) for next run 
                          , [part_nlc[0], part_nlc[1]]  
                         ]]
                       , _rtn ) 
                 : replace(_rtn, 0
                          , concat( replace(mat, 2, m_avail_len-plen-spacer)
                                  , [ [part_nlc[0], part_nlc[1]] ])  
                          )
        , newrtn = len(__rtn)>1? reverse(sortArrs(__rtn,by=2))
                               : __rtn
        , newparts=  [ if( pcount>1 ) 
                           [ part_nlc[0], part_nlc[1], pcount-1]
                     , if( len(parts)>1 )
                         each [ for(i=[1:len(parts)-1]) parts[i] ] 
                     ] 
        )
     debug?
       echo( _red(str("=============== _i= ", _i) ))   
       echo( parts = parts )
       echo(part_nlc = part_nlc)
       echo(plen = plen)
       echo(mat=mat)
       echo(m_stock_len = m_stock_len)
       echo(m_avail_len = m_avail_len)
       echo( neednew = neednew?_red(neednew):_green(neednew) )    
       echo( _rtn = _rtn )   
       echo( __rtn = __rtn )   
       echo( newrtn = newrtn )
       echo( len_newrtn = len(newrtn) )
       cutlist_for_1_mat(parts=newparts, matname=mat, _matsize=_matsize, _rtn=newrtn, spacer=spacer,_i=_i+1, debug=debug)    
     : cutlist_for_1_mat(parts=newparts, matname=mat, _matsize=_matsize, _rtn=newrtn, spacer=spacer,_i=_i+1, debug=debug)    
         
);

function cutlist_for_1_mat( 
	  parts 
        , mat = "B2x4x8" //=> <matname> || [x,y,z]
                         // Process only the parts that use this material
                         // It could be a string as one of the key in WOODS
                         // from which the size is obtain, or a customized size [x,y,z] 
                          
        , avail_lens= [] //=> [] || [n,n2...]
                         // lens of the avail_mats (available materials). 
                         // If given, these avail_mats will be used first.
                         // New material of full size (defined by mat) are 
                         // added if avail_mats are not enough. For example, let
                         // 
                         //  mat= "B2x4x8" and availabled_lens= [50,60] 
                         //
                         // means we have "B2x4x8" in 50 and 60 inches
                         // if we don't have any avail_mat, leave it [].
                                               
        , spacer = 1 // Define the space between two parts.  
        
        , drawparts   // Names of parts to be drawn. If undef, use the keys in parts.
        
        , _matsize    // get matsize from hash(WOODS, mat); if not 
                      // found, get size [x,y,z] from the mat that is in xxyxz form
                      // or in [x,y,z] form    
        , _rtn  = []
        , debug=0
        , _i=-1 
        )=
(  /*
      Attempt to find a better cut fit of parts using ONE specific type of material.  
      Mainly for use in DrawCutlist()

   parts: { partname:part } where a part is a hash:
     
    [ "mat" , <matname> like "B2x4x8". Could also be customized [x,y,z]
    
    , "len",  <len of wanted> like 36. Could be undef. see below. 
                       
    , "size", <size of wanted> like [36,LEN4,LEN2] --- the wanted size is determined
                                                       by either the len or size 
                                                       When len given, use it with the
                                                       other 2 measures (y,z) on mat, like 
                                                       [len, LEN4, LEN]. Otherwise, use
                                                       size = [36, LEN4/2, LEN2], which
                                                       means the mat's width, LEN4, is
                                                       cut in halves.   
                                                       
    , "count", 6   --- Count of copies. Note: this is to determine the output of 
                       cutlist, nothing to do w/ how many parts are actually drawn 
                       (which is determined by a drawing module separately).
                           
    , "color", ["teal",isCutlist?.5:1]
    , "actions", ["roty",-90, "y",-os] 
    
                 --- actions to bring the part from mat-position lying 
                     on the XY plane) to its init-position. Its final 
                     opsition requires additinal actions defined by
                     individual drawing module.
                                                    
                             actions          actions in its
                              here            drawing mod
                     mat-pos ------> init-pos -------------> final-pos
                                            
    , "decos", [.... ]  --- data used to change the pts.                                                               
    ]
    
   Return:
              mat_len 
        mat      |  avail_len
         |       |   |
    [ ["B2x4x8", 96, 9, ["TopBarSide", 21], ["BotBarShort", 18.75]
                      , ["BotBarShort", 18.75], ["BotBarShort", 18.75], ["CBar", 6]]
    , ["B2x4x8", 96, 9, ["TopBarIn", 21], ["TopBarIn", 21], ["TopBarMid", 21]
                      , ["TopBarSide", 21]]
    , ["B2x4x8", 80, 7.25, ["TopBarLong", 72]]
    , ["B2x4x8", 60, 4.5, ["Inleg_corner", 27], ["Inleg_corner", 27]]
    , ...
    ] 
    
   */

   _i==-1? 
     let( 
          //partnames = keys(parts)
        //, partvals = vals(parts)
        //, drawparts= drawparts==undef? [ for(i=range(partnames))
                                          //isstr( partnames[i])?
                                          //[partnames,[1,1]] 
                                         //]
        drawparts= drawparts==undef? keys(parts): drawparts
        //,_parts= [ for(kvs= hashkvs(parts))
        ,_parts= [ for( pn = share(keys(parts), drawparts) )
                    let( part = hash(parts, pn) )
                    if( (h( part,"mat")==mat) ) 
                    [ pn, hash(part,"len"), hash(part,"count", 1)
                            , hash(part,"matsize")  
                    ]
                      //=> [ <part_name>, <part_len>, <part_count>, <part_mat_size> ]
                  ]  
        , sorted_parts = reverse( sortArrs(_parts, by=1) ) // sorted by len
        
        , matsize = last(_parts[0])
        
        //, _matsize = hash(WOODS,mat, reverse(mat) )
                     //isarr(mat)?mat
                     //: hash(WOODS, mat) 
                     
        //, matsize = reverse(                 // matsize => [ 96, LEN4, LEN2 ]   
        //              _matsize? _matsize
        //              : [for(a=split(mat,"x")) num(a) ]
        //            )
                             
        , _rtn = avail_lens?        // _mats => ["B2x4x8",60,60] or ["B2x4x8",96,96]   
                     [ for(len=avail_lens)
                        [ mat, len, len ] 
                     ]
                : [[ mat, matsize[0], matsize[0] ]]                                           
        )
     debug? 
       echo("")
       echo(_red("cutlist()========== init, "), _i=_i)
       //echo( parts = parts )
       //echo( _parts = _parts )
       //echo( sorted_parts = sorted_parts )
       echo( drawparts = drawparts )
       echo( mate = mat )
       //echo( _matsize = _matsize )
       echo( matsize = matsize )
       echo( avail_lens = avail_lens ) 
       //echo( _mats = _mats ) 
       echo( _rtn = _rtn )        
       cutlist_for_1_mat(parts=sorted_parts, matname=mat, _matsize=matsize, _rtn=_rtn , spacer=spacer,_i=0, debug=debug) 
     : cutlist_for_1_mat(parts=sorted_parts, matname=mat, _matsize=matsize, _rtn=_rtn , spacer=spacer,_i=0, debug=debug) 
        
   : _i>=200  // <================ Set max part count in case things go wrong 
     || parts == []?
     
     //=> sort by leftover lens then by avail_lens, so the shorter mats are
     //   listed in front.  
     sortArrs( sortArrs(_rtn,by=2), by=1)
     
   : let( part_nlc= parts[0]       //=> [<part_name>, <part_len>, <part_count> ]
        , plen  = part_nlc[1]
        , pcount = part_nlc[2]
        , mat   = _rtn[0]
        , m_stock_len= _matsize[0] // original len
        , m_avail_len = mat[2]     // leftover of the previous mat
        , neednew= (plen+spacer)> m_avail_len
        , iLastFit = len([ for( mi= range(_rtn))          
                           if( (plen+spacer)<= _rtn[mi][2])1 ])-1                                
                    //=> iLastFit = i of the mat w/ the shortest leftover 
                    //   that fits. This mat will be the one to be filled so
                    //   when neednew is not true, it fills this mat first but
                    //   not the one w/ the longest leftover (i.e., _rtn[0]) 
                     
        , __rtn = neednew? 
                 concat( [[ mat[0]                    //=> matname
                          , m_stock_len               
                          , m_stock_len-plen-spacer   //=> avail_len (leftover) for next run 
                          , [part_nlc[0], part_nlc[1]]  
                         ]]
                       , _rtn ) 
                 : replace(_rtn, iLastFit
                          , concat( replace(_rtn[iLastFit], 2
                                          , _rtn[iLastFit][2]-plen-spacer)
                                  , [ [part_nlc[0], part_nlc[1]] ])  
                          )
//                 : replace(_rtn, 0
//                          , concat( replace(mat, 2, m_avail_len-plen-spacer)
//                                  , [ [part_nlc[0], part_nlc[1]] ])  
//                          )
        , newrtn = len(__rtn)>1? reverse(sortArrs(__rtn,by=2))
                               : __rtn
        , newparts=  [ if( pcount>1 ) 
                           [ part_nlc[0], part_nlc[1], pcount-1]
                     , if( len(parts)>1 )
                         each [ for(i=[1:len(parts)-1]) parts[i] ] 
                     ] 
        )
     debug?
       echo( _red(str("=============== _i= ", _i) ))   
       echo( parts = parts )
       echo(part_nlc = part_nlc)
       echo(plen = plen)
       echo(mat=mat)
       echo( iLastFit= iLastFit )
       echo(m_stock_len = m_stock_len)
       echo(m_avail_len = m_avail_len)
       echo( neednew = neednew?_red(neednew):_green(neednew) )    
       echo( _rtn = _rtn )   
       echo( __rtn = __rtn )   
       echo( newrtn = newrtn )
       echo( len_newrtn = len(newrtn) )
       cutlist_for_1_mat(parts=newparts, matname=mat, _matsize=_matsize, _rtn=newrtn, spacer=spacer,_i=_i+1, debug=debug)    
     : cutlist_for_1_mat(parts=newparts, matname=mat, _matsize=_matsize, _rtn=newrtn, spacer=spacer,_i=_i+1, debug=debug)    
         
);

module cutlist_test()
{
  echom("cutlist_test()");
  
  // Sample parts (Planter Bench):
  parts=[ ["Outleg", 23.25, 2]
        , ["Inleg_corner", 25.5, 4]
        , ["Inleg_middle", 22.6875, 2]
        // "<TopMainBars>:----------"
        , ["TopMainBars_long", 72, 2]
        , ["TopMainBars_side", 21, 2]
//        , ["TopMainBars_mid", 21, 1]
//        // "<TopInnerBars>:----------"
//        , ["TopInnerBars_short", 21, 14]
//        , ["TopInnerBars_support", 64.5, 2]
//        , ["<Bottom_bars>:----------"
//        , ["Bottom_bar_long", 72, 1]
//        , ["Bottom_bar_short", 18.75, 3]
//        // "<Bottom_bars>:----------"
//        , ["Corner_bar", 6, 14]
        ];
           
 cl = cutlist( parts = parts );
 
 echo(cl=cl);
}
//cutlist_test();


///. DrawCutlist module

module DrawCutlist_187a( cutlist=[]
                  , parts //= [ ["leg",28,4], ["hbar", 72,2, "red"] ]
                  , mats  = [ ["2x4x8",96] ]
                  , mat_spacer = 1 
                  , part_spacer = 1
                  )
{
   echom("DrawCutlist()");
   cutlist = cutlist? cutlist
             : cutlist(parts=parts,spacer=part_spacer);
   //echo(cutlist= _fmt(cutlist));
   //echo("");
   //echo( act_added = add_cutlist_actions(cutlist) );
   
   //  cutlist = [ ["2x4x8", 96, 9, ["leg", 28, "red"], ["leg", 28, "red"], ["leg", 28, "red"]]
   //            , ["2x4x8", 96, 23, ["hbar", 72, "blue"]]
   //            , ["2x4x8", 96, 23, ["hbar", 72, "blue"]]
   //            , ["2x4x8", 96, 67, ["leg", 28, "red"]]
   //            ]
   
   for( i = range(cutlist) )
   {

     Blue()
     MarkPts( [ [ -.5,  i*(LEN4+mat_spacer)+LEN4/2, LEN2 ]
              , [ 95.5,  i*(LEN4+mat_spacer)+LEN4*3/4, LEN2 ]
              ]
            , Line=0, Ball=0  
           ,  Label=["text", [ str("#",i+1, " (", mat[0], ")  " )
                             , str(mat[2], "in")
                             ] 
                   , "halign","right"
                   , "scale",5
                   , "followView", false ] 
           );
                
     mat = cutlist[i];
     _parts = slice(mat, 3);
     /* parts= [ ["Outleg", 24.75, 1, ["teal", 0.5]]
               , ["Inleg_middle", 24.1875, 2, ["teal", 0.5]]
               , ["Inleg_middle", 24.1875, 1, ["teal", 0.5]]
               ]
     */                   
         
     //echo(_green( _s("cutlist[{_}], parts={_}",[i, parts])));
     //if(parts)
     for( j = range(_parts) )
     {
        //echo(_blue( _s("   parts[{_}]: part= {_}, prelen={_}",[j,part, prelen] )));
        part = _parts[j];
        //echo(j=j, part=part);
        prelen = j==0?0
                  : sum( [ for( pp= slice( _parts,0,j) ) pp[1]+part_spacer ] );
        act = ["x",prelen, "y", i*(LEN4+mat_spacer)];
        
        part_pts = hashs(parts, [part[0],"pts"]);
        part_faces= hashs(parts, [part[0],"faces"]);
        //echo( j=j, partname=part[0], part=hash(parts, part[0]), part_pts=part_pts);
        //echo(i=i, j=j, partname = part[0], prelen=prelen, act=act);
        _ppts = part_pts? transPts(part_pts, actions=act)
                        : cubePts( [part[1], LEN4, LEN2], actions=act );
        ppts = part[4]?part[4]:_ppts;
        //echo(ppts=ppts);          
        
        DrawEdges( pts=ppts, faces=part_pts?part_faces:CUBEFACES, r=0.08 );
        
        Color( part[3]?part[3]:"gold")
        polyhedron( ppts, part_pts?part_faces:CUBEFACES );
        MarkCenter( get(ppts, [4,5,6,7])
                  , Label=["text", _s("{_} ({_})", part)
                          , "scale", 5, "followView",0
                          , "color", "black"
                          ] );
     }   
     
     matpts = cubePts(reverse( quicksort(2x4x8 ))
              , actions=[ "y", i*(LEN4+mat_spacer)] );
     DrawEdges( pts=matpts, faces=CUBEFACES , r=0.08);
     Blue(0.1)
     polyhedron( points=matpts, faces= CUBEFACES);      
   }
}                  

module DrawCutlist( 
      title
    , parts 
    , avail_mats=[]
         /* avail_mat_lens: Defined only when you have to use pre-cut wood 
         
            a hash of { <matname|matsize>: <len|lens> }
            avail_mats = [ 
                           "B2x4x8",[60,50]
                         , [96,LEN4,LEN2], 80 
                         ]
            => we have 3 pieces wood available:
                  
               "B2x4x8" 50-in              
               "B2x4x8" 60-in              
               [96,LEN4,LEN2] 80-in              
         */
    , drawopts          
    , mat_spacer = 1 
    , part_spacer = 1
    , debug=0
    )
{  
   /* 
    parts: { partname:part } where a part is a hash:
     
    [ "mat" , <matname> like "B2x4x8". Could also be customized [x,y,z]
    
    , "len",  <len of wanted> like 36. Could be undef. see below. 
                       
    , "size", <size of wanted> like [36,LEN4,LEN2] --- the wanted size is determined
                                                       by either the len or size 
                                                       When len given, use it with the
                                                       other 2 measures (y,z) on mat, like 
                                                       [len, LEN4, LEN]. Otherwise, use
                                                       size = [36, LEN4/2, LEN2], which
                                                       means the mat's width, LEN4, is
                                                       cut in halves.   
                                                       
    , "count", 6   --- Count of copies. Note: this is to determine the output of 
                       cutlist, nothing to do w/ how many parts are actually drawn 
                       (which is determined by a drawing module separately).
                           
    , "color", ["teal",isCutlist?.5:1]
    , "actions", ["roty",-90, "y",-os] 
    
                 --- actions to bring the part from mat-position lying 
                     on the XY plane) to its init-position. Its final 
                     opsition requires additinal actions defined by
                     individual drawing module.
                                                    
                             actions          actions in its
                              here            drawing mod
                     mat-pos ------> init-pos -------------> final-pos
                                            
    , "decos", [.... ]  --- data used to change the pts.                                                               
    ]
                    
   */
   echom("DrawCutlist()");
   
   parts= processParts( parts, drawopts );

   //
   // part_mats => the mats requested by parts
   //
   part_mats = uniq([for(p=vals(parts)) hash(p,"mat")] );
   echo( part_mats = part_mats );
      
   cutlist = [ each
               for( m = part_mats )                      // m=> <matname> || [x,y,z]
               let( avail_lens= h( avail_mats, m,[] ))
               cutlist_for_1_mat( parts= parts 
                                , mat  = m   
                                , avail_lens = //avail_lens==undef?undef
                                              len(avail_lens)>=-1?avail_lens:[avail_lens]
                                , drawopts=drawopts
                                , spacer = part_spacer
                                , debug=debug
                                )
             ];
             //    
             //    
             //                  | 
             //                  |
             // cutlist=> [ [<mat_name||[x,y,z]>,<mat_len>,<mat_leftover>
             //           , part1, part2 ... ]
             //           where part1, part2= [ <part_name>, <part_len> ]
             
   //cutlist = sortArrs( _cutlist, 1);                                          
   //_red("cutlist= ");
   //echo(cutlist = cutlist);
   //echo(arrprn(cutlist));
   //echo("");
   //echo( act_added = add_cutlist_actions(cutlist) );
   if(title)
   { Black()
     Text( str(title, ", part_spacer= ", part_spacer)
         , site= [ for(p=ORIGIN_SITE) p+[0,-5,0]]
         , scale=2
         );
   }
   //NAME=0;
   //LEN=1;
   //COUNT=2;
   
   for( i = range(cutlist) )
   {
     //echo(_red( _s("Loop thru cutlist, i={_}",i) ));
     //Blue()
     MarkPts( [ [ -.5,  i*(LEN4+mat_spacer)+LEN4/2, LEN2 ]
              , [ 95.5,  i*(LEN4+mat_spacer)+LEN4*3/4, LEN2 ]
              ]
            , Line=0, Ball=0  
           ,  Label=["text", [ str(" (", mat[0], ":", mat[1], ") ", "#",i+1, " " )
                             , str(mat[2], "in")
                             ] 
                   , "halign","right"
                   , "scale",8
                   , "color", COLORS[ index(part_mats, mat[0]) +1 ]
                   , "followView", false ] 
           );
     //echo(i=i, mat0=mat[0], color_i=index(part_mats, mat[0]), color= COLORS[ index(part_mats, mat[0]) ]);
                
     mat = cutlist[i]; //=> mat = [<matname>,<metlen>,<leftlen>, part, part ...]
                       //   part = [ <partname>, <partlen>, <partcount> ]  
                       //=> ["B2x4x8", 96, 1.5, ["TopBarLong", 72, 2], ["TopBarIn", 21, 1]]
     
     _matsize = hash(WOODS,mat[0], reverse(mat[0])); //=> [ LEN2, LEN4, 96 ];
     matsize = [ mat[1], _matsize[1], _matsize[0] ]; //=> [ 60, LEN4, LEN2 ];
     //echo(i=_red(i));
     //echo(mat = mat);
     //echo(_matsize = _matsize);
     //echo(matsize = matsize);
     _parts = slice(mat, 3);
     /*_parts= [ ["Outleg", 24.75, 1 ]
               , ["Inleg_middle", 24.1875, 2 ]
               , ["Inleg_middle", 24.1875, 1 ]
               ]
     */                   
     //echo( i = i, y = i*(LEN4+mat_spacer), leftover= mat[2] ) ;   
     //echo(_green( _s("cutlist[{_}], _parts={_}",[i, _parts])));
     //if(parts)
     for( j = range(_parts) )
     {
        //echo(_blue( _s("   parts[{_}]: part= {_}, prelen={_}",[j,part, prelen] )));
        part = _parts[j]; //=> part = [ <partname>, <partlen>, <partcount> ] 
        partname = part[0]; //hash(part, "partname" );
        plen     = part[1];
        pcount   = part[2];
        //echo(j=j, partname=partname, part=part, matsize= hashs(parts,[partname,"mat"]));
        //echo(j=j, part=part);
        
        part_pts  = hashs( parts, [partname, "pts"] );
        part_faces= hashs( parts, [partname, "faces"] );
        //echo( j=j, partname=part[0], part=hash(parts, part[0]), part_pts=part_pts);
        //echo(i=i, j=j, partname = part[0], prelen=prelen, act=act);
        
        prelen = j==0?0
                  : sum( [ for( pp= slice( _parts,0,j) ) pp[1]+part_spacer ] );
        act = ["x",prelen, "y", i*(LEN4+mat_spacer)];
        //echo(part_pts=part_pts);
        //echo(act=act);
        _ppts = part_pts? transPts(part_pts, actions=act)
                        : cubePts( [plen, LEN4, LEN2], actions=act );
        ppts = _ppts; //part[4]?part[4]:_ppts;
        //echo(ppts=ppts);          
        
        DrawEdges( pts=ppts, faces=part_pts?part_faces:CUBEFACES, r=0.08 );
        
        Color( hashs(parts, [partname, "color"], "gold" ))
        polyhedron( ppts, part_pts?part_faces:CUBEFACES );
        MarkCenter( get(ppts, [4,5,6,7])
                  , Label=["text", _s("{_} ({_})", [partname, plen])
                          , "scale", 5, "followView",0
                          , "color", "black"
                          ] );
     }   
     
     //matsize = hashs(parts, [partname,"mat"]); //(WOODS, mat[0]); 
     //matsize = part_mats[i];
     //echo(matsize = matsize);
     
     matpts = cubePts( matsize //[ mat[1], matsize[1], matsize[0] ]
              , actions=[ "y", i*(LEN4+mat_spacer)] );
     //matpts = cubePts(reverse( quicksort(2x4x8 ))
     //         , actions=[ "y", i*(LEN4+mat_spacer)] );
     DrawEdges( pts=matpts, faces=CUBEFACES , r=0.12);
     Gold(0.1)
     polyhedron( points=matpts, faces= CUBEFACES); 
     
          
   }

}                  


module DrawPart(partname, parts, actions, count, drawopts
               //, isdraw=1, isframe=1
               , debug_decos=0)
{   
  //echom(_s("DrawPart(\"{_}\")", partname ));
  log_b(_s("DrawPart(\"{_}\")", partname ), layer=2);
  log_(str("drawopts= ", drawopts), 2);
  //echo(drawopts=drawopts);
                                           
  _part = hash(parts, partname);
  part = hash(_part, "pts")? _part:
         processParts( parts=[partname, _part] //, isdraw=isdraw, isframe=isframe
                      , drawopt=drawopts)[1] ;

  //echo(_part = _part, isdraw=isdraw, isframe=isframe, drawopts=drawopts );
  log_(str("_part= ", _part ), layer=2) ;
  //echo( log_(str("part= ", part ), layer=2) );
  //echo(part = part );
  
  mat  = hash(part, "mat"); //=> <matname>||[x,y,z] like [96,LEN4,LEN2]
  matsize= hash(WOODS, mat, reverse(mat) ); 
          //=> material size like: [LEN2,LEN4,96]
  size = hash(part,"size", [ hash(part,"len"), matsize[1], matsize[0] ] ); 
          //=> stock size like: [LEN2,LEN4,60]
  
  //echo(mat=mat, sizes=sizes, size=size, part=part);

  act = h( part, "actions",[] ); //=> act to reach the final position
  //decos = h( part, "decos" );
      
  //_pts =cubePts( size, actions= concat( act?act:[], actions) );  
  //if(markpts)
  //  Red()MarkPts(_pts, Label=["text", range(_pts), "scale", 4]);           
  //echo(_pts=_pts);
//  pf= decos? decoPolyEdges(
//                  pts=_pts
//                , faces = CUBEFACES
//                , decos=decos
//                , debug = debug_decos
//                ): [_pts, CUBEFACES] ;
//  pts = pf[0];
//  faces = pf[1];
  
  pts = transPts(h(part, "pts"), concat( act?act:[], actions)) ;
  faces = h(part, "faces");
  
  ismarkpts=hash(part,"ismarkpts");
  if(ismarkpts)
  {
       MarkPts(pts//, Ball=0, Line=0
              ,Label= update( ["text", range(pts)
                              , "scale", 3
                              , "color","blue"]
                            , isarr(ismarkpts)?ismarkpts:[])
              ,opt= update( ["Ball",0, "Line",0]
                          , isarr(ismarkpts)?ismarkpts:[]
                          )              
              );   
  }
  
  dims = hash(part, "dims");
  if(dims)
  {
     Dims( pts, opts= dims );
  }
              
  if( hash(part, "isframe") )
    DrawEdges( [pts, faces], r=0.05);
    
  if( hash(part, "isdraw") )
    Color(hash(part,"color") ) polyhedron( pts, faces );
  
    
    
  if(count)
   //module _bom(name, mat, count)  // mat=["2x4x8", [L,LEN4,LEN2]]
  {
     echo(log_(
           str( _b( partname), " : "
              , mat, " : "
              , _blue( hash(part, "len" ))
              , " in"
              , count? _red(str(" X ", _b(count))):""
              )
          , layer=2 )
     );
  }
   //_bom( partname , size, count);
  log_e(layer=2); 
}

function processParts(parts //, isdraw=1, isframe=1
                     , drawopts)=
(
  // * Add pts and faces to parts if decos is defined; 
  // * Assure defined size and len 
  //
  // Note: actions is not considered. So the pts added are the
  //       part in its original position on the XY plane
  //       It is designed in such a way that the pts can either
  //       be used for drawing the final product or the cutlist
  //       (by applying different actions) 
  //echo(log_("processParts()"))
  [ each 
    //for(pn=share(keys(parts),drawparts)) // Use only parts given in drawparts 
    //for(pn= drawopts?share(keys(parts),keys(drawopts)):keys(parts)) // Use only parts given in drawparts 
    for(pn = keys(parts))
    let( p = hash(parts,pn)
       , opts= h(drawopts,pn,[]) 
       , decos = hash(p, "decos")
       , partmat= hash(p, "mat") //=> <mat_name> || [x,y,z]
       , matsize= reverse( h( WOODS, partmat, reverse(partmat)) ) //=> [x,y,z]
       //, matsize = isstr(_matsize)? reverse( hash(WOODS, _matsize) )
       //                           : _matsize 
                                  //, matsize = _matsize? _matsize
       //             :  
                      //, hash(p, "mat")
                      //)
       , partsize = hash(p,"size", [ hash(p,"len"), matsize[1], matsize[2] ] )
       , _pts = cubePts( partsize )
       , pf = decos? decoPolyEdges( pts=_pts, faces=CUBEFACES, decos= decos )
                     : [_pts, CUBEFACES] 
       )
    //echo("")
    echo(log_b("In processParts()", 3), partname=pn)
    //echo(_red("In processParts"), partname=pn)
   // echo( log_(_s("isdraw={_}, isframe={_}", [isdraw,isframe]) , 3))
    //echo( hash_drawopts_pn= hash(drawopts,pn) )
    //echo(pn=pn, size=size) //pts=pts)
    //echo(pn=pn, _matsize=_matsize, matsize= matsize, size=size)
    echo(log_(str("drawopts= ",drawopts), layer=3))
    echo(log_e(layer=3))   
     [pn, updates( ["name", pn]
                 , [p, [ "pts", pf[0], "faces", pf[1], "matsize", matsize
                       , "size", partsize, "len", partsize[0] 
                       , "isdraw", h(opts,"isdraw")//, isdraw ) 
                       , "isframe", h(opts,"isframe")//, isframe )
                       , "ismarkpts", h(opts, "ismarkpts", 0 )
                       , "dims", h(opts,"dims", [] )
                       
                       ]
                   ]
                 ) 
     ]  
   ]
);  