include <scadx.scad>
include <doctest.scad>




TICK_LEN_32 = 5;
TICK_WIDTH_32 = 2;
INTV_32 = 0.2;

tick_data=[
  "1/32", [1/32, TICK_LEN_32, TICK_WIDTH_32]   // [len, width]
, "1/16", [1/16, TICK_LEN_32*2, TICK_WIDTH_32]
, "1/8",  [1/8,  TICK_LEN_32*4, TICK_WIDTH_32]
, "1/4",  [1/4,  TICK_LEN_32*8, TICK_WIDTH_32]
, "1/2",  [1/2,  TICK_LEN_32*16, TICK_WIDTH_32*1.5]
, "1",    [1,    TICK_LEN_32*32, TICK_WIDTH_32*1.5]
];

Ruler();

module Ruler( opt= [ "len_in", 10 
                     , "smallest_tick", "1/32"
                     , "tick_data", tick_data
                     , "lines", ["0", "1/32", "top"]  // "0","1/32","1/16", ...,"top"
                     ] 
              )
{
   nIntvs = hash( opt, "len_in") / hash( tick_data, hash(opt, "smallest_tick"))[0];
   
   Color("black")
   for( intv= range(0,nIntvs) )
   {
     x = intv*INTV_32;
     y = hash(tick_data, hash(opt,"smallest_tick"))[1];
      
     echo([[ x, 0, 0], [ x, y, 0] ]);
      Line( [ [ x, 0, 0], [ x, y, 0] ] );       
   
   }



};