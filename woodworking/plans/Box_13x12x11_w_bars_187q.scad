include <../../scadx.scad>
include <../scadx_woodworking.scad>

ISLOG=0;
Box_13x12x11_enhanced_187q();  
 
module Box_13x12x11_enhanced_187q( // Add support bars
   
     isdraw=1
   , isframe=1  
   , offset=0    // distance to disassemble parts to show each part
   , isCutlist=0 // =1: Draw cutlist instead of product
   
   , avail_mats  // = ["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
                
   , cutlist_part_spacer=1 // the space seperates parts. This allows for considerations
                           // for saw-width, bad spot in lumber, etc. It's better to 
                           // print out cutlists of different part_spacer values
                              
   , drawopts // Overwrite the setting of global isdraw and isframe
              //=[ //"Side_L", ["isdraw",0] 
                 //, "Side_W", ["isdraw",0] 
                  //"Bottom", ["isdraw",1] 
                 //, "Vbar", ["isdraw",1] 
                 //, "Hbar", ["isdraw",1] 
                 //]               
   )
{
  echom("Box_13x12x11_enhanced_187q()");
  
  ////=====================================================
  //// Check WOODS definitions
  echo( WOODS= _fmth( WOODS ) );
  
  ////=====================================================
  //// Define Units
  
  pktsize = h(WOODS, "cedPkt_1x6_Lb"); 

  TH = pktsize[0];
  
  L = 13;
  W = pktsize[1]*2+TH*2; //12;
  H = 11;
  title= _s( "Box_13x12x11_enhanced ({_}x{_}x{_})",[L,W,H]); 
  
  BottomRaise = LEN2; // leave a space so the bottom is not
                   // in direct concact w/ the ground 
  
  
  //os = offset;
  
  ////=====================================================
  //// Echo Title to console 
  echo(_div( _b(title) , s="font-size:24px" ));
      
  ////=====================================================
  //// Main 
  if(isCutlist) 
     _Draw_cutlist(parts=parts, isdraw=isdraw, isframe=isframe, drawopts=drawopts);
  else {
    DrawParts( parts=parts, isdraw=isdraw, isframe=isframe, drawopts=drawopts );
  }
  
  ////=====================================================
  //// Define parts. 
  parts=  
  [
  
    "Side_L"
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", L
        , "color",["gold",.8]
        , "count", 4
        ]
    
  , "Side_W"
  
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", pktsize[1]*2 //W-TH*2
        , "color",["khaki",.8]
        , "count", 4
        ]    
        
  , "Bottom"
  
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", L-TH*2
        , "color",["olive",0.9]
        , "count", 2
        ]
        
  // enhanced w/ support bars:          
  
  , "Vbar"
  
      , [ "mat", "B1x2x8" 
        , "len", H-TH-BottomRaise
        , "color","teal"
        , "count", 4
        ]
  , "Hbar"
  
      , [ "mat", "B1x2x8" 
        , "len", W-TH*2
        , "color","teal"
        , "count", 3
        ]
  
  
  ];
  
  
  ////=====================================================
  //// Define modules to draw parts
   module DrawParts( parts, isdraw, isframe, drawopts=[])
  {
    //echom("DrawParts()");
    //echo(log_b("DrawParts()"), drawopts=drawopts);
    log_dps_b();
    log_dps(["parts2", parts2]);
    parts2= processParts( parts, isdraw, isframe, drawopts );
                      
    log_dps( ["isdraw", isdraw, "isframe", isframe
             , "drawopts", drawopts] );         
   
   VBars();
   HBar();
   Side_L();
   Side_W();
   Bottom();
   
   module Side_L()
   {
      DrawPart( partname="Side_L", parts=parts2
              , actions=[ "rotx", 90, "y", TH] );
      DrawPart( partname="Side_L", parts=parts2
              , actions=[ "rotx", 90, "y", TH, "z", pktsize[1]] );
   
      DrawPart( partname="Side_L", parts=parts2
              , actions=[ "rotx", 90, "y", TH*2+ pktsize[1]*2 ] );
      DrawPart( partname="Side_L", parts=parts2
              , actions=[ "rotx", 90, "y", TH*2+ pktsize[1]*2 , "z", pktsize[1]] );
   }
   
   module Side_W()
   {
      DrawPart( partname="Side_W", parts=parts2
              , actions=[ "rotx", 90, "rotz", 90, "y", TH] 
              );
      DrawPart( partname="Side_W", parts=parts2
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "z", pktsize[1]] 
              );
      DrawPart( partname="Side_W", parts=parts2
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "x", L-TH] 
              );
      DrawPart( partname="Side_W", parts=parts2
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "z", pktsize[1], "x", L-TH] 
              );
   }
   
   module Bottom()
   {
      DrawPart( partname="Bottom", parts=parts2
              , actions=[ "t", [TH,TH,BottomRaise] ] 
              );
      DrawPart( partname="Bottom", parts=parts2
              , actions=[ "t", [TH,TH+pktsize[1],BottomRaise] ] 
              );
   }
   
   module VBars()
   {
      DrawPart( partname="Vbar", parts=parts2
              , actions=[ "roty", -90 , "t", [TH+LEN1,TH,BottomRaise+TH] ] 
              );
      DrawPart( partname="Vbar", parts=parts2
              , actions=[ "roty", -90 , "t", [L-TH,TH,BottomRaise+TH] ] 
              );
      DrawPart( partname="Vbar", parts=parts2
              , actions=[ "roty", -90 , "t", [TH+LEN1,W-LEN2-TH,BottomRaise+TH] ] 
              );
      DrawPart( partname="Vbar", parts=parts2
              , actions=[ "roty", -90 , "t", [L-TH,W-TH-LEN2,BottomRaise+TH] ] 
              );
   }
   module HBar()
   {
      DrawPart( partname="Hbar", parts=parts2
              , actions=[ "rotx", 90, "rotz",90, "t", [TH,TH,0] ] 
              );
      DrawPart( partname="Hbar", parts=parts2
              , actions=[ "rotx", 90, "rotz",90, "t", [TH+(L-TH*2-LEN1*2)/2+LEN1/2,TH,0] ] 
              );
      DrawPart( partname="Hbar", parts=parts2
              , actions=[ "rotx", 90, "rotz",90, "t", [TH+L-TH*2-LEN1,TH,0] ] 
              );
   }
   
   
    log_dps_e();
    
  } //<= DrawParts()

  
  ////=====================================================
  //// Define modules to draw cutlist 
  
  module _Draw_cutlist(parts, isdraw, isframe, drawopts)
  {
    
    DrawCutlist( title= title //_s( "Box ( L{_}in x W{_}in x H{_}in )", [L,W,H])
                , parts=parts//2                 
                , avail_mats = avail_mats //["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
                , mat_spacer=1
                , part_spacer=cutlist_part_spacer 
                , isdraw=isdraw
                , isframe=isframe
                , drawopts= drawopts //keys(parts)
                , debug= 0
                );             
  }  
     
}
