include <../../scadx.scad>
include <../scadx_woodworking.scad>
echo("Box_13x12x11_187q.scad");

ISLOG=0; 
Box_13x12x11_187q();
   
module Box_13x12x11_187q( // A simple box
   
     isdraw=1
   , isframe=1  
   , islabel=1
   , ismarkpts=0
   
   , offset=0    // distance to disassemble parts to show each part
   , isCutlist=0 // =1: Draw cutlist instead of product
   
   , avail_mats  // = ["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
      
   , cutlist_mat_spacer = 1 // The space between materials            
   , cutlist_part_spacer=1 // The space seperates parts. This allows for considerations
                           // for saw-width, bad spot in lumber, etc. It's better to 
                           // print out cutlists of different part_spacer values
                              
   , drawopts // Overwrite the setting of global isdraw and isframe
              //= [   
                  //"Side_L", ["isdraw",1] 
                //, "Side_W", ["isdraw",1] 
                //]            
   )
{
  echom("Box_13x12x11_187q()");
  
  ////=====================================================
  //// Check WOODS definitions
  echo( WOODS= _fmth( WOODS ) );
  
  ////=====================================================
  //// Define Units
  
  L = 13;
  W = 12;
  H = 11;
  title= _s( "Box ({_}x{_}x{_})",[L,W,H]); 
  
  BottomRaise = 1; // leave a space so the bottom is not
                   // in direct concact w/ the ground 
  
  pktsize = h(WOODS, "cedPkt_1x6_Lb"); 
  TH = pktsize[0];
  
  //os = offset;
  
  ////=====================================================
  //// Echo Title to console 
  echo(_div( _b(title) , s="font-size:24px" ));
      
  
  ////=====================================================
  //// Define parts. 
  _parts=  
  [
  
    "Side_L"
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", L
        , "color","gold"
        , "count", 4
        ]
    
  , "Side_W"
  
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", pktsize[1]*2 //W-TH*2
        , "color","khaki"
        , "count", 4
        ]    
        
  , "Bottom"
  
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", L-TH*2
        , "color","olive"
        , "count", 2
        ]    
  
  ];
  
  ///=====================================================
  /// Update parts w/ other settings
  
  parts= processParts(_parts, isdraw, isframe, islabel, ismarkpts, drawopts);
  //echo( parts__ = _fmth(parts));
  ///=====================================================
  /// Main
  
  if(isCutlist) 
   DrawCutlist( title= title 
              , parts=parts                 
              , avail_mats = avail_mats 
              , mat_spacer=cutlist_mat_spacer
              , part_spacer=cutlist_part_spacer 
              , debug= 0
              );                  
  else {
    DrawParts( parts=parts );
  }

  ////=====================================================
  //// Define modules to draw parts
   module DrawParts( parts )
  {
    //echom("DrawParts()");
    //echo(log_b("DrawParts()"), drawopts=drawopts);
    log_dps_b();
    log_dps(["parts", parts]);
    //parts= processParts( parts, isdraw, isframe, drawopts );
                      
    log_dps( ["isdraw", isdraw, "isframe", isframe
             , "drawopts", drawopts] );         
   
   Side_L();
   Side_W();
   Bottom();
   
   module Side_L()
   {
      DrawPart( partname="Side_L", parts=parts
              , actions=[ "rotx", 90, "y", TH] );
      DrawPart( partname="Side_L", parts=parts
              , actions=[ "rotx", 90, "y", TH, "z", pktsize[1]] );
   
      DrawPart( partname="Side_L", parts=parts
              , actions=[ "rotx", 90, "y", TH*2+ pktsize[1]*2 ] );
      DrawPart( partname="Side_L", parts=parts
              , actions=[ "rotx", 90, "y", TH*2+ pktsize[1]*2 , "z", pktsize[1]] );
   }
   
   module Side_W()
   {
      DrawPart( partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH] 
              );
      DrawPart( partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "z", pktsize[1]] 
              );
      DrawPart( partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "x", L-TH] 
              );
      DrawPart( partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "z", pktsize[1], "x", L-TH] 
              );
   }
   
   module Bottom()
   {
      DrawPart( partname="Bottom", parts=parts
              , actions=[ "t", [TH,TH,BottomRaise] ] 
              );
      DrawPart( partname="Bottom", parts=parts
              , actions=[ "t", [TH,TH+pktsize[1],BottomRaise] ] 
              );
   
   
   }
   
    log_dps_e();
    
  } //<= DrawParts()
     
}
 