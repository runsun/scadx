include <../../scadx.scad>
include <../scadx_woodworking.scad>

ISLOG=0;
Planter_Bench_187u(isCutlist=0); 

module Planter_Bench_187u( 
     isdraw=1
   , Frame=true
   , Label=1
   //, ismarkpts=0
   
   , avail_mats = [ "B2x4x8", [60,70]     // material #1, len 60 and 70
                  , [96,LEN4/2,LEN2], 75  // material #2, len 75
                  ]
   , offset=0    // distance to disassemble parts to show each part
   
   , isCutlist= 1 // =1: Draw cutlist instead of product
   , cutlist_mat_spacer= 1
   , cutlist_part_spacer=1 
         // cutlist_part_spacer: 
         // The space seperates parts. This allows for considerations
         // for saw-width, bad spot in lumber, etc. It's better to 
         // print out cutlists of different part_spacer values   
   , drawopts  /// opts for individual part 
        //=[ 
          //"TopBarIn", ["isdraw",0]
         //, "CBar", ["isdraw",0] 
         //, "BarSupport", ["isdraw",0, "Frame",1, "ismarkpts",0] 
        // ]
   )
{
  echom("Planter_Bench_187u()");
  
  ///=====================================================
  /// Define frequently used units
  
  L = FT6;
  W = 24;
  H = 28.5;
  title= _s( "Planter_Bench ({_}x{_}x{_})",[L,W,H]); 
      
  nBar = 14; // # of short-bars on the top. They will be placed onto
             // the 2 sections seperated by TopBarMid 
  bar_w = LEN2/2; // w of the top short bars.
  bar_intv = (L-LEN2*3 - nBar* bar_w)/(nBar+2);
      
  os = offset;
  
  ///=====================================================
  /// Echo Title to console 
  
  echo(_div( _b(title) , s="font-size:24px" )); 
            
  ///=====================================================
  /// Define parts
  
  /*
    A part is a hash containing the following keys:
     
    [ "mat" , <matname> //=> like "B2x4x8". Could also be customized [x,y,z]
    
    , "len",  <len of wanted> //=> like 36. Could be undef. see below. 
                       
    , "size", <size of wanted> //=> like [36,LEN4,LEN2] 
               --- the wanted size is determined
                   by either the len or size above.
                   When len given, use it as the x-element
                   of size with the other 2 measures (y,z) on 
                   mat, like [len, LEN4, LEN]. Otherwise, use
                   size like: [36, LEN4/2, LEN2] (this case 
                   means the mat's width, LEN4, is cut in halves).   
                                                       
    , "count", 6 //=> Count of copies. Note: this is to determine the 
                      output of cutlist, nothing to do w/ how many parts 
                      are actually drawn (which is determined by a drawing 
                      module separately).
                           
    , "color", ["teal",isCutlist?.5:1]
    
    , "actions", ["roty",-90, "y",-os] 
                 //=> actions to bring the part from mat-position lying 
                     on the XY plane) to its init-position. Its final 
                     opsition requires additinal actions defined by
                     individual drawing module.
                                                    
                             actions          actions in its
                              here            drawing mod
                     mat-pos ------> init-pos -------------> final-pos
                                            
    , "decos", [.... ]  --- data used to change the pts.                                                               
    ]
  
  */   
  _parts=  
  [
    "Outleg", [ "mat" , "B2x4x8"
              , "len", H-LEN4 
              , "count", 6                        
              , "color", ["teal",isCutlist?.5:1]
              , "actions", ["roty",-90, "rotz",90, "t", [LEN4,LEN2-os,0]]
              ]
  , "Inleg_corner",  [ "mat" , "B2x4x8"
                     , "len",  H-LEN2
                     , "decos", [ [ [4,0]
                                  , [ [-LEN2,0] , [-LEN2,-LEN4], [0,-LEN4] ]
                                  ]
                                ]
                     , "count", 4
                     , "color", ["purple",isCutlist?.3:1]
                     , "actions", ["roty",-90, "rotz",-90,"y",LEN2, "z",LEN2]
                     ] 
                     
  , "Inleg_middle",[ "mat" , "B2x4x8"
                   , "len", H-LEN2-LEN4/4*3 
                   , "count", 2
                   , "color", ["purple",isCutlist?.3:1]
                   , "actions", ["rotx", -90, "roty",90
                               , "t",[L/2-LEN4/2+LEN4,LEN2,H-LEN2-LEN4/4*3+LEN2] ]
                   ]
                 
                   
  , "TopBarLong", [ "mat", "B2x4x8"
               , "len", L 
               , "count", 2 
               , "actions",["rotx",90, "t",[0,LEN2-os,H-LEN4+os]
                           ] 
               , "color", ["olive", isCutlist?.5:.8]
               ]
               
  , "TopBarSide", [ "mat", "B2x4x8"
            , "len", W-LEN2*2
            , "count", 2 
            , "actions",["rotx",90, "rotz",90, "t", [-os,LEN2, H-LEN4+os]] 
            , "color", ["olive", isCutlist?.5:.8]
            ]  
            
  , "TopBarMid", [ "mat", "B2x4x8"
           , "size", [W-LEN2*2, LEN4/2, LEN2]
           , "actions", [ "rotx",90, "rotz",90, "t",[L/2-LEN2/2, LEN2, H-LEN4/2+os*2] ]
           , "color", ["olive", isCutlist?.5:1]
            ]
  , "BarSupport", [ "mat", [96, LEN4/2, LEN2] //"B2x4x8"
               , "size", [L-LEN4*2, LEN4/2, LEN2]
               , "actions", ["rotx",90, "t", [LEN4, LEN2*2,H-LEN4+os]] 
               , "count", 2 
               , "color", "khaki"
               , "decos"
                  ,[ [ [1,5]
                   , [ [0,0], [0,-(L-LEN4*3)/2]  
                     , [-LEN4/4,-(L-LEN4*3)/2]  
                     , [-LEN4/4,-(L-LEN4*3)/2-LEN4]  
                     , [0,-(L-LEN4*3)/2-LEN4]  
                     ]
                   ]                 
                   ]     
               ]
  , "TopBarIn" , [ "mat", "B2x4x8"
              , "size", [W-LEN2*2, LEN4/2, LEN2/2]
              , "count", 4 
              , "actions", ["rotx",90, "rotz",90, "t", [0, LEN2, H-LEN4/2+os*2] ] 
              , "color", "khaki"
              ]
          
  , "BotBarLong", [ "mat", "B2x4x8"
                   , "len", L 
                   , "count", 1 
                   , "color", ["green",isCutlist?0.5:1]
                   , "actions", ["y", W-LEN4, "z", -os ]
                   , "decos"
                      ,[ [ [3,7]
                         , [ [0,0], [0,-L/2+LEN4/2]
                           , [-LEN2,-L/2+LEN4/2]
                           , [-LEN2,-LEN4-L/2+LEN4/2]
                           , [0,-LEN4-L/2+LEN4/2]
                           ]
                         ]  
                       , [ [6,2]
                         , [ [-LEN2,0]
                           , [-LEN2,-LEN4]
                           , [0,-LEN4]
                           ]
                         ]
                       , [ [3,7] ]
                       ]
                   ]
                   
  , "BotBarShort", [ "mat", //[96, LEN4,LEN2] 
                            "B2x4x8"
                    , "len", W-LEN4-LEN2 
                    , "count", 3 
                    , "actions", ["rotz", 90, "t",[LEN4,LEN2-os/2, -os] ] 
                    , "color", ["green",isCutlist?0.5:1]
                   ]
           
  , "CBar", [ "mat", //[96, LEN4,LEN2]     
                     "B2x4x8"
                 , "size", [6, LEN4/2, LEN2]
                 //, "len", 6 
                 , "count", 7
                 , "decos",[ [ [7,3], [[-LEN4/2,0],[0,-LEN4/2]] ] 
                           , [ [2,6] ]
                           ] 
                 , "color", "khaki"   
                 , "actions",[]        
                 ] 
  ];
  
  ///=====================================================
  /// Update parts w/ other settings
  
  parts= processParts(_parts, isdraw, Frame, Label, drawopts);
  
  ///=====================================================
  /// Main
  
  if(isCutlist) 
    DrawCutlist( title= title 
               , parts=parts                 
               , avail_mats = avail_mats 
               , mat_spacer= cutlist_mat_spacer
               , part_spacer= cutlist_part_spacer 
               , debug= 0
               );                  
  else {
    DrawParts( parts=parts );
  }

  ///=====================================================
  /// Draw parts module. It contains modules to draw
  /// individual parts. 
  
  module DrawParts( parts ) 
  {
    log_dps_b();
    
    Legs();
    TopOutBars();
    TopInBars();
    Bottom_bars();
    Corner_bars();


    module Legs() 
    {
      log_dps2_b("Legs()");
      log_dps2( str("sas=", sas) );
      log_dps2( str("sas2=", sas2) );
      
      sas = spreadActions( objlen=[LEN4,LEN2]
                         , spreadlen=[L,W],n=[3,2]);
      for(i = range(sas)) 
      { 
        act= sas[i];
        DrawPart( partname="Outleg", parts=parts
                , actions=sas[i]);        
      }
      
      sas2= spreadCornerActions([ X*L, ORIGIN, Y*W] );
      for(i=range(sas2))
        DrawPart( partname="Inleg_corner", parts=parts
                , actions=sas2[i]);
      
      DrawPart(  partname="Inleg_middle", parts=parts
                , drawopts=drawopts);
      DrawPart(  partname="Inleg_middle", parts=parts
                , actions=["mirror", [[0,W/2,0],ORIGIN]]);
                
      log_dps2_e("Legs()");
    }  
  
    module TopOutBars()
    {
      log_dps2_b("TopOutBars()");
      DrawPart(partname="TopBarLong", parts=parts, count=2);
      DrawPart(partname="TopBarLong", parts=parts, actions=["mirror", [W/2*Y,O]]); 
      DrawPart(partname="TopBarSide", parts=parts, count=2);
      DrawPart(partname="TopBarSide", parts=parts, actions=["mirror", [L/2*X,O]]);
      log_dps2_e("TopOutBars()");
    }  
  
    module TopInBars()
    {
      log_dps2_b("TopInBars()");
      DrawPart(partname="TopBarMid", parts=parts,  count=1);
      DrawPart(partname="BarSupport", parts=parts, count=2, markpts=0);   
      DrawPart(partname="BarSupport", parts=parts, actions=["y",W-LEN2*3]); 
        
      sas1= spreadActions(bar_w, L/2, nBar/2 +2);      
      for(i=[1:len(sas1)-2])
      { 
         DrawPart(partname="TopBarIn", parts=parts, actions= sas1[i], bar_w= bar_w );
         DrawPart(partname="TopBarIn", parts=parts, actions= concat( sas1[i], ["x", L/2])
                  , count=i== nBar/2? nBar:0 
                  , bar_w= bar_w
                  );
      }   
      
      log_dps2( ["bar_intv(in)", bar_intv], layer=LOG_DPS+1);
      log_dps2_e("TopInBars()");
    }  
  
    module Bottom_bars()
    {
      log_dps2_b("Bottom_bars()");
      DrawPart(partname="BotBarLong", parts=parts, count=1);
      sas = spreadActions( LEN4, L,3 ) ;
      for( i=range(sas)) 
        DrawPart(partname="BotBarShort", parts=parts,actions= sas[i], count= i==2? 3:0);     
      log_dps2_e(partname="Bottom_bars()");
    }
  
    module Corner_bars()
    {
      log_dps2_b("Corner_bars()");
      act= [ "rotx", 90, "roty", -45 
             , "t", [ 0, LEN2, H-LEN4-6/sqrt(2) ]
             ];
    
      Front();     
      translate(W*Y) mirror(Y) Front(print_count=14);                                   
    
 
      module Front_left()
      {    
        DrawPart(partname="CBar", parts=parts, actions=concat(act,["x",LEN4,"y",-os]), markpts=0); // "/"
        DrawPart(partname="CBar", parts=parts, actions=concat(act,["rotz",90, "t",[LEN2,LEN2*2,0]])); // "/"
        DrawPart(partname="CBar", parts=parts, actions=concat(act,["mirror"
                                           ,[X*((L-LEN4*3)/4+LEN4/2),O]
                                           , "y",-os
                                           ]));      // "\"
      }
      module Front(print_count)
      {
        Front_left();
        translate(L*X) mirror(X) Front_left();
        DrawPart(partname="CBar", parts=parts, actions=concat(act
                       ,["rotz",90
                        , "t",[LEN2+L/2-LEN2/2, LEN2*2,LEN4/2]])
                       , count=print_count); // "/"
      }
            
      log_dps2_e("Corner_bars()");
    }
    
    log_dps_e();
    
  } //<= DrawParts()

}
 