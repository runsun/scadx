/*
  kitchenIsland.scad

*/ 

include <../../scadx.scad>
include <../scadx_woodworking.scad>

DIM_OPT=[ "r",0.1 ];
DRAWEDGES_OPT = ["r", 0.05];   

KitchenShelf();
   
module KitchenShelf(draw=1, dim=1, markpts=0, BOM=0, drawEdges=1, $t=$t)
{
  Right_wall(draw=draw, dim=dim, markpts=markpts, drawEdges=drawEdges, BOM=BOM);
  Left_upper_wall(draw=draw, dim=dim, markpts=markpts, drawEdges=drawEdges, BOM=BOM);
  Left_lower_wall(draw=draw, dim=dim, markpts=markpts, drawEdges=drawEdges, BOM=BOM);
  Back_wall(dim=dim, draw=draw, markpts=markpts, drawEdges=drawEdges, BOM=BOM);
  
  Top(dim=dim, draw=draw, markpts=markpts, drawEdges=drawEdges, BOM=BOM);
  Layer3(dim=dim, draw=draw, markpts=markpts, drawEdges=drawEdges, BOM=BOM);
  Layer2(dim=0, draw=draw, markpts=markpts, drawEdges=drawEdges, BOM=BOM);
  Layer1(dim=dim, draw=draw, markpts=markpts, drawEdges=drawEdges, BOM=BOM);
  
  Counter_top(draw=draw && !BOM);
  
  /*
  ##########################################################################
        Set units
  ##########################################################################
  */

  FT6= 72;
  FT8= 96;
  FT10= 120;

  // Actual lengths in inches
  PLY1= 0.225;
  PLY2= 0.5;
  LEN1= 0.75;
  LEN2= 1.5;
  LEN4= 3.75;
  LEN3= LEN2+(LEN4-LEN2)/2; 
  LEN8= 7.25;
  
  isAnimate = 1; 
  
  //
  // Define frequently used units
  //
  
  //: constant 

  //w_top= 19+3/16;
  //w_bot= 19.75;
  //d_right= 12;
  //d_left = 25.5;
  //h = 71+5/16;
  //counter_h= 36.5;
  //counter_th= 1.5;
  //wallShelf_bottom_h = 53; // h of the wall_shelf bottom


  shelf_h = 71+5/16;
  shelf_y_upper = 19+3/16+1.5;
  shelf_y_lower = 19.75+1.5;
  shelf_y = shelf_y_lower;
  
  counter_z= 36.5;
  counter_th= 1.5;
  counter_x = 25.5;
  counter_l = 40;
  counter_offset = 1.75;
  wallshelf_bot_z = 53; // h of the wall_shelf bottom
  wallshelf_h = shelf_h- wallshelf_bot_z;
  wallshelf_x = 13.5;
  
  r_wall_th = PLY1;
  r_wall_x = 13.5;
  
  l_upper_wall_th = PLY1;
  l_upper_wall_x  = wallshelf_x;
  
  l_lower_wall_th = PLY2;
  l_lower_wall_h = counter_z;
  l_lower_wall_x = counter_x-6;
  
  back_wall_th = PLY2;
  back_wall_h = shelf_h-4.5+2;
  back_wall_y = shelf_y;
  
  //: modules 
  
  module Label(text, site, opt=[])
  { 
     Text( text, scale=3, site=site, color="gray",  opt=opt);
  }
  
  module Right_wall(dim=0, draw=0, markpts=0, drawEdges=1, BOM=0)
  {
    echom("Right_wall");
    y= shelf_y-PLY1;
    if(BOM)
    {
      Move( [ [0,y,0]
            , [0,y,70.81]
            , [1,y,70.81] ]
          , [ [1,y+6,,0]
            , [0,y+6,0]
            , [0,y+7,0]
            ]
          )
      Obj();
    }
    else Obj();
    
    module Obj()
    {
      pts=cubePts([ r_wall_x, r_wall_th, shelf_h-PLY2 ]
          , actions= ["y", y]
          );
      if(markpts) MarkPts(pts); 
      if(drawEdges) DrawEdges( [pts, rodfaces(4)]);                   
      Label( "Right_wall", site=get(pts,[1,5,4,0]));
      if(dim)
      Dims(pts, [ [[1,2,6] , ["Label",["actions",["rotx", -90,"rotz",90, "y",3]]]]
                , [6,7,3]
                , [7,3,2] ] );              
      if(draw) Darkkhaki() polyhedron( pts, rodfaces(4) );
    }     
  }  
  
  module Left_upper_wall(dim=0, draw=0,markpts=0,  drawEdges=1, BOM=0)
  {
    echom("Left_upper_wall");
    
    if(BOM)
    {
       translate( [0, 45,0] )
       RotFromTo( [0,0,1], [0,1,0] )
       translate( [0,0, -wallshelf_bot_z] )
       //Move( [O, [0,0,wallshelf_bot_z], [1,0,wallshelf_bot_z]]
       //    , ORIGIN_SITE  )
       Obj();
    } else Obj();
    
    module Obj()
    {
      pts=cubePts( [ l_upper_wall_x
                    , l_upper_wall_th
                    //, shelf_h-wallshelf_bot_z+4.5
                    ,(shelf_h-wallshelf_bot_z)+4.5
                    ] 
          , actions=[ "y", shelf_y_lower-shelf_y_upper
                    , "z", wallshelf_bot_z
                    ]    
          );
      if(markpts) MarkPts(pts); 
      if(drawEdges) DrawEdges( [pts, rodfaces(4)] );                   
      if(dim)
      Dims(pts, [ [[5,6,7] , ["Label",["actions",["rotx", 90,"rotz",-90, "y",3]]]]
                , [6,7,3]
                , [7,3,2] ] );              
        
      Label( "Left_upper_wall"
            , site= roll(get(pts, [0,1,5,4])) 
            , opt=["scale",0.6]
            );
      if(draw) polyhedron( pts, rodfaces(4) );  
    }   
  }
  
  module Left_lower_wall(dim=0, draw=0, markpts=0, drawEdges=1, BOM=0)
  {
    echom("Left_lower_wall");
    if(BOM)
    {
       translate( [55, 45,0] )
       RotFromTo( [0,1,0], [-1,0,0] )
       RotFromTo( [0,0,1], [0,1,0] )
       //translate( [0,0, -wallshelf_bot_z] )
       Obj();
    } else Obj();
    
    module Obj()
    {
      pts=cubePts( [ l_lower_wall_x
            , l_lower_wall_th
            , counter_z-counter_th] 
          , actions=["x", PLY1]
          );
      if(markpts) MarkPts(pts); 
      if(drawEdges) DrawEdges( [pts, rodfaces(4)] );                   
      if(dim)
      Dims(pts, [ [[1,2,6], ["Label",["actions",["rotx", -90,"rotz",90, "y",3]]]]
                , [6,7,3]
                , [7,3,2] ] );              
    
      Label( "Left_lower_wall"
            , site= roll(get(pts, [0,1,5,4]),-1) 
            , opt=["scale",0.7]
            );
          
      if(draw) Darkkhaki()polyhedron( pts, rodfaces(4) );     
    }
  }
  
  module Back_wall(dim=0, draw=0, markpts=0, drawEdges=1, BOM=0)
  {
    echom("Back_wall");
    //echo("In Back_wall, ", DIM_OPT=DIM_OPT);
    
    if(BOM)
    {
      //translate([71.31+4.5,0,0])
      Move( [O,[0,0,back_wall_h+4.5+2]
            , [0,1,back_wall_h+4.5+2]], ORIGIN_SITE)
      Obj();
    }
    else Obj();
    
    module Obj()
    { 
      pts=cubePts( [ back_wall_th, shelf_y-r_wall_th, back_wall_h] 
                 , actions=["z",4.5+2]);
      if(markpts) MarkPts(pts);
      cut= [ [ -LEN1, 0]
           , [-LEN1, -(shelf_h-wallshelf_bot_z)-4.5 ] 
           , [0, -(shelf_h-wallshelf_bot_z)-4.5 ] 
           ];
      pf = decoPolyEdge( pts
                        , rodfaces(4) 
                        , edge=[5,4]
                        , template= cut
                        );
     npts= pf[0];
     if(drawEdges) DrawEdges( pf );                   
     MarkPts( npts, Line=false );
     //Label( centerPt( get(pts,[0,3,4,7])), "Back_wall" );
   
     Label( "Back_wall"
          , site= get(npts, [0,4,7,3]) 
          );
   
     if(dim)
     Dims(npts, [ // top-width   
                     [5,6,1] 
                , // wood width 
                     [[7,6,4], ["Label",["actions",["rotx",90,"rotz",90,"y",3]]]]   
                , // top-half(cut) height
                     [[5,8,9], ["spacer", -3.5]] 
                , // cut width
                     [ [ npts[11]-npts[10]+npts[5],5,1] 
                     , [ "GuideQ",false, "Label",["actions",["y",2]]]
                     ]  
                , // bot-half(uncut) height
                     [[9,1,3],["Label",["actions",["rotx",180]]]]  
                , // total height 
                     [6,2,5]   
                , // bot-width
                     [[0,3,7],["Label",["actions",["rotx",180]]]] 
                ] );//, opt=dim_opt);
     if(draw) Darkkhaki() Poly( pf );   
   }
}
  
  module Counter_top(draw=1)
  {
    echom("Counter_top");
    if(draw)Obj();
    
    module Obj()
    {
       pts= cubePts([ counter_x, counter_l, counter_th]
                   , actions= [ "t"
                      , [ 0
                        , -counter_l+counter_offset
                        , counter_z-counter_th
                        ]]
                   );
      echo(pts=pts);             
      //MarkPts(pts); 
      Label( "Counter_top", site=get(pts,[7,4,5,6]));
             
      if(draw) 
      color("silver")
      polyhedron( pts, rodfaces(4) );
    }           
                      
  }
  
  
  module Top(dim=0, draw=0, markpts=0, drawEdges=1, BOM=0)
  {
    
    echom("Top");
    
    if(BOM)
    {
      translate([72,(l_upper_wall_x-PLY2)*3+2,-(shelf_h-PLY2)])
      rotate(axis=Z, a=-90)
      Obj();
    }
    else Obj();  
  
    module Obj()
    {
      pts=cubePts( [ l_upper_wall_x-PLY2
                    , back_wall_y-LEN1
                    , PLY2] 
          , actions=[ "x", PLY2 
                    , "y", LEN1 //shelf_y_lower-shelf_y_upper
                    , "z", shelf_h-PLY2]    
          );
     if(markpts) MarkPts(pts); 
        if(drawEdges) DrawEdges( [pts, rodfaces(4)] );                   
        if(dim)
        Dims(pts, [ [[5,6,7] , ["Label",["actions",["rotx", 90,"rotz",-90, "y",3]]]]
                  , [6,7,3]
                  , [7,3,2] ] );              
           
     Label( "Top"
              , site= get(pts, [7,4,5,6]) 
              , opt=["scale",0.6]
              );
     if(draw)  color("teal") polyhedron( pts, rodfaces(4) );     
    }
  
  
  }

  
  module Layer3(dim=0, draw=0, markpts=0, drawEdges=1, BOM=0)
  {
    
    echom("Layer3");
    
    if(BOM)
    {
      translate([72,(l_upper_wall_x-PLY2)*2+1,-wallshelf_bot_z])
      rotate(axis=Z, a=-90)
      Obj();
    }
    else Obj();  
  
    module Obj()
    {
      pts=cubePts( [ l_upper_wall_x-PLY2
                    , back_wall_y-LEN1-PLY1
                    , PLY2] 
          , actions=[ "x", PLY2 
                    , "y", LEN1 //shelf_y_lower-shelf_y_upper
                    , "z", wallshelf_bot_z]    
          );
     if(markpts) MarkPts(pts); 
        if(drawEdges) DrawEdges( [pts, rodfaces(4)] );                   
        if(dim)
        Dims(pts, [ [[5,6,7] , ["Label",["actions",["rotx", 90,"rotz",-90, "y",3]]]]
                  , [6,7,3]
                  , [7,3,2] ] );              
           
     Label( "Layer3"
              , site= get(pts, [7,4,5,6]) 
              , opt=["scale",0.6]
              );
     if(draw)  color("teal") polyhedron( pts, rodfaces(4) );     
    }
  
  
  }

  module Layer2(dim=0, draw=0, markpts=0, drawEdges=1, BOM=0)
  {
    
    echom("Layer2");
    
    if(BOM)
    {
      //translate([71.31,0,0])
      //Move( [ORIGIN,[0,0,80], [0,40,120]], ORIGIN_SITE)
      translate( [58,46,-counter_z-counter_th] )
      Obj();
    }
    else Obj();  
  
    module Obj()
    {
      pts=cubePts( [  counter_x-LEN1-PLY2 //l_upper_wall_x-PLY2
                    , back_wall_y-counter_offset-PLY1
                    , counter_th] 
          , actions=[ "x", PLY2 
                    , "y", counter_offset
                    , "z", counter_z-counter_th]    
          );
     
     arcpts =[ for(i=[0:5]) anglePt([[0,1,0],ORIGIN,[1,0,0]], len=3, a= i*90/5) ];
     arcpts =arcPts([[0,1,0],ORIGIN,[1,0,0]], rad=3, a= 90, n=6) ;
     cut=  addx( [ for (p=arcpts)
                   p+[-3,-3,0] ]);
     //MarkPts(cut);                   
     //Plane( concat( [ORIGIN], cut) );
     
      pf = decoPolyEdge( pts
                        , rodfaces(4) 
                        , edge=[3,7]
                        , template= cut
                        );
     npts= pf[0];  
     //echo( arcpts = arcpts );
     //echo(cut=cut);   
     //echo(npts=npts);  
     //echo(faces=pf[1]);   
     if(markpts) MarkPts(npts); 
     
        if(drawEdges) DrawEdges( pf);//[pts, rodfaces(4)] );                   
        if(dim)
        Dims(npts, [ [[5,6,7] , ["Label",["actions",["rotx", 90,"rotz",-90, "y",3]]]]
                  , [6,7,3]
                  , [7,3,2] ] );              
           
     Label( "Layer2"
              , site= get(npts, [7,4,5,6]) 
              , opt=["scale",0.6]
              );
     if(draw)  color("teal") polyhedron( npts, pf[1]) ;//rodfaces(4) );     
    }
  
  
  }

  module Layer1(dim=0, draw=0, markpts=0, drawEdges=1, BOM=0)
  {
    
    echom("Layer1");
    
    if(BOM)
    {
      //Move( [ [1,0, counter_z*0.55]
            //, [0,0, counter_z*0.55]
            //, [1,1, counter_z*0.55] ]
          //, [ [100,0,0], [80,0,0], [80,1,0] ]
          //)
      translate([72,l_upper_wall_x-PLY2,-counter_z*0.55])
      rotate(axis=Z, a=-90)
      Obj();
    }
    else Obj();  
  
    module Obj()
    {
      pts=cubePts( [ l_upper_wall_x-PLY2
                    , back_wall_y-PLY2-PLY1
                    , PLY2] 
          , actions=[ "x", PLY2 
                    , "y", PLY2 //shelf_y_lower-shelf_y_upper
                    , "z", counter_z*0.55]    
          );
     if(markpts) MarkPts(pts); 
        if(drawEdges) DrawEdges( [pts, rodfaces(4)] );                   
        if(dim)
        Dims(pts, [ [[5,6,7] , ["Label",["actions",["rotx", 90,"rotz",-90, "y",3]]]]
                  , [6,7,3]
                  , [7,3,2] ] );              
           
     Label( "Layer1"
              , site= get(pts, [7,4,5,6]) 
              , opt=["scale",0.6]
              );
     if(draw)  color("teal") polyhedron( pts, rodfaces(4) );     
    }
  
  
  }

}
