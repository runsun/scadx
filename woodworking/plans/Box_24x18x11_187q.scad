include <../../scadx.scad>
include <../scadx_woodworking.scad>

ISLOG=0;
Box_24x18x11_187q();
   
module Box_24x18x11_187q( // Add support bars
   
     isdraw=1
   , isframe=1  
   , islabel=1
   
   , nVBar = 4
   , nHBar = 5
   
   , offset=0    // distance to disassemble parts to show each part
   , isCutlist=0 // =1: Draw cutlist instead of product
   
   , avail_mats  // = ["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
                
   , cutlist_part_spacer=1 // the space seperates parts. This allows for considerations
                           // for saw-width, bad spot in lumber, etc. It's better to 
                           // print out cutlists of different part_spacer values
                              
   , drawopts // Overwrite the setting of global isdraw and isframe
      =[    
       //  "Bottom", ["isdraw",1, "isframe",0, "islabel",1]
       // "Vbar", ["isdraw",0, "isframe",1, "islabel",1, "ismarkpts",1]
        //"Hbar", ["isdraw",1, "isframe",0, "islabel",1]
        "Side_L", ["isdraw",1, "ismarkpts",0
                  ]
      , "Side_W", ["isdraw",1, "ismarkpts",0
                  ]
       ]
              
   )
{
  echom("Box_24x18x11_187q()");
  
  ////=====================================================
  //// Check WOODS definitions
  echo( WOODS= _fmth( WOODS ) );
  
  ////=====================================================
  //// Define Units
  
  pktsize = h(WOODS, "cedPkt_1x6_Lb"); 

  TH = pktsize[0];
  
  L = 24;
  W = pktsize[1]*3+TH*2; //12;
  H = pktsize[1]*2;
  title= _s( "Box_24x18x11 ({_}x{_}x{_})",[L,W,H]); 
  
  BottomRaise = LEN2*2; // leave a space so the bottom is not
                   // in direct concact w/ the ground 
  
  
  //os = offset;
  
  ////=====================================================
  //// Echo Title to console 
  echo(_div( _b(title) , s="font-size:24px" ));
      
  
  ////=====================================================
  //// Define parts. 
  _parts=  
  [
  
    "Side_L"
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", L
        , "color",["gold",.8]
        , "count", 4
        ]
    
  , "Side_W"
  
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", pktsize[1]*3 //W-TH*2
        , "color",["khaki",.8]
        , "count", 4
        ]    
        
  , "Bottom"
  
      , [ "mat", "cedPkt_1x6_Lb" 
        , "len", L-TH*2
        , "color",["olive",0.9]
        , "count", 2
        ]
        
  // enhanced w/ support bars:          
  
  , "Vbar"
  
      , [ "mat", "B1x2x8" 
        , "len", H-TH-BottomRaise
        , "color","teal"
        , "count", 4
        ]
  , "Hbar"
  
      , [ "mat", "B1x2x8" 
        , "len", W-TH*2
        , "color","teal"
        , "count", 3
        ]
  
  ];
  
  parts= processParts(_parts, isdraw, isframe, islabel, drawopts);
  ////=====================================================
  //// Main 
  if(isCutlist) 
   DrawCutlist( title= title //_s( "Box ( L{_}in x W{_}in x H{_}in )", [L,W,H])
                , parts=parts//2                 
                , avail_mats = avail_mats //["B2x4x8", [60,70], [96,LEN4,LEN2], 50 ]
                , mat_spacer=1
                , part_spacer=cutlist_part_spacer 
                , debug= 0
                );                  
  else {
    DrawParts( parts=parts );
  }
    
  ////=====================================================
  //// Define modules to draw parts
   module DrawParts( parts ) 
  {
    //echom("DrawParts()");
    //echo(log_b("DrawParts()"), drawopts=drawopts);
    log_dps_b();
    log_dps(["parts", parts]);
    log_dps( ["isdraw", isdraw, "isframe", isframe, "islabel", islabel
             , "drawopts", drawopts] );         
   
   VBars();
   HBar();
   Side_L();
   Side_W();
   Bottom();
   
   module Side_L()
   {
      DrawPart( partname="Side_L", parts=parts
              , actions=[ "rotx", 90, "y", TH] );
      DrawPart( partname="Side_L", parts= parts //updatekey(parts, partnamt
                                          //, "dims", [ [1] 
              , actions=[ "rotx", 90, "y", TH, "z", pktsize[1]] );
   
      DrawPart( partname="Side_L"
               , parts= parts
              , actions=[ "rotx", 90, "y", W ] );
      DrawPart( partname="Side_L"
              , parts= updatekey( parts
                                 , "Side_L"
                                 , ["dims"
                                   , [ [[2,3,7],["r",.075]]
                                     , [[3,[L,W,0],2],["r",.075]]
                                     , [[[0,0,H],2,3],["r",.075]]
                                     ]
                                   ]  
                                 )
              , actions=[ "rotx", 90, "y", W , "z", pktsize[1]] );
   }
   
   module Side_W()
   {
      DrawPart( partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH] 
              );
      DrawPart( partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "z", pktsize[1]] 
              );
      DrawPart( partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "x", L-TH] 
              );
      DrawPart( partname="Side_W", parts=parts
              , actions=[ "rotx", 90, "rotz", 90, "y", TH, "z", pktsize[1], "x", L-TH] 
              );
   }
   
   module Bottom()
   {
      DrawPart( partname="Bottom", parts=parts
              , actions=[ "t", [TH,TH,BottomRaise] ] 
              );
      DrawPart( partname="Bottom", parts=parts
              , actions=[ "t", [TH,TH+pktsize[1],BottomRaise] ] 
              );
      DrawPart( partname="Bottom", parts=parts
              , actions=[ "t", [TH,TH+pktsize[1]*2,BottomRaise] ] 
              );
   }
   
   module VBars()
   {
      acts= [ "roty", -90 , "rotz", 90 
            , "t", [TH+LEN2,TH+LEN1,BottomRaise+TH] 
            ];
      sas = spreadActions( objlen=LEN2, spreadlen=L-TH*2,n=nVBar);
      
      for(i = range(sas)) 
      { 
        act= sas[i];
        DrawPart( partname="Vbar", parts=parts
                , actions=concat(acts,sas[i]));
        DrawPart( partname="Vbar", parts=parts
                , actions=concat(acts,["y",W-LEN1-TH*2], sas[i]));
      }
      
   }
   module HBar()
   {
      acts=[ "rotx", 90, "rotz",90, "t", [TH,TH, BottomRaise-LEN2] ];
      sas = spreadActions( objlen=LEN1, spreadlen=L-TH*2,n=nHBar);
      
      for(i = range(sas)) 
      { 
        act= sas[i];
        DrawPart( partname="Hbar", parts=parts
                , actions=concat(acts,sas[i]));
      }   
   }
   
   
    log_dps_e();
    
  } //<= DrawParts()

 
}
