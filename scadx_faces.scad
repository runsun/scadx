//========================================


function clock_wize_fis_on_pi( faces      // = a_fis
                             , pi         // i of one pt 
                             , fis_on_pi  // [2,0,1]
                             )=
(
    /*
                  4 
              _.-'`'-._ 
         _.-'`         '-._
       2'._       fi=0     '- 5
        |  `-._          _.-'\   
        |       '-._   _-'    \
        |   fi=1    1'         \ 
       3-._         |   fi=2   _\            
           `-._     |      _.-'  6
               `-._ | _.-'  
                   0 `
    
    Let fis_of_pi = [0,1,2]  (it could be any order)
    
    clock_wize_fis_on_pi( faces, 1, [0,1,2] ) => [0,2,1]
     
    For fi_0 to be on the next of fi_1:
      [2,1] of fi_1 = reversed edge [1,2] of fi_0 
      
    edges:
      fi=0: [ [1,2],[2,4],[4,5],[5,1] ]
      fi=1: [ [1,0],[0,3],[3,2],[2,1] ]
      fi=2: [ [1,5],[5,6],[6,0],[0,1] ]
          
   */ 
   let( firng= range(fis_on_pi)
      //, fis_on_pi = 
      , a_pis_on_pi= [ for(fi=fis_on_pi) faces[fi] ] 
          // fis_on_pi= [2,0,1]
          // a_pis_on_pi = [ [6,0,1,5],[1,2,4,5],[3,2,1,0]]
        //----------------------------------------                                                                                           
      , a_edges_on_pi = [ for(pis=a_pis_on_pi) get2(pis) ]
          // a_edges_on_pi=
          //  [ [ [6,0],[0,1],[1,5],[5,6] ]
          //  , [ [1,2],[2,4],[4,5],[5,1] ]
          //  , [ [3,2],[2,1],[1,0],[0,3] ]
          //  ]
        //----------------------------------------                                                                                           
      , a_edges_on_pi_rolled = 
          [ for(egs=a_edges_on_pi)
             let(i= idx( [for(eg=egs)eg[0]], pi)) // i= index of edge where
                i>0? roll(egs, -i): egs            // the pi is the first i
          ]                                        // like [2,3] for pi=2
                                                  // [5,4] for pi=5
          // a_edges_on_pi_rolled=
          //  [ [ [1,5],[5,6],[6,0],[0,1] ]
          //  , [ [1,2],[2,4],[4,5],[5,1] ]
          //  , [ [1,0],[0,3],[3,2],[2,1] ]
          //  ]     
        //----------------------------------------                                                                                           
      , a_edges_on_pi_rev_last= 
         [ for(fi=firng)
            let( egs=a_edges_on_pi_rolled[fi])
           [ fis_on_pi[fi]
           , for(eg=egs)
             eg==last(egs)? [eg[1],eg[0]]: eg 
           ]
        ]  
          // a_edges_on_pi_rev_last=
          //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
          //  , [0, [1,2],[2,4],[4,5],[1,5] ]
          //  , [1, [1,0],[0,3],[3,2],[1,2] ]
          //  ]   
          // The 2,0,1 above is the fi
        //----------------------------------------                                                                                           
       , re_arranged = 
          _re_arrange_a_edges_on_pi_rev_last(a_edges_on_pi_rev_last)
          // re_arranged=
          //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
          //  , [1, [1,0],[0,3],[3,2],[1,2] ]
          //  , [0, [1,2],[2,4],[4,5],[1,5] ]
          //  ]   
        //----------------------------------------                                                                                           
       )
    //echo(faces=faces)   
    //echo(pi=pi)
    //echo(fis_on_pi=fis_on_pi)
    //echo(a_pis_on_pi=a_pis_on_pi)   
    //echo(a_edges_on_pi=a_edges_on_pi)   
    //echo(a_edges_on_pi_rolled=a_edges_on_pi_rolled)   
    //echo(a_edges_on_pi_rev_last=a_edges_on_pi_rev_last)   
    //echo("-------") 
    [for(x=re_arranged)x[0]]
   
);
     clock_wize_fis_on_pi=[ "clock_wize_fis_on_pi"
                           , "faces,pi,fis_of_pi", "array", "Face",
     "
      fis_of_pi: indices of faces surrounding pi, like [2,0,3]
     ;;
     ;; This func makes sure that the order of those faces are clock-wize
      
      "
    ,""
    ];
    function clock_wize_fis_on_pi_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3)
           , fis_on_1 = get_a_fis_on_pts(rodfaces)[1]
           , cw_fon1= clock_wize_fis_on_pi( rodfaces,1, fis_on_1 )
            )
        doctest( clock_wize_fis_on_pi,
        [ 
          "var rodfaces"  
          /// [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , "pi=1"
        , "var fis_on_1"  /// [0, 2, 3]
        , str( "edges_on_1= ")
          // [ [[2, 1], [1, 0], [0, 2]]
          // , [[0, 1], [1, 4], [4, 3], [3, 0]]
          // , [[1, 2], [2, 5], [5, 4], [4, 1]]
          // ]
        , str( [for(fi=fis_on_1) get2(rodfaces[fi])] )
        , ""
        
        , [ cw_fon1, [0, 3, 2], "rodfaces,1, fis_on_1"] 
        , str( "clock-wized edges_on_1=")
        , str( [for(fi=cw_fon1) get2(rodfaces[fi])] )
          // [ [[2, 1], [1, 0], [0, 2]]
          // , [[1, 2], [2, 5], [5, 4], [4, 1]]
          // , [[0, 1], [1, 4], [4, 3], [3, 0]]
          // ]  
        , "// Note that the 1st and 2nd face (faces[0],faces[3]) share same edge [1,2]"
        , "// Note that the 2nd and 3rd face (faces[3],faces[2]) share same edge [1,4]"
        , "// Note that the 3rd and 1st face (faces[2],faces[0]) share same edge [1,0]"
           
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces, "fis_on_1", fis_on_1]
        )
    ); 
   
function edgefi_update(ef1, ef2, _i=0)=
(
   _i> len(ef2)-2? ef1
   : let( eg2= ef2[_i]   // current edge by index, like [2,3]
        , f2s= ef2[_i+1] // current face hash, like 
                         //  [ "L",3, "R", 6 ]
                         //  3: left face index (in faces) = faces[3]
                         // An ef is:
                         // [ [2,3], ["L",3,"R",6] 
                         // , ...
                         // ]
        , eg2rev = [ eg2[1], eg2[0] ]                 
        , ef1= haskey(ef1, eg2) // eg2 found in ef1, so we need to replace
                                // [ "L",3, "R",6 ] of ef1.eg2
                                // with f2s (= ef2.eg2)
               ? update( ef1
                       , [ eg2 
                         , update( hash(ef1, eg2), f2s )
                         ]
                       )
               :haskey(ef1, eg2rev )?
                 update( ef1
                       , [ eg2rev
                         , update( hash(ef1, eg2rev), f2s )
                         ]
                       )
               : concat( ef1, [eg2, f2s] )
        )
      edgefi_update( ef1, ef2, _i=_i+2)
);   


{ // faces 
function faces( 

        shape="chain"
      , nside=4
      , nseg=3 // for chain,ring
      , tailroll=0
        // nseg is used for shape that has
        // segments unknown until run-time
        // For example, chain
        /* tailroll: when closed, you might want to 
        //      rotate the tail face to align with
        //      the head face so the closed link
        //      from head to tail won't look twisted
        // tailroll is an int as "how many pts to roll"
         
             tailroll=0     = 1       = 2
             45----46    46----38  38----37
             |      |    |      |  |      |
             |      |    |      |  |      |
             37----38    45----37  46----45
        */ 
     , nx // for mesh
     , ny // for mesh
     , isTriangle // for mesh   
     , nside_in   // for tubeEnd
     , idx_in   // for tubeEnd
     , idx_out  // for tubeEnd
     /* 
        shape= "tubeEnd" is for tubes that has different
        nsides in and out
        
        Set either te_nside_in and te_nside_out, or 
        te_idx_in and te_idx_out. 
        
        If setting te_nside_in/te_nside_out, the indices
        will start from 0:
        
        te_nside_in= 3
        te_nside=out=5
        ==> indices: in:[0,1,2], out:[3,4,5,6,7]

     */
      
)=    
(let( shapeIsCuts= isarr(shape) && isarr(shape[0]) && ispt( shape[0][0]) 
            // cuts is [ pts, pts, pts...]
    , shape = shapeIsCuts?
              (    len(shape[0])==1 && len(last(shape))==1? "spindle"
                : len(shape[0])==1 ? "conedown"
                : len(last(shape))==1? "cone"
                : "chain"
              ): shape

//             cuts? // If an arr is given, try to get shape
//                            // This arr should be:[ pts, pts, pts...]
//              (let( chk = [len(cuts[0])==0?0:1, len(last(cuts))==0?0:1]
//                 )
//                chk==[0,0]?"spindle"
//              : chk==[1,0]?"cone"
//              : chk==[0,1]?"conedown"
//              : len(cuts)==2? "rod"
//              : "chain" 
//              )
//             : shape 
    , s = nside //cuts? len(cuts[1])-1: nside                      
	, s2= 2*nside                              
	, s3= 3*nside
                
                
    )
	//==================================================
	// cubesides
	//       _6-_
	//    _-' |  '-_
	// 5 '_   |     '-7
	//   | '-_|   _-'| 
	//   |    |-4'   |
	//   |   _-_|    |
	//   |_-' 2 |'-_ | 
	//  1-_     |   '-3
	//     '-_  | _-'
	//        '-.'
	//          0
	shape=="cubesides"
	? [ for(i=range(s))
		  [ i
			, i==s-1?0:i+1
			, (i==s-1?0:i+1)+s
			, i+s
		  ]
	  ]

    : shape=="mesh"
        ?( isTriangle? 
             [for(x=range(nx-1)) 
               each [for(y=range(ny-1))
                     each [ [ x*ny+y, x*ny+y+1, (x+1)*ny+y ]
                          , [ x*ny+y+1, (x+1)*ny+y+1, (x+1)*ny+y ]
                          ]
                    ]
             ]
           : [for(x=range(nx-1)) 
               each [for(y=range(ny-1))
                     [ x*ny+y
                     , x*ny+y+1
                     , (x+1)*ny+y+1
                     , (x+1)*ny+y
                     ]
                    ]
             ]
         )
    : shape=="rod"
	? concat( [ reverse(range(s))], [range(s,s2) ],
			  faces("cubesides", s) ) 

	//==================================================
	// cone
    /*
              8
	//       /:`   
	//      / |: -
	//     /  | : `_ 
	//    /   |  :  '_
	//   /_.-'6-._:   `_
	//  5-._  |    :'-._`_  
	//  |   `'-._   :   `7
	//  |     |  `'-.4-'`|
	//  | _.-'2-._   |   |
	//  1-._       `'|._ |  
	//      `'-._    |   3
	//           `'-.:-'`
	//               0    
     */
    : shape=="cone"
    ? concat( [ reverse(range(s))]        // starting face
    
			, joinarr( [ for(c=range(nseg-1)) // nseg-1 because
				 [ for(f=faces("cubesides", nside)) // last one
					[ for(i=f) i+c*nside]           // is cone
				 ]
                      ])
                    
             // The last seg is the cone
            , [ for(i=range( (nseg-1)*nside // = 4 above 
                           , nseg*nside ))  // = 8 above 
                [  i, i==nseg*nside-1?(nseg-1)*nside:i+1
                , nseg*nside]
              ]            
			)   
 
	//==================================================
	// conedown
    /*
	      _6_    
	   _-' | '-._
	  5-._ |     '-._
	  |   '-._    _.'7
	  |    |  '-8'  | 
	  |   _2_   |   |
	  |_-'   '-.|   |
	 1'-._      |'-.|
	   '. '-._  | _.3
	     '.   '-4'/
	       '.  //
	         '/'      
	          0
	
     */
    : shape=="conedown"
    ? concat( 
                 // The 1st seg is the cone
                [ for(i=range( 1 , nside+1 ))  
                  [ i, i==1?nside:i-1, 0 ]
                ] 
    
             , nseg==1?[]
               :joinarr( [ for(c=[0:nseg-1]) // 
                     [ for(f=faces("cubesides", nside)) 
                         [ for(i=f) i+c*nside+1]           
                     ]
                  ])
                        
             , [ range( (nseg-1)*s+1,(nseg)*s+1) ] // ending face
             
			)   

	//==================================================
	// spindle
    /*
              0
	//       /:`   
	//      / |: -
	//     /  | : `_ 
	//    /   |  :  '_
	//   /_.-'3-._:   `_
	//  4-._  |    :'-._`_  
	//  |   `'-._   :   `2
	//  |     |  `'-.1-'`|
	//  | _.-'7-._   |   |
	//  8-._       `'|._ |  
	//      `'-._    |   6
	//           `'-.:-'`
	//               5    
     */
    : shape=="spindle" // spindle example in chainPts_demo_coln_arrow()
    ? concat( 
                 // The 1st seg is the cone
                [ for(i=range( 1 , nside+1 ))  
                  [ i, i==1?nside:i-1, 0 ]
                ] 
    
             , joinarr( [ for(c=[0:nseg-3]) // 
                     [ for(f=faces("cubesides", nside)) 
                         [ for(i=f) i+c*nside+1]           
                     ]
                  ])
                        
             // The last seg is the cone
            , let( i0= (nseg-2)*nside+1
                 , il=(nseg-1)*nside 
                 )
              [ for(i=[i0:il])
                [  i, i==il?i0:i+1, il+1] 
              ] 
			)                
                         
    //==================================================
	// tubeEnd
    // 
    // End faces for tubes that has different nsides 
    // on the inside and outside walls
    /*
       2018.12.6 --- much simpler new approach
    
                   0.    
                _-`  '.  
              .'   4   `. 
            -`  _-` `-_  `. 
          -`   9       5   `.  
        3`     |       |     `1
         `.    8       6   _-`
           `.   `-_ _-`  _'
             `.    7   _'
               `.    .'                   -`
                 `2`       
                 
       => [0,1,2,3, 9,8,7,6,5,4]   
    */

 
    /*   Old approach (before 2018.12.6)
             
         S         0.       P
          `.    _-`  '.   .`
            `..'   4   `.` 
            -``._-` `-_` `. 
          -`   9`.  .` 5   `.  
        3`     |  `O   |     `1
         `.    8 .` `. 6   _-`
           `.  .`-_ _-`. _'
             `.    7   _'.
            .` `.    .'   `.                -`
          .`     `2`        Q
          R
          
       Approach:
       
       1) Align minimal indices (0 and 4 above)
       2) Find the side having smaller nside (outside above)
       3) Find the midpts of each edge (P,Q,R,S above). 
          This divides out-side into nside parts.
       4) Loop through nside_in. ALL indices fall within one 
          part will pair up with the corresponding nside 
          indices. For example, 
          part 0: [0,4]
          part 1: [1,5],[1,6]
          part 2: [2,7]
          part 3: [3,8],[3,9]
       5) If there's only one pair in a part, means it will
          join neighbor parts on both directions to form 
          4-index faces. For example, 
          [0,4] => [4,0,1,5], [0,4,9,3]
          [2,7] => [7,2,3,8], [2,7,6,1]
       6) If there r more than one pair in a part, make 
          3-index faces:
          [1,5],[1,6] => [1,6,5]
          [3,8],[3,9] => [3,9,8]        
    */
    : shape=="tubeEnd"?
      let( 
          // When idx_out and idx_in are not given, auto
          // generate indices for out and in based on nside
          // and nside_in, resp. 
          //   idxo=[0, 1, 2, 3], idxi=[0,1,2,3,4,5]
          //
          // Outer indices
           idxo= or(idx_out,range(or(nside,4)))
         //, Lo  = len(idxo)
          // Inner indices
         , _idxi= or(idx_in, range(or(nside_in,6)))
         //, Li  = len(_idxi)
         
         , faces = concat( idxo, reverse(_idxi) )
         
         
         //----- below is old code b4 2018.12.6 -----
//          If auto generate, need to convert idxi 
//          from [0,1,2,3,4,5] to [4,5,6,7,8,9] 
//         , idxi = (!idx_out && !idx_in)?
//                      _idxi +repeat([(last(idxo)+1)], Li)
//                    : _idxi
//                  
//         , idxs= Li>=Lo? idxo// idx w/ small nside
//                        : idxi
//         , idxl= Li< Lo? idxo // idx w/ large nside
//                        : idxi
//         , Ls = len(idxs) 
//         , Ll = len(idxl) 
//         
//         , parts= [ for(i= range(idxs) ) // loop thru sma
//                     let( a_from = (360*(i-0.5))/Ls 
//                        , a_to   = (360*(i+0.5))/Ls
//                        , idx = idxs[i]
//                        , _pairs= [for(j= range(idxl))
//                                let(a = (360*j)/Ll )
//                                if( a>=a_from && a<a_to )
//                                 [idx, idxl[j]]
//                                ] 
//                         )
//                     concat(  
//                        [for(j= range(idxl))
//                          let(a = (360*j)/Ll )
//                          if( (a-360)>=a_from && (a-360)<a_to )
//                          [idx, idxl[j]]
//                        ] , _pairs
//                        )
//                  ]
//                        
//         /* parts:
//            [ [ [0, 4] ]
//            , [ [1, 5], [1, 6] ]
//            , [ [2, 7] ]
//            , [ [3, 8], [3, 9] ]
//            ]
//         */               
//         , _faces= [ 
//            for(i=range(parts))
//            let( part= parts[i]
//               , pre = parts[ i==0?len(parts)-1:i-1 ]
//               , next= parts[ i==len(parts)-1?0:i+1 ]
//               ) 
//              len(part)==1 ?
//                [ len(pre)==1?[]
//                  :concat( part[0],reverse(last(pre)) )
//                , concat( reverse(part[0]), next[0] )  
//                ]
//             :  concat( 
//
//                    part[0][0]==part[1][0]? // [[1,5],[1,6]]
//                    [ for( i= [1:len(part)-1] )
//                       app( part[i],part[i-1][1])
//                    ]
//                   :[ for( i= [1:len(part)-1] ) // [[5,1],[6,1]]
//                       app( part[i], part[i-1][0])
//                    ]
//                , 
//                  len(next)>1?
//                   [concat( reverse(last( part)), next[0])]
//                   :[]
//                )
//            ]
//         , faces= joinarr(_faces)
         
         
         )// let
     faces               
  //   [ "idxo",idxo, "idxi", idxi, "parts", arrprn(parts,2,nl=true), "faces_fmt",arrprn(faces,2,nl=true),"faces",faces ]
    
    //==================================================
	// tube
	//             _6-_
	//          _-' |  '-_
	//       _-'   14-_   '-_
	//    _-'    _-'| _-15   '-_
	//   5_  13-'  _-'  |      .7
	//   | '-_ |'12_2-_ |   _-' | 
	//   |    '-_'|    '|_-'    |
	//   |   _-| '-_10_-|'-_    |
	//   |_-'  |_-| 4' _-11 '-_ | 
	//   1_   9-  | |-'       _-3
	//     '-_  '-8'|      _-'    
	//        '-_   |   _-'
	//           '-_|_-'
        //              0
    //  bottom=	 [ [0,3,11, 8]
    //			 , [1,0, 8, 9]
    //			 , [2,1, 9,10]
    //			 , [3,2,10,11] ];
    //
    //	top	=	 [ [4,5,13,12]
    //			 , [5,6,14,13]
    //			 , [6,7,15,14]
    //			 , [7,4,12,15] ]
    : shape=="tube"
    ? (
        nside_in?
        
        concat( faces( "cubesides", nside )  // outside sides
			, [ for(f=faces("cubesides",nside_in )) //inner sides
					reverse( [ for(i=f) i+s2] )
			  ]
            , [ for(f = faces("tubeEnd" 
                       , idx_out= (range(nside))
                       , idx_in = (range( nside*2, nside*2+nside_in))
                       ) )
                    if(f) reverse(f)
              ]     
			, [ for(f = faces("tubeEnd"
                       , idx_out= (range(nside,nside*2))
                       , idx_in = (range(nside*2+nside_in
                                        ,(nside*2+nside_in*2))
                                  )
                        ))
                   if(f) reverse(f)      
              ]      
			)	  
       
	  : concat( faces( "cubesides", s )  // outside sides
			, [ for(f=faces( "cubesides", s ))    // inner sides
					reverse( [ for(i=f) i+s2] )
			  ]
			, [ for(i=range(s))        // bottom faces
				  [i, mod(i+s-1,s), mod(i+s-1,s)+s2, i+s2 ] 
			  ]
			, [ for(i=range(s,s2))    // top faces
				  [i, i==s2-1?s:i+1, i==s2-1?s3:i+s2+1, i+s2 ]
			  ]
			)	  
      ) 
	//==================================================
	// chain                
	//       _6-_-------10------14
	//    _-' |  '-_    | '-_   | '-_
	//  2'_   |     '-5-----'9-------13
	//   | '-_|   _-'|  |    |  |    |
	//   |    |-1'   |  |    |  |    |
	//   |   _-_|----|-11_---|--15   |
	//   |_-' 7 |'-_ |    '-_|    '-_| 
	//  3-_     |   '-4------8-------12
	//     '-_  | _-'
	//        '-.'
	//          0
	//
	:shape=="chain"
	? concat( [ reverse(range(s))]            // starting face
			, [ range( nseg*s,(nseg+1)*s) ] // ending face
			, joinarr( [ for(c=range(nseg))
				 [ for(f=faces("cubesides", nside))
					[ for(i=f) i+c*nside]
				 ]
			  ])
			)  
                    
    //==================================================
	// ring 
 	/*  
        ring is a chain-like shape with the beg
        and end faces joined.
                    
        A ring with nseg=6, nside=4            
    
               13-------------9
             _-`|`-_______10-` `-_
          _-'   _-`|       |`-_   `-_ 
      17.'____.' -_|_____11|   `-6____.5
        |`-_  |`-_-`        `-_-`| _-`| 
        |   `-_ _-`-22____2_-`  _-`   | 
      16|_____|`-_/_|______|\_-` |7__ |4
         `-_   21|  |      | |1 `  _-`
            `-_  |  |23___3| |  _-`
               `-|-/________\|-`
                 20          0
    
        Using a faces("chain", nside=4, nseg=5) ### NOTE: nseg=5
        
        Lets see what we need, and reverse the process of getting it.
        
        We need joining faces, which are extra faces needed for 
        the closing link:
        
        fj= [ [ 0,1,21,20 ]
            , [ 1,2,22,21 ]
            , [ 2,3,23,22 ]
            , [ 3,0,20,23 ]
            ]
            
        We have a roll func: roll([0,1,2,3],-1)= [1,2,3,0]
            
        A transposed fj would be:
        
                               Tis column is where we start:
                               
        fjt=[ [  0, 1, 2, 3 ]  range(nside) = A  (head face)
            , [  1, 2, 3, 0 ]  roll( A ,-1)
            , [ 21,22,23,20 ]  roll( B ,-1)
            , [ 20,21,22,23 ]  range( (nseg-1)*nside, nseg*nside) = B
            ]                  B= bottom face
            
            
    */
    
    	
    : shape=="ring"?
     let( _tailface = range( (nseg)*nside, (nseg+1)*nside)
        , tailface = tailroll? roll( _tailface, tailroll):_tailface
        )
     concat( transpose(
               [ range(nside)
               , roll( range(nside), -1)
               , roll(tailface,-1) //roll( range( (nseg)*nside, (nseg+1)*nside),-1)
               , tailface //range( (nseg)*nside, (nseg+1)*nside)
               ] 
              )
            , joinarr( [ for(c=range(nseg))
				 [ for(f=faces("cubesides", nside))
					[ for(i=f) i+c*nside]
				 ]
		     ])
	    )    
     //------------------------------------                
//     : shape=="tube2"? // 2018.12.6
//                       // Tube with different # of sides inside and outside
//                       //  Ref: tubeEnd 
//       /*
//                    9    
//                   .-._
//                .-' :13'-._
//             .-'    /|\    '-._
//           8'      / | \       '-.10
//           |'-.   12-._ \._      /|   
//           |   '-.:   '-11  '-._/ |
//           |   .' '-.   :     /'. |
//           |.'    :  '-.:    /   /3
//          1'-._   5-._  '-.7/   /
//               '-._   '-4  |   /
//                   '-._    |  /
//                       '-._| / 
//                           0 
//                           
//                                 */                 
//                        	    
//       let(  idxo= or(range(idx_out),range(or(nside,4)))
//          , _idxi= or(range(idx_in), range(or(nside_in,6)))
//          , Lo= len(idxo)
//          , Li= len(_idxi)
//          , L = Lo+Li
//          //, f_bot = concat( range(Lo), range(Lo+Li-1, Lo-1))
//          , f_bot = concat( range(Lo-1, 0-1), [Lo-1], range(Lo, L), [Lo])
//          , f_out = [for(i=range(Lo))
//                      [i, i==Lo-1?0:i+1 ,  (i==Lo-1?0:i+1)+L, i+L]
//                    ]
//          , f_in = [for(i=range(Lo, L))
//                      [i==L-1?Lo:i+1, i, i+L, (i==L-1?Lo:i+1)+L ]
//                   ]
//          //, f_top = concat( range( L+Lo-1, L-1), range(L+Lo, 2*L ))
//          , f_top = concat( range( L, L+Lo), [L], range(2*L-1, L+Lo-1 ), [2*L-1])
//          , faces = concat( [f_top], f_out, f_in, [f_bot] )         
//          )
//         //echo( idxo=idxo, _idxi=_idxi )
//         //echo( f_top= f_top ) 
//         //echo( f_out= f_out ) 
//         //echo( f_in= f_in ) 
//         //echo( f_bot= f_bot ) 
//         faces    
     : shape=="tube2"? // 2018.12.6
                       // Tube with different # of sides inside and outside
                       //  Ref: tubeEnd 
       /*
                    9    
                   .-._
                .-' :13'-._
             .-'    /|\    '-._
           8'      / | \       '-.10
           |'-.   12-._ \._      /|   
           |   '-.:   '-11  '-._/ |
           |   .' '-.   :     /'. |
           |.'    :  '-.:    /   /3
          1'-._   5-._  '-.7/   /
               '-._   '-4  |   /
                   '-._    |  /
                       '-._| / 
                           0 
                           
                                 */                 
                        	    
       let(  idxo= or(range(idx_out),range(or(nside,4)))
          , _idxi= or(range(idx_in), range(or(nside_in,6)))
          , Lo= len(idxo)
          , Li= len(_idxi)
          , L = Lo+Li
          //, f_bot = concat( range(Lo), range(Lo+Li-1, Lo-1))
          , f_bot = concat( range(Lo-1, 0-1), [Lo-1], range(Lo, L), [Lo])
          , f_out = [for(i=range(Lo))
                      [i, i==Lo-1?0:i+1 ,  (i==Lo-1?0:i+1)+L, i+L]
                    ]
          , f_in = [for(i=range(Lo, L))
                      [i==L-1?Lo:i+1, i, i+L, (i==L-1?Lo:i+1)+L ]
                   ]
          //, f_top = concat( range( L+Lo-1, L-1), range(L+Lo, 2*L ))
          , f_top = concat( range( L, L+Lo), [L], range(2*L-1, L+Lo-1 ), [2*L-1])
          , faces = concat( [f_top], f_out, f_in, [f_bot] )         
          )
         //echo( idxo=idxo, _idxi=_idxi )
         //echo( f_top= f_top ) 
         //echo( f_out= f_out ) 
         //echo( f_in= f_in ) 
         //echo( f_bot= f_bot ) 
         faces    
	                      
     //------------------------------------                
     :[]
);

    faces=["faces","shape,sides,count", "array", "Faces",
     " Given a shape ('rod', 'cubesides', 'tube', 'chain'), an int
    ;; (sides) and an optional int (count), return an array for faces of
    ;; a polyhedron for that shape.
    "];

    function faces_test( mode=MODE, opt=[] )=(//==================
        doctest( faces,
        [
         [ faces("cone",4,1)
            ,  [[1,4,0],[2,1,0],[3,2,0],[4,3,0],[1,2,6,5],[2,3,7,6],[3,4,8,7],[4,1,5,8],[1,2,3,4]]
            , "'cone', nside=4, nseg=1"
         ]
        ,
         [ faces("conedown",4,1)
            ,  [[1,4,0],[2,1,0],[3,2,0],[4,3,0],[1,2,6,5],[2,3,7,6],[3,4,8,7],[4,1,5,8],[1,2,3,4]]
            , "'conedown', nside=4, nseg=1"
         ]
        ] 
        ,mode=mode, opt=opt
        )
    );

  //echofh( faces("tubeEnd", nside=4, nside_in=9));
} // faces 


function get_a_fis_on_pts(faces)=
(  
  let( pts_i_max = max( flatten(faces) ) // pt index max
     , frng = [1:len(faces)-1]
     , _rtn = [ for(pti=[0:pts_i_max])
                [ for(fi=[0:len(faces)-1])
                  if(has(faces[fi],pti)) fi 
                ]
              ]
             // Pts[0] neighbors 3 faces: faces[0], faces[2], faces[4]
             // [[0, 2, 4], [0, 2, 3], [0, 3, 4], [1, 2, 4], [1, 2, 3], [1, 3, 4]]
             
     , rtn = [ for(pi=range(_rtn))
              clock_wize_fis_on_pi ( faces,pi,_rtn[pi] )
              // This makes them clock-wize. 
              // i.e., faces[0]-faces[2]-faces[4] is clock-wize for pts[0]
              // i.e., faces[0]-faces[3]-faces[2] is clock-wize for pts[1]
              // [[0, 2, 4], [0, 3, 2], [0, 4, 3], [1, 4, 2], [1, 2, 3], [1, 3, 4]] 
             ]        
     )
     rtn
);
    get_a_fis_on_pts=[ "get_a_fis_on_pts", "faces", "array", "Face",
     "Return an array of pt's face indices
     ;; --- faces that connect to a pt.
     ;;
     ;; [ [2,0,1], [5,4,2] ...]
     ;; 
     ;; means: 
     ;;     faces 2,0,1 are connected to pts[0] (3 faces)
     ;;     faces 5,4,2 are connected to pts[1] (3 faces)
     "
    ,""
    ];
    function get_a_fis_on_pts_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3) )
        doctest( get_a_fis_on_pts,
        [ 
          "var rodfaces"  
          /// [[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , [ get_a_fis_on_pts( rodfaces)
        
          //,[ [0, 2, 4], [0, 2, 3], [0, 3, 4]
          // , [1, 2, 4], [1, 2, 3], [1, 3, 4]]
          , [ [0, 2, 4], [0, 3, 2], [0, 4, 3]
            , [1, 4, 2], [1, 2, 3], [1, 3, 4]]   
     
          , "rodfaces"
          ]           
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces]
        )
    ); 
    

function get_edgefis(faces,_hash=[],_fi=0)=
(  // 
   // An edgefis is a hEdge (hash w/ key=edge) :
   // 
   // [ [2,3], ["L",3,"R",6,"F",[4], "B",[0,1]] 
   // , ...
   // ]
   // 
   // Means, for edge [2,3], 
   //   it's left face is faces[3]
   //   it's right face is faces[3]
   //   it's forward faces are faces[4]
   //   it's backward faces are faces[0,1]
   // 
   // **** Use edgefis_get( hEdge, eg ) to get value
   // 
    _fi<len(faces)? 
    let( face = faces[_fi]   // a face is an array of pt-indices
         , edges= get2(face) 
         , _new = [ for(eg=edges) 
                    each [eg, [ haskey(_hash, [eg[1],eg[0]])?"L":"R"
                              , _fi ] 
                         ]
                  ]              
         )
    ////get_edgeface(faces, _hash= update(_hash,_hash_new), _i=_i+1 )
    get_edgefis(faces, _hash= edgefi_update(_hash,_new), _fi=_fi+1 )
    
   // :_hash
    :// We have done adding the R/L faces
     // Now we add fowward and backward faces 
     let( pt_fis= get_a_fis_on_pts(faces) 
                 // pt_fis: indices of faces connect to a pt
                 // [ [2,0,1], [5,4,2] ...] = pts[0] has faces 2,0,1  
          )
       //echo( "... get_edgefi: ", pt_fis=pt_fis )   
       [ each for( eg=keys(_hash))
         let( h_LRfis = hash(_hash, eg) // ["L",4,"R",5]
            , LRfis = [hash( h_LRfis,"L"), hash( h_LRfis,"R")] //vals(h_LRfis) // [4,5]
            , p1fis = pt_fis[ eg[1] ]
            , Ffis  = dels( p1fis, LRfis )
            , p0fis = pt_fis[ eg[0] ]
            , Bfis  = dels( p0fis, LRfis )
            )
         //echo( "... get_edgefi: ", eg=eg, h_LRfis=h_LRfis, LRfis=LRfis, p1fis=p1fis, Ffis=Ffis, Bfis=Bfis )   
          [ eg, concat( h_LRfis
                          , ["F", Ffis,"B", Bfis]
                          )
              ] 
       ]
      
);

    get_edgefis=[ "get_edgefis", "faces", "array", "Face",
     "Return the edgefi data defined by faces.
     ;; 
     ;;  An edgefi is a hash defined as:
     ;;  
     ;;    {edge:{'R':Rfi, 'L':Lfi, 'F':[Ffi], 'B':[Bfi]}}
     ;;    
     ;;  where fi = index of face (in reference to faces)
     ;;        L,R,F,B = Left, Right, Forward and Backward, resp. 
     ;;  
     ;;  Like:
     ;;   [ [2, 1], ['R', 0, 'L', 3, 'F', [2], 'B', [4]]
     ;;   , [1, 0], ['R', 0, 'L', 2, 'F', [4], 'B', [3]]
     ;;   , ...
     ;;   ] 
     ;;  
     ;;  Use edgefis_get( hEdge, eg ) to get value          
     "
    ,""
    ];
    function get_edgefis_test( mode=MODE, opt=[] )=
    (   
        let( rodfaces= rodfaces(3)
           , conefaces = faces( shape= "cone", nseg=1) )
        doctest( get_edgefis,
        [ 
          "var rodfaces"  //[[2, 1, 0], [3, 4, 5], [0, 1, 4, 3], [1, 2, 5, 4], [2, 0, 3, 5]]
        , [get_edgefis(rodfaces)
          ,[ [2, 1], ["R", 0, "L", 3, "F", [2], "B", [4]]
           , [1, 0], ["R", 0, "L", 2, "F", [4], "B", [3]]
           , [0, 2], ["R", 0, "L", 4, "F", [3], "B", [2]]
           , [3, 4], ["R", 1, "L", 2, "F", [3], "B", [4]]
           , [4, 5], ["R", 1, "L", 3, "F", [4], "B", [2]]
           , [5, 3], ["R", 1, "L", 4, "F", [2], "B", [3]]
           , [1, 4], ["R", 2, "L", 3, "F", [1], "B", [0]]
           , [3, 0], ["R", 2, "L", 4, "F", [0], "B", [1]]
           , [2, 5], ["R", 3, "L", 4, "F", [1], "B", [0]]
           ]
            
          , "rodfaces", ["nl", true]
          ] 
        , ""
        , "var conefaces"
        , [get_edgefis(conefaces)
          ,[ [3, 2], ["R", 0, "L", 3, "F", [2], "B", [4]] 
           , [2, 1], ["R", 0, "L", 2, "F", [1], "B", [3]]
           , [1, 0], ["R", 0, "L", 1, "F", [4], "B", [2]]
           , [0, 3], ["R", 0, "L", 4, "F", [3], "B", [1]]
           , [1, 4], ["R", 1, "L", 2, "F", [3, 4], "B", [0]]
           , [4, 0], ["R", 1, "L", 4, "F", [0], "B", [2, 3]]
           , [2, 4], ["R", 2, "L", 3, "F", [1, 4], "B", [0]]
           , [3, 4], ["R", 3, "L", 4, "F", [1, 2], "B", [0]]
           ]
          , "conefaces", ["nl", true]
          ] 
            
        ]
        ,mode=mode, opt=opt, scope=["rodfaces", rodfaces, "conefaces", conefaces]
        )
    );
    
    

function get_face_edges(faces, isSortedPis=true)=
(
   isint(faces[0])?
   ( let(edges= get2(faces))
     isSortedPis? [for( eg = edges ) eg[0]<eg[1]?eg:[eg[1],eg[0]] ]
                 : edges
   ): [for(f=faces) get_face_edges(f)]            
);

    get_face_edges=["get_face_edges","faces,pis","edges/arr_edges","faces",
    " Return edges (when faces is a single face --- i.e., [int] like [3,2,1] ) 
    ;; or an array of edges (when faces is arr of pis --- [pis] like normal
    ;; faces) that contain pis
    ;;
    ;; isSortedPis: pt indices in an edge are sorted, like [2,3] but not [3,2]
    ;;              Default= true 
    ;; (2018.4.30)
    "];

    function get_face_edges_test( mode=MODE, opt=[] )=
    (
        let( faces=[ [3,2,1],[4,5,6] ] 
           )
        doctest( get_face_edges,
        [ 
          "var faces"
        , [get_face_edges( faces[0]),[[2,3], [1,2], [1,3]], "faces[0]"]
        , [get_face_edges( faces[0],false),[[3,2], [2,1], [1,3]], "faces[0],false"]
        , [get_face_edges( faces[1]),[[4,5], [5,6], [4,6]], "faces[1]"]
        , ""
        , [get_face_edges( faces)
              , [[[2,3], [1,2], [1,3]], [[4,5], [5,6], [4,6]]]
              , "faces"
          ]
        
        ]
        ,mode=mode, opt=opt, scope=["faces",faces] )
    );

      
function get_fis_have_pis(faces,pis)= 
(                                     
  // return array of faces that contain pis
  // The pis could be int or array 2018.4.29
  // Return array of fi 
  
  isint(pis)? [ for( fi= [0:len(faces)-1] ) 
                //each has(faces[fi],pis)? [fi,faces[fi]]:[]
                if( has(faces[fi],pis) ) fi
              ]
  : [ for( fi= [0:len(faces)-1] ) 
      //each contain(faces[fi], pis)?[fi,faces[fi]]:[] 
      if( contain(faces[fi], pis) ) fi 
    ]
);
    get_fis_have_pis=["get_fis_have_pis","faces,pis","fis","faces",
    " Return an arr of face indices for faces that contain pis
    ;; The pis could be int or array 
    ;; (2018.4.29)
    "];

    function get_fis_have_pis_test( mode=MODE, opt=[] )=
    (
      let( faces=[ [3,2,1],[4,5,6],[1,2,5], [7,8,9] ] 
         )
      doctest( get_fis_have_pis,
      [ "var faces"
      //, [get_fis_have_pis( faces, 9), [3, [7,8,9]], "faces,9"]
      //, [get_fis_have_pis( faces, [1,2]), [0,[3,2,1], 2,[1,2,5]], "faces,[1,2]"]
      //, [get_fis_have_pis( faces, 10),  [], "faces,10"]
      , [get_fis_have_pis( faces, 9), [3], "faces,9"]
      , [get_fis_have_pis( faces, [1,2]), [0, 2], "faces,[1,2]"]
      , [get_fis_have_pis( faces, 10),  [], "faces,10"]
      ]
      ,mode=mode, opt=opt, scope=["faces",faces] )
    );	


function get_fis_have_edges( faces, edges)=
(
   let( frng = [0:len(faces)-1]
      , aRefEdges = isint(faces[0][0]) ? // faces contain array of indices: [ [3,2,1], ... ]
                [for(fi=frng)get2(faces[fi])]: faces // Assure face-edges
      , edges0 = isint(edges[0])?[edges]:edges
      , query = [for(eg=edges0) [min(eg),max(eg)] ]  // sort them
      )
   //echo(faces=faces)
   //echo(edges=edges)
   //echo(aRefEdges=aRefEdges)
   //echo(edges0=edges0)
   //echo(query=query)   
   [ for( fi= frng )
     let( refEdges= aRefEdges[fi]
        , _refEdges= [ for(eg=refEdges) [min(eg),max(eg)] ] 
        )
     //echo( refEdges=refEdges, _refEdges=_refEdges )
     //each contain(_refEdges, query)? [fi, faces[fi]]:[] 
     if(contain(_refEdges, query)) fi 
   ]
);

    get_fis_have_edges=["get_fis_have_edges","faces,edges","fis","faces",
    " Return array of face indices for faces containing edges
    ;; The edges could be [pi,pi] or array of that 
    ;; (2018.4.29)
    "];

    function get_fis_have_edges_test( mode=MODE, opt=[] )=
    (
        let( faces=[ [3,2,1],[4,5,6],[1,2,5], [7,8,9] ] 
           )
        doctest( get_fis_have_edges,
        [ 
          "var faces"
        , [ get_fis_have_edges( faces,[1,2]), [0,2]     
          , "faces,[1,2]"]
        , [ get_fis_have_edges( faces,[[1,2],[1,5]]), [2]     
          , "faces,[[1,2],[1,5]]"]
        ]
        ,mode=mode, opt=opt, scope=["faces",faces] )
    );	

function get_near_fi(faces, fi, edge)= // return index of the face next to 
(                                      // the fi (index of current face)
                                       // across the edge (18.5.4)
   let( fi2 = get_fis_have_edges( faces, edge )
      , rtn = fi2[0]==fi? fi2[1]:fi2[0]
      )
      //echo( "get_near_fi: ", fi2=fi2)
      //echo( "get_near_fi: ", rtn=rtn)
      rtn
);

function edgefis_get(h,eg)= 
(                        
   // Special version of hash for edgefis
   // which requires that [a,b] and [b,a] be treated as the 
   // same edge 
   //
   // Usage: edgefis_get( get_edgefis(faces), [4,7] )
   //  
   let( v = hash(h,eg) )
   v? v:
   let(v= hash( h,[eg[1],eg[0]] ) )
   ["L", hash(v,"R"), "R", hash(v,"L"), "F", hash(v,"B"), "B", hash(v,"F") ]
);

function _re_arrange_a_edges_on_pi_rev_last(a_edges_on_pi_rev_last,_rtn=[],_i=0)=
(
    // Internal func for clock_wize_fis_on_pi()
    
    // a_edges_on_pi_rev_last=
    //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
    //  , [0, [1,2],[2,4],[4,5],[1,5] ]
    //  , [1, [1,0],[0,3],[3,2],[1,2] ]
    //  ]  
    // is re-arranged to:      
    //  [ [2, [1,5],[5,6],[6,0],[1,0] ]
    //  , [1 [1,0],[0,3],[3,2],[1,2] ]
    //  , [0 [1,2],[2,4],[4,5],[1,5] ]
    //  ]  
    // such that they are in order in terms of edge continuatility :
          
    //  [ [2, [1,5],...,[1,0] ]
    //  , [1, [1,0],...,[1,2] ]
    //  , [0 [1,2],...,[1,5] ]
    //  ]
     
    //echo(_i=_i)
    //echo(a_edges_on_pi_rev_last=a_edges_on_pi_rev_last)  
   _i>=len(a_edges_on_pi_rev_last)-1? _rtn
   :let( _rtn=_i==0?[a_edges_on_pi_rev_last[0]]:_rtn
       // _rtn=_i==0?[a_edges_on_pi_rev_last[0]]:_rtn
       , prevlast= last(last(_rtn)) 
        , _rtn2= concat( _rtn
                       , [ for(f=a_edges_on_pi_rev_last)
                           //echo(f=f)
                           if(f[1]==prevlast)f 
                           ]
                       )      
        )
    //echo(_rtn=_rtn)     
    //echo(_rtn2=_rtn2)     
    //echo(prevlast=prevlast)    
    _re_arrange_a_edges_on_pi_rev_last(a_edges_on_pi_rev_last
                                       ,_rtn=_rtn2,_i=_i+1)           
          
);  


function sievefaces( nO=4      // # of outer sides
                   , nI=[3,4]  // # of inner sides. Could be:
                                  //    undef ==> =nO
                                  //    int   ==> single hole
                                  //    [int,int...] ==> multiple holes  
                   )= // 2018.12.8  
(
  /* faces are to be arranged like:
     [ top_face
     , bottom_face
     , each out_faces
     , each in_faces_0 
     , each in_faces_1
     , ...
     ]

  This can be used for a block to add holes. 
  The order of pts should be arranged as
       
              
                     .7-----8._
                  .-' |     |  '-._
               .-'    |     |      '-9
             6'       |     |       /|
             |'-.    .2-----3._    / |   
             |   '-.'          '-_/  |
             |   .' '-.          / '.|
             |.'       '-.      /   /4
            1'-.          '-.5 /   /
                '-.          |    /
                   '-.       |   /
                      '-.    |  / 
                         '-. | /
                            '0/
 
  with faces: [ [5,6,7,8,9]   // top
              , [4,3,2,1,0]   // bottom
              , [0,1,6,5], [1,2,7,6], [2,3,8,7], [3,4,9,8], [4,0,5,9] // outer
              ]
  
  We then add holes to it. Points are in the following order:
        
                      .7-----8._
                  .-' |  17 |  '-._
               .-'16+----+  |      '-9
             6'     |\   |  |       /|
             |'-.   |.\__|--3._    / |   
             |   '-.|15  |14   '-_/  |
             |   .' '-.  |       / '.|
             |.'  12+--'-. 13   /   /4
            1'-.     \   |'-.5 /   /
                '-. 11\__|   |    /
                   '-.   10  |   /
                      '-.    |  / 
                         '-. | /
                            '0/
 */

 let( nI=nI==undef? [nO]: isint( nI )? [nI]:nI
     , L = nI+nO
     , top_out = range(nO,2*nO)
     , top_ins= [ for (i=range(len(nI)))
                  let( base = 2*nO+ 2*sum( slice( nI, 0,i) )+nI[i] )
                  //echo(i=i, base=base)
                  each  concat( [ nO ]
                          , [ for (j=range(base+nI[i]-1, base-1 ) ) j ]
                          , [ base+nI[i]-1 ]
                          )
                ]
     , top = concat( top_out, top_ins )
                
     , bot_out = range(nO-1,-1)
     , bot_ins= [ for (i= range(len(nI)))
                  let( base = 2*nO+ 2*sum( slice( nI, 0,i) ) )
                  //echo(i=i, base=base)
                  each concat( [ nO-1 ]
                         , [ for (j=range(base, base+nI[i]) ) j ]
                         , [ base ]
                         )
                ]
     , bot = concat( bot_out, bot_ins )
                
     , out= [for(i=range(nO))
              [ i, i==nO-1?0:i+1, (i==nO-1?0:i+1)+nO, i+nO]
            ] 
     , in = [for(i=range(nI))
              let( base = 2*nO+ 2*sum( slice( nI, 0,i) ) )
              //echo("for in", i=i, base=base)
              each [for(j=range(base, base+nI[i]))
                     [ j==base+nI[i]-1?base:j+1
                     , j
                     , j+nI[i]
                     , (j==base+nI[i]-1?base:j+1)+nI[i] ]
                   ]  
            ] 
     )
     //echo(nO=nO, nI=nI)
     //echo(top_out=top_out)
     //echo(top_ins=top_ins)
     //echo(top=top)
     //echo("---")
     //echo(bot_out=bot_out)
     //echo(bot_ins=bot_ins)
     //echo(bot=bot)
     //echo("---")
     //echo(out=out)
     //echo(in=in)
  [top, bot, each out, each in]
);

    sievefaces=["sievefaces","nO, nI", "array", "Faces",
     " Return faces for a shape of block containg one or more holes
     ;; 
     ;;  nO: # of outer sides
     ;;  nI: # of inner sides
     ;;      undef: nI = nO
     ;;      int  : # of inner sides for one hole
     ;;      [ints]: for multiple holes  
     ;;
     ;; Note: (1) seems OpenSCAD unable to display them correctly
     ;;       in some cases. For example, check out
     ;;       demo_sievefaces_multiholes_has_bug(3,2,1/10)
     ;;       in demo_sievefaces.scad. 
     ;;       (2) Will fail when union with other shape(s)
     "];

    function sievefaces_test( mode=MODE, opt=[] )=(//==================
        doctest( sievefaces,
        [
         [ sievefaces(4,[3,4,3])
         , [[4,5,6,7,4,13,12,11,13,4,21,20,19,18,21,4,27,26,25,27]
           ,[3,2,1,0,3,8,9,10,8,3,14,15,16,17,14,3,22,23,24,22]
           ,[0,1,5,4],[1,2,6,5],[2,3,7,6],[3,0,4,7]
           ,[9,8,11,12],[10,9,12,13],[8,10,13,11]
           ,[15,14,18,19],[16,15,19,20],[17,16,20,21],[14,17,21,18]
           ,[23,22,25,26],[24,23,26,27],[22,24,27,25]]
         , "4,[3,4,3]"
         ]
        
        ] 
        ,mode=mode, opt=opt
        )
    );




function get_scadx_faces_test_results( mode=MODE, opt=["showmode",true] )=
(
    // Let get_openscad_funcs_for_doctest.py be in /scadx/py,
    // then generated automatically using:
    // >>> py get_openscad_funcs_for_doctest.py -fo .. -fn scadx_faces.scad
    // Ref: https://gist.github.com/runsun/f777262e04b993a1ff2a0c03515dca6c

    // Usages (2 different approaches) :
    //
    // (1) results = get_scadx_faces_test_results(mode=MODE,opt=[]);
    //     for(r = results) echo( r[1] )
    // (2) scadx_file_test( "scadx_faces", mode=10, summaryOnly=true, opt=[] );
    //
    // doctest modes: ijk
    //   i: doc. 1=showdoc, (not given)= don't show
    //   j: test. 1=do test, (not given) or 0 = don't test
    //   k: usage. 0=don't show; 1=show failed only; 2= show all

    [
      //_re_arrange_a_edges_on_pi_rev_last_test( mode, opt ) // to-be-coded
     clock_wize_fis_on_pi_test( mode, opt )
    //, edgefi_update_test( mode, opt ) // to-be-coded
    //, edgefis_get_test( mode, opt ) // to-be-coded
    , faces_test( mode, opt )
    , get_a_fis_on_pts_test( mode, opt )
    , get_edgefis_test( mode, opt )
    , get_face_edges_test( mode, opt )
    , get_fis_have_edges_test( mode, opt )
    , get_fis_have_pis_test( mode, opt )
    //, get_near_fi_test( mode, opt ) // to-be-coded
    ]
);
