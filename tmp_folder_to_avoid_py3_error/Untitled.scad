include <scadx_obj.scad>

Keystone = [10.8,15.4,23.2];
Frigidaire = [12.3,17.3,25.5];

module Draw( brand= Keystone ){
    
    x=brand[0]; y=brand[1]; z=brand[2];
    pts= [ ORIGIN, [0,y,0], [x,y,0], [x,0,0]
         , [0,0,z],[0,y,z], [x,y,z], [x,0,z]
         ];
    polyhedron( points = pts
              , faces = faces( "rod", nside=4,nseg=1 )
              );
}
color(undef, 0.5)
Draw();

color("red", 0.2) Draw(Frigidaire);