include <scadx_core_dev.scad>
include <../doctest/doctest_dev.scad>
include <scadx_core_test_dev.scad>

module doctests(mode) //ops=["mode",112])
{
	accumulate_test(mode);
//	addx_test(mode);
//	addy_test(mode);
//	addz_test( mode );
    all_test( mode );
//	angle_test( mode );
//    angle_series_test( mode ); // THIS IS SLOW ....
//	angleBisectPt_test( mode ); // test with other function
//	anglePt_test( mode );
//	anyAnglePt_test( mode );
//    app_test( mode );
//    appi_test( mode );
//	arcPts_test( mode ); // NEED FIX (2014.08.07)
//	arrblock_test( mode );
//	
//	begwith_test( mode );
//  begword_test( mode );
//	between_test( mode );
//// blockidx_test( mode );  2015.2.23: to be removed
//	boxPts_test( mode );
//    calc_test( mode ); // SLOW (~30,36sec)
//    calc_basic_test( mode ); 
//  centroidPt_test( mode );
//// chkbugs_test( mode ); // need this
//	cirfrags_test( mode );
////    coordPts_test( mode ); // need this
//	cornerPt_test( mode );
//	countArr_test( mode );
//	countInt_test( mode );
//	countNum_test( mode );
//	countStr_test( mode );
//	countType_test( mode );
//	cubePts_test( mode );	
//
//	del_test( mode ); 
//	delkey_test( mode );
//	dels_test( mode );
//	det_test( mode );  // Need re-investigation
//  deeprep_test( mode );     
//	dist_test( mode );
//	distPtPl_test( mode );
//	distx_test( mode );
//	disty_test( mode );
//	distz_test( mode );
//    dot_test( mode );
//	echoblock_test( mode );
//	echo__test( mode );
//	echoh_test( mode );
////     echom_test( mode );
//	endwith_test( mode );
//	expandPts_test( mode );
//	fac_test( mode );
//  faces_test( mode );
//	fidx_test( mode ); // New2014.8.8   
//  flatten_test( mode ); // Need this
//	_fmt_test( mode );
//	_fmth_test( mode ); //==> Recursion detected calling function 'index' ==> 2015.2.25 don't see that problem
//	ftin2in_test( mode );	
//    //_getdeci_test( mode ); // need this
//	get_test( mode ); // <=== start using simplified scoping (6/11/2014)
//	getcol_test( mode );
//    getSideFaces_test( mode );
//    getsubops_test( mode );
//    getTubeSideFaces_test( mode );
//    getuni_test( mode ); // SLOW, ~45, 40 sec
//	_h_test( mode );
//	has_test( mode );
//	hash_test( mode );
//    hashex_test( mode );
//	hashkvs_test( mode );
//	haskey_test( mode );
//	incenterPt_test( mode );
//	index_test( mode );
//	inrange_test( mode );
//	int_test( mode );  // in test cases, only string needs to be quoted
//
//	intersectPt_test( mode ); // 14.8.8: WE NEED THIS
//
//	is0_test( mode ); // need add this 
//	is90_test( mode ); // randomly generated
//	isarr_test( mode );
//	isbool_test( mode );
//	isequal_test( mode );  // need test cases
//	isfloat_test( mode );
//	ishash_test( mode );
//	isint_test( mode );
//    isln_test( mode );
//	isnum_test( mode );
//	isOnPlane_test( mode );
//    isSameSide_test( mode );
//    ispl_test( mode );
//    ispt_test( mode );
//	isstr_test( mode );
//	istype_test( mode );  
//	join_test( mode );
//	joinarr_test( mode );
//	keys_test( mode );
//  //kidx_test( mode ); // need this. 15.4.17: Really? 
//	last_test( mode );
//	lcase_test( mode ); // need this
////////  linecoef_test( mode ); // probably don't need this 
//	lineCrossPt_test( mode ); // 2014.8.8: need test cases
//	lineCrossPts_test( mode ); // 2015.4.11: need test cases
//    longside_test( mode );
//	linePts_test( mode );
//	longside_test( mode );
//  lpCrossPt_test( mode ) // need this
////
//	_mdoc_test( mode );
//	midPt_test( mode );
//	minPt_test( mode );
//	minxPts_test( mode );
//	minyPts_test( mode );
//	minzPts_test( mode );
//	mod_test( mode );
//
//	normal_test( mode );
//	normalPt_test( mode ); // <=== needed 
//    normalPts_test( mode ); // need this
//	num_test( mode );
//	numstr_test( mode );
//	onlinePt_test( mode ); // test with other function
//	onlinePts_test( mode ); 
//	or_test( mode );
//	othoPt_test( mode );  
//    packuni_test( mode );
//	parsedoc_test( mode );//??
//	permute_test( mode );
//	pick_test( mode );
//	planecoefs_test( mode ); // test with other function
//	ppEach_test( mode ); // Removed on 2015.2.4. too easy and too less use to warran a function // Put back on 2015.2.6: easier to code in complex codes
//    pqr90_test( mode );// Need this
//    prod_test( mode );
//  projPt_test( mode ); // need this
//  quadrant_test( mode ) // need this
//	rand_test( mode );     // random, no test
//	randc_test( mode );
//  randInPlanePt_test( mode ); // need this
//  randInPlanePts_test( mode ); // need this
//  randOnPlanePt_test( mode ); // need this
//  randOnPlanePts_test( mode ); // need this
//	randPt_test( mode );
//	randPts_test( mode );
//  randRightAnglePts_test( mode ); // need this
//	range_test( mode );
//	ranges_test( mode );
////	rearr_test( mode ); // moved to "discard list"
//	repeat_test( mode );
//	replace_test( mode );	
//	reverse_test( mode );
//	ribbonPts_test( mode ); // we need this
//// 	Ring0_test( mode );
//	RM_test( mode );
//	RMx_test( mode );
//	RMy_test( mode );
//	RMz_test( mode );
//  RMs_test( mode ); // we need this ?
//	RM2z_test( mode );  // need this
////	RMz2p_test( mode );
//
//	rodfaces_test( mode ); // need this
//	rodPts_test( mode );
//	rodsidefaces_test( mode );
//	roll_test( mode );
//	rotangles_p2z_test( mode );    
//	rotangles_z2p_test( mode );  
//    rotMx_test( mode );
//    rotx_test( mode );
//   run_test( mode );
//	_s_test( mode );
//// 	select_test( mode ); // 14.8.9: removed. Many other ways already: rearr() or list comp 
//    scaleM_test( mode );
//	shift_test( mode );
//    shortside_test( mode ); // We need this
//	shrink_test( mode );
//	shuffle_test( mode );
//    sinpqr_test( mode ); 
//	slice_test( mode );
//	slope_test( mode );
//	split_test( mode );
//    splits_test( mode );
//	squarePts_test( mode ); // test with other function
//    subops_test( mode );
//	sumto_test( mode );
//	sum_test( mode );
//	switch_test( mode );
//  symmedianPt_test( mode );
//	transpose_test( mode );
//   trim_test( mode );
//	twistangle_test( mode );
//	type_test( mode );
//    typeplp_test( mode );
//	ucase_test( mode );
//	uconv_test( mode );
//	update_test( mode );
//	updates_test( mode );
//	vals_test( mode );
//	vlinePt_test( mode ); 


//======================================
//    CurvedCone_test( mode );
//	Block_test( mode );
//	Chain_test( mode );
//	ColorAxes_test( mode );
//	DashBox_test( mode );
//	DashLine_test( mode );
//	Dim_test( mode ); // Need fix: doc too long
//	LabelPt_test( mode ); // need add this
//	LabelPts_test( mode ); // need add this
//	Line0_test( mode );
//	Line_by_x_align_test( mode );
//	Line_test( mode );
//	LineByHull_test( mode );//	Mark90_test( mode );
//	MarkPts_test( mode );
//	Normal_test( mode );
//	Plane_test( mode );
//	PtGrid_test( mode );
//	PtGrids_test( mode );
//	Ring_test( mode );
//	Rod_test( mode );
//	RodCircle_test( mode );

}    

doctests( mode=112 ); //ops = ["mode",112] );

//===========================================
//
//           Version / History
//
//===========================================

            
scadx_core_test_ver=[
["20150207-1", "Separated from original scadx_core.scad"]
];

SCADX_CORE_TEST_VERSION = last(scadx_core_test_ver)[0];

echo_( "<code>||.........  <b>scadx core test version: {_}</b>  .........||</code>",[SCADX_CORE_TEST_VERSION]);
echo_( "<code>||Last update: {_} ||</code>",[join(shrink(scadx_core_test_ver[0])," | ")]);    