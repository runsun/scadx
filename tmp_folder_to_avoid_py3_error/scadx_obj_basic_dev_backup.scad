include <scadx_core.scad>
//include <scadx_obj_adv_dev.scad>

ECHO_MOD_TREE= true;  // 2015.5.22
    /* Every single module will have an argument: 
       mti (module tree indent) as its last arg.
       mti is an integer indicating which layer
       the module is in in the current module tree.
       
       module A( ... mti=0){
          if(ECHO_MOD_TREE) echom( "A", mti);
          B( ... mti=mti+1)
       }
    */ 
    
//mm = 0.01;  // millimeter

//========================================================
Arc=[[],
"

 angle not given:  draw arc:RS

                     _R 
                   _:  :
                  :     : 
                 -       :
                :         : 
               -           : 
       p ------S-----------'Q


 angle given: draw arc:DS

                     _R 
                   _:  :
                D :     : 
                 -'-_    :
                :    '-_  : 
               -     a  '-_: 
       p ------S-----------'Q


	 
"];

  
module Arc(pqr,opt=[]){

    //echom("Arc","");
    /*
      Given pqr, draw a curve c_PS: 
    
                       _R 
                  S _-`  
                 _-`:      
              _-`    :       
           _-`        :          
        _-`           :            
      Q---------------: P
             rad (radius of curve= dPQ if not given)

                       _R 
                  S _-`  
                 _:`: :    
              _-`  : : :     
           _-`      : : :        
        _-`         : : :           
      Q-------------:-P-:
      |..... rad .....|-|  
                       r  (base radius of the curve line PS)
    
                     _._       C: center of xsec 
                   -` | `-     rss: radius around C 
     Q------------|---C---|            
                   -  |  -`  
                    ``-``
    */
    df_opt=[ 
        "a", angle(pqr)  // angle determining cone length
      , "nseg", $fn==0?6:$fn //# of slices 
      , "fn", 6 //$fn==0?6:$fn    //# of points along bottom circle
                                   // if not given, use a_pqr
      , "rad", d01(pqr)  // rad of the curve. 
      , "rads", undef
      , "radpts", undef // Instead defining pts by dist to Q,
                         // radpts give 3D pt coords in ref. to Q. 
      
      , "r", 0.3         // Set rad of crossection on P and R ends at once
      , "rP", undef      // rad of the circle at P
	  , "rR", undef      // rad of the circle at R  
      
      
      // rs, rss, xsecpts: only one is used:
      , "rs", undef      // rads along the curve line PS 
      , "rss", undef     // r around the xsec center C. It sets 
                         //  a symmetric rs pattern for each xsec
      , "xsecpts", undef // [ slice1, slice2 ...] where slice?=[p1,p2...]
      
      , "rotate",0       
      
      , "frame", false // mark the curve line
      , "markxsec", false // true | i | [i1,i2 ...] mark xsec(s)
      , "xsecopt", []  // add to markxsec opt to customize xsec marking
      
      , "frame", false // draw lines of edges 
      , "showxsec2d", false // show xsec on xy plane
//      , "showobj", true // false to hide shape to debug (by viewing markers)
      , "hide",false
      
      , "closed", false  // close the arc to make it a circle
      , "sealed", false  // true: seal the 2 pts of xsecpts that's closet 
                         // to the Q. Works only when closed=true.
                         // Or, [i,j] where i,j are adjacent indices
                         // of the two pts on xsecpts.
      , "echopts", false // echo pts. Can copy/paste them and do a
                         // MarkPts( joinarr(pts),["chain",true] );
                        // example: 
                        // pts=[ [ [-0.3141, 1.06765, 0.97702]
                        //       , [-2.0401, 1.19704, 0.91436]
                        //       , [-1.1965, 0.27633, -0.2859]
                        //       ]
                        //     , [ [-0.3206, 0.85794, 1.1333]
                        //       , [-2.0515, 0.83005, 1.18784]
                        //       , [-1.2080, -0.0906, -0.01244]
                        //       ]
                        //     ... 
                        //     ]
                        //    ];
      
      //, "debug", false   // set to true to show pt indices
      ];//df_opt
      
	_opt= update( df_opt, opt);

    //echo(_opt = _opt);
    
    function opt(k,nf)=hash(_opt,k,nf);

    rad = opt("rad");
    r   = opt("r");
    rP  = or(opt("rP"), r);
    rR  = or(opt("rR"), r);
    radpts= opt("radpts");
    nseg= radpts?len(radpts):opt("nseg");
    rot = opt("rotate");
    fn  = opt("fn");
    rss = opt("rss");       
    markxsec=opt("markxsec");
    //showobj = opt("showobj");
    
    xsecpts= opt("xsecpts");
    nside = xsecpts?len(xsecpts):fn;
    
    //echo("rP,rR", rP, rR);
    
    a = opt("a");
    P = pqr[0]; 
    Q = pqr[1]; 
    N = N(pqr);    
    R= anglePt(pqr, a, len=rad);  // The end pt of arc
    //echo(a=a);
    //MarkPts( [P,Q,N,R], "pl;ch");
    //newpqr = [P,Q,R];
	
    /*
        +..__
     rP |    ``''--..
        |           | rR
        +-----------+
        P           R
    */
    _rs = opt("rs");
    rs = _rs? _rs: 
          [ for( i= range(nseg+1) ) rP-i*(rP-rR)/nseg  ] ;
         
    _rads= opt("rads");      
    rads= _rads? _rads : 
            [ for( i= range(nseg+1) ) rad  ] ;
                
    


    //================================================
    // Dealing with rotation when xsecpts are used
    //
    Cs = radpts? 
         [ for( p=addz(radpts,0)) 
             p==ORIGIN?Q: anglePt( pqr
                    , a= p.x==0? 90: atan( p.y/p.x )  
                    , len= norm( p ))
                                  
         ]  
         :[ for( i= range(nseg+1) )  // center of each slice      
            anglePt( pqr, i*a/(nseg), len=rads[i] ) 
         ];
    
         
         
    // Create T (tanglnPt_P) and Q2 ( Q after rotation)
    // when xsecpts are given with rotation
    /* 
       Q2 is the new Q after rotation          
                       _-`
         Q-----P-----C`-----
                 _-`
              _-`
            Q2
       T is a pt on the line normal to Q-C-Q2
         
    */      
    tq2s= xsecpts && rot?
          [ for( i= range(nseg+1) )
             let( ai= i*a/(nseg) 
              , C = Cs[i]
              , nextC= Cs[i+1]?Cs[i+1]:anglePt( pqr, a+10 )
              , T = tanglnPt_P( [C,Q,nextC],len=-2 ) //: aQCT=90
              , Q2= anglePt( [T,C,Q],a=90,a2=rot)
              , qcn= [Q,C,N]
              )
              [T,Q2]
         ]:[];
         
//    tq2s= xsecpts && rot?
//          [ for( i= range(nseg+1) )
//             let( ai= i*a/(nseg) 
//              , C = Cs[i]
//              , nextC= Cs[i+1]?Cs[i+1]:anglePt( pqr, a+10 )
//              , T = tanglnPt_P( [C,Q,nextC],len=-2 ) //: aQCT=90
//              , Q2= anglePt( [T,C,Q],a=90,a2=rot)
//              , qcn= [Q,C,N]
//              )
//              [T,Q2]
//         ]:[];
          
    /*
        // Draw the [T,C,Q2] triangle for debugging:
          
        for( i= range(tq2s) ){
            echo( i, tq2s[i] );
            MarkPts( [tq2s[i][0], Cs[i], tq2s[i][1]]
                   , ["chain",true,"label","TCQ","plane",true]);
        }
    */
    //===============================================
          
    pts= [ for( i= range( radpts?nseg:(nseg+1)) )
             let( ai= i*a/(nseg) 
              , C = Cs[i]
              , nextC= Cs[i+1]?Cs[i+1]:anglePt( pqr, a+10 )
              , T = tq2s[i][0] 
              , Q2= tq2s[i][1] 
              , qcn= [Q,C,N]
              )
              xsecpts? // xsecpts : [ [2,1],[3,5] ...]
              (
                rot?
                [ for( j= range(xsecpts) )
                       let( pt = xsecpts[j]  
                          , ptx= onlinePt( [C, Q2], len= -pt.x)
                          , pty= ptx+ uvN([Q2,C,T]) *pt.y
                          //, pty= ptx+ uv(  C, N([Q2,C,T]) )*pt.y
                          )
                       pty
                ]
                :[ for( j= range(xsecpts) )
                       let( pt = xsecpts[j]  
                          , ptx= onlinePt( [C, Q], len= -pt.x)
                          , pty= ptx+ uvN(pqr)*pt.y
                          //, pty= ptx+ uv( Q,N(pqr) )*pt.y
                          )
                       pty
                ]
              )
              :rss?
              [ for( j= range(fn) )
                  anglePt( qcn, a= j*360/fn+ rot, len=rss[i][j])
              ]
              :[ for( j= range(fn) )
                  anglePt( qcn, a= j*360/fn+ rot, len=rs[i])
              ]
         ];
    if(opt("echopts")) 
        echo( lenpts = len(pts), str( "pts to make Arc:<br/>", arrprn(pts,dep=2)) );     
    //MarkPts( get(pts,-1), "r=0.1;trp=0.5");
    
    //=======================================================
    //  markxsec: marking the cross-section points.
    //          
    //   Arc( ... opt=["markxsec",true] ) // mark the first xsec          
    //   Arc( ... opt=["markxsec",3] )    // mark the i=3 xsec       
    //   Arc( ... opt=["markxsec",[2,-1] ] ) // mark i=2 and the last xsec
    //           
    markxsecopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markxsec"
                     , df_subopt= update(
                                  ["r",0.06,"chain"
                                    ,["color","black","r",0.02
                                     , "closed",true]
                                  ], opt("xsecopt"))
                     , com_keys=[]
                     ); 
    //echo(markxsecopt= markxsecopt );          
    module Markxsec(pts, i, markxsecopt)
    {
       _i = fidx( pts, i );
        c = xsecpts?len(xsecpts):fn;
        _opt= getv(markxsecopt,"label")?
               concat( ["label", ["text", range(c*_i, c*(_i+1))]] 
                    , markxsecopt
                    ):markxsecopt;        
        /* ##### DO_NOT use update() here #####
           _opt= update( markxsecopt
                       , ["label", ["text", range(c*_i, c*(_i+1))]] 
                       );
           The markxsec on the root level, Arc(...opt=["markxsec",val]),
           other than taking either true/false or an opt, could also
           take other stuff like val=int or arr_of_int. Since it is
           an arr that looks like a hash, for example, [-3,2,-1],
           it will be added to markxsecopt directly. If we update label:
               ["r",0.06, -3,2,-1, "label",["text", "some"]]
                          ^^^^^^^
           hash() will fail to retrieve label opt. So we put the label
           up front using concat:  
               ["label",["text", "some"], "r",0.06, -3,2,-1 ]
        */
       // echo(_i=_i, c=c, _opt=_opt);
       MarkPts( pts[_i], _opt);
    }
    
    if( isarr(markxsec) ){ 
        for( i= markxsec ) Markxsec(pts,i,markxsecopt);            
    } else if( isint(markxsec)) { Markxsec(pts,i,markxsecopt);
    } else if( markxsec)  
        for( i= range(pts) ) Markxsec(pts,i,markxsecopt);            
        
    //=======================================================
    showxsecopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "showxsec2d"
                     , df_subopt=["chain",["color","black"]
                                 ,"label", range(xsecpts)]
                     , com_keys=[]
                     );
    
    //echo( xsecpts = addz(xsecpts,0), showxsecopt=showxsecopt  );
    // Why the following show error coord when is called with
    // "label", ["isxyz", true]
    if( xsecpts && showxsecopt ) 
        MarkPts( addz(xsecpts,0), showxsecopt);
    //=======================================================
    frameopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "frame"
                     //, df_subopt= ["label",true]
                     , df_subopt=["chain"
                                  ,["color","green","transp",0.3,"r",0.015]
                                   ,"ball",false
                                 ]
                     , com_keys=[]
                     );

    frameopt2 = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "frame"
                     , df_subopt=["chain"
                                  ,["closed",true
                                   ,"color","green","transp",0.3,"r",0.015]
                                   ,"ball",false
                                 ]
                     , com_keys=[]
                     );
//    echo( frame= opt("frame"));
    //echo( frameopt= frameopt);
//    echo( frameopt2= frameopt2);
    
    
    if( frameopt ){
        t_pts= transpose(pts);
        for( i=range(nside) ){
           MarkPts( t_pts[i], opt("closed")?frameopt2:frameopt);
        }
        for( i=range(pts) ){
           MarkPts( pts[i], frameopt2);
        }
    }
    
    //================================================
    
//    frameopt = subopt1( df_opt= df_opt
//                     , u_opt = opt
//                     , subname = "frame"
//                     , df_subopt=["color","black","r",0.06
//                                 ,"chain",["color","black","r",0.02]
//                                 ,"label",range(nseg)
//                                 ]
//            
//                     , com_keys=[]
//                     );
//    if( frameopt ) MarkPts(Cs,frameopt);             
    
//    echo(pts= str("<br/>",arrprn(pts,dep=2)));
    
//    for( sl= pts ){
//        MarkPts( sl, ["chain",true, "label",range(sl), "markpts",true] );
//    }
//    if(xsecpts){ MarkPts( addz(xsecpts,0)
//            , ["chain",true,"label",range(xsecpts)] ); }
//
        
        faces = faces( opt("closed")?"ring":"chain"
                     , nside=nside, nseg=nseg );
        
        
               function findunwantedfaces(sealed,_rtn=[],_i=0)=
               (
                  let( found = [ for(f=faces) 
                                 if([f[0],f[1]]==sealed) f
                                ] //: first run: [5,6,46,45], [5,6,16,15]   
                       , next = [ get(found,-1)[3], get(found,-1)[2]] // [15,16]
                     )
                  _i< nseg?
                     findunwantedfaces( next,_rtn=concat(_rtn,found),_i=_i+1)
                  :_rtn
               );     
                                 
    if(!opt("hide")){
        

        if( opt("closed")){
           
           //faces= faces("ring", nside=nside, count= nseg);
            
            
           sealed = opt("sealed");
           //echo(sealed = sealed);
           if(sealed ){ //: sealed = [5,6]
                uwfaces = findunwantedfaces(sealed);
                //echo( uwfaces=uwfaces);
                     // [[5,6,46,45],[5,6,16,15],[15,16,26,25]
                     // ,[25,26,36,35], [35, 36, 46, 45]]
                _wfaces = [ for(f=faces) if(idx(uwfaces,f)<0) f] ; 
               
                // Want to add 2 faces: [5,15,25,35,45],[6,16,26,36,46]
                tuwfaces = transpose(uwfaces);
                // transpose uwfaces:
                // [ [5,5,15,25,35], [6,6,16,26,36]
                // , [46,16,26,36,46], [45,15,25,35,45]]
                newfaces= [app( slice(tuwfaces[0],1), tuwfaces[3][0])
                          ,app( slice(tuwfaces[1],1), tuwfaces[2][0])
                          ];
               
    //           echo( uwfaces= uwfaces );
    //           echo( transposed= transpose(uwfaces));
    //           echo( newfaces= newfaces );
               
               //echo( wfaces= arrprn(wfaces,dep=2) );    
               wfaces = concat( _wfaces, newfaces); //concat( _wfaces,
               color( opt("color"), opt("transp") )    
               polyhedron( points= joinarr(  pts ) //pts
                          , faces = wfaces
                          );            
           } 
           else{ //wfaces=faces; }    
            
               
           
           color( opt("color"), opt("transp") )    
           polyhedron( points= joinarr(  pts ) //pts
                      , faces = faces
                      );                   
           }
        } else {
            //faces= faces("chain",nside=nside, count= nseg);      
            //echo(faces=faces);
//            echo(lenfaces = len(faces), faces= arrprn(faces,dep=2));
//            MarkPts( get(pts,-1), "r=0.1;trp=0.5");
//           
            color( opt("color"), opt("transp") )    
            polyhedron( points= joinarr(  pts ) //pts
                      , faces = faces
                      );              
        } 
 
  }//if_showobj   
    

}    

module Arc_demo_1()
{  echom("Arc_demo_1");
    
	pqr = randPts(3);
    Arc(pqr, "echopts"); 
	MarkPts(pqr, "ch");
    LabelPt( Q(pqr), "Default");
        
    pqr2= trslN(pqr,3); 
    Arc(pqr2, "a=90;fn=3;rad=3;r=1;cl=red;echopts");
//    Arc(pqr2, ["a",90, "fn",3, "rad",3,"r",1, "color","red"
//              ,"echopts", true]); 
	MarkPts(pqr2, ["chain",true]);
    LabelPt( Q(pqr2), "a=90,;fn=3;rad=3;r=1");
//    
}
//Arc_demo_1();

module Arc_demo_2_rPrRrs()
{ 
    echom("Arc_demo_2_rPrRrs");
//    opt=[ "a"  , 120
//        , "rad", 4
//        , "fn" , 12
//        , "nseg", 36
//        ];
    opt= "a=120;rad=4;fn=12;nseg=36";
    
	pqr = randPts(3,d=[-3,2]);
    Arc( pqr, str( opt, ";rP=0.5;rR=0.01")
         //update( opt, ["rP",0.5, "rR",0.01])
       );
	MarkPts(pqr, ["chain",true]);
    LabelPt(Q(pqr), "rP=0.5,rR=0.01,a=60,rad=4" );    
    
    
    rs= [ for(i=range(37)) sin(i*60)/5+0.5 ];
    //echo(rs=rs);
    pqr2= trslN( pqr,3); 
    Arc( pqr2, [opt, "color","red","transp",0.8,"rs",rs]
         //update( opt, ["color","red","transp",0.8,"rs",rs])
       ); 
	MarkPts(pqr2, ["chain",true, "transp", 0.3]);
    LabelPt(Q(pqr2), "rs=[ for(i=range(37)) sin(i*60)/5+0.5 ]" );    
 
}
//Arc_demo_2_rPrRrs();

module Arc_demo_2_rss()
{ 
    echom("Arc_demo_2_rss");
    function opt(h)=
        update( [ "a", 120, "rad",4
                , "fn", 24
                , "nseg", 18
                ], h) ;
                 	
    fn=24;
    rss= [ for(i=range(37)) 
            [for(j= range(fn))
                sin(j*45)/2+0.6
            ] 
        ];
    //echo(rss=rss);
    pqr= pqr();
    Arc(pqr, opt(["rss", rss])); 
	MarkPts(pqr, ["chain",true, "transp", 1]);
    LabelPt(Q(pqr),"Set rss (symmetric rs pattern for each xsec)");
            
    rss2= [ for(i=range(37)) 
            [for(j= range(fn))
                sin(j*60)/2+0.8
            ] 
        ];
    //echo(rss=rss);
    pqr2= trslN( pqr, 3.5); 
    Arc(pqr2, opt(["color","red","rss", rss2])); 
	MarkPts(pqr2, ["chain",true, "transp", 1]); 
    LabelPt(Q(pqr2),"For example, rss=[for(i=nseg)[for(j=fn)...]]");
            
}
//Arc_demo_2_rss();

module Arc_demo_3_rads()
{ 
    echom("Arc_demo_3_rads");
    
    pqr3= randPts(3);
    rads= [ for(i=range(25)) sin(i*60)/10+0.2+ d01(pqr3) ];
    //echo(rads=rads);
    Arc(pqr3, ["a",120, "fn",4, "nseg",24 ,"transp",1
              ,"rads", rads, "r",1
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 0.3]);    
    LabelPt(Q(pqr3),"Set rads (main curve pts)");

    
    pqr4= trslN(pqr3,3); //randPts(3);
    rads2= [ for(i=range(25)) sin(i*60)/10+0.2+ d01(pqr4) ];
    //echo(rads=rads2);
    Arc(pqr4, ["a",120, "fn",6, "rP", 1, "rR",0.01
              , "nseg",24 ,"transp",1
              ,"rads", rads2, "color","red"
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 0.3]);        
    LabelPt(Q(pqr4),"Set rads & rP, rR");
 
}
//Arc_demo_3_rads();

module Arc_demo_4_rads_rs()
{ 
    echom("Arc_demo_4_rads_rs");
    
    pqr3= randPts(3,d=[-2,3]);
    rs= [ for(i=range(25)) sin(i*60)/10+0.5 ];
    rads= [ for(i=range(25)) sin(i*60)/10+1+ d01(pqr3) ];
    echo(rads=rads);
    Arc(pqr3, ["a",180, "fn",12, "nseg",12 ,"transp",0.6
              ,"rads", rads, "rs",rs 
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 0.3]);    
    LabelPt(Q(pqr3),"Set both rs and rads");


    pqr4= trslN(pqr3,3);
    Arc(pqr4, ["a",180, "fn",12, "nseg",12 ,"transp",0.6
              ,"rads", rads, "rs",rs
              ,"hide",true, "frame", true
              ]); 
//  Doesn't know why frame is treated as false below:
//   Arc(pqr4, ["hide;a=180;fn=12;nseg=12","rads", rads
//              , "rs",rs , "frame", ["label",false]
//              ]); 

    
	MarkPts(pqr4, ["chain",true, "transp", 0.3]);    
    LabelPt(Q(pqr4),"hide obj and set frame");
    
    
    pqr5= trslN(pqr4,3); //randPts(3,d=[1,5]);
    rads2= [ for(i=range(13)) rand(4,5) ]; // sin(i*60)/10+0.2+ d01(pqr4) ];
    echo(rads=rads2);
    Arc(pqr5, ["a",180, "fn",6, "rP", 0.8, "rR",0.01
              , "nseg",12 ,"transp",0.8
              ,"rads", rads2, "color","green"
              ]); 
	MarkPts(pqr5, ["chain",true, "transp", 0.3]);        
    LabelPt(Q(pqr5),"Give noise: rads=[ for(i=range(13)) rand(4,5) ]");
    
}
//Arc_demo_4_rads_rs(); 

module Arc_demo_5_rotate()
{ 
    echom("Arc_demo_5_rotate");
    
    pqr3= pqr(); //randPts(3,d=[-4,-1]);
    rads3= [ for(i=range(13)) rand(4,5) ]; 
    Arc(pqr3, ["a",120, "fn",4, "rP", 1, "rR",0.01
              , "nseg",12 ,"transp",0.8
              ,"rads", rads3, "frame",["label",false]
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 0.3]);  
    
    
    uvN = uvN(pqr3)*3; 
    pqr4= [for(p=pqr3)p+uvN]; //randPts(3,d=[-3,0]);
    rads2= [ for(i=range(13)) rand(4,5) ]; // sin(i*60)/10+0.2+ d01(pqr4) ];
    echo(rads=rads2);
    Arc(pqr4, ["a",120, "fn",4, "rP", 1, "rR",0.01
              , "nseg",12 ,"transp",0.8
              ,"rads", rads2, "color","red", "rotate",45
              , "frame",,["label",false]
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 0.3]);
    LabelPt( pqr4[1], "rotate=45");
    
    
    
    pqr1= [for(p=pqr4)p+uvN];
    Arc(pqr1, ["a",45, "fn",6, "nseg",1, "rad",3
              , "r",1, "color","green","transp",0.8]); 
	MarkPts(pqr1, ["chain",true]);
        
    
    
    pqr2= [for(p=pqr1)p+uvN];;
    Arc(pqr2, ["a",45, "fn",6, "nseg",1
              , "r",1,"color","blue"
              ,"transp",0.8, "rotate",30
              , "rad",3
              ]); 
	MarkPts(pqr2, ["chain",true]);
    LabelPt( pqr2[1], "rotate=30");
}

//Arc_demo_5_rotate();

module Arc_demo_6_xsecpts_rotate()
{ 
    echom("Arc_demo_6_xsecpts_rotate");  
	
    xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    nseg=10;
    a=180;
    
    pqr2= randPts(3, d=[-3,0]);
    Arc(pqr2, ["a",a, "nseg",nseg ,"transp",1
              ,"xsecpts",xsecpts, "rotate",30
              , "color","red", "showxsec2d", true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    LabelPt(Q(pqr2),"  rotate=30");

    
    uvN = uvN(pqr2)*6;
    pqr3= [ for(p=pqr2) p+uvN ]; //randPts(3,d=[0,3]);
    Arc(pqr3, ["a",a, "nseg",nseg ,"transp",1
              ,"xsecpts",xsecpts
             // ,"frame",true
              , "markxsec",[0,-2]
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    LabelPt(Q(pqr3),"  no rotate");

    
    pqr4= [ for(p=pqr3) p+uvN ]; 
    Arc(pqr4, ["a",a, "nseg",nseg ,"transp",1
              ,"xsecpts",xsecpts, "rotate",-75
              //,"frame",true
              , "color","green"
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    LabelPt(Q(pqr4),"  rotate=-75");
    
}
//Arc_demo_6_xsecpts_rotate();

module Arc_demo_6_xsecpts_random()
{ 
    echom("Arc_demo_6_xsecpts_random");  
	
    fn=24;
    nseg=30; 
    
    pqr2= randPts(3,d=[3,7]);
    rads2= [ for(i=range(nseg+1)) rand(4,5) ];
    xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    Arc(pqr2, ["a",350, "fn",fn, "nseg",nseg ,"transp",0.9
              ,"xsecpts",xsecpts, "rads",rads2
              ,"frame", ["label",range(nseg),"color","black"]  
              , "showxsec2d",true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
                   
}
//Arc_demo_6_xsecpts_random();

module Arc_demo_7_markxsec()
{ 
    echom("Arc_demo_7_markxsec");  
	
    r=0.8;
    fn=6;
    nseg=10; 
    
    pqr2= randPts(3,d=[1,5]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    Arc(pqr2, ["a",90, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              //,"xsecpts",xsecpts//, "rads",rads2
              ,"frame", true //["label",range(nseg)]
              ,"markxsec", true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    LabelPt( Q(pqr2), "frame=true, markxsec=true");

    
    
    pqr3= [for(p=pqr2)p+uvN];
    Arc(pqr3, ["a",90, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"color","red", "markxsec", [1,-2,-1]
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    LabelPt( Q(pqr3), "markxsec=[1,-2,-1]");
        
}
//Arc_demo_7_markxsec();

module Arc_demo_8_closed()
{
    echom("Arc_demo_8_closed");  
	
    r=0.8;
    fn=4;
    nseg=10; 
    a=270;
    
    pqr2= randPts(3,d=[1,5]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //---------------------------------------------------
    //xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    Arc(pqr2, ["a",a, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              //,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr2), "frame=[\"label\",range(nseg)], markxsec=true");
    
    //---------------------------------------------------
    /*
       When it closes, the total segs will be nseg+1
    */
    
    pqr3= [for(p=pqr2)p+uvN];
    Arc(pqr3, ["a",a, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"color","red", "markxsec", [0,-1]
              ,"closed",true 
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr3), "markxsec=[1,-2,-1]");    

    //---------------------------------------------------
    // We will make it looks smooth like a ring:
    ac = 360/(nseg+1)*nseg;
    pqr4= [for(p=pqr3)p+uvN];
    Arc(pqr4, ["a",ac, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"color","red", "markxsec", [0,-1]
              ,"closed",true 
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr4), "markxsec=[1,-2,-1]");  

    //---------------------------------------------------
    
    pqr5= [for(p=pqr4)p+uvN*2];
    Arc(pqr5, ["a",120, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"color","red", "markxsec", [0,-1]
              ,"closed",true 
              ]); 
	MarkPts(pqr5, ["chain",true, "transp", 1]);
    LabelPt( Q(pqr5), "Can't close if angle too small (a=120)");      
} 

//Arc_demo_8_closed();

module Arc_demo_8_closed2()
{
    echom("Arc_demo_8_closed2");  
	
    r=0.8;
    fn=4;
    nseg=10; 
    a=270;
    aclosed = 360/(nseg+1)*nseg;
    
    pqr2= randPts(3,d=[-3,-6]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //---------------------------------------------------
    //xsecpts= [[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]];
    xsecpts= [[ 2,0],[2,2],[0,2],[0,0]];
    
    Arc(pqr2, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.6, "r",r, "rad",4
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", true //["label",range(nseg)]
              //,"markxsec", true
              ,"closed",true
              ,"echopts",true
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr2), "frame=[\"label\",range(nseg)], markxsec=true");

    //---------------------------------------------------
    xsecpts2= [[0,0],[ 2,0],[3,1],[3,2],[2,3]
             ,[ 1.5,4],[2,5],[4,6], [5,6],[ 4,6.6]
             , [3,6.5],[2,6.3],[1,5.5],[0,3.5]
             ];
    
    pqr3= trslN( pqr2, 7); 
    Arc(pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.8, "r",r, "rad",4
              ,"xsecpts",xsecpts2//, "rads",rads2
              //,"frame", true//["label",range(nseg)]
              //,"markxsec", true
              ,"showxsec2d", true
              //,"closed",true
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    
    pqr4= trslN( pqr3, 12);
    Arc(pqr4, ["a",aclosed, "fn",fn, "nseg",nseg,"rad",2, "rad",4
              ,"xsecpts",xsecpts2//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", true
              //,"showxsec2d", true
              ,"closed",true
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    
} 

//Arc_demo_8_closed2();

module Arc_demo_9_rads_ellipse()
{ 
    echom("Arc_demo_9_rads_ellipse");
    /*
       2D Formula of an ellipse around the ORIGIN:
      
       (x/a)^2 + (y/b)^2 = 1    (a,b: r on x,y)
    
       In order to get the angle-dependent distance 
       (between center and the curve bone), we need 
       to find functions: 
    
            x=f(g) and y=f(x)
    
       So, 
    
       (y/b)^2 = 1-(x/a)^2
                  _____________ 
        y/b  =  ./( 1-(x/a)^2 )
                  _____________
        y =  b  ./( 1-(x/a)^2 )   //<==== got one 
                                ______________ 
        So each pt is: [ x,  b./(1-x/a)(1+x/a) ]
        
        The distance from [0,0] to pt is:
              ________________________
        d=  ./ x^2 + b^2(1-x/a)(1+x/a) 
    
        Since for each pt [X,Y], 
 
        x = cos(g)*d
              ________________________
        d=  ./ x^2 + b^2(1-x/a)(1+x/a)  = x/cos(g)
    
        x^2 + b^2(1-x/a)(1+x/a) = x^2/cos(g)^2
        x^2 + b^2(1-(x/a)^2) = x^2/cos(g)^2
        x^2 + b^2- b^2(x/a)^2 = x^2/cos(g)^2
        x^2 + b^2- (bx/a)^2 = x^2/cos(g)^2
        x^2 + b^2- (b/a)^2x^2 = x^2/cos(g)^2
        b^2  = x^2/cos(g)^2 + (b/a)^2x^2 -x^2
        b^2  = (1/cos(g)^2 + (b/a)^2 -1) x^2
        x^2 = b^2 / (1/cos(g)^2 + (b/a)^2 -1) 
                  _________________________  
        x = b / ./(1/cos(g)^2 + (b/a)^2 -1)  //<==== solved 
        
    */
    
    nseg = 47;
    
    pqr3= randPts(3,d=[3,7]);
    a = 4; //dist(p01(pqr3));  // ellipse rad x
    b = a/1.5;  // ellipse rad y
    
    rads= [ for(i=range(nseg+1)) 
              let(g= i*360/nseg
                 ,x= b / sqrt( 1/pow(cos(g),2) + pow(b/a,2)-1 )
                 ,y= b * sqrt( (1-x/a)*(1+x/a) )
                 )
              sqrt( pow(x,2)+pow(y,2))
          ];
    
    echo(rads= arrprn(rads,dep=21) );
    
    xsecpts=[ [0,0], [2,0],[3,1], [1.8,1.2], [0,2.5]];
    // We will make it looks smooth like a ring:
    ac = 360/(nseg+1)*nseg;
    Arc(pqr3, ["a",ac, "fn",4, "nseg",nseg ,"transp",.9
              ,"rads", rads, "xsecpts",xsecpts
              //,"frame", ["label",false]
              ,"markxsec",[0,-1]
              ,"closed", true
              ,"showxsec2d", true
              ]); 
	//MarkPts(pqr3, ["chain",true, "transp", 0.3]);    
    //LabelPt(Q(pqr3),"Set rads (main curve pts)");

    
//    pqr4= trslN(pqr3,3); //randPts(3);
//    rads2= [ for(i=range(25)) sin(i*60)/10+0.2+ d01(pqr4) ];
//    //echo(rads=rads2);
//    Arc(pqr4, ["a",120, "fn",6, "rP", 1, "rR",0.01
//              , "nseg",24 ,"transp",1
//              ,"rads", rads2, "color","red"
//              ]); 
//	MarkPts(pqr4, ["chain",true, "transp", 0.3]);        
//    LabelPt(Q(pqr4),"Set rads & rP, rR");
 
}
//Arc_demo_9_rads_ellipse();

module Arc_demo_10_radpts() // radpts not very successful
{                           // This should be Chain's territory
    echom("Arc_demo_10_radpts");
        
    nseg = 47;
    
    pqr3= randPts(3,d=[3,7]);
    
    xsecpts=[ [0.5,-0.5], [3,0],[3,1], [1.8,1.2], [0,2.5]];
    MarkPts( addz(xsecpts) 
           , ["label",range(xsecpts), "chain",true] );
    radpts = [ for(p=xsecpts) p*3 ];
    echo(radpts=radpts);
    // We will make it looks smooth like a ring:
    ac = 360/(nseg+1)*nseg;
    Arc(pqr3, ["a",ac, "fn",4, "nseg",nseg ,"transp",0.3, "r",0.8
              ,"radpts", radpts//, "xsecpts",xsecpts
              //,"frame" //, ["label",false]
              ,"markxsec",[0,-1]
              //,"closed", true
              ,"showxsec2d", true
              ,"echopts",true
              ]); 
//	MarkPts(pqr3, ["chain",true, "transp", 0.3]); 

}
//Arc_demo_10_radpts();

module Arc_demo_11_sealed()
{
    echom("Arc_demo_11_sealed");  
	
    r=0.8;
    fn=4;
    nseg=4; 
    a=270;
    aclosed = 360/(nseg+1)*nseg;
    
    pqr2= randPts(3,d=[-3,-6]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //---------------------------------------------------
    xsecpts= [[ 3,0],[3,0.3],[2,1],[1.5,2],[1,2]
             /*,[1,0.2], [0.9,-0.1],[0.5,-0.2]*/, [0.5,0]  ,[0,-0.5]
             /* , [0.7,-0.6], [0.9,-0.8]*/,[1,-1],[2,-1],[2,0]];
    
    Arc(pqr2, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1, "r",r, "rad",4
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", range(nseg+1)
              ,"closed",true
              ,"showxsec2d",true
              ,"sealed",[5,6]
                //["label", ["isxyz",true,"scale",0.01]] 
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr2), "frame=[\"label\",range(nseg)], markxsec=true");
    ColorAxes();
    
    
    pqr3= trslN(pqr2,8);
    Arc(pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1, "r",r, "rad",4
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", range(nseg+1)
              ,"closed",true
              ,"showxsec2d",true
              ,"sealed",[6,7]
                //["label", ["isxyz",true,"scale",0.01]] 
              ]); 
	MarkPts(pqr3, ["chain",true, "transp", 1]);

    
    pqr4= trslN(pqr3,8);
    Arc(pqr4, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1, "r",r, "rad",4
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg)]
              //,"markxsec", range(nseg+1)
              //,"closed",true
              ,"showxsec2d",true
              //,"sealed",[6,7]
                //["label", ["isxyz",true,"scale",0.01]] 
              ]); 
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    
    //---------------------------------------------------
//    xsecpts2= [[0,0],[ 2,0],[3,1],[3,2],[2,3]
//             ,[ 1.5,4],[2,5],[4,6], [5,6],[ 4,6.6]
//             , [3,6.5],[2,6.3],[1,5.5],[0,3.5]
//             ];
//    
//    //---------------------------------------------------
//    pqr3= trslN( pqr2, 7); 
//    Arc(pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.8, "r",r, "rad",4
//              ,"xsecpts",xsecpts2//, "rads",rads2
//              ,"frame", ["label",range(nseg)]
//              ,"markxsec", true
//              ,"showxsec2d", true
//              //,"closed",true
//              ]); 
//	MarkPts(pqr3, ["chain",true, "transp", 1]);
//    
//    //---------------------------------------------------
//    pqr4= trslN( pqr3, 12);
//    Arc(pqr4, ["a",aclosed, "fn",fn, "nseg",nseg,"rad",2, "rad",4
//              ,"xsecpts",xsecpts2//, "rads",rads2
//              //,"frame", ["label",range(nseg)]
//              //,"markxsec", true
//              //,"showxsec2d", true
//              ,"closed",true
//              ]); 
//	MarkPts(pqr4, ["chain",true, "transp", 1]);
    
} 

//Arc_demo_11_sealed();

module Arc_demo_12_showobj_frame()
{
    echom("Arc_demo_12_showobj_frame");  
	
    r=0.8;
    fn=4;
    nseg=3; 
    a=270;
    aclosed = 360/(nseg+1)*nseg;
    
    pqr2= randPts(3,d=[-3,-6]);
    uvN = uv( pqr2[1], N(pqr2) )*5;
    
    //---------------------------------------------------
    xsecpts= [[ 3,0],[3,0.3],[2,1],[1.5,2],[1,2]
             /*,[1,0.2], [0.9,-0.1],[0.5,-0.2]*/, [0,-0.2]  ,[0,-0.5]
             /* , [0.7,-0.6], [0.9,-0.8]*/,[1,-1],[2,-1],[2,0]];
    
    Arc(pqr2, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.5, "r",r, "rad",3
              ,"xsecpts",xsecpts//, "rads",rads2
//              ,"frame", ["label",range(nseg)]
//              ,"markxsec", range(nseg+1)
              ,"closed",true
              ,"showxsec2d",true
              ,"sealed",[5,6]
              ]); 
	MarkPts(pqr2, ["chain",true, "transp", 1]);
    //LabelPt( Q(pqr2), "frame=[\"label\",range(nseg)], markxsec=true");
    //ColorAxes();
    //---------------------------------------------------

//    //---------------------------------------------------
    pqr3= trslN( pqr2, 7); 
    Arc(pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.1, "r",r, "rad",3
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg),"chain",["r",0.05,"closed",true]]
              ,"markxsec", range(nseg+1)
              ,"closed",true
              //,"showxsec2d",true
              //,"sealed",[5,6]
              ,"frame",true
              ,"hide",true
              ]);
	MarkPts(pqr3, ["chain",true, "transp", 1]);
    
//    //---------------------------------------------------

    pqr4= trslN( pqr3, 7); 
    Arc(pqr4, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",0.1, "r",r, "rad",3
              ,"xsecpts",xsecpts//, "rads",rads2
              //,"frame", ["label",range(nseg),"chain",["r",0.05,"closed",true]]
              //,"markxsec", range(nseg+1)
              ,"closed",true
              //,"showxsec2d",true
              ,"frame",true
              //,"sealed",[5,6]
             // ,"hide",true
              ]);
	MarkPts(pqr4, ["chain",true, "transp", 1]);
    
//    //--------------------------------------------------- 

    axisln = linePts( [ Q(pqr2),Q(pqr4)], 5);
    Line0( axisln, ["r",0.02,"color","red"] );
} 
//Arc_demo_12_showobj_frame();

module Arc_demo_13_markxsec_plane(){
    
    echom("Arc_demo_13_markxsec_plane");
    
    r=0.8;
    fn=4;
    nseg=8; 
    a=270;
    aclosed = 360/(nseg+1)*nseg;
    
    xsecpts= [[0,-0.8],[1,-0.95],[2,-1.2],[3,-1],[3.5,-0.5],[3.7,-0.2]
             ,[3.7,0.2],[3.5,0.5],[3,1],[2,1.2],[1,0.95],[0,0.8]
             ];
    
    
    pqr= randPts(3, d=[-4,-7]);
    Arc( pqr, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1
              , "r",r, "rad",2
              ,"xsecpts",xsecpts
              ,"closed",true
              ,"showxsec2d",true
              ]);    
    MarkPts(pqr, "ch;l"); 
 
 
 
    pqr1=trslN( pqr, 5);
    Arc( pqr1, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1
              , "r",r, "rad",2
              ,"xsecpts",xsecpts
              ,"closed",true
              //,"showxsec2d",true
              
              ,"hide",true
              ,"markxsec", true
              ,"xsecopt", ["ball",false
                          , "chain",false
                          , "plane",["th",0.3,"color","red"]]
              ]);    
    MarkPts(pqr1, "ch;l"); 
 

    pqr2=trslN( pqr1, 4);
    Arc( pqr2, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1
              , "r",r, "rad",2
              ,"xsecpts",xsecpts
              ,"closed",true
              //,"showxsec2d",true
              
              ,"hide",true
              ,"markxsec", true
              ,"xsecopt", ["ball",false
                          , "chain",false
                          , "plane",["th",0.1, "color", "green"
                                    , "rotate",30
                                    ,"rotaxis", [0,-1]
                                    ]]
              
              ]);    
    MarkPts(pqr2, "ch;l"); 
  

    pqr3=trslN( pqr2, 5);
    Arc( pqr3, ["a",aclosed, "fn",fn, "nseg",nseg ,"transp",1
              , "r",r, "rad",2
              ,"xsecpts",xsecpts
              ,"closed",true
              //,"showxsec2d",true
              
              ,"hide",true
              ,"markxsec", true
              ,"xsecopt", ["ball",false
                          , "chain",false
                          , "plane",["th",0.1, "color", "blue"
                                    , "rotate",30
                                    ,"rotaxis", [ 0, 6]
                                    ]]
              
              ]);    
    MarkPts(pqr3, "ch;l"); 
 
  //--------------------------------------------------- 

    axisln = linePts( [ Q(pqr),Q(pqr3)], 5);
    Line0( axisln, ["r",0.1,"color","black"] ); 
}
//Arc_demo_13_markxsec_plane();


//========================================================
Arrow=["Arrow","pq, ops", "n/a", "geometry"
, "



"];

/*
ERRMSG = [
  "rangeError"
    , "<br/>|  Index {iname}={index} in {fname}() out of range of {vname} (len={len})"
 ,"typeError"
    , "<br/>|  {vname} in {fname}() should be {tname}. A {vtype} given ({val}) "
 ,"arrTypeError"
    , "<br/>|  Array {vname} in {fname}() does NOT allow item of type {tname}. ({vname}: {val})"
];
function errmsg(errname,data)=
let ( _v= str(hash(data,"val"))
    , v = len(_v)>30? str( slice(_v,0, 30),"..."): _v
    , _data = update( data , ["val", v] )
    , data = [ for(i=range(_data))
                isint(i/2)? _data[i]: str("<u>", _data[i], "</u>") ] 
    )
(
  str("  [<u>",errname, "</u>] : ", _h( hash(ERRMSG, errname), data) )
);
    
function typeErr(data)= errmsg("typeError", data);
*/    

//module Arrow_b4_201507(pq, ops=[])
//{
//   // _mdoc("Arrow","");
//    //echo( pq= pq );
//    if( !isarr(pq)||!isarr(pq[0])||!isarr(pq[1]))
//       echo( str(typeErr( [ "fname","Arrow"
//                      , "vname", "pq"
//                      , "vtype", [type(pq), type(pq[0]), type(pq[1])]
//                      , "tname", "array of 3 arrays"
//                      , "val", pq ] ), ", pq =", val) ); 
//    df_fn = $fn==0?6:$fn;    
//    com_keys= [ "color","transp","fn"];
//    
//    df_ops=[ "r",0.02
//           , "fn", df_fn
//           , "heads",true
//           , "headP",true
//           , "headQ",true
//           , "debug",false
//           , "twist", 0
//           ];
//    
//    _ops= update( df_ops, ops );    
//       
//    function ops(k)= hash(_ops,k);
//    fn = ops("fn");
//    r  = ops("r");
//    L  = d01(pq);
//    
//    //======================================== arrow
//    // Decides arrow r and len automatically (based on line r)
//    // when they are not set by user:
//    arr_l = min(10*exp(-0.5*r)*r, 2*L/5);    
//                                // 6*r, but can't be too large 
//    arr_r = (3*exp(-0.8*r)) *r; // Must be >r, but with certain ratio
//                                // that is large ( 3x ) @ small r but
//                                // smaller @ large r. So we use a 
//                                // exponential function 
//    df_heads= ["r", arr_r, "len", arr_l, "fn", fn];
//    df_headP= [];
//    df_headQ= [];
//    
//    /*
//        _-'|          |'-_ 
//      P----A----------B----Q
//        '-_|          |_-'  
//           '          '
//    */
//    
//    // ops_arrow 
//    
//    u_hs = ops("heads");  // user input
//    u_hp = ops("headP");  // user input
//    u_hq = ops("headQ");  // user input
//    
////    fmt = [ "color", ops("color")    // The 
////          , "transp", ops("transp") 
////          , "fn", ops("fn")];
////    ops_arrowP =  concat( isarr(ap)?ap:[], isarr(arw)?arw:[], fmt, df_arrowP);
////    ops_arrowQ =  concat( isarr(aq)?aq:[], isarr(arw)?arw:[], fmt, df_arrowQ);
////
//    ops_headP= getsubops( ops, df_ops=df_ops
//                , com_keys= com_keys
//                , sub_dfs= ["heads", df_heads, "headP", df_headP ]
//                );
//                
//    ops_headQ= getsubops( ops, df_ops=df_ops
//                , com_keys= com_keys
//                , sub_dfs= ["heads", df_heads, "headQ", df_headQ ]
//                );
////
////    //echo( "ops_headP", ops_headP);
////    //echo( "ops_headQ", ops_headQ);
////    
//    function ops_headP(k) = hash( ops_headP,k );
//    function ops_headQ(k) = hash( ops_headQ,k );
//    
//    headpq = linePts( pq 
//                     , len=[ u_hs && u_hp?-ops_headP("len"):0 
//                           , u_hs && u_hq?-ops_headQ("len"):0]
//                     );
//    //echo("_ops",_ops);
//    
//    linepq = linePts( headpq, len= [u_hp?GAPFILLER:0
//                                   , u_hq?GAPFILLER:0 ] );    
//    
//    R= len(pq)>2? pq[2]:randPt();
//    //echo("arrow, concat(linepq,[R])",concat(linepq,[R]));
//    
//    //====================================== line 
//    Line( linepq, _ops);
//    //Line( concat(linepq,[R]), _ops);
//    //======================================
//    
//    
//    if(u_hs && u_hp) Cone( [ headpq[0], pq[0], R ], ops_headP );
//    if(u_hs && u_hq) Cone( [ headpq[1], pq[1], R ], ops_headQ );
//    
//}    
//


module Arrow(pq, opt=[])
{
    echom("Arrow");
   // _mdoc("Arrow","");
    //echo( pq= pq );
    if( !isarr(pq)||!isarr(pq[0])||!isarr(pq[1]))
       echo( str(typeErr( [ "fname","Arrow"
                      , "vname", "pq"
                      , "vtype", [type(pq), type(pq[0]), type(pq[1])]
                      , "tname", "array of 3 arrays"
                      , "val", pq ] ), ", pq =", val) ); 
    df_fn = $fn==0?6:$fn;    
    com_keys= [ "color","transp","fn"];
    
    df_opt=[ "r",0.02
           , "fn", df_fn
           , "nside", 4
           , "heads",true
           , "headP",true
           , "headQ",true
           //, "debug",false
          // , "twist", 0
           ];
    
    _opt= update( df_opt, opt );    
       
    function opt(k)= hash(_opt,k);
    fn = opt("fn");
    nside=opt("nside");
    r  = opt("r");
    L  = d01(pq);
    
    //======================================== arrow
    // Decides arrow r and len automatically (based on line r)
    // when they are not set by user:
    arr_l = min(10*exp(-0.5*r)*r, 2*L/5);    
                                // 6*r, but can't be too large 
    arr_r = (3*exp(-0.8*r)) *r; // Must be >r, but with certain ratio
                                // that is large ( 3x ) @ small r but
                                // smaller @ large r. So we use a 
                                // exponential function 
    df_heads= ["r", arr_r, "len", arr_l, "fn", fn];
    df_headP= [];
    df_headQ= [];
    
    /*
        _-'|          |'-_ 
      P----A----------B----Q
        '-_|          |_-'  
           '          '
    */
    
    // opt_arrow 
    
    u_hs = opt("heads");  // user input
    u_hp = opt("headP");  // user input
    u_hq = opt("headQ");  // user input
    
//    fmt = [ "color", opt("color")    // The 
//          , "transp", opt("transp") 
//          , "fn", opt("fn")];
//    opt_arrowP =  concat( isarr(ap)?ap:[], isarr(arw)?arw:[], fmt, df_arrowP);
//    opt_arrowQ =  concat( isarr(aq)?aq:[], isarr(arw)?arw:[], fmt, df_arrowQ);
//
    opt_headP= getsubops( opt, df_ops=df_opt
                , com_keys= com_keys
                , sub_dfs= ["heads", df_heads, "headP", df_headP ]
                );
                
    opt_headQ= getsubops( opt, df_ops=df_opt
                , com_keys= com_keys
                , sub_dfs= ["heads", df_heads, "headQ", df_headQ ]
                );
//
//    //echo( "opt_headP", opt_headP);
//    //echo( "opt_headQ", opt_headQ);
//    
    function opt_headP(k) = hash( opt_headP,k );
    function opt_headQ(k) = hash( opt_headQ,k );
    
    headpq = linePts( pq 
                     , len=[ u_hs && u_hp?-opt_headP("len"):0 
                           , u_hs && u_hq?-opt_headQ("len"):0]
                     );
    //echo("_opt",_opt);
    
    linepq = linePts( headpq, len= [u_hp?GAPFILLER:0
                                   , u_hq?GAPFILLER:0 ] );    
    
    R= len(pq)>2? pq[2]:randPt();
//    //echo("arrow, concat(linepq,[R])",concat(linepq,[R]));
//    
//    //====================================== line 
      //Chain( linepq, _opt);
//    //Line( concat(linepq,[R]), _opt);
//    //======================================
//    
//    
    /*
       Make a randchain to handle the arrow head/tail by
       adjusting the lens and rs of randchain:
       
          len     len      len    
             .             .
          _-`|             |`-_    
         +---+-------------+---+
          `-_|             |_-`
             `             ` 
        rs= [0, [r,0], [0,r],0]
    */
    isP= (u_hs && u_hp);
    isQ= (u_hs && u_hq);
    Lp = opt_headP("len");
    Lq = opt_headQ("len");
    rp = opt_headP("r");
    rq = opt_headQ("r");
    
    inL = L - (isP? Lp:0)- (isQ? Lq:0);
    
    lens= concat( isP?[Lp]:[], [inL], isQ?[Lq]:[] );
    rs  = concat( isP?[-r]:[0]
                , isP?[[rp,0]]:[]
                , isQ?[[0,rq]]:[]
                , isQ?[-r]:[0] );          
    echo(lens = lens);
    echo(rs = rs );
    echo(r=r);
    seed= app(pq,randPt());
    echo(seed=seed);
    //MarkPts(seed, "ch");
    chrtn = chainPts( seed= seed, shape="cone"
         , n= 2+(isP?1:0)+(isQ?1:0)
         , r=r, r2=r, nside=nside, lens=lens,rs=rs
         , a=180, a2=0, opt=opt  );
         
    echo(chrtn = chrtn);
    csecs = hash( chrtn, "csecs");
    echo( csecs = arrprn( csecs,2 ) );
    
//    for(i = range(csecs))
//    {
//        if(len( csecs[i])>1) MarkPts( csecs[i], "ch;ball=false");
//    }       
    
    for( i = range(csecs[1]) ){
        line=  [for(j=[1:len(csecs)-1]) csecs[j][i]];   
        echo(i=i, line=line);    
        MarkPts( line, "ch;ball;r=0.01");
    }   
//    Chain( seed= seed, shape="cone"
//         , n= 2+(isP?1:0)+(isQ?1:0)
//         , r=r, r2=r, nside=nside, lens=lens,rs=rs
//         , a=180, a2=0, opt=opt  );
    
}    




module Arrow_demo_1()
{
    echom("Arrow_demo_1");
// _mdoc("Arrow_demo_1"
//, "Yellow: Arrow( randPts(2) )
//");   
    pq = randPts(2);
    //MarkPts( pq, ["labels", "PQ", "colors",["red"]] );
    //Chain( pqr, ["closed",false]);
    Arrow( pq, opt=["body",false,"frame",true] );
    
//    pq2 = randPts(2);
//    //MarkPts( pq2, ["labels", "PQ", "colors",["green"]] );
//    Arrow( pq2, [ "color","green"
//                , "fn", 6, "r",0.3
//                ]);
//    
//    LabelPt( pq[0], " Arrow(pq)");
//    LabelPt( pq2[0], 
//    " Arrow(pq,[\"fn\",6,\"r\",0.3,\"color\",\"green\"])"
//    ,["color","green"]);
}    
//Arrow_demo_1();

module Arrow_demo_2_defaultArrowSettings()
{
 _mdoc("Arrow_demo_2_autoArrowRad"
, "The arrow rad and len are auto set, if not specified. 
;; The following are Arrow( pq, [''r'',r]) 
;; at  r= 0.01, 0.025, 0.05, 0.1, 0.2, 0.3, 0.5
;; This could be used as a guide line to set arrow rad/len.
");   

    rs= [0.01, 0.025, 0.05, 0.1, 0.2, 0.3, 0.5];
    
    LabelPt( [2,-1,4],
      "Arrows with different r:[0.01,0.025,0.05,0.1,0.2,0.3,0.5]"
    );
    
    for( i = range(rs) )
    {  pq = randPts(2);
       r = rs[i];
       L = d01(pq); 
       echo( "r", rs[i]
           , "arr_r", (3*exp(-0.8*r)) *r
           , "arr_l", min(10*exp(-0.5*r)*r, 2*L/5) 
           );   
       Arrow( pq, ["r", rs[i]]);
    }
  
}     

module Arrow_demo_3_lineProp()
{
 _mdoc("Arrow_demo_3_lineProp"
," Yellow: Arrow( pq, [''r'', 0.1, ''fn'',8, ''transp'', 0.8 ])
;; Red: Arrow( pq, [''r'', 0.1, ''heads'', false, ''color'',''red''] )
;; Green: Arrow( pq, [''r'', 0.1, ''headP'', false, ''color'',''green''] )
;; Blue: Arrow( pq, [''r'', 0.1, ''headQ'', false, ''color'',''blue''] )
"); 
//    Arrow(randPts(2));
//    Arrow( randPts(2)
//         , ["r", 0.1, "fn",8, "transp", 0.8 ]);   
    
    pq0= randPts(2);
    //MarkPts(pq0);
    //LabelPt( pq0[0], "  heads=false", ["color","red"]);
    Arrow( pq0        , ["r", 0.1, "heads", false, "color","red"] );

    pq= randPts(2);
    MarkPts(pq);
    LabelPt( pq[0], "  P (headP=false)", ["color","green"]);
    Arrow( pq
        , ["r", 0.1, "headP", false, "color","green"] );

    pq2= randPts(2);
    MarkPts(pq2);
    LabelPt( pq2[1], "  Q (headQ=false)", ["color","blue"]);
    Arrow( pq2
        , ["r", 0.1, "headQ", false, "color","blue"] );
 
}    
module Arrow_demo_4_arrowProp()
{
 _mdoc("Arrow_demo_4_arrowProp"
," Yellow: Arrow( pq, [''r'', 0.1,  ''headP'', [''color'',''red'']])
;; Red: Arrow( pq, [''r'', 0.1, ''color'',''blue''
;;                 , ''headQ'', [''color'',''green''
;;                               ,''r'', 0.5
;;                               ,''len'', 1.5
;;                               ,''fn'', 4
;;                               ]] )
"); 

    pq= randPts(2);
    MarkPts(pq);
    LabelPts( pq, [" P", " Q"] );
    Arrow( pq
         , ["r", 0.1,  "headP", ["color","red"]] );
    LabelPt( pq[0], "   Arrow(pq,[\"r\",0.1,\"headP\",[\"color\",\"red\"]])");
    
    pq2= randPts(2);
    MarkPts(pq2);
    LabelPts( pq2, [" P", " Q"] );
    
    Arrow( pq2
        , ["r", 0.1, "color","blue"
          , "headQ", ["color","green"
                      ,"r", 0.5
                      ,"len", 1.5
                      , "fn", 4
    ]] );
    
    color("blue")
    {LabelPt( pq2[0], "   Arrow( pq2, [\"r\", 0.1, \"color\",\"blue\" ");
    LabelPt( addy(pq2[0],-1.5), "   , \"headQ\", [\"color\",\"green\",\"r\",0.5,\"len\",1.5,\"fn\",4]])");
    }                  
 
}   
//Arrow_demo_1();    
//Arrow_demo_2_defaultArrowSettings();    
//Arrow_demo_3_lineProp();     
//Arrow_demo_4_arrowProp();    
    
    

////========================================================
//Block=[ "Block","pq,ops", "n/a", "3D, shape",
// " Given a 2-pointer pq, make a block that has its one edge lies on
//;; or parallel to the pq line. The block size is determined by pq,
//;; width and depth. It can rotate by an angle defined by ''rotate''
//;; about the pq-line. Default ops:
//;;
//;;  [ ''color'' , ''yellow''
//;;  , ''transp'', 1
//;;  , ''rotate'', 0           // rotate angle alout the line
//;;  , ''shift'' , ''corner''  //corner|corner2|mid1|mid2|center
//;;  , ''width'' , 1
//;;  , ''depth'' , 6
//;;  ]
//;;
//;; The shift is a translation decides where on the block it attaches
//;; itself to pq. It can be any [x,y,z], or one of the following strings:
//;;
//;;    corner       mid1          mid2         center
//;;    q-----+      +-----+      +-----+      +-----+
//;;   / :     :    p :     :    / q     :    / :     :
//;;  p   +-----+  +   +-----+  +   +-----+  + p +-----+
//;;   : /     /    : q     /    p /     /    : /     /
//;;    +-----+      +-----+      +-----+      +-----+
//;;
//;;    corner2
//;;    +-----+
//;;   / :     :
//;;  +   q-----+
//;;   : /     /
//;;    p-----+
//"];
//
//module Block(pq,ops){
//	Line( pq, update(ops, ["style", "block"]) );
//}
//
//module Block_test(ops){ doctest(Block, ops=ops);}
//
//module Block_demo_1()
//{
//	echom("Block_demo_1");
//	pq= randPts(2);
//	//pq = [ [2,1,3], [4,3,2]];
//	MarkPts(pq);
//	Block(pq);
//}
//
//module Block_demo_2()
//{
//	echom("Block_demo_2");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]], ["width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], update(ops, ["color","red"]));//
//	Block([[x-w,0,0], [x-w,0,z]], update(ops, ["color","green"]) );
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//	Block( [ [x, 0, z], [0, 0, z+1]], update(ops, ["color","blue", "rotate",360*$t] ) );
//	
//}
//
//
//module Block_demo_3()
//{
//	echom("Block_demo_3");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]]
//					   , [ "width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift","corner2" // <====
//					  ]) );
//
//
//}
//
//module Block_demo_4()
//{
//	echom("Block_demo_4");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]]
//					   ,[ "width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift","mid1" // <====
//					  ]) );
//
//}
//
//module Block_demo_5()
//{
//	echom("Block_demo_5");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]]
//					   , ["width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift","mid2" // <====
//					  ]) );
//}
//
//
//module Block_demo_6()
//{
//	echom("Block_demo_6");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]],[ "width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift","center" // <====
//					  ]) );
//}
//
//
//module Block_demo_7()
//{
//	echom("Block_demo_7");
//	x=5; 	y=6; 	z=4; 	w=0.5;
//
//	ops= [ "width",0.5
//		 , "depth",4
//		 , "transp",0.5
//		 ];
//
//	Block([O, [0,0,z+1]]
//					   , ["width",0.5
//					   , "depth",4]
//					  );
//	Block([[0,0,w], [x,0,w]], ops);
//	Block([[x-w,0,0], [x-w,0,z]], ops);
//
//	Line( [ [x, 0, z], [0, 0, z+1]] );
//	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
//
//	Block( [ [x, 0, z], [0, 0, z+1]]
//		, update(ops, ["color","blue", "rotate",360*$t
//						,"shift",[2*w, 0,w] // <====
//					  ]) );
//}
//
//
//module Block_demo()
//{
//	//Block_demo_1();
//	//Block_demo_2();
//	Block_demo_3();
//	//Block_demo_4();
//	//Block_demo_5();
//	//Block_demo_6();
//	//Block_demo_7();
//}
////Block_test( ["mode",22] );
////Block_demo();
////doc(Block);
//
//



//// shape========================================================


Chain=["Chain","pts,ops", "n/a", "3D, Shape",
 " Given array of pts (pts) and ops, make a chain of line
;; segments to connect points. ops:
;;
;;  [ ''r''     , 0.05
;;  , ''closed'',true
;;  , ''color'' ,false
;;  , ''transp'',false
;;  , ''usespherenode'',true
;;  ]
"];	



module Chain_test(ops){ doctest(Chain,ops=ops);}



module Chain0( pts, opt=[])
{
    echom("Chain0");
    
    df_opt = [ "r", 0.008
             , "nside", 4
             , "closed",false
             , "Rf",undef
             ];
    opt = update( df_opt, opt );

    chrtn = chainPts0( pts=pts, opt=opt );
    csecs = hash(chrtn, "csecs");
    
    //echofh(chrtn);
    //echo(debug = hash( chrtn, "debug"));
    
    //echo( csecs = arrprn( csecs ) );
    
    Rf0 = hash(chrtn, "Rf0");
    //echo(Rf0 = Rf0);
    //MarkPt(Rf0, "r=0.3;trp=0.3" );
    
    
    
    //for( xs = csecs ) MarkPts( xs ); //Chain( xs , opt="closed");
        
    faces = faces( shape= hash(opt,"closed")? "ring":"chain"
                 , nside= hash(opt,"nside")
                 , nseg = len(pts)-1
                 );
    //echo( faces = faces );
    
    color( hash( opt, "color")
         , hash( opt, "transp")
         )    
    polyhedron( points= joinarr(csecs)             
              , faces=faces );
}

module Chain0_demo(){
    
   echom("Chain0_demo"); 
   pqr= randPts(5); //pqr();
   //pqr = [[2.3226, -2.58512, -0.910189], [-2.53233, -2.73718, -0.367593], [-1.73259, -0.215232, -2.55716]];
   echo( pqr = pqr );  
   Chain0( pqr ); 
   //MarkPts( pqr, "r=0.2" ); 
    
   //plat1rtn = planeDataAt( pqr, 1 );
   //plat1 = hash(plat1rtn,"pl"); 
   //echo(plat1=plat1);
   //MarkPts(plat1, "pl");
    
   //MarkPt( [2.65243, -0.343472, 2.42548] );
    
   //Chain0( pqr(), ["color","red", "r", 0.05] ); 
    
}    
//Chain0_demo();


module Chain_old( chrtn, opt=[])
{
    echom("Chain");
    //if(chrtn){
    //echo( chrtn= chrtn);
    
    u_opt = popt(opt);
    df_opt = [ "nside", 4
             , "closed",false
             , "body", true
             ];
    opt = update( df_opt, u_opt );
    function opt(k,nf)= hash(opt, k, nf);
    //echo("Chain", opt=opt);
    closed = opt("closed");
    nside  = opt("nside");
    
    // -------------------------------------------    
    // If the 1st item or chrtn is a pt, then chrtn is 
    // the bone and need to go thru chainPts(pts ...) 
    // to get chrtn, which is:
    //    ["csecs", finalrtn
    //     ,"xpts0", xpts
    //     ,"bone", pts
    //     ,"hcut", [hcut,hcutax]
    //     ,"tcut", [tcut,tcutax]
    //     ,"Rf", Rf
    //     ]
    // where finalrtn is a list of xpts
    // 
    _chrtn = haskey( chrtn, "csecs")? chrtn
            : haskey( chrtn, "pts")? 
                chainPts( hash( chrtn, "pts"), opt=opt)
            : ispt(chrtn[0])? chainPts(chrtn,opt=opt) // 
            : chrtn ;
            
    //echo( _chrtn= arrprn(_chrtn,dep=3, nl=true) );
    
    xsecs = hash(_chrtn, "xsecs");
    //echo( xsecs = arrprn( xsecs, 2 ) );
    bone = hash(_chrtn, "bone");
    frmlines = transpose( xsecs );
    pts = joinarr( xsecs );
    seed=  hash(_chrtn, "seed"); //app( p01(bone), hash(chrtn,"Rf"));
    nseg = len( xsecs )-1; 
//    echo( str("<br/><b> xsecs= </b>", xsecs
//             ,"<br/><b> bone= </b>", bone
//             ,"<br/><b> frmlines= </b>", frmlines
//             ,"<br/><b> pts= </b>", pts
//             ,"<br/><b> seed= </b>", seed
//             ,"<br/><b> nseg= </b>", nseg
//             )); 
    //echo( xsecs = arrprn(xsecs,3,nl=true) );
    // -------------------------------------------    
    //MarkPts(seed, "pl=[trp,0.1];ball=false");
    
    subopts = subopts( 
    
          df_opt=df_opt
        , u_opt= u_opt
//        , touse =  [ "markpts"
//                    , "labels" 
//                    , "plane"  
//                    , "markxpts"   
//                    ]
        , dfs=[ "markpts", ["rng", range(pts)]
                           
              , "bone", ["r",0.06,"rng",range(bone)
                        ,"chain"
                         ,["r",0.02,"closed",closed]]
              , "seed", ["r",0.06,"rng",range(bone)
                        ,"chain",["r",0.02]]             
              , "frame", ["r",0.01, "color","green"
                         ,"rng", range(frmlines)
                         ]
              , "edges", ["r",0.01, "color","green"
                         ,"rng", range(frmlines)
                         ]
              , "markxpts", ["r",0.04
                          //, "closed",true
                          ,  "chain",["closed",true] 
                         ,"rng", range(xsecs)
                         ]
              , "pads", ["r",0.1, "color",undef
                         ,"rng", range(frmlines)
                         ]           
              ]
        , com_keys=[] 
       );
       
    function sub(subname)= hash(subopts, subname);
    
    //echo( subopts = arrprn(subopts,dep=3) );

    
    // -------------------------------------------    
    o_mpts = sub("markpts");
    if(pts && o_mpts)  {
        //echo(o_mpts=o_mpts);
        MarkPt( sel(pts,hash(o_mpts,"rng"))
                    , o_mpts );
    }    
    //MarkPts( pts, "r=0.03");   
   
    // -------------------------------------------    
    o_bone = sub("bone");
    if(bone && o_bone)  {
        echo(o_bone=o_bone, "<br/>",bone=bone, "<br/>"
            , selbone = sel(bone,hash(o_bone,"rng")));
        
        MarkPts( sel(bone,hash(o_bone,"rng"))
                    , o_bone );
    }    

    // -------------------------------------------    
    o_seed = sub("seed");
    if(seed && o_seed)  {
        echo(o_seed=o_seed, "<br/>",seed=seed);
        
        MarkPts( sel(seed,hash(o_seed,"rng"))
                    , o_seed );
    }       
    //-------------------------------------------
    o_xpts = sub("markxpts");
    //echo( o_xpts = o_xpts );
    if( xsecs && o_xpts ){
       //echo( rng = or(hash( o_xpts, "rng"), range(xsecs)) );
       echo( o_xpts = o_xpts );

         for( i= hash( o_xpts, "rng", range(xsecs))){
           echo( str("<b>",i, "</b>"
                    , arrprn(get(xsecs,i),nl=true)
                    , "<br/> opt=", delkey(o_xpts,"rng"))
               );
           MarkPts( get(xsecs,i), delkey(o_xpts,"rng"));
         } 
    }    
    
    //-------------------------------------------
    o_edges = sub("edges");
    if( frmlines && o_edges ){
       echo("edging, o_edges= ", o_edges);
       for( i= hash(o_edges, "rng", range(frmlines))){
           Chain( get(frmlines,i), o_edges);
       }         
    }    
    
    //-------------------------------------------
       
    o_frame = sub("frame");
    if(o_frame){
        if( frmlines ) { 
           for( fl= frmlines){ 
               Chain( fl, o_frame);
           }         
        }//if frmlines    
        
       if( xsecs ){
           echo("xsecing ...");
           for( xsec= xsecs){ //xsec= xsecs ) {
               //echo( i=i, xsec = xsec );
               // chaining of xsecs will be rotated by the amount
               // of 180/nside (say, 45 for nside=4) to match the
               // angle of xsecs and that of frmlines               
               Chain( xsec
                    , concat( ["closed",true], o_frame
                            , ["rot", hash(o_frame,"rot", 0)
                               +180/hash(o_frame,"nside"
                                   ,hash(opt,"nside"))
                              ] 
                            )
                    );
           }    
       }// if xsecs  
    }//if_o_frame
    
     closinglink= [  xsecs[0][0]
                    , bone[0]
                    , last(bone)
                    , last(xsecs)[0]
                  ];  
                  
     //echo( closinglink = closinglink);             
                  
     /* tailroll
    
        tailroll is the amount(int) of pts for the tailface
        to roll in order to match the tail face to the head
        face to overcome twisting in cases of closed chain
    
        twistangle 
    
     */
     //MarkPts( bone, "l");
                    
     twistangle = twistangle(closinglink);
     //twistangle = _twistangle; //?_twistangle:0;
     
     tailroll =  -floor( twistangle/ ( 360/ nside)); 
    //tailroll = 4;
     echo( nside=nside, twistangle= twistangle, tailroll = tailroll ); 

    echo(closed = closed, tailroll = tailroll );
    
//    faces= faces( closed?"ring":"chain"
//                  , nside=nside 
//                  , nseg= nseg 
//                  );
     
     faces = closed && tailroll?
             faces( "ring"
                  , nside=nside //len(xsecs[0])
                  , nseg= nseg //len(xsecs)-1 //(closed?0:1)
                  , tailroll = tailroll
                  )
             :faces( closed?"ring":"chain"
                  , nside=nside //len(xsecs[0])
                  , nseg= nseg // len(xsecs)-1 //(closed?0:1)
                  );
//
//    echo(faces = faces );
//     echo( lenfaces = len(faces), nside=nside, nseg= nseg //len(xsecs)-1
//         , expected_lenfaces=
//             closed? (nseg+1)*nside
//                   :nseg*nside+2,  faces=faces);   
//
     if(pts && opt("body"))
     {    
         //echo(len=len(faces), arrprn(faces,dep=3));
             //echo("Chain(): drawing body...");
         color(opt("color"),opt("transp"))   
         polyhedron( points= pts, faces=faces);  
     }    
  

 }
//

 
module Chain( 

     data     /* data could be: 
                   (1) pts
                   (2) chainbone return (has "pts") or 
                   (3) chaindata return (has "csecs") 
                */
     ,opt
     
    //===============================================     
    // The following are used to generate data using 
    // randchain( data,n,a,a2... ) when data is a pts.
    // They are ignored if data is (2) or (3) 
    
     , n      //=3, 
     , bone_a      //=[-360,360]
     , bone_a2     //=[-360,360]
     , seglen //= [0.5,3] // rand len btw two values
     , lens   //=undef
     , seed   //=undef // base pqr
     , smooth //= false // df:["n",10,"aH", 0.5,"dH",0.3
                     //   , "clos
                     
    //===============================================     
    // The following are used to generate data using
    // chainPts( data,n,a,a2... ) when data is a randchain
    // return. They are ignored if the data is already 
    // chainPts return. 

    , nside 
    , nsidelast 
    , r  
    , rlast 
    , rs    
    , closed 
    , twist 
    , rot   
    , Rf             
    , csec0 
    , cseclast                     
    , cut    
    , cutlast 
                    
//     , r //=1
//     , r2 //=undef
//     , rs //=[] // to be added to r 
//     , nside //=6
//     
//     , rot //=0   // The entire chain rotate about its bone
//               // away from the seed plane, which
//               // is [ p0, p1, Rf ]
//     
//     , xpts //=undef  // Define crosec. the r,r2, and rs
//                   // will be ignored, unless, see below
//     , xptratios //= undef //  ratio of r|r2|rs
//     
//     , xptadds //= false // arr of nums added to r|r2|rs
//                      // AND/OR xpts|xptratios
//     
//     
//     , Rf //=undef  // Reference pt to define the starting
//                 // seed plane from where the crosec 
//                 // starts 1st pt. Seed Plane=[p0,p1,Rf]
//                 
//     , twist //=0 // Angle of twist of the last crosec 
//               // comparing to the first crosec
//     
//     , closed //=false // If the chain is meant to be closed.
//                    // The starting and ending crosecs will
//                    // be modified. 
//     
//     , hcut //=undef//=[0,0]
//     , tcut //=undef//=[0,0] 
     
     //===============================================     
     // The following are Chain-specific, used for display
  
     
     , body
     , markpts
     , bone
     , frame
     , edges
     , markxpts
     , pads
     , shape = "chain"
     )
{
    echom("Chain");

    //
    // Prepare user opt
    //
    // Convert string opt to pure opt:
    u_opt = popt(opt); 
    //====================================================
     n      = und(n, hash(u_opt,"n"));     
     bone_a = und(bone_a, hash(u_opt,"bone_a"));    
     bone_a2= und(bone_a2, hash(u_opt,"bone_a2")); 
     seglen = und(seglen, hash(u_opt,"seglen"));
     lens   = und(lens, hash(u_opt,"lens"));
     seed   = or(seed, hash(u_opt,"seed", pqr()));
     smooth = und(smooth, hash(u_opt,"smooth"));
    
    
     _nside     = und(nside, hash(u_opt,"nside"));
     _nsidelast = und(nsidelast, hash(u_opt,"nsidelast"));
     r         = und(r, hash(u_opt,"r"));
     rlast     = und(rlast, hash(u_opt,"rlast"));  
     rs        = und(rs, hash(u_opt,"rs"));
     closed    = und(closed, hash(u_opt,"closed", false));
     twist     = und(twist, hash(u_opt,"twist"));
     rot       = und(rot, hash(u_opt,"rot"));
     Rf        = und(Rf, hash(u_opt,"Rf"));       
     csec0     = und(csec0, hash(u_opt,"csec0"));
     cseclast  = und(cseclast, hash(u_opt,"cseclast"));                   
     cut       = und(cut, hash(u_opt,"cut"));
     cutlast   = und(cutlast, hash(u_opt,"cutlast"));
    
    
     body     = und(body, hash(u_opt,"body", true));
     markpts  = und(markpts, hash(u_opt,"markpts"));
     bone     = und(bone, hash(u_opt,"bone"));
     frame    = und(frame, hash(u_opt,"frame"));
     edges    = und(edges, hash(u_opt,"edges"));
     markxpts = und(markxpts, hash(u_opt,"markxpts"));
     pads     = und(pads, hash(u_opt,"pads"));
    //====================================================
    df_opt = [];
    
    // 
    // Handle data, which could be:
    // 
    // (1) undef ==> give seed to grow from seed 
    // (2) pts ==> will get chain pts using chainPts
    // (3) randchain return => get chain pts w/ chainPts
    // (4) chainPts return => extract pts 
    // 
    //echo( data = data );


        //================================================
        //================================================
//        DATA_IS_PTS = "data is pts";
//        DATA_IS_CHAINBONE = "data has pts (=chainbone return)";
//        DATA_IS_CHAINDATA = "data has csecs (=chaindata return)";
//        DATA_UNKNOWN      = "data unknown, run chaindata with seed";
//        
//        //echo( DATA_IS_PTS=DATA_IS_PTS);
//        data_msgtxt= ispt( data[0] )? DATA_IS_PTS
//                  : hash( data, "pts")? DATA_IS_CHAINBONE
//                  : hash( data, "csecs")? DATA_IS_CHAINDATA
//                  : DATA_UNKNOWN;
//                    
//        //data_msg = _color(str( "Chain(): ", data_msgtxt ), "darkcyan");
//        data_msg = str( "Chain(): ", data_msgtxt );
//    //    
//        echo( data_msg);
//    //    
//        chbone= (data_msg==DATA_IS_PTS)? data
//               : DATA_IS_CHAINBONE? hash(data, "pts")
//               : DATA_IS_CHAINDATA? hash(data, "bone")
//               : let( // data unknown 
//                     bonedata= chainbone( 
//                                 n=n, a=bone_a, a2=bone_a2
//                                 , seglen=seglen
//                                 , lens=lens
//                                 , seed= und(seed, pqr())
//                                 , smooth=smooth  
//                                 )
//                 ) 
//                 hash( bonedata, "pts" );
//                 
//        echo( DATA_IS_PTS=DATA_IS_PTS);
//                                 
//        chrtn = data_msg== DATA_IS_CHAINDATA? data
//                : chaindata( chbone
//                            , nside =nside
//                            , nsidelast =nsidelast
//                            , r=r    
//                            , rlast=rlast  
//                            , rs =rs   
//                            , closed = closed 
//                            , twist = twist
//                            , rot   = rot
//                            , seed  = seed 
//                            , Rf    = Rf         
//                            , csec0 = csec0
//                            , cseclast = cseclast                    
//                            , cut     = cut
//                            , cutlast = cutlast
//                            )
//                  ;
//                
//        //echo( chrtn= arrprn(chrtn,dep=3, nl=true) );
//        csecs = hash(chrtn, "csecs");
//        
//        for( pl = csecs )
//           MarkPts(pl);
        //================================================
        //================================================
    
    
//    
    //for( csec = csecs ) Chain0(csec );
    
    csecs= ispt( data[0])?
            (let( chbone = chaindata( data
                            , nside =nside
                            , nsidelast =nsidelast
                            , r=r    
                            , rlast=rlast  
                            , rs =rs   
                            , closed = closed 
                            , twist = twist
                            , rot   = rot
                            , seed  = seed 
                            , Rf    = Rf         
                            , csec0 = csec0
                            , cseclast = cseclast                    
                            , cut     = cut
                            , cutlast = cutlast
                            )
                )
                hash( chbone, "csecs")
            ): hash( data, "csecs" )
            ;           
    
    
    echo( csecs = arrprn( csecs, 2 ) );
    //bone = hash(chrtn, "bone");
    frmlines = transpose( csecs );
    pts = joinarr( csecs );
    nseg = len( csecs )-1; 
    nside= len( csecs[0] );
//    //echo(nseg=nseg);
//    echo( str("<br/><b> csecs= </b>", csecs
//             ,"<br/><b> bone= </b>", bone
//             ,"<br/><b> frmlines= </b>", frmlines
//             ,"<br/><b> pts= </b>", pts
//             ,"<br/><b> seed= </b>", seed
//             ,"<br/><b> nseg= </b>", nseg
//             )); 
    //echo( csecs = arrprn(csecs,3,nl=true) );
    // -------------------------------------------    
    //MarkPts(seed, "pl=[trp,0.1];ball=false");
    
//    subopts = subopts( 
//    
//          df_opt=df_opt
//        , u_opt= u_opt
////        , touse =  [ "markpts"
////                    , "labels" 
////                    , "plane"  
////                    , "markxpts"   
////                    ]
//        , dfs=[ "markpts", ["rng", range(pts)]
//                           
//              , "bone", ["r",0.06,"rng",range(bone)
//                        ,"chain"
//                         ,["r",0.02,"closed",closed]]
//              , "seed", ["r",0.06,"rng",range(bone)
//                        ,"chain",["r",0.02]]             
//              , "frame", ["r",0.01, "color","green"
//                         ,"rng", range(frmlines)
//                         ]
//              , "edges", ["r",0.01, "color","green"
//                         ,"rng", range(frmlines)
//                         ]
//              , "markxpts", ["r",0.04
//                          //, "closed",true
//                          ,  "chain",["closed",true] 
//                         ,"rng", range(csecs)
//                         ]
//              , "pads", ["r",0.1, "color",undef
//                         ,"rng", range(frmlines)
//                         ]           
//              ]
//        , com_keys=[] 
//       );
//       
//    function sub(subname)= hash(subopts, subname);
    
    //echo( subopts = arrprn(subopts,dep=3) );
//
//    
//    // -------------------------------------------    
//    o_mpts = sub("markpts");
//    if(pts && o_mpts)  {
//        //echo(o_mpts=o_mpts);
//        MarkPt( sel(pts,hash(o_mpts,"rng"))
//                    , o_mpts );
//    }    
//    //MarkPts( pts, "r=0.03");   
//   
//    // -------------------------------------------    
//    o_bone = sub("bone");
//    if(bone && o_bone)  {
//        //echo(o_bone=o_bone, "<br/>",bone=bone, "<br/>"
//        //    , selbone = sel(bone,hash(o_bone,"rng")));
//        
//        MarkPts( sel(bone,hash(o_bone,"rng"))
//                    , o_bone );
//    }    
//
//    // -------------------------------------------    
//    o_seed = sub("seed");
//    if(seed && o_seed)  {
//        //echo(o_seed=o_seed, "<br/>",seed=seed);
//        
//        MarkPts( sel(seed,hash(o_seed,"rng"))
//                    , o_seed );
//    }       
//    //-------------------------------------------
//    o_xpts = sub("markxpts");
//    //echo( o_xpts = o_xpts );
//    if( csecs && o_xpts ){
//        
//       //echo( rng = or(hash( o_xpts, "rng"), range(csecs)) );
//      //echo( o_xpts = o_xpts );
//
//         for( i= hash( o_xpts, "rng", range(csecs))){
//           //echo( str("<b>",i, "</b>"
//           //         , arrprn(get(csecs,i),nl=true)
//           //         , "<br/> opt=", delkey(o_xpts,"rng"))
//           //    );
//           MarkPts( get(csecs,i), delkey(o_xpts,"rng"));
//         } 
//    }    
//    
//    //-------------------------------------------
//    o_edges = sub("edges");
//    if( frmlines && o_edges ){
//       //echo("edging, o_edges= ", o_edges);
//       for( i= hash(o_edges, "rng", range(frmlines))){
//           Chain( get(frmlines,i), o_edges);
//       }         
//    }    
//    
//    //-------------------------------------------
//       
//    o_frame = sub("frame");
//    //echo(o_frame = o_frame);
//    if(o_frame){
//        //echo(o_frame = o_frame);
//        if( frmlines ) { 
//           for( fl= frmlines){ 
//               Chain( fl, o_frame);
//           }         
//        }//if frmlines    
//        
//       if( csecs ){
////           echo("csecing ...");
////           echo( hash(o_frame,"rot"), hash(o_frame,"nside"), hash(opt,"nside"));
//           for( csec= csecs){ //csec= csecs ) {
//               //echo( i=i, csec = csec );
//               // chaining of csecs will be rotated by the amount
//               // of 180/nside (say, 45 for nside=4) to match the
//               // angle of csecs and that of frmlines               
//               Chain( csec
//                    
//                    , opt=concat( ["closed",true], o_frame
//                            , ["rot", hash(o_frame,"rot", 0)
//                               +180/hash(o_frame,"nside"
//                                   ,hash(opt,"nside", nside))
//                              ] 
//                            )
//                    );
//           }    
//       }// if csecs  
//    }//if_o_frame
//    
//     closinglink= [  csecs[0][0]
//                    , bone[0]
//                    , last(bone)
//                    , last(csecs)[0]
//                  ];  
//                  
//     //echo( closinglink = closinglink);             
//                  
//     /* tailroll
//    
//        tailroll is the amount(int) of pts for the tailface
//        to roll in order to match the tail face to the head
//        face to overcome twisting in cases of closed chain
//    
//        twistangle 
//    
//     */
//     //MarkPts( bone, "l");
//                    
//     twistangle = twistangle(closinglink);
//     //twistangle = _twistangle; //?_twistangle:0;
//     
//     tailroll =  -floor( twistangle/ ( 360/ nside)); 
//    //tailroll = 4;
//    // echo( nside=nside, twistangle= twistangle, tailroll = tailroll ); 
//
//    //echo(closed = closed, tailroll = tailroll );
//    
////    faces= faces( closed?"ring":"chain"
////                  , nside=nside 
////                  , nseg= nseg 
////                  );
//     
//     shape= closed && tailroll? "ring"
//            : closed? "ring"
//            : or(shape, opt( "shape", "chain"));
//
//     faces = faces( shape=shape
//                  , nside=nside 
//                  , nseg= nseg 
//                  , tailroll = tailroll
//                  );
//    //echo(pts = arrprn(pts,2) );
//    //echo( shape=shape, faces=faces );
////     faces = closed && tailroll?
////             faces( "ring"
////                  , nside=nside //len(csecs[0])
////                  , nseg= nseg //len(csecs)-1 //(closed?0:1)
////                  , tailroll = tailroll
////                  )
////             :faces( closed?"ring":"chain"
////                  , nside=nside //len(csecs[0])
////                  , nseg= nseg // len(csecs)-1 //(closed?0:1)
////                  );
//
//
//     if(pts && opt("body"))
//     {    
//         //echo(len=len(faces), arrprn(faces,dep=3));
//             //echo("Chain(): drawing body...");
//         color(opt("color"),opt("transp"))   
//         polyhedron( points= pts, faces=faces);  
//     }    
//  

 }//_Chain

// MarkPts( [[2.94746, 4.61618, 10.7011], [2.88179, 4.51477, 10.6891]] 
//  ,["r",0.01]);
 
module Chain_demo_simple(){
   
   echom("Chain_demo_simple");
    
   seed = pqr();
   bonedata = chainbone(seed=seed, n=4);
   bone = hash(bonedata, "pts"); 
   chdata = chaindata( bone );
   csecs =  hash(chdata, "csecs"):
   color("black") MarkPts(bone);
   Chain( chdata //data //seed= trslN(seed,-3)
        , n=4
    );

//   color("red") 
//   Chain( seed=seed, n=16, r=.54, nside=6 , seglen=2
//        , a=120, a2=10
//    );
   
    
}
Chain_demo_simple();

module Chain_demo_input(){
    
   echom("Chain_demo_input");
   
   pqr=pqr();
    
   //
   // Input seed
   // 
   MarkPts( pqr, "ch");
   Chain( seed=pqr, n=4, r=.8, closed=true, seglen=4
        , a=90, a2=0, rot=45
        , opt="trp=0.8;markxpts");
    
   pqr2= trslN(pqr, 4);
   MarkPts( pqr2, "ch" );
   Chain( seed=pqr2, n=6, twist=90, a=[170,185],a2=[-20,20]
        , opt= "trp=0.8;cl=red"  );
    
   pqr3= trslN(pqr, 8);
   MarkPts( pqr3, "ch" );
   Chain( seed=pqr3, n=6, r=1.5,r2=0.5, frame=true, a=[170,185],a2=[-20,20]
        , opt= "trp=0.8;cl=green;body=false"  );
   
   //
   // Input pts 
   // 
   pqr4= trslN(pqr, 12);
   MarkPts( pqr4, "ch" );
   pts = arcPts( pqr4, n=6, a=120, rad=6 );
   Chain( pts, r=1, "cl=green;trp=0.7" ); 
   
   //
   // Input randchain return
   // 
   pqr5= trslN(pqr, 16);
   MarkPts( pqr5, "ch" );
   rndch = randchain( seed=pqr5, a=[170,185],a2=[-20,20]
                    , n=8, lens=[3,1] );
   Chain( rndch, r=1, "cl=darkcyan;trp=0.7" ); 
   
   //
   // Input chainPts return 
   // 
   pqr6= trslN(pqr, 22);
   MarkPts( pqr6, "ch" );
   rndch6 = randchain( seed=pqr6, seglen=0.6, a=[175,185],a2=[-15,15]
                    , n=28 );
   //echo( rndch6=rndch6, haskey(rndch6));                 
   chrtn = chainPts( hash(rndch6,"pts"), twist=120, nside=4 );
   //echo( chrtn = chrtn);   
   //MarkPts( hash(chrtn,"bone"), "ch");
   Chain( chrtn, "trp=0.9" ); 
    
   
}
//Chain_demo_input();

module Chain_demo_rod(){    
    echom("Chain_demo_rod");
    
    pqr=pqr();
    N = N(pqr);
    uvn= uvN(pqr);
    uvm= uvN( [pqr[0],pqr[1],N]);
    Rf = pqr[2];
    
    Chain( p01(pqr), ["Rf",pqr[2]] );
    //MarkPts( pqr31, "pl"); 
        
//    //-----------------
//    
    pqr2= trslN(pqr,3);
    Chain( p01(pqr2), ["r",0.5,"transp",0.8,"Rf",pqr2[2]]);
    
    pqr3= trslN(pqr,6);
    Chain( p01(pqr3)
    , ["r=1;cl=red","transp",0.8,"Rf",pqr3[2]]);
    
        pqr31= trslN2(pqr3,3);
        MarkPts( pqr31, "pl");
        Chain( p01(pqr31)
        , ["r=1;cl=brown;rot=45","transp",0.8,"Rf",pqr31[2]]);
        
    pqr4= trslN(pqr,9);
    Chain( p01(pqr4)
         , ["r=1;nside=6;cl=darkcyan","transp",0.8,"Rf",pqr4[2]]);

    pqr5= trslN(pqr,12);
    Chain( p01(pqr5)
         , ["r=1;nside=24;cl=blue","transp",0.8,"Rf",pqr5[2]]);

}
//Chain_demo_rod();

module Chain_demo_polygon(){    
    echom("Chain_demo_polygon");
    
    pqr=pqr();
    
    pqr3= trslN(pqr,3);
    Chain( seed=pqr3, a2=0, closed=true
          , r=0.58, n=3, a= 180*(3-2)/3, seglen=5);

    pqr4= trslN(pqr,6);
    Chain( seed=pqr4, a2=0, closed=true
          , r=0.58, n=4, a= 180*(4-2)/4, seglen=4, opt="cl=red");

    pqr5= trslN(pqr,9);
    Chain( seed=pqr5, a2=0, closed=true
          , r=0.58, n=5, a= 180*(5-2)/5, seglen=3.5, opt="cl=green");

    pqr7= trslN(pqr,12);
    Chain( seed=pqr7, a2=0, closed=true
          , r=0.58, n=7, a= 180*(7-2)/7, seglen=3, opt="cl=blue");

    pqr36= trslN(pqr,16);
    Chain( seed=pqr36, a2=0, closed=true
          , r=0.58, n=36, a= 180*(36-2)/36, seglen=.6, opt="cl=darkcyan");

}
//Chain_demo_polygon();


module Chain_demo_rod_rot_twist(){    
    echom("Chain_demo_rod_rot_twist");
    
    pqr=pqr();
    N = N(pqr);
    uvn= uvN(pqr);
    uvm= uvN( [pqr[0],pqr[1],N]);
    Rf = pqr[2];
    
    MarkPts( pqr, "pl");
    Chain( p01(pqr), ["r=1;trp=0.8","Rf",pqr[2], "markxpts",["rng",[0]]] );
        
    //-----------------
    
    pqr2= trslN(pqr,3);
    MarkPts( pqr2, "pl");
    Chain( p01(pqr2), ["r=1;trp=0.8;rot=45","Rf",pqr2[2], "markxpts",["rng",[0]]]);
    
    pqr3= trslN(pqr,6);
    MarkPts( pqr3, "pl");
    Chain( p01(pqr3)
    , ["r=1;trp=0.8;cl=darkcyan","twist",30, "Rf",pqr3[2], "markxpts",["rng",[0]]]);
   
    pqr4= trslN(pqr,9);
    MarkPts( pqr4, "pl");
    Chain( p01(pqr4), 
         , opt=["r=1;trp=0.8;cl=darkcyan;rot=45","twist",30,"Rf",pqr4[2], "markxpts",["rng",[0]]]);
 
}
//Chain_demo_rod_rot_twist();


module Chain_demo_hcut(){    
    echom("Chain_demo_hcut");
    
    marking=true;
    xptsopt= marking? ["rng",[0]]:false;
    
    _pqr=pqr(); Q= onlinePt(p01(_pqr),len=5);
    pqr= [ _pqr[0], Q, _pqr[2] ];
    N = N(pqr);
    uvn= uvN(pqr);
    uvm= uvN( [pqr[0],pqr[1],N]);
    Rf = pqr[2];
    
    if(marking) MarkPts( pqr, "pl");
    Chain( p01(pqr), ["r=1;trp=0.4","Rf",pqr[2], "markxpts",xptsopt] );
        
    
    pqr2= trslN(pqr,3);
    if(marking) MarkPts( pqr2, "pl");
    Chain( p01(pqr2), ["r=1;trp=0.8","hcut",60,"Rf",pqr2[2], "markxpts",xptsopt]);

    //-----------------
    
    pqr3= trslN(pqr,8);
    if(marking) MarkPts( pqr3, "pl");
    Chain( p01(pqr3), ["r=1;trp=0.8;cl=darkcyan","hcut",30,"Rf",pqr3[2], "markxpts",xptsopt]);

    pqr4= trslN(pqr,11);
    if(marking) MarkPts( pqr4, "pl");
    Chain( p01(pqr4), ["r=1;trp=0.8;cl=darkcyan","hcut",[45,45],"Rf",pqr4[2], "markxpts",xptsopt]);

    //-----------------
    pqr5= trslN(pqr,16);
    if(marking) MarkPts( pqr5, "pl");
    Chain( p01(pqr5), ["r=1;trp=0.8;cl=green","hcut",[30,45],"Rf",pqr5[2], "markxpts",xptsopt]);

        pqr51= trslN2(pqr5,3);
        if(marking) MarkPts( pqr51, "pl");
        Chain( p01(pqr51)
          , markxpts= [ "rng",[0], "r",0.1, "chain",["closed", "r",0.05]]
          , opt=["r=1;trp=0.8;cl=green;rot=45","hcut",30,"Rf",pqr51[2]]);
}
//Chain_demo_hcut();



module Chain_demo_r_r2(){    
    echom("Chain_demo_r_r2");
    
    randchrtn=randchain(n=8, lenseg=1.5, a=[120,240], a2=[-60,60]);
    pts= hash(randchrtn, "pts");
    
    echo( pts = pts );
    
    Chain(pts);
    
    pqr2=trslN(pts,3);
    Chain(pqr2, "cl=red;r=0.1");

    pqr3=trslN(pts,7);
    Chain(pqr3, "cl=green;nside=8;r=0.1;r2=1");
    
    pqr4=trslN(pts,11);
    Chain(pqr4, "cl=darkcyan;nside=36;r=1;r2=0.01");
    
    pqr5=smoothPts(n=3, trslN(pts,15));
    Chain(pqr5, "cl=blue;nside=24;r=1;r2=0.01");

}
//Chain_demo_r_r2();

module Chain_demo_closed_nside(){    
    echom("Chain_closed_nside");
    
    pqr = pqr();
    rchrtn= randchain( n=10, len=2, seed=pqr
                    , a=[ 120,160 ], a2=[0,30]);
    pts = hash( rchrtn, "pts");
    MarkPt( pqr[0] );
    
    //pts=randPts(5);
//    pts=[[-0.254608, 2.94226, -0.265922], [-1.79069, 1.79273, 0.359467], [-2.8474, -2.51477, -1.47868], [-2.3854, 0.84323, 1.80297], [1.53019, -1.42591, -1.42311]];
//    pts = [[0.0335302, -2.27956, 2.68029], [-1.49161, 2.95183, 2.97162], [2.89684, -2.40119, -2.71649], [0.998082, -2.8189, -2.22895], [-2.98195, -1.33313, -0.273813]];
    echo( pts = pts );
    Chain(pts,"closed=false");
    
    pqr2=trslN(pts,5);
    Chain(pqr2, "cl=red;closed;r=.6;trp=1");

    pqr3=trslN(pts,10);
    Chain(pqr3, "cl=green;nside=9;closed;r=.6;trp=1");
    
    pqr4=trslN(pts,15);
    Chain(pqr4, "cl=blue;nside=20;closed;r=.6;trp=1");
    
//	path= [ [0,5,1]
//			,[3,5,0],[3.7,4.7,0], [4,4.3,1] //, [ 4,5,0]
//			, [4,3,-1], [1,3,2], [0,0,3], [4,0,-1] ];

}
//Chain_demo_closed_nside();

module Chain_demo_frame(){
    echom("Chain_demo_frame");
    
    n= 5;
    nside = 4; 
    
    pqr=pqr();
    rndch = randchain( n=n, len=2, seed=pqr
                    , a=[ 170,190 ], a2=[-50,50]);
    bone= hash(rndch, "pts");
//    bone = [[0.799646, 1.00731, 1.60879], [-0.0701428, 1.1432, 2.69276], [-0.57967, 1.28045, 3.29968], [-2.3397, 1.87979, 4.96158], [-3.16322, 2.2767, 6.10881]];
    
    echo(bone=bone);
    //------------
    Chain(bone);
  
    //------------
//    bone2 = trslN(bone, 2.5);
//    Chain(bone2, opt="r=1;r2=0.1");  
//    
//    //------------
//    bone3 = trslN(bone, 5);
//    Chain(bone3, opt=
//    "r=1;r2=0.1;trp=0.3;edges;body=false");  
//    
//        bone31 = trslN2(bone3, 4);
//        Chain(bone31, opt=
//        "r=1;r2=0.1;trp=0.3;edges=[r,0.1,color,red,rng,[0,2]];body=false");  
//   
//    //------------
//    bone4 = trslN(bone, 9);
//    Chain(bone4, opt=
//    "r=1;r2=0.1;trp=0.3;markxpts;body=false");  
//    
//        bone41 = trslN2(bone4, 4);
//        Chain(bone41, opt=
//        "r=1;r2=0.1;trp=0.3;markxpts=[plane,color,green, rng,[0,-2]];body=false");  
//////    
//////////
//    //------------
    bone5 = trslN(bone, 14);
    Chain(bone5, opt=
    "r=1;r2=0.1;trp=0.3;frame;body=false");  

        bone51 = trslN2(bone5, 4);
        Chain(bone51, opt=
        "r=1;trp=0.3;frame=[r,0.2];body=false");  

}
//Chain_demo_frame();

module Chain_demo_bone_seed(){
    echom("Chain_demo_bone_seed");
    
    n= 6;
    nside = 4; 
    
    pqr = randPts(3, [-1.5,1.5]);
    rndch = randchain( n=n, len=2, seed=pqr
                    , a=[ 150,180 ], a2=[-70,70]);
    bone = hash(rndch, "pts");
    echo(bone=bone);
    //------------
    //Chain(bone);
  
//    //------------
//    bone2 = trslN(bone, 4);
//    Chain(bone2, opt="r=0.7");  
//    
//    //------------
//    bone3 = trslN(bone, 8);
//    Chain(bone3, opt=
//    "r=0.8;trp=0.3;bone");  
//    //echo(bone3=bone3);
//    //MarkPts(bone3,"ch");
//    
//        bone31 = trslN2(bone3, -4);
//        Chain(bone31, opt=
//    "r=0.7;trp=0.5;closed;bone");  
////   
    //------------
    bone4 = trslN(bone, 12);
    Chain(bone4, opt=
    "r=0.8;trp=0.3;seed=[chain,false,plane,false,label,true];markxpts=[rng,[0],label,true,chain,[color,blue,closed,true]]");  
//    
//        bone41 = trslN2(bone4, 4);
//        Chain(bone41, opt=
//        "r=1;r2=0.1;trp=0.3;markxpts=[plane,color,green, rng,[0,-2]];body=false");  
////    
////////
////    //------------
//    bone5 = trslN(bone, 14);
//    Chain(bone5, opt=
//    "r=1;r2=0.1;trp=0.3;frame;body=false");  
//
//        bone51 = trslN2(bone5, 4);
//        Chain(bone51, opt=
//        "r=1;trp=0.3;frame=[r,0.2];body=false");  

}
//Chain_demo_bone_seed();

//
//csecs=[
//[ [6.7593, 4.21674, 5.98853]
//, [7.68704, 3.54409, 6.81729]
//, [7.08426, 2.30772, 6.4886]
//, [6.15651, 2.98036, 5.65985]
//]
//,
//[ [5.94189, 4.15437, 7.0998]
//, [6.66116, 3.62249, 7.78321]
//, [6.16209, 2.64187, 7.54528]
//, [5.44282, 3.17375, 6.86186]
//]
//,
//[ [5.48709, 4.16931, 7.69392]
//, [6.04254, 3.73148, 8.34405]
//, [5.59784, 2.90143, 8.16499]
//, [5.04238, 3.33926, 7.51487]
//]
//,
//[ [3.76548, 4.42571, 9.47389]
//, [4.02428, 4.22148, 9.77233]
//, [3.79938, 3.84371, 9.70883]
//, [3.54058, 4.04794, 9.41039]
//]
//,
//[ [2.95287, 4.62453, 10.7021]
//, [3.04145, 4.55682, 10.7891]
//, [2.96495, 4.43871, 10.7751]
//, [2.87638, 4.50642, 10.6881]
//]];
//
////csec= [ for(p=last(csecs)) [p.x-2.9, p.y-4.4, p.z-10.7]];
////echo( csec = csec);    
////MarkPts( csec, opt=["r", 0.01, "closed", true] );
////Chain( csec, opt=["r", 0.01, "closed", true] );
//
////MarkPts( last(csecs), opt=["r", 0.01, "closed", true] );
//csec = last(csecs);
//echo( csec = csec);    
//
//////Chain( csec, opt=["r", 0.01, "closed", true] );
//Chain( csecs[0], opt=["r", 0.05, "closed", true] );
////Chain( csecs[1], opt=["r", 0.01, "closed", true] );
////Chain( csecs[2], opt=["r", 0.01, "closed", true] );
////Chain( csecs[3], opt=["r", 0.01, "closed", true] );
////Chain( csecs[4], opt=["r", 0.01, "closed", true] );
//
//
//for( i = range(csecs) )
//{ 
//    echo(i);
//    //if( i< len(csecs)-1)
//    Chain( csecs[i], opt=["r", 0.05, "closed", true] );
//}

//----------------------------------------

//pts= [[4.01825, -4.60879, 8.09736], [4.60843, -5.18667, 8.22509], [4.84328, -5.12594, 7.41468], [4.2531, -4.54806, 7.28695]];
//
//Chain(pts,opt=["r",0.2,"closed",true, "rot",-45]);

//echo(".............. TESTING...............");
//pts=[ [6.24119, 4.49905, -2.06096]
//    , [6.18406, 4.59619, -2.1464]
//    , [6.15296, 4.49521, -2.24041]
//    , [6.21008, 4.39807, -2.15496]];
//MarkPts( pts, "r=0.005;pl" );
//echo( bone = pts );
//chrtn = chainPts( pts, opt= ["r", 0.2, "nside", 4
//        , "closed", true, "color", "green"
//        //, "rng", [0, 1, 2, 3]
//        ]);
//echo( chrtn = arrprn(chrtn,3,nl=true));


//module Chainf(pts, ops=[]){  // formatted Chain
//    ops = concat( ops, ["labelbeg", "P","r",0.01, "closed",false]);
//    labelbeg= hash(ops,"labelbeg");
//    //echo(" in chainf, ops",ops);
//    Chain( pts, ops=ops );
//    LabelPts( pts, labelbeg=="P"?"PQRSTUVWXYZABCDEF":
//                    labelbeg==0?range(pts):labelbeg
//                , ["scale", hash(ops,"scale", 0.03)] 
//                 );
//    MarkPts( pts, ["r",0.06] );
//}    

//Chainf(pqr());


//========================================================
ColorAxes=["ColorAxes", "ops", "n/a", "Shape",
 " Paint colors on axes. 
;;  [
;;   ''r'',0.012
;;  ,''len'', 4.5
;;  ,''xr'',-1
;;  ,''yr'',-1
;;  ,''zr'',-1
;;  ,''xops'', [''color'',''red''  , ''transp'',0.3]
;;  ,''yops'', [''color'',''green'', ''transp'',0.3]
;;  ,''zops'', [''color'',''blue'' , ''transp'',0.3]
;;  ]
"];

module ColorAxes( ops=[] )
{
	ops = concat( ops,
	[
		"r",0.012
		,"len", 4.5
		,"xr",-1
		,"yr",-1
		,"zr",-1
		,"fontscale", 1
		,"xops", ["color","red", "transp",0.3]
		,"yops", ["color","green", "transp",0.3]
		,"zops", ["color","blue", "transp",0.3]
	]);
	function ops(k)= hash(ops,k);
	r = ops("r");
	l = ops("len");

	fscale= ops("fontscale");
	//echo( "fscale" , fscale);
	//include <../others/TextGenerator.scad>
	
	xr= hash( ops, "xr", if_v=-1, then_v=r) ;
	yr= hash( ops, "yr", r, if_v=-1, then_v=r);
	zr= hash( ops, "zr", r, if_v=-1, then_v=r);
	
	echo( "r",r, "xr", xr);

	Line0( [[ l,0,0],[-l,0,0]], concat( ["r", xr], ops("xops") ));
	Line0( [[ 0, l,0],[0, -l,0]], concat( ["r", yr], ops("yops") ));
	Line0( [[ 0,0,l],[0,0,-l]], concat( ["r", zr], ops("zops") ));

	xc = hash(ops("xops"), "color");
	yc = hash(ops("yops"), "color");
	zc = hash(ops("zops"), "color");
	translate( [l+0.2, -6*r,0]) 
		rotate($vpr) scale( fscale/15 ) 
		color(xc) text( "x" );
	translate( [6*r, l+0.2, 0]) 
		rotate($vpr) scale( fscale/15 ) 
		color(yc) text( "y" );
	translate( [0, 0, l+0.2]) 
		rotate($vpr) scale( fscale/15 ) 
		color(zc) text( "z" );
} 
module ColorAxes_test(ops){ doctest( ColorAxes ,ops=ops);}


//========================================================
Cone=["Cone","pq, ops=[]", "n/a", "Geometry"
, "Given a 2 or 3-pointer pq and ops, make a cone along the PQ
;; line. If pq a 3-pointer, the points forming the cone bottom
;; will start from plane of the 3 points and circling around 
;; axis PQ in clockwise manner. Default ops:
;;
;; [ ''r'' , 3  // rad of the cone bottom
;; , ''len'', 0.3  // height of the cone
;; , ''fn'', $fn==0?6:$fn // # of segments along the cone curve,
;; ]
;;
;; New feature: You can mark points @ run-time:
;; 
;; Cone(pq, [''markpts'', true] )
;; Cone(pq, [''markpts'', [''labels'',''ABCDEF''] )
;; 
"];
module Cone_test(ops=[]){
    doctest( Cone, [], ops); }
    
module Cone( pq, ops=[] ){
    //_mdoc("Cone","");
                     
	ops= concat( ops
    , [ "r" , 1            // rad of the curve this obj follows
	  , "len", undef       // default to len_PQ. Could be negative
      , "fn", $fn==0?6:$fn // # of segments along the cone curve,
                           // = # of slices or segments
      , "markpts", false   // true or hash
      , "twist", 0
      ], OPS);

    /*      Q
           _|_ 
      _--'' | ''--_
    -{------P------}----- 
      ''-.__|__.-''
            |      | 
            |<---->|
                r    
    */
    
    function ops(k,df)=hash(ops,k,df);
    
    //echo("ops= ", ops);
    
    r = ops("r");
    fn= ops("fn");
    
    P = pq[0]; Q=pq[1]; 
    R= len(pq)==3? pq[2]: anglePt( [Q,P, randPt()], a=90, r=r);
    
    pqr = [P,Q,R];
    
    //     0
    //     | 
    //  _4-|-3_
    // 5_  +  _2
    //   -6-1-
    //
    // faces= [ [0,2,1]
    //        , [0,3,2] 
    //        , ... 
    //        , [0,6,5]
    //        , [0,1,6]
    //        ]
    
    faces= concat(
            [ for( i =range(fn) ) [0, i>fn-2?1:i+2, i+1] ]
            , [range(1, fn+1)]    
      );  

    //echo("faces", faces);
       
    arcpts = arcPts( [Q,P,R]
                    , pl="yz"
                    , a=360
                    , count=fn
                    , r=r
                    , twist= ops("twist")
                    ); 
//    echo( "arcpts", arcpts);   
            
    df_markpts= ["samesize",true, "labels", range(1, fn+1)];       
    m = ops("markpts");
	if(m) { MarkPts(arcpts , concat( isarr(m)?m:[], df_markpts));}
    
    color( ops("color"), ops("transp"))        
    polyhedron( points= concat( [Q], arcpts )
              , faces = faces );               
}

module Cone_demo_1(){
_mdoc("Cone_demo_1"
," yellow: Cone( randPts(2) )
;; green: Cone( randPts(3) , [ ''fn'',6
;;                , ''color'',''green''
;;                , ''transp'',0.5
;;                , ''markpts'',true  // <=== new feature in scadex
;;                ] 
 ");    

    pq = randPts(2);
    MarkPts( pq, ["labels","PQ"]);
    Cone( pq );
    LabelPt( pq[1], "  Cone(pq)");

    pqr = randPts(3);
    Plane(pqr, ["mode",3]);
    MarkPts( pqr, ["labels","PQR"]);
    Cone( pqr , [ "fn",6
                , "color","green"
                , "transp",0.5
                , "markpts",true  // <=== new feature in scadex
                ]);
    LabelPt( pqr[1], "  Cone(pqr,[\"fn\",6,\"markpts\",true...])"
            , ["color","green"]);
                            
    pqr2 = randPts(3);
    Plane(pqr2, ["mode",3]);
    MarkPts( pqr2, ["labels","PQR"]);
    Cone( pqr2 , [ "fn",6
                , "color","blue"
                , "transp",0.5
                , "markpts",true  // <=== new feature in scadex
                , "twist", 30 // <=== new 2015.1.25
                ]);
     LabelPt( pqr2[1]
     , "  Cone(pqr,[\"fn\",6,\"markpts\",true,\"twist\",30...])"
            , ["color","blue"]);
            
}    

//Cone_demo_1();

//========================================================
//Coord=["Coord", "pqr,ops", 
module Coord(pqr, ops=[])
{
    /*
       Set labels default. We want to allows for :
       Coord( pqr, ["labels","xyz"] )
       That is, setting it to a string, but not like the Multi 
       Complex Arguments pattern that only takes 
       true|false|style_hash. 
       We set all to false, but it can be either a string or a style_hash 
    */
    lbs = hash( ops, "labels", false ); 
    lbx = hash( ops, "labelx", false );  
    lby = hash( ops, "labely", false );
    lbz = hash( ops, "labelz", false );
    lbo = hash( ops, "labelo", false );
    
    // If it's a string, convert to ["text",label]  
    u_ops = update(ops,
            ["labels", isstr(lbs)?["text",lbs]:lbs
            ,"labelx", isstr(lbx)?["text",lbx]:lbx
            ,"labely", isstr(lby)?["text",lby]:lby
            ,"labelz", isstr(lbz)?["text",lbz]:lbz
            ,"labelo", isstr(lbo)?["text",lbo]:lbo
            ]);       
    df_ops=[ "r", 0.01
           , "len",5           
           //---------------
           , "axes", true
           , "x", true
           , "y", true
           , "z", true
           //---------------
//           , "labels", false
//           , "labelx", false
//           , "labely", false
//           , "labelz", false
//           , "labelo", false
           ];
    com_keys= ["transp","color","r", "len"];
    df_axes=[];
    df_lbs= ["labels","xyz", "color","black", "scale",0.03];
    
    _ops = update( df_ops, u_ops);
    function ops(k,df)=hash(_ops,k,df);
    
    //echo( ["ops",_fmth(ops), "u_ops",_fmth(u_ops),"_ops",_fmth(_ops)] );
    //===============================================
    function subops(sub_dfs)=
          getsubops(  u_ops = u_ops  
                    , df_ops = df_ops
                    , com_keys= com_keys
                    , sub_dfs= sub_dfs
                    );
    //===============================================
    
    pqr= or(pqr, pqr());
    //MarkPts(pqr);
    pts= coordPts( sel(pqr,[0,1,2])
                 , len=ops("len")
                 );
    
    //MarkPts(pts, ["r",0.2, "transp",0.3] );
    
    //=============================================== Axes
    ops_x= subops( ["axes",df_axes, "x", ["color","red"]] );
    ops_y= subops( ["axes",df_axes, "y", ["color","green"]] );
    ops_z= subops( ["axes",df_axes, "z", ["color","blue"]] );  
//    echo(ops_x= ops_x );  
//    echo( str("set ops_x= ", getsubops_debug( u_ops=u_ops, df_ops=df_ops
//                    , com_keys= com_keys
//                    , sub_dfs= ["axes", df_axes, "x", ["color","red"] ]
//                 ) ));   
    //echofh( ["ops_x",ops_x, "ops_y",ops_y, "ops_z",ops_z] );
    if( ops_x ) Line( sel(pts,[1,0]), len=hash(ops_x,"len"), ops= ops_x);    
    if( ops_y ) Line( sel(pts,[1,2]), len=hash(ops_x,"len"), ops= ops_y);    
    if( ops_z ) Line( sel(pts,[1,3]), len=hash(ops_x,"len"), ops= ops_z);    
    //=============================================== Labels
//    labels = istype(labels,"str","arr")? labels
//             
//    function getaxislabels( )=
//        let( labels = ops("labels")
//           , isstar = istype(labels,"str","arr")
////           , lbs = istype(labels,"str","arr")?labels[i]
////                   :labels==true? axname :""
//           ) //lbs
//        
//        :labels==true? 
//        [ for(i=range(4))
//            let( name= "xoyz"[i] )
//            isstar? labels[i]:labels==true?"
    
    ops_lbx= subops( ["labels",df_lbs, "labelx", ["text","x"]] );
    ops_lby= subops( ["labels",df_lbs, "labely", ["text","y"]] );
    ops_lbz= subops( ["labels",df_lbs, "labelz", ["text","z"]] );
    ops_lbo= subops( ["labels",df_lbs, "labelo", ["text","o"]] );
    if(ops_lbx) LabelPt( pts[0], hash(ops_lbx,"text",""), ops_lbx ); 
    if(ops_lbo) LabelPt( pts[1], hash(ops_lbo,"text",""), ops_lbo ); 
    if(ops_lby) LabelPt( pts[2], hash(ops_lby,"text",""), ops_lbx ); 
    if(ops_lbz) LabelPt( pts[3], hash(ops_lbz,"text",""), ops_lbx ); 
    
//    echofh( ["ops_lbx",ops_lbx, "ops_lby",ops_lby
//            , "ops_lbz",ops_lbz, "ops_lbo",ops_lbo] );
    echo( str("set ops_lbo= ", getsubops_debug( u_ops=u_ops, df_ops=df_ops
                    , com_keys= com_keys
                    , sub_dfs= ["labels",df_lbs, "labelo", ["text","o"]]
                 ) ));  
          
    echo( str("set ops_y= ", getsubops_debug( u_ops=u_ops, df_ops=df_ops
                    , com_keys= com_keys
                    , sub_dfs= ["axes",df_axes, "y", ["color","green"]]
                 ) ));        

}


//========================================================
module Cube(pqr, p=undef, r=undef, h=1)
{
	echom("Cube");
	cpts =cubePts(pqr,p=p,r=r,h=h);
//	echo("pqr:", pqr);
//	echo("cpts:", cpts);
	polyhedron( points= cpts, faces = CUBEFACES );
}

module Cube_demo()
{
	Cube(randPts(3));
	Cube(randPts(3),p=0.2,r=0.4,h=5);
	Cube(randPts(3),p=5,r=5,h=0.2);
}

//Cube_demo();




//========================================================
CurvedCone=["CurvedCone","pqr, ops=[]"
   , "n/a", "Geometry"
, "Given a 3-pointer pqr and ops, make a curved 
;; cone, where ops is:
;;
;; [ ''r'' , 3  // rad of the curve
;; , ''r2'', 0.3  // rad of the bottom of the cone
;; , ''a'', undef // angle determining cone length
;;              // if not given, use a_pqr
;; , ''fn'', $fn==0?6:$fn // # of segments along the cone curve,
;; , ''fn2'', $fn==0?6:$fn // # of points along bottom circle 
;; ]
"];
module CurvedCone_test(ops=[]){
    doctest( CurvedCone, [], ops); }
    
module CurvedCone( pqr, ops=[] )
{
/*          	. R  						 	       _
             _-' -      
          _-'     :
       _-'         :     
    _-'            : 
  Q+---------------+ P



                R  |N (N is on pl_pqr, and NP perp to PQ )
             _-' : | 
          _-'     || 
       _-'        _|_ 
    _-'      _--'' | ''--_
  Q+--------{------P------}----- 
             ''-.__|__.-''
                   |      | 
   |<------------->|<---->|
           r           r2 
    
*/
    //_mdoc("CurvedCone","");
                     
	ops= concat( ops
    , [ "r" , undef  // rad of the curve this obj follows
	  , "r2", 0.5  // rad of the bottom of the obj 
      , "a", undef // angle determining cone length
                   // if not given, use a_pqr
      , "fn", $fn==0?6:$fn // # of segments along the cone curve,
                    // = # of slices or segments
      , "fn2", $fn==0?6:$fn // # of points along bottom circle
      ], OPS);

    function ops(k,df)=hash(ops,k,df);
    

    r= ops("r")==undef?d01(pqr):ops("r"); 
    echo("r", r, "d01(pqr)", d01(pqr));
    
    a = ops("a")? ops("a"): angle(pqr);
    P = pqr[0]; Q=pqr[1]; R= anglePt(pqr, a, len=r);
    
    r2= ops("r2");
    fn= ops("fn");
    fn2=ops("fn2");
    
    pqr = [P,Q,R];
   
    function getPtsByLayer(i)=
    (   let( // X is a point along the R->P curve
             X = anglePt( pqr
                  , a=(fn-i-1)*a/(fn)
                  , len=r )
           , N = N([Q,X,R])
           ) 
        arcPts( [Q,X,N]
                    , pl="xy"
                    , a=360
                    , count=fn2
                    , r=r2 /fn*(i+1)
                    )
    );

    // This for loop is for debug only. It displays
    // lines thru circling pts of each layer, and also
    // label pts
    //    for( i = range(fn) ){
    //       pts = getPtsByLayer(i); 
    //       Chain( pts, ["r",0.01]);
    //       MarkPts( pts, ["samesized",true, "r", 0.02] );
    //       LabelPts( pts, range( i*fn2+1, (i+1)*fn2+1 )
    //                , ["scale",0.02] ); 
    //    }    
    
    
    // pts: [ [p10,p11,p12...p16]  <== layer 1
    //      , [p20,p21,p22...p26]
    //      ...
    //      , [pi0,pi1,pi2...pi6] ] <==layer i    
    pts_layers = [ for( i = range(fn) )
                   getPtsByLayer(i) ];
    //echo("");
    //echo("pts_layers: ", pts_layers);
    //echo( "pts_layers = ", pts_layers );
        
    pts = [ for(a=pts_layers, b=a) b] ; 
       
    //echo("");
    //echo("pts: ", pts);
    //echo("pts = ", pts); 
        
    ptsAll = concat( [R], pts );
    //echo("");
    //echo("with R: ptsAll: ", ptsAll);
    
    /*
    faces of a raised pantagon :
    
    faces = [ [1,0,5,6]
            , [2,1,6,7]
            , [3,2,7,8]
            , [4,3,8,9]
            , [0,4,9,5]
            , [0,1,2,3,4]
            , [5,6,7,8,9]
            ];
            
    fn = 4
    fn2= 6
    layer i = range( fn)
    faces= 
    [ [1,6,12,7]    // i = 1  
   , [2,1, 7,8]                    
   , [3,2, 8,9]
   , [4,3, 9,10]
   , [5,4,10,11]
   , [6,5,11,12]
   
   , [7,12,18,13]  // i = 2, j= range(fn2)  
          [(i-1)*fn2+1, fn2*2, fn2*3, i*fn2+1) 
   , [8,7, 13,14] 
          [(i-1)*fn2+j+1, (i-1)*fn2+j, i*fn2+j, i*fn2+j+1) 
   , [9,8,14,15]     
   , [10,9,15,16]
   , [11,10,16,17]
   , [12,11,17,18]
   
   , [13,18,24,19] // layer = 3
   , [14,13,19,20]
   , [15,14,20,21]
   , [16,15,21,22]
   , [17,16,22,23]
   , [18,17,23,24] 
   ]
            
    Last 2 are easy to make. Prior to that,         
    we make a "preface" first then transpose it:
       
       [ [1,2,3,4,0]  : concat( range(1,5), [0])
       , [0,1,2,3,4]  : range( 0, 5 )
       , [5,6,7,8,9]  : range( 5, 5+5 )
       , [6,7,8,9,5]  : concat( range(6,5+5), [5]) 
       ]
    ---------------------   
    If there are more than one layer, the above
    is the layer 0. Each layer will inc by 5:
    
    i=1:
       [ [5,6,7,8,9]  : range( 5, 5+5 )
       , [10,11,12,13,14]: range( 5+5, 10+5 )
       , [11,12,13,14,10]: concat( range(5+6,10+5), [10]) 
       , [6,7,8,9,5]  : concat( range(5+1,10), [5]) 
       ]
    
    layer i, fn:
    
        [ concat( range( i*fn+1, (i+1)*fn ), [fn] )
        , range( i*fn, (i+1)*fn )
        , range( (i+1)*fn, (i+2)*fn )
        , concat( range((i+1)*fn+1,(i+2)*fn),[i*fn] )
        ]
    */    
    
    // Faces w/o the first layer (which contains
    // triangles) and the bottom layer
//    function getSideFacesByLayer(i, fn, hasnext=false)=
//      transpose(
//        [ concat( range( i*fn+1, (i+1)*fn ), [fn] )
//        , range( i*fn, (i+1)*fn )
//        , range( (i+1)*fn, (i+2)*fn )
//        , hasnext? range((i+1)*fn+1,(i+2)*fn+1) 
//          : concat( range((i+1)*fn+1,(i+2)*fn),[i*fn] )
//        ]
//      )
//      ;
    
    // Layer0 contains triangles
    // 
    //     0
    //     | 
    //  _4-|-3_
    // 5_  +  _2
    //   -6-1-
    //
    // faces= [ [0,2,1]
    //        , [0,3,2] 
    //        , ... 
    //        , [0,6,5]
    //        , [0,1,6]
    //        ]
    
    function getLayer0Faces(fn)=
      [ for( i =range(fn-1) )
        [ 0, i>fn-3?1:i+2, i+1]
      ];  

    
    sidefaces = getTubeSideFaces
      ( [ for (i=range(1, fn+1))
          [ for (j=range(fn2)) (i-1)*fn2+j+1 ]
        ]      
      , isclockwise=true 
      );
    //echo("");      
    //echo("\nsidefaces = ",sidefaces);
                
    faces = mergeA( 
            [getLayer0Faces(fn2+1)
            , mergeA( sidefaces )  
            ,  [range((fn-1)*fn2+1, fn*fn2+1)] 
            ]);                
    //echo("");            
    //echo( "faces: ", faces);
    //echo("");
       
    //color("green", 0.8)            
    polyhedron( points= ptsAll
              , faces = faces );               
}

module CurvedCone_demo_1(){
_mdoc("CurvedCone_demo_1"
," yellow: CurvedCone( randPts(3) )
;; red  : a=60, r=8, r2=1,  fn= 4, fn2=6 
;; green: a=30, r=8, r2=0.5, fn=10, fn2=20 
;; blue : a=120,r=3, r2=1  , fn= 9, fn2=4 
 ");    

    pqr = randPts(3);
    MarkPts( pqr, ["labels","PQR"] ); Chain(pqr, ["closed",false]); 
    
    CurvedCone( pqr );
    LabelPt( midPt(p12(pqr)), "CurvedCone(pqr)");
    
    color("red", 0.6)
    CurvedCone( randPts(3), ["a",60
                     , "r", 8
                     , "r2", 1
                     , "fn",4 
                     , "fn2",6 
                     ] );
                     
    color("green", 0.6)
    CurvedCone( randPts(3), ["a",30
                     , "r", 8
                     , "r2", 0.5
                     , "fn",10
                     , "fn2",20
                     ] );
    
    color("blue", 0.6)
    CurvedCone( randPts(3), ["a",120
                     , "r", 3
                     , "r2", 1
                     , "fn",9
                     , "fn2",4
                     ] );    
}    

//CurvedCone_demo_1();


//========================================================
//cycleRange=["cycleRange","pts/i, 



//========================================================
DashBox=[ "DashBox", "pq,ops", "n/a", "3D, Shape", 
 " Given a 2-pointer (pq) and ops, make a box with dash lines parallel
;; to axes. The default ops is:
;;
;;   [ ''r'',0.005
;;   , ''dash'', 0.05
;;   , ''space'', 0.05
;;   , ''bySize'', true
;;   ]
;;
;; where r= radius of dash line, dash= the length of dash, space= space
;; between dashes. The p,q are the opposite corners of the box if bySize=
;; false. If bySize=true, p is a corner, and q contains size of box on
;; x,y,z direction.
"];  

module DashBox(pq, ops)
{
	ops=update(
		[ "r",0.005
		, "dash", 0.1
		//, "space", 0.05
		, "bySize", true
		], ops );
	function ops(k)= hash(ops,k);

	echo( ops("bySize"));

	pts = boxPts( pq, bySize= ops("bySize") );

	r= ops("r");

	color("blue"){
		Line( [ pts[0], pts[4] ], ops);
		DashLine( [ pts[1], pts[5] ], ops );
		DashLine( [ pts[2], pts[6] ], ops );
		DashLine( [ pts[3], pts[7] ], ops );
	}
	color("red"){
		DashLine( [ pts[0], pts[1] ], ops);
		DashLine( [ pts[2], pts[3] ], ops );
		DashLine( [ pts[4], pts[5] ], ops );
		DashLine( [ pts[6], pts[7] ], ops );
	}
	color("lightgreen"){
		DashLine( [ pts[0], pts[3] ], ops);
		DashLine( [ pts[1], pts[2] ], ops );
		DashLine( [ pts[4], pts[7] ], ops );
		DashLine( [ pts[5], pts[6] ], ops );
	}
}


module DashBox_test( ops ) {	doctest( DashBox, ops=ops); }

module DashBox_demo(pq, bySize=true)
{
	pq = [ [2,1,-1],[3,2,1]];
	DashBox( pq , [["bySize", bySize]]);

	//boxPts_demo( pq, bySize );
	MarkPts( boxPts( pq ) );

}
//DashBox_demo(bySize=true);
//doc(DashBox);


//========================================================
DashLine=["DashLine","pq,ops", "n/a", "3D, Shape", 
 " Given two-pointers(pq) and ops, draw a dash-line between pq.
;; Default ops:
;;
;;   [ ''r''     , 0.02     // radius
;;   , ''dash''  , 0.5      // dash length
;;   , ''space'' , undef    // space between dashes, default=dash/3
;;   , ''color'' , false
;;   , ''transp'', false
;;   , ''fn''    , $fn
;;   ]
"];

module DashLine(pts, ops=[])
{
	//echo("DashLine:");
	pts=countArr(pts)==0?
		[[0,0,0], pts] : pts;

	ops= update(["r",0.02
				,"dash",0.5
				,"space",undef
				,"color", false
				,"transp", false
				,"fn",$fn
				], ops);
	function ops(k)= hash(ops,k);

	dash = ops("dash");
	space= hash(ops, "space", dash/3); 
	s= dash+space;
	L = norm(pts[1]-pts[0]);
	count = L/s;
	if( L<=dash )
	{
		Line( pts, ops );	
	}else if (count<1)
	{
		Line( [pts[0], onlinePt(pts, ratio=dash/L)], ops );
	}else{
		for (i=[0:count])
		{
			if((i*s+dash)>L)
			{
				Line( [ onlinePt(pts, ratio=i*s/L)
			  		  , pts[1] ], ops);

			}else{
				Line( [ onlinePt(pts, ratio=i*s/L)
				  , onlinePt(pts, ratio=(i*s+dash)/L)]
				  ,ops);
			}
		}
	}
}


module DashLine_test(ops){ doctest(DashLine,ops=ops); }

module DashLine_demo(){

	pts= randPts(2);
	DashLine( pts); 
	MarkPts(pts);
	
	pts2 = randPts(2);
	DashLine( pts2, ops=["dash",0.02, "space",0.06] );
	MarkPts(pts2);
}
//DashLine_demo();
//DashLine_test();
//doc(DashLine);



//========================================================
LabelPt=["LabelPt", "p,label,ops=[]", "n/a", "Geometry, Doc",
 " Given a point (p), a string (label) and option (ops), write the
;; label next to the point. The label is written according to the 
;; viewpoint ($vpr) such that it is always facing the viewer. Other
;; properties of label are decided by ops. 
"];

module LabelPt_test( ops=["mode",13] ) { doctest( LabelPt, ops=ops ); }

// Require TextGenerator 
module LabelPt(pt,text="", opt=[], mti=0) 
{
    //if(ECHO_MOD_TREE) echom("LabelPt", mti);
    
    df_opt= [ "scale", 0.03
            , "tmpl", "" //{_`a|d2|aj=,}"
            , "isxyz", undef // label pt with coordinate
            , "shift", undef  // could be: 2.5, [3,1,0] 
            ];
    
    opt = update( df_opt, opt
                , df=["sc","scale","sh","shift"]);
    
    //echo( opt=opt);
    isxyz = hash( opt, "isxyz");
    tmpl = or(hash( opt,"tmpl"), isxyz?"{*`a|d2|aj=,}":"");
	scale = hash(opt,"scale")*mm;
    _label= isxyz? pt
            :hash(opt, "text", text);
    label = tmpl?_s2(tmpl,_label):str(_label);
    
    _shift = or(hash(opt, "shift"), [1,1,1]*scale*3 );
	shift = isarr(_shift)?_shift:_shift*[1,1,1] ;

    //echo(pt=pt, label=label, tmpl=tmpl, shift=shift);
    
//	echo("_shift", _shift, "shift", shift, "scale", scale);
//	echo("pt",pt,"label: ", label);
//	echo("LabelPt opt", opt);
	translate( pt+ shift ) 
		rotate($vpr) 
        translate( [-scale*4,-scale*5,0])
		scale( scale ) 
		color( hash(opt,"color"), hash(opt,"transp") ) 
		text( label
            , font="Liberation Mono" );
		//drawtext( str(label) );	
}

//translate([-0.03*4,-.15,0]) scale(0.03) text("0");
//LabelPt( [2,3,5], opt=["label","x"] );

//translate( [2,3,5] ) text("test"); 

module LabelPt_demo(mti=0)
{
    echom("LabelPt_demo", mti);
	pqr = randPts(3);
    echo(pqr=pqr);
	MarkPts( pqr, ["r", 0.05] );

	LabelPt( pqr[0], "P", mti=mti+1 );
	LabelPt( pqr[1], ["text","Q"], mti=mti+1 );
	LabelPt( pqr[2], "A", opt=["text","R"], mti=mti+1 );

	pt= randPt();
	MarkPts([pt]);
	LabelPt ( pt, "Scale=0.05", ["scale",0.05], mti=mti+1 ); 
//	 
}
//LabelPt_demo();


module LabelPt_demo_tmpl(mti=0)
{
    echom("LabelPt_demo_tmpl", mti);
	P = randPt();
    echo(P=P);
	MarkPt( P, ["r", 0.05] );

	LabelPt( P, P, opt=["shift",1], mti=mti+1 );
	LabelPt( P, opt=["isxyz", true, "color","red"], mti=mti+1 );
	LabelPt( P
          , opt=["isxyz", true
                , "tmpl","{*`a|d1|wr=/,/}"
                , "color","green"
                , "shift",-1
                ], mti=mti+1 );
	// The above template means: 
    // The text, [x,y,z], is an array, so if we use "_" 
    // instead of "*", {_`...}, the template will try to 
    // fill each of [x,y,z] to one "_". But we have only 
    // one "_", so it will only show "x". Using "*" ensures
    // entire [x,y,z] as a whole is packed into each "_".
    // "a" means the format (after "a") is applied to each 
    // member of array, but not to the [x,y,z] as a whole.  
}
//LabelPt_demo_tmpl();

module LabelPt_demo_sopt(mti=0)
{
    echom("LabelPt_demo_sopt", mti);
	P = randPt();
    echo(P=P);
	MarkPt( P, ["r", 0.05] );

	LabelPt( P, opt="sh=1;text=P", mti=mti+1 );
	LabelPt( P, opt="isxyz;cl=red;trp=0.2", mti=mti+1 );
	LabelPt( P, opt=["isxyz;cl=green;sh=-1"
              , "tmpl","{*`a|d1|wr=/,/}"], mti=mti+1 );
}
//LabelPt_demo_sopt();


////========================================================
//LabelPts=["LabelPts", "pts,labels,opt=[]", "n/a", "Geometry, Doc",
// " Given points (pts), labels (array of strings or a string) and option
//;; (opt), write the
//;; label next to the point. The label is written according to the 
//;; viewpoint ($vpr) such that it is always facing the viewer. Other
//;; properties of label are decided by opt. 
//;;
//;; New args 2014.8.16: 
//;;
//;;   prefix, suffix, begnum, usexyz
//;;
//"];
//
//module LabelPts_test( opt=["mode",13] ) { doctest( LabelPts, opt=opt ); }
//
//
//module LabelPts( pts, labels="", opt=[], mti=0 )
//{
//	echom("LabelPts", mti);
//    
//	opt = update(
//      [ "color", [0.4,0.4,0.4]
//	  //, "colors", COLORS
//	  , "isxyz",false
//      , "tmpl", ""
//      ], opt );
//	function opt(k,nf)= hash(opt, k, nf);
//    isxyz  = opt("isxyz");
//    color  = opt("color"); 
//    colors = opt("colors");
//    labels = opt("labels", labels);
//    //tmpl   = opt("tmpl");
//    
//	circle =concat([get(pts,-1)], pts, [pts[0]]);
//
//    for (i=[0:len(pts)-1])	{
//    
//        pt = len(pts)<3? pts[i]
//             : onlinePt( [ pts[i]
//                         , angleBisectPt( 
//                            [ circle[i], circle[i+1], circle[i+2] ])
//                         ], len=0
//                       );
//             
//        echo(pts_i=pts[i], pt=pt);           
//        LabelPt( pts[i]
//            , labels[i] //getlabel(i) //labels?labels[i]: getlabel(i)
//            , opt= update( opt, ["color", or(opt("colors")[i], color)]
//                         )
//            , mti=mti+1 ); 
//	}//for
//}
//
//
//module LabelPts_demo_1()
//{
//    echom("LabelPts_demo_1");
//    
//	pqr = randPts(3);
//    echo(pqr=pqr);
//	MarkPts( pqr, ["r", 0.05] );
//	LabelPts( pqr, "PQR" );
//	Plane( pqr, ["mode",3] );
//    
//    MarkPt( P(pqr) );
//    LabelPt( P(pqr), ["isxyz",true]);
//    
//    
////	lmn = randPts(3);
////	MarkPts( lmn, ["r",0.05] );
////	Plane( lmn, ["mode",3] );
////	LabelPts( lmn, ops=["color","red"]);
//}
////LabelPts_demo_1();
//
//module LabelPts_demo_1_2()
//{
//    echom("LabelPts_demo_1_2");
//    
//	fghi = squarePts(randPts(3));
//	MarkPts( fghi, ["r",0.05] );
//	Plane( fghi, ["mode",4] );
//	LabelPts( fghi, "FGHI", ["color","green"] );
//
//	pts = randPts(5);
//	MarkPts( pts, ["r",0.05, "samesize",true, "sametransp",true] );
//	LabelPts( pts, range(pts), ["color","purple"] );
//	Chain(pts, ["r", 0.02 ] );
//}
////LabelPts_demo_1_2();
//
//module LabelPts_demo_2_scale()
//{
//	echom("LabelPts_demo_2_scale");
//    
//    pqr = randPts(3);
//	MarkPts( pqr, ["r", 0.05] );
//	LabelPts( pqr, "PQR" );
//	Chain( pqr, ["r",0.02, "closed", false, "color","red"] ); 
//	LabelPt( pqr[1], "scale=0.03 (default)",["color","red", "shift",0.5] );
//
//	lmn = randPts(3);
//	MarkPts( lmn, ["r",0.05] );
//	Chain( lmn, ["r",0.02, "closed", false, "color","green"] ); 
//	LabelPts( lmn, "lmn", opt=["scale",0.06]);
//	LabelPt( lmn[1], "scale=0.06", ["color","green", "shift",0.5] );
////
//	fghi = randPts(4);
//	MarkPts( fghi, ["r",0.05] );
//	Chain( fghi, ["r",0.02, "closed", false, "color","blue"] ); 
//	LabelPts( fghi, "FGHI", ["scale",0.02,"color","red"] );
//	LabelPt( fghi[0], "scale=0.02", ["color","blue"] );
//}
////LabelPts_demo_2_scale();
//
//
//module LabelPts_demo_3_prefix_begnum_etc()
//{
//    echom("module LabelPts_demo_3_prefix_begnum_etc()");
//	// New 2014.8.16:
//	// prefix, suffix, begnum, usexyz
//
//	// Testing prefix, suffix with given labels 
//	pqr = randPts(3, x=[2,5]);
//	MarkPts( pqr, ["r", 0.05] );
//	LabelPts( pqr, range(pqr),["color","red", "prefix","p[", "suffix","]"] );
//	Chain( pqr, ["r",0.02, "closed", false, "color","red"] ); 
//	LabelPt( pqr[0], "[\"prefix\",\"p[\", \"suffix\",\"]\"]", ["color","red","shift",0.6] );
//
//	// Testing begnum=10 
//	lmn = randPts(3,y=[3,6]);
//	MarkPts( lmn, ["r",0.05] );
//	Chain( lmn, ["r",0.02, "closed", false, "color","green"] ); 
//	LabelPts( lmn, [10,11,12], ops=["scale",0.06]);
////    LabelPt( lmn[0], "[\"begnum\",10]", ["color","green","shift",0.6] );
//
//
//	fghi = randPts(2,z=[3,6]);
//	MarkPts( fghi, ["r",0.05] );
//	Chain( fghi, ["r",0.02, "closed", false, "color","blue"] ); 
//	LabelPts( fghi, fghi, opt=["scale",0.02,"color","blue"] );
//	//LabelPt( fghi[0], "scale=0.02", ["color","blue"] );
//}
//
////LabelPts_demo_3_prefix_begnum_etc();
//
//module LabelPts_demo()
//{
//	//LabelPts_demo_1();
//	//LabelPts_demo_2_scale();
//	LabelPts_demo_3_prefix_begnum_etc();
//}
////LabelPts_demo();


module Line0(pq,opt=[])
{   
    //echom("Line0");
    r = hash( opt, "r", 0.015 );
    fn = hash( opt, "fn", 6); 
    
//    pqr= len(pq)==2?app(pq,randPt()):pq;
//    qpr= sel(pqr, [1,0,2]);
//    
//    ptsP = arcPts( pqr, a=90, a2=360*(fn-1)/fn, rad=r, n=3 );
//    ptsQ = reverse( arcPts( qpr, a=90, a2=360*(fn-1)/fn, rad=r, n=3 ));
//    
//    pts = concat( ptsP, ptsQ );
//
//    faces = faces("rod", nside=fn); 
//    color( hash(opt,"color"), hash(opt,"transp"))
//    polyhedron( points = pts, faces= faces );
//    
    color( hash(opt, "color"), hash(opt, "transp",1) )
    hull()
    {   
       translate( pq[0])
       sphere( r= r*mm ); 
               
       translate( pq[1])
       sphere( r= r*mm);

    }        
   
}

module ChainByHull( pts, opt=[]) // for debug use
{
    for( i = [1:len(pts)-1] )
        Line0( [pts[i-1], pts[i]], opt=opt );
}    

//Line0( randPts(3) );
//Chain( randPts(10) );
////========================================================
//Line =["Line", "pq,ops", "n/a", "3D, Shape",
//"
// Given a 2-pointer pq and ops, draw a line between p,q in pq. The
//;; default ops:

module Line(pq, opt=[])
{
    //_mdoc("Line","");

    pqr= len(pq)==2?concat(pq, [randPt()]):pq; 
    
    df_opt=[ "r", 0.05  // radius
        , "rP", undef   // radius at the P end
        , "rQ", undef   // radius at the Q end
    
        , "len", d01(pqr) 
        , "ext", 0  // Extend outward. A # for len or "r#" for ratio
                    //   Could be array [extP,extQ] 
        , "extP",0  // extend outward from P pt
        , "extQ",0  // extend outward from Q pt
    
        , "fn", $fn
        , "markpts", false // Mark pts. false:disable; true:default
    
        , "ptsP", undef // csec pts defining the surface on P
        , "ptsQ", undef // csec pts defining the surface on Q
    
                           // hash to customize
        , "dim", false // true or hash, to show dimension. 
    
        , "a", 0    
        , "aP", 0
        , "aQ", 0
    
        , "rotate", 0  // rotate the line pts about the lPQ
                       // counter-clockwise away from plPQR
                       
        , "shift", undef
        ]; 
     
    _opt= update( df_opt, opt);
    function opt(k,df)=hash(_opt,k,df);

    r = opt("r")*mm;
    rP= or(opt("rP"),r);
    rQ= or(opt("rQ"),r);
    rot= opt("rotate");
    
//    echo( _fmth( ["r",r,"rP",rP,"rQ",rQ
//                 ,"ptsP", opt("ptsP")
//                 ,"ptsR", opt("ptsP")] ) );
    
    fn= opt("fn");
    tw= opt("twist");
    sh= opt("shift");
    //_markpts = opt("markpts"); 
    //markpts = _markpts==true?[]:_markpts;
    
                 
    //
    // set len
    // 
    len= opt("len")*mm;
    ext= opt("ext");
    // if "r0.4", treat it as a ratio and convert to len
        _extP=or(opt("extP"), isarr(ext)?ext[0]:ext);
        _extQ=or(opt("extQ"), isarr(ext)?ext[1]:ext);
        extP = (begwith(_extP,"r")
                ?num(slice(_extP,1))*len: _extP)*mm ;
        extQ = (begwith(_extQ,"r")
                ?num(slice(_extQ,1))*len: _extQ)*mm ;

    newpq= linePts( pqr, len= [extP,extQ] );
    
    P= newpq[0]; 
    Q= newpq[1]; 
    R= newpq[2]; //anglePt( pqr, pqr[2];
    pqr2= [P,Q,R];

    //dim=opt("dim");
    //if(dim) Dim2( pqr2, opt=ishash(dim)?dim:[]); //
    //echo("=> pqr2", pqr2 );
    
    // S is to make same clockwise-ness on P and Q
    //
    //  o-----o================o
    //  S     P                Q
    S = onlinePt(pqr2, len=-1);   
    spr = [S,P,R]; 
    //_ptsR = opt( "ptsR" );
    ptsP = or( opt("ptsP") 
             , arcPts( pqr2, a=90, a2=360*(fn-1)/fn+rot, rad=rP, n=fn )
             );
    ptsQ = or( opt("ptsQ") //hash( _opt, "ptsQ")
             , arcPts( spr, a=90, a2=360*(fn-1)/fn+rot, rad=rQ, n=fn )
             );
    
    pts = concat( ptsP, ptsQ );

    markptsopt = subopt1( df_opt= df_opt
                 , u_opt = opt
                 , subname = "markpts"
                 , df_subopt=["r",0.06
                             ,"chain",["color","black","r",0.02]]
                 , com_keys=[]
                 ); 
    if( markptsopt ) MarkPts( pts, markptsopt );
    
    //echo("markpts", markpts);
    
//    if(ishash(markpts))
//        Chainf(pts, concat(markpts,["labelbeg",0]) );
    
    faces = faces("rod", nside=fn); 
    //echo("faces", faces);
    
    color(opt("color"), opt("transp"))
    polyhedron( points = pts, faces= faces );
}

module Line_demo_1_basic() 
{ //_mdoc("Line_demo_1_basic","");
 
    pqr = randPts(3);
    Line( pqr );
    
    pqr2= trslN( pqr, 1.5);
    Line( pqr2, ["r",0.4] );
    MarkPts( pqr2, ["label","PQR","chain",true] );
    LabelPt( R(pqr2), "r=0.4");
    
    pqr3= trslN( pqr2, 2);
    Line( pqr3, ["rP",0.8, "rR", 0.002] );
    MarkPts( pqr3, ["label","PQR","chain",true] );
    LabelPt( R(pqr3), "rP=0.8,rR=0.002");

}    
//Line_demo_1_basic();

//module Line_demo_2()
//{_mdoc("Line_demo_2","");
//    
//    pqr = randPts(3);
//    //MarkPts( p01(pqr), ["labels","PQ"] );
//    Line( pqr, ["r",0.2, "len", 3, "dim",true] );
//    //Dim2( pqr );
//    
//    pq2= randPts(2);
//    Line(pq2);
//    LabelPt( pq2[0], "Line(pq)");
//}    
//Line_demo_2();


module Line_test( opt ){doctest (Line, opt=opt); }


//module Line_demo_1_default()
//{
//	echom("Line_demo_1_default");
//	pq= randPts(2);
//	//MarkPts(pq);
//	Line(pq ); //, ["markpts",true]);
//}

module Line_demo_1a_ext()
{
	
	echom("Line_demo_1a_ext");
	pqr= pqr();
	MarkPts(pqr,["label","PQR","chain",true]);
	Line(pqr, ["color","black","ext",1]);
	Line(pqr, ["transp",0.3,"r",0.2]);
    LabelPt( Q(pqr), opt=["shift",0.1,"text","   ext=1"] );
    

	pqr2= trslN( pqr, 1.5);
	MarkPts(pqr2,["label","PQR","chain",true]);
	Line(pqr2, ["color","black","ext", -1]);
	Line(pqr2, ["color","red", "transp",0.3,"r",0.2]);
    LabelPt( Q(pqr2), opt=["color","red","shift",0.1,"text","   ext=-1"] );
    

	pqr3= trslN( pqr2, 1.5);
	MarkPts(pqr3,["label","PQR","chain",true]);
	Line(pqr3, ["color","black","ext", [1,-1]]);
	Line(pqr3, ["color","green", "transp",0.3,"r",0.2]);
    LabelPt( Q(pqr3), opt=["color","green","shift",0.1,"text","   ext=[1,-1]"] );


	pqr4= trslN( pqr3, 1.5);
	MarkPts(pqr4,["label","PQR","chain",true]);
	Line(pqr4, ["color","black","ext", ["r0.5","r-0.5"]]);
	Line(pqr4, ["color","purple", "transp",0.3,"r",0.2]);
    LabelPt( Q(pqr4), opt=["color","purple","shift",0.1,"text","   ext=[\"r0.5\",\"r-0.5\"]: ratio of PQ"] );


	pqr5= trslN( pqr4, 2.5);
	MarkPts(pqr5,["label","PQR","chain",true]);
	Line(pqr5, ["color","black","extP", "r0.5"]);
	Line(pqr5, ["color","purple", "transp",0.3,"r",0.2]);
    LabelPt( Q(pqr5), opt=["color","purple","shift",0.1,"text","   extP=\"r0.5\": ratio of PQ"] );
    

}
//Line_demo_1a_extension();



module Line_demo_8_rotate()
{
    
    echom("Line_demo_8_rotate");
	pqr= pqr();
	MarkPts(pqr,["label","PQR","chain",true, "plane",true]);
	Line(pqr, ["r",1,"fn",6]);
    //LabelPt( Q(pqr), opt=["shift",0.1,"text","   ext=1"] );
    

	pqr2= trslN( pqr, 2.5);
	MarkPts(pqr2,["label","PQR","chain",true, "plane",true]);
	Line(pqr2, ["color","red", "r",1,"fn",6,"rotate",30]);
    //LabelPt( Q(pqr2), opt=["color","red","shift",0.1,"text","   ext=-1"] );
    
}
//Line_demo_8_rotate();


module Line_demo_9a_block()
{
	echom("Line_demo_9a_block");
	pq= randPts(2);
	//pq = [ [2,1,3], [4,3,2]];
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"style","block"]);
}


module Line_demo_9a2_block_extension()
{
	opt=["transp",0.3,  "markpts",true,"style","block"] ;

	echom("Line_demo_9a2_block_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, update( opt, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line(pq2, update( opt, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	//Line(pq3, update( opt, [["color","blue"],["extension0", 1],["extension1", 1]]));

	pq4= randPts(2);
	//MarkPts(pq3);
	//Line(pq4, update( opt, [["color","purple"],["extension0", -0.5],["extension1", -0.5]]));

}

module Line_demo_9b_block()
{
	echom("Line_demo_9b_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	opt= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 //, "markpts",true
		];

	Line([O, [0,0,z+1]], [ "style","block"
					   //, ["markpts",true]
			  		   , "width",0.5
					   , "depth",4
					  ]);

	Line([[0,0,w], [x,0,w]], update(opt, ["color","red"]));
	Line([[x-w,0,0], [x-w,0,z]], update(opt, ["color","green"]) );

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(opt, ["color","blue", "rotate",360*$t, "markpts",true]
		 ) );
	
}


module Line_demo_9c_block()
{
	echom("Line_demo_9c_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	opt= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], opt);
	Line([[x-w,0,0], [x-w,0,z]], opt);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(opt, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","corner2"] // <====
					  ) );


}

module Line_demo_9d_block()
{
	echom("Line_demo_9d_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	opt= [ //["markpts",true]
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], opt);
	Line([[x-w,0,0], [x-w,0,z]], opt);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(opt, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid1"] // <====
					  ) );

}

module Line_demo_9e_block()
{
	echom("Line_demo_9e_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	opt= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [//"markpts",true
			  			 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], opt);
	Line([[x-w,0,0], [x-w,0,z]], opt);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(opt, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid2"] // <====
					  ) );

}


module Line_demo_9f_block()
{
	echom("Line_demo_9f_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	opt= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [//"markpts",true
		 				 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], opt);
	Line([[x-w,0,0], [x-w,0,z]], opt);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(opt, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","center"] // <====
					  ) );

}

module Line_demo_9g_block()
{
	echom("Line_demo_9g_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	opt= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], ["style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], opt);
	Line([[x-w,0,0], [x-w,0,z]], opt);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(opt, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift",[2*w, 0,w] ]// <====
					  ) );

}

module Line_demo_10_shift()
{
	echom("Line_demo_10_shift");
	x=5; 	y=6; 	z=4; 	w=0.5;

	opt= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
		 	 , "r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "green"
				, "transp",0.5
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "transp",0.5
			    ]
			,"shift", "corner2"
			 ]);
}

module Line_demo2()
{
	pts = [ [-1,0,-1], [2,3,4]  ];
	MarkPts( pts );
	pts = randPts(2);
	//Line2( pts,[["r",0.5]] );
	//MarkPts(pts, [["transp",0.3],["r",0.25]] );
	Line( pts //, [["style","block"]
		, ["markpts",true
		 , "style","block", "transp",0.5
		, "rotate", $t*60
		, "shift", "corner"
		, "head", ["style","cone"]
		, "tail", ["style","cone"]
			]
			);
}

module Line_demo_11_block_touch()
{
	echom("Line_demo_11_block_touch");
	x=5; 	y=6; 	z=4; 	w=0.5;

	opt= [ "markpts",true
		 , "style","block"
		 , "width",w
		 , "depth",y
		 , "transp",0.5
		 ];

	p_touch = [x/3,y/3,z/3];
	MarkPts([ p_touch ] , ["r",0.3,"transp",0.3]);

	//Line([O, [0,0,z+1]], [ ["style","block"]
	//				   , ["width",w]
	//				   , ["depth",y]
	//				  ]);
	//Line([[0,0,w], [x,0,w]], opt);
	//Line([[x-w,0,0], [x-w,0,z]], opt);

	pts = [[x, 0, z], [0, 0, z+1]];
	//pts=randPts(2);
	Line( pts );
	//MarkPts( pts );
	
	Line( pts
		, update(opt, ["color","blue"//, "shift","corner2"
					  ,"touch", p_touch]  // <====
					  ) );
}

module Line_demo_12_verify_2D()
{
	//pq= [ [ 1.5, 1, 0 ], [ 1, 3, 0 ] ];
	pq_= randPts(2);
	echo(pq_);
	
	pq= [ [ pq_[0].x, pq_[0].y,0]
		, [ pq_[1].x, pq_[1].y,0] ];
	echo(pq);
	MarkPts( pq, ["grid",true] );
	Line( pq, ["markpts", true] );

	/*
	pq2= [ [ 1.5, 0,1 ], [ 1, 0,3 ] ];
	MarkPts( pq2, ["grid",true] );
	Line( pq2, ["markpts", true] );

	pq3= [ [ 0,1.5, 1 ], [ 0, 1, 3 ] ];
	MarkPts( pq3, ["grid",true] );
	Line( pq3, ["markpts", true] );
	*/
}

module Line_demo()
{   echom("Line_demo");
	Line_demo_1_default();
	//Line_demo_1a_extension(); // 2014.5.18
	//Line_demo_2_sizeColor();
	//Line_demo_3a_head_cone();
	//Line_demo_3b_head_cone_settings();
	//Line_demo_4a_tail_cone();
	//Line_demo_4b_tail_cone();
	//Line_demo_5a_headtail_cone();
	//Line_demo_5b_headtail_cone();
	//Line_demo_6a_headtail_sphere();
	//Line_demo_7_headtail_fn();
	//Line_demo_8_rotate();
	//Line_demo_9a_block();
	//Line_demo_9a2_block_extension();
	//Line_demo_9b_block();
	//Line_demo_9c_block();
	//Line_demo_9d_block();
	//Line_demo_9e_block();
	//Line_demo_9f_block();
	//Line_demo_9g_block();
	//Line_demo_10_shift();
	//Line_demo_11_block_touch();
	//Line_demo_12_verify_2D();
}
//Line_test( ["mode",22] );
//Line_demo();



//========================================================
Line2 =[["Line2", "pq,ops", "n/a", "3D, Shape"]
	  ,["Given a 2-pointer pq and ops, draw a line between p,q in pq."
	   ,"The default ops:"
		,"                                           " 
		," [[''r'', 0.05]                   // radius of the line    "
		," ,[''style'',''solid'']       // solid | dash | block      "
		," ,[''color'', ''yellow'']                                  "
		," ,[''transp'', 1]                       // transparancy    "
		," ,[''fn'', $fn]                                            "
		," ,[''rotate'', 0]             // rotate alout the line     "
		,", [''markpts'', false]      // Run MarkPts if true"
	    ," ,[''shift'', ''corner''] //corner|corner2|mid1|mid2|center   "
		,"  //................. style-block settings                 "
		," ,[''width'', 1]                                           "
		," ,[''depth'', 6]                                           "
		," ,[''touch'', false ] // a pt where the block will rotate  "
		,"                      // until the block touches the pt.   "
		,"  //................. style-dash settings                  "
		," ,[''dash'', 0.4]     // len of dash                       "
		," ,[''space'', false]  // space between dashes              "
		,"  //................. head/tail                            "
		," ,[''head'',head_ops ]                                     "
		," ,[''tail'',tail_ops ]                                     "
		," ]                                                         "
		,"where head_ops and tail_ops both are:                      "
		,"    [[''style'',false]   // false|cone|sphere              "
		,"    ,[''len'',line_r*6]       // length                    "
		,"    ,[''r'',line_r*3]         // radius                    "    
		,"    ,[''r0'',0]          // smaller radius                 "
		,"    ,[''fn'',ops(''fn'')]                                  "
		,"    ,[''rotate'',0]      // rotate about the line          "  
		,"    ,[''color'', ops(''color'')]                           " 
		,"    ,[''transp'',ops(''transp'')]                          "
		,"    ]                                                      "
		]
		];

module Line2( pq, ops){

	
	//echom("Line");
	//echo("In Line, original pq: ", pq);

	ops= updates( OPS,
		[ "r", 0.05			// radius of the line
		, "style","solid"   // solid | dash | block
		, "markpts", false
		, "shift", "corner" //corner|corner2|mid1|mid2|center
		//................. style-block settings	
		, "w", 1  //,"width", 1
		, "depth", 6
		, "touch", false  // a pt where the block will rotate 
					    // until the block touches the pt.  
		//................. style-dash settings	
		, "dash", 0.4     // len of dash
		, "space", false  // space between dashes
		//................. head and tail extension 2014.5.18
		, "extension0", 0  
		, "extension1", 0  
		//....................
		, "head", false
		, "tail", false

		], ops); function ops(k)= hash(ops,k);
	
	if(ops("markpts")) MarkPts(pq);	

	style = ops("style");
	r 	  = ops("r");
	w     = ops("w");
	headops  = ops("head");
	tailops  = ops("tail");
	depth = ops("depth");
	extension0 = ops("extension0");
	extension1 = ops("extension1");

	//echo("style",style, "rotate", ops("rotate"));

	// Re-sort p,q to make p has the min xyz. This is to 
	// simplify the calc of rotation later.
	p = minyPts(minxPts(minzPts(pq)))[0];
	switched = p!=pq[0];
	//echo("switched? ", switched);
	q = switched?pq[0]:pq[1]; //:pq[0]; 
	L = norm(p-q);
	
	x = q[0]-p[0];
	y = q[1]-p[1];
	z = q[2]-p[2];

	rot = rotangles_z2p( q-p ); 
	//echo_("rot={_}",[rot]);

	/*  
	KEEP THIS SECTION for debugging style=block
	//============================
	pz = [0,0,L];
	pm = rmx( rot[0] )* pz;
	Line( [O, pz]);
	Line( [O, pm] );
	
	pn = rmy( rot[1] ) * pm + p;
	Line( [O+p, pn], r=0.2, color="blue", transp=0.3);
	MarkPts( [pz,pm,pn], [["transp",0.3],["r",0.3]] );
	
	//============================
	*/


	// The shift is a translation [x,y,z] determines how (or, where 
	// on the line/block) the line/block attaches itself to pq. It
	// can be any 3-pointer. If style=block, one of the following 
	// preset values also works:
	//
	//    corner			mid1			mid2			center 
	//    q-----+      +-----+      +-----+      +-----+                      
	//   / \     \    p \     \    / q     \    / \     \     
	//  p   +-----+  +   +-----+  +   +-----+  + p +-----+
	//   \ /     /    \ q     /    p /     /    \ /     / 
 	//    +-----+      +-----+      +-----+      +-----+  

	//    corner2
	//    +-----+   
	//   / \     \  
	//  +   q-----+ 
	//   \ /     /  
 	//    p-----+   
	//

	shift_ = ops("shift");
	shift =  isarr(shift_)? shift_  // if shift_ is something like [2,-1,1]
	:hash(                          // if shift_ is one of the following  
		[ "corner",  [0,0,0] 
		, "corner2", [ -w, 0, 0] 
		, "mid1",    [ -w/2, 0 ,0] 
		, "mid2",    [ 0, -depth/2,0] 
		, "center" , [ -w/2, -depth/2, 0 ] 
		], shift_
	);
    		

	// head/tail settings:
	//
	// Each has a "len" for the length and a "r" for the radius. 
	// Also a r0 if needed depending on head/tail style (for
	// example, cone). Possible head/tail styles: 
	// 	
	// | sphere 
	// | cone : this is in fact a cylinder. In this case, the 
	// |        r and r0 values are assigned to the r1, r2 
	// |        arguments of the cylinder. It's default setting
	// |		   is to make an arrow.

	head0= updates( ops, 
			[ "style",false // false|cone|sphere
			, "len",r*6
			, "r",r*3
			, "r0",0
			], isarr( headops )?headops:[] );

	tail0= updates( ops,
			[ "style",false
			, "len",r*6
			, "r",r*3
			, "r0",0
			], isarr( tailops )?tailops:[] );

	// We need to chk if head/tail should be reversed
	head = switched?tail0:head0;
	tail = switched?head0:tail0;	

	function head(k)= hash(head,k);
	function tail(k)= hash(tail,k);
	
	hstyle = head("style");
	tstyle = tail("style");

	hl 	   = hstyle?head("len"):0;
	tl 	   = tstyle?tail("len"):0;


	// Rotation of head/tail around the line. 
	// If not defined, follow the line rotation
	//hrot   = hasht(head, "rotate", ops("rotate"));
	//trot   = hasht(tail, "rotate", ops("rotate"));	
	hrot   = hash(head, "rotate", ops("rotate"));
	trot   = hash(tail, "rotate", ops("rotate"));
	
	// headcut/tailcut: the length that is required to cut away
	// from the line to accomodate the head/tail. This determines
	// where exactly the head/tail is gonna be placed. For example, 
	// a cone (or arrow) starts from the exact p or q location
	// and extends its length inward toward the center of line.
	// A sphere however is placed at exactly the q or q, so half
	// of it will be extended outside of the p-q line. 
	headcut = hash( [ "cone", hl-0.0001 
					,"sphere", 0
					,false, 0
					], hstyle );
	tailcut = hash( [ "cone", tl-0.0001 
					,"sphere", 0
					,false, 0
					], tstyle );	


	// LL is the actual length that is shorter or equal to L
	// depending on the styles of head and tail. 
	//LL = L- headcut - tailcut;
	LL = L- headcut - tailcut + extension0 + extension1;

	
	// The point to start/end the line taking into consideration 
	// of headcut/tailcut. onlinePt is a function returning the
	// xyz of a pt lies along a line specified by pq.
	ph= head("style")? onlinePt([p,q], ratio = (L-headcut)/L):q; 
	pt= tail("style")? onlinePt([p,q], ratio = tailcut/L):p; 
	

	/*	
	------------ Calc the angle to "rotate to touch pt" in style_block
 
	Let: pqr the 3-pointer on the plane of block surface
		 pt : the touch pt

	        Pt
    		    /|
    		d1 /	 | 			
    		  /  | d2
    		 /a  |
		p----+------q-----r

	If we have r (a 3rd pt on block surface other than pq), we can calc:

		d1 = norm( pt-pq[0])
		d2 = distPtPl( pt, pqr )
	
	BUT, how do we find that 3rd pt r ?

	We know that the block is first placed on yz-plane when created.
	So any pt on yz-plane, like [0,1,1] serves the purpose. 

	We know that the transformation of style-block is:

		translate( tail("style")?pt:p )	 
		rotate(rot)
		rotate( [0, 0, ops("rotate")])
		translate( shift )
		cube( ... )

	By applying "this process" to [0,1,1] (skipping the last 3 lines),
	we should find the 3rd r pt we want. 

	So, how to come up with the forumla for "this process" ?

	We should already have all tools for this (see the following), 
	but for some reason it hasn't worked out yet. Has to come back.

	// First a unit pt on yz surface:
	puz = [0,1,1];

	// This pt is shifted following the shift of the block:
	puz_shift = puz + shift ;

	// It then rotates about z-axis following the block's ops("rotate"):
	puz_opsrot = rmz( ops("rotate"))* puz_shift ;

	// Then the real rotation, first about x, then y:
	puz_rx = rmx( rot.x )*puz_opsrot;
	puz_ry = rmy(rot.y)*puz_rx;

	// It then needs to translocate with the block:
	p3rd = puz_ry +(tail("style")?pt:p);	

	This p3rd is on the surface of the block.

	echo("ops[rotate]", ops("rotate"));
	echo("shift ", shift);
	echo("puz_opsrot ", puz_opsrot);
	echo("puz_rx ", puz_rx);
	echo("puz_ry ", puz_ry);
	echo("p3rd ", p3rd );
	
	//MarkPts([ puz, puz_opsrot, puz_rx, puz_ry, p3rd]
	//	,[["r",0.2],["transp",0.4]] );

	MarkPts([puz, p3rd],[["r",0.2],["transp",0.4]]);

	
	touch = ops("touch");
	//echo("touch ", touch);
	p3rd= rmy(rot.y)*
			rmx(rot.x)*
				rmz( ops("rotate"))* ([0,1,1]+shift)
			 + (tail("style")?pt:p);
	p3rd= rmy(rot.y)*
			rmx(rot.x)*
				rmz( ops("rotate"))* ([0,1,1]+shift)
			 + (tail("style")?pt:p);
	//MarkPts( [ p3rd ], [["r",0.2],["colors",["blue"]],["transp",0.4]] );
	d1 = norm( touch-p);
	d2 = distPtPl( touch, [p,q,p3rd] );
	rotang = -asin(d2/d1);
	//MarkPts([p3rd],[["r",0.2],["transp",0.4]]);
	echo("d1 = ", d1);
	echo("d2 = ", d2 );
	echo("rotang ", rotang);
	
*/
	touch = ops("touch");
	prerot =touch? rotang: ops("rotate");
	//echo("prerot ", prerot);
			

	//echo("Before drawing line, rot = ", rot);

	// ------------------------------------           The line itself :	
	if(style== "dash"){

		color(ops("color"), ops("transp"))
		DashLine([pt,ph], ops);

	} else if (style=="block") {    // block

		translate( tail("style")?pt:p )	 
		rotate(rot)
		rotate( [0,0, prerot]) //ops("rotate")])
		color(ops("color"), ops("transp"))
		translate( shift+ [0,0,switched?-extension1:-extension0] )
    		cube( [ops("width"), ops("depth"), LL ]
			, $fn=ops("fn"));

	} else {                       // solid

		translate( tail("style")?pt:p) 
		rotate(rot)
		rotate( [0,0, ops("rotate")])
    		color(ops("color"), ops("transp"))
		translate( shift+ [0,0,switched?-extension1:-extension0] )
    		cylinder( h=LL, r=r , $fn=ops("fn"));
	} 
 


	// ---------------------------------- The tail :
	//		
	if(tstyle=="cone")
	{
		translate( p )  
		rotate( rot)
		rotate( [0,0,trot] )
		color(tail("color"), tail("transp"))
		translate( shift )
    		cylinder( h=tl, r1=tail("r0"), r2=tail("r"), $fn=tail("fn"));

	} else 

	if (tstyle=="sphere")
	{
		translate( switched?pt:p ) 
		rotate( rot)
		rotate( [0,0,trot] )
		color(tail("color"), tail("transp"), $fn=tail("fn"))
		translate( shift )
    		sphere( r=tail("r"));
	} 

	// ----------------------------------- The head :
	// 
	if(hstyle=="cone")
	{
		translate( ph )  
		rotate( rot)
		rotate( [0,0,hrot] )
		color(head("color"), head("transp"))
		translate( shift )
    		cylinder( h=hl, r2=head("r0"), r1=head("r"), $fn=head("fn"));

	} else

	if (hstyle=="sphere")
	{
		translate( q ) 
		rotate( rot)
		rotate( [0,0,hrot] )
		color( head("color"), head("transp"), $fn=head("fn"))
		translate( shift )
    		sphere( r=head("r"));
	} 
	
} // Line2 

module Line2_test( ops ){doctest (Line, ops=ops); }


module Line2_demo_1_default()
{
	echom("Line2_demo_1_default");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true]);
}

module Line2_demo_1a_extension()
{
	ops=["transp",0.3,  "markpts",true, "transp",0.4] ;

	echom("Line2_demo_1a_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, update(ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line2(pq2, update(ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	Line2(pq3, update(ops, ["color","blue","extension0", 1,"extension1", 1]));

	pq4= randPts(2);
	//MarkPts(pq3);
	Line2(pq4, update(ops, ["color","purple","extension0", -0.5,"extension1", -0.5]));

}

module Line2_demo_2_sizeColor()
{
	echom("Line2_demo_2_sizeColor");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, [ "r", 0.5
			 , "color","green"
			 , "transp",0.3
			 , "fn", 6
			 , "markpts",true
			 ]);
}

module Line2_demo_3a_head_cone()
{
	echom("Line2_demo_3a_head_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true, "head"
			  ,["style","cone"]
			 ]);
}

module Line2_demo_3b_head_cone_settings()
{
	echom("Line2_demo_3b_head_cone_settings");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true,"head"
			  , [ "style","cone"
				,"color","blue"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5
			    ]
			 ]);
}

module Line2_demo_4a_tail_cone()
{
	echom("Line2_demo_4a_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				, "color","red"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5		    
				]
			 ]);
}

module Line2_demo_4b_tail_cone()
{
	echom("Line2_demo_4b_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				,"r", 0.2
				, "r0",0.6
				]
			 ]);
}

module Line2_demo_5a_headtail_cone()
{
	echom("Line2_demo_5a_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq,["markpts",true
			  ,"head"
			  , [ "style","cone"
			    ]
			  
			, "tail"
			  , [ "style","cone"
			 	]
			  
			 ]);
}

module Line2_demo_5b_headtail_cone()
{
	echom("Line2_demo_5b_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"head"
			  , [ "style","cone"
				, "color","blue"
				, "r", 0.4
				, "len",1
			 	, "transp", 0.6
				, "fn", 5
    
			    ]
			, "tail"
			  , [ "style","cone"
				, "r", 0.2
				, "r0",0.6
			 	]
			 ]);
}

module Line2_demo_6a_headtail_sphere()
{
	echom("Line2_demo_6a_headtail_sphere");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"head"
			  , [ "style","sphere"
				, "color", "yellow"
				, "r",.5
				, "transp", 0.4
			    ]
			  
			, "tail"
			  , [ "style","sphere"
				,"color", "red"
				, "r", .4
				, "transp", 0.4
				]
			  
			 ]);
}


module Line2_demo_7_headtail_fn()
{
	echom("Line2_demo_7_headtail_fn");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				]
			 ]);
}


module Line2_demo_8_rotate()
{
	echom("Line2_demo_8_rotate");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				, "rotate", 45  //<====
				]
			 ]);
}


module Line2_demo_9a_block()
{
	echom("Line2_demo_9a_block");
	pq= randPts(2);
	//pq = [ [2,1,3], [4,3,2]];
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"style","block"]);
}


module Line2_demo_9a2_block_extension()
{
	ops=["transp",0.3,  "markpts",true,"style","block"] ;

	echom("Line2_demo_9a2_block_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, update( ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line2(pq2, update( ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	//Line2(pq3, update( ops, [["color","blue"],["extension0", 1],["extension1", 1]]));

	pq4= randPts(2);
	//MarkPts(pq3);
	//Line2(pq4, update( ops, [["color","purple"],["extension0", -0.5],["extension1", -0.5]]));

}

module Line2_demo_9b_block()
{
	echom("Line2_demo_9b_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 //, "markpts",true
		];

	Line2([O, [0,0,z+1]], [ "style","block"
					   //, ["markpts",true]
			  		   , "width",0.5
					   , "depth",4
					  ]);

	Line2([[0,0,w], [x,0,w]], update(ops, ["color","red"]));
	Line2([[x-w,0,0], [x-w,0,z]], update(ops, ["color","green"]) );

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true]
		 ) );
	
}


module Line2_demo_9c_block()
{
	echom("Line2_demo_9c_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","corner2"] // <====
					  ) );


}

module Line2_demo_9d_block()
{
	echom("Line2_demo_9d_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //["markpts",true]
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid1"] // <====
					  ) );

}

module Line2_demo_9e_block()
{
	echom("Line2_demo_9e_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [//"markpts",true
			  			 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid2"] // <====
					  ) );

}


module Line2_demo_9f_block()
{
	echom("Line2_demo_9f_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [//"markpts",true
		 				 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","center"] // <====
					  ) );

}

module Line2_demo_9g_block()
{
	echom("Line2_demo_9g_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], ["style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift",[2*w, 0,w] ]// <====
					  ) );

}

module Line2_demo_10_shift()
{
	echom("Line2_demo_10_shift");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
		 	 , "r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "green"
				, "transp",0.5
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "transp",0.5
			    ]
			,"shift", "corner2"
			 ]);
}

module Line2_demo2()
{
	pts = [ [-1,0,-1], [2,3,4]  ];
	MarkPts( pts );
	pts = randPts(2);
	//Line2( pts,[["r",0.5]] );
	//MarkPts(pts, [["transp",0.3],["r",0.25]] );
	Line2( pts //, [["style","block"]
		, ["markpts",true
		 , "style","block", "transp",0.5
		, "rotate", $t*60
		, "shift", "corner"
		, "head", ["style","cone"]
		, "tail", ["style","cone"]
			]
			);
}

module Line2_demo_11_block_touch()
{
	echom("Line2_demo_11_block_touch");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "markpts",true
		 , "style","block"
		 , "width",w
		 , "depth",y
		 , "transp",0.5
		 ];

	p_touch = [x/3,y/3,z/3];
	MarkPts([ p_touch ] , ["r",0.3,"transp",0.3]);

	//Line2([O, [0,0,z+1]], [ ["style","block"]
	//				   , ["width",w]
	//				   , ["depth",y]
	//				  ]);
	//Line2([[0,0,w], [x,0,w]], ops);
	//Line2([[x-w,0,0], [x-w,0,z]], ops);

	pts = [[x, 0, z], [0, 0, z+1]];
	//pts=randPts(2);
	Line2( pts );
	//MarkPts( pts );
	
	Line2( pts
		, update(ops, ["color","blue"//, "shift","corner2"
					  ,"touch", p_touch]  // <====
					  ) );
}

module Line2_demo_12_verify_2D()
{
	//pq= [ [ 1.5, 1, 0 ], [ 1, 3, 0 ] ];
	pq_= randPts(2);
	echo(pq_);
	
	pq= [ [ pq_[0].x, pq_[0].y,0]
		, [ pq_[1].x, pq_[1].y,0] ];
	echo(pq);
	MarkPts( pq, ["grid",true] );
	Line2( pq, ["markpts", true] );

	/*
	pq2= [ [ 1.5, 0,1 ], [ 1, 0,3 ] ];
	MarkPts( pq2, ["grid",true] );
	Line2( pq2, ["markpts", true] );

	pq3= [ [ 0,1.5, 1 ], [ 0, 1, 3 ] ];
	MarkPts( pq3, ["grid",true] );
	Line2( pq3, ["markpts", true] );
	*/
}

module Line2_demo()
{
	//Line2_demo_1_default();
	//Line2_demo_1a_extension(); // 2014.5.18
	//Line2_demo_2_sizeColor();
	//Line2_demo_3a_head_cone();
	//Line2_demo_3b_head_cone_settings();
	//Line2_demo_4a_tail_cone();
	//Line2_demo_4b_tail_cone();
	//Line2_demo_5a_headtail_cone();
	//Line2_demo_5b_headtail_cone();
	//Line2_demo_6a_headtail_sphere();
	//Line2_demo_7_headtail_fn();
	//Line2_demo_8_rotate();
	Line2_demo_9a_block();
	//Line2_demo_9a2_block_extension();
	//Line2_demo_9b_block();
	//Line2_demo_9c_block();
	//Line2_demo_9d_block();
	//Line2_demo_9e_block();
	//Line2_demo_9f_block();
	//Line2_demo_9g_block();
	//Line2_demo_10_shift();
	//Line2_demo_11_block_touch();
	//Line2_demo_12_verify_2D();

	

}
//Line2_test( ["mode",22] );
//Line2_demo();

//========================================================
LineByHull=["LineByHull", "pq,r=0.05,fn=15", "n/a", "3D,Shape",
"
 Draw a line as thick as 2*r between the two points in pq using
;; hull(). This approach doesn't need to calc rotation/translation,
;; so the faithfulness of outcome is guaranteed. But is much slower
;; so not good for routine use.
"];

module LineByHull( pq,r=0.05, fn=15 )
{
	hull(){
		translate(pq[0])
		sphere(r=r, $fn=fn);
		translate(pq[1])
		sphere(r=r, $fn=fn);
	}
}

module LineByHull_test( ops ){ doctest(LineByHull, ops=ops); }

module LineByHull_demo()
{
	LineByHull( randPts(2) );
}
//LineByHull_demo();
//doc(LineByHull);




//========================================================
Mark90=["Mark90", "pqr,ops", "n/a", "Grid, Math, Geometry, 3D, Shape",
" Given a 3-pointer pqr and an ops, make a L-shape at the middle
;; angle if it is 90-degree. The default ops, other than usual
;; r, color and transp, has ''len''= 0.4
;;
;; ops: [ ''len'', 0.4
;;      , ''color'', false
;;      , ''transp'', 0.5
;;      , ''r'', 0.01
;;      ]
"];
	
module Mark90(pqr, ops)
{
	ops=update(   
		[ "len", 0.4
		, "color","green"
		, "transp", 0.5
		, "r", 0.01
		], ops );
	function ops(k) = hash(ops,k);

	p = pqr[0];
	q = pqr[1];
	r = pqr[2];
    len = ops("len");
	m = min( 0.4*norm(p-q), 0.4*norm(r-q), len);
	
//	pq1 = onlinePt( [q,p], len=m); //ratio= ops("len")/ norm(p-q) );
//	pq3 = onlinePt( [q,r], len=m); //ratio= ops("len")/ norm(r-q) );
// 	sqpts = squarePts( [pq1, q, pq3] );
	PP = onlinePt( [q,p], len=m); //ratio= ops("len")/ norm(p-q) );
	RR = onlinePt( [q,r], len=m); //ratio= ops("len")/ norm(r-q) );
 	//sqpts = squarePts( [PP, q, RR] );
		
    //echo(is90 = is90( [p,q], [q,r]), apqr = angle(pqr) );
    
	if (is90( [p,q], [q,r] ) ){
        
        // ChainByHull is for debug use
        ChainByHull( [PP, cornerPt([PP,q,RR]), RR], ops );
//        Chain0( [PP, cornerPt([PP,q,RR]), RR], ops );
//		Line0( [sqpts[2],sqpts[3]],ops );
//		Line0( [sqpts[0],sqpts[3]],ops );
	}
}


module Mark90_test(ops){ doctest( Mark90, ops=ops); }
//Mark90_demo();
//Mark90_test();
//doc(Mark90);


//========================================================
module MarkBezierPtsAt( bzrtn, opt=[] )                        
{     
   /*
   bzrtn: [bzpts,pqrs,param] = 
   [ bzpts, 
   , [P,Q,R,S]
   ,[["T",app(tlnQ,abiQ), "aT",aPQR, "aH",aH[0], "dH",dH[0], "H",Hq ]
    ,["T",app(tlnR,abiR), "aT",aQRS, "aH",aH[1], "dH",dH[1], "H",Hr ]
    ]
   ] 
   */ 
    echom("MarkBezierPtsAt");
    
    df_opt=[
          "showends",[true,true]
        , "bzpts", true
        , "markT", true
        , "markH", true
    ];
   _opt = update( df_opt, opt );
   function opt(k,nf)=hash(_opt, k,nf); 
   
   _showends= opt("showends");
   showends= !_showends? [0,0]
             : isarr(_showends)? _showends
             : [ _showends, _showends ];
   showhead = showends[0];
   showend  = showends[1];
   bzpts = (showhead && showend)? bzrtn[0]
           : showhead? slice(bzrtn[0], 0, -1)
           : showend ? slice(bzrtn[0], 1)
           : slice(bzrtn[0], 1,-1);
   pqrs  = bzrtn[1];
   param = bzrtn[2];
   
   bzptsopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "bzpts"
                     , df_subopt= "cl=red;chain;r=0.05"
                     , com_keys=["color","transp"]
                     ); 
                     
   markTopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markT"
                     , df_subopt=["show",[true,true], "r",0.015
                                 ,"chain",["r",0.015],"color","green"
                                 ,"ball",false,"transp",0.1,"label",true]
                     , com_keys=["color","transp"]
                     ); 
                     
   markHopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markH"
                     , df_subopt=["show",[true,true]
                                 ,"chain",["r",0.015],"r",0.05
                                 ,"color","gray"
                                 ,"transp",0.5,"label",["text",["","H"]]]
                     , com_keys=["color","transp"]
                     );  
   echo( bzptsopt= bzptsopt );
   echo( markTopt = markTopt );
   echo( markHopt = markHopt );
   
   //--------------------------------------
   // Given pqrs and generated Bezier curve pts
   if( bzptsopt )
   { MarkPts( bzpts, bzptsopt ); }

   //--------------------------------------
   // abi and tangent lines  
   /*
   param = 
   ,[["T",app(tlnQ,abiQ), "aT",aPQR, "aH",aH[0], "dH",dH[0], "H",Hq ]
    ,["T",app(tlnR,abiR), "aT",aQRS, "aH",aH[1], "dH",dH[1], "H",Hr ]
    ]
   */
   for( i=[0,1] )
   {
       if( markTopt && hash( markTopt,"show")[i] )
       {
           T = hash( param[i],"T");
           abi = T[2];
           Chain( [pqrs[1+i], abi], markTopt); 
           Mark90( [ T[0],pqrs[1+i], abi], markTopt );
           MarkPts( p01(T), markTopt); 
       }    
   }   
 
   for( i=[0,1] )
   {
       if( markHopt && hash( markHopt,"show")[i] )
       {
           H = hash( param[i],"H");
           MarkPts( [pqrs[1+i],H] 
                  , markHopt); 
       }    
   }   
}//MarkBezierPts
  
module MarkBezierPtsAt_demo(){
  
  n=5;
    
  pqr=pqr();
  MarkPts( pqr, "ch=[r,0.025];l=PQR");
  bzrtn= getBezierPtsAt( pqr, at=0, n=n);
         
  echo( bzrtn = arrprn( bzrtn, dep=2));  
  MarkBezierPtsAt( bzrtn );
    
    
  pqr2 = trslN(pqr, 4);
  MarkPts( pqr2, "ch=[r,0.025];cl=red;l=PQR");
  bzrtn2= getBezierPtsAt( pqr2, at=0, n=n);
  echo( bzrtn2 = arrprn( bzrtn2, dep=2));  
  MarkBezierPtsAt( bzrtn2, ["markT",["show",[1,0]]
                         , "markH",["show",[1,0]]
                         ]
                );
    
  pqr3 = trslN(pqr, 8);
  
  MarkPts( pqr3, "ch=[r,0.025];cl=green;l=PQR");
  
  bzrtn3= getBezierPtsAt( pqr3, at=0, n=n);
  echo( bzrtn3 = arrprn( bzrtn3, dep=2));  
  MarkBezierPtsAt( bzrtn3); 
//  , ["markT",["show",[1,0]]
//                         , "markH",["show",[1,0]]
//                         ]
//                );
                
  bzrtn32= getBezierPtsAt( pqr3,at=1, n=n, btw=[1,2]);
  echo( bzrtn32 = arrprn( bzrtn32, dep=2));  
  MarkBezierPtsAt( bzrtn32, ["markT",["show",[0,1]]
                         , "markH",["show",[0,1]]
                         ]
                ); 
}    
//MarkBezierPtsAt_demo();


module MarkBezierPtsAt_demo_chain(){
  
  n=10;
  
    
  pts= randchain(8, d=[-5,5], a=[45,135], a2=[0,45]);
//  pts =  [[2.12448, 3.3809, 3.13628], [1.70918, 2.70776, 2.63994], [0.776025, 3.09614, 4.13017], [1.49755, 3.3722, 5.68667], [2.40417, 2.75162, 5.48191], [2.0055, 2.36655, 4.81182], [0.416727, 2.38473, 6.83724], [1.89168, 2.62708, 7.14462]];  
  echo( pts= pts );
    
  MarkPts(pts,"ch=[color,blue,r,0.01];r=0.03");

  module smoothPts( pts, opt=[] ){
      
      for( i=[0:len(pts)-2] )
        { 
            bzrtn = getBezierPtsAt( pts, i, n=10, aH=.95, dH=0.4 );
            MarkBezierPtsAt( bzrtn, ["bzpts",["r",0.02], "markT",false, "markH",false] );
        }
  }   

  smoothPts( pts );
  
  pts2 = [ [4,0,0],[4,4,0],[0,4,0],[0,0,0],[4,0,0]];
  MarkPts(pts2,"ch=[color,blue,r,0.01];r=0.03");

  MarkBezierPts( pts2 );
  
}    
//MarkBezierPtsAt_demo_chain();


//========================================================
MarkPt=["MarkPt", "pts,ops", "n/a", "3D, Shape, Point",
"
 Given a series of points (pts) and ops, make a sphere at each pt.
;; Default ops:
;;
;;   [ ''r'',0.1
;;   , ''colors'',[''red'',''green'',''blue'',''purple'',''khaki'']
;;   , ''transp'',1
;;   , ''grid'',false
;;   , ''echo'',false
;;   ]
;;
;; The colors follow what's in colors. If len(pts)>len(colors),
;; colors recycles from the the 1st one and the transparancy and
;; radius reduced by ratio accordingly by a ratio each cycle. If
;; grid is true, draw PtGrid for each point; if echo is true,
;; echo each point.
"];


module MarkPt(pt, opt=[], mti=0) 
{
    if(ECHO_MOD_TREE) echom("MarkPt", mti);
        
    df_opt= [ "r",0.05
            , "transp",1
            , "color","red"
    
            , "grid", false
            , "label",false
            , "ball",true
            ];
    
    opt= update( df_opt
               , opt
               , df= ["g","grid","b","ball"
                     ,"l","label"]
               ) ;
    function opt(k,nf)= hash(opt,k,nf);
    //echo(opt=opt);
    
    gridopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "grid"
                     , df_subopt= ["mode",2]
                     , com_keys=["color"]
                     );
    labelopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "label"
                     , df_subopt= ["text","0", "shift",0.15]
                     , com_keys=["color"]
                     , mainprop="text"
                     );    
	if( opt("ball") ){
        translate( pt )
        color( hash(opt,"color"), hash(opt,"transp") )
        sphere( r = hash(opt,"r")*mm );
    }    
    //echo(gridopt = gridopt );
	if( gridopt ){ PtGrid(pt, gridopt, mti=mti+1); }
	if( labelopt ){ 
        LabelPt( pt
               , opt=labelopt, mti=mti+1 ); 
     }
}

module MarkPt_demo()
{
  MarkPt( randPt() );
  MarkPt( randPt(), "g" );
  MarkPt( randPt(), "l;cl=green" );
  MarkPt( randPt(), "b=false;g;l;cl=blue" );
}    
//MarkPt_demo();

module MarkPt_demo_grid()
{
  MarkPt( randPt() );
  MarkPt( randPt(), "g" );
  MarkPt( randPt(), ["cl=green", "grid",true] );
  MarkPt( randPt(), "cl=blue;g=[color,gold,r,0.05]" );
  //MarkPt( randPt(), "g=[color,blue]" );
} 
//MarkPt_demo_grid();

module MarkPt_demo_label()
{
  MarkPt( randPt(), "l;cl=gold" );
  MarkPt( randPt(), "cl=red;l=test" );
  MarkPt( randPt(), "cl=green;l=[isxyz,true]" );
  MarkPt( randPt(), ["cl=blue;r=0.2"
                    ,"label",["scale=0.015"
                             ,"isxyz",true
                             ,"tmpl","{*`a|d3|wr=,in}"
                             ]
                    ] );
   MarkPt( randPt(), ["cl=purple;ball=false"
                    ,"label",["scale=0.015"
                             ,"isxyz",true
                             ,"tmpl","{*`a|d3|wr=,in}"
                             ]
                    ] );   
  //MarkPt( randPt(), "g=[color,blue]" );
} 
//MarkPt_demo_label();

//========================================================
MarkPts=["MarkPts", "pts,ops", "n/a", "3D, Shape, Point",
"
 Given a series of points (pts) and ops, make a sphere at each pt.
;; Default ops:
;;
;;   [ ''r'',0.1
;;   , ''colors'',[''red'',''green'',''blue'',''purple'',''khaki'']
;;   , ''transp'',1
;;   , ''grid'',false
;;   , ''echo'',false
;;   ]
;;
;; The colors follow what's in colors. If len(pts)>len(colors),
;; colors recycles from the the 1st one and the transparancy and
;; radius reduced by ratio accordingly by a ratio each cycle. If
;; grid is true, draw PtGrid for each point; if echo is true,
;; echo each point.
"];

module MarkPts(pts, opt=[], mti=0) 
{
    //if(ECHO_MOD_TREE) echom("MarkPts", mti);
        
    df_opt= [ "r",0.1
            , "transp",1
            , "color",""
        	, "colors", ["red","green","blue","purple"]
			, "samesize", false
			, "sametransp", false
            , "rng",undef
    
	        , "ball", true
            , "grid", false
            , "label", false
            , "chain", false
            , "plane", false
            ];
    //opt= update( df_opt, opt );
    opt= update( df_opt, opt
           , df=["g","grid","l","label"
                ,"ch","chain","pl","plane"
                ,"b","ball"]
           );    
	function opt(k,nf)= hash(opt,k,nf);
    label = opt("label");
    
    gridopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "grid"
                     , df_subopt= ["mode",2]
                     , com_keys=["color","transp"]
                     );
    labelopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "label"
                     , df_subopt= ["text", range(pts)]
                     , com_keys=["color","transp"]
                     , mainprop="text"
                     ); 
    //echo(label=label, "<br/>", labelopt=labelopt);
    chainopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "chain"
                     , df_subopt= ["color",undef]
                     , com_keys=["color","transp"]
                     );                      
    planeopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "plane"
                     , df_subopt= ["color",undef]
                     , com_keys=["color","transp"]
                     );          
                     
    color  = opt("color");           
	colors = opt("colors");
	transp = opt("transp");
	samesize = opt("samesize");
	sametransp=opt("sametransp");
	label = opt("label");
     cL = len(colors);
    _rng = opt("rng"); 
    rng = isint(_rng)?[_rng]
              :isarr(_rng)? _rng: range(pts);    
    
	// color cycle. The more ccycle, the smaller the sphere
	// and the more transparent they are.  
	function ccycle(i) = floor(i/cL); 
    //echo( indices=indices);
    if(opt("ball"))
	for (j=range(rng)){
        i = rng[j];
		translate(get(pts,i))
		color( or( color
                 , opt("colors")[j-cL*ccycle(j)])   // cycling thru colors
			 , transp* ( sametransp?1:(1-0.3*ccycle(j))) // transp level
			 ) 
		sphere(r=opt("r")*mm*(samesize?1:(1-0.15*ccycle(j)))
			  );
	}

    if( gridopt ){
        for (j=range(rng)){
            i = rng[j]; 
             PtGrid( get(pts,i), opt=gridopt); 
        }    
    }
    if( planeopt ){ Plane(pts, opt=planeopt); }
    
    if( opt("echopts") ) echo( 
                  join( [for(i=range(pts))
                           str( "pts[",i,"]=", pts[i])
                         ]
                      ,"<br/>"));
    
                  
    labeltext = hash(labelopt,"text");
	if(labelopt){ 
        // pts for label. If to label the Q in pqr, the label
        // is placed on a pt outside of a_pqr. This is to make
        // sure that a polygon is not crowded by text.         
        labelpts= len(pts)>2? 
                  [ for( p3= ranges( pts, cover=[1,1]
                            , returnitems=true) )
                     onlinePt( [p3[1], angleBisectPt(p3)], len= -0.3 )
                  ]
                  :len(pts)==2? linePts(pts, len=0.3)
                  :pts;         
                    
        //for (i=range(pts) )
        for( j= range(rng)){
            i = rng[j];
            LabelPt( get(labelpts,i)
                   , opt= update( labelopt, ["text"
                                            ,get(labeltext,j)] )
                   , mti=mti+1 );
        }    
    }
//    if(chainopt) Chain0(pts, opt=chainopt);
    
    // ChainByHull is for debug only:
    if(chainopt) ChainByHull(pts, opt=chainopt);
}
//MarkPts(pqr()), ["chain",["r",0.02]] );

module MarkPts_test( ops ){ doctest(MarkPts,ops=ops);}
module MarkPts_demo_1()
{
    echom( "MarkPts_demo_1" );
	pts= randPts(4);
    MarkPts( pts, opt= "l,c,echopts");

	pts2= trslN(pts,3);
    MarkPts( pts2, "l;echopts;ch=[color,red]" );

	pts3= trslN(pts2,3);
    MarkPts( pts3, opt= [ "echopts",true
                       , "label",true //["text",range(pts)]
                       , "chain",["color","green"]
                       , "rng", [0,-2]
                       ] 
                       );
   
	pts4= trslN(pts3,3);
    MarkPts( pts4, opt= [ "echopts",true
                       //, "label",true //["text",range(pts)]
                       , "chain",["color","blue"]
                       , "rng", [0,-1]
                       , "label", "AB"
                       ] 
                       );     
}
//MarkPts_demo_1();


module MarkPts_demo_2_more_points()
{
	echom( "MarkPts_demo_2_more_points" );
	MarkPts( randPts(20));
}
//MarkPts_demo_2_more_points();


module MarkPts_demo_2_more_points_2()
{
	echom( "MarkPts_demo_2_more_points_2" );
	MarkPts( randPts(20), ["samesize",true]);
}
//MarkPts_demo_2_more_points_2();


module MarkPts_demo_2_more_points_3()
{
	echom( "MarkPts_demo_2_more_points_3" );
	MarkPts( randPts(30), "samesize,label||color=red");
//	MarkPts( randPts(30), ["samesize",true, "colors",["red"]
//    ,"label",true]);
}
//MarkPts_demo_2_more_points_3();


module MarkPts_demo_3_label()
{
	echom( "MarkPts_demo_3_label" );
    
    pqr=pqr();
    
	MarkPts( pqr, "ch;l;cl=red;r=0.15");
    
    MarkPts( trslN(pqr,3), "ch;cl=green;l=PQR" );
    
	MarkPts( trslN(pqr,6), "ch;cl=blue;l=[isxyz,true,color,red]");
    
    MarkPts( trslN(pqr,9)
           , ["ch;cl=purple","label",["text",range(3)
                                  ,"tmpl","P[{_`a|d2}]"
                                  ,"color","purple"
                                  ]]
            ); // labels = P1, P2

    
    MarkPts( trslN(pqr,12), "ch;clr=gold;i=0;l=A"); 
    MarkPts( trslN(pqr,15), "ch;clr=red;i=[0,-1];l=BC"); 

}
//MarkPts_demo_3_label();

module MarkPts_demo_4_grid()
{
    echom( "MarkPts_demo_4_grid" );
    pqr= pqr();
	MarkPts( pqr, "g,c||r=0.05");//["grid",true, "chain",true, "r",0.05] );
    LabelPt( R(pqr), "grid mode=2");
    
    MarkPts( trslN(pqr,4)
            , ["grid",["mode",1,"color","red","r",0.05], "chain",true, "r",0.05] );
    LabelPt( R(trslN(pqr,3)), "grid mode=1");

    MarkPts( trslN(pqr,8)
            , ["grid",["mode",3,"color","green"]
              , "chain",["r",0.05]] );  
    LabelPt( R(trslN(pqr,7)), "grid mode=3");
    
}
//MarkPts_demo_4_grid();

module MarkPts_demo_4_grid_2()
{
    echom( "MarkPts_demo_4_grid_2" );
	MarkPts( randPts(2, d=[-4,-1])
    , "g=[mode,1,color,red];ch;r=0.05");
    
    MarkPts( randPts(3,d=[-2,1]), 
    , "g=[mode,2,color,green,r,0.02];ch=[closed,true]");
    
    MarkPts( randPts(3,d=[2,4]), 
    , "i=1;g=[mode,2,color,blue,r,0.02];ch");
    
}
//MarkPts_demo_4_grid_2();

//module MarkPts_demo_4_grid_3_indices()
//{
//    echom( "MarkPts_demo_4_grid_3_indices" );
////	MarkPts( randPts(5), ["grid",["mode",1,"color","red"], "chain",true, "r",0.05] );
//    
//    MarkPts( randPts(5), ["grid",["mode",2,"color","green"], "chain",true, "r",0.05,"indices",[0,-1]] );
//}
//MarkPts_demo_4_grid_3_indices();


module MarkPts_demo_5_chain()
{
    echom( "MarkPts_demo_5_chain" );
	MarkPts( randPts(4)
    ,"l;ch=[color,red,closed,true]");
    
    MarkPts( pqr()
    , "l;b=false;ch=[color,green,r,0.05,closed,true]");
    
}
//MarkPts_demo_5_chain();

module MarkPts_demo_plane()
{
    
    echom( "MarkPts_demo_plane" );
	
    pqr2=pqr();
    pts2= app( pqr2, cornerPt(pqr2) );
	MarkPts( pts2,"l;pl=[color,red]");
//    , ["label",true, "plane",["th",1,"color","red"]]
//    );    

    pqr3=randPts(3, d=[3,6]);
    //MarkPts( pqr3 );
    pts3= squarePts(pqr3);
	MarkPts( pts3,"l;pl=[th,1,color,green]");
    
}    
//MarkPts_demo_plane();



//========================================================
Normal=["Normal", "pqr,ops","n/a","3D, Line, Geometry",
 " Given a 3-pointer (pqr) and ops, draw the normal of plane made up by
;; pqr. The normal will be made passed through Q (pqr[1]). Its length
;; is determined by L/3 where L the shortest length of 3 nside. The
;; default ops:
;;
;; [ ''r''      , 0.03
;; , ''transp'' , 0.5
;; , ''mark90p'', true
;; , ''mark90r'', true
;; , ''len''    , L/3
;; ]
"];
 
module Normal_test( ops=["mode",13] ){ doctest( Normal, ops=ops); }

module Normal( pqr, ops )
{
	L = min( d01(pqr), d02(pqr), d12(pqr));
	ops = update( [
			  "r"     , 0.03
			  ,"len"  , L/3
			, "transp", 1
			, "mark90p", false
			, "mark90r", true
			, "reverse", false
			, "label", false
			], ops);
	function ops(k)= hash(ops,k);
	echo(ops);
	mark90p = ops("mark90p");
	mark90r = ops("mark90r");

	Nrm = (ops("reverse")?-1:1)*normal( pqr )/3;
	_N = Nrm + pqr[1];
	N = onlinePt( [ORIGIN+ pqr[1],_N]
				, len=ops("len") );

	MarkPts([N]);

	if( mark90p ) { Mark90( [ N,pqr[1],pqr[0] ], mark90p ); } 
	if( mark90r ) { Mark90( [ N,pqr[1],pqr[2] ], mark90r ); } 

	Line0([ ORIGIN+ pqr[1], N ], concat( ["head",["style","cone"]], ops) );
	
}

module Normal_demo_1()
{
	pqr = randPts(3);
	MarkPts( pqr, ["grid",["mode",2]] );
	Chain( pqr );
	Normal( pqr );

	color("red") Normal(pqr, ["reverse",true]);
}

module Normal_demo()
{
	Normal_demo_1();
}
//Normal_demo();



//========================================================
Pie=[[]];

module Pie( angle, r, ops=[] )
{
	
	ops = concat( ops
		// ,["h",1
		//  ]
		 ,OPS);
	
	function ops(k) = hash( ops, k);
	
	arcpts = concat( [[0,0]], arcPts( a=angle, r = r, count = angle) );
	//echo( "arcpts: ", arcpts);

	paths = [range(len(arcpts))];

	color(ops("color"), ops("transp"))
	polygon ( points=arcpts, paths=paths); 



}

module Pie_demo()
{

	Pie(angle=60, r=3);

	ColorAxes();
	
	translate( [0,0,-1])
	Pie( angle=80, r=4 );

	scale([1,1,0.5])
	translate( [0,0,-2])
	Pie( angle=280, r=4 );
	
	translate( [0,0,-3])
	Pie( angle=360, r=5 ); //<==== note this

}
//Pie_demo();



//========================================================
Plane=[ "Plane", "pqr,opt", "n/a", "2D, Geometry, Shape", 
" 
 Given a 3-pointer pqr and opt, draw a plane in space based on
;; settings in opt:
;;
;;  [ 'mode', 1 // mode = 1~4
;;  , 'color', 'brown'
;;  , 'transp', .4
;;  , 'frame', false
;;  , 'r', 0.005
;;  , 'th', 0 // thickness
;;  ]
;;
;; # mode=1: rect parallel to axes planes
;;
;;  The 1st pt is the anchor(starting) pt
;;  The 2nd pt is treated like sizes:
;;     [ x,y,* ] = parallel to xy-plane (* = any)
;;     [ x,0,z ] = parallel to xz-plane
;;     [ 0,y,z ] = parallel to yz-plane
;;  The 3rd pt, r, is ignored.
;;  Plane( [ [1,2,3],[4,-5,6]], [['mode',1]] )
;;     draw a plane of size 4 on x, 5 on -y
;;     starting from [1,2,3]
;;
;; # mode=2 rectangle
;;
;;   line p1-p2 is a side of rectangle.
;;   p3 is on the side line parallel to line p1~p2
;;
;; # mode=3 triangle
;;
;; # mode=4 parallelogram
;;
;; # frame: default= false
;;
;;   Draw a frame around using Chain if frame is true or is
;;    a hash as the opt option for Chain. For example
;;
;;    Plane( pqr, [['frame', ['color', 'blue'] ]] )
;;
;;    draw a blue frame.
;;
;; NEW 2014.6.19: len(pqr) can be >3. But all faces connect to pqr[0].
"];

//doc(Plane);


module Plane(pts, opt=[])
{
	//echom("Plane");
	//echo("pqr=", pqr);
    df_opt=[  
        "transp", .7
        ,"th",undef // thickness
        //,"frame", false
        //,"r",0.005
        ,"rotate",0
        ,"rotaxis",[0,1] // 2-indices as a line pass thru
                         // 2 pts in pts, or a 2-pointer
                         // like [ [2,3,4],[0,1,0]] 
        ,"shift",0 // int: along N dir; 3=[0,0,3]
        ,"rotfirst",true
        ,"markpts", false
        ,"hide",false
         ];
	opt = update(df_opt, opt, df=["rot","rotate"
                                 ,"sh","shift"]);
	function opt(k,nf)=hash(opt,k,nf);
	c = opt("color");
	t = opt("transp");
	//m = opt("mode");
	th= opt("th");
    sh = opt("shift");
    rot= opt("rotate");
    _rotaxis= opt("rotaxis");
    rotaxis= isln(_rotaxis)?_rotaxis
             : sel(pts, _rotaxis);
    
    //----------------------------------------------
    df_markpts= ["r",0.06,"chain"
                  ,["color","green","r",0.005, "closed",true]
                  ,"label",true
                  ] ;
    mp_opt= subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markpts"
                     , df_subopt= df_markpts
                     , com_keys=[]
                     );
//    mp_opt = markptsopt?
//             update( markptsopt, ["chain",false]): false;
//    chainopt= hash( markptsopt, "chain");
    
    //mp_opt? update( markptsopt, ["closed",true] ):false;
    

    //------------------------------------
    
//    function rot(pts0, pts)=
//    (
//        let( pts2 = pts==undef? pts0:pts 
//           )
//        pts==undef?
//            concat( 
//            [ pts2[0], pts2[1]]   
//            , [for(i=[2:len(pts2)-1])
//                rotpqr( [pts0[0],pts0[1], pts2[i]], rot)[2]
//                
//              ]
//            )
//        : [for(i=range(pts2))
//                rotpqr( [pts0[0],pts0[1], pts2[i]], rot)[2]
//          ]   
//    );

//------------------------------------
//    echo( pts = pts );
//    echo( shift_pts = shf(pts));
//    echo( rot_pts = rot(pts));
//    echo( rot_shift_pts = rot(pts, shf(pts)));
//    echo( shift_rot_pts = shf(pts, rot(pts)));
    function shf(pts0, pts)=
    (
        let( pts = pts==undef? pts0:pts
           , pqr = sel(pts0,[0,1,2])
           , _sh = isint(sh)?[0,0,sh]: sh
           , uvp = uv( p10(pqr))
           , uvn = uv( pqr[1], N(pqr))
           , uvm = uv( pqr[1], N2(pqr))
           , sh = uvp*_sh[0]+ uvn*_sh[2]+uvm*_sh[1]
           )
        [for(p=pts) p+sh ]
    );
     
    pts2= sh&&rot?
//           ( opt("rotfirst")? shf(pts, rot(pts))
//                           : rot(pts, shf(pts))
            ( opt("rotfirst")? shf(pts, rotPts(pts, rotaxis,rot))
                           : rotPts(shf(pts), rotaxis,rot)

           ): sh? shf(pts)
            : rot? rotPts(pts, rotaxis,rot)
            : pts;

     npt = len(pts2);
    
    if( isnum(th)){
        //echo(th=th, len_pts2 = len(pts2));
        p3s= sel( pts2, ranges( pts2, cover=[1,1]) );
        
        pts1= trslN(pts2,-th/2);
        pts2= trslN(pts2,th/2);
        

        
//        ns = [for(i=range(pts))
//                let( pqr= [ pts[ i==0?i= 
        
//        n= uv([ORIGIN,normal( pts )])*th/2;
//        pts1 = [ for(p=pts) p+n ];
//        pts2 = [ for(p=pts) p-n ];
        ptss = concat( pts1,pts2); 
        //MarkPts( ptss, "c,l");   
        //echo(normal=uv(normal(pts)), th=th, n=n, ptss=ptss);
        //MarkPts( ptss, ["label", range(ptss)] );
        if(!opt("hide")){
        color( opt("color"), opt("transp") )
        polyhedron( points=ptss
                  , faces= faces(shape="rod",nside=len(pts)) 
                  );
        }
        if(mp_opt){
           echo(mp_opt=mp_opt);
           MarkPts( pts1, mp_opt );
           MarkPts( pts2, update( mp_opt
                                , hash(mp_opt,"label")==true?
                                   ["label",["text",range(len(pts1),len(ptss) )]]:[]
                                ));
           ch = hash( mp_opt, "chain"); 
           if(ch)
           {  for(i= range( pts1))
                Line0( [pts1[i],pts2[i]], opt=ch);
           }
  
        }//if_mp_opt
    } else {
        if(!opt("hide")){
        color( opt("color"), opt("transp") )
        polyhedron( points=pts2, faces= [range(pts)] );
        }    
        if(mp_opt) MarkPts(pts2, mp_opt);
        
    }//if
   

}//Plane

module Plane_demo_1()
{
    echom("Plane_demo_1");
    
    pts1= pqr();
    MarkPts( pts1, ["label",range(3)] );
    Plane( pts1, ["th",0.2] );
    
    pqr2 = trslN(pts1,1.5);
    pts2 = squarePts( pqr2 );
    MarkPts( pqr2, ["label",range(4),"chain",true] );
    Plane( pts2, ["color","red","th",0.5] );
    
    pqr3 = trslN(pts2,2);
    pts3 = squarePts( pqr3 );
    MarkPts( pqr3, ["label",range(4),"chain",true] );
    Plane( pts3, ["color","green","th",2] );

    pqr4 = trslN(pts3,3);
    pts4 = squarePts( pqr4 );
    MarkPts( pqr4, ["label",range(4),"chain",true] );
    Plane( pts4, ["color","red","th",2, "shift",[1,0,0]] );

    pqr5 = trslN(pts4,3);
    pts5 = squarePts( pqr5 );
    MarkPts( pqr5, ["label",range(4),"chain",true] );
    Plane( pts5, ["color","green","th",2, "shift",[0,1,0]] );

    pqr6 = trslN(pts5,4);
    pts6 = squarePts( pqr6 );
    MarkPts( pqr6, ["label",range(4),"chain",true] );
    Plane( pts6, ["color","blue","th",2, "shift",[0,0,1]] );

}
//Plane_demo_1();

module Plane_demo_2_rotate() // 2014.6.19
{
    echom("Plane_demo_2_rotate");
    
    pqr = pqr();
    // Make sure it's a board plane (but not look like a line):
    pts1=  [  onlinePt( p01(pqr), len=-4)
             , pqr[1]
             , onlinePt( [projPt( p01(pqr), pqr[2]), pqr[2]], len=4)
           ] ;
    
    
    MarkPts( pts1, ["label",range(3)] );
    echo("th=0");
    Plane( pts1);
    
    pqr2 = trslN(pts1,1.5);
    pts2 = squarePts( pqr2 );
    MarkPts( pqr2, ["label",range(4),"chain",true] );
    echo(_red("red, th=0.5"));
    Plane( pts2, ["color","red","th",0.5] );
    
    pqr3 = trslN(pts2,2);
    pts3 = squarePts( pqr3 );
    MarkPts( pqr3, ["label",range(4),"chain",true] );
    echo(_green("green, th=1"));
    Plane( pts3, ["color","green","th",1] );

    pqr4 = trslN(pts3,3);
    pts4 = squarePts( pqr4 );
    MarkPts( pqr4, ["label",range(4),"chain",true] );
    echo(_red("red, th 1, rot 30"));
    Plane( pts4, ["color","red","th",1, "rotate",30] );

    pqr5 = trslN(pts4,3);
    pts5 = squarePts( pqr5 );
    MarkPts( pqr5, ["label",range(4),"chain",true] );
    echo(_green("green, th 1 shift 1"));
    Plane( pts5, ["color","green","th",1,  "shift",1] );

//    pts5s = trslN( pts5, 1);
//    MarkPts( pts5s, "l,ch");
//    pts5sr = [ for(i=range(pts5s))
//                 rotpqr( [ pts5[0], pts5[1], pts5s[i]], 30)[2]
//             ];
//    MarkPts( pts5sr, "l,ch");
    

    pqr6 = trslN(pts5,4);
    pts6 = squarePts( pqr6 );
    MarkPts( pqr6, ["label",range(4),"chain",true] );
    echo(_red("red th 1 rot 45, shift 0.8, rotfirst"));
    Plane( pts6, ["color","red","th",1
                 , "rotate",45, "shift",[0,0,1]
                 , "rotfirst", true]
                 );

    pqr7 = pqr6; //trslN2(pts6,4);
    pts7 = squarePts( pqr7 );
    MarkPts( pqr7, ["label",range(4),"chain",true] );
    echo(_blue("blue, th 1 rot 45 shift 0.8, rotfirst=false"));
    Plane( pts7, ["color","blue","th",1
                 ,"rotate",45,"shift",[0,0,1] 
                 , "rotfirst", false]
                 );

    //pqr7 = pqr6; //trslN2(pts6,4);
//    pts7 = squarePts( pqr7 );
//    MarkPts( pqr7, ["label",range(4),"chain",true] );
//    Plane( pts7, ["th",1
//                 //,"rotate",45
//                 ,"shift",[0,0,0.8]] 
//                 );

}
//Plane_demo_2_rotate();

module Plane_demo_2_rotate2() // 2014.6.19
{
    echom("Plane_demo_2_rotate2");
    
    pqr = pqr();
    // Make sure it's a board plane (but not look like a line):
    pts1=  [  onlinePt( p01(pqr), len=-4)
             , pqr[1]
             , onlinePt( [projPt( p01(pqr), pqr[2]), pqr[2]], len=4)
           ] ;
    
    pqr3 = trslN(pts1,1.5);
    pts3 = squarePts( pqr3 );
    MarkPts( pts3, "l;ch=[r,0.05]");
    echo(_green("green, th=1"));
    Plane( pts3, ["th",1] );

    pqr4 = trslN(pts1,5);
    pts4 = squarePts( pqr4 );
    MarkPts( pts4,  "l;ch=[r,0.05]");
    echo(_red("red, th 1, rot 30"));
    Plane( pts4, "cl=red;th=1;rot=30");

    pqr5 = trslN(pts1,8);
    pts5 = squarePts( pqr5 );
    MarkPts( pts5, "l;ch=[r,0.05]");
    echo(_green("green, th 1 shift 1"));
    Plane( pts5, "cl=green;th=1;rot=30;rotaxis=[1,2]");
    
 
    pqr6 = trslN(pts1,14);
    pts6 = squarePts( pqr6 );
    MarkPts( pts6,  "l;ch=[r,0.05]");
    echo(_red("red th 1 rot 45, shift 0.8, rotfirst"));
    Plane( pts6, "cl=blue;th=1;rot=30;rotaxis=[0,2]");
    
    pqr7 = trslN(pts1,17);
    pts7 = squarePts( pqr7 );
    MarkPts( pts7,  "l;ch=[r,0.05]");
    echo(_red("red th 1 rot 45, shift 0.8, rotfirst"));
    Plane( pts7, "th=1;rot=90;rotaxis=[2,3]");

}
//Plane_demo_2_rotate2();

module Plane_demo_3_markpts() // 2014.6.19
{
    echom("Plane_demo_3_markpts");
    
    pqr = pqr();
    // Make sure it's a board plane (but not look like a line):
    pts1=  [  onlinePt( p10(pqr), len=2)
             , pqr[1]
             , onlinePt( [projPt( p01(pqr), pqr[2]), pqr[2]], len=3)
           ] ;
    
    
    MarkPts( pts1, "l");
    echo("th=0");
    Plane( pts1);
    
    pqr2 = trslN(pts1,1.5);
    pts2 = squarePts( pqr2 );
    echo(_red("red, th=0.5"));
    Plane( pts2, ["color","red","markpts",true] );
    
    pqr3 = trslN(pts2,2);
    pts3 = squarePts( pqr3 );
    MarkPts( pqr3, "l,c");
    echo(_green("green, th=1"));
    Plane( pts3, ["color","green","th",1] );

    pqr4 = trslN(pts3,3);
    pts4 = squarePts( pqr4 );
    MarkPts( pqr4, "c");
    echo(_red("blue, th 1, markpts"));
    Plane( pts4, ["color","blue","th",1, "hide",true,"markpts",true] );
//
//    pqr5 = trslN(pts4,3);
//    pts5 = squarePts( pqr5 );
//    MarkPts( pqr5, ["label",range(4),"chain",true] );
//    echo(_green("green, th 1 shift 1"));
//    Plane( pts5, ["color","green","th",1,  "shift",1] );
//
//    pts5s = trslN( pts5, 1);
//    MarkPts( pts5s, "l,c");
//    pts5sr = [ for(i=range(pts5s))
//                 rotpqr( [ pts5[0], pts5[1], pts5s[i]], 30)[2]
//             ];
//    MarkPts( pts5sr, "l,c");
//    
//
//    pqr6 = trslN(pts5,4);
//    pts6 = squarePts( pqr6 );
//    MarkPts( pqr6, ["label",range(4),"chain",true] );
//    echo(_red("red th 1 rot 45, shift 0.8, rotfirst"));
//    Plane( pts6, ["color","red","th",1
//                 , "rotate",45, "shift",[0,0,1]
//                 , "rotfirst", true]
//                 );
//
//    pqr7 = pqr6; //trslN2(pts6,4);
//    pts7 = squarePts( pqr7 );
//    MarkPts( pqr7, ["label",range(4),"chain",true] );
//    echo(_blue("blue, th 1 rot 45 shift 0.8, rotfirst=false"));
//    Plane( pts7, ["color","blue","th",1
//                 ,"rotate",45,"shift",[0,0,1] 
//                 , "rotfirst", false]
//                 );

    //pqr7 = pqr6; //trslN2(pts6,4);
//    pts7 = squarePts( pqr7 );
//    MarkPts( pqr7, ["label",range(4),"chain",true] );
//    Plane( pts7, ["th",1
//                 //,"rotate",45
//                 ,"shift",[0,0,0.8]] 
//                 );

}
//Plane_demo_3_markpts();

module Plane_test(opt){ doctest(Plane,opt=opt); }

//Plane_test( ["mode",22] );
//Plane_demo();


	 
//========================================================

PtGrid=["PtGrid", "pt,opt", "n/a","3D, Point, Line, Geometry" ,
"
 Given a pt and opt, draw thin cylinders from pt to axes or axis planes
;; for the purpose of displaying the pt location in 3D. Default opt:
;;
;;  [ 'highlightaxes', true
;;  , 'r',0.007    // line radius
;;  , 'projection', false, // draw projection on axis planes.
;;  ,                      // 'xy'|'yz'|'xy,yz'|...
;;  , 'mark90', 'auto' // draw a L-shape to mark the 90-degree angle
;;  , 'mode',2 // 0: none
;;             // 1: 2lines: pt~p_xy, p@xy~Origin
;;             // 2: plane and line: pt~p@xy, p@xy~x, p@xy~y
;;             // 3: block
;;      
;;         |
;;         |     |      mode 1
;;     ----+-----|-.---
;;        /  '-_ |
;;       :      ':
;;         |
;;         |     |       mode 2
;;     ----+-----|-.---
;;        /      |/
;;       :-------/
;;      / 
;;         |_______    mode 3
;;        /|      /|
;;       /-------+ |
;;    ---|-|-----|-+---
;;       |/      |/
;;       :-----'-/
;;      / 
"];
/*
    module Obj( opt=[], transl=[0,0,0] )
    {
        opt= update( ["arm", true]
                    ,opt);
        function opt(k,df)= hash(opt,k,df);

        df_arm=["len",3];
        
        opt_arm= getsubopt( opt
                    , com_keys= ["transp"]
                    , sub_dfs= ["arm", df_arm ]
                    );
        function opt_arm(k,df) = hash(opt_arm,k,df);
        
        echo("opt_arm",opt_arm);
        
        translate(transl) color(opt("color"), opt("transp"))
        cube( size=[0.5,1,2] );
        if ( opt_arm )
        {   translate( transl+[0,1, 1]) 
            color(opt_arm("color"), opt_arm("transp"))
            cube( size=[0.5,opt_arm("len"),0.5] );
        }   
    }
    
    
    module obj( opt=[] )
    {
        u_opt = opt;
        df_opt= [ "r",0.02
                , "heads",true
                , "headP",true
                , "headQ",true
                ];
        df_heads= ["r", 0.5, "len", 1];  
        df_headP= [];
        df_headQ= [];
                
        opt = update( df_opt, u_opt );
        function opt(k,df)= hash(opt,k,df);
        
        com_keys = [ "color","transp","fn"];
        
        opt_headP= getsubopt( opt=u_opt, df_opt=df_opt
                    , com_keys= com_keys;
                    , sub_dfs= ["heads", df_heads, "headP", df_headP ]
                    );
        opt_headQ= getsubopt( opt=u_opt, df_opt=df_opt
                    , com_keys=com_keys;
                    , sub_dfs= ["heads", df_heads, "headQ", df_headQ ]
                    );
            
        function opt_headP(k) = hash( opt_headP,k );
        function opt_headQ(k) = hash( opt_headQ,k ); 
    }
    
    
    PtGrid.line
    PtGrid.mark90
    
*/



module PtGrid(pt, opt, mti=0)
{
    if(ECHO_MOD_TREE) echom("PtGrid", mti);   
    
    df_proj  = []; // sub obj df
    //df_mark90= []; // sub obj df     
    df_axes  = ["r",0.02,"transp",0.5]; // sub obj df
    com_keys = ["color","transp","r"]; // propt also be decided on root level
    
    df_opt = 
        [ "r",0.005
        , "transp", 0.4
        , "color", undef
        , "coord", [[1,0,0],[0,0,0],[0,1,0],[0,0,1]]
        , "mode", 2 // 0: none
                    // 1: line // pt:p_xy, p_xy:Origin
                    // 2: plane and line //pt:p_xy, p_xy:x, p_xy:y
                    // 3: block
        
        // -------sub obj-------
        , "proj"  , false //"xy"|"yz"|"xy,yz"|"xy,yz,xz"| ...
        , "mark90", false
        , "axes"  , true
        ]; 
       
    u_opt = opt;         // user opt. Set this so it's clear later
    opt = update( df_opt, u_opt );
    
    //echo( opt = opt );
    
    function opt(k,df)= hash(opt,k,df);
	
    lineopt= hashex(opt,["color","transp","r"]);
    //echo( lineopt = lineopt );
    r= opt("r");
	mode = opt("mode");
	
    proj = opt("proj");
    axes = opt("axes");
    //mark90 = opt("mark90");
    
    //============== sub obj =======================
    function getsub( sub_dfs )=
        subopt( opt=u_opt
              , df_opt=df_opt
              , com_keys= com_keys
              , sub_dfs= sub_dfs
              );
    opt_axisx = getsub( ["axes", df_axes, "axisx", ["color","red"]] );
    opt_axisy = getsub( ["axes", df_axes, "axisy", ["color","green"] ] );
    opt_axisz = getsub( ["axes", df_axes, "axisz", ["color","blue"] ] );
    opt_projxy= getsub( ["proj", df_proj, "projxy",["color","blue"] ] );
    opt_projxz= getsub( ["proj", df_proj, "projxz",["color","green"] ] );
    opt_projyz= getsub( ["proj", df_proj, "projyz",["color","red"] ] );     
    //opt_mark90= getsub( ["mark90", df_mark90] );

    //============== setup coord =====================

    _coord = opt("coord");
    coord = len(_coord)==3? coordPts(_coord): _coord;
    //Coord(coord);
    
    aO= coord[1]; // points defining axis
    aX= coord[0];
    aY= coord[2];
    aZ= coord[3];
    // Frequently used point:
    Pxy = projPt( pt, [aX,aO,aY]);
    Pyz = projPt( pt, [aY,aO,aZ]);
    Pxz = projPt( pt, [aX,aO,aZ]);
            
    module L_line( projPt, Pt3=aO ) // Draw an L line from pt to projPt 
    {                            // to Pt3 (df=aO that is the ORIGIN)
        pqr = [pt, projPt, Pt3 ];
        Chain( pqr, lineopt );
        //if(mark90) Mark90( pqr, opt=opt_mark90 );  
    }    
    
    // =========== pt-plane Proj lines 
    
    if(proj)
    {        
        if (has(proj,"xy")) L_line( Pxy );
        if (has(proj,"yz")) L_line( Pyz );
        if (has(proj,"xz")) L_line( Pxz );
    }    
      
//    echo( _fmth(
//       ["proj",proj] , "has(proj,\"xy\")",haskey(proj, "xy") ]
//    ));
    // ============= Grid lines 
    
    if(mode==1 && !proj && !has(proj,"xy"))  L_line( Pxy ) ;
    else
    {
        Px =  othoPt( [ aO,pt,aX] ); 
        Py =  othoPt( [ aO,pt,aY] ); 

        L_line( Pxy, Px );  // pt - Pxy - Px
        
        if (mode==2)
        {
            Line0( [Pxy,Py], opt=lineopt );
//            if(mark90) {
//                Mark90( [pt,Pxy,Px], opt=opt_mark90);
//                Mark90( [pt,Pxy,Py], opt=opt_mark90);
//            }
        }    
        else if (mode==3)
        {
            Pz =  othoPt( [ aO,pt,aZ] ); 
            Chain([ Pyz, Pz, Pxz, Px, Pxy
					  , Py, Pyz, pt, Pxy ],opt=lineopt);
			Line0( [pt, Pxz], opt=lineopt); 
            
//            if(mark90) {
//                Mark90( [pt,Pxy,Px], opt=opt_mark90);
//                Mark90( [pt,Pxy,Py], opt=opt_mark90);
//            }
        }    
    }    
 
}
//P = randPt();
//PtGrid( P, ["mode",2] );
//MarkPt( P );



//
//module PtGrid(pt, ops)
//{
//	L= norm(pt);
//	//echo_("in PtGrid, pt={_}, L={_}, L/5={_}",[pt, L,(L/5)]);
//	ops=updates( //OPS,
//		[[ "r",0.006
//		, "transp", 0.8
//		//, "color", "red"
//		//, "style", "line" // line|dashline
//		//, "dash", (L/5) //0.05]
//		//, "space", 0.05
//		//, "scale", 0
//		//, "scalefaces", "all1" 
//		//, "othoToAxis", false
//		, "projection", false //"xy"|"yz"|"xy,yz"|"xy,yz,xz"| ...
//		//, "quickgrid", true
//		, "mark90", "auto"
//		, "highlightaxes", true
//		, "mode", 2 // 0: none
//					// 1: line // pt:p_xy, p_xy:Origin
//					// 2: plane and line //pt:p_xy, p_xy:x, p_xy:y
//					// 3: block
//			 
//		], ops] );
//	function ops(k)= hash(ops,k);
//	//echo(ops);
//	r= ops("r");
//	mode = ops("mode")==true?2: ops("mode");
//	style= ops("style");
//	mark90= ops("mark90");
//	
//	xcolor = "red";
//	ycolor = "lightgreen";
//	zcolor = "blue";
//		
//	p_xy = [pt.x, pt.y, 0   ]; //pxy0(pt);
//	p_yz = [   0, pt.y, pt.z]; //p0yz(pt);
//	p_xz = [pt.x,    0, pt.z]; //px0z(pt);
//	p_x  = [pt.x,    0, 0   ]; //px00(pt);
//	p_y  = [   0, pt.y, 0   ]; //p0y0(pt);
//	p_z  = [   0,    0, pt.z]; //p00z(pt);
//
//	module line ( pq, lineops=false ) { 
//		Line( pq, update( ops, lineops) );
//	}
//
//	module chain ( pts, chainops=false) { 
//		Chain( pts, update(ops,chainops) );
//	}
//	
//	module projline (face="xy",pt2plane=true, projops=false){ 
//		// Draw a L-shape line
//		//echo("in projline, face= ", face);
//		pts=[pt, face=="xy"?p_xy:face=="xz"?p_xz:p_yz, ORIGIN];
//		if(pt2plane)
//		{	chain( pts, update(ops, ["closed",false], projops) ); }
//		else 
//		{	line( pts, update(ops, projops) ); }
//		
//		if( mark90 || mark90=="auto")
//		Mark90( pts, updates(ops, projops));
//		
//	}
//
//	//
//	// mark90
//	//
//	if( mark90=="auto" ){
//		if( mode==1 ) Mark90( [ORIGIN, p_xy, pt ], ops );
//	}
//	else if( mark90 ){
//		//Mark90( [ORIGIN, p_xy, pt ], mark90); //update(ops, mark90) );
//		Mark90( [ORIGIN, p_xy, pt ], update(ops, mark90) );
//	}
//	
//	// 
//	// grid mode
//	//
//	// echo("in PtGrid, mode = ", mode );
//
//	if(mode==1) { projline("xy"); //chain( [pt, p_xy, ORIGIN] );	
//
//	} else 
//
//	if(mode==2) { chain( [pt, p_xy, p_x] ,["closed",false]);
//				line( [p_xy, p_y] );			
//	} else
//	
//	if(mode==3) {
//				chain([ p_yz, p_z, p_xz, p_x, p_xy
//					  , p_y, p_yz, pt, p_xy ],["closed",false]);
//				line( [pt, p_xz]);
//	}
//
//	//
//	// highlight axes
//	//
//	hlops = ops("highlightaxes"); 
//	hlops = hash( ops, "highlightaxes"
//			   ,	if_v  =true
//			   , then_v=["r", r]  // <=== default axes setting
//			   );	
//	if( hlops )
//	//  assign( axesops= update( ops, hlops ))
//	{
//        axesops= update( ops, hlops );
//		//echo("hlops", hlops);
//		//echo("axesops", axesops);
//		color( hash(hlops, "color", xcolor), hash(hlops, "transp"))
//		Line( [ORIGIN, p_x], axesops );
//		color( hash(hlops, "color", ycolor), hash(hlops, "transp"))
//		Line( [ORIGIN, p_y], axesops );
//		color( hash(hlops, "color", zcolor), hash(hlops, "transp"))
//		Line( [ORIGIN, p_z], axesops );
//	}
//
//	//
//	// projection = "xy"|"yz"|"xy,yz"|"xy,yz,xz"| ... 
//	//
//	proj  = ops("projection");
//	//echo(proj);
//	if(proj)
////	assign( faces = proj==true?["xy"]    // if ==true, set to "xy"
////				:proj?split(proj,",") // else, split them
////					:false
////	)
//    {  
//    faces = proj==true?["xy"] // if ==true, set to "xy"
//			:proj?split(proj,",") // else, split them
//					:false;
//        
//	//echo(faces);
//	for( face=faces ){ 
//		//echo( "-- face = ", face);
//		if(!(mode==1 && face=="xy")) // projline drawn automatically
//			projline(face); }       // when (mode==1 && face=="xy")
//	}
//	
//}



//========================================================
Ring=["Ring", "ops", "n/a", "3D, Shape",
"
;; Given the ops, draw a ring on a plane that has its normal defined;;
;; in ops. Default ops:
;;
;;  [ 'ro',3
;;  , 'ri',2
;;  , 'r2o',0
;;  , 'r2i',0
;;  , 'target', ORIGIN
;;  , 'normal', [0,0,1]
;;  , 'rotate', 0
;;  , 'color',  false
;;  , 'transp', 1
;;  , 'fn', $fn
;;  , 'h', 1
;;  ]
"];

module Ring( ops )
{
	ops=update( [
		 "ro",3
		,"ri",2
		,"r2o",0
		,"r2i",0
		,"target", ORIGIN 
		,"normal", [0,0,1]
		,"rotate", 0      
		,"color",  false  
		,"transp", 1      
		,"fn", $fn        
		,"h",      1      
		//,"inrod", false
		//,"outrod", false
		], ops ); 
	function ops(k)= hash(ops,k);

	//echo("ops('outrod' = ", ops("outrod"));

	h  = ops("h"  );
	rc = ops("rc" );
	fn = ops("fn" );
	ro = ops("ro" );
	ri = ops("ri" );
	r2o= ops("r2o");
	r2i= ops("r2i");
	color = ops("color");
	transp= ops("transp");
	rotang= ops("rotate");

	inrod_ = ops("inrod");
	outrod_ = ops("outrod");
	//inbar_ = ops("inbar");
	//outbar_ = ops("outbar");

	outrod = !outrod_?false
			  : update([ "style", "rod" // rod|block
					   , "len", 1 
					   , "count",  12  
					   ,"color",  color  
					   ,"transp", transp      
					   ,"fn", fn        
					   , "r",  0.4      // for style = rod
					   ,"width",0.4   // for style = bar
					   ,"h", h    // for style = bar
					   ,"markcolor",false // [[1,"red"],[4,"blue"]]
					   ], outrod_ );
	function outrod(k)= hash(outrod, k);

	inrod = !inrod_?false
			  : update([ "style", "rod" // rod|block
					   , "len", ri-r2o 
					   , "count",  6  
					   ,"color",  color  
					   ,"transp", transp      
					   ,"fn", fn        
					   , "r",  0.4      // for style = rod
					   ,"width",0.4   // for style = bar
					   ,"h", h    // for style = bar
					   ,"markcolor",false // [1,"red",4,"blue"]
					   ], inrod_ );	
	function inrod(k)= hash(inrod, k);

	inrodmarkcolor= inrod("markcolor");
	outrodmarkcolor= outrod("markcolor");	
	//echo("outrodmarkcolor = ", outrodmarkcolor);

	outrodPts= arcPts( r=ro, a=360, count=outrod("count") );
	inrodPts= arcPts( r=ri, a=360, count=inrod("count") );

	//outrodstyle = trueor(outrod("style","bar")!="bar","block");

	//echo(ORIGIN);
	//echo(hash(ops,"h"));

	translate( ops("target") )
	rotate( rotangles_z2p( ops("normal") ) )
	rotate( [0,0, rotang ] ){

	  difference(){
		color( color, transp )
		cylinder(r=ro, h=h,   center=true, $fn=fn );
		cylinder(r=ri, h=h+1, center=true, $fn=fn );
	  }
	
	  if( r2o>0 ){ 
		difference(){
		color( color, transp )
			cylinder(r=r2o, h=h, center=true, $fn=fn );
			cylinder(r=r2i, h=h+1, center=true, $fn=fn );
		}
	  }
	  //------------------------------------------------
	  if( outrod ){
		if( outrod("style")=="block"){
			for( i= [0:len(outrodPts)-1] ){
                p= outrodPts[i];
				L=len(outrodPts);
				FL=floor(len(outrodPts)/4);
				rodcolor= haskey(outrodmarkcolor,i)
								?hash(outrodmarkcolor,i)
								:outrod("color"); 
					
//			  assign( p= outrodPts[i]
//					, L=len(outrodPts)
//					, FL=floor(len(outrodPts)/4)
//					, rodcolor= haskey(outrodmarkcolor,i)
//								?hash(outrodmarkcolor,i)
//								:outrod("color") 
//					)
                //{
				//echo("rodcolor = ", rodcolor );
                //echo("outrodmarkcolor = ", outrodmarkcolor );    
			   Line( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= -outrod("len") )
					] 
			     ,  update( outrod, ["shift","center"
								 ,"color",rodcolor
								  ,"depth",outrod("h") 
								  ,"extension0",
								     // Extends to cover line-curve gap: 
									hash(outrod, "extension0",
									 ro-shortside(ro, outrod("width") )
									+ GAPFILLER )
								, "rotate", outrod("rotate")>0?
										    ((i>=FL)
											&&( i< L-(L/4==FL?FL: FL+1)				  
											)?-1:1) 
                                                  *outrod("rotate")
											:0]	
				//,["color", i==0?"red": i==1?"green":outrod("color") ]

								)
                   );	
			}
		} else if (outrod("style")=="rod"){
			//for( p= outrodPts ){
			for( i= [0:len(outrodPts)-1] ){
              	p= outrodPts[i];
				rodcolor= haskey(outrodmarkcolor,i)
								?hash(outrodmarkcolor,i)
								:outrod("color"); 
					 
//			  assign( p= outrodPts[i]
//					, rodcolor= haskey(outrodmarkcolor,i)
//								?hash(outrodmarkcolor,i)
//								:outrod("color") 
//					)
                
			   Line( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= -outrod("len") 
							)
					] 
			     , update( outrod, ["color", rodcolor] )
					
                   );	
			}
		}
	
	  } // if 
	  //------------------------------------------------
	  if( inrod ){
		if( inrod("style")=="block"){
			for( i= [0:len(inrodPts)-1] ){
			    p= inrodPts[i];
				L=len(inrodPts);
				FL=floor(len(inrodPts)/4);
				rodcolor= haskey(inrodmarkcolor,i)
								?hash(inrodmarkcolor,i)
								:inrod("color") ;					
//			  assign( p= inrodPts[i]
//					, L=len(inrodPts)
//					, FL=floor(len(inrodPts)/4)
//					, rodcolor= haskey(inrodmarkcolor,i)
//								?hash(inrodmarkcolor,i)
//								:inrod("color") 
//					)
                //{
			  //echo_("L={_}, FL={_}, i={_}, i>=FL({_}), i-FL={_}, L-i={_}, i/4={_}"
				//	,[L,FL,i,i>=FL,i-FL,L-i, i/4] );	
			   Line( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= inrod("len") )
					] 
			     ,  update( inrod, [ "shift","center"
								, "color",rodcolor
								, "depth",inrod("h") 
								, "extension1",
								     // Extends to cover line-curve gap: 
									hash(inrod, "extension1", 
									 ro-shortside(ro, inrod("width") )
									+ GAPFILLER 
									)

								  
				//,["color", i==0?"red": i==1?"green":inrod("color") ]

								// If there's block rotation, need to change the
								// sign of rotation based on i  
/*
    L              FL    i_change  L-i_change
	4: +--+        1          3    1
	5: +--++                  3    2
	6: +---++                 4    2
	7: +----++                5    2 

	8: ++----++    2          6    2
	9: ++----+++              6    3
	10:++-----+++             7    3
	11:++------+++             8   3

	12:+++------+++     3     9    3
	13:+++------++++          9    4
	14:+++-------++++         10   4 
	15:+++--------++++        11   4

	16:++++--------++++   4   12   4 

*/								, "rotate", inrod("rotate")>0?
										    ((i>=FL)
											&&( i< L-(L/4==FL?FL: FL+1)				  
											)?-1:1) 
                                                  *inrod("rotate")
											:0]		
								)
                   );	
			  //}// assign
			}// for
		} else if (inrod("style")=="rod"){
			for( i= [0:len(inrodPts)-1] ){
                p= inrodPts[i];
				rodcolor= haskey(inrodmarkcolor,i)
								?hash(inrodmarkcolor,i)
								:inrod("color"); 
					
//			  assign( p= inrodPts[i]
//					, rodcolor= haskey(inrodmarkcolor,i)
//								?hash(inrodmarkcolor,i)
//								:inrod("color") 
//					)
                
			Line( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= inrod("len")
									// Extends to cover line-curve gap: 
									+ r2o-shortside(r2o, inrod("r") )
									+ GAPFILLER 
							)
					] 
			     , update( inrod, ["color", rodcolor] )
                   );	
			}
	  } // if 
	  
	  }
	 }//

} // Ring



module Ring_demo_1()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0] 
			  ,"r2o", 1
		 	  ,"r2i", 0.8
			  ,"rotate", 720*$t
			  ,"color", "lightgreen"
			  ,"outrod", ["count",6,"width",1, "style","block","h",0.5]
			  ,"inrod", ["count",4]
			  ] 
		);
	Line( pq, ["r",0.5, "extension0", 2] );
	
}	

module Ring_demo_2()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
			  ,"r2o", 1
		 	  ,"r2i", 0.8
			  ,"rotate", 720*$t
			  ,"color", "lightgreen"
			  ,"outrod", ["count",12,"width",1, "h",0.5,"color","khaki"]
			  ,"inrod", ["count",24
						,"style","block"
						,"h", 0.6
						,"width",0.1
						,"color","blue"
					    ]
			   
			  ] 
		);
	//}
	Line( pq, ["r",0.5, "extension0", 2] );
	
}

module Ring_demo_3()
{
	
	//pq = randPts(2);
	pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
			  //,"h", 5
			  ,"ro", 3.2
		 	  ,"ri", 2.8
			  ,"r2o", 1
		 	  ,"r2i", .7
			  ,"rotate", 720*$t
			  ,"color", "lightgreen" 
			  //,"outrod", ["count",12,"width",1, "h",0.5,"color","khaki"] 
			  ,"inrod",   ["count",15
						,"style","block"
						,"h", 1
						,"width",0.1
						,"color","blue"
						,"rotate", 30
						,"extension1", 0.25 // overwrite default exteionsion1
										    // to cover the gaps between rods
											// and the out curve of ring2 
					    ]
			    
			  ] 
		);


	Line( pq, [["r",0.5], ["extension0", 2]] );
	
}

module Ring_demo_4()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0] 
			  //,"h", 5
			  ,"ro", 3.2
		 	  ,"ri", 2.8
			  ,"r2o", 1
		 	  ,"r2i", .7
			  ,"rotate", 720*$t
			  ,"color", "lightgreen"
			  ,"inrod", ["count",6,"width",1, "h",0.5]
			  ,"outrod", ["count",12
						,"style","block"
						,"h", 1.6
						,"width",0.2
						,"color","blue"
						,"rotate", 60
						,"extension0", .35 // overwrite default exteionsion1
										    // to cover the gaps between rods
											// and the out curve of ring2 
					    ]
			    
			  ] 
		);


	Line( pq, ["r",0.5, "extension0", 2] );
	
}

module Ring_demo_5_markrod()
{
	
	pq = randPts(2);
	pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	pq = [ORIGIN, [0,0,3]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
			 // ,"inrod", ["count",6,"width",1, "h",0.5] 
			  ,"outrod", ["count",12
						,"style","block"
						,"color","blue"
						,"transp",0.4
						,"markcolor", [0,"red",5,"green"] 
						,"markpts", true
						]
			   	
			  ,"inrod", ["count",6
						//,"style","block"
						//,"color","blue"
						,"transp",0.4
                        ,"r",0.3
						,"markcolor", [0,"purple"] 
					    ]
			    ] 
			   
		);


	Line( pq, ["r",1.5, "extension0", 2] );
	
}

module Ring_demo_6()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
              , "ri",0
               , "h",0.3
               
			 // ,"inrod", ["count",6,"width",1, "h",0.5] 
			  ,"outrod", ["count",24, "len", 0.5
						,"style","block"
						,"markcolor", [0,"blue"] 
					    //,"color","blue"
						]
			   	
			  /*,"inrod", ["count",6
						//,"style","block"
						//,"color","blue"
						,"transp",0.4
                        ,"r",0.3
						,"markcolor", [0,"purple"] 
					    ] */
			    ] 		   
		);

	Line( pq, ["r",.5, "extension0", 0] );
}

module Ring_demo()
{
	Ring_demo_1();
	//Ring_demo_2();
	//Ring_demo_3();
	//Ring_demo_4();
	//Ring_demo_5_markrod();
    //Ring_demo_6();
	//
}
module Ring_test( ops ){ doctest(Ring,ops=ops);}

//Ring_demo();




//========================================================
Ring0=[["Ring0", "ops", "n/a", "3D, Shape"],
"
 Given an ops, draw a simple ring on a plane that has its normal defined\\
 in ops. Default ops:\\
 \\
  [ 'normal', [0,0,1] \\
  , 'target', ORIGIN  // where to translate to\\
  , 'ro',3 // outer radius\\
  , 'ri',2 // inner radius\\
  ]\\
"];

module Ring0( ops )
{
	echom("Ring0");

		ops= updates( OPS
			,[[ "normal", [0,0,1]
			 , "target", ORIGIN 
			 , "ro",3
			 , "ri",2
			 ], ops] );

		function ops(k)=hash(ops,k);

		h  = ops("h" );
		fn = ops("fn");

		//echo(ops);

		translate( ops("target") )
		rotate( rotangles_z2p( ops("normal") ) )
	  	difference(){
			color( ops("color"), ops("transp") )
			cylinder(r=ops("ro"), h=h,   center=true, $fn=fn );
			cylinder(r=ops("ri"), h=h+1, center=true, $fn=fn );
	  	}
}

module Ring0_demo_1()
{
	pq = randPts(2);
	Ring0( [ "normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   ] );
	//c = randcolor();
	//echo(c);
	Line(pq, ["r",rands( 1,20,1)[0]/10,"extension0",2, "color", randc()] );
}

module Ring0_demo_2()
{
	pq = randPts(2);
	MarkPts(pq, ["r",0.5]);
	Line(pq, ["r",0.6, "color", randc()] );
	Ring0( [ "normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "h", 10
		   , "transp", 0.3
		,"ro", 0.8, "ri", 0.7	
		   ] );
}

module Ring0_demo_3_tube()
{
	pts = randPts(9);
	pq=randPts(2);
	MarkPts(pq, ["r",0.5]);
	Line(pq, ["r",0.6, "color", randc()] );
	L = norm( pq[1]-pq[0] );
	Ring0( [ "normal", pq[1]-pq[0]
		   , "target", midPt(pq) //pq[1] 
		   , "color", randc()
		   , "h", L
		   , "transp", 0.3
		,"ro", 0.8, "ri", 0.7	
		   ] );
}

module Ring0_demo_4_tubes()
{
	pts = randPts(6,r=2);
	MarkPts(pts, ["r",.12, "colors",["brown"]]);

    c = randc();
 
	for (i=[0:len(pts)-2])
//	assign(  L= norm(pts[i+1]-pts[i])
//		  )
	{
     L= norm(pts[i+1]-pts[i]);   
	 Ring0( [ "normal", pts[i+1]-pts[i]
		   , "target", midPt([pts[i],pts[i+1]]) 
		   , "color", "khaki" //c //randc()
		   , "h", L
		   , "transp", 1//0.7
		   , "ro", .18, "ri", .16	
		   ] );
	}
	
}

module Ring0_test( ops ){ doctest(Ring0,ops=ops);}

module Ring0_demo()
{
	//Ring0_demo_1();
	//Ring0_demo_2();
	//Ring0_demo_3_tube();
	Ring0_demo_4_tubes();
}	

//Ring0_demo();
//doc(Ring0);





//========================================================
Rod=["Rod", "pqr,ops", "n/a", "Geometry,Line",
 " Given a 2- or 3-pointer (pqr), draw a line (cylinder) from point P
;; to Q using polyhedron. The 3rd point, R, is to make a plane (of pqr), 
;; from that the angle of drawn points start in a anti-clock-wise manner 
;; (when looking from P-to-Q direction). So R is to determine where the
;; points start. If it is not important, use 2-pointer for pqr and R will
;; be randomly set. 
;;
;; Imagine pqr as a new coordinate system where P is on the x axis, Q is 
;; the ORIGIN and R is on xy plane: 
;; 
;;       N (z)
;;       |
;;       |________
;;      /|'-_     |
;;     / |   '-_  |
;;    |  |      : |
;;    |  Q------|---- M (y)
;;    | / '-._/ |
;;    |/_____/'.|
;;    /          '-R
;;   /
;;  P (x)
;;
;; In the graph below, P,Q,R and 0,4 are co-planar. 
;;
;;       _7-_
;;    _-' |  '-_
;; 6 :_   |Q.   '-4
;;   | '-_|  '_-'| 
;;   |    '-5' '-|
;;   |    | |    '-_
;;   |  p_-_|    |  'R
;;   |_-' 3 |'-_ | 
;;  2-_     |   '-0
;;     '-_  | _-'
;;        '-.'
;;          1
;;
;; headangle/tailangle are 90, but customizable.
;;
;;                         R
;;                        :
;; |       .--------.    :  |
;; |     .'          '. :   |  
;; |   P'--------------Q.   | 
;; | .' pa            qa '. |
;; |------------------------|
"];

//========================================================
 
//module Rod_b4155t( pqr=randPts(3), ops=[]) //r=0.6, count=6, showindex=true)
//{
//	// A Rod making use of rodPts
//
//	//echom("Rod");
//
//	//------------------------------------------
//	ops = concat
//	( ops,
//	, [ 
//	  //------ To be sent to rodPts()
//		"r", 0.05
//	  , "nside",     6
//	  , "label", "" 
//	  , "p_pts",undef  // pts on the P end
//	  , "q_pts",undef  // pts on the q end
//	  , "p_angle",90   // cutting angle on p end
//	  , "q_angle",90   // cutting angle on q end
//	  //, "twist_angle", 0 // see twistangle()
//	  , "rotate",0  	  // rotate about the PQ line, away from xz plane
//	  , "cutAngleAfterRotate",true // to determine if cutting first, or
//								  // rotate first. 
//	  //------ The above are to sent to rodPts()
//	
//	  , "has_p_faces", true
//	  , "has_q_faces", true
//	  , "addfaces",[]
//	  , "addpts",[]
//
//	  , "echodata",false
//	  , "markrange",false // Set to true or some range to mark points
//						 //if true, full range
//	  , "markOps",  []    // [markpt setting, label setting] 
//	  , "labelrange",false
//	  , "labelOps", []
//	  ]
//	, OPS
//	);
//	function ops(k)=hash(ops,k);
//	nside = ops("nside");
//
//	rodpts = rodPts( pqr, ops);
//    echo(rodpts = rodpts);
//	p_pts = rodpts[0];
//	q_pts = rodpts[1];
//
//	// Check if angles are correct:
//	//	echo( "angle p:", angle( [Q(pqr),P(pqr), p_pts[0]] ));
//	//	echo( "angle q:", angle( [P(pqr),Q(pqr), q_pts[0]] ));
//
//	allpts = concat( p_pts, q_pts, ops("addpts") );
//	//echo("Rod.allpts: ", allpts);
//
//	_label = ops("label");
//	label= _label==true?"PQ":_label;
//
//	//if(label) LabelPts( p01(pqr), label ) ;
//
////	p_faces = reverse( range(nside) );
////	// faces need to be clock-wise (when viewing at the object) to avoid 
////	// the error surfaces when viewing "Thrown together". See
////	// http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
////	q_faces = range(nside, nside*2); 
////	faces = concat( ops("has_p_faces")?[p_faces]:[]
////				 , ops("has_q_faces")?[q_faces]:[]
////				 , rodsidefaces(nside)
////				 , ops("addfaces")
////				 );
////	faces = rodfaces(nside);
//	faces = faces("rod",nside);
//	
//	echo("Rod.faces:", faces);
//
//	//=========== marker and label =============
//	allrange= range(allpts);
//	markrange = hash(ops, "markrange" , if_v=true, then_v=allrange);
//	labelrange= hash(ops, "labelrange", if_v=true, then_v=allrange);
//	markops = or(hash(ops, "markops"),[]); 
//	labelops= or(hash(ops, "labelops"),["labels",""]); 
//	pts_to_mark = [ for(i=markrange)	allpts[i] ];
//	pts_to_label= [ for(i=labelrange)	allpts[i] ];
//
//	// We make them BEFORE the polyhedron so they are always visible
//	if(markrange){ MarkPts( pts_to_mark, markops); }
//	if(labelrange){ LabelPts( pts_to_label, hash(labelops, "labels"), labelops); }
//	// =========================================
//
//	color( ops("color"), ops("transp") )
//	polyhedron( points = allpts 
//			  , faces = faces
//			 );
//
//	//==========================================
//	if( ops("echodata"))
//	{
//		echo(
//			 "<b>Rod()</b>:<br/><b>ops</b> = ", ops
//			,join([""
//			,str("<b>q_pts</b> = ", q_pts )
//			,str("<b>p_pts</b> = ", p_pts )
//			,str("<b>faces</b> = ", faces )
//			],";<br/>"));
//	}
//}
////========================================================

module _Rod_dev( pqr=randPts(3), ops=[]) //r=0.6, count=6, showindex=true)
{
	// This is the original version before rodPts() was built. After we 
	// have rodPts, the rod point calc was taken out so the current Rod()  
	// simply makes use of rodPts();

	//echom("Rod");

	//------------------------------------------
	ops = concat
	( ops,
	, [ "r", 0.05
	  , "nside",     6
	  , "label", "" 
	  , "p_pts",undef  // pts on the P end
	  , "q_pts",undef  // pts on the q end
	  , "p_angle",90   // cutting angle on p end
	  , "q_angle",90   // cutting angle on q end
	  , "rotate",0  // rotate about the PQ line, away from xz plane
	  , "cutAngleAfterRotate",true // to determine if cutting first, or
						  // rotate first. 
	  , "echodata",false

	  , "markrange",false // Set to true or some range to mark points
						 //if true, full range
	  , "markOps",  []    // [markpt setting, label setting] 
	  , "labelrange",false
	  , "labelOps", []
	  ]
	, OPS
	);

	function ops(k) = hash( ops, k);

	//echo("Rod.ops:",ops);
	//echo("Rod, p-, q-angle: ", [p_angle, q_angle] );

	r = ops("r");
	rot = ops("rotate");
	nside = ops("nside");
	p_angle = ops("p_angle");
	q_angle = ops("q_angle");
	cutAngleAfterRotate= ops("cutAngleAfterRotate");

	//echo("nside", nside);
	//------------------------------------------
	// pqr0: original pqr before rotate
	//
	pqr0= pqr==undef
		 ? randPts(3)
		 : len(pqr)==2
		   ? concat( pqr, [randPt()] )
		   : pqr ;

	//pq = p01(pqr0);
	P = pqr0[0];
	Q = pqr0[1];
	R1= pqr0[2];
	pq= [P,Q];
	N = N(pqr0);
	M = N2(pqr0);

	// Before rotation: coplanar: P Q A B 0 4 R1
	//
	//                          _-5
	//                       _-'.' '.
	//                    _-' .'     '.
	//                 _-'  .'         '.
	//              _-'   _6    _-Q----_-4--------------A 
	//            1'   _-'  '_-'   '_-'.'            _-'
	//          .' '.-'   _-' '. _-' .'           _-'
	//        .' _-' '. -'    _-'. .' '.       _-'
	//      .'_-'   _-''.  _-'   _-7    R1  _-'
	//	  2-'     P-----0-'----------------B     
	//      '.         .'  _-'
	//        '.     .' _-'
	//          '. .'_-'
	//            3-'
	//                     
	// After rotation by 45 degree: coplanar: P Q 4 0 R2 (R2 not shown)  
	//                 
	//	      5-------4
	//	     /|      /|
	//	    / |  Q'-/----------A
	//	   /  |  /\/  |       /
	//	  /   6-/-/---7      /
	//   /   / / /  \/      /
	//	1-------0   /\  	  /
	//	|  / /  |  /  R1  /
	//	| / P.--|-/------B
	//	|/    '.|/          
	//  2-------3          
	//           
	// The pl_PQ40 rotates (swings like a door) to make 0-4 move away 
	// from pl_PQAB. So [P,Q,R2] will be the new pqr to work with.

	// So, if rotation, we make a R2 that is on the pl_PQ40 to make a new 
	// pqr: [P,Q,R2] 

	// We use the othoPt T as the rotate center to carry the rotation:
	//
	//	       N (z)
	//	       |
	//	       |
	//	      /|
	//	     / |       R2  
	//	    |  |     .' 
	//	    |  Q---_------- M (y)
	//	    | / '-:_
	//	    |/_-'   '._
	//	  T /----------'-R1
	//	   /
	//	  P (x)
	//   /
	//  /
	// P2

	// Calc R2 = post-rotation R using T

	T = othoPt( [ P,R1,Q ] );

	// Since we need to get R2 by rotating an angle on the PTR1 plane, 
	// of which its normal is decided by the order of P,T, R1, the location 
	// of T is critical that it could get the rotation to the wrong side.
	// 
	// (1)
	//      R1_             PQ > TQ
	//      |  '-._
	//	p---T------'Q
	//
	// (2)           R1     PQ > TQ     
	//              /|
	//     P-------Q T
	//   
	// (3)   R1_            PQ < TQ
	//       |  '-._
	//       |       '-._
	//       T  p-------'Q
	// (4)
	//              R1      PQ < TQ
	//            .'|
	//          .'  |
	//        .'    |
	//   P---Q      T
	//   
	// Shown above, case (3) will cause PTR1 to go to wrong direction. 
	// So we make a new p1_for_rot as P to ensure that it is always on 
	// the far left:

	p1_for_rot= angle(pqr0)>=90
				? P
				: onlinePt([norm(T-Q)>=norm(P-Q)? T:P,Q], len=-1 );

	R2 = anyAnglePt( [ p1_for_rot, T, R1]     
                   , a= rot 
                   , pl="yz"
                   , len = dist([T,R1]) 
				  );

	P2 = onlinePt( [P,Q], len=-1);  // Extended P: P2----P----Q

	norot_pqr_for_p = [P2,P, R1];
	norot_pqr_for_q = [P, Q, R1];
	rot_pqr_for_p   = [P2,P, R2];
	rot_pqr_for_q   = [P, Q, R2];

	pqr_for_p = cutAngleAfterRotate&&rot>0
				? rot_pqr_for_p
				: norot_pqr_for_p;
	pqr_for_q = cutAngleAfterRotate&&rot>0
				? rot_pqr_for_q
				: norot_pqr_for_q;		

	// p_pts90 = [p0,p1,p2,p3]
	// q_pts90 = [p5,p6,p7,p8]
	// 
	// If p_angle=0, and p_pts not given, p_pts90 will
	// be the final points on p end for polyhedron 

	// p_pts90 and q_pts90 decides the points of side lines. 
	// It doesn't matter if cutAngleAfterRotate is true, 'cos 
	// the position of side lines is independent of cut-angle.
	// So we use rot_pqr_for_p. It will be = rot_pqr_for_p
	// If rotate=0 (then R2=R1).
	// 
	// Later we will project these lines to p_plane and q_plane,
	// Those planes are what decides the final sideline pts. 
	p_pts90= arcPts( rot_pqr_for_p
				  , r=r, a=360, pl="yz", count=nside );

	q_pts90= arcPts( rot_pqr_for_q
				   , r=r, a=360, pl="yz", count=nside );

	//Line( [P, p_pts90[0] ],["r",0.05] );
	//MarkPts( [P2],["colors",["purple"]] );
	//MarkPts( p_pts90 );
	//Chain( p_pts90 );

	//-------------------------------------------------
	// Cut an angle from P end. 
	//
	// If cutAngleAfterRotate, we want to use the pqr 
	// BEFORE rotate.
	Ap = anyAnglePt( cutAngleAfterRotate
					? [P2,P,R1]
					: [P2,P,R2]
					//rot_pqr_for_p 
					// cutAngleAfterRotate
					//? norot_pqr_for_p
					//: rot_pqr_for_p 
				  , a=p_angle+(90-p_angle)*2
				  , pl="xy"); 
	Np = N([ Ap, P, Q ]);  // Normal point at P
	//echo("Nh= ", Nh);
	//MarkPts( [Ah], ["labels",["Ah"]] );
	p_plane = [Ap, Np, P ];  // The head surface 
	//Plane( headplane, ["mode",4] );
	p_pts=or( ops("p_pts")
			  , p_angle==0
			   ? p_pts90
			   : 	[ for(i=range(nside)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], p_plane ) ]
			  );

	//-------------------------------------------------
	// Cut an angle from Q end. 
	Aq = anyAnglePt( cutAngleAfterRotate
					? [P,Q,R1]
					: [P,Q,R2]
					//rot_pqr_for_q
					// cutAngleAfterRotate
					//? rot_pqr_for_q
					//: norot_pqr_for_q 
				  , a=q_angle //90-q_angle
				  , pl="xy"); // Angle point for tail
	Nq = N( cutAngleAfterRotate
					? norot_pqr_for_q
					: rot_pqr_for_q );  // Normal point at Q
	//echo("Nt= ", Nt);
	//MarkPts( [At], ["labels",["At"]] );

	// q_plane is the surface of the final pts landed. Final pts
	// are determined by extending the side lines to meet the 
	// q_plane where the intersections are those final pts are. 
	//
	// These side lines will be the same no matter it's rotate-cut_angle
	// or cut_angle-then-rotate. 
	//
	// The q_plane, however, will be different (cut_angle before or
	// after rotate). 

	q_plane = [Aq, Nq, Q ];//:pqr0;  // The head surface 
	//Plane( headplane, ["mode",4] );
	q_pts=or( ops("q_pts")
			  , q_angle==0
			    ? q_pts90
			    : [ for(i=range(nside)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], q_plane ) ]
			  );


//	echo("rot = ", rot);
//	echo("angle( [ p_pts[0], P, Q] )= ", angle( [ p_pts[0], P, Q] ));
//	echo("angle( [ q_pts[0], Q, P] )= ", angle( [ q_pts[0], Q, P] ));

//	if( tailangle==90 ){ tailpts = tailpts90
//	}

	allpts = concat( p_pts, q_pts );

	_label = ops("label");
	label= _label==true?"PQ":_label;

	//if(ops("showsideindex")) LabelPts( allpts, range(allpts) ) ;
	if(label) LabelPts( pq, label ) ;

	p_faces =  range(nside); 
	q_faces = range(nside, nside*2); 
	faces = concat( [p_faces], [q_faces], rodsidefaces(nside) );
	//echo("faces: ", faces);


	//=========== marker and label =============
	allrange= range(allpts);
	markrange = hash(ops, "markrange" , if_v=true, then_v=allrange);
	labelrange= hash(ops, "labelrange", if_v=true, then_v=allrange);
	markops = or(hash(ops, "markops"),[]); 
	labelops= or(hash(ops, "labelops"),["labels",""]); 
	pts_to_mark = [ for(i=markrange)	allpts[i] ];
	pts_to_label= [ for(i=labelrange)	allpts[i] ];

	// We make them BEFORE the polyhedron so they are always visible
	if(markrange){ MarkPts( pts_to_mark, markops); }
	if(labelrange){ LabelPts( pts_to_label, hash(labelops, "labels"), labelops); }
	// =========================================

	color( ops("color"), ops("transp") )
	polyhedron( points = allpts 
			  , faces = faces
			 );

	// We make marker AFTER the polyhedron so they are hidden behind
	// the object
	
//	echo_( "markrange={_}, markops={_}",[markrange,markops]);
//	echo_( "labelrange={_}, labelops={_}",[labelrange,labelops]);

	//
	// To check if the p-, q-angle work:
	//
//	echo_("Rod: cutAngleAfterRotate:{_}, p_angle={_}, angle( [Q, P, allpts[0]] )={_} "
//		, [cutAngleAfterRotate, p_angle, angle( [Q, P, allpts[0]] )] 
//		);
//	echo_("Rod: cutAngleAfterRotate:{_}, q_angle={_}, angle( [P, Q, allpts[{_}]] )={_} "
//		, [cutAngleAfterRotate, q_angle, nside, angle( [P,Q, allpts[nside]] ) ]
//		);
  


	//==========================================
	if( ops("echodata"))
	{
		echo(
			 "<b>Rod()</b>:<br/><b>ops</b> = ", ops
			,join([""
			,str("<b>q_pts</b> = ", q_pts )
			,str("<b>p_pts</b> = ", p_pts )
			,str("<b>faces</b> = ", faces )
			],";<br/>"));
	}
}


//========================================================
module Obj(opt=[]){
    
    df_opt=[ 
             "bone",[]
           , "seed", []
           , "xpts", []
    
           //, "markpts", false
           , "plane", false
           , "labels", false
           , "frame",false
           ];
    
    
}  

//========================================================

module Rod( pqr=pqr(), opt=[] ){
    
    echom("Rod");
    
   // MarkPts( pqr, ["chain",true,"label",false,"plane",true]);
    //["text",["Po","Qo","Ro"]]] );
    
    df_opt= 
    [
      "nside",6
    , "transp",1 
    
    , "dim", false
    , "bone", false
    , "markxsec", false
    , "frame", false
    , "radlines", false
    , "hide", false 
    , "echopts", false
    ];
    
    //echo(pqr=pqr);
    _opt= isstr(opt)?sopt(opt):opt;
    opt = update( df_opt, _opt );
    function opt(k,nf)= hash(opt,k,nf);
    //echo(opt=opt);
    
    //----------------------------------------------
    df_markxsec= ["r",0.06,"chain"
                  ,["color","black","r",0.02, "closed",true]
                  ] ;
    markxsecopt= subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markxsec"
                     , df_subopt= df_markxsec
                     , com_keys=[]
                     );
    //----------------------------------------------
    
    nside= or(len( opt("xsecpts")),opt("nside"));
    pqr = len(pqr)==2?app(pqr, randPt()):pqr;
    //echo(pqr=pqr);

    ptsPnQ = rodPts( pqr, opt );
    allpts = joinarr( sel(ptsPnQ,[0,1]) );
    
    if(opt("echopts")) {
        echo(pqr = pqr );
        echo( str( "ptsPnQ=<br/>",
        arrprn( ptsPnQ, dep=2)));
    }    
    //----------------------------------------------
    df_markpts=  ["r",0.06,"chain",["r",0.02,"closed",true]
                 ,"label",true] ;
        
    if( opt("markpts") ) {
        
        markptsopt= subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "markpts"
                     , df_subopt= df_markpts
                     , com_keys=[]
                     );
        
        //Label has to be treated as a whole for both p-face
        // and q-face. chain, however, has to be treated 
        // seperatedly. 
        if( hash(markptsopt,"label"))
            MarkPts( allpts, update( markptsopt, ["chain",false]));
        
        nolabel = update( markptsopt, ["label",false]);
        MarkPts( ptsPnQ[0], nolabel);                 
        MarkPts( ptsPnQ[1], nolabel);                 
    }
    //----------------------------------------------
    if( opt("frame") ) {        

        frameopt= subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "frame"
                     , df_subopt= [ "markpts",false, "label",false
                                  , "chain",[ "r",0.01,"color","green"
                                            , "closed",true] 
                                  ]
                     , com_keys=[]
                     );
                   
        for(i=range(nside)){
            Line0( [allpts[i], allpts[i+nside]]
                  , opt= hash(frameopt,"chain") );
        }
        
        mp_opt= update( df_markpts, frameopt );
        mp_label = hash( frameopt, "label" );
        MarkPts( ptsPnQ[0], mp_opt );                 
        MarkPts( ptsPnQ[1]
           , update( mp_opt
                   , mp_label==true?
                        ["label",["text", range(nside,2*nside)]
                     ]:mp_label?mp_label:[]
                   )
               );
    }
    //----------------------------------------------
    radlinesopt= subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "radlines"
                     , df_subopt= ["color","green","r",0.01]
                     , com_keys=[]
                     );
    if(radlinesopt){
        for(i=range(nside)){
              Line0( [pqr[0], allpts[i]], opt= radlinesopt );   
              Line0( [pqr[1], allpts[i+nside]], opt= radlinesopt );   
        }
    }    
    //----------------------------------------------
    boneopt = subopt1( df_opt= df_opt
                     , u_opt = opt
                     , subname = "bone"
                     , df_subopt=["color","black","r",0.06
                                 ,"chain",["color","black","r",0.02]
                                 ,"label","PQ"
                                 ]
            
                     , com_keys=[]
                     );
    if( boneopt ) MarkPts( p01(pqr),boneopt); 
    //----------------------------------------------
    
    
//    MarkPts( sel(ptsPnQ,[0,1,2])
//           , ["chain",true,"label",["text","PQR","shift",1,"color","red"]] );
    
//    MarkPts( [ ptsPnQ[2],P(pqr)], ["chain",true
//              , "label",["text",["Mp",""]]] );
//    MarkPts( [ ptsPnQ[3],Q(pqr)], ["chain",true
//              , "label",["text",["Mq",""]]] );
    
    //echo(nside=nside);
    
    //echo( ptsPnQ = arrprn(ptsPnQ, dep=2) );
//    MarkPts( ptsPnQ[0], ["chain",true,"label",true] );
//    MarkPts( ptsPnQ[1], ["chain",true,"label",true] );
    
    //MarkPts( ptsPnQ[1], ["chain",["color","red"],"color","red"] );
    //MarkPts( addz(opt("xsecpts"),0), "cl");
    
	if( !opt("hide")){
        faces = faces("rod",nside );
        color( opt("color"), opt("transp") )
        polyhedron( points = allpts 
                  , faces = faces
                 );
    }//if
}//Rod 

//Rod( randPts(3, d=[1,6]),opt=["rotate",30, "xsecpts",
//[[ 2,0],[2,1],[1,1],[0.5,2],[0,2],[0,-2],[1,-2],[1,0]] ]
//);

//pts= [[-1.01092, 2.59624, -0.851656], [2.5989, -2.61717, -0.630292], [-1.81425, -1.01677, -1.92352], [-8.23054, 13.0231, -1.29439], [-1.01092, 2.59624, -0.851656]];
//MarkPts( pts, ["chain",true,"label",["text",["Po","Qo","Ro", "Px","P"]]]);

//pqr= [[3.68999, 3.18951, 2.36557], [1.56907, 1.60905, 1.90884], [1.23829, 1.73406, 2.36217]];
//echo( rodPts(pqr ) );
//MarkPts( pqr, "c,p,l");
//Mark90( pqr );
//echo( angle( pqr ) );
//
//echo( rotpqr( pqr, 0) ) ;

module Rod_demo_0()
{ _mdoc("Rod_demo_0"
,
 " More demos showing varying nside. Default: [''nside'',6]. "
);

	Rod();

    pqr = randPts(3, d=[1,4]);
	Rod(pqr, ["r",1 ] ); 
    MarkPts( pqr, opt="p,c||label=PQR");
////
	Rod( trslN(pqr,2), opt=["color","red", "r",0.5,"nside",4, "len",2] );
////
	Rod( trslN(pqr,4), opt=["color","green", "r",0.4,"nside",20, "len",3] );
}
//Rod_demo_0();

module Rod_demo_1_bone_frame_radlines()
{ 
    echom("Rod_demo_1_bone_frame_radlines");
    
    nside=6; r= 0.8;
    
    
	pqr = pqr();
	MarkPts( pqr, "p,c"); 
    Rod( pqr, ["r",r, "nside",nside] );
              //,"frame", true] );
    
    pqr1 = trslN(pqr,2.5);
	//MarkPts( pqr1, "p,c"); 
    Rod( pqr1, _s("bone||r={_}|nside={_}|color=red|transp=0.4",[r,nside]));
    LabelPt( onlinePt( p01(pqr1), len=-0.8), "bone", ["color","red"]);

    pqr1_2 = trslN2(pqr1,2.5);
	//MarkPts( pqr1, "p,c"); 
    Rod( pqr1_2
       , ["r",r, "nside",nside, "color","red","transp",0.4
         , "bone", ["chain",["n",4,"color","blue","r",0.1]]
         ]
       );  
    //LabelPt( onlinePt( p01(pqr1_2), len=-0.5), "bone", ["color","red"]);
    
    pqr2 = trslN(pqr1,2.5);
	//MarkPts( pqr, "pc"); 
    Rod( pqr2, ["r",r, "nside", nside, "transp", 0.4, "hide",true
               , "markpts",true]);
    LabelPt( onlinePt( p01(pqr2), len=-0.8), "markpts", ["color","blue"]);
        
    
    pqr3 = trslN(pqr2,2.5);
	//MarkPts( pqr, "pc"); 
    Rod( pqr3, _s("frame||r={_}|nside={_}|color=blue|transp=0.4|hide=true",[r,nside]));
    LabelPt( onlinePt( p01(pqr3), len=-0.8), "frame", ["color","red"]);

    
    pqr4 = trslN(pqr3,2.5);
	//MarkPts( pqr, "pc"); 
    Rod( pqr4, , ["r",r, "nside", nside, "transp", 0.4, "showobj",false
               ,  "radlines",true,"frame",true]);
    LabelPt( onlinePt( p01(pqr4), len=-0.8), "frame,radlines", ["color","red"]);


}
//Rod_demo_1_bone_frame_radlines();

//echo( pqr(p=1,r=2,a=90));

module Rod_demo_2_markpts()
{
    echom("Rod_demo_2_markpts");
    space = 2;
    
    pqr = pqr(p=2);
    
    Rod( pqr, opt = "markpts||transp=0.5||len=2" );
    //MarkPts( pqr, "c,p||label=PQR");
    
    pqr1= trslN( pqr, space);
    Rod( pqr1, opt = ["len=3","markpts"
                     , ["label",["tmpl","P{*`a}"
                                ,"text","ABCDEFGHILMN" //range(pqr1)
                                ]]] );
    
    pqr2= trslN( pqr, 2*space);
    Rod( pqr2, opt = ["markpts" 
                     ,  [  "hide",  true
                        ,  "label",[ "isxyz",true
                                   , "scale", 0.01
                                   , "color","black"
                                   , "tmpl","{*`a|d2}"
                                   ]
                        , "chain",["r",0.05, "closed",true]
                        ]
                        
                     ] );

//    pqr2= trslN( pqr, 2*space);
//    Rod( pqr2, opt = ["markpts" 
//                     ,  [  "hide",  true
//                        ,  "label",[ "isxyz",true
//                                   , "scale", 0.01
//                                   , "color","black"
//                                   , "tmpl","{*`a|d2}"
//                                   ]
//                        , "chain",["r",0.05, "closed",true]
//                        ]
//                        
//                     ] );
    
//	Rod(opt=["transp",0.5, "r",0.3,"markpts",true]);
//	Rod(opt=["transp",0.5, "r",0.3,"label","AB"]);
//	
//	Rod(opt=["color","red","transp",0.3, "r",0.6
//			, "labelrange", true
//			]);
//
//[{_`a|d2}]
//	Rod(opt=["color","green","transp",0.3, "r",0.3
//			, "markrange",true]);
//
//	Rod(opt=["color","blue","transp",0.3, "r",0.6
//			, "markrange",[0,2,4,6,8,10]]);
//
//	Rod(opt=["color","brown","transp",0.3, "r",0.6
//			, "labelrange",[0,6]
//			, "labelopt",["labels",["1st","2nd"]]
//			, "markrange",[0,6]
//			]
//		);
//
//	Rod(opt=["color","gray","transp",0.3, "r",0.6
//			, "labelrange",true
//			, "labelopt",["color","red","scale",0.02]
//			, "markrange",[0,6]
//			]
//		);

}
//Rod_demo_2_markpts();



module Rod_demo_3_angles()
{
_mdoc("Rod_demo_3_angles",
" p_angle, q_angle
;;
;; yellow one: 60, 45
;; red one: 30, 60
;;
;; default (when not given) = 90
");

	pqr = randPts(3);

	Chain(pqr, ["r",0.05]);

	MarkPts( pqr, ["labels", ["P(60)","Q(45)","R"]] );

	Rod(pqr, opt=["transp",0.5, "r",1, "showsideindex",true
			, "p_angle",60
			, "q_angle",45
			, "echodata", true
			]);


	pqr2 = randPts(3);

	Chain(pqr2, ["r",0.05]);

	MarkPts( pqr2, ["labels", ["P(30)","Q(60)","R"]] );

	Rod(pqr2, opt=["transp",0.5, "r",1, "showsideindex",true
			, "p_angle",30
			, "q_angle",60
			, "echodata", true
			, "color","red"
			]);
}
//Rod_demo_3_angles();



module Rod_demo_4_angles()
{
_mdoc("Rod_demo_4_angles",
" Show how can the p_angle, q_angle be applied to join rods. 
;;
;; Note that this is just for demo. The joints in this demo
;; actually have two exact same faces overlapping. 
");

	pqr = randPts(3);
	rqp = p210(pqr);

	Chain(pqr);

	M = angleBisectPt( pqr );
	angle = angle(pqr)/2;
	MarkPts(pqr, ["labels","PQR"] );
	Rod(pqr, opt=["transp",0.5, "r",0.4
				//,"label",true, "showsideindex",true
				, "q_angle", angle //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp, opt=["transp",0.5, "r",0.4
				//,"label",true, "showsideindex",true
				, "q_angle", angle // angle( [ P(pqr), R(pqr), M] )
				]);
	
	//==========================
	pqr2 = randPts(3);
	rqp2 = p210(pqr2);

	//Chain(pqr);

	M2 = angleBisectPt( pqr2 );
	angle2 = angle(pqr2)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("red",0.7){
	Rod(pqr2, opt=["transp",0.2, "r",0.3, "nside",3
				//,"label",true, "showsideindex",true
				, "q_angle", angle2 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp2, opt=["transp",0.2, "r",0.3, "nside",3
				//,"label",true, "showsideindex",true
				, "q_angle", angle2 // angle( [ P(pqr), R(pqr), M] )
				]);
	}

	//==========================
	pqr3 = randPts(3);
	rqp3 = p210(pqr3);

	//Chain(pqr);

	M3 = angleBisectPt( pqr3 );
	angle3 = angle(pqr3)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("green", 0.7){
	Rod(pqr3, opt=["transp",0.2, "r",0.3, "nside",4
				//,"label",true, "showsideindex",true
				, "q_angle", angle3 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp3, opt=["transp",0.2, "r",0.3, "nside",4
				//,"label",true, "showsideindex",true
				, "q_angle", angle3 // angle( [ P(pqr), R(pqr), M] )
				]);
	}

	//==========================
	pqr4 = randPts(4);
	rqp4 = p210(pqr4);

	//Chain(pqr);

	M4 = angleBisectPt( pqr4 );
	angle4 = angle(pqr4)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("blue", 0.7){
	Rod(pqr4, opt=["transp",0.2, "r",0.3, "nside",18
				//,"label",true, "showsideindex",true
				, "q_angle", angle4 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp4, opt=["transp",0.2, "r",0.3, "nside",18
				//,"label",true, "showsideindex",true
				, "q_angle", angle4 // angle( [ P(pqr), R(pqr), M] )
				]);
	}
}
//Rod_demo_4_angles();



module Rod_demo_5_user_q_pts()
{
_mdoc("Rod_demo_5_user_q_pts",
" This demo shows how to use customized q_pts
;; with expanded q_pts.
;;
;; The pts when angle=90
;;
;;   q_pts90 = arcPts( pqr, r=r, a=360, pl=''yz'', count=nside ); 
;;
;; Expand them to twice the r
;;
;;   ex_q_pts90= expandPts( q_pts90, dist=2*r ); 
;;
;;   Rod( pqr, [''q_pts'', ex_q_pts90, ...] ); 
");

	pqr = randPts(3);
	
	//P2 = onlinePt( p01(pqr), len=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);

	nside = 6;
	r = 0.5;
	
	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=nside );

	// Expand them to twice the r
	ex_q_pts90= expandPts( q_pts90, dist=2*r );

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",r, "q_pts", ex_q_pts90
			 , "nside",nside
			 , "showsideindex",true,"transp",0.4] );
}
//Rod_demo_5_user_q_pts();



module Rod_demo_6_user_q_pts()
{
_mdoc("Rod_demo_6_user_q_pts",
" Another demo shows how to use customized q_pts.
;;
;;   nside=24; 
;;   spark_ratio=3; 
;;
;; The pts when angle=90
;;
;;   q_pts90 = arcPts( pqr, r=r, a=360, pl=''yz'', count=nside ); 
;;
;; Construct the points:
;;
;;   pts= [for(i=range(nside))
;;          mod(i,3)==0? onlinePt( [Q, q_pts90[i]], ratio=spark_ratio)
;;                     : q_pts90[i]
;;        ];
;;
;;   Rod( pqr, [''q_pts'', pts, ...] ); 
");

	// This demo shows how to use customized headpts

	pqr = randPts(3);
	
	//P2 = onlinePt( p01(pqr), len=-1);


	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);

	nside = 24;
	r = 0.5;
	spark_ratio = 3;

	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=nside );

	pts= [for(i=range(nside))
			mod(i,3)==0? onlinePt( [Q, q_pts90[i]], ratio=spark_ratio)
						: q_pts90[i]
		];

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",r, "q_pts", pts
			 , "nside",nside
			 , "showsideindex",false
			,"transp",1] );
}
//Rod_demo_6_user_q_pts();



module Rod_demo_7_gear()
{
_mdoc("Rod_demo_7_gear",
 " This demo shows how to use Rod() to make a simple gear-like
;; structure using customized p_pts/q_pts
;;
;; Assuming each tooth has 6 pts (defined as pt_per_tooth):
;;      __
;;     /  ^      23 
;;     |  |    1    4
;;  ---+  +    0    5
;;
;; We extend the point certain distance outward according to 
;; mod(i, pt_per_tooth). 
");     
	

	_pqr = randPts(3);
	
	pqr = newx( _pqr, onlinePt( p10(_pqr), len=0.5) );
	//P2 = onlinePt( p01(pqr), dist=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);
	P2=onlinePt( [Q,P], len=1);

	teeth = 18;
	pt_per_tooth = 6;
	nside = teeth* pt_per_tooth;
	r = 1; //0.5;
	tooth_height=0.2;

	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=nside );
	
	//tailpts90 = arcPts( [P2,P,R], r=r, a=360, pl="yz", count=nside );

	gear_pts_q= [for(i=range(nside))
			 mod(i,pt_per_tooth)==1||mod(i,pt_per_tooth)==4
			  ? onlinePt( [q_pts90[i],Q], len=-tooth_height*1/2)
			  : mod(i,pt_per_tooth)==2||mod(i,pt_per_tooth)==3
			    ? onlinePt( [q_pts90[i],Q], len=-tooth_height)
			//:mod(i,pt_per_tooth)==0|| mod(i,pt_per_tooth)==5
				: q_pts90[i]
		];

	dpq = P-Q;
	gear_pts_p = [for(p=gear_pts_q) p+dpq];

	//=========================

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",5, "len",0.5
			, "q_pts", gear_pts_q
			, "p_pts", gear_pts_p
			 , "nside",nside
			 , "showsideindex",false
			,"transp",0.8] );
}
//Rod_demo_7_gear();



module Rod_demo_8_rotate()
{
nside=6;
_mdoc("Rod_demo_8_rotate",
_s(" red: rotate {_} degree
;; green: rotate {_} degree
;; 
;; Note how the rotation goes (counter clock-wise)
", [360/nside/2, 360/nside])
);
	
	pqr = randPts(3);
	pqr=[[-2.73324, -2.05788, -2.44777], [-0.285864, -1.8622, -1.77504], [2.84959, 2.00984, -2.64569]];
	P=P(pqr); Q=Q(pqr); R=R(pqr);

	MarkPts( pqr, ["labels","PQR"] );
	Plane( [ onlinePt( [Q,P], ratio=2)
		   , Q(pqr)
		   ,onlinePt( [Q,R], ratio=2)
		   ] ,  , ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",1, "nside",nside, "markrange",[0],"labelrange",[0],"transp",0.9] );
//
	// 2014.8/21: 
	// Found that sometimes the rotation goes to the opposite direction
	// 
	//	It seems that it occurs in (2) case:
	//
	//	(1)
	//    
	//        R._
	//           '-._
	//	p---T------'Q
	//
	//
	//	(2)    
	//        R._
	//           '-._
	//               '-._
	//	    T  p-------'Q
	//
	// Fixed in 20140821-3

	//echo("pqr: ", pqr);
	T = othoPt( [P,R,Q] );
	echo( "Is TQ > PQ ? ", norm(T-Q)> norm(P-Q) );


	P=P(pqr); Q=Q(pqr); R=R(pqr);

	Rod( [ onlinePt( [P,Q], len=0.3)
		 , onlinePt( [Q,P], len=0.3)
		 , R ]
		, ["r",2, "nside",nside,"markrange",[0],"labelrange",[0],"color","red", "transp",0.6
			  , "rotate", 360/nside/2] );

	Rod( [ onlinePt( [P,Q], len=0.5)
		 , onlinePt( [Q,P], len=0.5)
		 , R ]
		, ["r",3, "nside",nside,"markrange",[0],"labelrange",[0],"color","green", "transp",0.3
			  , "rotate", 360/nside] );
}
//Rod_demo_8_rotate();




module Rod_demo_8_1_q_rotate() // give up
{
_mdoc("Rod_demo_8_1_q_rotate",
" We give up this appoach of making chain using Rod with both 
twistangle and p- q-angle specified. 'cos it's too difficult 
to get both of twistangle and p- q-angle right. 2014.8.29

");

	pqr = randPts(3);
	pqrs1 = concat( pqr, [randPt()] );

	shift = normalPt(pqr,len=1.5)-pqrs1[1];

	pqrs2= [for(p=pqrs1) p+shift];
	pqrs3 = [for(p=pqrs2) p+shift];

pqrs1= [[2.26761, 1.21599, 0.51161], [0.447378, -2.10168, 0.613161], [-0.582338, -1.77329, 1.33888], [-2.13944, 0.683927, -2.8544]];
pqrs2= [[3.02213, 0.840008, 1.75231], [1.20189, -2.47767, 1.85386], [0.172176, -2.14928, 2.57958], [-1.38492, 0.307941, -1.6137]];
pqrs3=  [[3.77664, 0.464021, 2.99301], [1.95641, -2.85365, 3.09457], [0.92669, -2.52527, 3.82029], [-0.630407, -0.0680459, -0.372994]];

	aPQR = angle( pqr ) /2;
	aQRS = angle( p123(pqrs1) )/2;
	taPQRS= twistangle( pqrs1 ); 

	echo("aPQR=", aPQR);
	echo("aQRS=", aQRS);
	echo("taPQRS=", taPQRS);
	 
	echo( "shift=", shift);
	echo( "pqrs1=", pqrs1);
	echo( "pqrs2=", pqrs2);
	echo( "pqrs3=", pqrs3);
	
	MarkPts( pqrs1, ["labels", "PQRS"] );

	opt= ["r",0.4, "transp",0.9];


	Chain( pqrs1 );
	Rod( p012(pqrs1) , concat(["q_angle",aPQR],opt) );
	Rod( p123(pqrs1) , concat(["p_angle",aPQR, "q_angle",aQRS
						, "rotate", -taPQRS  // Rod_QR is calc based on QRS, so 
												// need to make a twiste
						, "cutAngleAfterRotate",false // aPQR is calc based on PQR,                                                         
													 // that is BEFORE rotate
						//, "_rotate", taPQRS  // need to rotate q-end back to QRS 
							],opt) );
//	Rod( p123(pqrs1) , concat(["p_angle",aPQR, "q_angle",aQRS, "rotate", taPQRS, "cutAngleAfterRotate",true],opt) );
	Rod( p231(pqrs1) , concat(["p_angle",aPQR],opt) );

//====================================
	
//	opqr1 = randPts(4);
//	shift = normalPt(p012(opqr1),len=1.5)-opqr1[1];
//	//Normal(p012(opqr1),["color","red","len",3]);
//	opqr2 = [for(p=opqr1) p+shift];
//	opqr3 = [for(p=opqr2) p+shift];
//	//pts = [pqr, pqr2, pqr3];
//	
//	//ap = angle( opqr )/2 ;
//	aOPQ = angle( opqr1 )/2;
//	aPQR = angle( p123(opqr1) )/ 2 ;
//
//	taOPQ= twistangle( reverse(opqr1) );
//
//	echo("aPQR=", aPQR);
//	echo("aOPQ=", aOPQ);
//	echo("taOPQ=", taOPQ);
//	 
//	echo( "shift=", shift);
//	echo( "opqr1=", opqr1);
//	echo( "opqr2=", opqr2);
//	echo( "opqr3=", opqr3);
//	echo( "p123(opqr1) = ", p123(opqr1));
//	echo( "slice(opqr1,1)",slice(opqr1,1));
//
//	
//	MarkPts( opqr1, ["labels", "OPQR"] );
//
//	opt= ["r",0.4, "transp",0.9];
//
//	Chain( opqr1 );
//	Rod( p012(opqr1) , concat(["q_angle",aOPQ],opt) );
//	Rod( p123(opqr1) , concat(["p_angle",aOPQ, "q_angle",aPQR, "rotate", taOPQ, "cutAngleAfterRotate",true],opt) );
//	Rod( p231(opqr1) , concat(["p_angle",aPQR],opt) );
//	
//	Chain( opqr2, ["color","red"]);
//	Rod( p123(opqr1) , concat(["q_angle",aPQR],opt) );
//	Rod( p321(opqr1) , concat(["q_angle",aPQR],opt) );
////
////	Chain( opqr3, ["color","green"] );
////	Rod( p123(opqr3) , opt );
////	Rod( reverse(p123(opqr3)) , concat([ ], opt) );


}

//Rod_demo_8_1_q_rotate();



module Rod_demo_9_rotate_and_angles()
{
_mdoc("Rod_demo_9_rotate_and_angles"
," This demo shows cases when both rotate and p_angle/q_angle are set.
;; It is tricky 'cos the order makes difference. 
;;
;; First we make two copies of a same Rod side by side (yellow), then 
;; add a larger one to each. Make both p_angle and q_angle = 45. 
;; 
;; red: cutAngleAfterRotate= true // default :  rotate then cut
;; green: cutAngleAfterRotate= false // cut then rotate
;;
;; The blue dots on them show how the first point rotates by 30 degree.
");

	nside=6;

	pqr = randPts(3);
	pqr2 = [for(p=pqr) p+ normalPt(pqr,len=4)-Q(pqr)];
	
	for(pts=[pqr,pqr2])
	{
		Chain( pts ,["r",0.02, "color","blue"]);
		
		MarkPts( pts, ["labels","PQR"] );
		
		Plane(pts, ["mode",3]);
		
		Rod( pts, ["r",1.7, "nside",nside, "transp",1
				  ,"markrange",[0]
				  ,"labelrange",[0]
				] 
				);
	}	

	xpqr = concat( linePts( p01(pqr), dist=[0.1,0.1]), [R(pqr)]);
	xpqr2= [for(p=xpqr) p+ normalPt(xpqr,len=4)-Q(xpqr)];

	echo( "xpqr: ", xpqr );
	echo( "xpqr2:", xpqr2);

	opt=[ "r",2
		, "nside",nside
		, "labelrange",true
		, "transp",0.7, 
		, "markrange",[0]
		, "markopt",["colors",["blue"]]
		, "p_angle",45
		, "q_angle",45
		, "rotate", 360/nside/2
		];

	Rod( xpqr, concat( ["color","red"  , "cutAngleAfterRotate", true],opt ));
	Rod( xpqr2,concat( ["color","green", "cutAngleAfterRotate", false],opt) );


}
//Rod_demo_9_rotate_and_angles();


//==========================
module Rod_demo_10_join_rotate()
{
_mdoc( "Rod_demo_10_chain_rotate"
, " Show the role of cutAngleAfterRotate in making Rod joint when
;; rotation is needed. 
;;
;; dfcolor: no rotate 
;; red    : rotate. cutAngleAfterRotate= true (default)
;; green  : rotate, cutAngleAfterRotate= false
;;
");
	pqr = randPts(3);
	pqr2 = [for(p=pqr) p+ normalPt(pqr,len=1.5)-Q(pqr)];
	pqr3 = [for(p=pqr) p+ normalPt(pqr,len=3)-Q(pqr)];
	pts = [pqr, pqr2, pqr3];

	colors=[ undef, "red", "green" ];
	rotates=[ 0, 45, 45 ];
	caar  =[ undef, true, false ];

	for(i= range(3))
	{
		//Chain( pts[i] ); // ,["r",0.02, "color","blue"]);

		JRod( pts[i]
			, rotate = rotates[i]
			, opt = [ "cutAngleAfterRotate", caar[i]
					, "color",colors[i]
					, "transp", 0.8
					, "r", 0.5
					, "nside", 4
					, "label",true
					]
			);

	}	
	jr_opt= [ "transp", 0.7
			, "r", 0.5
			, "nside", 4
			, "label",true
			];

	module JRod(pqr=randPts(3), rotate=0, opt=[] )
	{
		Chainf(pqr, ["closed",false]);

		P=P(pqr); Q=Q(pqr); R=R(pqr); 
		echo("Rod_demo_10_chain_rotate.pqr=", pqr);
		angle = angle(pqr)/2; 
		//echo("angle= ", angle);
		rqp= [R,Q,P];
		Rod( pqr, concat(["rotate", rotate,"q_angle",angle],opt,jr_opt) );
		Rod( rqp, concat(["rotate",-rotate,"q_angle",angle],opt,jr_opt) );
	}		
}

//==========================
module Rod_demo_11_chain()
{
_mdoc( "Rod_demo_11_chain"
, " Show the role of cutAngleAfterRotate in making Rod joint when
;; rotation is needed. 
;;
;; dfcolor: no rotate 
;; red    : rotate. cutAngleAfterRotate= true (default)
;; green  : rotate, cutAngleAfterRotate= false
;;
");
	nside = 36;
	count = 4;
	
	linepts= randPts(count+1);

	//Chain( linepts );
	//MarkPts( linepts, ["labels",true] );

	_allrps = [for(i=range(count-1))
				let( pqr = [ linepts[i], linepts[i+1], linepts[i+2] ]
				   , opqr= [ linepts[i-1],  linepts[i], linepts[i+1], linepts[i+2] ]
				   , ang = angle(pqr)/2
				)
				rodPts( pqr
					  , ["r",0.5, "nside", nside
						,"q_angle", i>=count-2?90:ang
						,"q_rotate", i==0?0:twistangle(opqr)
						]
					  )
			 ];

//	allrps = concat( _allrps[0][0], [ for(rp=_allrps) rp[1] ]);
	allrps = concat( _allrps[0][0], joinarr( [ for(rp=_allrps) rp[1] ] ));
	//LabelPts( allrps, labels="");
	//Chain( allrps );					

//	p00  = onlinePt( p01(linepts), len=-1 );
//	plast= onlinePt( [get(linepts,-1), get(linepts,-2)] , len=-1 );
//
//	linepts2= concat( [p00], linepts, [plast] );
//
//	Chain( linepts2, ["transp",0.5, "r",0.3, "color","red"] );
//
//	//rp = rodPts( linepts,["nside",nside,"q_angle", ang, "r", 0.5] )[1];
//	//Chain( rp );
//
//	rp1 = rodPts( slice(linepts, 0,3),["nside",nside,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp1 );
//
//	rp2 = rodPts( slice(linepts, 1,4),["nside",nside,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp2 );
		
//	pts= joinarr( 
//			[for(c=range(1,count+1))
//				let( pqr=[ linepts2[c-1]
//						, linepts2[c]
//						, linepts2[c+1]
//						]
//				   , ang = angle(pqr)/2
//					)
//				rodPts(pqr,["nside",nside,"q_angle", ang] )
//			]
//		);

//[ "r", 0.05
//	  , "nside",     6
//	  , "label", "" 
//	  , "p_pts",undef  // pts on the P end
//	  , "q_pts",undef  // pts on the q end
//	  , "p_angle",90   // cutting angle on p end
//	  , "q_angle",90   // cutting angle on q end
//	  , "rotate",0  // rotate about the PQ line, away from xz plane
//	  , "cutAngleAfterRotate",true // to determine if cutting first, or
//						  // rotate first. 
//	  ]
	faces = faces("chain", nside=nside, count=count);

//	echo("faces_demo_5_chain.N=", N);
	echo("<br/>faces_demo_5_chain._allrps=", _allrps);
	echo("<br/>faces_demo_5_chain.allrps=", allrps);
	//echo("<br/>faces_demo_5_chain.pts=", pts);
	echo("<br/>faces_demo_5_chain.faces=", faces);
	//color(false, 0.8)
	polyhedron( points = allrps
			 , faces = faces
			 );

}

//Rod_demo_0();
//Rod_demo_1();
//Rod_demo_2_labels();
//Rod_demo_3_angles();
//Rod_demo_4_angles();
//Rod_demo_5_user_q_pts();
//Rod_demo_6_user_q_pts();
//Rod_demo_7_gear();
//Rod_demo_8_rotate();
//Rod_demo_8_1_q_rotate(); // given-up appoach, wait to be removed. 2014.8.29
//Rod_demo_9_rotate_and_angles();
//Rod_demo_10_join_rotate();
//Rod_demo_11_chain();
//Rod_demo_12_addfaces();

module Rod_demo_12_addfaces()
{
	echom("Rod_demo_11_addfaces");
	r = 1.5;
	nside = 6;
	pqr = randPts(3);

	Chain(pqr);
	rodpts = rodPts( pqr, ["r",1.5, "nside", nside] );
//	
//	echo( "rodpts: ", rodpts);
//	echo( "joinarr(rodpts): ", joinarr(rodpts) );
//	LabelPts( join(rodpts),true );

	MarkPts( joinarr(rodpts), ["labels",true] );
//	inner_p_pts = reverse( arcPts( p102(pqr), r=r*2/3, a=360, pl="yz", count=nside));    
//	inner_q_pts =  arcPts( pqr, r=r*2/3, a=360, pl="yz", count=nside);    

	inner_p_pts = expandPts( rodpts[0], dist=-0.5 );
	inner_q_pts = expandPts( rodpts[1], dist=-0.5 );

	echo(inner_p_pts, inner_q_pts );
	Chain(inner_p_pts);
	Chain(inner_q_pts);

	faces = rodsidefaces(nside);

	Rod( pqr,  ["r",1.5, "nside", nside, "label",true, "transp", 0.7

		,"has_p_faces",false
		,"has_q_faces",false
		,"addpts", concat( inner_p_pts, inner_q_pts)
		,"addfaces", concat( [for(f=faces) f+[nside*2,nside*2,nside*2,nside*2] ] )
	]);
}

module Rod_demo_12_hole()
{
_mdoc( "Rod_demo_12_hole"
,"
");
	nside=4;
	pqr = randPts(3);
	Chain(pqr);
	//MarkPts( pqr, ["labels","PQR"] );
	Rod( pqr, ["r",.7, "transp",0.8, "nside",nside
			  ,"label",true
				, "labelrange",true
				, "labelopt", ["begnum",nside*2, "color","red" ] 
			  ] );  

	Rod( pqr, ["r",2, "transp",0.2, "nside",nside
			  ,"label",true
				, "labelrange",true] );  

//    p_faces=	 [ [0,3,11, 8]
//			 , [1,0, 8, 9]
//			 , [2,1, 9,10]
//			 , [3,2,10,11] ];
//
//	q_faces=	 [ [4,5,13,12]
//			 , [5,6,14,13]
//			 , [6,7,15,14]
//			 , [7,4,12,15] ]

}

//Rod_demo_12_hole();
module Rod_bugfixing()
{
	pqr=[[-2.73324, -2.05788, -2.44777], [-0.285864, -1.8622, -1.77504], [2.84959, 2.00984, -2.64569]];
	rqp= [R(pqr),Q(pqr),P(pqr)];
	Rod( pqr, ["r", 0.8, "transp", 0.7, "label",true
				, "labelrange",true, "labelopt",["begnum",0,"prefix",""]]);
	Chain(pqr);
	MarkPts(pqr);

	//Rod( rqp, ["r", 0.8, "transp", 0.7, "label",true
	//			, "labelrange",true, "labelopt",["begnum",0,"prefix",""]]);

}
//Rod_bugfixing();




//========================================================
Text=["subarr","arr, cover=[1,1], cycle=true", "array", "Array",
 " Given an array (arr) and range of cover (cover), which is in the form
;; of [m,n], i.e., [1,1] means items i-1,i,i+1, return an array of items.
;;
;; Set cycle=true (default) to go around the beginning or end and resume 
;; from the other side. 
"];

module Text(pqr, txt){
    
  	module transform(followVP=ops("vp"))
	{
		translate( midPt( [L0,L1] ) )
	     color( ops("color"), ops("transp") )
		if(followVP){ rotate( $vpr ) children(); }
		else { rotate( rotangles_z2p( Q-P ) )
			  rotate( [ 90, -90, 0] )
			  children();
			}
	}
	conv = ops("conv");
	//echo("conv",conv, "L", L, "numstr(L, uconv=uconv)", numstr(L, conv=conv));
	transform()
	scale(0.04) text(str(ops("prefix")
						  //, L, "="
						  , numstr(L
								, conv=ops("conv")
								, d=ops("decimal")
								, maxd=ops("maxdecimal")
								)
						  , ops("suffix") ));
    
  
}

module Text_dev(){

  module AxisLines( oxyz
      , lnops=[]
      , plops=[]
    )
  {
      ln_ops= concat( lnops, ["r",0.03, "transp", 0.3]);
      pl_ops= concat( lnops, ["color","blue", "mode",3, "transp",0.2]); 
      
      Line( p01( oxyz), concat(["color", "red"], ln_ops) );
      Line( p02( oxyz), concat(["color", "green"], ln_ops) );
      Line( p03( oxyz), concat(["color", "blue"], ln_ops) );
      
      color( "green", hash(pl_ops,"transp") )
      { Plane( p123( oxyz), pl_ops );
        
      }    
      
//      color( "green", hash(pl_ops,"transp") )
//      { Plane( p012( oxyz), pl_ops );
//        Plane( p023( oxyz), pl_ops );
//        Plane( p013( oxyz), pl_ops );
//      }    
  }    
  
  pqr = randPts(3);
  //pqr = [[-1.8992, -2.12299, -1.62216], [1.25559, -0.48364, -1.36525], [-1.10772, -1.4157, 0.591315]];
//  pqr = [[-2.84926, -0.433825, 1.42911], [0.0140438, 1.59454, 1.47263], [0.313024, 0.256869, 0.98651]]; 
  // ERROR:
  // pqr= [[0.982085, -0.443367, -2.85914], [2.66226, 2.26935, 0.904825], [2.38218, 2.66048, 0.311629]]
  echo("pqr", pqr);
  
  P= pqr[0]; 
  Q= pqr[1];
  R= pqr[2];
  N= onlinePt( [Q, N(pqr)]  , len= 3 );
  M= onlinePt( [Q, N([N,Q,P])], len= 3 );

 
  O = Q;
  X = P;
  Y = M;
  Z = N;
  // New coordinate: 0:Q, x:P, y:M, z:N
  MarkPts( [O,X,Y,Z], ["labels","OPQR"] );
  
  
  // ==============================
    
  xyzo_0 = [O,X,Y,Z];

  //AxisLines( xyzo_0 );

  // ==============================

  xyzo_1 = [ for(p=xyzo_0) p+(ORIGIN-O)];
      
  AxisLines( xyzo_1,plops=["color","green"]);
  
  MarkPts( xyzo_1, ["labels","OXYZ"] );
  
  Arrow( [ O, xyzo_1[0] ]
       , ["color","red", "arrow",["r",0.15,"len",0.4], "arrowP",false ] );
  
  //========================    

  A = [ norm(xyzo_1[1]), 0, 0];
  MarkPts([A], ["labels","A"] );
  
  apts = arcPts( [A,ORIGIN, xyzo_1[1]],r=A.x, count=20 ); 
  Line( [ORIGIN,A] , ["r",0.005]);
  Chain( apts, ["closed",false, "r",0.01] );
    
  
  D = N( [A, ORIGIN, xyzo_1[1]], len=3);  
  YY = xyzo_1[2];
  ZZ = xyzo_1[3];
  
  T = othoPt( [ORIGIN, YY, D]);
  Tz= othoPt ( [ORIGIN, ZZ, D]);
  
  Line( [YY,T], ["r",0.03] );
  Mark90( [YY,T,ORIGIN] ); 
  MarkPts( [D,T], ["labels", "DT"] );
  Line( linePts( [ORIGIN, D], len=[5,5]), ["r", 0.03,"color","red"]); 
  
  //---------------------------------------
  a_xoa = angle([ xyzo_1[1], ORIGIN, A]);
  
  B= anyAnglePt( [ORIGIN, T, YY]
                , a=a_xoa, len= norm(T-YY), pl="yz"
                ); 
  MarkPts( [B], ["labels","B"] );              
  Chain( [ORIGIN, B,T], ["closed",false, "r",0.03] );              

  Line( [ORIGIN,B] , ["r",0.01]);
  aptsY = arcPts( [B,T, YY],r= norm(B-T) ); 
  echo("aptsY", aptsY);
  Chain( aptsY, ["closed",false,"r",0.01] );
  
//---------------------------------------
  a_xoa = angle([ xyzo_1[1], ORIGIN, A]);
  
  C= anyAnglePt( [ORIGIN, Tz, ZZ]
                , a=-a_xoa, len= norm(Tz-ZZ), pl="yz"
                ); 
  MarkPts( [C], ["labels","C"] );              
  Chain( [ORIGIN, C,Tz], ["closed",false, "r",0.03] );              

  AxisLines( [ORIGIN, A,B,C] );
}
//Text_dev();

scadx_obj_basic_history=[
["before", "history lost"]
,["20150519-", "Re-introduce Line0()" ]
,["20150522-1", "Fixing MarkPt(), PtGrid(),LabelPt(); Intruduce mti(module tree indent)" ]
,["20150525-2", "LabelPt labels with formatted pt easily; Deprecating LabelPts, use MarkPts() which now has several subobjs: (sphere) marking, label, grid, chain" ]
,["20150526-1", "Arc() refined with customizable rs, rads and twist." ]
,["20150526-2", "Arc() arg xsecpts makes it much more powerful." ]
,["20150527-1", "Arc() can rotate when xsecpts." ]
,["20150527-2", "Arc() can markxsec." ]
,["20150528-1", "Arc() can close, showxsec2d" ]
,["20150528-2", "Arc() can echopts; Arc ellipse example" ]
,["20150528- ", "Arc() radpts not very successful"]
,["20150529-1", "Arc(): new arg: sealed, showobj. Both excellent. Sealed is supposed to be the property of to-be-implemented Ring"]
,["20150529-2", "Arc(): new arg: frame, another excellent property"]

,["20150530-1", "Apply sopt to MarkPt, MarkPts"]
,["20150530-1", "MarkPt, MarkPts can 'hide'"]
,["20150601-1", "Plane upgraded to a top level obj."]
,["20150602-1", "Plane: fix minor bugs. Plane can markpts. MarkPts: new arg indices for selective markings"]
,["20150603-1", "LabelPt(),Chain(), MarkPt(),MarkPts() can take sopt,mopt. Disable Chainc,Chainf"]
,["20150604-1", "Plane() uses rotPts (instead of rotpqr) for rotate. Can rotate about any axis."]
,["20150604-2", "Review Arc() and fix some bugs."]
,["20150608-1", "MarkBezierPts"]
,["20150610-", "Imp projPt: New arg along; minor imp update"]
,["20150615-1","Recode Chain();Chain now uses tailroll to adjust last xsec"]
,["20150615-2","Imp Chain()"]
,["20150617-1","Bigfx Chain(): fix: twistangle returned nan (chk: Chain_demo_frame)"]
,["20150705-1","Imp: Chain0() no longer uses hull; Chain(): large improvement to make use of new randchain and chainPts; Chain_demo_input()"]
,["20150706-1","bugfix: When Chain( frame=true), xpts failed to rotate by 45. Wierd cos it was good b4. Fixed anyway by inserting an nside."]

//    function d01pow2(pqr)= pow(norm(pqr[1]-pqr[0]),2); // pw(d01(pqr))  
//    function d12pow2(pqr)= pow(norm(pqr[2]-pqr[1]),2);
//    function d02pow2(pqr)= pow(norm(pqr[2]-pqr[0]),2);
];

/* Funcs that take a varying len of args:


dist(pq), dist(P,Q)

*/

SCADX_OBJ_BASIC_VERSION = last(scadx_obj_basic_history)[0];



