// doctest.scad: OpenSCAD documentation and unit testing library
// Copyright (C) 2015 Runsun Pan (run+sun (at) gmail.com) 
// Source: https://github.com/runsun/doctest.scad
// 
// This library is licensed under the AGPL 3.0 described in
// http://www.gnu.org/licenses/agpl.txt
//
// This file, doctest.scad, is part of scadx library
//
// The OpenSCAD_DocTest library is free software: you can redistribute it and/or 
// modify it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//-------------------------------------------------------------------------


// openscad_doctest.scad
//
// For a quick look, check out openscad_doctest_demo_1.scad 
// For details, check out openscad_doctest_help.scad 


/* Command line
   
   This file can be loaded and run in command line. The main purpose
   is to output the return of doctest_demo() to a html file. To do so:
   
   >>> openscad doctest.scad -o dump.stl -D "ISDEMO=true" 2> doctest_demo.htm
   
   This would give you a well formatted html file. 
*/
//. Init   
ISDEMO=false; 

// Used to disable the display of "ECHO:" when dumping to html file
html_cmt_beg = ISDEMO? "<!--":"";
html_cmt_end = ISDEMO? "-->":"";

// 2014.9.30:
// Apply DT_PRECISION (set to 8) to DT_ZERO 

// old one:
// DT_ZERO=1e-13;  	// used to chk a value that's supposed to be 0: abs(v)<DT_ZERO

DT_PRECISION=8;
DT_ZERO=pow(10,-DT_PRECISION); // used to chk a value that's supposed to be 0: a

//DT_MODES=[		// doctest modes
// 0, "No doc, no test, show function headers only."  // New mode: 000
//,1, "No doc, no test, show all test cases as usage examples." // 002
//                                                     // 010 (newly added)
//,2, "No doc. Do tests, but show only failed cases."  // 011
//,3, "No doc. Do tests and show all test cases."      // 012
//
//,10, "Show doc, no test." // 100
//,11, "Show doc, no test, show all test cases as usage examples." // 102
//
//                                                            //110 (newly added
//,12, "Show doc, do tests, but show only the error cases."  // 111
//,13, "Show doc, test and show all test cases."  // 112
//];

//SHOW_NO_DOC=0;
//SHOW_DOC=100; // 0, 100
//
////DO_NO_TEST=0;
//DO_TESTS=10; // 0,10
//
//SHOW_NO_TEST=0;
//SHOW_FAIL_TESTS = 1;
//SHOW_ALL_TESTS=2;


// ========================================================
//
/// Utilities 
// functions needed for doctest. All are prefixed with dt_.
// Documentations/tests are in the file openscad_doctest_help.scad
//
// ========================================================
dt_begwith=["dt_begwith","o,x","T|F","Core"
, "Return true if o begins w/ x
;;
;;  begwith( 'abcdef', 'def' )= false
;;  begwith( 'abcdef', 'abc' )= true
;;  begwith( ['ab','cd','ef'], 'ab' )= true
;;
"]; 
function dt_begwith(o,x)= x==""||x==[]?undef:dt_index(o,x)==0;
function dt_countStr(arr)= dt_countType(arr, "str") ;
function dt_countType(arr, typ, _i_=0)=
(
     _i_<len(arr)?
	 ( 
		typ=="num"?
		((dt_isnum(arr[_i_])?1:0)+dt_countType(arr,"num",_i_+1))
		:((dt_type(arr[_i_])==typ?1:0)+dt_countType(arr, typ, _i_+1)) 
	 )
	 :0
);

// 2015.3.27: Disabled dt_echo_. 
//module dt_echo_(s, arr=undef)
//{
//	if(arr==undef){ echo(s); }
//	else if (dt_isarr(arr)){
//		echo( dt_s(s, arr));
//	}else { echo( str(s, arr) ); }
//}

// 2015.3.27: Disabled dt_endwith . Only dt_split depends on it. dt_split is disabled too.
//function dt_endwith(o,x)= 
//( 
//	dt_isarr(o)? 
//	  o[len(o)-1]==x
//	  :dt_index(o,x)+len(x)==len(o)
//);

function dt_getModeStr(mode)=
  dt_hash( 
  [
    000, "!doc, !test, !show_test"
   ,002, "!doc, !test, show_all_tests"
  
   ,010, "!doc, test, !show_test"
   ,011, "!doc, test, show_fail_tests"
   ,012, "!doc, test, show_all_tests"
  
   ,100, "doc, !test, !show_test"
   ,102, "doc, !test, show_all_tests"
  
   ,110, "doc, test, !show_test"
   ,111, "doc, test, show_fail_tests"
   ,112, "doc, test, show_all_tests"
  ], mode);
  

//. Tools
  
function dt_fmt(x, s=_span("&quot;,&quot;", "color:purple") 
			, n=_span(",", "color:brown") 
			, a=_span(",", "color:navy") 
			, ud=_span(",", "color:brown") 
			, fixedspace= true
		   )=
(
	x==undef? dt_replace(ud, ",", x) 
	:dt_isarr(x)? dt_replace(a, ",", dt_replace(str(x),"\"","&quot;")) 
	: dt_isstr(x)? dt_replace(s,",",fixedspace?dt_replace(str(x)," ", "&nbsp;"):str(x)) 
		:dt_isnum(x)||x==true||x==false?dt_replace(n,",",str(x))
				:x
);

function dt_get(o,i, iscycle=false)= 
( 
	iscycle? 
	( o[ i%len(o)<0?(len(o)+i%len(o)):i%len(o)] ) 
	: ( dt_inrange(o,i)?( o[i<0?(len(o)+i):i] ):undef )
);

function dt_hash(h,k, notfound=undef, _i=0)= 
(
	!dt_isarr(h)? notfound      // not a dt_hash
	:_i>len(h)-1? notfound  // key not found
		: h[_i]==k?        // key found
			 (_i==len(h)-1?notfound  // value not found
                : h[_i+1] 
			 )
			: dt_hash( h=h, k=k, notfound=notfound, _i=_i+2)           
);

// 2015.3.27: Disabled dt_haskey
//
//function dt_haskey(h,k,_i_=0)=
//(
//	dt_index(dt_keys(h),k)>=0
//);

function dt_index(o,x,_i=0)= 
(
	x==""?-1: 
	(
		dt_type(o)=="arr"?			// array
		(	x==o[_i]?_i
			: _i==len(o)?-1
              :dt_index(o,x,_i+1) 
		):(						// string				
			_i+len(x)>len(o)?-1
			 : dt_slice( o, _i, _i+len(x))==x? _i
		     : dt_index( o,x,_i+1)
		)
	)
);


function dt_inrange(o,i)=
(
	/*
	len: 1 2 3 4 5
		 0 1 2 3 4   i>=0
   		-5-4-3-2-1   i <0    
	*/
	
	len(o)==0? false
	: !dt_isint(i)?false
		:i>=0? i < len(o)
	  		: abs(i) <= len(o)
);


function dt_isarr(x)=   dt_type(x)=="arr";
function dt_isequal(a,b, prec)=   // New 2014.9.30: prec
						abs(a-b)< pow(10,-dt_or(prec, DT_PRECISION) );
function dt_isint(x)=   dt_type(x)=="int";
function dt_isnum(x)=   dt_type(x)=="int" || dt_type(x)=="float";
function dt_isstr(x)=   dt_type(x)=="str";
function dt_join(arr, sp=",", _i_=0)= 
(
	_i_<len(arr)?
	str(
		arr[ _i_ ]
		, _i_<len(arr)-1?sp:""
		,dt_join(arr, sp, _i_+1)
	):""
); 

// 2015.3.27: Disabled dt_keys
//
//function dt_keys(h, _i_=0)=
//(
//[ for(i=[0:2:len(h)-2]) h[i] ]

//==================================== 
// #1
// This code causes recursive error:
//  
// ERROR: Recursion detected calling function 'concat'
// ERROR: Recursion detected calling function 'dt_index'
// 
// when running doctest_tool_test(mode=?) where mode= 1,2,3,11,12,13
// 
//	 _i_>=len(h)-1? []
//	 :( concat( [h[_i_]]
//	         , dt_keys( h, _i_+2) 
//			 )
//	 )
//
// Oddly, different codes (#2, #3 below) that give same return
// doesn't produce the recursive errof:
//
// #2
//_i_>=len(h)? []
//	 _i_<len(h)-1
//	 ?( concat( [h[_i_]]
//	         , dt_keys( h, _i_+2) 
//			 )
//	 ):[]
//
// #3
//	[ for(i=[0:2:len(h)-2]) h[i] ]
//==================================== 


function dt_multi(x,n=2)=
(
	dt_isarr(x)?
	concat(x, n>1?dt_multi(x,n-1):[])
	:str(x, n>1?dt_multi(x,n-1):"")
);

function dt_or(x,dval)=x?x:dval;


//function dt_replace(o,old,new="",_i=0)=
//(  let( isarr = dt_isarr(o)
//      , L = len(old)
//      , now = isarr? o[_i]
//              : L==1? o[_i]
//                :dt_join( [ for( j=[_i:_i+L-1]) o[j]],"")
//      , _new = now==old?new:now
//      )               
//   isarr
//   ?
//      ( _i>=len(o) ?[]
//       :concat( [_new], dt_replace( o,old,new,_i+1)) 
//      )
//   :        
//      ( _i>len(o)-L ?""
//       :str( _new, dt_replace( o,old,new,_i+L) )
//      )
//); 
function dt_replace(s,old,new="",_i=0)=   // 2015.3.25: reduce its application 
                                          // for string only AND recode completely.
                                          // THis version is 3X faster than old one
(  let( L = len(old)
      , now =  L==1? s[_i]
                :dt_join( [ for( j=[_i:_i+L-1]) s[j]],"")
      , _new = now==old?new:s[_i]
//      , debug= dt_s(
//               "<br/>_i={_}, now={_}, now==old={_}, _new="
//               , [_i, now,  now==old])
      )               
   s[_i]==undef ?""
   //:str( debug," ", _new, dt_replace( s,old,new,_i+ (now==old?L:1)) )
   :str( _new, dt_replace( s,old,new,_i+ (now==old?L:1)) )
                
);     

//replace_data= "This is a test for a test";                
//echo( replace_data );
//echo( dt_replace(replace_data, "e", "E") );
//echo( dt_replace(replace_data, "is", "IS") );
//echo( dt_replace(replace_data, "test", "TEST") );
//echo( dt_replaces(replace_data, ["tes","TES", "is","IS", "a","A"]) );
//              
                                
function dt_replaces(o, pairs,_i=0)=
(   
   _i<= len(pairs)/2+2? 
   dt_replaces( 
           dt_replace( o, pairs[_i], pairs[_i+1] )
           , pairs, _i+2 
    ):o
);              


function dt_s(s,arr, sp="{_}", _i_=0)= 
( 
	dt_index(s,sp)>=0 && len(arr)>_i_ ?
		str( dt_slice(s,0, dt_index( s,sp))
			, arr[_i_] 
			,  dt_index(s,sp)+len(sp)==len(s)?
				"":(dt_s( dt_slice( s, dt_index(s,sp)+len(sp)), arr, sp, _i_+1))
		   ): s
);

function dt_slice(s,i,j=undef)= 
(
  let( j= j==undef? len(s): j
     )
  !dt_inrange(s,i) || (j>0 && i>j) || (i<0 && j<i)? 
  undef
  :(
	s=="" || i==j ?""
    : dt_join( [ for(k=[0:len(s)-1]) if( i<=k && k<j ) s[k] ], "" )
		                     
  ) // dt_inrange
  //:undef
);
    
//function dt_slice(s,i,j=undef)= 
//(
//  !dt_inrange(s,i) || (j>0 && i>j) || (i<0 && j<i)? 
//  undef
//  :(
//	dt_isstr(s)?(
//	s==""?
//	 "":	(i==j?
//		 "": str(
//				  dt_get(s,i)
//				, ( (  i<0? 
//				      (len(s)+i):i
//                     )<(
//				      j==undef ?
//      			      len(s)
//      			      : (j<0?len(s)+j:j)
//                       )-1
//			       )?
//			       dt_slice(s,i+1, j)
//			       :""
//		   		)//str
//		)//i==j
//	):                       
//	(s==[]?				// array
//	 []:	( i==j?
//		  []: concat(
//				  	[dt_get(s,i)]
//				  , ( ( i<0?(len(s)+i):i
//                       )<
//					  ( j==undef ?
//      			           len(s)
//      			           : (j<0?len(s)+j:j)
//                       )-1
//			         )?
//			         dt_slice(s,i+1, j) : []
//		   		   ) //concat
//		 )//i==j
//	)//s==[]
//  ) // dt_inrange
//  //:undef
//);    

//slice_data = "This is a test";
//echo( slice_data );
//echo( dt_slice( slice_data, 1 ));
    
// 2015.3.26: Disabled dt_split
//
//function dt_split(s,sp=" ")=
//(
//	dt_index(s,sp)<0?
//	[s]: concat(
//		 [ dt_slice(s, 0, dt_index(s,sp))]
//		   , dt_endwith(s,sp)?
//			 "":dt_split( dt_slice(s, dt_index(s,sp)+len(sp)), sp ) 
//		)
//);

function dt_type(x)= 
(
	x==undef?undef:
	( abs(x)+1>abs(x)?
	  floor(x)==x?"int":"float"
	  : str(x)==x?
		"str"
		: str(x)=="false"||str(x)=="true"?"bool"
			:([0]==[0])? "arr":"unknown"
	)
);


//===========================================
//
//. Console html tools 
//
//===========================================

_SP  = "&nbsp;";
_SP2 = "　";  // a 2-byte space 
_BR  = "<br/>"; 

function _tag(tagname, t="", s="", a="")=
(
	str( "<", tagname, a?" ":"", a
		, s==""?"":str(" style='", s, "'")
		, ">", t, "</", tagname, ">")
);

function _div (t, s="", a="")=_tag("div", t, s, a);
function _span(t, s="", a="")=_tag("span", t, s, a);
function _table(t, s="", a="")=_tag("table", t, s, a);
function _pre(t, s="font-family:ubuntu mono;margin-top:5px;margin-bottom:0px", a="")=_tag("pre", t, s, a);
function _code (t, s="", a="")=_tag("code", t, s, a);
function _tr   (t, s="", a="")=_tag("tr", t, s, a);
function _td   (t, s="", a="")=_tag("td", t, s, a);
function _b   (t, s="", a="")=_tag("b", t, s, a);
function _i   (t, s="", a="")=_tag("i", t, s, a);
function _u   (t, s="", a="")=_tag("u", t, s, a);
function _h1  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h1", t, s, a); 
function _h2  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h2", t, s, a); 
function _h3  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h3", t, s, a); 
function _h4  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h4", t, s, a); 

function _color(t,c) = _span(t, s=str("color:",c));
function _red  (t) = _span(t, s="color:red");
function _green(t) = _span(t, s="color:green");
function _blue (t) = _span(t, s="color:blue");
function _gray (t) = _span(t, s="color:gray");

function _tab(t,spaces=4)= dt_replace(t, dt_multi("&nbsp;",spaces)); // replace tab with spaces
function _cmt(s)= _span(s, "color:darkcyan");   // for comment

// Format the function/module argument
function _arg(s)= _span( s?str("<i>"
					,dt_replace(dt_replace(s,"''","&quot;"),"\"","&quot;")//"“"),"\"","“")
					,"</i>"):""
					, "color:#444444;font-family:ubuntu mono");


//========================================================
//========================================================
//========================================================

  
//. Main  
  
module doctest(doc
              , recs=[]
              
              , mode=undef      // mode is set either by mode = dtl, where
              , isdoc=1         //  d=doc (0|1), t=test(0!1), l=test-level (0|1|2)
              , istest=1        // or set them seperately: isdoc=d, istest=t
              , testlevel= 2    // testlevel= l
              
              , ops=[]
              , scope=[]
              //, selftest=false // New 2015.3.18
              )
{
	//-----------------------------------
	//. Setting
	//-----------------------------------

	ops= concat( ["scope", scope], ops, 
	[
          "asstr", false  // test them as string
        , "prec", false   // New 2014.9.30. An integer to define 
                          // number of significant digits below 0.
        , "nl", false // New 2015.3.18: Set true to show result
                           // in the next line     
        , "labelhead", "#"  
                // New 2015.3.29: 
                // If rec = [ "# ..." ], then the original rec items:
                //   [        got, arg, want, ops ] becomes:
                //   [ label, got, arg, want, ops ], and funcname
                // will be replaced with label 
                // 
                //  [begwith("abc","a"),"'abc','a'", true]
                //  ==> |> begwith("abc","a")= true
                // 
                //  1) First is a str begs with #:
                //
                //  [ "#myfunc", dt_begwith("abc","a"),"'abc','a'", true]  
                //  ==> |> myfunc("abc","a")= true
                //
                //  2) If it ends with "=" disables the argument:
                //  
                //
                //  [#myfunc=", dt_begwith("abc","a"),"'abc','a'", true]  
                //  ==> |> myfunc= true
                //
                //  3) If it contains {_}, enter args there. This allows 
                //     post-function string
                //
                //  [#join(myfunc({_}),br)", begwith("abc","a"),"'abc','a'", ...]
                //  ==> |> join(myfunc("abc","a")),br)= ...  
                
        , "docbr", ";;"  // the line break in doc
        // Each rec be displayed as:
        //   |#: xxx   usage
        //   |#> xxx   test-pass
        //   ?#> xxx   test-failed
        , "prefix", "|"   // prefix for each rec line    
        , "testprompt", ">"      // rec prompt when test-pass
        , "failprompt", _red("?")// rec prompt when test-failed
        , "usageprompt", ":"     // rec prompt when no test
	]);
	function ops(k)=dt_hash(ops,k);

	fname = doc[0];  // function name  like "sum"
	args  = doc[1];  // argument string like "x,y"
	retn  = doc[2];  // return dt_type ("array", "string"...), "n/a" if module
	tags  = doc[3];  // like, "Array,Inspect"
	docdata= doc[4]; // New 7/7/14
	
	//echo("ops = ", ops);

	//mode = ops("mode"); 
    mode = mode==undef?
           isdoc*100+istest*10+ testlevel: mode ;
       
    //echo(_b(str("<br/>doctest, mode=", mode=mode), s="background:yellow"));
    
	//echo(str("mode: ",mode, ", ", dt_getModeStr(mode)));
    
	prefix = ops("prefix");	       // "|"
    prefsp= str(prefix, " ");      // "| "
    brpref= str(_BR, prefix);      // "<br/>|"
    //brprefsp= str(brpref, " ");    // "<br/>| " 
    testprompt = ops("testprompt"); // ">"
    failprompt = ops("failprompt"); // "?"
    usageprompt= ops("usageprompt");// ":"
    
//	docmode = floor(mode/10);  // 0: no doc
//							// 1: show dow
//							


//	is_show_doc= docmode>0;
//	is_do_test = testmode > 1;
//	is_show_header= true;      // always show header for now;
//	is_module = retn=="n/a";

    function getdigit(n,d=1)=
       // getdigit(456,1) = 6
       // getdigit(456,2) = 5
       // getdigit(456,3) = 4
       d>1? getdigit( floor(n/10), d-1)
       : (n<10?n
         : round( 10*(n/10 - floor(n/10)) ) //round() to make sure it's integer
         );
        //    echo( d456_1= getdigit( 456,1)  
        //        , d456_2= getdigit( 456,2)
        //        , d456_3= getdigit( 456,3)
        //        );
        
    is_module = retn=="n/a";
    is_show_doc = getdigit(mode,3)==1;

    is_do_test = getdigit(mode,2)==1; 
    is_show_header= true;      // always show header for now;
	show_test_level= getdigit(mode, 1); // => 0,1,2
                   
    //echo( show_level_12= getdigit(12,1) );


//    echo( is_module=is_module, is_show_doc=is_show_doc
//        , is_do_test=is_do_test, is_show_header=is_show_header
//        , show_test_level=show_test_level
//        //, testmode = testmode
//        );


	//. Header


	function _header_fname(nfail)=
	(	
		// Return the formatted function name. 
		// If nfail>0, format it with red. 
		_b( nfail==0? fname:_red(fname) )
	);
    
  function _header_args()=
	(	
		// get formatted args for the header.
 		_blue( str( " <b>(</b> ", _arg(args) , " <b>)</b>") )
	);
    

	function _header_testcount( testcount, nfail)=
	(
		/// get testcount/failed count. Be called
        // only (1) for functions (but not modules)
		// and (2) when tests were perormed. i.e.,
        // mode = 010,011,012,110,111,112 (middle=1) 
		str( _green("=")
		   , retn
		   , testcount==0 ? ""
			: dt_s( " ( tested:{_}{_} )"
				, [ testcount
				  , nfail==0?""
                    : _red(str( "/failed:",nfail) )
 				  ]
			    )
		   )
	);	


	function _header_getHeader(doc, testcount=0, nfail=0)=
	(  	
		// Given doc = [ docname, argstring, return, tags, doclines ],
		// (where return="n/a" if doc is for a module), return doc header.
        //
        // A doc header is the very first line of text like :
        // 
        //  dt_begwith ( o,x )=T|F ( tested:8/failed:2 )
        // 
		// Note that when only header is shown (mode<100 & show_test_level=0),
		// it'd be a one-liner. We 1) skip br; 2) do NOT underscore. 

//		_u(str((mode==0||mode==2)?"</u>":"<br/>" // header is a one-liner

		_u(str(
                 ( mode==0 || mode==10 || mode==11)
                 //mode<100 && show_test_level==0)
                 ? "</u>"  // stop the underline initiated by _u.
                 : "<br/>" 
			  , _header_fname(nfail)   //= dt_begwith
			  , _header_args()         //= ( o,x )
		      , retn=="n/a"?""         //= ( tested:8/failed:2 ) 
                :_header_testcount( testcount, nfail)
              , "</u>  "
              ,  _tag("sup", str("(dt mode:",mode, ")"), s="color:gray" ) 
	     ))
	); 



	//. Doc

	function _doc_getdoc( doc , tags, _i_=0 )=
	( 
        let( docbr= ops("docbr")
           , doc = dt_replaces( doc, 
                    [ docbr, brpref, "'", "\""] )
           )
//      let( lines = dt_split(doc, ops("docbr")) 
//         , lnbr  = str( "<br/>", prefix, " " )
//         ,doc = dt_join(
//            [ for(i=[0:len(lines)-1])
//                dt_replace(lines[i],"'","\"")		
//            ], lnbr
//            )
//         )
      str( prefsp, doc , brpref, " <u>Tags:</u> ",tags )
       //)
	); 


  
	//-------------------------------------------------------
	//-------------------------------------------------------
	// 
	//. Test functions
	// 
    // Goal: => [ "","",failtext, "","", failtext, ""...]        
	//-------------------------------------------------------
	//-------------------------------------------------------
  
	//-----------------------------------
	//   test settings
	//-----------------------------------


	scope  = ops("scope");
	asstr  = ops("asstr");
    newline= ops("nl");
    prec   = ops("prec");
    labelh= ops("labelhead");

//-------------------------------------------------------
	function _test_do_a_test(rec)=  //==> "" (when pass) 
								        //    [want,got]  (when fail)  
   /// let                     
	 let( rops = rec[3]
        , got  = rec[0]
        , want = rec[2]
        , fgot = dt_fmt(got)
        // 2014.9.30: add prec
		, prec= dt_or( dt_hash( rops,"prec")     // rec-level prec
				 , dt_hash(ops,"prec", false) // test-level prec
				 )
          // 2015.3.20: add nl        
          ,nl = dt_or( dt_hash( rops, "nl")
                 , dt_hash(ops,"nl", false) // test-level prec
				 ) 
          ,asstr= dt_or( dt_hash( rops,"asstr")     // rec-level prec
				 , dt_hash(ops,"asstr", false) // test-level prec
				 )       
         // ,notest= dt_hash(rec[3],"asstr",false)     // rec-level prec
		)
	( /// content 
	     dt_isstr(rec)||
		(
//			(dt_hash(ops,"asstr")            // test-level asstr
//		     || dt_hash(rec[3],"asstr", false)// rec-level asstr
//		  	) 
			asstr? str( got )==str( want )
			: ( dt_isnum( want )
			    ? (
					// when want is a number, we use dt_isequal()		
					dt_isequal( got, want , prec)  // 2014.9.30: add prec
				  )	
			    : got == want
                )
		)? ""                   // <------ pass
         :[ want, got ]         // <------ fail
//        : nl?str( _BR, "got:", _BR
//                ,  fgot //dt_replace( fgot, "<br/>", "<br/>")
//                //, _BR
//                )
//            :str( " got: ", fgot )
	);
	
//	//-------------------------------------------------------
//	function _test_do_a_test(rec)=  //==> ""|failtext 
//								// where failtext is something like "got: 1"
//	 let( rops = rec[3]
//        , got  = rec[0]
//        , want = rec[2]
//        , fgot = dt_fmt(got)
//        // 2014.9.30: add prec
//		, prec= dt_or( dt_hash( rops,"prec")     // rec-level prec
//				 , dt_hash(ops,"prec", false) // test-level prec
//				 )
//          // 2015.3.20: add nl        
//          ,nl = dt_or( dt_hash( rops, "nl")
//                 , dt_hash(ops,"nl", false) // test-level prec
//				 ) 
//          ,asstr= dt_or( dt_hash( rops,"asstr")     // rec-level prec
//				 , dt_hash(ops,"asstr", false) // test-level prec
//				 )       
//         // ,notest= dt_hash(rec[3],"asstr",false)     // rec-level prec
//		)
//	(
//	     dt_isstr(rec)||
//		(
////			(dt_hash(ops,"asstr")            // test-level asstr
////		     || dt_hash(rec[3],"asstr", false)// rec-level asstr
////		  	) 
//			asstr? str( got )==str( want )
//			: ( dt_isnum( want )
//			    ? (
//					// when want is a number, we use dt_isequal()		
//					dt_isequal( got, want , prec)  // 2014.9.30: add prec
//				  )	
//			    : got == want
//                )
//		)? ""                   // <------ pass
//        
//        : nl?str( _BR, "got:", _BR
//                ,  fgot //dt_replace( fgot, "<br/>", "<br/>")
//                //, _BR
//                )
//            :str( " got: ", fgot )
//	);
//	
//	//-------------------------------------------------------
//	function _test_do_a_test_new(rec)=  //==> ""|failtext 
//								// where failtext is something like "got: 1"
//	 let( 
//         rops = rec[ len(rec)-1 ] // last item. This could be 3 (normal)
//                                   // or 4 (when 1st item is str beg/with labelhead
////        , labelh= dt_or( dt_hash( rops,"labelhead")     // rec-level labelhead
////				 , labelh) // test-level labelhead
////				
////        , haslabel = labelh && dt_begwith(rec[0], labelh)
////        , rec = haslabel? dt_slice( rec, 1):rec
//        , got  = rec[ 0 ]//haslabel?1:0]
//        //, arg  = rec[ 1]//haslabel?2:1]
//        , want = rec[ 2]//haslabel?3:2]
//        , fgot = dt_fmt(got)
//        // 2014.9.30: add prec
//		, prec= dt_or( dt_hash( rops,"prec")     // rec-level prec
//				 , dt_hash(ops,"prec", false) // test-level prec
//				 )
//          // 2015.3.20: add nl        
//          ,nl = dt_or( dt_hash( rops, "nl")
//                 , dt_hash(ops,"nl", false) // test-level prec
//				 ) 
//          ,asstr= dt_or( dt_hash( rops,"asstr")     // rec-level prec
//				 , dt_hash(ops,"asstr", false) // test-level prec
//				 )       
//         // ,notest= dt_hash(rec[3],"asstr",false)     // rec-level prec
//		)
//	(
//	     dt_isstr(rec)||
//		(
////			(dt_hash(ops,"asstr")            // test-level asstr
////		     || dt_hash(rec[3],"asstr", false)// rec-level asstr
////		  	) 
//			asstr? str( got )==str( want )
//			: ( dt_isnum( want )
//			    ? (
//					// when want is a number, we use dt_isequal()		
//					dt_isequal( got, want , prec)  // 2014.9.30: add prec
//				  )	
//			    : got == want
//                )
//		)? ""                   // <------ pass
//        
//        : nl?str( _BR, "   got:", _BR,  fgot)
//            :str( " got: ", fgot )
//	);


	//-------------------------------------------------------
	function _test_do_tests(recs=recs)=  //==> [ "","",failtext, "","notest" ...]
	(	
		// Given recs, do the testing on all recs and return array containing
		// 0 (pass) or failtext for each rec ( like, [0,0,"...",0,"..."] )
		// Each rec: [ arg_arr, got, want, ops]
		//  where ops could have : "asstr" "//", etc
		[ for( rec= recs ) 
            dt_hash( rec[3], "notest")? "notest"
            : _test_do_a_test( rec= rec ) ]  		
	);


//    //-------------------------------------------------------
//	function _test_do_tests_new(recs=recs)=  //==> [ "","",failtext, "","notest" ...]
//	(	
//		// Given recs, do the testing on all recs and return array containing
//		// 0 (pass) or failtext for each rec ( like, [0,0,"...",0,"..."] )
//		// Each rec: [ arg_arr, got, want, ops]
//		//  where ops could have : "asstr" "//", etc
//		[ for( rec= recs ) 
//            let( rops= rec[ len(rec)-1]
//               , labelh = dt_hash( rops, "labelhead", labelh)
//               )
//            dt_hash( rec[3], "notest")? "notest"
//            : dt_begwith( rec[0], labelh)?
//              _test_do_a_test( rec= dt_slice(rec,1) ) 
//              :_test_do_a_test( rec= rec ) 
//        ]  		
//	);
        
	//-------------------------------------------------------
	//-------------------------------------------------------
	// 
	//. Display
	// 
	//-------------------------------------------------------
	//-------------------------------------------------------

    function _display_startline()=
        str(_BR, _code(dt_multi("=", 65), s="color:darkblue")); //s="background:#ddd"));
        
	function _display_getScopeLines( scope=scope )=
	(
		// Return an array containing scope item pairs as strings
	 	// with formatting. For example,
		// 
		// You write this:
	 	//     ["s", "a_text" ,"t", [2,3,5]]
		//   
		// and this function give you this:
	 	// 	  [ "> s= \”a_text\""
		//     , "> t= [2, 3, 5]"   ]
       scope?[ for(i=[0:2:len(scope)-2])
          str( prefix,  scope[i], "= ", dt_fmt( scope[i+1] ), "; ") 
       ]:[]	
	);

	//-------------------------------------------------------
	function _display_getInlineRem(rec)=   //==> ""|rem 
	(	
		// Given a test rec, like:
		// [["-3.8"],int("-3.8"),-3, [["//","_Note this_"]]]
		// return the formatted rem (in this case, " // _Note this_" )
        let( rem = dt_hash( rec[3], "//" ) )
        rem? _green(str( " // ", dt_replace(rem,"'","\"") ) ):""
//		dt_haskey(rec[3],"//")? 
//		_green(str( " // ", dt_hash(rec[3],"//") ) ):""
	);

       
	//-------------------------------------------------------
    function _display_getStrRecLine( rec )=
	    let( rec= dt_replace(rec, "'","\"")
           )    
        str(prefix,dt_begwith(rec,"//")?_cmt(rec):rec);
       
       
	function _display_getRecLine( rec, i, testi )=
	(   // i: index of rec
        // testi: index of test in recs. A rec sometimes is a string
        //    that can't be counted as a "test". So testi is always <=i
        //
        // Return: a rec string, or ""
        //
        let( 
         br= "<br/>", pf = prefix
       , rops= rec[3] // rec ops 
       , isnotest =  dt_hash( rops, "notest")
       , failmsg= is_do_test==false || isnotest ?"":failmsgs[i]
       
       // --- fname ---
       , myf = dt_hash( rops, "myfunc") //=[f,a],f,ud
       , myfa= dt_isarr(myf)?myf:dt_isstr(myf)?[myf,undef]:undef //=[f,a],[f,ud],ud
            /* 
               myf, myfa: Custom func name/args
           
                [ want, arg, got, ["myfunc", "A_label"] ]
                [ want, arg, got, ["myfunc", ["A_label", "my_arg"] ]
             
               will give: 
             
                |> A_label= want 
                |> A_label( my_arg ) = want
           */
       , f0 = myf? myfa[0]:fname
       , f  = _color(f0, failmsg?"red":"darkblue")
       
       // --- arg ---
       , a0 = myf? myfa[1]:rec[1]
       , a  = a0==undef? "" 
              : dt_replace( 
                  dt_s( "<b>(</b><i style=\"color:#555\"> {_} </i><b>)</b>"
                      , [failmsg?_red(a0):a0])
                      , "'","\"" )   
                      
       , nl  = dt_hash(rops,"nl")
       
       , eq  = failmsg? "": _color("= ","gray")       
                    
//       , eq  = failmsg? (nl?str(_BR, prefix, "   want: ")
//                         :" want: " 
//                      )   
//                    :_color("= ","gray")
       , nlpf= str( pf, _span("   ",s="background:#dfe"))
       , want= nl? dt_replace( dt_fmt( rec[2] ), br, str(br, nlpf))
                 : dt_fmt( rec[2] )
       , got = nl? dt_replace( dt_fmt( rec[0] ), br, str(br, nlpf))
                 : dt_fmt( rec[0] )
       , isasstr= dt_or( dt_hash(rec[3],"asstr")     // rec-level prec
				 , dt_hash(ops,"asstr", false) // test-level prec
				 ) 
       , asstr = is_do_test&&isasstr?"*":""
      // , pf  = failmsg?_red("?"): (mode==0||mode==1||mode==10||mode==11)?"!":"|"
       , rem = _display_getInlineRem(rec)
       , right = nl?
                  (failmsg?
                     str( rem, br
                        , nlpf,_u("want"),":<br/>", nlpf,want, br
                        , nlpf, _u("got"), ":<br/>", nlpf, got
                        )
                  :  str(rem, br,  nlpf, want ) 
                  )
                 :(failmsg?
                     str( " want: ",want, " got: ", got, rem )
                  :  str( want, rem)
                  )      
                   
//       , rec = dt_isstr(rec)? 
//               dt_replace(rec, "'","\""): rec
       )    
    //dt_isstr(rec)? str(prefix,dt_begwith(rec,"//")?_cmt(rec):rec)
   //: ((show_test_level==1 && failmsgs[i]) || show_test_level==2) ?
     //:
     str( is_do_test&& failmsg?_red("?"):prefix
        , testi
        , failmsgs[i]=="notest"?usageprompt: is_do_test?testprompt:usageprompt 
        , " " //failmsg?_red("?"):" "
        , f,a, asstr
        //, nl && failmsg? rem:""
        , eq
        , right
//        , failmsg?( nl?str(_BR, prefix, "   want: ")
//                  :" want: "):""
//        , nl? str( !failmsg?rem:""
//                 , "<br/>", prefix)
//             :"" //,"   "):"" 
////                 , "<br/>", prefix):"" 
//        , nl? dt_replace(right, "<br/>",str("<br/>",prefix))//,"   "))
//            : str(right, rem) 
            
//        , nl? dt_replace(right, "<br/>",str("<br/>",prefix))
//            : str(right, rem) 
        ) // str

	);

//    failmsgs= is_do_test?_test_do_tests():[]; // [ "", " got:3", "" ...]
//    fails = failmsgs ==[]?0:len( [ for(t=failmsgs) if(t) 1 ] );
    //echo( failmsgs = failmsgs);
   // echo( fails= fails, len_recs=len(recs), len_failmsgs= len(failmsgs) );
    
    //echo("b4 _display_getRecLines, show_test_level= ", show_test_level );
	//-------------------------------------------------------
	function _display_getRecLines( recs=recs,  _i_=0, testi=0 )=
	( 
        let( rec = recs[_i_]
           , iss = dt_isstr( rec )
           )
		_i_>len(recs)-1?[]
		: concat(

                    (
                      ((show_test_level==1) && !arePass[_i_]) 
                      //((show_test_level==1) && failmsgs[_i_]!="") 
                       || show_test_level==2
                    )? [ 
                        iss
                        ? _display_getStrRecLine( rec )
                        : _display_getRecLine( rec, _i_, testi )
                       ]
                    : []
				
				, _display_getRecLines(recs, _i_=_i_+1
                    , testi = testi+ ( iss?0:1)
                                       )
			    )
	);
//	echo( fails= fails, len_recs=len(recs), len_failmsgs= len(failmsgs) 
//         , len__display_getRecLines= len(_display_getRecLines()));
    
	//-------------------------------------------------------
	//-------------------------------------------------------
	// 
	//. START PROCESSING
	// 
	//-------------------------------------------------------
	//-------------------------------------------------------
    failmsgs= is_do_test?_test_do_tests():[]; // [ "", " got:3", "", "notest" ...]
    arePass= [ for(x=failmsgs) x==""?true:false ];
        
    nfail = failmsgs ==[]?0:
            len( [ for(t=failmsgs) if(t!=""&&t!="notest") 1 ] );
    notest_count = len( [ for (f=failmsgs )
                          if(f=="notest")1]);         
    testcount= len(recs)
               -dt_countStr(recs)// Exclude comments/labels
               - notest_count
               ;
    //echo(failmsgs = failmsgs);
    //echo(dt_slice( ["a","b","c"],1));                      
    //echo(arePass=arePass);                      
    //-------------------------------------------------------

    h = _header_getHeader(doc, testcount=is_do_test?testcount:0, nfail=nfail);

    //echo("recs = ", recs );

//    testlines= show_test_level==0?[] : 
//        _display_getRecLines();
      
    //function _display_testsAsDemo()= str( "<br/>", dt_join( testlines, "<br/>") );
    show_both = is_show_doc && show_test_level>0;
    
    content = str(  
    
        // Draw a line to start, when needed
        //( !is_show_doc && show_test_level==0 )?"": _display_startline()
        ( mode== 000 ||mode== 010 || mode==011 )?"": _display_startline()
        
        ,h  // always show header
        
        ,mode==0||mode==10||(mode==011&&nfail==0)? //mode=0,10: no need to show scope,doc,or test
            (html_cmt_beg?"<br/>":"") 
         :_pre(str(
          is_show_doc?
            _doc_getdoc( docdata, tags )
            :""
        ,// dt_index( [101,102,111,112], mode)>=0 ?
        //mode==101 || mode==102 ||mode==111||mode==112?
        , show_both? brpref : ""
        , show_both? _BR: ""
//             str( prefix,"<br/>"):""     
        ,  
//          _pre( str( 
//                 mode==101 || mode==102 ||mode==111||mode==112?
//                 str(prefix, "<br/>"):""
        ,dt_join( concat( 
                         show_test_level==0?[]:_display_getScopeLines()
                        ,_display_getRecLines( recs=recs)
                        )
                 , "<br/>"
                 )  
           ))
          
        );
     
    echo( str( html_cmt_end
             , content, 
             , html_cmt_beg
             ));
    
} //doctest 

  

module doctest_demo(ops=[])
{
  echo( str(html_cmt_end, "<br/>"
           , _b("doctest_demo()", s="font-size:20px")
           , "<br/>"
           , html_cmt_beg?"<br/>":""
           , html_cmt_beg ));
    
  a="abcdef";
  b="abc";
    
  dt_begwith_tests= [
    [ dt_begwith("abcdef","def"), "'abcdef','def'", false ]
  , [ dt_begwith("abcdef","abc"), "'abcdef','abc'",  true ]
  , [ dt_begwith("abcdef","abc"), "'abcdef','abc'",  false, ["//","I`m remark"] ]
  , [ dt_begwith("abcdef", ""),"'abcdef', ''",  undef,["asstr",true,"//","asstr=true"] ]
  , [ dt_begwith(["ab","cd","ef"], "ab"),"['ab','cd','ef'], 'ab'", true]
  , [ dt_begwith([], "ab"),"[], 'ab'", false, ["//","I`m a remark"] ]
  , ""
  , "// The new rec-specific ops, 'myfunc', allows for more flexible rec"
  , "// This is useful when testing of a function has to rely on other funcs."
  , "//"
  , "// [ 0,'', 1, ['myfunc','newfunc']]"
  , "// [ 0,'', 0, ['myfunc',['newfunc','myarg']]"
  , ""
  , [ 0,"", 1, ["myfunc","newfunc", "//", "['myfunc','newfunc']"]]
  , [ 0,"", 0, ["myfunc",["newfunc","myarg"]
               , "//","['myfunc',['newfunc','myarg']]"]]
  , ""
  , "// ops = ['asstr',true]"
  , ""
  , "// Sometimes it`s unable to report correct match due to rounding error."
  , "// The following 2 examples show how it works. They both match 1/3 to"
  , "// 1/3+0.0000001. One fails, the other, (not the * symbol), pass by "
  , "// setting ['asstr', true]"
  , ""
  
  , [ 1/3,"", 1/3+0.0000001, ["myfunc","1/3= "]] 
  , [ 1/3,"", 1/3+0.0000001, ["myfunc","1/3 ", "asstr",true
                             , "//", "['asstr',true]"]] 

  , ""
  , "// ops = ['prec', false]"
  , ""
  , "// You can force it to match with certain precision:" 
  , ""
  , [ 2.823, "", 2.824, ["myfunc", "2.823= "] ]
  , [ 2.823, "", 2.824, ["myfunc", "2.823 "
                       ,"prec",2
                       ,"//", "['prec',2]"
                       ] ]
  , ""
  , "// ops = ['nl',true]"
  , ""
  , [ dt_begwith("", "abc"),"'', 'abc'", false, ["nl",true,"//","I`m a remark"]]
  , [ dt_begwith("", "ab"),"'', 'ab'", true, ["nl",true,"//","I`m a remark"]]
  , [ "a<br/>b<br/>c","", ["a","b"], ["myfunc","newfunc","nl",true, "//", "['myfunc','newfunc']"]]
    
  , ""
  , "// ops = ['notest',true]"
  , ""
  , "// 'notest', which is NOT set in the default ops, can be set to "
  , "// avoid the testing of this rec, no matter what mode doctest is run."
  , [ dt_begwith("abcdef","abc"), "'abcdef','abc'",  "", ["notest",true] ]
  
  , ""
  , "// new default: ops = ['labelhead','#']"
  , ""
  , [ "#_has label head", dt_begwith("abc","a"), "'abc','a'",  true ]
//  
    
    
  ];
  module _header(t){ echo();
        echo(_span(_b(t) //str(t,dt_multi("=",30)))
       , s="font-size:18px;color:darkblue"));}
  
//  echo( doc_original= dt_begwith[4] );
//  echo( doc_t2T= dt_replace(dt_begwith[4], "t","T") );
//  echo( doc_t2T_e2E= dt_replaces(dt_begwith[4], ["t","T","e","E"]) );
//  echo( doc_if2_IF= dt_replace(dt_begwith[4], "if","IF") );
       
  //_header( dt_getModeStr(0));
  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=0
         , scope=["a",a,"b",b]);
  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=10
         , scope=["a",a,"b",b]);

       
  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=2
         , scope=["a",a,"b",b]);
//
////
  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=11
         , scope=["a",a,"b",b]);       

  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=12
         , scope=["a",a,"b",b]);       

//==========================

  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=100
         , scope=["a",a,"b",b]);
       
  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=102
         , scope=["a",a,"b",b]);
//
  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=110
         , scope=["a",a,"b",b]);
////
  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=111
         , scope=["a",a,"b",b]);       

  //echo("<br/>");     
  doctest( dt_begwith, dt_begwith_tests, mode=112
         , scope=["a",a,"b",b]); 
         
 
         
         
//  _header("mode=1:");    
//  doctest( dt_begwith, dt_begwith_tests, ops=["mode",1] );
//  _header("mode=2:");    
//  doctest( dt_begwith, dt_begwith_tests, ops=["mode",2] );
//  _header("mode=3:");    
//  doctest( dt_begwith, dt_begwith_tests, ops=["mode",3] );
//  _header("mode=10:");    
//  doctest( dt_begwith, dt_begwith_tests, ops=["mode",10] );
//  _header("mode=11:");    
//  doctest( dt_begwith, dt_begwith_tests, ops=["mode",11] );
//  _header("mode=12:");    
//  doctest( dt_begwith, dt_begwith_tests, ops=["mode",12] );
//  _header("mode=13:");    
//  doctest( dt_begwith, dt_begwith_tests, ops=["mode",13] );
//    
}
    



module dt_join_speed_test()
{
   runcount = 1000; 
   d= [ 
        "this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
        ,"this is a speed test data for dt_join" 
      ];
    
   module all_at_once()
   {
      a= dt_join( d );
   }    

   module seperate()
   {  
      d1= [for(i=[0:len(d)/2-1]) d[i]];
      d2= [for(i=[len(d)/2-1:len(d)-1]) d[i]];    
      a1= dt_join( d1 );
      a2= dt_join( d2 );
      a = dt_join( [d1,d2] );
   }  
   
   for( i=[0:runcount-1] ) all_at_once();   // 0.365s
//   for( i=[0:runcount-1] ) seperate();    // 0.776s 
       
}    
    
//dt_join_speed_test();

    

TODO=[
"output (echo) well-formatted doc in html for command line op:
  $ openscad myfile.scad -o dump.png 2> tmp.htm"
,"Considering change the use of '2 single quotes' to one single quote"
,"A rec [ args...], is turned to funcname(arg)=???. Make possible for anyword(...)=???." 
,"bug: // Case below produces exact same result, but why test fails ? 
   [ ''pq,ratios=range(0.2,0.2,0.7)'', onlinePts(pq,ratios=range(0.2,0.2,0.7))
                                        , [[2, 1, 0], [4, 1, 0], [6, 1, 0]]] "
//=======================
,"Provide ''nextline'' option for showing up result of test starting from the next line"
,"highlight funcname, too, in a test when failed"
];


doctest_ver=[
["20140930_1", "Add pre (precision) arg. Check function cirfrags() in scadex.dev.scad for examples." ]
,["20140907_1", "When showing rec, make arg red if failed. See _display_getRecLine()" ]
,["20140720_1", "Remove all doc and tests of utility functions and help text to openscad_doctest_help.scad " ]
,["20140719_1", "doctest_help() and help_test done." ]
,["20140718_2", "help_text() almost finished." ]
,["20140718_1", "All tests go well." ]
,["20140717_2", "Rename to openscad_doctest.scad." ]
,["20140717_1", "Rename from scadex_doctest to scad_doctest, in order to migrate to a stand-alone doctest lib (no need for scadex.scad)." ]
,["20140708_2", "in scadex_dev.scad: rename doctest to doctest_old, doctest2 to doctest; MOVE this new doctest to a new file scadex_doctest.scad (this file)." ]
,["20150318_1", "REVERSE the [arg,want...] to [want,arg...]; Remove funcwrap (too difficult to use); Recode _display_getRecLine for easier debug; Rename to doctest_dev.scad" ]
,["20150324_1", "New: dt_replaces(); Re-code and simplified significantly. Still under construct" ]
,["20150325_1", "dt_replace: recode, and remove array and only for string. 3x faster" ]
,["20150325_2", "dt_index: remove arg wrap (no need); dt_slice: remove array feature. Speed: 3.467s (compare:12.336s on 0318_1) " ]
,["20150325_3", "All modes (10 of them) work. Also these new features: newline, showing test mode on header." ]
,["20150326_1", "command line to run doctest_demo()." ]
,["20150326_2", "New rec-specific ops, notest, allows suppression of a test. Changed ops newline=>nl, rem=>//" ]
,["20150327_1", "Bugfixes. Recoding some. Disable dt_keys, etc. Speed improved by 10%. It's now 4x faster then before 3.25."] 
,["20150329_1", "Recode the 'right' part of getRecLine() for easier debug."] 
];

/* profiling test: 2015.3.25

time openscad-nightly doctest_dev.scad -o dump.stl 

real	0m12.336s
user	0m10.973s
sys	0m1.351s

// After recoding dt_replace

real	0m4.389s
user	0m3.883s
sys	0m0.490s

// After recoding dt_slice
real	0m3.467s
user	0m3.227s
sys	0m0.235s


// 20150327_1:

real	0m3.162s
user	0m2.909s
sys	0m0.250s

*/




DOCTEST_VERSION = doctest_ver[len(doctest_ver)-1][0];

echo( str( html_cmt_end
         , _color(
                   str( "<code>||.........  <b>doctest.scad version: "
                      , DOCTEST_VERSION
                      , "</b>  .........||</code>"
                      )
                ,"brown")
         , "<br/>"
         , html_cmt_beg
         ));

//doctest_demo();
if(ISDEMO) doctest_demo();
