// scadx constants

GAPFILLER = 0.0001; // used to extend len to cover gap
ORIGIN = [0,0,0];
$fn    = 12; 
ZERO   = 1e-13;     // used to chk a value that's supposed to be 0: abs(v)<zero
IM     = [[1,0,0],[0,1,0],[0,0,1]]; //Identity matrix (3x3)
PI     = 3.14159265358979323846264338327950;
COLORS = ["red","green","blue","purple","brown","khaki"];   // default colors

A_Z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
a_z = "abcdefghijklmnopqrstuvwxyz";

CUBEFACES=[ [0,1,2,3], [4,5,6,7]
		  , [0,1,5,4], [1,2,6,5]
		  , [2,3,7,6], [3,0,4,7] 
		  ];
          
/*
   Functions available to calc(). Note: to add functions, add name here,
   and add it's action inside calc().
*/
CALC_FUNCS= [
    "abs", "sign", "sin", "cos", "tan", "acos", "asin", "atan", "atan2", "floor", "round", "ceil", "ln"
    //, "len", "let"
    , "log", "pow", "sqrt", "exp", "rands"
    , "min", "max"
    ];

CALC_OPS= ["^","*","/","+","-"];

CALC_BLOCKS = joinarr(concat( [["(",")"]]
                    ,  [for(f=CALC_FUNCS) [ str(f,"("), ")"] ] ));
    

// A common option for shapes
OPS=[ "r" , 0.1
	, "h", 1
	, "w", 1
	, "len",1 
	, "color", undef 
	, "transp", 1
	, "fn", $fn
	, "rotate", 0
	, "rotates", [0,0,0]
	, "translate", [0,0,0]	 
	];
 
scadx_categories=
[
	  "Array"
	, "Doctest"
	, "Geometry"
	, "Hash"
	, "Index"
	, "Inspect"
	, "Math"
	, "Number"
	, "Point"
	, "2D_Object"
	, "3D_Object"
	, "String"
	, "Type"
];