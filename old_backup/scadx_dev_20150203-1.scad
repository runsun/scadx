
/*
scadex -- an OpenScad lib.

	I. language extension 
	II. Function doc and Test Tools 
	III. Shape

Naming convention:

arr : array
str : string
int : integer

pq  : [P,Q]
pqr : [P,Q,R]

v,pt: a vector, a point 
pl  : a plane
pts : many points 

2-pointer: pq, st
3-pointer: pqr, stu
4-pointer: pqrs

Single capital letters: points/vector (P=[2,5,0]);

aPQR: angle of P-Q-R
lPQ : line PQ, or len of line PQ, depending on the context

function: starts with lower case (angle(), hash(...)) 
module for shape: starts with capitals (Line, MarkPts)

*/

//
//  Thanks to PGreenland's TextGenerator:
//  http://www.thingiverse.com/thing:59817
//
// include <../others/TextGenerator.scad>

$fn = 12; 
GAPFILLER = 0.0001; // used to extend len to cover gap
ZERO   = 1e-13;     // used to chk a value that's supposed to be 0: abs(v)<zero
ORIGIN = [0,0,0];
IM     = [[1,0,0],[0,1,0],[0,0,1]]; //Identity matrix (3x3)
PI     = 3.14159265358979323846264338327950;
COLORS = ["red","green","blue","purple","brown","khaki"];   // default colors

CUBEFACES=[ [0,1,2,3], [4,5,6,7]
		  , [0,1,5,4], [1,2,6,5]
		  , [2,3,7,6], [3,0,4,7] 
		  ];

// A common option for shapes
OPS=[ "r" , 0.1
	, "h", 1
	, "w", 1
	, "len",1 
	, "color", undef 
	, "transp", 1
	, "fn", $fn
	, "rotate", 0
	, "rotates", [0,0,0]
	, "translate", [0,0,0]	 
	];
 

scadex_categories=
[
	  "Array"
	, "Doctest"
	, "Geometry"
	, "Hash"
	, "Index"
	, "Inspect"
	, "Math"
	, "Number"
	, "Point"
	, "2D_Object"
	, "3D_Object"
	, "String"
	, "Type"
];

/*
    Module argument design pattern

1. Undef arg with default val

    module obj( ops=[] )
    {
        ops= concat( ops, [ "r", 3 ], OPS );
    }

2. Two or more related SIMPLE args, set it at once or seperate

    module obj( ops=[] )
    {
        ops = concat( ops,
              [ "r", 3
              //, "rP", undef  // don't really need this
              //, "rQ", undef  // don't really need this
              ], OPS );
        function ops(k,df) = hash(ops, k, df);
        r = ops("r");
        rP= ops("rP", r);  // if rP undef, use r
        rQ= ops("rQ", r);  // if rQ undef, use r
        ...
    }
    
3. ONE complex arg

    module obj( ops=[] )
    {
        OPS = concat( ops, OPS);
        function ops(k,df) = hash(ops, k, df);

        df_head = ["r",2];
        _h = ops("head");
        ops_head= concat( ishash(_h)?_h:[], df_head ); 
        ...
    }    
               
4. two or more layers of multiple complex args 
   (example, heads in Arrow. Or Dim2() ):

    ops= concat( ops, 
               [ "r",0.02
               , "heads",true
               , "headP",true
               , "headQ",true
               ], OPS );    
    function ops(k)= hash(ops,k);
    fn = ops("fn");

    df_heads= ["r", 0.5, "len", 1];
    
    ops_headP= getsubops( ops
                , com_keys= [ "color","transp","fn"]
                , subs= ["heads", df_heads, "headP", [] ]
                );
    ops_headQ= getsubops( ops
                , com_keys=[ "color","transp","fn"]
                , subs= ["heads", df_heads, "headQ", [] ]
                );
        
    function ops_headP(k) = hash( ops_headP,k );
    function ops_headQ(k) = hash( ops_headQ,k ); 
 
    NOTE:
        
        For multiple layer sub-units, like:
        
        body - arm - finger
        
        An each approach is to allow setting, for example, fingers like:
        
        obj( ["arm", ["finger", ["count",3]]] )

*/

module ArgumentPatternDemo_simple()
{
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= concat( ops
            , [ "color", "blue" ]  // <== set default here
            , OPS );
        function ops(k,df) = hash(ops,k,df);
        color = ops("color");
        len   = ops("len", 2); // <== or here. Note that len doesn't
                               //     need to be defined in ops
        translate(transl) color( ops("color"), 0.4) 
        cube(x=0.5,y=0.5,z=1 );
    }  
    Obj();    
    Obj( ["color","red"], [0,1,0] );
}    

module ArgumentPatternDemo_multiple_simples()
{
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= concat( ops
            , [ "limlen", 2
              ], OPS );
        function ops(k,df) = hash(ops,k,df);
        lim = ops("limlen");
        arm = ops("armlen",lim);
        leg = ops("leglen",lim); 
        
        //echo("ll, arm,leg", ll, arm, leg);
        translate(transl)
        color( ops("color"), 0.4) { 
            cube( size=[0.5,1,2] );
            translate( [0,1, 1]) cube( size=[0.5,arm,0.5] );
            translate( [0,0.5, -leg]) cube( size=[0.5,0.5,leg] );
        }    
    }  
    Obj();    
    Obj( ["limlen",1, "color","red"], [2,0,0] );
    Obj( ["armlen",0.5, "color","blue"], [4,0,0] );
    LabelPt( [0, 0, 2], "Obj()=> both lims= default 2" );
    LabelPt( [2, 1, 1.5], "Obj([\"limlen\",1])=> both lims=1",["color","red"] );
    LabelPt( [4, 1, 1.5], "Obj([\"armlen\",0.5])=> arm len=0.5", ["color","blue"] ); 
}    

module ArgumentPatternDemo_complex()
{_mdoc("ArgumentPatternDemo_complex","");
  
    module Obj( ops=[], transl=[0,0,0] )
    {
        ops= update( ["arm", true]
                    ,ops);
        function ops(k,df)= hash(ops,k,df);

        df_arm=["len",3];
        
        ops_arm= getsubops( ops
                    , com_keys= ["transp"]
                    , sub_dfs= ["arm", df_arm ]
                    );
        function ops_arm(k,df) = hash(ops_arm,k,df);
        
        echo("ops_arm",ops_arm);
        
        translate(transl) color(ops("color"), ops("transp"))
        cube( size=[0.5,1,2] );
        if ( ops_arm )
        {   translate( transl+[0,1, 1]) 
            color(ops_arm("color"), ops_arm("transp"))
            cube( size=[0.5,ops_arm("len"),0.5] );
        }   
    }  
    Obj();    
    Dim( [ [0,4,1.5], [0,1,1.5], [2,1,1.5]] );
    Obj( ["arm",false], [1.5,0,0] );
    Obj( ["arm",["len",2]], [3,0,0] );
    Obj( ["arm",["color","red"]], [4.5,0,0] );
    Obj( ["color", "green"], [6,0,0] );
    Obj( ["transp", 0.5], [7.5,0,0] );
    
    LabelPt( [0.5,4.2,1.5], "1.Obj()");
    LabelPt( [1.8,3,1.2], "2.Obj([\"arm\",false])");
    LabelPt( [3.6,3.5,1.5], "3.Obj([\"arm\",[\"len\",2]])");
    LabelPt( [4.8,4.2,1.5], "4.Obj([\"arm\",[\"color\",\"red\"]])",["color","red"]);
    LabelPt( [6.5,4.2,1.5], "5.Obj([\"color\",\"green\"])",["color","green"]);
    
    LabelPt( [8,4.2,1.5], "6.Obj([\"transp\",0.5])");
    
    LabelPt( [10, 0, 0], "Note #5,#6: #5 color set body color but not"
    ,["color","blue"]);
    LabelPt( [10, 0, -0.6], "arm color, but #6 set transp of both, "
    ,["color","blue"]);
    LabelPt( [10, 0, -1.2], "because transp is in com_keys "
    ,["color","blue"]);
    
}  
module ArgumentPatternDemo_multiple_complice()
{
  _mdoc("ArgumentPatternDemo_complice"
," 0: armL has transp=0.6 'cos  df_armL=[''transp'',0.6]
;; 1,2,4: no armL 'cos armL= false 
;; 2    : no arms 'cos arms= false
;; 3~7  : both len = 2 'cos arms=[''len'',2]
;; 5,6  : armL ops placed inside arms [] have no effect
;; 7    : armL ops should have one [] by its own 
;; 8    : set color on obj-level doesn't apply to arms but transp,
;;        does 'cos transp is in com_keys but color is not.
;; 9    : transp set on obj-level can be overwritten by that 
;;      : set in armL [].
");
    
    module Obj( ops=[], transl=[0,0,0] )
    {
        df_ops= ["arms", true
               , "armL", true
               , "armR", true
               ];

        _ops= update(df_ops, ops);
        function ops(k,df)= hash(_ops,k,df);

        df_arms=["len",3];
        df_armL=["transp",0.6];
        df_armR=[];
        
        ops_armL= getsubops( 
                      ops  
                    , df_ops = df_ops
                    , com_keys= ["transp"]
                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]
                    );
        ops_armR= getsubops( 
                      ops
                    , df_ops = df_ops
                    , sub_dfs= ["arms", df_arms, "armR", df_armR ]
                    , com_keys= ["transp"]
                    );
        function ops_armL(k,df) = hash(ops_armL,k,df);
        function ops_armR(k,df) = hash(ops_armR,k,df);

        echo("");
        echo("ops", ops);
        echo("df_ops", df_ops);
        echo("ops_armL", ops_armL);
        echo("ops_armR", ops_armR);
        
        translate(transl) color(ops("color"), ops("transp"))
        cube( size=[0.5,1,2] );
        if ( ops_armR )
        {   
            translate( transl+[0,1, 1]) 
            color(ops_armR("color"), ops_armR("transp"))
            cube( size=[0.5,ops_armR("len"),0.5] );
        }    
        if ( ops_armL )
        {    translate( transl+[0,-ops_armL("len"), 1]) 
            color(ops_armL("color"), ops_armL("transp"))
            cube( size=[0.5,ops_armL("len"),0.5] );
            
        }   
    }  
    Obj();    
    //Dim( [ [0,4,1.5], [0,1,1.5], [2,1,1.5]] );
    
    all_ops= [
        ["arms",false]  // 1
        ,["armL",false]  // 2
        ,["arms",["len",2]] // 3
        ,["arms",["len",2], "armL", false] // 4
        ,["arms",["len",2, "armL", false]] // 5
        ,["arms",["len",2, "armL", ["color","red"]]] // 6
        ,["arms",["len",2], "armL", ["color","red", "len",1]] // 7
        ,["color","red", "transp", 0.3] // 8
        ,["transp", 0.5, "armL", ["transp", 1] ] // 9  
        ,["armR",["color", "blue"]]
    ];
    for(i=range(all_ops)){
        Obj( all_ops[i], [1.5*(i+1), 0, 0] );
        LabelPt( [(i+1)*1.5+0.3, 4.3, 1.5], str(i+1, " ops=", all_ops[i]) 
               , ["color", "blue"] );
    }
 
}  
//ArgumentPatternDemo_simple();
//ArgumentPatternDemo_multiple_simples();
//ArgumentPatternDemo_complex();
//ArgumentPatternDemo_multiple_complice();

/*
***********************************************************

	I. Extension

***********************************************************
*/




//========================================================
function p2v(pq)= pq[1]-pq[0];          // Convert a 2-pointer pq to a vector

function dx(pq)= pq[1].x - pq[0].x;  
function dy(pq)= pq[1].y - pq[0].y;
function dz(pq)= pq[1].z - pq[0].z;

function d01(pqr)= norm(pqr[1]-pqr[0]); // dist btw p and q 
function d12(pqr)= norm(pqr[2]-pqr[1]); 
function d02(pqr)= norm(pqr[2]-pqr[0]);

function d01pow2(pqr)= pow(norm(pqr[1]-pqr[0]),2);  // square of dist btw p and q 
function d12pow2(pqr)= pow(norm(pqr[2]-pqr[1]),2);
function d02pow2(pqr)= pow(norm(pqr[2]-pqr[0]),2);

function minx(pq)= min(pq[0][0],pq[1][0]); // smallest x btw p and q 
function miny(pq)= min(pq[0][1],pq[1][1]);
function minz(pq)= min(pq[0][2],pq[1][2]);
function maxx(pq)= max(pq[0][0],pq[1][0]); // largest x btw p and q
function maxy(pq)= max(pq[0][1],pq[1][1]);
function maxz(pq)= max(pq[0][2],pq[1][2]);

function px00(p)=[ p[0],0,0];  // for pt [x,y,z] => [x,0,0]
function p0y0(p)=[ 0, p[1],0 ];
function p00z(p)=[ 0,0, p[2]];
function pxy0(p)=[ p[0],p[1],0]; // for pt [x,y,z] => [x,y,0]
function p0yz(p)=[ 0, p[1],p[2] ];
function px0z(p)=[ p[0],0, p[2]];

function p01(pts)= [pts[0], pts[1]];  // pick pt-0, pt-1 from pts 
function p10(pts)= [pts[1], pts[0]];
function p02(pts)= [pts[0], pts[2]];
function p20(pts)= [pts[2], pts[0]];
function p03(pts)= [pts[0], pts[3]];
function p30(pts)= [pts[3], pts[0]];
function p12(pts)= [pts[1], pts[2]];
function p21(pts)= [pts[2], pts[1]];
function p13(pts)= [pts[1], pts[3]];
function p31(pts)= [pts[3], pts[1]];
function p23(pts)= [pts[2], pts[3]];
function p32(pts)= [pts[3], pts[2]];

function p012(pts)= [pts[0],pts[1],pts[2]];
function p013(pts)= [pts[0],pts[1],pts[3]];
function p021(pts)= [pts[0],pts[2],pts[1]];
function p023(pts)= [pts[0],pts[2],pts[3]];
function p031(pts)= [pts[0],pts[3],pts[1]];
function p032(pts)= [pts[0],pts[3],pts[2]];

function p102(pts)= [pts[1],pts[0],pts[2]];
function p103(pts)= [pts[1],pts[0],pts[3]];
function p120(pts)= [pts[1],pts[2],pts[0]];
function p123(pts)= [pts[1],pts[2],pts[3]];
function p130(pts)= [pts[1],pts[3],pts[0]];
function p132(pts)= [pts[1],pts[3],pts[2]];

function p201(pts)= [pts[2],pts[0],pts[1]];
function p203(pts)= [pts[2],pts[0],pts[3]];
function p210(pts)= [pts[2],pts[1],pts[0]];
function p213(pts)= [pts[2],pts[1],pts[3]];
function p230(pts)= [pts[2],pts[3],pts[0]];
function p231(pts)= [pts[2],pts[3],pts[1]];

function p301(pts)= [pts[3],pts[0],pts[1]];
function p302(pts)= [pts[3],pts[0],pts[2]];
function p310(pts)= [pts[3],pts[1],pts[0]];
function p312(pts)= [pts[3],pts[1],pts[2]];
function p320(pts)= [pts[3],pts[2],pts[0]];
function p321(pts)= [pts[3],pts[2],pts[1]];

function pp2(pts,i,j)= [pts[i], pts[j]];    // pick 2 points 
function pp3(pts,i,j,k)= [pts[i], pts[j], pts[k]];
function pp4(pts,i,j,k,l)= [pts[i], pts[j], pts[k], pts[l]];



//function reset(pqr,[]

function newx(pt,x)= [x, pt.y, pt.z];
function newy(pt,y)= [pt.x, y, pt.z];
function newz(pt,z)= [pt.x, pt.y, z];

function podPt(P,Q,R,S)=[ (Q-P).x * (S-R).x   // product of difference, used in lineCrossPt
					    , (Q-P).y * (S-R).y
					    , (Q-P).z * (S-R).z ];
 
function dot(P,Q)= P*Q; // P*Q is generally easier than dot(P,Q). But if P and Q are complex, 
						// then, for example dot(P-Q,R-S) * dot(P-R,Q-S)  would be easier than
					    //  ((P-Q)*(R-S))*((P-R)*(Q-S))
					    // See it in lineCrossPt()

function P(pqr) = pqr[0];
function Q(pqr) = pqr[1];
function R(pqr) = pqr[2];
function N(pqr,len=1,reverse=false) = normalPt(pqr,len=len,reverse=reverse);
function N2(pqr,len=1,reverse=false) = 
		normalPt( [P(pqr), Q(pqr), N(pqr)],len=len,reverse=reverse);

groupdocs=[
"addx", 
" Given a point or points (pt) and a {x} value ({x}), add {x} to pt.{x}.
;; When pt is a collection of points, add {x} to each pt if {x} is
;; a number, or add {x}[i] to pt[i].{x} if {x} is an array. That is, 
;; by setting {x} as array, we can add different values to each pt[i].{x}.  
;; Note that addz() - unlike addx, addy - expands a 2D pt to 3D.
"
];

function groupdoc(fname, ops, src=groupdocs )=
(
	_h(hash( src, fname), ops)
);

//// core =========================================================
accumulate=[ "accumulate", "arr", "array", "Math" ,
" Given a numeric array [n0,n1,n2 ...], return an array 
;; [ n0, n0+n1, n0+n1+n2 ... ]
"];

function accumulate(arr, i=0)=
(
	[for (i=range(arr)) sum( slice(arr, 0,i+1))]
);

module accumulate_test( ops=["mode",13] )
{
	doctest( accumulate,
	[ ["[3,4,2,5]", accumulate([3,4,2,5]), [3,7,9,14] ]
	],  ops );
}	
//accumulate_test( ["mode",13] );
//doc(accumulate);
//test(accumulate);


//// core ========================================================
addx=[ "addx", "pt,x=0", "array", "Point, Array", groupdoc( "addx", ["x", "x"] ),
];
addy=[ "addy", "pt,y=0", "array", "Point, Array", groupdoc( "addx", ["x", "y"] )
];
addz=[ "addz", "pt,z=0", "array", "Point, Array", groupdoc( "addx", ["x", "z"] )
];

function addx(pt,x=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addx(pt[i], isarr(x)?x[i]:x) ]
	: len(pt)==3
       ? [ pt.x+x,pt.y,pt.z ] 
	   : [ pt.x+x,pt.y]  
);

function addy(pt,y=0,_I_=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addy(pt[i], isarr(y)?y[i]:y) ]
	: len(pt)==3
       ? [ pt.x,pt.y+y,pt.z ] 
	   : [ pt.x,pt.y+y] 
);

function addz(pt,z=0,_I_=0)=
(
	isarr(pt[0]) // pt is a pts (list of points)
	? [for(i=range(pt)) addz(pt[i], isarr(z)?z[i]:z) ]
	: len(pt)==3
       ? [ pt.x,pt.y,pt.z+z ] 
	   : [ pt.x,pt.y,z] 
);

module addx_test( ops=["mode",13] )
{
        pts = [[2,3,4],[5,6,7]];

	doctest( addx,
	[ "// To a point:"
	//, ["[2,3]", addx([2,3]), [2,3]]
	, ["[2,3], 1", addx([2,3],x=1), [3,3]]
	//, ["[2,3,4]", addx([2,3,4]), [2,3,4]]
	, ["[2,3,4], 1", addx([2,3,4],x=1), [3,3,4]]
	, "// To points:"
	//, ["[[2,3],[5,6]]", addx([[2,3],[5,6]]), [[2,3],[5,6]]]
	, ["[[2,3],[5,6]], 1", addx([[2,3],[5,6]],x=1), [[3,3],[6,6]]]
	, ["[[2,3],[5,6]], [8,9]", addx([[2,3],[5,6]],x=[8,9]), [[10,3],[14,6]]]
	//, ["pts", addx(pts), [[2,3,4],[5,6,7]]]
	, ["[[2,3,4],[5,6,7]], 1", addx(pts,x=1), [[3,3,4],[6,6,7]]]
	, ["[[2,3,4],[5,6,7]], [2,3]", addx(pts,x=[2,3]), [[4,3,4],[8,6,7]]]
	]); //, ops, ["pts",pts]);
}
module addy_test( ops=["mode",13] )
{
	pts = [[2,3,4],[5,6,7]];

	doctest( addy,
	[ "// To a point:"
	//, ["[2,3]", addy([2,3]), [2,3]]
	, ["[2,3], 1", addy([2,3],y=1), [2,4]]
	//, ["[2,3,4]", addy([2,3,4]), [2,3,4]]
	, ["[2,3,4], 1", addy([2,3,4],y=1), [2,4,4]]
	, "// To points:"
	//, ["[[2,3],[5,6]]", addy([[2,3],[5,6]]), [[2,3],[5,6]]]
	, ["[[2,3],[5,6]], 1", addy([[2,3],[5,6]],y=1), [[2,4],[5,7]]]
	, ["[[2,3],[5,6]], [8,9]", addy([[2,3],[5,6]],y=[8,9]), [[2,11],[5,15]]]
	//, ["pts", addy(pts), [[2,3,4],[5,6,7]]]
	, ["[[2,3,4],[5,6,7]], 1", addy(pts,y=1), [[2,4,4],[5,7,7]]]
	, ["[[2,3,4],[5,6,7]], [2,3]", addy(pts,y=[2,3]), [[2,5,4],[5,9,7]]]
	], ops, ["pts",pts]);
}
module addz_test( ops=["mode",13] )
{
	pts = [[2,3,4],[5,6,7]];

	doctest( addz,
	[ "// To a point:"
	//, ["[2,3]", addz([2,3]), [2,3,0]]
	, ["[2,3], 1", addz([2,3],z=1), [2,3,1]]
	//, ["[2,3,4]", addz([2,3,4]), [2,3,4]]
	, ["[2,3,4], 1", addz([2,3,4],z=1), [2,3,5]]
	, "// To points:"
	//, ["[[2,3],[5,6]]", addz([[2,3],[5,6]]), [[2,3,0],[5,6,0]]]
	, ["[[2,3],[5,6]], 1", addz([[2,3],[5,6]],z=1), [[2,3,1],[5,6,1]]]
	, ["[[2,3],[5,6]], [8,9]", addz([[2,3],[5,6]],z=[8,9]), [[2,3,8],[5,6,9]]]
	//, ["pts", addz(pts), [[2,3,4],[5,6,7]]]
	, ["[[2,3,4],[5,6,7]], 1", addz(pts,z=1), [[2,3,5],[5,6,8]]]
	, ["[[2,3,4],[5,6,7]], [2,3]", addz(pts,z=[2,3]), [[2,3,6],[5,6,10]]]
	], ops, ["pts",pts]);
}



///// core ========================================================
angle=["angle","pqr","number","Angle,Geometry",
 " Given a 3-pointer (pqr), the angle of P-Q-R. "
];

//Given a 3-pointer (pqr), and optional point (s), return the angle of P-Q-R. If
//;; s given, or DTS where T is the projection of S on PQ (othoPt( PSQ )) and D 
//;; is S's projectrion on pqr plane. 
//;;
//;;                      
//;;        S----------D  R
//;;        |'.        | /|
//;;        |: '.      |/ |
//;;        | :  '.    :  |
//;;        |  :   '. /|  |.'
//;;        |   .    /.| .'
//;;        '.--:---/--T'
//;;          '. : / .'
//;;            ':/.'
//;;      -------Q---------
//;;           .'
//;;          P

/*
         P       \\
         :.      \\
        /: :     \\
       / :  :    \\
      /  :   :   \\
     /   :    :  \\
    Q----D-----R \\

	D= othoPt( qpr )
	sinQ = PD / PQ
                        
                    P  \\
                  .^/  \\
                .^ /:  \\
              .^  / :  \\
            .^   /  :  \\
          .^    /   :  \\
        R------Q    D  \\

    PR^2 > QR^2+PQ^2: 

	angle(pqr) = asin(PD/PQ) = asin( norm(othoPt(qpr)-pqr[0])/norm(pqr[1]-pqr[0]) )

	Also:

	sinA = sqrt( (L-2a)(L-2b)(L-2c) L) /2bc ,  where L = a+b+c 

*/
function angle(pqr)=
 let( P=pqr[0], Q=pqr[1], R=pqr[2] )
(
	acos( (( R-Q)*( P- Q) ) / d01(pqr) / d12(pqr) )
//	S? angle( [ projPt(S, pqr), othoPt([P,S,Q] ), S ] )
//	:acos( (( R-Q)*( P- Q) ) / d01(pqr) / d12(pqr) )
);

module angle_test( ops = ["mode",13] )
{
	p = [1,0,0];
	q = [0,sqrt(3),0];
	r = ORIGIN;

	scope=[ "pqr1", [ p,q,r]
		,  "pqr2", [ p,r,q]
		,  "pqr3", [ r,p,q]
		,  "pqr4", randPts(3)
		,  "pqr5", randPts(3)
		,  "pqr6", randPts(3)
		,  "pqr7", [ORIGIN, [1,0,0], [3,2,0]]
		];
	function scp(k)= hash(scope, k);
	
	doctest( angle,
	[
		["pqr1", angle(scp("pqr1")),30 ]
		,["pqr2", angle(scp("pqr2")),90 ]
		,["pqr3", angle(scp("pqr3")),60 ]
		,["pqr7", angle(scp("pqr7")),135 ]
		,"// pqr4,5,6 are Random 3-pointers (can't be tested): "
		,str(">> angle(pqr4) = ", _fmt( angle( scp("pqr4"))))
		,str(">> angle(pqr5) = ", _fmt( angle( scp("pqr5"))))
		,str(">> angle(pqr6) = ", _fmt( angle( scp("pqr6"))))
	
	], ops, scope
	);

}
//angle_test(); 
//doc(angle);
module angle_demo_s()
{_mdoc("angle_demo_s", 
" P = [1,0,0]
;; Q = ORIGIN
;; R = [0,sqrt(3),0]
");    
    P = [1,0,0];
    Q = ORIGIN;
    R = [0,sqrt(3),0];
	
    pqr = [P,Q,R];
	Chain( pqr );
	MarkPts( pqr, ["labels", "PQR"] );
	echo("angle( pqr )=", angle([P,Q,R]));
	echo("angle( rpq )=", angle([R,P,Q]));
    echo("angle( prq )=", angle([P,R,Q]));
    
//	pqr = randPts(3);
//	P=P(pqr); Q=Q(pqr); R=R(pqr);
//	S = randPt();
//	//echo( projPt(S, pqr));
//	T= othoPt( [P,S,Q] );
//	J= projPt( S, pqr );
//
//	Chain( pqr );
//	MarkPts( pqr, ["labels", "PQR"] );
//	MarkPts([S,T,J],["labels","STJ"] );
//
//	color("red", 0.5) Chain( [P,S,Q], ["r",0.03] );
//	color("green", 0.5) Chain( [S,T,J ], ["r",0.03, "closed",true] );
//	Mark90( [P,T,S] );
//    Mark90( [T,J,S] );
//	Plane( pqr, ["mode",4] );
//	//Plane( [P,Q, normalPt(pqr)], ["mode",4] );
//
//	a = angle( pqr,S );
//	echo("angle( pqr,S )=", a);
//	echo("angle([J,T,S])=", angle([J,T,S]) );
//	scale(0.03) text( str( a ) );

}
// angle_demo_s();



///// core ========================================================
angleBisectPt=["angleBisectPt", "pqr,len=undef,ratio=undef", "point", "Angle,Point,Geometry",
" Given a 3-pointer (pqr), return a point D on line PR such that the
;; line QD is aPQR's angle bisector (divide aPQR evenly).
;;
;;          _.Q
;;        _- /|
;;      _'  / |
;;   _-'   /  |
;;  P-----D---R
;; 
;; New 150125: Add len and ratio arguments
"]; 

function angleBisectPt( pqr, len=undef, ratio=undef )= 
(
    let( Q= pqr[1]
        ,D= onlinePt( p02(pqr) 
            , ratio= norm(pqr[0]-pqr[1])
                    /(norm(pqr[0]-pqr[1])
                     +norm(pqr[1]-pqr[2])) 
                   )
     )
    len!=undef? onlinePt( [Q,D], len=len )
        :ratio!=undef? onlinePt( [Q,D], ratio=ratio)
        :D
);

module angleBisectPt_test( ops=["mode",13] )
{ 

	scope=[ "pqr1", randPts(3)
		 , "pqr2", randPts(3)
		 ];
	pqr1= hash(scope, "pqr1");
	pqr2= hash(scope, "pqr2");

	angle1 = angle( pqr1 );
	angle2 = angle( pqr2 );

	doctest( angleBisectPt, 
	[
		""
		, "We get 2 random 3-pointers, pqr1 and pqr2, to see if the angle is divided evenly"
		,""
		,["pqr1", angle([pqr1[0],pqr1[1]
                 , angleBisectPt( pqr1 )])
                , angle(pqr1)/2, ["funcwrap","angle([p,q,{_}])= angle(pqr1)/2"] ]
		,""
		,"With pqr2:"
		,""
		,["pqr2", angle([pqr2[0],pqr2[1], angleBisectPt( pqr2 )])
				, angle(pqr2)/2
				, ["funcwrap","angle([p,q,{_}])= angle(pqr2)/2"] ]
		,""
		,"Note that this is an example of using other function, angle(), to test the"
		,"targeted one (angleBisectPt), which showcases the power of doctest()."
	]
	, ops,scope
	);

}

module angleBisectPt_demo_1()
{
	echom( "angleBisectPt_demo_1" );
	pqr = randPts(3);
	
	p=pqr[0];
	q=pqr[1];
	r=pqr[2];

	d = angleBisectPt(pqr);

	s=_s("<br/>|>--------&lt;  angleBisectPt_demo_1  >--------
     <br/>|> Get 3 random pts: 
     <br/>|> pqr={_}
     <br/>|> d = angleBisectPt(pqr)= {_}
     <br/>|> Check if two angles divided by QD are equal:
     <br/>|> angle( [p,q,d] ) = {_}
     <br/>|> angle( [r,q,d] ) = {_}
     <br/>|>"
      ,[ _fmt(pqr)
	  , _fmt(angleBisectPt(pqr))
       , _fmt( angle([p,q,d]) )
       , _fmt( angle([r,q,d]) )
       ]
    );
 
	echo(s);

	/*echo_("pqr= {_}", [_fmt(pqr)]);
	echo( "angleBisectPt(pqr)= d =  ", _fmt(angleBisectPt(pqr)  ));	
	echo( "angle([p,q,d]) = ", _fmt( angle([p,q,d]) ));
	echo( "angle([r,q,d]) = ", _fmt( angle([r,q,d]) ));
	*/
}

module angleBisectPt_demo_2(){
	echom("angleBisectPt_demo_2");
    ops= ["r",0.04,"markpts",true];
	
	pqr=[ [0,-1,0 ], [1,2,0 ],[4,0,0 ]];
    pqr = randPts(3);
    Q = pqr[1];
    Chainf( pqr, ops=["closed",true] );
    
    D= angleBisectPt( pqr );
    
    E = angleBisectPt( pqr, ratio=0.5 );
    F = angleBisectPt( pqr, len=5 );

    MarkPts([D,E,F], ["labels","DEF"] );
    Line0( [Q,F], ["r", 0.01] ); 
    
    Dim2( [Q,F, N([ P(pqr),D,F])] ); 
    
}

//angleBisectPt_demo_1();
//angleBisectPt_demo_2();




//==============================================
anglePt=["anglePt","pqr,a,len", "point", "Angle,Point,Geometry",
" Given a 3-pointer (pqr), an angle (a) and a length (len), return the point
;; S that is len away from Q with an angle *a* away from PQ line on the R
;; side of PQ line. It is co-plane with pqr. Refer to *anyAnglePt()* for an 
;; anglePt in any plane.
;;
;;           R     _ S
;;         -'   _-' 
;;       -'  _-'  len
;;     -' _-' 
;;   -'_-'  a
;;  Q-------------------P
"];

function anglePt( pqr, a, len=1)=
(


/*
       B-----------_R-----A 
       |         _:  :    |
       |  T._---:-----:---+ S 
       |  |  '-:_      :  | 
       |  |   :  '-._   : |
       |  |  :     a '-._:| 
       p--+--+-----------'Q
          U  D    


       B----_R-----A--._ 
       |      :    |    ''-_
       |       : S +--------T 
       |        :  |      -'|
       |         : |   _-'  |
       |          :|_-'     |
       p----------'Q--------U

            a = a_PQT
*/
	abs(a)==90? // if a=90, return S
		onlinePt( [ Q(pqr), squarePts( pqr )[2] ]  // [Q,A]
			    , len= sign(a)*len
				)
	:abs(a)==270? // if a=270
		onlinePt( [ Q(pqr), squarePts( pqr )[2] ]  // [Q,A]
			    , len= -sign(a)*len
				)
	:squarePts(
	[ 
		// U
		onlinePt( p10(pqr) // [Q,P] // squarePts( pqr )[2] ]  // [Q,A]
		    , len= len*cos(a)
			)
		// Q	
		, Q(pqr)

		// S
		,onlinePt( [ Q(pqr), squarePts( pqr )[2] ]  // [Q,A]
		    , len= len*sin(a)
			)	
	])[3]

);

module anglePt_test( ops=["mode",13] )
{ 
	doctest( anglePt, [], ops);
}

module anglePt_demo_1()
{ _mdoc("anglePt_demo_1"
,"Make angle 30 and 60"
);
    pqr = randPts();
    P= P(pqr);
    Q= Q(pqr);
    R= R(pqr);
    
    Chain( pqr, ["closed",false,"r",0.02]);
    MarkPts( pqr );
    LabelPts( pqr, "PQR");
    
    A30 = anglePt( pqr, 30, len=1.5);
    A60 = anglePt( pqr, 60, len=2.5);
    
    Chain( [A30, Q, A60], ["closed",false, "r",0.03,"color","green"] );
    
    MarkPts( [A30,A60] );
    LabelPts( [A30,A60], [" A30"," A60"]);
    
	// make a random square, pqab

//	pqab = squarePts(randPts(3));
//	P = pqab[0]; 
//	Q = pqab[1]; 
//	A = pqab[2]; 
//	B = pqab[3];
//	Chain( pqab, ["r",0.01] );
//	MarkPts( pqab );
//	LabelPts( pqab, "PQAB" );
//
//	// Get the R with arbitrary distance between A,B:
//
//	ratio = rands(0,1,1)[0];
//	R = onlinePt( [A,B], ratio=ratio );
//	LabelPt( R, "R", ["color","black"] );
//	pqr = [P,Q,R];	
//	Plane( pqr, ["mode",3] );
//
//	ang = 30; 
//	len = 1.5; 
//	T = onlinePt( [P,B], len= len); //len2>0?sin(a)*len2:len )
//	D = onlinePt( [P,Q], len= atan2(ang)*len); //len2>0?sin(a)*len2:len )
//	
//	S = squarePts( [D,P,T ] )[3];
//		 
//	LabelPts( [T,D,S], "TDS" );
//
//	Plane( [P,D,S,T]);



//	Ap = anglePt( [P,Q,R], ang, len=1.5);
//	echo( "angle DPAp= ", angle( [D,P,Ap] ));
//	MarkPts([Ap]);
//	LabelPt( Ap, ["Ap"] );
//	Line0([P,Ap]);

}

module anglePt_demo_2()
{_mdoc("anglePt_demo_2"
,""
);
	// make a random square, pqab

	pqr = randPts(3);
	as = 6;
	MarkPts( pqr );
	LabelPts( pqr, "PQR" );
	Chain( pqr, ["closed",false] );
	P = pqr[0];
	Q = pqr[1];
	R = pqr[2];

	//echo( "lens = ", lens );

	module plotAngp(a,i)
	{ 
		//echo( "a = ", a );
		p = anglePt( pqr, a=a, r= min( d01(pqr), d02(pqr),d12(pqr)) );
		LabelPt( p, str("P",i));
		Line0( [Q,p], ["r", 0.01]);
		MarkPts( [p] );
	}
	for (i = [0:as-1] ){
		a = 360*i/as;
        //echo("i = ", i, "a= ", a);
		plotAngp( a,i );
	}
//	for (i = [0:as-1] ) assign(a = 360*i/as){
//		//echo("i = ", i, "a= ", a);
//		plotAngp( a,i );
//	}

}
module anglePt_demo_3_90()
{ _mdoc("anglePt_demo_3_90"
," First we made a random right-angle pqr using
;; randRightAnglePts(), then make a S that is on the pqr
;; plane using anglePt(pqr), then using anglePt(pqs, 90) 
;; to make a pt90 that is right angle to line PQ.
"    
);
// NOTE (7/7/2014) :
//
// anglePt(pqr, a=90) sometimes produces [nan,nan,nan]
//
// 4 example cases in which it goes wrong:
//
// ECHO: "pqr", [[2.06056, 0.685988, 6.36888], [3.00336, 1.06282, 2.49988], [2.22362, -0.236707, 2.1833]]
// ECHO: "pqs", [[2.06056, 0.685988, 6.36888], [3.00336, 1.06282, 2.49988], [1.95778, -0.257458, 3.5786]]
//ECHO: "pt90", [nan, nan, nan]

//ECHO: "pqr", [[4.62891, -4.93576, 0.252527], [2.45234, -1.79303, 1.42973], [2.24605, -2.05891, 1.75812]]
//ECHO: "pqs", [[4.62891, -4.93576, 0.252527], [2.45234, -1.79303, 1.42973], [2.60142, -3.70384, 2.00122]]
//ECHO: "pt90", [nan, nan, nan]

//ECHO: "pqr", [[1.15245, -2.62106, 1.36749], [1.01775, -2.65868, -2.63006], [1.05541, -3.91357, -2.61952]]
//ECHO: "pqs", [[1.15245, -2.62106, 1.36749], [1.01775, -2.65868, -2.63006], [1.1078, -4.05891, -1.20484]]
//ECHO: "pt90", [nan, nan, nan]

//ECHO: "pqr", [[-0.394555, 0.856362, -1.03282], [0.291087, -0.669823, 2.60045], [-0.6279, -0.364459, 2.90214]]
//ECHO: "pqs", [[-0.394555, 0.856362, -1.03282], [0.291087, -0.669823, 2.60045], [-1.23265, 0.295526, 1.73654]]
//ECHO: "pt90", [nan, nan, nan]

	echom("anglePt_demo_3_90");
	pqr = randRightAnglePts(r=4); // make a right triangle pqr
	echo("pqr", pqr);
	echo("angle pqr", angle(pqr));

	P = pqr[0];
	Q = pqr[1];
	R = pqr[2];
	S = anglePt(pqr, a=45, r=2); // create a 45 deg pt, S, on pqr plane
		
	echo("pqs", [P,Q,S]);
	pt90 = anglePt( [P,Q,S], a=90, r=2 ); // use pqs as new 3-pointer to get 
									 // a 90-deg pt
	echo("pt90", pt90);
	MarkPts([pt90], ["r",0.1, "transp",0.2]);
	//Line0( [Q, pt90], ["transp",0.2] );

	MarkPts( [P,Q,R,S], ["labels", "PQRS"] );
    LabelPt( pt90, " pt90");
	Line0( [Q,S], ["r", 0.02] );
	Mark90( pqr );
	//Line0([P,Q]); //Chain( pqr );
    Chain( pqr, ["r",0.01, "closed",false]);
}

module anglePt_demo_4_90_debug()
{_mdoc("anglePt_demo_4_90_debug"
," This is a debug version of demo_3_90. It simply checks
;; several cases of pqr in which anglePt(pqr, a=90) returns 
;; [nan,nan,nan]. It should have been fixed by now. 
");
    
// NOTE (7/7/2014) : use the example cases in demo_3 that generate errors:
//
// anglePt(pqr, a=90) sometimes produces [nan,nan,nan]
//
// 4 example cases in which it goes wrong:
//
	pqr1= [[2.06056, 0.685988, 6.36888], [3.00336, 1.06282, 2.49988], [1.95778, -0.257458, 3.5786]];
	pqr2= [[4.62891, -4.93576, 0.252527], [2.45234, -1.79303, 1.42973], [2.24605, -2.05891, 1.75812]];
	pqr3= [[1.15245, -2.62106, 1.36749], [1.01775, -2.65868, -2.63006], [1.05541, -3.91357, -2.61952]];
	pqr4= [[-0.394555, 0.856362, -1.03282], [0.291087, -0.669823, 2.60045], [-1.23265, 0.295526, 1.73654]];

    pt1= anglePt( pqr1, a=90, r=2 );
    pt2= anglePt( pqr2, a=90, r=2 );
    pt3= anglePt( pqr3, a=90, r=2 );
    pt4= anglePt( pqr4, a=90, r=2 );
    
    echo("pqr1=>", pt1);
    echo("pqr2=>", pt2);
    echo("pqr3=>", pt3);
    echo("pqr4=>", pt4);
	
//	//echom("anglePt_demo_4_90_debug");
//	pqr = pqr2;
//	echo("pqr1", pqr);
//	echo("angle pqr1", angle(pqr));
//
//	P = pqr[0];
//	Q = pqr[1];
//	R = pqr[2];
//	S = anglePt(pqr, a=45, r=2); // create a 45 deg pt, S, on pqr plane
//		
//	echo("pqs", [P,Q,S]);
//	echo("angle pqs", angle([P,Q,S]));
//
//	pt90 = anglePt( [P,Q,S], a=90, r=2 ); // use pqs as new 3-pointer to get 
//									 // a 90-deg pt
//	echo("pt90", pt90);
//	MarkPts([pt90], ["r",0.1, "transp",0.2]);
//	Line0( [Q, pt90], ["transp",0.2] );
//
//	MarkPts( [P,Q,R,S], ["labels", "PQRS"] );
//	Line0( [Q,S], ["r", 0.02] );
//	Mark90( pqr );
//	Line0( [P,Q] ); //Chain( pqr );

	// Check the code in anglePt() that is sent to squarePts:
	//
	//	squarePts(
	//	[ 
	//		// U
	//		onlinePt( p10(pqr) // [Q,P] // squarePts( pqr )[2] ]  // [Q,A]
	//		    , len= r*cos(a)
	//			)
	//		// Q	
	//		, pqr[1]
	//
	//		// S
	//		,onlinePt( [ pqr[1], squarePts( pqr )[2] ]  // [Q,A]
	//		    , len= r*sin(a)
	//			)	
	//	])[3]

//	U = onlinePt( p10([P,Q,S]) , len= 2*cos(90) );
//	Q2= [P,Q,S][1];
//	S2= onlinePt( [ pqr[1], squarePts( [P,Q,S] )[2] ], len= 2*sin(90));
//
//	echo("[U,Q2,S2]", [U,Q2,S2]);
//	echo("squarePts([U,Q2,S2])", squarePts([U,Q2,S2]));


}

module anglePt_demo()
{
	anglePt_demo_1();
	//anglePt_demo_2();
	//anglePt_demo_3_90();
	//anglePt_demo_4_90_debug();
}
//anglePt_demo();




//==============================================
anyAnglePt=["anyAnglePt","pqr,a,len=1,pl=undef", "point", "Angle,Point,Geometry",
" Given 3-pointer (pqr), angle (a), length (len) and plane (pl), return a point 
;; that is *len* away from Q at an angle *a*. The pl decides on what plane the angle
;; is formed. Referring to the following graph, where N is the normalPt of pqr, and 
;; M is the normalPt of NQP:
;;
;;   pl= undef (default): aRQN
;;   pl= xy,pm or pr: aPQR or aPQM ( = anglePt(pqr) )
;;   pl= yz,mn: aMQN
;;   pl= xz,pn: aNQP
;;
;; plane= pn or xz
;; a = a3
;; dir: aPQN  (z)
;;             N      a2 .--
;;        --.  |         |'-_      plane= mn or yz
;;     a3 -'|  |_________    '-_   a = a2
;;      -'    /| '-_     |         dir: aMQN
;;           / |    '-_  |
;;          |  |       : |
;;          |  Q-------|---------------------------- M (y)
;;          | / '-._/  |     .--     plane = undef
;;          |/_____/'-.|     |'.     a=a0
;;          /           '-._    '.   dir: aRQN
;;         /      ._        '-._  a0
;;        /    ------>  a1      '-._
;;       /        -'                '-._
;;      /                               R
;;     /  plane = xy, pm or pr
;;    P   a = a1
;;   (x)  dir: aPQM
"];

function anyAnglePt( pqr, a, len=1, pl=undef)=  // pqn, pqr, qnn, qrn
(
	anglePt
	( 
	   pl=="yz"||pl=="mn"?          [ N2(pqr,len=-1), Q(pqr), N(pqr) ]
	 : pl=="xy"||pl=="pm"||pl=="pr"? pqr //[ P(pqr), Q(pqr), N2(pqr,reverse=true) ]
	 : pl=="xz"||pl=="pn"?          [ P(pqr), Q(pqr), N(pqr) ]
	 :                              [ R(pqr), Q(pqr), N(pqr) ]
	, a=a, len=len
	)
);


module anyAnglePt_test( ops=["mode",13] )
{ 
	doctest( anyAnglePt, 
	[], ops
	);

}
module anyAnglePt_demo_1()
{_mdoc("anyAnglePt_demo_1"
," pqr: random
;; N,M: normal to pqr
;; rn : anyAnglePt( pqr, a=30, len=L)
;; xy : anyAnglePt( pqr, a=30, len=L, pl=''xy'')
;; xz : anyAnglePt( pqr, a=30, len=L, pl=''xz'')
;; yz : anyAnglePt( pqr, a=30, len=L, pl=''yz'')     
");
    
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "color","red" ] );
	Line0([Q,N],["color","green"]);
	Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	L = norm(R-Q)/2;

	pt_rn =anyAnglePt( pqr, a=30, len=L);
	pt_xy =anyAnglePt( pqr, a=30, len=L, pl="xy");
	pt_xz =anyAnglePt( pqr, a=30, len=L, pl="xz");
	pt_yz =anyAnglePt( pqr, a=30, len=L, pl="yz");

	Line0( [Q, pt_rn], ["r",0.01,"color","red"] );
	Line0( [Q, pt_xy], ["r",0.01,"color","green"] );
	Line0( [Q, pt_xz], ["r",0.01,"color","blue"] );
	Line0( [Q, pt_yz], ["r",0.01,"color","purple"] );

	Line( [ projPt(pt_rn, pqr), pt_rn],["head",["style","cone"]] );
	Line( [ projPt(pt_xy, [P,Q,N]), pt_xy],["head",["style","cone"]] );
	Line( [ projPt(pt_xz, pqr), pt_xz],["head",["style","cone"]] );
	Line( [ projPt(pt_yz, pqr), pt_yz],["head",["style","cone"]] );
    
//	Line( [ onlinePt([Q,R], len=norm(pt_rn-Q)), pt_rn],["head",["style","cone"]] );
//	Line( [ onlinePt([Q,P], len=norm(pt_xy-Q)), pt_xy],["head",["style","cone"]] );
//	Line( [ onlinePt([Q,P], len=norm(pt_xz-Q)), pt_xz],["head",["style","cone"]] );
//	Line( [ onlinePt([Q,M], len=norm(pt_yz-Q)), pt_yz],["head",["style","cone"]] );


	MarkPts( [pt_rn, pt_xy, pt_xz, pt_yz]
		,["labels", ["rn", "xy","xz","yz"]] );
	

}


module anyAnglePt_demo_2()
{
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "color","red" ] );
	Line0([Q,N],["color","green"]);
	Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	function getpts(count=30, a=rand(360), len=1, pl="yz")=
	(

		count>0? concat( [anyAnglePt( pqr,a=a,len=len,pl=pl)]
					, getpts( count-1, a=rand(360), len=len, pl=pl )
					):[]
	);


	pts_xy = getpts(pl="xy", len=1);
	MarkPts( pts_xy, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );

	pts_yz = getpts(40, pl="yz", len=1.5);
	//echo("pts_yz", pts_yz);
	MarkPts( pts_yz, ["colors",["green"], "samesize",true
				, "sametransp",true, "r",0.05] );


	pts_xz = getpts(50, pl="xz", len=2);
	MarkPts( pts_xz, ["colors",["blue"], "samesize",true
				, "sametransp",true, "r",0.05] );

}

module anyAnglePt_demo_3()
{
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "color","red" ] );
	Line0([Q,N],["color","green"]);
	Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );


	function getpts(count=30,  len=1, pl="yz",_I_=0)=
	(
		_I_<count? concat( [anyAnglePt( pqr,a= _I_*360/count,len=len,pl=pl)]
					, getpts( count, len=len, pl=pl,_I_=_I_+1 )
					):[]
	);

 
	pts_xy = getpts(pl="xy", len=1);
	MarkPts( pts_xy, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts_xy, ["r",0.01, "color","red"] );

	pts_yz = getpts(30, pl="yz", len=1.5);
	//echo("pts_yz", pts_yz);
	MarkPts( pts_yz, ["colors",["green"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts_yz, ["r",0.01, "color","green"] );


	pts_xz = getpts(30, pl="xz", len=2);
	MarkPts( pts_xz, ["colors",["blue"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts_xz, ["r",0.01, "color","blue"] );
}

module anyAnglePt_demo_4()
{
	echom("anyAnglePt_demo_4");
	pqr = randPts(3);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "color","red" ] );
	Line0([Q,N],["color","green"]);
	Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	function getpts(count=30,  len=1,_I_=0)=
	(
		_I_<count? concat( [anyAnglePt( pqr,a= _I_*360/count,len=len)]
					, getpts( count, len=len, pl=pl,_I_=_I_+1 )
					):[]
	);

 
	pts = getpts(len=dist([R,Q]));
	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts, ["r",0.01, "color","red"] );
	for( i = range( 30) ) Line0( [Q, pts[i]], ["r",0.01, "transp",0.4]);

}

module anyAnglePt_demo_5()
{_mdoc("anyAnglePt_demo_5"
," Follow a sine wave.
");
    
	echom("anyAnglePt_demo_5");
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false] );
	//Line0([Q,N],["color","green"]);
	//Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	count=36;
	function getSinePts( count=count, len=3, _I_=0)=
	(
		_I_<count? concat( 
			[
				//360/count*_I_, len*sin(360/count*_I_), 
				anyAnglePt( [P,Q, anglePt(pqr, 360/count*_I_)]
						,a=360/count*_I_
                        , len= len*sin(360/count*_I_))]
					, getSinePts( count, len=len, _I_=_I_+1 )
		):[]
	);


	pts = getSinePts();
	//echo("pts",pts);

	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts, ["r",0.01, "color","red"] );

	for( i = range( count) ) 
        Line0( [Q, pts[i]], ["r",0.01, "transp",0.4]);

}

//anyAnglePt_demo_1();
//anyAnglePt_demo_2();
//anyAnglePt_demo_3();
//anyAnglePt_demo_4();
//anyAnglePt_demo_5();


// =========================================================
arcPts=["arcPts", "pqr, rad=2, a, count=6, pl=''xy'', cycle, pitch=0", "points", "Point,Geometry",
" Given a 3-pointer(pqr), an angle(a), radius(rad), count of points from 
;; 0 ~ a (when cycle is not given) or count of points per cycle ( when
;; cycle is given), and optional cycle and pitch, return an array of 3d 
;; points for a curve spanning angle 0~a. Note that count 6= 6 points.
;; 
;; Since 1 cycle = 360 degrees, if cycle is given, the angle a is meaningless,
;; so will be ignored.
;;
;; The points will span a curve originating from the point Q, and on a 
;; plane defined by pl (see anyAnglePt doc for definition). 
;; 
;; If *pitch* is given, which defines the distant of *pitch* of a spiral, 
;; then a spiral will be drawn along the PQ line (i.e., the arc be drawn
;; on the yz plane => pl=''yz''), starting from Q to P. A negative pitch 
;; produces a spiral going P-to-Q direction starting from Q. 
;;
;; Note that the spiral arc feature is available only along the PQ line. 
;; 
 
"];
function arcPts( 
    pqr=randPts(3)
    , a=undef
    , r=1
    , count=6
    , pl="xy"
    , cycle=undef
    , twist = 0 
    , pitch=0)=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, _a= a==undef?angle(pqr):a
	, a = cycle?cycle*360:_a 
	, pl= pitch?"yz":pl 
	)
(
	[ for(i=range(count)) 
		anyAnglePt( [ P
					, onlinePt([Q,P],len= i*pitch/count)
					, R
				 	] , a= twist+ i*a/(count-(a==360?0:1))
				  , len=r, pl=pl) ]
);

module arcPts_test( ops=["mode",13] )
{
	doctest( arcPts, ops=ops );
}

module arcPts_demo_1()
{_mdoc("arcPts_demo_1",
" arcPts( pqr=randPts(3), a=90, r=1, count=6, pl=''xy'', cycle, pitch=0)
;; -- random pqr
;; -- red  : arcPts( pqr )
;; -- green: arcPts( pqr, r=2, a=150, count=10 ) 
;; -- blue : arcPts( pqr, r=3, a=360, count=12, pl=''yz'' )
");
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["closed",false, "transp",0.4] );
	//Line0([Q,N],["color","green"]);
	//Line0([Q,M],["color","blue"]);

 	Mark90( [P,Q,N]);
	Mark90( [P,Q,M] );
	Mark90( [N,Q,M] );
	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	//----------------------------------
	pts = arcPts( pqr );
	echo("pts",pts);
	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts, ["r",0.01, "color","red","closed",false] );
	for( i = range( 6) ) Line0( [Q, pts[i]], ["r",0.01, "transp",0.4]);	


	//----------------------------------
	pts2= arcPts( pqr, r=2, a=150, count=10, pl="xz" );
	//echo("pts",pts);
	MarkPts( pts2, ["colors",["green"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts2, ["r",0.01, "color","green", "closed",false] );
	for( i = range( 10) ) Line0( [Q, pts2[i]], ["r",0.01, "transp",0.4]);	
	LabelPts( pts2, range(pts2) );


	//----------------------------------
	pts3= arcPts( pqr, r=3, a=360, count=12, pl="yz" );
	//echo("pts",pts);
	MarkPts( pts3, ["colors",["blue"], "samesize",true
				, "sametransp",true, "r",0.05] );
	Chain( pts3, ["r",0.01, "color","blue"] );
	for( i = range( 12 ) ) Line0( [Q, pts3[i]], ["r",0.01, "transp",0.4]);	
	LabelPts( pts3, range(pts3) );
}

module arcPts_demo_2()
{_mdoc("arcPts_demo_2",
"Use the cycle and pitch to make a spiral arc:
;;
;; arcPts( pqr, r=2, count=count, pl=''yz'', cycle=3, pitch= 2)
;;
;; pitch: distance between 2 cycles
");
	echom("arcPts_demo_2");
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= N(pqr); M=N2(pqr, len=-1);

	Chain(pqr, ["r",0.1,"transp",0.4, "closed",false] );
	Line0([Q,N],["color","green","r",0.06, "transp",0.2]);
	Line0([Q,M],["color","blue","r",0.06, "transp",0.2]);

// 	Mark90( [P,Q,N]);
//	Mark90( [P,Q,M] );
//	Mark90( [N,Q,M] );
//	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	MarkPts([ P,Q,R, N, M], ["labels", "PQRNM"] );

	// We are gonna make a spiral here. 
	cycles = 5; 
	pts_per_cycle   = 12;  // Each cycle has ? pts.
	pitch= 3;    // The distance between the start of this cycle
                           // to the next one
    //shift_per_pt = pitch / pts_per_cycle;
	
	//pts = [ for (c=range(cycles))
			
	 
	count = 36;
	pts = arcPts( pqr, r=2, count=count, pl="yz", cycle=3, pitch= 3);
	//echo("pts",pts);
	MarkPts( pts, ["colors",["red"], "samesize",true
				, "sametransp",true, "r",0.05, "transp",0.5] );
	Chain( pts, ["r",0.01, "color","red", "transp", 0.5] );
	for( i = range( count) ) 
		Line0( [ pts[i], othoPt( [P, pts[i], Q]) ]
			, ["r",0.03, "transp",0.8]);	

}


module arcPts_demo_3_twist()
{_mdoc("arcPts_demo_3_twist",
" 
");
    
	pqr = randPts(3, r=4);
	P= P(pqr); Q=Q(pqr); R=R(pqr);
	N= onlinePt([ Q, N(pqr)], len=4); 
    M= N2(pqr, len=-4);

// 	Mark90( [P,Q,N]);
//	Mark90( [P,Q,M] );
//	Mark90( [N,Q,M] );
//	Mark90( [N,Q,R] );
	//echo( "aPQM = ", angle([P,Q,M]));
	Chain( [N,Q,M], ["r",0.01,"closed",false] );
    MarkPts( [N,M], ["labels","NM"]);
    Chainf(pqr);

    module fmtchain( pts, color, labelbeg=0)
    {
        MarkPts( pts, ["colors",[color], "samesize",true
				, "sametransp",true, "r",0.05] );
        Chain( pts, ["r",0.01, "color",color,"closed",false] );
        for( i = range(pts) ) Line0( [Q, pts[i]]
            , ["r",0.01, "transp",0.4]);	
        if( labelbeg=="P" )
        {LabelPts( pts, "PQRSTUVWXYZ", ops=["scale", 0.02] ); }
        if( labelbeg==0 )
        {LabelPts( pts, range(pts), ops=["scale", 0.02] ); }
        
    };    
	//----------------------------------
	pts = arcPts( pqr, a=90, r=2, count=4 );
	fmtchain(pts);
    
	pts2 = arcPts( pqr, a=90, r=2.5, count=4, twist=10 );
	fmtchain(pts2, "red");

    
//    MarkPts( pts, ["colors",["red"], "samesize",true
//				, "sametransp",true, "r",0.05] );
//	Chain( pts, ["r",0.01, "color","red","closed",false] );
//	for( i = range( 6) ) Line0( [Q, pts[i]], ["r",0.01, "transp",0.4]);	

}

//arcPts_demo_1();
//arcPts_demo_2();
//arcPts_demo_3_twist();





//// Shape ========================================================
Arc=[[],
"

 angle not given:  draw arc:RS

                     _R 
                   _:  :
                  :     : 
                 -       :
                :         : 
               -           : 
       p ------S-----------'Q


 angle given: draw arc:DS

                     _R 
                   _:  :
                D :     : 
                 -'-_    :
                :    '-_  : 
               -     a  '-_: 
       p ------S-----------'Q


	 
"];


//function deArcPts( a, r )=
//(
//	between(0,a,45, include=0) || between(-360,a,-315, include=0)
//	?[ [0,0], [r,0], [r,-r], [-r,-r], [-r,r],[r,r], [r, sin(a)*r] ]
//	:between(45,a,135, include=[1,0]) || between(-315,a,-225, include=[1,0])
//	 ?[ [0,0], [r,0], [r,-r], [-r,-r], [-r,r], [r, sin(a)*r] ]
//	 :between(0,a,45, include=[1,0]) || between(-360,a,-315, include=[1,0])
//	  ?[ [0,0], [r,0], [r,-r], [-r,-r],  [r, sin(a)*r] ]
//	  :between(0,a,45, include=[1,0]) || between(-360,a,-315, include=[1,0])
//	   ?[ [0,0], [r,0], [r,-r],  [r, sin(a)*r] ]
//	   :[ [0,0], [r,0], [r, sin(a)*r] ]
//);
//
//module DeArcShape( a, r )
//{
//	pts = deArcPts( 60, 1 );
//	echo(pts);
//	echo(range(len(pts)));
//	polygon( points=pts, paths = [range( len(pts) )] );
//
//
//}
//
//
//module DeArcShape_demo()
//{
//	DeArcShape(30);
//}
//DeArcShape_demo();

                           
//polygon( points= [ [0,0], [2,0], [2,-2], [-2,-2], [-2,0], [-2,2], [2,2], [2,sin(30)*2]]
////	   , paths=[ [1,2,3],[1,3,4],[0,4,5],[0,5,7],[5,6,7] ]
////	   , paths=[ [0,1,2],[0,2,3],[0,4,5],[0,5,7],[0,6,7] ]
//		,paths=[[0,1,2],[2,3,5],[5,7,0],[5,6,7]]
//
//);

//polygon( points= [ [0,0], [2,0], [2,-2], [-2,-2], [-2,0], [-2,2], [2,2], [2,sin(30)*2]]
////	   , paths=[ [1,2,3],[1,3,4],[0,4,5],[0,5,7],[5,6,7] ]
//	   , paths=[ [0,1,2,3,4,5,6,7] ]
////	   , paths=[ [0,1,2],[0,2,3],[0,4,5],[0,5,7],[0,6,7] ]
////		,paths=[[0,1,2],[2,3,5],[5,7,0],[5,6,7]]

//);

module Arc(pqr,ops=[]){

    //_mdoc("Arc","");
                     
	ops= concat( ops
    , [ "rad", d01(pqr)  // rad of the curve this obj follows. 
      , "r", 0.2         // Set rad of crossection on P and R ends at once
      , "rP", undef      // rad of the circle at P
	  , "rR", undef      // rad of the circle at R  
      , "a", angle(pqr)  // angle determining cone length
                         // if not given, use a_pqr
      , "slice", $fn==0?6:$fn //# of slices 
      , "fn", $fn==0?6:$fn    //# of points along bottom circle
      , "debug", false   // set to true to show pt indices
      ], OPS);

    function ops(k,df)=hash(ops,k,df);

    rad=ops("rad");
    r = ops("r");
    rP= ops("rP", r);
    rR= ops("rR", r);
    sl= ops("slice");
    fn=ops("fn");
    //echo("rP,rR", rP, rR);
    
    a = ops("a");
    P = pqr[0]; Q=pqr[1]; 
    
    R= anglePt(pqr, a, len=r);  // The end pt of arc

    pqr = [P,Q,R];
	
    function getPtsBySlice(i)= 
    (  let( 
             X = anglePt( pqr   // X is a point along the P->R curve
                  , a= i*a/sl
                  , len=rad )
           , X2= anglePt( pqr   // X2 is a little bit further down the arc
                  , a= i*a/sl+1
                  , len=rad )
           ) 
        arcPts( [Q,X, N([Q,X,X2])]
                    , pl="xy"
                    , a=360
                    , count= fn
                    , r= rP + i*(rR-rP) /fn
                    )
    );
    
    // pts: [ [p10,p11,p12...p16]  <== layer 1
    //      , [p20,p21,p22...p26]
    //      ...
    //      , [pi0,pi1,pi2...pi6] ] <==layer i    
    pts_layers = [ for( i = range(sl+1) )
                   getPtsBySlice(i) ];
    
    pts = [ for(a=pts_layers, b=a) b] ; 

    // This for loop is for debug only. It displays
    // lines thru circling pts of each layer, and also
    // label pts
    if( ops("debug")){
        MarkPts( pts, ["samesize",true, "r", 0.01] );
        LabelPts( pts );    
    }
    
    faces = faces("chain", sides=fn, count= sl);      

    color( ops("color"), ops("transp") )    
    polyhedron( points= pts
              , faces = faces
              );              
}    

module Arc_demo_1()
{ _mdoc("Arc_demo_1","");
    
	pqr = randPts(3);
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
	Chainf( pqr );
	LabelPt( midPt( p01(pqr)), " Arc(pqr)" ); 
    Arc(pqr); //, ["angle", 70]);
	
    pqr2= randPts(3); 
    Chainf(pqr2, ops=["color","red"] );
    Arc(pqr2, ["rad",3,"color","red"] );
    Dim2( [Q(pqr2), onlinePt( p10(pqr2), len=3), R(pqr2)]
        , ["color","red"] );
    LabelPt( midPt( p02(pqr2) )
    , "Arc(pqr,[\"rad\",3,\"color\",\"red\"])", ["color","red"]);    
}

module Arc_demo_2()
{ _mdoc("Arc","");
    
	pqr = randPts(3);
    echo("pqr", pqr);
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
	Chainf( pqr ); 
        
    Arc(pqr);
	
    Arc(pqr, ["r",0.1, "rad",3, "slice",4, "color","red"]); 
    R2 = onlinePt( [Q,P], len=3);
    Dim2( [ Q, R2, R ],["color","red"] );
    LabelPt( angleBisectPt(pqr, len=1.5) , 
    " [\"r\",0.1,\"rad\",3,\"slice\",4,\"color\",\"red\"]"
    ,["color","red"] ); 
    
	Arc(pqr, ["rP",0.1, "rR", 0.8, "rad",6, "color","blue"]); 
    LabelPt( angleBisectPt(pqr, len=7) , 
    " [\"rP\",0.1,\"rR\",0.8,\"rad\",6,\"color\",\"blue\"]"
    ,["color","blue"] ); 
 	

}

module Arc_demo_3_debug()
{ _mdoc("Arc_demo_3_debug","");
    
	pqr = randPts(3);
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
	Chainf( pqr );
//	LabelPt( Q,
//        , "Arc(pqr,[\"debug\",true,\"slice\",4,\"fn\",6, \"rad\",2,\"r\",1])" ); 
    Arc(pqr, ["debug",true, "a",90, "slice",4
    ,"rad",5,"fn",6,"r",0.7, "transp", 0.7]); 
}

module Arc_demo_4_angle()
{ _mdoc("Arc_demo_4_angle","");
    
	pqr = randPts(3);
    P=pqr[0]; Q=pqr[1]; R=pqr[2];
	Chainf( pqr );

    M = angleBisectPt( pqr, len = 4.5 );
    Line0( [Q,M], ["r", 0.01]);
    
    Arc(pqr, ["rad",2]);
   
    Arc(pqr, ["a",30, "rad",2.5, "color","red" ]);
    Arc(pqr, ["a",60, "rad",3, "color","green" ]);
    Arc(pqr, ["a",90, "rad",3.5, "color","blue" ]);
    
   LabelPt(Q, "   a=30(red),60(green),90(blue)");
    
}
//Arc_demo_1();
//Arc_demo_2();
//Arc_demo_3_debug();
//Arc_demo_4_angle();

//angle_test(["mode",13]);



//// core ========================================================
arrblock=[ "arrblock","arr,indent=''  
'',v=''| '',t=''-'',b=''-'',lt=''.'',lb=\"'\"","array","Array, Doc",
 " Given an array of strings, reformat each line
;; for echoing a string block . Arguments:
;;  indent: prefix each line; 
;;  v : vertical left edge, default '| '
;;  t : horizontal line on top, default '-'
;;  b : horizontal line on bottom, default '-'
;;  lt: left-top symbol, default '.'
;;  lb: left-bottom symbol, default '
"
];

function arrblock(arr, indent="  "
					, v="| "
					, t="-"
					, b="-"
					, lt="."
					, lb="'"
					, lnum=undef
					, lnumdot=". "
					 
)=// _i_=-1)=
(
  concat( [ str( indent, lt,repeat(t, 45) ) ]
		, isint(lnum)
		  ? [ for( i=range(arr) ) str( indent, v, i+lnum, lnumdot, arr[i]) ]
		  : [ for( i=range(arr) ) str( indent, v,                , arr[i]) ]
		, [ str( indent, lb,repeat(b, 45) ) ]
  ) 
//	_i_==-1?concat( t?
//					[ str( indent, lt,repeat(t, 45) )						 
//					]:[]
//				  ,	arrblock( arr, indent=indent,v=v,t=t,b=b ,lt=lt,lb=lb, _i_=0 )
//				  )
//	  :_i_==len(arr)? (b? [str(indent, lb, repeat(b, 45) )	]:[])
//	    : concat( [str( indent, v, arr[_i_] )]
//				, arrblock( arr, indent=indent,v=v,t=t,b=b ,lt=lt,lb=lb, _i_=_i_+1)
//				)
);

module arrblock_test(ops)
{

	doctest
	(
		arrblock
		,[
			["[''abc'',''def'']", join(arrblock(["abc","def"]),"<br/>")
			, join( [ "  .---------------------------------------------"
			  		, "  | abc"
			  		, "  | def"
			  		, "  '---------------------------------------------"
					], "<br/>" ) 
			  		
			, ["funcwrap", "join({_}, ''&lt;br/&gt;'')" ]
			]

		,	["[''abc'',''def'', ''ghi'']"
			, join(arrblock(["abc","def","ghi"], lnum=1),"<br/>")
             , join( [ "  .---------------------------------------------"
			  		, "  | 1. abc"
			  		, "  | 2. def"
			  		, "  | 3. ghi"
			  		, "  '---------------------------------------------"
					], "<br/>" ) 
			  		
			, ["funcwrap", "join({_}, ''&lt;br/&gt;'')" ]
			]
		 ]
		,ops
	);
}
//for (line=arrblock(["abc","def"])) echo(line);
//arrblock_test( ["mode", 22] );
//doc(arrblock);

//// core ========================================================
arrItcp=["arrItcp", "a1,a2", "array", "Array, Math",
 " Given two arrays, return an array containing
;; items showing up in both.
;; 
;; ref: hasAny(o1,o2): return true if o1,o2 (string or array) intersect.
"];

function arrItcp(a1,a2)=
(
	[for(a=a1) for(b=a2) if(a==b) a]
);


module arrItcp_test(ops)
{
	doctest
	(
		arrItcp
	   	,[
			[ "[1,2,3,4,5],[2,4,6]", arrItcp([1,2,3,4,5],[2,4,6]), [2,4]]
		]
		,ops
	);
}

//arrItcp_test( ["mode",22] );
//doc(arrItcp);

// ========================================================
Arrow=["Arrow","pq, ops", "n/a", "geometry"
, "



"];

module Arrow(pq, ops=[])
{
    //_mdoc("Arrow","");
    
    df_fn = $fn==0?6:$fn;    
    com_keys= [ "color","transp","fn"];
    
    df_ops=[ "r",0.02
           , "fn", df_fn
           , "heads",true
           , "headP",true
           , "headQ",true
           , "debug",false
           , "twist", 0
           ];
    
    _ops= update( df_ops, ops );    
       
    function ops(k)= hash(_ops,k);
    fn = ops("fn");
    r  = ops("r");
    L  = d01(pq);
    
    //======================================== arrow
    // Decides arrow r and len automatically (based on line r)
    // when they are not set by user:
    arr_l = min(10*exp(-0.5*r)*r, 2*L/5);    
                                // 6*r, but can't be too large 
    arr_r = (3*exp(-0.8*r)) *r; // Must be >r, but with certain ratio
                                // that is large ( 3x ) @ small r but
                                // smaller @ large r. So we use a 
                                // exponential function 
    df_heads= ["r", arr_r, "len", arr_l, "fn", fn];
    df_headP= [];
    df_headQ= [];
    
    /*
        _-'|          |'-_ 
      P----A----------B----Q
        '-_|          |_-'  
           '          '
    */
    
    // ops_arrow 
    
    u_hs = ops("heads");  // user input
    u_hp = ops("headP");  // user input
    u_hq = ops("headQ");  // user input
    
//    fmt = [ "color", ops("color")    // The 
//          , "transp", ops("transp") 
//          , "fn", ops("fn")];
//    ops_arrowP =  concat( isarr(ap)?ap:[], isarr(arw)?arw:[], fmt, df_arrowP);
//    ops_arrowQ =  concat( isarr(aq)?aq:[], isarr(arw)?arw:[], fmt, df_arrowQ);
//
    ops_headP= getsubops( ops, df_ops=df_ops
                , com_keys= com_keys
                , sub_dfs= ["heads", df_heads, "headP", df_headP ]
                );
                
    ops_headQ= getsubops( ops, df_ops=df_ops
                , com_keys= com_keys
                , sub_dfs= ["heads", df_heads, "headQ", df_headQ ]
                );
//
//    //echo( "ops_headP", ops_headP);
//    //echo( "ops_headQ", ops_headQ);
//    
    function ops_headP(k) = hash( ops_headP,k );
    function ops_headQ(k) = hash( ops_headQ,k );
    
    headpq = linePts( pq 
                     , len=[ u_hs && u_hp?-ops_headP("len"):0 
                           , u_hs && u_hq?-ops_headQ("len"):0]
                     );
    //echo("_ops",_ops);
    
    linepq = linePts( headpq, len= [u_hp?GAPFILLER:0
                                   , u_hq?GAPFILLER:0 ] );    
    
    R= len(pq)>2? pq[2]:randPt();
    //echo("arrow, concat(linepq,[R])",concat(linepq,[R]));
    
    //====================================== line 
    Line( linepq, _ops);
    //Line( concat(linepq,[R]), _ops);
    //======================================
    
    
    if(u_hs && u_hp) Cone( [ headpq[0], pq[0], R ], ops_headP );
    if(u_hs && u_hq) Cone( [ headpq[1], pq[1], R ], ops_headQ );
    
}    

module Arrow_demo_1()
{
 _mdoc("Arrow_demo_1"
, "Yellow: Arrow( randPts(2) )
");   
    pq = randPts(2);
    //MarkPts( pq, ["labels", "PQ", "colors",["red"]] );
    //Chain( pqr, ["closed",false]);
    Arrow( pq );
    
    pq2 = randPts(2);
    //MarkPts( pq2, ["labels", "PQ", "colors",["green"]] );
    Arrow( pq2, [ "color","green"
                , "fn", 6, "r",0.3
                ]);
    
    LabelPt( pq[0], " Arrow(pq)");
    LabelPt( pq2[0], 
    " Arrow(pq,[\"fn\",6,\"r\",0.3,\"color\",\"green\"])"
    ,["color","green"]);
//    
//    
}    
module Arrow_demo_2_defaultArrowSettings()
{
 _mdoc("Arrow_demo_2_autoArrowRad"
, "The arrow rad and len are auto set, if not specified. 
;; The following are Arrow( pq, [''r'',r]) 
;; at  r= 0.01, 0.025, 0.05, 0.1, 0.2, 0.3, 0.5
;; This could be used as a guide line to set arrow rad/len.
");   

    rs= [0.01, 0.025, 0.05, 0.1, 0.2, 0.3, 0.5];
    
    LabelPt( [2,-1,4],
      "Arrows with different r:[0.01,0.025,0.05,0.1,0.2,0.3,0.5]"
    );
    
    for( i = range(rs) )
    {  pq = randPts(2);
       r = rs[i];
       L = d01(pq); 
       echo( "r", rs[i]
           , "arr_r", (3*exp(-0.8*r)) *r
           , "arr_l", min(10*exp(-0.5*r)*r, 2*L/5) 
           );   
       Arrow( pq, ["r", rs[i]]);
    }
  
}     

module Arrow_demo_3_lineProp()
{
 _mdoc("Arrow_demo_3_lineProp"
," Yellow: Arrow( pq, [''r'', 0.1, ''fn'',8, ''transp'', 0.8 ])
;; Red: Arrow( pq, [''r'', 0.1, ''heads'', false, ''color'',''red''] )
;; Green: Arrow( pq, [''r'', 0.1, ''headP'', false, ''color'',''green''] )
;; Blue: Arrow( pq, [''r'', 0.1, ''headQ'', false, ''color'',''blue''] )
"); 
//    Arrow(randPts(2));
//    Arrow( randPts(2)
//         , ["r", 0.1, "fn",8, "transp", 0.8 ]);   
    
    pq0= randPts(2);
    //MarkPts(pq0);
    //LabelPt( pq0[0], "  heads=false", ["color","red"]);
    Arrow( pq0        , ["r", 0.1, "heads", false, "color","red"] );

    pq= randPts(2);
    MarkPts(pq);
    LabelPt( pq[0], "  P (headP=false)", ["color","green"]);
    Arrow( pq
        , ["r", 0.1, "headP", false, "color","green"] );

    pq2= randPts(2);
    MarkPts(pq2);
    LabelPt( pq2[1], "  Q (headQ=false)", ["color","blue"]);
    Arrow( pq2
        , ["r", 0.1, "headQ", false, "color","blue"] );
 
}    
module Arrow_demo_4_arrowProp()
{
 _mdoc("Arrow_demo_4_arrowProp"
," Yellow: Arrow( pq, [''r'', 0.1,  ''headP'', [''color'',''red'']])
;; Red: Arrow( pq, [''r'', 0.1, ''color'',''blue''
;;                 , ''headQ'', [''color'',''green''
;;                               ,''r'', 0.5
;;                               ,''len'', 1.5
;;                               ,''fn'', 4
;;                               ]] )
"); 

    pq= randPts(2);
    MarkPts(pq);
    LabelPts( pq, [" P", " Q"] );
    Arrow( pq
         , ["r", 0.1,  "headP", ["color","red"]] );
    LabelPt( pq[0], "   Arrow(pq,[\"r\",0.1,\"headP\",[\"color\",\"red\"]])");
    
    pq2= randPts(2);
    MarkPts(pq2);
    LabelPts( pq2, [" P", " Q"] );
    
    Arrow( pq2
        , ["r", 0.1, "color","blue"
          , "headQ", ["color","green"
                      ,"r", 0.5
                      ,"len", 1.5
                      , "fn", 4
    ]] );
    
    color("blue")
    {LabelPt( pq2[0], "   Arrow( pq2, [\"r\", 0.1, \"color\",\"blue\" ");
    LabelPt( addy(pq2[0],-1.5), "   , \"headQ\", [\"color\",\"green\",\"r\",0.5,\"len\",1.5,\"fn\",4]])");
    }                  
 
}   
//Arrow_demo_1();    
//Arrow_demo_2_defaultArrowSettings();    
//Arrow_demo_3_lineProp();     
//Arrow_demo_4_arrowProp();    
    
    

////// core ========================================================
//begCrossLinePts= [
//"begCrossLinePts", "r,p1,p2", "[q1,q2]",  "2D, Point, Geometry",
// " Given a 2-pointer pq (2d), return [A,B] for the crossline
//;; that goes thru p and perpendicular to line-p~q. r=length/2.
//;;
//;;    A          -+-
//;;    |           |
//;;  p +-----+ q   | 2r
//;;    |           |
//;;    B          -+-
//"];
//
//function begCrossLinePts( pq, r=0.5 )=
//(
//  [ r/2/norm(pq[1]-pq[0])*[-(pq[1][1]-pq[0][1]), (pq[1][0]-pq[0][0])] +pq[0]
//  , r/2/norm(pq[1]-pq[0])*[ (pq[1][1]-pq[0][1]),-(pq[1][0]-pq[0][0])] +pq[0]
//  ]	
//);
//
//module begCrossLinePts_test(ops)
//{
//	doctest
//	( 
//	  begCrossLinePts
//	, [
//		["[[0,0],[3,4]],10", begCrossLinePts([[0,0],[3,4]],10),[[-4,3], [4, -3]] ]
//      , ["[[0,0],[6,8]],10", begCrossLinePts([[0,0],[6,8]],10),[[-4,3], [4, -3]] ]
//	  , ["[[0,0],[4,3]],10", begCrossLinePts([[0,0],[4,3]],10),[[-3,4], [3, -4]] ]
//	  , ["[[0,0],[4,-3]],10", begCrossLinePts([[0,0],[4,-3]],10),[[3,4], [-3,-4]]]
//      ]
//	,ops
//    );	
//}
//
//module begCrossLinePts_demo()
//{	
//	data=[	
//		   [[[0,-2],[3,3]],10]
//         , [[[4,1],[6,8]],10]
//	     , [[[2,5],[4,3]],10]
//	     , [[[0,1],[4,-3]],10]
//      ];      
//	for( arg= data ){
//		//echo(arg, begCrossLinePts( arg[0], 5 ) );
//		Line0(arg[0], ["r",0.1]);
//		color("green", 0.5)
//		Line0( begCrossLinePts( arg[0], 3 ));
//	}
//}
//begCrossLinePts_test( ["mode",22] );
//begCrossLinePts_demo();
//doc(begCrossLinePts);


//// core ========================================================
begwith=[ "begwith", "o,x", "true|false",  "String, Array, Inspect",
" Given a str|arr (o) and x, return true if o starts with x.
;;
;; New 2014.8.11:
;;
;; x can be an array, return true if o ends with any item in x.
"];

function begwith(o,x)= 
(
	x==""||x==[]
	? undef
	: isarr(x)
	  ? sum( [for(i=range(x)) begwith(o,x[i])?1:0 ] )>0
	  : index(o,x)==0
);
module begwith_test(ops)
{

	begwith_tests=
	[ 
  	  [ "''abcdef'', ''def''", begwith("abcdef", "def"), false ]
	, [ "''abcdef'', ''abc''",  begwith("abcdef", "abc"),true ]
	, [ "''abcdef'', ''''",  begwith("abcdef", ""),undef ]
	, [ "[''ab'',''cd'',''ef''], ''ab''", begwith(["ab","cd","ef"], "ab"),true]
	, [ "[], ''ab''", begwith([], "ab"),false]
	, [ "'''', ''ab''", begwith("", "ab"),false]
	, "// New 2014.8.11"
  	, [ "''abcdef'', [''abc'',''ghi'']", begwith("abcdef", ["abc","ghi"]), true ] 
  	, [ "''ghidef'', [''abc'',''ghi'']", begwith("ghidef", ["abc","ghi"]), true ] 
  	, [ "''aaadef'', [''abc'',''ghi'']", begwith("aaadef", ["abc","ghi"]), false ] 
 	, [ "[''ab'',''cd'',''ef''], [''gh'',''ab'']", begwith(["ab","cd","ef"], ["gh","ab"]),true]
	];
	
	doctest( begwith, begwith_tests, ops );
}
//begwith_test( ["mode",22] );
//doc(begwith);




// ========================================================
between=["between", "low,n,high,include=0", "T|F", "Inspect"  ,
" Given a number (n) and low/high bounds, return true if n is between them.
;; include decides if the bounds be included. Set it to 0 (open, not included)
;; or 1 (closed, included), or [0,1] or [1,0] to set them differently.
"];

function between( low, n, high, include=0 )=
(
  (
	(include==1 || include[0]) ? 
		( low<=n) : (low<n )
  )&&
  (
	(include==1 || include[1]) ? 
		( n<=high) : (n<high )
  )
);

module between_test( ops = ["mode",13] )
{
	a=5;
   doctest( between,
	[
	  ["3,a,8", between(3,a,8), true ]
	  ,["3,a,5", between(3,a,5), false ]
	  ,["3,a,5,[0,1]", between(3,a,5,[0,1]), true, ["rem","Include right end"] ]
	  ,["3,a,5,1", between(3,a,5,1), true, ["rem","Include both ends"] ]
	  ,""	
	  ,["5,a,8", between(5,a,8), false ]
	  ,["5,a,8,[0,1]", between(5,a,8,[0,1]), false, ["rem","Include right end"] ]
	  ,["5,a,8", between(5,a,8), false ]
	  ,["5,a,8,[1,0]", between(5,a,8,[1,0]), true, ["rem","Include left end"] ]
	  ,["5,a,8,1", between(5,a,8,1), true, ["rem","Include both ends"] ]
	], ops, ["a",a] );    
}

//between_test();



//// shape ========================================================
Block=[ "Block","pq,ops", "n/a", "3D, shape",
 " Given a 2-pointer pq, make a block that has its one edge lies on
;; or parallel to the pq line. The block size is determined by pq,
;; width and depth. It can rotate by an angle defined by ''rotate''
;; about the pq-line. Default ops:
;;
;;  [ ''color'' , ''yellow''
;;  , ''transp'', 1
;;  , ''rotate'', 0           // rotate angle alout the line
;;  , ''shift'' , ''corner''  //corner|corner2|mid1|mid2|center
;;  , ''width'' , 1
;;  , ''depth'' , 6
;;  ]
;;
;; The shift is a translation decides where on the block it attaches
;; itself to pq. It can be any [x,y,z], or one of the following strings:
;;
;;    corner       mid1          mid2         center
;;    q-----+      +-----+      +-----+      +-----+
;;   / :     :    p :     :    / q     :    / :     :
;;  p   +-----+  +   +-----+  +   +-----+  + p +-----+
;;   : /     /    : q     /    p /     /    : /     /
;;    +-----+      +-----+      +-----+      +-----+
;;
;;    corner2
;;    +-----+
;;   / :     :
;;  +   q-----+
;;   : /     /
;;    p-----+
"];

module Block(pq,ops){
	Line( pq, update(ops, ["style", "block"]) );
}

module Block_test(ops){ doctest(Block, ops=ops);}

module Block_demo_1()
{
	echom("Block_demo_1");
	pq= randPts(2);
	//pq = [ [2,1,3], [4,3,2]];
	MarkPts(pq);
	Block(pq);
}

module Block_demo_2()
{
	echom("Block_demo_2");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]], ["width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], update(ops, ["color","red"]));//
	Block([[x-w,0,0], [x-w,0,z]], update(ops, ["color","green"]) );

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Block( [ [x, 0, z], [0, 0, z+1]], update(ops, ["color","blue", "rotate",360*$t] ) );
	
}


module Block_demo_3()
{
	echom("Block_demo_3");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]]
					   , [ "width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift","corner2" // <====
					  ]) );


}

module Block_demo_4()
{
	echom("Block_demo_4");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]]
					   ,[ "width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift","mid1" // <====
					  ]) );

}

module Block_demo_5()
{
	echom("Block_demo_5");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]]
					   , ["width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift","mid2" // <====
					  ]) );
}


module Block_demo_6()
{
	echom("Block_demo_6");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]],[ "width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift","center" // <====
					  ]) );
}


module Block_demo_7()
{
	echom("Block_demo_7");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Block([O, [0,0,z+1]]
					   , ["width",0.5
					   , "depth",4]
					  );
	Block([[0,0,w], [x,0,w]], ops);
	Block([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Block( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t
						,"shift",[2*w, 0,w] // <====
					  ]) );
}


module Block_demo()
{
	//Block_demo_1();
	//Block_demo_2();
	Block_demo_3();
	//Block_demo_4();
	//Block_demo_5();
	//Block_demo_6();
	//Block_demo_7();
}
//Block_test( ["mode",22] );
//Block_demo();
//doc(Block);




//// core ========================================================
boxPts= ["boxPts","pq,bySize=true", "Array",  "3D, Point",
 " Given 2-pointer (pq) and bysize(default= true), return
;; a 8-pointer array representing the 8 corners of a box that
;; has all edges perpendicular to axes. The box starts from the
;; first pt (=p), and, if bysize=true, then the 2nd pt (q) is
;; the sizes of box along x,y,z. If bySize=false,  q is used
;; as the opposite corner. No matter which corner the p is at,
;; the return 8 points starts w/ the left-bottom pt  (the one
;; has the smaller x,y,z), then go anti-clockwise  to include
;; all 4 bottom pts, then same manner of the top  4 pts. So the
;; 5th pt (i=4) is right on top of the 1st.
;;
;;       z      4----------7_
;;       |      | '-_      | '-_
;;       |      0_---'-_---3_   '-_
;;   '-_ |        '-_   '5---------6
;;  ----'+------y    '-_ |      '-_|
;;       | '-_          '1---------2
;;            ' x
;;
;; NOTE: 
;;
;; 1) Use cubePts that is similar but produces box points not 
;;    necessary perpendicular to the axes;
;; 2) Use rodfaces(4) to generate the faces needed for polyhedron.
;;     
"];
 
function boxPts(pq, bySize=true)=
(
 	bySize?  
	  // q is edge lengths. Just replace pq below with [pq[0], pq[0]+pq[1]]
    [ [ minx([pq[0], pq[0]+pq[1]])
	  , miny([pq[0], pq[0]+pq[1]])
	  , minz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , miny([pq[0], pq[0]+pq[1]])
      , minz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , minz([pq[0], pq[0]+pq[1]]) ]
	, [ minx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , minz([pq[0], pq[0]+pq[1]]) ]
	, [ minx([pq[0], pq[0]+pq[1]])
      , miny([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , miny([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	, [ maxx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	, [ minx([pq[0], pq[0]+pq[1]])
      , maxy([pq[0], pq[0]+pq[1]])
      , maxz([pq[0], pq[0]+pq[1]]) ]
	]
	: // we need min*, max* to "sort" the point coordinates
	[ [ minx(pq), miny(pq), minz(pq) ]
	, [ maxx(pq), miny(pq), minz(pq) ]
	, [ maxx(pq), maxy(pq), minz(pq) ]
	, [ minx(pq), maxy(pq), minz(pq) ]
	, [ minx(pq), miny(pq), maxz(pq) ]
	, [ maxx(pq), miny(pq), maxz(pq) ]
	, [ maxx(pq), maxy(pq), maxz(pq) ]
	, [ minx(pq), maxy(pq), maxz(pq) ]
	]
);

module boxPts_test( ops )
{
	scope=["pq",[[1,2,-1],[2,1,1]]];
	pq = hash(scope,"pq");

	doctest
	(
		boxPts
		,[
		  [	"pq", boxPts(pq), [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
							, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0]]
		  ]
		 ,[ "pq,true",boxPts(pq,true)
			, [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
			, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0]]      
		  ]
		]
		,ops, scope
	);
}

module boxPts_demo(pq = [ [2,1,-1],[3,2,1]], bySize=true)
{
	//Line0(pq);
	//MarkPts(pq);

	pts = boxPts( pq, bySize );

	for (i=[0:7]){
		translate(pts[i])
		if(i==0||i==4){
			color("red")
			sphere(r= i<4?0.08:0.05);
		}else if(i==1||i==5){
			color("green")
			sphere(r= i<4?0.08:0.05);
		}else if(i==2||i==6){
			color("blue")
			sphere(r= i<4?0.08:0.05);
		}else{
			sphere(r= i<4?0.08:0.05);
		}
	}

	for(p0=pq){
		translate(p0)
		color("green",0.1)
		sphere(r=0.2);
	}
}  	
//boxPts_demo(bySize=false);
//boxPts_test();
//



//// shape ========================================================
Chain=["Chain","pts,ops", "n/a", "3D, Shape",
 " Given array of pts (pts) and ops, make a chain of line
;; segments to connect points. ops:
;;
;;  [ ''r''     , 0.05
;;  , ''closed'',true
;;  , ''color'' ,false
;;  , ''transp'',false
;;  , ''usespherenode'',true
;;  ]
"];	

module Chain(pts, ops=[])
{
    //echo("in Chain ops",ops);
    
	ops= concat(ops,
		 ["r", 0.05
		 ,"r1", false  // <=== what is this ?
		 ,"closed",true
		 ,"color",false
		 ,"transp",false
		 ,"usespherenode",true
		]);
    
//	ops= update(
//		 ["r", 0.05
//		 ,"r1", false  // <=== what is this ?
//		 ,"closed",false
//		 ,"color",false
//		 ,"transp",false
//		 ,"usespherenode",true
//		], ops);
//    
    //echo("in Chain ops",ops);
    
	function ops(k)= hash(ops,k);
	r = ops("r");

	path = concat( pts, ops("closed")?[pts[0]]:[] );
	//echo( "paths in Chain:", paths);
	//echo("ops in Chain: ", ops );
	//echo("r in Chain: ", r);
	//union(){
	for(i=[0:len(path)-2]){
		//echo( i, paths[i]);
		color(ops("color"), ops("transp"))
		if(i>0){translate(path[i])
			if(!ops("usespherenode")){
				//rotate( nor
				cylinder( r=r, h=r*5.0, center=true );
			} else {
				//sphere( r=r+0.005, h=r, center=true );
				sphere( r=r, h=r, center=true );
			}
		}		
		Line0( [pts[i], path[i+1]], ops );
	  }
	//}
}
module Chain_test(ops){ doctest(Chain,ops=ops);}

module Chain_demo_1()
{
	//paths= [ [0,5,0], [ 4,5,0], [4,3,0], [1,3,0], [0,0,0], [4,0,0] ];
	path= [ [0,5,1]
			,[3,5,0],[3.7,4.7,0], [4,4.3,1] //, [ 4,5,0]
			, [4,3,-1], [1,3,2], [0,0,3], [4,0,-1] ];
	//color("silver", 0.9)
	Chain(path);
	
	Chain( randPts(5), ["r",0.2, "closed",true
					  ,"transp",0.3,"color","green"]);

	Chain( boxPts( randPts(2) ),["color","red"]);
}

module Chain_demo_2() // testing chains on axis planes
{
	ops = [ "transp", 0.4, "closed", true ];
	pathxy = randPts(3, z=0);
	Chain( pathxy, update( ops, ["color","red"]) );
	//echo(pathxy);
	
	pathyz = randPts(4, x=0);
	Chain( pathyz, update( ops, ["color","green"]) );
	
	pathxz = randPts(5, y=0);
	Chain( pathxz, update( ops, ["color","blue"]) );
	
}

module Chain_demo_3()
{
	pathxy = randPts(8);
	Chain( pathxy, ["color","red", "transp", 0.4, "usespherenode", false]) ;
	//echo(pathxy);
	
	pathxy2 = randPts(5);
	Chain( pathxy2, ["r",0.2, "color","green", "transp", 0.4, "usespherenode", false]) ;
	
	//pathyz = randPts(4, x=0);
	//Chain( pathyz, update( ops, ["color","green"]) );
	
	//pathxz = randPts(5, y=0);
	//Chain( pathxz, update( ops, ["color","blue"]) );
	
}

module Chain_demo()
{
	Chain_demo_1();
	//Chain_demo_2();
	//Chain_demo_3();
}
//Chain_demo();
//doc(Chain);

module Chainf(pts, ops=[]){  // formatted Chain
    ops = concat( ops, ["labelbeg", "P","r",0.01, "closed",false]);
    labelbeg= hash(ops,"labelbeg");
    //echo(" in chainf, ops",ops);
    Chain( pts, ops=ops );
    LabelPts( pts, labelbeg=="P"?"PQRSTUVWXYZABCDEF":
                    labelbeg==0?range(pts):labelbeg
                , ["scale", hash(ops,"scale", 0.03)] 
                 );
    MarkPts( pts, ["r",0.06] );
}    

//========================================================
cirfrags=[ "cirfrags", "r,fn,fs=0.01,fa=12", "number", "Geometry",
 " Given a radius (r) and optional fn, fs, fa, return the number 
;; of fragments (NoF) in a circle. Similar to $fn,$fs,$fa but
;; this function (1) doesn't put a limit to r and (2) could return
;; a float. In reality divising a circle shouldn't have given partial
;; fragments (like 5.6 NoF/cicle). But we might need to calc the 
;; frags for an arc (part of a circle) so it'd better not to round it
;; off at circle level. 
;; fa: min angle for a fragment. Default 12 (30 NoF/cycle). 
;; fs: min frag size. W/ this, small circles have smaller 
;; NoFs than specified using fa. Default=0.01 (Note: built-in $fs 
;; has default=2). 
;; fn: set NoF directly. Setting it disables fs & fa. 
;;
;; When using fs/fa for a circle, min NoF = 5. 
;;
;; See: http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn

"];
 
function cirfrags(r, fn, fs=0.01, fa=12)=
( // from: get_fragments_from_r
  // http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn
    fn? max(fn,3)
	: max(min(360 / fa, r*2*PI / fs), 5)
);

module cirfrags_test( ops=["mode",13] )
{
	doctest( cirfrags, 
	[ 
		"// Without any frag args, the default frag is 30 (no matter r is):"
		,["r=6",   cirfrags(6), 30]
		,["r=3",   cirfrags(3), 30]
		,["r=0.1", cirfrags(0.1), 30]

		,""
		,"// fn (num of frags) - set NoF directly. Independent of r, too:"
		,""
		
		,["r=3,fn=10", cirfrags(3,fn=10), 10]
		,["r=3,fn=3",  cirfrags(3,fn=3), 3]
		,["r=0.05, fn=12", cirfrags(0.05, fn=12), 12]

		,""
		,"// fs (frag size)"
		,""
		,["r=1,fs=1",  cirfrags(1,fs=1), 6.28319-4.69282*1e-6]
		,["r=1,fs=1",  cirfrags(1,fs=1), 6.28319, ["prec",6,"rem","prec=6"]]
		,["r=1,fs=1",  cirfrags(1,fs=1), 6.28319, ["prec",5,"rem","prec=5"]]
		,["r=1,fs=1",  cirfrags(1,fs=1), 6.28319, ["prec",4,"rem","prec=4"]]
		,["r=3,fs=1",  cirfrags(3,fs=1), 18.8496]
		,["r=6,fs=1",  cirfrags(6,fs=1), 30]
		,["r=6,fs=2",  cirfrags(6,fs=2), 18.8496]
		,["r=12,fs=4", cirfrags(12,fs=4),18.8496]
		,["r=3,fs=10", cirfrags(3,fs=10),5]
		,["r=6,fs=100",cirfrags(6,fs=100), 5,["rem","min=5 using fa/fs"] ]

		,""
		,"// fa (frag angle)"
		,""
		,["r=3,fa=10", cirfrags(3,fa=10), 36]
		,["r=6,fa=100", cirfrags(6,fa=100), 5,["rem","min=5 using fa/fs"] ]
		,""

	]
	, ops
	);
}



//========================================================
ColorAxes=["ColorAxes", "ops", "n/a", "Shape",
 " Paint colors on axes. 
;;  [
;;   ''r'',0.012
;;  ,''len'', 4.5
;;  ,''xr'',-1
;;  ,''yr'',-1
;;  ,''zr'',-1
;;  ,''xops'', [''color'',''red''  , ''transp'',0.3]
;;  ,''yops'', [''color'',''green'', ''transp'',0.3]
;;  ,''zops'', [''color'',''blue'' , ''transp'',0.3]
;;  ]
"];

module ColorAxes( ops=[] )
{
	ops = concat( ops,
	[
		"r",0.012
		,"len", 4.5
		,"xr",-1
		,"yr",-1
		,"zr",-1
		,"fontscale", 1
		,"xops", ["color","red", "transp",0.3]
		,"yops", ["color","green", "transp",0.3]
		,"zops", ["color","blue", "transp",0.3]
	]);
	function ops(k)= hash(ops,k);
	r = ops("r");
	l = ops("len");

	fscale= ops("fontscale");
	//echo( "fscale" , fscale);
	//include <../others/TextGenerator.scad>
	
	xr= hash( ops, "xr", if_v=-1, then_v=r) ;
	yr= hash( ops, "yr", r, if_v=-1, then_v=r);
	zr= hash( ops, "zr", r, if_v=-1, then_v=r);
	
	echo( "r",r, "xr", xr);

	Line0( [[ l,0,0],[-l,0,0]], concat( ["r", xr], ops("xops") ));
	Line0( [[ 0, l,0],[0, -l,0]], concat( ["r", yr], ops("yops") ));
	Line0( [[ 0,0,l],[0,0,-l]], concat( ["r", zr], ops("zops") ));

	xc = hash(ops("xops"), "color");
	yc = hash(ops("yops"), "color");
	zc = hash(ops("zops"), "color");
	translate( [l+0.2, -6*r,0]) 
		rotate($vpr) scale( fscale/15 ) 
		color(xc) text( "x" );
	translate( [6*r, l+0.2, 0]) 
		rotate($vpr) scale( fscale/15 ) 
		color(yc) text( "y" );
	translate( [0, 0, l+0.2]) 
		rotate($vpr) scale( fscale/15 ) 
		color(zc) text( "z" );
} 
module ColorAxes_test(ops){ doctest( ColorAxes ,ops=ops);}


//========================================================
Cone=["Cone","pq, ops=[]", "n/a", "Geometry"
, "Given a 2 or 3-pointer pq and ops, make a cone along the PQ
;; line. If pq a 3-pointer, the points forming the cone bottom
;; will start from plane of the 3 points and circling around 
;; axis PQ in clockwise manner. Default ops:
;;
;; [ ''r'' , 3  // rad of the cone bottom
;; , ''len'', 0.3  // height of the cone
;; , ''fn'', $fn==0?6:$fn // # of segments along the cone curve,
;; ]
;;
;; New feature: You can mark points @ run-time:
;; 
;; Cone(pq, [''markpts'', true] )
;; Cone(pq, [''markpts'', [''labels'',''ABCDEF''] )
;; 
"];
module Cone_test(ops=[]){
    doctest( Cone, [], ops); }
    
module Cone( pq, ops=[] ){
    //_mdoc("Cone","");
                     
	ops= concat( ops
    , [ "r" , 1            // rad of the curve this obj follows
	  , "len", undef       // default to len_PQ. Could be negative
      , "fn", $fn==0?6:$fn // # of segments along the cone curve,
                           // = # of slices or segments
      , "markpts", false   // true or hash
      , "twist", 0
      ], OPS);

    /*      Q
           _|_ 
      _--'' | ''--_
    -{------P------}----- 
      ''-.__|__.-''
            |      | 
            |<---->|
                r    
    */
    
    function ops(k,df)=hash(ops,k,df);
    
    //echo("ops= ", ops);
    
    r = ops("r");
    fn= ops("fn");
    
    P = pq[0]; Q=pq[1]; 
    R= len(pq)==3? pq[2]: anglePt( [Q,P, randPt()], a=90, r=r);
    
    pqr = [P,Q,R];
    
    //     0
    //     | 
    //  _4-|-3_
    // 5_  +  _2
    //   -6-1-
    //
    // faces= [ [0,2,1]
    //        , [0,3,2] 
    //        , ... 
    //        , [0,6,5]
    //        , [0,1,6]
    //        ]
    
    faces= concat(
            [ for( i =range(fn) ) [0, i>fn-2?1:i+2, i+1] ]
            , [range(1, fn+1)]    
      );  

    //echo("faces", faces);
       
    arcpts = arcPts( [Q,P,R]
                    , pl="yz"
                    , a=360
                    , count=fn
                    , r=r
                    , twist= ops("twist")
                    ); 
//    echo( "arcpts", arcpts);   
            
    df_markpts= ["samesize",true, "labels", range(1, fn+1)];       
    m = ops("markpts");
	if(m) { MarkPts(arcpts , concat( isarr(m)?m:[], df_markpts));}
    
    color( ops("color"), ops("transp"))        
    polyhedron( points= concat( [Q], arcpts )
              , faces = faces );               
}

module Cone_demo_1(){
_mdoc("Cone_demo_1"
," yellow: Cone( randPts(2) )
;; green: Cone( randPts(3) , [ ''fn'',6
;;                , ''color'',''green''
;;                , ''transp'',0.5
;;                , ''markpts'',true  // <=== new feature in scadex
;;                ] 
 ");    

    pq = randPts(2);
    MarkPts( pq, ["labels","PQ"]);
    Cone( pq );
    LabelPt( pq[1], "  Cone(pq)");

    pqr = randPts(3);
    Plane(pqr, ["mode",3]);
    MarkPts( pqr, ["labels","PQR"]);
    Cone( pqr , [ "fn",6
                , "color","green"
                , "transp",0.5
                , "markpts",true  // <=== new feature in scadex
                ]);
    LabelPt( pqr[1], "  Cone(pqr,[\"fn\",6,\"markpts\",true...])"
            , ["color","green"]);
                            
    pqr2 = randPts(3);
    Plane(pqr2, ["mode",3]);
    MarkPts( pqr2, ["labels","PQR"]);
    Cone( pqr2 , [ "fn",6
                , "color","blue"
                , "transp",0.5
                , "markpts",true  // <=== new feature in scadex
                , "twist", 30 // <=== new 2015.1.25
                ]);
     LabelPt( pqr2[1]
     , "  Cone(pqr,[\"fn\",6,\"markpts\",true,\"twist\",30...])"
            , ["color","blue"]);
            
}    

//Cone_demo_1();


//========================================================
cornerPt=[ "cornerPt", "pqr", "pt", "3D, Geometry, Point, Math",
"
 Given a 3-pointer (pqr), return a pt that's on the opposite side
;; of Q across PR line.
;;
;;        Q-----R  Q--------R
;;       /   _//    ^.      |^.
;;      /  _/ /       ^.    |  ^.
;;     /_/   /          ^.  |    ^.
;;    //    /             ^.|      ^. 
;;   P     pt               P        pt
"];
 

function cornerPt(pqr)=
(
	// midpt: (pqr[2]-poq[0])/2+pqr[0]
    onlinePt( [pqr[1],midPt	([pqr[0],pqr[2]])], ratio=2)
);

module cornerPt_test(ops)
{ doctest( cornerPt, 
	[ 
		[ "[  [2,0,4],ORIGIN, [1,0,2]   ]", cornerPt([ [2,0,4],ORIGIN,[1,0,2]  ]), [3,0,6]   ]
	], ops
 );
}
//cornerPt_test( ["mode",22] );
//doc(cornerPt);
 


//========================================================
countArr= ["countArr", "arr", "int", "Array, Inspect",
" Given array (arr), count the number of arrays in arr.
"];

function countArr(arr)= countType(arr, "arr");

module countArr_test( ops )
{
	doctest
	( 
	  countArr
	, [
	   [ "[''x'',2,[1,2],5.1]", countArr(["x",2,[1,2],5.1]), 1 ]
	  ]
	, ops
	);

} 
//doc(countArr);




//========================================================
countInt=["countInt", "arr", "int", "Array, Inspect",
" Given array (arr), count the number of integers in arr.
"];

function countInt(arr)= countType(arr, "int");

module countInt_test( ops )
{
	doctest
	( 
	  countInt
	, [
		[ "[''x'',2,''3'',5.3,4]", countInt(["x",2,"3",5.3,4]), 2 ]
	  ]
	, ops
	);

} 
//doc(countInt);




//========================================================
countNum= ["countNum", "arr", "int", "Array, Inspect",
" Given array (arr), count the number of numbers in arr.
"];

function countNum(arr)= countType(arr, "num");

module countNum_test( ops )
{
	doctest
	( 
	  countNum
	, [
		[ "[''x'',2,''3'',5.3,4]", countNum(["x",2,"3",5.3,4]), 3 ]
	  ]
	, ops
	);
} 
//doc(countNum);




//========================================================
countStr =  ["countStr", "arr", "int", "Array, Inspect",
" Given array (arr), count the number of strings in arr.
"];

function countStr(arr)= countType(arr, "str") ;
module countStr_test( ops )
{
	doctest
	( 
	  countStr
	, [
		[ "[''x'',2,''3'',5]", countStr(["x",2,"3",5]), 2 ]
	  ,	[ "[''x'',2, 5]", countStr(["x",2,5]), 1 ]
	  ]
	, ops
	);

} // countStr_test();
//doc(countStr);




//========================================================
countType= ["countType", "arr,typ", "int", "Array, Inspect",
" Given array (arr) and type name (typ), count the items of type typ in arr.\\
"];

function countType(arr, typ, _i_=0)=
(
	
     _i_<len(arr)?
	 ( 
		typ=="num"?
		((isnum(arr[_i_])?1:0)+countType(arr,"num",_i_+1))
		:((type(arr[_i_])==typ?1:0)+countType(arr, typ, _i_+1)) 
	 )
	 :0
	
	/*  
	//===================================================
	// #	The following version works, and skips the need of 
    // #     extra _i_=0. However, it seems to be much slower.
		
	len(arr)==0? 0
	: ( typ=="num"&& isnum(arr[0])
	  ||	 type(arr[0])==typ ? 1:0
      )+ (len(arr)>1 ? 
    //      countType(shrink(arr),typ) 
	    countType(slice(arr,1),typ) 
		 :0
         )
	//===================================================
	*/ 
);

module countType_test( ops )
{

	countType_tests=  
	[
		[ "[''x'',2,[1,2],5.1], ''str''"
			, countType(["x",2,[1,2],5.1], "str"), 1 ]
	  ,	[ "[''x'',2,[1,2],5.1,3], ''int''"
			, countType(["x",2,[1,2],5.1,3],"int"), 2 ]
	  ,	[ "[''x'',2,[1,2],5.1,3], ''float''"
			, countType(["x",2,[1,2],5.1,3],"float"), 1 ]
	];

	doctest(countType, countType_tests,ops);

} 
//doc(countType);






//========================================================
module Cube(pqr, p=undef, r=undef, h=1)
{
	echom("Cube");
	cpts =cubePts(pqr,p=p,r=r,h=h);
//	echo("pqr:", pqr);
//	echo("cpts:", cpts);
	polyhedron( points= cpts, faces = CUBEFACES );
}

module Cube_demo()
{
	Cube(randPts(3));
	Cube(randPts(3),p=0.2,r=0.4,h=5);
	Cube(randPts(3),p=5,r=5,h=0.2);
}

//Cube_demo();



//========================================================
cubePts=["cubePts", "pqr,p=undef,r=undef,h=1", "points", "Points",
" Like boxPts but is not limited to axes-parallel boxes. That is, 
;; it produces 8 points for a cube of any size at any location along 
;; any direction. 
;; 
;; The input pqr = [P,Q,R0] is used to make the square PQRS that is
;; coplanar with R0. It is then added the h to make TUVW by finding 
;; the normal of pqr. Optional p,r,h: numbers indicating lengths as 
;; shown belo.
;;
;;     U------------------V_ 
;;     |'-_     R0        | '-_
;;     |   '-_ -''-_      |    '-_
;;     |   _. '-T---'-------------W
;;     |_-'     |      '-_|       |
;;     Q_-------|---------R_      | h
;;       '-_    |    r      '-_   |
;;       p  '-_ |              '-_|
;;             'P-----------------S
;;                        r
;; Return a 8-pointer: [P,Q,R,S,T,U,V,W] with order:
;;
;;     5----------6_ 
;;     | '-_      | '-_
;;     1_---'-_---2_   '-_
;;       '-_   '4---------7
;;          '-_ |      '-_|
;;             '0---------3
;;
;; Use rodfaces(4) to generate the faces needed for polyhedron. 
;; 
;; New 20141003: shift 
;;
;;     U------------------V_ 
;;     |'-_               | '-_
;;     |   '-_            |    '-_
;;    Q|------'-T-----------------W
;;     | '-_    |  r      |       |
;;     |_---'-_-|----------_      | 
;;       '-_   'P           '-_   | ---
;;          '-_ |              '-_|   -shift
;;             'X------------------ ---
"];

function cubePts( pqr, p=undef, r=undef, h=1,shift)=
 let( Q = Q(pqr)
	, h_vector = normalPt(pqr, len=h)-Q
	, shiftvec = onlinePt( [N(pqr), Q], len=-shift )-Q
	, pqr = isnum(shift)
			? [ for (p=pqr) p+shiftvec ] 
			:pqr 
    )	
(	concat( squarePts( pqr, p=p,r=r )
		  , squarePts( //addv( pqr, normalPt(pqr, len=h)-Q(pqr))
					  [for (pt=pqr) sign(h)>0?(pt-h_vector):(pt+h_vector) ]
                      ,p=p,r=r )
	)		
);

module cubePts_test(ops){ doctest(cubePts,ops=ops);}

module cubePts_demo()
{
	module demo(color="red", p=undef, r=undef, h=2)
	{
		pqr = randPts(3,r=4);
		MarkPts([ R(pqr)], ["labels",["R0"]] );
		Chain( pqr, ["r",0.02]);

		echo("p,r,h= ", p, r, h);

		cpts = cubePts(pqr,p=p,r=r,h=h);
		MarkPts( cpts, ["labels", "PQRSTUVW"] );
		Chain( slice(cpts, 0, 4 ), ["r",0.01]);
		Chain( slice(cpts, 4, 8 ), ["r",0.01]);
	
		Mark90( p3(cpts,4,0,1) );
		Mark90( p3(cpts,4,0,3) );
	
		//Normal( pqr );
		//MarkPts( [normalPt(pqr,len=1)], ["r",0.2,"transp",0.3] );//
//		Chain( addv( pqr, normalPt(pqr, len=1)-Q(pqr) ) );
//		Chain( 
//			squarePts( addv( pqr, normalPt(pqr, len=1)-Q(pqr)),p,r )
//			,["r",0.2,"transp",0.3] );
	
		pqrs = squarePts( pqr, p=p,r=r );
		Chain( pqrs, ["r",0.1, "transp",0.3] );		

		//echo("pq, qr = ", dist
		color(color, 0.2)
		polyhedron( points= cpts, faces = CUBEFACES );
	
		Dim( p3(cpts, 0,1,2),["color","red", "prefix", "p="] ); // measure pq
		Dim( p3(cpts, 2,1,0),["color","green", "prefix","r="]  ); // measure qr
		Dim( p3(cpts, 4,0,3),["color","blue", "prefix","h="]  ); // measure pt

	}

	//demo();
	//demo("green",p=3);
	//demo("blue",r=3);
	demo("purple",p=5,r=3);

}

//cubePts_demo();


//========================================================
CurvedCone=["CurvedCone","pqr, ops=[]"
   , "n/a", "Geometry"
, "Given a 3-pointer pqr and ops, make a curved 
;; cone, where ops is:
;;
;; [ ''r'' , 3  // rad of the curve
;; , ''r2'', 0.3  // rad of the bottom of the cone
;; , ''a'', undef // angle determining cone length
;;              // if not given, use a_pqr
;; , ''fn'', $fn==0?6:$fn // # of segments along the cone curve,
;; , ''fn2'', $fn==0?6:$fn // # of points along bottom circle 
;; ]
"];
module CurvedCone_test(ops=[]){
    doctest( CurvedCone, [], ops); }
    
module CurvedCone( pqr, ops=[] )
{
/*          	. R  						 	       _
             _-' -      
          _-'     :
       _-'         :     
    _-'            : 
  Q+---------------+ P



                R  |N (N is on pl_pqr, and NP perp to PQ )
             _-' : | 
          _-'     || 
       _-'        _|_ 
    _-'      _--'' | ''--_
  Q+--------{------P------}----- 
             ''-.__|__.-''
                   |      | 
   |<------------->|<---->|
           r           r2 
    
*/
    //_mdoc("CurvedCone","");
                     
	ops= concat( ops
    , [ "r" , undef  // rad of the curve this obj follows
	  , "r2", 0.5  // rad of the bottom of the obj 
      , "a", undef // angle determining cone length
                   // if not given, use a_pqr
      , "fn", $fn==0?6:$fn // # of segments along the cone curve,
                    // = # of slices or segments
      , "fn2", $fn==0?6:$fn // # of points along bottom circle
      ], OPS);

    function ops(k,df)=hash(ops,k,df);
    

    r= ops("r")==undef?d01(pqr):ops("r"); 
    echo("r", r, "d01(pqr)", d01(pqr));
    
    a = ops("a")? ops("a"): angle(pqr);
    P = pqr[0]; Q=pqr[1]; R= anglePt(pqr, a, len=r);
    
    r2= ops("r2");
    fn= ops("fn");
    fn2=ops("fn2");
    
    pqr = [P,Q,R];
   
    function getPtsByLayer(i)=
    (   let( // X is a point along the R->P curve
             X = anglePt( pqr
                  , a=(fn-i-1)*a/(fn)
                  , len=r )
           , N = N([Q,X,R])
           ) 
        arcPts( [Q,X,N]
                    , pl="xy"
                    , a=360
                    , count=fn2
                    , r=r2 /fn*(i+1)
                    )
    );

    // This for loop is for debug only. It displays
    // lines thru circling pts of each layer, and also
    // label pts
    //    for( i = range(fn) ){
    //       pts = getPtsByLayer(i); 
    //       Chain( pts, ["r",0.01]);
    //       MarkPts( pts, ["samesized",true, "r", 0.02] );
    //       LabelPts( pts, range( i*fn2+1, (i+1)*fn2+1 )
    //                , ["scale",0.02] ); 
    //    }    
    
    
    // pts: [ [p10,p11,p12...p16]  <== layer 1
    //      , [p20,p21,p22...p26]
    //      ...
    //      , [pi0,pi1,pi2...pi6] ] <==layer i    
    pts_layers = [ for( i = range(fn) )
                   getPtsByLayer(i) ];
    //echo("");
    //echo("pts_layers: ", pts_layers);
    //echo( "pts_layers = ", pts_layers );
        
    pts = [ for(a=pts_layers, b=a) b] ; 
       
    //echo("");
    //echo("pts: ", pts);
    //echo("pts = ", pts); 
        
    ptsAll = concat( [R], pts );
    //echo("");
    //echo("with R: ptsAll: ", ptsAll);
    
    /*
    faces of a raised pantagon :
    
    faces = [ [1,0,5,6]
            , [2,1,6,7]
            , [3,2,7,8]
            , [4,3,8,9]
            , [0,4,9,5]
            , [0,1,2,3,4]
            , [5,6,7,8,9]
            ];
            
    fn = 4
    fn2= 6
    layer i = range( fn)
    faces= 
    [ [1,6,12,7]    // i = 1  
   , [2,1, 7,8]                    
   , [3,2, 8,9]
   , [4,3, 9,10]
   , [5,4,10,11]
   , [6,5,11,12]
   
   , [7,12,18,13]  // i = 2, j= range(fn2)  
          [(i-1)*fn2+1, fn2*2, fn2*3, i*fn2+1) 
   , [8,7, 13,14] 
          [(i-1)*fn2+j+1, (i-1)*fn2+j, i*fn2+j, i*fn2+j+1) 
   , [9,8,14,15]     
   , [10,9,15,16]
   , [11,10,16,17]
   , [12,11,17,18]
   
   , [13,18,24,19] // layer = 3
   , [14,13,19,20]
   , [15,14,20,21]
   , [16,15,21,22]
   , [17,16,22,23]
   , [18,17,23,24] 
   ]
            
    Last 2 are easy to make. Prior to that,         
    we make a "preface" first then transpose it:
       
       [ [1,2,3,4,0]  : concat( range(1,5), [0])
       , [0,1,2,3,4]  : range( 0, 5 )
       , [5,6,7,8,9]  : range( 5, 5+5 )
       , [6,7,8,9,5]  : concat( range(6,5+5), [5]) 
       ]
    ---------------------   
    If there are more than one layer, the above
    is the layer 0. Each layer will inc by 5:
    
    i=1:
       [ [5,6,7,8,9]  : range( 5, 5+5 )
       , [10,11,12,13,14]: range( 5+5, 10+5 )
       , [11,12,13,14,10]: concat( range(5+6,10+5), [10]) 
       , [6,7,8,9,5]  : concat( range(5+1,10), [5]) 
       ]
    
    layer i, fn:
    
        [ concat( range( i*fn+1, (i+1)*fn ), [fn] )
        , range( i*fn, (i+1)*fn )
        , range( (i+1)*fn, (i+2)*fn )
        , concat( range((i+1)*fn+1,(i+2)*fn),[i*fn] )
        ]
    */    
    
    // Faces w/o the first layer (which contains
    // triangles) and the bottom layer
//    function getSideFacesByLayer(i, fn, hasnext=false)=
//      transpose(
//        [ concat( range( i*fn+1, (i+1)*fn ), [fn] )
//        , range( i*fn, (i+1)*fn )
//        , range( (i+1)*fn, (i+2)*fn )
//        , hasnext? range((i+1)*fn+1,(i+2)*fn+1) 
//          : concat( range((i+1)*fn+1,(i+2)*fn),[i*fn] )
//        ]
//      )
//      ;
    
    // Layer0 contains triangles
    // 
    //     0
    //     | 
    //  _4-|-3_
    // 5_  +  _2
    //   -6-1-
    //
    // faces= [ [0,2,1]
    //        , [0,3,2] 
    //        , ... 
    //        , [0,6,5]
    //        , [0,1,6]
    //        ]
    
    function getLayer0Faces(fn)=
      [ for( i =range(fn-1) )
        [ 0, i>fn-3?1:i+2, i+1]
      ];  

    
    sidefaces = getTubeSideFaces
      ( [ for (i=range(1, fn+1))
          [ for (j=range(fn2)) (i-1)*fn2+j+1 ]
        ]      
      , isclockwise=true 
      );
    //echo("");      
    //echo("\nsidefaces = ",sidefaces);
                
    faces = mergeA( 
            [getLayer0Faces(fn2+1)
            , mergeA( sidefaces )  
            ,  [range((fn-1)*fn2+1, fn*fn2+1)] 
            ]);                
    //echo("");            
    //echo( "faces: ", faces);
    //echo("");
       
    //color("green", 0.8)            
    polyhedron( points= ptsAll
              , faces = faces );               
}

module CurvedCone_demo_1(){
_mdoc("CurvedCone_demo_1"
," yellow: CurvedCone( randPts(3) )
;; red  : a=60, r=8, r2=1,  fn= 4, fn2=6 
;; green: a=30, r=8, r2=0.5, fn=10, fn2=20 
;; blue : a=120,r=3, r2=1  , fn= 9, fn2=4 
 ");    

    pqr = randPts(3);
    MarkPts( pqr, ["labels","PQR"] ); Chain(pqr, ["closed",false]); 
    
    CurvedCone( pqr );
    LabelPt( midPt(p12(pqr)), "CurvedCone(pqr)");
    
    color("red", 0.6)
    CurvedCone( randPts(3), ["a",60
                     , "r", 8
                     , "r2", 1
                     , "fn",4 
                     , "fn2",6 
                     ] );
                     
    color("green", 0.6)
    CurvedCone( randPts(3), ["a",30
                     , "r", 8
                     , "r2", 0.5
                     , "fn",10
                     , "fn2",20
                     ] );
    
    color("blue", 0.6)
    CurvedCone( randPts(3), ["a",120
                     , "r", 3
                     , "r2", 1
                     , "fn",9
                     , "fn2",4
                     ] );    
}    

//CurvedCone_demo_1();


//========================================================
//cycleRange=["cycleRange","pts/i, 



//========================================================
DashBox=[ "DashBox", "pq,ops", "n/a", "3D, Shape", 
 " Given a 2-pointer (pq) and ops, make a box with dash lines parallel
;; to axes. The default ops is:
;;
;;   [ ''r'',0.005
;;   , ''dash'', 0.05
;;   , ''space'', 0.05
;;   , ''bySize'', true
;;   ]
;;
;; where r= radius of dash line, dash= the length of dash, space= space
;; between dashes. The p,q are the opposite corners of the box if bySize=
;; false. If bySize=true, p is a corner, and q contains size of box on
;; x,y,z direction.
"];  

module DashBox(pq, ops)
{
	ops=update(
		[ "r",0.005
		, "dash", 0.1
		//, "space", 0.05
		, "bySize", true
		], ops );
	function ops(k)= hash(ops,k);

	echo( ops("bySize"));

	pts = boxPts( pq, bySize= ops("bySize") );

	r= ops("r");

	color("blue"){
		Line( [ pts[0], pts[4] ], ops);
		DashLine( [ pts[1], pts[5] ], ops );
		DashLine( [ pts[2], pts[6] ], ops );
		DashLine( [ pts[3], pts[7] ], ops );
	}
	color("red"){
		DashLine( [ pts[0], pts[1] ], ops);
		DashLine( [ pts[2], pts[3] ], ops );
		DashLine( [ pts[4], pts[5] ], ops );
		DashLine( [ pts[6], pts[7] ], ops );
	}
	color("lightgreen"){
		DashLine( [ pts[0], pts[3] ], ops);
		DashLine( [ pts[1], pts[2] ], ops );
		DashLine( [ pts[4], pts[7] ], ops );
		DashLine( [ pts[5], pts[6] ], ops );
	}
}


module DashBox_test( ops ) {	doctest( DashBox, ops=ops); }

module DashBox_demo(pq, bySize=true)
{
	pq = [ [2,1,-1],[3,2,1]];
	DashBox( pq , [["bySize", bySize]]);

	//boxPts_demo( pq, bySize );
	MarkPts( boxPts( pq ) );

}
//DashBox_demo(bySize=true);
//doc(DashBox);


//========================================================
DashLine=["DashLine","pq,ops", "n/a", "3D, Shape", 
 " Given two-pointers(pq) and ops, draw a dash-line between pq.
;; Default ops:
;;
;;   [ ''r''     , 0.02     // radius
;;   , ''dash''  , 0.5      // dash length
;;   , ''space'' , undef    // space between dashes, default=dash/3
;;   , ''color'' , false
;;   , ''transp'', false
;;   , ''fn''    , $fn
;;   ]
"];

module DashLine(pts, ops=[])
{
	//echo("DashLine:");
	pts=countArr(pts)==0?
		[[0,0,0], pts] : pts;

	ops= update(["r",0.02
				,"dash",0.5
				,"space",undef
				,"color", false
				,"transp", false
				,"fn",$fn
				], ops);
	function ops(k)= hash(ops,k);

	dash = ops("dash");
	space= hash(ops, "space", dash/3); 
	s= dash+space;
	L = norm(pts[1]-pts[0]);
	count = L/s;
	if( L<=dash )
	{
		Line0( pts, ops );	
	}else if (count<1)
	{
		Line0( [pts[0], onlinePt(pts, ratio=dash/L)], ops );
	}else{
		for (i=[0:count])
		{
			if((i*s+dash)>L)
			{
				Line0( [ onlinePt(pts, ratio=i*s/L)
			  		  , pts[1] ], ops);

			}else{
				Line0( [ onlinePt(pts, ratio=i*s/L)
				  , onlinePt(pts, ratio=(i*s+dash)/L)]
				  ,ops);
			}
		}
	}
}


module DashLine_test(ops){ doctest(DashLine,ops=ops); }

module DashLine_demo(){

	pts= randPts(2);
	DashLine( pts); 
	MarkPts(pts);
	
	pts2 = randPts(2);
	DashLine( pts2, ops=["dash",0.02, "space",0.06] );
	MarkPts(pts2);
}
//DashLine_demo();
//DashLine_test();
//doc(DashLine);


//========================================================
del=["del", "arr,x,byindex=false,count=1", "array", "Array",
 " Given an array arr, an x and optional byindex, delete all
;; occurances of x from the array, or the item at index x when
;; byindex is true. 
;;
;; New 2014.8.7: arg *count* when byindex=true: del a number 
;; of items starting from index x.
"];

function del(arr,x, byindex=false,count=1)=
let( x = byindex? fidx(arr, x):x )
(
	(len(arr)==0?[]
	: byindex
	  ? [for(i=range(arr)) if (i<x || i>=x+count) arr[i] ]
	  : [for(k=arr) if (k!=x) k] 
//	  : concat( arr[0]==x? []:[arr[0]]
//				, del(shrink(arr),x, byindex=false)
//				)
	)
);

module del_test( ops )
{
	doctest
	(
		del
		,[ 
			 [ "[2,4,4,8], 4", del( [2,4,4,8], 4), [2,8] ]
			,[ "[2,4,6,8], 6", del( [2,4,6,8], 6), [2,4,8] ]
			,[ "[2,4,6,8], 2", del( [2,4,6,8], 2), [4,6,8] ]
			,[ "[3,undef, 4, undef], undef", del( [ 3,undef, 4, undef], undef) , [3,4] ]
			,"// byindex = true"
			,[ "[2,4,6,8], 0, true", del( [2,4,6,8], 0, true), [4,6,8] ]
			,[ "[2,4,6,8], 3, true", del( [2,4,6,8], 3, true), [2,4,6] ]
			,[ "[2,4,6,8], 2, true", del( [2,4,6,8], 2, true), [2,4,8] ]
			,"//"
			,"// New 2014.8.7: arg *count* when byindex=true"
			,"//"
			,[ "[2,4,6,8], 1, true", del( [2,4,6,8], 1, true), [2,6,8] ]
			,[ "[2,4,6,8], 1, true,2", del( [2,4,6,8], 1, true,2), [2,8] ,["rem","del 2 tiems"]]
			,[ "[2,4,6,8], 2, true,2", del( [2,4,6,8], 2, true,2), [2,4], ["rem","del only 1 'cos it's in the end."] ]
			,[ "[], 1, true", del( [], 1, true), [] ]
			

		 ]
		,ops
	);
}
//del_test(["mode",22]);
//doc(del);





//========================================================
delkey=[ "delkey", "h,k", "hash",  "Hash",
" Given a hash (h) and a key (k), delete the [key,val] pair from h. \\
"];
function delkey(h,key, _i_=0)=
(
     _i_>=len(h)? []
	 : concat( h[_i_]==key? []:[h[_i_], h[_i_+1]]
	         , delkey( h, key, _i_+2) 
			 )
);

module delkey_test( ops )
{
	scope=["h1", ["a",1,"b",2]
		  ,"h2", [1,10,2,20,3,30] ];
	h1 = hash( scope, "h1" );
	h2 = hash( scope, "h2");

	doctest
	(
	 delkey
	,[
	   ["[], ''b''", delkey([], "b"), [] ]
	 , ["h1, ''b''", delkey(h1, "b"), ["a",1] ]
	 , ["h1, ''c''", delkey(h1, "c"), ["a",1,"b",2] ]
	 , ["h2, 1", delkey(h2, 1), [2,20,3,30]]
	 , ["h2, 2", delkey(h2, 2), [1,10,3,30]]
	 , ["h2, 3", delkey(h2, 3), [1,10,2,20]]
	 ], ops,  scope
	);
}
//delkey_test( ["mode",22] );
//doc(delkey);


//========================================================
dels=["dels", "a1,a2", "array", "Array",
 " Given arrays a1, a2 delete all items in a2 from a1.
;;
;; Ref: arrItcp
"];

function dels(a1,a2)=
let( a2=isarr(a2)?a2:[a2] )
(
	[for(x=a1) if (index(a2,x)==-1) x] 
);

module dels_test( ops )
{
	doctest
	(
		dels
		,[ 
			 [ "[2,4,4,8], 4", dels( [2,4,4,8], 4), [2,8] ]
			,[ "[2,4,6,8], [4,6]", dels( [2,4,6,8], [4,6]), [2,8] ]
		 ]
		,ops
	);
}


//========================================================
det=["det","pq","array","Array,Math,Geometry",
" Given a 2-pointer (pq), return the determinant. "
];
module det_test(ops=["mode",13])
{
	doctest( det, 
	[
	]
	,ops );
}


function det(pq)=
(
	dot( pq[0], pq[0] ) * dot( pq[1],pq[1]) - pow( dot(pq[0],pq[1]),2)
//( N1 . N1 ) ( N2 . N2 ) - ( N1 . N2 )2
);

//========================================================
Dim2=[
];

/*
   Object definition:

  guideP                                 guideQ
   |                                      |
   |  arrowP                       arrowQ |
   |  _='   lineP            lineQ  '=_   |  
   |-'-------------  label ------------'- |
   | '-_          |<- gap->|         _-'  |
   |    '                           '     |
     <-- spacer                             <-- spacer      
   P--------------------------------------Q 
   
   gap, gapP, gapQ are prop of Dim2
   
   Dim2:guide controls guideP and guideQ, which could be overwritten 
   by Dim2:guideP, Dim2:guideQ
   
   
   
*/


module Dim2( pqr, ops=[])
{ pqr = len(pqr)>2?pqr:concat( pqr, [randPt()]);
 _mdoc("Dim2"
, "
");
    
    df_ops=   
    
        [ "r"     , 0.015  // rad of line
        , "color" , "blue"  
        , "transp", 0.4    
        , "spacer", 0.2    // dist btw guide and target   
        , "gap"   , 0.4    // gap between two arrows
        , "h", 0.5  //  vercal pos of line.  Could be negative
                    /* h ________      0.5         0
                        |   1    |  |_______|  |       |
                        |        |  |       |  |_______|
                        P        Q  P       Q  P       Q
                    */
        , "vp"    , true   // is following viewport ?
        , "conv", undef
        , "decimal", undef
        , "maxdecimal", undef
        
        , "followslope", true   // 
        /*  
            followslope= true:  R decides the plane and side  
                /               where the dim line lies.   
               /'-._
              /     '-._  /
             P'-._      '/
             |    '-._  /        
             |        'Q
             |         | 
             |         R 

            false:

             |_________|   R further decides direction of guides
             |         |   and dim line. 
             P'-._     |
             |    '-._ |        
             |        'Q
             |         | 
             |         R 
        */
        
        // Subunit settings:
        
        // Set to true to use default ops. false to hide. hash
        // to customize. For label, string to replace dim value.
        
        , "label", true  // false or hash, or text
        
        , "guides", true  // false or hash
        , "guideP", true  // false or hash
        , "guideQ", true  // false or hash

        , "arrows", true  // false or hash
        , "arrowP", true  // false or hash
        , "arrowQ", true  // false or hash

        ];        
    // 3 subunits: guides, arrow, label
    
    df_guides= ["len", 0.8];
    df_arrows= ["headP",false];
    df_label=
        [ "scale",0.4
        , "prefix",""
        , "suffix",""
        , "conv", undef
        , "decimal", undef
        , "maxdecimal", undef
        , "text", undef // set to string replace df value 
        ];
        
    /* subunit "arrow" composes of 2 Arrow modules, one points to
       hP, one hQ. Seperated by gap. The ops is designed as though
       that the entire df_arrow is one Arrow module, 'cos it's 
       unlikely for the two arrows to be formatted differently:
    
        |    arrowP                   arrowQ   |
        |  _='                           '=_   |  
      hP|-'-------------  label ------------'- |hQ
        | '-_          |<- gap->|         _-'  |
        |    '                           '     |
           <-- spacer                             <-- spacer      
        P--------------------------------------Q 
        
        Note that on each of the two Arrows, it's own internal
        arrowP (not the arrowP of this Dim module)  is disabled. 
        */

    _ops = update( df_ops, ops); // df_ops updated w/ user_ops
    function ops(k,df) = hash(_ops,k,df);
   
    //
    //================================================= sub ops 
    //
    
    function _getsubops_( sub_dfs )=
        getsubops( ops, df_ops= df_ops
        , com_keys= ["r", "color", "transp"]
        , sub_dfs= sub_dfs
        );
        
    ops_guideP= _getsubops_( ["guides", df_guides, "guideP", [] ] );
    ops_guideQ= _getsubops_( ["guides", df_guides, "guideQ", [] ] );
    ops_arrowP= _getsubops_( ["arrows", df_arrows, "arrowP", [] ] );
    ops_arrowQ= _getsubops_( ["arrows", df_arrows, "arrowQ", [] ] );
    function ops_guideP(k)= hash( ops_guideP, k); 
    function ops_guideQ(k)= hash( ops_guideQ, k);
    function ops_arrowP(k)= hash( ops_arrowP, k);
    function ops_arrowQ(k)= hash( ops_arrowQ, k);

    _label = ops("label");
    ops_label = isstr(_label)
            ? concat(["text", _label], df_label)   
            : ishash(_label)
            ? concat(_label, df_label )
            : df_label;

    function ops_label(k)= hash( ops_label, k); 

    //
    //================================================= var
    //
    
    r     = ops("r");
    color = ops("color");  
    transp= ops("transp");    
    spacer= ops("spacer");
    gap   = ops("gap");
    h     = ops("h");
    followslope= ops("followslope");
    
    a_pqr = angle(pqr);
    
    //
    //================================================= calc pts
    //
    
    P = pqr[0];
    Q = pqr[1];
    Ro= pqr[2];
    
    /* When followslope = true, R has to be recalc
       such that new a_PQR = 90

            /  
           /'-._
          /     '-._  /
        P'-._       '/
         |    '-._  /        
         |        'Q
         |        /| 
         |       / Ro
                R
            /                    
           /'-._
          /     '-._  /
        Q'-._       '/
        /|    '-._  /        
       / |        '| P
      R  |         | 
         Ro                                          
    */
    R = followslope
         ? ( a_pqr==90
             ? Ro 
             : anglePt( pqr, a=90)
           )  
         : Ro ;  
       
     /*
    P----Q   Need X to make pts along XP line
    |    |
    X----R
    */   
    X= cornerPt( [P,Q,R] ); 
       
    dPQ = norm(P-Q);
    L = (!followslope) && a_pqr!=90         // L= the dim to be reported
        ? norm( P- othoPt(p201(pqr))):dPQ;
    
    //-------------------------------------------- guide 
    /*
            gP2         gQ2  --- 
             |           |    |
          hP +-----------+ hQ | g_len  
             |           |    |
         gP0=gP1         gQ1 ---
    spacer-->            |    |
             Po          |    | gQx_len  
             | '-_       |    |  
             X    '-_   gQ0  ---       
             |       '-_    <--- spacer    
             |          'Qo 
             |           |
             |           Ro     
         
    gPx=0 @ followslope=true, or a_PQR>=90
    gQx=0 @ followslope=true, or a_PQR<=90
    */ 
    h = ops("h");
    //echo("h",h);
    
    g_len = hash( ops_guideP, "len", hash(df_guides,"len") );
    
    // extra len of guides due to slope of PQ
    gPx_len= (!followslope)&& a_pqr<90? shortside( L, dPQ ): 0;
    gQx_len= (!followslope)&& a_pqr>90? shortside( L, dPQ ): 0;
    
    gP0 = onlinePt( [P,X], len= -spacer );
    gP1 = onlinePt( [P,X], len= -spacer - gPx_len );    
    gP2 = onlinePt( [P,X], len= -spacer - gPx_len -  g_len);    
    hP  = onlinePt( [gP1,gP2], ratio= h);
    
    gQ0 = onlinePt( [Q,R], len= -spacer );
    gQ1 = onlinePt( [Q,R], len= -spacer - gQx_len );    
    gQ2 = onlinePt( [Q,R], len= -spacer - gQx_len -  g_len);    
    hQ  = onlinePt( [gQ1,gQ2], ratio= h);

    //echo("ops(''guides'')", ops("guides"));
    //echo("ops_guideP, ops_guideQ",ops_guideP, ops_guideQ);
    if( ops_guideP ) Line( [gP0, gP2], ops_guideP); 
    if( ops_guideQ ) Line( [gQ0, gQ2], ops_guideQ); 
   
    /* 
    Allows arrow heads to be turned off from the obj level. Each arrow 
    (of arrowP, arrowQ) is byteself an Arrow module, in which the dir is
    decided by it's start/end pts, named, P,Q, internally (lets call them 
    iP, iQ here). In Dim, they are defined as pointing from the gap outward:
    
         |    arrowP                   arrowQ  |
         |  _='                           '=_  |  
    hP=iQ|-'------------iP      iP-----------'-| hQ=iQ
         | '-_          |<- gap->|         _-' |
         |    '                           '    |
          <-- spacer                            <-- spacer      
         P-------------------------------------Q 
    
    So on the Dim level, we have a property arrows containing two Arrows,
    and each have its own headP, headQ. 
    
    The default of Dim is to hide the headP of Arrow module (defined in df_arrows).
    
    To turn off the arrow head of arrowP (of Dim) in Dim, we can use: 
    
    [1]  Dim( ...[ "arrowP"          <=== arrowP of Dim
                , ["headQ", false] <=== define the arrowQ of arrowP
                ]
           );      
    
    We make this possible: 
    
    [2]  Dim( ...[ "arrowP"          <=== arrowP of Dim
                , ["head", false]  <=== define the arrowQ of arrowP
                ]
           );    
    
    That is, no need to specify headQ. You can also replace false with 
    a valid hash to set the arrow head. 
    
    Both [1] and [2], or even ["heads",...], give the same result.
    
    */

    //
    //================================================= line
    //
    
    // hP, hQ: where line intersect guides
    // gapP, gapQ: where the arrow line starts. 
    //             determined by ["line",["gap",0]]   
    /*
         gP2                    gQ2
          |                      |
       hP +-----gapP    gapQ-----+ hQ
          |                      |
         gP1                    gQ1    
    
       But, remember that the real hP needs to take
       into account the len of arrow head
    
             _-'| 
       _hP <----+--------- gapP
             '-_| hP 
    
           |<-->| len of arrow head 
    
    */

    gap = ops("gap"); 
    lineL = (L-gap)/2;
    
    begP = onlinePt( [hP,hQ], len=lineL);
    begQ = onlinePt( [hQ,hP], len=lineL);

    if(ops_arrowP) Arrow( [ begP, hP], ops_arrowP );
    if(ops_arrowQ) Arrow( [ begQ, hQ], ops_arrowQ );
         
    //       
    //================================================= label
    //
    
	module transform(followVP=ops("vp"))
	{
        echo("vp", followVP);
		translate( midPt( [hP,hQ] ) )
	     color( ops("color"), ops("transp") )
		if(followVP){ rotate( $vpr ) children(); }
		else { rotate( rotangles_z2p( Q-P ) )
			  rotate( [ 90, -90, 0] )
			  children();
			}
	}
	conv = ops("conv");
	//echo("conv",conv, "L", L, "numstr(L, uconv=uconv)", numstr(L, conv=conv));
	transform()
	scale(0.04) text(str(ops_label("prefix")
						  //, L, "="
						  , numstr(L
								, conv=ops("conv")
								, d=ops("decimal")
								, maxd=ops("maxdecimal")
								)
						  , ops_label("suffix") )
                    , valign="center"
                    , halign="center");
   
         
}// Dim2
    
module Dim2_demo_1()
{ a=0;
}


module Dim2_demo_1_plane()
{ _mdoc("Dim2_demo_1_plane"
,  "Show some ways to measure PQ:    
;; blue : Dim2( [P,Q,R] )
;; red  : Dim2( [P,Q,V] )
;; green: Dim2( [P,Q,U] )
");    
   x=3; y=2; z=2;  
   P = [0,y,z];
   Q = [x,y,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y,0];
   V = [x,0,0];
   W = ORIGIN; 
   pts =  [P,Q,R,S,T,U,V,W];
    
   color(undef,0.5) polyhedron( points =pts, faces=CUBEFACES );
    
   MarkPts( pts, ["r",0.05] );
   LabelPts(pts, "PQRSTUVW");
  

   module Label(pqr, txt, color){
       LabelPt( pqr, txt, ["color",color, "scale", 0.015] );
   }
   
   Dim2( [P,Q,R], ["color","green"]);
   Label( [0,y-0.1,z+1.2], "Dim2([P,Q,U]) or Dim2([P,Q,T])",  "blue"); 

   Dim2( [P,Q,V], ["color","red"] );
   Label( [-0.2,y+0.6,z+0.7], "Dim2([P,Q,V],[\"color\",\"red\"])",  "red"); 

   Dim2( [P,Q,U]);
   Label( [0,y+1,z+0.1],"Dim2([P,Q,R],[\"color\",\"green\"])"
            , "green");
}

module Dim2_demo_2_color()
{_mdoc("Dim2_demo_2_color", "");
   x=3; y=2; z=3;  
   P = [0,y,z];
   Q = [x,y,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y,0];
   V = [x,0,0];
   W = ORIGIN; 
   pts =  [P,Q,R,S,T,U,V,W];
    
  color(undef,0.5) polyhedron( points =pts, faces=CUBEFACES );
    
   MarkPts( pts );
   LabelPts(pts, "PQRSTUVW");
  
   Dim2( [R,Q,U], ["color","red"]);
   Dim2( [R,S,P], ["guides",["color","red"]] );  
   Dim2( [P,Q,R], ["arrows",["color","red"]]);
    
   Dim2( [P,S,T], ["color","red"
                  ,"arrows", ["color","green"]
                  ,"arrowP", ["color","yellow"]
                  ] );

   Dim2( [P,S,Q], ["color","red"
                  ,"guides", ["color","green"]
                  ,"guideP", ["color","yellow"]
                  ] );
   module Label(pqr, txt, color){
       LabelPt( pqr, txt, ["color",color, "scale", 0.015] );
   }
   
   Label( [x,0,z+1.2], "Dim2([R,Q,U],[\"color\",\"red\"])", "red");
   Label( [x,-2.5,z],"[R,S,P],[\"guides\",[\"color\",\"red\"]] ","blue");
   Label( [2*x/3, y,z], "[P,Q,R],[\"arrows\",[\"color\",\"red\"]]","blue");
   Label( [0,0,z+1], 
        "...[\"color\",\"red\",\"arrows\", [\"color\",\"green\"]","green");
   Label( [0,0,z+.7], 
        "      ,\"arrowP\", [\"color\",\"yellow\"]]","green");
   Label( [-1,-2, z], "...[\"color\",\"red\",\"guides\", [\"color\",\"green\"],\"guideP\", [\"color\",\"yellow\"]]", "blue");

}    

module Dim2_demo_3_followslope()
{ _mdoc("Dim2_demo_3_followslope", "");
   x=3; y=2; z=3;  
   P = [0,y,z];
   Q = [x,y/2,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y/2,0];
   V = [x,0,0];
   W = ORIGIN; 
   A = [0, y, 2*z/3];
   B = [x, y/2, 2*z/3];
   C = [x, 0, 2*z/3];
   D = [0, y, z/3];
   E = [x, y/2, z/3];
   F = [x, 0, z/3];
    
   pts =  [P,Q,R,S,T,U,V,W, A,B,C,D,E,F];
    
  color(undef,0.5) polyhedron( points =pts, faces=CUBEFACES );
    
   //MarkPts( pts );
   LabelPts(pts, "PQRSTUVWABCDEF");
  
   //Dim2( [P,Q,R]);  
    
    Dim2( [P,Q,R], ["followslope", true] );
    Dim2( [A,B,C], ["followslope", false, "color", "red"] );
    Dim2( [E,D,F], ["followslope", true] );
    Dim2( [U,T,W], ["followslope", false] );

   module Label(pqr, txt, color="blue"){
       LabelPt( pqr, txt, ["color",color, "scale", 0.015] );
   }
  
   Label( [0.2, y+0.5,z], "Dim2([P,Q,R],[\"followslope\",true])");
   Label( [0.2, y+0.5, 2*z/3], "Dim2([A,B,C],[\"followslope\",false])","red");
   
}    

module Dim2_demo_3_hide()
{ _mdoc("Dim2_demo_3_hide", "");
   x=3; y=2; z=3;  
   P = [0,y,z];
   Q = [x,y/2,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y/2,0];
   V = [x,0,0];
   W = ORIGIN; 
   A = [0, y, 2*z/3];
   B = [x, y/2, 2*z/3];
   C = [x, 0, 2*z/3];
   D = [0, y, z/3];
   E = [x, y/2, z/3];
   F = [x, 0, z/3];
    
   pts =  [P,Q,R,S,T,U,V,W, A,B,C,D,E,F];
    
  color(undef,0.5) polyhedron( points =pts, faces=CUBEFACES );
    
   //MarkPts( pts );
   LabelPts(pts, "PQRSTUVWABCDEF");
  
   //Dim2( [P,Q,R]);  

   module Label(pqr, txt, color="blue"){
       LabelPt( pqr, txt, ["color",color, "scale", 0.015] );
   }
    Dim2( [P,S,R], ["arrows",false] );
    Label( [-1,0,z], "[P,S,R],[\"arrows\",false]");
      
    Dim2( [P,S,T], ["arrowP",false, "color","red"] );
    Label( [0,0,z+1], "[P,S,T],[\"arrowP\",false]", "red");

    //===================
    Dim2( [W,V,U], ["guides",false] );
    Label( [0,-3,0], "[W,V,U],[\"guides\",false]");   
   
    Dim2( [W,V,P], ["guideP",false, "color","red"] );
    Label([ x/3, -2.5,-.8], "[W,V,P],[\"guideP\",false]","red");
   
    Dim2( [W,V,R], ["guideQ",false, "color","green"] );
    Label( [x/2,0,-1.2], "[W,V,R],[\"guideQ\",false]","green");   
    
    //===================
    Dim2( [V,U,T], ["color","purple"
                   , "arrowP",["heads",false]
                   , "arrowQ",["heads", ["r", 0.1]]
                   ]
        );
    Label( [ x+0.5, y/2, 0]
            , "[V,U,T],[\"arrowP\",[\"heads\",false]", "purple");
    Label( [ x+0.5, y/2, -0.35]
            , "        ,\"arrowQ\",[\"heads\",[\"r\",0.1]] ]", "purple");
   
//    Dim2( [P,Q,R], ["followslope", true, ["guardP",false]] );
//    Dim2( [A,B,C], ["followslope", false, "color", "red", ["guardP",false]] );
//    Dim2( [E,D,F], ["followslope", true, ["guardQ",false]] );
//    Dim2( [U,T,W], ["followslope", false, ["guardQ",false]] );


  
//   Label( [0.2, y+0.5,z], "Dim2([P,Q,R],[\"followslope\",true])");
//   Label( [0.2, y+0.5, 2*z/3], "Dim2([A,B,C],[\"followslope\",false])","red");
   
}    

//Dim2_demo_1_plane();
//Dim2_demo_2_color();
//Dim2_demo_3_hide();
Dim2_demo_3_followslope();

//========================================================
Dim=["Dim","pqr,ops","n/a","Geometry",
" Given a 3-pointer (pqr), with angle P-Q-R = 90, draw dimention line to specify
;; the len of PQ. Flanked by headguide/tailguide that can be turned off. The pt, R, 
;; defines the plane where line is. R must be on the tailguide toward the target T.
;; i.e., T-R-Q, not T-Q-R. If the angle is set to:
;; (1) integer, a new pt, X, is created to which the tailguide wil extend to
;; (2) true, then R is taken as a point on the target, and the tailguide will be
;;     extended to reach R. Or, if angle is given as an integer, ...
;; ops:
;; [ ''prefix'',''''
;; , ''suffix'',''''
;; , ''viewport'', true
;; , ''angle'', 0
;; , ''spacer'', 0.5
;; , ''offsetratio'', 0.5
;; , ''guidelen'', 1
;; , ''headguide'', true
;; , ''tailguide'', true
;; , ''lineops'', []
;; , ''conv'', undef
;; , ''decimal'', undef
;; ]
;; (1) Default:
;;  |     3.5       |      
;;  |< ----------- >| ---   
;;  |               |  | offsetratio
;;                     |   //:spacers
;;  P---------------Q ---              
;;
;; (2) angle = integer:
;;  |     3.5       |      
;;  |< ----------- >|            
;;                  |           
;;  P - - - -       Q                  
;;   '-._a          |  
;;       '-._       |
;;           '-._      
;;               '-.X (X is calculated)
;;
;; (3) angle= true:  
;;
;;  |     3.5       |      
;;  |< ----------- >|   
;;  |               |  
;;                  |  
;;  P - - - -       Q                  
;;   '-._a          |  
;;       '-._       |
;;           '-._      
;;               '-.R
"];
module Dim_test( ops=["mode",13] ){ doctest(Dim, ops=ops); }

module Dim(pqr, ops=[])
{
	ops=concat(ops,
	[ "r", 0.015
	, "color", "blue"
	, "transp", 0.5
	, "prefix",""
	, "suffix",""
	, "vp", true   // is following viewport ?
	, "a", 0
	, "spacer", 0.2
	, "offsetratio", 0.5
	, "guidelen", 1
	, "headguide", true
	, "tailguide", true
	//, "dataformat", "normal" // "normal", "ft_ftin"
	, "lineops", []
	// new 7/13/14
	, "conv", undef
	, "decimal", undef
	, "maxdecimal", undef
	]);
//    OPS=[ "r" , 1
//	, "h", 1
//	, "w", 1
//	, "len",1 
//	, "color", false 
//	, "transp", 1
//	, "fn", $fn
//	, "rotate", 0
//	, "rotates", [0,0,0]
//	, "translate", [0,0,0]	 
//	];
	//echo(ops);
	function ops(k) = hash(ops,k);
	r = ops("r");
	a = ops("a");
     //color = ops("color");
     //transp= ops("transp");
     lineops= ops("lineops");
	guidelen = ops("guidelen");
	spacer   = ops("spacer");
	offsetratio = ops("offsetratio");

	// R must be on the tailguide
	// toward the target. i.e., target-R-Q, not target-Q-R.
	// 
	// headguide      tailguide
	//  |     3.5       |      
	//  |< ----------- >|  .--   
	//  |               |  |__ offsetratio
	//                  |  
	//  P - - - -       Q                  
	//   '-._a          |  
	//       '-._       |
	//           '-._      
	//  S            '-.R (when a=true) or X (when a=int)

	L = norm( pqr[0]-pqr[1] );
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];
	//S= squarePts( pqr, ["mode",4] )[3];
	S= squarePts( pqr )[3];

	//  head_p0       tail_p0
	// 
	//   |     3.5       |      
	//   |< ----------- >|   
	//   |               |  
	//                   |
	//  head_p1          |  
	//                   |                  
	//                   |  
	//                   
	//                tail_p1

	head_p0 = onlinePt( [P,S], len=-(guidelen+spacer) );
	head_p1 = onlinePt( [head_p0, P], len= guidelen   );
	tail_p0 = onlinePt( [Q,R], len=-(guidelen+spacer) );
	QX = angle==0? 0
		: str(angle)=="true"? norm(Q-R)
			:tan(a)*L;
	tail_p1 = onlinePt( [Q,R], len= QX-spacer );
	
	if(ops("headguide")) Line0( [ head_p0, head_p1 ],ops );
	if(ops("tailguide")) Line0( [ tail_p0, tail_p1 ],ops );		
		
	// the dimension line:
	
	L0 = onlinePt( [head_p0,head_p1], ratio= 1-offsetratio );
	L1 = onlinePt( [tail_p0,tail_p1], len= guidelen*(1-offsetratio) );

	arrowops = ["style","cone","r",0.1, "len",0.4];
	
	Line( [L0,L1], concat( ["head", arrowops
						,"tail", arrowops]
						, lineops, ops ) );

	//include <../others/TextGenerator.scad>
	//rot= rotangles_z2p( Q-P );
	//translate( midPt( [L0,L1] ) )
	//rotate( rot ) //rotangles_z2p( Q-P ) )
	//rotate([0,90,0])
	//rotate([0,0,90] )
	//translate( [ 0, -0.4, 0] )
//	color( ops("color"), ops("transp") )
	
//	fmt = ops("conv");
//	function getoutput(L, fmt=fmt)=
//	(
//		fmt=="ft_ftin"?
//			str( floor(L), "'"
//				, isequal(L, floor(L))?
//				  "":str( (L-floor(L))*12, "\"")
//				)
//			:L	
//	);

	module transform(followVP=ops("vp"))
	{
		translate( midPt( [L0,L1] ) )
	     color( ops("color"), ops("transp") )
		if(followVP){ rotate( $vpr ) children(); }
		else { rotate( rotangles_z2p( Q-P ) )
			  rotate( [ 90, -90, 0] )
			  children();
			}
	}
	conv = ops("conv");
	//echo("conv",conv, "L", L, "numstr(L, uconv=uconv)", numstr(L, conv=conv));
	transform()
	scale(0.04) text(str(ops("prefix")
						  //, L, "="
						  , numstr(L
								, conv=ops("conv")
								, d=ops("decimal")
								, maxd=ops("maxdecimal")
								)
						  , ops("suffix") ));

	//Line_test(["mode", 10]);
}
//echo( 3.69461, numstr(3.69461, conv="ft:ftin") );
//echo(  numstr( 0.5,conv="ft:ftin" ) );

module Dim_demo_basic_1()
{ _mdoc("Dim_demo_1"
, "Show some ways to measure PQ:    
;; blue : Dim( [P,Q,R] )
;; red  : Dim( [P,Q,V] )
;; green: Dim( [P,Q,U] )
");    
   x=3; y=2; z=3;  
   P = [0,y,z];
   Q = [x,y,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y,0];
   V = [x,0,0];
   W = ORIGIN; 
   pts =  [P,Q,R,S,T,U,V,W];
    
   polyhedron( points =pts, faces=CUBEFACES );
    
   MarkPts( pts );
   LabelPts(pts, "PQRSTUVW");
  
   Dim( [P,Q,R] );
   color("red") Dim( [P,Q,V] );
   color("green") Dim( [P,Q,U]);
   
   
}

module Dim_demo_basic_2()
{ _mdoc("Dim_demo_1"
, "Show some ways to measure PQ:    
;; blue : Dim( [P,Q,R] )
;; red  : Dim( [P,Q,S] )
;; green: Dim( [P,Q,T] )
");    
   x=3; y=2; z=3;  
   P = [0,y,z];
   Q = [x,y/2,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y/2,0];
   V = [x,0,0];
   W = ORIGIN; 
   pts =  [P,Q,R,S,T,U,V,W];
    
   polyhedron( points =pts, faces=CUBEFACES );
    
   MarkPts( pts );
   LabelPts(pts, "PQRSTUVW");
  
   Dim( [P,Q,R] , ["a", 90-angle([Q,P,S])] );
   //color("red") Dim( [P,Q,S] );
   //color("green") Dim( [P,Q,T]);
} 


module Dim_demo_2()
{
	//----------------------------------------
	module demo(ops=[])
	{
		pts = randPts(3);
		op = othoPt( pts );
		pqr = [ pts[0], op, pts[1] ];
		c = hash(ops,"color");

		MarkPts( pqr );
		//Line0( [pqr[0], pqr[1]], ["color",c] );
		Chain( pqr, ["closed",true, "color",c, "r",0.02, "transp",0.3] );
		Dim( pqr, ops ); //["color",color, "angle", angle] );
		//Line0( [pqr[0], pqr[1]], ["color",color] );
		//Dim( pqr, ["color",color, "angle", angle] );

		//Chain( squarePts( pqr ),["transp",0.2] );
	
	}
	//----------------------------------------
	demo( );
	demo( ["color","green"] );
	//demo( ["color","red", "suffix", "(angle=0 default)", "vp", false] );

	//demo( ["angle",true, "color","green", "suffix", "(angle=true)"] );
	//demo( ["angle", 30, "color","blue","suffix", "(angle=30)"] );
}
//Dim_demo_1();

module Dim_demo_3()
{
	//----------------------------------------
	module demo(ops=[])
	{
		pts = randPts(3);
		op = othoPt( pts );
		pqr = [ pts[0], op, pts[1] ];
		c = hash(ops,"color");

		MarkPts( pqr );
		//Line0( [pqr[0], pqr[1]], ["color",c] );
		Chain( pqr, ["closed",true, "color",c, "r",0.02, "transp",0.3] );
		Dim( pqr, ops ); //["color",color, "angle", angle] );
		//Line0( [pqr[0], pqr[1]], ["color",color] );
		//Dim( pqr, ["color",color, "angle", angle] );
	}
	//----------------------------------------
	//demo( );
	demo( ["color","red", "conv","ft:ftin", "decimal", 2] );
	demo( ["color","green", "conv","cm:in", "suffix", "cm:in"] );
	demo( ["color","blue", "conv","ft:cm", "suffix","ft:cm", "decimal", 2] );
	//demo( ["angle",true, "color","green", "suffix", "(angle=true)"] );
	//demo( ["angle", 30, "color","blue","suffix", "(angle=30)"] );
}


module Dim_demo()
{
	Dim_demo_basic_1();
	//Dim_demo_basic_2();
	//Dim_demo_2();
	//Dim_demo_3();
}
//Dim_demo();


//========================================================
dist=["dist","pq","number","Math, Geometry",
"
 Given a 2-pointer (pq), return the distance between p,q.
"];
function dist(pq) = norm(pq[1]-pq[0]);  // Get distance between a 2-pointer
module dist_test( ops )
{
	pq = randPts(2);
	p = pq[0]; q=pq[1];
	d = sqrt( pow(p.x-q.x,2)+pow(p.y-q.y,2)+pow(p.z-q.z,2) );
	doctest( dist, 
	[
		["pq", dist(pq), d]
	]	,ops, ["pq",pq]
	);
}

//========================================================
distPtPl=["distPtPl", "pt,pqr", "number", "Math, Geometry, 3D" ,
 " Given a pt and a 3-pointer (pqr) , return the signed distance between
;; the plane formed by pqr and the point.
"];
	
/*
	Pt = [x0, y0, z0 ]
	Pl = Ax + By + Cz + D = 0;

 	      Ax0 + By0 + Cz0 + D
	d= --------------------------
		sqrt( A^2 + B^2 + C^2 ) = norm([A,B,C])

	pc = planecoefs( pqr ) = [A,B,C,D]

	Ax0 + By0 + Cz0 + D=  [pc[0],pc[1],pc[2]] * Pt  + pc[3] = 

*/
function distPtPl(pt,pqr)=
(
	( planecoefs(pqr, has_k=false)* pt  + planecoefs(pqr)[3]) 
	/ norm(	planecoefs(pqr, has_k=false) )
);

module distPtPl_test( ops )
{
	pt= [1,2,0];
	pqr = [ [-1,0,2], [1,0,-3], [2,0,4]];
	//echo( "planecoefs({_}, false) =", planecoefs(pqr));
    //echo( pt * planecoefs(pqr, has_k=false) );
	
	doctest
	(
		distPtPl
		,[
			 ["pt,pqr", distPtPl(pt,pqr), 2 ]
		], ops, [ "pt", pt
		  , "pqr", pqr ]	
	);
}

module distPtPl_demo()
{
	pt = randPts(1);
	pqr= randPts(3);
}
//distPtPl_test(["mode",22]);
//doc(distPtPl);




//========================================================
distx = ["distx", "pq", "number", "Math, Geometry",
" Given a 2-pointer pq, return difference of x between pq. i.e., 
;;
;; distx(pq) = pq[1][0]-pq[0][0]
"];

function distx(pq)= pq[1][0]-pq[0][0];

module distx_test( ops )
{
	pq = [[0,3,1],[4,5,-1]];
	pq2= reverse(pq);
	
	scope=["pq", pq, "pq2",pq2];

	doctest( distx
			,[ 
				[ "pq", distx(pq), 4 ]			   		
				,[ "pq2", distx(pq2), -4 ]			   		
			 ],ops, scope
		   );
}
//distx_test(["mode",22]);
//doc(distx);

//========================================================
disty= ["disty", "pq", "number", "Math, Geometry",
" Given a 2-pointer pq, return difference of y between pq. i.e.,
;;
;; disty(pq) = pq[1][1]-pq[0][1]
"];

function disty(pq)= pq[1][1]-pq[0][1];

module disty_test( ops )
{
	pq = [[0,3,1],[4,5,-1]];
	pq2= reverse(pq);
	
	scope=["pq", pq, "pq2",pq2];

	doctest( disty
			,[ 
				[ "pq", disty(pq), 2 ]			   		
				,[ "pq2", disty(pq2), -2 ]			   		
			 ],ops,  scope
		   );
}
//disty_test(["mode",22]);
//doc(disty);

//========================================================
distz= ["distz", "pq", "number", "Math, Geometry",
" Given a 2-pointer pq, return difference of z between pq. i.e.,
;;
;; distz(pq) = pq[1][2]-pq[0][2]
"];

function distz(pq)= pq[1][2]-pq[0][2];

module distz_test( ops )
{
	pq = [[0,3,1],[4,5,-1]];
	pq2= reverse(pq);
	
	scope=["pq", pq, "pq2",pq2];

	doctest( distz
			,[ 
				[ "pq", distz(pq), -2 ]			   		
				,[ "pq2", distz(pq2), 2 ]			   		
			 ],ops,  scope
		   );
}
//distz_test(["mode",22]);
//doc(distz);


//========================================================
echoblock=[ "echoblock", "arr", "n/a", "Doctest, Console" , 
" Given an array of strings, echo them as a block. Arguments:
;;
;;   indent: prefix each line
;;   v : vertical left edge, default ''| ''
;;   h : horizontal line on top and bottom, default ''-''
;;   lt: left-top symbol, default ''.''
;;   lb: left-bottom symbol, default \"'\"
"];
module echoblock(arr, indent="  ", v="| ",t="－",b="－", lt=".", lb="'")
{
	lines = arrblock(arr, indent=indent, v=v,t=t,b=b, lt=lt, lb=lb);

	//for ( line= lines )echo(line);
	//echo(  str("<span style='font-family:Droid Sans Mono'><br/>", join(lines, "<br/>" ), "<br/></span>"));
	_Div( join(lines,"<br/>")
		, "font-family:Droid Sans Mono"
		);  
}
 
module echoblock_test( ops ){ doctest( echoblock, ops=ops); }

module echoblock_demo()
{
	echom( "echoblock_demo" );
	echo( "echoblock([\"line1\",\"line2\",\"line3\"]) shows:");
	echoblock(["line1","line2","line3"]);
}
//echoblock_demo();
//doc(echoblock);
//echo( join( ["<br/>","line1","line2","line3"], "<br/>"));


//========================================================
echo_=["echo_", "s,arr=undef", "n/a", "Console",
" Given a string containing one or more ''{_}'' as blanks to fill,
;; and an array (arr) as the data to fill, echo a new s with all
;; blanks replaced by arr items one by one. If the arr is given as 
;; a string, it is the same as echo( str(s,arr) ).
;;
;; Example usage:
;;
;; echo_( ''{_}.scad, version={_}'', [''scadex'', ''20140531-1''] );
"];


module echo__test( ops ){ doctest( echo_, ops=ops); }

module echo_(s, arr=undef)
{
	if(arr==undef){ echo(s); }
	else if (isarr(arr)){
		echo( _s(s, arr));
	}else { echo( str(s, arr) ); }
}

module echo__demo()
{
	echo_("echox");
	echo_("{_},{_}", ["name","scadex"]);
	echo_("{_}", ["scadex"]);
}
//echo__demo();
//doc(echo_);



//========================================================
echoh=["echoh","s,h,showkey","n/a", "Hash, Console",
" Given a string template (s) containing ''{key}'' where key is a key of
;; a hash, and a hash (h), return a new string in which all keys in {??}
;; are replaced by their corresponding values in h. If showkey is true, 
;; replaced by key=val.
"]; 

module echoh(s, h, showkey) 
{  echo( _h(s, h, showkey=showkey));
}

module echoh_test(ops){ doctest( echoh, ops=ops);}

module echoh_demo()
{
	echoh("{name},{ver}", ["name","scadex","ver",3.3]);
	echoh("{name},{ver}", ["name","scadex","ver",3.3], true);
}
//echoh_demo();



//========================================================

module echom(s)
{
	echo_( "--------<  {_}  >--------", [s] );
}

module echom_demo()
{
	echom("echom");
}

//echom_demo();
//accumulate_test(["mode",2]);


//========================================================
endwith=[ "endwith", "o,x", "T/F",  "String, Array, Inspect",
"
 Given a str|arr (o) and x, return true if o ends with x.
;;
;; New 2014.8.11:
;;
;; x can be an array, return true if o ends with any item in x.
"];

function endwith(o,x)= 
let( x=isarr(x)?x:[x] )
( 
	isarr( o )
	? sum( [for(xx=x) last(o)==xx?1:0 ] )>0
	: isstr( o )
	  ? sum( [for(xx=x) slice(o,-len(xx))==xx?1:0 ] )>0
	  : false
);

module endwith_test( ops )
{
	doctest
	(
	 endwith
	,[ 
	    [ "''abcdef'', ''def''", endwith("abcdef", "def"), true ]
	  , [ "''abcdef'', ''def''", endwith("abcdef", "abc"), false ]
	  , [ "[''ab'',''cd'',''ef''], ''ef''",  endwith(["ab","cd","ef"], "ef"),true ]
	  , [ "[''ab'',''cd'',''ef''], ''ab''",  endwith(["ab","cd","ef"], "ab"),false ]
	  , [ "33, ''def''", endwith(33, "abc"), false ]
	  , "// New 2014.8.11"
  	, [ "''abcdef'', [''def'',''ghi'']", endwith("abcdef", ["def","ghi"]), true ] 
  	, [ "''abcghi'', [''def'',''ghi'']", endwith("abcghi", ["def","ghi"]), true ] 
  	, [ "''abcddd'', [''def'',''ghi'']", endwith("aaaddd", ["def","ghi"]), false ] 
 	, [ "[''ab'',''cd'',''ef''], [''gh'',''ef'']", endwith(["ab","cd","ef"], ["gh","ef"]),true]	
	  ], ops
	);
}
//endwith_test(["mode",22]);
//doc(endwith);




//========================================================
expandPts=["expandPts", "pts,dist=0.5,cycle=true", "points", "Points, Geometry",
" Given an array of points (pts), return an array of points that *expand* every 
;; point outward as indicated by Q ==> Q2 in the graph below:
;;
;; d = dist
;;
;; ---------------------Q2
;;                     /
;;                    /
;; P----------Q_     /
;;           /  '-_ /
;;          /   d  :
;;         /      /
;;        /      /
;;       R      /
"
];


/*
 	d = dist
 
   ---------------------Q2
                       /        
                      /        
   P----------Q_     /
             /  '-_ / 
            /   d  : 
           /      / 
          /      / 
         R      /     



*/
module expandPts_test( ops=["mode",13] ){ doctest( expandPts, ops=ops ); }

function expandPts(pts,dist=0.5, cycle=true, _i_=0)=
 let( p3s = subarr( pts,cycle=cycle )
	)
//	, p3s = [for(ijk=i3s) pts[
(	// 
	[ for(i=range(pts))
		onlinePt( [pts[i], angleBisectPt( p3s[i] )]
				, len=-dist/sin(angle(p3s[i])/2)
			    )
	]
				  
//[for(ijk=tris)  
//	[ for(p=range(-1,len(pts)+1, obj=pts, cycle=true)) i ]

//	[ for(i=range(-1,len(pts)+1)) fidx(pts, i, cycle=true) ]

//	_i_<len(pts)?
//	concat( 
//		[ onlinePt
//           ( 
//			[ pts[_i_]
//			, angleBisectPt
//			  ( 
//				neighbors(pts,_i_, cycle=true) 
//			  )
//			], len=-dist/
//					sin(angle(neighbors(pts,_i_, cycle=true))/2)
//		  )// onlinePt
//		]
//	, expandPts( pts, dist, cycle, _i_+1)
//	):[]
);

module expandPts_demo_1_open()
{
	echom("expandPts");
	pts = randOnPlanePts( randPts(3), 5);
	pts = randPts(6);
	larger= expandPts(pts, dist=0.5);
	smaller= expandPts(pts,dist=-0.5);
	Chain( pts, ["color","red", "r",0.01, "closed",false] );
	//Chain( larger, ["color","red","transp",0.3]);
	//Chain( smaller);
	MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [pts[i], larger[i]], ["r",0.01, "head",["style","cone"]] );
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1), larger[i] ]);

	}
	//Dim( [pts[0], larger[0]] );
}

module expandPts_demo_2()
{
	echom("expandPts");
	pts = randOnPlanePts( 5 );
	pts = randPts(6);
	larger = expandPts(pts, dist=0.3);
	smaller= expandPts(pts, dist=-0.3);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	//Chain( larger, ["color","red","transp",0.3]);
	//Chain( smaller);
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}
	//Dim( [pts[0], larger[0]] );
}

module expandPts_demo_3()
{
	echom("expandPts");
	pts = randOnPlanePts(5);
	//pts = randPts(4);
	larger = expandPts(pts, dist=0.1);
	smaller= expandPts(pts, dist=-0.1);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green","r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01, "closed",true] );
	MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}
	//Dim( [pts[0], larger[0]] );
}

module expandPts_demo_4_arc()
{
	pts = arcPts(r=3, a=120, count=24);
	//echo("pts:", pts);
	larger = expandPts(pts, dist=0.1);
	smaller= expandPts(pts, dist=-0.1);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green","r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01, "closed",true] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}

}
module expandPts_demo()
{
	echom("expandPts_demo");
	//p6 = randPts(6);
	//echo( "p6:",p6);
	//echo( "xp6:",expandPts( p6 ));

	//expandPts_demo_1_open();
	//expandPts_demo_2();
	//expandPts_demo_3();
	expandPts_demo_4_arc();
}	  
//expandPts_demo();	

//angle_test( ["mode",13] );
//arcPts_test( ["mode",13]);

////========================================================
//expand=[[]];
//
//function expand(pts,ratio=1.2, _i_=0)=
//(
//	len(pts)==2?
//	[ onlinePt( pts, ratio=ratio/2 )
//	, onlinePt( pts, ratio=-ratio/2)
//	]
//	: len(pts)==3?
//	  [ onlinePt( [pts[0], incenterPt(pts)], ratio = ratio]
//	  , onlinePt( [pts[1], incenterPt(pts)], ratio = ratio]
//	  , onlinePt( [pts[2], incenterPt(pts)], ratio = ratio]
//	  ]
//	  : _i_<len(pts)?
//		concat(  [ get(pts, _i_-1 )
//				 ,get(pts, _i_   )
//				 ,get(pts, _i_==len(pts)-1?0:_i_+1 )] 





//========================================================
linePts=["linePts","pq, len, ratio", "pq", "Array, Points, Geometry, Line",
 " Extend/Shrink line. Given a 2-pointer (pq) and len or ratio , return 
;; a new 2-pointer for a line with extended/shortened lengths. 
;;
;; Both len and ratio can be positive|negative. Positive= extend, negative=shrink.
;; 
;; They can be either 2-item array (like [1,0]) or a number. If a number, it's the
;; same as [number,number].
;;
;; If array [a,b], a,b for p, q side, respectively. For example, 
;;
;;   linePts( pq, len=-1): both sides shrink by 1
;;   linePts( pq, len=[1,1]): both sides extends by 1

;; Compared to onlinePt, this function:
;; (1) is able to extend both ends. 
;; (2) return 2-pointers.
;;
;; linePts(pq, len=1) = [ onlinePt( pq, len=-1)
;;                      , onlinePt( p10(pq), len=-1 ]
"];


function linePts(pq,len=undef, ratio=[1,1])=
 let( len=isnum(len)?[len,len]:len
	, ratio=isnum(ratio)?[ratio,ratio]:ratio
	)
( 
   [ onlinePt( pq,  len= -len[0], ratio=-ratio[0])
   , onlinePt( p10(pq), len= -len[1], ratio=-ratio[1])
   ]
);		

module linePts_test( ops=["mode",13] )
{
	doctest( linePts,ops=ops );
}

module linePts_demo_1_len()
{_mdoc("linePts_demo_1_len",
 " yellow: linePts(pq, len=0.5)
;; red   : linePts(pq, len=[1,0])
;; blue  : linePts(pq, len=[1.5,1.5])
");

	pq=randPts(2);
	MarkPts( pq, ["labels","PQ", "r",0.2] );
	Line0(pq);
    
	xpq = linePts(pq, len=0.5);
	Line0(xpq, ["r",0.25,"transp",0.5,"color","red"] );

	x2pq = linePts(pq, len=[1,0]);
	Line0(x2pq, ["r",0.4,"color","green", "transp",0.3] );

	x3pq = linePts(pq, len=[1.5,3]);
	Line0(x3pq, ["r",0.55,"color","blue", "transp",0.2] );
    
    P= pq[0]; Q=pq[1]; R= randPt();
    pqr = concat( pq, [R] );
    LabelPt( anglePt( [P, midPt([P,Q]), R] , a=90, len=1.5), "linePts(pq)" );
    LabelPt( anglePt( pqr, a=110, len=1.5), "linePts(pq,len=0.5)"
            , ["color","red"] );
    LabelPt( anglePt( [Q,P,R], a=120, len=2), "linePts(pq,len=[1,0])"
            , ["color","green"] );
    LabelPt( anglePt( [x3pq[0], x3pq[1],R], a=120, len=2), "linePts(pq,len=[1.5,3])"
            , ["color","blue"] );    
    
}
module linePts_demo_2_ratio()
{
	pq=randPts(2);
	MarkPts( pq, ["labels","PQ"] );
	Line0(pq);
	xpq = linePts(pq, ratio=0.2);
	Line0(xpq, ["r",0.25,"transp",0.5] );

	x2pq = linePts(pq, ratio=[0.2,0]);
	Line0(x2pq, ["r",0.4,"color","red", "transp",0.3] );

	x3pq = linePts(pq, ratio=[0.4, 0.4]);
	Line0(x3pq, ["r",0.55,"color","blue", "transp",0.2] );
}
module linePts_demo_3_shrink()
{
	_pq=randPts(2);
	P= _pq[0];
    Q= onlinePt(_pq, len=3);
    pq = [P,Q];
    
	x2pq = linePts(pq, ratio=[-0.2,0]);
	Line0(x2pq, ["r",0.4,"color","red", "transp",0.3] );

	x3pq = linePts(pq, ratio=[-0.4, -0.4]);
	Line0(x3pq, ["r",0.55,"color","blue", "transp",0.2] );
    
    MarkPts( pq, ["labels","PQ"] );
	xpq = linePts(pq, ratio=-0.2);
	Line0(xpq, ["r",0.25,"transp",0.5] );
    Line0(pq, ["transp",0.3]);
	
}

//linePts_demo_1_len();
//linePts_demo_2_ratio();
//linePts_demo_3_shrink();

//========================================================
fac=[ "fac", "n", "int",  "Math",
"
 Given n, return the factorial of n (= 1*2*...*n) 
"];

function fac(n=5)= n*(n>2?fac(n-1):1);
module fac_test( ops )
{
	doctest
	( fac
	,  [ 
		[ "5",  fac(5), 120 ]
	  , [ "6", fac(6), 720 ]
	  ], ops
    );
}

//fac_test( ["mode",22] );
//doc(fac);



//========================================================
fidx=["fidx", "o,i,cycle=false,fitlen=false", "int|undef", "Index, Range, Array, String, Inspect",
" Fit index. Check if index i is in range of o (a str|arr). i could be negative.
;; If yes, convert it to *legal* index value (means, positive int). For o of len L,
;; this would mean -L &lt;= i &lt; L. That is, i should be in range *(-L,L]*. 
;; 
;; New 2014.8.17: o could be an integer for range 0 ~ o
;;
;; If i out of this range, return undef (when cycle=false) or:
;; -- set fitlen=true to return the last index
;; -- set cycle=true to loop from the other end of o. So if cycle=true, a given i 
;;     will never go undef, no matter how large or small it is.  
"];

function fidx(o,i,cycle=false, fitlen=false)=
let( L=isint(o)?o:len(o), j = cycle? mod(i,L):i )
(
	/*
	len: 1 2 3 4 5
		 0 1 2 3 4   i>=0
   		-5-4-3-2-1   i <0    

  				   0 1 2 3 4   : legal index
  		-5-4-3-2-1 0 1 2 3 4   : proper index

	When out of range: 

		if cycle=false: return false
		if cycle=true: 
			cycle through array as many as needed. So if cycle is set
					to true, it never returns false.  

	*/

	-L<=j&&j<L 
	? ( (j<0? L:0)+j )
	:fitlen==true?(L-1):undef
);

module fidx_test( ops )
{

	doctest
	( 
	 fidx, [
		"// string "
	  , [ "''789'',2", fidx("789",2), 2 ]
	  ,	[ "''789'',3", fidx("789",3), undef, ["rem","out of range"] ]
	  ,	[ "''789'',-2", fidx("789",-2), 1 ]
	  ,	[ "''789'',-3", fidx("789",-3), 0 ]
	  ,	[ "''789'',-4", fidx("789",-4), undef, ["rem","out of range"] ]
	  ,	[ "'',0", fidx("",0), undef ]
	  ,	[ "''789'',''a''", fidx("789","a"), undef ]
	  ,	[ "''789'',true", fidx("789",true), undef ]
	  ,	[ "''789'',[3]", fidx("789",[3]), undef ]
	  ,	[ "''789'',undef", fidx("789",undef), undef ]
	  , "// array"
	  ,	[ "[7,8,9],2", fidx([7,8,9],2), 2 ]
	  ,	[ "[7,8,9],3", fidx([7,8,9],3), undef, ["rem","out of range"] ]
	  ,	[ "[7,8,9],-2", fidx([7,8,9],-2), 1 ]
	  ,	[ "[7,8,9],-3", fidx([7,8,9],-3), 0 ]
	  ,	[ "[7,8,9],-4", fidx([7,8,9],-4), undef, ["rem","out of range"] ]
	  ,	[ "[],0", fidx([],0), undef ]
	  ,	[ "[7,8,9],''a''", fidx([7,8,9],"a"), undef ]
	  ,	[ "[7,8,9],true", fidx([7,8,9],true), undef ]
	  ,	[ "[7,8,9],[3]", fidx([7,8,9],[3]), undef ]
	  ,	[ "[7,8,9],undef", fidx([7,8,9],undef), undef ]

	  , "// If out of range, return undef, or:"
	  , "//  -- Set cycle to go around the other end."
	  , "//     (it never goes out of bound) "
	  , "//  -- Set fitlen to return the last index."
	  , "//"
	  ,	[ "[7,8,9],3", fidx([7,8,9],3), undef, ["rem","out of range"] ]
	  ,	[ "[7,8,9],3,fitlen=true", fidx([7,8,9],3,fitlen=true), 2, ["rem","fitlen"] ]
	  ,,	[ "[7,8,9],3, cycle=true", fidx([7,8,9], 3 , true), 0 ]
	  ,	[ "[7,8,9],4, cycle=true", fidx([7,8,9], 4 , true), 1 ]
	  ,	[ "[7,8,9],7, cycle=true", fidx([7,8,9], 7 , true), 1 ]
	  ,	[ "[7,8,9],-4", fidx([7,8,9],-4), undef, ["rem","out of range"] ]
	  ,	[ "[7,8,9],-4, cycle=true", fidx([7,8,9],-4,true), 2  ]
	  ,	[ "[7,8,9],-16, cycle=true", fidx([7,8,9],-16,true), 2 ]
	  ,	[ "[7,8,9],-5, cycle=true", fidx([7,8,9],-5,true), 1 ]
	  
	  ,"//"
	  ,"// New 2014.8.17: o can be an integer for range 0~o"
	  ,"//"
	  ,	[ "3,5", fidx(3,5), undef, ["rem","out of range"] ]
	  ,	[ "3,5,fitlen=true", fidx(3,5,fitlen=true), 2, ["rem","get the last idx"] ]
	  ,	[ "3,4,cycle=true", fidx(3,4,cycle=true), 1, ["rem","cycle from begin"] ]
	  , [ "3,-2", fidx(3,-2), 1 ]
	  ,	[ "3,-3", fidx(3,-3), 0 ]
	  ,	[ "3,-4", fidx(3,-4), undef, ["rem","out of range"] ]
	  ,	[ "3,-4,cycle=true", fidx(3,-4,cycle=true), 2, ["rem","cycle=true"] ]
	  , "//"	

	  ], ops
	);
}


//
////========================================================
//fitrange=[ "fitrange", "o,i", "int|false", "Index, Array, String, Inspect",
//" Check if index i is in range of o (a str|arr). i could be negative.
//;; If yes, convert it to proper index value (means, if <0, convert to
//;; positive). Else return false.
//;; 
//;; Note: get(o,i) == o[ fitrange(o,i) ], i could be <0 in both cases.
//"];
//
//function fitrange(o,i)=
//(
//	/*
//	len: 1 2 3 4 5
//		 0 1 2 3 4   i>=0
//   		-5-4-3-2-1   i <0    
//	*/
//	
//	(-len(o)<=i) && ( i<len(o)) 
//	? ( (i<0? len(o):0)+i )
//	: false
//);
//
//module fitrange_test( ops )
//{
//
//	doctest
//	( 
//	 fitrange, [
//		"## string ##"
//	  , [ "''789'',2", fitrange("789",2), 2 ]
//	  ,	[ "''789'',3", fitrange("789",3), false ]
//	  ,	[ "''789'',-2", fitrange("789",-2), 1 ]
//	  ,	[ "''789'',-3", fitrange("789",-3), 0 ]
//	  ,	[ "''789'',-4", fitrange("789",-4), false ]
//	  ,	[ "'',0", fitrange("",0), false ]
//	  ,	[ "''789'',''a''", fitrange("789","a"), false ]
//	  ,	[ "''789'',true", fitrange("789",true), false ]
//	  ,	[ "''789'',[3]", fitrange("789",[3]), false ]
//	  ,	[ "''789'',undef", fitrange("789",undef), false ]
//	  , "## array ##"
//	  ,	[ "[7,8,9],2", fitrange([7,8,9],2), 2 ]
//	  ,	[ "[7,8,9],3", fitrange([7,8,9],3), false ]
//	  ,	[ "[7,8,9],-2", fitrange([7,8,9],-2), 1 ]
//	  ,	[ "[7,8,9],-3", fitrange([7,8,9],-3), 0 ]
//	  ,	[ "[7,8,9],-4", fitrange([7,8,9],-4), false ]
//	  ,	[ "[],0", fitrange([],0), false ]
//	  ,	[ "[7,8,9],''a''", fitrange([7,8,9],"a"), false ]
//	  ,	[ "[7,8,9],true", fitrange([7,8,9],true), false ]
//	  ,	[ "[7,8,9],[3]", fitrange([7,8,9],[3]), false ]
//	  ,	[ "[7,8,9],undef", fitrange([7,8,9],undef), false ]
//	  ], ops
//	);
//}


//========================================================
//
// UNDER CONSTRUCT
//
_fmth=[["_fmth"
       ,"h, pairbreak='' , '', keywrap=''<b>,</b>'', valwrap=''<u>,</u>''" 
	   ,"string","String, Doc"
       ]];

function _fmth(h, pairbreak=" , "
			  , keywrap="<u><b>,</b></u>"
			  , valwrap="<i>,</i>"
			  , _i_=0)=
(
	_i_>len(h)-1? []  
	: str( _i_==0?"[":""
		 , replace(keywrap,",",_fmt(h[_i_]))

		 , _i_>len(h)-2?""
                        :str( ":"
                            , replace(valwrap,",",_fmt(h[_i_+1]))
                            ) //_arg(str(h[_i_+1]))) )		
		 , _i_>len(h)-3?"":pairbreak 
		 , _i_>len(h)-3?"]":_fmth( h, pairbreak, keywrap, valwrap, _i_=_i_+2)

		 )    
);

module _fmth_test( ops )
{
	scope=[ "h", ["num",23,"str","test", "arr", [2,"a"] ]
		  , "h2",["pqr", [2,3,4]]
		  , "h3",["pqr", [[0,1,2],[2,3,4]] ]
		  ];
	h= hash(scope, "h");
	h2= hash(scope, "h2");
	h3= hash(scope, "h3");

	r1 = _fmth(h);
	//r2 = _fmth(h2);
	//r3 = _fmth(h3);

	doctest(
	 _fmth
	,[
		//["h", _fmth(h), ""]
	  //, 
	//	_fmth(h)
	 //"No test is done."
	//, "2nd line string"
	 str("_fmth(h): ", _fmth(h))
	,str("_fmth(h2): ", _fmth(h2))
	,str("_fmth(h3): ", _fmth(h3))

//	  ["[''a'',23]", _fmth(["a",23]), "[\"a\",23]" ]
//	, ["h", _fmth(h ),""]
//	, ["h2", _fmth( h2),""]
//	, ["h3", _fmth( h3),""]

	],  ops, ,scope 
	);
}
//_fmth_test();
//test_example_test();
//accumulate_test();

//echo( _fmth(["num",23,"str","test", "arr", [2,"a"] ]) );

//========================================================
_fmt=["_fmt", "x,[s,n,a,ud,fixedspace]", "string", "String" 
," Given an variable (x), and formattings for string (s), number (n),
;; array (a) or undef (ud), return a formatted string of x. If fixedspace
;; is set to true, replace spaces with html space. The formatting is
;; entered in ''(prefix),(suffix)'' pattern. For example, a ''{,}'' converts
;; x to ''{x}''.
"
];


function _fmt(x, s=_span("&quot;,&quot;", "color:purple") 
			, n=_span(",", "color:brown") //magenta")//blueviolet")//brown")
			, a=_span(",", "color:navy")//midnightblue")//cadetblue")//mediumblue")//steelblue") //#993300") //#cc0060")  
			, ud=_span(",", "color:brown") //orange") 
			//, level=2
			, fixedspace= true
		   )=
(
	x==undef? replace(ud, ",", x) 
	:isarr(x)? replace(a, ",", replace(str(x),"\"","&quot;")) 
	: isstr(x)? replace(s,",",fixedspace?replace(str(x)," ", "&nbsp;"):str(x)) 
		:isnum(x)||x==true||x==false?replace(n,",",str(x))
				:x
);

module _fmt_test( ops )
{

	doctest(
	 _fmt
	,[
	   str("_fmt(3.45): ", _fmt(3.45))
     ,  str("_fmt([''a'',[3,''b''], 4]): ", _fmt(["a",[3,"b"], 4] ))
     ,  str("_fmt(''a string with     5 spaces''): ", _fmt("a string with     5 spaces"))			, str("_fmt(false): ", _fmt(false))

	],  ops
	);
}

module _fmt_demo()
{
	echom("_fmt");
	echo( str("# ", _fmt(345), " and array ", _fmt([2,"a",[3,"b"], 4]), " are ", _fmt("formatted"), " by ", _fmt("fmt with     5 spaces") 
			) ); 
	echo( _fmt(true), _fmt(false), _fmt(undef));
 
}
//_fmt_demo();
//doc(_fmt);
//accumulate_test();




//========================================================
ftin2in=["ftin2in","ftin","number","Math, Number, Unit",
" Given a ftin ( could be a string like 3'6'', or an array like [3,6], 
;; or a number 3.5, all for 3 feet 6 in ), convert to inches.
"];

function ftin2in(ftin)=
(
	isnum(ftin)
	? ftin*12
	: isarr(ftin)
	  ? ftin[0]*12+ftin[1]
	  : endwith(ftin,"'")
		? num( replace(ftin,"'",""))*12

	    : index(ftin,"'")>0
		   ? num( split(ftin,"'")[0] )*12 
			+ num( replace(split(ftin,"'")[1],"\"",""))
		   : num( replace(ftin,"\"",""))
);

module ftin2in_test( ops=["mode",13] )
{
	doctest( ftin2in,
	[
	  [ "3.5", ftin2in(3.5), 42 ]
	 ,[ "[3,6]", ftin2in([3,6]), 42 ]
	, [ "3'6''", ftin2in("3'6\""), 42 ]
	, [ "8'", ftin2in("8'"), 96 ]
	, [ "6''", ftin2in("6\""), 6 ]
	], ops
	);
}

//========================================================
get=["get", "o,i,cycle=false", "item", "Index, Inspect, Array, String, Range" ,
 " Given a str or arr (o), an index (i), get the i-th item. 
;; i could be negative.
;;
;; New argument *cycle*: cycling from the other end if out of range. This is
;; useful when looping through the boundary points of a shape.
"];

function get(o,i, cycle=false)= 
( 
	o[ fidx(o,i,cycle=cycle) ]	
);

module get_test( ops=["mode",13] )
{
	arr = [20,30,40,50];

	doctest
	( get
		,[ 
		  "## array ##"
		 , [ "arr, 1", get( arr, 1 ), 30 ]
    		 , [ "arr, -1", get(arr, -1), 50 ]
    		 , [ "arr, -2", get(arr, -2), 40 ]
    		 , [ "arr, true", get(arr, true), undef ]
    		 , [ "[], 1", get([], 1), undef ]
		 , [ "[[2,3],[4,5],[6,7]], -2", get([[2,3],[4,5],[6,7]], -2), [4,5] ]
    		 , "## string ##"
    		 , [ "''abcdef'', -2", get("abcdef", -2), "e" ]
    		 , [ "''aabcdef'', false", get("abcdef", false), undef ]
    		 , [ "'''', 1", get("", 1), undef ]
		 , "//New 2014.6.27: cycle"
		 , [ "arr, 4", get(arr,4), undef]
		 , [ "arr, 4, cycle=true", get(arr,4,cycle=true), 20]
		 , [ "arr, 10, cycle=true", get(arr,10,cycle=true), 40]
		 , [ "arr, -5", get(arr,-5), undef]
		 , [ "arr, -5, cycle=true", get(arr,-5,cycle=true), 50]

    		 ],ops,  ["arr", arr]
	);
}
//get_test(  );
//doc( get );


//========================================================
getcol=[ "getcol","mm,i=0", "array", "Array, Index",
 " Given a matrix and index i, return a 1-d array containing
;; item i of each matrix row (i.e., the column i of the multi
;; matrix. The index i could be negative for reversed indexing.
"];
				
function getcol(mm,i=0)=
(
	[ for(row=mm) get(row,i) ]
);

module getcol_test( ops )
{
	mm= [[1,2,3],[3,4,5]];
	doctest
	(
		getcol
	,	[
			[ "mm", getcol(mm), [1,3] ]
		,	[ "mm,2", getcol(mm,2), [3,5] ]
		,	[ "mm,-2", getcol(mm,-2), [2,4] ]
		], ops, ["mm", [[1,2,3],[3,4,5]] ]
	);
}
//getcol_test(["mode",22]);
//doc( getcol );

//========================================================
getSideFaces=["getSideFaces"
    ,"topIndices, botIndices, isclockwise=true"
    ,"array", "Array,Geometry"
, "Given topIndices and botIndices (both are array
;; of indices), return an array of indices for the 
;; side faces arguments for the polyhedron() function
;; to create a column object (note that the top
;; and bottom faces are not included).
;; isclockwise: true if looking down from top to 
;; bottom, the order of indices is clockwise. Default
;; is true.
"];
/*   
  clockwise (default)
;;       _2-_
;;    _-' |  '-_
;;   1_   |    '-3
;;   | '-_|   _-'| 
;;   |    '-0'   |
;;   |   _6_|    |
;;   |_-'   |'-_ | 
;;   5 _     |   7
;;     '-_  | _-'
;;        '-4' 

  => [ [0,3,7,4], [1,0,4,5], [2,1,5,6],[3,2,6,7]]

  clockwise = false
;;       _2-_
;;    _-' |  '-_
;;   3_   |    '-1
;;   | '-_|   _-'| 
;;   |    '-0'   |
;;   |   _6_|    |
;;   |_-'   |'-_ | 
;;   7 _    |   '5
;;     '-_  | _-'
;;        '-4' 

  => [ [3,0,4,7], [2,3,7,6], [1,2,5,6],[0,1,5,4] ]
 [[0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]] #FAIL#
"
*/
function getSideFaces(topIndices
                    , botIndices
                    , clockwise=true)=
let( ts= topIndices
   , bs= botIndices
   , tlast = last(ts)
   , blast = last(bs)
   )
(
  clockwise
  ? [for (i=range(ts))
      i==0
      ? [ ts[0], tlast, blast, bs[0] ]
      : [ ts[i], ts[i-1], bs[i-1], bs[i] ]
     ]
  : [for (i=range(ts))
      i<len(ts)-1
      ? [ ts[i], ts[i+1], bs[i+1], bs[i] ]
      : [ tlast, ts[0], bs[0], blast ]
      ]
);

module getSideFaces_test(ops)
{ 
    d1=[ [0,1,2,3],[4,5,6,7]];
	d2=[ [6,7,8],[9,10,11]];
	doctest(getSideFaces,
	[ ["d1[0], d1[1]", getSideFaces(d1[0],d1[1]),
        [[0,3,7,4], [1,0,4,5], [2,1,5,6], [3,2,6,7]]
      ]
     ,["d1[0], d1[1], clockwise=false"
        , getSideFaces(d1[0],d1[1],false),
        [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
      ]
     ,["d2[0], d2[1]", getSideFaces(d2[0],d2[1]),
        [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
      ]
     ,["d2[0], d2[1], clockwise=false"
        , getSideFaces(d2[0],d2[1],false),
        [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
      ]
	]
	,ops, ["d1", d1, "d2",d2]);
  
}

//function hashex(h, keys, _i_=0)=
//  _i_<len(keys)
//  ? concat( [ keys[_i_], hash(h, keys[_i_])], hashex(h, keys, _i_+1) )
//  :[];
function hashex(h, keys, _i_=0)=
  let( hh = [for(k=keys) [k, hash(h,k)] ] )
  [ for(kv=hh, x=kv) x ]   ;

//========================================================
getsubops= ["getsubops","ops, com_keys, sub_dfs"
            ,"hash or false", "module"
," Define the options of multiple sub objs. The scenario is that an obj O 
;; can have n common subobjs, such as left-arm, right-arm. They belong to
;; ''arms''. We want :
;;
;; 1. Both arms can be turned off by O[''arms'']=false
;; 2. Common arm props can be set on the obj level by
;;    O[''r'',1] that applies to entire obj including other parts
;; 3. Common arm props can also be set on the obj level by
;;    by O[''arms'']=[''r'',2] that will only apply to arms. 
;; 4. Each arm can be turned off by O[''leftarm'']=false
;; 5. Each arm can be set by O[''leftarm'']= [''r'',1]
;;
;; ops: obj-level ops 
;; subs: a hash of 2 k-v pairs: [''arms'', df_arms, ''armL'', df_armL]
;;       ''arms'': name used in ops as ops=[ ... ''arms'', true ...]
;;       df_arms : hash for default arms 
;;       ''armL'': name  used in ops as ops=[ ... ''armL'', true ...]
;;       df_armL : hash for default left arm
;; com_keys: like ''len'',''color''. This is the common keys that
;;           will set on entire obj and also carries over to all arms
;;
;; NOTE: com_keys SHOULD shows up in ops but NEVER in df_arms or df_armL
"];

function getsubops( 
      ops
    , df_ops=[] // default ops given at the design time
    , sub_dfs   // a hash showing layers of sub op defaults
                // ["arms", df_arms, "armL", df_armL]
                // The 1st k-v pair, ("arms",df_arms) is the group [name
                // , default], to which the next ("armL",df_armL), belongs.
    , com_keys  // names of sub ops, which you want them to be also be set 
                // from the obj level. Ex,["r", "color"]
    )=
(  let(subnames = keys( sub_dfs ) // name of subunit   ["arms", "armL"]
      ,df_subs  = vals( sub_dfs ) // df opt of subunit 
                                  // [df_arms, df_armL, df_armR]
      ,_df_ops    = [ for(i=range(df_ops)) // part of user ops that have 
                                           // keys showing up in com_keys
                      let( key= df_ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) df_ops[i]
                  ] // ["r",1] 
      ,u_ops    = [ for(i=range(ops)) // part of user ops that have keys 
                                      // showing up in com_keys
                      let( key= ops[ round(i/2)==i/2? i:i-1 ] )
                      if( index( com_keys, key)>=0) ops[i]
                  ] // ["r",1] 
      ,u_subops = [ for(sn= subnames)  // Extract subops settings from 
                    hash(ops,sn) ]     // user input ops ops, like 
                                       // ["arms",true, "armL",["r",1]]
      // order of reversed priority:
      // [  df_ops, df_arms, df_armL, u_ops,u_arms, u_armL ] 
      ,subops = updates(
          _df_ops
        , [df_subs[0]
          , df_subs[1]
          , u_ops
          , u_subops[0]==true? []:ishash(u_subops[0])?u_subops[0]:false
          , u_subops[1]==true? []:ishash(u_subops[1])?u_subops[1]:false
          ])

       ,show= sum( [ for(k=subnames) hash(ops,k)==false?1:0])==0
       )
       //_//df_ops
       //[ for(s=u_subops) s==false?1:0] //u_subops
    show? subops 
        : false
);
    

module getsubops_test( ops=[] )
{
    
    //=========================================
    //=========================================
    
    opt=[];
    df_arms=["len",3,"r",1];
    df_armL=["fn",5];
    df_armR=[];
    df_ops= [ "len",2
            , "arms", true
            , "armL", true
            , "armR", true
            ];
    com_keys= ["fn"];
    
    /*
        setops: To simulate obj( ops=[...] )
        args  : ops  
                // In real use, following args are set inside obj()
                df_ops
                df_subops // like ["arms",[...], "armL",[...]] 
                com_keys 
                objname   // like "obj","armL","armR"          
                propname  // like "len"
    */
    function setops(   
            ops=opt
            , df_ops=df_ops
            , df_subops=[ "arms",df_arms, "armL",df_armL,"armR",df_armR]
            , com_keys = com_keys
            , objname ="obj"
            , propname=undef
            )=
        let(
             _ops= update( df_ops, ops )
           , df_arms= hash(df_subops, "arms")  
           , df_armL= hash(df_subops, "armL")  
           , df_armR= hash(df_subops, "armR")  
           , ops_armL= getsubops( _ops, df_ops = df_ops
                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]                    
                    , com_keys= com_keys
                    )
            , ops_armR= getsubops( _ops, df_ops = df_ops
                        , sub_dfs= ["arms", df_arms, "armR", df_armR ]
                        , com_keys= com_keys
                        )
            , ops= objname=="obj"?_ops
                    :objname=="df_ops"?df_ops
                    :objname=="armL"?ops_armL
                    :ops_armR
            )
        // ["ops",_ops,"df_arms", df_arms, "ops_armL",ops_armL, "ops_armR",ops_armR];
        propname==undef? ops:hash(ops, propname);
      
     /**
        Debugging Arrow(), in which a getsubops call results in the warning:
        DEPRECATED: Using ranges of the form [begin:end] with begin value
        greater than the end value is deprecated.
            
     */       
     function debug_Arrow(ops=["r", 0.1, "heads", false, "color","red"])=
        let (
        
           df_ops=[ "r",0.02
               , "fn", $fn
               , "heads",true
               , "headP",true
               , "headQ",true
               , "debug",false
               , "twist", 0
               ]
          ,_ops= update(df_ops,ops) 
          ,df_heads= ["r", 1, "len", 2, "fn",$fn]    
          , ops_headP= getsubops( _ops, df_ops=df_ops
                , com_keys= [ "color","transp","fn"]
                , sub_dfs= ["heads", df_heads, "headP", [] ]
                ) 
        )
        ops_headP;
        
     doctest("getsubops"
        , [ ""
            ,"In above settings, ops is the user input, the rest are"
            ,"set at design time. With these:"
            ,""
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]]
            ,["ops"
                 , setops()
                 , ["len",2,"arms",true,"armL", true, "armR", true]]
            ,["ops_armL"
                 , setops(objname="armL")
                 , ["len", 3, "r", 1, "fn", 5]]
            ,["ops_armR"
                 , setops(objname="armR")
                 , [ "len", 3, "r", 1]]
                            
            ,""
            ,"Set r=4 at the obj level. Since r doesn't show up in com_keys, "
            ,"it only applies to obj, but not either arm. "
            ,""
            
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true],]
            ,["ops"
                 , setops(ops=["r",4])
                 , ["len",2,"arms",true,"armL", true, "armR", true,"r",4],]
            ,["ops_armL"
                 , setops(ops=["r",4],objname="armL")
                 , ["len", 3, "r", 1,"fn", 5]]
            ,["ops_armR"
                 , setops(ops=["r",4],objname="armR")
                 , [ "len", 3, "r", 1]]
                            
            ,""
            ,"Set fn=10 at the obj level. Since fn IS in com_keys, "
            ,"it applies to obj and both arms. "
            ,""
            
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true],]
            ,["ops"
                 , setops(ops=["fn",10])
                 , ["len",2,"arms",true,"armL", true, "armR", true, "fn",10],]
            ,["ops_armL"
                 , setops(ops=["fn",10],objname="armL")
                 , ["len", 3, "r", 1, "fn", 10]]
            ,["ops_armR"
                 , setops(ops=["fn",10],objname="armR")
                 , ["len", 3, "r", 1, "fn", 10]]  
                 
        
            ,""
            ,"Disable armL:"
            ,""
            
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]]
            ,["ops"
                 , setops(ops=["armL",false])
                 , ["len",2,"arms",true,"armL", false, "armR", true]]
            ,["ops_armL"
                 , setops(ops=["armL",false],objname="armL")
                 , false]
            ,["ops_armR"
                 , setops(ops=["armL",false],objname="armR")
                 , ["len", 3, "r", 1]]


            ,""
            ,"Disable both arms:"
            ,""
            
            ,["df_ops"
                 , setops(objname="df_ops")
                 , ["len",2,"arms",true,"armL", true, "armR", true]]
            ,["ops"
                 , setops(ops=["arms",false])
                 , ["len",2,"arms",false,"armL", true, "armR", true]]
            ,["ops_armL"
                 , setops(ops=["arms",false],objname="armL")
                 , false]
            ,["ops_armR"
                 , setops(ops=["arms",false],objname="armR")
                 , false]
        
        
//            ,""
//            ,"Debugging Arrow(), in which a getsubops call results in the warning:"
//            ,"DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated."
//            ,""
//            
//            ,["debug_Arrow(ops=[''r'', 0.1, ''heads'', false, ''color'',''red'']"
//             , debug_Arrow(ops=["r", 0.1, "heads", false, "color","red"]),[]
//             ]
                            
          ]
          
          

          
        , ops, [ "df_ops",df_ops, 
               , "df_arms", df_arms
               , "df_armL", df_armL
               , "df_armR", df_armR
               , "com_keys", com_keys
               , "ops",opt
               ]
    );
}


//========================================================
getTubeSideFaces=["getTubeSideFaces",
  "indexSlices, clockwise=true",
  "array", "Array, Geometry"
, "Given an array of indexSlices (an indexSlice
;; is a list of indices representing the order 
;; of points for a circle), return an array of 
;; faces for running polyhedon(). Typically
;; usage is in a column or tube of multiple
;; segments. For example, CurvedCone.
;;
;; See getSideFaces that is for tube of only
;; one segment. 
"];  
function getTubeSideFaces(
         indexSlices, clockwise=true)=  
  let( ii = indexSlices )
( 
    //mergeA( 
      [ for (i=range(len(ii)-1))
        getSideFaces( topIndices= ii[i]
                    , botIndices= ii[i+1]   
                    , clockwise=clockwise
                    )
      ]
    //)
);

module getTubeSideFaces_test(ops)
{ 
    d1=[ [6,7,8,9],[10,11,12,13],[14,15,16,17] ];
    
	doctest(getTubeSideFaces,
	[ ["d1", getTubeSideFaces(d1),
        [[[ 6, 9,13,10], [ 7, 6,10,11]
         ,[ 8, 7,11,12], [ 9, 8,12,13]]
        ,[[10,13,17,14], [11,10,14,15]
         ,[12,11,15,16], [13,12,16,17]]
        ]
      ]
//     ,["d1[0], d1[1], clockwise=false"
//        , getSideFaces(d1[0],d1[1],false),
//        [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
//      ]
//     ,["d2[0], d2[1]", getSideFaces(d2[0],d2[1]),
//        [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
//      ]
//     ,["d2[0], d2[1], clockwise=false"
//        , getSideFaces(d2[0],d2[1],false),
//        [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
//      ]
	]
	,ops, ["d1", d1]);
  
}
//========================================================
_h= [ "_h", "s,h, showkey=false, wrap=''{,}''", "string", "Hash, String",
"
 Given a string (s), a hash (h) and a wrapper string like {,},
;; replace the {key} found in s with the val where [key,val] is
;; in h. If showkey=true, replace {key} with key=val. This
;; is useful when echoing a hash:
;;
;; _h( ''{w},{h}'', [''h'',2,''w'',3])=> ''3,2''
;; _h( ''{w},{h}'', [''h'',2,''w'',3], true)=> ''w=3,h=2''
"];

function _h(s,h, showkey=false, wrap="{,}",isfmt=true, _i_=0)= 
( 
	len(h)<2?s
	:_i_>len(h)?s
	  :_h(
		  replace(s, replace(wrap,",",h[_i_])
				, str( showkey?h[_i_]:"", showkey?"=":""
					, h[_i_+1] //isfmt?_fmt(h[_i_+1]):h[_i_+1] 
					)
				)
		 , h
		 , wrap= wrap
		 , showkey= showkey
		 , _i_=_i_+2
		 )
);
	
module _h_test( ops )
{
	data=["m",3,"d",6];
	tmpl= "2014.{m}.{d}";
	doctest
	( 
		_h, [
		 	[
				str( tmpl, ", ", data )
				, _h("2014.{m}.{d}",  data)
				, "2014.3.6"
		 	]
			,"// showkey=true :"
	 	 	,[
				"''2014.[m].[d]'', data, showkey=true, wrap=''[,]''"
				, _h("2014.[m].[d]",  data, true, "[,]")
				, "2014.m=3.d=6"
		  	]
			,"// hash has less key:val " 
			,[
				"tmpl, [''m'',3]"
				, _h("2014.{m}.{d}", ["m",3])
				, "2014.3.{d}"
		 	]
			,"// hash has more key:val " 
			,[
				"tmpl, [''a'',1,''m'',3,''d'',6]"
				, _h("2014.{m}.{d}", ["a",1,"m",3,"d",6] )
				, "2014.3.6"
		 	]	
	  	  ], ops , ["tmpl", tmpl, "data", data]
	);
}
//h__test( ["mode",22] );
//doc( _h );


//========================================================
has=["has","o,x","T|F","String, Array, Inspect",
"
 Given an array or string (o) and x, return true if o 
;; contains x. 
"];
function has(o,x)= index(o,x)>-1;

module has_test(ops)
{ 
	doctest(has,
	[ ["''abcdef'',''d''",has("abcdef","d"),true]
	, ["''abcdef'',''m''",has("abcdef","m"),false]
	, ["[2,3,4,5],3",has([2,3,4,5],3),true]	
	, ["[2,3,4,5],6",has([2,3,4,5],6),false]	
	]
	,ops);
}	
//has_test(["mode",22]);
//doc( has );

//========================================================
hasAny=["hasAny", "o1,o2", "T|F", "Inspect, Array, String",
" Given two arrays/string, o, o2, return true if and only
;; if any of items in one appears in another. Same as
;; len(arrItcp(o1,o2))>0, but arrItcp is for arr only.
"];

function hasAny(o1,o2)= 
(
	sum( [for(i=range(o2)) has(o1,o2[i])?1:0 ] )>0
);
//	len(o2)==0?false
//	: ((index(o1, o2[0])>-1)||hasAny(o1,shrink(o2)) );

module hasAny_test( ops )
{
	doctest
	(
		hasAny
		,[
		  ["''abcdef'', [''b'',''x'']", hasAny("abcdef", ["b","x"]), true]
		 ,["''abcdef'', [''y'',''x'']", hasAny("abcdef", ["y","x"]), false]
		 ,["''abcdef'', ''bx''", hasAny("abcdef", "bx" ), true]
		 ,["[2,4,6], [4,5]", hasAny([2,4,6], [4,5]), true]
		 ,["[2,4,6], [5,9]", hasAny([2,4,6], [5,9]), false]
		]
		, ops
	);
}
//hasAny_test();
//doc( hasAny );




//========================================================
hash=[ "hash", "h,k, notfound=undef, if_v=undef, then_v=undef, else_v=undef", "Any","Hash",
" Given an array (h, arranged like h=[k,v,k2,v2 ...] to serve
;; as a hash) and a key (k), return a v from h: hash(h,k)=v.
;; If key not found, or value not found (means k is the last item
;; in h), return notfound. When if_v is defined, check if v=if_v.
;; If yes, return then_v. If not, return else_v if defined, or v
;; if not.
"];

function hash(h,k, notfound=undef, if_v=undef, then_v=undef, else_v=undef, _i_=0)= 
(
	!(isarr(h)||isstr(h))? notfound      // not a hash
	:_i_>len(h)-1? notfound  // key not found
		: h[_i_]==k?        // key found
			 (_i_==len(h)-1?notfound  // value not found
				: (if_v==undef    
					? (h[_i_+1]==undef?notfound:h[_i_+1])     
                            // if v=undef, return notfound; else v 
					: h[_i_+1]==if_v  // else, ask if v==if_v
						? then_v  	  // if yes, return if_v  
						:( else_v==undef    // else, ask if else_v defined
								?h[_i_+1]  // if no, return v
								:else_v)   // if yes, return else_v 
				  )	 
			)
			: hash( h=h, k=k, notfound=notfound, if_v=if_v, then_v=then_v
				  , else_v=else_v, _i_=_i_+2)           
);


module hash_test( ops )
{
	h= 	[ -50, 20 
			, "xy", [0,1] 
			, 2.3, 5 
			,[0,1],10
			, true, 1 
			, 1, false
			,-5
			];

	doctest
	(
		hash  
		,[ [ "h, -50", hash(h, -50), 20 ]
    		 , [ "h, ''xy''", hash(h, "xy"), [0,1] ]
		 , [ "h, 2.3", hash(h, 2.3), 5 ]
    		 , [ "h, [0,1]", hash(h, [0,1]), 10 ]
		 , [ "h, true", hash(h, true), 1 ]
    		 , [ "h, 1", hash(h, 1), false ]
    		 , [ "h, ''xx''", hash(h, "xx"), undef ]
		 , "## key found but it's in the last item without value:"
    		 , [ "h, -5", hash(h, -5), undef ]
		 , "## notfound is given: "
    		 , [ "h, -5,''df''", hash(h, -5,"df"), "df" , ["rem", "value not found"] ]
	 	 , [ "h, -50, ''df''", hash(h, -50,"df"), 20 ]
    		 , [ "h, -100, ''df''", hash(h, -100,"df"), "df" ]
		 , "## Other cases:"
	 	 , [ "[], -100", hash([], -100), undef ]
	 	 , [ "true, -100", hash(true, -100), undef ]
    		 , [ "[[undef,10]], undef", hash([ [undef,10]], undef), undef ]
 		 , "  "
	 	 , "## Note below: you can ask for a default upon a non-hash !!"
		 , [ "true, -100, ''df''", hash(true, -100, "df"), "df"]
	 	 , [ "true, -100 ", hash(true, -100), undef]
	 	 , [ "3, -100, ''df''", hash(3, -100, "df"), "df"]
	 	 , [ "''test'', -100, ''df''", hash("test", -100, "df"), "df"]
		 , [ "false, 5, ''df''", hash(false, 5, "df"), "df"]
		 , [ "0, 5, ''df''", hash(0, 5, "df"), "df" ]
		 , [ "undef, 5, ''df''", hash(undef, 5, "df"), "df" ]
		 , "  "
		 , "## New 20140528: if_v, then_v, else_v "
		 , [ "h, 2.3, if_v=5, then_v=''got_5''", hash(h, 2.3, if_v=5, then_v="got_5"), "got_5" ]
		 , [ "h, 2.3, if_v=6, then_v=''got_5''", hash(h, 2.3, if_v=6, then_v="got_5"), 5 ]
		 , [ "h, 2.3, if_v=6, then_v=''got_5'', else_v=''not_5''"
					, hash(h, 2.3, if_v=6, then_v="got_5", else_v="no_5"), "no_5" ]
		 ,"  "
		 ,"## New 20140730: You can use string for single-letter hash:"
		 ,["''a1b2c3'',''b''", hash("a1b2c3","b"), "2"]
           ," "
           ,"## 20150108: test concat "
           ,["[''a'',3,''b'',4,''a'',5]", hash(["a",3,"b",4,"a",5],"a"), 3] 
		], ops,["h",h]
	);
}
//hash_test(["mode",13]);
//doc(hash );


////========================================================
//keyidx=["keyidx", "h,k", "int", "Hash",
//" Given a hash (h) and a key (k), return the index of the key
//;; in the hash. Note that it's not the index of k in the keys() 
//;; array:
//;;
//;; h = [''a'',1,  ''b'',2];
//;; index(h,''b'') => b;
//;; hashi(h,''a'') = index(keys(h),k) => 1;\\
//;;
//;; If key not found, return -1. \\
//"];
//
//function hashi(h,k)=
//( 
//	index(keys(h),k)
//);
//
//module hashi_test( ops )
//{
//	h = ["a",1,  "b",2];
//	//echo( keys(h));
//	
//	doctest
//	(hashi, 
//		[[ "h,''a''", hashi(h,"a"), 0 ]
//		,["h,''b''", hashi(h,"b"), 1 ] 
//		,["h,''c''", hashi(h,"c"), -1 ]
//		],ops, ["h", h] 
//	);
//}
//hashi_test(["mode",22]);
//doc( hashi );


////========================================================
//hashi=[["hashi","h,key", "int", "Hash"],
//"
// Given a hash (h) and a key (k), return the index of the item\\
// [k,v]. Note that it's not the index of k in the h array:\\
//  \\
// h = [''a'',1,  ''b'',2];\\
// index(h,''a'') => 2;\\
// hashi(h,''a'') = index(keys(h),k) => 1;\\
//  \\
// If key not found, return -1. \\
//"];
//
//function hashi(h,k)=
//( 
//	index(keys(h),k)
//);
//
//module hashi_test( ops )
//{
//	h = ["a",1,  "b",2];
//	//echo( keys(h));
//	
//	doctest
//	(hashi, 
//		[[ "h,''a''", hashi(h,"a"), 0 ]
//		,["h,''b''", hashi(h,"b"), 1 ] 
//		,["h,''c''", hashi(h,"c"), -1 ]
//		],ops, ["h", h] 
//	);
//}
//hashi_test(["mode",22]);
//doc( hashi );
	



//
////========================================================
//hashkv=[["hashkv", "h,k", "2 member array", "Hash"],
//"
// Given a hash (h) and a key (k), return [k,v], or [] if\\
// k not found.\\
//"];
//
//function hashkv(h,k)=
//(
//	//h[hashi(h,k)]
//	haskey(h,k)?[k, hash(h,k) ]:[]
//	
//);
//
//module hashkv_test( ops )
//{
//	h = ["a",1,  "b",2];
//	doctest
//	(
//		hashkv, 
//		[[ "h,''a''", hashkv(h,"a"), ["a",1] ]
//		,["h,''b''", hashkv(h,"b"), ["b",2] ] 
//		,["h,''c''", hashkv(h,"c"), [] ]
//		],ops, ["h", h] 
//	);	
//}
//hashkv_test(["mode",22]);
//doc( hashkv );


//========================================================
hashkvs=["hashkvs", "h", "array of arrays", "Hash",
"
 Given a hash (h), return [[k1,v1], [k2,v2] ...].
"];

function hashkvs(h, _i_=0)=
(
//	!isarr(h)? undef        // not a hash
//	:_i_>len(h)-1? []       // end of search
//		: concat( 
//				 [[h[_i_],h[_i_+1]]]
//			    , hashkvs(h, _i_=_i_+2)
//				)
	[for(i=[0:2:len(h)-2]) [h[i],h[i+1]]]
);

module hashkvs_test( ops )
{
	h = ["a",1,  "b",2, 3,33];
	//doc(hashkvs);
	//test(hashkvs, recs, ops);

	doctest( hashkvs,
	[		
		[ str(h), hashkvs(h), [["a",1],["b",2],[3,33]] ]
	], ops
	);	
}
//hashpairs_test( ["mode",2]);
//doc(hashkvs);




//========================================================
haskey=[ "haskey", "h,k", "T|F", "Hash, Inspect",
"
 Check if the hash (h) has a key (k)
"];

function haskey(h,k,_i_=0)=
(
	index(keys(h),k)>=0
/*	o[_i_][0]==x?true
	:(_i_<len(o)?
		haskey( o, x, _i_+1)
		: false		
	 )
*/

);

module haskey_test( ops )
{
	table = 	[ -50, 20 
			, "80", 25 
			, 2.3, "yes" 
			,[0,1],5
			, true, "YES" 
			];

	doctest
	(haskey, 
		 [ [ "table, -50", haskey(table, -50), true ]
    		 , [ "table, ''80''", haskey(table, "80"), true ]
		 , [ "table, ''xy''", haskey(table, "xy"), false ]
		 , [ "table, 2.3", haskey(table, 2.3), true ]
    		 , [ "table, [0,1]", haskey(table, [0,1]), true ]
		 , [ "table, true", haskey(table, true), true ]
    		 , [ "table, -100", haskey(table, -100), false ]
    		 , [ "[], -100", haskey([], -100), false ]
    		 , [ "[[undef,1], undef", haskey( [undef,1], undef), true
			, [["rem","###### NOTE THIS!"]] ]
    		 ],ops, ["table", table]
	);

}
//haskey_test(["mode",22]);
//doc(haskey);

////========================================================
//inc=["inc", "arr, n", "array", "Array",
//"
// Given an array (arr) and a number (n), increase every item
//;; in arr by n.
//;;
//;; Note 2014.8.8: This function might not needed since we have
//;; list comprehension:
//;;
//;; = [for(x=arr)x+n]
//"];
//
//function inc(arr, n) = arr + repeat( [n], len(arr) );
////doc(inc);
//
//module inc_test(ops)
//{
//	doctest( inc,
//	[
//		["[2,4,5,9],3", inc([2,4,5,9],3), [5,7,8,12] ]
//	],ops
//	);
//}



//========================================================
incenterPt=["incenterPt", "pqr", "point", "Geometry"
,
 " Given a 3-pointer (pqr), return the incenter point of the triangle, which
;; is where all 3 angle bisector lines meet."
];

function incenterPt(pqr)=
(
	lineCrossPt( pqr[0], angleBisectPt( rearr(pqr, [1,0,2]) )
			  , pqr[1], angleBisectPt( pqr )
			  )
);

module incenterPt_test(ops=["mode",13]) { doctest (incenterPt, ops=ops); }

module incenterPt_demo()
{
	pqr = randPts(3);
	ic  = incenterPt(pqr);
	MarkPts( concat(pqr,[ic]) );
	Chain( pqr );
}
//incenterPt_demo();

//========================================================
idx=["index", "o,x", "int", "Index,Array,String,Inspect",
" Return the index of x in o (a str|arr), or undef if not found.
"];
function idx(a,x)=
    [for(i=[0:len(a)-1]) if(a[i]==x)i][0];
        
//========================================================
index=["index", "o,x", "int", "Index,Array,String,Inspect",
" Return the index of x in o (a str|arr), or -1 if not found.
"];	 

function index(o,x, wrap=undef, _i_=0)= 
 let( x  = wrap?replace(wrap,",",x):x
	, oi = isstr(o)
		   ? slice(o, _i_, _i_+len(x))
		   : o[_i_]
    , end= _i_==len(o)
	, found= isnum(x)? isequal(oi,x): oi== x 
    )
(
	x=="" || end ?-1
	: found 
	  ? _i_
	  : index(o,x,undef,_i_+1)
		
 
//	x==""?-1: 
//	(
//		isarr(o)?			// array
//		(	x==o[_i_]?
//			_i_:( _i_<len(o)?
//            index(o,x,undef, _i_+1):-1 
//		  	)
//		):(						// string
//			wrap==undef?
//			(						
//			_i_+len(x)>len(o)?
//			-1: slice( o, _i_, _i_+len(x))==x? 
//				_i_: index( o,x, undef, _i_+1)
//
//			):( // has wrap like "{,}". Basically it just
//				// replaces x with replace(wrap,",",x)
//				// NOTE_20140514:
//				//   mmmhhh ... why am I doing this? 
//				//     index("a{_b_}{_c_}","{_,_}","c") 
//				//   could have been achieved with:
//				//     index("a{_b_}{_c_}", replace("{,}",",","c") )
//				//   but it does look longer
//
//			_i_+len(x)>len(o)?
//			-1: slice( o, _i_, _i_+len(replace(wrap,",",x)))
//                      ==(replace(wrap,",",x))? 
//				_i_: index( o,x, wrap, _i_+1)
//			)
//		)
//	)
);

//index_test();

module index_test( ops )
{

     arr = ["xy","xz","yz"];
	arr2= ["I_am_the_findw"];

	doctest
	(index,  
	 [ 
	 "## Array indexing ##"
	 , [ "arr, ''xz''", index(["xy","xz","yz"],"xz"),  1 ]
    	 , [ "arr, ''yz''", index(["xy","xz","yz"],"yz"),  2 ]
    	 , [ "arr, ''xx''", index(["xy","xz","yz"],"xx"),  -1 ]
    	 , [ "[], ''xx''", index([],"xx"),  -1 ]
    	 ,"## String indexing ##"
	 , [ "arr2, ''a''", index( "I_am_the_findw","a"),  2 ]
    	 , [ "arr2, ''am''", index( "I_am_the_findw","am"),  2 ]
    	 , [ "''abcdef'',''f''", index(  "abcdef","f"),  5 ]
    	 , [ "''findw'',''n''", index( "findw","n"),  2 ]
    	 , [ "arr2, ''find''", index( "I_am_the_findw","find"),  9 ]
    	 , [ "arr2, ''the''", index( "I_am_the_findw","the"),  5 ]
    	 , [ "arr2, ''findw''", index("I_am_the_ffindw","findw"),  10 ]
    	 , [ "''findw'', ''findw''", index("findw", "findw"),  0 ]
    	 , [ "''findw'', ''dwi''", index( "findw","dwi"),  -1 ]
    	 , [ "arr2, ''The''", index("I_am_the_findw","The"),  -1 ]
	 , [ "arr2, ''tex''",index( "replace__text","tex"),  9 ]
    	 , [ "''replace__ttext'', ''tex''", index("replace__ttext","tex"), 10 ]
    	 , [ "''replace_t_text'',''tex''", index( "replace_t_text","text"),  10]
	 , [ "''abc'', ''abcdef''",index("abc", "abcdef"),  -1 ]
	 , [ "'''', ''abcdef''",index( "","abcdef"),  -1 ]
	 , [ "'''', ''abc''",index( "abc",""),  -1 ]
	 ,"## String indexing with wrap ##"
	 , [ "''{a}bc'',''a'', ''{,}''", index("{a}bc","a",wrap="{,}"), 0] 
	 , [ "''a}bc'',''a'', '',}''", index("a}bc","a",wrap=",}"), 0] 
	 , [ "''a{b}c'',''b'', ''{,}''", index("a{b}c","b",wrap="{,}"), 1] 
	 , [ "''a//bc'',''b'', ''//,''", index("a//bc","b",wrap="//,"), 1] 
	 , [ "''abc:d'',''c'', '',:''", index("abc:d","c",wrap=",:"), 2] 
	 , [ "''a{_b_}{_c_}'',''c'', ''{_,_}''", index("a{_b_}{_c_}","c",wrap="{_,_}"), 6] 
	 ,"## Others ##"
	 , [ "[2, ,3, undef], undef", index([2,3, undef], undef), 2 ]
    	 ],ops, ["arr",arr, "arr2",arr2]
	);
}
//index_test(["mode",22]);
//doc(index);


//========================================================
inrange=[ "inrange", "o,i", "T|F", "Index, Array, String, Inspect",
" Check if index i is in range of o (a str|arr). i could be negative.\\
"];

function inrange(o,i)=
(
	/*
	len: 1 2 3 4 5
		 0 1 2 3 4   i>=0
   		-5-4-3-2-1   i <0    
	*/
	
	(-len(o)<=i) && ( i<len(o)) 

);

module inrange_test( ops )
{

	doctest
	( 
	 inrange, [
		"## string ##"
	  , [ "''789'',2", inrange("789",2), true ]
	  ,	[ "''789'',3", inrange("789",3), false ]
	  ,	[ "''789'',-2", inrange("789",-2), true ]
	  ,	[ "''789'',-3", inrange("789",-3), true ]
	  ,	[ "''789'',-4", inrange("789",-4), false ]
	  ,	[ "'',0", inrange("",0), false ]
	  ,	[ "''789'',''a''", inrange("789","a"), false ]
	  ,	[ "''789'',true", inrange("789",true), false ]
	  ,	[ "''789'',[3]", inrange("789",[3]), false ]
	  ,	[ "''789'',undef", inrange("789",undef), false ]
	  , "## array ##"
	  ,	[ "[7,8,9],2", inrange([7,8,9],2), true ]
	  ,	[ "[7,8,9],3", inrange([7,8,9],3), false ]
	  ,	[ "[7,8,9],-2", inrange([7,8,9],-2), true ]
	  ,	[ "[7,8,9],-3", inrange([7,8,9],-3), true ]
	  ,	[ "[7,8,9],-4", inrange([7,8,9],-4), false ]
	  ,	[ "[],0", inrange([],0), false ]
	  ,	[ "[7,8,9],''a''", inrange([7,8,9],"a"), false ]
	  ,	[ "[7,8,9],true", inrange([7,8,9],true), false ]
	  ,	[ "[7,8,9],[3]", inrange([7,8,9],[3]), false ]
	  ,	[ "[7,8,9],undef", inrange([7,8,9],undef), false ]
	  ], ops
	);
}
//inrange_test(["mode",22]);
//doc(inrange);


//========================================================
int=[ "int", "x", "int", "Type, Number",
" Given x, convert x to integer. 
;;
;; int(''-3.3'') => -3
"];

function int(x,_i_=0)=
(
	isint(x)? x
	:isfloat(x)? sign(x)*round(abs(x))
	 :isarr(x)? undef
	   :x==""? 0
	  :index(x,".")==0? 0
		: ( _i_==0 && x[0]=="-"? 
			-int(x,_i_+1)
			:(_i_< min( len(x),
					  index(x,".")<0?
					  len(x):index(x,".")
					)?
			(pow(10, (min( len(x),
							index(x,".")<0?
							len(x):index(x,".")
						 )-_i_-1
					 )
				)
			* hash(  ["0",0, "1",1, "2",2
					,"3",3, "4",4, "5",5
					,"6",6, "7",7, "8",8
					,"9",9], x[_i_] )
			
			+ int(x, _i_+1)
			 ):0
			)
		  )
);
module int_test( ops )
{
	doctest
	( 
		int, [
		["3",int(3),3]
	  , ["3.3",int(3.3),3]
	  , ["-3.3",int(-3.3),-3]
	  , ["3.8",int(3.8),4]
	  , ["9.8",int(9.8),10]
	  , ["-3.8",int(-3.8),-4]
	  , ["''-3.3''",int("-3.3"),-3]
	  , ["''-3.8''",int("-3.8"),-3, ["rem"," !!Note this!!"]]
	  , ["''3''",int("3"),3]
	  , ["''3.0''",int("3.0"),3]
	  , ["''0.3''",int("0.3"),0]
	  , ["''34''",int("34"),34]
	  , ["''-34''",int("-34"),-34]
	  , ["''-34.5''",int("-34.5"),-34]
	  , ["''345''",int("345"),345]
	  , ["''3456''",int("3456"),3456]
	  , ["''789987''",int("789987"),789987]
	  , ["''789.987''",int("789.987"),789, ["rem"," !!Note this!!"]]
	  , ["[3,4]",int([3,4]),undef]
	  , ["''x''",int("x"),undef]
	  , ["''34-5''",int("34-5"),undef]
	  , ["''34x56''",int("34x56"),undef]
	 ],ops	
	);
}
//int_test(["mode",22]);
//doc(int);


//========================================================
intersectPt=["intersectPt", "pq, target", "point", "Array, Point, Geometry"
, " 

"];

module intersectPt_test( ops=["mode",13] )
{
	doctest( intersectPt, ops=ops );
}

/*

Intersection of line and plan:
http://paulbourke.net/geometry/pointlineplane/

The equation of a plane (points P are on the plane with normal N and point P3 on the plane) can be written as
N dot (P - P3) = 0
The equation of the line (points P on the line passing through points P1 and P2) can be written as
P = P1 + u (P2 - P1)
The intersection of these two occurs when
N dot (P1 + u (P2 - P1)) = N dot P3
Solving for u gives

u = (N dot (P3-P1) ) / (N dot (P2-P1))

P = P1 +   (N dot (P3-P1) ) / (N dot (P2-P1)) * (P2 - P1)


[P1,P2] => pq
[P3, P4, P5] = target
N = normal(rst)

P = pq[0] + dot(normal(rst), target[0]-pq[0]) 
		 / dot(normal(rst), pq[1]    -pq[0])
		 * (pq[1]-pq[0])


*/

function intersectPt( pq, target) = 
(
	len(target)>2
	?  // target =[r,s,t ...]: line pq intersets with plane rst
//	  pq[0] + dot(normal(rst)+target[1], target[1]-pq[0]) 
//		 / dot(normal(rst)+target[1], pq[1]    -pq[0])
//		 * (pq[1]-pq[0])
	  onlinePt( pq, len= dist([pq[0],projPt( pq[0], target )])
			/cos( angle([projPt( pq[0], target ),pq[0],pq[1]])) )	
	: //target =[r,s]: line pq intersets with line rs
	lineCrossPt( pq[0], pq[1], mn[0],mn[1] )
);

module intersectPt_demo()
{
	echom("intersectPt");

	rstu=  [[-2.65857, 0.0436345, -1.84591], [-1.33038, -1.84482, 1.12422], [3.58766, 0.935956, 0.693008], [2.25947, 2.8244, -2.27714]];

	T = rstu[2];

	MarkPts( rstu, ["labels","RSTU"] );
	
	pq= [[-0.591133, -2.27006, -1.82472], [2.36348, 2.54317, 0.528988]];

	pq2 = [ onlinePt( pq, ratio=0.25 ), onlinePt( pq, ratio= 0.75 ) ];
	P = pq2[0];
	Q = pq2[1];
	//echo( "pq2", pq2 );

	J = projPt( P, rstu );

	Line0( [P,J], ["r",0.01] );
	Mark90([P,J,T]);

	aJPQ = angle([J,P,Q]);
	echo("aJPQ", aJPQ);
	PX = dist([P,J])/cos( aJPQ);
	//X = onlinePt( [P,Q], dist = PX);

	X = onlinePt( [P,Q], len= dist([P,projPt( P, rstu )])/cos( angle([projPt( P, rstu ),P,Q])) );

	X = intersectPt( pq, rstu );

	MarkPts([P,Q,J,X], ["labels", "PQJX"]); 

	Line0( pq, ["r", 0.01] );
	Plane( rstu );
}

module intersectPt_demo_2()
{
	echom( "intersectPt_demo_2" );
	pq = randPts(2);
 	pq = [[-1.21325, 0.501097, -1.23254], [1.71766, 1.58609, 0.690459]];

	P = pq[0];
	Q = pq[1];
	rstu = randOnPlanePts(4);
	//rstu= [[2.51256, 0.0578135, -3.09367], [1.32865, 4.43852, -1.8887], [-5.60561, 0.027173, 3.29636],[0.429039, -0.251856, -1.47247] ];

	echo("pq", pq);
	echo("rstu",rstu);

	J = projPt( pq[0], rstu );
	echo("J", J );

	aJPQ = angle( [J,P,Q]);
	echo("aJPQ", aJPQ);

	//Dim( [P,J,rstu[1] ] );
	
	PJ = dist([P,J]);
	echo("PJ",PJ);

	PX = PJ / cos(aJPQ);
	echo("PX", PX);

	X = onlinePt( [P,Q], len = PX );
	echo( "X", X);
	MarkPts([J,X], ["labels","JX"]);

	Line0( [J, rstu[0]], ["r",0.01] );	
	Line0( [J, rstu[1]], ["r",0.01] );	
	Line0( [J, rstu[2]], ["r",0.01] );	
	Line0( [J, rstu[3]], ["r",0.01] );	

	Plane( rstu );
	MarkPts( pq, ["labels", "PQ"] );
	MarkPts( rstu, ["labels","RSTU"] );
	//X = intersectPt( pq, rstu );
	//MarkPts( X, ["labels","X"] );
	Line0(pq, ["r",0.02] );
}

//intersectPt_demo();
//intersectPt_demo_2();






/*


	P1 = Q + t * (P-Q)
	P2 = N + s * (M-N)

	when P1=P2

	Q.x + t(P-Q).x = N.x + t(M-N).x


	t = 

	let:	A = a point on both pq and mn
		c = PA / PQ
 		
	=> A = [ c * dx(pq), c * dy(pq), c * dz(pq) ] 
		= c * [ dx(pq), dy(pq), dz(pq) ] 
		= c* (Q-P)
		= PA (Q-P) / PQ

	let d = MA/MN
	=> A = MA (N-M ) / MN

	PA(Q-P)/PQ = MA(N-M)/MN

	 PA							   MA
	---- [ dx(pq), dy(pq), dz(pq) ] = ---- [ dx(mn), dy(mn), dz(mn) ] 
	 PQ							   MN	

	let a = PA/PQ, b = MA/MN

	a [ dx(pq), dy(pq), dz(pq) ] = b [ dx(mn), dy(mn), dz(mn) ] 
	
	a * dx(pq) = b * dx(mn)
	a * dy(pq) = b * dy(mn)
	a * dz(pq) = b * dz(mn)

	a =  b * dx(mn) / dx(pq)
	    (a * dy(pq) / dy(mn)) * dx(mn) / dx(pq)

			(N-M) PQ
	PA = MA ---------
			(Q-P) MN

	//-----------------------------------------------

      P3 
	   &'-_
	   |&  '-._     
	   | &     '-._
	   |  &        '-_ 
	   |   &         _'P2
	   |  b &      -' /
	   |     &  _-'  /	
	   |      X'    /
	   |  a -' &   /
	   |  -'    & /
	   |-'_______&
     P1            P4


http://paulbourke.net/geometry/pointlineplane/

Line a = P:Q
Line b = M:N

This note describes the technique and algorithm for determining the intersection point of two lines (or line segments) in 2 dimensions.

The equations of the lines are

Pa = P + ua ( Q - P )
Pb = M + ub ( N - M )

Solving for the point where Pa = Pb gives the following two equations in two unknowns (ua and ub)

P.x + ua (Q.x - P.x) = M.x + ub (N.x - M.x)

and 

P.y + ua (Q.y - P.y) = M.y + ub (N.y - M.y)

Solving gives the following expressions for ua and ub

k = [ (N.y-M.y)(Q.x-P.x) - (N.x-M.x)(Q.y-P.y) ]

ua = [ (N.x-M.x)(P.y-M.y) - (N.y-M.y)(P.x-M.x) ] / k

ub = [ (Q.x-P.x)(P.y-M.y) - (Q.y-P.y)(P.x-M.x) ] / k




Line a = P1:P2
Line b = P3:P4

This note describes the technique and algorithm for determining the intersection point of two lines (or line segments) in 2 dimensions.

The equations of the lines are

Pa = P1 + ua ( P2 - P1 )
Pb = P3 + ub ( P4 - P3 )

Solving for the point where Pa = Pb gives the following two equations in two unknowns (ua and ub)

x1 + ua (x2 - x1) = x3 + ub (x4 - x3)

and 

y1 + ua (y2 - y1) = y3 + ub (y4 - y3)

Solving gives the following expressions for ua and ub

k = [ (y4-y3)(x2-x1) - (x4-x3)(y2-y1) ]

ua = [ (x4-x3)(y1-y3) - (y4-y3)(x1-x3) ] / k

ub = [ (x2-x1)(y1-y3) - (y2-y1)(x1-x3) ] / k



	
*/


//);



//========================================================
is90=["is90", "pq1,pq2", "T|F", "Inspect, Geometry",
"
 Given 2 2-pointers pq1, pq2 (each represents a line), return
;; true if the angle between two lines is 90 degree.
;;
;; New 20140613: can take a single 3-pointer, pqr:
;;
;;   is90( pqr );
;;
;; Same as angle( pqr )==90. Refer to : othoPt_test() for testing.
"];

function is90(pq1, pq2)=
( 
	pq2==undef                   // When pq1 is a 3-pointer and pq2 not given; is90(pqr)
	? (pq1[1]-pq1[0]) * (pq1[2]-pq1[1])<ZERO  
	: p2v(pq1)*p2v(pq2)< ZERO    // is90( [p1,q1],[p2,q2] )
);

module is90_test(ops)
{
	pqr = randPts(3);           // 3 random points
	sqPts = 	squarePts(pqr);   //  This gives 4 points of a square, must be perpendicular
							  // The first 2 pts = pq[0], pq[1]. 	

	pq1 = [ pqr[0], pqr[1]   ];  // Points 0,1 of the square
	_pq2= [ pqr[1], sqPts[2] ];  // Points 1,2 of the square

	// Make a random translation to pq2x:
	t = randPt();
	pq2 = [ _pq2[0]+t, _pq2[1]+t ];

	doctest( is90
	,[
	  "// A 3-pointer, pqr, is randomly generated. It is then made to generate"
	 ,"// a square with sqarePts(pqr), in which side PQ is our pq1, and a side"
	 ,"// perpendicular to pq is translated to somewhere randomly and taken as pq2."
	,  [ "pq1,pq2", is90(pq1,pq2), true ]
	],ops, ["pq1",pq1,"pq2",pq2]  );
}

module is90_demo()
{
	pqr = randPts(3);

	sqPts = 	squarePts(pqr);   // This gives 4 points of a square, must be perpendicular

	//MarkPts(sqPts);

	pq = [ pqr[0], pqr[1]   ];
	pq2= [ pqr[1], sqPts[2] ];
	
	//echo(( pqr[0]-pqr[1])*(pqr[1]-sqPts[2] ));
	p3 = concat( pq, [pq2[1]] ); // 3 points that the middle is the 90-deg angle 

	//v=pow(norm( p3[0]-p3[1]),2)+ pow(norm(p3[1]-p3[2]),2)- pow( norm(p3[0]-p3[2]),2);
	//echo(v, v<1e-10, v ==0);

	Plane( p3, [["mode",3]] );
	MarkPts( p3 );
	echo( is90( pq, pq2 ) );

}
//is90_test();
//is90_demo();
//doc(is90);


//========================================================
isarr=[ "isarr","x", "T|F", "Type, Inspect, Array",
"
 Given x, return true if x is an array.
"];

function isarr(x)= type(x)=="arr";

module isarr_test( ops )
{
	doctest
	(
		isarr, [
		[ [1,2,3], isarr([1,2,3]), true]
	 ,	[ "''a''", isarr("a"), false]
	 ,	[ "true", isarr(true), false]
	 ,	[ "false", isarr(false), false]
	 ,	[ "undef", isarr(undef), false]
	 ],ops
	);
}
//isarr_test(["mode",22]);
//doc(isarr);



//========================================================
isbool=[ "isbool","x", "T|F", "Type, Inspect",
"
 Given x, return true if x is a boolean (true|false).
"];

function isbool(x)= x==true || x==false; 

module isbool_test( ops )
{
	doctest
	(
		isbool, [
		[ [1,2,3], isbool([1,2,3]), false]
	 ,	[ "''a''", isbool("a"), false]
	 ,	[ "true", isbool(true), true]
	 ,	[ "false", isbool(false), true]
	 ,	[ "undef", isbool(undef), false]
	 ],ops
	);
}
//doc(isbool);


//========================================================
isequal=["isequal", "a,b", "T|F", "Inspect",
_s(" Given 2 numbers a and b, return true if they are determined to
;; be the same value. This is needed because sometimes a calculation
;; that should return 0 ends up with something like 0.0000000000102.
;; So it fails to compare with 0.0000000000104 (another shoulda-been
;; 0) correctly. This function checks if a-b is smaller than the
;; predifined ZERO constant ({_}) and return true if it is.
", [ZERO])
];

function isequal(a,b)=
(
  	isnum(a)&&isnum(b)
	? (abs(a-b)< ZERO)
	: a==b
);

module isequal_test( ops )
{
	doctest( isequal, 
	[ 
		str("// ZERO = ",ZERO)
	 	,["1e-15,0", isequal(1e-15,0),true]
	 	,["1e-12,0", isequal(1e-12,0),false]
		,["1e-9,1e-10", isequal(1e-9,1e-10),false]
		,["1e-12,1e-13", isequal(1e-12,1e-13),false]
		,["1e-14,1e-13", isequal(1e-14,1e-13),true]
		,["''1e-15'',0", isequal("1e-15",0),false]
	 	,["''1e-15'',''0''", isequal("1e-15","0"),false]
	 	,["''abc'',''abc''", isequal("abc","abc"),true]
		,["undef,undef", isequal(undef,undef),true]
		,["undef,false", isequal(undef,false),false]
	 ],ops
	);
}


//doc(isequal);





//========================================================
isfloat=[ "isfloat","x", "T|F","Type, Inspect, Number",
"
 Check if x is a float.
"];

function isfloat(x)= type(x)=="float";

module isfloat_test( ops )
{
	doctest(isfloat, 
	[
		[ "3", isfloat(3), false]
	 ,	[ "3.2", isfloat(3.2), true]
	 ,	[ "''a''", isfloat("a"), false]
	],ops
	);
}
//isfloat_test(["mode",22]);
//doc(isfloat);




//========================================================
ishash = [ "ishash","x", "T|F", "Type, Inspect, Number",
"
 Check if x **could** be a hash, i.e., an array with even number length.
"];

function ishash(x)=  (type(x)=="arr") && ( len(x)/2 == floor(len(x)/2));

module ishash_test( ops=["mode",13] )
{
	doctest(ishash, 
	[
		[ "[1,2,3]", ishash([1,2,3]), false]
	 ,	[ "[1,2,3,4]", ishash([1,2,3,4]), true]
	 ,	[ "3", ishash(3), false]
	 ,	[ "3.2", ishash(3.2), false]
	 ,	[ "''a''", ishash("a"), false]
	],ops
	);
}
//ishash_test();


//========================================================
isint = [ "isint","x", "T|F", "Type,Inspect, Number",
"
 Check if x is an integer.
"];

function isint(x)=   type(x)=="int";

module isint_test( ops )
{
	doctest(isint, 
	[
		[ "3", isint(3), true]
	 ,	[ "3.2", isint(3.2), false]
	 ,	[ "''a''", isint("a"), false]
	],ops
	);
}
//isint_test(["mode",22]);
//doc(isint);



//========================================================
isnum =[ "isnum","x", "T|F", "Type,Inspect,Number",
"
 Check if x is a number.
"];

function isnum(x)=   type(x)=="int" || type(x)=="float";

module isnum_test( ops )
{
	doctest(isnum, 
	[
		[ "3", isnum(3), true]
	 ,	[ "3.2", isnum(3.2), true]
	 ,	[ "''a''", isnum("a"), false]
	],ops
	);
}
//isnum_test( ["mode",22] );
//doc(isnum);



//========================================================
function isOnline(pt, pq, exclusive=true)=
(
	/* under construction

	*/
2

);




//========================================================
isOnPlane=["isOnPlane", "pqr,pt,planecoefs", "T|F", "Inspect, Point, Plane",
"
 Given a 3-pointer (pqr) and a point (pt) and that will be converted to planecoefs,
;; (= [a,b,c,k] or [a,b,c]) or given planecoefs directly, check if the pt is on
;; the plane:
;;
;;   a*pt.x + b*pt.y + c*pt.z + k == ZERO
"];

function isOnPlane( pqr, pt )=
 let( pco = isarr(pqr[0])?planecoefs(pqr):pqr  
	, got =  pco[0]*pt.x
	   + pco[1]*pt.y
	   + pco[2]*pt.z
	   + (len(pco)==3?0:pco[3])
	)
(
	is0(got)
);

module isOnPlane_test( ops )
{
	pqr    = randPts(3);
	pcoefs = planecoefs( pqr );
	othopt = othoPt( pqr );
	abipt  = angleBisectPt( pqr );
	midpt  = midPt( p01(pqr) );

	doctest( isOnPlane,
	[
		
		 "## Randomly create a 3-pointer, pqr, and get its planecoefs, pcoefs."
		,"## From pqr also create othoPt, angleBisectPt and midPt (of p,q)."
		,"## They should all be on the same plane."
		,[ "pqr,P(pqr)", isOnPlane( pqr,P(pqr) ), true ]
		,[ "pqr, othopt", isOnPlane( pqr, othopt  ), true ]
		,[ "newy(pqr,othopt), othopt", isOnPlane( newy(pqr,othopt), othopt  ), true ]
		,[ "pcoefs, othopt", isOnPlane( pcoefs,othopt), true ]
		,"// angleBisectPt: "
		,[ "newy(pqr,abipt), abipt", isOnPlane( newy( pqr,abipt), abipt  ), true ]
		,[ "pcoefs, abipt", isOnPlane( pcoefs, abipt ), true ]
		,"// Mixed:"
		,[ "[othopt, abipt, midpt], pqr[2]"
			, isOnPlane( [othopt, abipt, midpt] , pqr[2] ), true ]
	], ops, ["pqr", pqr
		    , "pcoefs", pcoefs
			, "othopt", othopt
			, "abipt", abipt
			, "midpt", midpt]
	);
}



//========================================================
isstr= [ "isstr","x", "T|F", "Type,String,Inspect",
"
 Check if x is a string.\\
"];

function isstr(x)=   type(x)=="str";

module isstr_test( ops )
{
	doctest(isstr, 
	[
		[ "3", isstr(3), false]
	 ,	[ "3.2", isstr(3.2), false]
	 ,	[ "''a''", isstr("a"), true]
	],ops
	);
}
//isstr_test( ["mode",22] );
//doc(isstr);


//
////========================================================
is0=["is0","n","T|F","Inspect, Number",
str( " Given a number, check if it is 'equivalent' to zero. The 
;; decision is made by comparing it to the constant, ZERO=", ZERO )
,"isequal"
];

function is0(n) = abs(n)<ZERO;

module is0_test( ops=["mode",13] )
{
	doctest( is0
	,[
		"// A value, 1e-15, is NOT zero:"
		,str("//   1e-15==0: ", 1e-15==0 )
		,"// but it's so small that it is equivalent to zero:"
		,[ "1e-15",is0(1e-15), true ]
	 ],ops
	);
}


//========================================================
istype=["istype","o,t","T|F", "core, inspect",
"
  Given a variable (o) and a typename (t), return true if 
;; o is type t. It can also check if o is one of multiple types
;; given, in which case, t could be entered as:
;;
;; istype(o, [''arr'',''str'']
;; istype(o, ''arr,str'')
;; istype(o, ''arr str'')
"];

function istype(o,t)=
let( t = index(t,",")>=0
		? split(t,",")
		:index(t," ")>=0
		? split(t," "): t )
(	
//	[t, [for(x=t) type(o)==x?1:0 ] , sum([for(x=t) type(o)==x?1:0 ])]

	isstr(t)
	? type(o)==t
	: isarr(t)
	  ? has(t, type(o)) 
	  : false
);

module istype_test( ops=["mode",13] )
{
	doctest( istype,
	[
		["''3'',''str''", istype("3","str"), true]
		,["''3'',[''str'',''arr'']", istype("3",["str","arr"]), true]
		,["3,[''str'',''arr'']", istype(3,["str","arr"]), false]
		,["3,[''str'',''arr'',''int'']", istype(3,["str","arr","int"]), true]
		,["3,''str,arr,int''", istype(3,"str,arr,int"), true]
		,["3,''str arr int''", istype(3,"str arr int"), true]
		,["3,''str arr bool''", istype(3,"str arr bool"), false]
	], ops
	);
}


//========================================================
linePlcrossPt=["linePlcrossPt","pq,","point", "Geometry",
"


"];



//========================================================
join=[ "join", "arr, sp=\",\"" , "string","Array",
" Join items of arr into a string, separated by sp.
"];

function join(arr, sp="", _i_=0)=
//let(L=len(arr)) 
(
	_i_<len(arr)?
	str(
		arr[ _i_ ]
		, _i_<len(arr)-1?sp:""
		,join(arr, sp, _i_+1)
	):""

//	L==0?"":str( arr[0], L==1?"":sp, join(slice(arr,1),sp=sp) )

); 

module join_test( ops )
{
	doctest( join, 
	
	[
	    [ "[2,3,4]",      join([2,3,4]),      "234" ]
	  , [ "[2,3,4], ''|''", join([2,3,4], "|"), "2|3|4" ]
	  , [ "[2,[5,1],''a''], ''/''", join([2,[5,1],"a"], "/"), "2/[5, 1]/a" ]
	  , [ "[], ''|''", join([], "|"), "" ]
	],ops
	);
}
//join_test( ["mode",22] );
//doc(join);



//========================================================
joinarr=[ "joinarr", "arr" , "array","Array",
" Given an array(arr), concat items in arr. See the difference below:
;; 
;;  concat( [1,2], [3,4] ) => [1,2,3,4] 
;;  joinarr( [ [1,2],[3,4] ] ) => [1,2,3,4] 
;;  join( [ [1,2],[3,4] ] ) => ''[1,2],[3,4]'' 
"];

function joinarr(arr,_I_=0)= 
(
	_I_>len(arr)-1? []
	: concat( arr[_I_], joinarr( arr, _I_+1) )  
); 

module joinarr_test( ops )
{
	doctest( joinarr, 
	
	[
	    [ "[[2,3],[4,5]]",  joinarr([[2,3],[4,5]]),    [2,3,4,5] ]
//	  , [ "[2,3,4], ''|''", joinarr([2,3,4], "|"), "2|3|4" ]
//	  , [ "[2,[5,1],''a''], ''/''", joinarr([2,[5,1],"a"], "/"), "2/[5, 1]/a" ]
//	  , [ "[], ''|''", joinarr([], "|"), "" ]
	],ops
	);
}
//join_test( ["mode",22] );
//doc(join);



//========================================================
keys=["keys", "h", "array", "Hash",
"
 Given a hash (h), return keys of h as an array.
"];

function keys(h)=
(
	[for(i=[0:2:len(h)-2]) h[i]]
);

module keys_test( ops )
{
	doctest
	( keys, 
	  [
		[ "[''a'',1,''b'',2]", keys(["a",1,"b",2]), ["a","b"] ]
	  ,	[ "[1,2,3,4]", keys([1,2,3,4]), [1,3] ]
	  ,	[ "[1,2],0,3,4", keys([[1,2],0,3,4]), [[1,2],3] ]
	  ,	[ "[]", keys([]), [] ]
	  ],ops
	);
}
//keys_test( ["mode",22] );
//doc(keys);


//========================================================
kidx=["kidx", "h,k", "hash", "Array, Hash"
, "Return index of key k in hash h. It's the order of k
;; in keys(h).
;; 2015.1.30"
];
function kidx(h,k)=
    [for(i=[0:2:len(h)-2]) if(h[i]==k)i/2][0];


//========================================================
LabelPt=["LabelPt", "p,label,ops=[]", "n/a", "Geometry, Doc",
 " Given a point (p), a string (label) and option (ops), write the
;; label next to the point. The label is written according to the 
;; viewpoint ($vpr) such that it is always facing the viewer. Other
;; properties of label are decided by ops. 
"];

module LabelPt_test( ops=["mode",13] ) { doctest( LabelPt, ops=ops ); }

// Require TextGenerator 
module LabelPt(p,label, ops=[]) 
{
	//include <../others/TextGenerator.scad>
	ops = concat( ops,
	[ "shift", false  // could be: 2.5, [3,1,0] 
	, "scale", 0.03
	], OPS);
	scale = hash(ops,"scale");
	_shift = hash(ops, "shift"
				, if_v=false
				, then_v=[1,1,1]*scale*3 );
				//, else_v= [0,0,0] ) ;
	shift = isarr(_shift)?_shift:_shift*[1,1,1] ;
	//echo("shift", shift, "scale", scale);
	//echo("p",p,"label: ", label);
	//echo("LabelPt ops", ops);
	translate( p+ shift ) 
		scale( scale ) 
		rotate($vpr) 
		color( hash(ops,"color"), hash(ops,"transp") ) 
		text( str(label), font="Liberation Mono" );
		//drawtext( str(label) );
		
}

module LabelPt_demo()
{
	pqr = randPts(3);
	MarkPts( pqr, ["r", 0.05] );

	LabelPt( pqr[0], "P" );
	LabelPt( pqr[1], "Q" );
	LabelPt( pqr[2], "R" );

	pt= randPt();
	MarkPts([pt]);
	LabelPt ( pt, "Scale=0.05", ["scale",0.05] ); 
	 
}
//LabelPt_demo();




//========================================================
LabelPts=["LabelPts", "pts,labels,ops=[]", "n/a", "Geometry, Doc",
 " Given points (pts), labels (array of strings or a string) and option
;; (ops), write the
;; label next to the point. The label is written according to the 
;; viewpoint ($vpr) such that it is always facing the viewer. Other
;; properties of label are decided by ops. 
;;
;; New args 2014.8.16: 
;;
;;   prefix, suffix, begnum, usexyz
;;
"];

module LabelPts_test( ops=["mode",13] ) { doctest( LabelPts, ops=ops ); }


module LabelPts( pts, labels="", ops=[] )
{
	//echom("LabelPts");
	ops = concat(ops
	, [ "color", [0.4,0.4,0.4]
	  , "colors", COLORS
	  , "prefix", "" //labels?"":""
	  , "suffix", ""
	  , "begnum", 0
	  , "usexyz",false
      ], OPS );
	function ops(k)= hash(ops, k);
	begnum = ops("begnum");
	prefix = ops("prefix");
	suffix = ops("suffix");
	usexyz = ops("usexyz");
    colors = hash( ops, "colors"); //, false)
    
	function getlabel(i)= 
	(
		usexyz
		? pts[i]
		:str( prefix, labels?labels[i]:(begnum+i), suffix )
	);
    
	//echo("ops",ops);
	circle =concat([get(pts,-1)], pts, [pts[0]]);
	//echo("circle: ",circle);
	if(len(pts)<3){

//		for (i=[0:len(pts)-1])	assign( 
//			colors = hash( ops, "colors", false)
//		){ 
        for (i=[0:len(pts)-1])	{
            
			LabelPt( pts[i]
				, getlabel(i) //labels?labels[i]: getlabel(i)
				, concat( ["color", hash(ops,"color", colors[i])]
						, ops ) ); 
	
		}
	
	}else{

		for (i=[0:len(pts)-1])
		{
            abp = angleBisectPt( 
                   [ circle[i], circle[i+1], circle[i+2] ] 
            );			   
//		for (i=[0:len(pts)-1])
//		assign( colors = hash( ops, "colors", false)
//			 , abp = angleBisectPt( [ circle[i], circle[i+1], circle[i+2] ] )
//			 ) 
//		{ 
			//echo("in LabelPts, i = ", i);
			LabelPt( onlinePt( [pts[i],abp], len=-0.2)
				, getlabel(i) //labels?labels[i]:getlabel(i)
				, concat( ["color", hash(ops,"color", colors[i])]
						, ops ) ); 
	
		}
	}

}


module LabelPts_demo_1()
{
	pqr = randPts(3);
	MarkPts( pqr, ["r", 0.05] );
	LabelPts( pqr, "PQR" );
	Plane( pqr, ["mode",3] );

	lmn = randPts(3);
	MarkPts( lmn, ["r",0.05] );
	Plane( lmn, ["mode",3] );
	LabelPts( lmn, ops=["color","red"]);

	fghi = randPts(4);
	MarkPts( fghi, ["r",0.05] );
	Plane( fghi, ["mode",3] );
	LabelPts( fghi, "FGHI", ["color","green"] );

//	A = randPts(1);
//	MarkPts( A, ["r",0.05, "grid", true] );
//	LabelPts( A, ["Single A"], ["color","blue"] );

	pts = randPts(5);
	MarkPts( pts, ["r",0.05, "samesize",true, "sametransp",true] );
	LabelPts( pts, range(pts), ["color","purple"] );
	Chain(pts, ["r", 0.02 ] );
}
module LabelPts_demo_2_scale()
{
	pqr = randPts(3);
	MarkPts( pqr, ["r", 0.05] );
	LabelPts( pqr, "PQR" );
	Chain( pqr, ["r",0.02, "closed", false, "color","red"] ); 
	LabelPt( pqr[0], "scale=0.03 (default)",["color","red"] );

	lmn = randPts(3);
	MarkPts( lmn, ["r",0.05] );
	Chain( lmn, ["r",0.02, "closed", false, "color","green"] ); 
	LabelPts( lmn, "lmn", ops=["scale",0.06]);
	LabelPt( lmn[0], "scale=0.06", ["color","green"] );

	fghi = randPts(4);
	MarkPts( fghi, ["r",0.05] );
	Chain( fghi, ["r",0.02, "closed", false, "color","blue"] ); 
	LabelPts( fghi, "FGHI", ["scale",0.02,"color","red"] );
	LabelPt( fghi[0], "scale=0.02", ["color","blue"] );
}

module LabelPts_demo_3_prefix_begnum_etc()
{
	// New 2014.8.16:
	// prefix, suffix, begnum, usexyz

	// Testing prefix, suffix with given labels 
	pqr = randPts(3);
	MarkPts( pqr, ["r", 0.05] );
	LabelPts( pqr, range(pqr),["color","red", "prefix","p[", "suffix","]"] );
	Chain( pqr, ["r",0.02, "closed", false] ); 


	// Testing begnum=10 
	lmn = randPts(3);
	MarkPts( lmn, ["r",0.05] );
	Chain( lmn, ["r",0.02, "closed", false, "color","green"] ); 
	LabelPts( lmn,ops=["scale",0.06, "begnum",10]);


	fghi = randPts(4);
	MarkPts( fghi, ["r",0.05] );
	Chain( fghi, ["r",0.02, "closed", false, "color","blue"] ); 
	LabelPts( fghi, fghi, ops=["scale",0.02,"color","blue"] );
	//LabelPt( fghi[0], "scale=0.02", ["color","blue"] );
}
module LabelPts_demo()
{
	//LabelPts_demo_1();
	//LabelPts_demo_2_scale();
	LabelPts_demo_3_prefix_begnum_etc();
}
//LabelPts_demo();



//========================================================
last=[ "last", "o", "item", "Index, String, Array" ,
"
 Given a str or arr (o), return the last item of o.
"];

function last(o)= o[len(o)-1]; 

module last_test( ops )
{
	doctest
	( last, 
	 [ [ [20,30,40,50], last([20,30,40,50]), 50 ]
    	 , [ "abcdef", last("abcdef"), "f" ]
     ],ops
	);
}
//last_test( ["mode",22] );
//doc(last);



//========================================================
lcase=["lcase", "s","str","String",
 " Convert all characters in the given string (s) to lower case. "
];
_alphabetHash= split("A,a,B,b,C,c,D,d,E,e,F,f,G,g,H,h,I,i,J,j,K,k,L,l,M,m,N,n,O,o,P,p,Q,q,
R,r,S,s,T,t,U,u,V,v,W,w,X,x,Y,y,Z,z",",");
function lcase(s, _I_=0)=
(
  	_I_<len(s)
	? str( hash( _alphabetHash, s[_I_], notfound=s[_I_] )
		, lcase(s,_I_+1)
		)
	:""
);

module lcase_test( ops= ["mode",13] )
{
	doctest( lcase,
	[
		["''aBCC,Xyz''", lcase("aBC,Xyz"), "abc,xyz"]
	], ops
	);
}

//========================================================
Line0 =[ "Line0", "pq,ops", "n/a", "3D, Shape",
"
 Given a 2-pointer pq and ops, draw a cylinder as a simple
;; line between p,q in pq. Default ops:
;;
;;   [ ''r'' , 1
;;   , ''color'',false
;;   , ''transp'', 1
;;   , ''fn'', $fn
;;   , ''markpts'', false // Run MarkPts if true
;;   , ''grid'', false // draw grid for p (grid=1) or q(=2) or both (=3)
;;   ]
"];

module Line0( pq, ops )
{
	//echom("Line0");
	ops=concat( ops, [
	[ "r"      , 0.1 
	, "grid"   , false  
	, "markpts", false   
	], OPS 
	]);	
	function ops(k)= hash(ops,k);
	
	g = ops("grid");
	if(g==1||g==3) PtGrid(p); 
	if(g==2||g==3) PtGrid(q); 
	//echo("pq : ", pq);
	//echo(" in Line0 ops = ", ops);
	// Re-sort p,q to make p has the min xyz. This is to 
	// simplify the calc of rotation later.
	p = minyPts(minxPts(minzPts(pq)))[0];
	q = p==pq[0]?pq[1]:pq[0];
	
	//echo("in Line0, color=", ops("color"), ", transp", ops("transp"), ", r=", ops("r"));
	translate( p )
	rotate( rotangles_z2p( q-p ))
	color( ops("color"), ops("transp") )
	cylinder( r=ops("r"),h=norm(q-p), $fn= ops("fn") );

	m = ops("markpts");
	if(m) { MarkPts(pq, (isarr(m))?m:[]);}
	
}

module Line0_test( ops ) { doctest( Line0, ops=ops ); }
//Line0_test( ["mode",22] );


module Line0_demo_1()
{ 
	echom("Line0_demo_1");

	pq = randPts(2); //MarkPts(pq);
	Line0( pq);

	pq1 = randPts(2); //MarkPts(pq1);
	Line0( pq1, ["color","red","markpts",true] );

	pq2 = randPts(2);
	Line0( pq2, ["r",0.3, "color","blue"
				,"transp",0.2, "markpts",["r",0.3]] );

}

module Line0_demo_2_verify_2D()
{
	echom("Line0_demo_2_verify_2D");

	ops=["markpts", true
		, "grid", 1
		, "transp", 0.4
		];
	//pq= [ [ 1.5, 1, 0 ], [ 1, 3, 0 ] ];
	pq= [ pxy0(randPt()), pxy0(randPt()) ]; 
	//MarkPts( pq, ["grid",true] );
	Line0( pq, update( ops, ["color","red"]) ); //["markpts", true, "grid", 3] );

	
	pq2= [ [ 1.5, 0,1 ], [ 1, 0,3 ] ];
	pq2= [ px0z(randPt()), px0z(randPt()) ];
	//MarkPts( pq2, ["grid",true] );
	Line0( pq2, update( ops, ["color","green"]) ); //, ["markpts", true, "grid", 3] );

	pq3= [ [ 0,1.5, 1 ], [ 0, 1, 3 ] ];
	pq3= [ p0yz(randPt()), p0yz(randPt()) ];
	//MarkPts( pq3, ["grid",true] );
	Line0( pq3, , update( ops, ["color","blue"]) ); // ["markpts", true, "grid", 3] );	
}

module Line0_demo()
{
	//Line0_demo_1();
	Line0_demo_2_verify_2D();
}
//Line0_demo();
//doc(Line0);
	


//========================================================
Line_by_x_align=["Line_by_x_align", "pq,ops", "n/a", "3D, Shape" ,
"
 Given a 2-pointer (pq), draw a line connecting the 2 pts.
;; Unlike Line0/Line that starts with a line on z-axis then
;; rotates/translates to pq, this module starts with a line
;; on x-axis then to pq. Therefore, different rotation angles
;; are used to do the rotation. As a result, this module serves
;; as a backup module to chk the Line0/Line results.
"];

module Line_by_x_align( pq, ops )
{
	ops = update(  [ "r", 0.05
				  , "color",false
				  , "transp", 1
				  , "fn", false 
				  , "markpts", false
				  ], ops	
				);
	function ops(k) = hash(ops, k);

	// First, rearrange the order of pq into p,q such that 
	// p has smaller x. If x equal, then p has smaller y; if 
	// both x and y are equal, then p has smaller z. If all 
	// x,y,z are equal ... wait, r u kidding me?
	// This can also be achieved with:
	p = minzPts(minyPts(minxPts(pq)))[0];	
	q = p==pq[0]?pq[1]:pq[0];           
	
	L = norm(p-q);
	r = ops("r");
	color = ops("color");
	transp = ops("transp");
	fn = ops("fn");

	x = q[0]-p[0];
	y = q[1]-p[1];
	z = q[2]-p[2];

	if(ops("markpts")) MarkPts(pq);	

	ay = -atan( z/slopelen(x,y) );  // rotate@y to xz-plane
	az = x==0?90:atan(y/x); 		   // rotate from xz-plane to target 	
	rot = [0, ay, az ];
	translate( p ) 
	rotate( [0,90,0]+rot )
	color(color, transp)
	cylinder( h=L, r=r, $fn=fn?fn:$fn );
}	

module Line_by_x_align_test( ops ){ doctest(Line_by_x_align, ops=ops); }

module Line_by_x_align_demo()
{ 
	echom("Line_by_x_align_demo");

	pq = randPts(2); //MarkPts(pq);
	Line_by_x_align( pq );

	pq1 = randPts(2); //MarkPts(pq1);
	Line_by_x_align( pq1, [["color","red"],["markpts",true]] );

	pq2 = randPts(2);
	Line_by_x_align( pq2, [["r",0.3], ["color","blue"]
				,["transp",0.2], ["markpts",true]] );
}

module Line_by_x_align_demo_2_verify_2D()
{
	pq= [ [ 1.5, 1, 0 ], [ 1, 3, 0 ] ];
	MarkPts( pq, [["grid",true]] );
	Line_by_x_align( pq, [["markpts", true]] );

	
	pq2= [ [ 1.5, 0,1 ], [ 1, 0,3 ] ];
	MarkPts( pq2, [["grid",true]] );
	Line_by_x_align( pq2, [["markpts", true]] );

	pq3= [ [ 0,1.5, 1 ], [ 0, 1, 3 ] ];
	MarkPts( pq3, [["grid",true]] );
	Line_by_x_align( pq3, [["markpts", true]] );
	
}
//Line_by_x_align_test( ["mode",22] );
//Line_by_x_align_demo();
//Line_by_x_align_demo_2_verify_2D();
//doc(Line_by_x_align);




////========================================================
//Line =["Line", "pq,ops", "n/a", "3D, Shape",
//"
// Given a 2-pointer pq and ops, draw a line between p,q in pq. The
//;; default ops:

module Line(pq, ops=[])
{
    //_mdoc("Line","");
    //P= pq[0]; Q=pq[1];
    pqr= len(pq)==3?pq:concat(pq, [randPt()]); 
    
    df_ops=[ "r", 0.01     // radius
        , "rP", undef   // radius at the P end
        , "rQ", undef   // radius at the Q end
    
        , "len", d01(pq) 
        , "ext", 0      // extend outward. A # for len or "r#" for ratio
        , "extP",undef  // extend outward from P pt
        , "extQ",undef  // extend outward from Q pt
    
        , "fn", $fn
        , "markpts", false // Mark pts. false:disable; true:default
                           // hash to customize
        , "dim", false // true or hash, to show dimension. 
    
        , "a", 0    
        , "aP", undef
        , "aQ", undef
    
        , "twist", 0   // twist the line pts counter-clockwise away
                       // from the pqr plane when pq is a 3-pointer
                       // (= pqr). If pq is a 2-pointer, a random
                       // R will be assigned. 
        , "shift", undef
        ]; 
     
    _ops= update( df_ops, ops);
     //echo(" line,_ops", _ops);
    function ops(k,df)=hash(_ops,k,df);
       
    len= ops("len");
    ext= ops("ext");
    // if "r0.4", treat it as a ratio and convert to len
        _extP=ops("extP", ext);
        _extQ=ops("extQ", ext);
        extP = isstr(_extP)&&index(_extP,"r")==0
                ?num(slice(_extP,1))*len: _extP ;
        extQ = isstr(_extQ)&&index(_extQ,"r")==0
                ?num(slice(_extQ,1))*len: _extQ ;

    r = ops("r");
    rP= ops("rP",r);
    rQ= ops("rQ",r);
    
    fn= ops("fn");
    tw= ops("twist");
    sh= ops("shift");
    _markpts = ops("markpts"); 
    markpts = _markpts==true?[]:_markpts;
    
    //echo("ops", ops);

    newpq= linePts( pqr, len= [extP,extQ] );
    
    P= newpq[0]; 
    Q= newpq[1]; 
    R= pqr[2];
    
    //pq = linePts;
    //echo(_s( "P={_}, Q={_}",[P,Q]));
    pqr2= [P,Q,R];

    dim=ops("dim");
    if(dim) Dim2( pqr2, ops=ishash(dim)?dim:[]); //
    //echo("=> pqr2", pqr2 );
    // S is to make same clockwise-ness on P and Q
    //
    //  o-----o================o
    //  S     P                Q
    S = onlinePt(pqr2, len=-1);   
    spr = [S,P,R]; 
    ptsP = arcPts( pqr2, a=360, r=rP, count=fn, pl="yz" );
    ptsQ = arcPts( spr, a=360, r=rQ, count=fn, pl="yz" );
    
    pts = concat( ptsQ, ptsP );
    
    //echo("markpts", markpts);
    
    if(ishash(markpts))
        Chainf(pts, concat(markpts,["labelbeg",0]) );
    
    faces = faces("rod", sides=fn); 
    //echo("faces", faces);
    
    color(ops("color"), ops("transp"))
    polyhedron( points = pts, faces= faces );
}

module Line_demo_1_basic() 
{_mdoc("Line_demo_1_basic","");
 
    pq = randPts(2);
    MarkPts( pq, ["labels","PQ"] );
    Line( pq );
    LabelPt( pq[0], "Line(pq)");
    
    pq2= randPts(2);
    Line(pq2, ["dim",true]);
    LabelPt( pq2[0], "Line(pq, [\"dim\",true])", ["color","blue"]);
    
    pqr= randPts(3);
    MarkPts( pqr, ["labels", "PQR"] );
    Line( pqr, ["dim",true] );
    LabelPt( pqr[1], "Line(pqr, [\"dim\",true])", ["color","green"]);
}    

module Line_demo_2()
{_mdoc("Line_demo_2","");
    
    pqr = randPts(3);
    //MarkPts( p01(pqr), ["labels","PQ"] );
    Line( pqr, ["r",0.2, "len", 3, "dim",true] );
    //Dim2( pqr );
    
    pq2= randPts(2);
    Line(pq2);
    LabelPt( pq2[0], "Line(pq)");
}    
//Line_demo_1_basic();
//Line_demo_2();


module Line_test( ops ){doctest (Line, ops=ops); }


module Line_demo_1_default()
{
	echom("Line_demo_1_default");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true]);
}

module Line_demo_1a_extension()
{
	ops=["transp",0.3,  "markpts",true, "transp",0.4] ;

	echom("Line_demo_1a_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, update(ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line(pq2, update(ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	Line(pq3, update(ops, ["color","blue","extension0", 1,"extension1", 1]));

	pq4= randPts(2);
	//MarkPts(pq3);
	Line(pq4, update(ops, ["color","purple","extension0", -0.5,"extension1", -0.5]));

}

module Line_demo_2_sizeColor()
{
	echom("Line_demo_2_sizeColor");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, [ "r", 0.5
			 , "color","green"
			 , "transp",0.3
			 , "fn", 6
			 , "markpts",true
			 ]);
}

module Line_demo_3a_head_cone()
{
	echom("Line_demo_3a_head_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true, "head"
			  ,["style","cone"]
			 ]);
}

module Line_demo_3b_head_cone_settings()
{
	echom("Line_demo_3b_head_cone_settings");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true,"head"
			  , [ "style","cone"
				,"color","blue"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5
			    ]
			 ]);
}

module Line_demo_4a_tail_cone()
{
	echom("Line_demo_4a_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				, "color","red"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5		    
				]
			 ]);
}

module Line_demo_4b_tail_cone()
{
	echom("Line_demo_4b_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				,"r", 0.2
				, "r0",0.6
				]
			 ]);
}

module Line_demo_5a_headtail_cone()
{
	echom("Line_demo_5a_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq,["markpts",true
			  ,"head"
			  , [ "style","cone"
			    ]
			  
			, "tail"
			  , [ "style","cone"
			 	]
			  
			 ]);
}

module Line_demo_5b_headtail_cone()
{
	echom("Line_demo_5b_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"head"
			  , [ "style","cone"
				, "color","blue"
				, "r", 0.4
				, "len",1
			 	, "transp", 0.6
				, "fn", 5
			    ]
			, "tail"
			  , [ "style","cone"
				, "r", 0.2
				, "r0",0.6
			 	]
			 ]);
}

module Line_demo_6a_headtail_sphere()
{
	echom("Line_demo_6a_headtail_sphere");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"head"
			  , [ "style","sphere"
				, "color", "yellow"
				, "r",.5
				, "transp", 0.4
			    ]
			  
			, "tail"
			  , [ "style","sphere"
				,"color", "red"
				, "r", .4
				, "transp", 0.4
				]
			  
			 ]);
}


module Line_demo_7_headtail_fn()
{
	echom("Line_demo_7_headtail_fn");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				]
			 ]);
}


module Line_demo_8_rotate()
{
	echom("Line_demo_8_rotate");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				, "rotate", 45  //<====
				]
			 ]);
}


module Line_demo_9a_block()
{
	echom("Line_demo_9a_block");
	pq= randPts(2);
	//pq = [ [2,1,3], [4,3,2]];
	//MarkPts(pq);
	Line(pq, ["markpts",true
			  ,"style","block"]);
}


module Line_demo_9a2_block_extension()
{
	ops=["transp",0.3,  "markpts",true,"style","block"] ;

	echom("Line_demo_9a2_block_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, update( ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line(pq2, update( ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	//Line(pq3, update( ops, [["color","blue"],["extension0", 1],["extension1", 1]]));

	pq4= randPts(2);
	//MarkPts(pq3);
	//Line(pq4, update( ops, [["color","purple"],["extension0", -0.5],["extension1", -0.5]]));

}

module Line_demo_9b_block()
{
	echom("Line_demo_9b_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 //, "markpts",true
		];

	Line([O, [0,0,z+1]], [ "style","block"
					   //, ["markpts",true]
			  		   , "width",0.5
					   , "depth",4
					  ]);

	Line([[0,0,w], [x,0,w]], update(ops, ["color","red"]));
	Line([[x-w,0,0], [x-w,0,z]], update(ops, ["color","green"]) );

	Line( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true]
		 ) );
	
}


module Line_demo_9c_block()
{
	echom("Line_demo_9c_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","corner2"] // <====
					  ) );


}

module Line_demo_9d_block()
{
	echom("Line_demo_9d_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //["markpts",true]
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid1"] // <====
					  ) );

}

module Line_demo_9e_block()
{
	echom("Line_demo_9e_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [//"markpts",true
			  			 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid2"] // <====
					  ) );

}


module Line_demo_9f_block()
{
	echom("Line_demo_9f_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], [//"markpts",true
		 				 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","center"] // <====
					  ) );

}

module Line_demo_9g_block()
{
	echom("Line_demo_9g_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line([O, [0,0,z+1]], ["style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line([[0,0,w], [x,0,w]], ops);
	Line([[x-w,0,0], [x-w,0,z]], ops);

	Line( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift",[2*w, 0,w] ]// <====
					  ) );

}

module Line_demo_10_shift()
{
	echom("Line_demo_10_shift");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	pq= randPts(2);
	//MarkPts(pq);
	Line(pq, ["markpts",true
		 	 , "r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "green"
				, "transp",0.5
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "transp",0.5
			    ]
			,"shift", "corner2"
			 ]);
}

module Line_demo2()
{
	pts = [ [-1,0,-1], [2,3,4]  ];
	MarkPts( pts );
	pts = randPts(2);
	//Line2( pts,[["r",0.5]] );
	//MarkPts(pts, [["transp",0.3],["r",0.25]] );
	Line( pts //, [["style","block"]
		, ["markpts",true
		 , "style","block", "transp",0.5
		, "rotate", $t*60
		, "shift", "corner"
		, "head", ["style","cone"]
		, "tail", ["style","cone"]
			]
			);
}

module Line_demo_11_block_touch()
{
	echom("Line_demo_11_block_touch");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "markpts",true
		 , "style","block"
		 , "width",w
		 , "depth",y
		 , "transp",0.5
		 ];

	p_touch = [x/3,y/3,z/3];
	MarkPts([ p_touch ] , ["r",0.3,"transp",0.3]);

	//Line([O, [0,0,z+1]], [ ["style","block"]
	//				   , ["width",w]
	//				   , ["depth",y]
	//				  ]);
	//Line([[0,0,w], [x,0,w]], ops);
	//Line([[x-w,0,0], [x-w,0,z]], ops);

	pts = [[x, 0, z], [0, 0, z+1]];
	//pts=randPts(2);
	Line( pts );
	//MarkPts( pts );
	
	Line( pts
		, update(ops, ["color","blue"//, "shift","corner2"
					  ,"touch", p_touch]  // <====
					  ) );
}

module Line_demo_12_verify_2D()
{
	//pq= [ [ 1.5, 1, 0 ], [ 1, 3, 0 ] ];
	pq_= randPts(2);
	echo(pq_);
	
	pq= [ [ pq_[0].x, pq_[0].y,0]
		, [ pq_[1].x, pq_[1].y,0] ];
	echo(pq);
	MarkPts( pq, ["grid",true] );
	Line( pq, ["markpts", true] );

	/*
	pq2= [ [ 1.5, 0,1 ], [ 1, 0,3 ] ];
	MarkPts( pq2, ["grid",true] );
	Line( pq2, ["markpts", true] );

	pq3= [ [ 0,1.5, 1 ], [ 0, 1, 3 ] ];
	MarkPts( pq3, ["grid",true] );
	Line( pq3, ["markpts", true] );
	*/
}

module Line_demo()
{
	//Line_demo_1_default();
	//Line_demo_1a_extension(); // 2014.5.18
	//Line_demo_2_sizeColor();
	//Line_demo_3a_head_cone();
	//Line_demo_3b_head_cone_settings();
	//Line_demo_4a_tail_cone();
	//Line_demo_4b_tail_cone();
	//Line_demo_5a_headtail_cone();
	//Line_demo_5b_headtail_cone();
	//Line_demo_6a_headtail_sphere();
	//Line_demo_7_headtail_fn();
	//Line_demo_8_rotate();
	//Line_demo_9a_block();
	//Line_demo_9a2_block_extension();
	//Line_demo_9b_block();
	//Line_demo_9c_block();
	//Line_demo_9d_block();
	//Line_demo_9e_block();
	//Line_demo_9f_block();
	//Line_demo_9g_block();
	//Line_demo_10_shift();
	//Line_demo_11_block_touch();
	//Line_demo_12_verify_2D();
}
//Line_test( ["mode",22] );
Line_demo();



//========================================================
Line2 =[["Line2", "pq,ops", "n/a", "3D, Shape"]
	  ,["Given a 2-pointer pq and ops, draw a line between p,q in pq."
	   ,"The default ops:"
		,"                                           " 
		," [[''r'', 0.05]                   // radius of the line    "
		," ,[''style'',''solid'']       // solid | dash | block      "
		," ,[''color'', ''yellow'']                                  "
		," ,[''transp'', 1]                       // transparancy    "
		," ,[''fn'', $fn]                                            "
		," ,[''rotate'', 0]             // rotate alout the line     "
		,", [''markpts'', false]      // Run MarkPts if true"
	    ," ,[''shift'', ''corner''] //corner|corner2|mid1|mid2|center   "
		,"  //................. style-block settings                 "
		," ,[''width'', 1]                                           "
		," ,[''depth'', 6]                                           "
		," ,[''touch'', false ] // a pt where the block will rotate  "
		,"                      // until the block touches the pt.   "
		,"  //................. style-dash settings                  "
		," ,[''dash'', 0.4]     // len of dash                       "
		," ,[''space'', false]  // space between dashes              "
		,"  //................. head/tail                            "
		," ,[''head'',head_ops ]                                     "
		," ,[''tail'',tail_ops ]                                     "
		," ]                                                         "
		,"where head_ops and tail_ops both are:                      "
		,"    [[''style'',false]   // false|cone|sphere              "
		,"    ,[''len'',line_r*6]       // length                    "
		,"    ,[''r'',line_r*3]         // radius                    "    
		,"    ,[''r0'',0]          // smaller radius                 "
		,"    ,[''fn'',ops(''fn'')]                                  "
		,"    ,[''rotate'',0]      // rotate about the line          "  
		,"    ,[''color'', ops(''color'')]                           " 
		,"    ,[''transp'',ops(''transp'')]                          "
		,"    ]                                                      "
		]
		];

module Line2( pq, ops){

	
	//echom("Line");
	//echo("In Line, original pq: ", pq);

	ops= updates( OPS,
		[ "r", 0.05			// radius of the line
		, "style","solid"   // solid | dash | block
		, "markpts", false
		, "shift", "corner" //corner|corner2|mid1|mid2|center
		//................. style-block settings	
		, "w", 1  //,"width", 1
		, "depth", 6
		, "touch", false  // a pt where the block will rotate 
					    // until the block touches the pt.  
		//................. style-dash settings	
		, "dash", 0.4     // len of dash
		, "space", false  // space between dashes
		//................. head and tail extension 2014.5.18
		, "extension0", 0  
		, "extension1", 0  
		//....................
		, "head", false
		, "tail", false

		], ops); function ops(k)= hash(ops,k);
	
	if(ops("markpts")) MarkPts(pq);	

	style = ops("style");
	r 	  = ops("r");
	w     = ops("w");
	headops  = ops("head");
	tailops  = ops("tail");
	depth = ops("depth");
	extension0 = ops("extension0");
	extension1 = ops("extension1");

	//echo("style",style, "rotate", ops("rotate"));

	// Re-sort p,q to make p has the min xyz. This is to 
	// simplify the calc of rotation later.
	p = minyPts(minxPts(minzPts(pq)))[0];
	switched = p!=pq[0];
	//echo("switched? ", switched);
	q = switched?pq[0]:pq[1]; //:pq[0]; 
	L = norm(p-q);
	
	x = q[0]-p[0];
	y = q[1]-p[1];
	z = q[2]-p[2];

	rot = rotangles_z2p( q-p ); 
	//echo_("rot={_}",[rot]);

	/*  
	KEEP THIS SECTION for debugging style=block
	//============================
	pz = [0,0,L];
	pm = rmx( rot[0] )* pz;
	Line0( [O, pz]);
	Line0( [O, pm] );
	
	pn = rmy( rot[1] ) * pm + p;
	Line0( [O+p, pn], r=0.2, color="blue", transp=0.3);
	MarkPts( [pz,pm,pn], [["transp",0.3],["r",0.3]] );
	
	//============================
	*/


	// The shift is a translation [x,y,z] determines how (or, where 
	// on the line/block) the line/block attaches itself to pq. It
	// can be any 3-pointer. If style=block, one of the following 
	// preset values also works:
	//
	//    corner			mid1			mid2			center 
	//    q-----+      +-----+      +-----+      +-----+                      
	//   / \     \    p \     \    / q     \    / \     \     
	//  p   +-----+  +   +-----+  +   +-----+  + p +-----+
	//   \ /     /    \ q     /    p /     /    \ /     / 
 	//    +-----+      +-----+      +-----+      +-----+  

	//    corner2
	//    +-----+   
	//   / \     \  
	//  +   q-----+ 
	//   \ /     /  
 	//    p-----+   
	//

	shift_ = ops("shift");
	shift =  isarr(shift_)? shift_  // if shift_ is something like [2,-1,1]
	:hash(                          // if shift_ is one of the following  
		[ "corner",  [0,0,0] 
		, "corner2", [ -w, 0, 0] 
		, "mid1",    [ -w/2, 0 ,0] 
		, "mid2",    [ 0, -depth/2,0] 
		, "center" , [ -w/2, -depth/2, 0 ] 
		], shift_
	);
    		

	// head/tail settings:
	//
	// Each has a "len" for the length and a "r" for the radius. 
	// Also a r0 if needed depending on head/tail style (for
	// example, cone). Possible head/tail styles: 
	// 	
	// | sphere 
	// | cone : this is in fact a cylinder. In this case, the 
	// |        r and r0 values are assigned to the r1, r2 
	// |        arguments of the cylinder. It's default setting
	// |		   is to make an arrow.

	head0= updates( ops, 
			[ "style",false // false|cone|sphere
			, "len",r*6
			, "r",r*3
			, "r0",0
			], isarr( headops )?headops:[] );

	tail0= updates( ops,
			[ "style",false
			, "len",r*6
			, "r",r*3
			, "r0",0
			], isarr( tailops )?tailops:[] );

	// We need to chk if head/tail should be reversed
	head = switched?tail0:head0;
	tail = switched?head0:tail0;	

	function head(k)= hash(head,k);
	function tail(k)= hash(tail,k);
	
	hstyle = head("style");
	tstyle = tail("style");

	hl 	   = hstyle?head("len"):0;
	tl 	   = tstyle?tail("len"):0;


	// Rotation of head/tail around the line. 
	// If not defined, follow the line rotation
	//hrot   = hasht(head, "rotate", ops("rotate"));
	//trot   = hasht(tail, "rotate", ops("rotate"));	
	hrot   = hash(head, "rotate", ops("rotate"));
	trot   = hash(tail, "rotate", ops("rotate"));
	
	// headcut/tailcut: the length that is required to cut away
	// from the line to accomodate the head/tail. This determines
	// where exactly the head/tail is gonna be placed. For example, 
	// a cone (or arrow) starts from the exact p or q location
	// and extends its length inward toward the center of line.
	// A sphere however is placed at exactly the q or q, so half
	// of it will be extended outside of the p-q line. 
	headcut = hash( [ "cone", hl-0.0001 
					,"sphere", 0
					,false, 0
					], hstyle );
	tailcut = hash( [ "cone", tl-0.0001 
					,"sphere", 0
					,false, 0
					], tstyle );	


	// LL is the actual length that is shorter or equal to L
	// depending on the styles of head and tail. 
	//LL = L- headcut - tailcut;
	LL = L- headcut - tailcut + extension0 + extension1;

	
	// The point to start/end the line taking into consideration 
	// of headcut/tailcut. onlinePt is a function returning the
	// xyz of a pt lies along a line specified by pq.
	ph= head("style")? onlinePt([p,q], ratio = (L-headcut)/L):q; 
	pt= tail("style")? onlinePt([p,q], ratio = tailcut/L):p; 
	

	/*	
	------------ Calc the angle to "rotate to touch pt" in style_block
 
	Let: pqr the 3-pointer on the plane of block surface
		 pt : the touch pt

	        Pt
    		    /|
    		d1 /	 | 			
    		  /  | d2
    		 /a  |
		p----+------q-----r

	If we have r (a 3rd pt on block surface other than pq), we can calc:

		d1 = norm( pt-pq[0])
		d2 = distPtPl( pt, pqr )
	
	BUT, how do we find that 3rd pt r ?

	We know that the block is first placed on yz-plane when created.
	So any pt on yz-plane, like [0,1,1] serves the purpose. 

	We know that the transformation of style-block is:

		translate( tail("style")?pt:p )	 
		rotate(rot)
		rotate( [0, 0, ops("rotate")])
		translate( shift )
		cube( ... )

	By applying "this process" to [0,1,1] (skipping the last 3 lines),
	we should find the 3rd r pt we want. 

	So, how to come up with the forumla for "this process" ?

	We should already have all tools for this (see the following), 
	but for some reason it hasn't worked out yet. Has to come back.

	// First a unit pt on yz surface:
	puz = [0,1,1];

	// This pt is shifted following the shift of the block:
	puz_shift = puz + shift ;

	// It then rotates about z-axis following the block's ops("rotate"):
	puz_opsrot = rmz( ops("rotate"))* puz_shift ;

	// Then the real rotation, first about x, then y:
	puz_rx = rmx( rot.x )*puz_opsrot;
	puz_ry = rmy(rot.y)*puz_rx;

	// It then needs to translocate with the block:
	p3rd = puz_ry +(tail("style")?pt:p);	

	This p3rd is on the surface of the block.

	echo("ops[rotate]", ops("rotate"));
	echo("shift ", shift);
	echo("puz_opsrot ", puz_opsrot);
	echo("puz_rx ", puz_rx);
	echo("puz_ry ", puz_ry);
	echo("p3rd ", p3rd );
	
	//MarkPts([ puz, puz_opsrot, puz_rx, puz_ry, p3rd]
	//	,[["r",0.2],["transp",0.4]] );

	MarkPts([puz, p3rd],[["r",0.2],["transp",0.4]]);

	
	touch = ops("touch");
	//echo("touch ", touch);
	p3rd= rmy(rot.y)*
			rmx(rot.x)*
				rmz( ops("rotate"))* ([0,1,1]+shift)
			 + (tail("style")?pt:p);
	p3rd= rmy(rot.y)*
			rmx(rot.x)*
				rmz( ops("rotate"))* ([0,1,1]+shift)
			 + (tail("style")?pt:p);
	//MarkPts( [ p3rd ], [["r",0.2],["colors",["blue"]],["transp",0.4]] );
	d1 = norm( touch-p);
	d2 = distPtPl( touch, [p,q,p3rd] );
	rotang = -asin(d2/d1);
	//MarkPts([p3rd],[["r",0.2],["transp",0.4]]);
	echo("d1 = ", d1);
	echo("d2 = ", d2 );
	echo("rotang ", rotang);
	
*/
	touch = ops("touch");
	prerot =touch? rotang: ops("rotate");
	//echo("prerot ", prerot);
			

	//echo("Before drawing line, rot = ", rot);

	// ------------------------------------           The line itself :	
	if(style== "dash"){

		color(ops("color"), ops("transp"))
		DashLine([pt,ph], ops);

	} else if (style=="block") {    // block

		translate( tail("style")?pt:p )	 
		rotate(rot)
		rotate( [0,0, prerot]) //ops("rotate")])
		color(ops("color"), ops("transp"))
		translate( shift+ [0,0,switched?-extension1:-extension0] )
    		cube( [ops("width"), ops("depth"), LL ]
			, $fn=ops("fn"));

	} else {                       // solid

		translate( tail("style")?pt:p) 
		rotate(rot)
		rotate( [0,0, ops("rotate")])
    		color(ops("color"), ops("transp"))
		translate( shift+ [0,0,switched?-extension1:-extension0] )
    		cylinder( h=LL, r=r , $fn=ops("fn"));
	} 
 


	// ---------------------------------- The tail :
	//		
	if(tstyle=="cone")
	{
		translate( p )  
		rotate( rot)
		rotate( [0,0,trot] )
		color(tail("color"), tail("transp"))
		translate( shift )
    		cylinder( h=tl, r1=tail("r0"), r2=tail("r"), $fn=tail("fn"));

	} else 

	if (tstyle=="sphere")
	{
		translate( switched?pt:p ) 
		rotate( rot)
		rotate( [0,0,trot] )
		color(tail("color"), tail("transp"), $fn=tail("fn"))
		translate( shift )
    		sphere( r=tail("r"));
	} 

	// ----------------------------------- The head :
	// 
	if(hstyle=="cone")
	{
		translate( ph )  
		rotate( rot)
		rotate( [0,0,hrot] )
		color(head("color"), head("transp"))
		translate( shift )
    		cylinder( h=hl, r2=head("r0"), r1=head("r"), $fn=head("fn"));

	} else

	if (hstyle=="sphere")
	{
		translate( q ) 
		rotate( rot)
		rotate( [0,0,hrot] )
		color( head("color"), head("transp"), $fn=head("fn"))
		translate( shift )
    		sphere( r=head("r"));
	} 
	
} // Line2 

module Line2_test( ops ){doctest (Line, ops=ops); }


module Line2_demo_1_default()
{
	echom("Line2_demo_1_default");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true]);
}

module Line2_demo_1a_extension()
{
	ops=["transp",0.3,  "markpts",true, "transp",0.4] ;

	echom("Line2_demo_1a_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, update(ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line2(pq2, update(ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	Line2(pq3, update(ops, ["color","blue","extension0", 1,"extension1", 1]));

	pq4= randPts(2);
	//MarkPts(pq3);
	Line2(pq4, update(ops, ["color","purple","extension0", -0.5,"extension1", -0.5]));

}

module Line2_demo_2_sizeColor()
{
	echom("Line2_demo_2_sizeColor");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, [ "r", 0.5
			 , "color","green"
			 , "transp",0.3
			 , "fn", 6
			 , "markpts",true
			 ]);
}

module Line2_demo_3a_head_cone()
{
	echom("Line2_demo_3a_head_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true, "head"
			  ,["style","cone"]
			 ]);
}

module Line2_demo_3b_head_cone_settings()
{
	echom("Line2_demo_3b_head_cone_settings");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true,"head"
			  , [ "style","cone"
				,"color","blue"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5
			    ]
			 ]);
}

module Line2_demo_4a_tail_cone()
{
	echom("Line2_demo_4a_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				, "color","red"
				, "r", 0.4
				, "len", 1
			 	, "transp", 0.6
				, "fn", 5		    
				]
			 ]);
}

module Line2_demo_4b_tail_cone()
{
	echom("Line2_demo_4b_tail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true,"tail"
			  , [ "style","cone"
				,"r", 0.2
				, "r0",0.6
				]
			 ]);
}

module Line2_demo_5a_headtail_cone()
{
	echom("Line2_demo_5a_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq,["markpts",true
			  ,"head"
			  , [ "style","cone"
			    ]
			  
			, "tail"
			  , [ "style","cone"
			 	]
			  
			 ]);
}

module Line2_demo_5b_headtail_cone()
{
	echom("Line2_demo_5b_headtail_cone");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"head"
			  , [ "style","cone"
				, "color","blue"
				, "r", 0.4
				, "len",1
			 	, "transp", 0.6
				, "fn", 5
			    ]
			, "tail"
			  , [ "style","cone"
				, "r", 0.2
				, "r0",0.6
			 	]
			 ]);
}

module Line2_demo_6a_headtail_sphere()
{
	echom("Line2_demo_6a_headtail_sphere");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"head"
			  , [ "style","sphere"
				, "color", "yellow"
				, "r",.5
				, "transp", 0.4
			    ]
			  
			, "tail"
			  , [ "style","sphere"
				,"color", "red"
				, "r", .4
				, "transp", 0.4
				]
			  
			 ]);
}


module Line2_demo_7_headtail_fn()
{
	echom("Line2_demo_7_headtail_fn");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				]
			 ]);
}


module Line2_demo_8_rotate()
{
	echom("Line2_demo_8_rotate");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "blue"
				, "r",.5
				, "len", 1
				, "transp", 0.4
				, "fn",6
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "r", .4
				, "transp", 0.6
				, "rotate", 45  //<====
				]
			 ]);
}


module Line2_demo_9a_block()
{
	echom("Line2_demo_9a_block");
	pq= randPts(2);
	//pq = [ [2,1,3], [4,3,2]];
	//MarkPts(pq);
	Line2(pq, ["markpts",true
			  ,"style","block"]);
}


module Line2_demo_9a2_block_extension()
{
	ops=["transp",0.3,  "markpts",true,"style","block"] ;

	echom("Line2_demo_9a2_block_extension");
	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, update( ops, ["color","red","extension0", 1]));

	pq2= randPts(2);
	//MarkPts(pq2);
	Line2(pq2, update( ops, ["color","lightgreen","extension1", 1]));

	pq3= randPts(2);
	//MarkPts(pq3);
	//Line2(pq3, update( ops, [["color","blue"],["extension0", 1],["extension1", 1]]));

	pq4= randPts(2);
	//MarkPts(pq3);
	//Line2(pq4, update( ops, [["color","purple"],["extension0", -0.5],["extension1", -0.5]]));

}

module Line2_demo_9b_block()
{
	echom("Line2_demo_9b_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 //, "markpts",true
		];

	Line2([O, [0,0,z+1]], [ "style","block"
					   //, ["markpts",true]
			  		   , "width",0.5
					   , "depth",4
					  ]);

	Line2([[0,0,w], [x,0,w]], update(ops, ["color","red"]));
	Line2([[x-w,0,0], [x-w,0,z]], update(ops, ["color","green"]) );

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true]
		 ) );
	
}


module Line2_demo_9c_block()
{
	echom("Line2_demo_9c_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","corner2"] // <====
					  ) );


}

module Line2_demo_9d_block()
{
	echom("Line2_demo_9d_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //["markpts",true]
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [ //"markpts",true
			  			"style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid1"] // <====
					  ) );

}

module Line2_demo_9e_block()
{
	echom("Line2_demo_9e_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [//"markpts",true
			  			 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","mid2"] // <====
					  ) );

}


module Line2_demo_9f_block()
{
	echom("Line2_demo_9f_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ //"markpts",true
		  "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], [//"markpts",true
		 				 "style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );
	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift","center"] // <====
					  ) );

}

module Line2_demo_9g_block()
{
	echom("Line2_demo_9g_block");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	Line2([O, [0,0,z+1]], ["style","block"
					   , "width",0.5
					   , "depth",4
					  ]);
	Line2([[0,0,w], [x,0,w]], ops);
	Line2([[x-w,0,0], [x-w,0,z]], ops);

	Line2( [ [x, 0, z], [0, 0, z+1]] );
	//MarkPts( [ [x, 0, z], [0, 0, z+1]] );

	Line2( [ [x, 0, z], [0, 0, z+1]]
		, update(ops, ["color","blue", "rotate",360*$t, "markpts",true
						,"shift",[2*w, 0,w] ]// <====
					  ) );

}

module Line2_demo_10_shift()
{
	echom("Line2_demo_10_shift");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "style","block"
		 , "width",0.5
		 , "depth",4
		 , "transp",0.5
		 ];

	pq= randPts(2);
	//MarkPts(pq);
	Line2(pq, ["markpts",true
		 	 , "r",0.2, "fn", 4
			 ,"head"
			  , [ "style","cone"
				, "color", "green"
				, "transp",0.5
			    ]
			, "tail"
			  , [ "style","sphere"
				, "color", "red"
				, "transp",0.5
			    ]
			,"shift", "corner2"
			 ]);
}

module Line2_demo2()
{
	pts = [ [-1,0,-1], [2,3,4]  ];
	MarkPts( pts );
	pts = randPts(2);
	//Line2( pts,[["r",0.5]] );
	//MarkPts(pts, [["transp",0.3],["r",0.25]] );
	Line2( pts //, [["style","block"]
		, ["markpts",true
		 , "style","block", "transp",0.5
		, "rotate", $t*60
		, "shift", "corner"
		, "head", ["style","cone"]
		, "tail", ["style","cone"]
			]
			);
}

module Line2_demo_11_block_touch()
{
	echom("Line2_demo_11_block_touch");
	x=5; 	y=6; 	z=4; 	w=0.5;

	ops= [ "markpts",true
		 , "style","block"
		 , "width",w
		 , "depth",y
		 , "transp",0.5
		 ];

	p_touch = [x/3,y/3,z/3];
	MarkPts([ p_touch ] , ["r",0.3,"transp",0.3]);

	//Line2([O, [0,0,z+1]], [ ["style","block"]
	//				   , ["width",w]
	//				   , ["depth",y]
	//				  ]);
	//Line2([[0,0,w], [x,0,w]], ops);
	//Line2([[x-w,0,0], [x-w,0,z]], ops);

	pts = [[x, 0, z], [0, 0, z+1]];
	//pts=randPts(2);
	Line2( pts );
	//MarkPts( pts );
	
	Line2( pts
		, update(ops, ["color","blue"//, "shift","corner2"
					  ,"touch", p_touch]  // <====
					  ) );
}

module Line2_demo_12_verify_2D()
{
	//pq= [ [ 1.5, 1, 0 ], [ 1, 3, 0 ] ];
	pq_= randPts(2);
	echo(pq_);
	
	pq= [ [ pq_[0].x, pq_[0].y,0]
		, [ pq_[1].x, pq_[1].y,0] ];
	echo(pq);
	MarkPts( pq, ["grid",true] );
	Line2( pq, ["markpts", true] );

	/*
	pq2= [ [ 1.5, 0,1 ], [ 1, 0,3 ] ];
	MarkPts( pq2, ["grid",true] );
	Line2( pq2, ["markpts", true] );

	pq3= [ [ 0,1.5, 1 ], [ 0, 1, 3 ] ];
	MarkPts( pq3, ["grid",true] );
	Line2( pq3, ["markpts", true] );
	*/
}

module Line2_demo()
{
	//Line2_demo_1_default();
	//Line2_demo_1a_extension(); // 2014.5.18
	//Line2_demo_2_sizeColor();
	//Line2_demo_3a_head_cone();
	//Line2_demo_3b_head_cone_settings();
	//Line2_demo_4a_tail_cone();
	//Line2_demo_4b_tail_cone();
	//Line2_demo_5a_headtail_cone();
	//Line2_demo_5b_headtail_cone();
	//Line2_demo_6a_headtail_sphere();
	//Line2_demo_7_headtail_fn();
	//Line2_demo_8_rotate();
	Line2_demo_9a_block();
	//Line2_demo_9a2_block_extension();
	//Line2_demo_9b_block();
	//Line2_demo_9c_block();
	//Line2_demo_9d_block();
	//Line2_demo_9e_block();
	//Line2_demo_9f_block();
	//Line2_demo_9g_block();
	//Line2_demo_10_shift();
	//Line2_demo_11_block_touch();
	//Line2_demo_12_verify_2D();

	

}
//Line2_test( ["mode",22] );
//Line2_demo();

//========================================================
LineByHull=["LineByHull", "pq,r=0.05,fn=15", "n/a", "3D,Shape",
"
 Draw a line as thick as 2*r between the two points in pq using
;; hull(). This approach doesn't need to calc rotation/translation,
;; so the faithfulness of outcome is guaranteed. But is much slower
;; so not good for routine use.
"];

module LineByHull( pq,r=0.05, fn=15 )
{
	hull(){
		translate(pq[0])
		sphere(r=r, $fn=fn);
		translate(pq[1])
		sphere(r=r, $fn=fn);
	}
}

module LineByHull_test( ops ){ doctest(LineByHull, ops=ops); }

module LineByHull_demo()
{
	LineByHull( randPts(2) );
}
//LineByHull_demo();
//doc(LineByHull);


//========================================================

// WHY DO I NEED THIS FUNCTION ?

/*
linecoef=[["linecoef", "p,[q]", "num", "Math,Line"]	
		 , [ "For a 2D line y=ax+b that passes through both"
			  ,	"p and q, return b. Can be either linecoef(p,q)"
			  ,	" or linecoef([p,q])" 
		]];

function linecoef(p,q=undef) = q!=undef?
						 (p[1]- slope(p,q)* p[0])
						 :(p[1][1]- slope(p)* p[1][0]);

module linecoef_test( ops )
{
	pq = [[0,3],[4,5]];

	doctest( linecoef, [
				[ [pq], linecoef(pq), 3 ]
			],ops
		);
}
linecoef_test( ["mode",22]);

*/



//========================================================
linecoefs=[[],
"
 Given a 2-pointer pq, return an array [a,b,c] where a,b,c fits\\
 
"];



//========================================================
lineCrossPt=["lineCrossPt", "P,Q,M,N","point", "Geometry",
"
 Given 4 points (P,Q,M,N), return the point (R below) where extended
;; lines of PQ and MN intesects.
;;
;;     M        
;;      :       Q
;;       :    -'
;;        R-'
;;      -' :
;;    P'    N
;;
;;   M
;;    : 
;;     :
;;      N      Q 
;;           -'
;;        R-'
;;      -'
;;    P'
"];

function lineCrossPt( P,Q,M,N )=
(
// Ref: Paul Burke tutorial 
//   http://paulbourke.net/geometry/pointlineplane/
// and his code in c :
//   http://paulbourke.net/geometry/pointlineplane/lineline.c
                   
//	P + (Q-P) * ( ((M-P)*(M-N))* ((M-N)*(P-Q)) - ((M-P)*(P-Q))*((M-N)*(M-N)))
// 	 		 / ( ((P-Q)*(P-Q))* ((M-N)*(M-N)) - ((M-N)*(P-Q))*((M-N)*(P-Q)))
                   
	P + (Q-P) * ( dot(M-P,M-N)* dot(M-N,P-Q) - dot(M-P,P-Q)* dot(M-N,M-N) )
 	 		 / ( dot(P-Q,P-Q)* dot(M-N,M-N) - dot(M-N,P-Q)* dot(M-N,P-Q) )
);

module lineCrossPt_test(ops=["mode",13]){ doctest( lineCrossPt, ops=ops ); }

module lineCrossPt_demo_1()
{
	echom("lineCrossPt_demo");
	pqm = randPts(3);
	P = pqm[0];
	Q = pqm[1];
	R = onlinePt( [P,Q], ratio= rands(0.2,1,1)[0] ); 
	M = pqm[2];
	N = onlinePt( [M,R], ratio = rands(1.5,2,1)[0]);
	
	//echo("[P,Q,M,N] = ", [P,Q,M,N]);

	MarkPts([P,Q,M,N,R]);
	Line0( [P,Q], ["r",0.01, "color","red"] );
	Line0( [M,N], ["r",0.01, "color","red"] );

	A = lineCrossPt(P,Q,M,N);
	B = crossPt( [P,Q], [M,N] );
	//echo("R: ", R );
	//echo("A: ", A );
	//echo("B: ", B );
	MarkPts( [A], ["r",0.2, "transp",0.4] );
	MarkPts( [B], ["r",0.3, "colors",["green"], "transp",0.2] );
	LabelPts( [P,Q,M,N,R,A], "PQMNRA" );
}


module lineCrossPt_demo_2_nocontact()
{
	echom("lineCrossPt_demo");
	pqm = randPts(3);
	P = pqm[0];
	Q = pqm[1];
	R = onlinePt( [P,Q], ratio= rands(0.2,1,1)[0] ); 
	M = pqm[2];
	N = onlinePt( [M,R], ratio = rands(0,0.8,1)[0]);
	
	//echo("[P,Q,M,N] = ", [P,Q,M,N]);

	MarkPts([P,Q,M,N,R]);
	Line0( [P,Q], ["r",0.02, "color","green"] );
	Line0( [M,N], ["r",0.02, "color","green"] );

	A = lineCrossPt(P,Q,M,N);
	B = crossPt( [P,Q], [M,N] );
	//echo("R: ", R );
	//echo("A: ", A );
	//echo("B: ", B );
	MarkPts( [A], ["r",0.2, "transp",0.4] );
	MarkPts( [B], ["r",0.3, "colors",["green"], "transp",0.2] );
	LabelPts( [P,Q,M,N,R,A], "PQMNRA" );
}
module lineCrossPt_demo()
{
	lineCrossPt_demo_1();
	lineCrossPt_demo_2_nocontact();
}
//lineCrossPt_demo();


//========================================================
longside=["longside","a,b","number","Math,Geometry,Triangle",
 " Given two numbers (a,b) as the lengths of two short sides of a 
;; right triangle, return the long side c ( c^2= a^2 + b^2 )."
,"shortside"
];
function longside(a,b)= sqrt(pow(a,2)+pow(b,2));
module longside_test( ops=["mode",13] )
{ doctest( longside, [
	[ "3,4", longside(3,4), 5 ]
	], ops
	);
}


lpCrossPt=["lpCrossPt", "pq,tuv", "point", "Geometry",
 " Given a 2-pointer for a line (pq) and a 3-pointer for a plane (tuv),
;; return the point of intercept between pq and tuv. 
;;
    m=dist(pq)

    P   m
    |'-._	  
    |    'Q._  n
    |dp   |  '-._
    |     |dq    '-.
   -+-----+----------X-

    (m+n)/m = dp/(dp-dq) 

    onlinePt( pq, ratio = dp/(dp-dq) )

"];

function lpCrossPt(pq, tuv)=
 let( P=pq[0]
	, Q=pq[1]
	, L= dist(pq)
	, dp = distPtPl(P, tuv)
	, dq = distPtPl(Q, tuv)
	, sameside = sign(dp)==sign(dq)
	, ratio = sameside ?  dp/(dp-dq)
					 :abs(dp)/(abs(dp)+abs(dq))
	)
(
	onlinePt( pq, ratio = ratio)
);


module lpCrossPt_demo()
{
	pq = randPts(2);
	tuv = randPts(3);
	Rod( pq, ["label",true] );
	Plane( squarePts(tuv), ["mode",4] );

	X = lpCrossPt( pq, tuv );
	MarkPts( [X], ["labels","X"] );

}

//lpCrossPt_demo();

////========================================================
//map=["map","arr, fname,args", "array", "Array", 
// " Given an array (arr), a function name and an array as args (args), run 
//;; that function to each of the element and return them.
//;;
//;; Note: the feature of this function is very limited.
//"];
//function map( arr, fname, args=[], _I_=0 )=
//(
//	fname=="concat"
//	? (
//		_I_<len
//
//	  )
//	:undef
//	
//
//);

//========================================================
Mark90=["Mark90", "pqr,ops", "n/a", "Grid, Math, Geometry, 3D, Shape",
" Given a 3-pointer pqr and an ops, make a L-shape at the middle
;; angle if it is 90-degree. The default ops, other than usual
;; r, color and transp, has ''len''= 0.4
;;
;; ops: [ ''len'', 0.4
;;      , ''color'', false
;;      , ''transp'', 0.5
;;      , ''r'', 0.01
;;      ]
"];
	
module Mark90(pqr, ops)
{
	ops=update(   
		[ "len", 0.4
		, "color",false
		, "transp", 0.5
		, "r", 0.01
		], ops );
	function ops(k) = hash(ops,k);

	p = pqr[0];
	q = pqr[1];
	r = pqr[2];
	m = min( 0.7*norm(p-q), 0.7*norm(r-q), ops("len"));
	
	pq1 = onlinePt( [q,p], len=m); //ratio= ops("len")/ norm(p-q) );
	pq3 = onlinePt( [q,r], len=m); //ratio= ops("len")/ norm(r-q) );
 	sqpts = squarePts( [pq1, q, pq3] );
		
	if (is90( [p,q], [q,r] ) ){
		Line0( [sqpts[2],sqpts[3]],ops );
		Line0( [sqpts[0],sqpts[3]],ops );
	}
}

module Mark90_demo()
{
	pqr = randPts(3);
	MarkPts( pqr );
	sqs = squarePts( pqr );
	Plane( sqs , ["mode",4]);
	Mark90(sqs, ["len", 0.4, "color", "black"] );
}

module Mark90_test(ops){ doctest( Mark90, ops=ops); }
//Mark90_demo();
//Mark90_test();
//doc(Mark90);


//========================================================
MarkPts=["MarkPts", "pts,ops", "n/a", "3D, Shape, Point",
"
 Given a series of points (pts) and ops, make a sphere at each pt.
;; Default ops:
;;
;;   [ ''r'',0.1
;;   , ''colors'',[''red'',''green'',''blue'',''purple'',''khaki'']
;;   , ''transp'',1
;;   , ''grid'',false
;;   , ''echo'',false
;;   ]
;;
;; The colors follow what's in colors. If len(pts)>len(colors),
;; colors recycles from the the 1st one and the transparancy and
;; radius reduced by ratio accordingly by a ratio each cycle. If
;; grid is true, draw PtGrid for each point; if echo is true,
;; echo each point.
"];

module MarkPts(pts, ops) 
{
	ops= update( ["r",0.1,"transp",1
				 ,"grid",false
				 ,"echopts",false
				 ,"colors", ["red","green","blue","purple"]
				 ,"labels",false
				 ,"samesize", false
				 ,"sametransp", false
				 ], ops);
	function ops(k)= hash(ops,k);
	colors = ops("colors");
	transp = ops("transp");
	samesize = ops("samesize");
	sametransp=ops("sametransp");
	labels = ops("labels");
     cL = len(colors);
	//echo("<br/>ops = ", ops);

	// color cycle. The more ccycle, the smaller the sphere
	// and the more transparent they are.  
	function ccycle(i) = floor(i/cL); 

	for (i=[0:len(pts)-1]){
		translate(pts[i])
		color( ops("colors")[i-cL*ccycle(i)]   // cycling thru colors
			, transp* ( sametransp?1:(1-0.3*ccycle(i))) // transp level
			) 
		sphere(r=ops("r")*(samesize?1:(1-0.15*ccycle(i)))
			  ); // radius

		if(ops("grid")){ PtGrid(pts[i], ops=["mode", ops("grid")]); }
		if(ops("echopts")){ echo_("MarkPts #{_}={_}",[i, pts[i]]);}
	}
	if( labels ){

		LabelPts( pts, labels=labels==true?"":labels
				, ops=["shift", ops("r")*1.3 ]
					 
					);
	}
}

module MarkPts_test( ops ){ doctest(MarkPts,ops=ops);}
module MarkPts_demo_1()
{
	ops= ["r",0.1,  "echopts",true, "grid",true] ;
	MarkPts( randPts(3), ops=ops);
	MarkPts( randPts(12));
}

module MarkPts_demo_2_more_points()
{
	MarkPts( randPts(20));
}

module MarkPts_demo_2_more_points_2()
{
	MarkPts( randPts(20), ["samesize",true]);
}

module MarkPts_demo_2_more_points_3()
{
	MarkPts( randPts(30), ["samesize",true, "colors",["red"]]);
}

module MarkPts_demo_2_more_points_4()
{
	MarkPts( randPts(30), ["samesize",true, "sametransp",true, "colors",["red"]]);
}

module MarkPts_demo_3_label()
{
	MarkPts( randPts(3), ["r",0.15,  "labels",true, "colors",["red"],"samesize",true, "sametransp",true]); // labels = P1, P2 ... 

	MarkPts( randPts(3), ["r",0.15,  "labels","ABC", "colors",["green"],"samesize",true, "sametransp",true]); // labels = P1, P2 ... 

	pts = randPts(10);
	Chain( pts, ["r",0.02,["closed",false]]);
	MarkPts( pts, ["labels",increase(range(pts)), "colors",["blue"],"samesize",true, "sametransp",true]); // labels = P1, P2 ... 

}

module MarkPts_demo_4_grid()
{
	function getops(grid, color, label=false)=
	  ["grid", grid, "colors",[color],"samesize",true, "sametransp",true
	   ,"labels", [label] ];

	MarkPts(randPts(1), getops(true, "red", "grid:true") );
	MarkPts(randPts(1), getops(1, "green", "grid:1") );
	MarkPts(randPts(1), getops(2, "blue", "grid:2") );
	MarkPts(randPts(1), getops(3, "purple", "grid:3") );

}
module MarkPts_demo()
{
	//MarkPts_demo_1();
	//MarkPts_demo_2_more_points();
	//MarkPts_demo_2_more_points_2();
	//MarkPts_demo_2_more_points_3();
	//MarkPts_demo_2_more_points_4();
	MarkPts_demo_3_label();
	//MarkPts_demo_4_grid();

}
//MarkPts_test( ["mode",22]);
//MarkPts_demo();
//doc(MarkPts);




//========================================================
//arrmax=["arrmax","a","number","Math, Array",
// " Given an array of numbers, return the max number. The builtin
//;; max() takes arguments like max(a,b...). This function makes
//;; this possible: arrmax( [a,b..] ).
//"];
//
//function arrmax(arr)=
//(
//	max(arr[0],arrmax(shrink(arr)))
//);
//
//module arrmax_test( ops=["mode",13] )
//{
//	doctest
//	(arrmax,
//		[
//			[ "[2,3,1,4]", arrmax([2,3,1,4]), 4]
//			,[ "[2,3,1]", arrmax([2,3,1]), 3]
//		],ops
//	);
//}
//maxa_test( ["mode",22] );
//doc(maxa);




//========================================================
parsedoc=["parsedoc","string","string", "String, Doc",
 " Given a string for documentation, return formatted string.
"];

function parsedoc( s
			, prefix ="|"
			, indent =""
			, replace=[]
			, linenum
			)=
 let( s   = split(s, ";;")	)
(
	
  join(
	[for( i=range(s) )
		 str( indent
			, prefix
			, isnum(linenum)?str((linenum+i),". "):"" 
			, replace
			  (
				replace
				( 
				  replace( s[i], "''", "&quot;" )
				, "<", "&lt;"
				)
			  , ">", "&gt;"
			  )
			)
	],"<br/>")									
);

module parsedoc_test( ops=["mode",13] )
{
	doctest( parsedoc, ops=ops );
}

//========================================================
_mdoc=["_mdoc", "mname,s", "n/a", "Doc",
 " Given a module name (mname) and a doc string (s), parse s (using 
;; parsedoc()) and echo to the console.
"];

module _mdoc(mname, s){ 
	echo(_pre(_code(
			str("<br/><b>"
			, mname
			, "()</b><br/><br/>"
			, parsedoc(s)
			)
			,s="color:blue"
		)));
}				  

module _mdoc_test( ops=["mode",13] )
{
	doctest( _mdoc, ops=ops );
}
//	function _doc_dt_getDocLinesString( lines , tags, _i_=0 )=
//	(
//		_i_>len(lines)-1?""
//		:str(
//			 _i_==0? prefix :""
//			, dt_replace(lines[_i_], "''", "&quot;" )  // #2
//				, "<br/>"
//				, prefix 
//			,  _doc_dt_getDocLinesString( lines, tags, _i_=_i_+1 )
//			, _i_==len(lines)-1? str("<br/>", prefix, " <u>Tags:</u> ",tags):""
//		)	
//	
//	); 


//========================================================
mergeA=["mergeA", "a_list_of_arrays"
    ,"array", "Array"
, "merge the arrays into one. This is useful to merge
;; the result of a list comprehension:
;;
;; mergeA( [for (i...) ...] )
"];
function mergeA(arrays,_i_=0)=
//[ for (a = l) for (b = a) b ]; 
(    _i_<len(arrays)
    ? concat( arrays[_i_], mergeA(arrays, _i_+1) )
    :[]
);

module mergeA_test(ops=[])
{
  As = [[0, 2, 1], [0, 3, 2], [0, 4, 3]];
  As2= [[[0,1,2], [3,4,5]], [[6,7,8], [9,10,11]]]; 
  
  doctest(mergeA,
    [
      ["As",  mergeA(As),  [0,2,1,0,3,2,0,4,3]]
    , ["As2", mergeA(As2), [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]]]
    ]
  , ops, ["As",As,"As2",As2]
  );
}

//========================================================
midPt=[ "midPt", "PQ", "pt", "Math, Geometry, Point" ,
"
 Given a 2-pointer PQ, return the pt that's in the middle of P and Q
"];

function midPt(PQ)= (PQ[1]-PQ[0])/2 + PQ[0];

module midPt_test(ops)
{ doctest( midPt, 
	[ 
	[  "[ [2,0,4],[1,0,2] ]", midPt([ [2,0,4],[1,0,2]  ]), [1.5,0,3]   ]
	], ops
 );
}
//midPt_test( ["mode",22] );
//doc(midPt);
 


//
////========================================================
//mina=[["mina","a","number","Math, Array"],
//"
// Given an array of numbers, return the min number. The builtin\\
// min() takes arguments like min(a,b...). This function makes\\
// this possible: mina( [a,b..] ).\\
//"];
//
//function mina(arr)=
//(
//	min(arr[0],mina(shrink(arr)))s
//);
//
//module mina_test( ops )
//{
//	doctest
//	(
//		mina
//		,[
//			[ [2,3,1,4], mina([2,3,1,4]), 1]
//			,[ [2,3,1], mina([2,3,1]), 1]
//		], ops
//	);
//}
//mina_test( ["mode",22] );
//doc(mina);



//========================================================
minPt=[ "minPts", "pts", "array", "3D, Point, Math",
"
 Given an array of points (pts), return a pt that
;; has the min x then min y then min z.
"];
 
function minPts(pts)=
(
	minzPts( minyPts( minxPts( pts ) ) )[0]
);

module minPt_test( ops )
{
	pts1= [[2,1,3],[0,2,1] ];
	pts2= [[2,1,3],[0,3,1],[0,2,0], [0,2,1]];
	
	doctest
	(	minPt
		,[
			 ["pts1", minPts( pts1 ), [0,2,1] ]
			,["pts2", minPts( pts2 ), [0,2,0] ]
		 ], ops, ["pts1", pts1, "pts2", pts2]
	);
}
//minPt_test();
//doc(minPt);





//========================================================
minxPts=[ "minxPts", "pts", "array", "Math"	,
"
 Return an array containing the pt(s) that has the min x.
"];
 
function minxPts(pts, _i_=0)=
(
    /* minx = min(getcol(pts,0)); */
	_i_<len(pts)?
	 concat( pts[_i_][0]==min(getcol(pts,0))? [pts[_i_]]:[]
			, minxPts( pts, _i_+1) 
			)
	 :[]
);

module minxPts_test( ops )
{
	pts1= [[2,1,3],[0,2,1] ];
	pts2= [[2,1,3],[0,3,1],[0,2,0]];
	
	doctest
	( minxPts
		,[
			[ "pts1" , minxPts( pts1 ), [[0,2,1]] ]
			,["pts2", minxPts( pts2 ), [[0,3,1],[0,2,0]] ]
		 ],ops, ["pts1", pts1, "pts2", pts2]
	);
}
//minxPts_test( ["mode",22] );
//doc(minxPts);



//========================================================
minyPts=[ "minyPts", "pts", "array", "Math",
"
 Return an array containing the pt(s) that has the min y.
"];
function minyPts(pts, _i_=0)=
(
    /* minx = min(getcol(pts,0)); */
	_i_<len(pts)?
	 concat( pts[_i_][1]==min(getcol(pts,1))? [pts[_i_]]:[]
			, minyPts( pts, _i_+1) 
			)
	 :[]
);

module minyPts_test( ops )
{
	pts1= [[2,1,3],[0,2,1],[3,3,0]];
	pts2= [[2,1,3],[0,3,1],[3,1,0]];
	
	doctest
	( minyPts
		,[
			 ["pts1", minyPts( pts1 ), [[2,1,3]] ]
			,["pts2", minyPts( pts2 ), [[2,1,3],[3,1,0]] ]
		 ],ops, ["pts1", pts1, "pts2", pts2]
	);
}
//minyPts_test( ["mode",22] );
//doc(minyPts);



//========================================================
minzPts=[ "minzPts", "pts", "array", "Math",
"
 Return an array containing the pt(s) that has the min y.
"];

function minzPts(pts, _i_=0)=
(
    /* minx = min(getcol(pts,0)); */
	_i_<len(pts)?
	 concat( pts[_i_][2]==min(getcol(pts,2))? [pts[_i_]]:[]
			, minzPts( pts, _i_+1) 
			)
	 :[]
);

module minzPts_test( ops )
{
	pts1= [[2,1,3],[0,2,1]];
	pts2= [[2,1,3],[0,3,1],[0,2,1]];
	
	doctest
	( minzPts
		,[
			 ["pts1", minzPts( pts1 ), [[0,2,1]] ]
			,["pts2", minzPts( pts2 ), [[0,3,1],[0,2,1]] ]
		 ], ops, ["pts1", pts1, "pts2", pts2]
	);
}
//minzPts_test( [["mode", 22]] );
//doc(minzPts);


//========================================================
mod=["mod", "a,b", "number", "Number, Math, Core",
" Given two number, a,b, return the remaining part of division a/b.
;;
;; Note: OpenScad already has an operator for this, it is the percentage
;; symbol. This is here because this code file might be parsed by python
;; that reads the percentage symbol as a string formatting site and causes
;; errors.
"];

function mod(a,b) = sign(a)*(abs(a) - floor(abs(a)/abs(b))*abs(b) );

module mod_test( ops=["mode",13] )
{
	doctest( mod,
	[
	 ["5,3", mod(5,3), 2]
	,["-5,3", mod(-5,3), -2 ]
	,["5,-3", mod(5,-3), 2]
	,["7,3", mod(7,3),1]
	,["-7,3", mod(-7,3), -1 ]
	,["3,3", mod(3,3), 0 ]
	,["3.8,1", mod(3.8,1), 0.8 ]
	,["38,1", mod(38,1), 0 ]
	], ops
	);
}
//mod_test();


////========================================================
//neighbors=["neighbors"
//			, "arr, i,left=1,right=1, cycle=false"
//			, "array", "Array",
//"
// Given an array, index i, left and right counts (left, right),
//;; return a range [i-left, i+right+1] of points. It cycles from
//;; the other end if *cycle* is set to true (default=false). Both
//;; left and right's default=1, so neighbor(arr,i) will (if in range)
//;; return an array of 3 (i-1,i,i+1). The value of i could be negative. 
//"];
//
//function neighbors(arr, i, left=1, right=1, cycle=true)=
//let( j=fidx(arr,i),a= fidx(arr, j-left),b=fidx(arr,j+left+1) )
//(  [for( ii= range(min(a,b), max(a,b), cycle=cycle) ) arr[ii] ]
////	// to make sure i is in range: i = (i<0?(len(arr)+i):i)
////	cycle? 
////	(
////	    ( (i<0?(len(arr)+i):i) -left<0)?
////			concat( slice(arr, i-left), slice(arr,0, i+right+1) )
////		 :( (i<0?(len(arr)+i):i)+right>len(arr)-1)? 
////		// [0,1,2,3,4,5,6] 
////		//	i=1, left=2, right=2 => [6,0,1,2,3]
////		// 	i=5, left=2, right=2 => [3,4,5,0,1]
////			concat( slice(arr, i-left), slice(arr,0, i+right+1-len(arr)) )
////		: slice( arr, (i<0?(len(arr)+i):i)-left
////				   , (i<0?(len(arr)+i):i)+right+1 )
////	): slice( arr
////			, max( (i<0?(len(arr)+i):i)-left, 0)
////			, min( (i<0?(len(arr)+i):i)+right+1, len(arr)) )
//);
//
//module neighbors_test( ops=["mode",13] )
//{
//	arr= [10, 11, 12, 13, 14, 15, 16]; //range(10,17);
//		
//	doctest( neighbors,
//	[
//		"// All in range:"
//		,["arr,4", neighbors(arr,4), [13,14,15]]			
//		,["arr,-2", neighbors(arr,-2), [14,15,16]]			
//		,["arr,4,2,1", neighbors(arr,4,2,1), [12,13,14,15]]			
//		,["arr,4,1,2", neighbors(arr,4,1,2), [13,14,15,16]]			
//		,["arr,4,2,2", neighbors(arr,4,2,2), [12,13,14,15,16]]			
//		,"// Left side out of range:"
//		,["arr,0,2,2", neighbors(arr,0,2,2), [10,11,12]]			
//		,["arr,0,2,2,true",neighbors(arr,0,2,2,true), [15,16,10,11,12]]	
//		,str("// Right side out of range, arr[6] = ", arr[6])		
//		,["arr,6,2,2", neighbors(arr,6,2,2), [14,15,16]]			
//		,["arr,6,2,2,true", neighbors(arr,6,2,2,true), [14,15,16,10,11]]	
//		,["arr,-1,2,2", neighbors(arr,-1,2,2), [14,15,16]]			
//		,["arr,-1,2,2,true", neighbors(arr,-1,2,2,true), [14,15,16,10,11]]
//	//	,"// Extreme cases:"
//	//	,["arr,3,5,5,true", neighbors(arr,3,5,5,true), [14,15,16,10,11]]
//			
//	], ops, ["arr",arr]
//	);
//}



module neighbors_demo()
{
	echom("neighbors");
	pts = randPts(6);
	Chain( pts, ["r",0.025, "closed",false] );
	//echo("pts",pts);
	MarkPts( pts, ["labels",true] );

	nbs = neighbors(pts,2);
	Chain( nbs, ["r",0.1, "transp", 0.2, "closed",false, "color","red"] );
	//echo("nbs", nbs);
	
}	
//neighbors_demo();




function neighborsAll(arr, left=1, right=1, cycle=true, _i_=0)=
(
	_i_<len(arr)?
	concat( [ neighbors( arr, _i_, left, right, cycle ) ]
		  , neighborsAll(arr, left, right, cycle, _i_+1)
		  ):[]
);


module neighborsAll_demo_1()
{
	echom("neighborsAll");
	
	pts2 = randPts(6);
	Chain( pts2, ["r",0.025,  "color","red"] );
	//echo("pts",pts);
	MarkPts( pts2, ["labels",true] );
	//echo("pts2", pts2);
	nbsall = neighborsAll( pts2, cycle=true );
	for (i= [0:len(pts2)-1] )
	{
		Line0( rearr( nbsall[i], [0,2] ), ["r",0.01] );
		Plane( nbsall[i], ["mode",3, "color","blue"]);
	}

}
module neighborsAll_demo_2()
{
	echom("neighborsAll");
	
	pts2 = randPts(9);
	//Chain( pts2, ["r",0.025,  "color","red"] );
	//echo("pts",pts);
	MarkPts( pts2, ["labels",true] );
	echo("pts2", pts2);
	nbsall = neighborsAll( pts2, cycle=true );
	for (i= [0:len(pts2)-1] )
	{
		Line0( rearr( nbsall[i], [0,2] ), ["r",0.01] );
		Plane( nbsall[i], ["mode",3, "color","blue"]);
	}

}	
module neighborsAll_demo()
{//neighborsAll_demo_1();
 neighborsAll_demo_2();
}	
//neighborsAll_demo();

//neighbors_test();




//========================================================
normal= ["normal","pqr","pt","Math, Geometry",
"
 Given a 3-pointer pqr, return the normal (a vector) to the plane
;; formed by pqr.
"];

function normal(pqr, len=undef)=
(
	isnum(len)? onlinePt( [ORIGIN, cross( pqr[0]-pqr[1], pqr[2]-pqr[1] )], len=len)
	:cross( pqr[0]-pqr[1], pqr[2]-pqr[1] )
);

module normal_test( ops )
{
	pqr = [[-1, -2, 2], [1, -4, -1], [2, 3, 3]];
	doctest
	(
		normal, 
		[
			[ pqr , normal(pqr), [-13, 11, -16] ]

		], ops
	);
}

module normal_demo()
{
	//pqr = [[-1, -2, 2], [1, -4, -1], [2, 3, 3]];
	pqr = randPts(3);
	PtGrid( pqr[0] );
	PtGrid( pqr[1] );
	PtGrid( pqr[2] );

	Chain( pqr, ["closed",false]);

	//PtGrid( normal(pqr) );
    
    nm = normal(pqr);
	color("blue")
	Line0( [[0,0,0],nm]);
    
	Plane(pqr, ["mode",4] );

    // We create a line that is perpendicular to the normal and 
    // passing through pqr[0]. This line has to be on the plane [pqr]

    // othopt = pqr[1]'s projection on line [ORIGIN:nm],
    //        = normal's projection on the plane
    othopt = othoPt( [ ORIGIN
                    , pqr[1]
                      , nm ] );
                      
    // Red line: the line between one corner and the othopt                  
    Line0( [othopt, pqr[1]], ["r",0.05, "color","red"] );                   
    
    
    // Yellow line; extension of the normal to reach the plane:
    Line( [nm, othopt], ["r",0.1, "color","yellow", "transp",0.3] );
    
    // If an L-shape shows up, means it's 90-degree:
    Mark90( [pqr[1], othopt, nm],["color","black", "r",0.02] );


	// len = 1
	nm2 = normal(pqr, len=1);
	Line0( [ORIGIN, nm2], ["r",0.2, "transp",0.3] );

	MarkPts( pqr, ["labels","PQR"] );
	MarkPts( [nm,nm2], ["labels",["N","N1"]]);
}
//normal_test();
//normal_demo();
//doc(normal);



//========================================================
Normal=["Normal", "pqr,ops","n/a","3D, Line, Geometry",
 " Given a 3-pointer (pqr) and ops, draw the normal of plane made up by
;; pqr. The normal will be made passed through Q (pqr[1]). Its length
;; is determined by L/3 where L the shortest length of 3 sides. The
;; default ops:
;;
;; [ ''r''      , 0.03
;; , ''transp'' , 0.5
;; , ''mark90p'', true
;; , ''mark90r'', true
;; , ''len''    , L/3
;; ]
"];
 
module Normal_test( ops=["mode",13] ){ doctest( Normal, ops=ops); }

module Normal( pqr, ops )
{
	L = min( d01(pqr), d02(pqr), d12(pqr));
	ops = update( [
			  "r"     , 0.03
			  ,"len"  , L/3
			, "transp", 1
			, "mark90p", false
			, "mark90r", true
			, "reverse", false
			, "label", false
			], ops);
	function ops(k)= hash(ops,k);
	echo(ops);
	mark90p = ops("mark90p");
	mark90r = ops("mark90r");

	Nrm = (ops("reverse")?-1:1)*normal( pqr )/3;
	_N = Nrm + pqr[1];
	N = onlinePt( [ORIGIN+ pqr[1],_N]
				, len=ops("len") );

	MarkPts([N]);

	if( mark90p ) { Mark90( [ N,pqr[1],pqr[0] ], mark90p ); } 
	if( mark90r ) { Mark90( [ N,pqr[1],pqr[2] ], mark90r ); } 

	Line([ ORIGIN+ pqr[1], N ], concat( ["head",["style","cone"]], ops) );
	
}

module Normal_demo_1()
{
	pqr = randPts(3);
	MarkPts( pqr, ["grid",["mode",2]] );
	Chain( pqr );
	Normal( pqr );

	color("red") Normal(pqr, ["reverse",true]);
}

module Normal_demo()
{
	Normal_demo_1();
}
//Normal_demo();




//========================================================
normalPt=["normalPt", "pqr,len,reversed", "point", "Geometry, Point",
 " Given a 3-pointer (pqr), optional len and reversed, return a 
;; point representing the normal of pqr."
];
function normalPt(pqr, len=undef, reversed=false)=
(
	len==undef? (reversed?-1:1)*normal(pqr)+pqr[1]
			  :onlinePt( [ pqr[1], (reversed?-1:1)*normal(pqr)+pqr[1]], len= len )
);

module normalPt_test( ops=["mode",13] )
{
	doctest( normalPt,
	[]
	, ops
	);
}
		

module normalPt_demo()
{
	echom("normalPt");
	pqr = randPts(3);
	P = pqr[0];  Q=pqr[1]; R=pqr[2];
	Chain( pqr, ["closed", false] );
	N = normalPt( pqr );
	echo("N",N);
	MarkPts( concat( pqr,[N]) , ["labels","PQRN"] );
	//echo( [Q, N] );
	Mark90( [N,Q,P] );
	Mark90( [N,Q,R] );
	Dim( [N, Q, P] );
	Line0( [Q, N], ["r",0.02,"color","red"] );
	Nr = normalPt( pqr, len=1, reversed=true);
	echo("Nr",Nr);
	Mark90( [Nr,Q,P] );
	Mark90( [Nr,Q,R] );
	Dim( [Nr, Q, P] );
	Line0( [Q, Nr], ["r",0.02,"color","green"] );

}
//normalPt_demo();
//Dim_test( );



//========================================================
normalPts=[[]];
function normalPts(pts, len=1)=
 let(tripts= subarr(pts,  cycle=true)
	)
( 
	[for(pqr=tripts) normalPt(pqr,len=len)]
);


module normalPts_demo_1()
{_mdoc("normalPts_demo_1",
"");

	pts = randOnPlanePts( 4 ); //randPts(3), 4);
	tripts= subarr( pts, cover=[1,1],cycle=true);
	
	norms= [for(pqr=tripts) normalPt(pqr,len=1)-Q(pqr) ];
	

	angles = [for(pqr=tripts) angle(pqr) ];

	up = normalPts(pts, len=1);
	down= normalPts(pts, len=-0.5);

	Chain( pts, ["color","red", "r",0.03, "closed",true] );
	Chain( up, ["color","green","r",0.02,"closed",true]);
	MarkPts( pts, ["labels",true, "r",0.05] );
	
	range_planeIndices= range(pts,cover=[0,1],cycle=true);
	rngp = range_planeIndices;
	echo("rngp",rngp);

	for (i=range(pts)){

		Line( [down[i], up[i]], ["r",0.005, "color","blue"] );
		Plane ( [ up  [ rngp[i][0] ]
				, up  [ rngp[i][1] ]
				, down[ rngp[i][1] ]
				, down[ rngp[i][0] ]
				]);	
	}
	//Dim( [pts[0], larger[0]] );
}

module normalPts_demo_2_arc()
{
	pts2d = arcPts(r=3, a=120, count=9);
	
	pts= pts2d;
	//echo("pts:", pts);
	larger = normalPts(pts, len=0.5);
	smaller= normalPts(pts, len=-0.5);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green","r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01, "closed",true] );
	MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

	}

}

module normalPts_demo_3_arc()
{
	pts = arcPts(r=3, a=120, count=9);
	//echo("pts:", pts);
	larger = expandPts(pts, dist=0.2);
	smaller= expandPts(pts, dist=-0.2);
	up = normalPts(pts, len=0.2);
	down= normalPts(pts, len=-0.2);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green", "r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01,"closed",true] );
	Chain( up, ["color","green", "r",0.01,"closed",true]);
	Chain( down, ["color","blue", "r",0.01,"closed",true] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(up,i+1, cycle=true), up[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(down,i+1, cycle=true), down[i] ]);
	}

}

module normalPts_demo_4_arc()
{
	count=12;
	pts = arcPts(r=3, a=120, count=count);
	
	larger = expandPts(pts, dist=0.2);
	smaller= expandPts(pts, dist=-0.2);
	up = normalPts(pts, len=0.2);
	down= normalPts(pts, len=-0.2);
//	Chain( pts, ["color","red", "r",0.01, "closed",true] );
//	Chain( larger, ["color","green", "r",0.01,"closed",true]);
//	Chain( smaller, ["color","blue", "r",0.01,"closed",true] );
//	Chain( up, ["color","khaki", "r",0.01,"closed",true]);
//	Chain( down, ["color","purple", "r",0.01,"closed",true] );
	MarkPts( up, ["r",0.05] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );
	//LabelPts( larger, labels=range(larger) );
	//LabelPts( up, labels=repeat([len(smaller)],len(smaller))+ range(smaller));
	//LabelPts( smaller, labels=repeat([len(smaller)*2],len(smaller))+ range(smaller));
	//LabelPts( down, labels=repeat([len(smaller)*3],len(smaller))+ range(smaller));
 
	echo( "len pts:", len(pts));
    allpts = concat( larger, up, smaller, down );
	function getfaces(_I_=0)=
	(	_I_<len(pts)-1?
		concat(  [ [_I_, _I_+1, _I_+len(pts)+1, _I_+len(pts) ]
				, [ _I_+len(pts), _I_+len(pts)+1, _I_+2*len(pts)+1, _I_+2*len(pts)]
				, [ _I_+len(pts)*2, _I_+len(pts)*2+1, _I_+len(pts)*3+1, _I_+len(pts)*3]
				, [ _I_+len(pts)*3, _I_+len(pts)*3+1, _I_+1, _I_] //_I_+len(pts)*4+1, _I_+len(pts)*4]
				]
			  , getfaces(_I_+1)
			):[]
	); 
	faces = concat( [ ]//[0, count, 2*count, 3*count ]]
//				    , [count-1,
				 , getfaces());
	echo("faces", faces);

	polyhedron( points = allpts, faces = faces );

}
module normalPts_demo_5_convexity()
{
	//
	// show normal sign(s) along a path for convexity determination later
	// 

	echom("normalPts_demo_5_convexity");

	
	pts2d = arcPts(r=3, a=120, count=9);
	
	function to3dPts(pts,z=0,_i_=0)=
	( _i_<len(pts)? concat(
		 [[pts[_i_][0], pts[_i_][1], z]]
		, to3dPts( pts, z, _i_+1)
		):[]
	);
	pts = to3dPts(pts2d);
	//echo("pts:", pts);
	larger = expandPts(pts, dist=0.2);
	smaller= expandPts(pts, dist=-0.2);
	up = normalPts(pts, len=0.2);
	down= normalPts(pts, len=-0.2);
	Chain( pts, ["color","red", "r",0.01, "closed",true] );
	Chain( larger, ["color","green", "r",0.01,"closed",true]);
	Chain( smaller, ["color","blue", "r",0.01,"closed",true] );
	Chain( up, ["color","green", "r",0.01,"closed",true]);
	Chain( down, ["color","blue", "r",0.01,"closed",true] );
	//MarkPts( pts, ["labels",true, "r",0.02] );
	//MarkPts( larger );

	for (i=[0:len(pts)-1]){

		Line( [smaller[i], larger[i]], ["r",0.005, "color","blue"] );
		color("red");
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(larger,i+1, cycle=true), larger[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(smaller,i+1, cycle=true), smaller[i] ]);

		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(up,i+1, cycle=true), up[i] ], ["color","red"]);
		Plane( [ pts[i], get(pts,i+1, cycle=true)
			   , get(down,i+1, cycle=true), down[i] ]);
	}

}
//normalPts_demo_1();
//normalPts_demo_2_arc();
//normalPts_demo_3_arc();
//normalPts_demo_4_arc();


//========================================================
num=[ "num", "x", "number", "Type, Number",
" Given x, convert x to number. x could be a number, a number string,
; or a string for feet-inches like 3'4'' that will be converted to number
; in feet.
;
; num(''-3.3'') => -3.3
; num(''2'6'''') => 2.5  // 2.5 feet 
"];

function num(x,_i_=0)=
(
	isnum(x)
	 ? x
	 : isstr(x)
	   ?(
		 x==""
		 ? undef
		 : endwith(x,"'")||endwith(x,"\"")
		 ? ftin2in(x)/12
		 : _i_<len(x)
		   ?(
			index(x,".")==0
		  	? num( str("0",x), _i_=0)	
		 	: _i_==0 && x[0]=="-"
		   		?-num(x,_i_=1)
		   		: (_i_== index(x,".") //x[_i_]=="."
				  ? 0
					// 345.6      3456
					// 210 -1     3210  <== pow(10, ?)
					// 01234      0123  <== _i_
					//    3         -1  <== index(x,".")
					
				  : pow(10, (index(x,".")<0
							?(len(x)-_i_-1)
							: ( index(x,".")-_i_ )
							  -( index(x,".")>_i_?1:0)	
						   )
					  )
				    * hash( ["0",0, "1",1, "2",2
				 		,"3",3, "4",4, "5",5
						,"6",6, "7",7, "8",8
						,"9",9], x[_i_] )

				  )+ num(x, _i_+1)
		    )
		   :0 // out of bound
		)
		:undef // everything other than string and num
);
module num_test( ops )
{
	doctest
	( 
		num, [
	   ["-3.8",num(-3.8),-3.8]
	  , ["''3''",num("3"),3]
	  , ["''340''",num("340"),340]
	  , ["''3040''",num("3040"),3040]
	  , ["''3.4''",num("3.4"),3.4]
	  , ["''30.04''",num("30.04"),30.04]
	  , ["''340''",num("340"),340]
	  , ["''3450''",num("3450"),3450]
	  , ["''.3''",num(".3"),0.3]
	  , ["''-.3''",num("-.3"),-0.3]
	  , ["''0.3''",num("0.3"),0.3]
	  , ["'-'0.3''",num("-0.3"),-0.3]
	  , ["''-15''",num("-15"),-15]
	  , ["''-3.35''",num("-3.35"),-3.35]
	  , ["''789.987''",num("789.987"),789.987]
	  , ""	
	  , "// Convert to feet:"
	  , ["''2'6''''", num("2'6\""), 2.5]
	  , ["''3''''", num("3\""), 0.25]
	  , ["''3'\"", num("3'"), 3]
	  , ""	
	  , ["[3,4]",num([3,4]),undef]
	  , ["''x''",num("x"),undef]
	  , ["''34-5''",num("34-5"),undef]
	  , ["''34x56''",num("34x56"),undef]
	  , ["''''",num(""),undef]
	 ],ops	
	);
}

function _getdeci(n)= index(str(n),".")<0?0:len(split(str(n),".")[1]);

//========================================================
numstr=["numstr","n,d=undef,dmax=undef, convert=undef", "string", "Number, String",
" Convert a number to string. d = decimal:
; 
; numstr(3.8, d=3) = 3.80   ... (a)
; numstr(3.8765, d=3) = 3.877 ... (b)
; 
; If we want to limit decimal points (b) but don't want to add
; extra 0 (a), then we can use dmax:
;
; numstr(3.8, dmax=3) = 3.8
; numstr(3.8765, dmax=3) = 3.877
"];
function numstr(n,d=undef, dmax=undef, conv=undef)=
(
	conv=="ft:ftin"
	?(n<1
	   ? str( numstr(n*12, d, dmax) , "\"")
	   : mod(n,1)==0
		? str(n, "'")
		: str( floor(n), "'"
			, numstr( mod(n,1)*12,d,dmax), "\"")	
	 )
	:conv=="in:ftin"
	? numstr( ( isstr(n)&&endwith(n,"\"")?num(shift(n,"\"","")):n )
			 /12, d=d, dmax=dmax, conv="ft:ftin")
	:conv
	? numstr( uconv(n, from=split(conv,":")[0], to=split(conv,":")[1] )
			, d=d, dmax=dmax )
//	: conv=="in:cm"
//	? numstr( n*2.54, d=d, dmax=dmax)
//	: conv=="ft:cm"
//	? numstr( n*12*2.54, d=d, dmax=dmax)

//	: conv=="ftin:cm" && isstr(n)
//	? numstr( endwith(n,"'")
//			 ? num(shift(n)*12 
//			index(n, "'")>0? // 3'2", 3'
//			( num(split(n,"'")[0])*12
//			 )+(len(split(n,"'"))>1?
//			  split(n,"'")[0]*12:0
//			 ) n*12*2.54, d=d
//			)

	// Finish conv part above. Start processing decimal 

	: isint(d)&&d>-1
	  ?(
		isint(dmax)  // when both defined
		? _getdeci(n)== min(d,dmax)
           ? str(n)
		  : _getdeci(n)< min(d,dmax)
		    ? str( n, repeat("0", min(d,dmax)-_getdeci(n)))
			: str( round(n*pow(10,min(d,dmax)))
				        /pow(10,min(d,dmax))
				)	
		: // d given, dmax not
		  _getdeci(n)== d
           ? str(n)
		  : _getdeci(n)< d
		    ? str( n, isint(n)?".":"", repeat("0", d-_getdeci(n)))
			: str( round(n*pow(10, d))
				        /pow(10,d)
				)
	   )
	  :(
		isint(dmax)
		?_getdeci(n)<= dmax
           ? str(n)
		  : str( round(n*pow(10, dmax))
				        /pow(10, dmax)
			  )			 
		:str(n)
	   )
);

module numstr_test( ops=["mode",13])
{
	doctest( numstr,
	[
		"// Integer: "
		,["0", numstr(0), "0"]
		,["5", numstr(5), "5"]
		,["5,dmax=2", numstr(5,dmax=2), "5"]
		,["5,d=2", numstr(5,d=2), "5.00"]
		,["38,d=0", numstr(38, d=0), "38"]
		,["38,d=2", numstr(38, d=2), "38.00"]
		,"// Float. Note the difference between d and dmax: "
		,["3.80", numstr(3.80), "3.8"]
		,["38.2,dmax=3", numstr(38.2, dmax=3), "38.2"]
		,["38.2,d=3, dmax=2", numstr(38.2, d=3, dmax=2), "38.20"]
		,["38.2,d=3, dmax=4", numstr(38.2, d=3, dmax=4), "38.200"]
		,["38.2,d=5, dmax=4", numstr(38.2, d=5, dmax=4), "38.2000"]
		,["3.839, d=0", numstr(3.839,d=0), "4"]
		,["3.839, d=1", numstr(3.839,d=1), "3.8"]
		,["3.839, d=2", numstr(3.839,d=2), "3.84"]
		,["3.839, d=3", numstr(3.839,d=3), "3.839"]
		,["3.839, d=4", numstr(3.839,d=4), "3.8390"]
		,["3.839, d=1, dmax=2", numstr(3.839,d=1, dmax=2), "3.8"]
		,["3.839, d=2, dmax=2", numstr(3.839,d=2, dmax=2), "3.84"]
		,["3.839, d=3, dmax=2", numstr(3.839,d=3, dmax=2), "3.84"]
		,["3.839, d=4, dmax=2", numstr(3.839,d=4, dmax=2), "3.84"]		
		,["-3.839, d=1", numstr(-3.839,d=1), "-3.8"]
		,["-3.839, d=2", numstr(-3.839,d=2), "-3.84"]
		,["-3.839, d=4", numstr(-3.839,d=4), "-3.8390"]

		,"// conversion: "
		,"// conv = ''ft:ftin''"

		,["0.5,conv=''ft:ftin''", numstr(0.5,conv="ft:ftin"), "6\""]
		,["3,conv=''ft:ftin''", numstr(3,conv="ft:ftin"), "3'"]
		,["3.5,conv=''ft:ftin''", numstr(3.5,conv="ft:ftin"), "3'6\""]
		,["3.5,d=2,conv=''ft:ftin''", numstr(3.5,d=2,conv="ft:ftin"), "3'6.00\""]
		,["3.343,conv=''ft:ftin''", numstr(3.343,conv="ft:ftin"), "3'4.116\""]
		,["3.343,d=2, conv=''ft:ftin''", numstr(3.343,d=2,conv="ft:ftin"), "3'4.12\""]
		,"// conv = ''in:ftin''"
		,["96,conv=''in:ftin''", numstr(96,conv="in:ftin"), "8'"]
		,["32,conv=''in:ftin''", numstr(32,conv="in:ftin"), "2'8\""]
		,"// d is ignored if negative:"
		,["38,d=-1", numstr(38, d=-1), "38"]
		
	], ops );
}
//numstr_test();


//========================================================
onlinePt=[ "onlinePt", "PQ,len=false,ratio=0.5", "pt", "Point",
"
 Given a 2-pointer (PQ), return a point (pt) that lies along the
;; line between P and Q, with its distance away from P defined by 
;; *dist* or by *ratio* if dist is not given. When ratio=1, return Q.
;; Both *dist* and *ratio* can be negative.
"];

function onlinePt(pq, len=false, ratio=0.5)=
(
	[ (isnum(len)?len/dist(pq):ratio)*distx(pq)+pq[0].x
	, (isnum(len)?len/dist(pq):ratio)*disty(pq)+pq[0].y 
	, (isnum(len)?len/dist(pq):ratio)*distz(pq)+pq[0].z ] 
	
);

module onlinePt_test( ops )
{
	pq= randPts(2);
	d1pt= onlinePt( pq, len=1 );
	r3pt= onlinePt( pq, ratio=0.3);
	distpq = dist(pq);
 
	doctest( onlinePt, 
	[
	   [ "pq,0", onlinePt(pq,0), pq[0] ]
	 , [ "pq,ratio=1", onlinePt(pq,ratio=1), pq[1] ]

	 , [ "pq,len=1", norm(pq[0]- onlinePt(pq,len=1))
		, 1, ["funcwrap", "norm({_}-pq[0])"]]
	 , [ "pq,len=-2", norm(pq[1]- onlinePt(pq,len=-2))
		, distpq+2, ["funcwrap", "norm({_}-pq[1])"]]

	 , [ "pq,ratio=0.3", norm(pq[0]- onlinePt(pq,ratio=0.3))
		, distpq*0.3, ["funcwrap", "norm({_}-pq[0])"]]
	 , [ "pq,ratio=-0.3", norm(pq[1]- onlinePt(pq,ratio=-0.3))
		, distpq*1.3, ["funcwrap", "norm({_}-pq[1])"]]	 
	 ]
	, ops, ["pq", pq, "distpq", distpq, "d1pt", d1pt, "r3pt", r3pt]
            );
}

module onlinePt_demo()
{
	pr = randPts(2);
	q = onlinePt( pr, len=-0.8 );
	Line(pr);
	MarkPts( [pr[0],q,pr[1]], ["grid",true,"echopts",true] );

	eg = randPts(2); 
	f = onlinePt( eg, ratio=0.33 );
	Line(eg);
	MarkPts( [eg[0],f,eg[1]], ["grid",true,"echopts",true] );

}
//onlinePt_test( ["mode",22] );
//onlinePt_demo();
//doc(onlinePt);





//========================================================
onlinePts=["onlinePts","PQ,lens=false,ratios=false","path","Point",
"
 Given a 2-pointer (PQ) and a numeric array assigned to either lens
;; or ratios, return an array containing pts along the PQ line with 
;; distances (from P to each pt) defined in the given array. Refer to
;; onlinePt().
;; This is good for getting multiple points on pq line. For example, to
;; get 3 points that divide pq line evenly into 4 parts:
;;
;;    onlinePts( pq, ratios=[ 0.25, 0.5, 0.75 ] );
"];

function onlinePts(pq, lens=false, ratios=false)=
(
	ratios==false?
	( len(lens)==0?[]
	  :	concat( [onlinePt( pq, len=lens[0])]
		  , onlinePts( pq, lens=shrink(lens) )
		  )
	):
	( len(ratios)==0?[]
	  :	concat( [onlinePt( pq, ratio=ratios[0])]
		  , onlinePts( pq, ratios= shrink(ratios) )
		  )
	)
);

module onlinePts_test( ops )
{
	pq= [[0,1,0],[10,1,0]]; //randPts(2);
	lens= [-1,2];
	ratios= [0.2, 0.4, 0.6];
	distpq = dist(pq);
 
	doctest( onlinePts, 
	[
	   [ "pq,[-1,2]", onlinePts(pq,lens), [[-1, 1, 0], [2, 1, 0]] ]
	,  [ "pq,ratios=[0.2, 0.4, 0.6]", onlinePts(pq,ratios=ratios), [[2, 1, 0], [4, 1, 0], [6, 1, 0]]]
	 ]
	, ops, ["pq", pq] 
            );
}

/*
module onlinePts_test( ops )
{
	pq = [ORIGIN, [5,5,1]];
	lens = [0.5, 1, 2, 2.5];

	doctest( onlinePts
	,[
		[ [pq ,lens]
		, onlinePts( pq, lens= lens )
		,  [[0.35007, 0.35007, 0.070014], [0.70014, 0.70014, 0.140028], [1.40028, 1.40028, 0.280056], [1.75035, 1.75035, 0.35007]]
		]
	], update(ops, ["asstr",true])
	);
}
*/
module onlinePts_demo_1()
{
	pq = randPts(2);
	lens = [0.5, 1, 2, 2.5];
	pts = onlinePts( pq, lens = lens );
	
	Line0(pq, ["r",0.05, "color", "red", "transp",0.5] );
	MarkPts( pts );

	ratios = accumulate(repeat([0.2],5));
	pq2 = randPts(2);
	pts2 = onlinePts( pq2, ratios=ratios );
	//echo("in onlinePts, ratios = ", ratios);
	//echo("in onlinePts, pts2 = ", pts2);
	
	Line0(pq2, ["r",0.05, "color", "green", "transp",0.5] );
	MarkPts( pts2 );
}

module onlinePts_demo_2_evenly_divide()
{
	module evenDiv(count, color="red")
	{
		ratios = accumulate(repeat([1/count],count));
		pq2 = randPts(2);
		pts2 = onlinePts( pq2, ratios=ratios );	
		Line0(pq2, ["r",0.05, "color", color, "transp",0.5] );
		MarkPts( pts2 );
	}

	
	evenDiv( 4 );
	evenDiv( 6, "green");
}
module onlinePts_demo()
{
	//onlinePts_demo_1();
	onlinePts_demo_2_evenly_divide();
}

//onlinePts_test( ["mode",22] );
//onlinePts_demo();
//doc( onlinePts );



/*
//========================================================
function othoCrossLinePts(pqr, r=1)=
(
	[ onlinePt( 
		[ pqr[1], othoPt( pqr) ], ratio= 1/norm( othoPt(pqr)-pqr[1])
		)

	,onlinePt( 
		[ pqr[1], othoPt( pqr) ], ratio= -1/norm( othoPt(pqr)-pqr[1])
		)
	]
);

module othoCrossLinePts_demo(){
    //pqr=[ [0,-1], [1,2],[4,0]];
	pqr=randPts(pqr);
	clpts= othoCrossLinePts( pqr );
	union(){
		Line( pqr, w=0.05);
		Line( shrink(pqr), w=0.05);
		Line( [pqr[0], pqr[2]], w=0.05);
		color("blue")
		Line( clpts, w=0.05 );
		color("green",0.2)
		Line( [pqr[1],othoPt( pqr)], w=0.2);
	}

	pqr2=[ [3,1], [1,0],[-4,0]];
	clpts2= othoCrossLinePts( pqr2 );
	union(){
		Line( pqr2, w=0.05);
		Line( shrink(pqr2), w=0.05);
		Line( [pqr2[0], pqr2[2]], w=0.05);
		color("blue")
		Line( clpts2, w=0.05 );
		color("green", 0.2)
		Line( [pqr2[1],othoPt( pqr2)], w=0.2);
	}
	echo(clpts);
	echo(clpts2);
}
//othoCrossLinePts_demo();

*/

//========================================================
othoPt= [ "othoPt", "pqr", "pt", "Point",
"
 Given a 3-pointer (pqr), return the coordinate of the point D
;; such that line QD is at 90 degree with line PR.
;;
;;         Q
;;         :.
;;        /: :
;;       / :  :
;;      /  :   :
;;     /   :_   :
;;    /____:_l___:
;;   P     D      R
"];

function othoPt( pqr )= 
(
/*  Return [x,y] of D
           Q          ___
           ^\          :
          /: \         :   
       a / :  \_  b    : h
        /  :    \_     :
       /   :-+    \    :    
     P-----D-!------R ---

	|------ c ------|  
        
	a^2 = h^2+ PD^2
	b^2 = h^2+ DR^2  
	a^2- PD^2 = b^2-(c-PD)^2
			  = b^2-( c^2- 2cPD+PD^2 )
			  = b^2- c^2 + 2cPD -PD^2
	a^2 = b^2-c^2+ 2cPD
	PD = (a^2-b^2+c^2)/2c
	ratio = PD/c = (a^2-b^2+c^2)/2c^2    
*/
	onlinePt( [pqr[0],pqr[2]]
	        , ratio=( d01pow2(pqr) - d12pow2(pqr) + d02pow2(pqr))/2/d02pow2(pqr) 
	)
);
module othoPt_test( ops )
{
	pqr= randPts(3);
	opt= othoPt( pqr );
	doctest( othoPt,
	[
		[ "pqr", is90([ pqr[0], opt],[opt, pqr[1]]), true,
			["funcwrap", "is90([ pqr[0], {_}], [othoPt(pqr),  pqr[1]])"]
		]
		,[ "pqr", is90([ pqr[0], opt, pqr[1]]), true,
			["funcwrap", "is90([ pqr[0], {_},  pqr[1]])"]
		]
		,[ "pqr", angle([ pqr[0], opt, pqr[1]]), 90,
			["funcwrap", "angle([ pqr[0], {_}, pqr[1]])"]
		]
	], ops, ["pqr",pqr]
	);
}
//is90_test();

module othoPt_demo(){
    ops = [["r",0.02],["color","blue"]];

	//pqr=[ [0,-1,0], [1,2,3],[4,0,-1]];
	pqr=randPts(3);
	otho = othoPt(pqr);
	
	Chain( pqr, ["closed",true] );
	Line0( [pqr[1],othoPt( pqr)], ops);
	MarkPts( pqr);
	Mark90( [ norm(pqr[0]-pqr[1])>norm(pqr[1]-pqr[2])?pqr[0]:pqr[2], otho, pqr[1] ] );
	//echo("pqr= ", pqr);
	//echo("is90 pqr: ", is90([ pqr[0], pqr[1]], [ pqr[1], othoPt(pqr)] ) );
	
	//pqr2=[ [3,1,-1], [1,0,2],[-4,0,1]];
	pqr2= randPts(3);
	otho2 = othoPt(pqr2);
	//echo("pqr2= ", pqr2);
	MarkPts( pqr2 );
	Plane( pqr2, ["mode",3] ) ;//Chain( pqr2, [["closed",true]]);
	Line0( [pqr2[1],othoPt( pqr2)], ops);
	Mark90( [ norm(pqr2[0]-pqr2[1])>norm(pqr2[1]-pqr2[2])?pqr2[0]:pqr2[2], otho2, pqr2[1] ] );
	
	//echo("is90 pqr2: ", is90([ pqr2[0], pqr2[1]], [ pqr2[1], othoPt(pqr2)] ) );
	
}
//othoPt_demo();
//othoPt_test( ["mode",22] );
//doc(othoPt);
//

//	countArr(a1)!=len(a1)
//	? errmsg( "typeError", ["fname","concatEach","vname","a1", "vtype",type(a1),"tname","Array of arrays"])
//	:countArr(a2)!=len(a2)
//	? errmsg( "typeError", ["fname","concatEach","vname","a2", "vtype",type(a2),"tname","Array of arrays"]):
//	_I_< max(len(a1),len(a2))
//	?  
//	concat( [ concat( a
	



//========================================================
permute=["permute","arr","array","Array,Math",
 " Given an array, return its permutation."
];

function permute(arr, _I_=0)=
(
	len(arr)==2
	? [arr,[arr[1],arr[0]]]
	:len(arr)==3
	? [arr, p021(arr), p102(arr), p120(arr), p201(arr), p210(arr) ]  
	:_I_<len(arr)
	? concat(
			prependEach( permute( del(arr,_I_, byindex=true),_I_+1)
						, arr[_I_]
						)
			, permute(arr,_I_+1) )
	: []

);

module permute_test( ops=["mode",13] )
{
	doctest( permute
	,[
	  ["[2,4,6]", permute([2,4,6])
		, [[2,4,6],[2,6,4],[4,2,6],[4,6,2],[6,2,4],[6,4,2]]
	  ]
	 , ["[1,2,3,4]", permute([1,2,3,4]),
		[ [1, 2, 3, 4], [1, 2, 4, 3], [1, 3, 2, 4]
		, [1, 3, 4, 2], [1, 4, 2, 3], [1, 4, 3, 2]
		, [2, 1, 3, 4], [2, 1, 4, 3], [2, 3, 1, 4]
		, [2, 3, 4, 1], [2, 4, 1, 3], [2, 4, 3, 1]
		, [3, 1, 2, 4], [3, 1, 4, 2], [3, 2, 1, 4]
		, [3, 2, 4, 1], [3, 4, 1, 2], [3, 4, 2, 1]
		, [4, 1, 2, 3], [4, 1, 3, 2], [4, 2, 1, 3]
		, [4, 2, 3, 1], [4, 3, 1, 2], [4, 3, 2, 1]
	    ] 
	   ]
	,  ["range(3)", permute(range(3)),[[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]]]
	], ops
	);
}
//permu_test();

//========================================================
pick=["pick", "o,i", "pair", "Array",  
"
 Given an array or a string (o) and index (i), return a two-member array :
;;
;; [ o[i], the rest of o ] // when o is array
;; [ o[i], [ pre_i string, post_i_string] ] //when o is a string
;;
;; Note that index could be negative.
"];
function pick(o,i) = 
(
  	concat( [get(o,i)]
		  , isarr(o)
			? [ del(o,i, byindex=true) ] 
			: [[ slice(o,0,i), slice(o,i+1) ]]
		  )
  
);

module pick_test(ops)
{
	arr = range(1,5);
	arr2= [ [-1,-2], [2,3],[5,7], [9,10]] ;
	
	doctest( pick
	,[
		"//Array:"
		,["arr,2", pick(arr, 2), [ 3, [1,2,4] ] ]
		,["arr,-3", pick(arr,-3), [ 2, [1,3,4] ] ]
		,["arr2,2", pick(arr2,2), [ [5,7], [ [-1,-2], [2,3], [9,10]] ] ]
		,"//String:"
		,["''abcdef'',3", pick("abcdef",3), ["d", ["abc", "ef"]] ]
		,["''this is'',2", pick("this is",2), [ "i", ["th", "s is"] ] ]
		,"//Note:"
		,str("split(''this is'',''i'') = ", split("this is","i") )
	],ops, ["arr", arr, "arr2", arr2]
	);
}
//pick_test( ["mode",13] );





//========================================================
Pie=[[]];

module Pie( angle, r, ops=[] )
{
	
	ops = concat( ops
		// ,["h",1
		//  ]
		 ,OPS);
	
	function ops(k) = hash( ops, k);
	
	arcpts = concat( [[0,0]], arcPts( a=angle, r = r, count = angle) );
	//echo( "arcpts: ", arcpts);

	paths = [range(len(arcpts))];

	color(ops("color"), ops("transp"))
	polygon ( points=arcpts, paths=paths); 



}

module Pie_demo()
{

	Pie(angle=60, r=3);

	ColorAxes();
	
	translate( [0,0,-1])
	Pie( angle=80, r=4 );

	scale([1,1,0.5])
	translate( [0,0,-2])
	Pie( angle=280, r=4 );
	
	translate( [0,0,-3])
	Pie( angle=360, r=5 ); //<==== note this

}
//Pie_demo();



//========================================================
Plane=[ "Plane", "pqr,ops", "n/a", "2D, Geometry, Shape", 
" 
 Given a 3-pointer pqr and ops, draw a plane in space based on
;; settings in ops:
;;
;;  [ ''mode'', 1 // mode = 1~4
;;  , ''color'', ''brown''
;;  , ''transp'', .4
;;  , ''frame'', false
;;  , ''r'', 0.005
;;  , ''th'', 0 // thickness
;;  ]
;;
;; # mode=1: rect parallel to axes planes
;;
;;  The 1st pt is the anchor(starting) pt
;;  The 2nd pt is treated like sizes:
;;     [ x,y,* ] = parallel to xy-plane (* = any)
;;     [ x,0,z ] = parallel to xz-plane
;;     [ 0,y,z ] = parallel to yz-plane
;;  The 3rd pt, r, is ignored.
;;  Plane( [ [1,2,3],[4,-5,6]], [[''mode'',1]] )
;;     draw a plane of size 4 on x, 5 on -y
;;     starting from [1,2,3]
;;
;; # mode=2 rectangle
;;
;;   line p1-p2 is a side of rectangle.
;;   p3 is on the side line parallel to line p1~p2
;;
;; # mode=3 triangle
;;
;; # mode=4 parallelogram
;;
;; # frame: default= false
;;
;;   Draw a frame around using Chain if frame is true or is
;;    a hash as the ops option for Chain. For example
;;
;;    Plane( pqr, [[''frame'', [''color'', ''blue''] ]] )
;;
;;    draw a blue frame.
;;
;; NEW 2014.6.19: len(pqr) can be >3. But all faces connect to pqr[0].
"];

//doc(Plane);


module Plane(pqr, ops=[])
{
	//echom("Plane");
	//echo("pqr=", pqr);
	ops = concat(ops, [
				"mode", 1 // 1: rect parallel 2 xy-, xz- or yz- plan
							// 2: rect
							// 3: triangle
							// 4: parallelogram
				,"color","brown"
				,"transp", .4
				,"frame", false
				,"r",0.005
				,"th",0 // thickness
				 ]);
	function ops(k)=hash(ops,k);
	c = ops("color");
	t = ops("transp");
	m = ops("mode");
	th= ops("th");
	p1= pqr[0];
	p2= pqr[1];
	p3= pqr[2];
	p4 = m<4?undef
		:cornerPt(pqr);
	frame = ops("frame");

	//echo("ops = ",ops);

	// mode=1: axis-plane parallel rectangle 
	// 	
	// The 1st pt is the anchor(starting) pt
	// The 2nd pt is treated like sizes:
	//   [ x,y,* ] = parallel to xy-plane (* = any)
	//   [ x,0,z ] = parallel to xz-plane
	//   [ 0,y,z ] = parallel to yz-plane 
	//	The 3rd pt, r, is ignored.
	// Plane( [ [1,2,3],[4,-5,6]], [["mode",1]] )
	//   draw a plane of size 4 on x, 5 on -y
	//   starting from [1,2,3]
	m1p2 = p2.x==0?(p1+p0y0(p2)):p2.y==0?(p1+px00(p2)):(p1+px00(p2));
	m1p3 = p2.x==0?(p1+p0yz(p2)):p2.y==0?(p1+px0z(p2)):(p1+pxy0(p2));
	m1p4 = p2.x==0?(p1+p00z(p2)):p2.y==0?(p1+p00z(p2)):(p1+p0y0(p2));

	// mode=2 rectangle
	//
	// p1, p2 are 2 corners and form a line
	// p3 is on the line parallel to line p1~p2

	// all points
	ps= m==1?[p1,m1p2, m1p3, m1p4]
		:m==2?squarePts(pqr)
		:m==3?pqr
		:concat(pqr,[p4]);
		  
	/* 
		Trying to add thickness (th). The th is the dist between 
		the plane PL1 and its clone PL2 that can be obtained by 
		translation from PL1 by a scaled copy of a vector V that 
		is the normal of any 3 pts. This translation will move 
		PL1 through a line L that passes through P1 (any point on
		PL1) to Q1 (any pt on PL2).
		
		We can get this V easily (= normal(pqr)), but first need
		to figure out the scale factor k that makes the dist of 
		translation equal to th. For this we need to figure out
		what Q is. 

			vect = normal(pqr);
		 
		The th is:

			sqrt( k^2*x^2+ k^2*y^2 + k^2*z^2 )= th

			k^2*x^2+ k^2*y^2 + k^2*z^2 )= th^2

			k^2 = th^2 / ( x^2+ y^2 + z^2)

			k = th/ sqrt(x^2+ y^2 + z^2)) = th / norm([x,y,z])

	*/

	function getfaces(pts, _i_=0)=
	(	                            // new 2014.6.19
		_i_<len(pts)-1?
		concat( [ [0, _i_+1, _i_+2]]
			 , getfaces( pts,_i_+1)
			 )
		:[]
	);

	//echo("In Plane, pqr = ", pqr, ", faces = ", getfaces(pqr) );

	if(len(pqr)>3){ // new 2014.6.19
		//echo( "pqr > 3" );
		color("blue",t)
		polyhedron( points=pqr, faces=getfaces(pqr) );

		if(frame){
			if(isarr(frame)){
				//Chain( pqr, update(ops, update(["closed",true], frame)) );
				Chain( pqr, concat(frame, ["closed",true], ops) );
			}else{			
				Chain( pqr, concat(["closed",true],ops) );
			}
		}
	
	}else{

		if(m==3){
			color( c, t )
			polyhedron( points=pqr, faces=[[0,1,2]] );
			if( th>0){
//				echo_("normal(pqr)={_} ",[normal(pqr)]);
//				echo_("norm(pqr)={_} ",[norm(pqr[0])]);
//				echo_("normal(pqr)*th/norm(pqr)={_} "
//					,[normal(pqr)*th/norm(pqr[0])]);
				translate( normal(pqr)*th/norm(pqr[0]) )	
				polyhedron( points=pqr, faces=[[0,1,2]] );
			}
		}else{
			color( c, t )
			polyhedron( points=ps, faces=[[0,1,2],[2,3,0]] );
		}
		if(frame){
			if(isarr(frame)){
				//Chain( ps, update(ops, update(["closed",true], frame)) );
				Chain( ps, concat(frame, ["closed",true], ops) );
			}else{			
				Chain( ps, update(ops, ["closed",true]) );
			}
		}
	}
}

module Plane_demo_1()
{

	// mode=1
	Plane( randPts(3,r=false, x=[-1,-2],y=[-1,-2]), ["color","red","transp",1] );
	//Plane( [ randPts(1,r=1)[0], px0z(randPts(1,r=2)[0])]
	//					, ["color","lightgreen","transp",1] );
	//Plane( [ randPts(1,r=1)[0], p0yz(randPts(1,r=2)[0])]
	//					, ["color","blue","transp",1] );

	Plane( [ randPt(r=false, x=[-1,-2],y=[-1,-2]), px0z(randPts(1,r=2)[0])]
						, ["color","lightgreen","transp",1] );
	Plane( [ randPt(r=false, x=[-1,-2],y=[-1,-2]), p0yz(randPts(1,r=2)[0])]
						, ["color","blue","transp",1] );

	// mode=2
	pqr2 = randPts(3,r=false, x=[2,3],y=[2,3]);
	MarkPts( pqr2 );
	Plane( pqr2, ["mode",2,"color","red"]  );


	// mode=3
	pqr3 = randPts(3, ,r=false, x=[0,3],y=[-1,-3]);
	Plane( pqr3, ["mode",3,"color","green","th",0.5]  );
	
	// mode=4
	pqr4 = randPts(3, r=3);
	//MarkPts( pqr4 );
	Plane( pqr4, ["mode",4,"color","blue"]  );

	pqr5 = randPts(3, r=false, x=[0,2],y=[0,2],z=[-1,-3]);;
	Plane( pqr5, [ "mode",2, "color","khaki"
				, "frame",["r",0.08
						   ,"color","blue"
						   ,"transp",.8
						   ]
                   
				]);

}

module Plane_demo_2() // 2014.6.19
{
	pts = randPts(5);
	//echo( "pts: ", pts);
	MarkPts(pts);
	Plane( pts, ["color","red", "frame",true] );
	//echo( "Leaving demo 2");
}

module Plane_demo()
{
	//Plane_demo_1();
	Plane_demo_2();
}

//Plane_demo();

module Plane_test(ops){ doctest(Plane,ops=ops); }

//Plane_test( ["mode",22] );
//Plane_demo();




//========================================================
planecoefs=["planecoefs", "pqr, has_k=true", "arr", "Geometry, 3D, Math" ,
"
 Given 3-pointer pqr, return [a,b,c,k] for the plane ax+by+cz+k=0.
"];

function planecoefs(pqr, has_k=true)=
(
	/*
	https://answers.yahoo.com/question/index?qid=20110121141727AAncAu4

	Using normal vector <5, 6, -3> and point (2, 4, -3), we get equation of plane: 
	5 (x-2) + 6 (y-4) - 3 (z+3) = 0 
	5x - 10 + 6y - 24 - 3z - 9 = 0 
	5x + 6y - 3z = 43

	normal = [a,b,c]
	k = -(ax+by+c)

	*/	
	concat( normal(pqr) 
		  , has_k?[-( normal(pqr)[0]* pqr[0][0]
			 + normal(pqr)[1]* pqr[0][1]
			 + normal(pqr)[2]* pqr[0][2]
             )]:[]			 
		)	
);


module planecoefs_demo()
{
	// How do we know if the result of planecoefs are correct ?
	// 1. get random 3 points (randPts(3))
	// 2. Calc planecoefs
	// 3. Calc otho pt 
	// 4. Chk planecoefs using otho pt

	pqr = randPts(3);
	Plane(pqr, ["mode",4] );
	MarkPts( pqr );
	pcs = planecoefs(pqr); // =[ a,b,c,k ]
	a= pcs[0];
	b= pcs[1];
	c= pcs[2];
	k= pcs[3];
	
	otho = othoPt( pqr );
	MarkPts( [otho ], ["colors",["purple"], "transp",0.4] );

	isOnPlane= a*otho.x + b*otho.y + c*otho.z+k <= ZERO;

	echo("isOnPlane = ", isOnPlane);

}
module planecoefs_test(ops)
{
	pqr = randPts(3);
	//pcs = planecoefs(pqr); // =[ a,b,c,k ]
	//a= pcs[0];
	//b= pcs[1];
	//c= pcs[2];
	//k= pcs[3];
	othopt = othoPt( pqr );
	//abpt= angleBisectPt( pqr );
	//mcpt= cornerPt( pqr );
	
	doctest( planecoefs, 
	[
		[ "pqr", isOnPlane( planecoefs(pqr),othopt ), true 
		, ["funcwrap","isOnPlane( othopt, planecoefs= {_} )"]
		]
		,"// Refer to isOnPlane_test() for more tests."

	], ops, ["pqr", pqr, "othopt", othopt] );



}
//planecoefs_test();
//planecoefs_demo();
//doc(planecoefs);


//========================================================
prependEach=[ "prependEach", "arr,x", "arr", "Array",
 " Given an array of arrays (arr) and an item (x), prepend x to 
;; each array in arr.
"];

function prependEach( arr, x, _I_=0)=
(
	[ for(a=arr) concat([x],a) ]
//	_I_<len(arr)
//	? concat( [concat( [x], arr[_I_] )]
//		   ,prependEach(arr,x,_I_+1)
//			)
//	:[]
);

module prependEach_test( ops )
{ 
	arr=[[1,2],[3,4],[5,6]];
	doctest( prependEach,
	[
		["arr,9", prependEach(arr,9), [[9,1,2],[9,3,4],[9,5,6]] ]
	],ops, ["arr",arr]
	);
} 


//========================================================
projPt=[["projPt", "pt,pqr", "point", "Geometry, Point"],
"
 Given a point (pt) and a 3-pointer (pqr), return the projection of pt\\
 onto the pqr plane.\\
"];

function projPt( pt, pqr )=
(
	/*
http://stackoverflow.com/questions/8942950/how-do-i-find-the-orthogonal-projection-of-a-point-onto-a-plane

		The projection of a point q = (x, y, z) onto a plane 
given by a point p = (a, b, c) and a normal n = (d, e, f) is

q_proj = q - dot(q - p, n) * n

	n = (normal(pqr)/norm(normal(pqr)))
	*/
	pt - dot(pt-pqr[1], (normal(pqr)/norm(normal(pqr))))*(normal(pqr)/norm(normal(pqr))) 
);

module projPt_demo( )
{
	echom("projPt");

	pqrs = randPts(4 );
	echo( pqrs );
	P= pqrs[0];  Q=pqrs[1];  R=pqrs[2];  S=pqrs[3];
	pqr = [P,Q,R];

	J = projPt( S, pqr);
	echo("J: ", J);
	Plane( pqr, ["mode",3] );

	C= incenterPt(pqr);

	MarkPts( pqrs );
	MarkPts( [J,C] );
	LabelPts( [P,Q,R,S,J,C], "PQRSJC" );
	
	Chain( [C,J,S], ["closed",false] );
	Mark90( [C,J,S] );

	echo( "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;Check if the distance = distPtPl(...): ");
	echo( "norm( J - S )  = ", norm(J-S) );
	echo( "distPtPl(S,pqr)= ", distPtPl( S, pqr ) );
}

//projPt_demo();

	 
//========================================================

PtGrid=["PtGrid", "pt,ops", "n/a","3D, Point, Line, Geometry" ,
"
 Given a pt and ops, draw thin cylinders from pt to axes or axis planes
;; for the purpose of displaying the pt location in 3D. Default ops:
;;
;;  [ ''highlightaxes'', true
;;  , ''r'',0.007    // line radius
;;  , ''projection'', false, // draw projection on axis planes.
;;  ,                      // ''xy''|''yz''|''xy,yz''|...
;;  , ''mark90'', ''auto'' // draw a L-shape to mark the 90-degree angle
;;  , ''mode'',2 // 0: none
;;             // 1: 2lines: pt~p_xy, p@xy~Origin
;;             // 2: plane and line: pt~p@xy, p@xy~x, p@xy~y
;;             // 3: block
;;]
"];
/*
  , ''scale'', 1    // scale for internal grids \\
  , ''othoToAxis'', false // if true, draw from pt to axes \\
  , ''faces'',''all0'' // a str setting what face(s) \\
  ,    has the face grids. A box formed by the pt and the \\
  ,    origin has 6 faces, 3 touching the origin, 3 touching \\
  ,    the pt, represented by 0 and 1, respectively. A *faces* \\
  ,    ''??0'' means the face(s) that cover the origin will \\
  ,    have grids. For example, ''xy0, yz1''= both the face on \\
  ,    xy plane touching [0,0,0], and the face on yz1 plane \\
  ,    not touching [0,0,0] have the grids. To show grids on \\
  ,    all faces, use ''all0, all1''. \\ 
"];
*/

module PtGrid(pt, ops)
{
	L= norm(pt);
	//echo_("in PtGrid, pt={_}, L={_}, L/5={_}",[pt, L,(L/5)]);
	ops=updates(OPS,
		[[ "r",0.006
		, "transp", 0.8
		//, "color", "red"
		//, "style", "line" // line|dashline
		//, "dash", (L/5) //0.05]
		//, "space", 0.05
		//, "scale", 0
		//, "scalefaces", "all1" 
		//, "othoToAxis", false
		, "projection", false //"xy"|"yz"|"xy,yz"|"xy,yz,xz"| ...
		//, "quickgrid", true
		, "mark90", "auto"
		, "highlightaxes", true
		, "mode", 2   // 0: none
					// 1: line // pt:p_xy, p_xy:Origin
					// 2: plane and line //pt:p_xy, p_xy:x, p_xy:y
					// 3: block
			 
		], ops] );
	function ops(k)= hash(ops,k);
	//echo(ops);
	r= ops("r");
	mode = ops("mode")==true?2: ops("mode");
	style= ops("style");
	mark90= ops("mark90");
	
	xcolor = "red";
	ycolor = "lightgreen";
	zcolor = "blue";
		
	p_xy = [pt.x, pt.y, 0   ]; //pxy0(pt);
	p_yz = [   0, pt.y, pt.z]; //p0yz(pt);
	p_xz = [pt.x,    0, pt.z]; //px0z(pt);
	p_x  = [pt.x,    0, 0   ]; //px00(pt);
	p_y  = [   0, pt.y, 0   ]; //p0y0(pt);
	p_z  = [   0,    0, pt.z]; //p00z(pt);

	module line ( pq, lineops=false ) { 
		Line0( pq, update( ops, lineops) );
	}

	module chain ( pts, chainops=false) { 
		Chain( pts, update(ops,chainops) );
	}
	
	module projline (face="xy",pt2plane=true, projops=false){ 
		// Draw a L-shape line
		//echo("in projline, face= ", face);
		pts=[pt, face=="xy"?p_xy:face=="xz"?p_xz:p_yz, ORIGIN];
		if(pt2plane)
		{	chain( pts, update(ops, ["closed",false], projops) ); }
		else 
		{	line( pts, update(ops, projops) ); }
		
		if( mark90 || mark90=="auto")
		Mark90( pts, updates(ops, projops));
		
	}

	//
	// mark90
	//
	if( mark90=="auto" ){
		if( mode==1 ) Mark90( [ORIGIN, p_xy, pt ], ops );
	}
	else if( mark90 ){
		//Mark90( [ORIGIN, p_xy, pt ], mark90); //update(ops, mark90) );
		Mark90( [ORIGIN, p_xy, pt ], update(ops, mark90) );
	}
	
	// 
	// grid mode
	//
	// echo("in PtGrid, mode = ", mode );

	if(mode==1) { projline("xy"); //chain( [pt, p_xy, ORIGIN] );	

	} else 

	if(mode==2) { chain( [pt, p_xy, p_x] ,["closed",false]);
				line( [p_xy, p_y] );			
	} else
	
	if(mode==3) {
				chain([ p_yz, p_z, p_xz, p_x, p_xy
					  , p_y, p_yz, pt, p_xy ],["closed",false]);
				line( [pt, p_xz]);
	}

	//
	// highlight axes
	//
	hlops = ops("highlightaxes"); 
	hlops = hash( ops, "highlightaxes"
			   ,	if_v  =true
			   , then_v=["r", r]  // <=== default axes setting
			   );	
	if( hlops )
	//  assign( axesops= update( ops, hlops ))
	{
        axesops= update( ops, hlops );
		//echo("hlops", hlops);
		//echo("axesops", axesops);
		color( hash(hlops, "color", xcolor), hash(hlops, "transp"))
		Line0( [ORIGIN, p_x], axesops );
		color( hash(hlops, "color", ycolor), hash(hlops, "transp"))
		Line0( [ORIGIN, p_y], axesops );
		color( hash(hlops, "color", zcolor), hash(hlops, "transp"))
		Line0( [ORIGIN, p_z], axesops );
	}

	//
	// projection = "xy"|"yz"|"xy,yz"|"xy,yz,xz"| ... 
	//
	proj  = ops("projection");
	//echo(proj);
	if(proj)
//	assign( faces = proj==true?["xy"]    // if ==true, set to "xy"
//				:proj?split(proj,",") // else, split them
//					:false
//	)
    {  
    faces = proj==true?["xy"] // if ==true, set to "xy"
			:proj?split(proj,",") // else, split them
					:false;
        
	//echo(faces);
	for( face=faces ){ 
		//echo( "-- face = ", face);
		if(!(mode==1 && face=="xy")) // projline drawn automatically
			projline(face); }       // when (mode==1 && face=="xy")
	}
	//if (proj=="xy" && mode!=1) { projline("xy");} 
	//else if (proj=="yz") {Line0( [ORIGIN, p_yz], ops );}
	//else if (proj=="xz") {Line0( [ORIGIN, p_xz], ops );}
	


	/*
	if ( ops("quickgrid")) {
	
		color(ops("color")){
			DashLine( [ pt, p_xy ], ops );
			//DashLine( [ p_x, p_xy], ops );
			//DashLine( [ p_y, p_xy], ops );
			//DashLine( [ pt, p_z  ], ops );
			DashLine( [ p_xy, ORIGIN  ], ops );
		}	
	}
	else assign(

		scale     = ops("scale")
		, scalefaces= ops("scalefaces")
		, xscount = abs(pt.x<0?ceil( pt.x/ops("scale") ):floor( pt.x/ops("scale") ))
		, yscount = abs(pt.y<0?ceil( pt.y/ops("scale") ):floor( pt.y/ops("scale") ))
		, zscount = abs(pt.z<0?ceil( pt.z/ops("scale") ):floor( pt.z/ops("scale") ))

	){ 
	echo(" axes_ops = ", axes_ops );
	// Lines parallel to X
	color( xcolor ){
		DashLine( [[pt[0],pt[1],0 ],[0,pt[1],0 ]], ops );	// xy0, 0y0
		DashLine( [[pt[0],0,pt[2] ], [0,0,pt[2]]], ops ); // x0z, 00z
		DashLine( [pt, [0, pt[1],pt[2]]], ops); 			// xyz, 0yz
		}

	// Lines parallel to Y
	color( ycolor ){
		DashLine( [[pt[0],pt[1],0 ],[pt[0],0,0 ]], ops ); // xy0, x00
		DashLine( [pt, [pt[0],0,pt[2] ]], ops); 			// xyz, x0z
		DashLine( [[0, pt[1],pt[2]], [0,0,pt[2]]],ops );	// 0yz, 00z
		}

	// Lines parallel to Z
	color( zcolor ){	
		DashLine( [pt, [pt[0],pt[1],0 ]], ops);            // xy0, xyz
		DashLine( [[pt[0],0,pt[2] ], [pt[0],0,0] ], ops ); // x0z, x00
		DashLine( [[0, pt[1],pt[2]], [0,pt[1],0]], ops );  // 0yz, 0y0
		}
	}	
	*/
	
	//*
	//Lines along axes
	// axes_ops  

	
	 /*
	[ "r",    4*r
				, "dash",  3*ops("dash")
				, "space", 2*ops("space")
				];
	*/

	/*

	color( xcolor )
	if(pt[0]>0){
		Line0( [ ORIGIN,[pt[0],0, 0] ], update(axes_ops,["r",4*r]) );
	}else{
		DashLine( [ ORIGIN,[pt[0],0, 0] ], axes_ops);
	}

	color( ycolor )
	if(pt[1]>0){
		//MarkPts([[0,pt[1],0]]);
		Line0( [ ORIGIN,[0,pt[1],0] ], update(axes_ops,["r",4*r]) );
	}else{
		DashLine( [ ORIGIN,[0,pt[1],0] ], axes_ops);
	}

	color( zcolor )
	if(pt[2]>0){
		Line0( [ ORIGIN,[0,0,pt[2]] ], update(axes_ops,["r",4*r]) );
	}else{
		DashLine( [ ORIGIN,[0,0,pt[2]] ], axes_ops);
	}

	//Lines JL to axes 
	if(ops("othoToAxis")){
		DashLine( [pt, [pt[0],0,0 ]], ops );
		DashLine( [pt, [0, pt[1],0]], ops );
		DashLine( [pt, [0,0,pt[2] ]], ops );
	}

	//Projection lines onto surfaces
	if(ops("projection")){
	  color("gray"){
	  	DashLine( [ ORIGIN, [pt[0],pt[1],0 ]], ops );
	  	DashLine( [ ORIGIN, [pt[0],0,pt[2] ]], ops );
	  	DashLine( [ ORIGIN, [0,pt[1],pt[2] ]], ops );
		}	
	} 

	// Internal Grid lines

	scale = ops("scale");
	scalefaces = ops("scalefaces");
	
	xscount = abs(pt.x<0?ceil( pt.x/scale ):floor( pt.x/scale ));
	yscount = abs(pt.y<0?ceil( pt.y/scale ):floor( pt.y/scale ));
	zscount = abs(pt.z<0?ceil( pt.z/scale ):floor( pt.z/scale ));

	if( scale && hasAny(scalefaces, ["xy0","all0"])){
	  if(xscount>0){
		for( i = [1: xscount]){
			color( ycolor)
			DashLine([[ i*scale*sign(pt.x), pt.y, 0 ]
					   ,[ i*scale*sign(pt.x),    0, 0 ]],ops );
		}
	  }
	  if(yscount>0){
		for( i = [1: yscount]){
			color( xcolor )
			DashLine( [[pt.x, i*scale*sign(pt.y), 0 ]
					  ,  [   0, i*scale*sign(pt.y), 0 ]],ops );			
		}	
	  }
	}

	if( scale && hasAny(scalefaces, ["xy1", "all1"]) ){
	  if(xscount>0){
		for( i = [1: xscount]){
			color( ycolor)
			DashLine([[ i*scale*sign(pt.x), pt.y, pt.z ]
					   ,[ i*scale*sign(pt.x),    0, pt.z ]],ops );
		}
	  }
	  if(yscount>0){
		for( i = [1: yscount]){
			color( xcolor )
			DashLine( [[pt.x, i*scale*sign(pt.y), pt.z ]
					  ,  [   0, i*scale*sign(pt.y), pt.z ]],ops );			
		}	
	  }
	}	

	if( scale && hasAny(scalefaces, ["xz0", "all0"])){
	  if(xscount>0){
		for( i = [1: xscount]){
			color( zcolor)
			DashLine([[ i*scale*sign(pt.x), 0, pt.z ]
					   ,[ i*scale*sign(pt.x),    0, 0 ]],ops );
		}
	  }
	  if(zscount>0){
		for( i = [1: zscount]){
			color( xcolor )
			DashLine( [[pt.x, 0,i*scale*sign(pt.z) ]
					  ,[   0, 0, i*scale*sign(pt.z) ]],ops );			
		}	
	  }
	}


	if( scale && hasAny( scalefaces, ["xz1", "all1"] )){
	  if(xscount>0){
		for( i = [1: xscount]){
			color( zcolor)
			DashLine([[ i*scale*sign(pt.x), pt.y, pt.z ]
					   ,[ i*scale*sign(pt.x), pt.y, 0 ]],ops );
		}
	  }
	  if(zscount>0){
		for( i = [1: zscount]){
			color( xcolor )
			DashLine( [[pt.x, pt.y, i*scale*sign(pt.z) ]
					  ,[   0, pt.y, i*scale*sign(pt.z) ]],ops );			
		}	
	  }
	}	


	if( scale && hasAny(scalefaces, ["yz0", "all0"])){
	  if(yscount>0){
		for( i = [1: yscount]){
			color( zcolor)
			DashLine([[ 0, i*scale*sign(pt.y),  pt.z ]
					 ,[ 0, i*scale*sign(pt.y),     0 ]],ops );
		}
	  }
	  if(zscount>0){
		for( i = [1: zscount]){
			color( ycolor )
			DashLine( [[0, pt.y, i*scale*sign(pt.z) ]
					  ,[   0, 0, i*scale*sign(pt.z) ]],ops );			
		}	
	  }
	}


	if( scale && hasAny(scalefaces, ["yz1", "all1"] )){
	  if(yscount>0){
		for( i = [1: yscount]){
			color( zcolor)
			DashLine([[ pt.x, i*scale*sign(pt.y), 0 ]
					 ,[ pt.x, i*scale*sign(pt.y), pt.z ]],ops );
		}
	  }
	  if(zscount>0){
		for( i = [1: zscount]){
			color( ycolor )
			DashLine( [[pt.x, pt.y, i*scale*sign(pt.z) ]
					  ,[pt.x, 0, i*scale*sign(pt.z) ]],ops );			
		}	
	  }
	}	
	*/
}


module PtGrid_demo_1_mode()
{
	pts = randPts(3);
	MarkPts( pts );
	PtGrid( pts[0], ["mode",1] );
	PtGrid( pts[1], ["mode",2] );
	PtGrid( pts[2], ["mode",3] ); // Note that mark90 is disabled automatically
}

module PtGrid_demo_2_mark90()
{
	pts = randPts(3);
	MarkPts( pts );
	PtGrid( pts[0] );
	PtGrid( pts[1], ["mark90",true] );
	PtGrid( pts[2], ["mark90",["color","red", "r",0.05] ] );
}


module PtGrid_demo_3_projection()
{
	pts = randPts(4);
	PtGrid( pts[0], ["mode",1] );
	PtGrid( pts[1], ["mode",1, "projection","xz"] );
	PtGrid( pts[2], ["mode",1, "projection","xz,yz"] );
	PtGrid( pts[3], ["mode",1, "projection","xz,yz", "mark90", false] );
	MarkPts( pts, ["transp",0.5] );
}

module PtGrid_demo_4_highlightaxes()
{
	pts = randPts(3);
	PtGrid( pts[0]);
	PtGrid( pts[1], ["highlightaxes",false] );
	PtGrid( pts[2], ["highlightaxes", ["r",0.08, "transp", 0.1, "color", "purple" ]] );
	MarkPts( pts, ["transp",0.5] );
}


module PtGrid_demo()
{
	//PtGrid_demo_1_mode();
	//PtGrid_demo_2_mark90();
	//PtGrid_demo_3_projection();
	PtGrid_demo_4_highlightaxes();
}
module PtGrid_test( ops ){ doctest(PtGrid,ops=ops);}

//PtGrid_demo();
//PtGrid_test();
//doc(PtGrid);



//========================================================
PtGrids=["PtGrids", "pts,ops", "n/a", "3D, Point, Line, Geometry",
"
 Given a series of points, draw grid based on ops.
;; Refer to PtGrid (for a single pt) for ops definition.
"];

module PtGrids(pts, ops)
{
	for(i=[0:len(pts)-1]){
		//echo_("In PtGrids, i={_}, pts[i]={_}",[i,pts[i]] );
		PtGrid( pts[i], ops );
	}
}

module PtGrids_demo()
{
	pts = randPts(3);
	PtGrids(pts, ["mode",3]);
	MarkPts(pts, ["transp",0.3]);
}

module PtGrids_test( ops ) { doctest( PtGrids, ops=ops ); }
//PtGrids_demo();
//PtGrids_test();
//doc(PtGrids);


//========================================================
quadrant=["quadrant", "pqr,s", "int", "Integer, Math, Geometry",
 " Given a 3-pointer (pqr) and a point, return the quadrant of S on the 
;; yz plane in a coordinate system where P is on the positive X and Q 
;; is the origin. 
;;
;;       
;;       6
;;  +----N-----+
;;  : 2  |  1  :
;;  :    |     :
;; 7-----P-----R- 5
;;  :    :     :
;;  : 3  :  4  :
;;  +----:-----+ 
;;       8                  R
;;                        /|  
;;                       / |
;;                      /  |
;;                     /   |
;;                    /    |
;;        S.---------/--T  |
;;        |:        /   |  |   _.'
;;        | :      /    |  :.-'
;;        U.-:----/-----+-'
;;          '.:  /  _.-'
;;            ':/.-'
;;   N---------Q---------
;;       _.-' 
;;    P-'
;;
;; quadrant(pqr,S) => 2
;;
"];

function quadrant(pqr,s)=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, N = N(pqr)
	, dST = distPtPl( s, pqr )
	, dSU = distPtPl( s, [N,Q,P] )
	, signs = [ -sign( dST ), sign( dSU )]
	)
(
	 hash( [[1,1],1
			,[-1,1],2
			,[-1,-1],3
			,[1,-1],4
			,[0,0],0
			,[0,1], 6
			,[-1,0], 7
			,[0,-1], 8
			,[1,0],  5
			], signs )
);
 
module quadrant_test( ops=["mode",13] )
{
	doctest( quadrant,
	ops=ops
	);
}

module quadrant_demo()
{

	pqr = randPts(3); S=randPt();
	P=P(pqr); Q=Q(pqr); R=R(pqr);
	N=N(pqr); Z=N( [N,Q,P] );


	T = projPt( S, pqr );
	U = projPt( S, [N,Q,P] );

	Chain( [Q,T,S ], ["r",0.02] ); Mark90( [Q,T,S] );
	Chain( [Q,U,S ], ["r",0.02] ); Mark90( [Q,U,S] );
	Line0( [S,Q] );

	Chain( pqr );
	MarkPts( [P,Q,R,N,S,Z,T,U], ["labels", "PQRNSZTU"] );	

	color(false, 0.2){

	Plane( pqr, ["mode",4] );
	Plane( [P,Q,N], ["mode",4] );
	Plane( [P,Q,N], ["mode",4] );
	Plane( [P,Q,N], ["mode",4] );

	}

	echo(quadrant(pqr,S)) ;
}

//quadrant_demo();


//========================================================
rand=["rand", "m=1, n=false", "Any", "Random, Number, Array, String",
" Given a number (m), return a random number between 0~m.
;; If another number (n) is given, return a random number between m~n.
;; If m is an array or string, return one of its member randomly.
"
];
function rand(m=1, n=false)=
(
	isarr( m )||isstr(m) ? m[ floor(rand(len(m))) ]
	: (isnum(n)
		? rands( min(m,n), max(m,n), 1)
		: rands( 0, m, 1)
	  )[0]
);

module rand_test( ops=["mode",13] )
{
	arr=["a","b","c"];
	doctest( rand,
	[
		"// rand(a)"
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,"//rand(a,b)"
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,"//rand(arr)"
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,"// rand(len(arr))=> float"
		,"// rand(range(arr))=> integer = floor(rand(len(arr)))"
		,str(">> rand(range(arr)):", rand(range(arr)))
		,str(">> rand(range(arr)):", rand(range(arr)))
		,str(">> rand(range(arr)):", rand(range(arr)))

	], ops, ["arr",arr]
	);
}
//module 


//========================================================
randc=["randc", "r=[0,1],g=[0,1],b=[0,1]", "arr", "Core" ,
"
 Given r,g,b (all have default [0,1], which is the max range),
;; return an array with random values for color. Examples:
;;
;; >> color( randc() )  sphere(r=1);
;; >> darker = [0, .4];
;; >> color( randc( r=darker, g=darker ) )  sphere(r=1);
"];
 
function randc(r=[0,1],g=[0,1],b=[0,1])=
(
	[ rands(r[0],r[1],1)[0]
	, rands(g[0],g[1],1)[0]
	, rands(b[0],b[1],1)[0] ]	
);
module randc_test( ops ){ doctest(randc, [], ops=ops);}
module randc_demo()
{
	light = [0.8,1];
	dark  = [0,0.4];
	translate(randPt()) color(randc(r=dark,g=dark,b=dark)) sphere(r=0.5);
	translate(randPt()) color(randc()) sphere(r=1);
	translate(randPt()) color(randc(r=light, g=light,b=light)) sphere(r=1.5);
}
//randc_test( ["mode",22] );
//randc_demo();
//doc(randc);





//========================================================
randConvexPts=[[]];
/*
convex polygons – those with the property that 
given any two points inside the polygon, the entire line segment connecting those two 
points must lie inside the polygon. Such polygons are the intersection of half-planes, 
which means that if you extend the boundary edges into infinite lines the polygon will 
always lie completely on one side of each edge line. If a point is on the same side of each 
edge as the polygon, then it must lie inside the polygon.

http://www.cs.oberlin.edu/~bob/cs357.08/VectorGeometry/VectorGeometry.pdf
*/
function randOnPlaneConvexPts( count=4, pqr=randPts(3)  )=
(
3
 // THIS IS HARD	

);




//========================================================
randPt=["randPt", "r=false,x,y,z", "pt", "Geometry, Point, 3D",  
"
 Given a common range (r, default=false) or ranges x,y,z (each
;; default to [-3,3]), return a random pt that is in the range
;; defined by x,y,z, or by [-r,r] if r is given. Refer torandPts
;; that generates multiple random points.
"];

function randPt(r=false, x=[-3,3],y=[-3,3],z=[-3,3])=
(
 randPts(1,r=r,x=x,y=y,z=z)[0]
);

module randPt_test(ops){ doctest( randPt, ops=ops ); }

module randPt_demo()
{
	MarkPts( [randPt(1)
			,randPt(3)
			,randPt(x=[4,6],y=[1,3],z=[2,4])
			]
			, ["grid",true] );
}
//randPt_test( ["mode", 22] );
//randPt_demo();
//doc(randPt);





//========================================================
randPts=["randPts", "count,r,x,y,z", "pts", "Geometry, Point, 3D",
"
 Given a count of pts and a common range (r, default=false) or ranges
;; x,y,z (each default to [-3,3]), return a random pts that are in the
;; range defined by x,y,z, or by [-r,r] if r is given.
"];
/*
	,[ "Return an array of random points (len=count, default= 3). "
	 , "The x,y,z, each default to [-3,3], define the range. The 2nd "
	 , "argument, r, (default=false), replaces x,y,z settings with "
	 , "[-i,i] if set to an integer i. Refer to randPt(r,x,y,z) for "
	 , "a single random pt."]
	];
*/
function randPts(count=3, r=false, x=[-3,3],y=[-3,3],z=[-3,3])=
(
  count==0? []
	:concat( [[ x==0?0:(rands(isnum(r)?(-r):x[0],isnum(r)?r:x[1],1)[0])
 			 , y==0?0:(rands(isnum(r)?(-r):y[0],isnum(r)?r:y[1],1)[0])
 			 , z==0?0:(rands(isnum(r)?(-r):z[0],isnum(r)?r:z[1],1)[0])
 			 ]], randPts(count-1, r=r, x=x,y=y,z=z)
		  )
);

module randPts_test(ops){ doctest( randPts, ops=ops ); }

module randPts_demo()
{
	pts = randPts(3);
	for (i=[0:len(pts)-1]){ echo( pts[i] ); 
	}
	MarkPts( pts, ["grid",true]);
	
}
//randPts_demo();
//randPts_test();
//doc(randPts);




//========================================================
//randCirclingPts=[[]];
//function randCirclingPts(count=4, r=undef)
//(
//3
//
//);

//========================================================
randInPlanePt=[[]];
function randInPlanePt(pqr)=
(
	[onlinePt( 
			[pqr[0], onlinePt([pqr[1],pqr[2]], ratio=rand())]
		   , ratio=rand()
		)
	,onlinePt( 
			[pqr[1], onlinePt([pqr[2],pqr[0]], ratio=rand())]
		   , ratio=rand()
		)
	,onlinePt( 
			[pqr[2], onlinePt([pqr[0],pqr[1]], ratio=rand())]
		   , ratio=rand()
		)
	][ floor(rand(3))]
);

module randInPlanePt_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	S = randInPlanePt(pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	LabelPts( concat(pqr,[S]) , "PQRS");
	MarkPts( [P,Q,R,S] , ["grid",false]);	
	
}
//randInPlanePt_demo();



//========================================================
randInPlanePts=[[]];
function randInPlanePts(pqr, count=2)=
(
   count>0? concat( [randInPlanePt( pqr )]	
                  , randInPlanePts( pqr, count-1)
                  ) :[]
);

module randInPlanePts_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randInPlanePts(pqr, 4); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	LabelPts( ps );
	MarkPts( concat( pqr, ps));	
}
module randInPlanePts_demo_2()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randInPlanePts(pqr, 100); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps) { MarkPts([p], ["r",0.08]); }
}

//randInPlanePts_demo();
//randInPlanePts_demo_2();


////========================================================
//randOneFacePts=[[]];
//function randOneFacePts( count=5, pqr = randPts(3) )=
//(
//	// average angle = 360 / 5
//3	
//	
//
//
//);

//========================================================
randOnPlanePt=[[]];
//
// optional r : distance from pqr's incenterPt. Can be: a number, an array [a,b] for range
// optional a : random range of aXQP (X = returned Pt). Default 360. Can be [a1,a2] for range]  
//
function randOnPlanePt(pqr=randPts(3), r=undef, a=360)=
(
//  onlinePt( [pqr[floor(rand(3))], randInPlanePt(pqr)], ratio= rand(3) )
   anglePt( pqr
//		 [  pqr[0], incenterPt(pqr)
//		   ,pqr[1]
//		   ]
		  , a = isnum(a)?(rand(1)*a)
					: rands( a[0], a[1],1)[0]
			,r = r==undef? max( d01(pqr), d02(pqr), d12(pqr) ) * rand(1)
				:isarr(r)? rands( r[0], r[1],1)[0]
				:r
		)	
);

module randOnPlanePt_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	S = randOnPlanePt(pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	pqrs = concat(pqr,[S]);
	LabelPts( pqrs , "PQRS");
	MarkPts( pqrs , ["grid",false]);	
	Chain( pqrs, ["r", 0.08, "color", "red", "transp",0.1]);
	
}
//randOnPlanePt_demo();







//========================================================
randOnPlanePts=[[]];
function randOnPlanePts(count=2, pqr=randPts(3), r=undef, a=360 )=
(
   count>0? concat( [randOnPlanePt( r=r, pqr, a=a )]	
                  , randOnPlanePts( count-1, pqr, r=r,a=a )
                  ) :[]
);

module randOnPlanePts_demo()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randOnPlanePts(4, pqr=pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	LabelPts( ps );
	MarkPts( pqr);
	MarkPts(ps, ["r",0.2, "transp", 0.4]);	
}
module randOnPlanePts_demo_2()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	MarkPts( [incenterPt(pqr)], ["r",.6, "color", "blue"] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randOnPlanePts(100, pqr=pqr); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps) { MarkPts([p], ["r",0.05]); }
}

module randOnPlanePts_demo_3_stars()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	MarkPts( [Q], ["r",.6, "colors", ["blue"], "transp",0.3] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps = randOnPlanePts(100, pqr, r=[2.2, 3]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps) { MarkPts([p], ["r",0.05, "colors", [randc()]]); }
}


module randOnPlanePts_demo_4_angle()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];

	Chain( pqr, ["r", 0.02]);

	//MarkPts( [incenterPt(pqr)], ["r",.6, "color", "blue"] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	ps30 = randOnPlanePts(30, pqr, a=30); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	ps4560 = randOnPlanePts(30, pqr, a=[75, 90]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	ps = randOnPlanePts(30, pqr, a=[100, 120], r=[2.3, 3]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
	
	LabelPts( pqr , "PQR");
	//LabelPts( ps );
	//MarkPts(  ps);	
	for(p=ps30) { MarkPts([p], ["r",0.05, "color", ["red"]]); }
	for(p=ps4560) { MarkPts([p], ["r",0.05, "colors", ["green"]]); }
	for(p=ps) { MarkPts([p], ["r",0.05, "colors", ["blue"]]); }

}

module randOnPlanePts_demo_5_angle()
{
	pqr = randPts(3);
	P= pqr[0];
	Q= pqr[1];
	R= pqr[2];
	MarkPts( pqr, ["labels","PQR"] );

	//pt2 = anglePt( pqr, a=90, r=1); randOnPlanePts(2, pqr, a=[90,90] );
	P2 = normalPt( pqr );
	R2 = normalPt( [P,Q,P2] ); //anglePt( pqr, a=90, r=1);

//	pqr2 =  [ pt2[0], pqr[1], pt2[1] ] ;


//	P2= pqr[0];
	//Q= pqr[1];
//	R2= pqr[2];
	//echo( P2, Q, R2 );

	pqr2 = [ P2, Q, R2 ];

	Chain( pqr, ["r", 0.02]);

	Chain( pqr2, ["r", 0.02, "color", "green"]);


	pts = randOnPlanePts( 20, pqr );
	//MarkPts( pts, ["r",0.05,"samesize",true,"colors",["red"]] );

	pts2 = randOnPlanePts(100, pqr2);
	//echo(pts2);
	MarkPts( pts2, ["r",0.05,"samesize",true,"colors",["green"]] );
	

	//MarkPts( [incenterPt(pqr)], ["r",.6, "color", "blue"] );
	//pOnQR = onlinePt([Q,R], ratio=rands(0,1,1)[0]); 

	//ps30 = randOnPlanePts(30, pqr, a=30); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
//	ps4560 = randOnPlanePts(30, pqr, a=[75, 90]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
//	ps = randOnPlanePts(30, pqr, a=[100, 120], r=[2.3, 3]); //onlinePt( [P, pOnQR], ratio=rands(0,1,1)[0]); 
//	
//	LabelPts( pqr , "PQR");
//	//LabelPts( ps );
//	//MarkPts(  ps);	
//	for(p=ps30) { MarkPts([p], ["r",0.05, "color", ["red"]]); }
//	for(p=ps4560) { MarkPts([p], ["r",0.05, "colors", ["green"]]); }
//	for(p=ps) { MarkPts([p], ["r",0.05, "colors", ["blue"]]); }

}


//randOnPlanePts_demo();
//randOnPlanePts_demo_2();
//randOnPlanePts_demo_3_stars();
//randOnPlanePts_demo_4_angle();
//randOnPlanePts_demo_5_angle();




//========================================================
randRightAnglePts=[[],
"
 r: first arm length (default: rand(5) )
 dist: dist from ORIGIN. It could be a point (indicating translocation) or integer (default: rand(3))
	 If it is an integer, will draw at range inside [dist,dist,dist] location, and the Q will always be the one
	 closest to ORIGIN.	
 r2: 2nd arm length. If not given (default), auto pick a random value
	between 0~r

 randRightAnglePts( r=3, r2=3) gives points of a *special right triangle*
 in which 3 angles are 90, 45, 45.

"];

function randRightAnglePts( 
    r=rand(5), 
    dist=randPt(), 
    r2=-1
    )=
( 
    RMs( [ rand(360), rand(360), rand(360)] 
	   , //shuffle(
			[
			 [      r,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
			,[      0,       0, 0]+(isnum(dist)?[dist,dist,dist]:dist)
			,[      0,r2>0?r2:rand(r), 0]+(isnum(dist)?[dist,dist,dist]:dist) 
		 	]
		//)
	   ) 
); 

module randRightAnglePts_demo()
{
	echom("randRightAnglePts");
	as = [ rand(360), rand(360), rand(360)];
	rm = RM(as);
	
	//echo( "as = ", as);
	//echo( "rm = ", rm);

	pqr = randRightAnglePts();
	echo( "pqr", pqr );

	Chain( pqr, ["r",0.02] );
	MarkPts( pqr,["grid",true] );
	Mark90 (pqr );
	LabelPts (pqr, "PQR" );
	ColorAxes();
}

//randRightAnglePts_demo();



//========================================================
range=["range", "i,j,k,cycle", "array", "Core",

 " Given i, optional j,k, return a range or ranges of ordered numbers 
;; without the end point. 
;;
;; (1) basic
;;
;;  range(3) => [0,1,2]   // from 0 to 3, inc by 1 (= for(n=[0:i-1] )
;;  range(3,6) => [3,4,5] // inc by 1  (= for(n=[i:j-1] )
;;  range(3,2,6)= > [3,5] // inc by 2  (= for(n=[i:j:k-1] )
;;  range(6,3)
;;  range(6,2,3)  
;;
;; (2) i= array or string
;;
;;   range([3,4,5]) => [0,1,2]
;;
;;   for(x=arr)        ... the items
;;   for(i=range(arr)) ... the indices 
;;
;;   range([3,4,5],isitem=true) => [3,4,5]
;;
;; (3) cycle= i,-i, true: cycling (see tests)
;;
;; (4) cover= [m,n] 
;;
;;   range(4, cover=[0,1],cycle=true )= [[0,1],[1,2],[2,3],[3,0]] 
;;   range(4, cover=[0,1],cycle=false)= [[0,1],[1,2],[2,3],[3]] 
;;
;; (6) obj, isitem -- return items but not indices
;;
;;   range( 3,cover=[1,1],isitem=true,obj=obj )
;;      = [[12, 10, 11], [10, 11, 12], [11, 12, 10]]
;;   range( obj,cover=[1,1],isitem=true )
;;      = [[13, 10, 11], [10, 11, 12], [11, 12, 13], [12, 13, 10]]
",
"subarr"
];


function range(i,j,k, cycle)=
let ( 
	 obj = isarr(i)||isstr(i)? i :undef
	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
	,beg= inum==1 
            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
    ,end= inum==1 
		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
		   : inum==2?j:k
		   
	,cycle=cycle==true?1:cycle

	,intv = sign(end-beg)        // when inum=3, the middle one is
			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	

	,residual= or( mod(abs(end-beg), abs(intv)), intv)

	,extraidx= abs(intv)>abs(beg-end) // End points to be skipped, 
	           ? 1                    // depending on the intv. If
	           : residual             // intv too large, set to 1
	,objrange= obj
			   ? range ( len(obj)
						, cycle=cycle ) 
			   : []
											 
)(
//	[beg, intv, end ]
//    [ for(i=[beg:intv:end-extraidx]) i	]

	( intv==0 || beg==end || (beg==0&&end==undef) || obj==[] || obj=="" )
	? []
	: obj  // Allowing range(arr) or range(str)
	  ? objrange

	  // Non obj : =================

	  : cycle>0
		  ? concat( 
				[ for(i=[beg:intv:end-extraidx]) i ]
				,[ for(i=[beg:intv:beg+(cycle-1)*intv]) i]
				)
		  : cycle<0
			?  concat( 
				[ for(i=[end+cycle*intv:intv:end-intv]) i]
				,[ for(i=[beg:intv:end-extraidx]) i ]
				)
		 	:[ for(i=[beg:intv:end-extraidx]) i	]
		  
);

module range_test( ops )
{
	obj = [10,11,12,13];
	doctest( range
	,[
		""
		,"// 1 arg: starting from 0"
		,["5", range(5), [0,1,2,3,4]]
		,["-5", range(-5), [0, -1, -2, -3, -4] ]
//		,["0", range(0), [] ]
//		,["[]", range([]), [] ]
		,""
		,"// 2 args: "
		,["2,5", range(2,5), [2,3,4]]
		,["5,2", range(5,2), [5,4,3]]
		,["-2,1", range(-2,1), [-2,-1,0]]
		,["-5,-1", range(-5,-1), [-5,,-4,-3,-2]]

		,"", "// Note these are the same:"
		,["3", range(3), [0,1,2]]
		,["0,3", range(0,3), [0,1,2]]
		,["-3", range(-3), [0,-1,-2]]
		,["0,-3", range(0,-3), [0,-1,-2]]
		,["1", range(1),[0] ]
		,["-1", range(-1),[0] ]
		

		,""
		,"// 3 args, the middle one is interval. Its sign has no effect"
		,["2,0.5,5", range(2,0.5, 5), [2,2.5,3,3.5, 4, 4.5]]
		,["2,-0.5,5", range(2, -0.5, 5), [2,2.5,3,3.5, 4, 4.5]]
		,["5,-1,0", range(5,-1,0), [5,4,3,2,1]]
		,["5,1,2", range(5,1,2), [5,4,3]]
		,["8,2,0", range(8,2,0),[8,6,4,2]]
		,["0,2,8", range(0,2,8),[0, 2, 4, 6]]			
		,["0,3,8", range(0,3,8),[0, 3, 6]]
		
		,""
		,"// Extreme cases: "
		
		,["", range(), [], ["rem", "Give nothing, get nothing"]]
		,["0", range(0),[], ["rem", "Count from 0 to 0 gets you nothing"] ]
		,["2,2", range(2,2),[], ["rem", "2 to 2 gets you nothing, either"] ]
		,["2,0,4", range(2,0,4),[], ["rem", "No intv gets you nothing, too"] ]
		,["0,1", range(0,1),[0] ]
		,["0,1,1", range(0,1,1),[0] ]

		,"// When interval > range, count only the first: "
		,["2,5,4", range(2,5,4),[2] ]
		,["2,5,-1", range(2,5,-1),[2] ]
		,["-2,5,-4", range(-2,5,-4),[-2] ]
		,["-2,5,1", range(-2,5,1),[-2] ]

		,""
		," range( <b>obj</b> )"
		,""
		,"// range( arr ) or range( str ) to return a range of an array"
		,"// so you don't have to do : range( len(arr) )"
		,""
		,str("> obj = ",obj)
		,""
		,["obj",range(obj),[0,1,2,3]] 
		,["[3,4,5]", range([3,4,5]), [0,1,2] ]
		,["''345''", range("345"), [0,1,2] ]
		,["[]", range([]), [] ]
		,["''''", range(""), [] ]

		,""
		," range( ... <b>cycle= i,-i,true</b>...)"
		,""
		,"// New 2014.9.7: "
		,"// cycle= +i: extend the index on the right by +i count "
		,"// cycle= -i: extend the index on the right by -i count"
		,"// cycle= true: same as cycle=1 "
		,"" 
		,["4,cycle=1", range(4,cycle=1), [0,1,2,3,0], ["rem","Add first to the right"] ]
		,["4,cycle=-1", range(4,cycle=-1), [3,0,1,2,3], ["rem","Add last to the left"]]
		,["4,cycle=true", range(4,cycle=true), [0,1,2,3,0],["rem","true=1"] ]
		,""

		,["2,5,cycle=1", range(2,5,cycle=1), [2,3,4,2], ["rem","Add first to the right"] ]
		,["2,5,cycle=2", range(2,5,cycle=2), [2,3,4,2,3], ["rem","Add 1st,2nd to the right"] ]
		,["2,5,cycle=-1", range(2,5,cycle=-1), [4,2,3,4],["rem","Add last to the left"] ]
		,["2,5,cycle=-2", range(2,5,cycle=-2), [3,4,2,3,4] ]
		,["2,5,cycle=true", range(2,5,cycle=true), [2,3,4,2], ["rem","true=1"] ]
		

		,""
		,["2,1.5,8,cycle=2", range(2,1.5,8,cycle=2), [2, 3.5, 5, 6.5, 2, 3.5] ]
		,["2,1.5,8,cycle=-2", range(2,1.5,8,cycle=-2), [5, 6.5, 2, 3.5, 5, 6.5] ]
		,["2,1.5,8,cycle=true", range(2,1.5,8,cycle=true), [2, 3.5, 5, 6.5, 2] ]
		
		,""
		," range(<b>obj</b>,<b>cycle=i,-1,true</b>...)"
		,""
		,str("> obj = ",obj)
		,["obj,cycle=1",   range(obj,cycle=1)	,[0,1,2,3,0]] 
		,["obj,cycle=2",   range(obj,cycle=2)	,[0,1,2,3,0,1]] 
		,["obj,cycle=-1",range(obj,cycle=-1)	,[3,0,1,2,3]] 
		,["obj,cycle=-2",range(obj,cycle=-2)	,[2,3,0,1,2,3]] 

	]
	, ops
	);
}


//========================================================
ranges=["ranges", "i,j,k,cover,cycle", "array", "Core",

 " Given i, optional j,k, return a range or ranges of ordered numbers 
;; without the end point. 
;;
;; (1) basic
;;
;;  range(3) => [0,1,2]   // from 0 to 3, inc by 1 (= for(n=[0:i-1] )
;;  range(3,6) => [3,4,5] // inc by 1  (= for(n=[i:j-1] )
;;  range(3,2,6)= > [3,5] // inc by 2  (= for(n=[i:j:k-1] )
;;  range(6,3)
;;  range(6,2,3)  
"];


function ranges(i,j,k, cover, cycle)=
let ( 
	 obj = isarr(i)||isstr(i)? i :undef
	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
	,beg= inum==1 
            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
    ,end= inum==1 
		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
		   : inum==2?j:k
		   
	,intv = sign(end-beg)        // when inum=3, the middle one is
			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	

	,xbeg =  cover?cover[0]:0    // extra beg
	,xend =  cover?cover[1]:0    // extra end

	,residual= or( mod(abs(end-beg), abs(intv)), intv)

	,extraidx= abs(intv)>abs(beg-end) // End points to be skipped, 
	           ? 1                    // depending on the intv. If
	           : residual             // intv too large, set to 1
	,objrange= obj
			   ? range ( len(obj)
						, cycle=cycle
						, cover=cover ) 
			   : []
											 
)(
	( intv==0 || beg==end || (beg==0&&end==undef) || obj==[] || obj=="" )
	? []
	: obj  // Allowing range(arr) or range(str)
	  ? ranges ( len(obj)
				, cover=cover
				, cycle=cycle
				)

	  // Non obj : =================

	  : cover 
		? [ for( i=[beg:intv:end-extraidx] )  
				[ for(jj=range( (i-xbeg<beg && !cycle) ? beg: i-xbeg 
							 , (i+xend>=end && !cycle) ? end: i+xend+1
							 )) 
				  cycle
				  ? jj< beg || jj>=end 
					? fidx(abs(beg-end),jj-abs(beg),cycle=true)+abs(beg)
					: jj
				  : jj		
				]
		  ]

		// no cover  ---------------
		: cycle
		  ? concat( [for(i=[beg:intv:end-extraidx])[i] ], [[beg]] )
		  :[ for(i=[beg:intv:end-extraidx]) [i]	]
	  
);


module ranges_test( ops )
{
	obj = [10,11,12,13];
	doctest( ranges
	,[
		""
		,"// 1 arg: starting from 0"
		,["5", ranges(5), [[0], [1], [2], [3], [4]] ]
		,["-5", ranges(-5), [[0], [-1], [-2], [-3], [-4]] ]
		,""
		,"// 2 args: "
		,["2,5", ranges(2,5), [[2], [3], [4]]]
		,["5,2", ranges(5,2), [[5], [4], [3]]]
		,["-2,1", ranges(-2,1), [[-2], [-1], [0]]]
		,["-5,-1", ranges(-5,-1), [[-5], [-4], [-3], [-2]]]

		,"", "// Note these are the same:"
		,["3", ranges(3), [[0], [1], [2]]]
		,["0,3", ranges(0,3), [[0], [1], [2]]]
		,["-3", ranges(-3), [[0],[-1],[-2]]]
		,["0,-3", ranges(0,-3), [[0],[-1],[-2]]]
		,["1", ranges(1),[[0]] ]
		,["-1", ranges(-1),[[0]] ]
		

		,""
		,"// 3 args, the middle one is interval. Its sign has no effect"
		,["2,0.5,5", ranges(2,0.5, 5), [[2], [2.5], [3], [3.5], [4], [4.5]]]
		,["2,-0.5,5", ranges(2, -0.5, 5), [[2], [2.5], [3], [3.5], [4], [4.5]]]
		,["5,-1,0", ranges(5,-1,0), [[5], [4], [3], [2], [1]]]
		,["5,1,2", ranges(5,1,2), [[5], [4], [3]]]
		,["8,2,0", ranges(8,2,0),[[8], [6], [4], [2]]]
		,["0,2,8", ranges(0,2,8),[[0], [2], [4], [6]]]			
		,["0,3,8", ranges(0,3,8),[[0], [3], [6]] ]
		
		,""
		,"// Extreme cases: "
		
		,["", ranges(), [], ["rem", "Give nothing, get nothing"]]
		,["0", ranges(0),[], ["rem", "Count from 0 to 0 gets you nothing"] ]
		,["2,2", ranges(2,2),[], ["rem", "2 to 2 gets you nothing, either"] ]
		,["2,0,4", ranges(2,0,4),[], ["rem", "No intv gets you nothing, too"] ]
		,["0,1", ranges(0,1),[[0]] ]
		,["0,1,1", ranges(0,1,1),[[0]] ]

		,"// When interval > range, count only the first: "
		,["2,5,4", ranges(2,5,4),[[2]] ]
		,["2,5,-1", ranges(2,5,-1),[[2]] ]
		,["-2,5,-4", ranges(-2,5,-4),[[-2]] ]
		,["-2,5,1", ranges(-2,5,1),[[-2]] ]

		,""
		," ranges( <b>obj</b> )"
		,""
		,"// ranges( arr ) or ranges( str ) to return a range of an array"
		,"// so you don't have to do : ranges( len(arr) )"
		,""
		,str("> obj = ",obj)
		,""
		,["obj",ranges(obj),[[0], [1], [2], [3]]] 
		,["[3,4,5]", ranges([3,4,5]), [[0], [1], [2]]]
		,["''345''", ranges("345"), [[0], [1], [2]] ]
		,["[]", ranges([]), [] ]
		,["''''", ranges(""), [] ]

		,""
		," ranges( ... <b>cycle= i,-i,true</b>...)"
		,""
		,"// cycle= true: extend the index on the right by +i count "
		,"" 
		,["4,cycle=true", ranges(4,cycle=true), [[0],[1],[2],[3],[0]], ["rem","Add first to the right"] ]
		,["2,5,cycle=true", ranges(2,5,cycle=true), [[2],[3],[4],[2]] ]
		,["2,1.5,8,cycle=true", ranges(2,1.5,8,cycle=true), [[2],[3.5],[5],[6.5],[2]] ]
		
		,""
		," ranges(<b>obj</b>,<b>cycle=true</b>...)"
		,""
		,str("> obj = ",obj)
		,["obj,cycle=true",   ranges(obj,cycle=true)	,[[0],[1],[2],[3],[0]]] 
	
		,""
		," ranges(... <b>cover</b>=[i,j] )"
		,""
		,"// cover=[1,2] means, instead of returning i, return"
		,"//[i-1,i,i+1,i+2]. Note that when cover is set, return array of arrays."
		,"// Also note that w/cover, cycle=+i/-i is treated as cycle=true."

		,["3", ranges(3), [[0],[1],[2]]]
		,["3,cover=[0,1]", ranges(3,cover=[0,1],cycle=false)
			, [[0, 1], [1, 2], [2]]]
		,["3,cover=[0,1],cycle=true", ranges(3,cover=[0,1],cycle=true)
			, [[0, 1], [1, 2], [2,0]]]
		
		,""
		,["3,cover=[1,0]", ranges(3,cover=[1,0])
			, [[0], [0, 1], [1, 2]]]
		,["3,cover=[1,0],cycle=true", ranges(3,cover=[1,0],cycle=true)
			, [[2,0], [0, 1], [1, 2]]]

		,""
		,["3,cover=[1,2]", ranges(3,cover=[1,2])
			, [[0, 1, 2], [0, 1, 2], [1, 2]], ["rem","This is tricky. Compare next."]]
		,["3,cover=[1,2],cycle=true", ranges(3,cover=[1,2],cycle=true)
			, [[2,0, 1, 2], [0, 1, 2, 0], [1, 2, 0, 1]]]
		,["3,cover=[0,3],cycle=true", ranges(3,cover=[0,3],cycle=true)
			, [[0, 1, 2, 0], [1, 2, 0, 1], [2,0,1,2]]]

		,""
		,["3,cover=[0,0]", ranges(3,cover=[0,0]), [[0], [1], [2]]]
		,""
		,["5,8", ranges(5,8), [[5], [6], [7]]]
		,["5,8,cover=[0,0]", ranges(5,8,cover=[0,0]), [[5], [6], [7]]]
		,["5,8,cover=[1,0]", ranges(5,8,cover=[1,0])
						, [[5], [5, 6], [6, 7]]]
		,["5,8,cover=[0,1]", ranges(5,8,cover=[0,1])
						, [[5, 6], [6, 7], [7]]]
		,["5,8,cover=[1,0],cycle=true", ranges(5,8,cover=[1,0],cycle=true)
						, [[7,5], [5, 6], [6, 7]]]
		,["5,8,cover=[0,2],cycle=true", ranges(5,8,cover=[0,2],cycle=true)
						, [[5, 6,7], [6, 7, 5], [7,5,6]]]

		,""
		,["0,2,8,cover=[0,1]", ranges(0,2,8,cover=[0,1])
							,[[0, 1], [2, 3], [4, 5], [6, 7]]
							,["rem","Not:[[0,2],[2,4],[4,6],[6]]"]
							]
		,["0,3,8,cover=[0,1]", ranges(0,3,8,cover=[0,1])
							,[[0, 1], [3, 4], [6, 7]]
							,["rem","Not:[[0,3],[3,6],[6]]"]
							]
		,["0,2,6,cover=[0,2],cycle=true", ranges(0,2,6,cover=[0,2],cycle=true)
				,[[0, 1, 2], [2, 3, 4], [4, 5, 0]]
				,["rem","Not [0,2,4],[2,4,0],[4,0,2]"] ]

		,""
		,["obj",ranges(obj),[[0],[1],[2],[3]]] 
		,["obj,cover=[1,1]",ranges(obj,cover=[1,1])
			,[[0,1],[0,1,2],[1,2,3],[2,3]] ]  
		,["obj,cover=[1,1], cycle=true",ranges(obj,cover=[1,1], cycle=true)
			,[[3,0,1],[0,1,2],[1,2,3],[2,3,0]] ]  

	]
	, ops
	);
}


function range_original(i,j,k, cycle, cover, isitem)=
let ( 
	 obj = isarr(i)||isstr(i)? i :undef
	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
	,beg= inum==1 
            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
    ,end= inum==1 
		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
		   : inum==2?j:k
		   
	,cycle=cycle==true?1:cycle

	,intv = sign(end-beg)        // when inum=3, the middle one is
			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	

	,xbeg =  cover?cover[0]:0    // extra beg
	,xend =  cover?cover[1]:0    // extra end

	,residual= or( mod(abs(end-beg), abs(intv)), intv)

	,extraidx= abs(intv)>abs(beg-end) // End points to be skipped, 
	           ? 1                    // depending on the intv. If
	           : residual             // intv too large, set to 1
	,objrange= obj
			   ? range ( len(obj)
						, cycle=cycle
						, cover=cover
						, isitem=isitem ) 
			   : []
											 
)(
	( intv==0 || beg==end || (beg==0&&end==undef) || obj==[] || obj=="" )
	? []
	: obj  // Allowing range(arr) or range(str)
	  ? isitem
		? cover
		  ? [ for(r=objrange) [ for(i=r) get(obj,i) ] ]
		  : [ for(i=objrange) get(obj,i) ]
	    : objrange

	  // Non obj : =================

	  : cover 
		? [ for( i=[beg:intv:end-extraidx] )  
				[ for(jj=range( (i-xbeg<beg && !cycle) ? beg: i-xbeg 
							 , (i+xend>=end && !cycle) ? end: i+xend+1
							 )) 
				  cycle
				  ? jj< beg || jj>=end 
					? fidx(abs(beg-end),jj-abs(beg),cycle=true)+abs(beg)
					: jj
				  : jj		
				]
		  ]

		// no cover  ---------------
		
		: cycle>0
		  ? concat( 
				[ for(i=[beg:intv:end-extraidx]) i ]
				,[ for(i=[beg:intv:beg+(cycle-1)*intv]) i]
				)
		  : cycle<0
			?  concat( 
				[ for(i=[end+cycle*intv:intv:end-intv]) i]
				,[ for(i=[beg:intv:end-extraidx]) i ]
				)
		 	:[ for(i=[beg:intv:end-extraidx]) i	]
		  
);
//	[_beg,_end,beg, end]//, intv, cycle]
//);



//========================================================
rearr=[ "rearr", "arr, order", "array", "Array" ,
 " Given an array (arr) and an integer array (order), return a new array containing
;; items i-th specified in *order*. For example, rearr( arr_of_3, [0,1,2,0,1,2] )
;; will duplicate the array, and rearr(arr_of_2, [1,0]) reverses it. Application
;; example, for a triangle defined by pqr, its 3 angles can be obtained with:
;;
;;  angle( pqr ) // = angle( rearr(pqr,[0,1,2]) ) = angle( rearr(pqr,[]) )
;;  angle( rearr(pqr, [0,2,1]) )
;;  angle( rearr(pqr, [1,0,2]) )
;;
;; NOTE 2014.8.9: the new list comp can handle it:
;;
;;  angle( [for(i=[1,0,2]) pqr[i]] )
;;
;; but doesn't look as straightforward. i.e., needs to think a bit about it.
"];

function rearr(arr, order=[],_i_=0)=
(
	[for(i=order) arr[i]]
);

module rearr_test( ops )
{
	pqr = randPts(3);
	doctest( rearr,
	[
	  ["pqr,[0,2,1]", rearr(pqr, [0,2,1]), [pqr[0], pqr[2],pqr[1]] ]
	, ["pqr,[2,0,1]", rearr(pqr, [2,0,1]), [pqr[2], pqr[0],pqr[1]] ]
	 //,[]
	], ops, ["pqr", pqr]
	);
}
//rearr_test(["mode", 13]);



//========================================================
repeat=["repeat", "x,n=2", "str|arr", "Array,String" ,
"
 Given x and n, duplicate x (a str|int|arr) n times.
"];

function repeat(x,n=2)=
(
	isarr(x)?
	concat(x, n>1?repeat(x,n-1):[])
	:str(x, n>1?repeat(x,n-1):"")
);

module repeat_test( ops )
{
	doctest( 
	repeat, [ 
		[ "''d''", repeat("d"), "dd" ]
	 ,	[ "''d'',5''", repeat("d",5), "ddddd" ]
	 ,	[ "[2,3]", repeat([2,3]), [2,3,2,3] ]
	 ,	[ "[2,3],3", repeat([2,3],3), [2,3,2,3,2,3] ]
	 ,  ["3",  repeat(3), "33" ]
	 ,  ["3,5",  repeat(3,5), "33333", ["rem","return a string"] ]
	],ops
	);
} 
//multi_test( ["mode",22] ); 
//doc(multi);




//========================================================
replace =[ "replace", "o,x,new=\"\"", "str|arr", "String, Array",
"
 Given a str|arr (o), x and optional new. Replace x in o with new when
;; o is a str; replace the item x (an index that could be negitave) 
;; with new when o is an array.
" ];

function replace(o,x,new="")=
let( x= isarr(o)?fidx(o,x):x)
( 
  isarr(o)
  ? (inrange(o,x)
	? concat( slice(o, 0, x), [new], x==len(o)-1?[]:slice(o,x+1)): o 
	)
  : (index(o,x)<0? 
	  o :str( slice(o,0, index(o,x))
			, new 
			, (index(o,x)==len(o)-len(x)?""
				:replace( slice(o, index(o,x)+len(x)), x, new) )
		   )
    )
);


module replace_test( ops )
{
	doctest( replace, 
	[
	  ["''a_text'', ''e''", 			replace("a_text", "e"), "a_txt"]
	 ,["''a_text'', ''''", 			replace("a_text", ""), "a_text"]
	 ,["''a_text'', ''a_t'', ''A_T''", replace("a_text", "a_t", "A_T"), 	"A_Text"]
	 ,["''a_text'', ''ext'', ''EXT''", 	replace("a_text", "ext", "EXT"), "a_tEXT"]
	 ,["''a_text'', ''abc'', ''EXT''", 	replace("a_text", "abc", "EXT"), "a_text"]
	 ,["''a_text'', ''t'',   ''{t}''", 	replace("a_text", "t", 	"{t}"), "a_{t}ex{t}"]
	 ,["''a_text'', ''t'', 4",     replace("a_text", "t", 	4), 		"a_4ex4"]
	 ,["''a_text'', ''a_text'', ''EXT''", replace("a_text", "a_text","EXT"), "EXT"]
    	 ,["'''', ''e''", 			replace("", "e"), ""]
	 ,"# Array # "
	 ,["[2,3,5], 0, 4", replace([2,3,5], 0, 4), [4,3,5] ]
	 ,["[2,3,5], 1, 4", replace([2,3,5], 1, 4), [2,4,5] ]
	 ,["[2,3,5], 2, 4", replace([2,3,5], 2, 4), [2,3,4] ]
	 ,["[2,3,5], 3, 4", replace([2,3,5], 3, 4), [2,3,5] ]
	 ,["[2,3,5], -1, 4", replace([2,3,5], -1, 4), [2,3,4] ]

	 ], ops
	);
} 		



//replace_test( ["mode",22] );
//doc(replace);



//========================================================
reverse=[ "reverse", "o", "str|arr", "Array, String",
"
 Reverse a string or array.\\
"];

function reverse(o)=
(
	isarr(o)
	? [ for(i=range(o)) o[len(o)-1-i] ]
	: join( [ for(i=range(o)) o[len(o)-1-i] ], "" )
);

module reverse_test( ops ){

	doctest(
	reverse
	,[
		[  [2,3,4], reverse([2,3,4]), [4,3,2] ]
	   ,[  [2], reverse([2]), [2] ]
	   ,[ "''234''",   reverse( "234" ), "432"  ]
	   ,[ "''2''",   reverse( "2" ), "2"  ]
	   ,[ "''",   reverse( "" ), ""  ]
	 ], ops
	);
}
//reverse_test( ["mode",22] );
//doc(reverse);




//========================================================
ribbonPts=[[]];
/*


	pts = [ p0,p1,p2 ...]

	ribbonPts( pts ) = [ pts1, pts2 ] 
		where pts1 = [N1,N2,N3 ...]
			 pts2 = [S1,S2,S3 ...]

	ribbonPts( pts, paired=true ) = [ pair_1, pair_2 ... ] 
		where pair_i = [Ni,Si]

	Note: 

	let 
		pairs = ribbonPts( pts, paired=true );
	then
		ribbonPts( pts, paired=false ) = [ getcol( pairs,0), getcol(pairs,1) ]
 
	or even easier:

		ribbonPts( pts, paired=false ) = transpose( pairs )


 // neighbors(arr, i, left=1, right=1, cycle=false)
 // neighborsAll(arr, left=1, right=1, cycle=false)

*/
function ribbonPts( pts, len=1, paired=true, closed=false,_I_=0 )=
(
	closed?[]
	:(
	_I_< len(pts)?
	concat( 	[ [normalPt( neighbors( pts, _I_), len=len ) 
			  ,normalPt( neighbors( pts, _I_), len=-len ) 
			  ]
			 ]
		 , ribbonPts( pts, len=len, paired=paired, closed=closed,_I_=_I_+1 )
		):[]
	)
  
);

module ribbonPts_demo_1()
{
	nPt=5;
	echom("ribbonPts_demo_1");
	pts = randOnPlanePts(nPt);
	echo("pts", pts);
	MarkPts( pts, ["labels", range(pts)] );

	rbpairs = ribbonPts( pts, len=0.3);
	
	echo("len(rbpairs)", len(rbpairs));
	Chain( pts, ["closed", false, "r",0.01, "color","black"] );
	rbs = transpose( rbpairs ); 

	echo("transpose correct?", transpose(rbs)==rbpairs );
	echo("rbs = ", len(rbs));
	Chain( rbs[0], 
			["closed", false, "color", "red", "r", 0.02, "transp",0.2] );
	//Chain( rbs[1], 
	//		["closed", false, "color", "green", "r", 0.02, "transp",0.2] );
	for(i=[0:len(rbpairs)-1]){
		echo_("i={_}, pts[i]={_}, rbpairs[{_}] = {_}"
					, [i,pts[i],i, rbpairs[i] ]) ;
		MarkPts( rbpairs[i] );
		Line0 ( rbpairs[i], ["r",0.01, "color","black"] );

		if( i>0 && i<len(rbpairs)-1)
			LabelPt( rbpairs[i][0], 
				str("aN =", angle([ normal(neighbors(pts,i-1))
								, ORIGIN
								,normal(neighbors(pts,i))
								] )
					)
			); 
		if( i< len(rbpairs)-1)
		Plane( concat( rbpairs[i], reverse(get(rbpairs, i+1, cycle=false)))  );
		//Line0( [pts[i],rbpairs[i][0] ] );
	}

}
//ribbonPts_demo_1();


//========================================================
Ring=["Ring", "ops", "n/a", "3D, Shape",
"
;; Given the ops, draw a ring on a plane that has its normal defined;;
;; in ops. Default ops:
;;
;;  [ ''ro'',3
;;  , ''ri'',2
;;  , ''r2o'',0
;;  , ''r2i'',0
;;  , ''target'', ORIGIN
;;  , ''normal'', [0,0,1]
;;  , ''rotate'', 0
;;  , ''color'',  false
;;  , ''transp'', 1
;;  , ''fn'', $fn
;;  , ''h'', 1
;;  ]
"];

module Ring( ops )
{
	ops=update( [
		 "ro",3
		,"ri",2
		,"r2o",0
		,"r2i",0
		,"target", ORIGIN 
		,"normal", [0,0,1]
		,"rotate", 0      
		,"color",  false  
		,"transp", 1      
		,"fn", $fn        
		,"h",      1      
		//,"inrod", false
		//,"outrod", false
		], ops ); 
	function ops(k)= hash(ops,k);

	//echo("ops(''outrod'' = ", ops("outrod"));

	h  = ops("h"  );
	rc = ops("rc" );
	fn = ops("fn" );
	ro = ops("ro" );
	ri = ops("ri" );
	r2o= ops("r2o");
	r2i= ops("r2i");
	color = ops("color");
	transp= ops("transp");
	rotang= ops("rotate");

	inrod_ = ops("inrod");
	outrod_ = ops("outrod");
	//inbar_ = ops("inbar");
	//outbar_ = ops("outbar");

	outrod = !outrod_?false
			  : update([ "style", "rod" // rod|block
					   , "len", 1 
					   , "count",  12  
					   ,"color",  color  
					   ,"transp", transp      
					   ,"fn", fn        
					   , "r",  0.4      // for style = rod
					   ,"width",0.4   // for style = bar
					   ,"h", h    // for style = bar
					   ,"markcolor",false // [[1,"red"],[4,"blue"]]
					   ], outrod_ );
	function outrod(k)= hash(outrod, k);

	inrod = !inrod_?false
			  : update([ "style", "rod" // rod|block
					   , "len", ri-r2o 
					   , "count",  6  
					   ,"color",  color  
					   ,"transp", transp      
					   ,"fn", fn        
					   , "r",  0.4      // for style = rod
					   ,"width",0.4   // for style = bar
					   ,"h", h    // for style = bar
					   ,"markcolor",false // [1,"red",4,"blue"]
					   ], inrod_ );	
	function inrod(k)= hash(inrod, k);

	inrodmarkcolor= inrod("markcolor");
	outrodmarkcolor= outrod("markcolor");	
	//echo("outrodmarkcolor = ", outrodmarkcolor);

	outrodPts= arcPts( r=ro, a=360, count=outrod("count") );
	inrodPts= arcPts( r=ri, a=360, count=inrod("count") );

	//outrodstyle = trueor(outrod("style","bar")!="bar","block");

	//echo(ORIGIN);
	//echo(hash(ops,"h"));

	translate( ops("target") )
	rotate( rotangles_z2p( ops("normal") ) )
	rotate( [0,0, rotang ] ){

	  difference(){
		color( color, transp )
		cylinder(r=ro, h=h,   center=true, $fn=fn );
		cylinder(r=ri, h=h+1, center=true, $fn=fn );
	  }
	
	  if( r2o>0 ){ 
		difference(){
		color( color, transp )
			cylinder(r=r2o, h=h, center=true, $fn=fn );
			cylinder(r=r2i, h=h+1, center=true, $fn=fn );
		}
	  }
	  //------------------------------------------------
	  if( outrod ){
		if( outrod("style")=="block"){
			for( i= [0:len(outrodPts)-1] ){
                p= outrodPts[i];
				L=len(outrodPts);
				FL=floor(len(outrodPts)/4);
				rodcolor= haskey(outrodmarkcolor,i)
								?hash(outrodmarkcolor,i)
								:outrod("color"); 
					
//			  assign( p= outrodPts[i]
//					, L=len(outrodPts)
//					, FL=floor(len(outrodPts)/4)
//					, rodcolor= haskey(outrodmarkcolor,i)
//								?hash(outrodmarkcolor,i)
//								:outrod("color") 
//					)
                //{
				//echo("rodcolor = ", rodcolor );
                //echo("outrodmarkcolor = ", outrodmarkcolor );    
			   Line( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= -outrod("len") )
					] 
			     ,  update( outrod, ["shift","center"
								 ,"color",rodcolor
								  ,"depth",outrod("h") 
								  ,"extension0",
								     // Extends to cover line-curve gap: 
									hash(outrod, "extension0",
									 ro-shortside(ro, outrod("width") )
									+ GAPFILLER )
								, "rotate", outrod("rotate")>0?
										    ((i>=FL)
											&&( i< L-(L/4==FL?FL: FL+1)				  
											)?-1:1) 
                                                  *outrod("rotate")
											:0]	
				//,["color", i==0?"red": i==1?"green":outrod("color") ]

								)
                   );	
			}
		} else if (outrod("style")=="rod"){
			//for( p= outrodPts ){
			for( i= [0:len(outrodPts)-1] ){
              	p= outrodPts[i];
				rodcolor= haskey(outrodmarkcolor,i)
								?hash(outrodmarkcolor,i)
								:outrod("color"); 
					 
//			  assign( p= outrodPts[i]
//					, rodcolor= haskey(outrodmarkcolor,i)
//								?hash(outrodmarkcolor,i)
//								:outrod("color") 
//					)
                
			   Line0( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= -outrod("len") 
							)
					] 
			     , update( outrod, ["color", rodcolor] )
					
                   );	
			}
		}
	
	  } // if 
	  //------------------------------------------------
	  if( inrod ){
		if( inrod("style")=="block"){
			for( i= [0:len(inrodPts)-1] ){
			    p= inrodPts[i];
				L=len(inrodPts);
				FL=floor(len(inrodPts)/4);
				rodcolor= haskey(inrodmarkcolor,i)
								?hash(inrodmarkcolor,i)
								:inrod("color") ;					
//			  assign( p= inrodPts[i]
//					, L=len(inrodPts)
//					, FL=floor(len(inrodPts)/4)
//					, rodcolor= haskey(inrodmarkcolor,i)
//								?hash(inrodmarkcolor,i)
//								:inrod("color") 
//					)
                //{
			  //echo_("L={_}, FL={_}, i={_}, i>=FL({_}), i-FL={_}, L-i={_}, i/4={_}"
				//	,[L,FL,i,i>=FL,i-FL,L-i, i/4] );	
			   Line( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= inrod("len") )
					] 
			     ,  update( inrod, [ "shift","center"
								, "color",rodcolor
								, "depth",inrod("h") 
								, "extension1",
								     // Extends to cover line-curve gap: 
									hash(inrod, "extension1", 
									 ro-shortside(ro, inrod("width") )
									+ GAPFILLER 
									)

								  
				//,["color", i==0?"red": i==1?"green":inrod("color") ]

								// If there's block rotation, need to change the
								// sign of rotation based on i  
/*
    L              FL    i_change  L-i_change
	4: +--+        1          3    1
	5: +--++                  3    2
	6: +---++                 4    2
	7: +----++                5    2 

	8: ++----++    2          6    2
	9: ++----+++              6    3
	10:++-----+++             7    3
	11:++------+++             8   3

	12:+++------+++     3     9    3
	13:+++------++++          9    4
	14:+++-------++++         10   4 
	15:+++--------++++        11   4

	16:++++--------++++   4   12   4 

*/								, "rotate", inrod("rotate")>0?
										    ((i>=FL)
											&&( i< L-(L/4==FL?FL: FL+1)				  
											)?-1:1) 
                                                  *inrod("rotate")
											:0]		
								)
                   );	
			  //}// assign
			}// for
		} else if (inrod("style")=="rod"){
			for( i= [0:len(inrodPts)-1] ){
                p= inrodPts[i];
				rodcolor= haskey(inrodmarkcolor,i)
								?hash(inrodmarkcolor,i)
								:inrod("color"); 
					
//			  assign( p= inrodPts[i]
//					, rodcolor= haskey(inrodmarkcolor,i)
//								?hash(inrodmarkcolor,i)
//								:inrod("color") 
//					)
                
			Line0( [ [p.x,p.y,0]
				   , onlinePt( [ [p.x,p.y,0], ORIGIN ]
				               , len= inrod("len")
									// Extends to cover line-curve gap: 
									+ r2o-shortside(r2o, inrod("r") )
									+ GAPFILLER 
							)
					] 
			     , update( inrod, ["color", rodcolor] )
                   );	
			}
	  } // if 
	  
	  }
	 }//

} // Ring



module Ring_demo_1()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0] 
			  ,"r2o", 1
		 	  ,"r2i", 0.8
			  ,"rotate", 720*$t
			  ,"color", "lightgreen"
			  ,"outrod", ["count",6,"width",1, "style","block","h",0.5]
			  ,"inrod", ["count",4]
			  ] 
		);
	Line( pq, ["r",0.5, "extension0", 2] );
	
}	

module Ring_demo_2()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
			  ,"r2o", 1
		 	  ,"r2i", 0.8
			  ,"rotate", 720*$t
			  ,"color", "lightgreen"
			  ,"outrod", ["count",12,"width",1, "h",0.5,"color","khaki"]
			  ,"inrod", ["count",24
						,"style","block"
						,"h", 0.6
						,"width",0.1
						,"color","blue"
					    ]
			   
			  ] 
		);
	//}
	Line( pq, ["r",0.5, "extension0", 2] );
	
}

module Ring_demo_3()
{
	
	//pq = randPts(2);
	pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
			  //,"h", 5
			  ,"ro", 3.2
		 	  ,"ri", 2.8
			  ,"r2o", 1
		 	  ,"r2i", .7
			  ,"rotate", 720*$t
			  ,"color", "lightgreen" 
			  //,"outrod", ["count",12,"width",1, "h",0.5,"color","khaki"] 
			  ,"inrod",   ["count",15
						,"style","block"
						,"h", 1
						,"width",0.1
						,"color","blue"
						,"rotate", 30
						,"extension1", 0.25 // overwrite default exteionsion1
										    // to cover the gaps between rods
											// and the out curve of ring2 
					    ]
			    
			  ] 
		);


	Line( pq, [["r",0.5], ["extension0", 2]] );
	
}

module Ring_demo_4()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0] 
			  //,"h", 5
			  ,"ro", 3.2
		 	  ,"ri", 2.8
			  ,"r2o", 1
		 	  ,"r2i", .7
			  ,"rotate", 720*$t
			  ,"color", "lightgreen"
			  ,"inrod", ["count",6,"width",1, "h",0.5]
			  ,"outrod", ["count",12
						,"style","block"
						,"h", 1.6
						,"width",0.2
						,"color","blue"
						,"rotate", 60
						,"extension0", .35 // overwrite default exteionsion1
										    // to cover the gaps between rods
											// and the out curve of ring2 
					    ]
			    
			  ] 
		);


	Line( pq, ["r",0.5, "extension0", 2] );
	
}

module Ring_demo_5_markrod()
{
	
	pq = randPts(2);
	pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	pq = [ORIGIN, [0,0,3]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
			 // ,"inrod", ["count",6,"width",1, "h",0.5] 
			  ,"outrod", ["count",12
						,"style","block"
						,"color","blue"
						,"transp",0.4
						,"markcolor", [0,"red",5,"green"] 
						,"markpts", true
						]
			   	
			  ,"inrod", ["count",6
						//,"style","block"
						//,"color","blue"
						,"transp",0.4
                        ,"r",0.3
						,"markcolor", [0,"purple"] 
					    ]
			    ] 
			   
		);


	Line( pq, ["r",1.5, "extension0", 2] );
	
}

module Ring_demo_6()
{
	
	pq = randPts(2);
	//pq =  [[0.371372, 2.76898, 0.526452], [2.73253, -2.9135, 0.142007]];
	echo( pq );

	//rotate( [ 0,0, 60*$t ] )
	//union(){
	Ring ( ops=[ "normal", pq[1]-pq[0]
			  ,"target", pq[0]
              , "ri",0
               , "h",0.3
               
			 // ,"inrod", ["count",6,"width",1, "h",0.5] 
			  ,"outrod", ["count",24, "len", 0.5
						,"style","block"
						,"markcolor", [0,"blue"] 
					    //,"color","blue"
						]
			   	
			  /*,"inrod", ["count",6
						//,"style","block"
						//,"color","blue"
						,"transp",0.4
                        ,"r",0.3
						,"markcolor", [0,"purple"] 
					    ] */
			    ] 		   
		);

	Line( pq, ["r",.5, "extension0", 0] );
}

module Ring_demo()
{
	Ring_demo_1();
	//Ring_demo_2();
	//Ring_demo_3();
	//Ring_demo_4();
	//Ring_demo_5_markrod();
    //Ring_demo_6();
	//
}
module Ring_test( ops ){ doctest(Ring,ops=ops);}

//Ring_demo();




//========================================================
Ring0=[["Ring0", "ops", "n/a", "3D, Shape"],
"
 Given an ops, draw a simple ring on a plane that has its normal defined\\
 in ops. Default ops:\\
 \\
  [ ''normal'', [0,0,1] \\
  , ''target'', ORIGIN  // where to translate to\\
  , ''ro'',3 // outer radius\\
  , ''ri'',2 // inner radius\\
  ]\\
"];

module Ring0( ops )
{
	echom("Ring0");

		ops= updates( OPS
			,[[ "normal", [0,0,1]
			 , "target", ORIGIN 
			 , "ro",3
			 , "ri",2
			 ], ops] );

		function ops(k)=hash(ops,k);

		h  = ops("h" );
		fn = ops("fn");

		//echo(ops);

		translate( ops("target") )
		rotate( rotangles_z2p( ops("normal") ) )
	  	difference(){
			color( ops("color"), ops("transp") )
			cylinder(r=ops("ro"), h=h,   center=true, $fn=fn );
			cylinder(r=ops("ri"), h=h+1, center=true, $fn=fn );
	  	}
}

module Ring0_demo_1()
{
	pq = randPts(2);
	Ring0( [ "normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   ] );
	//c = randcolor();
	//echo(c);
	Line(pq, ["r",rands( 1,20,1)[0]/10,"extension0",2, "color", randc()] );
}

module Ring0_demo_2()
{
	pq = randPts(2);
	MarkPts(pq, ["r",0.5]);
	Line(pq, ["r",0.6, "color", randc()] );
	Ring0( [ "normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "h", 10
		   , "transp", 0.3
		,"ro", 0.8, "ri", 0.7	
		   ] );
}

module Ring0_demo_3_tube()
{
	pts = randPts(9);
	pq=randPts(2);
	MarkPts(pq, ["r",0.5]);
	Line(pq, ["r",0.6, "color", randc()] );
	L = norm( pq[1]-pq[0] );
	Ring0( [ "normal", pq[1]-pq[0]
		   , "target", midPt(pq) //pq[1] 
		   , "color", randc()
		   , "h", L
		   , "transp", 0.3
		,"ro", 0.8, "ri", 0.7	
		   ] );
}

module Ring0_demo_4_tubes()
{
	pts = randPts(6,r=2);
	MarkPts(pts, ["r",.12, "colors",["brown"]]);

    c = randc();
 
	for (i=[0:len(pts)-2])
//	assign(  L= norm(pts[i+1]-pts[i])
//		  )
	{
     L= norm(pts[i+1]-pts[i]);   
	 Ring0( [ "normal", pts[i+1]-pts[i]
		   , "target", midPt([pts[i],pts[i+1]]) 
		   , "color", "khaki" //c //randc()
		   , "h", L
		   , "transp", 1//0.7
		   , "ro", .18, "ri", .16	
		   ] );
	}
	
}

module Ring0_test( ops ){ doctest(Ring0,ops=ops);}

module Ring0_demo()
{
	//Ring0_demo_1();
	//Ring0_demo_2();
	//Ring0_demo_3_tube();
	Ring0_demo_4_tubes();
}	

//Ring0_demo();
//doc(Ring0);


//========================================================
Gear = [];
module Gear( ops )
{
	//echom("Gear");
	ops=updates( OPS,[
		[ "style", "rod"     // rod|block|cone
		, "count",  12  
		, "normal", [0,0,1]
		, "target", ORIGIN 
		, "dir"  , "out"     // out|in|up|down  (outward|inward...)
		, "radius", 3  
		, "r"    ,  0.1      // for style = rod
		, "w"    ,  0.4      // for style = bar
		, "h"    ,  0.5      // for style = bar
		, "markcolor",false  // [1,"red",4,"blue"]
		, "rodrotate", 0     // Rotation of individual rod about its axis. 
						   // Only works
						   //  for style=block. Note that OPS has a 
						   //  "rotate",too, that would be for the
						   //  rotate of entire RodCircle about its axis.
				
		], ops 
		]);	
	function ops(k)= hash(ops,k);
}





//========================================================
RM=[ "RM","a3","matrix", "Math, Geometry",
"
;; Given a rotation array (a3=[ax,ay,az] containing rotation angles about
;; x,y,z axes), return a rotation matrix. Example:
;;
;;  p1= [2,3,6]; 
;;  a3=[90,45,30]; 
;;  p2= RM(a3) * p1;  // p2= coordinate after rotation
;;
;; In OpenScad the same rotation is carried out this way:
;;
;;   rotate( a3 )
;;   translate( p1 )
;;   sphere(r=1); 
;;
;; This rotates a point marked by a sphere from p1 to p2
;; but the info of p2 is lost. RM(a3) fills this hole, thus
;; provides a possibility of retrieving coordinates of
;; rotated shapes.
;;
;; The reversed rotation from p2 to p1 can be achieved by simply applying
;; a transpose :
;;
;;  p3 = transpose(RM(a3)) * p2 = p1
"];


function RM(a3)=
(
	a3.z==0?
	IM*RMy(a3.y) * RMx(a3.x) 
	:RMz(a3.z) * RMy(a3.y) * RMx(a3.x) 

//  No idea why the following doesn't work with [0,y,0]:
//  Note: Run demo_reverse( "On y", p0y0(randPt()), "lightgreen");
//        in rotangles_p2z_demo();
//	(a3.z==0?IM:RMz(a3.z)) * 
//	(a3.y==0?IM:RMy(a3.y)) *
//	(a3.x==0?IM:RMy(a3.x)) 
	
);

module RM_test(ops){ doctest(RM,
[],ops); }

//RM_test( [["mode",22]] );
//doc(RM);

module RM_demo()
{
	a3 = randPt(r=1)*360;
	echo("in RM, a3= ",a3);
	rm = RM(a3);
	echo(" rm = ", rm);
}
//RM_demo();


//========================================================
RMx=["RMx","ax","matrix", "Math, Geometry",
"
 Given an angle (ax), return the rotation matrix for
;; rotating a pt about x-axis by ax. Example:
;;
;;  pt = [2,3,4]; 
;;  pt2= RMx(30)*pt;  
;;
;; In openscad, you can rotate the pt to pt2 by thiss:
;;
;;  rotate( [30,0,0 ])
;;  translate( pt )
;;  sphere(r=1); 
;;
;; However,  the coordinate info of pt2 is lost. RMx
;; (and RMy, RMz) provides a way to retrieve it. This
;; is a work around solution to an often asked question:
;; how to obtain the coordinates of a shape.
"];	

function RMx(ax)= [
	[1,0,0]
	,[0, cos(-ax), sin(-ax) ]
	,[0,-sin(-ax), cos(-ax) ]
];

module RMx_test(ops)
{
	doctest( RMx
	,[
	
	],ops );
}
//RMx_test( ["mode",22] );
//doc(RMx);




////========================================================
RMy=["RMy","ay","matrix", "Math, Geometry",
"
 Given an angle (ay), return the rotation matrix for
;; rotating a pt about y-axis by ay. See RMx for details.
"];	
function RMy(ay)=[
	 [cos(-ay), 0, -sin(-ay) ]
	,[0,        1,        0  ]
	,[sin(-ay), 0,  cos(-ay) ]
];
module RMy_test(ops)
{
	doctest( RMy
	,[
	
	],ops );
}
//RMy_test( ["mode",22] );



//========================================================
RMz=["RMz","az","matrix", "Math, Geometry",
"
 Given an angle (az), return the rotation matrix for
;; rotating a pt about z-axis by az. See RMx for details.
"];

function RMz(az)=[
	 [ cos(-az), sin(-az), 0 ]
	,[-sin(-az), cos(-az), 0 ]
	,[       0,         0, 1 ]
];
module RMz_test(ops)
{
	doctest( RMz
	,[
	
	],ops );
}
//RMz_test( ["mode",22] );
//doc(RMz);


//========================================================
RMs=[[]];
function RMs( angles, pts, _i_=0 )=
(
	_i_<len(pts)? concat( [ RM(angles)*pts[_i_] ]
                         , RMs( angles, pts, _i_+1)
					  ):[]  
);

module RMs_demo()
{
	echom("RMs");
	a3 = randPt(r=1)*360;
	echo("in RMs, a3= ",a3);
	rm = RM(a3);
	echo(" rm = ", rm);
	p = randPt();
	echo( " p = ", p);
	echo( "rm*p = ", rm*p );
	echo( "RMs(a3, [p]) = ", RMs(a3, [p]) );
	
}
//RMs_demo();


//========================================================
RM2z=["RM2z","pt,keepPositive=true", "matrix", "Math, Geometry", 
"
 Given a pt, return the rotation matrix that rotates
;; the pt to z-axis. If keepPositive is true, the matrix will
;; always rotate pt to positive z. The xyz of the new pt on z-axis, ;;
;; pt2, is thus:
;;
;;  pt2 = RM2z(pt)*pt
;;
;; The reversed rotation, i.e., rotate a point on z back to pt,
;; can be achieved as:
;;
;;  pt3 = transpose( RM2z(pt) )*pt2  = pt
"];

function RM2z(pt,keepPositive=true)=
(
	/*pt.z==0
	?RM( rotangles_p2z(pt,keepPositive=true) ) 
	:RMy((keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/noRM(pt)))
	*RMx( sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z)) )
	*/
	RM( rotangles_p2z(pt,keepPositive=true) )
);

module RM2z_demo()
{
	pt= randPt();
		//If wanna check a pt on the xy-plane :
		//pt= [randPt()[0],randPt()[0],0];
	Line( [O,pt] );
	pt2 = RM2z(pt)*pt;
	Line( [O,pt2] );
	//echo_("In RM2z_demo, pt={_}, pt2= {_}", [pt,pt2]);
	//echo_("In RM2z_demo, len pt - len pt2= {_}", [norm(pt)-norm(pt2)]);
	//echo_("In RM2z_demo, RM2z= {_}", [RM2z(pt)]);
	MarkPts( [pt, pt2] );

	pt3 = transpose( RM2z(pt))*pt2;
	translate(pt3)
	color("blue", 0.2)
	sphere(r=0.3);
	PtGrid(pt3);

}
//RM2z_demo();


module RM2z_demo2()
{
	pt= randPts(1)[0];
	ptz= [0,0,pt.z];
}
module RM2z_test(ops)
{
	doctest( RM2z
	,[
	
	],ops );
}
//RM2z_demo();
//RM2z_test( ["mode",22] );
//doc(RM2z);





//========================================================
RMz2p=[ "RMz2p","pt","array", "Math, Geometry",
"
 Given a pt, return the rotation matrix that rotates the pt to
;; z-axis. The xyz of the new pt, pn, is:
;;
;;  pn = RM2z(pt)*pt
"];

function RMz2p(pt)=
(
3
//ax = -sign(pt.z)*asin( pt.y/L );  // rotate@y to xz-plane
//	ay = pt.z==0?90:atan(pt.x/pt.z); 	// rotate from xz-plane to target 	
	
);

//echo("a<span style='font-size:20px'><i>this is a&nbsp;test</i></span>b");

module RMz2p_demo()
{
	pt= randPts(1)[0];  // random pt
	Line( [O,pt] );
	PtGrid(pt);

	echo("pt = ", pt);
	L = norm(pt);       
	ptz = [0,0,sign(pt.z)*L];      // ptz: projection pt on z-axis
	Line( [O,ptz]); 

	// rot=[ 0, -atan( z/slopelen(x,y) ), x==0?90:atan(y/x)];
	// ay = -atan( z/slopelen(x,y) );  // rotate@y to xz-plane
	// az = x==0?90:atan(y/x); 		   // rotate from xz-plane to target 	
	/*
		rotate from x to xz-plane to target
		x ==> xz ==> target
		
		x, y, z
		   ay = -atan( z/slopelen(x,y) );  // rotate@y to xz-plane
		      az = x==0?90:atan(y/x); 	// rotate from xz-plane to target 	
	
		z, x, y
		   ax = -atan( y/slopelen(z,x) );  // rotate@y to xz-plane
		      ay = z==0?90:atan(x/z); 	// rotate from xz-plane to target 	
		
		
		

	*/

	sloxz = slopelen(pt.x,pt.z);
	sloyz = slopelen(pt.y,pt.z);

	// rotate at y to yz-plane
	//ax = -atan(pt.z/sloyz);   // rotate about x-axis from z-axis
	//ay = pt.x==0?90:atan(pt.z/pt.x) ;   // rotate about y axis from
											// where ax already applied
    //ax = -sign(pt.z)*sign(pt.y)*atan( abs(pt.y/slopelen(pt.z,pt.x)) );  // rotate@y to xz-plane
	ax = -sign(pt.z)*asin( pt.y/L );  // rotate@y to xz-plane
	ay = pt.z==0?90:atan(pt.x/pt.z); 	// rotate from xz-plane to target 	
	/*
	++ -
	+- +
	-+ +
	-- -

	pt.y	, pt.z, slozx, 
	  +    -    +       +
	  -    +    -       +
	  -    -    -       - 
      +    +    +       - 

	*/
	echo("pt.y = ", pt.y);
	echo("pt.z = ", pt.z);
	echo("slozx= ", pt.y/slopelen(pt.z,pt.x) );

	echo("ax", ax);
	echo("ay", ay);
	//=======================
	// rotate ax from z-axis 
	rotate( [ax, 0, 0 ] )
	translate( ptz )
	color("green", 0.3)
	sphere(r=0.1);

	// pt after rotating ax from z-axis. This pt must be yz plane
	pt2 = RMx(ax)*ptz;
	//Line([O,pt2]);

	echo("pt2=", pt2);

	// translate to pt2 directly
	translate( pt2 )
	color("green", 0.2)
	sphere(r=0.15);
	color("green", 0.3)
	Line( [O, pt2] );
	//=============================

	
	//rotate( [0, ay, 0 ] )
	//rotate( [ax, 0, 0 ] )
	rotate( [ax, ay, 0 ] )
	translate( ptz )
	color("blue", 0.3)
	sphere(r=0.2);


	pt3 = RMy(ay)*RMx(ax)*ptz;
	translate( pt3 )
	color("blue", 0.2)
	sphere(r=0.3);
	
	//=============================


	//MarkPts([pt,ptz, pt2]);


/*
	RMz2p = RMx(ax); //RMy(ay)*RMx(ax);

	pt3 = RMz2p*ptz;

	MarkPts( [pt, ptz] );

	Line([O,pt3]);
	translate(pt3 )
	color("blue", 0.2)
	sphere(r=0.3);

	*/	
	/*
	pt2 = RM2z(pt)*pt;
	Line( [O,pt2] );
	echo_("In RM2z_demo, pt={_}, pt2= {_}", [pt,pt2]);
	echo_("In RM2z_demo, len pt - len pt2= {_}", [norm(pt)-norm(pt2)]);
	echo_("In RM2z_demo, RM2z= {_}", [RM2z(pt)]);
	MarkPts( [pt, pt2] );

	pt3 = transpose( RM2z(pt))*pt2;
	translate(pt3)
	color("blue", 0.2)
	sphere(r=0.3);
	*/
}
//RMz2p_demo();

module RM2z_demo2()
{
	pt= randPts(1)[0];
	ptz= [0,0,pt.z];
}

//========================================================
rodfaces=["rodfaces","sides=6, hashole","array", "Array, Geometry, Faces",
 " Given count of sides (sides) of a rod( column, cylinder), return the 
;; faces required for the polyhedron() function to render the sides of
;; that rod. For a rod with sides=4 below: 
"];

function rodfaces(sides=6,hashole=false)=
 let( sidefaces = rodsidefaces(sides)
	, holefaces = hashole? 
				  [ for(f=sidefaces) 
					 reverse( [for(i=f) [i+sides*2] ])
				  ]:[]
	)
(
	hashole
	? concat( range(sides-1, -1)
			, range(sides, sides*2)
			, sidefaces
			) 	
	: concat( [ range(sides-1, -1) ]
			, [ range(sides, sides*2) ]
			, sidefaces
			)
);

//	p_faces = reverse( range(sides) );
//	// faces need to be clock-wise (when viewing at the object) to avoid 
//	// the error surfaces when viewing "Thrown together". See
//	// http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
//	q_faces = range(sides, sides*2); 
//concat( ops("has_p_faces")?[p_faces]:[]
//				 , ops("has_q_faces")?[q_faces]:[]
//				 , rodsidefaces(sides)
//				 , ops("addfaces")
//				 );

//========================================================
rodsidefaces=["rodsidefaces","sides=6,twist=0","array", "Array, Geometry, Faces",
 " Given count of sides (sides) of a rod( column, cylinder), return the 
;; faces required for the polyhedron() function to render the sides of
;; that rod. For a rod with sides=4 below: 
;;         
;;       _6-_
;;    _-' |  '-_
;; 5 '_   |     '-7
;;   | '-_|   _-'| 
;;   |    '(4)   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;; 
;; twist=0
;;
;;   [0,1,5,4]
;; , [1,2,6,5]
;; , [2,3,7,6]
;; , [3,0,4,7]
;;
;; twist = 1:
;;
;;       _5-_
;;    _-' |  '-_
;;(4)'_   |     '-6
;;   | '-_|   _-'| 
;;   |    '-7'   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;;
;;   [0,1,4,7]
;; , [1,2,5,4]
;; , [2,3,6,5]
;; , [3,0,7,6]
;;
;;
;; twist = 2:
;;       (4)_
;;    _-' |  '-_
;; 7 '_   |     '-5
;;   | '-_|   _-'| 
;;   |    '-6'   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;;
;;   [0,1,7,6]
;; , [1,2,4,7]
;; , [2,3,5,4]
;; , [3,0,6,5]
;;
;; Note that the top and bottom faces are NOT included. 
"];

function rodsidefaces( sides=6, twist=0) =
 let( top= twist
		   ? roll( range(sides,2*sides), count=twist) 
		   : range(sides,2*sides)
	)
(
	[ for(i = range(sides) )
	  [ i
	  , i==sides-1?0:i+1
	  , i==sides-1? top[0]: top[i+1]
	  , top[i]
//	  , i + 1 + (i- twist==sides-1?0:sides) -twist
//	  , i+sides - twist +( i==0&&twist>0?sides:0)
	  ]
	]
);



module rodsidefaces_test( ops=["mode",13] )
{
	doctest( rodsidefaces,
	[
	 ["4", rodsidefaces(4), 
			[ [0,1,5,4]
			, [1,2,6,5]
			, [2,3,7,6]
			, [3,0,4,7]
			]
	]
	,["5", rodsidefaces(5), 
			[ [0, 1, 6, 5]
			, [1, 2, 7, 6]
			, [2, 3, 8, 7]
			, [3, 4, 9, 8]
			, [4, 0, 5, 9]
			]
	 ]
	,["4,twist=1", rodsidefaces(4,twist=1), 
			[ [0,1,4,7]
			, [1,2,5,4]
			, [2,3,6,5]
			, [3,0,7,6]
			]
	]
	,["4,twist=-1", rodsidefaces(4,twist=-1), 
			[ [0,1,6,5]
			, [1,2,7,6]
			, [2,3,4,7]
			, [3,0,5,4]
			]
	]
	,["4,twist=2", rodsidefaces(4,twist=2), 
			[ [0,1,7,6]
			, [1,2,4,7]
			, [2,3,5,4]
			, [3,0,6,5]
			]
	 ]

	], ops
	);
}


module rodsidefaces_demo_1()
{
	//==== sides=4
	pqr = randPts(3); 
	bps = cubePts(pqr ); // function cubePts( pqr, p=undef, r=undef, h=1)
    *MarkPts( bps );
	*LabelPts( bps, range(bps) );
	fs = rodsidefaces( 4 );
	color(undef, 0.4)
	*polyhedron( points = bps, faces = fs ); 

	//==== sides= 5
	c=5;
	pqr2 = randPts(3);
	
	cps1 = arcPts( pqr2, r=2, count=c, pl="yz", cycle=1 );

	newp = onlinePt(p01(pqr2), len=-0.2);
	cps2 = arcPts( [ newp, P(pqr2), R(pqr2) ] , r=2, count=c, pl="yz", cycle=1 );

	cps = concat( cps1, cps2);
	MarkPts( cps );
	LabelPts( cps, range(cps) );
	fs2 = rodsidefaces(c);
	echo("fs2 = ", fs2);
  	
	//LabelPts( cps1);
	//LabelPts( cps2);
	color(undef, 0.4)
	polyhedron( points = cps, faces = fs2 ); 
	Chain( cps1, ops=["closed",true]);
	Chain( cps2, ops=["closed",true]);
}

//rodsidefaces_demo_1();

//========================================================
rodPts=["rodPts","","pts","Point, Array, Geometry",
 " Given 
;;
"];

function rodPts( pqr=randPts(3), ops=[] )=
 let( _ops = [ "r", 0.05
	  , "sides",     6
	  , "label", "" 
	  , "p_pts",undef  // pts on the P end
	  , "q_pts",undef  // pts on the q end
	  , "p_angle",90   // cutting angle on p end
	  , "q_angle",90   // cutting angle on q end
	  , "rotate",0  // rotate about the PQ line, away from xz plane
	  , "q_rotate", 0 // see twistangle()
	  , "cutAngleAfterRotate",true // to determine if cutting first, or
						  // rotate first. 
	  ]
	,ops = concat( ops, _ops )
	,r       = hash(ops, "r")
	,rot     = hash(ops,"rotate")
	,sides   = hash(ops,"sides")
	,p_angle = hash(ops,"p_angle")
	,q_angle = hash(ops,"q_angle")
	,q_rotate = hash(ops,"q_rotate")
	,qidx_rotate= round(q_rotate/360)*sides
	,cutAngleAfterRotate= hash(ops,"cutAngleAfterRotate")

	,pqr0= pqr==undef
			? randPts(3)
		 	: len(pqr)==2
		   	  ? concat( pqr, [randPt()] )
		   	  : pqr 
	,P = pqr0[0]
	,Q = pqr0[1]
	,R1= pqr0[2]
	,pq= [P,Q]
	,N = N(pqr0)
	,M = N2(pqr0)
	,T = othoPt( [ P,R1,Q ] )
	,p1_for_rot= angle(pqr0)>=90
				? P
				: onlinePt([norm(T-Q)>=norm(P-Q)? T:P,Q], len=-1 )
	,R2 = anyAnglePt( [ p1_for_rot, T, R1]     
                   , a= rot 
                   , pl="yz"
                   , len = dist([T,R1]) 
				  )
	,P2 = onlinePt( [P,Q], len=-1)

	,norot_pqr_for_p = [P2,P, R1]
	,norot_pqr_for_q = [P, Q, R1]
	,rot_pqr_for_p   = [P2,P, R2]
	,rot_pqr_for_q   = [P, Q, R2]

//	pqr_for_p = cutAngleAfterRotate&&rot>0
//				? rot_pqr_for_p
//				: norot_pqr_for_p;
//	pqr_for_q = cutAngleAfterRotate&&rot>0
//				? rot_pqr_for_q
//				: norot_pqr_for_q;		

	,p_pts90= arcPts( rot_pqr_for_p
				  , r=r, a=360, pl="yz", count=sides )

	,_q_pts90= arcPts( rot_pqr_for_q
				   , r=r, a=360, pl="yz", count=sides )
	, q_pts90= qidx_rotate
			   ? concat( slice(_q_pts90, qidx_rotate), slice(_q_pts90, 0, qidx_rotate)) 
			   : _q_pts90

	//-------------------------------------------------
	// Cut an angle from P end. 
	,Ap = anyAnglePt( cutAngleAfterRotate
					? [P2,P,R1]
					: [P2,P,R2]
				  , a=p_angle+(90-p_angle)*2
				  , pl="xy")
	,Np = N([ Ap, P, Q ])  // Normal point at P
	,p_plane = [Ap, Np, P ]  // The head surface 
	,p_pts=or( hash(ops,"p_pts")
			  , p_angle==0
			   ? p_pts90
			   : 	[ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], p_plane ) ]
			  )
	//-------------------------------------------------
	// Cut an angle from Q end. 
	,Aq = anyAnglePt( cutAngleAfterRotate
					? [P,Q,R1]
					: [P,Q,R2]					
				  , a=q_angle //90-q_angle
				  , pl="xy") // Angle point for tail
	,Nq = N( cutAngleAfterRotate
					? norot_pqr_for_q
					: rot_pqr_for_q )  // Normal point at Q
	,q_plane = [Aq, Nq, Q ]//:pqr0;  // The head surface 
	,q_pts =or( hash(ops,"q_pts")
			  , q_angle==0
			    ? q_pts90
			    : [ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], q_plane ) ]
			  )
//	,q_pts= qidx_rotate>0
//			? concat( slice( _q_pts, sides-qidx_rotate)
//		 			, slice( _q_pts, 0, sides-qidx_rotate)
//					)
//			: qidx_rotate<0
//	  			? concat( slice(_q_pts, qidx_rotate)
//						, slice(_q_pts, 0+qidx_rotate)
//		 				)
//				: _q_pts
	
)( 
	[p_pts,q_pts]
);


module rodPts_demo_twistangle()
{
 echom("rodPts_demo_twistangle");

 pqr = randPts(3);

 

}

//========================================================
Rod=["Rod", "pqr,ops", "n/a", "Geometry,Line",
 " Given a 2- or 3-pointer (pqr), draw a line (cylinder) from point P
;; to Q using polyhedron. The 3rd point, R, is to make a plane (of pqr), 
;; from that the angle of drawn points start in a anti-clock-wise manner 
;; (when looking from P-to-Q direction). So R is to determine where the
;; points start. If it is not important, use 2-pointer for pqr and R will
;; be randomly set. 
;;
;; Imagine pqr as a new coordinate system where P is on the x axis, Q is 
;; the ORIGIN and R is on xy plane: 
;; 
;;       N (z)
;;       |
;;       |________
;;      /|'-_     |
;;     / |   '-_  |
;;    |  |      : |
;;    |  Q------|---- M (y)
;;    | / '-._/ |
;;    |/_____/'.|
;;    /          '-R
;;   /
;;  P (x)
;;
;; In the graph below, P,Q,R and 0,4 are co-planar. 
;;
;;       _7-_
;;    _-' |  '-_
;; 6 :_   |Q.   '-4
;;   | '-_|  '_-'| 
;;   |    '-5' '-|
;;   |    | |    '-_
;;   |  p_-_|    |  'R
;;   |_-' 3 |'-_ | 
;;  2-_     |   '-0
;;     '-_  | _-'
;;        '-.'
;;          1
;;
;; headangle/tailangle are 90, but customizable.
;;
;;                         R
;;                        :
;; |       .--------.    :  |
;; |     .'          '. :   |  
;; |   P'--------------Q.   | 
;; | .' pa            qa '. |
;; |------------------------|
"];

//========================================================

module Rod( pqr=randPts(3), ops=[]) //r=0.6, count=6, showindex=true)
{
	// A Rod making use of rodPts

	//echom("Rod");

	//------------------------------------------
	ops = concat
	( ops,
	, [ 
	  //------ To be sent to rodPts()
		"r", 0.05
	  , "sides",     6
	  , "label", "" 
	  , "p_pts",undef  // pts on the P end
	  , "q_pts",undef  // pts on the q end
	  , "p_angle",90   // cutting angle on p end
	  , "q_angle",90   // cutting angle on q end
	  //, "twist_angle", 0 // see twistangle()
	  , "rotate",0  	  // rotate about the PQ line, away from xz plane
	  , "cutAngleAfterRotate",true // to determine if cutting first, or
								  // rotate first. 
	  //------ The above are to sent to rodPts()
	
	  , "has_p_faces", true
	  , "has_q_faces", true
	  , "addfaces",[]
	  , "addpts",[]

	  , "echodata",false
	  , "markrange",false // Set to true or some range to mark points
						 //if true, full range
	  , "markOps",  []    // [markpt setting, label setting] 
	  , "labelrange",false
	  , "labelOps", []
	  ]
	, OPS
	);
	function ops(k)=hash(ops,k);
	sides = ops("sides");

	rodpts = rodPts( pqr, ops);

	p_pts = rodpts[0];
	q_pts = rodpts[1];

	// Check if angles are correct:
	//	echo( "angle p:", angle( [Q(pqr),P(pqr), p_pts[0]] ));
	//	echo( "angle q:", angle( [P(pqr),Q(pqr), q_pts[0]] ));

	allpts = concat( p_pts, q_pts, ops("addpts") );
	//echo("Rod.allpts: ", allpts);

	_label = ops("label");
	label= _label==true?"PQ":_label;

	if(label) LabelPts( p01(pqr), label ) ;

//	p_faces = reverse( range(sides) );
//	// faces need to be clock-wise (when viewing at the object) to avoid 
//	// the error surfaces when viewing "Thrown together". See
//	// http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
//	q_faces = range(sides, sides*2); 
//	faces = concat( ops("has_p_faces")?[p_faces]:[]
//				 , ops("has_q_faces")?[q_faces]:[]
//				 , rodsidefaces(sides)
//				 , ops("addfaces")
//				 );
//	faces = rodfaces(sides);
	faces = faces("rod",sides);
	
	echo("Rod.faces:", faces);

	//=========== marker and label =============
	allrange= range(allpts);
	markrange = hash(ops, "markrange" , if_v=true, then_v=allrange);
	labelrange= hash(ops, "labelrange", if_v=true, then_v=allrange);
	markops = or(hash(ops, "markops"),[]); 
	labelops= or(hash(ops, "labelops"),["labels",""]); 
	pts_to_mark = [ for(i=markrange)	allpts[i] ];
	pts_to_label= [ for(i=labelrange)	allpts[i] ];

	// We make them BEFORE the polyhedron so they are always visible
	if(markrange){ MarkPts( pts_to_mark, markops); }
	if(labelrange){ LabelPts( pts_to_label, hash(labelops, "labels"), labelops); }
	// =========================================

	color( ops("color"), ops("transp") )
	polyhedron( points = allpts 
			  , faces = faces
			 );

	//==========================================
	if( ops("echodata"))
	{
		echo(
			 "<b>Rod()</b>:<br/><b>ops</b> = ", ops
			,join([""
			,str("<b>q_pts</b> = ", q_pts )
			,str("<b>p_pts</b> = ", p_pts )
			,str("<b>faces</b> = ", faces )
			],";<br/>"));
	}
}
//========================================================

module _Rod_dev( pqr=randPts(3), ops=[]) //r=0.6, count=6, showindex=true)
{
	// This is the original version before rodPts() was built. After we 
	// have rodPts, the rod point calc was taken out so the current Rod()  
	// simply makes use of rodPts();

	//echom("Rod");

	//------------------------------------------
	ops = concat
	( ops,
	, [ "r", 0.05
	  , "sides",     6
	  , "label", "" 
	  , "p_pts",undef  // pts on the P end
	  , "q_pts",undef  // pts on the q end
	  , "p_angle",90   // cutting angle on p end
	  , "q_angle",90   // cutting angle on q end
	  , "rotate",0  // rotate about the PQ line, away from xz plane
	  , "cutAngleAfterRotate",true // to determine if cutting first, or
						  // rotate first. 
	  , "echodata",false

	  , "markrange",false // Set to true or some range to mark points
						 //if true, full range
	  , "markOps",  []    // [markpt setting, label setting] 
	  , "labelrange",false
	  , "labelOps", []
	  ]
	, OPS
	);

	function ops(k) = hash( ops, k);

	//echo("Rod.ops:",ops);
	//echo("Rod, p-, q-angle: ", [p_angle, q_angle] );

	r = ops("r");
	rot = ops("rotate");
	sides = ops("sides");
	p_angle = ops("p_angle");
	q_angle = ops("q_angle");
	cutAngleAfterRotate= ops("cutAngleAfterRotate");

	//echo("sides", sides);
	//------------------------------------------
	// pqr0: original pqr before rotate
	//
	pqr0= pqr==undef
		 ? randPts(3)
		 : len(pqr)==2
		   ? concat( pqr, [randPt()] )
		   : pqr ;

	//pq = p01(pqr0);
	P = pqr0[0];
	Q = pqr0[1];
	R1= pqr0[2];
	pq= [P,Q];
	N = N(pqr0);
	M = N2(pqr0);

	// Before rotation: coplanar: P Q A B 0 4 R1
	//
	//                          _-5
	//                       _-'.' '.
	//                    _-' .'     '.
	//                 _-'  .'         '.
	//              _-'   _6    _-Q----_-4--------------A 
	//            1'   _-'  '_-'   '_-'.'            _-'
	//          .' '.-'   _-' '. _-' .'           _-'
	//        .' _-' '. -'    _-'. .' '.       _-'
	//      .'_-'   _-''.  _-'   _-7    R1  _-'
	//	  2-'     P-----0-'----------------B     
	//      '.         .'  _-'
	//        '.     .' _-'
	//          '. .'_-'
	//            3-'
	//                     
	// After rotation by 45 degree: coplanar: P Q 4 0 R2 (R2 not shown)  
	//                 
	//	      5-------4
	//	     /|      /|
	//	    / |  Q'-/----------A
	//	   /  |  /\/  |       /
	//	  /   6-/-/---7      /
	//   /   / / /  \/      /
	//	1-------0   /\  	  /
	//	|  / /  |  /  R1  /
	//	| / P.--|-/------B
	//	|/    '.|/          
	//  2-------3          
	//           
	// The pl_PQ40 rotates (swings like a door) to make 0-4 move away 
	// from pl_PQAB. So [P,Q,R2] will be the new pqr to work with.

	// So, if rotation, we make a R2 that is on the pl_PQ40 to make a new 
	// pqr: [P,Q,R2] 

	// We use the othoPt T as the rotate center to carry the rotation:
	//
	//	       N (z)
	//	       |
	//	       |
	//	      /|
	//	     / |       R2  
	//	    |  |     .' 
	//	    |  Q---_------- M (y)
	//	    | / '-:_
	//	    |/_-'   '._
	//	  T /----------'-R1
	//	   /
	//	  P (x)
	//   /
	//  /
	// P2

	// Calc R2 = post-rotation R using T

	T = othoPt( [ P,R1,Q ] );

	// Since we need to get R2 by rotating an angle on the PTR1 plane, 
	// of which its normal is decided by the order of P,T, R1, the location 
	// of T is critical that it could get the rotation to the wrong side.
	// 
	// (1)
	//      R1_             PQ > TQ
	//      |  '-._
	//	p---T------'Q
	//
	// (2)           R1     PQ > TQ     
	//              /|
	//     P-------Q T
	//   
	// (3)   R1_            PQ < TQ
	//       |  '-._
	//       |       '-._
	//       T  p-------'Q
	// (4)
	//              R1      PQ < TQ
	//            .'|
	//          .'  |
	//        .'    |
	//   P---Q      T
	//   
	// Shown above, case (3) will cause PTR1 to go to wrong direction. 
	// So we make a new p1_for_rot as P to ensure that it is always on 
	// the far left:

	p1_for_rot= angle(pqr0)>=90
				? P
				: onlinePt([norm(T-Q)>=norm(P-Q)? T:P,Q], len=-1 );

	R2 = anyAnglePt( [ p1_for_rot, T, R1]     
                   , a= rot 
                   , pl="yz"
                   , len = dist([T,R1]) 
				  );

	P2 = onlinePt( [P,Q], len=-1);  // Extended P: P2----P----Q

	norot_pqr_for_p = [P2,P, R1];
	norot_pqr_for_q = [P, Q, R1];
	rot_pqr_for_p   = [P2,P, R2];
	rot_pqr_for_q   = [P, Q, R2];

	pqr_for_p = cutAngleAfterRotate&&rot>0
				? rot_pqr_for_p
				: norot_pqr_for_p;
	pqr_for_q = cutAngleAfterRotate&&rot>0
				? rot_pqr_for_q
				: norot_pqr_for_q;		

	// p_pts90 = [p0,p1,p2,p3]
	// q_pts90 = [p5,p6,p7,p8]
	// 
	// If p_angle=0, and p_pts not given, p_pts90 will
	// be the final points on p end for polyhedron 

	// p_pts90 and q_pts90 decides the points of side lines. 
	// It doesn't matter if cutAngleAfterRotate is true, 'cos 
	// the position of side lines is independent of cut-angle.
	// So we use rot_pqr_for_p. It will be = rot_pqr_for_p
	// If rotate=0 (then R2=R1).
	// 
	// Later we will project these lines to p_plane and q_plane,
	// Those planes are what decides the final sideline pts. 
	p_pts90= arcPts( rot_pqr_for_p
				  , r=r, a=360, pl="yz", count=sides );

	q_pts90= arcPts( rot_pqr_for_q
				   , r=r, a=360, pl="yz", count=sides );

	//Line0( [P, p_pts90[0] ],["r",0.05] );
	//MarkPts( [P2],["colors",["purple"]] );
	//MarkPts( p_pts90 );
	//Chain( p_pts90 );

	//-------------------------------------------------
	// Cut an angle from P end. 
	//
	// If cutAngleAfterRotate, we want to use the pqr 
	// BEFORE rotate.
	Ap = anyAnglePt( cutAngleAfterRotate
					? [P2,P,R1]
					: [P2,P,R2]
					//rot_pqr_for_p 
					// cutAngleAfterRotate
					//? norot_pqr_for_p
					//: rot_pqr_for_p 
				  , a=p_angle+(90-p_angle)*2
				  , pl="xy"); 
	Np = N([ Ap, P, Q ]);  // Normal point at P
	//echo("Nh= ", Nh);
	//MarkPts( [Ah], ["labels",["Ah"]] );
	p_plane = [Ap, Np, P ];  // The head surface 
	//Plane( headplane, ["mode",4] );
	p_pts=or( ops("p_pts")
			  , p_angle==0
			   ? p_pts90
			   : 	[ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], p_plane ) ]
			  );

	//-------------------------------------------------
	// Cut an angle from Q end. 
	Aq = anyAnglePt( cutAngleAfterRotate
					? [P,Q,R1]
					: [P,Q,R2]
					//rot_pqr_for_q
					// cutAngleAfterRotate
					//? rot_pqr_for_q
					//: norot_pqr_for_q 
				  , a=q_angle //90-q_angle
				  , pl="xy"); // Angle point for tail
	Nq = N( cutAngleAfterRotate
					? norot_pqr_for_q
					: rot_pqr_for_q );  // Normal point at Q
	//echo("Nt= ", Nt);
	//MarkPts( [At], ["labels",["At"]] );

	// q_plane is the surface of the final pts landed. Final pts
	// are determined by extending the side lines to meet the 
	// q_plane where the intersections are those final pts are. 
	//
	// These side lines will be the same no matter it's rotate-cut_angle
	// or cut_angle-then-rotate. 
	//
	// The q_plane, however, will be different (cut_angle before or
	// after rotate). 

	q_plane = [Aq, Nq, Q ];//:pqr0;  // The head surface 
	//Plane( headplane, ["mode",4] );
	q_pts=or( ops("q_pts")
			  , q_angle==0
			    ? q_pts90
			    : [ for(i=range(sides)) 
					lpCrossPt( [q_pts90[i],p_pts90[i]], q_plane ) ]
			  );


//	echo("rot = ", rot);
//	echo("angle( [ p_pts[0], P, Q] )= ", angle( [ p_pts[0], P, Q] ));
//	echo("angle( [ q_pts[0], Q, P] )= ", angle( [ q_pts[0], Q, P] ));

//	if( tailangle==90 ){ tailpts = tailpts90
//	}

	allpts = concat( p_pts, q_pts );

	_label = ops("label");
	label= _label==true?"PQ":_label;

	//if(ops("showsideindex")) LabelPts( allpts, range(allpts) ) ;
	if(label) LabelPts( pq, label ) ;

	p_faces =  range(sides); 
	q_faces = range(sides, sides*2); 
	faces = concat( [p_faces], [q_faces], rodsidefaces(sides) );
	//echo("faces: ", faces);


	//=========== marker and label =============
	allrange= range(allpts);
	markrange = hash(ops, "markrange" , if_v=true, then_v=allrange);
	labelrange= hash(ops, "labelrange", if_v=true, then_v=allrange);
	markops = or(hash(ops, "markops"),[]); 
	labelops= or(hash(ops, "labelops"),["labels",""]); 
	pts_to_mark = [ for(i=markrange)	allpts[i] ];
	pts_to_label= [ for(i=labelrange)	allpts[i] ];

	// We make them BEFORE the polyhedron so they are always visible
	if(markrange){ MarkPts( pts_to_mark, markops); }
	if(labelrange){ LabelPts( pts_to_label, hash(labelops, "labels"), labelops); }
	// =========================================

	color( ops("color"), ops("transp") )
	polyhedron( points = allpts 
			  , faces = faces
			 );

	// We make marker AFTER the polyhedron so they are hidden behind
	// the object
	
//	echo_( "markrange={_}, markops={_}",[markrange,markops]);
//	echo_( "labelrange={_}, labelops={_}",[labelrange,labelops]);

	//
	// To check if the p-, q-angle work:
	//
//	echo_("Rod: cutAngleAfterRotate:{_}, p_angle={_}, angle( [Q, P, allpts[0]] )={_} "
//		, [cutAngleAfterRotate, p_angle, angle( [Q, P, allpts[0]] )] 
//		);
//	echo_("Rod: cutAngleAfterRotate:{_}, q_angle={_}, angle( [P, Q, allpts[{_}]] )={_} "
//		, [cutAngleAfterRotate, q_angle, sides, angle( [P,Q, allpts[sides]] ) ]
//		);
  


	//==========================================
	if( ops("echodata"))
	{
		echo(
			 "<b>Rod()</b>:<br/><b>ops</b> = ", ops
			,join([""
			,str("<b>q_pts</b> = ", q_pts )
			,str("<b>p_pts</b> = ", p_pts )
			,str("<b>faces</b> = ", faces )
			],";<br/>"));
	}
}

module Rod_demo_0()
{ _mdoc("Rod_demo_0"
,
 " Basic Rod() operation with the following code:
;;
;;  Rod( pqr, [''r'',1, ''sides'',5, ''labelrange'',true,''transp'',0.4] ); 
;;
;; The surface of pqr and side-indices are shown. Note how the point
;; indices are arranged. "
);	

	pqr = randPts(3);
	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",1, "sides",5, "labelrange",true,"transp",0.4] );
}
//Rod_demo_0();



module Rod_demo_1()
{ _mdoc("Rod_demo_1"
,
 " More demos showing varying sides. Default: [''sides'',6]. "
);

	Rod();

	Rod(randPts(2), ["color","red", "r",0.3, "echodata",true] );

	Rod( ops=["color","green", "r",0.5,"sides",4] );

	Rod( ops=["color","blue", "r",0.4,"sides",20] );
}
//Rod_demo_1();



module Rod_demo_2_labels()
{
_mdoc("Rod_demo_2_labels",
" Demos showing marker and label. Check out MarkPts() and LabelPts() 
;; for detailed options. 
;; 
;; dfcolor: [''label'',true] => show PQ
;; dfcolor: [''label'',''AB''] => show A,B
;; red    : [''labelrange'', true] => show all indices
;; green  : [''markrange'', true] => mark all pts
;; blue   : [''markrange'', [0,2,4,6,8,10]] => mark selected pt
;; purple : [''labelrange'',[0,6]
;;          ,''labelops'',[''labels'',[''1st'',''2nd'']]
;;          ,''markrange'',[0,6]
;;          ]
;; gray   : [''labelrange'',true
;;          , ''labelops'',[''color'',''red'',''scale'',0.02]
;;          , ''markrange'',[0,6]
;;          ]
");

	Rod(ops=["transp",0.5, "r",0.3,"label",true]);
	Rod(ops=["transp",0.5, "r",0.3,"label","AB"]);
	
	Rod(ops=["color","red","transp",0.3, "r",0.6
			, "labelrange", true
			]);

	Rod(ops=["color","green","transp",0.3, "r",0.3
			, "markrange",true]);

	Rod(ops=["color","blue","transp",0.3, "r",0.6
			, "markrange",[0,2,4,6,8,10]]);

	Rod(ops=["color","brown","transp",0.3, "r",0.6
			, "labelrange",[0,6]
			, "labelops",["labels",["1st","2nd"]]
			, "markrange",[0,6]
			]
		);

	Rod(ops=["color","gray","transp",0.3, "r",0.6
			, "labelrange",true
			, "labelops",["color","red","scale",0.02]
			, "markrange",[0,6]
			]
		);

}
//Rod_demo_2_labels();



module Rod_demo_3_angles()
{
_mdoc("Rod_demo_3_angles",
" p_angle, q_angle
;;
;; yellow one: 60, 45
;; red one: 30, 60
;;
;; default (when not given) = 90
");

	pqr = randPts(3);

	Chain(pqr, ["r",0.05]);

	MarkPts( pqr, ["labels", ["P(60)","Q(45)","R"]] );

	Rod(pqr, ops=["transp",0.5, "r",1, "showsideindex",true
			, "p_angle",60
			, "q_angle",45
			, "echodata", true
			]);


	pqr2 = randPts(3);

	Chain(pqr2, ["r",0.05]);

	MarkPts( pqr2, ["labels", ["P(30)","Q(60)","R"]] );

	Rod(pqr2, ops=["transp",0.5, "r",1, "showsideindex",true
			, "p_angle",30
			, "q_angle",60
			, "echodata", true
			, "color","red"
			]);
}
//Rod_demo_3_angles();



module Rod_demo_4_angles()
{
_mdoc("Rod_demo_4_angles",
" Show how can the p_angle, q_angle be applied to join rods. 
;;
;; Note that this is just for demo. The joints in this demo
;; actually have two exact same faces overlapping. 
");

	pqr = randPts(3);
	rqp = p210(pqr);

	Chain(pqr);

	M = angleBisectPt( pqr );
	angle = angle(pqr)/2;
	MarkPts(pqr, ["labels","PQR"] );
	Rod(pqr, ops=["transp",0.5, "r",0.4
				//,"label",true, "showsideindex",true
				, "q_angle", angle //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp, ops=["transp",0.5, "r",0.4
				//,"label",true, "showsideindex",true
				, "q_angle", angle // angle( [ P(pqr), R(pqr), M] )
				]);
	
	//==========================
	pqr2 = randPts(3);
	rqp2 = p210(pqr2);

	//Chain(pqr);

	M2 = angleBisectPt( pqr2 );
	angle2 = angle(pqr2)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("red",0.7){
	Rod(pqr2, ops=["transp",0.2, "r",0.3, "sides",3
				//,"label",true, "showsideindex",true
				, "q_angle", angle2 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp2, ops=["transp",0.2, "r",0.3, "sides",3
				//,"label",true, "showsideindex",true
				, "q_angle", angle2 // angle( [ P(pqr), R(pqr), M] )
				]);
	}

	//==========================
	pqr3 = randPts(3);
	rqp3 = p210(pqr3);

	//Chain(pqr);

	M3 = angleBisectPt( pqr3 );
	angle3 = angle(pqr3)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("green", 0.7){
	Rod(pqr3, ops=["transp",0.2, "r",0.3, "sides",4
				//,"label",true, "showsideindex",true
				, "q_angle", angle3 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp3, ops=["transp",0.2, "r",0.3, "sides",4
				//,"label",true, "showsideindex",true
				, "q_angle", angle3 // angle( [ P(pqr), R(pqr), M] )
				]);
	}

	//==========================
	pqr4 = randPts(4);
	rqp4 = p210(pqr4);

	//Chain(pqr);

	M4 = angleBisectPt( pqr4 );
	angle4 = angle(pqr4)/2;
	//MarkPts([M2], ["labels",["M2"]] );
	color("blue", 0.7){
	Rod(pqr4, ops=["transp",0.2, "r",0.3, "sides",18
				//,"label",true, "showsideindex",true
				, "q_angle", angle4 //angle( [ P(pqr), Q(pqr), M] )
				]);

	Rod(rqp4, ops=["transp",0.2, "r",0.3, "sides",18
				//,"label",true, "showsideindex",true
				, "q_angle", angle4 // angle( [ P(pqr), R(pqr), M] )
				]);
	}
}
//Rod_demo_4_angles();



module Rod_demo_5_user_q_pts()
{
_mdoc("Rod_demo_5_user_q_pts",
" This demo shows how to use customized q_pts
;; with expanded q_pts.
;;
;; The pts when angle=90
;;
;;   q_pts90 = arcPts( pqr, r=r, a=360, pl=''yz'', count=sides ); 
;;
;; Expand them to twice the r
;;
;;   ex_q_pts90= expandPts( q_pts90, dist=2*r ); 
;;
;;   Rod( pqr, [''q_pts'', ex_q_pts90, ...] ); 
");

	pqr = randPts(3);
	
	//P2 = onlinePt( p01(pqr), len=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);

	sides = 6;
	r = 0.5;
	
	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=sides );

	// Expand them to twice the r
	ex_q_pts90= expandPts( q_pts90, dist=2*r );

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",r, "q_pts", ex_q_pts90
			 , "sides",sides
			 , "showsideindex",true,"transp",0.4] );
}
//Rod_demo_5_user_q_pts();



module Rod_demo_6_user_q_pts()
{
_mdoc("Rod_demo_6_user_q_pts",
" Another demo shows how to use customized q_pts.
;;
;;   sides=24; 
;;   spark_ratio=3; 
;;
;; The pts when angle=90
;;
;;   q_pts90 = arcPts( pqr, r=r, a=360, pl=''yz'', count=sides ); 
;;
;; Construct the points:
;;
;;   pts= [for(i=range(sides))
;;          mod(i,3)==0? onlinePt( [Q, q_pts90[i]], ratio=spark_ratio)
;;                     : q_pts90[i]
;;        ];
;;
;;   Rod( pqr, [''q_pts'', pts, ...] ); 
");

	// This demo shows how to use customized headpts

	pqr = randPts(3);
	
	//P2 = onlinePt( p01(pqr), len=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);

	sides = 24;
	r = 0.5;
	spark_ratio = 3;

	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=sides );

	pts= [for(i=range(sides))
			mod(i,3)==0? onlinePt( [Q, q_pts90[i]], ratio=spark_ratio)
						: q_pts90[i]
		];

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",r, "q_pts", pts
			 , "sides",sides
			 , "showsideindex",false
			,"transp",1] );
}
//Rod_demo_6_user_q_pts();



module Rod_demo_7_gear()
{
_mdoc("Rod_demo_7_gear",
 " This demo shows how to use Rod() to make a simple gear-like
;; structure using customized p_pts/q_pts
;;
;; Assuming each tooth has 6 pts (defined as pt_per_tooth):
;;      __
;;     /  ^      23 
;;     |  |    1    4
;;  ---+  +    0    5
;;
;; We extend the point certain distance outward according to 
;; mod(i, pt_per_tooth). 
");     
	

	_pqr = randPts(3);
	
	pqr = newx( _pqr, onlinePt( p10(_pqr), len=0.5) );
	//P2 = onlinePt( p01(pqr), dist=-1);

	P=P(pqr);
	Q=Q(pqr);
	R=R(pqr);
	P2=onlinePt( [Q,P], len=1);

	teeth = 18;
	pt_per_tooth = 6;
	sides = teeth* pt_per_tooth;
	r = 1; //0.5;
	tooth_height=0.2;

	// The pts when angle=90
	q_pts90 = arcPts( pqr, r=r, a=360, pl="yz", count=sides );
	
	//tailpts90 = arcPts( [P2,P,R], r=r, a=360, pl="yz", count=sides );

	gear_pts_q= [for(i=range(sides))
			 mod(i,pt_per_tooth)==1||mod(i,pt_per_tooth)==4
			  ? onlinePt( [q_pts90[i],Q], len=-tooth_height*1/2)
			  : mod(i,pt_per_tooth)==2||mod(i,pt_per_tooth)==3
			    ? onlinePt( [q_pts90[i],Q], len=-tooth_height)
			//:mod(i,pt_per_tooth)==0|| mod(i,pt_per_tooth)==5
				: q_pts90[i]
		];

	dpq = P-Q;
	gear_pts_p = [for(p=gear_pts_q) p+dpq];

	//=========================

	MarkPts( pqr, ["labels","PQR"] );
	Plane(pqr, ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",5, "len",0.5
			, "q_pts", gear_pts_q
			, "p_pts", gear_pts_p
			 , "sides",sides
			 , "showsideindex",false
			,"transp",0.8] );
}
//Rod_demo_7_gear();



module Rod_demo_8_rotate()
{
sides=6;
_mdoc("Rod_demo_8_rotate",
_s(" red: rotate {_} degree
;; green: rotate {_} degree
;; 
;; Note how the rotation goes (counter clock-wise)
", [360/sides/2, 360/sides])
);
	
	pqr = randPts(3);
	pqr=[[-2.73324, -2.05788, -2.44777], [-0.285864, -1.8622, -1.77504], [2.84959, 2.00984, -2.64569]];
	P=P(pqr); Q=Q(pqr); R=R(pqr);

	MarkPts( pqr, ["labels","PQR"] );
	Plane( [ onlinePt( [Q,P], ratio=2)
		   , Q(pqr)
		   ,onlinePt( [Q,R], ratio=2)
		   ] ,  , ["mode",3]);
	Chain( pqr );
	Rod( pqr, ["r",1, "sides",sides, "markrange",[0],"labelrange",[0],"transp",0.9] );
//
	// 2014.8/21: 
	// Found that sometimes the rotation goes to the opposite direction
	// 
	//	It seems that it occurs in (2) case:
	//
	//	(1)
	//    
	//        R._
	//           '-._
	//	p---T------'Q
	//
	//
	//	(2)    
	//        R._
	//           '-._
	//               '-._
	//	    T  p-------'Q
	//
	// Fixed in 20140821-3

	//echo("pqr: ", pqr);
	T = othoPt( [P,R,Q] );
	echo( "Is TQ > PQ ? ", norm(T-Q)> norm(P-Q) );


	P=P(pqr); Q=Q(pqr); R=R(pqr);

	Rod( [ onlinePt( [P,Q], len=0.3)
		 , onlinePt( [Q,P], len=0.3)
		 , R ]
		, ["r",2, "sides",sides,"markrange",[0],"labelrange",[0],"color","red", "transp",0.6
			  , "rotate", 360/sides/2] );

	Rod( [ onlinePt( [P,Q], len=0.5)
		 , onlinePt( [Q,P], len=0.5)
		 , R ]
		, ["r",3, "sides",sides,"markrange",[0],"labelrange",[0],"color","green", "transp",0.3
			  , "rotate", 360/sides] );
}
//Rod_demo_8_rotate();




module Rod_demo_8_1_q_rotate() // give up
{
_mdoc("Rod_demo_8_1_q_rotate",
" We give up this appoach of making chain using Rod with both 
twistangle and p- q-angle specified. 'cos it's too difficult 
to get both of twistangle and p- q-angle right. 2014.8.29

");

	pqr = randPts(3);
	pqrs1 = concat( pqr, [randPt()] );

	shift = normalPt(pqr,len=1.5)-pqrs1[1];

	pqrs2= [for(p=pqrs1) p+shift];
	pqrs3 = [for(p=pqrs2) p+shift];

pqrs1= [[2.26761, 1.21599, 0.51161], [0.447378, -2.10168, 0.613161], [-0.582338, -1.77329, 1.33888], [-2.13944, 0.683927, -2.8544]];
pqrs2= [[3.02213, 0.840008, 1.75231], [1.20189, -2.47767, 1.85386], [0.172176, -2.14928, 2.57958], [-1.38492, 0.307941, -1.6137]];
pqrs3=  [[3.77664, 0.464021, 2.99301], [1.95641, -2.85365, 3.09457], [0.92669, -2.52527, 3.82029], [-0.630407, -0.0680459, -0.372994]];

	aPQR = angle( pqr ) /2;
	aQRS = angle( p123(pqrs1) )/2;
	taPQRS= twistangle( pqrs1 ); 

	echo("aPQR=", aPQR);
	echo("aQRS=", aQRS);
	echo("taPQRS=", taPQRS);
	 
	echo( "shift=", shift);
	echo( "pqrs1=", pqrs1);
	echo( "pqrs2=", pqrs2);
	echo( "pqrs3=", pqrs3);
	
	MarkPts( pqrs1, ["labels", "PQRS"] );

	ops= ["r",0.4, "transp",0.9];

	Chain( pqrs1 );
	Rod( p012(pqrs1) , concat(["q_angle",aPQR],ops) );
	Rod( p123(pqrs1) , concat(["p_angle",aPQR, "q_angle",aQRS
						, "rotate", -taPQRS  // Rod_QR is calc based on QRS, so 
												// need to make a twiste
						, "cutAngleAfterRotate",false // aPQR is calc based on PQR,                                                         
													 // that is BEFORE rotate
						//, "_rotate", taPQRS  // need to rotate q-end back to QRS 
							],ops) );
//	Rod( p123(pqrs1) , concat(["p_angle",aPQR, "q_angle",aQRS, "rotate", taPQRS, "cutAngleAfterRotate",true],ops) );
	Rod( p231(pqrs1) , concat(["p_angle",aPQR],ops) );

//====================================
	
//	opqr1 = randPts(4);
//	shift = normalPt(p012(opqr1),len=1.5)-opqr1[1];
//	//Normal(p012(opqr1),["color","red","len",3]);
//	opqr2 = [for(p=opqr1) p+shift];
//	opqr3 = [for(p=opqr2) p+shift];
//	//pts = [pqr, pqr2, pqr3];
//	
//	//ap = angle( opqr )/2 ;
//	aOPQ = angle( opqr1 )/2;
//	aPQR = angle( p123(opqr1) )/ 2 ;
//
//	taOPQ= twistangle( reverse(opqr1) );
//
//	echo("aPQR=", aPQR);
//	echo("aOPQ=", aOPQ);
//	echo("taOPQ=", taOPQ);
//	 
//	echo( "shift=", shift);
//	echo( "opqr1=", opqr1);
//	echo( "opqr2=", opqr2);
//	echo( "opqr3=", opqr3);
//	echo( "p123(opqr1) = ", p123(opqr1));
//	echo( "slice(opqr1,1)",slice(opqr1,1));
//
//	
//	MarkPts( opqr1, ["labels", "OPQR"] );
//
//	ops= ["r",0.4, "transp",0.9];
//
//	Chain( opqr1 );
//	Rod( p012(opqr1) , concat(["q_angle",aOPQ],ops) );
//	Rod( p123(opqr1) , concat(["p_angle",aOPQ, "q_angle",aPQR, "rotate", taOPQ, "cutAngleAfterRotate",true],ops) );
//	Rod( p231(opqr1) , concat(["p_angle",aPQR],ops) );
//	
//	Chain( opqr2, ["color","red"]);
//	Rod( p123(opqr1) , concat(["q_angle",aPQR],ops) );
//	Rod( p321(opqr1) , concat(["q_angle",aPQR],ops) );
////
////	Chain( opqr3, ["color","green"] );
////	Rod( p123(opqr3) , ops );
////	Rod( reverse(p123(opqr3)) , concat([ ], ops) );


}

//Rod_demo_8_1_q_rotate();



module Rod_demo_9_rotate_and_angles()
{
_mdoc("Rod_demo_9_rotate_and_angles"
," This demo shows cases when both rotate and p_angle/q_angle are set.
;; It is tricky 'cos the order makes difference. 
;;
;; First we make two copies of a same Rod side by side (yellow), then 
;; add a larger one to each. Make both p_angle and q_angle = 45. 
;; 
;; red: cutAngleAfterRotate= true // default :  rotate then cut
;; green: cutAngleAfterRotate= false // cut then rotate
;;
;; The blue dots on them show how the first point rotates by 30 degree.
");

	sides=6;

	pqr = randPts(3);
	pqr2 = [for(p=pqr) p+ normalPt(pqr,len=4)-Q(pqr)];
	
	for(pts=[pqr,pqr2])
	{
		Chain( pts ,["r",0.02, "color","blue"]);
		
		MarkPts( pts, ["labels","PQR"] );
		
		Plane(pts, ["mode",3]);
		
		Rod( pts, ["r",1.7, "sides",sides, "transp",1
				  ,"markrange",[0]
				  ,"labelrange",[0]
				] 
				);
	}	

	xpqr = concat( linePts( p01(pqr), dist=[0.1,0.1]), [R(pqr)]);
	xpqr2= [for(p=xpqr) p+ normalPt(xpqr,len=4)-Q(xpqr)];

	echo( "xpqr: ", xpqr );
	echo( "xpqr2:", xpqr2);

	ops=[ "r",2
		, "sides",sides
		, "labelrange",true
		, "transp",0.7, 
		, "markrange",[0]
		, "markops",["colors",["blue"]]
		, "p_angle",45
		, "q_angle",45
		, "rotate", 360/sides/2
		];

	Rod( xpqr, concat( ["color","red"  , "cutAngleAfterRotate", true],ops ));
	Rod( xpqr2,concat( ["color","green", "cutAngleAfterRotate", false],ops) );


}
//Rod_demo_9_rotate_and_angles();


//==========================
module Rod_demo_10_join_rotate()
{
_mdoc( "Rod_demo_10_chain_rotate"
, " Show the role of cutAngleAfterRotate in making Rod joint when
;; rotation is needed. 
;;
;; dfcolor: no rotate 
;; red    : rotate. cutAngleAfterRotate= true (default)
;; green  : rotate, cutAngleAfterRotate= false
;;
");
	pqr = randPts(3);
	pqr2 = [for(p=pqr) p+ normalPt(pqr,len=1.5)-Q(pqr)];
	pqr3 = [for(p=pqr) p+ normalPt(pqr,len=3)-Q(pqr)];
	pts = [pqr, pqr2, pqr3];

	colors=[ undef, "red", "green" ];
	rotates=[ 0, 45, 45 ];
	caar  =[ undef, true, false ];

	for(i= range(3))
	{
		//Chain( pts[i] ); // ,["r",0.02, "color","blue"]);

		JRod( pts[i]
			, rotate = rotates[i]
			, ops = [ "cutAngleAfterRotate", caar[i]
					, "color",colors[i]
					, "transp", 0.8
					, "r", 0.5
					, "sides", 4
					, "label",true
					]
			);

	}	
	jr_ops= [ "transp", 0.7
			, "r", 0.5
			, "sides", 4
			, "label",true
			];

	module JRod(pqr=randPts(3), rotate=0, ops=[] )
	{
		Chainf(pqr, ["closed",false]);

		P=P(pqr); Q=Q(pqr); R=R(pqr); 
		echo("Rod_demo_10_chain_rotate.pqr=", pqr);
		angle = angle(pqr)/2; 
		//echo("angle= ", angle);
		rqp= [R,Q,P];
		Rod( pqr, concat(["rotate", rotate,"q_angle",angle],ops,jr_ops) );
		Rod( rqp, concat(["rotate",-rotate,"q_angle",angle],ops,jr_ops) );
	}		
}

//==========================
module Rod_demo_11_chain()
{
_mdoc( "Rod_demo_11_chain"
, " Show the role of cutAngleAfterRotate in making Rod joint when
;; rotation is needed. 
;;
;; dfcolor: no rotate 
;; red    : rotate. cutAngleAfterRotate= true (default)
;; green  : rotate, cutAngleAfterRotate= false
;;
");
	sides = 36;
	count = 4;
	
	linepts= randPts(count+1);

	//Chain( linepts );
	//MarkPts( linepts, ["labels",true] );

	_allrps = [for(i=range(count-1))
				let( pqr = [ linepts[i], linepts[i+1], linepts[i+2] ]
				   , opqr= [ linepts[i-1],  linepts[i], linepts[i+1], linepts[i+2] ]
				   , ang = angle(pqr)/2
				)
				rodPts( pqr
					  , ["r",0.5, "sides", sides
						,"q_angle", i>=count-2?90:ang
						,"q_rotate", i==0?0:twistangle(opqr)
						]
					  )
			 ];

//	allrps = concat( _allrps[0][0], [ for(rp=_allrps) rp[1] ]);
	allrps = concat( _allrps[0][0], joinarr( [ for(rp=_allrps) rp[1] ] ));
	//LabelPts( allrps, labels="");
	//Chain( allrps );					

//	p00  = onlinePt( p01(linepts), len=-1 );
//	plast= onlinePt( [get(linepts,-1), get(linepts,-2)] , len=-1 );
//
//	linepts2= concat( [p00], linepts, [plast] );
//
//	Chain( linepts2, ["transp",0.5, "r",0.3, "color","red"] );
//
//	//rp = rodPts( linepts,["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	//Chain( rp );
//
//	rp1 = rodPts( slice(linepts, 0,3),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp1 );
//
//	rp2 = rodPts( slice(linepts, 1,4),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp2 );
		
//	pts= joinarr( 
//			[for(c=range(1,count+1))
//				let( pqr=[ linepts2[c-1]
//						, linepts2[c]
//						, linepts2[c+1]
//						]
//				   , ang = angle(pqr)/2
//					)
//				rodPts(pqr,["sides",sides,"q_angle", ang] )
//			]
//		);

//[ "r", 0.05
//	  , "sides",     6
//	  , "label", "" 
//	  , "p_pts",undef  // pts on the P end
//	  , "q_pts",undef  // pts on the q end
//	  , "p_angle",90   // cutting angle on p end
//	  , "q_angle",90   // cutting angle on q end
//	  , "rotate",0  // rotate about the PQ line, away from xz plane
//	  , "cutAngleAfterRotate",true // to determine if cutting first, or
//						  // rotate first. 
//	  ]
	faces = faces("chain", sides=sides, count=count);

//	echo("faces_demo_5_chain.N=", N);
	echo("<br/>faces_demo_5_chain._allrps=", _allrps);
	echo("<br/>faces_demo_5_chain.allrps=", allrps);
	//echo("<br/>faces_demo_5_chain.pts=", pts);
	echo("<br/>faces_demo_5_chain.faces=", faces);
	//color(false, 0.8)
	polyhedron( points = allrps
			 , faces = faces
			 );

}

//Rod_demo_0();
//Rod_demo_1();
//Rod_demo_2_labels();
//Rod_demo_3_angles();
//Rod_demo_4_angles();
//Rod_demo_5_user_q_pts();
//Rod_demo_6_user_q_pts();
//Rod_demo_7_gear();
//Rod_demo_8_rotate();
//Rod_demo_8_1_q_rotate(); // given-up appoach, wait to be removed. 2014.8.29
//Rod_demo_9_rotate_and_angles();
//Rod_demo_10_join_rotate();
//Rod_demo_11_chain();
//Rod_demo_12_addfaces();

module Rod_demo_12_addfaces()
{
	echom("Rod_demo_11_addfaces");
	r = 1.5;
	sides = 6;
	pqr = randPts(3);

	Chain(pqr);
	rodpts = rodPts( pqr, ["r",1.5, "sides", sides] );
//	
//	echo( "rodpts: ", rodpts);
//	echo( "joinarr(rodpts): ", joinarr(rodpts) );
//	LabelPts( join(rodpts),true );

	MarkPts( joinarr(rodpts), ["labels",true] );
//	inner_p_pts = reverse( arcPts( p102(pqr), r=r*2/3, a=360, pl="yz", count=sides));    
//	inner_q_pts =  arcPts( pqr, r=r*2/3, a=360, pl="yz", count=sides);    

	inner_p_pts = expandPts( rodpts[0], dist=-0.5 );
	inner_q_pts = expandPts( rodpts[1], dist=-0.5 );

	echo(inner_p_pts, inner_q_pts );
	Chain(inner_p_pts);
	Chain(inner_q_pts);

	faces = rodsidefaces(sides);

	Rod( pqr,  ["r",1.5, "sides", sides, "label",true, "transp", 0.7

		,"has_p_faces",false
		,"has_q_faces",false
		,"addpts", concat( inner_p_pts, inner_q_pts)
		,"addfaces", concat( [for(f=faces) f+[sides*2,sides*2,sides*2,sides*2] ] )
	]);
}

module Rod_demo_12_hole()
{
_mdoc( "Rod_demo_12_hole"
,"
");
	sides=4;
	pqr = randPts(3);
	Chain(pqr);
	//MarkPts( pqr, ["labels","PQR"] );
	Rod( pqr, ["r",.7, "transp",0.8, "sides",sides
			  ,"label",true
				, "labelrange",true
				, "labelops", ["begnum",sides*2, "color","red" ] 
			  ] );  

	Rod( pqr, ["r",2, "transp",0.2, "sides",sides
			  ,"label",true
				, "labelrange",true] );  

//    p_faces=	 [ [0,3,11, 8]
//			 , [1,0, 8, 9]
//			 , [2,1, 9,10]
//			 , [3,2,10,11] ];
//
//	q_faces=	 [ [4,5,13,12]
//			 , [5,6,14,13]
//			 , [6,7,15,14]
//			 , [7,4,12,15] ]

}

//Rod_demo_12_hole();



//========================================
faces=["faces","shape,sides,count", "array", "Array, Geometry",
 " Given a shape (''rod'', ''cubesides'', ''tube'', ''chain''), an int
;; (sides) and an optional int (count), return an array for faces of
;; a polyhedron for that shape.
"];

module faces_test( ops=["mode", 13] )
{
    doctest( faces,
	ops=ops
	);
}


function faces(shape="rod", sides=6, count=3)= // count is used for shape that has
 let( s = sides                                // segments unknown until run-time
	, s2= 2*sides                              // For example, chain
	, s3= 3*sides
)(
	// cube, cubesides
	//       _6-_
	//    _-' |  '-_
	// 5 '_   |     '-7
	//   | '-_|   _-'| 
	//   |    |-4'   |
	//   |   _-_|    |
	//   |_-' 2 |'-_ | 
	//  1-_     |   '-3
	//     '-_  | _-'
	//        '-.'
	//          0
	shape=="cubesides"
	? [ for(i=range(s))
		  [ i
			, i==s-1?0:i+1
			, (i==s-1?0:i+1)+s
			, i+s
		  ]
	  ]
 	: shape=="rod"
	? concat( [ reverse(range(s))], [range(s,s2) ],
			  faces("cubesides", s) ) 
	// tube
	//             _6-_
	//          _-' |  '-_
	//       _-'   14-_   '-_
	//    _-'    _-'| _-15   '-_
	//   5_  13-'  _-'  |      .7
	//   | '-_ |'12_2-_ |   _-' | 
	//   |    '-_'|    '|_-'    |
	//   |   _-| '-_10_-|'-_    |
	//   |_-'  |_-| 4' _-11 '-_ | 
	//   1_   9-  | |-'       _-3
	//     '-_  '-8'|      _-'    
	//        '-_   |   _-'
	//           '-_|_-'
    //              0
	: shape=="tube"
	? concat( faces( "cubesides", s )  // outside sides
			, [ for(f=faces( "cubesides", s ))    // inner sides
					reverse( [ for(i=f) i+s2] )
			  ]
			, [ for(i=range(s))        // bottom faces
				  [i, mod(i+s-1,s), mod(i+s-1,s)+s2, i+s2 ] 
			  ]
			, [ for(i=range(s,s2))    // top faces
				  [i, i==s2-1?s:i+1, i==s2-1?s3:i+s2+1, i+s2 ]
			  ]
			)	  
	
			//  bottom=	 [ [0,3,11, 8]
			//			 , [1,0, 8, 9]
			//			 , [2,1, 9,10]
			//			 , [3,2,10,11] ];
			//
			//	top	=	 [ [4,5,13,12]
			//			 , [5,6,14,13]
			//			 , [6,7,15,14]
			//			 , [7,4,12,15] ]

	//                 
	//       _6-_-------10------14
	//    _-' |  '-_    | '-_   | '-_
	//  2'_   |     '-5-----'9-------13
	//   | '-_|   _-'|  |    |  |    |
	//   |    |-1'   |  |    |  |    |
	//   |   _-_|----|-11_---|--15   |
	//   |_-' 7 |'-_ |    '-_|    '-_| 
	//  3-_     |   '-4------8-------12
	//     '-_  | _-'
	//        '-.'
	//          0
	//
	:shape=="chain"
	? concat( [ reverse(range(s))]            // starting face
			, [ range( count*s,(count+1)*s) ] // ending face
			, joinarr( [ for(c=range(count))
				 [ for(f=faces("cubesides", sides))
					[ for(i=f) i+c*sides]
				 ]
			  ])
			)  
	:[]
);

module faces_demo_1()
{
	pqr = randPts(3);
	S= cornerPt(pqr);
	pqrs = concat( pqr, [S]);
	
	vector = randPt();

	tuvw = [for(p=pqrs) p+vector ];

	pts = concat( pqrs, tuvw );

	MarkPts( pts, ["labels",true] );
    
    LabelPt( pts[0], "  faces=faces(\"rod\",sides=4)");
	polyhedron( points = pts
			 , faces = faces("rod", sides=4)
			 );
}
//faces_demo_1();

module faces_demo_2_cube()
{
	pqr = randRightAnglePts();
	S= cornerPt(pqr);
	pqrs = concat( pqr, [S]);
	
	vector = normal(pqr);

	tuvw = [for(p=pqrs) p+vector ];

	pts = concat( pqrs, tuvw );

	MarkPts( pts, ["labels",true] );

	polyhedron( points = pts
			 , faces = faces("rod", sides=4)
			 );
}
//faces_demo_2_cube();


module faces_demo_3_cylinder()
{
	pqr = randPts(3);
    MarkPts( pqr );
    
	Q = Q(pqr);
	N = normalPt( pqr );
	Chain(pqr);
	sides = 8;

	pts= joinarr(rodPts( pqr, ["r",2,"sides",sides] )); //arcPts( pqr, r=2, a=360, count=sides, plane="yz");
	//q_pts= [for(p=p_pts) p+(N-Q)];	

	//pts = concat( p_pts,q_pts );

	MarkPts( pts, ["labels",true] );

	faces = faces("rod", sides=sides);

//	echo("faces_demo_3_cylinder.N=", N);
//	echo("faces_demo_3_cylinder.p_pts=", p_pts);
//	echo("faces_demo_3_cylinder.q_pts=", q_pts);
	echo("faces_demo_3_cylinder.faces=", faces);
	color(false, 0.6)
	polyhedron( points = pts
			 , faces = faces
			 );
}
//faces_demo_3_cylinder();


module faces_demo_4_tube()
{
	pqr = randPts(3);
	Chain(pqr);
	MarkPts( pqr, ["labels","PQR"] );

	Q = Q(pqr);
	N = normalPt( pqr );
	G = onlinePt( [Q,N], len=3 );
	sides = 48;

	//p_pts= arcPts( pqr, r=2, a=360, count=sides, pl="yz");
	//q_pts= [for(p=p_pts) p+(G-Q)];	
	pts1 = joinarr(rodPts( pqr, ["r",2,"sides",sides] )); //concat( p_pts,q_pts );

	//p_pts2= expandPts( p_pts, dist=-1);
	//q_pts2= expandPts( q_pts, dist=-1);
	pts2 = joinarr(rodPts( pqr, ["r",1, "sides",sides])); //concat( p_pts2,q_pts2 );

	pts = concat(pts1,pts2);

	//MarkPts( pts, ["labels",true] );

	faces = faces("tube", sides=sides);

//	echo("faces_demo_4_tube.N=", N);
//	echo("faces_demo_4_tube.pts1=", pts1);
//	echo("faces_demo_4_tube.pts2=", pts2);
//	echo("faces_demo_4_tube.faces=", faces);
	color(false, 0.8)
	polyhedron( points = pts
			 , faces = faces
			 );
}
//faces_demo_4_tube();

module faces_demo_5_chain()
{
	sides = 4;
	count = 4;
	
	linepts= randPts(count+1);

	//Chain( linepts );
	//MarkPts( linepts, ["labels",true] );

	_allrps = [for(i=range(count-1))
				let( pqr = [ linepts[i], linepts[i+1], linepts[i+2] ]
				   , ang = angle(pqr)/2
				)
				rodPts( pqr
					  , ["r",0.5, "sides", sides
						,"q_angle", i>=count-2?90:ang
						]
					  )
			 ];

//	allrps = concat( _allrps[0][0], [ for(rp=_allrps) rp[1] ]);
	allrps = concat( _allrps[0][0], joinarr( [ for(rp=_allrps) rp[1] ] ));
	LabelPts( allrps, labels="");
	//Chain( allrps );					

//	p00  = onlinePt( p01(linepts), len=-1 );
//	plast= onlinePt( [get(linepts,-1), get(linepts,-2)] , len=-1 );
//
//	linepts2= concat( [p00], linepts, [plast] );
//
//	Chain( linepts2, ["transp",0.5, "r",0.3, "color","red"] );
//
//	//rp = rodPts( linepts,["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	//Chain( rp );
//
//	rp1 = rodPts( slice(linepts, 0,3),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp1 );
//
//	rp2 = rodPts( slice(linepts, 1,4),["sides",sides,"q_angle", ang, "r", 0.5] )[1];
//	Chain( rp2 );
		
//	pts= joinarr( 
//			[for(c=range(1,count+1))
//				let( pqr=[ linepts2[c-1]
//						, linepts2[c]
//						, linepts2[c+1]
//						]
//				   , ang = angle(pqr)/2
//					)
//				rodPts(pqr,["sides",sides,"q_angle", ang] )
//			]
//		);

//[ "r", 0.05
//	  , "sides",     6
//	  , "label", "" 
//	  , "p_pts",undef  // pts on the P end
//	  , "q_pts",undef  // pts on the q end
//	  , "p_angle",90   // cutting angle on p end
//	  , "q_angle",90   // cutting angle on q end
//	  , "rotate",0  // rotate about the PQ line, away from xz plane
//	  , "cutAngleAfterRotate",true // to determine if cutting first, or
//						  // rotate first. 
//	  ]
	faces = faces("chain", sides=sides, count=count);

//	echo("faces_demo_5_chain.N=", N);
	echo("<br/>faces_demo_5_chain._allrps=", _allrps);
	echo("<br/>faces_demo_5_chain.allrps=", allrps);
	//echo("<br/>faces_demo_5_chain.pts=", pts);
	echo("<br/>faces_demo_5_chain.faces=", faces);
	color(false, 0.8)
	polyhedron( points = allrps
			 , faces = faces
			 );

}
//faces_demo_5_chain();






module Rod_bugfixing()
{
	pqr=[[-2.73324, -2.05788, -2.44777], [-0.285864, -1.8622, -1.77504], [2.84959, 2.00984, -2.64569]];
	rqp= [R(pqr),Q(pqr),P(pqr)];
	Rod( pqr, ["r", 0.8, "transp", 0.7, "label",true
				, "labelrange",true, "labelops",["begnum",0,"prefix",""]]);
	Chain(pqr);
	MarkPts(pqr);

	//Rod( rqp, ["r", 0.8, "transp", 0.7, "label",true
	//			, "labelrange",true, "labelops",["begnum",0,"prefix",""]]);

}
//Rod_bugfixing();





//========================================================
RodCircle=["RodCircle", "ops", "n/a", "3D, Shape",
"
 Draw rods around a circle based on the given ops. Other than the
;; settings defined in the common ops OPS, this ops has the following
;; defaults:
;;
;; [ ''style'', ''rod'' // rod|block
;; , ''count'',  12
;; , ''normal'', [0,0,1] // the circle normal
;; , ''target'', ORIGIN  // the circle center
;; , ''dir''  , ''out'' // direction(out|in|up|down)
;; , ''radius'', 3
;; , ''r''    ,  0.1    // for style = rod
;; , ''w''    ,  0.4    // for style = block
;; , ''h''    ,  0.5    // for style = block
;; , ''markcolor'',false  // example:[1,''red'',4,''blue'']
;; , ''rodrotate'', 0
;;      // Rotation of individual rods about its own axis. Only works
;;      // for style=block. Note that OPS has a ''rotate'',too, that 
;;      // would be for the rotate of entire RodCircle about circle axis.
;; ]
"];



module RodCircle( ops )
{
	
	//echom("RodCircle");
	ops=updates( OPS,[
		[ "style", "rod"     // rod|block|cone
		, "count",  12  
		, "normal", [0,0,1]
		, "target", ORIGIN 
		, "dir"  , "out"     // out|in|up|down  (outward|inward...)
		, "radius", 3  
		, "r"    ,  0.1      // for style = rod
		, "w"    ,  0.4      // for style = bar
		, "h"    ,  0.5      // for style = bar
		, "markcolor",false  // [1,"red",4,"blue"]
		, "rodrotate", 0     // Rotation of individual rod about its axis. 
						   // Only works
						   //  for style=block. Note that OPS has a 
						   //  "rotate",too, that would be for the
						   //  rotate of entire RodCircle about its axis.
				
		], ops 
		]);	
	function ops(k)= hash(ops,k);

	style = ops("style");
	count = ops("count");
	color = ops("color");
	transp= ops("transp");
	fn    = ops("fn");
	normal= ops("normal");
	target= ops("target");
	dir   = ops("dir");
	rlen  = ops("len");
	r     = ops("r");
	h     = ops("h");
	radius= ops("radius");
	markcolor = ops("markcolor");
	rodrotate= ops("rodrotate");

	isupdown = dir=="up" || dir=="down";

	//echo("ops = ", ops );
	//echo("isupdown? ", isupdown);

	// For style = up|down, rods align with the normal. Because of the order
	// of the points that form the normal (or, the direction of the normal),
	// going upward or downward as desired requires special treatment. What
	// we do here is to, kind of, sort the points like what's been done in 
	// Line(), and chk if the two points has to switch, which will be the 
	// factor to set rod direction. 
	pq = [ target, normal + target ];
	switched= minyPts(minxPts(minzPts(pq)))[0]==pq[1];

	//echo("switched? ", switched );

	// Setup the points for the rods. Each rod starts from a begPt
	// and ends in a endPt. These pts are first created as 2d series
	// using arcPts then converted to 3d. All are on the xy-plane.  
	begPts = addz( arcPts( r=radius, a=360, count=ops("count") ) );
	endPts =  dir=="down"? addz( begPts, switched?-rlen:rlen )
		    : dir=="up"? addz( begPts, switched?rlen:-rlen )
			: addz(
				dir=="out"? arcPts( r=radius+rlen, a=360, count=ops("count") )
			            : arcPts( r=radius-rlen, a=360, count=ops("count") ) // in
			  );  

	//echo( "begPts: ", begPts);
	//echo( "endPts: ", endPts);

	//MarkPts( begPts );
	//MarkPts( endPts );

	translate( ops("target") )
	rotate( rotangles_z2p( ops("normal") ) )
	rotate( [0,0, ops("rotate") ] )
	{

	//=============================================
	// Start of translation/rotation
	//=============================================
	if( style=="rod" ){

		for( i= [0:len(begPts)-1] ){
			p= begPts[i];
			q= endPts[i];
			rodcolor= haskey(markcolor,i)
								?hash(markcolor,i) 
								:color;
					
//			  assign( p= begPts[i]
//					,q= endPts[i]
//					, rodcolor= haskey(markcolor,i)
//								?hash(markcolor,i) 
//								:color
//					)
			   //MarkPts( [p,q] );
			   Line( [ p,q ]	
			     , update( ops, ["color", rodcolor, "transp",1] )
					
                   );	
			}
	}
	if( style == "block") {
			for( i= [0:len(begPts)-1] ){
			  assign ( p= begPts[i]
					, q= endPts[i]
					, L= len(begPts)
					, FL=floor( len(begPts) /4) 
						// FL is needed to adjust block orientation 
						// when rotation is set for block
					, rodcolor= haskey(markcolor,i)
								?hash(markcolor,i)
								:color
					 //Extends to cover line-curve gap
					, contactsealer = dir=="out"?
									(radius-shortside(radius, ops("w")/2 )
									+ GAPFILLER)
									:dir=="in"?
									 -radius+shortside(radius, ops("w")/2 )
									+GAPFILLER
									 :0

					){
			//echo("rodcolor = ", rodcolor );
             //echo("outrodmarkcolor = ", outrodmarkcolor ); 
			//echo_( "{_}: pq = {_}", [i, pq] );  
			//echo_("L={_}, FL={_}",[L,FL]); 
			//echo("contactsealer = ", contactsealer );
			//echo_( "isupdown={_}, rodrotate={_}, rotate={_} ", [isupdown, rodrotate, rodrotate+(isupdown?30:0)] );
			   Line( [ p,q ]
				  // , onlinePt( [ pq, ORIGIN ]
				  //             , len= -rlen )
				//	] 
			     ,  update( ops, ["style","block"
							   , "shift", isupdown?"mid2":"center"
								 ,"color",rodcolor
								 ,"width",ops("w")
								  ,"depth",h 
								 // , "markpts", true	
								  ,"extension0", contactsealer												
								, "rotate",  isupdown  // rotate for up/down
											?(i*360/count+rodrotate)
											: (rodrotate*(
												(i>L-FL)||((i>=0)&&(i<=FL))?-1:1 
											  ))+ (i==0?90:0)
											// The last part, rotrotate by 90,
											// is needed for i=0 in block style 

								])
                   );	
			}}
	}


	

	}//=============================================
	//  END of translation/rotation
	//=============================================

}
//echo( [3,4]+repeat([2],2) );
//echo( inc([3,4],2) );

module RodCircle_demo_1_dir()
{
	RodCircle(["markcolor",[0,"red"], "color", "red"]);
	RodCircle(["markcolor",[0,"red"], "dir", "in", "color","lightgreen"]);
	RodCircle(["markcolor",[0,"red"], "dir", "up", "color","blue"]);
	RodCircle(["markcolor",[0,"red"], "dir", "down", "color","purple"]);
}

module RodCircle_demo_2a_normal_out()
{
	echom("RodCircle_demo_2a_normal_out");
	pq = randPts(2);
	radius = rands( 1,20,1)[0]/10;

	RodCircle( [ "normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts(pq);
	Line(pq, ["r", radius
			,"extension0",0.5, "color", randc(), "transp",0.1] );

}

module RodCircle_demo_2b_normal_in()
{
	echom("RodCircle_demo_2b_normal_in");
	pq = randPts(2);
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ "dir","in"
		   ,"normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts(pq);
	Line(pq, ["r", radius 
			,"extension0",0.5, "color", randc(), "transp",0.1] );

}

module RodCircle_demo_2c_normal_up()
{
	echom("RodCircle_demo_2c_normal_up");
	pq = randPts(2);
	radius = rands( 6,20,1)[0]/10;

	RodCircle( [ "dir","up"
		   ,"normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts(pq);
	Line0(pq, ["r", radius 
			//,"extension0",0.5
			, "color", randc(), "transp",0.2] );

}

module RodCircle_demo_2d_normal_down()
{
	echom("RodCircle_demo_2d_normal_down");
	pq = randPts(2);
	//echo("pq = ", pq); //pq = minzPt(pq_)==pq_[0]?pq_:reverse(pq_);
	//echo("normal = ", normal );
	radius = rands( 6,20,1)[0]/10;

	RodCircle( [ "dir","down"
		   ,"normal",pq[1]-pq[0]// sign(pq[1].z-pq[0].z)*( pq[1]-pq[0] )
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts( pq );
	Line0(pq, ["r", radius 
			//,"extension0",0.5
			, "color", randc(), "transp",0.2] );	
}

module RodCircle_demo_3a_block_out()
{
	echom("RodCircle_demo_3a_block_out");
	pq = randPts(2);
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.8
			,"markcolor",[0,"red"]
		     , "markpts", true
			] );
	MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0
			, "color", randc()
			, "transp",0.2
			, "markpts", true
			] );


}


module RodCircle_demo_3b_block_in()
{
	echom("RodCircle_demo_3b_block_in");
	pq = randPts(2);
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "in"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.4
			,"h", 1
			,"markcolor",[0,"red"]
		   ] );
	MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0, "color", randc(), "transp",0.2] );

}


module RodCircle_demo_3c_block_up()
{
	echom("RodCircle_demo_3c_block_up");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "up"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.8
			,"markcolor",[0,"red"]
			,"markpts", true
			//,"rotate", 90
		   ] );
	MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0, "color", randc(), "transp",0.2] );
}


module RodCircle_demo_3c2_block_up_rotate()
{
	echom("RodCircle_demo_3c2_block_up_rotate");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "up"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			//,"w", 0.8
			,"markcolor",[0,"red"]
			//,"markpts", true
			,"rodrotate", 30
			,"w", 2
			,"h",0.1
			,"len",2
			,"transp",0.5
			
		   ] );
	//MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0, "color", randc(), "transp",0.2] );
}

module RodCircle_demo_3d_block_down()
{
	echom("RodCircle_demo_3d_block_down");
	pq = randPts(2);
	//pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "down"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.8
			,"markcolor",[0,"red"]
			,"markpts", true
		   ] );
	//MarkPts( pq );
	//Line(pq, ["r", radius
	//		,"extension0",2, "color", randc(), "transp",0.2] );
	Line(pq, ["r", radius,
			"extension0",0
			//, "color", randc()
			, "transp",0.2] );

}


module RodCircle_demo_3d2_block_down_rotate()
{
	echom("RodCircle_demo_3d2_block_down_rotate");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle([ 
			  "style", "block"
			, "dir", "down"
			, "count", 24
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  "blue" //randc()
		   	, "radius", 1 //radius
			, "w", 1.2
			, "h",0.1
			, "len",2
			, "transp",0.5
			, "markcolor",[0,"red"]
			//,"markpts", true
			, "rodrotate", 30
		   ] );
	//MarkPts( pq );
	Line(pq, ["r", radius, 
			"extension0",0
			//, "color", randc()
			, "transp",0.6] );

}


module RodCircle_demo_4a_block_out_rotate()
{
	echom("RodCircle_demo_4a_block_out_rotate");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = 2; //rands( 15,20,1)[0]/10;
/*
L	L/4 
2	.5	-+
3	.75	-++
4	1	-++-
5		--++-
6		--+++-
7		--++++-
8	2	--++++--
9   		---++++--
10  		---+++++--
11  		---++++++--
12  	3	---++++++---
13  		----++++++---
14  		----+++++++---
15  		----++++++++---
16  	4	----++++++++----

-- The last - part: i>=(L-floor(L/4)) 
-- The first - part: i>=1, <floor(L/4) 

*/
	RodCircle( [ 
			  "style", "block"
			, "dir", "out"
			, "count", 12
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  "blue" // randc()
		   	, "radius", radius
			,"w", 2
			,"len",2
			,"h", 0.2
			,"transp", 0.8
			,"markcolor",[0,"red", 9,"green"]
			,"markpts", true
			,"rodrotate", 45
		   ] );
	MarkPts( pq );
	Line(pq, [ "r", radius, 
			"extension0",2, "color", randc(), "transp",0.2] );


}

module RodCircle_demo_4b_block_in_rotate()
{
	echom("RodCircle_demo_4b_block_in_rotate");
	pq = randPts(2);
	//pq = [ORIGIN, [0,0,3]];  
	radius = 2; //rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "in"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  "blue" // randc()
		   	, "radius", radius
			,"w", 1
			,"len",1
			,"h", 0.2
			,"markcolor",[0,"red"]
			,"markpts", true
			,"rodrotate", 45
		   ] );
	MarkPts( pq );
	Line(pq, [ "r", radius, 
			"extension0",2, "color", randc(), "transp",0.2] );

}

module RodCircle_demo()
{
	RodCircle_demo_1_dir();
	//RodCircle_demo_2a_normal_out();
	//RodCircle_demo_2b_normal_in();
	//RodCircle_demo_2c_normal_up();
	//RodCircle_demo_2d_normal_down();
	//RodCircle_demo_3a_block_out();
	//RodCircle_demo_3b_block_in();
	//RodCircle_demo_3c_block_up();
	//RodCircle_demo_3c2_block_up_rotate();
	//RodCircle_demo_3d_block_down();
	//RodCircle_demo_3d2_block_down_rotate();

	//RodCircle_demo_4a_block_out_rotate();
	//RodCircle_demo_4b_block_in_rotate();
}

module RodCircle_test( ops ){ doctest( RodCircle, ops=ops);}

//RodCircle_demo();
//doc(RodCircle);



//========================================================
roll=["roll", "arr,count=1", "arr", "Array"
," Given an array (arr) and an int (count), roll arr items
;; as shown below:
;;
;; roll( [0,1,2,3] )   => [0,1,2],[3] => [3],[0,1,2] => [3,0,1,2]
;; roll( [0,1,2,3],-1) => [0],[1,2,3] => [1,2,3],[0] => [1,2,3,0] 
;; roll( [0,1,2,3], 2) => [0,1],[2,3] => [2,3],[0,1] => [2,3,0,1]
;;
;; Compare range w/cycle, which (1) returns indices (2) doesn't 
;; maintain array size:
;;
;; |> obj = [10, 11, 12, 13]
;; |> range( obj,cycle=1 )= [0, 1, 2, 3, 0]
;; |> range( obj,cycle=2 )= [0, 1, 2, 3, 0, 1]
;; |> range( obj,cycle=-1 )= [3, 0, 1, 2, 3]
;; |> range( obj,cycle=-2 )= [2, 3, 0, 1, 2, 3]

"];

function roll(arr,count=1)=
 let(L= len(arr)
	,count= count<0?L+count:count)
(
 	count==0 || count>L-1? arr
	:concat( [for(i= [L-count:L-1]) arr[i]]
		   , [for(i= [0:L-count-1]) arr[i]]
		   )
); 

module roll_test( ops=["mode",13])
{
	doctest( roll,
	[
		 [ "[0,1,2,3]", roll([0,1,2,3]), [3,0,1,2] ]
		,[ "[0,1,2,3],-1", roll([0,1,2,3],-1), [1,2,3,0] ]
		,[ "[0,1,2,3], 2", roll([0,1,2,3], 2), [2,3,0,1] ]
		,[ "[0,1,2,3,4,5], 2", roll([0,1,2,3,4,5], 2), [4,5,0,1,2,3] ]
		,[ "[0,1,2,3],0", roll([0,1,2,3],0), [0,1,2,3] ]
    ], ops
	);
}




//========================================================
rotangles_p2z=["rotangles_p2z","pt,keepPositive=true","Array","Math, Geometry",
"
 Given a 3D pt, return the rotation array that
;; would rotate pt to a point on z-axis, ptz= [0,0,L],
;; where L is the len of pt to the ORIGIN.
;; Two ways to rotate pt to ptz using the rotation
;; array (RA) is obtained:
;;
;; 1. Scadex way: (has the target coordinate)
;;
;;    RA = rotangles_p2z( pt ); 
;;    ptz = RM( RA )*pt; 
;;    translate( ptz )
;;    sphere9 r=1 ); 
;;
;; 2. OpenScad way: (lost the target coordinate)
;;
;;    RA = rotangles_p2z( pt ); 
;;    rotate( RA )
;;    translate( pt )
;;    sphere( r=1 ); 
"
];
        
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);

module rotangles_p2z_demo()
{
	echom("rotangles_p2z");

	module demo( label, pt, col)
	{
	    echo_("<{_}>, {_} line:  pt={_}, "
			,[label, col, pt ] );
 
		Line0([O,pt],[["markpts",false],["color",col]]);
		PtGrid(pt);

		// get the angles
		r2za= rotangles_p2z(pt, true 
				//,pt.x==0 && pt.y==0 && pt.z<0?false:true
			);   
		//echo_("pt={_}",[pt]);
		echo_("r2za ={_} ", [r2za]);


		// Demo two ways of rotating a pt to the z-axis using r2za:
		// Two spheres should be at the same spot

		// #1  Rotate from pt to z   (shown as small, dense sphere)
		rotate( r2za )     
		translate(pt)
		color( col, 1)
		sphere(r=0.1);

		// #2   Translate to the rotated pt (shown as large, transparant sphere )
		pt_on_z = RM(r2za)*pt; //RMy(r2za.y)*RMx(r2za.x) * pt;
		translate( pt_on_z )
		color(col,0.2)
		sphere(r=0.2);

	}

    //// The transparant ball must match 
    //// the solid ball on the z-axis:

	//demo( "Random pt", randPt(), "orange");
	//demo( "On xy-plane", pxy0(randPt()), "red");
	//demo( "On yz-plane", p0yz(randPt()), "green");
	//demo( "On xz-plane", px0z(randPt()), "blue");
	//demo( "On x", px00(randPt()), "red");
	//demo( "On y", p0y0(randPt()), "lightgreen");
	//demo( "On z", p00z(randPt()), "lightblue");


	module demo_reverse( label, pt, col)  // z=>p
	{
	    echo_("<{_}>, {_} line:  pt={_}, "
			,[label, col, pt ] );
 
		Line0([O,pt],[["markpts",false],["color",col]]);
		PtGrid(pt);
		MarkPts([ pt ],[["colors",[col]]] );

		r2za= rotangles_p2z(pt);   // get the angles
	
		RM_p2z = RM( r2za );              // RM for p to z
		RM_z2p = transpose( RM_p2z );	  // RM for z to p

		//echo("RM_z2p = ", RM_z2p);

		pz = [0,0, norm(pt)];
		translate( pz )
		sphere(r=0.1);		

		//echo("pz = ", pz);

		pt2 = RM_z2p* pz ;

		//echo("pt2 = ", pt2);

		translate( pt2 )
		color(col,0.2)
		sphere(r=0.2);		


	}

    //// The transparant ball must match 
    //// the solid ball on the end of line:

	//demo_reverse( "Random pt", randPt(), "orange");
	//demo_reverse( "On xy-plane", pxy0(randPt()), "red");
	//demo_reverse( "On yz-plane", p0yz(randPt()), "green");
	//sdemo_reverse( "On xz-plane", px0z(randPt()), "blue");
	//demo_reverse( "On x", px00(randPt()), "red");
	//demo_reverse( "On y", p0y0(randPt()), "lightgreen");  // <======
	//demo_reverse( "On z", p00z(randPt()), "lightblue");
}
module rotangles_p2z_test(ops)
{ doctest( rotangles_p2z, ops=ops ); }
//rotangles_p2z_test( ["mode",22] );

//rotangles_p2z_demo();
//doc(rotangles_p2z);





//========================================================
rotangles_z2p=["rotangles_z2p","pt","Array","Math, Geometry",
"
 Given a 3D pt, return the rotation array that
;; would rotate a pt on z, ptz (=[0,0,L] where L is
;; the distance between pt and the origin), to pt
;; Usage:
;;
;;    RA= rotangles_z2p( pt ); 
;;    rotate( RA )
;;    translate( ptz )
;;    sphere( r=1 ); 
"
];

/*
function rotangles_p2z(pt,keepPositive=true)=
(
  [ pt.z==0? sign(pt.y)*90
	:sign(pt.z)*asin(pt.y/slopelen(pt.y,pt.z))
  , pt.z==0? -asin(pt.x/slopelen(pt.y,pt.x))
	:(keepPositive&&pt.z<0?180:0)-sign(pt.z)*asin(pt.x/norm(pt))
  , 0 ]
);
*/
function rotangles_z2p(pt)=
(
	//// 2014.5.20: add 
	////   (pt.x<0?-1:1)
	//// to cover the case of : [x,0,0] when x<0
	////
	(pt.z==0&&pt.y!=0)
	?[ -sign(pt.y)*90, 0, -sign(pt.x)*sign(pt.y)*abs(atan(pt.x/pt.y))]  
	:[-sign(pt.z)*asin( pt.y/ norm(pt) )  // rotate@y to xz-plane
	 , pt.z==0?((pt.x<0?-1:1)*90):atan(pt.x/pt.z) 	      // rotate from xz-plane to target 	
	 , 0]

);	

module rotangles_z2p_demo()
{
	echom("rotangles_z2p_demo");

	module demo( label, pt, col)
	{
	    echo_("<{_}>, {_} line:  pt={_}, "
			,[label, col, pt ] );
 
		Line0([O,pt],[["markpts",false],["color",col]]);
		PtGrid(pt);
	translate( pt )
	color(col)
	sphere(r=0.1);

	echo("pt = ", pt);
	a_p2z= rotangles_p2z(pt, keepPositive=false);
	echo("a_p2z = ", a_p2z);

	ptz = RMy(a_p2z.y)*RMx(a_p2z.x) * pt;
	//MarkPts( [ pt, ptz ], [["grid",true]] );
	translate( ptz )
	sphere(r=0.1);


	a_z2p = rotangles_z2p( pt );
	echo("a_z2p", a_z2p);

	color(col,0.2)
	rotate( a_z2p )
	translate( ptz )
	sphere(r=0.3);

	}

	//demo( "Random pt", randPt(), "orange");
	//demo( "On xy-plane", pxy0(randPt()), "red");
	//demo( "On yz-plane", p0yz(randPt()), "green");
	//demo( "On xz-plane", px0z(randPt()), "blue");
	//demo( "On x", px00(randPt()), "red");   // <=======
	//demo( "On y", p0y0(randPt()), "lightgreen");
	demo( "On z", p00z(randPt()), "lightblue");
}
module rotangles_z2p_test(ops)
{ doctest( rotangles_z2p, ops=ops ); }

//rotangles_z2p_test( ["mode",22] );
//rotangles_z2p_demo();





//========================================================
_s= [ "_s", "s,arr,sp=''{_}''" , "str", "String",
"
 Given a string (s) and an array (arr), replace the sp in s with
;; items of arr one by one. This serves as a string template.
"
] ;

function _s(s,arr, sp="{_}", _i_=0)= 
( 
	index(s,sp)>=0 && len(arr)>_i_ ?
		str( slice(s,0, index( s,sp))
			, arr[_i_] 
			,  index(s,sp)+len(sp)==len(s)?
				"":(_s( slice( s, index(s,sp)+len(sp)), arr, sp, _i_+1))
		   ): s
);
	
module _s_test( ops )
{
	doctest( 
	_s
	, [
		 ["''2014.{_}.{_}'', [3,6]", _s("2014.{_}.{_}", [3,6]), "2014.3.6"]
	 	,["'''', [3,6]", _s("", [3,6]), ""]
	 	,["''2014.{_}.{_}'', []", _s("2014.{_}.{_}", []), "2014.{_}.{_}"]
	 	,["''2014.{_}.{_}'', [3]", _s("2014.{_}.{_}", [3]), "2014.3.{_}"]
	 	,["''2014.3.6'', [4,5]",     _s("2014.3.6", [4,5]), "2014.3.6"]
		,["''{_} car {_} seats?'', [''red'',''blue'']"
			,_s("{_} car {_} seats?", ["red","blue"]), "red car blue seats?"]	 
		 ], ops
	);
}
//s__test( ["mode",22] );


//========================================================
shift= [ "shift", "o,i=1", "str|arr", "String,Array" ,
" Return a str|arr short than o by one on the right side
;; = slice(o,0,-1).
" 
];
   
function shift( o )=
( 
	slice(o,0,-1)
);

module shift_test( ops )
{
	doctest(
	shift ,[
		[  "[2,3,4]", shift([2,3,4]), [2,3] ]
	   ,[  "[2]" , shift([2]), [] ]
	   ,[  "[]" , shift([]), [] ]
	   ,[  "''234''",   shift( "234" ), "23"  ]
	   ,[ "''2''",   shift( "2" ), ""  ]
	   ,[ "''''",   shift( "" ), ""  ]
	 ],ops
	);
}
//shift_test( ["mode",22] );



//========================================================
shortside=["shortside","b,c","number","Math,Geometry,Triangle",
 " Given two numbers (b,c) as the lengths of the longest side and one of 
;; the shorter sides a right triangle, return the the other short side a.
;; ( c^2= a^2 + b^2 )."
,"longside"
];
function shortside(b,c)= sqrt(abs(pow(c,2)-pow(b,2)));

module shortside_test( ops=["mode",13] )
{ doctest( shortside, [
	[ "5,4", shortside(5,4), 3 ]
	,[ "5,3", shortside(5,3), 4 ]
	], ops
	);
}



shrink= [ "shrink", "o,i=1", "str|arr", "String, Array",
"
 Return a str|arr short (on the left side) than o by one.\\
"
];

function shrink( o )=
(
	isarr(o)
	? ( len(o)<=1?[]: [for(i=[1:len(o)-1]) o[i]] )
	: ( len(o)<=1?"": join( [for(i=[1:len(o)-1]) o[i]] ,"")  )
);
module shrink_test( ops )
{
	doctest(
	shrink
	,[
		[  [2,3,4], shrink([2,3,4]), [3,4] ]
	   ,[  [2] , shrink([2]), [] ]
	   ,[  "''234''",   shrink( "234" ), "34"  ]
	   ,[ "''2''",   shrink( "2" ), ""  ]
	   ,[ "''''",   shrink( "" ), ""  ]
	 ], ops
	);
}
//shrink_test( ["mode",22] );




//========================================================
shuffle=["shuffle", "arr", "array", "Array",
" Given an array (arr), return a new one with all items randomly shuffled.
"];
function shuffle(arr, _i_=rand(1))=
(
	len(arr)>0? concat( [pick(arr, floor( len(arr)*_i_))[0] ]
					, shuffle( pick(arr, floor( len(arr)*_i_))[1], _i_=rand(1) )
					):[]
);

module shuffle_test( ops=["mode",13] )
{
	arr = range(6);
	arr2= [ [-1,-2], [2,3],[5,7], [9,10]] ;
	doctest( shuffle,
	[
		str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr)= ", shuffle(arr) )
		,str( "shuffle(arr2)= ", shuffle(arr2) )
		,str( "shuffle(arr2)= ", shuffle(arr2) )
		,str( "shuffle(arr2)= ", shuffle(arr2) )
	], ops, ["arr", arr, "arr2", arr2]
	);
}
//shuffle_test();



//========================================================
sinpqr= ["sinpqr","pqr","number","Math,Geometry",
 " Given a 3-pointer (pqr), return sin( RT/QR ) where T is 
;; the R's projection on PQ line (T=othoPt([P,R,Q])).
;;
;;           R
;;          /| 
;;         / |.'
;;        / .'T
;;       /.'
;;  ----Q------
;;    .'
;;   P
"]; 

function sinpqr( pqr )=
 let( P=P(pqr), Q=Q(pqr), R=R(pqr)
	, T= othoPt( [P,R,Q] )
	, RT= dist([R,T])
	, RQ= dist([R,Q])
	, onQside = norm(T-P) > norm(Q-P)?1:-1
	)
(
	sign(onQside)* sin( angle([R,Q,T]) )
);

module sinpqr_demo()
{
	pqr= randPts(3);
	//pqr= randRightAnglePts();

	P=P(pqr); Q=Q(pqr); R=R(pqr);
	T= othoPt( [P,R,Q] );
	Chain( pqr );
	MarkPts( [P,Q,R,T], ["labels", "PQRT"] );
	Plane( pqr, ["mode",4]);
	n = sinpqr( pqr ); 
	a = asin(n);
	scale(0.03) text( str( n ) );

_mdoc("sinpqr_demo",
_s(" RT={_}
;; RQ={_}
;; a(pqr)= {_}
;; onQside={_}
", [ dist([R,T]), dist([R,Q]), angle(pqr), norm(T-P) > norm(Q-P)?1:-1]
)); 
	
}	
//sinpqr_demo();


//========================================================
slice=[ "slice", "s,i,j", "str|arr", "String,Array" ,
 " Slice s (str|arr) from i to j-1. 
;;
;; - If j not given or too large, slice from i to the end. 
;; - Both could be negative/positive.
;; - If i >j, do a reversed slicing
;;
;; Note that this func doesn't do cycling (i.e., when it is
;; out of range, it doesn't continue from the other end). 
;; Use *range()* for that purpose.
"
];

/*
http://forum.openscad.org/Recursion-problem-tc7666.html
function subseq(v,start,end,i=0) = 
  i < len(v) 
    ? i >= start && i <= end 
      ? concat([v[i]], subseq(v,start,end,i+1)) 
      : subseq(v,start,end,i+1) 
    : [] ; 
*/
/*
function slice(o,i,j)= 
let( i=i<-(len(o))?0:fidx(o,i)
   , j=j==undef||j>len(o)-1? len(o): fidx(o, j)
   )
(
	isarr(o)
	? range( i,j, obj=o, isitem=true)
	: join( range( i,j, obj=o, isitem=true ), "" ) 
//	[i,j]
);
*/

function slice(o,i,j)= 
 let( i= i<-(len(o))?0:fidx(o,i)
    , j=j==undef||j>len(o)-1? len(o): fidx(o, j)
	, L=len(o)
	, ret= i<j? [for(k=[i:j-1])o[k]]
			:i>j? [for(k=[L-i:L-j-1])o[L-k]]
		    :[]
	)
(
	isarr(o)? ret: join(ret,"")
);

module slice_test( ops )
{
	s="012345";
	a=[10,11,12,13,14,15];
	doctest( 
	slice
	, [
	  "## slice a string ##"
	  
	  ,["''012345'',2",slice(s,2),"2345"]		 
	  ,["''012345'',2,4",slice(s,2,4),"23"]
	  ,["''012345'',0,2",slice(s,0,2),"01"]		 
	  ,["''012345'',-2",slice(s,-2),"45"]		 
	  ,["''012345'',1,-1",slice(s,1,-1),"1234"]		 
	  ,["''012345'',-3,-1",slice(s,-3,-1),"34"]		 
	  ,["''012345'',3,3",slice(s,3,3),""]		 
	  ,["'''',3",slice("",3),""]		 
	  
	  , "## slice an array ##" 	
	  , [ "[10,11,12,13,14,15],2", slice(a, 2), [12,13,14,15]]
    	  , [ "[10,11,12,13,14,15],2,5", slice(a, 2,5), [12,13,14]]
    	  , [ "[10,11,12,13,14,15],2,20", slice(a, 2,20), [12,13,14,15], ["rem", "When out of range"]]
	  , [ "[10,11,12,13,14,15],-3", slice(a, -3), [13,14,15]]
    	  , [ "[10,11,12,13,14,15],-20,-3", slice(a, -20, -3), [10,11,12], ["rem", "When out of range"]]
    	  , [ "[10,11,12,13,14,15],-8,-3", slice(a, -8, -3), [10,11,12], ["rem", "When out of range"]]
    	  , [ "[[1,2],[3,4],[5,6]],2"
	  	, slice([[1,2],[3,4],[5,6]], 2), [[5,6]]]
    	  , [ "[[1,2],[3,4],[5,6]], 1,1"
	  	, slice([[1,2],[3,4],[5,6]], 1,1), []]
    	  , [ "[], 1", slice([], 1), []]
    	  , [ "[9], 1", slice([9], 1), []]

    	  ,""
	  ,"// Reversed slicing:"
	  ,""
	  , [ "[10,11,12,13,14,15], 2,5", slice(a, 2,5), [12,13,14]]
	  , [ "[10,11,12,13,14,15], 5,2", slice(a, 5,2), [15,14,13]]
	   , [ "''012345'', 5,2", slice(s, 5,2), "543"]
	  ,[ "''012345'', 3,1", slice(s, 3,1), "32"]
	  
	  ,[ "''012345'', -3,-5", slice(s, -3,-5), "32"]
	  ] , ops
	);
}
//slice_test( ["mode",22] );


//========================================================
slope= [ ["slope", "p,q=undef", "num",  "Math,Line" ],
"
 Return slope. slope(p,q) where p,q are 2-D points or\\
 slope(p) where p = [p1,p2]\\
"
];

function slope(p,q=undef)= q!=undef?
						((q[1]-p[1])/(q[0]-p[0]))
						:((p[1][1]-p[0][1])/(p[1][0]-p[0][0]));

module slope_test( ops )
{	
	doctest( slope
		   , [ 
				[ [[0,0],[4,2]], slope([0,0],[4,2]), 0.5 ]
		   	 ,  [ [ [[0,0],[4,2]] ], slope( [[0,0],[4,2]] ), 0.5 ]
			 ,  [ [ [[1,1],[4,2]] ], slope( [[0,0],[4,2]] ), 0.5 ]
			 ],ops
		  // , [["echo_level", 1]]	
		   );
}

//slope_test( ["mode",22] );




//========================================================
split= [ "split", "s, sp=\" \"", "arr", "String" ,
"
 Split a string into an array.
"
];

function split(s,sp=" ")=
 let(i = index(s,sp) 
	)	
(
	i<0
	? [s]
	: concat(
		 [ slice(s, 0, i)]
		 , split( slice(s, i+len(sp)), sp ) 
//		   , endwith(s,sp)?
//			 "":split( slice(s, i+len(sp)), sp ) 
		)
);


module split_test( ops )
{
	doctest( 
	split
	,[ 
		[ "''this is it''", split("this is it"), ["this","is","it"]]
	 ,	[ "''this is it'',''i''", split("this is it","i"), ["th","s ","s ","t"]]
	 ,	[ "''this is it'',''t''", split("this is it","t"), ["","his is i",""]]
	 ,	[ "''this is it'',''is''", split("this is it","is"), ["th"," "," it"]]
	 ,	[ "''this is it'','',''", split("this is it",","), ["this is it"]]
	 ,	[ "''Scadex'',''Scadex''", split("Scadex","Scadex"), ["",""]]
	], ops
	);
}


//========================================================
splits= [ "splits", "s, sp=[]", "arr", "String",
 " Split a string into an array based on the item in array sp . 
;;
;; splits(''abcdef'',[''b'',''e'']) => [''a'',''cd'',''f'']
;;
;; Developer note: 
;;
;; Most other recursions in this file go through items in a list
;; with a pattern:
;;
;;   _I_&lt;len(arr)
;;   ? ...
;;   : []           // Note this
;; 
;; This code, however, demos a rare case of recursion into layers 
;; of a tree:
;;
;;   _I_&lt;len(arr)
;;   ? ...
;;   : arr          // major difference
"];

function splits(s,sp=[], _I_=0)=
let(s = isarr(s)?s:[s] )
(
	_I_<len(sp)
	?  splits( joinarr([ for(x=s) split(x, sp=sp[_I_]) ]), sp, _I_+1)
	:s	
);

module splits_test( ops )
{
	//s="this is it";
	s="abc_def_ghi";
	doctest( 
	splits
	,[ 
	 [ "s,[''b'']", splits(s,["b"]), ["a", "c_def_ghi"]]
	,[ "s1,[''b'',''_'']", splits(s,["b","_"]), ["a", "c","def", "ghi"]]	
	,[ "s1,[''b'',''_'',''e'']",splits(s,["b","_","e"]), ["a", "c","d","f", "ghi"]]
	], ops, ["s",s]
	);
}


//========================================================
squarePts=["squarePts", "pqr,p,r", "pqrs", "Math, Geometry",
" Given a 3-pointer pqr, return a 4-pointer that has p,q of the
;; given pqr plus two pts, together forms a square passing through
;; r or has r lies along one side. This is useful to find the
;; square defined by p,q,r.
;;
;; Given [p,q,r], return [P,Q,N,M]
;;   P-------Q
;;   |      /|
;;   |     / |
;;   |    /  |
;;   M---R---N
;;
;;   P------Q
;;   |'-_   |:
;;   |   '-_| :
;;   |      '-_:
;;   |      |  ':
;;   M------N    R
;;
;; New 2014.7.15: new OPTIONAL arg: p, r to set size. 
;;
;;          p  
;;   P---|-----Q
;;   |   |    /| 
;;   |   |   / | r
;;   |   +--/---
;;   |     /   |
;;   '----R----'
"
];

//\\
// If size is given:\\
//\\
//         size
//   p---------------n
//   |'-           -'|
//   |  '-       -'  |
//   |    '-   -'    |
//   |      'x'      |
//   |        '-     |
//   |          '-   |
//   |            '- |
//   m---------------q

    
function squarePts(pqr, p=undef, r=undef)=
(
	p
	?( r
	   ? squarePts( [ anyAnglePt(pqr,a=90, len=r,pl="xy")
				  , Q(pqr)
				  , onlinePt(p10(pqr),len=r) 
				  ] )
	   : squarePts( [ onlinePt(p10(pqr),len=p), Q(pqr), R(pqr) ] )	 
	 ) 
	: r
	  ? squarePts( [ P(pqr)
				  , Q(pqr)
				  , anyAnglePt(pqr,a=90, len=r,pl="xy")	
				  //, onlinePt(p12(pqr),dist=r) 
				  ] )
	  : [  pqr[0]
	 	, pqr[1]
	 	, cornerPt( [pqr[2], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[1]]) 
	 	, cornerPt( [pqr[0], othoPt( [pqr[0],pqr[2],pqr[1]]),pqr[2]])
		]
);

module squarePts_demo()
{
	ops= ["closed",true,"r",0.03];
	ops2= ["closed",true,"r",0.2,"transp",0.3];

	pqr = randPts(3);
	Chain( pqr,ops );
	p4 = squarePts(pqr);
	MarkPts(p4,["labels","PQRS"]);
	Chain( p4,ops2 );

	pqr2 = randPts(3);
	Chain( pqr2, ops);
	pp4 = squarePts(pqr2);
	MarkPts(pp4,["labels","PQRS"]);
	Chain( pp4,concat(["color","red"], ops2) );
}

module squarePts_demo_2_debug()
{
	echom("squarePts_demo_2_debug");
// 7/7/2014: 
// anglePt(a=90), that uses squarePts, sometimes goes wrong. See anglePt_demo_3_90() and anglePt_demo_4_90_debug().

// Example wrong cases:

// [[3.00336, 1.06282, 2.49988], [3.00336, 1.06282, 2.49988], [1.99609, -0.615917, 2.09092]]

// bug found: it turns out that the P and Q are the same !!! Fix this in
// anglePt()

	pqr1= [[3.00336, 1.06282, 2.49988], [3.00336, 1.06282, 2.49988], [1.99609, -0.615917, 2.09092]];

	ops= ["closed",true,"r",0.03];

	echo("pqr1", pqr1);
	MarkPts(pqr1, ["grid",true]);
	//p4_1 = squarePts(pqr1);
	//echo("p4_1", p4_1);

}

module squarePts_demo_3_setsize()
{
	//ops= ["closed",true,"r",0.03];
	pqr = randPts(3,r=4);
	MarkPts(pqr,["labels","PQR"]);
	pqrs = squarePts(pqr);
	Chain( pqrs,["r",0.02,"color","red"] );


	pqrs2= squarePts(pqr, p=2);
	//MarkPts(pqrs2);
	//pp4 = squarePts(pqr2);
	Chain( pqrs2, ["r",0.06, "transp",0.3, "color","green"] );

	pqrs3= squarePts(pqr, r=2);
	//MarkPts(pqrs2);
	//pp4 = squarePts(pqr2);
	Chain( pqrs3, ["r",0.1, "transp",0.3, "color","blue"] );

	pqrs4= squarePts(pqr, p=3,r=3);
	//MarkPts(pqrs2);
	//pp4 = squarePts(pqr2);
	Chain( pqrs4, ["r",0.15, "transp",0.3, "color","purple"] );
	Dim( p012(pqrs4),["spacer",1], ["color","purple"]);
	Dim( p123(pqrs4),["spacer",1], ["color","purple"]);


	Dim( p301(pqrs), ["color","red"] );
	Dim( p230(pqrs), ["color","red"] );
	Dim( p012(pqrs2), ["color","green"]);
	Dim( p123(pqrs3), ["color","blue"]);


}

module squarePts_test( ops )
{

	pqr = randPts(3);
	sqs = squarePts( pqr );
	
    doctest( squarePts, 
	[
	  [ "pqr", is90( rearr( squarePts( pqr),[0,1,2] ) ), true
		, ["funcwrap", "is90( select( {_}, [0,1,2] ) )"]
	  ]

	  ,[ "pqr", is90( rearr( squarePts( pqr),[1,2,3] ) ), true
		, ["funcwrap", "is90( select( {_}, [1,2,3] ) )"]
	  ]

	,[ "pqr", is90( rearr( squarePts( pqr),[2,3,0] ) ), true
		, ["funcwrap", "is90( select( {_}, [2,3,0] ) )"]
	  ]
	], ops, ["pqr",pqr, "sqs", sqs]
     );
}
//squarePts_demo();
//squarePts_demo_2_debug();
//squarePts_demo_3_setsize();
//squarePts_test( ["mode",22] );
//doc(squarePts);




//=============================================================
subops=["subops", "parentops=false, subopsname='''',default=[]", "hash", "Hash",
"
 Set the ops of a sub-component of a parent (like, leg features of a
;; robot). Given a parent ops (for robot properties), a subopsname (''leg''),
;; and a default hash (like [''count'',4]), return:
;;
;;   [],      if parentops[subopsname] is false; 
;;   default, if parentops[subopsname] is true; 
;; else:
;;   update( default, parentops[subopsname])
;;
;; A case study: F below has a subunit called Arm that is not added by
;; default. Users can do F( [''arm'',true] ) to show it in predefined
;; default, Arm ops, or give runtime ops to customize it.
;;
;; module F(ops){
;;   ops= update( [ ( F's default ops here )
;;                , ''arm'',false              // disabled arm
;;                ], ops );                   // updated with runtime ops
;;   armops= subops( ops, ''arm'', [''len'',5]); // set armops default and allows
;;                                           // it be updated at runtime
;;   if(armops) Arm( armops ); // armops has one of the following values:
;;                             // false, [''len'',5], or updated version like [''len'',7]
;; } 
"
];

function subops(parentops=false, subopsname="",default=[])=
(
	hash(parentops, subopsname)==false?[]
	:hash(parentops, subopsname)==true? default	
		:update( default, hash(parentops, subopsname) )
);

module subops_test(ops)
{
	scope=[ "tire_dft", ["a",10]
		 , "car1", ["seat",2, "tire",false ]
		 , "car2", ["seat",2,"tire",true]
		 , "car3", ["seat",2,"tire",["a",8, "d",15] ]
		  ];
	car1= hash(scope, "car1");
	car2= hash(scope, "car2");
	car3= hash(scope, "car3");
	dft = hash(scope, "tire_dft");

	doctest( subops,
		[	
		 "// When tire=false :"
		, ["parentops=car1, subopsname=''tire'', default= tire_dft"
			, subops( parentops=car1, subopsname="tire", default=dft)
			,[]
			]
		,"// When tire is set to true at runtime :"
		,["parentops=car2, subopsname=''tire'', default= tire_dft"
			, subops( parentops=car2, subopsname="tire", default=dft)
			,["a",10]
			]
		,"// When tire is given a value at runtime :"
		,["parentops=car3, subopsname=''tire'', default= tire_dft"
			, subops( parentops=car3, subopsname="tire", default=dft)
			,["a", 8, "d", 15]
			]
		,"// When parentops not given:"
		,["default=tire_dft"
			, subops( default=dft)
			,["a", 10]
			]
		], ops,["scope",scope]
		);
}
//doc(subops);
//subops_test();		




//========================================================
sumto =[  "sumto", "n", "int", "Math" ,
"
 Given n, return 1+2+...+n \\
"
];

function sumto(n)= n+ (n>1?sumto(n-1):0);

module sumto_test( ops )
{
	doctest( 
	sumto
	,[ [ "5", sumto(5), 15 ]
    	 , [ "6", sumto(6), 21 ]
	 ],ops
	);
}
//sumto_test( ["mode", 22 ] );



//========================================================
sum=["sum", "arr", "number", "Math",
"
 Given an array, return the sum of its items.\\
"
];

function sum(arr)=
(
	len(arr)==0?0
	:( arr[0] + sum(shrink(arr)) )
);

module sum_test( ops )
{
    doctest( sum, 
    [
        [[2,3,4,5],sum([2,3,4,5]),14]
    ], ops );
}
//sum_test( ["mode",22] );




ERRMSG = ["rangeError"
		, "Asking {iname}={index} for *{vname}* (len={len}) in {fname}()"
		//, "{iname}={index} IN *{fname}()* OUT OF RANGE OF *{vname}* (LEN={len})"
		,"typeError"
		//, "*{vname}*(a {vtype}) IN *{fname}*() should have been a *{tname}*" 
		, "*{vname}* in {fname}() should be *{tname}*. {vtype} given " 
		];
function errmsg(errname,data)=
(
  str("# ",errname, ": ", _h( hash(ERRMSG, errname), data), " #" )
);



//========================================================
switch=["switch","arr,i,j","array", "Array",
 " Given an array and two indices (i,j), return a new array
;; with items i and j switched. i,j could be negative. If either
;; one out of range, return undef.
"];

function switch(arr,i,j)=
 let(i=fidx(arr,i)
	,j=fidx(arr,j)
	,ii=min(i,j)
	,jj=max(i,j))
(
	ii==jj
	? arr
	: concat( slice(arr,0,ii)
		  	, [arr[jj]]
		  	, slice(arr,ii+1,jj)
		  	, [arr[ii]]
		  	, slice(arr,jj+1)
		  	)

//	!isarr(arr)
//	? errmsg( "typeError"
//			,["fname","switch","vname","arr", "vtype",type(arr), "tname","array"] )
//	:!inrange(arr,i)
//	? errmsg( "rangeError"
//			,["fname","switch", "iname","i","index",i, "vname","arr", "len",len(arr)] )
//	: 
//	!inrange(arr,j)
//	? errmsg( "rangeError"
//			,["fname","switch", "iname","j","index",j, "vname","arr", "len",len(arr)] )
//	: 
//	(i>=0?i:(len(arr)+i))==( j>=0?j:(len(arr)+j)) 
//	? arr
//	:
//	concat( slice(arr,0, min( i>=0?i:(len(arr)+i)
//						 , j>=0?j:(len(arr)+j)
//						  )
//				)
//		, get(arr, max( i>=0?i:(len(arr)+i)
//					 , j>=0?j:(len(arr)+j)
//					)
//			)
//		, slice(arr, min( i>=0?i:(len(arr)+i)
//					  , j>=0?j:(len(arr)+j))+1
//				  , max(i>=0?i:(len(arr)+i)
//					  ,j>=0?j:(len(arr)+j))
//			  )
//		, get(arr, min(i>=0?i:(len(arr)+i)
//					,j>=0?j:(len(arr)+j))
//			 )
//		, max( i>=0?i:(len(arr)+i)
//			, j>=0?j:(len(arr)+j))>=len(arr)-1
//		  ? [] 
//		  : slice( arr, max( i>=0?i:(len(arr)+i)
//					    , j>=0?j:(len(arr)+j))+1
//			    )
//		)
);

module switch_test( ops=["mode",13] )
{
	arr=[2,3,4,5,6];
	doctest( switch,
	[		
		["arr,1,2", switch(arr,1,2), [2,4,3,5,6] ]
	,	["arr,0,2", switch(arr,0,2), [4,3,2,5,6] ]
	,	["arr,0,-1", switch(arr,0,-1), [6,3,4,5,2] ]
	,	["arr,-1,-5", switch(arr,-1,-5), [6,3,4,5,2] ]
	,	["arr,-1,4", switch(arr,-1,4), [2,3,4,5,6], ["rem","No switch"] ]
	,"//"
	,"// Error message when giving wrong arguments: (new as of 20140730)"
	,"//"
	,	["arr,-1,9", switch(arr,-1,9), "# rangeError: j=9 IN *switch()* OUT OF RANGE OF *arr* (LEN=5) #"] 
	,	["arr,-1,-9", switch(arr,-1,-9), "# rangeError: j=-9 IN *switch()* OUT OF RANGE OF *arr* (LEN=5) #"] 
	,	["arr,-9,-1", switch(arr,-9,-1), "# rangeError: i=-9 IN *switch()* OUT OF RANGE OF *arr* (LEN=5) #"]
	,	["''23456'',-9,-1", switch("23456",-9,-1), "# typeError: *arr*(a str) IN *switch*() should have been a *array* #"]
	],ops, ["arr", arr]
	);
}


//========================================================
subarr=["subarr","arr, cover=[1,1], cycle=true", "array", "Array",
 " Given an array (arr) and range of cover (cover), which is in the form
;; of [m,n], i.e., [1,1] means items i-1,i,i+1, return an array of items.
;;
;; Set cycle=true (default) to go around the beginning or end and resume 
;; from the other side. 
"];

function subarr(arr, cover=[1,1],cycle=true)=
 let( rngs = ranges( arr, cycle=cycle, cover=cover) )
(
 [ for(a=rngs) [for(i=a) arr[i]]]
);

module subarr_test(ops=["mode",13])
{
	arr=[10,11,12,13];
	doctest( subarr,
	[
		 ["arr", subarr( arr ), [[13,10,11],[10, 11,12],[11,12,13],[12,13,10]] ]
		,["arr,cycle=false", subarr( arr,cycle=false ), [[10,11],[10, 11,12],[11,12,13],[12,13]] ]
		,["arr,cover=[0,1]", subarr( arr,cover=[0,1] ), [[10,11],[11,12],[12,13],[13,10]] ]
		,["arr,cover=[2,0]", subarr( arr,cover=[2,0] ), [[12,13,10],[13,10,11],[10,11,12],[11,12,13]] ]
	],ops, ["arr",arr]
	);
}


//========================================================
Text=["subarr","arr, cover=[1,1], cycle=true", "array", "Array",
 " Given an array (arr) and range of cover (cover), which is in the form
;; of [m,n], i.e., [1,1] means items i-1,i,i+1, return an array of items.
;;
;; Set cycle=true (default) to go around the beginning or end and resume 
;; from the other side. 
"];

module Text(pqr, txt){
    
  	module transform(followVP=ops("vp"))
	{
		translate( midPt( [L0,L1] ) )
	     color( ops("color"), ops("transp") )
		if(followVP){ rotate( $vpr ) children(); }
		else { rotate( rotangles_z2p( Q-P ) )
			  rotate( [ 90, -90, 0] )
			  children();
			}
	}
	conv = ops("conv");
	//echo("conv",conv, "L", L, "numstr(L, uconv=uconv)", numstr(L, conv=conv));
	transform()
	scale(0.04) text(str(ops("prefix")
						  //, L, "="
						  , numstr(L
								, conv=ops("conv")
								, d=ops("decimal")
								, maxd=ops("maxdecimal")
								)
						  , ops("suffix") ));
    
  
}

module Text_dev(){

  module AxisLines( oxyz
      , lnops=[]
      , plops=[]
    )
  {
      ln_ops= concat( lnops, ["r",0.03, "transp", 0.3]);
      pl_ops= concat( lnops, ["color","blue", "mode",3, "transp",0.2]); 
      
      Line0( p01( oxyz), concat(["color", "red"], ln_ops) );
      Line0( p02( oxyz), concat(["color", "green"], ln_ops) );
      Line0( p03( oxyz), concat(["color", "blue"], ln_ops) );
      
      color( "green", hash(pl_ops,"transp") )
      { Plane( p123( oxyz), pl_ops );
        
      }    
      
//      color( "green", hash(pl_ops,"transp") )
//      { Plane( p012( oxyz), pl_ops );
//        Plane( p023( oxyz), pl_ops );
//        Plane( p013( oxyz), pl_ops );
//      }    
  }    
  
  pqr = randPts(3);
  //pqr = [[-1.8992, -2.12299, -1.62216], [1.25559, -0.48364, -1.36525], [-1.10772, -1.4157, 0.591315]];
//  pqr = [[-2.84926, -0.433825, 1.42911], [0.0140438, 1.59454, 1.47263], [0.313024, 0.256869, 0.98651]]; 
  // ERROR:
  // pqr= [[0.982085, -0.443367, -2.85914], [2.66226, 2.26935, 0.904825], [2.38218, 2.66048, 0.311629]]
  echo("pqr", pqr);
  
  P= pqr[0]; 
  Q= pqr[1];
  R= pqr[2];
  N= onlinePt( [Q, N(pqr)]  , len= 3 );
  M= onlinePt( [Q, N([N,Q,P])], len= 3 );

 
  O = Q;
  X = P;
  Y = M;
  Z = N;
  // New coordinate: 0:Q, x:P, y:M, z:N
  MarkPts( [O,X,Y,Z], ["labels","OPQR"] );
  
  
  // ==============================
    
  xyzo_0 = [O,X,Y,Z];

  //AxisLines( xyzo_0 );

  // ==============================

  xyzo_1 = [ for(p=xyzo_0) p+(ORIGIN-O)];
      
  AxisLines( xyzo_1,plops=["color","green"]);
  
  MarkPts( xyzo_1, ["labels","OXYZ"] );
  
  Arrow( [ O, xyzo_1[0] ]
       , ["color","red", "arrow",["r",0.15,"len",0.4], "arrowP",false ] );
  
  //========================    

  A = [ norm(xyzo_1[1]), 0, 0];
  MarkPts([A], ["labels","A"] );
  
  apts = arcPts( [A,ORIGIN, xyzo_1[1]],r=A.x, count=20 ); 
  Line0( [ORIGIN,A] , ["r",0.005]);
  Chain( apts, ["closed",false, "r",0.01] );
    
  
  D = N( [A, ORIGIN, xyzo_1[1]], len=3);  
  YY = xyzo_1[2];
  ZZ = xyzo_1[3];
  
  T = othoPt( [ORIGIN, YY, D]);
  Tz= othoPt ( [ORIGIN, ZZ, D]);
  
  Line0( [YY,T], ["r",0.03] );
  Mark90( [YY,T,ORIGIN] ); 
  MarkPts( [D,T], ["labels", "DT"] );
  Line0( linePts( [ORIGIN, D], len=[5,5]), ["r", 0.03,"color","red"]); 
  
  //---------------------------------------
  a_xoa = angle([ xyzo_1[1], ORIGIN, A]);
  
  B= anyAnglePt( [ORIGIN, T, YY]
                , a=a_xoa, len= norm(T-YY), pl="yz"
                ); 
  MarkPts( [B], ["labels","B"] );              
  Chain( [ORIGIN, B,T], ["closed",false, "r",0.03] );              

  Line0( [ORIGIN,B] , ["r",0.01]);
  aptsY = arcPts( [B,T, YY],r= norm(B-T) ); 
  echo("aptsY", aptsY);
  Chain( aptsY, ["closed",false,"r",0.01] );
  
//---------------------------------------
  a_xoa = angle([ xyzo_1[1], ORIGIN, A]);
  
  C= anyAnglePt( [ORIGIN, Tz, ZZ]
                , a=-a_xoa, len= norm(Tz-ZZ), pl="yz"
                ); 
  MarkPts( [C], ["labels","C"] );              
  Chain( [ORIGIN, C,Tz], ["closed",false, "r",0.03] );              

  AxisLines( [ORIGIN, A,B,C] );
}
//Text_dev();


//========================================================
transpose= [ "transpose", "mm", "mm",  "Array, Math" ,
"
 Given a multmatrix, return its transpose.
"
];

function transpose(mm)=
(
	[ for(c=range(mm[0])) [for(r=range(mm)) mm[r][c]] ]

//	_i_>len(mm[0])-1?[]
//	: concat( [ getcol(mm,_i_)]
//			, transpose( mm, _i_+1)
//			)
);

module transpose_test( ops )
{
	mm = [[1,2,3],[4,5,6]];
	mm2= [[1,4],[2,5],[3,6]];
	doctest
	( 
		transpose
	,	[
			[  mm , transpose(mm), mm2 ]
		,	[  mm2 , transpose(mm2), mm ]
		], ops
	);
}

//transpose_test( ["mode",22] );


//========================================================
or=["or","x,dval","any|dval",  "Hash",
"
 Given x,dval, if x is evaluated as true, return x. Else return
;; dval. Same as x?x:dval but x only appears once.
"  
];

function or(x,dval)=x?x:dval;

module or_test( ops )
{
	doctest
	(
		or
		,[
			 ["false, 3", or(false,3), 3]
			,["undef, 3", or(undef,3), 3]
			,["0,     3", or(0    ,3), 3]
			,["50,    3", or(50   ,3), 50]
		 ], ops
	);
}
//trueor_test( ["mode",22] );	



//========================================================
twistangle=["twistangle","pqrs", "number", "Angle, Geometry",
 " Given a 4-pointer (pqrs), return the angle between PQ and RS. 
;;
;;  +-----:-----+   a: 0~90
;;  :     :  S  :  
;;  :     : /   : 
;;  :     :R    :
;;  ------Q-----P 

;;  +-----:-----+   a:90~180
;;  : S   :     :   
;;  :  '. :     : 
;;  :    R:     :
;;  ------Q-----P 

;;  ------Q-----P  a: 0~ -90
;;  :     :R    :
;;  :     : '.  :
;;  :     :   S :
;;  +-----:-----+ 

;;  ------Q-----P  a: -90~ -180
;;  :    R:     :
;;  :   / :     :
;;  :  S  :     :
;;  +-----:-----+ 
;;
;;                     D   S
;;                     :  /|
;;                     : / |
;;                     :/  |   
;;                     /   |  .'
;;                    /:  .|'
;;       B        :  / :.' |
;;  P    |        : / .C---:
;;  |'.  |   :    :/.'_.-''
;;  |  '.| --:----R--'------
;;  |    |.  :  .'
;;  |    | '.:.'
;; -|----|-_.Q -------    
;;  |.'_.|'.'
;;  '----A'
;;     .'
"];
function twistangle( pqrs )=
 let( pqr= p012(pqrs), P=P(pqr), Q=Q(pqr), R=R(pqr), S= pqrs[3]
	, C= othoPt( [Q,S,R] )
	, D= projPt( S, pqr )
	, a= angle([D,C,S])	
	, quad = quadrant( [Q,R,P],S)
	, ks= [0,0,1,a,2,a,3,180-a,4,-a, 5,0,6,90,7,180,8,-90 ]
	)
( 
	 hash( ks, quad )
);

module twistangle_test(ops=["mode",13])
{
	doctest( twistangle,
	ops=ops
	);
}
module twistangle_demo()
{	
	pqrs = randPts(4);
	P=P(pqrs); Q=Q(pqrs); R=R(pqrs);
	S=pqrs[3];
	
	MarkPts([P,Q,R,S],["labels","PQRS"]);
	Chain( [P,Q,R,S ] );
	//Line0( [Q,S] );

	echo( "quad:", quadrant( [Q,R,P],S), "twistangle:", twistangle( [P,Q,R, S] ));

}
//twistangle_demo();



//========================================================
type=[ "type", "x", "str", "Type, Inspect" ,
"
 Given x, return type of x as a string.
"
];

function type(x)= 
(
	x==undef?undef:
	( abs(x)+1>abs(x)?
	  floor(x)==x?"int":"float"
	  : str(x)==x?"str"
		: str(x)=="false"||str(x)=="true"?"bool"
			:(x[0]==x[0])? "arr":"unknown"
	)
);

module type_test( ops )
{
	doctest
	(
	type,[  
		[[2,3], type([2,3]), "arr" ]
    	 , [ "[]", type([]), "arr" ]
	 , [ "-4", type(-4), "int"]
	 , [ "0", type( 0 ), "int"]
	 , [ "0.3", type( 0.3 ), "float"]
	 , [ "-0.3", type( -0.3 ), "float"]
	 , [ "''a''",type("a"), "str"]
	 , [ "''10''", type("10"), "str"]
	 , [ "true", type(true), "bool"]
	 , [ "false", type(false), "bool"]
	 , [ "undef", type(undef), undef]
    	 ],ops
	);
}
//type_test( ["mode",22] );


//========================================================
ucase=["ucase", "s","str","String",
 " Convert all characters in the given string (s) to upper case. "
];
_alphabetHash2= split("a,A,b,B,c,C,d,D,e,E,f,F,g,G,h,H,i,I,j,J,k,K,
l,L,m,M,n,N,o,O,p,P,q,Q,r,R,s,S,t,T,u,U,v,V,w,W,x,X,y,Y,z,Z",",");

function ucase(s, _I_=0)=
(
  	_I_<len(s)
	? str( hash( _alphabetHash2, s[_I_], notfound=s[_I_] )
		, ucase(s,_I_+1)
		)
	:""
);

module ucase_test( ops= ["mode",13] )
{
	doctest( ucase,
	[
		["''abc,Xyz''", ucase("abc,Xyz"), "ABC,XYZ"]
	], ops
	);
}

//========================================================
uconv=["uconv", "n, units=''in,cm'', uconv_table=UCONV_TABLE)", "number", "Math, Number",
" Convert n's unit based on the *units*, which has default ''in,cm''. 
;; The conversion factor is stored in a constant UCONV_TABLE. To use
;; something not specified in UCONV_TABLE, do this:
;;
;;   uconv(n, ''u1,u2'', factor )
;;
;; Or you can define own table:
;;
;;   table=[''u1,u2'',factor]
;;   uconv( n, ''u1,u2'', table )
;;
;; n can be a string like \"2'8''\". If so the arg *from* is ignored.
;;
;; Related:
;;
;; num(''3'6\"'')= 3.5  // in feet
;; numstr( 3.5, conv=''ft:ftin'') = ''3'6\"'' // return str
;; ftin2in(''3'6\"'') = 42 
"];

UCONV_TABLE= [ "in,cm", 2.54
			  , "cm,in", 1/2.54
			  , "ft,cm", 2.54*12
			  , "cm,ft", 1/(2.54*12)
			  , "ft,in", 12
			  , "in,ft", 1/12	
			  ];
function uconv(n,units="in,cm", uconv_table=UCONV_TABLE)=
let( 
	isftin= endwith(n, ["'","\""] ) 
	, m = isstr(n)
		  ? isftin 
			? ftin2in(n)
			: num(n)
		  :n
    , from_= isftin? "in": split(units,",")[0]
    , to_  = split(units,",")[1]
	, from = or(from_, "in")
	, to   = or(to_,"cm")
	, units= str(from, ",", to) 
	, uconv_table= isnum(uconv_table)? [units, uconv_table]:uconv_table
   	)
(
	(from==to)
	? m
	: m * hash( uconv_table
			  , units
			  , notfound="units not found") // Why this notfound doesn't show up ?
										   // The functions used, ftin2in and hash,
				                            // and everything else work fine, can't
										   // find where this error-reporting goes 
										   // wrong. The result is correct, though. 
										   // 2014.8.12	
);

module uconv_test( ops=["mode",13])
{
	doctest( uconv,
	[
	  ["3,''in,''", uconv(3,"in,"), 3*2.54, ["rem","Default 'to' = 'cm'"] ]
	  ,["3,''cm,in''", uconv(3,"cm,in"), 3/2.54 ]
	  ,["100, ''cm,ft''", uconv(100,"cm,ft"), 100/2.54/12]
	  ,"// Use own units:"
	  ,["100, ''cm,aaa''", uconv(100,"cm,aaa"), undef]
	  ,["100, ''cm,aaa'', 2", uconv(100,"cm,aaa", 2), 200, ["rem","Enter a factor"]]
	  ,["100, ''cm,aaa'', [''cm,aaa'',3]", uconv(100,"cm,aaa", ["cm,aaa",3]), 300, ["rem","Enter a table"]]
	  ,"// When n is a string like \"2'8''\", arg *from* is ignored:"
	  ,["''1'3'''', '',in''", uconv("1'3\"", ",in"), 15]
	  ,["''1'3'''', '',cm''", uconv("1'3\"", ",cm"), 15*2.54]
	  ,["''1'3'''', '',ft''",    uconv("1'3\"", ",ft"), 15/12]
	  ,["''1'3'''', '',aaa''",    uconv("1'3\"", ",aaa"), undef]
	  
 
	],ops);
} 

//========================================================
update= ["update", "h,h1", "hash", "Hash" ,
"
 Given two hashes (h,h1) update h with items in h1.
"
];

function update(h,g)=
  let( h= isarr(h)?h:[]
     , g= isarr(g)?g:[] )
  [ for(i2=[0:len(h)+len(g)-1])      // Run thru entire range of h + g
      let(is_key= round(i2/2)==i2/2  // i2 is even= this position is a key
         ,is_h = i2<len(h)           // Is i2 currently in the range of h ?
         ,cur_h = is_h?h:g           // Is current hash the h or g?
         ,cur_i = is_h?i2:(i2-len(h)) // Convert i2 to h or g index
         ,k= cur_h[ is_key?cur_i:cur_i-1 ]  // Current key
         ,overlap = kidx((is_h?g:h),k)>=0   // Is cur k-v overlap btw h & g?
      )
      if( !(overlap&&!is_h)) // Skip if overlap and current hash is g
         is_key? k
               : overlap?hash(g,k):cur_h[cur_i]    
];
      
      
//function update(h,h1,_I_=0)=
//let(  
//	 newk = h1[_I_]
//	,newv = h1[_I_+1]
//	,keyindex= index( keys(h), newk )
//	,h = keyindex>-1
//		  ? replace( h, keyindex*2+1, newv )
//		  : concat( h, [newk, newv] ) 
//	)
//(
//	_I_<len(h1)-2
//	? update( h, h1, _I_+2)
//	: h 
//);

module update_test( ops )
{

	doctest(
	 update
	  
	,[
	   [ "[1,11],[2,22]",update( [1,11],[2,22] ), [1,11,2,22]  ]
	 , [ "[1,11,2,22],[2,33]",update( [1,11,2,22], [2,33] ), [1,11,2,33]  ]
	 , [ "[1,11,2,22],[1,3,3,33]",update([1,11,2,22],[1,3,3,33]), [1,3,2,22,3,33]]
	 , [ "[1,11,2,22],[]",update([1,11,2,22],[]), [1,11,2,22]]
	 , ""
	 , " Clone a hash: "
	 , ""
	 , [ "[],[3,33]",update( [],[3,33] ), [3,33]  ]
	 , ""
	 , " Show that the order of h2 doesn't matter"
	 , ""
	 , [ "[''a'',1,''b'',2,''c'',3], [''b'',22,''c'',33]"
		, update( ["a",1,"b",2,"c",3],["b",22,"c",33] )
		, ["a",1,"b",22,"c",33]  ]
	 , [ "[''a'',1,''b'',2,''c'',3], [''c'',33,''b'',22]"
		,update( ["a",1,"b",2,"c",3], ["c",33,"b",22] )
		,["a",1,"b",22,"c",33]  ]
	 ], ops
	);
}

//update_test( ["mode",2]);




//========================================================
updates= ["updates", "h,hs", "hash", "Hash" ,
"
 Given a hash (h) and an array of multiple hashes (hs),
;; update h with hashes one by one.
;; 
;; New 2015.2.1: enter h,hs as a one-layer array: updates(hs).
;; That is, updates the hs[0] with the rest in hs
"
];

function updates(h,hs)=
    let ( _h= hs==undef? h[0]:h
        , hs= hs==undef? shrink(h): hs 
        )
(	len(hs)==0? _h
	: updates( update(_h,hs[0]), shrink(hs) )
);
//(
//	len(hs)==0? h
//	: updates( update(h,hs[0]), shrink(hs) )
//);

module updates_test( ops )
{ 
	doctest( updates	  
	,[
	   [ [[1,11,2,22],[[2,20],[1,3,3,33]]]
		,updates( [1,11,2,22],[[2,20],[1,3,3,33]] )
		, [1, 3, 2, 20, 3, 33] 
 		   
 	   ]	 
	 , [ [[1,11,2,22],[[1,3,3,33],[2,20]]]
		,updates( [1,11,2,22],[[1,3,3,33],[2,20]] )
		, [1, 3, 2, 20, 3, 33] 
 	   ]	 
	 , [ [[1,11,2,22],[[1,3,3,33],[],[2,20]]]
		,updates( [1,11,2,22],[[1,3,3,33],[],[2,20]] )
		, [1, 3, 2, 20, 3, 33] 
 	   ]	 
	 , [ [[1,11,2,22],[[1,3,3,33,2,20]]]
		,updates( [1,11,2,22],[[1,3,3,33,2,20]] )
		, [1, 3, 2, 20, 3, 33] 
 	   ]	 
	 , [ [[1,11,2,22],[[1,3,3,33],[2,20],[2,12,4,40]]]
		,updates( [1,11,2,22],[[1,3,3,33],[2,20],[2,12,4,40]] )
		, [1,3, 2,12, 3,33, 4,40] 
 	   ]
    , [ [[],[[1,3,3,33]]]
		,updates( [],[[1,3,3,33]] )
		, [1,3,  3,33] 
 	   ]
    , [ [[],[[1,3,3,33],[2,20]]]
		,updates( [],[[1,3,3,33],[2,20]] )
		, [1,3, 3,33, 2,20] 
 	   ]		 
    , [ [[],[[1,3,3,33],[2,20],[2,12,4,40]]]
		,updates( [],[[1,3,3,33],[2,20],[2,12,4,40]] )
		, [1,3, 3,33, 2,12, 4,40] 
 	   ]		
     ,""
     ,"New 2015.2.1: enter (h,hs) as a one-layer array: updates(hs). That is, "
     ," updates the hs[0] with the rest in hs"
     ,""
     , ["[[''a'',1,''b'',2,''c'',3], [''a'',11,''d'',4],[''c'',33,''e'',5] ]"
        ,updates( [["a",1,"b",2,"c",3],["a",11,"d",4],["c",33,"e",5] ])
        ,["a",11,"b",2,"c",33,"d",4,"e",5]
       ] 
     , ["[[''a'',1,''b'',2,''c'',3],[],[''c'',33,''e'',5] ]"
        ,updates( [["a",1,"b",2,"c",3],[],["c",33,"e",5] ])
        ,["a",1,"b",2,"c",33,"e",5]
       ] 
     , ["[[], [''a'',11,''d'',4],[''c'',33,''e'',5] ]"
        ,updates( [[],["a",11,"d",4],["c",33,"e",5] ])
        ,["a",11,"d",4,"c",33,"e",5]
       ]     
	 ], ops
	);
}
//updates_test( ["mode",22]);

//========================================================
vals=[  "vals", "h", "arr",  "Hash",
"
 Return the vals of a hash as an array.
"
];

function vals(h)=
(
	[ for(i=[0:2:len(h)-2]) h[i+1] ]
);
module vals_test( ops )
{
	doctest( 
	vals, [
		[ "[''a'',1,''b'',2]", vals(["a",1,"b",2]), [1,2] ]
	  ,	[ [1,2,3,4], vals([1,2,3,4]), [2,4] ]
	  ,	[ [[1,2],3, 0,[3,4]], vals([[1,2],3, 0,[3,4]]), [3,[3,4]] ]
	  ,	[ [], vals([]), [] ]
	  ], ops
	//, [["echo_level",1]]
	);
}
//vals_test( ["mode",22] );



//======================================
vlinePt=["vlinePt", "pqr,len=1", "points", "Array, Geometry, Points",
 " Given a 3-pointer pqr and a len(default 1), return a point T such that
;; 
;; (1) TQ is perpendicular to PQ 
;; (2) T is on the plane of PQR
;; (3) T is on the R side of PQ line
;; 
;;           T   R
;;        len|  / 
;;        ---| /  
;;        |  |/
;;   P-------Q
"];

module vlinePt_test( ops=["mode",13] )
{
	doctest( vlinePt, ops=ops );
}

function vlinePt(pqr,len=1)=
 let(Q=Q(pqr)
	,N=N(pqr)
	,T=N([N,Q,P(pqr)])
	)
(
	onlinePt( [Q,T], len=len )
);

module vlinePt_demo_0()
{_mdoc("vlinePt_demo_0",
"");

	pqr= randPts(3);
	P=P(pqr); Q= Q(pqr); R=R(pqr);


	T = vlinePt(pqr);
	echo("T=",T);
	//Line0( [Q,T] );
	pqt = [P,Q,T];
	Mark90( pqt, ["r",0.05, "color","red"] );
	Plane( pqt, ["mode",3] );
	MarkPts( [P,Q,R,T], ["labels","PQRT"] );
	Chain(pqt, ["r",0.05, "transp",1,"color","red"]); 
	Chain(pqr, ["r",0.15, "transp",0.5]); 
	
}
//vlinePt_demo_0();



//=======================================================================
//=======================================================================
//=======================================================================


//======================================
function slopelen(x,y)= sqrt(x*x+y*y); 

//======================================
// The rotation needed for a line
// on X-axis to align with line-p1-p2
// 
function lineRotation(p1,p2)=
(
 [ 0,-sign(p2[2]-p1[2])*atan( abs((p2[2]-p1[2])
		                    /slopelen((p2[0]-p1[0]),(p2[1]-p1[1]))))
    , p2[0]==p1[0]?0
	  :sign(p2[1]-p1[1])*atan( abs((p2[1]-p1[1])/(p2[0]-p1[0])))]
);


scadex_ver=[
["20150202_1", "getsubops: fixed warning (beg shouldn't be larger 
than end) by inserting let(h=isarr(h)?h:[],g=isarr(g)?g:[]) to update()"
] 
,["20150201_1", "updates() now can take one argument (instead of 2); Still debugging getsubops about beg shouldn't be larger than end warning."] 
,["20150131_1", "getsubops revised; tested"] 
,["20150130_1", "kidx(); Updated update();"] 
,["20150125_5", "Cone() new arg: twist."] 
,["20150125_4", "arcPts() new arg: twist."] 
,["20150125_3", "Arc() done."] 
,["20150125_2", "angleBisecPt: new arg len, ratio; hash:let notfound be shown when value is set to undef; Chainf(): fix bug."] 
,["20150125_1", "Chainf(): formatted Chain; Arc in progress"] 
,["20150124_1", "Change Arrow args from arrow to head. Text() in progress"] 
,["20150123_1", "Dim2's guides and arrows seem to be done. "] 
,["20150122_1", "hashex done. getsubops seems tested ok, checking if can use in Dim2 "] 
,["20150121_2", "getsubops in progress. hashex in progress "] 
,["20150121_1", "Dim2 in progress. "] 
,["20150120_1", "Arrow doc and demos. "] 
,["20150119_2", "Arrow (the line and arrow faces not yet aligned. "] 
,["20150119_1", "Cone(), w/new feature: set markpts on the fly"] 
,["20150117_2", "CurvedCone_demo_1() cleanup"]
,["20150117_1", "getSideFaces(), tested"
              , "getTubeSideFaces(), tested"
              , "mergeA(), tested"
              , "CurvedCone, CurvedCone_demo_1()"
               ]
,[ "20150116-1", "CurvedCone first step done. Check CurvedCone_demo_1()", 
    "Change some argument name r to len" 
]
,[ "20140921-1","Change args of isOnPlane()" 
]
,[ "20140918-1","New:roll(arr,count)" 
]
,[ "20140916-1","join(): change default sp from , to empty str."
			,"numstr(): change arg maxd back to dmax." 
]
,[ "20140914-1","New ranges(): similar to range, but return array of arrays."
			, "Both range() and ranges() now return only indices. No more return obj content. Both need docs." 
]
,[ "20140912-1","range(): Revert to entire 0909-1. Re-design range to have simpler args: range(i,j,k, cycle)." 
]
,[ "20140910-2_segmentation_fault","range(): revert this func back to 0909-1, but it causes segmentation fault. Next version: copy entire 0909-1" 
]
,[ "20140910-2","range(): revert it back to 0909-1." 
]
,[ "20140910-1","Re-design range() for partial obj ranging: not yet succeed but already very very slow (too much condition checks in each call). Will revert it back to 0909-1 in next ver." 
]
,[ "20140909-1","Re-design range(): remove args: maxidx, obj; allow cycle= i,-i" 
]
,[ "20140901-1","New:vlinePt()." 
]
,[ "20140831-2","onlinePt: change arg name dist to len." 
]
,[ "20140831-1","Misc improvements. (mainly Rod())" 
]
,[ "20140825-1","New: twistangle()" 
]
,[ "20140823-1","New arg s for angle().","New quadrant()" 
]
,[ "20140822-3","faces() demo of shape ''tube''. Pretty cool." 
]
,[ "20140822-2","New: rodPts(). Makes Rod() uses it."
			, "New: rodfaces(), faces()" 
]
,[ "20140822-1","Rod(): Fix another rotation bug. Doc done, too."
			, "LabelPts: default prefix changed from P to '', begnum from 1 to 0" 
]
,[ "20140821-3","Rod(): Fix rotation bug found earlier today"
]
,[ "20140821-2","Rod(): New args markrange, markOps, labelrange, labelOps; remove showsideindex",
	"Rename mdoc() to parsedoc()"
]
,[ "20140821-1","Bug in Rod : sometime rotate to wrong side. See Rod_demo_8_rotate()"
]
, [ "20140820-1","New mdoc(), _mdoc()"
			  , "Fix major Rod bugs (guess so). More demo with _mdoc"
]
,[ "20140819-1","New Rod arg: rotate; more Rod demos (including gears)"
]
,[ "20140818-3","New: subarr()"
]
,[ "20140818-2","Revised range(): add new args: obj and isitem"
]
,[ "20140818-1", "Revised fidx(o,i): o can be an integer"
			 ,"Revised range(): remove arg obj, add new arg cover and maxidx"
]
,[ "20140817-2", "New: lpCrossPt()"]
,[ "20140817-1", "Rename: rodfaces() to rodsidefaces()"
			 , "New: Rod()"]
,[ "20140816-3", "New: rodfaces()"]
,[ "20140816-2", "New arcPts, good implementation. "]
,[ "20140816-1", "New for LabelPts: args: prefix, suffix, begnum, usexyz; Use new builtin text()"]
,["20140815-1","Done with the migration to OpenSCAD 2014.05.31 snapshot in terms
			of doctest behaviors. Need to fix shape making. Somehow it seems that
			the rotation of points goes wrong."]
,["20140814-1","New:dels()"
	, "Improved: isequal(), now can take var of non-number. tests done"
	, "is0: doc and tests done."]
,["20140812-1","Fix bug: endwith(), istype(). Doc and tests done."]
,["20140811-2","New: begwith and endwith can check if beg/end with any of [a,b,...] ", "Working on: new istype(), improving uconv()"]
,["20140811-1","New: splits()", "Rename: trueor()=> or()"]
,["20140810-1","Disable neighbors(). The new range() can cover it."
			, "consider to disable rearr(). The new list comp can handle it.==> maybe not"
			, "Make range() accept str as the 1st arg as well, so range(''abc'')=[0,1,2]. This is used in reversed()"
			, "New arg for fidx: fitlen"
			, "Simplify slice code, allowing reversed slicing" 
			, "New: joinarr()"]
,["20140809-1","Rename all *iscycle* to *cycle*"
			, "Del range() and copy range2() code to range()"
			, "range2(): new arg: obj,cycle. Doc and test done."
]
,["20140808-3","Remove fitrange(). It can be replaced by fidx()"
			,"New: range2(), much elegantly written than range()."
			,"Re-evaluating the need of neighbors ... it might be able to use range2() instead"
]
,["20140808-2","New function: fidx(). This should be very powerful. "
]
,["20140808-1","Disabled for future removal: mina, maxa, inc, hashi, hashkv"
			,"Renamed: multi()=>repeat(), midCornerPt()=>cornerPt()"
]
,["20140807_1", "START CODING WITH 2014.05.31/Linux 64bit"
			, "Remove addv (too easy to code with list comp so no need a func)"
			, "New arg for del(): *count*."
			, "New func: fitrange() (consider to replace inrange in the future )" 
			, "Re-chk code for use of OpenScad_DocTest for doc testing --- down to hash()"
		    
]
,["20140730_1", "New: prependEach(), permute()"
]
,["20140729_1", "New:switch()", "New: ucase(), lcase()"
			,"New: Error message reporting in switch(). Consider apply it to other functions in the future"
]
,["20140715_5", "New:cubePts(), Cube() done (Cube() needs doc)"]
,["20140715_4", "squarePts(): fix runtime error. "]
,["20140715_3", "Dim(): Fixed bug (S= squarePt(pqr,[''mode'',4]): remove mode. "]
,["20140715_2", "New: addv()"]
,["20140715_1", "New optional args p,q for squarePts() to set sizes." 
             , "_TODO_: to change boxPts from anti-clockwise to clockwise for pqr polyhedron geometry making. --- may not need it after squarePts() has p,r "]
,["20140714_1", "New: p012(pts) ...p320(pts) 24 funclets"
			, "New: p2(), p3(), p4() 3 funclets" ]
,["20140713_2", "numstr(): change arg dmax to maxd; Dim(): new arg maxdecimal." ]
,["20140713_1", "New args for Dim(): conv, decimal." ]
,["20140712_1", "numstr() done." ]
,["20140711_4", "New: uconv() --- need more tests." ]
,["20140711_3", "num() now can convert f'i'' to feet." ]
,["20140711_2", "New: ftin2in()" ]
,["20140711_1", "New: num(numstr)=>num" ]
,["20140710_1", "Working on: new: numstr() for Dim() to use" ]
,["20140709_2", "rand() can take an array or string, return one item randomly." ]
,["20140709_1", "Introducing groupdoc feature and apply it to addx/addy/addz" ]
,["20140708_3", "addx, addy, addz improved, plus their docs and tests. Remove add_z_to_pts" ]
,["20140708_2", "rename doctest to doctest_old, doctest2 to doctest; MOVE this new doctest to a new file scadex_doctest.scad" ]
,["20140708_1", "doctest2: simplify doc from fname=[[fname,arg,retn,tags],doclines] to [fname,arg,retn,tags,doclines]; also using ; instead of \\ for linebreak in doclines." ]
,["20140707_2", "More demos for anyAnglePt." 
]
,["20140707_1", "anglePt bug fixed: failed when a=90 (detected by anyAnglePt_demo_4();" 
]
,["20140703_1", "neighbors(), neighborsAll(): change default of cycle to true"
		   , "normalPts(): fix bug (arg *len* was written as dist in the code)"
		   , "normalPt: change default:len=undef, new: reversed=false" 
]
,["20140701_1", "angle(): use simpler formula.","anyangle()", "P(),Q(),R(),N(),M()"
			 ]
,["20140630_1", "randOnPlanePt: new arg a (for angle); and change the center of pts from incenterPt to Q" ]
,["20140629_1", "anglePt(): change angle to aPQR (originally aQPR) and reaname arg from (pqr, ang, len) to (pqr,a,r).", "Fix randOnPlanePts (it uses anglePt) accordingly" ]
,["20140628_2", "normalPt(), normalPts()"]
,["20140628_1", "expandPts(). cycle=false needs fix."]
,["20140627_4", "neighbors(), neighborsAll()"]
,["20140627_3", "mod()", "remove increase(). Has an inc() already."]
,["20140627_2", "get() got a new arg cycle", "In work: intersectPt(), expand()"]
,["20140627_1", "Output scadex version info when loaded."]
,["20140626_5", "LabelPts_demo_2_scale()"]
,["20140626_4", "increase(pts,n=1)", "LabelPts and MarkPts_demo() can set labels=range(pts) instead of range(len(pts))"]
,["20140626_3", "LabelPts Bug fixed.(demo with LabelPts_demo() and MarkPts_demo())"]
,["20140626_2", "MarkPts: Now can set labels", "LabelPts Bug found: fail to output label when pts contains only one item."]
,["20140626_1", "ishash()"
			, "Improved LabelPts arguments (shift)"
			, "range can do range(arr)"]
,["20140625_5", "Pie() 2D"]
,["20140625_4", "projPt() code, doc and test done."]
,["20140625_3", "Normal() new argument:reverse; Also show arrow but not just line."]
,["20140625_2", "Revising Arc()=> failed. But made a good 2D pie."]
,["20140625_1", "between() code, doc and test done."]
,["20140624_7", "randRightAnglePts improved (allows for dist= a point or an integer."]
,["20140624_6", "Bugfix for pick() and shuffle()."]
,["20140624_5", "shuffle() code, doc and test done."]
,["20140624_4", "pick() code, doc and test done."]
,["20140624_3", "range() code, doc and test done. "]
,["20140624_2", "randRightAnglePts() improved to allow fixed sizes right tria. "]
,["20140624_1", "randRightAnglePts() done. "]
,["20140623_4", "det(), RMs()", "Working on randRightAnglePts()"]
,["20140623_3", "Fix randOnPlanePt(), randOnPlanePts()"]
,["20140623_2", "Bug fix for anglePt()"]
,["20140623_1", "p021, p120, p102, p201, p210", "rearr()", "incenterPt()", "Bug in doctest when mode= 2"]
,["20140622_3", "randInPlanePts(), randOnPlanePt, randOnPlanePts():this one needs fixes"
]
,["20140622_2", "rand()", "p01() ... p03() ... p30()", "randInPlanePt()"
]
,["20140622_1", "lineCrossPt(), crossPt()", "dot()", "is0()", "prodPt()", "addx, addy, addz, dx, dy, dz"
]
,["20140620_1", "Default *closed* param in Chain set to true"
 , "Mark90 seems to have false-positive error"
 , "making anglePt() ==> doesn't work very well. Need more work"
 , "include PGreenland's TextGenerator."
 , "COLORS for default colors. Set LabelPts default colors to COLORS"
]
,["20140619_3", "Plane improved: len(pqr) can be >3. But all faces connect to pqr[0]"]
,["20140619-2", "LabelPt(), LabelPts(). Both need doc. "
  ,"randPt, randPts x,y,z range doesn't seem to work", "LabelPts()" ]
,["20140619-1", "ColorAxes()"]
,["20140618-1", "Dim(), needs fixes on the angle when vp=false"]
,["20140613-5", "Rename: dx=>distx, dy=>disty, dz=>distz."]
,["20140613-4", "Done rewriting doc and test to fit new doctest."]
,["20140613-3", "new func: select()"
		,"Rewriting doc and test to fit new doctest ... down to split()"]
,["20140613-2", "newx(), newy(), newz()", "tests for isOnlinePt, planecoefs"
		,"Rewriting doc and test to fit new doctest ... down to planecoefs()"]
,["20140613-1", "New: isOnPlane()"
		,"Rewriting doc and test to fit new doctest ... down to Plane()"]
,["20140612-2", "onlinePt takes only 3d pts.", "Random tests for onlinePt()"
		,"Rewriting doc and test to fit new doctest ... down to onlinePts()"]
,["20140612-1", "Done migration from index(x,o) to index(o,x)."]
,["20140611-3", "About to make a big change to change index(x,o) to index(o,x) to be 
consistent with other functions."]
,["20140611-2", "doctest: change arg from (doc,recs=[], ops=[], ops2=[]) 
to (doc,recs=[], ops=[], scope=[]). So we can do:
 doctest(doc, recs, ops, [''i'',3])
but not 
 doctest(doc, recs, ops, [''scope'',[''i'',3]] )
"]
,["20140611-1", "doctest: for functions difficult to test (like, _fmt), testcount is 0, 
so do not show 'Tests:', show 'Usage:' instead."
]
,["20140610-5", "scadex_doctest: some improvements."
			, "Rewriting doc and test to fit new doctest ... down to echoblock()" ]
,["20140610-4", "scadex_doctest: some improvement on its header" ]
,["20140610-3", "doctest(): header adds some highlighting; need re-factoring but we'll leave it for now. " ]
,["20140610-2", "doctest() done. " ]
,["20140610-1", "Rename scadex_test to scadex_doctest and improve header, footer" ]
,["20140609-3", "Fine-tuning doctest()"
		, "On the way to change doctest mode 0 to list all func names" ]
,["20140609-2", "DT_MODES; New doctest mostly done. Needs only minor adjustment. " ]
,["20140609-1", "Combining doc() and test() into doctest() about complete. " ]
,["20140608-1", "Combine doc() and test() into doctest(). They were previously
together but separated around 5/29. Put them back to doctest(), and aiming to
have doc() and test() call doctest() with specific parameters." ]
,["20140607-1", "Using concat (instead of update) to setup ops in test(). Will gradually adapt this pattern for all modules." ]
,["20140606-3", "New module Normal(pqr)", "Fix bug in angle() for angle >90"
			, "MarkPts: Fix err that [''grid'',[ops]] doesn't have effect" ]
,["20140606-2", "New ops *funcwrap* for test. Chk angleBisectPt_test() for example"]
,["20140606-1", "New func angle(), _fmth() ", "test() uses isequal() for number comparison" ]
,["20140605-4","Rename h_() to _h(), s_() to _s()"]
,["20140605-3","test() refined."]
,["20140605-2","New func subops(); test() now can show doc (when [''showdoc'',true])."]
,["20140605-1","New function _getDocLines(), so doc can be called to either doc() or test()."]
,["20140604-1","doc for test() finished."]
,["20140603-1","Rename midAnglePt to angleBisectPt"
	,"Rename pts_2d_2_3d to add_z_to_pts"
	,"Done revising doc of each funcmod for new doc() from 
      top down"]
,
["20140601-2","New: isbool, isequal", "Remove ishash, LineNew"
	,"Disable midAngleCrossLinePts()"
	,"Revising doc of each funcmod for new doc() from 
      top down ... to midAnglePt() "]
, 
["20140601-1","Revising doc of each funcmod for new doc() from 
              \\ top down ... to isarr() "]
,["20140531-1","Remove fontPts(), hasht()", "Rename hashitems to hashhkvs"
		, "When showing doc header, replace _ with ＿ (_ -> ＿) to make it higher" 
		, "Revising doc of each funcmod for new doc() from top down ... to hashkvs "]
,["20140530-7","Remove Box()"
		 , "Revising doc of each funcmod for new doc() from top down ... to echoblock."]
,["20140530-6","Revising doc of each funcmod for new doc() from top down ... to Block."]
,["20140530-5","doc(): better algorithm to speed up. Also allows for correct \\
	display of mono-spacing text. We finally have a system that displays code and \\
	arranged ascii graph correctly in both the code editor and console. "]
,["20140530-4","doc(): new doc format that uses double back-slash \\
	to indicate line-breaking", "_tab(): convert tab to html spaces"]
,["20140530-3","test() done, including scoping for test records "]
,["20140530-2","test() mostly done. "]
,["20140530-1","test() basic done. Still buggy."]
,["20140529-3","Start to seperate doctest into doc() and test(). doc() done."]
,["20140529-2","Basic feature of doctests works fine."]
,["20140529-1","Rename fmt to _fmt. Failed to make deep fmt into array."]
,["20140528-4","console html tools", "Working on doctest2", "fmt()"]
,["20140528-3","onlinePts()"]
,["20140528-2","PtGrids for multiple points. Note that the 'scale' feature in PtGrid is disabled now. "]
,["20140528-1","Disgard hashif; add new arg to hash: if_v, then_v, else_v"]
,["20140527-3","hash: fix default argument recursion error", "working on hashif()"]
,["20140527-2","Mark90 auto adjusts the size of L to fit small pt xyz"]
,["20140527-1","Remove PtFrame; Remove the DashLine style op in PtGrid"]
,["20140526-1","Rename PtGrids to PtGrid and significantly simplies it to reduce process time, including change the use of DashLine to Line"]
,["20140525-1","accumulate()", "onlinePts()"]
,["20140524-1","randPts() allows setting x,y,z = 0 to put pt on plane or axis."]
,["20140522-6","Rename Line0 to Line_by_x_align and LineNew to Line0"]
,["20140522-5", "Dashline: Fix bug of over-draw when dash > line length  " ]
,["20140522-4", "LineNew()." ]
,["20140522-3", "RodCircle: done. Seems rodrotate in all conditions are good." ]
,["20140522-2", "RodCircle:Fix problems with the rodrotate --- almost" ]
,["20140522-1", "RodCircle, almost done", "inc()", "pts_2d_2_3d()", "Fixed arcPts: at a=360, should remove the last one, but not the first" ]
,["20140521-3", "randc() for random color", "OPS as a common ops for all shapes" ]
,["20140521-2", "New func: updates(h,hs) for multiple updates."
			, "Remove the feature of multiple hashes in update." ]
,["20140521-1", "Improved update: can update up to 3 hashes at once" ]
,["20140520-4", "Working on : More Rings; improving update" ]
,["20140520-3", "One more demo for Ring" ]
,["20140520_2-2", "Done docs for new hash design. Ready to merge back to scadex" ]
,["20140520_2-1", "Found that all files before this have wrong year tag (2013 should have been 20140. Embarrassing.", "Finish the doc for rotangles_p2z and rotangles_z2p" ]
,["20130518-3", "New: hash2 using [k,v,k,v ...] but not [[k,v],[k,v]...] as data. " ]
,["20130518-2", "Ring: add feature to mark rod colors with ''markrod''", "Ring_demo() --- weird circle line around the line through. " ]
,["20130518-1", "Line: new arg extension0/extension1 for line extension", "arcPts: add ''a==360&&_i_==0?[]:'' to skip _i_=0 when a=360"
			,"shortside(),longside()" ]
,["20130517-4", "New module Ring()"]
,["20130517-3", "New function dist(pq) (same as norm(p-q))", "onlinePt got new argument *dist*"]
,["20130517-2", "Rename all rm (rotation matrix) to RM."]
,["20130517-1", "Let rm2z uses rotangles_p2z"]
,["20130516-3", "Revising doc down to reverse()", "replace(): add replacing array item"]
,["20130516-2", "Mark90()"]
,["20130516-1", "is90()"]
,["20130515-1", "Found Line's bug source in rotangles_z2p( Not in rotangles_p2z). Fixed"]
,["20130514-5", "Fix bug (unable to rotate [x,y,0] to z) in rotangles_p2z. The bug in Line still there"]
,["20130514-4", "The bug in Line could be due to the new-found bug in rotangles_p2z -- unable to rotate [x,y,0] to z-axis. "]
,["20130514-3", "Revising function docs to use new doctest"
  , "Found bug in Line (but not in Line0): unable to draw lines on xy-plane (like [[1,1,0],[-1,2,0]]. "]
,["20130514-2", "Revising function docs to use new doctest"
  , "Add *markpts* argument to Line0 and Line"]
,["20130514-1", "Revising function docs to use new doctest", "Rename getd() to trueor()", "Make hash return [v1,v2...] when item = [k,v1,v2...]", "Rename the argument *strick* in ishash to *relax"]
,["20130513-6", "Revising function docs to use new doctest", "Remove h_ feature from echo_ and create a new func echoh"]
,["20130513-5", "Revising function docs to use new doctest", "Func del allows byindex" ]
,["20130513-4", "Revising function docs to use new doctest", "Rename countType??? to count???" ]
,["20130513-3", "doctest: fine tuning; Revising function docs to use new doctest" ]
,["20130513-2", "doctest: fine tuning; Revising function docs to use new doctest" ]
,["20130513-1", "doctest: fine tuning, looks good" ]
,["20130512-4", "doctest: fine tuning, new arg: mode" ]
,["20130512-3", "doctest done, remove DT_old and doctest3" ]
,["20130512-2", "doctest3 clean up", "rename: doctest=>DT_old, doctest3=>doctest" ]
,["20130512-1", "doctest3, mainly based on doctest, works fine." ]
,["20130509-2", "sum(arr)","Trying doctests, good idea, almost done, but extremely slow, maybe more than 100x slower than doctest, has to give up" ]
,["20130509-1", "arrblock()" ]
,["20130507-3", "Remove Arrow()", "Adding thickness for plane(working)" ]
,["20130507-2", "Plane: allows for changing frame settings " ]
,["20130507-1", "randPt() (only one pt)" ]
,["20130506-1", "Block " ]
,["20130505-4", "Plane: done mode 1 ~ 4 " ]
,["20130505-3", "squarePts()" ]
,["20130505-2", "Plane: mode 1 done" ]
,["20130505-1", "Chain: change argument pattern to ops" ]
,["20130504-2", "Plane: allows 4-side plane", "midPt()", "midCornerPt()" ]
,["20130504-1", "Line: failed to make touch work","Rename Surface to Plane" ]
,["20130503-4", "Line: found mechanism to find the p3rd -- the pt on surface. Still under construction." ]
,["20130503-3", "Line: let shift apply not only to block but to line, too", 
				"Remove Line2. In Line, rename blockAnchor to shift" ]
,["20130503-1", "Line style block: allows any shift" ]
,["20130502-4", "echom(modulename).", "Detailed Line_demo_???"
			, "Line:Fix rotate in block style" ]
,["20130502-3", "Del Line, rename Line3 to Line, style=block to be fixed."]
,["20130502-2", "module Line0, to replace Line as the simple line module."]
,["20130502-1", "Fix Line3 (same problems also in Line2) head/tail at wrong side when switched"]
,["20130501-2", "Working on Line3 head/tail cone"]
,["20130501-1", "rotangles_z2p() made with RMz2p_demo(). Leave RMz2p_demo for future references", "Rename rot2z_angles to rotangles_p2z"]
,["20130428-1", "rmx(), rmy(), rmz(), rm2z(), rot2z_angles()"]
,["20130425-3", "Complete PtGrid new scale feature and doc"]
,["20130425-2", "hasAny()", "arrItcp()"]
,["20130425-1", "has()", "Add 'scale' to PtGrid()"]
,["20130424-1", "Remove dot(pq) ==> simply p*q; PtGrid takes new arg 'scale'"]
,["20130423-4", "Surface()","distPtPl()", "dot(pq)"]
,["20130423-3", "Line2: Improved. New shift:corner2."]
,["20130423-2", "Line2: Improve doc."]
,["20130423-1", "Line2: Rename style panel to block."]
,["20130422-6", "Add new style panel to Line2"
		     , "randPts() take new argument r."]
,["20130422-5", "Fix Line2 head/tail color and transp issues."]
,["20130422-4", "Re-organize Line2 examples."]
,["20130422-3", "Bugfix Line2."]
,["20130422-2", "Add fn arg to DashLine, Line"
			,"Fix arrow position problem in Line2" ]
,["20130422-1", "Rename markPts to MarkPts. Improve MarkPts()"]
,["20140421-1", "markPts(), randPts()"]
,["20140420-3", "Line2(): new head/teail style: cylinder"]
,["20140420-2", "Improved Line2()"]
,["20140420-1", "del()", "Raname: arrmax=>maxa, arrmin=>mina", "Line2()"]
,["20140419-3", "minPt()"]
,["20140419-2", "minxPts(), minyPts(), minzPts()"]
,["20140419-1", "mina(),maxa()"]
,["20140418-5", "Improved Box()"]
,["20140418-4", "Add a strick flag to ishash()"]
,["20140418-3", "Improve update: item order of h2 doesn't matter."]
,["20140418-2", "hashi()", "hashkv()"]
,["20140418-1", "Rename hashd to hasht", "New hashd()"]
,["20140417-3","Box()."]
,["20140417-2","DashBox()."]
, ["20140417-1","boxPts() improved."]
,["20140416-3","boxPts()"]
,["20140416-2","getd()", "hashd()"]
,["20140416-1","Remove DottedLine()", "Modify PtGrid to take hash argument", "PtGrid_test()"]
,["20140415-4","Modify DashLine to take hash argument"]
,["20140415-3", "echo_()"]
,["20140415-2","Rename rep() to s_()", "h_()"]
,["20140415-1", "normal_demo()","Allow wrap in index()"]
,["20140413-1", "PtGrid()"]
,["20140412-1","Rename Lines to Chain", "scadex_demo()", "DashLine()","DottedLine()"]
,["20140409-2","normal()", "planecoefs()"]
,["20140409-1","getcol()", "transpose()","Rename semiAnglePt to midAnglePt"
			, "Rename crossLinePts to othoCrossLinePts"
			, "Rename crossPts to begCrossLinePts" ]
,["20140406-5", "othoCrossLinePts()", "semiAnglePt()"]
,["20140406-4","Line_test() and _demo()"
			, "Use convert_scadex.py to create scadex.scad and scadex_test.scad" ]
,["20140406-3", "Fix othoPt", "Line() module"]
,["20140406-2","Restructure the code layout a bit to make them consistent"]
,["20140406-1","disable dist()", "Define doc of each function with name of it"]
,["20140405-1", "Change [_] in rep() to {_}", "add echo_level arg to doctest"
	, "dx(),dy(),dz()","linecoef", "slope()", "othoPt()", "begCrossLinePts()"
	, "Rename arcPoints to arcPts"]
,["20140402-1", "Rename curvePoints() to arcPoints"]
,["20140401-2", "reverse();"]
,["20140401-1", "Hey it's April Fool!; curvePoints();In shift(), change arg i to _i_;"]
,["20140326-2", "Remove test_run() (use doctest instead); Update doc for doctest(); scad_ver" ]
,["20140326-1", "doctest(); Rename items() to vals(); Remove stepdown()" ]
,["20140324-1", "multi_test(); echoblock()" ]
,["20140321-2", "shrink()" ] 
,["20140321-1", "stepdown()" ]
,["20140320-3", "Trying to add doc feature in test_run(), under construction." ]
,["20140320-2", "Allows update to add single [k,v] pair more easily
			get(o,i) now get(o,i,[j]) that can retrieve multiarray data." ]
,["20140320-1", "more examples for update()." ]
,["20140319-6", "update(). Surprisingly easy." ] 
,["20140319-5", "ishash();delkey()" ]
,["20140319-4", "bugfix: test_run(): make sure no * is shown when the test-specific 
			asstr is set to false in cases where function-wise asstr is true;
			Setup norm_test() to demo the use of test_run() (asstr, rem, etc);
			Add indent to output when test fails in test_run()" ]

,["20140319-3", "countType(); Rename count???Items to count???Type, and use countType( except countNum )" ]
,["20140319-2", "Only int allowed in i in get(o,i) now;
			 slice(s,i,j)=undef when i,j order wrong" ]
,["20140319-1", "inrange_test()" ]
,["20140318-1", "keys()" ]
,["20140315-1", "Start using 2013.03.11 with concat feature turned on; 
           bugfix: previous ver doesn't show * for test-specific asstr;
           slice works on array; 
           split() " ]
,["20140313-1", "int(),countArr(),countInt(), countNum(); Add "*" for asstr" ] 
,["20140312-2", "Re-doc for test_run(); minor bug fixes." ]
,["20140312-1", "1.Restructure again for easier test cases writing for user." ]
,["20140311-1", "1.Restructure test arguments to allow for 'asstr' and 'rem' options
		    2. One test_echo() for all (including those with optional args)" ]
,["20140309-3", "rename from tools to scadex" ]
,["20140309-2", "More doc for the functions. " ]
,["20140309-1", "Remove findw (use index instead)" ] 
,["20140308-3", "more thorough doc." ]
,["20140308-2", "doc for test tools" ]
,["20140308-1", "test features done; code structured" ]
,["20140307-4", "test_echo1(), test_echo2()" ]
,["20140307-3", "rep()" ]
,["20140307-2", "make index() take string. deprecricating findw()." ]
,["20140307-1", "fix bugs in findw." ]
];

//
//Line_demo();
SCADEX_VERSION = scadex_ver[0][0];
//scadex_demo();

echo_( "<code>||.........  <b>scadex version: {_}</b>  .........||</code>",[scadex_ver[0][0]]);
echo_( "<code>||Last update: {_} ||</code>",[join(shrink(scadex_ver[0])," | ")]);


$fn=20;


getWrapPts=[ "Given n points , return a nx2 multmatrix that each"
			,"2-item array has 2 points corresponding to the "
			,"original point given. If it's in the edge of line,"
			,"the points are othoCrossLinePts; otherwise, "
			,"midAngleCrossLinePts. Each new pt is r away from the"
			,"original pt. The main purpuse of this is to provide "
			,"points to make a 'wrapping' or 'coating' along a "
			,"series of pts."
			];
function getWrapPts( pts, r=1, _i_=0 )=
(
	//_i_==len(pts)-1?[]
	//:
	_i_>= len(pts)-1? [ begCrossLinePts( [ [pts[_i_],pts[_i_-1]]], r ) ]
		:concat( [_i_==0?  begCrossLinePts( [pts[0],pts[1]], r ) 
				:midAngleCrossLinePts( [pts[_i_],pts[_i_+1],pts[_i_+2]],r)
				]
			   , getWrapPts( pts, r=r, _i_=_i_+1 ) 
			   )	
);

module getWrapPts_demo()
{
	pts= [[2,-3,1], [1,0,3], [-2,1,4], [-4,2,2], [-4,4,0] ];
	Chain( pts, usespherenode=true );
	npts= getWrapPts(pts);
	echo(npts);

}
//getWrapPts_demo();


module alignface_try()
{
	p0 = [2,2,1];
	pqr1 = [ p0, [4,1,2], [3,2,-1]];
	pqr2 = [ p0, [3,0,-1], [1,2,0]];

	
	Chain(pqr1, r=0.01, closed=true);
	color("blue")
	Chain(pqr2, r=0.01, closed=true);

	echo( "planecoefs pqr1: ", planecoefs(pqr1));
	echo( "planecoefs pqr2: ", planecoefs(pqr2));

	pc1 = planecoefs([pqr1[0]-p0,pqr1[1]-p0,pqr1[2]-p0]);
	pc2 = planecoefs([pqr2[0]-p0,pqr2[1]-p0,pqr2[2]-p0]);
	echo( "planecoefs pqr1-p0: ", pc1);
	echo( "planecoefs pqr2-p0: ", pc2);

	translate(-p0)
	//rotate( pc1)
	{color("khaki")
	rotate(slice(pc1,0))//,-1))
	Chain(pqr1, r=0.02, closed=true);
	color("lightblue")
	Chain(pqr2, r=0.02, closed=true);
 	}

	polyhedron( points = pqr1, faces=[[0,1,2]] );
	polyhedron( points = pqr2, faces=[[0,1,2]] );
	PtGrid(p0);
}
//alignface_try();


module formatEcho(strarr,ops)
{
	block_definition=[
	 "[","]"
	,"(",")"
	,"''","''"
	];
	
	exclusive_blocks= ["''"];
	
	/*function fmt( line ) = 
	( 
	  len(line)==0?""
	  : (line[0]=="'" && line[1]=="'") ?
		str( 
			
			haskey( block_definition, line[0] )?
		
		
	);
	ops=update([
	*/
	


} 

//$fn=100;
//Chain( arcPts(a=120, r=3, count=20 ),r=0.2);

//getWrapPts_demo();


trueor2=[["trueor","x,dval","any|dval", "Hash"]
	 ,["Means ''trueorefault''. Given x,dval, if x is evaluated "
	  ,"to be true, return x. Else dval. Same as x?x:dval but "
	  ,"x only appears once."
	  ]  
	 ];


module help(fname)
{
	echo_( " | {_} ({_}) |=> {_} (group: {_} )", fname[0] );	
	
	echoblock(fname[1]);
}
//help(trueor2);

//========================================================
get2=[
	 ["get", "o,i,[j]", "item", "Index,Inspect,Array,String" ]
	,[ "Get the i-th item of o (a str|arr). The optional 2nd "
	 , "index, j, is for the j-th item of the returned array"
	 , "in cases of a matrix(multidimentional array). "
	 , "Both i,j could be negative."
	 ]
	];

get2_tests=[ 
		  "## array ##"
		 , [ [[20,30,40,50], 1], get( [20,30,40,50], 1 ), 301 ]
    		 , [ [[20,30,40,50], -1], get([20,30,40,50], -1), 50 ]
    		 , [ [[20,30,40,50], -2], get([20,30,40,50], -2), 40 ]
    		 , [ [[20,30,40,50], true], get([20,30,40,50], true), undef ]
    		 , [ [[], 1], get([], 1), undef ]
		 , [ [[[2,3],[4,5],[6,7]], -2], get([[2,3],[4,5],[6,7]], -2), [4,5] ]
    		 ,"## multiarray ##"
		 , [ [[[2,3],[4,5],[6,7]], -2, 0], get([[2,3],[4,5],[6,7]], -2,0), 4 ]
    		 , [ [[[2,3],[4,5],[6,7]], -2, -1], get([[2,3],[4,5],[6,7]], -2,-1), 5 ]
    		 , "## string ##"
    		 , [ ["abcdef", -2], get("abcdef", -2), "e" ]
    		 , [ ["abcdef", false], get("abcdef", false), undef ]
    		 , [ ["", 1], get("", 1), undef ]
    		 ];


/* 
---------------------------------------------

multmatrix( m = 
[
	 [ X, tzx ,tyx, x ]
	,[ tzy, Y, txy, y ]
	,[ txz, tyz, Z, z ]
	,[0, 0, 0, 1 ]
])

tzx: tilt with surface on xz plane unchanged and with the 
	 z-axis edge attached (unchanged ) and tilts along x direction.

---------------------------------------------
	A=[a,b,c]
	B=[x,y,z]

dot prod: 	A * B = ax+by+cz

cross prod:

	|A x B| = |A||B|cos(theta)

	A x B  = [bz-cy, cx-az, ay-bx ] 

	which is the determinant of : 

		 [[i,j,k]
		  [a,b,c]
		  [x,y,z]]

---------------------------------------------
3 vectors, A,B,C, forms a 3-D object.

The vol is : | A * ( B x C ) |

If it is 0, means A,B,C on the same plane 
	
	= abs( A * cross(B,C) ) == 0

*/ 
 
/*
Rotation Matrix
http://www.cs.brandeis.edu/~cs155/Lecture_07_6.pdf
*/

/*  Line formula in 3d space

  vector form of the equation of a line:

    P= Q +t*[a,b,c] 

    where [a,b,c] = Q-P

  parametric form of the equation of a line:

    P.x = Q.x + at
    P.y = Q.y + bt
    P.z =	 Q.z + ct

  symmetric equations of the line:

	( Q.x-P.x )/a = ( Q.y-P.y )/b = ( Q.z-P.z )/c 


    P.z =	 Q.z + ct

       
http://mathforum.org/library/drmath/view/65721.html

Good lesson:
http://tutorial.math.lamar.edu/Classes/CalcIII/EqnsOfLines.aspx

*/

/*
Formula in 3D space:
http://math.stackexchange.com/questions/73237/parametric-equation-of-a-circle-in-3d-space

A circle of radius r on the pqr plane, center at q:

x(θ)= Q.x+ r*cos(θ)*(P/norm(P)).x+rsin(θ)*(R/norm(R)).x
y(θ)= Q.y+ r*cos(θ)*(P/norm(P)).y+rsin(θ)*(R/norm(R)).y
z(θ)= Q.z+ r*cos(θ)*(P/norm(P)).z+rsin(θ)*(R/norm(R)).z

*/

/*
Kit Wallace 3D polyhedron index
http://kitwallace.co.uk/3d/solid-index.xq?3D=yes

http://kitwallace.tumblr.com/tagged/polyhedra

https://github.com/openscad/openscad/wiki/OEP1%3A-Generalized-extrusion-module#discretized-space-curves

*/

/*
transformation:

Understanding the Transformation Matrix in Flash 8
http://www.senocular.com/flash/tutorials/transformmatrix/

Transformation matrix function library
http://forum.openscad.org/Transformation-matrix-function-library-td5154.html

Jar-O-Math! 
http://forum.openscad.org/Jar-O-Math-td8539.html


$fn=30;
//echo("</span>");


//===========================================
// console html tools /// start
//===========================================

_SP  = "&nbsp;";
_SP2 = "　";  // a 2-byte space 
_BR  = "<br/>"; 

function _tag(tagname, t="", s="", a="")=
str( "<", tagname, a?" ":"", a
		, s==""?"":str(" style='", s, "'")
		, ">", t, "</", tagname, ">");

function _div (t, s="", a="")=_tag("div", t, s, a);
function _span(t, s="", a="")=_tag("span", t, s, a);
function _table(t, s="", a="")=_tag("table", t, s, a);
function _pre(t, s="font-family:ubuntu mono;margin-top:5px;margin-bottom:0px", a="")=_tag("pre", t, s, a);
function _code (t, s="", a="")=_tag("code", t, s, a);
function _tr   (t, s="", a="")=_tag("tr", t, s, a);
function _td   (t, s="", a="")=_tag("td", t, s, a);
function _b   (t, s="", a="")=_tag("b", t, s, a);
function _i   (t, s="", a="")=_tag("i", t, s, a);
function _u   (t, s="", a="")=_tag("u", t, s, a);
function _h1  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h1", t, s, a); 
function _h2  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h2", t, s, a); 
function _h3  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h3", t, s, a); 
function _h4  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h4", t, s, a); 


function _color(t,c) = _span(t, s=str("color:",c));
function _red  (t) = _span(t, s="color:red");
function _green(t) = _span(t, s="color:green");
function _blue (t) = _span(t, s="color:blue");
function _gray (t) = _span(t, s="color:gray");

function _tab(t,spaces=4)= replace(t, repeat("&nbsp;",spaces)); // replace tab with spaces
function _cmt(s)= _span(s, "color:darkcyan");   // for comment

// Format the function/module argument
function _arg(s)= _span( s?str("<i>"
					,replace(replace(s,"''","&quot;"),"\"","&quot;")//"“"),"\"","“")
					,"</i>"):""
					, "color:#444444;font-family:ubuntu mono");


module _Div ( t, s="", a="" ){ echo( _div (t, s, a)); }
module _Span( t, s="", a="" ){ echo( _span(t, s, a)); }
module _B   ( t, s="", a="" ){ echo( _b   (t, s, a)); }


//===================================
// Builtin doc
//===================================

cube = [["cube","size,center","n/a","3D, Shape"],
"
 Creates a cube at the origin of the coordinate system. When center\\
 is true, the cube will be centered on the origin, otherwise it is \\
 created in the first octant. The argument names are optional if the\\
 arguments are given in the same order as specified in the parameters.\\
 \\
 cube(size = 1, center = false);\\
 cube(size = [1,2,3], center = true);\\
"];

norm = [["norm","v","number","Math, Geometry"],
"
 Givena vector (v), returns the euclidean norm of v. Note this\\
 returns is the actual numeric length while len returns the number\\
 of elements in the vector or array.\\
"];

//doc(norm);
//doc(cube);
//doc(_fmt);

/*

:   &  ^.  '-_   -._  -._    -.._
 :   &   ^.   '-_   '-._ ''--._  ''--.._
  :   &    ^.    '-_    '-._   ''--._   ''--.._
   :   &     ^.     '-_     '-._     ''--._    ''--.._
    :   &      ^.      '-_      '-._       ''--._     ''--.._ 
    1   1       2        3         4            6             7  
           
            |
     '-_    |
        '-_ | 
   --------'+----------
            | '-_
            |    '-_
            |       '   


           |
      ^.   | 
        ^. |      
    -------+--------
           |^.     
           |  ^.      
           |    ^.
         


      P3 
	   &'-_
	   |&  '-._     
	   | &     '-._
	   |  &        '-_ 
	   |   &         _'P2
	   |  b &      -' /
	   |     &  _-'  /	
	   |      X'    /
	   |  a -' &   /
	   |  -'    & /
	   |-'_______&
     P1            P4


	//	      5-------4
	//	     /|      /|
	//	    / |  Q'-/----------C
	//	   /  |  /\/'-|       /
	//	  /   6-/-/---7'-.   /
	//   /   / / /  \/ '. R2/
	//	1-------0   /\  	'./
	//	|  / /  |  /  R1  /'.
	//	| / P.--|-/------D   A    
	//	|/    '.|/          /
	//  2-------3          /
	//           '.       /
	//             '.    /
	//               '. /
    //                 B

;;       N (z)
;;       |
;;       |________
;;      /|'-_     |
;;     / |   '-_  |
;;    |  |      : |
;;    |  Q------|---- M (y)
;;    | / '-._/ |
;;    |/_____/'.|
;;    /          '-R
;;   /
;;  P (x)
;;
;; In the graph below, P,Q,R and 0,4 are co-planar. 
;;
;;       _7-_
;;    _-' |  '-_
;; 6 :_   |Q.   '-4
;;   | '-_|  '_-'| 
;;   |    '-5' '-|
;;   |    | |    '-_
;;   |   _-_|    |  'R
;;   |_-' 3 |'-_ | 
;;  2-_    P|   '-0
;;     '-_  | _-'
;;        '-.'
;;          1




;;       _6-_
;;    _-' |  '-_
;; 5 '_   |     '-7
;;   | '-_|   _-'| 
;;   |    '-4'   |
;;   |    | |    |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0

	//                          _-5
	//                       _-'.' '.
	//                    _-' .'     '.
	//                 _-'  .'         '.
	//              _-'   _6    _-Q----_-4--------------A 
	//            1'   _-'  '_-'   '_-'.'            _-'
	//          .' '.-'   _-'  '._-' .'           _-'
	//        .' _-' '. -'    _-'. .' '.       _-'
	//      .'_-'   _-''.  _-'   _-7    R1  _-'
	//	 2-'    P-----0-'----------------B     
	//      '.         .'  _-'
	//        '.     .' _-'
	//          '. .'_-'
	//            3-'
	//                     


;;                     D   S
;;                     :  /|
;;                     : / |
;;                     :/  |   
;;                     /   |  .'
;;                    /:  .|'
;;       B        :  / :.' |
;;  P    |        : / .C---:
;;  |'.  |   :    :/.'_.-''
;;  |  '.| --:----R--'------
;;  |    |.  :  .'
;;  |    | '.:.'
;; -|----|-_.Q -------    
;;  |.'_.|'.'
;;  '----A'
;;     .'

             R      
            / '-_
           /     '-_
          /         '-_
         /             '-_
       P -----------------'Q

             ___
         .:'' | ''-
        /     |  / 
       :      | / 
       +------+'-----+
       :      |      :
        :     |     /
         ':.._|_..:'



  ./{           }       
    
   {  }^2
          
   Z{    }

	2*x^2 + 5*x + 6 

*/


//===========================================
// console html tools /// end
//===========================================
/*
file structure:

	scadex_core.scad    --- core lib 
	scadex_api.scad     --- doc/test/api 
	scadex_doctest.scad --- doctest lib
	scadex_shape.scad   --- shape lib
	scadex.scad         --- calling the above 
*/

//===========================================
//
// Console html tools 
//
//===========================================

_SP  = "&nbsp;";
_SP2 = "　";  // a 2-byte space 
_BR  = "<br/>"; 

function _tag(tagname, t="", s="", a="")=
(
	str( "<", tagname, a?" ":"", a
		, s==""?"":str(" style='", s, "'")
		, ">", t, "</", tagname, ">")
);

function _div (t, s="", a="")=_tag("div", t, s, a);
function _span(t, s="", a="")=_tag("span", t, s, a);
function _table(t, s="", a="")=_tag("table", t, s, a);
function _pre(t, s="font-family:ubuntu mono;margin-top:5px;margin-bottom:0px", a="")=_tag("pre", t, s, a);
function _code (t, s="", a="")=_tag("code", t, s, a);
function _tr   (t, s="", a="")=_tag("tr", t, s, a);
function _td   (t, s="", a="")=_tag("td", t, s, a);
function _b   (t, s="", a="")=_tag("b", t, s, a);
function _i   (t, s="", a="")=_tag("i", t, s, a);
function _u   (t, s="", a="")=_tag("u", t, s, a);
function _h1  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h1", t, s, a); 
function _h2  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h2", t, s, a); 
function _h3  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h3", t, s, a); 
function _h4  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h4", t, s, a); 

function _color(t,c) = _span(t, s=str("color:",c));
function _red  (t) = _span(t, s="color:red");
function _green(t) = _span(t, s="color:green");
function _blue (t) = _span(t, s="color:blue");
function _gray (t) = _span(t, s="color:gray");

function _tab(t,spaces=4)= dt_replace(t, dt_multi("&nbsp;",spaces)); // replace tab with spaces
function _cmt(s)= _span(s, "color:darkcyan");   // for comment

// Format the function/module argument
function _arg(s)= _span( s?str("<i>"
					,dt_replace(dt_replace(s,"''","&quot;"),"\"","&quot;")//"“"),"\"","“")
					,"</i>"):""
					, "color:#444444;font-family:ubuntu mono");

module tmp()
{
	difference(){
	rotate_extrude(convexity=10,$fn=100)
	translate( [3,0,0])
		circle(0.15);
	translate([0,0,-1])
	cube([5,3,4]);
	}
		
}
//tmp();

// Output console :
// openscad scadex_doctesting.scad -o dump.png --enable=concat 2> tmp.htm



module polyhedron_test()
{
//http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#polyhedron
  pts_ = [
	       [0, -10, 60], [0, 10, 60], [0, 10, 0], [0, -10, 0], [60, -10, 60], [60, 10, 60], 
	       [10, -10, 50], [10, 10, 50], [10, 10, 30], [10, -10, 30], [30, -10, 50], [30, 10, 50]
	       ];
 

	pts = [for(p=pts_) p/10];

	//LabelPts( pts, labels="" );
	MarkPts( pts, ["labels",true] );
     faces = [
		  [0,3,2],  [0,2,1],  [4,0,5],  [5,0,1],  [5,2,4],  [4,2,3],
                  [6,8,9],  [6,7,8],  [6,10,11],[6,11,7], [10,8,11],
		  [10,9,8], [3,0,9],  [9,0,6],  [10,6, 0],[0,4,10],
                  [3,9,10], [3,10,4], [1,7,11], [1,11,5], [1,8,7],  
                  [2,8,1],  [8,2,11], [5,11,2]
		  ];

//   Below is the so-called bad faces:
     faces = [
		  [0,2,3],   [0,1,2],  [0,4,5],  [0,5,1],   [5,4,2],  [2,4,3],
                  [6,8,9],  [6,7,8],  [6,10,11], [6,11,7], [10,8,11],
		  [10,9,8], [0,3,9],  [9,0,6], [10,6, 0],  [0,4,10],
                  [3,9,10], [3,10,4], [1,7,11],  [1,11,5], [1,7,8],  
                  [1,8,2],  [2,8,11], [2,11,5]
		  ];
//     
//	for( fi=faces){
//		Plane( [for(i=fi) pts[i]], ["mode",3, "color", randc()] );
//	}
	color(undef, 0.5)
	polyhedron
    (points=pts, 
     faces = faces
     );
}

//polyhedron_test();
