include <scadx_obj_basic_dev.scad>

//========================================================
Dim=[
];

/*
   Object definition:

  guideP                                 guideQ
   |                                      |
   |  arrowP                       arrowQ |
   |  _='   lineP            lineQ  '=_   |  
   |-'-------------  label ------------'- |
   | '-_          |<- gap->|         _-'  |
   |    '                           '     |
     <-- spacer                             <-- spacer      
   P--------------------------------------Q 
   
   gap, gapP, gapQ are prop of Dim
   
   Dim:guide controls guideP and guideQ, which could be overwritten 
   by Dim:guideP, Dim:guideQ

*/

//module Dim( pqr, ops=[])
//{ pqr = len(pqr)>2?pqr:concat( pqr, [randPt()]);
//// _mdoc("Dim"
////, "
////");
//    
//    df_ops=   
//    
//        [ "r"     , 0.015  // rad of line
//        , "color" , "blue"  
//        , "transp", 0.4    
//        , "spacer", 0.2    // dist btw guide and target   
//        , "gap"   , 0.4    // gap between two arrows
//        , "h", 0.5  //  vercal pos of line.  Could be negative
//                    /* h ________      0.5         0
//                        |   1    |  |_______|  |       |
//                        |        |  |       |  |_______|
//                        P        Q  P       Q  P       Q
//                    */
//        , "vp"    , true   // is following viewport ?
//        , "conv", undef
//        , "decimal", undef
//        , "maxdecimal", undef
//        
//        , "followslope", true   // 
//        /*  
//            followslope= true:  R decides the plane and side  
//                /               where the dim line lies.   
//               /'-._
//              /     '-._  /
//             P'-._      '/
//             |    '-._  /        
//             |        'Q
//             |         | 
//             |         R 
//
//            false:
//
//             |_________|   R further decides direction of guides
//             |         |   and dim line. 
//             P'-._     |
//             |    '-._ |        
//             |        'Q
//             |         | 
//             |         R 
//        */
//        
//        // Subunit settings:
//        
//        // Set to true to use default ops. false to hide. hash
//        // to customize. For label, string to replace dim value.
//        
//        , "label", true  // false or hash, or text
//        
//        , "guides", true  // false or hash
//        , "guideP", true  // false or hash
//        , "guideQ", true  // false or hash
//
//        , "arrows", true  // false or hash
//        , "arrowP", true  // false or hash
//        , "arrowQ", true  // false or hash
//
//        ];        
//    // 3 subunits: guides, arrow, label
//    
//    df_guides= ["len", 0.8];
//    df_arrows= ["headP",false];
//    df_label=
//        [ "scale",0.4
//        , "prefix",""
//        , "suffix",""
//        , "conv", undef
//        , "decimal", undef
//        , "maxdecimal", undef
//        , "text", undef // set to string replace df value 
//        ];
//        
//    /* subunit "arrow" composes of 2 Arrow modules, one points to
//       hP, one hQ. Seperated by gap. The ops is designed as though
//       that the entire df_arrow is one Arrow module, 'cos it's 
//       unlikely for the two arrows to be formatted differently:
//    
//        |    arrowP                   arrowQ   |
//        |  _='                           '=_   |  
//      hP|-'-------------  label ------------'- |hQ
//        | '-_          |<- gap->|         _-'  |
//        |    '                           '     |
//           <-- spacer                             <-- spacer      
//        P--------------------------------------Q 
//        
//        Note that on each of the two Arrows, it's own internal
//        arrowP (not the arrowP of this Dim module)  is disabled. 
//        */
//
//    _ops = update( df_ops, ops); // df_ops updated w/ user_ops
//    function ops(k,df) = hash(_ops,k,df);
//   
//    //
//    //================================================= sub ops 
//    //
//    
//    function _getsubops_( sub_dfs )=
//        getsubops( ops, df_ops= df_ops
//        , com_keys= ["r", "color", "transp"]
//        , sub_dfs= sub_dfs
//        );
//        
//    ops_guideP= _getsubops_( ["guides", df_guides, "guideP", [] ] );
//    ops_guideQ= _getsubops_( ["guides", df_guides, "guideQ", [] ] );
//    ops_arrowP= _getsubops_( ["arrows", df_arrows, "arrowP", [] ] );
//    ops_arrowQ= _getsubops_( ["arrows", df_arrows, "arrowQ", [] ] );
//    function ops_guideP(k)= hash( ops_guideP, k); 
//    function ops_guideQ(k)= hash( ops_guideQ, k);
//    function ops_arrowP(k)= hash( ops_arrowP, k);
//    function ops_arrowQ(k)= hash( ops_arrowQ, k);
//
//    _label = ops("label");
//    ops_label = isstr(_label)
//            ? concat(["text", _label], df_label)   
//            : ishash(_label)
//            ? concat(_label, df_label )
//            : df_label;
//
//    function ops_label(k)= hash( ops_label, k); 
//
//    //
//    //================================================= var
//    //
//    
//    r     = ops("r");
//    color = ops("color");  
//    transp= ops("transp");    
//    spacer= ops("spacer");
//    gap   = ops("gap");
//    h     = ops("h");
//    followslope= ops("followslope");
//    
//    a_pqr = angle(pqr);
//    
//    //
//    //================================================= calc pts
//    //
//    
//    P = pqr[0];
//    Q = pqr[1];
//    Ro= pqr[2];
//    
//    /* When followslope = true, R has to be recalc
//       such that new a_PQR = 90
//
//            /  
//           /'-._
//          /     '-._  /
//        P'-._       '/
//         |    '-._  /        
//         |        'Q
//         |        /| 
//         |       / Ro
//                R
//            /                    
//           /'-._
//          /     '-._  /
//        Q'-._       '/
//        /|    '-._  /        
//       / |        '| P
//      R  |         | 
//         Ro                                          
//    */
//    R = followslope
//         ? ( a_pqr==90
//             ? Ro 
//             : anglePt( pqr, a=90)
//           )  
//         : Ro ;  
//       
//     /*
//    P----Q   Need X to make pts along XP line
//    |    |
//    X----R
//    */   
//    X= cornerPt( [P,Q,R] ); 
//       
//    dPQ = norm(P-Q);
//    L = (!followslope) && a_pqr!=90         // L= the dim to be reported
//        ? norm( P- othoPt(p201(pqr))):dPQ;
//    
//    //-------------------------------------------- guide 
//    /*
//            gP2         gQ2  --- 
//             |           |    |
//          hP +-----------+ hQ | g_len  
//             |           |    |
//         gP0=gP1         gQ1 ---
//    spacer-->            |    |
//             Po          |    | gQx_len  
//             | '-_       |    |  
//             X    '-_   gQ0  ---       
//             |       '-_    <--- spacer    
//             |          'Qo 
//             |           |
//             |           Ro     
//         
//    gPx=0 @ followslope=true, or a_PQR>=90
//    gQx=0 @ followslope=true, or a_PQR<=90
//    */ 
//    h = ops("h");
//    //echo("h",h);
//    
//    g_len = hash( ops_guideP, "len", hash(df_guides,"len") );
//    
//    // extra len of guides due to slope of PQ
//    gPx_len= (!followslope)&& a_pqr<90? shortside( L, dPQ ): 0;
//    gQx_len= (!followslope)&& a_pqr>90? shortside( L, dPQ ): 0;
//    
//    gP0 = onlinePt( [P,X], len= -spacer );
//    gP1 = onlinePt( [P,X], len= -spacer - gPx_len );    
//    gP2 = onlinePt( [P,X], len= -spacer - gPx_len -  g_len);    
//    hP  = onlinePt( [gP1,gP2], ratio= h);
//    
//    gQ0 = onlinePt( [Q,R], len= -spacer );
//    gQ1 = onlinePt( [Q,R], len= -spacer - gQx_len );    
//    gQ2 = onlinePt( [Q,R], len= -spacer - gQx_len -  g_len);    
//    hQ  = onlinePt( [gQ1,gQ2], ratio= h);
//
//    //echo("ops(''guides'')", ops("guides"));
//    //echo("ops_guideP, ops_guideQ",ops_guideP, ops_guideQ);
//    if( ops_guideP ) Line( [gP0, gP2], ops_guideP); 
//    if( ops_guideQ ) Line( [gQ0, gQ2], ops_guideQ); 
//   
//    /* 
//    Allows arrow heads to be turned off from the obj level. Each arrow 
//    (of arrowP, arrowQ) is byteself an Arrow module, in which the dir is
//    decided by it's start/end pts, named, P,Q, internally (lets call them 
//    iP, iQ here). In Dim, they are defined as pointing from the gap outward:
//    
//         |    arrowP                   arrowQ  |
//         |  _='                           '=_  |  
//    hP=iQ|-'------------iP      iP-----------'-| hQ=iQ
//         | '-_          |<- gap->|         _-' |
//         |    '                           '    |
//          <-- spacer                            <-- spacer      
//         P-------------------------------------Q 
//    
//    So on the Dim level, we have a property arrows containing two Arrows,
//    and each have its own headP, headQ. 
//    
//    The default of Dim is to hide the headP of Arrow module (defined in df_arrows).
//    
//    To turn off the arrow head of arrowP (of Dim) in Dim, we can use: 
//    
//    [1]  Dim( ...[ "arrowP"          <=== arrowP of Dim
//                , ["headQ", false] <=== define the arrowQ of arrowP
//                ]
//           );      
//    
//    We make this possible: 
//    
//    [2]  Dim( ...[ "arrowP"          <=== arrowP of Dim
//                , ["head", false]  <=== define the arrowQ of arrowP
//                ]
//           );    
//    
//    That is, no need to specify headQ. You can also replace false with 
//    a valid hash to set the arrow head. 
//    
//    Both [1] and [2], or even ["heads",...], give the same result.
//    
//    */
//
//    //
//    //================================================= line
//    //
//    
//    // hP, hQ: where line intersect guides
//    // gapP, gapQ: where the arrow line starts. 
//    //             determined by ["line",["gap",0]]   
//    /*
//         gP2                    gQ2
//          |                      |
//       hP +-----gapP    gapQ-----+ hQ
//          |                      |
//         gP1                    gQ1    
//    
//       But, remember that the real hP needs to take
//       into account the len of arrow head
//    
//             _-'| 
//       _hP <----+--------- gapP
//             '-_| hP 
//    
//           |<-->| len of arrow head 
//    
//    */
//
//    gap = ops("gap"); 
//    lineL = (L-gap)/2;
//    
//    begP = onlinePt( [hP,hQ], len=lineL);
//    begQ = onlinePt( [hQ,hP], len=lineL);
//
//    if(ops_arrowP) Arrow( [ begP, hP], ops_arrowP );
//    if(ops_arrowQ) Arrow( [ begQ, hQ], ops_arrowQ );
//         
//    //       
//    //================================================= label
//    //
//    
//	module transform(followVP=ops("vp"))
//	{
//        //echo("vp", followVP);
//		translate( midPt( [hP,hQ] ) )
//	     color( ops("color"), ops("transp") )
//		if(followVP){ rotate( $vpr ) children(); }
//		else { rotate( rotangles_z2p( Q-P ) )
//			  rotate( [ 90, -90, 0] )
//			  children();
//			}
//	}
//	conv = ops("conv");
//	//echo("conv",conv, "L", L, "numstr(L, uconv=uconv)", numstr(L, conv=conv));
//	transform()
//	scale(0.04) text(str(ops_label("prefix")
//						  //, L, "="
//						  , numstr(L
//								, conv=ops("conv")
//								, d=ops("decimal")
//								, maxd=ops("maxdecimal")
//								)
//						  , ops_label("suffix") )
//                    , valign="center"
//                    , halign="center");
//   
//         
//}// Dim
//    

module Dim( pqr, ops=[])
{ pqr = len(pqr)>2?pqr:concat( pqr, [randPt()]);
// _mdoc("Dim"
//, "
//");
    
    df_ops=   
    
        [ "r"     , 0.015  // rad of line
        , "color" , "blue"  
        , "transp", 0.4    
        , "spacer", 0.2    // dist btw guide and target   
        , "gap"   , 0.4    // gap between two arrows
        , "h", 0.5  //  vercal pos of line.  Could be negative
                    /* h ________      0.5         0
                        |   1    |  |_______|  |       |
                        |        |  |       |  |_______|
                        P        Q  P       Q  P       Q
                    */
        , "vp"    , true   // is following viewport ?
        , "conv", undef
        , "decimal", undef
        , "maxdecimal", undef
        
        , "followslope", true   // 
        /*  
            followslope= true:  R decides the plane and side  
                /               where the dim line lies.   
               /'-._
              /     '-._  /
             P'-._      '/
             |    '-._  /        
             |        'Q
             |         | 
             |         R 

            false:

             |_________|   R further decides direction of guides
             |         |   and dim line. 
             P'-._     |
             |    '-._ |        
             |        'Q
             |         | 
             |         R 
        */
        
        // Subunit settings:
        
        // Set to true to use default ops. false to hide. hash
        // to customize. For label, string to replace dim value.
        
        , "label", true  // false or hash, or text
        
        , "guides", true  // false or hash
        , "guideP", true  // false or hash
        , "guideQ", true  // false or hash

        , "arrows", true  // false or hash
        , "arrowP", true  // false or hash
        , "arrowQ", true  // false or hash

        ];        
    // 3 subunits: guides, arrow, label
    
    df_guides= ["len", 0.8];
    df_arrows= ["headP",false];
    df_label=
        [ "scale",0.4
        , "prefix",""
        , "suffix",""
        , "conv", undef
        , "decimal", undef
        , "maxdecimal", undef
        , "text", undef // set to string replace df value 
        ];
        
    /* subunit "arrow" composes of 2 Arrow modules, one points to
       hP, one hQ. Seperated by gap. The ops is designed as though
       that the entire df_arrow is one Arrow module, 'cos it's 
       unlikely for the two arrows to be formatted differently:
    
        |    arrowP                   arrowQ   |
        |  _='                           '=_   |  
      hP|-'-------------  label ------------'- |hQ
        | '-_          |<- gap->|         _-'  |
        |    '                           '     |
           <-- spacer                             <-- spacer      
        P--------------------------------------Q 
        
        Note that on each of the two Arrows, it's own internal
        arrowP (not the arrowP of this Dim module)  is disabled. 
        */

    _ops = update( df_ops, ops); // df_ops updated w/ user_ops
    function ops(k,df) = hash(_ops,k,df);
   
    //
    //================================================= sub ops 
    //
    
    function _getsubops_( sub_dfs )=
        getsubops( ops, df_ops= df_ops
        , com_keys= ["r", "color", "transp"]
        , sub_dfs= sub_dfs
        );
        
    ops_guideP= _getsubops_( ["guides", df_guides, "guideP", [] ] );
    ops_guideQ= _getsubops_( ["guides", df_guides, "guideQ", [] ] );
    ops_arrowP= _getsubops_( ["arrows", df_arrows, "arrowP", [] ] );
    ops_arrowQ= _getsubops_( ["arrows", df_arrows, "arrowQ", [] ] );
    function ops_guideP(k)= hash( ops_guideP, k); 
    function ops_guideQ(k)= hash( ops_guideQ, k);
    function ops_arrowP(k)= hash( ops_arrowP, k);
    function ops_arrowQ(k)= hash( ops_arrowQ, k);

    _label = ops("label");
    ops_label = isstr(_label)
            ? concat(["text", _label], df_label)   
            : ishash(_label)
            ? concat(_label, df_label )
            : df_label;

    function ops_label(k)= hash( ops_label, k); 

    //
    //================================================= var
    //
    
    r     = ops("r");
    color = ops("color");  
    transp= ops("transp");    
    spacer= ops("spacer");
    gap   = ops("gap");
    h     = ops("h");
    followslope= ops("followslope");
    
    a_pqr = angle(pqr);
    
    //
    //================================================= calc pts
    //
    
    P = pqr[0];
    Q = pqr[1];
    Ro= pqr[2];
    
    /* When followslope = true, R has to be recalc
       such that new a_PQR = 90

            /  
           /'-._
          /     '-._  /
        P'-._       '/
         |    '-._  /        
         |        'Q
         |        /| 
         |       / Ro
                R
            /                    
           /'-._
          /     '-._  /
        Q'-._       '/
        /|    '-._  /        
       / |        '| P
      R  |         | 
         Ro                                          
    */
    R = followslope
         ? ( a_pqr==90
             ? Ro 
             : anglePt( pqr, a=90)
           )  
         : Ro ;  
       
     /*
    P----Q   Need X to make pts along XP line
    |    |
    X----R
    */   
    X= cornerPt( [P,Q,R] ); 
       
    dPQ = norm(P-Q);
    L = (!followslope) && a_pqr!=90         // L= the dim to be reported
        ? norm( P- othoPt(p201(pqr))):dPQ;
    
    //-------------------------------------------- guide 
    /*
            gP2         gQ2  --- 
             |           |    |
          hP +-----------+ hQ | g_len  
             |           |    |
         gP0=gP1         gQ1 ---
    spacer-->            |    |
             Po          |    | gQx_len  
             | '-_       |    |  
             X    '-_   gQ0  ---       
             |       '-_    <--- spacer    
             |          'Qo 
             |           |
             |           Ro     
         
    gPx=0 @ followslope=true, or a_PQR>=90
    gQx=0 @ followslope=true, or a_PQR<=90
    */ 
    h = ops("h");
    //echo("h",h);
    
    g_len = hash( ops_guideP, "len", hash(df_guides,"len") );
    
    // extra len of guides due to slope of PQ
    gPx_len= (!followslope)&& a_pqr<90? shortside( L, dPQ ): 0;
    gQx_len= (!followslope)&& a_pqr>90? shortside( L, dPQ ): 0;
    
    gP0 = onlinePt( [P,X], len= -spacer );
    gP1 = onlinePt( [P,X], len= -spacer - gPx_len );    
    gP2 = onlinePt( [P,X], len= -spacer - gPx_len -  g_len);    
    hP  = onlinePt( [gP1,gP2], ratio= h);
    
    gQ0 = onlinePt( [Q,R], len= -spacer );
    gQ1 = onlinePt( [Q,R], len= -spacer - gQx_len );    
    gQ2 = onlinePt( [Q,R], len= -spacer - gQx_len -  g_len);    
    hQ  = onlinePt( [gQ1,gQ2], ratio= h);

    //echo("ops(''guides'')", ops("guides"));
    //echo("ops_guideP, ops_guideQ",ops_guideP, ops_guideQ);
    if( ops_guideP ) Line( [gP0, gP2], ops_guideP); 
    if( ops_guideQ ) Line( [gQ0, gQ2], ops_guideQ); 
   
    /* 
    Allows arrow heads to be turned off from the obj level. Each arrow 
    (of arrowP, arrowQ) is byteself an Arrow module, in which the dir is
    decided by it's start/end pts, named, P,Q, internally (lets call them 
    iP, iQ here). In Dim, they are defined as pointing from the gap outward:
    
         |    arrowP                   arrowQ  |
         |  _='                           '=_  |  
    hP=iQ|-'------------iP      iP-----------'-| hQ=iQ
         | '-_          |<- gap->|         _-' |
         |    '                           '    |
          <-- spacer                            <-- spacer      
         P-------------------------------------Q 
    
    So on the Dim level, we have a property arrows containing two Arrows,
    and each have its own headP, headQ. 
    
    The default of Dim is to hide the headP of Arrow module (defined in df_arrows).
    
    To turn off the arrow head of arrowP (of Dim) in Dim, we can use: 
    
    [1]  Dim( ...[ "arrowP"          <=== arrowP of Dim
                , ["headQ", false] <=== define the arrowQ of arrowP
                ]
           );      
    
    We make this possible: 
    
    [2]  Dim( ...[ "arrowP"          <=== arrowP of Dim
                , ["head", false]  <=== define the arrowQ of arrowP
                ]
           );    
    
    That is, no need to specify headQ. You can also replace false with 
    a valid hash to set the arrow head. 
    
    Both [1] and [2], or even ["heads",...], give the same result.
    
    */

    //
    //================================================= line
    //
    
    // hP, hQ: where line intersect guides
    // gapP, gapQ: where the arrow line starts. 
    //             determined by ["line",["gap",0]]   
    /*
         gP2                    gQ2
          |                      |
       hP +-----gapP    gapQ-----+ hQ
          |                      |
         gP1                    gQ1    
    
       But, remember that the real hP needs to take
       into account the len of arrow head
    
             _-'| 
       _hP <----+--------- gapP
             '-_| hP 
    
           |<-->| len of arrow head 
    
    */

    gap = ops("gap"); 
    lineL = (L-gap)/2;
    
    begP = onlinePt( [hP,hQ], len=lineL);
    begQ = onlinePt( [hQ,hP], len=lineL);

    if(ops_arrowP) Arrow( [ begP, hP], ops_arrowP );
    if(ops_arrowQ) Arrow( [ begQ, hQ], ops_arrowQ );
         
    //       
    //================================================= label
    //
    
	module transform(followVP=ops("vp"))
	{
        //echo("vp", followVP);
		translate( midPt( [hP,hQ] ) )
	     color( ops("color"), ops("transp") )
		if(followVP){ rotate( $vpr ) children(); }
		else { rotate( rotangles_z2p( Q-P ) )
			  rotate( [ 90, -90, 0] )
			  children();
			}
	}
	conv = ops("conv");
	//echo("conv",conv, "L", L, "numstr(L, uconv=uconv)", numstr(L, conv=conv));
	transform()
	scale(0.04) text(str(ops_label("prefix")
						  //, L, "="
						  , numstr(L
								, conv=ops("conv")
								, d=ops("decimal")
								, maxd=ops("maxdecimal")
								)
						  , ops_label("suffix") )
                    , valign="center"
                    , halign="center");
   
         
}// Dim
    

module Dim_demo_1()
{ 
    pqr=pqr();
    Arrow( p01(pqr));
    //Dim(pqr);
}
Dim_demo_1();


module Dim_demo_1_plane()
{ _mdoc("Dim_demo_1_plane"
,  "Show some ways to measure PQ:    
;; blue : Dim( [P,Q,R] )
;; red  : Dim( [P,Q,V] )
;; green: Dim( [P,Q,U] )
");    
   x=3; y=2; z=2;  
   P = [0,y,z];
   Q = [x,y,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y,0];
   V = [x,0,0];
   W = ORIGIN; 
   pts =  [P,Q,R,S,T,U,V,W];
    
   color(undef,0.5) polyhedron( points =pts, faces=CUBEFACES );
    
   MarkPts( pts, ["r",0.05] );
   LabelPts(pts, "PQRSTUVW");
  

   module Label(pqr, txt, color){
       LabelPt( pqr, txt, ["color",color, "scale", 0.015] );
   }
   
   Dim( [P,Q,R], ["color","green"]);
   Label( [0,y-0.1,z+1.2], "Dim([P,Q,U]) or Dim([P,Q,T])",  "blue"); 

   Dim( [P,Q,V], ["color","red"] );
   Label( [-0.2,y+0.6,z+0.7], "Dim([P,Q,V],[\"color\",\"red\"])",  "red"); 

   Dim( [P,Q,U]);
   Label( [0,y+1,z+0.1],"Dim([P,Q,R],[\"color\",\"green\"])"
            , "green");
}

//Dim_demo_1_plane();

module Dim_demo_2_color()
{_mdoc("Dim_demo_2_color", "");
   x=3; y=2; z=3;  
   P = [0,y,z];
   Q = [x,y,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y,0];
   V = [x,0,0];
   W = ORIGIN; 
   pts =  [P,Q,R,S,T,U,V,W];
    
  color(undef,0.5) polyhedron( points =pts, faces=CUBEFACES );
    
   MarkPts( pts );
   LabelPts(pts, "PQRSTUVW");
  
   Dim( [R,Q,U], ["color","red"]);
   Dim( [R,S,P], ["guides",["color","red"]] );  
   Dim( [P,Q,R], ["arrows",["color","red"]]);
    
   Dim( [P,S,T], ["color","red"
                  ,"arrows", ["color","green"]
                  ,"arrowP", ["color","yellow"]
                  ] );

   Dim( [P,S,Q], ["color","red"
                  ,"guides", ["color","green"]
                  ,"guideP", ["color","yellow"]
                  ] );
   module Label(pqr, txt, color){
       LabelPt( pqr, txt, ["color",color, "scale", 0.015] );
   }
   
   Label( [x,0,z+1.2], "Dim([R,Q,U],[\"color\",\"red\"])", "red");
   Label( [x,-2.5,z],"[R,S,P],[\"guides\",[\"color\",\"red\"]] ","blue");
   Label( [2*x/3, y,z], "[P,Q,R],[\"arrows\",[\"color\",\"red\"]]","blue");
   Label( [0,0,z+1], 
        "...[\"color\",\"red\",\"arrows\", [\"color\",\"green\"]","green");
   Label( [0,0,z+.7], 
        "      ,\"arrowP\", [\"color\",\"yellow\"]]","green");
   Label( [-1,-2, z], "...[\"color\",\"red\",\"guides\", [\"color\",\"green\"],\"guideP\", [\"color\",\"yellow\"]]", "blue");

}    

module Dim_demo_3_followslope()
{ _mdoc("Dim_demo_3_followslope", "");
   x=3; y=2; z=3;  
   P = [0,y,z];
   Q = [x,y/2,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y/2,0];
   V = [x,0,0];
   W = ORIGIN; 
   A = [0, y, 2*z/3];
   B = [x, y/2, 2*z/3];
   C = [x, 0, 2*z/3];
   D = [0, y, z/3];
   E = [x, y/2, z/3];
   F = [x, 0, z/3];
    
   pts =  [P,Q,R,S,T,U,V,W, A,B,C,D,E,F];
    
  color(undef,0.5) polyhedron( points =pts, faces=CUBEFACES );
    
   //MarkPts( pts );
   LabelPts(pts, "PQRSTUVWABCDEF");
  
   //Dim( [P,Q,R]);  
    
    Dim( [P,Q,R], ["followslope", true] );
    Dim( [A,B,C], ["followslope", false, "color", "red"] );
    Dim( [E,D,F], ["followslope", true] );
    Dim( [U,T,W], ["followslope", false] );

   module Label(pqr, txt, color="blue"){
       LabelPt( pqr, txt, ["color",color, "scale", 0.015] );
   }
  
   Label( [0.2, y+0.5,z], "Dim([P,Q,R],[\"followslope\",true])");
   Label( [0.2, y+0.5, 2*z/3], "Dim([A,B,C],[\"followslope\",false])","red");
   
}    

module Dim_demo_3_hide()
{ _mdoc("Dim_demo_3_hide", "");
   x=3; y=2; z=3;  
   P = [0,y,z];
   Q = [x,y/2,z];
   R = [x,0,z]; 
   S = [0,0,z]; 
   T = [0,y,0];
   U = [x,y/2,0];
   V = [x,0,0];
   W = ORIGIN; 
   A = [0, y, 2*z/3];
   B = [x, y/2, 2*z/3];
   C = [x, 0, 2*z/3];
   D = [0, y, z/3];
   E = [x, y/2, z/3];
   F = [x, 0, z/3];
    
   pts =  [P,Q,R,S,T,U,V,W, A,B,C,D,E,F];
    
  color(undef,0.5) polyhedron( points =pts, faces=CUBEFACES );
    
   //MarkPts( pts );
   LabelPts(pts, "PQRSTUVWABCDEF");
  
   //Dim( [P,Q,R]);  

   module Label(pqr, txt, color="blue"){
       LabelPt( pqr, txt, ["color",color, "scale", 0.015] );
   }
    Dim( [P,S,R], ["arrows",false] );
    Label( [-1,0,z], "[P,S,R],[\"arrows\",false]");
      
    Dim( [P,S,T], ["arrowP",false, "color","red"] );
    Label( [0,0,z+1], "[P,S,T],[\"arrowP\",false]", "red");

    //===================
    Dim( [W,V,U], ["guides",false] );
    Label( [0,-3,0], "[W,V,U],[\"guides\",false]");   
   
    Dim( [W,V,P], ["guideP",false, "color","red"] );
    Label([ x/3, -2.5,-.8], "[W,V,P],[\"guideP\",false]","red");
   
    Dim( [W,V,R], ["guideQ",false, "color","green"] );
    Label( [x/2,0,-1.2], "[W,V,R],[\"guideQ\",false]","green");   
    
    //===================
    Dim( [V,U,T], ["color","purple"
                   , "arrowP",["heads",false]
                   , "arrowQ",["heads", ["r", 0.1]]
                   ]
        );
    Label( [ x+0.5, y/2, 0]
            , "[V,U,T],[\"arrowP\",[\"heads\",false]", "purple");
    Label( [ x+0.5, y/2, -0.35]
            , "        ,\"arrowQ\",[\"heads\",[\"r\",0.1]] ]", "purple");
   
//    Dim( [P,Q,R], ["followslope", true, ["guardP",false]] );
//    Dim( [A,B,C], ["followslope", false, "color", "red", ["guardP",false]] );
//    Dim( [E,D,F], ["followslope", true, ["guardQ",false]] );
//    Dim( [U,T,W], ["followslope", false, ["guardQ",false]] );


  
//   Label( [0.2, y+0.5,z], "Dim([P,Q,R],[\"followslope\",true])");
//   Label( [0.2, y+0.5, 2*z/3], "Dim([A,B,C],[\"followslope\",false])","red");
   
}    

//Dim_demo_1_plane();
//Dim_demo_2_color();
//Dim_demo_3_hide();
//Dim_demo_3_followslope();




//========================================================
RodCircle=["RodCircle", "ops", "n/a", "3D, Shape",
"
 Draw rods around a circle based on the given ops. Other than the
;; settings defined in the common ops OPS, this ops has the following
;; defaults:
;;
;; [ ''style'', ''rod'' // rod|block
;; , ''count'',  12
;; , ''normal'', [0,0,1] // the circle normal
;; , ''target'', ORIGIN  // the circle center
;; , ''dir''  , ''out'' // direction(out|in|up|down)
;; , ''radius'', 3
;; , ''r''    ,  0.1    // for style = rod
;; , ''w''    ,  0.4    // for style = block
;; , ''h''    ,  0.5    // for style = block
;; , ''markcolor'',false  // example:[1,''red'',4,''blue'']
;; , ''rodrotate'', 0
;;      // Rotation of individual rods about its own axis. Only works
;;      // for style=block. Note that OPS has a ''rotate'',too, that 
;;      // would be for the rotate of entire RodCircle about circle axis.
;; ]
"];



module RodCircle( ops )
{
	
	//echom("RodCircle");
	ops=updates( OPS,[
		[ "style", "rod"     // rod|block|cone
		, "count",  12  
		, "normal", [0,0,1]
		, "target", ORIGIN 
		, "dir"  , "out"     // out|in|up|down  (outward|inward...)
		, "radius", 3  
		, "r"    ,  0.1      // for style = rod
		, "w"    ,  0.4      // for style = bar
		, "h"    ,  0.5      // for style = bar
		, "markcolor",false  // [1,"red",4,"blue"]
		, "rodrotate", 0     // Rotation of individual rod about its axis. 
						   // Only works
						   //  for style=block. Note that OPS has a 
						   //  "rotate",too, that would be for the
						   //  rotate of entire RodCircle about its axis.
				
		], ops 
		]);	
	function ops(k)= hash(ops,k);

	style = ops("style");
	count = ops("count");
	color = ops("color");
	transp= ops("transp");
	fn    = ops("fn");
	normal= ops("normal");
	target= ops("target");
	dir   = ops("dir");
	rlen  = ops("len");
	r     = ops("r");
	h     = ops("h");
	radius= ops("radius");
	markcolor = ops("markcolor");
	rodrotate= ops("rodrotate");

	isupdown = dir=="up" || dir=="down";

	//echo("ops = ", ops );
	//echo("isupdown? ", isupdown);

	// For style = up|down, rods align with the normal. Because of the order
	// of the points that form the normal (or, the direction of the normal),
	// going upward or downward as desired requires special treatment. What
	// we do here is to, kind of, sort the points like what's been done in 
	// Line(), and chk if the two points has to switch, which will be the 
	// factor to set rod direction. 
	pq = [ target, normal + target ];
	switched= minyPts(minxPts(minzPts(pq)))[0]==pq[1];

	//echo("switched? ", switched );

	// Setup the points for the rods. Each rod starts from a begPt
	// and ends in a endPt. These pts are first created as 2d series
	// using arcPts then converted to 3d. All are on the xy-plane.  
	begPts = addz( arcPts( r=radius, a=360, count=ops("count") ) );
	endPts =  dir=="down"? addz( begPts, switched?-rlen:rlen )
		    : dir=="up"? addz( begPts, switched?rlen:-rlen )
			: addz(
				dir=="out"? arcPts( r=radius+rlen, a=360, count=ops("count") )
			            : arcPts( r=radius-rlen, a=360, count=ops("count") ) // in
			  );  

	//echo( "begPts: ", begPts);
	//echo( "endPts: ", endPts);

	//MarkPts( begPts );
	//MarkPts( endPts );

	translate( ops("target") )
	rotate( rotangles_z2p( ops("normal") ) )
	rotate( [0,0, ops("rotate") ] )
	{

	//=============================================
	// Start of translation/rotation
	//=============================================
	if( style=="rod" ){

		for( i= [0:len(begPts)-1] ){
			p= begPts[i];
			q= endPts[i];
			rodcolor= haskey(markcolor,i)
								?hash(markcolor,i) 
								:color;
					
//			  assign( p= begPts[i]
//					,q= endPts[i]
//					, rodcolor= haskey(markcolor,i)
//								?hash(markcolor,i) 
//								:color
//					)
			   //MarkPts( [p,q] );
			   Line( [ p,q ]	
			     , update( ops, ["color", rodcolor, "transp",1] )
					
                   );	
			}
	}
	if( style == "block") {
			for( i= [0:len(begPts)-1] ){
			  assign ( p= begPts[i]
					, q= endPts[i]
					, L= len(begPts)
					, FL=floor( len(begPts) /4) 
						// FL is needed to adjust block orientation 
						// when rotation is set for block
					, rodcolor= haskey(markcolor,i)
								?hash(markcolor,i)
								:color
					 //Extends to cover line-curve gap
					, contactsealer = dir=="out"?
									(radius-shortside(radius, ops("w")/2 )
									+ GAPFILLER)
									:dir=="in"?
									 -radius+shortside(radius, ops("w")/2 )
									+GAPFILLER
									 :0

					){
			//echo("rodcolor = ", rodcolor );
             //echo("outrodmarkcolor = ", outrodmarkcolor ); 
			//echo_( "{_}: pq = {_}", [i, pq] );  
			//echo_("L={_}, FL={_}",[L,FL]); 
			//echo("contactsealer = ", contactsealer );
			//echo_( "isupdown={_}, rodrotate={_}, rotate={_} ", [isupdown, rodrotate, rodrotate+(isupdown?30:0)] );
			   Line( [ p,q ]
				  // , onlinePt( [ pq, ORIGIN ]
				  //             , len= -rlen )
				//	] 
			     ,  update( ops, ["style","block"
							   , "shift", isupdown?"mid2":"center"
								 ,"color",rodcolor
								 ,"width",ops("w")
								  ,"depth",h 
								 // , "markpts", true	
								  ,"extension0", contactsealer												
								, "rotate",  isupdown  // rotate for up/down
											?(i*360/count+rodrotate)
											: (rodrotate*(
												(i>L-FL)||((i>=0)&&(i<=FL))?-1:1 
											  ))+ (i==0?90:0)
											// The last part, rotrotate by 90,
											// is needed for i=0 in block style 

								])
                   );	
			}}
	}


	

	}//=============================================
	//  END of translation/rotation
	//=============================================

}
//echo( [3,4]+repeat([2],2) );
//echo( inc([3,4],2) );

module RodCircle_demo_1_dir()
{
	RodCircle(["markcolor",[0,"red"], "color", "red"]);
	RodCircle(["markcolor",[0,"red"], "dir", "in", "color","lightgreen"]);
	RodCircle(["markcolor",[0,"red"], "dir", "up", "color","blue"]);
	RodCircle(["markcolor",[0,"red"], "dir", "down", "color","purple"]);
}

module RodCircle_demo_2a_normal_out()
{
	echom("RodCircle_demo_2a_normal_out");
	pq = randPts(2);
	radius = rands( 1,20,1)[0]/10;

	RodCircle( [ "normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts(pq);
	Line(pq, ["r", radius
			,"extension0",0.5, "color", randc(), "transp",0.1] );

}

module RodCircle_demo_2b_normal_in()
{
	echom("RodCircle_demo_2b_normal_in");
	pq = randPts(2);
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ "dir","in"
		   ,"normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts(pq);
	Line(pq, ["r", radius 
			,"extension0",0.5, "color", randc(), "transp",0.1] );

}

module RodCircle_demo_2c_normal_up()
{
	echom("RodCircle_demo_2c_normal_up");
	pq = randPts(2);
	radius = rands( 6,20,1)[0]/10;

	RodCircle( [ "dir","up"
		   ,"normal", pq[1]-pq[0]
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts(pq);
	Line0(pq, ["r", radius 
			//,"extension0",0.5
			, "color", randc(), "transp",0.2] );

}

module RodCircle_demo_2d_normal_down()
{
	echom("RodCircle_demo_2d_normal_down");
	pq = randPts(2);
	//echo("pq = ", pq); //pq = minzPt(pq_)==pq_[0]?pq_:reverse(pq_);
	//echo("normal = ", normal );
	radius = rands( 6,20,1)[0]/10;

	RodCircle( [ "dir","down"
		   ,"normal",pq[1]-pq[0]// sign(pq[1].z-pq[0].z)*( pq[1]-pq[0] )
		   , "target", pq[0] 
		   , "color", randc()
		   , "radius", radius
		   ] );
	MarkPts( pq );
	Line0(pq, ["r", radius 
			//,"extension0",0.5
			, "color", randc(), "transp",0.2] );	
}

module RodCircle_demo_3a_block_out()
{
	echom("RodCircle_demo_3a_block_out");
	pq = randPts(2);
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.8
			,"markcolor",[0,"red"]
		     , "markpts", true
			] );
	MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0
			, "color", randc()
			, "transp",0.2
			, "markpts", true
			] );


}


module RodCircle_demo_3b_block_in()
{
	echom("RodCircle_demo_3b_block_in");
	pq = randPts(2);
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "in"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.4
			,"h", 1
			,"markcolor",[0,"red"]
		   ] );
	MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0, "color", randc(), "transp",0.2] );

}


module RodCircle_demo_3c_block_up()
{
	echom("RodCircle_demo_3c_block_up");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "up"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.8
			,"markcolor",[0,"red"]
			,"markpts", true
			//,"rotate", 90
		   ] );
	MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0, "color", randc(), "transp",0.2] );
}


module RodCircle_demo_3c2_block_up_rotate()
{
	echom("RodCircle_demo_3c2_block_up_rotate");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "up"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			//,"w", 0.8
			,"markcolor",[0,"red"]
			//,"markpts", true
			,"rodrotate", 30
			,"w", 2
			,"h",0.1
			,"len",2
			,"transp",0.5
			
		   ] );
	//MarkPts( pq );
	Line(pq, ["r", radius
			,"extension0",0, "color", randc(), "transp",0.2] );
}

module RodCircle_demo_3d_block_down()
{
	echom("RodCircle_demo_3d_block_down");
	pq = randPts(2);
	//pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "down"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  randc()
		   	, "radius", radius
			,"w", 0.8
			,"markcolor",[0,"red"]
			,"markpts", true
		   ] );
	//MarkPts( pq );
	//Line(pq, ["r", radius
	//		,"extension0",2, "color", randc(), "transp",0.2] );
	Line(pq, ["r", radius,
			"extension0",0
			//, "color", randc()
			, "transp",0.2] );

}


module RodCircle_demo_3d2_block_down_rotate()
{
	echom("RodCircle_demo_3d2_block_down_rotate");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = rands( 15,20,1)[0]/10;

	RodCircle([ 
			  "style", "block"
			, "dir", "down"
			, "count", 24
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  "blue" //randc()
		   	, "radius", 1 //radius
			, "w", 1.2
			, "h",0.1
			, "len",2
			, "transp",0.5
			, "markcolor",[0,"red"]
			//,"markpts", true
			, "rodrotate", 30
		   ] );
	//MarkPts( pq );
	Line(pq, ["r", radius, 
			"extension0",0
			//, "color", randc()
			, "transp",0.6] );

}


module RodCircle_demo_4a_block_out_rotate()
{
	echom("RodCircle_demo_4a_block_out_rotate");
	pq = randPts(2);
	pq = [ORIGIN, [0,0,3]];  
	radius = 2; //rands( 15,20,1)[0]/10;
/*
L	L/4 
2	.5	-+
3	.75	-++
4	1	-++-
5		--++-
6		--+++-
7		--++++-
8	2	--++++--
9   		---++++--
10  		---+++++--
11  		---++++++--
12  	3	---++++++---
13  		----++++++---
14  		----+++++++---
15  		----++++++++---
16  	4	----++++++++----

-- The last - part: i>=(L-floor(L/4)) 
-- The first - part: i>=1, <floor(L/4) 

*/
	RodCircle( [ 
			  "style", "block"
			, "dir", "out"
			, "count", 12
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  "blue" // randc()
		   	, "radius", radius
			,"w", 2
			,"len",2
			,"h", 0.2
			,"transp", 0.8
			,"markcolor",[0,"red", 9,"green"]
			,"markpts", true
			,"rodrotate", 45
		   ] );
	MarkPts( pq );
	Line(pq, [ "r", radius, 
			"extension0",2, "color", randc(), "transp",0.2] );


}

module RodCircle_demo_4b_block_in_rotate()
{
	echom("RodCircle_demo_4b_block_in_rotate");
	pq = randPts(2);
	//pq = [ORIGIN, [0,0,3]];  
	radius = 2; //rands( 15,20,1)[0]/10;

	RodCircle( [ 
			  "style", "block"
			, "dir", "in"
			, "count", 8
		   	, "normal", pq[1]-pq[0]
		   	, "target", pq[0] 
		   	, "color",  "blue" // randc()
		   	, "radius", radius
			,"w", 1
			,"len",1
			,"h", 0.2
			,"markcolor",[0,"red"]
			,"markpts", true
			,"rodrotate", 45
		   ] );
	MarkPts( pq );
	Line(pq, [ "r", radius, 
			"extension0",2, "color", randc(), "transp",0.2] );

}

module RodCircle_demo()
{
	RodCircle_demo_1_dir();
	//RodCircle_demo_2a_normal_out();
	//RodCircle_demo_2b_normal_in();
	//RodCircle_demo_2c_normal_up();
	//RodCircle_demo_2d_normal_down();
	//RodCircle_demo_3a_block_out();
	//RodCircle_demo_3b_block_in();
	//RodCircle_demo_3c_block_up();
	//RodCircle_demo_3c2_block_up_rotate();
	//RodCircle_demo_3d_block_down();
	//RodCircle_demo_3d2_block_down_rotate();

	//RodCircle_demo_4a_block_out_rotate();
	//RodCircle_demo_4b_block_in_rotate();
}

module RodCircle_test( ops ){ doctest( RodCircle, ops=ops);}

//RodCircle_demo();
//doc(RodCircle);





//=======================================================================
//=======================================================================
//=======================================================================





//getWrapPts_demo();


module alignface_try()
{
	p0 = [2,2,1];
	pqr1 = [ p0, [4,1,2], [3,2,-1]];
	pqr2 = [ p0, [3,0,-1], [1,2,0]];

	
	Chain(pqr1, r=0.01, closed=true);
	color("blue")
	Chain(pqr2, r=0.01, closed=true);

	echo( "planecoefs pqr1: ", planecoefs(pqr1));
	echo( "planecoefs pqr2: ", planecoefs(pqr2));

	pc1 = planecoefs([pqr1[0]-p0,pqr1[1]-p0,pqr1[2]-p0]);
	pc2 = planecoefs([pqr2[0]-p0,pqr2[1]-p0,pqr2[2]-p0]);
	echo( "planecoefs pqr1-p0: ", pc1);
	echo( "planecoefs pqr2-p0: ", pc2);

	translate(-p0)
	//rotate( pc1)
	{color("khaki")
	rotate(slice(pc1,0))//,-1))
	Chain(pqr1, r=0.02, closed=true);
	color("lightblue")
	Chain(pqr2, r=0.02, closed=true);
 	}

	polyhedron( points = pqr1, faces=[[0,1,2]] );
	polyhedron( points = pqr2, faces=[[0,1,2]] );
	PtGrid(p0);
}
//alignface_try();


module formatEcho(strarr,ops)
{
	block_definition=[
	 "[","]"
	,"(",")"
	,"''","''"
	];
	
	exclusive_blocks= ["''"];
	
	/*function fmt( line ) = 
	( 
	  len(line)==0?""
	  : (line[0]=="'" && line[1]=="'") ?
		str( 
			
			haskey( block_definition, line[0] )?
		
		
	);
	ops=update([
	*/
	


} 

//$fn=100;
//Chain( arcPts(a=120, r=3, count=20 ),r=0.2);

//getWrapPts_demo();


trueor2=[["trueor","x,dval","any|dval", "Hash"]
	 ,["Means ''trueorefault''. Given x,dval, if x is evaluated "
	  ,"to be true, return x. Else dval. Same as x?x:dval but "
	  ,"x only appears once."
	  ]  
	 ];


module help(fname)
{
	echo_( " | {_} ({_}) |=> {_} (group: {_} )", fname[0] );	
	
	echoblock(fname[1]);
}
//help(trueor2);

//========================================================
get2=[
	 ["get", "o,i,[j]", "item", "Index,Inspect,Array,String" ]
	,[ "Get the i-th item of o (a str|arr). The optional 2nd "
	 , "index, j, is for the j-th item of the returned array"
	 , "in cases of a matrix(multidimentional array). "
	 , "Both i,j could be negative."
	 ]
	];

get2_tests=[ 
		  "## array ##"
		 , [ [[20,30,40,50], 1], get( [20,30,40,50], 1 ), 301 ]
    		 , [ [[20,30,40,50], -1], get([20,30,40,50], -1), 50 ]
    		 , [ [[20,30,40,50], -2], get([20,30,40,50], -2), 40 ]
    		 , [ [[20,30,40,50], true], get([20,30,40,50], true), undef ]
    		 , [ [[], 1], get([], 1), undef ]
		 , [ [[[2,3],[4,5],[6,7]], -2], get([[2,3],[4,5],[6,7]], -2), [4,5] ]
    		 ,"## multiarray ##"
		 , [ [[[2,3],[4,5],[6,7]], -2, 0], get([[2,3],[4,5],[6,7]], -2,0), 4 ]
    		 , [ [[[2,3],[4,5],[6,7]], -2, -1], get([[2,3],[4,5],[6,7]], -2,-1), 5 ]
    		 , "## string ##"
    		 , [ ["abcdef", -2], get("abcdef", -2), "e" ]
    		 , [ ["abcdef", false], get("abcdef", false), undef ]
    		 , [ ["", 1], get("", 1), undef ]
    		 ];


/* 
---------------------------------------------

multmatrix( m = 
[
	 [ X, tzx ,tyx, x ]
	,[ tzy, Y, txy, y ]
	,[ txz, tyz, Z, z ]
	,[0, 0, 0, 1 ]
])

tzx: tilt with surface on xz plane unchanged and with the 
	 z-axis edge attached (unchanged ) and tilts along x direction.

---------------------------------------------
	A=[a,b,c]
	B=[x,y,z]

dot prod: 	A * B = ax+by+cz

cross prod:

	|A x B| = |A||B|cos(theta)

	A x B  = [bz-cy, cx-az, ay-bx ] 

	which is the determinant of : 

		 [[i,j,k]
		  [a,b,c]
		  [x,y,z]]

---------------------------------------------
3 vectors, A,B,C, forms a 3-D object.

The vol is : | A * ( B x C ) |

If it is 0, means A,B,C on the same plane 
	
	= abs( A * cross(B,C) ) == 0

*/ 
 
/*
Rotation Matrix
http://www.cs.brandeis.edu/~cs155/Lecture_07_6.pdf
*/

/*  Line formula in 3d space

  vector form of the equation of a line:

    P= Q +t*[a,b,c] 

    where [a,b,c] = Q-P

  parametric form of the equation of a line:

    P.x = Q.x + at
    P.y = Q.y + bt
    P.z =	 Q.z + ct

  symmetric equations of the line:

	( Q.x-P.x )/a = ( Q.y-P.y )/b = ( Q.z-P.z )/c 


    P.z =	 Q.z + ct

       
http://mathforum.org/library/drmath/view/65721.html

Good lesson:
http://tutorial.math.lamar.edu/Classes/CalcIII/EqnsOfLines.aspx

*/

/*
Formula in 3D space:
http://math.stackexchange.com/questions/73237/parametric-equation-of-a-circle-in-3d-space

A circle of radius r on the pqr plane, center at q:

x(θ)= Q.x+ r*cos(θ)*(P/norm(P)).x+rsin(θ)*(R/norm(R)).x
y(θ)= Q.y+ r*cos(θ)*(P/norm(P)).y+rsin(θ)*(R/norm(R)).y
z(θ)= Q.z+ r*cos(θ)*(P/norm(P)).z+rsin(θ)*(R/norm(R)).z

*/

/*
Kit Wallace 3D polyhedron index
http://kitwallace.co.uk/3d/solid-index.xq?3D=yes

http://kitwallace.tumblr.com/tagged/polyhedra

https://github.com/openscad/openscad/wiki/OEP1%3A-Generalized-extrusion-module#discretized-space-curves

*/

/*
transformation:

Understanding the Transformation Matrix in Flash 8
http://www.senocular.com/flash/tutorials/transformmatrix/

Transformation matrix function library
http://forum.openscad.org/Transformation-matrix-function-library-td5154.html

Jar-O-Math! 
http://forum.openscad.org/Jar-O-Math-td8539.html


$fn=30;
//echo("</span>");


//===========================================
// console html tools /// start
//===========================================

_SP  = "&nbsp;";
_SP2 = "　";  // a 2-byte space 
_BR  = "<br/>"; 

function _tag(tagname, t="", s="", a="")=
str( "<", tagname, a?" ":"", a
		, s==""?"":str(" style='", s, "'")
		, ">", t, "</", tagname, ">");

function _div (t, s="", a="")=_tag("div", t, s, a);
function _span(t, s="", a="")=_tag("span", t, s, a);
function _table(t, s="", a="")=_tag("table", t, s, a);
function _pre(t, s="font-family:ubuntu mono;margin-top:5px;margin-bottom:0px", a="")=_tag("pre", t, s, a);
function _code (t, s="", a="")=_tag("code", t, s, a);
function _tr   (t, s="", a="")=_tag("tr", t, s, a);
function _td   (t, s="", a="")=_tag("td", t, s, a);
function _b   (t, s="", a="")=_tag("b", t, s, a);
function _i   (t, s="", a="")=_tag("i", t, s, a);
function _u   (t, s="", a="")=_tag("u", t, s, a);
function _h1  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h1", t, s, a); 
function _h2  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h2", t, s, a); 
function _h3  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h3", t, s, a); 
function _h4  (t, s="margin-top:0px;margin-bottom:0px", a="")= _tag("h4", t, s, a); 


function _color(t,c) = _span(t, s=str("color:",c));
function _red  (t) = _span(t, s="color:red");
function _green(t) = _span(t, s="color:green");
function _blue (t) = _span(t, s="color:blue");
function _gray (t) = _span(t, s="color:gray");

function _tab(t,spaces=4)= replace(t, repeat("&nbsp;",spaces)); // replace tab with spaces
function _cmt(s)= _span(s, "color:darkcyan");   // for comment

// Format the function/module argument
function _arg(s)= _span( s?str("<i>"
					,replace(replace(s,"''","&quot;"),"\"","&quot;")//"“"),"\"","“")
					,"</i>"):""
					, "color:#444444;font-family:ubuntu mono");


module _Div ( t, s="", a="" ){ echo( _div (t, s, a)); }
module _Span( t, s="", a="" ){ echo( _span(t, s, a)); }
module _B   ( t, s="", a="" ){ echo( _b   (t, s, a)); }


//===================================
// Builtin doc
//===================================

cube = [["cube","size,center","n/a","3D, Shape"],
"
 Creates a cube at the origin of the coordinate system. When center\\
 is true, the cube will be centered on the origin, otherwise it is \\
 created in the first octant. The argument names are optional if the\\
 arguments are given in the same order as specified in the parameters.\\
 \\
 cube(size = 1, center = false);\\
 cube(size = [1,2,3], center = true);\\
"];

norm = [["norm","v","number","Math, Geometry"],
"
 Givena vector (v), returns the euclidean norm of v. Note this\\
 returns is the actual numeric length while len returns the number\\
 of elements in the vector or array.\\
"];

//doc(norm);
//doc(cube);
//doc(_fmt);

/*

:   &  ^.  '-_   -._  -._    -.._
 :   &   ^.   '-_   '-._ ''--._  ''--.._
  :   &    ^.    '-_    '-._   ''--._   ''--.._
   :   &     ^.     '-_     '-._     ''--._    ''--.._
    :   &      ^.      '-_      '-._       ''--._     ''--.._ 
    1   1       2        3         4            6             7  
           
            |
     '-_    |
        '-_ | 
   --------'+----------
            | '-_
            |    '-_
            |       '   


           |
      ^.   | 
        ^. |      
    -------+--------
           |^.     
           |  ^.      
           |    ^.
         


      P3 
	   &'-_
	   |&  '-._     
	   | &     '-._
	   |  &        '-_ 
	   |   &         _'P2
	   |  b &      -' /
	   |     &  _-'  /	
	   |      X'    /
	   |  a -' &   /
	   |  -'    & /
	   |-'_______&
     P1            P4


	//	      5-------4
	//	     /|      /|
	//	    / |  Q'-/----------C
	//	   /  |  /\/'-|       /
	//	  /   6-/-/---7'-.   /
	//   /   / / /  \/ '. R2/
	//	1-------0   /\  	'./
	//	|  / /  |  /  R1  /'.
	//	| / P.--|-/------D   A    
	//	|/    '.|/          /
	//  2-------3          /
	//           '.       /
	//             '.    /
	//               '. /
    //                 B

;;       N (z)
;;       |
;;       |________
;;      /|'-_     |
;;     / |   '-_  |
;;    |  |      : |
;;    |  Q------|---- M (y)
;;    | / '-._/ |
;;    |/_____/'.|
;;    /          '-R
;;   /
;;  P (x)
;;
;; In the graph below, P,Q,R and 0,4 are co-planar. 
;;
;;       _7-_
;;    _-' |  '-_
;; 6 :_   |Q.   '-4
;;   | '-_|  '_-'| 
;;   |    '-5' '-|
;;   |    | |    '-_
;;   |   _-_|    |  'R
;;   |_-' 3 |'-_ | 
;;  2-_    P|   '-0
;;     '-_  | _-'
;;        '-.'
;;          1




;;       _6-_
;;    _-' |  '-_
;; 5 '_   |     '-7
;;   | '-_|   _-'| 
;;   |    '-4'   |
;;   |    | |    |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0

	//                          _-5
	//                       _-'.' '.
	//                    _-' .'     '.
	//                 _-'  .'         '.
	//              _-'   _6    _-Q----_-4--------------A 
	//            1'   _-'  '_-'   '_-'.'            _-'
	//          .' '.-'   _-'  '._-' .'           _-'
	//        .' _-' '. -'    _-'. .' '.       _-'
	//      .'_-'   _-''.  _-'   _-7    R1  _-'
	//	 2-'    P-----0-'----------------B     
	//      '.         .'  _-'
	//        '.     .' _-'
	//          '. .'_-'
	//            3-'
	//                     


;;                     D   S
;;                     :  /|
;;                     : / |
;;                     :/  |   
;;                     /   |  .'
;;                    /:  .|'
;;       B        :  / :.' |
;;  P    |        : / .C---:
;;  |'.  |   :    :/.'_.-''
;;  |  '.| --:----R--'------
;;  |    |.  :  .'
;;  |    | '.:.'
;; -|----|-_.Q -------    
;;  |.'_.|'.'
;;  '----A'
;;     .'

             R      
            / '-_
           /     '-_
          /         '-_
         /             '-_
       P -----------------'Q

             ___
         .:'' | ''-
        /     |  / 
       :      | / 
       +------+'-----+
       :      |      :
        :     |     /
         ':.._|_..:'



  ./{           }       
    
   {  }^2
          
   Z{    }

	2*x^2 + 5*x + 6 

*/


/*
file structure:

	scadx_core.scad    --- core lib 
	scadx_obj.scad     --- obj lib
	scadx_api.scad     --- doc/test/api 
	scadx_doctest.scad --- doctest lib
	scadx.scad         --- calling the above 
*/


  