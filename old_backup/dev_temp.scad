//include <scadx_core_dev.scad>
include <scadx_obj_basic_dev.scad>
  
 
module test_tube(){
    
    pqr=pqr();
    h = 2;
    pqr_t = [ pqr[0], onlinePt(p01(pqr), len=1+h), pqr[2] ];
    pqr_b = [ pqr[0], onlinePt(p01(pqr), len=1), pqr[2] ];
    nside_o=4;
    echo(nside_o=nside_o);

    nside_i=4; 
    ro = 3;
    ri = 1.5;

    // top_out  
    to= [for(i=range(nside_o)) anglePt( pqr_t, a=90, a2= i*360/nside_o, len=ro)];
    bo= [for(i=range(nside_o)) anglePt( pqr_b, a=90, a2= i*360/nside_o, len=ro)];

    ti= [for(i=range(nside_i)) anglePt( pqr_t, a=90, a2= i*360/nside_o, len=ri)];
    bi= [for(i=range(nside_i)) anglePt( pqr_b, a=90, a2= i*360/nside_o, len=ri)];
        
    xpts = concat( [bo],[to],[bi],[ti] );
    //for(xp= xpts) MarkPts( xp, "ch=[closed,true]");

    pts = joinarr( xpts );
    //MarkPts( pts,"l=[color,black]");
    echo(nside_o=nside_o);
    faces = faces("tube", nside=4);
    facesxxx=[ 
      [0, 1, 5, 4]
    , [1, 2, 6, 5]
    , [2, 3, 7, 6]
    , [3, 0, 4, 7]

    , [12, 13, 9, 8]
    , [13, 14, 10, 9]
    , [14, 15, 11, 10]
    , [15, 12, 8, 11]
    , [0,3,2,1,8,11,10,9]
    , [4,5,6,7, 12,13,14,15]
    //, [4,5,6,7, 15,14,13,12 ] //12,13,14,15]
    ,
    //, [0, 3, 11, 8]
    //, [1, 0, 8, 9]
    //, [2, 1, 9, 10]
    //, [3, 2, 10, 11]
    //, [4, 5, 13, 12]
    //, [5, 6, 14, 13]
    //, [6, 7, 15, 14]
    //, [7, 4, 12, 15]
    ];

    //echo(faces= arrprn(faces,2));
    //color(undef,1)
    //polyhedron(points= pts, faces=faces);
    //
}


module test_tubeEnd(){
    
    pqr = pqr();

    sq= [ for(i=range(4)) anglePt( pqr, a=90, a2 = i*360/4 ,len=4) ];
    six= [ for(i=range(6)) anglePt( pqr, a=90, a2 = i*360/6, len=2 ) ];
    echo(sq=sq);
    pts = concat( sq,six);
    echo(sq=sq);

    MarkPts(sq, "ch=[closed,true];l" );
    MarkPts(six, ["ch=[closed,true]]","label","456789"] );

    midpts = [ for(i=range(4)) 
               midPt( get(sq, [i, sq[i+1]?i+1:0]))
    //              , i>len(sq)-1?0:i+1]) )
             ];
    echo(midpts= midpts);

    MarkPts( get(midpts,[0,2]), "ch;ball=false");
    MarkPts( get(midpts,[1,3]), "ch;ball=0");

    a=[1,2,3];
    echo(range(a,cycle=[-1,1]));

    tube_faces = faces("tubeEnd");

    ptss= joinarr([ 
     for(i = range(tube_faces)) 
        let(faces= tube_faces[i]
           ,clr = COLORS[i]
           )
          get(pts, faces)
    ]);
     
    echo(ptss = ptss[0]);
     
    for( i=range(ptss))MarkPts( ptss[i]
        , ["plane",["color",COLORS[i]],"transp",0.5]);
}

module GatewayArch(){
    /* 
        St.Luis Gateway Arch
    
    * Following a hyperbolic cos: cosh(a) = ( exp(a)+exp(-a) ) / 2   
      2015.6.29
    */
    
    echom("GatewayArch");
    
    n=37;  // this has to be odd number
    
    // Construct the hyperbolic cos data
    xs0= [for(i=range(n/2)) i ]; 
    xs=  concat( reverse(xs0)*-1 , slice(xs0,1) )/8;
    ys= [for(x=xs) (exp(x)+exp(-x))/2];
    echo(xs=xs);
    echo(ys=ys);   
    
    // Make the bone chain
    maxy = max(ys);    
    pts = [for(i=range(n))
            [xs[i],0, maxy- ys[i]]
          ];
    echo(pts= arrprn(pts,2));
    //MarkPts( pts, "ch;r=0.05;samesize;cl=red;sametransp"); 
    
    // Make the list of r (radius) for use along the 
    // bone chain.
    rs = [ for(r=xs/20) ((exp(r)+exp(-r))-2)*25 ];
    echo(rs= arrprn(rs,1));
           
    // Make chain points. 
    chret = chainPts( pts, nside=3, r=0.16, rs=rs );
    chpts = hash( chret, "xsecs");
//    echo(len_chpts= len(chpts)
//        , chpts= arrprn( chpts, 2));
    
    // Make the object
    faces = faces("chain", nside=3, nseg= n-1);
    color("darkgray",1)
    polyhedron( points= joinarr(chpts)
              , faces= faces);
}    
GatewayArch();
