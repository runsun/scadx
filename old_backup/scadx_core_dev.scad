include <scadx_init.scad>
include <scadx_funclet.scad>
include <scadx_array.scad>
include <scadx_range.scad>
include <scadx_hash.scad>
include <scadx_inspect.scad>
include <scadx_geometry.scad>
include <scadx_string.scad>
include <scadx_eval.scad>
include <scadx_match.scad>
include <scadx_error.scad>
include <scadx_args.scad>


//========================================================
function asc(c, n = 0) = 
(     // from nophead
      c == chr(n) ? n : asc(c, n + 1)
);

//========================================================
function det(pq)=
(
	dot( pq[0], pq[0] ) * dot( pq[1],pq[1]) - pow( dot(pq[0],pq[1]),2)
);

//========================================================
function dot(P,Q)= 
    let( pq= Q==undef? P:[P,Q] )
(   pq[0]*pq[1]
);

//========================================================
function fac(n=5)= n*(n>2?fac(n-1):1);

//========================================================
function ftin2in(ftin)=
(
	isnum(ftin)
	? ftin*12
	: isarr(ftin)
	  ? ftin[0]*12+ftin[1]
	  : endwith(ftin,"'")
		? num( replace(ftin,"'",""))*12

	    : index(ftin,"'")>0
		   ? num( split(ftin,"'")[0] )*12 
			+ num( replace(split(ftin,"'")[1],"\"",""))
		   : num( replace(ftin,"\"",""))
);

//========================================================
function getdeci(n)= 
(
   index(str(n),".")<0?0:len(split(str(n),".")[1])
);
    


//========================================================
function dor(x,df)= x==undef?df:x; 


//========================================================
function mod(a,b) = sign(a)*(abs(a) - floor(abs(a)/abs(b))*abs(b) );

//========================================================
function or(x,dval)=x?x:dval;

//========================================================
function permute(arr, _rtn=[], _i=-2)=
(   //This one works , but it's not tail recursion (take 41 sec for len(arr)=5):            
    len(arr)==2? [ arr, [arr[1],arr[0]]]
    : joinarr( 
       [ for( i = [0:len(arr)-1] )
            let( pleft = pick( arr, i )
               , p = pleft[0]
               , ar= pleft[1]
               , ar2= permute( ar )
               )
            [ for(x = ar2) concat([p],x) ]
       ] 
     )
);

// Not quite get there yet:            
//function permute(arr,_i=0)=
//(
//   _i>len(arr)-1? arr
//   : [ for(j=[_i:len(arr)-1])
//       let(
//            rtn = switch( arr,j)
//          )
//       permute( rtn, _i=_i+1)
//    ] 
//);            

//========================================================
function pick(o,i) = 
(
  	concat( [get(o,i)]
		  , isarr(o)
			? [ del(o,i, byindex=true) ] 
			: [[ slice(o,0,i), slice(o,i+1) ]]
		  )
  
);

    
//========================================================
function prod(arr)=
   arr? arr[0]* prod(slice(arr,1)) :1;

//========================================================
function rand(m=1, n=undef, seed=undef)=
(
	isarr( m )||isstr(m) ? m[ floor(rand(len(m),seed=seed)) ]
	: (isnum(n)
		? (seed==undef? rands( min(m,n), max(m,n), 1)
                     : rands( min(m,n), max(m,n), 1, seed) )
		: (seed==undef? rands( 0, m, 1)
                     : rands( 0, m, 1, seed) )
	  )[0]
);

//========================================================
function randc( r=[0,1],g=[0,1],b=[0,1] )=
(
    [ rands(r[0],r[1],1)[0]
	, rands(g[0],g[1],1)[0]
	, rands(b[0],b[1],1)[0] ]	
);

////========================================================
//function range(i,j,k, cycle, returnitems=false)=
//let ( 
//	 obj = isarr(i)||isstr(i)? i :undef
//	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
//	,beg= inum==1 
//            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
//    ,end= inum==1 
//		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
//		   : inum==2?j:k
//		   
//	,cycle=cycle==true?1:cycle
//
//	,intv = sign(end-beg)        // when inum=3, the middle one is
//			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	
//
//	,residual= or( mod(abs(end-beg), abs(intv)), intv)
//
//	,extraidx= abs(intv)>abs(beg-end) // End points to be skipped, 
//	           ? 1                    // depending on the intv. If
//	           : residual             // intv too large, set to 1
//	,objrange= obj
//			   ? range ( len(obj)
//						, cycle=cycle ) 
//			   : []
//	, rtn=										 
//
//        ( intv==0 || beg==end || (beg==0&&end==undef) 
//        || obj==[] || obj=="" )
//        ? []
//        : obj  // Allowing range(arr) or range(str)
//          ? objrange
//
//          // Non obj : =================
//
//          : cycle>0
//          ? concat( 
//                [ for(i=[beg:intv:end-extraidx]) i ]
//                ,[ for(i=[beg:intv:beg+(cycle-1)*intv]) i]
//                )
//          : cycle<0
//            ?  concat( 
//                [ for(i=[end+cycle*intv:intv:end-intv]) i]
//                ,[ for(i=[beg:intv:end-extraidx]) i ]
//                )
//          : isarr(cycle)
//            ? concat( 
//                [ for(i=[end+cycle[0]*intv:intv:end-intv]) i]
//                ,[ for(i=[beg:intv:end-extraidx]) i ]
//                ,[ for(i=[beg:intv:beg+(cycle[1]-1)*intv]) i]
//                )
//          : [ for(i=[beg:intv:end-extraidx]) i	]
//
//)(
//    returnitems? sel(obj,rtn): rtn
//);

////========================================================
//function ranges(i,j,k, cover=1, cycle=true, returnitems=false, _debug=[])=
//let ( 
//     cover= cover>0?[0,cover]:cover<0?[-cover,0]:cover // allow +n, -m, or [m,n]
//	,obj = isarr(i)||isstr(i)? i :undef
//	,inum= countNum([i,j,k])          // how many numbers in [i,j,k]
//	,beg= inum==1 
//            ? ( i!=0?0:i ): i  // if only 1 number, i is the end point
//    ,end= inum==1 
//		   ? ( i!=0?i:0 )     // when inum==3, k is the end point
//		   : inum==2?j:k
//		   
//	,intv = sign(end-beg)        // when inum=3, the middle one is
//			* abs(inum>2 ? j:1)	// the inteval. Otherwise intv= 1	
//
//    ,sign = sign(end-beg)
//    ,baserange= obj?range(obj):range(beg,intv,end) 
//    ,left = cover[0]
//    ,right= len(baserange)>1?cover[1]:0    
//    
//    ,newrange = concat( 
//                 cycle?range( end-cover[0]*intv, sign*intv, end ):[]
//                ,baserange
//                ,cycle?range( beg, sign*intv, beg+intv*cover[1] ):[]
//                )
////	,_debug= chkbugs( _debug,
////             ["cover",cover
////             ,"left",left
////             ,"right",right
////             , "baserange", baserange
////             , "newrange", newrange
////             ,"beg",beg
////             ,"end",end
////             ,"intv",intv
////             ]
////             )
//    ,rtn=        
//	( intv==0 || beg==end || (beg==0&&end==undef) || obj==[] || obj=="" )
//	? []
//	: obj  // Allowing range(arr) or range(str)
//	  ? ranges ( len(obj)
//				, cover=cover
//				, cycle=cycle
//				)
//
//	  // Non obj : =================
//
//	  : cover
//        //? _debug
//	  ?//[ _debug,   // uncomment to debug
//           [ for( i=range(left,1, left+len(baserange)) )
//               //range( i-left, i+right+1) //
//               let( islast = i==left+len(baserange)-1
//                  , cycledleft = (i==left&&!cycle?0:left) // When cycle, adj the
//                  , cycledright= (islast&&!cycle?0:right) // 1st and last 
//                  )
//               [for( ii=range( i - cycledleft
//                             , i + cycledright + 1 
//                             ) ) newrange[ii] 
//               ] 
//           ]
//         // ]      // uncomment to debug
//
//	   // no cover  ---------------
//       : cycle
//		  ? concat( [for(i=[beg:intv:end])[i] ], [[beg]] )
//		  :[ for(i=[beg:intv:end]) [i]	]
//)(          
//   returnitems? sel(obj,rtn): rtn        
//);


//========================================================
UCONV_TABLE = [ "in,cm", 2.54
			  , "cm,in", 1/2.54
			  , "ft,cm", 2.54*12
			  , "cm,ft", 1/(2.54*12)
			  , "ft,in", 12
			  , "in,ft", 1/12	
			  ];
function uconv(n,units="in,cm", utable=UCONV_TABLE)=
let( 
	  frto = split(units,",")   
    //, isone = len(frto)==1    // units = "cm", "in", ... without ","
    , isftin= endwith(n, ["'","\""] ) 
	, m    = or(num(n), n) 
    , from = (isftin||len(frto)==1) ? "in":frto[0]
	, to   = or( frto[1] , frto[0] ) 
	, units= str(from, ",", to) 
    
	)
(
//    [ from, to ]
	(from==to) ? m
	: isnum(utable)? m* utable
    : m * hash( utable
			  , units )
);



//echo( sum([ [1,2,3],[4,5,6]]));      

//========================================================
//function uopt(opt1,opt2, df=[])=
//(  /*
//     Allow a module to mix sopt and opt:
//     
//       MarkPts( pts, "l,c|color=red", ["grid","true"] );
//       MarkPts( pts, ["color","red"], "l,c|color=red" );
//       
//       df=["l","label","c","chain"]
//   */
//   let( _1 = isstr(opt1)? sopt(opt1,df):opt1?opt1:[]
//      , _2 = isstr(opt2)? sopt(opt2,df):opt2?opt2:[]
//      , intra_sopt1 = hash(_1, "sopt","")
//      , intra_sopt2 = hash(_2, "sopt","")
//      , sopt_from_opt1= intra_sopt1? sopt( intra_sopt1, df ):[]
//      , sopt_from_opt2= intra_sopt2? sopt( intra_sopt2, df ):[]
//      )
//   updates( [ intra_sopt1?delkey(_1,"sopt"):_1
//            ,  sopt_from_opt1
//            , intra_sopt1?delkey(_2,"sopt"):_2
//            , sopt_from_opt2 ] )
//);






//ERRMSG = [
//  "rangeError"
//    , "<br/> => Index {iname}(={got}) out of range of {vname} (len={len})"
// ,"typeError"
//    , "<br/> => {vname}'s type {rel} {want}{unwant}. A {got} is given (value={val}) "
// ,"arrItemTypeError"
//    , "<br/> => Array {vname} {rel} item of type {want}{unwant}. ({vname}: {val})"
//,"gTypeError"
//    , "<br/> => {vname}'s gtype {rel} {want}{unwant}. A {got} is given (value={val}) "
//,"lenError"    
//    , "<br/> => {vname}'s len {rel} {want}{unwant}. A {got} is given (value={val}) "
//];   
               


////======================================
//vlinePt=["vlinePt", "pqr,len=1", "points", "Point",
// " Given a 3-pointer pqr and a len(default 1), return a point T such that
//;; 
//;; (1) TQ is perpendicular to PQ 
//;; (2) T is on the plane of PQR
//;; (3) T is on the R side of PQ line
//;; 
//;;           T   R
//;;        len|  / 
//;;        ---| /  
//;;        |  |/
//;;   P-------Q
//"];
//
//module vlinePt_test( ops=["mode",13] )
//{
//	doctest( vlinePt, ops=ops );
//}
//
//function vlinePt(pqr,len=1)=
// let(Q=Q(pqr)
//	,N=N(pqr)
//	,T=N([N,Q,P(pqr)])
//	)
//(
//	onlinePt( [Q,T], len=len )
//);
//
//module vlinePt_demo_0()
//{_mdoc("vlinePt_demo_0",
//"");
//
//	pqr= randPts(3);
//	P=P(pqr); Q= Q(pqr); R=R(pqr);
//
//
//	T = vlinePt(pqr);
//	echo("T=",T);
//	//Line0( [Q,T] );
//	pqt = [P,Q,T];
//	Mark90( pqt, ["r",0.05, "color","red"] );
//	Plane( pqt, ["mode",3] );
//	MarkPts( [P,Q,R,T], ["labels","PQRT"] );
//	Chain(pqt, ["r",0.05, "transp",1,"color","red"]); 
//	Chain(pqr, ["r",0.15, "transp",0.5]); 
//	
//}
////vlinePt_demo_0();

       
            
