include <scadx_core.scad>
include <../doctest/doctest2_dev.scad>
//
// The funcs here are usually called by scadx_doctesting_dev.scad.
// To test them directly in this file, un-mark the following line:
//include <../doctest/doctest_dev.scad>
//
FORHTML=false;
MODE=11;

/* 
=================================================================
                    scadx_core_test.scad
=================================================================
*/
/*
      fname = doct[ DT_iFNAME ]  // function name like "sum"
	, args  = doct[ DT_iARGS ]   // argument string like "x,y"
  //, rtntype = doct[ DT_iTYPE ] // return type
    , tags  = doct[ DT_iTAGS ]   // like, "Array, Inspect"
  //, doc   = doct[ DT_iDOC ]    // documentation (str)
    , rels  = doct[ DT_iREL ]
*/

////=========================================================
//accum=[ "accum", "arr", "array", "Math" 
//,"Given [a,b,c...], return [a,a+b,a+b+c...]
//"
//,"sum, sumto"
//];
//
//function accum_test( mode=MODE, opt=[] )=
//(
//    doctest2( accum,
//    [ 
//       [ accum([3,4,2,5]), [3,7,9,14], [3,4,2,5] ] 
//    ]
//    , mode=mode, opt=opt 
//    )
//);  
//echo( accum_test( mode=112 )[1] );

//========================================================
addx=[ "addx", "pt,x=0", "array", "Point",
 "Given a point or points (pt) and a x value, add x to pt.x.
` When pt is a collection of points, add x to each pt if x is
` a number, or add x[i] to pt[i].x if x is an array. That is, 
` by setting x as array, we can add different values to each 
` pt[i].x. 
` 
`   addx([2,3,4],x=1) => [3,3,4]
`   addx([[2,3],[5,6]],x=1) => [[3,3],[6,6]]
`   addx([[2,3],[5,6]],x=[8,9]) => [[10,3],[14,6]]
"
,"addy,addz,app,appi"
];

//========================================================
addx=[ "addx", "pt,x=0", "array", "Point"
, "Given a point or points (pt) and a x value, add x to pt.x.
;; When pt is a collection of points, add x to each pt if x is
;; a number, or add x[i] to pt[i].x if x is an array. That is, 
;; by setting x as array, we can add different values to each 
;; pt[i].x. 
;; 
;;   addx([2,3,4],x=1) => [3,3,4]
;;   addx([[2,3],[5,6]],x=1) => [[3,3],[6,6]]
;;   addx([[2,3],[5,6]],x=[8,9]) => [[10,3],[14,6]]
"
,"addy,addz,app,appi"
];

function addx_test( mode=MODE, opt=[] )=
(   
    let( pts= [[2,3,4],[5,6,7]] )
    doctest2( addx,
    [ 
      "// To a point:"
    , [addx([2,3],x=1), [3,3], "[2,3],x=1"]
    , [addx([2,3,4],x=1), [3,3,4], "[2,3,4],x=1"]
    , "// To points:"
    , [addx([[2,3],[5,6]],x=1), [[3,3],[6,6]], "[[2,3],[5,6]],x=1"]
    , [addx([[2,3],[5,6]],x=[8,9]), [[10,3],[14,6]], "[[2,3],[5,6]], x=[8,9]"]
    , [addx(pts,x=1), [[3,3,4],[6,6,7]], "[[2,3,4],[5,6,7]], x=1"]
    , [addx(pts,x=[2,3]), [[4,3,4],[8,6,7]], "[[2,3,4],[5,6,7]], x=[2,3]"]
    ]
    ,mode=mode, opt=opt
    )
);

//echo( addx_test(mode=112)[1] );

//========================================================
addy=[ "addy", "pt,y=0", "array", "Point"
, "Add y to pt. See doc in addx"
,"addx,addz,app,appi"
];

function addy_test( mode=MODE, opt=[] )=
(   
    let( pts= [[2,3,4],[5,6,7]] )
    doctest2( addy,
	[ 
      "// To a point:"
	, [addy([2,3],y=1), [2,4], "[2,3], y=1"]
	, [addy([2,3,4],y=1), [2,4,4], "[2,3,4], y=1"]
	, "// To points:"
	, [addy([[2,3],[5,6]],y=1), [[2,4],[5,7]], "[[2,3],[5,6]], y=1"]
	, [addy([[2,3],[5,6]],y=[8,9]), [[2,11],[5,15]], "[[2,3],[5,6]], y=[8,9]"]
	, [addy(pts,y=1), [[2,4,4],[5,7,7]], "[[2,3,4],[5,6,7]], y=1"]
	, [addy(pts,y=[2,3]), [[2,5,4],[5,9,7]], "[[2,3,4],[5,6,7]], y=[2,3]"]
	], mode=mode, opt=opt)
);

//========================================================
addz=[ "addz", "pt,z=0", "array", "Point"
, "Add z to pt. See doc in addx.
;; 
;; Note: addz(), unlike addx or addy, expands a 2D pt to 3D.
"
,"addx,addy,app,appi"
];
function addz_test( mode=MODE, opt=[] )=
(
    let( pts= [[2,3,4],[5,6,7]] )
    
    doctest2( addz,
	[ 
      "// To a point:"
	, [addz([2,3],z=1), [2,3,1], "[2,3], z=1"]
	, [addz([2,3,4],z=1), [2,3,5], "[2,3,4], z=1"]
	, "// To points:"
	, [addz([[2,3],[5,6]],z=1), [[2,3,1],[5,6,1]], "[[2,3],[5,6]], z=1"]
	, [addz([[2,3],[5,6]],z=[8,9]),[[2,3,8],[5,6,9]], "[[2,3],[5,6]], z=[8,9]"]
	, [addz(pts,z=1), [[2,3,5],[5,6,8]], "[[2,3,4],[5,6,7]], z=1"]
	, [addz(pts,z=[2,3]), [[2,3,6],[5,6,10]], "[[2,3,4],[5,6,7]], z=[2,3]"]
	], mode=mode, opt=opt )
);
    
//================================================================
all=["all", "arr,isdefined=false,item=undef)","T|F","Core, Inspect",
"Return true if all items in *arr* is:
;;  (1) ==item (when isdefined=true), or
;;  (2) treated as true (when isdefined=false)"
,"index"
];

function all_test( mode=112,opt=[])=
(
  doctest2( all,
  [
    [ all( [true,1,"2",-1] ), true, [true,1,"2",-1] ]
  , [ all( [true,1,"2",0] ), false, [true,1,"2",0] ]
  , [ all( [true,1,"2",undef] ), false, [true,1,"2",undef] ]
  , ""
  , "// Use isdefined:"
  , ""
  , [ all( [true,1,"2",-1],true,true ), false, "[true,1,'2',-1],true,true" ]
  , [ all( [0,0,0]),false, [0,0,0] ]
  , [ all( [0,0,0],true,0 ), true, "[0,0,0],true,0" ]
  , [ all( [undef,undef] ), false, [undef,undef] ]
  , [ all( [undef,undef],true,undef ), true
        , "[undef,undef],true,undef"]
  ]
  , mode=mode, opt=opt
  )
);      

//========================================================

angle=["angle","pqr,refpt=undef, lineHasToMeet=true","number","Angle",
 "Given a 3-pointer (pqr), return the angle of P-Q-R. 
;;    
;; 2015.3.3: new arg *refpt* : a point given to decide the sign of 
;;  angle. If the refpt is on the same side of normalPt, the angle
;;  is positive. Otherwise, negative. 
;;     
;; 2015.3.8: 
;;    
;;  Allows for angle( [line1,line2]). If lineHasToMeet(default), 2 lines 
;;  must have a lineCrossPt. If lineHasOtMeet=false , then the angle of 
;;  both lines' unit vectors is returned.
"
,"angle, angleBisectPt, anglePt, incenterPt, is90, Mark90"
];

function angle_test( mode=MODE, opt=[] ) =
( 
	let( p = [1,0,0]
        ,q = [0,sqrt(3),0]
        ,r = ORIGIN

        ,pqr1=[ p,q,r]
        ,pqr2=[ p,r,q]
        ,pqr3= [ r,p,q]
        ,pqr4= randPts(3)
        ,pqr5= randPts(3)
        ,pqr6= randPts(3)
        ,pqr7= [ORIGIN, [1,0,0], [3,2,0]]
        
        ,line1 = [[0,0,0],[0,0,1]]
        ,line2 = [[1,0,0],[2,0,0]] 
        ,twolines = [randPts(2),randPts(2)]
    )
//    function lines2()= [randPts(2), randPts(2)]; 
//    function angarg(lineHasToMeet)= str( "[randPts(2),randPts(2)], lineHasToMeet="
//                                       , lineHasToMeet );
	doctest2( angle,
	[
		[angle(pqr1),30, "pqr1" ]
		,[angle(pqr2),90, "pqr2" ]
		,[angle(pqr3),60, "pqr3" ]
		,[angle(pqr7),135, "pqr7" ]
        ,[angle([[1,0,0],[2,0,0],[3,0,0]]), 180, [[1,0,0],[2,0,0],[3,0,0]] ]
		,"// The following 3 are random demos. No test."
        ,[ angle( randPts(3) ),"","randPts(3)",["notest",true] ]
        ,[ angle( randPts(3) ),"","randPts(3)",["notest",true] ]
        ,[ angle( randPts(3) ),"","randPts(3)",["notest",true] ]
        ,"// angle of two lines:"
        //, str("Are line1,line2 on plane? ",isOnPlane( app(line1, line2[0]), line2[1]))
        //, str("lineCrossPt = ", lineCrossPt( line1,line2 ) )
        ,[ angle( [ line1,line2 ], lineHasToMeet=true), 90
         , "[line1,line2], lineHasToMeet=true" ] 
        ,[ angle( [ line1,line2 ], lineHasToMeet=false), 90
         , "[line1,line2], lineHasToMeet=false" ]

        ,"// twolines: randomly generated 2x2 array: [ [p1,q1], [p2,q2] ] "
//        ,[ angle( twolines, lineHasToMeet=true )
//          , "twolines, lineHasToMeet=true",""
//          ,["notest",true]]
        ,[ angle( twolines, lineHasToMeet=false ), ""
          , "twolines, lineHasToMeet=false"
          ,["notest",true]]

	
	], mode=mode, scope=["pqr1",pqr1,"pqr2",pqr2, "pqr3",pqr3
                      //  ,"pqr4",pqr4, "pqr5",pqr5, "pqr6",pqr6
                        ,"pqr7", pqr7
                        ,"line1",line1, "line2",line2
                        ,"lines", twolines
                        ]
                        ,opt=opt
	)
);

//echo( angle_test(mode=112)[1] );

//module angle_series_test( mode=MODE, opt=[] )=( //======================
//   _pqr= randPts(3);
//   echo( _pqr=_pqr );
//   P=P(_pqr); Q=Q(_pqr);
//    
//   //angs = [10,30,45,60,90,120,150, 170];
//   _angs= [for(i=range(10)) rand( 1,180 )];
//   angs = quicksort(_angs);
//   
//   Rs = [ for(a=angs) anglePt(_pqr, a=a) ]; 
//   precs= [5,10,20,40,60,"-"]; 
//   recs = [ for( i=range(Rs),p=precs )
//            let( a=angs[i], R=Rs[i], pqr=[P,Q,R] 
//               , ang= p!="-"?angle(pqr):0
//               , angcor = p!="-"?angle_cordic(pqr, prec=p):0
//               ) 
//            p=="-"? "//-----------------------"
//            :_s("a={_},p={_}, [angle,angle_cordic, diff] = [{_}, {_}, {_}]"
//              , [a,p,ang,angcor, ang-angcor]
//              )
//          ];   
//   doctest2(angle_series,
//    recs, mode=mode );
//}

//========================================================
angleBisectPt=["angleBisectPt", "pqr,len=undef,ratio=undef", "Angle,Point", "Angle, Point, Geometry"
,"Return a pt D so aPQD = aPQR/2 and D on lPR (if len and ratio not defined).
;; 
;; Point D is pqr`s angle bisector (divide aPQR evenly).
;; 
;;          _.Q
;;        _- /|
;;      _'  / |
;;   _-'   /  |
;;  P-----D---R
"
,"angle"
]; 

function angleBisectPt_test( mode=MODE, opt=[] )=
(
	let( pqr1 = randPts(3)
       , pqr2 = randPts(3)   
       , ang1 = angle( pqr1 )
       , ang2 = angle( pqr2 )
    )
	doctest2( angleBisectPt, 
	[
		"To test, we get 2 random 3-pointers, pqr1 and pqr2, to see if the angle is divided evenly."
		,""
        //,"var pqr1"
        ,"var ang1 // =angle(pqr1)"
		,[angle([pqr1[0],pqr1[1], angleBisectPt( pqr1 )])*2
                , ang1, "[P,Q,angleBisectPt(pqr1)]"
                //, ["myfunc","angle([p,q,angleBisectPt(pqr1)])*2"]
                , ["fname","angle(_)*2"]
        ]
		,""
        //,str("pqr2= ", arrprn( pqr2, 2))
        ,"var ang2 // =angle(pqr2)"
		,[angle([pqr2[0],pqr2[1], angleBisectPt( pqr2 )])*2 
                , ang2, "[P,Q,angleBisectPt(pqr2)]"
                //, ["myfunc","angle([p,q,angleBisectPt(pqr2)])*2"]
                , ["fname","angle(_)*2"]
         ]       
	]
	, mode=mode
    , scope= ["pqr1",pqr1,"pqr2",pqr2, "ang1",ang1,"ang2",ang2 ]
    , opt=opt
	)
);

//==============================================

anglePt=["anglePt","pqr,a=aPQR,a2=0,len=dPQ,ratio=1", "point", "Angle,Point"
, "Return pt S so aPQS=a, dSQ=len. If a2 given, return T= anglePt(SQN,a=a2,...) where N is pqr`s normal. 
;; 
;; a: aPQS. Default= aPQR (i.e., S=R) 
;; a2: aSQN where N= pqr`s normal. Default=0
;; len: default = dPQ
;; ratio: default = 1
;; 
;;  a2 not fiven:
;; 
;;           R     _ S    
;;         -'   _-' 
;;       -'  _-'  len
;;     -' _-' 
;;   -'_-'  a
;;  Q-------------------P
;; 
;;  a2 fiven:
;; 
;;        N         _T
;;        |      _-'
;;        |   _-:   a2= aSQT
;;       /|_-'   :
;;      | Q------'. --------- M 
;;      |/:'-._/  :
;;      /__:__/'-.|
;;     /    :      '-._
;;    /      R    _-'   S
;;   P ... a= aPQS
;; 
;; To rotate any pt X about [P,Q] line by angle ang : 
;; 
;;    anglePt( [P, othoPt( [P,X,Q] ), X], a2= ang ) 
"
 ];

function anglePt_test( mode=MODE, opt=[] )=
(
	doctest2( anglePt, [], mode=mode,opt=opt )
);

////==========================================================
//
//app=["app", "arr,x", "arr", "Array"
//,"Append x to arr"
//,"appi"
//];
//
//function app_test( mode=MODE, opt=[] ) =
//(  
//    doctest2( app,
//    [ 
//       [ app( [1,2,3],4 ),  [1,2,3,4], "[1,2,3], 4" ]
//    ], mode=mode )
//);
//
////==========================================================
//
//appi=["appi", "arr,i,x,conv=false", "arr", "Array"
//, "Append x to arr[i]. If conv=false, the arr[i] MUST be an array, or will return an error msg. 
//;; 
//;; If conv and it's not an array, it will be converted to array, 
//;; then appended with x. Arg i could be negative. 
//;; 
//;;   appi([1,2,[3,33],4], 2,9)= [1,2,[3,33,9],4] 
//;;   appi([1,2,[3,33],4],-2,9)= [1,2,[3,33,9],4] 
//;; 
//;; When arr[i] not an array:
//;; 
//;;   appi([1,2,[3,33],4],1,9) =   [typeError] : 
//;;       arr[1] in appi() should be arr. A int given (2) 
//;; 
//;; But setting conv=true:
//;; 
//;;   appi([1,2,[3,33],4],1,9,true)= [1,[2,9],[3,33],4]"
//,"app"
//];
//
//function appi_test( mode=MODE, opt=[] )=
//(
//    let( a=[1,2,[3,33],4] )
//    
//    doctest2( appi,
//    [
//      [ appi(a, 2,9), [1,2,[3,33,9],4],"[1,2,[3,33],4], 2,9" ]
//    //, [ appi(a, 2,9,true), [1,2,[3,33,9],4],"[1,2,[3,33],4], 2,9" ]
//    , [ appi(a,-2,9), [1,2,[3,33,9],4], "[1,2,[3,33],4],-2,9" ]
//      
//    ,""
//    ,"// If item-i not an array:"
//    ,""
//    ,[appi(a,1,9), "", "[1,2,[3,33],4],1,9)", ["notest",true] ]
//      
//    ,""
//    ,"// Set conv=true to turn it into an array:"
//    ,""
//    , [ appi(a,1,9,true), [1,[2,9],[3,33],4], "[1,2,[3,33],4],1,9,true"]
//    
//    ], mode=mode, opt=opt 
//    )
//);

//==========================================================

arcPts=["arcPts", "pqr,rad=2,a,count=6,pl='xy', cycle, pitch=0"
, "points", "Point"
, "Return #= count of 3d pts for a curve spanning angle 0~a w/ radius=rad.
;;
;; Given a 3-pointer(pqr), an angle(a), radius(rad), count of points from 
;; 0 ~ a (when cycle is not given) or count of points per cycle ( when
;; cycle is given), and optional cycle and pitch, return an array of 3d 
;; points for a curve spanning angle 0~a. Note that count 6= 6 points.
;; 
;; Since 1 cycle = 360 degrees, if cycle is given, the angle a is meaningless,
;; so will be ignored.
;;
;; The points will span a curve originating from the point Q, and on a 
;; plane defined by pl (see anyAnglePt doc for definition). 
;; 
;; If *pitch* is given, which defines the distant of *pitch* of a spiral, 
;; then a spiral will be drawn along the PQ line (i.e., the arc be drawn
;; on the yz plane => pl='yz'), starting from Q to P. A negative pitch 
;; produces a spiral going P-to-Q direction starting from Q. 
;;
;; Note that the spiral arc feature is available only along the PQ line." 
];

function arcPts_test( mode=MODE, opt=[] )=
(
	doctest2( arcPts, [], mode=mode, opt=opt )
);

//echo( arcPts_test(mode=112)[1] );

//==========================================================

arrblock=[ "arrblock","arr,indent='  ',v='| ', t='-',b='-',lt='.', lb='\"'","array", "Doc"
, "Given an array of strings, reformat each line
;; for echoing a string block . Arguments:
;; 
;;  indent: prefix each line; 
;;  v : vertical left edge, default '| '
;;  t : horizontal line on top, default '-'
;;  b : horizontal line on bottom, default '-'
;;  lt: left-top symbol, default '.'
;;  lb: left-bottom symbol, default `
;;  lnum: undef. Set to int to start line number
;;  lnumdot: '. '
;; 
;; join(arrblock( ['abc','def', 'ghi'], lnum=1 ), &lt;br/&gt;)= ''
;;  .---------------------------------------------
;;  | 1. abc
;;  | 2. def
;;  | 3. ghi
;;  `---------------------------------------------
"];

function arrblock_test( mode=MODE, opt=[] )=
(
    let( br="<br/>" )
	doctest2
	(
		arrblock
		,[
            [ join( arrblock(["abc","def"]), br)
            , join( [ "  .---------------------------------------------"
			  		, "  | abc"
			  		, "  | def"
			  		, "  '---------------------------------------------"
					], br)
            , ["abc","def"]
            , ["nl",true, "myfunc", "join( arrblock(['abc','def']), br) "] 
           ]  
    
		,	[ join(arrblock(["abc","def","ghi"], lnum=1),br)
            , join([ "  .---------------------------------------------"
			  		, "  | 1. abc"
			  		, "  | 2. def"
			  		, "  | 3. ghi"
			  		, "  '---------------------------------------------"
					], br ) 
			, "['abc','def', 'ghi'], lnum=1"
			, ["nl",true
              , "myfunc"
                 , ["join", "arrblock( ['abc','def','ghi'], lnum=1), br"]
              ] 
			]
		 ]
		,mode=mode, scope=["br","&lt;br/>"]
	)
);

//===========================================================================

arrprn=["arrprn","arr,dep,br,ind,commafirst,compact","str","format"
,"Format arr to a tree-like structure.
;; 
;; dep=1: depth of expension
;; br='&lt;br/&gt;': the html line break
;; ind= [ '&nbsp;', '&nbsp;']: joined to be the indent 
;; commafirst=true: ',' is placed before data, vertically align w/ [, ]
;; compact=true: 
;;    Pack multiple '['s if one next to each other."
,"fmt"
];

function arrprn_test( mode=MODE, opt=[] )=
(   
    let( arr= [ [[2,2],[2,3]], [[2,2],[2,3],[2,4],[2,5]], [[[3,2],[[4,2],[2,3]]]] ]
    )
    
    doctest2( arrprn,
    [
    "<b>Set dep (depth)</b>" 
    , [ "", arrprn(arr), "arr,dep=1", ["nl",true, "notest",true, "//","default"] ]
    , [ "", arrprn(arr, dep=2),"arr,dep=2", ["nl",true, "notest",true, "//", "(Noew: use a large dep for full depth)"] ]
    , "<b>Default commafirst = true: </b>" 
    , [ "", arrprn(arr, dep=2, commafirst=false)
            ,"arr,dep=2,commafirst=false", ["nl",true, "notest",true] ]
    , "<b>Default compact = true: </b>" 
    , [ "", arrprn(arr, dep=2, compact=false)
            ,"arr,dep=2,compact=false", ["nl",true, "notest",true] ]
    , "<b>Default ind= [ '&nbsp;', '&nbsp;']: </b>" 
    , [ "", arrprn(arr, dep=2, ind=["#", "..."])
            ,"arr,dep=2,ind=['#','...']", ["nl",true, "notest",true] ]
    ], mode=mode, opt=opt, scope=["arr",arr]
    )
);    

//echo( arrprn_test(mode=112)[1] );

//===========================================================================

assert=["assert","unit,var,check,[val,want,range,rel,got,pass,showfullval]"
       ,"str|true","Core,Debug"
,"Assert that a value or the type of a var is as expected. If pass, 
;; return true. Else, return an error msg like:
;;
;; ERROR:
;; [rangeError]:
;; => var i in get() should be  0&lt;=i&lt;=3. Got 4.(var= [10, 11, 12, 13])
;; 
;; ERROR:
;; [typeError]:
;; => var o in join() should be type arr. Got str.(o= 'abc')
;;
;; unit : Name of function or module where the target var in
;; var  : Target var nbame
;; val  : The var value
;; check: What to check, like 'type','range', etc. When fails,
;;        the returned str shows (check)Error. For example,
;;        check= 'type' ==> 'typeError'
;; range: For range check only. [i,j] to indicate inclusive val 
;;        range. If i>j, means target value x must be x>=i and 
;;        j>=x. If val is an array, range sets to [0,len(val)-1].
;; want : The expected. Could be a value,type,etc, depending on 'check'
;; rel  : A str to describe the relationship. Usually automatically
;;        set, but can be customized. See CHECK TYPE example below. 
;; got  : If not given, set to val
;; pass : True|False. Automatically decided, but can be customized.
;; showfullval: Default false. The (val=...) in the end of errmsg
;;        could be very long so it is cut to 30 chars. If set this
;;        to true, entire val will be shown.
"
,"errmsg"
];

function assert_test( mode=MODE, opt=[] )=
(
   let(  
    opt= ["highlight",true, "notest",true, "nl",false]
   )
    
    doctest2( assert,
    [
    
    ""
    , _b("** CHECK VALUE **")
    , ""
     
    , [ //replace( 
        assert( check="value"
              , unit="join"
              , var = "x"
              , val =  4
              , want= 5
              )//, "<", "&lt;")
       ,""
       ,"unit='join',var='x',val=4,check='value',want=5"
       , opt]
    , ""   
    ,   _b("** CHECK RANGE **")
    , ""
    , [ assert( check = "range"
              , unit="get"
              , var="i"
              , val= [10,11,12,13]
              , got= 4
              )
      ,""
      ,"unit='get',var='i',check='range',val=[10,11,12,13],got=4"
      , opt
      ] 
    ,""
    , [ assert( check = "range"
              , unit="get"
              , var="i"
              , range=[10,2]
              , got=4
              )
      , ""
      , "unit='get',var='i',check='range',range=[10,2], got=4"
      ,  opt
      ] 
       
    ,""
    ,  _b("** CHECK TYPE **")
    , ""
    
    , [ assert( check="type"
              , unit="join"
              , var = "o"
              , val = "abc"
              , want="arr"
              )
       , ""
       , "unit='join',var='o',val='abc',check='type',want='arr'"
       , opt
       ]
    ,""      
    , [ assert( check="type"
              , unit="join"
              , var = "o"
              , val = [1,2,[3,4]]
              , want= "arr"
              , rel = "shouldn't have item type"
              , pass = countArr([1,2,[3,4]])==0
              )
      , ""
      , "unit='join',var='o',val=[1,2,[3,4]],check='type',rel='shouldn`t have item type',want='arr',pass=countArr(o)==0"
      , opt]   
   
        
    ] 
     ,  mode=mode, opt=opt )
);    

//echo( assert_test(mode=112)[1] );


//==========================================================

begwith=["begwith","o,x","true|false", "String,Array,Inspect"
, "Check if o (str|arr) starts with x.
;; 
;; begwith('abcdef', 'abc')= true
;; begwith('abcdef', '')= undef
;; begwith(['ab','cd','ef'], 'ab')= true
;; 
;; New 2014.8.11:
;; 
;; x can be an array, return true if o begins with any item in x.
;; 
;; New 2015.2.17
;; 
;;  If x is array, instead of returning T/F, return the m or undef
;;  where m is the item in x that matches.
;; 
;; begwith('abcdef',['abc','ghi'])= 'abc' 
;; begwith(['ab','cd','ef'],['gh','ab'])= 'ab'
"    
,"begword,endwith"
];

function begwith_test( mode=MODE, opt=[] )=
(
	doctest2( begwith, 
	[ 
  	  [ begwith("abcdef","def"), false, "'abcdef', 'def'" ]
	, [ begwith("abcdef","abc"), true, "'abcdef', 'abc'" ]
	, [ begwith("abcdef", ""), undef, "'abcdef', '" ]
	, [ begwith(["ab","cd","ef"], "ab"), true, "['ab','cd','ef'], 'ab'"]
	, [ begwith([], "ab"), false, "[], 'ab'"]
	, [ begwith("", "ab"), false, "', 'ab'"]
	, "// New 2014.8.11"
  	, [ begwith("abcdef",["abc","ghi"]),"abc", "'abcdef',['abc','ghi']"] 
  	, [ begwith("ghidef",["abc","ghi"]),"ghi", "'ghidef',['abc','ghi']"] 
  	, [ begwith("aaadef",["abc","ghi"]),undef, "'aaadef',['abc','ghi']"] 
 	, [ begwith(["ab","cd","ef"], ["gh","ab"])
         , "ab", "['ab','cd','ef'], ['gh','ab']"]
	]
    , mode=mode, opt=opt )
);

//==========================================================
begmatch=["begmatch","s,ptns", "arr|undef", "Core, Inspect"
, "Check what pattern matches beginning of s. Return [pattern_name,matched_word] or undef.
;; 
;; ptns = list of [pattern_name, w0, w] 
;;        (see begword for the definition of w0 and w)
;; 
;; Example:
;; 
;; begmatch( 'a+b/2'
;;         , [ ['op','+-*/']
;;           , ['word']  // use default begword settings
;;           , ['num', '.0123456789']
;;           ] )
"
,"begwith,begword"
];

function begmatch_test( mode=MODE, opt=[] )=
(
    let(ptns= [ ["op","+-*/",""]
          , ["word"]  // use default begword settings
          , ["num", ".0123456789",".0123456789"]
          ])
    doctest2( begmatch,
    [
      "var ptns"
    , ""
    , [ begmatch( "ab+2.3/cd", ptns ), ["word","ab"],"'ab+2.3/cd',ptns"]
    , [ begmatch( "+2.3/cd", ptns ), ["op","+"],"'+2.3/cd',ptns"]
    , [ begmatch( "2.3/cd", ptns ), ["num","2.3"],"'2.3/cd',ptns"]
    , [ begmatch( "/cd", ptns ), ["op","/"],"'/cd',ptns"]
    , 
    ]
    , mode=mode, opt=opt, scope=["ptns",ptns]
    )
);
//==========================================================

begword=["begword","s,isnum=false,w0=...,w=...","str","String"
//begword=["begword","s,w,w0,isnum=false,dfw0=..., dfw=...","str","String"
,"Match beginning of s to word defined by w0 & w. Return the word matched or ''.
;; 
;; The pattern is defined as chars-allowed in w0 and w. 
;; It will first check if the 1st char can be found in w0, 
;; followed by checking if each char can be found in w 
;; until it fails. 
;; 
;; default:
;; 
;;   w0= str(a_z, A_Z, '_')  // df letter for s[0]
;;   w = str(a_z, A_Z, '_', '1234567890') // df letters
;; 
;; where a_z = 'abcdefghijklmnopqrstuvwxyz'
;; 
;; Set isnum=true to return the beginning digits
;; 
;;   begword( 'abc123,def' ) => 'abc123'
;;   begword( '123abc,def' ) => ''
;;   begword( '123abc,def',isnum=true ) => '123'
"
,"begwith,endwith"];

function begword_test( mode=MODE, opt=[] )=
(
    doctest2( begword
    , [
        [ begword("abc+123"), "abc","'abc+123'" ]
       ,[ begword("123"), "", "'123'"]
       ,[ begword("b123"), "b123", "'b123'"]
       ,[ begword("ab_c123"), "ab_c123", "'ab_c123'" ]
       ,[ begword("ab_c123",w="bc"), "ab", "'ab_c123',w='bc'"
                ]
       ,[ begword("x01+123"), "x01", "'x01+123'"]
       
       ,[ begword("abc123+5"), "abc123", "'abc123+5'" ]
       ,[ begword("_x123+5"), "_x123", "'_x123+5'" ]
       ,""
       ,"// Return the leading spaces: "
       ,[ begword("   abc",w0=" ",w=" "), "   "
             , "'   abc',w0=' ',w=' '"]
       ,""
       ,"// Match single char: "
       ,[ begword("+abc",w0="+-*",w=""), "+"
             , "'+abc',w0='+-*',w=''"]
       ,[ begword("*-abc",w0="+-*",w=""), "*"
             , "'*-abc',w0='+-*',w=''"]
       ,""
       ,"// Find leading numbers: "      
       ,[ begword("12.3+abc",isnum=true), "12.3", "'12.3+abc',isnum=true"]
       ,[ begword(".123+abc",isnum=true), ".123", "'.123+abc',isnum=true"]
       ,[ begword("0.5+abc",isnum=true), "0.5","'0.5+abc',isnum=true"]
       ,[ begword("12.3",isnum=true), "12.3","'12.3',isnum=true"]
       ,[ begword("3+a",isnum=true), "3","'3+a',isnum=true"]
       ,[ begword("(3+a)",isnum=true), "","'(3+a)',isnum=true"]
       ,[ begword("(3+a)"), "","'(3+a)'"]
       
       ,""
       ,"// new: we (word end)"
       ,[ begword("{abc}+123",w0="{",we="}"), "{abc}",
                "'{abc}+123',w0='{',we='}'" ]
       ,[ begword("sin(2+3)/2",w0="nis"
                  ,w=str("(",a_z,A_Z,"+-*/","0123456789")
                  , we=")")
         , "sin(2+3)"
         ,"'sin(2+3)/2',w0='nis',w=str('(',a_z,A_Z,'+-*/','0123456789') , we=')'" ]
     
//       ,"", "// It's too expensive to solve the problem below, so we leave it:"
//       ,""
//       ,str("> begword(\".12.3+abc\",isnum=true)= ", "\"\""
//            , str("\"",_red( begword(".12.3+abc",isnum=true) ) )
//           )
//       , [ begword(".12.3+abc",isnum=true), ".12.3", "'.12.3+abc',isnum=true"] 
       
    ], mode=mode, opt=opt )
);

// ========================================================

between=["between", "low,n,high,include=0", "T|F", "Inspect"
, "Check if n is between low/high bound.
;; 
;; include: decides if the bounds be included. Set it to 0 
;; (open, not included) or 1 (closed, included), or [0,1] 
;; or [1,0] to set them differently.
;; 
;;   between( 3,a,8 )= true
;;   between( 3,a,5 )= false
;;   between( 3,a,5,[0,1] )= true // Include right end
;;   between( 3,a,5,1 )= true // Include both ends
"];

function between_test( mode=MODE, opt=[] )=
(
   let(a=5)
   doctest2( between,
	[
       "var a",""
	  ,[ between(3,a,8),true, "3,a,8" ]
	  ,[ between(3,a,5),false, "3,a,5" ]
	  ,[ between(3,a,5,[0,1]),true, "3,a,5,[0,1]", ["//","Include right end"] ]
	  ,[ between(3,a,5,1),true, "3,a,5,1",  ["//","Include both ends"] ]
	  ,""	
	  ,[ between(5,a,8),false, "5,a,8" ]
	  ,[ between(5,a,8,[0,1]),false, "5,a,8,[0,1]",  ["//","Include right end"] ]
	  ,[ between(5,a,8),false,"5,a,8" ]
	  ,[ between(5,a,8,[1,0]),true, "5,a,8,[1,0]", ["//","Include left end"] ]
	  ,[ between(5,a,8,1),true, "5,a,8,1", ["//","Include both ends"] ]
	], mode=mode, scope=["a",a], opt=opt )
);



//module blockidx_test( mode=MODE, opt=[] )=(//==========================
//    doctest2( blockidx
//    ,[
////        ["'(x+1)*2'", blockidx("(x+1)*2"), [[0, "(", 4, ")"]] ]
////      , ["'x*(y+1)'", blockidx("x*(y+1)"), [[2, "(", 6, ")"]] ] 
////      , ["'(y+(z+2))*x'", blockidx("(y+(z+2))*x"), [[0, "(", 8, ")"], [3, "(", 7, ")"]] ]
////      , ["'((x+1)+y)*(z+2)'", blockidx("((x+1)+y)*(z+2)"),[[0, "(", 8, ")"], [1, "(", 5, ")"], [10, "(", 14, ")"]] ]
//      
//     [blockidx("((x+1)+y)*(z+2)", rootonly=true), "'((x+1)+y)*(z+2), rootonly=true'"
//           ,[[0, "(", 8, ")"], [10, "(", 14, ")"]] 
//        ]
//
////      , ["'(x+1)*(y+2)'",blockidx("(x+1)*(y+2)"),[[0, "(", 4, ")"], [6, "(", 10, ")"]] ]
////
////     ,  ["'[x+1]*[y+2]', ['[',']']",blockidx("[x+1]*[y+2]", blk=["[","]"])
////            ,[[0, "[", 4, "]"], [6, "[", 10, "]"]] ]
////      , ["'(x+1)*sin[y+2]', ['(',')','sin[',']']",blockidx("(x+1)*sin[y+2]", blk=["(",")","sin[","]"])
////            ,[[0, "(", 4, ")"], [6, "sin[", 13, "]"]] ]
////      , ["'(x+1)*[y+2]', ['(',')','[',']']",blockidx("(x+1)*[y+2]", blk=["(",")","[","]"])
////            ,[[0, "(", 4, ")"], [6, "[", 10, "]"]] ]
////      , ["'a(x+1)b*c[y+2]d', ['a(',')b','c[',']d']",blockidx("a(x+1)b*c[y+2]d", blk=["a(",")b","c[","]d"])
////            ,[[0, "a(", 5, ")b"], [8, "c[", 13, "]d"]]  ]
//      
//    ], mode=mode
//    );
//}  

//======================================================
//
//boxPts= ["boxPts","pq,bySize=true", "Array",  "Point, Geometry"
//, "Given 2-pointer (pq) and bysize(default= true), return
//;; a 8-pointer array representing the 8 corners of a box that
//;; has all edges perpendicular to axes. The box starts from the
//;; first pt (=p), and, if bysize=true, then the 2nd pt (q) is
//;; the sizes of box along x,y,z. If bySize=false,  q is used
//;; as the opposite corner. No matter which corner the p is at,
//;; the return 8 points starts w/ the left-bottom pt  (the one
//;; has the smaller x,y,z), then go anti-clockwise  to include
//;; all 4 bottom pts, then same manner of the top  4 pts. So the
//;; 5th pt (i=4) is right on top of the 1st.
//;; 
//;;       z      4----------7_
//;;       |      | `-_      | `-_
//;;       |      0_---`-_---3_   `-_
//;;   `-_ |        `-_   `5---------6
//;;  ----`+------y    `-_ |      `-_|
//;;       | `-_          `1---------2
//;;            ` x
//;; 
//;; NOTE: 
//;; 
//;; 1) Use cubePts that is similar but produces box points not 
//;;    necessary perpendicular to the axes;
//;; 2) Use rodfaces(4) to generate the faces needed for polyhedron.
//"];
//
//function boxPts_test( mode=MODE, opt=[] )=
//(
//    let(scope=["pq",[[1,2,-1],[2,1,1]]]
//	   , pq = hash(scope,"pq")
//       )
//	doctest2
//	(
//		boxPts
//		,[
//		  [	boxPts(pq), [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
//							, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0],       ], "pq", ["nl",true]
//		  ]
//		 ,[ boxPts(pq,true)
//			, [[1, 2, -1], [3, 2, -1], [3, 3, -1], [1, 3, -1]
//			, [1, 2, 0], [3, 2, 0], [3, 3, 0], [1, 3, 0]] 
//            , "pq,true", ["nl",true]
//		  ]
//		]
//		,mode=mode, scope=scope, opt=opt
//	)
//);

//======================================================

calc_flat=["calc_flat","s,scope","number","Math"
, "Calc simple formula s (no ( ) ).
;; 
;; operator priority: %, ^, */, +-, &|><=
;;
"
];
function calc_flat_test( mode=MODE, opt=[] )=
(
    let(scope=["a",3,"bb",4])
    doctest2( calc_flat,
    [
      str("scope= ", fmt(scope))
    , [ calc_flat("2+3",scope), 5, "'2+3',scope" ]    
    , [ calc_flat(5,scope), 5, "5,scope" ]    
    , [ calc_flat("5",scope), 5, "'5',scope" ]    
    , [ calc_flat("-5",scope), -5, "'-5',scope" ]    
    , [ calc_flat("a+bb",scope), 7, "'a+bb',scope" ]    
    , [ calc_flat("a",scope), 3, "'a',scope" ]    
    , [ calc_flat(" a + bb/ 2",scope), 5, "' a + bb/ 2',scope" ]    
    , [ calc_flat("a+bb^2",scope), 19, "'a+bb^2',scope" ]    
    , [ calc_flat("a^bb",scope), 81, "'a^bb',scope" ]    
    , [ calc_flat("a/bb*3",scope), 2.25, "'a/bb*3',scope" ]    
    , [ calc_flat("a*bb^2",scope), 48, "'a*bb^3',scope" ]    
    , [ calc_flat("2bb^2",scope), 32, "'2bb^2',scope" ]    
    , [ calc_flat("3.6/a",scope), 1.2, "'3.6/a',scope" ]    
    , [ calc_flat("2bb+3.6",scope), 11.6, "'2bb+3.6',scope" ]    
    , ""
    , [ calc_flat("a+2bb",scope), 11, "'a+2bb',scope" ] 
    ,""
    ,"// Allow new operators:"
    , [ calc_flat("1+a%2+bb",scope), 6, "'1+a%2+bb',scope" ]    
    , [ calc_flat("3=3",scope), 1, "'3=3',scope" ]    
    , [ calc_flat("a>bb",scope), 0, "'a>bb',scope" ]    
    , [ calc_flat("a<bb",scope), 1, "'a&lt;bb',scope" ]    
    , [ calc_flat("a&bb",scope), 1, "'a&bb',scope",["//","a and b"] ]    
    , [ calc_flat("a|bb",scope), 1, "'a|bb',scope",["//","a or b"] ]    
    , [ calc_flat("3<4",scope), 1, "'3&lt;4',scope" ]    
    
    ]
    , mode=mode
    , opt= opt //update(opt,["showscope",true])
    , scope=scope
    )
);

//echo( calc_flat_test()[1] );

//function calc_test( mode=MODE, opt=[] )=
//(
//    let( isdebug=false)
//    
//    doctest2( calc
//    ,[ 
//       [calc("11"),11,"'11'",  11 ]
//     , [calc("3/2+1"),2.5, "'3/2+1'", 2.5 ]
//     , [calc("x+1", ["x",3]),4, "'x+1',['x',3]", 4 ]
//     , [calc("(y+2)*x", ["x",3,"y",4]),18, "'(y+2)*x',['x',3,'y',4]", 18 ]
//     , [calc("x*(y+2)", ["x",3,"y",1]),9, "'x*(y+2)',['x',3,'y',1]", 9 ]
//     , [calc("x*(y+2)^2", ["x",3,"y",1]),81, "'x*(y+2)^2',['x',3,'y',1]", 81 ]
//     , [calc("x*((y+2)^2)", ["x",3,"y",1]),27, "'x*((y+2)^2)',['x',3,'y',1]", 27 ]
//     , [calc("sin(x)", ["x",30]),0.5,"'sin(x)',['x',30]",  0.5 ]
//     , [calc("sin(x)*2", ["x",30], isdebug=isdebug),1,"'sin(x)*2',['x',30]"
//         ,  1 ]
//     , [calc("sin(x)^2", ["x",30]),0.25, "'sin(x)^2',['x',30]",  0.25 ]
//     , [calc("(x+2)+sin(x)", ["x",30]), 32.5, "'(x+2)+sin(x)',['x',30 ]", 32.5 ]
//     , [calc("(x+2)+(sin(x)^2)", ["x",30]), 32.25,"'(x+2)+(sin(x)^2)',['x',30 ]", 32.25 ]
//    
//    
//     , [calc("sin(x)^2+(cos(x)^2)", ["x",30]), 1,"'sin(x)^2+(cos(x)^2)',['x',30]", 1 ]
// 
//       ,""
//       ,"// 3x is interpretated as 3 * x"
//       ,""
//     , [calc("2x", ["x",3]), 6,"'2x',['x',3]", 6 ]
//     , [calc("2x+1", ["x",3]), 7,"'2x+1',['x',3]", 7 ]
//     , [calc("2x+1.5y", ["x",3,"y",4]),12,"'2x+1.5y',['x',3,'y',4]",  12 ]
//     
//     , ""
//     , "// Net yet ready for 2(x+1) "
//     , ""
//     , [ calc("2(x+1)", ["x",3]), 8, "'2(x+1)', ['x',3]", ["notest",true] ] 
////     , str(" calc(\"2(x+1)\", [\"x\",3]) wants: 8, but: ", calc("2(x+1)", ["x",3]) )
//     
//     ], mode=mode );
//
//    module debug(fml)
//    {
//        uni = getuni(fml, blk=CALC_BLOCKS);
//        puni= packuni(uni);
//        echo( "<br/>", _fmth(["fml",fml, "uni",uni,"puni",puni]) ); //uni= _fmt(uni), puni= _fmt(puni) );
//    }
////      debug("2(x+1)");
////    debug("(y+2)*x");
////    debug("sin(x)*2");   
////    debug("(x+1)*sin(y+2)");
////    debug("(x+1)*sin(y+2)*3");
////    debug("sin(x)^2+cos(x)^2");
////    debug("(x+2)+(sin(x)^2)");
////    debug("(sin(x))^2+((cos(x))^2)")
//);   

//===============================================
calc_blk=["calc_blk","s,scope","num","Math",
"calc"
];

function calc_blk_test( mode=MODE, opt=[] )=
(  //==========================
    //isdebug=false;
    
    doctest2( calc_blk
    ,[ 
       ""
     ,  [calc_blk("11"),11,"'11'" ]
     , [calc_blk("3/2+1"),2.5, "'3/2+1'" ]
     , [calc_blk("x+1", ["x",3]),4, "'x+1',['x',3]" ]
     , [calc_blk("(y+2)*x", ["x",3,"y",4]),18, "'(y+2)*x',['x',3,'y',4]" ]
     , [calc_blk("x*(y+2)", ["x",3,"y",1]),9, "'x*(y+2)',['x',3,'y',1]" ]
     , [calc_blk("x*(y+2)^2", ["x",3,"y",1]),27, "'x*(y+2)^2',['x',3,'y',1]" ]
     , [calc_blk("x*(2y+2)^2", ["x",3,"y",1]),48, "'x*(2y+2)^2',['x',3,'y',1]" ]
    , [calc_blk("x*((y+2)^2)", ["x",3,"y",1]),27, "'x*((y+2)^2)',['x',3,'y',1]" ]
     , [calc_blk("(x*(y+2))^2", ["x",3,"y",1]),81, "'(x*(y+2))^2',['x',3,'y',1]" ]
//     , [calc_blk("sin(x)", ["x",30]),0.5,"'sin(x)',['x',30]",  0.5 ]
////     , [calc_blk("sin(x)*2", ["x",30], isdebug=isdebug),1,"'sin(x)*2',['x',30]"
////         ,  1 ]
//     , [calc_blk("sin(x)^2", ["x",30]),0.25, "'sin(x)^2',['x',30]",  0.25 ]
//     , [calc_blk("(x+2)+sin(x)", ["x",30]), 32.5, "'(x+2)+sin(x)',['x',30 ]", 32.5 ]
//     , [calc_blk("(x+2)+(sin(x)^2)", ["x",30]), 32.25,"'(x+2)+(sin(x)^2)',['x',30 ]", 32.25 ]
//    
//    
//     , [calc_blk("sin(x)^2+(cos(x)^2)", ["x",30]), 1,"'sin(x)^2+(cos(x)^2)',['x',30]", 1 ]
// 
//       ,""
//       ,"// 3x is interpretated as 3 * x"
//       ,""
//     , [calc_blk("2x", ["x",3]), 6,"'2x',['x',3]", 6 ]
//     , [calc_blk("2x+1", ["x",3]), 7,"'2x+1',['x',3]", 7 ]
//     , [calc_blk("2x+1.5y", ["x",3,"y",4]),12,"'2x+1.5y',['x',3,'y',4]",  12 ]
//     
//     , ""
//     , "// Net yet ready for 2(x+1) "
//     , ""
//     , [ calc_blk("2(x+1)", ["x",3]), 8, "'2(x+1)', ['x',3]", ["notest",true] ] 
//     , str(" calc_blk(\"2(x+1)\", [\"x\",3]) wants: 8, but: ", calc_blk("2(x+1)", ["x",3]) )
     
     ], mode=mode )

//    module debug(fml)
//    {
//        uni = getuni(fml, blk=CALC_BLOCKS);
//        puni= packuni(uni);
//        echo( "<br/>", _fmth(["fml",fml, "uni",uni,"puni",puni]) ); //uni= _fmt(uni), puni= _fmt(puni) );
//    }
//      debug("2(x+1)");
//    debug("(y+2)*x");
//    debug("sin(x)*2");   
//    debug("(x+1)*sin(y+2)");
//    debug("(x+1)*sin(y+2)*3");
//    debug("sin(x)^2+cos(x)^2");
//    debug("(x+2)+(sin(x)^2)");
//    debug("(sin(x))^2+((cos(x))^2)");
);    

//echo( calc_blk_test()[1] );

//===============================================
calc_func=["calc_func","s,scope","num","Math",
"Calc a string formula with variables defined in scope.
;; 
;; 1) Allowed operators in order of priority:
;;
;;  %, ^, */, +-, &lt;>=&|
;;
;; 2) Boolean operations (&lt;>=) are indicated with single
;; letter op, i.e., 
;;  
;;   a&b is a&&b, a|b is a||b, a=b is a==b
;; 
;; There's no way to carry out >=, &lt;= or &lt;>.
;;
;; 3) Also acceptable is conditional statement:
;; 
;;  a>b?b:c
;;
;; 4) 2a is treated as 2*a
;; 
;; 5) Spaces will be ignored.
"
,"calc_axb,calc_flat"
];

function calc_func_test( mode=MODE, opt=[] )=
(  //==========================
    //isdebug=false;
    
    doctest2( calc_func
    ,[ 
       "// Simple operations. Note that */ has priority over +-:"
       
     , [calc_func("11"),11,"'11'" ]
     , [calc_func("-11"),-11,"'-11'" ]
     , [calc_func("1+3/2"),2.5, "'1+3/2'" ]
     ,""
     ,"// With variables:"
     
     , [calc_func("x+1", ["x",3]),4, "'x+1',['x',3]" ]
     , [calc_func("x+yz^2", ["x",3,"yz",4]),19, "'x+yz^2',['x',3,'yz',4]" ]
     , ""
     , "// With paranthesis:"
     
     , [calc_func("(y+2)*x", ["x",3,"y",4]),18, "'(y+2)*x',['x',3,'y',4]" ]
     , [calc_func("x*(y+2)^2", ["x",3,"y",1]),27, "'x*(y+2)^2',['x',3,'y',1]" ]
     , [calc_func("x*(2y+2)^2", ["x",3,"y",1]),48, "'x*(2y+2)^2',['x',3,'y',1]" ]
     , [calc_func("(x*(y+2))^2", ["x",3,"y",1]),81, "'(x*(y+2))^2',['x',3,'y',1]" ]
     
     ,""
     ,"// built-in functions:"
     
     , [calc_func("sin(x)",["x",30]),0.5,"'sin(x)',['x',30]" ]
     , [calc_func("cos(x)",["x",30]),0.866025,"'cos(x)',['x',30]"]
     , [calc_func("round(x/2+1)",["x",3]),3,"'round(x/2+1)',['x',3]"]
          
     , [calc_func("sin(x)^2",["x",30]),0.25, "'sin(x)^2',['x',30]"]
     , [calc_func("(x+2)+sin(x)", ["x",30]), 32.5, "'(x+2)+sin(x)',['x',30 ]"]
     , [calc_func("(x+2)+(sin(x)^2)", ["x",30]), 32.25,"'(x+2)+(sin(x)^2)',['x',30 ]"]
     , [calc_func("cos(x)sin(x)", ["x",30]), 0.433013,"'(x+2)+(sin(x)^2)',['x',30 ]", ["asstr",true]]
     ,"// sin(x)^2+cos(x)^2 produces 0.999999, requiring "
     ,"//  round(0.999999) to make it 1:"
     , [round(calc_func("sin(x)^2+cos(x)^2", ["x",30])), 1,"'sin(x)^2+(cos(x)^2)',['x',30]" ]
         
       ,""
       ,"// 3x is interpretated as 3 * x"
       
     , [calc_func("2x", ["x",3]), 6,"'2x',['x',3]"]
     , [calc_func("2x+1", ["x",3]), 7,"'2x+1',['x',3]"]
     , [calc_func("2x+1.5y", ["x",3,"y",4]),12
            ,"'2x+1.5y',['x',3,'y',4]"]
     , [ calc_func("2(x+1)", ["x",3]), 8, "'2(x+1)', ['x',3]" ] 
     , [calc_func("2sin(x)",["x",30]),1,"'2sin(x)',['x',30]" ]
//     ,""
     ,""
     ,"// negative"
     , [calc_func("sin(270)"),-1,"'sin(270)'"]
     
     ,""
     , "// conditional"
     , [calc_func("a<b?2a:2b+1",["a",3,"b",4]),6
            ,"a&lt;b?2a:2b+1,['a',3,'b',4]"]
     , [calc_func("a>b?2a:2b+1",["a",3,"b",4]),9
            ,"a>b?2a:2b+1,['a',3,'b',4]"]
     , ""
     , "// array in scope returns a list "
     , [calc_func("sin(x)",["x",[0,90,270]]),[0,1,-1]
            ,"'sin(x)',['x',[0,90,270]"]
     , [calc_func("1+x^2",["x",range(4)]),[1,2,5,10]
            ,"'1+x^2',['x',range(4)"]
     , [calc_func("x%2?x^2:0",["x",range(6)]),[0,1,0,9,0,25]
            ,"'x%2?x^2:0',['x',range(6)]"]
     
     ], mode=mode, opt=opt )

);    

//echo( calc_func_test(mode=112)[1] );

//module calc_basic_test( mode=MODE, opt=[] )=(//==========================
//    doctest2( calc_basic
//    ,[ 
//       [ calc_basic([5]),5, [5]]
//      ,[ calc_basic(["x"], scope=["x",3]), 3, "['x'],['x',3]"]
//      ,[ calc_basic(["x","+",1],["x",3]), 4, "['x','+',1], ['x',3]" ]
//      ,[ calc_basic(["x","+","3"], scope=["x",3]), 6, "['x','+','3'],['x',3]"]
//      ,[ calc_basic(["x","*","2"], scope=["x",3]),6,"['x','*','2'],['x',3]"]
//      ,[ calc_basic([3,"+","x","+",3], scope=["x",3]),9,"[3,'+','x','+',3],['x',3]"]
//      ,[ calc_basic([2,"+","x","*",2], scope=["x",3]),10,"[2,'+','x','*',2],['x',3]"]
//
//      ,"// Multiple vars"
//      ,[ calc_basic([2,"+","x0","*","y"], scope=["x0",3,"y",4]),20,  
//          "[2,'+','x0','*','y'],['x0',3,'y',4]"]
//       
//       ,""
//       ,"// power "
//       ,""
//       ,[ calc_basic(["x","^","3"], scope=["x",3]), 27,"['x','^','3'],['x',3]"]
//       
//       ,[ calc_basic(["x","^","2","/","4"], scope=["x",3]),2.25, "['x','^','2','/','4'],['x',3]"]
//       ,[ calc_basic(["x","^","0.5"], scope=["x",4]),2,"['x','^','0.5'],['x',3]"]
//       ,[ calc_basic(["x","^","-2"], scope=["x",2]),0.25,"['x','^','-2'],['x',3]"]
//
//       ,""
//       ,"// negative "
//       ,""
//       ,[ calc_basic(["x","*","-2"], scope=["x",3]), -6,"['x','*','-2'],['x',3]"]
//       ,[ calc_basic(["-2","*","x"], scope=["x",3]),-6,"['-2','*','x'],['x',3]"]
//       ,[ calc_basic(["x","-","-21"], scope=["x",3]),24,"['x','-','-21'],['x',3]"]        
//       ,""
//       ,"// functions. Func name must be the first item."
//       ,""
//       ,[calc_basic(["sin",30]),0.5,["sin",30]]
//       ,[ calc_basic(["ceil",2.7]),3,["ceil",2.7]]
//                  
//        ,""
//        ,"// 2015.2.23: trying to make 2x+3 work:"
//        ,""
//        ,[calc_basic([2, "*", "x", "+", 1], ["x",3]),7
//                ,"[2,'*','x','+',1], ['x',3]"
//                ]
//        ,""
//       ,"// It takes only simple array, i.e., the array should NOT have array item."
//       ,""
//       ,str("> calc('x*2')= ", _red(calc_basic("x*2") ))
//       ,""
//       ,str("> calc((['2', '+', 'ceil', ['2.7']])= "
//            , _red( calc_basic(["2", "+", "ceil", ["2.7"]]) )
//            )
////       ,""
////       ,"// debugging "
////       ,""
////       ,[["sin",30], calc_basic(["sin",30]), 0.5]
//       
//       
//    ], mode=mode )
//);   


//==================================================

calc_shrink=["calc_shrink","data,level","arr","calc"
, "Shrink data (for internal use of calc_flat)"
];

function calc_shrink_test( mode=MODE, opt=[] )=
(
   let( data1 = [3,"-",4,"*",5,"+", 3 ]
      )
   doctest2( calc_shrink,
   [
     "var data1"
   , [ calc_shrink(data1),-14,data1 ]
   ]
   , mode=mode, opt=opt, scope=["data1",data1]
   )
);

//echo( calc_shrink_test()[1] );

//==================================================
centroidPt=["centroidPt","pqr","Point","Point"
, "Return the centroid pt of pqr (lines of pt-to-midPt meet).
;;
;; ref: http://en.wikipedia.org/wiki/Centroid
"];

function centroidPt_test( mode=MODE, opt=[] )=
( 
    doctest2( centroidPt,
    [
    ], mode=mode, opt=opt )
);    


//==================================================
cirfrags=[ "cirfrags", "r,fn,fs=0.01,fa=12", "number", "Object, Geometry",
 " Given a radius (r) and optional fn, fs, fa, return the number 
;; of fragments (NoF) in a circle. Similar to $fn,$fs,$fa but
;; this function (1) doesn't put a limit to r and (2) could return
;; a float. In reality divising a circle shouldn't have given partial
;; fragments (like 5.6 NoF/cicle). But we might need to calc the 
;; frags for an arc (part of a circle) so it'd better not to round it
;; off at circle level. 
;; fa: min angle for a fragment. Default 12 (30 NoF/cycle). 
;; fs: min frag size. W/ this, small circles have smaller 
;; NoFs than specified using fa. Default=0.01 (Note: built-in $fs 
;; has default=2). 
;; fn: set NoF directly. Setting it disables fs & fa. 
;;
;; When using fs/fa for a circle, min NoF = 5. 
;;
;; See: http://en.wikibooks.org/wiki/OpenSCAD_User_Manual/The_OpenSCAD_Language#.24fa.2C_.24fs_and_.24fn
"];

function cirfrags_test( mode=MODE, opt=[] )=
(
	doctest2( cirfrags, 
	[ 
		"// Without any frag args, the default frag is 30 (no matter r is):"
		,[cirfrags(6),30, "r=6",    30]
		,[cirfrags(3),30, "r=3",   30]
		,[cirfrags(0.1),30, "r=0.1", 30]

		,""
		,"// fn (num of frags) - set NoF directly. Independent of r, too:"
		,""
		
		,[cirfrags(3,fn=10),10,"r=3,fn=10"]
		,[cirfrags(3,fn=3),3 , "r=3,fn=3"]
		,[cirfrags(0.05, fn=12),12, "r=0.05, fn=12"]

		,""
		,"// fs (frag size)"
		,""
        ,[cirfrags(1,fs=1), 2*PI*1/1, "r=1,fs=1"]
        ,[cirfrags(2,fs=1), 2*PI*2/1, "r=2,fs=1"]
        ,[cirfrags(3,fs=1), 2*PI*3/1, "r=3,fs=1"]
        //,[cirfrags(6,fs=1), 2*PI*6/1, "r=6,fs=1"]
//		
//		,[cirfrags(1,fs=1),6.28319-4.69282*1e-6, "r=1,fs=1"]
//		,[cirfrags(2,fs=1),6.28319-4.69282*1e-6, "r=2,fs=1"]
//		,[cirfrags(1,fs=1),6.28319,"r=1,fs=1", ["asstr",true, "prec",6,"//","prec=6"]]
//		,[cirfrags(1,fs=1),6.28319,"r=1,fs=1", ["notest",true, "prec",6,"//","prec=6"]]
//		,[ cirfrags(1,fs=1),6.28319,"r=1,fs=1", ["prec",5,"//","prec=5"]]
//		,[cirfrags(1,fs=1),6.28319,"r=1,fs=1",  ["prec",4,"//","prec=4"]]
//		,[cirfrags(3,fs=1),18.8496,"r=3,fs=1",  ["asstr",true]]
//		,[cirfrags(6,fs=1),20,"r=6,fs=1"]

//		,[cirfrags(6,fs=2),18.8496,"r=6,fs=2", ["asstr",true]]
//		,[cirfrags(12,fs=4),18.8496,"r=12,fs=4", ["asstr",true]]
//		,[cirfrags(3,fs=10),5,"r=3,fs=10"]
//		,[cirfrags(6,fs=100),5,"r=6,fs=100", ["//","min=5 using fa/fs"] ]

		,""
		,"// fa (frag angle)"
		,""
		,[cirfrags(3,fa=10),36,"r=3,fa=10",  36]
		,[cirfrags(6,fa=100),5,"r=6,fa=100",  5,["rem","min=5 using fa/fs"] ]
		,""

	]
	, mode=mode, opt=opt
	)
);

////==================================================
//
//combine=["combine", "a,b", "arr","Array"
//,"Combine arrays a,b and no repeat in the result"
//,"join,slice,split"
//];
//
//function combine_test( mode=MODE, opt=[] )=
//(
//    doctest2( combine,
//    [
//      [ combine( [2,3,4],[3,5,7] ), [2,3,4,5,7], "[2,3,4],[3,5,7]" ]
//    , [ combine( [],[3,5,7] ), [3,5,7], "[],[3,5,7]" ]
//    , [ combine( [2,3,4],[] ), [2,3,4], "[2,3,4],[]" ]
//    ]
//    , mode=mode, opt=opt
//    )
//);

//==================================================
coordPts=["coordPts", "pqr,len,x,y,z","pts","Geo",
""
];

function coordPts_test( mode=MODE, opt=[] )=
(
    doctest2( coordPts, 
	[ 
	  
	], mode=mode, opt=opt
    )
);

//==================================================
cornerPt=[ "cornerPt", "pqr", "pt", "Point",
"
 Given a 3-pointer (pqr), return a pt that's on the opposite side
;; of Q across PR line.
;;
;;        Q-----R  Q--------R
;;       /   _//    ^.      |^.
;;      /  _/ /       ^.    |  ^.
;;     /_/   /          ^.  |    ^.
;;    //    /             ^.|      ^. 
;;   P     pt               P        pt
"];
 
 
function cornerPt_test( mode=MODE, opt=[] )=
(
    doctest2( cornerPt, 
	[ 
	  [ cornerPt([ [2,0,4],ORIGIN,[1,0,2]  ]),  [3,0,6]
            ,"[ [2,0,4],ORIGIN,[1,0,2]]"
      ]
	], mode=mode, opt=opt
    )
);

//==================================================
countArr= ["countArr", "arr", "int", "Array, Inspect, Type",
" Given array (arr), count the number of arrays in arr.
"];

function countArr_test( mode=MODE, opt=[] )=
(
	doctest2( countArr,
	[
	  [ countArr(["x",2,[1,2],5.1]),1, ["x",2,[1,2],5.1]]
	]
	, mode=mode, opt=opt
	)
);

//==================================================
countInt=["countInt", "arr", "int", "Array, Inspect, Type",
" Given array (arr), count the number of integers in arr.
"];

function countInt_test( mode=MODE, opt=[] )=
(
	doctest2( countInt, 
    [
	  [ countInt(["x",2,"3",5.3,4]),2, "['x',2,'3',5.3,4]"]
	]
	, mode=mode, opt=opt
	)
);

//==================================================
countNum= ["countNum", "arr", "int", "Array, Inspect, Type",
" Given array (arr), count the number of numbers in arr.
"];

function countNum_test( mode=MODE, opt=[] )=
(
	doctest2( countNum,
	[
	  [ countNum(["x",2,"3",5.3,4]),3, "['x',2,'3',5.3,4]" ]
	]
	, mode=mode, opt=opt
	)
);

//==================================================
countStr =  ["countStr", "arr", "int", "Array, Inspect, Type",
" Given array (arr), count the number of strings in arr.
"];

function countStr_test( mode=MODE, opt=[] )=
(
	doctest2( countStr,
	[
	  [ countStr(["x",2,"3",5]),2, "['x',2,'3',5]" ]
	, [ countStr(["x",2,5]),1, "['x',2, 5]" ]
	]
	, mode=mode, opt=opt
	)
); // countStr_test();

//==================================================
countType= ["countType", "arr,typ", "int", "Array, Inspect, Type",
" Given array (arr) and type name (typ), count the items of type typ in arr.\\
"];

function countType_test( mode=MODE, opt=[] )=
(
	doctest2( countType,
	[
      [ countType(["x",2,[1,2],5.1],"str"),1
        , "['x',2,[1,2],5.1], 'str'" ]
    , [ countType(["x",2,[1,2],5.1,3], "int"),2
        , "['x',2,[1,2],5.1,3], 'int'" ]
    , [ countType(["x",2,[1,2],5.1,3] ,"float"),1
        , "['x',2,[1,2],5.1,3], 'float'" ]
	] 
    , mode=mode, opt=opt )
); 

//==================================================
cubePts=["cubePts", "pqr,p=undef,r=undef,h=1", "points", "Point",
" Like boxPts but is not limited to axes-parallel boxes. That is, 
;; it produces 8 points for a cube of any size at any location along 
;; any direction. 
;; 
;; The input pqr = [P,Q,R0] is used to make the square PQRS that is
;; coplanar with R0. It is then added the h to make TUVW by finding 
;; the normal of pqr. Optional p,r,h: numbers indicating lengths as 
;; shown belo.
;;
;;     U------------------V_ 
;;     |'-_     R0        | '-_
;;     |   '-_ -''-_      |    '-_
;;     |   _. '-T---'-------------W
;;     |_-'     |      '-_|       |
;;     Q_-------|---------R_      | h
;;       '-_    |    r      '-_   |
;;       p  '-_ |              '-_|
;;             'P-----------------S
;;                        r
;; Return a 8-pointer: [P,Q,R,S,T,U,V,W] with order:
;;
;;     5----------6_ 
;;     | '-_      | '-_
;;     1_---'-_---2_   '-_
;;       '-_   '4---------7
;;          '-_ |      '-_|
;;             '0---------3
;;
;; Use rodfaces(4) to generate the faces needed for polyhedron. 
;; 
;; New 20141003: shift 
;;
;;     U------------------V_ 
;;     |'-_               | '-_
;;     |   '-_            |    '-_
;;    Q|------'-T-----------------W
;;     | '-_    |  r      |       |
;;     |_---'-_-|----------_      | 
;;       '-_   'P           '-_   | ---
;;          '-_ |              '-_|   -shift
;;             'X------------------ ---
;;
;; New 2015.3.7: pqr is optional. If not given, make it with randPts(3)
"];

function cubePts_test( mode=MODE, opt=[] )=
(
    doctest2(cubePts,mode=mode, opt=opt )
);

////==================================================
//deeprep=["deeprep","arr,indices,new", "arr","Array"
//, "Given arr, and list of indices ([i,j,k...], and a value, new, replace 
//;; the item arr[i][j][k]... with new. Items in indices could be negative. 
//;; 
//;; arr=
//;;  [0,1,                      ,3]
//;;        [20,            ,22]
//;;             [40,41,42]
//;;
//;;  deeprep( arr, [2,1,-1], 99 ):
//;; 
//;;  [0,1,                      ,3]
//;;        [20,            ,22]
//;;             [40,41,99]
//;;
//;;   deeprep( [0,1,[20,21,22],3],[-2,1],99 )= [0, 1, [20, 99, 22], 3]
//;;"
//];
//
//function deeprep_test( mode=MODE, opt=[] )=
//(     
//    doctest2( deeprep, 
//    [
//      [ deeprep([0,1,2,3],indices=[2], new=99 )
//            , [0,1,99,3]
//            ,"[0,1,2,3],[2],99"
//      ]
//    , [ deeprep([0,1,2,3],indices=[-1], new=99 )
//            , [0,1,2, 99]
//            ,"[0,1,2,3],[-1],99"
//      ]
//    , [ deeprep([0,1,[20,21,22],3],indices=[-2,1], new=99 )
//            , [0,1,[20,99,22],3]
//            ,"[0,1,[20,21,22],3],[-2,1],99"
//      ]
//    , [ deeprep([0,1,[20,[40,41,42],22],3]
//                            ,indices=[2,1,-1], new=99 
//                     )
//            , [0,1,[20,[40,41,99],22],3 ]
//            ,"[0,1,[20,[40,41,42],22],3], [2,1,-1],99"
//            , ["nl",true]
//      ]
//    , [ deeprep([0,1,[20,[40,41,42],22],3]
//                            ,indices=[2,-1], new=99 
//                     )
//            , [0,1,[20,[40,41,42],99],3]
//            ,"[0,1,[20,[40,41,42],22],3], [2,-1],99"
//            , ["nl",true]
//      ]
//    , [ deeprep([0,1,2,3],indices=[], new=99)
//            , [0,1,2,3]
//            ,"[0,1,2,3],[],99"
//      ]        
//    ]
//    , mode=mode, opt=opt )
//);   

////==================================================
//del=["del", "arr,x,byindex=false,count=1", "array", "Array",
// " Given an array arr, an x and optional byindex, delete all
//;; occurances of x from the array, or the item at index x when
//;; byindex is true. 
//;;
//;;   del( [2,4,6,8], 2 )= [4, 6, 8]
//;;   del( [3,undef, 4, undef], undef )= [3, 4]
//;;   del( [2,4,6,8], 2, byindex=true )= [2, 4, 8] 
//;;                      
//;; New 2014.8.7: arg *count*: when byindex=true, count sets 
//;;  how many items starting from x (an index) to del.
//;;
//;;   del( [2,4,6,8], 1, true,2 )= [2, 8] // del 2 tiems starting from 1                      
//"];
//
//function del_test( mode=MODE, opt=[] )=
//(
//	doctest2( del,
//    [ 
//      [ del( [2,4,4,8], 4), [2,8], "[2,4,4,8], 4" ]
//    , [ del( [2,4,6,8], 6), [2,4,8], "[2,4,6,8], 6" ]
//    , [ del( [2,4,6,8], 2), [4,6,8], "[2,4,6,8], 2" ]
//    , [ del( [3,undef,4,undef],undef),[3,4],"[3,undef,4,undef],undef" ]
//    , "// byindex = true"
//    , [ del( [2,4,6,8], 0, true), [4,6,8], "[2,4,6,8], 0, true" ]
//    , [ del( [2,4,6,8], 3, true), [2,4,6], "[2,4,6,8], 3, true" ]
//    , [ del( [2,4,6,8], 2, true), [2,4,8], "[2,4,6,8], 2, true" ]
//    , "//"
//    , "// New 2014.8.7: arg *count* when byindex=true"
//    , "//"
//    , [ del( [2,4,6,8], 1, true), [2,6,8], "[2,4,6,8], 1, true" ]
//    , [ del( [2,4,6,8], 1, true,2), [2,8], "[2,4,6,8], 1, true,2"
//               ,["//","del 2 tiems"]]
//    , [ del( [2,4,6,8], 2, true,2), [2,4], "[2,4,6,8], 2, true,2"
//        , ["//","del only 1 'cos it's in the end."] ]
//    , [ del( [], 1, true), [], "[], 1, true" ]
//    ]
//    , mode=mode, opt=opt
//	)
//);

//==================================================


//==================================================
delkey=[ "delkey", "h,k", "hash",  "Hash",
" Given a hash (h) and a key (k), delete the [key,val] pair from h. 
;;
;;   delkey( ['a',1,'b',2], 'b' )= ['a', 1]      
"];

function delkey_test( mode=MODE, opt=[] )=
(
	let( h1= ["a",1,"b",2]
	   , h2= [1,10,2,20,3,30] 
	   )

	doctest2( delkey,
	[
      "var h1"
    , [delkey([], "b"),[], "[], 'b'" ]
    , [delkey(h1, "b"), ["a",1], "h1, 'b'" ]
    , [delkey(h1, "c"), ["a",1,"b",2], "h1, 'c'" ]
    , ""
    , "var h2"
    , [delkey(h2, 1), [2,20,3,30], "h2, 1"]
    , [delkey(h2, 2), [1,10,3,30], "h2, 2"]
    , [delkey(h2, 3), [1,10,2,20], "h2, 3"]
    ]
    , mode=mode, opt=opt, scope= ["h1",h1,"h2",h2]
	)
);

////==================================================
//dels=["dels", "a1,a2", "array", "Array",
// " Given arrays a1, a2 delete all items in a2 from a1.
//;;
//;;   dels( [2,4,6,8], [4,6] )= [2, 8]
//;;
//;; Ref: arrItcp
//"];
//
//function dels_test( mode=MODE, opt=[] )=
//(
//	doctest2( dels,
//    [ 
//      [ dels( [2,4,4,8], 4), [2,8], "[2,4,4,8], 4" ]
//    , [ dels( [2,4,6,8], [4,6]), [2,8], "[2,4,6,8], [4,6]" ]
//    ]
//    , mode=mode, opt=opt
//	)
//);


//==================================================
det=["det","pq","array","Array,Math",
" Given a 2-pointer (pq), return the determinant. "
];

function det_test(mode=MODE, opt=[] )=
(
	doctest2( det, 
	[
	]
	, mode=mode, opt=opt )
);



//==================================================
dist=["dist","[a,b];a,b","number","len",
"The distance between a,b, both could be pt, line or plane.
;;
;; Or, when a is [P,Q] and b is one of str 'x','y','z', return
;; the dist of Q.x-Px, Q.y-P.y, Q.z-P.z, respectively. These
;; values could be negative. 
;;
;; Those involving in plane could be negative as well, such
;; that it can tell which side of the plane the target is.
;;
;; Given a plane [P,Q,R], pt N is on the positive side:
;;
;;       N
;;       |
;;       Q 
;;      / '-._
;;     /      'R
;;    P 
"
];

function dist_test( mode=MODE, opt=[] )=
(
	let(
    pqrs = randPts(4)
    ,P = pqrs[0]
    ,Q = pqrs[1]
    ,R = pqrs[2]
    ,S = pqrs[3]
    ,L1= [ [2,0,0], [2,2,0] ]
    ,L2= [ [0,0,0], [0,2,0] ]
    ,L3= [ [0,0,0], [0,2,2] ]
    ,dPQ = sqrt( pow(P.x-Q.x,2)
           + pow(P.y-Q.y,2)+ pow(P.z-Q.z,2) )
    )

	doctest2( dist, 
    [   
     _b("pt-pt")
    ,""
    , "var P", "var Q"
    , [ dist(P,Q),  dPQ, "P,Q" ]
    , [ dist([P,Q]),dPQ, "[P,Q]" ]

    ,"" //--------------------------------------------
    ,_b("pt-line")
    ,"// Any of the following work:"
    ,"//    (pt,ln), ([pt,ln]), (ln,pt), ([ln,pt])"
    ,""
    , "var L1", "var L2","var L3"

    , [ dist( L1, L2[1] ), 2, "L1, L2[1]" ]
    , [ dist( [L3[1], L1] ), longside(2,2), "[ L3[1],L1 ]" ]
    , [ dist( [L3[1], L3] ), 0, "[ L3[1],L3 ]" ]
        
    ,"" //--------------------------------------------
    ,_b("pt-plane")
    ,"// Any of the following work:"
    ,"//    (pt,pl), ([pt,pl]), (pl,pt), ([pl,pt])"
    ,""
    , [ dist( L3[1], app(L1,ORIGIN) ), -2, str("L3[1],", app(L1,ORIGIN)) ]
    
    ,"" //--------------------------------------------
    ,_b("line-line")
    ,"// Line-line dist: (ln,ln), ([ln,ln])"
    ,""
    , [ dist([L1,L2]), 2, "[L1,L2]", ["//","parallel"] ]
    , [ dist(L1,L3), 2, "L1,L3" ]
    , [ dist([L2,L3]), 0, "[L2,L3]", ["//","intersect"] ]
    ,"// https://www.youtube.com/watch?v=t8Ziil0954U"
    , [ dist([[[1,1,3],[0,2,2]],[[2,1,0],[3,-1,1]]])
        , 2*pow(2,0.5),[[ [1,1,3], [0,2,2]], [[2,1,3],[3,-1,1]]]]
    ,"// https://www.youtube.com/watch?v=sbvvIcw77VY"
    , [ dist([[[-2,1,0],[-1,0,1]],[[0,1,0],[1,2,2]]])
        , 6/pow(14,0.5),[[[-2,1,0],[-1,0,1]],[[0,1,0],[1,2,2]]]]


    ,"" //--------------------------------------------
    ,_b("line-plane")
    ,"// Line-plane dist: "
    ,"// (ln,pl), (ln,pl), ([pl,ln]), ([ln,pl])"
    ,""
    , [ dist( L1, app(L2,L3[1]) ), -2
        , str("L1,",app(L2,L3[1])) ]

    , [ dist( L1, [L2[0],L3[1],L2[1]] ), 2
        , str("L1,",[L2[0],L3[1],L2[1]]) ]

    
    ,"" //----------------------------------------------
    ,_b("b='x','y','z':")
    
    , [ dist(L1,"x"), 0, "L1,'x'"]
    , [ dist(L2,"y"), 2, "L2,'y'"]
    , [ dist([L1,"z"]), 0, "[L1,'z']"]
    ]	
    ,mode=mode, opt= opt //update( opt, ["showscope",true])
    ,scope=[ "P",P, "Q",Q,"R",R,"S",S
           , "L1",L1,"L2",L2, "L3",L3
           ]
	)
);

//echo( dist_test(mode=112)[1] );

//==================================================
distPtPl=["distPtPl", "pt,pqr", "number", "len" ,
 " Given a pt and a 3-pointer (pqr) , return the signed distance between
;; the plane formed by pqr and the point.
"];
	
/*
	Pt = [x0, y0, z0 ]
	Pl = Ax + By + Cz + D = 0;

 	      Ax0 + By0 + Cz0 + D
	d= --------------------------
		sqrt( A^2 + B^2 + C^2 ) = norm([A,B,C])

	pc = planecoefs( pqr ) = [A,B,C,D]

	Ax0 + By0 + Cz0 + D=  [pc[0],pc[1],pc[2]] * Pt  + pc[3] = 

*/
function distPtPl_test( mode=MODE, opt=[] )=
(
	let( pt= [1,2,0]
	   , pqr = [ [-1,0,2], [1,0,-3], [2,0,4]]
       )
	doctest2( distPtPl,
    [
      "var pt"
    , "var pqr"
    , [distPtPl(pt,pqr), 2, "pt,pqr" ]
	]
    , mode=mode, opt=opt
    , scope=[ "pt", pt
		    , "pqr", pqr ]	
	)
);

//==================================================
distx = ["distx", "pq", "number", "len",
" Given a 2-pointer pq, return difference of x between pq. i.e., 
;;
;; distx(pq) = pq[1][0]-pq[0][0]
"];

function distx_test( mode=MODE, opt=[] )=
(
	let( pq = [[0,3,1],[4,5,-1]]
	   , pq2= reverse(pq)
	   )       
	doctest2( distx, 
    [ 
      [ distx(pq), 4,"pq" ]			   		
    , [ distx(pq2), -4,"pq2" ]			   		
    ]
    , mode=mode, opt=opt, scope=["pq", pq, "pq2",pq2]
    )
);

//==================================================
disty= ["disty", "pq", "number", "len",
" Given a 2-pointer pq, return difference of y between pq. i.e.,
;;
;; disty(pq) = pq[1][1]-pq[0][1]
"];

function disty_test( mode=MODE, opt=[] )=
( 
	let( pq = [[0,3,1],[4,5,-1]]
	   , pq2= reverse(pq)
       )	
	doctest2( disty,
    [ 
      [ disty(pq), 2,"pq" ]			   		
    , [ disty(pq2), -2,"pq2" ]			   		
    ]
    , mode=mode, scope=["pq", pq, "pq2",pq2]
    )
);

//==================================================
distz= ["distz", "pq", "number", "len",
" Given a 2-pointer pq, return difference of z between pq. i.e.,
;;
;; distz(pq) = pq[1][2]-pq[0][2]
"];

function distz_test( mode=MODE, opt=[] )=
(
	let( pq = [[0,3,1],[4,5,-1]]
	   , pq2= reverse(pq)
       )
	doctest2( distz,
    [ 
      [ distz(pq), -2,"pq" ]			   		
    , [ distz(pq2), 2,"pq2" ]			   		
    ]
    ,mode=mode, opt=opt, scope=["pq", pq, "pq2",pq2]
	)
);



//==================================================
dot=["dot", "P,Q", "num", "Geometry, Math"
, "dot product of P,Q, same as P*Q . P*Q is generally easier than dot(P,Q). 
;; But if P and Q are complex, then dot() might be easier. For example
;; dot(P-Q,R-S) * dot(P-R,Q-S)  would be easier and more descriptive than 
;; ((P-Q)*(R-S))*((P-R)*(Q-S))
;;
;;   P= [2,3,4];  Q= [-1,0,2]; 
;;   dot( P,Q )= 6
;;   dot( [P,Q] )= 6
;;
"];

function dot_test(mode=MODE, opt=[] )=
( 
    let( P = [2,3,4]
       , Q = [-1,0,2]
       )
    
    doctest2( dot,
    [
      "var P", "var Q", str( "P*Q = ", P*Q )
    , ""
    , [ dot(P,Q), 6,"P,Q"]
    , [ dot([P,Q]), 6,"[P,Q]" ]
    ]
    , mode=mode, opt=opt, scope=["P",P,"Q",Q] 
    )
);

//==================================================
echoblock=[ "echoblock", "arr", "", "Doc, Console" , 
" Given an array of strings, echo them as a block. Arguments:
;;
;;   indent: prefix each line
;;   v : vertical left edge, default '| '
;;   h : horizontal line on top and bottom, default '-'
;;   lt: left-top symbol, default '.'
;;   lb: left-bottom symbol, default \"'\"
;;
;; See arrblock()
"];

function echoblock_test( mode=MODE, opt=[] )=
(
    doctest2( echoblock, mode=mode, opt=opt )
);

//==================================================
echo_=["echo_", "s,arr=undef", "", "Console",
" Given a string containing one or more '{_}' as blanks to fill,
;; and an array (arr) as the data to fill, echo a new s with all
;; blanks replaced by arr items one by one. If the arr is given as 
;; a string, it is the same as echo( str(s,arr) ).
;;
;; Example usage:
;;
;; echo_( '{_}.scad, version={_}', ['scadex', '20140531-1'] );
"];

function echo__test( mode=MODE, opt=[] )=
(
    doctest2( echo_, mode=mode, opt=opt )
);


//==================================================
echofh=["echofh","h","", "Hash, Console",
" Given a string template (s) containing '{key}' where key is a key of
;; a hash, and a hash (h), return a new string in which all keys in {??}
;; are replaced by their corresponding values in h. If showkey is true, 
;; replaced by key=val.
"]; 

function echofh_test( mode=MODE, opt=[] )=
(
    doctest2( echofh, mode=mode, opt=opt )
);

//==================================================
echoh=["echoh","s,h,showkey","", "Hash, Console",
" Given a string template (s) containing '{key}' where key is a key of
;; a hash, and a hash (h), return a new string in which all keys in {??}
;; are replaced by their corresponding values in h. If showkey is true, 
;; replaced by key=val.
"]; 


function echoh_test( mode=MODE, opt=[] )=
(
    doctest2( echoh, mode=mode, opt=opt )
);

//==================================================

endwith=[ "endwith", "o,x", "T/F",  "String, Array, Inspect",
"
 Given a str|arr (o) and x, return true if o ends with x.
;;
;; New 2014.8.11:
;;
;;  x can be an array, return true if o ends with any item in x.
;;
;; New 2015.2.17
;;
;;  If x is array, instead of returning T/F, return the m or undef
;;  where m is the item in x that matches.
;;
;; Ref: begwith()
"];

function endwith_test( mode=MODE, opt=[] )=
(
	doctest2( endwith, 
	[ 
    [ endwith("abcdef", "def"), true, "'abcdef', 'def'" ]
    , [ endwith("abcdef", "abc"), false, "'abcdef', 'def'" ]
    , [ endwith(["ab","cd","ef"], "ef"),true, "['ab','cd','ef'], 'ef'" ]
    , [ endwith(["ab","cd","ef"], "ab"),false, "['ab','cd','ef'], 'ab'" ]
    , [ endwith(33, "abc"), false, "33, 'def'" ]
    , "// New 2014.8.11"
    , [ endwith("abcdef", ["def","ghi"]), "def", "'abcdef', ['def','ghi']" ] 
    , [ endwith("abcghi", ["def","ghi"]),  "ghi", "'abcghi', ['def','ghi']" ] 
    , [ endwith("aaaddd", ["def","ghi"]), undef, "'abcddd', ['def','ghi']" ] 
    , [ endwith(["ab","cd","ef"], ["gh","ef"]),"ef", "['ab','cd','ef'], ['gh','ef']"]	
    ]
    , mode=mode, opt=opt
	)
);


//==================================================
endword=["endword","s,isnum=false,w0=...,w=...","str","String",
  "Check if s ends with word."
//,"Match beginning of s to word defined by w0 & w. Return the word matched or ''.
//;; 
//;; The pattern is defined as chars-allowed in w0 and w. 
//;; It will first check if the 1st char can be found in w0, 
//;; followed by checking if each char can be found in w 
//;; until it fails. 
//;; 
//;; default:
//;; 
//;;   w0= str(a_z, A_Z, '_')  // df letter for s[0]
//;;   w = str(a_z, A_Z, '_', '1234567890') // df letters
//;; 
//;; where a_z = 'abcdefghijklmnopqrstuvwxyz'
//;; 
//;; Set isnum=true to return the beginning digits
//;; 
//;;   endword( 'abc123,def' ) => 'abc123'
//;;   endword( '123abc,def' ) => ''
//;;   endword( '123abc,def',isnum=true ) => '123'
//"
,"begwith,endwith"];


function endword_test( mode=MODE, opt=[] )=
(
    doctest2( endword, mode=mode, opt=opt )
);

//==================================================
errmsg=["errmsg","errname,data,errmsg,showfulval","str","Core,Debug",
  "Return a preformatted error msg. 
  
  " 
,""];

function errmsg_test( mode=MODE, opt=[] )=
(
    let( err1= [ "fname","get"
            , "iname","i"
            , "vname","arr"
            , "len", 10
            , "got", 11
            ]
       , err2= [ "fname","join"
            , "vname","o"
            , "rel", "should be"
            , "want","arr"
            , "got", "str"
            , "val", "abc"
            ] 
       , err3= [ "fname","capital"
            , "vname","o"
            , "rel", "should NOT be"
            , "unwant","arr"
            , "got", "arr"
            , "val", [1,2,3]
            ]      
       , opt= ["highlight",false,"notest",true, "nl",true]
       )
    
    doctest2( errmsg, 
    [
        [ errmsg("rangeError" , err1
                ) //,"<","&lt;")
        ,"<span style='color:red'>[<u><b>rangeError</b></u>] in <u>get</u>(): <br/> => Index <u>i</u>(=<u>11</u>) out of range of <u>arr</u> (len=<u>10</u>)</span>"
        ,err1
        ,opt
        ]  
        
    ,   [ errmsg("typeError" , err2
                )// ,"<","&lt;")
        ,"<span style='color:red'>[<u><b>typeError</b></u>] in <u>join</u>(): <br/> => <u>o</u>'s type should be <u>arr</u>. A <u>str</u> is given (value=abc) </span>"
        ,err2
        ,opt
        ]  
        
    ,   [ replace(errmsg("typeError" , err3
                ) ,"<","&lt;")
        ,"<span style='color:red'>[<b><u>typeError</u></b>] in <u>capital</u>(): <br/> => <u>o</u>'s type should NOT be <u></u><u>arr</u>. A <u>arr</u> is given (value=<u>[1, 2, 3]</u>) </span>"
        ,err3
        ,opt
        ]           
    ]
    , mode=mode, opt=opt )
);
//echo( errmsg_test( mode=112)[1] );

//==================================================
eval=["eval","s,scope","any","String,Core,Inspect",
  "Eval s to a value"
,""];

function eval_test( mode=MODE, opt=[] )=
(
    doctest2( eval, 
    [
      [ eval("3"), 3, "'3'"]
    , [ eval("-3.5"), -3.5, "'-3.5'"]
    , [ eval("true"), true, "'true'"]
    , [ eval("false"), false, "'false'"]
    , [ eval("undef"), undef, "'undef'"]
    , [ eval("\"false\""), "false", "''false''"]
    , [ eval("[0,1,23]"), [0,1,23], "'[0,1,23]'"] 
    , [ eval(" [3.4, 5, \"age\"] "), [3.4,5,"age"]
              , "' [3.4, 5, \"age\"] '"]
    , [ eval(" age ",["age",20]), 20, "' age ', ['age',20]"]
    , [ eval(" [5, age] ",["age",20]), [5,20]
              , "' [5, age] ', ['age',20]"]
    , [ eval(" [5, \"age\"] ",["age",20]), [5,"age"]
              , "' [5, \"age\"] ', ['age',20]"]
    , ""         
    , "//Nested array taken care by evalarr():"
    , [ eval("[5, [age, 10]]",["age",20]), [5,[20,10]]
              , "'[5, [age, 10]]', ['age',20]"
              ,["nl",true]]
    ]
    , mode=mode, opt=opt )
);


//==================================================
evalarr=["evalarr","s,scope","any","String,Core,Inspect",
  "Eval an arr"
,""];

function evalarr_test( mode=MODE, opt=[] )=
(  
    let( s1= "[2, [3, 4],5,[a, [ b,6]]]"
       , s2= "[35,[2.5,[a,-1],3,[b,2],1]"
       , scp=["a",10,"b",11]
       )
    doctest2( evalarr, 
    [
      "", "var s1",""
    , [ evalarr(s1)
            , [2,[3, 4],5,["a", ["b",6]]]
            , "s1"
            ]
    , "", "var scp",""      
    , [ evalarr(s1,scope=scp)
            , [2,[3, 4],5,[10, [11,6]]]
            , "s1,scp"
            ]
            
    , "", "var s2",""        
    , [ evalarr(s2)
            , [35,[2.5,["a",-1],3,["b",2],1]]
            , "s2"
            ]
    , [ evalarr(s2,scope=scp)
            , [35,[2.5,[10,-1],3,[11,2],1]]
            , "s2,scp"
            ]
    ,""
    ,[ evalarr("[5, [age, 10]]")
            , [5,["age",10]]
            , "`[5, [age, 10]]`"
            ]
    ,[ evalarr("[5, [\"age\", 10]]")
            , [5,["age",10]]
            , "`[5, [\"age\", 10]]`"
            ]
    
    ,""
    ,[ evalarr( "[0,1]" ),[0,1],"'[0,1]'"]
    ,[ evalarr( "[true,false]" ),[true,false]
              , "'[true,false]'"]
    ,[ evalarr( "[false,true]" ),[false,true]
              ,"'[false,true]'"]
    
//    ,""  
//    ,"<b>blksymbol</b> and <b>sp</b>:"
//    ,""
//    ,[ evalarr( "[a;b=1;l=[c;d=2,e,3]]"//,sp=";"
//              , itemptn= str(A_zu,0_9d,";=,- \"")
//              )
//              , ["a","b=1","l",["c","d=2","e",3]]
//              ,"[a;b=1;l=[c;d=2,e,3]]"
//              ,["nl",true]
//     ]
    ]//doctest
    , mode=mode, opt=opt, scope=["s1",s1,"s2",s2,"scp",scp] 
    )
);

//echo( evalarr_test( mode=12 )[1]);

//==================================================
expandPts=["expandPts", "pts,dist=0.5,cycle=true", "points", "Point",
" Given an array of points (pts), return an array of points that *expand* every 
;; point outward as indicated by Q ==> Q2 in the graph below:
;;
;; d = dist
;;
;; ---------------------Q2
;;                     /
;;                    /
;; P----------Q_     /
;;           /  '-_ /
;;          /   d  :
;;         /      /
;;        /      /
;;       R      /
"
];

function expandPts_test( mode=MODE, opt=[] )=
(
    doctest2( expandPts, mode=mode, opt=opt )
);

//==================================================
fac=[ "fac", "n", "int",  "Math",
"
 Given n, return the factorial of n (= 1*2*...*n) 
"];

function fac_test( mode=MODE, opt=[] )=(//====================
	
    doctest2( fac,
    [ 
    [ fac(5), 120, 5 ]
    , [ fac(6), 720, 6 ]
    ], mode=mode, opt=opt
    )
);


//==================================================
faces=["faces","shape,sides,count", "array", "Faces,Geometry",
 " Given a shape ('rod', 'cubesides', 'tube', 'chain'), an int
;; (sides) and an optional int (count), return an array for faces of
;; a polyhedron for that shape.
"];

function faces_test( mode=MODE, opt=[] )=(//==================
    doctest2( faces,
    []
	,mode=mode, opt=opt
	)
);


////==================================================
//fidx=["fidx", "o,i,cycle=false,fitlen=false", "int|undef", "Index, Array, String, Inspect",
//" Try to fit index i to range of o (arr or str). If i is negative, convert it 
//;; to *legal* index value (means, positive int). For o of len L, this would mean 
//j;; -L &lt;= i &lt; L. That is, i should be in range *(-L,L]*. 
//;; 
//;; New 2014.8.17: o could be an integer for range 0 ~ o
//;;
//;; If i out of this range, return undef (when cycle=false) or:
//;; -- set fitlen=true to return the last index
//;; -- set cycle=true to loop from the other end of o. So if cycle=true, a given i 
//;;     will never go undef, no matter how large or small it is.  
//;;
//;; ref:
//;;
//;; get(arr,i)
//;; range(arr)
//"];
//
//function fidx_test( mode=MODE, opt=[] )=(//==================
//
//	doctest2( fidx, 
//    [
//		"// string "
//	  , [ fidx("789",2),2, "'789',2" ]
//	  ,	[ fidx("789",3), undef, "'789',3",  ["//","out of range"] ]
//	  ,	[ fidx("789",-2),1,"'789',-2" ]
//	  ,	[ fidx("789",-3),0, "'789',-3" ]
//	  ,	[ fidx("789",-4),undef, "'789',-4", ["//","out of range"] ]
//	  ,	[ fidx("",0),undef, "'',0" ]
//	  ,	[ fidx("789","a"), undef, "'789','a'" ]
//	  ,	[ fidx("789",true), undef, "'789',true" ]
//	  ,	[ fidx("789",[3]), undef, "'789',[3]" ]
//	  ,	[ fidx("789",undef), undef, "'789',undef" ]
//	  , "// array"
//	  ,	[ fidx([7,8,9],2), 2, "[7,8,9],2", ]
//	  ,	[ fidx([7,8,9],3), undef, "[7,8,9],3", ["//","out of range"] ]
//	  ,	[ fidx([7,8,9],-2), 1, "[7,8,9],-2", 1 ]
//	  ,	[ fidx([7,8,9],-3), 0, "[7,8,9],-3", 0 ]
//	  ,	[ fidx([7,8,9],-4), undef, "[7,8,9],-4", ["//","out of range"] ]
//	  ,	[ fidx([],0), undef,  "[],0" ]
//	  ,	[ fidx([7,8,9],"a"), undef, "[7,8,9],'a'" ]
//	  ,	[ fidx([7,8,9],true), undef,"[7,8,9],true" ]
//	  ,	[ fidx([7,8,9],[3]), undef,"[7,8,9],[3]" ]
//	  ,	[ fidx([7,8,9],undef), undef,"[7,8,9],undef" ]
//
//	  , "// If out of range, return undef, or:"
//	  , "//  -- Set cycle to go around the other end."
//	  , "//     (it never goes out of bound) "
//	  , "//  -- Set fitlen to return the last index."
//	  , "//"
//	  ,	[ fidx([7,8,9],3), undef,"[7,8,9],3", ["//","out of range"] ]
//	  ,	[ fidx([7,8,9],3,fitlen=true), 2, "[7,8,9],3,fitlen=true"
//                               , ["//","fitlen"] ]
//	  , [ fidx([7,8,9],3,true), 0, "[7,8,9],3, cycle=true" ]
//	  ,	[ fidx([7,8,9],4,true), 1, "[7,8,9],4, cycle=true" ]
//	  ,	[ fidx([7,8,9],7,true), 1, "[7,8,9],7, cycle=true" ]
//	  ,	[ fidx([7,8,9],-4), undef,"[7,8,9],-4",  ["//","out of range"] ]
//	  ,	[ fidx([7,8,9],-4,true), 2, "[7,8,9],-4, cycle=true"  ]
//	  ,	[ fidx([7,8,9],-16,true), 2, "[7,8,9],-16, cycle=true" ]
//	  ,	[ fidx([7,8,9],-5,true), 1, "[7,8,9],-5, cycle=true" ]
//	  
//	  ,"//"
//	  ,"// New 2014.8.17: o can be an integer for range 0~o"
//	  ,"//"
//	  ,	[ fidx(3,5), undef,"3,5", ["//","out of range"] ]
//	  ,	[ fidx(3,5,fitlen=true), 2, "3,5,fitlen=true"
//            , ["//","get the last idx"] ]
//	  ,	[ fidx(3,4,cycle=true), 1, "3,4,cycle=true"
//            , ["//","cycle from begin"] ]
//	  , [ fidx(3,-2), 1, "3,-2" ]
//	  ,	[ fidx(3,-3), 0, "3,-3" ]
//	  ,	[ fidx(3,-4), undef,"3,-4", ["//","out of range"] ]
//	  ,	[ fidx(3,-4,cycle=true), 2, "3,-4,cycle=true",  ["//","cycle=true"] ]
//	  , "//"	
//
//	  ], mode=mode, opt=opt
//	)
//);
//
////echo( fidx_test( mode=112 )[1] );

////==================================================
//flatten=["flatten", "a,d=undef", "arr", "Core, array",
//,"Flatten array a. d: depth. If d=undef (default), 
//;; flatten all; if d=0, return a.
//"];
//
//function flatten_test( mode=MODE, opt=[] )=
//(
//    let( a=[ [[1,2],[2,3]], [[[1,2],[2,3]]] ]
//       , b=[ ["c",[2,3]], [["cc",[2,3]]] ]
//       , c=[ ["abc", ["def","g"]],"h"]
//       )
//    
//    doctest2( flatten,
//    [
//      "var a",""
//    , [ flatten(a), [1, 2, 2, 3, 1, 2, 2, 3], "a" ]
//    , [ flatten(a,0), [ [[1,2],[2,3]], [[[1,2],[2,3]]] ] , "a,d=0"]
//    , [ flatten(a,1), [[1, 2], [2, 3], [[1, 2], [2, 3]]] , "a,d=1"]
//    , [ flatten(a,2), [1, 2, 2, 3, [1, 2], [2, 3]] , "a,d=2"]
//    , "","var b",""
//    , [ flatten(b,1), ["c", [2, 3], [ "cc", [2, 3]]] , "b,d=1"]
//    , [ flatten(b,2), ["c", 2, 3, "cc", [2, 3]] , "b,d=2"]
//    , "", "var c",""
//    , [ flatten(c), [ "abc", "def","g","h"] , "c"]
//    , "// Note that flatten( [a,b,c],d=1 ) = concat( a,b,c )"
//    , "// when all a,b,c are arrays."
//    ]
//    ,mode=mode, opt=opt, scope=["a",a,"b",b,"c",c]
//    )    
//);

//==================================================
_f=["_f", "x,fmt,sp='|',pad=' '", "str", "String, Doc, Console" 
,"Format x with format fmt.
;;
;; fmt could have one or more of 3 layer formats:
;;
;;   f1|f2|f3, seperated by sp
;;
;;   f1: single char formatting like:
;;       b: boldface (or i,u for italic, underscore)
;;       %: percentage
;;       v: subscript
;;       ^: superscript
;;       a: loop through x if x is array
;;   f2: 2-char formatting like:
;;       w9: width=9. For width>9, use a,b,c ...
;;           wa=10, wb=11,... wm=22, etc.
;;       d2: decimal(precision) position=2 
;;       al: align left (or ac, ar for center,right)
;;       px: padded with char 'x' to reach width. 
;;           Default padding can be set on func level:
;;           _f( ... pad='abc')
;;           which allows for padding longer than 1 char.
;;   f3: key=value type of formatting like:
;;       wr={,} : wrap with { and }
;;       a={,}  : wrap with { and } if x is array 
;;       s={,}  : wrap with { and } if x is str
;;       n={,}  : wrap with { and } if x is number
;;       c=red  : make it red (or, bc for background color)
;;       x=a:b  : convert x from unit a to unit b
;;                like x=in:ftin (see numstr())
;;       aj=,   : Used when x is arr and flag a is used
;;
;; To use f1, just: fmt='b%'
;;
;; To use f2, either combine with f1: 'b%|w9d2' or '|w9d2'
;;
;; For f3: 'b%|w9d2|c=red' or '|w9d2|c=red' or '||c=red'
;; 
;; For more f3, just keep adding with |:
;;   'b%|w9d2|c=red|bc=blue', '||c=red|bc=blue'
"
];

function _f_test( mode=MODE, opt=[] )=
(
    doctest2( _f,
    [
      "// Integer: "
    , [ _f(5), "5", "5"]
    , [ _f("5"), "5", "'5'"]
    
    ,""
    ,"// f1 in fmt. f1 can have one or more of bui^v%,"
    ,"// each char for a format setting." 
    ,""
    , [ _f(0.5,"%"), "50%", "0.5,'%'", ["//","%"]]
    , [ _f(1.4,"%"), "140%", "1.4,'%'", ["//","%"]]
    , [ _f(30,"b"), "<b>30</b>", "30,'b'", ["//","b"]]
    , [ _f(30,"i"), "<i>30</i>", "30,'i'", ["//","i"]]
    , [ _f(30,"u"), "<u>30</u>", "30,'u'", ["//","u"]]
    , [ _f(30,"bu"), "<u><b>30</b></u>", "30,'bu'", ["//","bu"]]
    , [ _f(30,"^"), "<sup>30</sup>", "30,'^'"
                  , ["//","^: superscript"]]
    , [ _f(30,"v"), "<sub>30</sub>", "30,'v'"
                  , ["//","v: subscript"]]
    ,, [ _f(0.3,"%bu"), "<u><b>30%</b></u>", "0.3,'%bu'"
                  , ["//","multiple "]]
    
    ,""
    ,"// f2 in fmt. f1 can have one or more pair of "
    ,"// w?a?p?d?. Each pair for a format setting, with "
    ,"// the first char the format name, the 2nd value." 
    ,""    
    , [ _f(3.8,"|d2"), "3.80", "3.8,'|d2'"]
    , [ _f(3.839,"|d2"), "3.84", "3.839,'|d2'"]
    , [ _f(3.839,"|d0"), "4", "3.839,'|d0'"]
    , [ _f(3.839,"|d0"), "4", "3.839,'|d0'"]
    , [ _f(3.839,"|w8"), "   3.839", "3.839,'|w8'"]
    , [ _f(3.839,"|w8d2"), "    3.84", "3.839,'|w8d2'"]
    , [ _f(3.839,"|w8al"), "3.839   ", "3.839,'|w8al'"]
    , [ _f(3.839,"|w8ac"), "  3.839 ", "3.839,'|w8ac'"]
    , [ _f(3.839,"|w8d2ac"), "  3.84  ", "3.839,'|w8d2ac'"]
    , [ _f(3.839,"|w4"), "3.83", "3.839,'|w4'"
         ,["//","w too small, get cut"]
      ]
    , [ _f(3.839,"|w8acp#"), "##3.839#", "3.839,'|w8acp#'"]
    , [ _f("abcdefghij","|wc"), "  abcdefghij"
            , "'abcdefghij','|wc'"
            , ["//","wc: set width to 12. wa:10,wb:11..."]
      ]
    ,""
    ,"// Combine f1 and f2" 
    ,""    
    , [ _f(1.4685,"%|d1"), "146.9%", "1.4685,'%|d2'", ["//","% & d2"]]
    , [ _f(0.4685,"b%|d0w9p-ac"), "<b>---47%---</b>", "146.85,'b%|d2w9p-ac'", ["//","%"]]
    , [ _f(1.685,"%|w9al"), "168.5%   ", "1.685,'%|w9al'"]
    , [ _f(1.685,"%|w9ac"), "  168.5% ", "1.685,'%|w9ac'"]
    , [ _f(1.685,"%|w6"), "168.5%", "1.685,'%|w6'"]
    , [ _f(1.685,"%|w4"), "168%", "1.685,'%|w4'"]
   ,""
    ,"// f3 in fmt. "
    ,"//  'w={,}' "
    ,""    
    , [ _f(30, "||wr={,}"), "{30}", "30, '||wr={,}'", ["//","wrap"]]
    , [ _f(30, "||a={,}"), "30", "30, '||a={,}'", ["//","wrap if arr"] ]
    , [ _f(30, "||n=/,/"), "/30/", "30, '||n=|,|'",["//","wrap if num"] ]
    , [ _f([2,3], "||a={,}"), "{[2, 3]}", "[2,3], '||a={,}'" ]
//    , [ _f(30, "||sty=color:red;")
//          ,"<span style='color:red;'>30</span>"
//          ,""
//      ]
//    , [ _f(30, "||fs=20")
//            , "<span style=\"font-size:20px\">30</span>"
//            , "30, '||fs=20'" 
//            , ["showhtm",true]
//      ]
    ,""
    , [ _f(96, "||x=in:ftin"), "8'", "96, '||x=in:ftin'", ["//","Unit convert"] ]
    , [ _f(3.5, "||x=ft:ftin"), "3'6\"", "3.5, '||x=ft:ftin'" ]
    , [ _f(3.5, "||x=ft:in"), "41\"", "3.5, '||x=ft:in'" ]
    , [ _f(1, "||x=cm:in"), "2.54\"", "1, '||x=cm:in'" ]
    , [ _f(96, "||wr={,}|x=in:ftin"), "{8'}"
                , "96, '||wr={,}|x=in:ftin'" ]
      
    ,""  
    , "// The following are hard to test, but we can show it:"
    , str( "_f(",_arg("96, '||c=blue'"),") = ", _f(96, "||c=blue") )
    , str( "_f(",_arg("96, '|w6ac|wr={,}|c=cyan|bc=black', pad='&nbsp;'"),") = "
        , _f(96, "|w6ac|wr=<code>{,}</code>|c=cyan|bc=black", pad="&nbsp;") )
    
    ,""
    ,"// Combine f1, f2 and f3" 
    ,""    
    , [ _f(1.4685, "%b|wbd2acpx|wr={,}")
        ,"{<b>xx146.85%xx</b>}", "1.4685, '%b|wbacpx|wr={,}'" ]
    , [ _f(1.4685, "%b|wbd2acpx|wr={,}|n=/,/")
        ,"/{<b>xx146.85%xx</b>}/", "1.4685, '%b|wbacpx|wr={,}|n=/,/'" ]
    , [ _f(1.4685, "%b|wbd2acpx|n=/,/|wr={,}")
            ,"/{<b>xx146.85%xx</b>}/", "1.4685, '%b|wbacpx|n=/,/|wr={,}'" ]
  
    ,""
    ,"// flag a for array: "
    ,"//  _f( [2,3,4], 'a|d1') = '[2.0, 3.0, 4.0]'" 
    ,""
    ,[ _f( [2,3,4], "a|d1"), "[2.0, 3.0, 4.0]"
         , "[2,3,4], 'a|d1'"
     ]     
    ,[ _f( [2,3,4], "a|d1|aj=,"), "[2.0,3.0,4.0]"
         , "[2,3,4], 'a|d1|aj=,'"
     ]     
    ,[ _f( [0.2,0.3,0.4], "a%"), "[20%, 30%, 40%]"
         , "[0.2,0.3,0.4], 'a%'"
     ]   
    ,"","Chk out the effect of a: ",""
    , [ _f( [2,3,4], "||wr={,}"), "{[2, 3, 4]}"
         , "[2,3,4], '||wr={,}'"]
    , [ _f( [2,3,4], "a||wr={,}"), "[{2}, {3}, {4}]"
         , "[2,3,4], 'a||wr={,}'"]
//         
//    , str("_f( [2,3,4], \"a||wr={,}\")= "
//         , _f( [2,3,4], "a||wr={,}"))
//    ]   
//    , str("_f( [2,3,4], \"||wr={,}\")= "
//         , _f( [2,3,4], "||wr={,}"))
//    , str("_f( [2,3,4], \"a||wr={,}\")= "
//         , _f( [2,3,4], "a||wr={,}"))
    ]//doctest 
    , mode=mode, opt=opt
    )
);

//echo( _f_test(mode=12)[1] );

//==================================================
_fmt=["_fmt", "x,[s,n,a,ud,fixedspace]", "string", "String, Doc, Console" 
," Given an variable (x), and format settings for string (s), number (n),
;; array (a) or undef (ud), return a formatted string of x. If fixedspace
;; is set to true, replace spaces with html space. The formatting is
;; entered in '(prefix),(suffix)' pattern. For example, a '{,}' converts
;; x to '{x}'.
"
];

function _fmt_test( mode=MODE, opt=[] )=(//====================

	doctest2(
	 _fmt
	,[
	   str("_fmt(3.45): ", _fmt(3.45))
     , str("_fmt(3.45, n='{,}'): ", _fmt(3.45, n="{,}"))
     ,  str("_fmt(['a',[3,'b'], 4]): ", _fmt(["a",[3,"b"], 4] ))
     ,  str("_fmt(['a',[3,'b'], 4], n='{,}'): ", _fmt(["a",[3,"b"], 4],n="{,}" ))
     ,  str("_fmt(['a',[3,'b'], 4], a='&lt;u>,&lt;/u>'): ", _fmt(["a",[3,"b"], 4],a="<u>,</u>" ))
     ,  str("_fmt('a string with     5 spaces'): ", _fmt("a string with     5 spaces"))			, str("_fmt(false): ", _fmt(false))

	],  mode=mode, opt=opt
	)
);


//==================================================
_fmth=["_fmth" ,"h,pairbreak=' , ',keywrap='<b>,</b>',valwrap='<u>,</u>', pairwrap=',',eq='=',iskeyfmt=false,sectionhead='####',comment='////'" 
	   ,"string","String, Doc"
,"Return a formatted hash. Usually used with echo().
;;
;;  keywrap,valwrap,pairwrap: strs wrap around each key,val or pair,
;;     respectively.
;;  eq: symbol between key and val
;;  iskeyfmt: format key if true
;;  sectionhead, comment: if key is a sectionhead or comment, convert
;;     the pair to section head or comment, respectively
;;
;; The return is a single line of string. Setting pairbreak to &lt;br/&gt;
;; will break the line to lines of k-v pairs. 
;;
"];

function _fmth_test( mode=MODE, opt=[] )=
(
	let( h = ["num",23,"str","test", "arr", [2,"a"] ]
	   , h2= ["pqr", [2,3,4]]
	   , h3= ["pqr", [[0,1,2],undef] ]
	   )

	doctest2(_fmth,
	[
	 "var h"
    ,str("_fmth(h): ", _fmth(h))
    , "var h2"
	,str("_fmth(h2): ", _fmth(h2))
    , "var h3"
	,str("_fmth(h3): ", _fmth(h3))
    ,str("_fmth(h3,iskeyfmt=true): ", _fmth(h3, iskeyfmt=true))
    ,str("_fmth(h,pairbreak='&lt;br/>'): ", str("<br/>",_fmth(h,pairbreak="<br/>")))
//	,[ "", _fmth(h,pairbreak="<br/>")
//     ,"h,pairbreak='&lt;br/>'", ["notest", true, "nl",true]
//          ]
    
//	  ["['a',23]", _fmth(["a",23]), "[\"a\",23]" ]
//	, ["h", _fmth(h ),""]
//	, ["h2", _fmth( h2),""]
//	, ["h3", _fmth( h3),""]

	],  mode=mode, opt=opt, ,scope=["h",h, "h2",h2,"h3",h3]
	)
);


//==================================================
ftin2in=["ftin2in","ftin","number","Unit, len",
"Given a ftin ( could be a string like 3`6``, or an array like [3,6], 
;; or a number 3.5, all for 3 feet 6 in ), convert to inches.
;;
;;   ftin2in( 3.5 )= 42
;;   ftin2in( [3,6] )= 42
;;   ftin2in( 3`6`` )= 42
;;   ftin2in( 8` )= 96
;;   ftin2in( 6`` )= 6
;;
"];

function ftin2in_test( mode=MODE, opt=[] )=(//====================
	doctest2( ftin2in,
	[
	  [ ftin2in(3.5), 42, "3.5",  ]
	 ,[ ftin2in([3,6]), 42, "[3,6]",  ]
	, [ ftin2in("3'6\""), 42, "3`6'" ]
	, [ ftin2in("8'"), 96, "8'" ]
	, [ ftin2in("6\""), 6, "6'" ]
	], mode=mode, opt=opt
	)
);

//==================================================
get=["get", "o,i,cycle=false", "item|arr", "Index, Inspect, Array, String" ,
 " Given a str or arr (o), an index (i), get the i-th item. 
;; i could be negative.
;;
;; New argument *cycle*: cycling from the other end if out of range. This is
;; useful when looping through the boundary points of a shape.
;;
;;   arr= [20, 30, 40, 50]; 
;;   get( arr, 1 )= 30
;;   get( arr, -2 )= 40
;;   get( 'abcdef', -2 )= 'e'
;;   //New 2014.6.27: cycle
;;   get( arr, 4 )= undef
;;   get( arr, 4, cycle=true )= 20
;;   get( arr, 10 )= undef
;;   get( arr, 10, cycle=true )= 40
;;   get( arr, -5 )= undef
;;   get( arr, -5, cycle=true )= 50
;;   // New 2015.6.20: i could be array
;;   get(arr,[0,-1])= [20, 50]
"];

function get_test( mode=MODE, opt=[] )=
(
	let( arr = [20,30,40,50]
       , pts=[ [0,1,2],[3,4,5],[6,7,8]]
       )

	doctest2( get,
    [ 
    "## array ##"
    , "var arr"
    , [ get( arr, 1 ), 30, "arr, 1" ]
    , [ get(arr, -1), 50, "arr, -1", ]
    , [ get(arr, -2), 40, "arr, -2" ]
    , [ get(arr, true), undef, "arr, true" ]
    , [ get([], 1), undef, "[], 1" ]
    , [ get([[2,3],[4,5],[6,7]],-2), [4,5], "[[2,3],[4,5],[6,7]], -2" ]
    , "## string ##"
    , [ get("abcdef", -2), "e", "'abcdef', -2" ]
    , [ get("abcdef", false), undef, "'aabcdef', false" ]
    , [ get("", 1), undef, "'', 1" ]
    , "//New 2014.6.27: cycle"
    , [ get(arr,4), undef, "arr, 4" ]
    , [ get(arr,4,cycle=true), 20, "arr, 4, cycle=true" ]
    , [ get(arr,10), undef, "arr, 10" ]
    , [ get(arr,10,cycle=true), 40, "arr, 10, cycle=true" ]
    , [ get(arr,-5), undef, "arr, -5" ]
    , [ get(arr,-5,cycle=true), 50, "arr, -5, cycle=true" ]
    ,""
    , "//New 2015.6.20: i could be an array. This replaces sel()"
    , [ get(arr, [0,-1]), [20,50], "arr,[0,-1]" ]
    , [ get(arr, [[0,-1],[1,-2]]), [[20,50],[30,40]], "arr,[[0,-1],[1,-2]]" ]
    ,""
    , "//New 2015.6.20: app"
    , [ get(arr, -1,app=100), [50,100], "arr, -1,app=100", ]
    , [ get(arr, -1,app=[60,70]), [50,60,70], "arr, -1,app=[60,70]", ]
    , [ get(arr, [0,-1],app=100), [20,50,100], "arr,[0,-1],app=100" ]
    , [ get(arr,[0,-1],app=[60,70]),[20,50,60,70],"arr,[0,-1],app=[60,70]" ]

    ,"//New: pp"
    , [ get(arr, -1,pp=100), [100,50], "arr, -1,pp=100", ]
    , [ get(arr, -1,pp=[60,70]), [60,70,50], "arr, -1,pp=[60,70]", ]
    , [ get(arr, [0,-1],pp=100), [100, 20,50], "arr,[0,-1],pp=100" ]
    , [ get(arr,[0,-1],pp=[60,70]),[60,70, 20,50],"arr,[0,-1],pp=[60,70]" ]
    ,""
    , [ get(arr, -1,pp="a",app="b"),["a",50,"b"],"arr,-1,pp='a',app='b'"]
    
    ,""
    ,"In the following cases, we get 2 pts, and append new pt(s) :"
    ,""
    ,"var pts"
    , [get(pts, [0,-1], app=[0,0,1]), [[0,1,2],[6,7,8],[0,0,1]]
       , "pts, [0,-1], app=[0,0,1]"]
    , [get(pts, [0,-1], app=[[0,0,1],[0,0,2]])
            , [[0,1,2],[6,7,8],[0,0,1],[0,0,2]]
       , "pts, [0,-1], app=[[0,0,1],[0,0,2]]"]
    
    ]
    , mode=mode, opt=opt,  scope=["arr", arr,"pts",pts]
	)
);

//echo( get_test( mode=12 )[1] );

//==================================================
get3=["get3", "o,i,cycle=false", "item", "Index, Inspect, Array, String" ,
 " Given a str or arr (o), an index (i), get the i-1, i, and i+1 items. 
;; i could be negative.
;;
"];

function get3_test( mode=MODE, opt=[] )=
(
	let( arr = [20,30,40,50] )

	doctest2( get3,
    [ 
    "## array ##"
    , "var arr"
    , [ get3( arr, 1 ), [20,30,40], "arr, 1" ]
    , [ get3( arr, 0 ), [50,20,30], "arr, 0" ]
    , [ get3( arr, -1 ), [40,50,20], "arr, -1" ]
    , "## string ##"
    , [ get3( "abcdef", 1 ), ["a","b","c"], "'abcdef', 1" ]
    , [ get3( "abcdef", 0 ), ["f","a","b"], "'abcdef', 0" ]
    , [ get3( "abcdef", -1 ), ["e","f","a"], "'abcdef', -1" ]

    ]
    , mode=mode, opt=opt,  scope=["arr", arr]
	)
);

//echo( get3_test( mode=12 )[1] );

//==================================================
getcol=[ "getcol","mm,i=0", "array", "Array, Index",
 " Given a matrix and index i, return a 1-d array containing
;; item i of each matrix row (i.e., the column i of the multi
;; matrix. The index i could be negative for reversed indexing.
"];

function getcol_test( mode=MODE, opt=[] )=
(
	let( mm= [[1,2,3],[3,4,5]]
       )
       
	doctest2( getcol,
    [
      [ getcol(mm), [1,3],"mm" ]
    , [ getcol(mm,2), [3,5], "mm,2" ]
    , [ getcol(mm,-2), [2,4], "mm,-2" ]
    ]
    , mode=mode, opt=opt, scope=["mm", mm ]
	)
);



//========================================
getdeci=["getdeci","n","num","Math",
"Return number of decimal points"
];

function getdeci_test( mode=MODE, opt=[] )=
(
    doctest2( getdeci,
    [
    ]
    , mode=mode, opt=opt
    )
);


//==================================================
getSideFaces=["getSideFaces","topIndices, botIndices, isclockwise=true","array", "Faces"
, "Given topIndices and botIndices (both are array
;; of indices), return an array of indices for the 
;; side faces arguments for the polyhedron() function
;; to create a column object (note that the top
;; and bottom faces are not included).
;; isclockwise: true if looking down from top to 
;; bottom, the order of indices is clockwise. Default
;; is true.
;; 
;;  clockwise (default)
;;       _2-_
;;    _-' |  '-_
;;   1_   |    '-3
;;   | '-_|   _-'| 
;;   |    '-0'   |
;;   |   _6_|    |
;;   |_-'   |'-_ | 
;;   5 _    |    7
;;     '-_  | _-'
;;        '-4' 
;;
;;  => [ [0,3,7,4], [1,0,4,5], [2,1,5,6],[3,2,6,7]]
;;
;;  clockwise = false
;;       _2-_
;;    _-' |  '-_
;;   3_   |    '-1
;;   | '-_|   _-'| 
;;   |    '-0'   |
;;   |   _6_|    |
;;   |_-'   |'-_ | 
;;   7 _    |   '5
;;     '-_  | _-'
;;        '-4' 
;;
;;  => [ [3,0,4,7], [2,3,7,6], [1,2,5,6],[0,1,5,4] ]
;; [[0, 1, 5, 4], [1, 2, 6, 5], [2, 3, 7, 6], [3, 0, 4, 7]] 
"];
function getSideFaces_test( mode=MODE, opt=[] )=
(
    let( d1=[ [0,1,2,3],[4,5,6,7]]
	   , d2=[ [6,7,8],[9,10,11]] 
       )
    
	doctest2( getSideFaces,
	[ 
      [getSideFaces(d1[0],d1[1]),
        [[0,3,7,4], [1,0,4,5], [2,1,5,6], [3,2,6,7]]
        ,"d1[0], d1[1]", ["nl",true]
      ]
     ,[getSideFaces(d1[0],d1[1],false),
        [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
        , "d1[0], d1[1], clockwise=false", ["nl",true]
      ]
     ,[getSideFaces(d2[0],d2[1]),
        [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
        , "d2[0], d2[1]", ["nl",true]
      ]
     ,[ getSideFaces(d2[0],d2[1],false),
        [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
       , "d2[0], d2[1], clockwise=false", ["nl",true]
      ]
	]
	, mode=mode, opt=opt
    , scope=["d1", d1, "d2",d2]
    )
);


//========================================================
gtype=["gtype","x,dim=3","pt|ln|pl|cir|ball|false", "Type,Inspect",
"Return what type of geometry shape x is. It could be one
;; of ('pt','ln','pl','cir','ball'), indicating that x is
;; a point, line, plane, circle, ball, resp. If none of them,
;; return undef. 
;;
;; They are determined by the criteria below:
;;
;;  pt: [i,j,k]
;;  ln: [p,q] and p!=q
;;  pl: [p,q,r] and p!=q!=r and not co-linear 
;;  cir: [ ln,r] or [r,ln] in 3D
;;       or [pt,r] or [r,pt] in 2D
;;  ball: [ pt,r] or [r,pt] in 3D
;;  
;; dim : dimension. Default=3 
"];

function gtype_test( mode=MODE, opt=[] )=
(
    let( P=[2,3,4]
       , Q=[5,6,7]
       , R=[3,5,7]
       )
    
    doctest2( gtype,
    [
      [gtype( P ), "pt", "P"]
    , [gtype( [P,Q] ), "ln", "[P,Q]"]
    , [gtype( [P,Q,R] ), "pl", "[P,Q,R]"]
    , [gtype( "a" ), undef, "'a'"]
    , [gtype( [P,Q,R,P] ), undef, "[P,Q,R,P]"]
    , [gtype( [[P,Q],1] ), "cir", "[[P,Q],1]"]
    , [gtype( [2,[P,Q]] ), "cir", "[2,[P,Q]]"]
    , [gtype( [P,1] ), "ball", "[P,1]"]
    , [gtype( [0.5,P] ), "ball", "[0.5,P]"]
    
    ], mode=mode, opt=opt, scope=["P",P,"Q",Q,"R",R]
    )
);
//echo( gtype_test(mode=112)[1] );

//========================================================
subopt=["subopt","u_opt,df_opt,df_subs,com_key","arr","core",
"Set child component opt.
;; 
;; u_opt:  user input opt  
;; df_opt: Default obj opt set at the design time, have
;;           settings for show/hide of group and members 
;;      
;;           [ 'r',1          // general option
;;           , 'arms', true   // Show all arms
;;           , 'armL', false  // But hide armL
;;           ]  
;;                  
;;         Note that only true|false are allowed. Or you 
;;         can skip it to let the other settings decide.
;;               
;; df_subs: A hash of two pairs showing both group and member 
;;          defaults: group first, and member the 2nd:
;;                    
;;           [ group_name, df_group, memb_name, df_memb]
;;                 
;;          Example: [ 'arms', df_arms, 'armL', df_armL ] 
;;
;; com_keys: names of the settings that children inherited  
;;           from the root (main) opt, like ['color','transp']
"
];


module subopt_test( mode=MODE, opt=[] ){
    echom("subopt_test");
    opt=[];
    df_arms=["len",3,"r",1];
    df_armL=["fn",5];
    df_armR=[];
    df_opt= [ "len",2
            , "arms", true
            , "armL", true
            , "armR", true
            ];
    com_keys= ["fn"];
    
    /*
        setopt: To simulate obj( opt=[...] )
        args  : opt  
                // In real use, following args are set inside obj()
                df_opt
                df_subopt // like ["arms",[...], "armL",[...]] 
                com_keys 
                objname   // like "obj","armL","armR"          
                propname  // like "len"
    */
    function setopt(   
            opt=opt
            , df_opt=df_opt
            , df_subopt=[ "arms",df_arms, "armL",df_armL,"armR",df_armR]
            , com_keys = com_keys
            
            //
            // The following two are for this testing only.
            //
            , objname ="obj" // what opt you want to test
                             //  "obj"   : _opt
                             //  "df_opt": df_opt
                             //  "armL"  : opt_armL
                             //  "armR"  : opt_armR
            , propname=undef // If not set, return entire opt indicated
                             //  with objname; if set, find its value
                             
            )=
        let(
             _opt   = update( df_opt, opt )
           , df_arms= hash(df_subopt, "arms")  
           , df_armL= hash(df_subopt, "armL")  
           , df_armR= hash(df_subopt, "armR")  
           , opt_armL= subopt( _opt
                             , df_opt = df_opt
                             , sub_dfs= ["arms", df_arms, "armL", df_armL ]                         , com_keys= com_keys
                             )
           , opt_armR= subopt( _opt
                             , df_opt = df_opt
                             , sub_dfs= ["arms", df_arms, "armR", df_armR ]
                             , com_keys= com_keys
                             )
           , opt= objname=="obj"?_opt
                    :objname=="df_opt"?df_opt
                    :objname=="armL"?opt_armL
                    :opt_armR
            )
        // ["opt",_opt,"df_arms", df_arms, "opt_armL",opt_armL, "opt_armR",opt_armR];
        propname==undef? opt:hash(opt, propname);
      
     /**
        Debugging Arrow(), in which a getsubopt call results in the warning:
        DEPRECATED: Using ranges of the form [begin:end] with begin value
        greater than the end value is deprecated.
            
     */       
     function debug_Arrow(opt=["r", 0.1, "heads", false, "color","red"])=
        let (
        
           df_opt=[ "r",0.02
               , "fn", $fn
               , "heads",true
               , "headP",true
               , "headQ",true
               , "debug",false
               , "twist", 0
               ]
          ,_opt= update(df_opt,opt) 
          ,df_heads= ["r", 1, "len", 2, "fn",$fn]    
          , opt_headP=subopt( _opt
                      , df_opt=df_opt
                      , com_keys= [ "color","transp","fn"]
                      , sub_dfs= ["heads", df_heads, "headP", [] ]
                ) 
        )
        opt_headP;
        
     rtn= doctest2(subopt
        , [ ""
            ,"In above settings, opt is the user input, the rest are"
            ,"set at design time. With these:"
            ,""
            ,"// The default opt:"     
            ,[setopt(objname="df_opt")
                 , ["len",2,"arms",true,"armL", true, "armR", true]
                 ,"df_opt"
                 ]

            ,"// The object level (root) opt:"
            ,[setopt(objname="obj", opt=["color","red"])
                 , ["len",2,"arms",true,"armL", true, "armR", true, "color","red"]
                 ,"obj, opt=['color','red']"
                 ]
            ,[setopt()
                 , ["len",2,"arms",true,"armL", true, "armR", true]
                 , "opt"
                 ]
            ,[ setopt(objname="armL")
                 , ["len", 3, "r", 1, "fn", 5]
                 , "opt_armL"
                 ]
            ,[ setopt(objname="armR")
                 , [ "len", 3, "r", 1]
                 , "opt_armR"
                 ]
//                            
//            ,""
//            ,"Set r=4 at the obj level. Since r doesn't show up in com_keys, "
//            ,"it only applies to obj, but not either arm. "
//            ,""
//            
//            ,[ setopt(objname="df_opt")
//                 , ["len",2,"arms",true,"armL", true, "armR", true]
//                 , "df_opt"
//                 ]
//            ,[ setopt(opt=["r",4])
//                 , ["len",2,"arms",true,"armL", true, "armR", true,"r",4]
//                 , "opt"
//                 ]
//            ,[ setopt(opt=["r",4],objname="armL")
//                 , ["len", 3, "r", 1,"fn", 5]
//                 , "opt_armL"
//                 ]
//            ,[ setopt(opt=["r",4],objname="armR")
//                 , [ "len", 3, "r", 1]
//                 , "opt_armR"
//                 ]
//                            
//            ,""
//            ,"Set fn=10 at the obj level. Since fn IS in com_keys, "
//            ,"it applies to obj and both arms. "
//            ,""
//            
//            ,[ setopt(objname="df_opt")
//                 , ["len",2,"arms",true,"armL", true, "armR", true]
//                 , "df_opt"
//                 ]
//            ,[ setopt(opt=["fn",10])
//                 , ["len",2,"arms",true,"armL", true, "armR", true, "fn",10]
//                 , "opt"
//                 ]
//            ,[ setopt(opt=["fn",10],objname="armL")
//                 , ["len", 3, "r", 1, "fn", 10]
//                 , "opt_armL"
//                 ]
//            ,[ setopt(opt=["fn",10],objname="armR")
//                 , ["len", 3, "r", 1, "fn", 10]
//                 , "opt_armR"
//                 ]
//        
//            ,""
//            ,"Disable armL:"
//            ,""
//            
//            ,[ setopt(objname="df_opt")
//                 , ["len",2,"arms",true,"armL", true, "armR", true]
//                 , "df_opt"
//                 ]
//            ,[ setopt(opt=["armL",false])
//                 , ["len",2,"arms",true,"armL", false, "armR", true]
//                 , "opt"
//                 ]
//            ,[ setopt(opt=["armL",false],objname="armL")
//                 , false
//                 , "opt_armL"
//                 ]
//            ,[ setopt(opt=["armL",false],objname="armR")
//                 , ["len", 3, "r", 1]
//                 , "opt_armR"
//                 ]
//
//
//            ,""
//            ,"Disable both arms:"
//            ,""
//            
//            ,[ setopt(objname="df_opt")
//                 , ["len",2,"arms",true,"armL", true, "armR", true]
//                 , "df_opt"
//                 ]
//            ,[ setopt(opt=["arms",false])
//                 , ["len",2,"arms",false,"armL", true, "armR", true]
//                 , "opt"
//                 ]
//            ,[ setopt(opt=["arms",false],objname="armL")
//                 , false
//                 , "opt_armL"
//                 ]
//            ,[ setopt(opt=["arms",false],objname="armR")
//                 , false
//                 , "opt_armR"
//                 ]
        
        
//            ,""
//            ,"Debugging Arrow(), in which a getsubopt call results in the warning:"
//            ,"DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated."
//            ,""
//            
//            ,["debug_Arrow(opt=['r', 0.1, 'heads', false, 'color','red']"
//             , debug_Arrow(opt=["r", 0.1, "heads", false, "color","red"]),[]
//             ]
                            
          ]
          
        , mode=mode, opt=opt, scope= [ "df_opt",df_opt, 
               , "df_arms", df_arms
               , "df_armL", df_armL
               , "df_armR", df_armR
               , "com_keys", com_keys
               , "opt",opt
               ]
    );
    //echo("$$$");
    echo( rtn[1] );
}

//subopt_test(mode=112);


//==================================================
function getTubeSideFaces_test( mode=MODE, opt=[] )=
( 
    let( d1=[ [6,7,8,9],[10,11,12,13],[14,15,16,17] ] )
    
	doctest2(getTubeSideFaces,
	[ 
      "var d1"
    , [ getTubeSideFaces(d1),
        [[[ 6, 9,13,10], [ 7, 6,10,11]
         ,[ 8, 7,11,12], [ 9, 8,12,13]]
        ,[[10,13,17,14], [11,10,14,15]
         ,[12,11,15,16], [13,12,16,17]]
        ], "d1", ["ml",2]
      ]
//     ,["d1[0], d1[1], clockwise=false"
//        , getSideFaces(d1[0],d1[1],false),
//        [[0,1,5,4], [1,2,6,5], [2,3,7,6], [3,0,4,7]]
//      ]
//     ,["d2[0], d2[1]", getSideFaces(d2[0],d2[1]),
//        [[6,8,11,9], [7,6,9,10], [8,7,10,11]]
//      ]
//     ,["d2[0], d2[1], clockwise=false"
//        , getSideFaces(d2[0],d2[1],false),
//        [[6,7,10,9], [7,8,11,10], [8,6,9,11]]
//      ]
	]
	,mode=mode, opt=opt, scope=["d1", d1])
);



//==================================================
_h= [ "_h", "s,h, showkey=false, wrap='{,}'", "string", "Hash, Doc",
"
 Given a string (s), a hash (h) and a wrapper string like {,},
;; replace the {key} found in s with the val where [key,val] is
;; in h. If showkey=true, replace {key} with key=val. This
;; is useful when echoing a hash:
;;
;; _h( '{w},{h}', ['h',2,'w',3])=> '3,2'
;; _h( '{w},{h}', ['h',2,'w',3], true)=> 'w=3,h=2'
"];

function _h_test( mode=MODE, opt=[] )=
(
	let( data=["m",3,"d",6]
       , tmpl= "2014.{m}.{d}"
       )
	doctest2( _h, 
    [
      "var data"
    , [ _h("2014.{m}.{d}",  data)
        , "2014.3.6"
        ,str( tmpl, ", ", data )
      ]
    , "// showkey=true :"
    , [
        _h("2014.[m].[d]",  data, true, "[,]")
        , "2014.m=3.d=6"
        , "'2014.[m].[d]', data, showkey=true, wrap='[,]'"
      ]
    , "// hash has less key:val " 
    , [
        _h("2014.{m}.{d}", ["m",3])
        , "2014.3.{d}"
        , "tmpl, ['m',3]"
      ]
    , "// hash has more key:val " 
    , [
        _h("2014.{m}.{d}", ["a",1,"m",3,"d",6] )
        , "2014.3.6"
        , "tmpl, ['a',1,'m',3,'d',6]"
      ]	
    ], mode=mode, opt=opt , scope=["tmpl", tmpl, "data", data]
	)
);

//echo( _h_test( mode=112)[1] );

//==================================================
has=["has","o,x","T|F","String, Array, Inspect",
" Given an array or string (o) and x, return true if o 
;; contains x. 
;;
;;   has( 'abcdef','d' )= true
;;   has( 'abcdef','m' )= false
;;   has( [2,3,4,5],3 )= true
;;   has( 'abcdef',['d','y'] )= true 
;;
;; 2015.3.11: non-arr and non-str gets false
;;
;;   has(false,...) = false   
;;   has(3,...) = false   
"];

function has_test( mode=MODE, opt=[] )=
(
	doctest2( has,
	[ 
      [has("abcdef","d"),true, "'abcdef','d'"]
	, [has("abcdef","m"),false, "'abcdef','m'"]
	, [has([2,3,4,5],3),true, "[2,3,4,5],3"]	
	, [has([2,3,4,5],6),false, "[2,3,4,5],6"]	
	, [has([2,3,4,5],undef),false, "[2,3,4,5],undef"]
    , ""
    , "// New 2015.2.16: x could be an array, and return true if has any"
    , ""
    , [has("abcdef",["d","y"]),true, "'abcdef',['d','y']"]
	, [has("abcdef",["m","y"]),false,"'abcdef',['m','y']"]
	, [has([2,3,4,5],[3,9]),true,"[2,3,4,5],[3,9]"]	
	, [has([2,3,4,5],[6,9]),false,"[2,3,4,5],[6,9]"]	
    ]
	,mode=mode, opt=opt )
);	

//==================================================

hash=[ "hash", "h,k, notfound=undef", "Any","Hash",
" Given an array (h, arranged like h=[k,v,k2,v2 ...] to serve
;; as a hash) and a key (k), return a v from h: hash(h,k)=v.
;; If key not found, or value not found (means k is the last item
;; in h), return notfound.
;;
;; New 2015.4.20: use list comprehension (instead of recursion)
"];

function hash_test( mode=MODE, opt=[] )=
(
//	let(h= 	[ -50, 20 
//			, "xy", [0,1] 
//			,[0,1],10
//			, true, 1 
//            , "c",true
//			]
//       ,h2= "c;xy=[0,1];true=1"
//       ,h3= ["c;xy=[0,1]", -50,20
//       )

    let( h1= ["c",true, "a",30, "xy",[0,1], "r",3 ] 
       , h2= ["c;a=30;xy=[0,1]","r",3]
       , h3= "c;a=30;xy=[0,1];r=3"
       , h4= "c|a=30|xy=[0,1]|r=3"
       , h5= "a=3;b=B;c=3"
       )
	doctest2( hash,  
    [ 
      "", "var h1"
    , ""
    , [ hash(h1, "c"), true, "h1,'c'" ]
    , [ hash(h1, "xy"), [0,1], "h1,'xy'"  ]
    , [ hash(h1, "b"), undef, "h1,'b'"  ]
    , [ hash(h1, "b",notfound=3), 3, "h1,'b',notfound=3"  ]
    , str( "//Same as: udf(hash(h1,'b'),3)= ", udf(hash(h1,"b"),3) )
    ,"","//Return h1 when key not given:"
    , [ hash(h1), h1, "h1"  ]
    
    
    , "", "// The 1st item is a packed hash called <b>sopt</b>:"
    , "var h2 "
    , ""  
    , [ hash(h2, "c"), true, "h2,'c'" ]
    , [ hash(h2, "xy"), [0,1], "h2,'xy'"  ]
    , [ hash(h2, "b"), undef, "h2,'b'"  ]
    , [ hash(h2, "b",notfound=3), 3, "h2,'b',notfound=3"  ]
    , [ hash(h2), h1, "h2"  ]
    
    , "", "// Entire hash is a <b>sopt</b>:"
    , "var h3 "
    , ""  
    , [ hash(h3, "c"), true, "h3,'c'" ]
    , [ hash(h3, "xy"), [0,1], "h3,'xy'"  ]
    , [ hash(h3, "b"), undef, "h3,'b'"  ]
    , [ hash(h3, "b",notfound=3), 3, "h3,'b',notfound=3"  ]
    , [ hash(h3), h1, "h3"  ]
    
    , "", "// Change the sp to '|':"
    , "var h4 "
    , ""  
    , [ hash(h4, "c", sp="|"), true, "h4,'c',sp='|'" ]
    , [ hash(h4, "xy", sp="|"), [0,1], "h4,'xy',sp='|'"  ]
    , [ hash(h4, "b", sp="|"), undef, "h4,'b',sp='|'"  ]
    , [ hash(h4, "b",notfound=3, sp="|"), 3, "h4,'b',notfound=3,sp='|'"  ]
    , [ hash(h4, sp="|"), h1, "h4,sp='|'"  ]
    
//    ,""
//    ,"var h5"
//    ,""
//    , [ hash(h5,"a"), 3, "h5,'a'"]
//    , [ hash(h5,"b"), "B", "h5,'b'"]
    
    
    
//    , [ hash(h, 2.3), 5, "h,2.3" ]
//    , [ hash(h, [0,1]), 10, "h,[0,1]" ]
//    , [ hash(h, true), 1, "h,true" ]
//    , [ hash(h, 1), false, "h,1" ]
//    , [ hash(h, "xx"), undef, "h,'xx'" ]
//
//    , "## key found but it's in the last item without value:"
//    , [ hash(h, -5), undef, "h, -5" ]
//    , "## notfound is given: "
//    , [ hash(h, -5,"df"), "df", "h,-5,'df'", ["//", "value not found"] ]
//    , [ hash(h, -50,"df"), 20, "h,-50,'df'" ]
//    , [ hash(h, -100,"df"), "df", "h,-100,'df'" ]
//    , "## Other cases:"
//    , [ hash([], -100), undef, "[],-100" ]
//    , [ hash(true, -100), undef, "true,-100" ]
//    , [ hash([ [undef,10]], undef), undef, "[[undef,10]],undef" ]
//    , " "
//    , "## Note below: you can ask for a default upon a non-hash !!"
//    , [ hash(true, -100, "df"), "df", "true,-100,'df'"]
//    , [ hash(true, -100), undef, "true,-100 "]
//    , [ hash(3, -100, "df"), "df", "3,-100,'df'"]
//    , [ hash("test", -100, "df"), "df", "'test',-100,'df'"]
//    , [ hash(false, 5, "df"), "df", "false,5,'df'"]
//    , [ hash(0, 5, "df"), "df", "0,5,'df'" ]
//    , [ hash(undef, 5, "df"), "df", "undef,5,'df'" ]
//    , "  "
//    , "## New 20140528: if_v, then_v, else_v "
//    , ""
//    , str("> h= ", _fmth(h, eq=", ", iskeyfmt=true))
//    , ""
//    , [ hash(h, 2.3, if_v=5, then_v="got_5"), "got_5", "h,2.3,if_v=5, then_v='got_5'" ]
//    , [ hash(h, 2.3, if_v=6, then_v="got_5"), 5, "h,2.3,if_v=6, then_v='got_5'" ]
//    , [ hash(h, 2.3, if_v=6, then_v="got_5", else_v="not_5"), "not_5", "h, 2.3, if_v=6, then_v='got_5', else_v='not_5'"
//             ]
//    ,"  "
//    ,"## New 20140730: You can use string for single-letter hash:"
//    ,[hash("a1b2c3","b"), "2", "'a1b2c3','b'" ]
//    ,[hash("aAbBcC","b"), "B", "'aAbBcC','b'" ]
    //           ," "
    //           ,"## 20150108: test concat "
    //           ,["['a',3,'b',4,'a',5]", hash(["a",3,"b",4,"a",5],"a"), 3] 
    ], mode=mode, opt=opt
        , scope=["h1",h1,"h2",h2,"h3",h3,"h4",h4,"h5",h5]
    )
);

//echo( hash_test( mode=12 )[1] );

//==================================================

//hash2=[ "hash2", "h,k, notfound=undef", "Any","hash2",
//" Given an array (h, arranged like h=[k,v,k2,v2 ...] to serve
//;; as a hash2) and a key (k), return a v from h: hash2(h,k)=v.
//;; If key not found, or value not found (means k is the last item
//;; in h), return notfound.
//;;
//;; New 2015.4.20: use list comprehension (instead of recursion)
//"];
//
//function hash2_test( mode=MODE, opt=[] )=
//(
//	let(h= 	[ "xy", [0,1], "color","red"]
//       ,h2= [ "c,r=1", "xy", [0,1], "clr","red"]
//	   ,h3= "c,r=1,xy=[0,1],clr=red"
//       )
// c|d|x=[2,3]| c;d;e;x=3 c|dc,d,x,/ x/c/d/e=3/
// 
// "c,d,e=3,color=red,r=0.5"
// "c;d;e=3;color=red;r=0.5"
// "c/d/e=3/color=red/r=0.5" 
// 
// 
// 
// 
//	doctest2( hash2,  
//    [ 
//      "var h"//,"var h2","var h3"
//    , ""
//    , [ hash2(h, "xy"), [0,1], "h,'xy'"  ]
//    , [ hash2(h, "color"), "red", "h,'color'" ]
//    
//    , "", "var h2",""
//    
//    , [ hash2(h2, "c"), true, "h2,'c'" ]
//    , [ hash2(h2, "r"), 1, "h2,'r'" ]
//    , [ hash2(h2, "clr"), "red", "h2,'clr'" ]
//    
//    , "", "var h3",""
//    
//    , [ hash2(h3, "c", sp="|"), true, "h3,'c'" ]
//    , [ hash2(h3, "r"), 1, "h3,'r'" ]
//    , [ hash2(h3, "clr"), "red", "h3,'clr'" ]
//    
//    , "## key found but it's in the last item without value:"
//    , [ hash2(h, -5), undef, "h, -5" ]
//    , "## notfound is given: "
//    , [ hash2(h, -5,"df"), "df", "h,-5,'df'", ["//", "value not found"] ]
//    , [ hash2(h, -50,"df"), 20, "h,-50,'df'" ]
//    , [ hash2(h, -100,"df"), "df", "h,-100,'df'" ]
//    , "## Other cases:"
//    , [ hash2([], -100), undef, "[],-100" ]
//    , [ hash2(true, -100), undef, "true,-100" ]
//    , [ hash2([ [undef,10]], undef), undef, "[[undef,10]],undef" ]
//    , " "
//    , "## Note below: you can ask for a default upon a non-hash2 !!"
//    , [ hash2(true, -100, "df"), "df", "true,-100,'df'"]
//    , [ hash2(true, -100), undef, "true,-100 "]
//    , [ hash2(3, -100, "df"), "df", "3,-100,'df'"]
//    , [ hash2("test", -100, "df"), "df", "'test',-100,'df'"]
//    , [ hash2(false, 5, "df"), "df", "false,5,'df'"]
//    , [ hash2(0, 5, "df"), "df", "0,5,'df'" ]
//    , [ hash2(undef, 5, "df"), "df", "undef,5,'df'" ]
//    , "  "
////    , "## New 20140528: if_v, then_v, else_v "
////    , ""
////    , str("> h= ", _fmth(h, eq=", ", iskeyfmt=true))
////    , ""
////    , [ hash2(h, 2.3, if_v=5, then_v="got_5"), "got_5", "h,2.3,if_v=5, then_v='got_5'" ]
////    , [ hash2(h, 2.3, if_v=6, then_v="got_5"), 5, "h,2.3,if_v=6, then_v='got_5'" ]
////    , [ hash2(h, 2.3, if_v=6, then_v="got_5", else_v="not_5"), "not_5", "h, 2.3, if_v=6, then_v='got_5', else_v='not_5'"
////             ]
//    ,"  "
//    ,"## New 20140730: You can use string for single-letter hash2:"
//    ,[hash2("a1b2c3","b"), "2", "'a1b2c3','b'" ]
//    ,[hash2("aAbBcC","b"), "B", "'aAbBcC','b'" ]
//    //           ," "
//    //           ,"## 20150108: test concat "
//    //           ,["['a',3,'b',4,'a',5]", hash2(["a",3,"b",4,"a",5],"a"), 3] 
//    ], mode=mode, opt=opt, scope=["h",h,"h2",h2,"h3",h3]
//    )
//);
//
//echo( hash2_test( mode=112)[1] );

//==================================================
hashex= ["hashex", "h,keys", "hash", "Hash",
"Extract a sub hash from a hash based on given keys"
];
function hashex_test( mode=MODE, opt=[] )=
(
    //_mdoc("hashex_test","");
    let( h=	[ -50, 20 
			, "xy", [0,1] 
			, 2.3, 5 
			] 
       )
    doctest2( hashex,
    [ 
      str("> h= ", _fmth(h, eq=", ", iskeyfmt=true))
    , ""
    , [ hashex(h, ["xy",2.3]), ["xy", [0,1], 2.3, 5] 
       , "h, ['xy',2.3]"
      ]
    ]
    , mode=mode, opt=opt )
);

//==================================================
hashkvs=["hashkvs", "h", "array of arrays", "Hash",
"
 Given a hash (h), return [[k1,v1], [k2,v2] ...].
"];

function hashkvs_test( mode=MODE, opt=[] )=
(
	let( h = ["a",1,  "b",2, 3,33] )

	doctest2( hashkvs,
	[ 
      str("h= ", _fmth(h))
    , [ hashkvs(h), [["a",1],["b",2],[3,33]], h ]
	]
    , mode=mode, opt=opt
	)
);

//==================================================
haskey=[ "haskey", "h,k", "T|F", "Hash, Inspect",
"
 Check if the hash (h) has a key (k)
"];
 
function haskey_test( mode=MODE, opt=[] )=
(
	let( table = [ -50, 20 
			, "80", 25 
			, 2.3, "yes" 
			,[0,1],5
			, true, "YES" 
			] )
            
	doctest2( haskey, 
    [ 
      str( "table= ", _fmth(table))
    , [ haskey(table, -50), true, "table, -50" ]
    , [ haskey(table, "80"), true, "table, '80'" ]
    , [ haskey(table, "xy"), false, "table, 'xy'" ]
    , [ haskey(table, 2.3), true, "table, 2.3" ]
    , [ haskey(table, [0,1]), true, "table, [0,1]" ]
    , [ haskey(table, true), true, "table, true" ]
    , [ haskey(table, -100), false, "table, -100" ]
    , [ haskey([], -100), false, "[], -100" ]
    , [ haskey( [undef,1], undef), true, "[[undef,1], undef", 
                 , ["//","# NOTE THIS!"] ]
    ],mode=mode, opt=opt//, scope=["table", table]
	)
);

//==================================================
incenterPt=["incenterPt", "pqr", "point", "Angle, Point"
," Return the incenter point (all 3 angle bisector lines meet)."
];

function incenterPt_test(mode=MODE, opt=[] )=
(
    doctest2(incenterPt, mode=mode, opt=opt )
);
        
//==================================================
idx=["idx", "o,x", "int", "Index,String",
"Return the index of x in o (str|arr), or -1 if not found.
;; 
;; Compare to index:
;; 
;;  Unlike index, idx does`t use slice() and join() for each 
;;  possible char check in s. Should be faster than index when 
;;  x is a word but not a single char.
"
,"index"
];

function idx_test( mode=MODE, opt=[] )=
(
    //let( s= "I_am_the_findw"
    //   , arr=["xy","xz","yz"]
    //   ) 

	doctest2(idx,  
	[ 
      //"var s","var arr"
   // , ""    
   // , 
    "//## Array indexing ##"
    , [ idx(["xy","xz","yz"],"xz"),  1, "arr, 'xz'" ]
    , [ idx(["xy","xz","yz"],"yz"),  2, "arr, 'yz'" ]
    , [ idx(["xy","xz","yz"],"xx"),  -1, "arr, 'xx'" ]
    , [ idx([],"xx"), -1, "[], 'xx'" ]
    , "//## String indexing ##"
    , [ idx( "I_am_the_findw","a"),  2, "s, 'a'" ]
    , [ idx(  "abcdef","f"),  5, "'abcdef','f'" ]
    , [ idx( "findw","n"),  2, "'findw','n'" ]
    , [ idx( "I_am_the_findw","am"),  2, "s, 'am'",  ]
    , [ idx( "I_am_the_findw","_the"),  4, "s, '_the'",  ]
    , [ idx( "I_am_the_findw","find"),  9, "s, 'find'" ]
    , [ idx( "I_am_the_findw","the"),  5,"s, 'the'",  ]
    , [ idx("I_am_the_ffindw","findw"),  10,"s, 'findw'" ]
    , [ idx("findw", "findw"),  0, "'findw', 'findw'" ]
    , [ idx( "findw","dwi"),  -1, "'findw', 'dwi'" ]
    , [ idx("I_am_the_findw","The"),  -1,"s, 'The'" ]
    , [ idx( "replace__text","tex"),  9, "s, 'tex'" ]
    , [ idx("replace__ttext","tex"), 10, "'replace__ttext', 'tex'" ]
    , [ idx( "replace_t_text","text"),  10, "'replace_t_text','tex'"]
    , [ idx("abc", "abcdef"),  -1, "'abc', 'abcdef'" ]
    , [ idx( "","abcdef"),  -1,"'', 'abcdef'" ]
    , [ idx( "abc",""),  -1, "'abc',''" ]

    ,"// ## Others ##"
    , [ idx([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
    , [ idx([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
    
    ]
    ,mode=mode, opt=opt//, scope=[ "s",s, "arr",arr]
         
	)
);

//echo(idx_test()[1]);

//==================================================
index=["index", "o,x", "int", "Index,Array,String",
" Return the index of x in o (a str|arr), or -1 if not found.
;;
;;   arr= ['xy', 'xz', 'yz']; 
;;   index( arr, 'yz' )= 2
;;   index( arr, 'xx' )= -1
;;
;;   index( 'I_am_the_findw', 'am' )= 2
","idx"
];	 

function index_test( mode=MODE, opt=[] )=
(
    //let( arr = ["xy","xz","yz"]
	//   , s= "I_am_the_findw"

	doctest2(index,  
	[ 
      "## Array indexing ##"
    , [ index(["xy","xz","yz"],"xz"),  1, "arr, 'xz'" ]
    , [ index(["xy","xz","yz"],"yz"),  2, "arr, 'yz'" ]
    , [ index(["xy","xz","yz"],"xx"),  -1, "arr, 'xx'" ]
    , [ index([],"xx"), -1, "[], 'xx'" ]
    , "## String indexing ##"
    , [ index( "I_am_the_findw","a"),  2, "s, 'a'" ]
    , [ index( "I_am_the_findw","am"),  2, "s, 'am'",  ]
    , [ index(  "abcdef","f"),  5, "'abcdef','f'" ]
    , [ index( "findw","n"),  2, "'findw','n'" ]
    , [ index( "I_am_the_findw","find"),  9, "s, 'find'" ]
    , [ index( "I_am_the_findw","the"),  5,"s, 'the'",  ]
    , [ index("I_am_the_ffindw","findw"),  10,"s, 'findw'" ]
    , [ index("findw", "findw"),  0, "'findw', 'findw'" ]
    , [ index( "findw","dwi"),  -1, "'findw', 'dwi'" ]
    , [ index("I_am_the_findw","The"),  -1,"s, 'The'" ]
    , [ index( "replace__text","tex"),  9, "s, 'tex'" ]
    , [ index("replace__ttext","tex"), 10, "'replace__ttext', 'tex'" ]
    , [ index( "replace_t_text","text"),  10, "'replace_t_text','tex'"]
    , [ index("abc", "abcdef"),  -1, "'abc', 'abcdef'" ]
    , [ index( "","abcdef"),  -1,"'', 'abcdef'" ]
    , [ index( "abc",""),  -1, "'abc',''" ]

    ,"## Others ##"
    , [ index([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
    , [ index([2,3, undef], undef), 2, "[2, ,3, undef], undef" ]
    
    ],mode=mode, opt=opt//, scope=["arr",arr, "s",s]
         
	)
);

//==================================================
inrange=[ "inrange", "o,i", "T|F", "Index, Array, String, Inspect",
" Check if index i is in range of o (a str|arr). i could be negative.
;;
;; ## string ##
;;   inrange( '789',2 )= true
;;   inrange( '789',-2 )= true
;;   inrange( '789',-4 )= false
;; ## array ##
;;   inrange( [7,8,9],2 )= true
;;   inrange( [7,8,9],-2 )= true
;;   inrange( [7,8,9],-4 )= false
"];

function inrange_test( mode=MODE, opt=[] )=
(
	doctest2( inrange,
	[
	  "## string ##"
    , [ inrange("789",2), true , "'789',2"]
    , [ inrange("789",3), false , "'789',3"]
    , [ inrange("789",-2), true , "'789',-2"]
    , [ inrange("789",-3), true , "'789',-3"]
    , [ inrange("789",-4), false , "'789',-4"]
    , [ inrange("",0), false , "',0"]
    , [ inrange("789","a"), false , "'789','a'"]
    , [ inrange("789",true), false , "'789',true"]
    , [ inrange("789",[3]), false , "'789',[3]"]
    , [ inrange("789",undef), false , "'789',undef"]
	, "## array ##"
    , [ inrange([7,8,9],2), true , "[7,8,9],2"]
    , [ inrange([7,8,9],3), false , "[7,8,9],3"]
    , [ inrange([7,8,9],-2), true , "[7,8,9],-2"]
    , [ inrange([7,8,9],-3), true , "[7,8,9],-3"]
    , [ inrange([7,8,9],-4), false , "[7,8,9],-4"]
    , [ inrange([],0), false , "[],0"]
    , [ inrange([7,8,9],"a"), false , "[7,8,9],'a'"]
    , [ inrange([7,8,9],true), false , "[7,8,9],true"]
    , [ inrange([7,8,9],[3]), false , "[7,8,9],[3]"]
    , [ inrange([7,8,9],undef), false , "[7,8,9],undef"]
	]
    , mode=mode, opt=opt
	)
);

//echo( inrange_test( mode=112)[1] );

//==================================================
int=[ "int", "x", "int", "Type, Number",
" Given x, convert x to integer. 
;;
;; int('-3.3') => -3
"];

function int_test( mode=MODE, opt=[] )=
(
	doctest2( int, 
    [ 
      [ int(3),3, "3"]
    , [ int(3.3),3, "3.3"]
    , [ int(-3.3),-3, "-3.3"]
    , [ int(3.8),4, "3.8"]
    , [ int(9.8),10, "9.8"]
    , [ int(-3.8),-4, "-3.8"]
    , [ int("-3.3"),-3, "'-3.3'"]
    , [ int("-3.8"),-3, "'-3.8'", ["//"," !!Note this!!"]]
    , [ int("3"),3, "'3'"]
    , [ int("3.0"),3, "'3.0'"]
    , [ int("0.3"),0, "'0.3'"]
    , [ int("34"),34, "'34'"]
    , [ int("-34"),-34, "'-34'"]
    , [ int("-34.5"),-34, "'-34.5'"]
    , [ int("345"),345, "'345'"]
    , [ int("3456"),3456, "'3456'"]
    , [ int("789987"),789987, "'789987'"]
    , [ int("789.987"),789, "'789.987'", ["//"," !!Note this!!"]]
    , [ int([3,4]),undef, "[3,4]"]
    , [ int("x"),undef, "'x'"]
    , [ int("34-5"),undef, "'34-5'"]
    , [ int("34x56"),undef, "'34x56'"]
	]
    ,mode=mode, opt=opt	
	)
);

////==================================================
//intersectPt=["intersectPt", "pq, target", "point", "Point"
//, " 
//
//"];
//
//function intersectPt_test( mode=MODE, opt=[] )=
//(
//	doctest2( intersectPt, mode=mode, opt=opt )
//);


//==================================================
is0=["is0","n","T|F","Inspect",
str( " Given a number, check if it is 'equivalent' to zero. The 
;; decision is made by comparing it to the constant, ZERO=", ZERO )
,"isequal"
];

function is0_test( mode=MODE, opt=[] )=
(
	doctest2( is0,
    [
      "// A value, 1e-15, is NOT zero:"
    , str("//   1e-15==0: ", 1e-15==0 )
    , "// but it's so small that it is equivalent to zero:"
    , [ is0(1e-15), true, "1e-15" ]
	]
    , mode=mode, opt=opt
	)
);

//==================================================
is90=["is90", "pq1,pq2", "T|F", "Inspect",
"Check if two lines form 90 degree angle. Don't have to be co-plane.
;;
;; New 20140613: can take a single 3-pointer, pqr:
;;
;;   is90( pqr );
;;
;; Same as angle( pqr )==90. Refer to : othoPt_test() for testing.
"];

function is90_test( mode=MODE, opt=[] )=
(
	let( pqr = randPts(3)           // 3 random points
	   , sqPts = squarePts(pqr)   //  This gives 4 points of a square, must be perpendicular
							  // The first 2 pts = pq[0], pq[1]. 	

	   , pq1 = [ pqr[0], pqr[1]   ]  // Points 0,1 of the square
	   , _pq2= [ pqr[1], sqPts[2] ]  // Points 1,2 of the square

	// Make a random translation to pq2x:
	   , t = randPt()
	   , pq2 = [ _pq2[0]+t, _pq2[1]+t ]
       )
       
	doctest2( is90,
	[
	  "// A 3-pointer, pqr, is randomly generated. It is then made to generate"
	 ,"// a square with sqarePts(pqr), in which side PQ is our pq1, and a side"
	 ,"// perpendicular to pq is translated to somewhere randomly and taken as pq2."
	,  [ is90(pq1,pq2), true, "pq1,pq2"  ]
	]
    ,mode=mode, opt=opt, scope=["pq1",pq1,"pq2",pq2]  
    )
);

//==================================================
isarr=[ "isarr","x", "T|F", "Type, Inspect, Array",
"
 Given x, return true if x is an array.
"];
function isarr_test( mode=MODE, opt=[] )=(//=========================
	doctest2( isarr, 
    [ [ isarr([1,2,3]), true, [1,2,3]]
    , [ isarr("a"), false, "'a'"]
    , [ isarr(true), false, "true"]
    , [ isarr(false), false, "false"]
    , [ isarr(undef), false, "undef"]
	],mode=mode, opt=opt
	)
);
 
//==================================================
isat=["isat","s,i,x","T|F","Inspect",
"Check if x is at index i of str s.
;; 
;; Compare to index() or idx():
;; 
;; s='abc+def+def'
;; 
;; idx(s,'def')=4
;; isat( s,4,'def')=true
;; isat( s,8,'def')=true : idx can`t reach this
;; 
;; x could be array:
;; isat('ab+cd+cd',4,['b+','d+'])= 'd+'
"
,"idx"
];

//==================================================
function isat_test( mode=MODE, opt=[] )=
(
    doctest2( isat,
    [
      [ isat("ab+cd+cd", 3, "cd"),true, "'ab+cd+cd',3,'cd'"]
    , [ isat("ab+cd+cd", 6, "cd"),true, "'ab+cd+cd',6,'cd'"]
    , [ isat("ab+cd+cd", 1, "cd"),false, "'ab+cd+cd',1,'cd'"]
    , [ isat("ab+cd+cd", 1, "b+"),true, "'ab+cd+cd',1,'b+'"]
    , [ isat("ab+cd+cd", 1, ["b+","d+"]),"b+", "'ab+cd+cd',1,['b+','d+']"]
    , [ isat("ab+cd+cd", 4, ["b+","d+"]),"d+", "'ab+cd+cd',4,['b+','d+']"]
    , [ isat("ab+cd+cd", 4, ["b","d"]),"d", "'ab+cd+cd',4,['b','d']"]
    , [ isat( "a+cos(2)+sin(5)", 2, ["cos","sin"])
      , "cos"
      , "'a+cos(2)+sin(5)', 2, ['cos','sin']"
      ] 
    ]
    ,mode=mode, opt=opt
    )
);
//echo( isat_test()[1] );



//==================================================
isbool=[ "isbool","x", "T|F", "Type, Inspect",
"
 Given x, return true if x is a boolean (true|false).
"];

function isbool_test( mode=MODE, opt=[] )=(//====================
	doctest2( isbool, 
    [ [ isbool([1,2,3]), false, [1,2,3]]
    , [ isbool("a"), false, "'a'"]
    , [ isbool(true), true, "true"]
    , [ isbool(false), true, "false"]
    , [ isbool(undef), false, "undef"]
    ],mode=mode, opt=opt
	)
);

//==================================================
iscoln=[ "iscoln","a,b", "T|F", "Geom, Inspect",
"Chk if pts in a and pts in b are all on the same line.
;; 
;; iscoln( [P,Q],R )
;;       ( [P,Q,R ... ] )
;;       ( P, [Q,R ...])
;;       ( [P,Q], [R,S ...])
" 
];

function iscoln_test( mode=MODE, opt=[] )=(//====================

    let( pts = onlinePts( randPts(2), lens= range(4)) //0,0.2,0.8))
       )
	doctest2( iscoln, 
    [ 
     str( ">> pts= <br/>", arrprn( pts, 2))
    , [ iscoln(pts), true, "pts"]
    
    , [ iscoln(slice(pts,2),slice(pts,0,2)), true
        , "slice(pts,2),slice(pts,0,2)"]
    
    , [ iscoln( pts[0], slice(pts,1)), true
        , "pts[0], slice(pts,1)"] 

    , [ iscoln( slice(pts,0,-1), last(pts)), true
        , "slice(pts,0,-1), last(pts)"] 

    , [ iscoln( pts, randPt()), false
        , "pts, randPt()"] 


    ],mode=mode, opt=opt, scope=["pts",pts]
	)
);

//echo( iscoln_test( mode=112 )[1] );

 
//==================================================
 isequal=["isequal", "a,b", "T|F", "Inspect",
_s(" Given 2 numbers a and b, return true if they are determined to
;; be the same value. This is needed because sometimes a calculation
;; that should return 0 ends up with something like 0.0000000000102.
;; So it fails to compare with 0.0000000000104 (another shoulda-been
;; 0) correctly. This function checks if a-b is smaller than the
;; predifined ZERO constant ({_}) and return true if it is.
", [ZERO])
];

function isequal_test( mode=MODE, opt=[] )=(//====================
	doctest2( isequal, 
	[ str("// ZERO = ",ZERO)
    , [ isequal(1e-15,0),true, "1e-15,0"]
    , [ isequal(1e-12,0),false, "1e-12,0"]
    , [ isequal(1e-9,1e-10),false, "1e-9,1e-10"]
    , [ isequal(1e-12,1e-13),false, "1e-12,1e-13"]
    , [ isequal(1e-14,1e-13),true, "1e-14,1e-13"]
    , [ isequal("1e-15",0),false, "'1e-15',0"]
    , [ isequal("1e-15","0"),false, "'1e-15','0'"]
    , [ isequal("abc","abc"),true, "'abc','abc'"]
    , [ isequal(undef,undef),true, "undef,undef"]
    , [ isequal(undef,false),false, "undef,false"]
	],mode=mode, opt=opt
	)
);


//==================================================
isfloat=[ "isfloat","x", "T|F","Type, Inspect, Number",
"
 Check if x is a float.
"];
function isfloat_test( mode=MODE, opt=[] )=(//====================
	doctest2(isfloat, 
	[ [ isfloat(3), false, "3"]
    , [ isfloat(3.2), true, "3.2"]
    , [ isfloat("a"), false, "'a'"]
	],mode=mode, opt=opt
	)
);

//==================================================
ishash = [ "ishash","x", "T|F", "hash,type, Inspect",
"
 Check if x **could** be a hash, i.e., an array with even number length.
"]; 
function ishash_test( mode=MODE, opt=[] )=(//====================
	doctest2(ishash, 
	[ [ ishash([1,2,3]), false, "[1,2,3]"]
    , [ ishash([1,2,3,4]), true, "[1,2,3,4]"]
    , [ ishash(3), false, "3"]
    , [ ishash(3.2), false, "3.2"]
    , [ ishash("a"), false, "'a'"]
	],mode=mode, opt=opt
	)
);

//==================================================
 isint = [ "isint","x", "T|F", "Type,Inspect, Number",
"
 Check if x is an integer.
"];
function isint_test( mode=MODE, opt=[] )=(//====================
	doctest2(isint, 
	[ [ isint(3), true, "3"]
    , [ isint(3.2), false, "3.2"]
    , [ isint("a"), false, "'a'"]
	],mode=mode, opt=opt
	)
);

//==================================================
isln=["isln","x,dim=3","T|F","Geo, Inspect"
,"Return true if x is a line. Default is3d=true"
];
 
function isln_test( mode=MODE, opt=[] )=(//=================
    let( P=[2,3,4]
       , Q=[5,6,7]
       , R=[3,4,5]
       )
    doctest2( isln,
    [ 
      [isln( P ), false, "P" ]
    , [isln( [P,Q] ), true, "[P,Q]" ]
    , [isln( [P,["a",2,3]] ), false, "[P,['a',2,3]]" ]
    , [isln( [P,P] ), false,  "[P,P]" ]
    , [isln( [P,Q,R] ), false, "[P,Q,R]" ]
    , [isln( [[2,3],[5,6]] ), false, [[2,3],[5,6]]]
    , [isln( [[2,3],[5,6]],2 ), true, "[[2,3],[5,6]],dim=2"]
    ]
    , mode=mode, opt=opt , scope=["P",P,"Q",Q,"R",R]
    )
);


//==================================================
isnum =[ "isnum","x", "T|F", "Type,Inspect,Number",
"
 Check if x is a number.
"];
function isnum_test( mode=MODE, opt=[] )=(//=========================
	doctest2(isnum, 
	[ 
      [ isnum(3), true, "3"]
    , [ isnum(3.2), true, "3.2"]
    , [ isnum("a"), false, "'a'"]
	]
    ,mode=mode, opt=opt
	)
);

//==================================================
 isOnPlane=["isOnPlane", "pqr,pt", "T|F", "Inspect, Plane",
"
 Given a 3-pointer (pqr) and a point (pt) ( to be converted to planecoefs,
;; = [a,b,c,k] or [a,b,c])), check if the pt is on
;; the plane:
;;
;;   a*pt.x + b*pt.y + c*pt.z + k == ZERO
"];

function isOnPlane_test( mode=MODE, opt=[] )=( //======================
	
    let( pqr    = randPts(3)
	   , pcoefs = planecoefs( pqr )
	   , othopt = othoPt( pqr )
	   , abipt  = angleBisectPt( pqr )
	   , midpt  = midPt( p01(pqr) )
       )
	doctest2( isOnPlane,
	[ 
      "// Randomly create pqr, and get its planecoefs, pcoefs."
    , "var pqr", "var pcoefs"
    , ""
    , "// From pqr, create othoPt, angleBisectPt and midPt of p,q."
    , "// They should all be on the same plane."
    , "var othopt", "var abipt", "var midpt"
    , [ isOnPlane( pqr,P(pqr) ), true, "pqr,P(pqr)" ]
    , [ isOnPlane( pqr, othopt ), true,  "pqr, othopt" ]
    , [ isOnPlane( newy(pqr,othopt),  othopt  ), true, "newy(pqr,othopt), othopt" ]
    , [ isOnPlane( pcoefs,othopt),  true, "pcoefs, othopt" ]
    , ""
    , "// angleBisectPt: "
    , [ isOnPlane( newy( pqr,abipt), abipt ),true, "newy(pqr,abipt), abipt" ]
    , [ isOnPlane( pcoefs, abipt ),  true, "pcoefs, abipt" ]
    , ""
    , "// Mixed:"
    , [ isOnPlane( [othopt, abipt, midpt] , pqr[2] ), true, 
       "[othopt, abipt, midpt], pqr[2]" ]
    , ""
    , "// normalPt:"
    , [ isOnPlane( pqr, N(pqr)), false, "pqr,N(pqr)"]
    
	], mode=mode, opt= opt //update(opt,["showscope",true])
       , scope= ["pqr", pqr
                , "pcoefs", pcoefs
                , "othopt", othopt
                , "abipt", abipt
                , "midpt", midpt]
	)
);
//echo( isOnPlane_test()[1] );

//========================================================
//projPt_demo();
isparal=["isparal", "a,b", "T|F", "Geo",
"Check if a and b are parallel. a, b could be line or plane. 
;;
;; They can be entered in either projPt(a,b) or 
;;  projPt( [a,b] ) form. For example:
;;
;; L  = [ [P,Q], [R,S] ]; 
;; PL = [ [P0,Q0,R0], [P1,Q1,R1], [P2,Q2,R2] ]; 
;; 
;; isparal( L, PL )  // or (PL,L)
;; isparal( [PL,L] ) // or ( [L,PL] )
"];

function isparal_test( mode=MODE, opt=[] )=
(
    doctest2( isparal, 
    [
    ]
    , mode=mode, opt=opt
    )
);

//==================================================
ispl=["ispl","x,dim=3","T|F","Geo, Inspect"
,"Return true if x is a plane. Default is3d=true"
];
function ispl_test( mode=MODE, opt=[] )=(//=================

    let( P=[2,3,4]
       , Q=[5,6,7]
       , R=[3,4,5]
       )
    doctest2( ispl,
    [ 
      [ ispl( P ), false, "P" ]
    , [ ispl( [P,Q] ), false, "[P,Q]" ]
    , [ ispl( [P,Q,R] ), true, "[P,Q,R]"]
    , [ ispl( [P,P,R] ), false, "[P,P,R]"]
    , [ ispl( [P,Q,P] ), false, "[P,Q,P]"]
    , [ ispl( [P,Q,Q] ), false, "[P,Q,Q]"]
    , [ ispl( [[2,3],[5,6],[7,8]] ), false, [[2,3],[5,6],[7,8]]]
    , [ ispl( [[2,3],[5,6],[7,8]],2 ), true, "[[2,3],[5,6],[7,8]],dim=2"]
    ]
    , mode=mode, opt=opt , scope=["P",P,"Q",Q,"R",R]
    )
);

//==================================================
ispt = ["ispt","x,dim=3","T|F","Geo, Inspect"
,"Return true if x is a point. Default is3d=true"
];

function ispt_test( mode=MODE, opt=[] )=( //===================

    doctest2( ispt,
    [ 
      [ ispt([2,3,4]), true, [2,3,4] ]
    , [ ispt([2,3,4,5]), false, [2,3,4,5] ]
    , [ ispt([2,3,"4"]), false, [2,3,"4"] ]
    , [ ispt([2,3]), false, [2,3] ]
    , [ ispt([2,3],2), true, "[2,3],dim=2" ]
    , [ ispt( 234), false, 234]
    , [ ispt( "234"), false, "'234'"]
    ],mode=mode, opt=opt
    )
);

//==================================================
isSameSide=["isSameSide", "pts,pqr", "T/F", "Geometry",
" Return true if all pts is on the side of plane pqr."];

function isSameSide_test( mode=MODE,ops=[])=
(
   doctest2( isSameSide,
   [
      
   ], mode=mode, ops=ops )
);
     

//==================================================
isstr= [ "isstr","x", "T|F", "Type,String,Inspect",
"
 Check if x is a string.
"];

function isstr_test( mode=MODE, opt=[] )=(//=====================

	doctest2( isstr, 
	[ 
      [ isstr(3), false, "3"]
    , [ isstr(3.2), false, "3.2"]
	, [ isstr("a"), true, "'a'"]
	],mode=mode, opt=opt
	)
);


   
   
//==================================================
istype=["istype","o,t","T|F", "type, inspect",
"
  Given a variable (o) and a typename (t), return true if 
;; o is type t. It can also check if o is one of multiple types
;; given, in which case, t could be entered as:
;;
;; istype(o, ['arr','str'] )
;; istype(o, 'arr,str')
;; istype(o, 'arr str')
"];

function istype_test( mode=MODE, opt=[] )=(//=====================

	doctest2( istype,
	[ 
      [ istype("3","str"), true, "'3','str'"]
    , [ istype("3",["str","arr"]), true, "'3',['str','arr']"]
    , [ istype(3,["str","arr"]), false, "3,['str','arr']"]
    , [ istype(3,["str","arr","int"]), true, "3,['str','arr','int']"]
    , [ istype(3,"str,arr,int"), true, "3,'str,arr,int'"]
    , [ istype(3,"str arr int"), true, "3,'str arr int'"]
    , [ istype(3,"str arr bool"), false, "3,'str arr bool'"]
	], mode=mode, opt=opt
	)
);

////==================================================
//join=[ "join", "arr, sp=\",\"" , "string","Array",
//" Join items of arr into a string, separated by sp.
//"];
//
//function join_test( mode=MODE, opt=[] )=(//=====================
//	doctest2( join, 
//	[ [ join([2,3,4]),      "234" , "[2,3,4]"]
//    , [ join([2,3,4], "|"), "2|3|4" , "[2,3,4], '|'"]
//    , [ join([2,[5,1],"a"], "/"), "2/[5, 1]/a" , "[2,[5,1],'a'], '/'"]
//    , [ join([], "|"), "" , "[], '|'"]
//	],mode=mode
//	)
//);
//
////==================================================
//joinarr=[ "joinarr", "arr" , "array","Array",
//" Given an array(arr), concat items in arr. See the difference below:
//;; 
//;;  concat( [1,2], [3,4] ) => [1,2,3,4] 
//;;  joinarr( [ [1,2],[3,4] ] ) => [1,2,3,4] 
//;;  join( [ [1,2],[3,4] ] ) => '[1,2],[3,4]' 
//"];
//
//function joinarr_test( mode=MODE, opt=[] )=(//=====================
//
//	doctest2( joinarr, 
//	[ 
//      [ joinarr([[2,3],[4,5]]), [2,3,4,5], "[[2,3],[4,5]]" ]
//    , ""
//    , "// 2015.2: you can also use the new run(): "
//    , ""
//    , [ run("concat",[[2,3],[4,5]]), [2,3,4,5],[[2,3],[4,5]],
//            ["myfunc", "run('concat',[[2,3],[4,5]])" ] ]
//    , ""
//    , "// 2015.4: or the new flatten(a,d=1): "
//    , ""
//    , [ flatten([[2,3],[4,5]]), [2,3,4,5], [[2,3],[4,5]],
//            ["myfunc", "flatten{_}" ] ]    
//	]
//    , mode=mode, opt=opt
//	)
//);
//
////echo( joinarr_test(mode=12)[1]);

//==================================================
keys=["keys", "h", "array", "Hash",
"
 Given a hash (h), return keys of h as an array.
"];

function keys_test( mode=MODE, opt=[] )=(//====================
    
	doctest2( keys, 
	[ 
      [ keys(["a",1,"b",2]), ["a","b"] , "['a',1,'b',2]"]
    , [ keys([1,2,3,4]), [1,3] , "[1,2,3,4]"]
    , [ keys([[1,2],0,3,4]), [[1,2],3] , "[1,2],0,3,4"]
    , [ keys([]), [] , "[]"]
	]
    , mode=mode, opt=opt
	)
);

//==================================================
kidx=["kidx", "h,k", "hash", "Array, Hash"
, "Return index of key k in hash h. It's the order of k
;; in keys(h).
;; 2015.1.30"
];

function kidx_test( mode=MODE, opt=[] )=(//====================
    
	doctest2( kidx, 
	[ 
	]
    , mode=mode, opt=opt
	)
);

//==================================================
last=[ "last", "o", "item", "Index, String, Array" ,
" Given a str or arr (o), return the last item of o.
"];
function last_test( mode=MODE, opt=[] )=(//==================
	doctest2( last, 
	 [ 
       [ last([20,30,40,50]), 50, [20,30,40,50] ]
     , [ last([]), undef,[] ]
     , [ last("abcdef"), "f", "'abcdef'" ]
     , [ last(""), undef,"''" ]
     ]
     , mode=mode, opt=opt
	)
);

//==================================================
lcase=["lcase", "s","str","String",
 " Convert all characters in the given string (s) to lower case. "
];
function lcase_test( mode=MODE, opt=[] )=(//=======================
	doctest2( lcase,
	[ 
      [lcase("aBC,Xyz"), "abc,xyz", "'aBCC,Xyz'"]
	]
    , mode=mode, opt=opt
	)
);


////==================================================
//lineCrossPt=["lineCrossPt", "P,Q,M,N","point", "Point",
//"
// Given 4 points (P,Q,M,N), return the point (R below) where extended
//;; lines of PQ and MN intesects.
//;;
//;;     M        
//;;      :       Q
//;;       :    -'
//;;        R-'
//;;      -' :
//;;    P'    N
//;;
//;;   M
//;;    : 
//;;     :
//;;      N      Q 
//;;           -'
//;;        R-'
//;;      -'
//;;    P'
//;;
//;; 2015.2.6: All the following 3 work:
//;;
//;;     lineCrossPt( P,Q,M,N )
//;;     lineCrossPt( pq, mn )
//;;     lineCrossPt( [pq,mn] )
//;;     lineCrossPt( pqmn )
//"];
//
//function lineCrossPt_test(mode=MODE, opt=[] )=
//( 
//    doctest2( lineCrossPt, mode=mode, opt=opt )
//);

////==================================================
//lineCrossPts=["lineCrossPts","pq,mn","2-pointer", "Geo",
//"Return 2-pointer, [U,V], for the shortest line segment between
//;; any two pts from line pq and mn. 
//;;
//;; The dist([U,V]) is the distance between lines pq,mn. 
//;; If U=V then pq,rs crosses;
//;;
//;; Ref: http://geomalgorithms.com/a07-_distance.html
//"];
//
//function lineCrossPts_test(mode=MODE, opt=[] )=
//( 
//    doctest2( lineCrossPts, mode=mode, opt=opt )
//);

//==================================================
linePts=["linePts","pq, len, ratio", "pq", "Point, Line",
 " Extend/Shrink line. Given a 2-pointer (pq) and len or ratio , return 
;; a new 2-pointer for a line with extended/shortened lengths. 
;;
;; Both len and ratio can be positive|negative. Positive= extend, negative=shrink.
;; 
;; They can be either 2-item array (like [1,0]) or a number. If a number, it's the
;; same as [number,number].
;;
;; If array [a,b], a,b for p, q side, respectively. For example, 
;;
;;   linePts( pq, len=-1): both sides shrink by 1
;;   linePts( pq, len=[1,1]): both sides extends by 1

;; Compared to onlinePt, this function:
;; (1) is able to extend both ends. 
;; (2) return 2-pointers.
;;
;; linePts(pq, len=1) = [ onlinePt( pq, len=-1)
;;                      , onlinePt( p10(pq), len=-1 ]
;; 
;; application: tanglnPt
"];

function linePts_test( mode=MODE, opt=[] )=
(
	doctest2( linePts,mode=mode, opt=opt )
);

//==================================================
longside=["longside","a,b","number","Triangle",
 " Given two numbers (a,b) as the lengths of two short sides of a 
;; right triangle, return the long side c ( c^2= a^2 + b^2 )."
,"shortside"
];
function longside_test( mode=MODE, opt=[] )=
(
    doctest2( longside, 
    [ [ longside(3,4), 5, "3,4" ]
	], mode=mode, opt=opt
	)
);

//==================================================
lpCrossPt=["lpCrossPt", "pq,tuv", "point", "Point,Geometry",
 " Given a 2-pointer for a line (pq) and a 3-pointer for a plane (tuv),
;; return the point of intercept between pq and tuv. 
;;
    m=dist(pq)

    P   m
    |'-._	  
    |    'Q._  n
    |dp   |  '-._
    |     |dq    '-.
   -+-----+----------X-

    (m+n)/m = dp/(dp-dq) 

    onlinePt( pq, ratio = dp/(dp-dq) )

"];
function lpCrossPt_test( mode=MODE, opt=[] )=
(
	doctest2( lpCrossPt, mode=mode, opt=opt )

);





//======================================
match=["match","s,ps,want","arr","Inspect"
 ,"Match each pattern p in ps to s, return array of [i,j, [m1,m2,m3...]] or false 
;;  
;; An item of returned array could be [i,[m1]], 
;; [i,[m1,m2]] or [i,[m1,m2,m3]] depending on how 
;; many among p1,p2 and p3 are given. A match requires
;; matching of all p`s given.
;; 
;; A pattern can be a string like 'abcde', which matches
;; any char from a to e; or array like ['sin','cos'] which
;; matches either 'sin' or 'cos'. 
;; 
;; Global constants: 
;; 
;;   A_Z: A to Z
;;   a_z: a to z
;;   A_z: A to z
;;   A_zu: A to z + '_'
;;   0_9: 0 to 9
;;   0_9d: 0 to 9 + '.'
;; 
;; Example:
;; 
;; fml= 'a+cos(2)+sin(5)'
;; match(fml, p1=['sin','cos'])= [[2,['cos']],[9,['sin']]]
;; match(fml, p1=A_z,p2='(')= [[4, ['s','(']], [11, ['n','(']]]
","match_at,match_ps_at"
];
 

//======================================
match=["match","s,ps,want","arr","Inspect"
 ,"Match each pattern p in ps to s, return array of [i,j,m1m2.., [m1,m2,m3...]] or false 
;; 
;; ps: a list of patterns. A pattern p is in the form of 
;;     px or [px,n] where px is either a string containing
;;     all possible chars, or an array containing all possible
;;     words. n is the count chars to match, works only when px 
;;     is a string. See match_ps_at() for more. 
;;    
;; Return array of [i,j,m1m2..,[m1,m2...]] where i is the starting i, j 
;; is the last i of matched. Each m corresponds to each px given.
;; 
;; want: int or array of indices determining what match to report.
;; 
;; For example, 
;;  ps= [a_z,'(',0_9,')']
;;  match(s,ps )=    [2, 7,'cos(2)', ['cos', '(', '2', ')']]
;;  match(s,ps,2 )=  [2, 7,'cos(2)', ['2']]
;; match(s,ps,[0,2])=[2, 7,'cos(2)', ['cos', '2']]
;;  
;; Note: s could be array
"
,"match_at,match_ps_at"
];
 
 function match_test( mode=MODE, opt=[] )=
 (
    let( fml = "a+cos(2)+sin(5)"
             // 0123456789 1234
       ) 
    doctest2( match,
    [
      "var fml"
     ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
     , ""
      
    , "// 1-item ps"  
    
    , [ match( fml, ps=[A_z] )
             , [[0,0,"a",["a"]], [2,4, "cos",["cos"]], [9,11,"sin",["sin"]]]
             , "fml, ps=[A_z]"
       , ["nl",true]
      ]
    , [ match( fml, ps=[["cos","sin"]] )
             , [[2,4,"cos", ["cos"]], [9,11, "sin",["sin"]]]
             , "fml, ps=[['cos','sin']]"
       , ["nl",true]
      ]
    , [ match( fml, ps=[["sin"]] )
             , [[9, 11,"sin",["sin"]]]
             , "fml, ps=[['sin']]"
       , ["nl",true]
      ]
    ,""
    ,"// 2-item ps"  
    
    , [ match( fml, ps=[A_z,"(["] )
             , [[2,5, "cos(", ["cos","("]], [9,12, "sin(", ["sin","("]]]
             , "fml, ps=[A_z,'([']"
       , ["nl",true]
      ]

    , [ match( "EE+ccc+AAaa+BBbb+DDD", ps=[A_Z,a_z] )
             , [[7, 10,"AAaa", ["AA","aa"]]
               ,[12,15,"BBbb", ["BB","bb"]]]
             , "'EE+ccc+AAaa+BBbb+DDD', ps=[A_Z,a_z]"
       , ["nl",true]
      ]
    
    ,""
    ,"// more-item ps" 
    
    , [ match( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] )
             , [ [2,7, "cos(2)", ["cos","(","2",")"]]
               , [9,14,"sin(5)", ["sin","(","5",")"]]]
             , "fml, ps=[['cos(','sin('],'[(',0_9,')]']"
      , ["nl",true]
      ]

    , [ match( fml, ps=["+-", ["sin(","cos("], 0_9,")]"] )
             , [ [1,7, "+cos(2)", ["+", "cos(","2",")"]]
               , [8,14,"+sin(5)", ["+", "sin(","5",")"]]]
             , "fml, ps=[['cos(','sin('],'[(',0_9,')]']"
      , ["nl",true]
      ]
      
    ,""
    ,"// Want only item 0 and 2 in the matches:"
    
    , [ match( fml, ps=["+-", ["sin(","cos("], 0_9,")]"] 
              , want=[0,2]
              )
             , [ [1,7, "+cos(2)", ["+", "2"]]
               , [8,14,"+sin(5)", ["+", "5"]]]
             , "fml, ps=[['cos(','sin('],'[(',0_9,')]'], want=[0,2]"
      , ["nl",true]
      ]      
    , [ match( fml, ps=["(",0_9,")"], want=[1] )
             , [[5, 7, "(2)", ["2"]]
             , [12, 14, "(5)",["5"]]]
             , "fml, ps=['(',0_9,')'], want=[1]"
       , ["nl",true]
      ]
 
     ,""
     ,"// Each p, if a string of chars, can set how many"
     ,"// chars to match"
    , [ match( fml, ps=[[A_z,2]] )
             , [[2, 3, "co", ["co"]], [9, 10, "si", ["si"]]]
             , "fml, ps=[[A_z,2]]"
       , ["nl",true]
      ]    

    , [ match( fml, ps=[[A_z,2],"(",0_9,")"] )
             , [[3, 7, "os(2)", ["os","(","2",")"]]
              , [10, 14, "in(5)", ["in","(","5",")"]]]
             , "fml, ps=[[A_z,2],'(',0_9,')']"
       , ["nl",true]
      ]
      //===
    , "", "// Nested blocks "
    , ""  
    , [ match( "a((c+(d+1))+1)"
             , ps=[["(",1],str(a_z,"+",0_9),[")",1]] 
             )
             , [[1, 13, "((c+(d+1))+1)", ["(","(c+(d+1))+1",")"]]
              , [2, 10, "(c+(d+1))", ["(","(d+1)",")"]]
              , [5, 9, "(d+1)", ["(", "d+1", ")"]]
               ]
             , "fml, ps=[[A_z,2],'(',0_9,')']"
       , ["nl",true]
      ]

    ], mode=mode, opt=opt, scope=["fml",fml]
    )
 );
//echo( match_test(mode=112)[1] );

//===========================================
match_at=[ "match_at", "s,i,p,n", "array|false","Inspect",
"Match pattern p to index i of string s. Return [i,j,word] or false. 
;; 
;; p: pattern, like: A_Z, 0_9, 'abcdef', ['cos','sin'] 
;; 
;; If p a str, match s[i] to any char of p, continue for
;; a number of n (or end of s if n not defined);
;; 
;; If p an arr, match any item in p to the substr starting
;; from i for only once (means, n has no effect).
;; 
;; If matched, return [i,j,word] where word is the matched
;; word and j is its last index.
"
, "match_ps_at"
];

function match_at_test( mode=MODE, opt=[] )=
(
   let( fml = "a+cos(2)+sin(5)"
            // 0123456789 1234
       )
   doctest2( match_at,
   [
     "var fml"
    ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
    , ""
    , [ match_at(fml, 2, a_z), [2,4,"cos"],"fml,2, a_z"]
    , [ match_at(fml, 1, a_z), false,"fml,1, a_z"]
    , [ match_at(fml, 3, a_z), [3,4,"os"],"fml,3, a_z"]
    , [ match_at(fml, 2, a_z,2), [2,3,"co"],"fml,2, a_z, n=2"]
    , [ match_at(fml, 3, a_z,2), [3,4,"os"],"fml,3, a_z, n=2"]
    , [ match_at(fml, 12, str("(",0_9,")")), [12,14,"(5)"]
            ,"fml,12, str('(',0_9,')')"]
    , [ match_at(fml, 2, str(a_z,0_9,"(")), [2,6,"cos(2"],"fml,2, str(a_z,0_9,'(')"]
    , [ match_at(fml, 2, str(a_z,0_9,"("),4), [2,5,"cos("],"fml,2, str(a_z,0_9,'('),n=4"]
    , [ match_at(fml, 2, ["abc","def"]), false,"fml,2, ['abc','def']"]
    , [ match_at(fml, 14, "[]{}()"), [14,14,")"],"fml,14,'[]{}()'"]
    , ""
    ,"// Next one returns sin, not sincos. When p is array, match only once"
    , [ match_at("a+sincos(2)", 2, ["cos","sin"])
                 , [2,4,"sin"],"'a+sincos(2)', 2, ['cos','sin']"
                 ]
    
   ]
   , mode=mode, opt=opt, scope=["fml",fml]
   )
);

//echo( match_at_test(mode=112)[1] );

//===========================================
matchr_at=[ "matchr_at", "s,i,p,n", "array|false","Inspect",
"Reversed match pattern p to index i of string s. Return [i,j,word] or false. 
;; 
;; p: pattern, like: A_Z, 0_9, 'abcdef', ['cos','sin'] 
;; 
;; If p a str, match s[i] to any char of p, continue for
;; a number of n (or end of s if n not defined);
;; 
;; If p an arr, match any item in p to the substr starting
;; from i for only once (means, n has no effect).
;; 
;; If matched, return [i,j,word] where word is the matched
;; word and j is its last index.
"
, "match_ps_at"
];


function matchr_at_test( mode=MODE, opt=[] )=
(
   let( fml = "a+cos(2)+sin(5)"
            // 0123456789 1234
       )
   doctest2( matchr_at,
   [
     "var fml"
    ,"// i: 0123456789 1234"
    , ""
    , [ matchr_at(fml, -1, ")"), [14,14,")"],"fml,-1, ')'"]
    , [ matchr_at("abc+def", -1, a_z), [4,6,"def"],"'abc+def',-1, a_z"]
    , [ matchr_at("abc+def", -2, a_z), [4,5,"de"],"'abc+def',-2, a_z"]
    , [ matchr_at("abc+def", 2, a_z), [0,2,"abc"],"'abc+def',2, a_z"]
    , [ matchr_at("abc+def", -1, a_z,2), [5,6,"ef"],"'abc+def',-1, a_z,2"]
    , [ matchr_at("abc+def", -1, str(a_z,"+")), [0,6,"abc+def"],"'abc+def',-1, str(a_z,'+')"]
    
    , [ matchr_at("abc+def", -1, ["abc","def"])
               , [4,6,"def"],"'abc+def',-1, ['abc','def']"]
    , [ matchr_at("abc+def", -2, ["abc","def"])
               , false,"'abc+def',-2, ['abc','def']"]
    , [ matchr_at("abc+def", 6, ["abc","def"])
               , [4,6,"def"],"'abc+def',6, ['abc','def']"]
    , [ matchr_at("abc+sin", -1, CALC_FUNCS)
               , [4,6,"sin"],"'abc+sin',-1, CALC_FUNCS"]
               
    
//    , [ matchr_at(fml, 3, a_z), [3,4,"os"],"fml,3, a_z"]
//    , [ matchr_at(fml, 2, a_z,2), [2,3,"co"],"fml,2, a_z, n=2"]
//    , [ matchr_at(fml, 3, a_z,2), [3,4,"os"],"fml,3, a_z, n=2"]
//    , [ matchr_at(fml, 12, str("(",0_9,")")), [12,14,"(5)"]
//            ,"fml,12, str('(',0_9,')')"]
//    , [ matchr_at(fml, 2, str(a_z,0_9,"(")), [2,6,"cos(2"],"fml,2, str(a_z,0_9,'(')"]
//    , [ matchr_at(fml, 2, str(a_z,0_9,"("),4), [2,5,"cos("],"fml,2, str(a_z,0_9,'('),n=4"]
//    , [ matchr_at(fml, 2, ["abc","def"]), false,"fml,2, ['abc','def']"]
//    , [ matchr_at(fml, 14, "[]{}()"), [14,14,")"],"fml,14,'[]{}()'"]
//    , ""
//    ,"// Next one returns sin, not sincos. When p is array, match only once"
//    , [ matchr_at("a+sincos(2)", 2, ["cos","sin"])
//                 , [2,4,"sin"],"'a+sincos(2)', 2, ['cos','sin']"
//                 ]
    
   ]
   , mode=mode, opt=opt, scope=["fml",fml]
   )
);

//echo( matchr_at_test(mode=112)[1] );

//===========================================
match_rp=["match_rp","s,ps,new|h[,want]","str","Inspect, match",
"Replace what's matched by the pattern with new string(s).
;;
;; s: target string
;; ps: patterns 
;; new: a str (all matched will be replaced by it)
;;      or arr (matches replaced one by one)
;; h: Or you can use a hash, h, to replace matches
;;    that can find the key in h with its corresponding
;;    value in h.
;; want: int: to replace the i-th item in the match.
;;            For example, if match = 
;;            [[2, 7, 'cos(5)', ['cos','(','5',')']],
;;            want=2 will get 5 replaced.
;;       arr: [1,2,3] or [1,3]: replace items 1~3 
;;            In the above example, [1,3] will replace (5).
","replace,_s,_s2"
];

function match_rp_test( mode=MODE, opt=[] )=
(

 let( fml = "a+cos(2)+sin(5)"
             // 0123456789 1234
       ) 
    doctest2( match_rp,
    [
      "var fml"
     ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
     , ""
     ,"Note: match( fml, ps=[A_z] ) ="
     , replace(str(match( fml, ps=[A_z] ))," ","")
     ,""
     
    , [ match_rp( fml, ps=[A_z],new="XX" )
             , "XX+XX(2)+XX(5)"
             , "fml, ps=[A_z],'XX'"
       , ["nl",false]
      ]

    , [ match_rp( fml, ps=[A_z],new=["B","SIN","COS"] )
             , "B+SIN(2)+COS(5)"
             , "fml, ps=[A_z],['B','SIN','COS']"
       , ["nl",false]
      ]

    , "","// Using hash:", ""
    , [ match_rp( fml, ps=[A_z]
         ,h=["a",5, "sin","COS", "cos","TAN"] )
          , "5+TAN(2)+COS(5)"
          , "fml, ps=[A_z],h=['a',5,'sin','COS','cos','TAN']"
       , ["nl",true]
      ]
      
    ,"", "// more-item ps", ""

    , [ match_rp( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] 
                , new="XX")
             , "a+XX+XX"
             , "fml, ps=[['cos(','sin('],'[(',0_9,')]'],'XX'"
      , ["nl",true]
      ]      

    ,""
    ,"//Using <b>want</b>:"
    ,"// Note want=[1,2,3] is the same as want=[1,3]"
    ,"", "var fml"
    , [ match_rp( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] 
                , new="XX", want=[1,2,3])
             , "a+cosXX+sinXX"
             , "fml, [['cos(','sin('],'[(',0_9,')]'],'XX', want=[1,2,3]"
      , ["nl",true]
      ] 

    , [ match_rp( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] 
                , new="XX", want=[1,3])
             , "a+cosXX+sinXX"
             , "fml, [['cos(','sin('],'[(',0_9,')]'],'XX', want=[1,3]"
      , ["nl",true]
      ] 
      
     ,"" 
     ,"// match(fml, ps=[['cos(','sin('],'[(',0_9,')]'], want=2 )="
     , str("//", match( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"],want=2 ))
     ,""  
       
    , [ match_rp( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] 
                , new="XX", want=2)
             , "a+cos(XX)+sin(XX)"
             , "fml, [['cos(','sin('],'[(',0_9,')]'], 'XX',want=2"
      , ["nl",true]
      ]      
      
      
//    , [ match( fml, ps=[["cos","sin"]] )
//             , [[2,4,"cos", ["cos"]], [9,11, "sin",["sin"]]]
//             , "fml, ps=[['cos','sin']]"
//       , ["nl",true]
//      ]
//    , [ match( fml, ps=[["sin"]] )
//             , [[9, 11,"sin",["sin"]]]
//             , "fml, ps=[['sin']]"
//       , ["nl",true]
//      ]
//    ,""
//    ,"// 2-item ps"  
//    
//    , [ match( fml, ps=[A_z,"(["] )
//             , [[2,5, "cos(", ["cos","("]], [9,12, "sin(", ["sin","("]]]
//             , "fml, ps=[A_z,'([']"
//       , ["nl",true]
//      ]
//
//    , [ match( "EE+ccc+AAaa+BBbb+DDD", ps=[A_Z,a_z] )
//             , [[7, 10,"AAaa", ["AA","aa"]]
//               ,[12,15,"BBbb", ["BB","bb"]]]
//             , "'EE+ccc+AAaa+BBbb+DDD', ps=[A_Z,a_z]"
//       , ["nl",true]
//      ]
//    
//    ,""
//    ,"// more-item ps" 
//    
//    , [ match( fml, ps=[["sin",,"cos"], "[(", 0_9,")]"] )
//             , [ [2,7, "cos(2)", ["cos","(","2",")"]]
//               , [9,14,"sin(5)", ["sin","(","5",")"]]]
//             , "fml, ps=[['cos(','sin('],'[(',0_9,')]']"
//      , ["nl",true]
//      ]
//
//    , [ match( fml, ps=["+-", ["sin(","cos("], 0_9,")]"] )
//             , [ [1,7, "+cos(2)", ["+", "cos(","2",")"]]
//               , [8,14,"+sin(5)", ["+", "sin(","5",")"]]]
//             , "fml, ps=[['cos(','sin('],'[(',0_9,')]']"
//      , ["nl",true]
//      ]
//      
//    ,""
//    ,"// Want only item 0 and 2 in the matches:"
//    
//    , [ match( fml, ps=["+-", ["sin(","cos("], 0_9,")]"] 
//              , want=[0,2]
//              )
//             , [ [1,7, "+cos(2)", ["+", "2"]]
//               , [8,14,"+sin(5)", ["+", "5"]]]
//             , "fml, ps=[['cos(','sin('],'[(',0_9,')]'], want=[0,2]"
//      , ["nl",true]
//      ]      
//    , [ match( fml, ps=["(",0_9,")"], want=[1] )
//             , [[5, 7, "(2)", ["2"]]
//             , [12, 14, "(5)",["5"]]]
//             , "fml, ps=['(',0_9,')'], want=[1]"
//       , ["nl",true]
//      ]
// 
//     ,""
//     ,"// Each p, if a string of chars, can set how many"
//     ,"// chars to match"
//    , [ match( fml, ps=[[A_z,2]] )
//             , [[2, 3, "co", ["co"]], [9, 10, "si", ["si"]]]
//             , "fml, ps=[[A_z,2]]"
//       , ["nl",true]
//      ]    
//
//    , [ match( fml, ps=[[A_z,2],"(",0_9,")"] )
//             , [[3, 7, "os(2)", ["os","(","2",")"]]
//              , [10, 14, "in(5)", ["in","(","5",")"]]]
//             , "fml, ps=[[A_z,2],'(',0_9,')']"
//       , ["nl",true]
//      ]
//      //===
//    , "", "// Nested blocks "
//    , ""  
//    , [ match( "a((c+(d+1))+1)"
//             , ps=[["(",1],str(a_z,"+",0_9),[")",1]] 
//             )
//             , [[1, 13, "((c+(d+1))+1)", ["(","(c+(d+1))+1",")"]]
//              , [2, 10, "(c+(d+1))", ["(","(d+1)",")"]]
//              , [5, 9, "(d+1)", ["(", "d+1", ")"]]
//               ]
//             , "fml, ps=[[A_z,2],'(',0_9,')']"
//       , ["nl",true]
//      ]

    ], mode=mode, opt=opt, scope=["fml",fml]
    )
 );
 
// echo( match_rp_test( mode=112)[1] );

//===========================================
match_ps_at=[ "match_ps_at", "s,i,ps,want", "arr|false","Inspect",
"Match multiple pattern ps to s starting from i. Return [i,j,m1m2..., [m1,m2...]] or false.
;; 
;; ps: a list of patterns. A pattern p is in the form of 
;;     px or [px,n] where px is either a string containing
;;     all possible chars, or an array containing all possible
;;     words. n is the count chars to match, works only when px 
;;     is a string.
;;    
;; Return [i,j,m1m2...,[m1,m2...]] where i is the starting i, j is the
;; last i of matched, and each m corresponds to each px given.
;; 
;; want: array of indices determining what match to report.
;; 
;; For example, given ps=[a_z,'(',0_9,')'], you could get: 
;;    [2, 7, 'cos(2)', ['cos', '(', '2', ')']]
;; 
;; If want=[0,2], get: [2, 7, 'cos(2)', ['cos', '2']]
"
,"match_at"
];

function match_ps_at_test( mode=MODE, opt=[] )=
(
   let( fml = "a+cos(2)+sin(5)"
            // 0123456789 1234
       )
   doctest2( match_ps_at,
   [
     "var fml"
    ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
    , ""
    , [ match_ps_at(fml, 2, [a_z]), [2,4,"cos",["cos"]],"fml,2,[a_z]"]
    , [ match_ps_at(fml, 2, [[a_z,2]]), [2,3,"co",["co"]],"fml,2,[[a_z,2]]", ["nl",false]]

    , [ match_ps_at(fml, 3, [[a_z,2]]), [3,4,"os",["os"]],"fml,3,[[a_z,2]]", ["nl",false]]

    ,""
    ,"<b>set n</b> for count of match when a pattern is a string"
    ,""
    , [ match_ps_at(fml, 2, [a_z,"(",0_9,")"])
        , [2, 7, "cos(2)", ["cos", "(", "2", ")"]]
        ,"fml,2,[a_z,'(',0_9,')']", ["nl",false]]
    , [ match_ps_at(fml, 2, [[a_z,2],"(",0_9,")"])
        , false,"fml,2,[[a_z,2],'(',0_9,')']", ["nl",false]]

   , [ match_ps_at(fml, 3, [[a_z,2],"(",0_9,")"])
        , [3, 7, "os(2)", ["os", "(", "2", ")"]],"fml,3,[[a_z,2],'(',0_9,')']", ["nl",false]]

   ,""
   ,"<b>want=[...]</b>"
   ,""
   , [ match_ps_at(fml, 3, [[a_z,2],"(",0_9,")"],[0,2])
        , [3, 7, "os(2)", ["os", "2"]]
        , "fml,3,[[a_z,2],'(',0_9,')'], want=[0,2]", ["nl",false]]

   , [ match_ps_at(fml, 3, [[a_z,2],"(",0_9,")"],2)
        , [3, 7, "os(2)", ["2"]]
        , "fml,3,[[a_z,2],'(',0_9,')'], want=2", ["nl",false]]
    
    
//    , "", "// Nested blocks... can't do this yet "
//    , ""  
//    , [ match_ps_at( "a((c+(d+1))+1)", 2
//             , ps=[["(",1],str(a_z,"+(",0_9),[")",1]] 
//             )
//             , [[2, 10, "(c+(d+1))", ["(","(d+1)",")"]]
//               ]
//             , "fml, ps=[['(',1],str(a_z,'+(',0_9),[')',1]]"
//       , ["nl",true]
//      ]
      
   ]//doctest2
   , mode=mode, opt=opt, scope=["fml",fml]
   )
);

//echo( match_ps_at_test(mode=112)[1] );

//===========================================
match_nps_at=[ "match_nps_at", "s,i,nps", "arr|false","Inspect",
"Match any named ps in nps to s starting from i. Return [name, i,j,[m1,m2...]] or false.
;; 
;; nps: a list of named patterns: [np,np...]. A named pattern is:
;; 
;; np: [name, ps, want]
;; 
;; name: name of this pattern set;
;; ps  : list of patterns = [p,p,...]. All have to be matched in 
;;       order for this pattern set to be matched;
;; want: array of indices indicating which pattern (among ps)
;;       result to be returned. -1= all, [2]= the 3rd pattern
;; 
;; p   : A pattern p is in the form of px or [px,n] where px is 
;;       either a string containing all possible chars, or an 
;;       array containing all possible words. n is the count 
;;       chars to match for this p, works only when px is a string.
;;    
;; Return [name, i,j,m1m2..., [m1,m2...]] where i is the starting i, j is 
;; the last i of matched, and each m corresponds to each px given.
"
,"match, match_at, match_ps_at"
];

function match_nps_at_test( mode=MODE, opt=[] )=
(
   let( fml = "a+cos(2)+sin(5)"
            // 0123456789 1234
      , nps1= [ ["num", [0_9],-1]
              , ["word",[a_z],-1]
              ]
      , nps2= [ ["num", [0_9],-1]
              , ["word",[a_z],-1]
              , ["op",["^*/+-",["cos","sin"]], -1] 
              ]
      , nps3= [ ["num",[0_9],-1]
              , ["op", ["^*/+-",["cos","sin"]],1]
              ] 
      
       )
   doctest2( match_nps_at,
   [
     "var fml"
     ,"//&nbsp;&nbsp;&nbsp;&nbsp;i: 0123456789 1234"
     , "", "var nps1"
     ,""
    , [ match_nps_at(fml, 2, nps=nps1 )
      , ["word",2,4,"cos",["cos"] ]
      , "fml,2, nps=nps1"
      , ["nl",false]
      ]
    , ""
    , "// nps2 : match numbers, word or op+cos/sin."
    , "// return all (n=-1)"
    , "var nps2"
    , ""
    , [ match_nps_at(fml, 1, nps=nps2 )
      , ["op", 1, 4, "+cos", ["+", "cos"]]
      , "fml,1, nps=nps2"
      , ["nl",false]
      ]
    , ""
    , "// nps3 : match either numbers or op+cos. If number,"
    , "// return all (n=-1). If op+cos, return item 1."
    , "var nps3"
    , ""
    , [ match_nps_at(fml, 1, nps=nps3 )
      , ["op", 1, 4, "+cos", ["cos"]]
      , "fml,1, nps=nps3"
      , ["nl",false]
      ]      
    , [ match_nps_at(fml, 8, nps=nps3 )
      , ["op", 8, 11, "+sin", ["sin"]]
      , "fml,8, nps=nps3"
      , ["nl",false]
      ]      
    , [ match_nps_at(fml, 7, nps=nps3 )
      , false
      , "fml,7, nps=nps3"
      , ["nl",false]
      ]      
      
//    , [ match_nps_at(fml, 2, [[a_z,2]]), [2,3,["co"]],"fml,2,[[a_z,2]]", ["nl",false]]
//
//    , [ match_nps_at(fml, 3, [[a_z,2]]), [3,4,["os"]],"fml,3,[[a_z,2]]", ["nl",false]]
//
//    ,""
//    ,"<b>set n</b> for count of match when a pattern is a string"
//    ,""
//    , [ match_nps_at(fml, 2, [a_z,"(",0_9,")"]), [2, 7, ["cos", "(", "2", ")"]],"fml,2,[a_z,'(',0_9,')']", ["nl",false]]
//    , [ match_nps_at(fml, 2, [[a_z,2],"(",0_9,")"]), false,"fml,2,[[a_z,2],'(',0_9,')']", ["nl",false]]
//
//   , [ match_nps_at(fml, 3, [[a_z,2],"(",0_9,")"]), [3, 7, ["os", "(", "2", ")"]],"fml,3,[[a_z,2],'(',0_9,')']", ["nl",false]]
//
//   ,""
//   ,"<b>want=[...]</b>"
//   ,""
//   , [ match_nps_at(fml, 3, [[a_z,2],"(",0_9,")"],[0,2])
//        , [3, 7, ["os", "2"]]
//        , "fml,3,[[a_z,2],'(',0_9,')'], want=[0,2]", ["nl",false]]
//
//   , [ match_nps_at(fml, 3, [[a_z,2],"(",0_9,")"],2)
//        , [3, 7, ["2"]]
//        , "fml,3,[[a_z,2],'(',0_9,')'], want=2", ["nl",false]]
    
   ]
   , mode=mode, opt=opt, scope=["fml",fml, "nps1",nps1
                               ,"nps2",nps2 
                               ,"nps3",nps3 
                               ]
   )
);

//echo( match_nps_at_test(mode=112)[1] );

//==================================================
_mdoc=["_mdoc", "mname,s", "n/a", "Doc",
 " Given a module name (mname) and a doc string (s), parse s (using 
;; parsedoc()) and echo to the console.
"];
function _mdoc_test( mode=MODE, opt=[] )=
(
	doctest2( _mdoc, mode=mode, opt=opt )
);


//==================================================
midPt=[ "midPt", "PQ", "pt", "Point" ,
"Given a 2-pointer PQ, return the pt that's in the middle of P and Q
"];

function midPt_test( mode=MODE, opt=[] )=
(
    let( pq= [ [2,0,4],[1,0,2] ] )
    doctest2( midPt, 
	[ 
	  [ midPt( pq ), [1.5,0,3], pq ]
	], mode=mode, opt=opt, scope=["pq",pq]
 )
);

////==================================================
//minPt=[ "minPts", "pts", "array", "Point, Math",
//"Given an array of points (pts), return a pt that
//;; has the min x then min y then min z.
//"];
// 
//function minPt_test( mode=MODE, opt=[] )=
//(	
//    let( pts1= [[2,1,3],[0,2,1] ]
//	   , pts2= [[2,1,3],[0,3,1],[0,2,0], [0,2,1]]
//       )
//	
//	doctest2( minPt,
//	[
//      "var pts1", "var pts2"
//    , [ minPts( pts1 ), [0,2,1], "pts1" ]
//    , [ minPts( pts2 ), [0,2,0], "pts2" ]
//    ]
//    , mode=mode, opt=opt, scope=["pts1", pts1, "pts2", pts2]
//	)
//);
//
////==================================================
//function minxPts_test( mode=MODE, opt=[] )=(//======================
//	
//    let( pts1= [[2,1,3],[0,2,1] ]
//	   , pts2= [[2,1,3],[0,3,1],[0,2,0]]
//       )
//	
//	doctest2( minxPts,
//	[
//      "var pts1", "var pts2"
//    , [ minxPts( pts1 ), [[0,2,1]], "pts1" ]
//    , [ minxPts( pts2 ), [[0,3,1],[0,2,0]], "pts2" ]
//    ]
//    , mode=mode, opt=opt, scope= ["pts1", pts1, "pts2", pts2]
//	)
//);
//
////==================================================
//function minyPts_test( mode=MODE, opt=[] )=(//==================
//	
//    let( pts1= [[2,1,3],[0,2,1],[3,3,0]]
//	   , pts2= [[2,1,3],[0,3,1],[3,1,0]]
//	   )
//       
//	doctest2( minyPts, 
//    [
//      "var pts1", "var pts2"
//    , [ minyPts( pts1 ), [[2,1,3]], "pts1" ]
//    , [ minyPts( pts2 ), [[2,1,3],[3,1,0]], "pts2" ]
//    ]
//    ,mode=mode, opt=opt, scope=["pts1", pts1, "pts2", pts2]
//	)
//);
//
////==================================================
//function minzPts_test( mode=MODE, opt=[] )=(//==================
//	
//    let( pts1= [[2,1,3],[0,2,1]]
//	   , pts2= [[2,1,3],[0,3,1],[0,2,1]]
//	   )
//	doctest2( minzPts,
//    [
//      "var pts1", "var pts2"
//    , [ minzPts( pts1 ), [[0,2,1]], "pts1"  ]
//    , [ minzPts( pts2 ), [[0,3,1],[0,2,1]], "pts2" ]
//    ]
//    , mode=mode, opt=opt, scope=["pts1", pts1, "pts2", pts2]
//	)
//);

//==================================================
mod=["mod", "a,b", "number", "Number, Math",
" Given two number, a,b, return the remaining part of division a/b.
;;
;; Note: OpenScad already has an operator for this, it is the percentage
;; symbol. This is here because this code file might be parsed by python
;; that reads the percentage symbol as a string formatting site and causes
;; errors.
"];

function mod_test( mode=MODE, opt=[] )=
(
	doctest2( mod,
	[
      [ mod(5,3), 2, "5,3"]
    , [ mod(-5,3), -2 , "-5,3"]
    , [ mod(5,-3), 2, "5,-3"]
    , [ mod(7,3),1, "7,3"]
    , [ mod(-7,3), -1 , "-7,3"]
    , [ mod(3,3), 0 , "3,3"]
    , [ mod(3.8,1), 0.8 , "3.8,1"]
    , [ mod(38,1), 0 , "38,1"]
	]
    , mode=mode, opt=opt
	)
);

//==================================================
normal= ["normal","pqr","pt","math",
"Given a 3-pointer pqr, return the normal (a vector) to the plane
;; formed by pqr.
"];
function normal_test( mode=MODE, opt=[] )=
(
	let( pqr = [[-1, -2, 2], [1, -4, -1], [2, 3, 3]] )
    
	doctest2( normal, 
    [
      [ normal(pqr), [-13, 11, -16], pqr ]
    ]
    , mode=mode, opt=opt
	)
);

//==================================================
normalLn=["normalLn","pqr,len=undef, sign=1", "2-pointer", "Geometry",
" Return a 2-pointer, [Q,N], where Q is the mid pt of pqr and N is 
;; the normal pt. Set len to determine the length, and sign for 
;; direction."
];

function normalLn_test( mode=MODE, opt=[] )=
(
	doctest2( normalLn,
	[]
	, mode=mode, opt=opt
	)
);
//==================================================
function normalPt_test( mode=MODE, opt=[] )=
(
	doctest2( normalPt,
	[]
	, mode=mode, opt=opt
	)
);
		

//module normalPt_demo()
//{
//	echom("normalPt");
//	pqr = randPts(3);
//	P = pqr[0];  Q=pqr[1]; R=pqr[2];
//	Chain( pqr, ["closed", false] );
//	N = normalPt( pqr );
//	echo("N",N);
//	MarkPts( concat( pqr,[N]) , ["labels","PQRN"] );
//	//echo( [Q, N] );
//	Mark90( [N,Q,P] );
//	Mark90( [N,Q,R] );
//	Dim( [N, Q, P] );
//	Line0( [Q, N], ["r",0.02,"color","red"] );
//	Nr = normalPt( pqr, len=1, reversed=true);
//	echo("Nr",Nr);
//	Mark90( [Nr,Q,P] );
//	Mark90( [Nr,Q,R] );
//	Dim( [Nr, Q, P] );
//	Line0( [Q, Nr], ["r",0.02,"color","green"] );
//
//}
//normalPt_demo();
//Dim_test( );


//========================================================
normalPts=["normalPts", "pts,len=1", "pts", "Geo",
"Return normal pts of each pt in pts"
];
function normalPts_test( mode=MODE, opt=[] )=
(
    doctest2( normalPts, [], mode=mode, opt=opt )
);

//========================================================
num=[ "num", "x", "number", "Type, Number",
" Given x, convert x to number. x could be a number, a number string,
; or a string for feet-inches like 3`4' that will be converted to number
; in inches.
;
; num('-3.3') => -39  
; num('2`6\"') => 18   
"];
function num_test( mode=MODE, opt=[] )=
(
	doctest2( num, 
    [
      [ num(-3.8),-3.8, "-3.8"]
    , [ num("3"),3, "'3'"]
    , [ num("340"),340, "'340'"]
    , [ num("3040"),3040, "'3040'"]
    , [ num("3.4"),3.4, "'3.4'"]
    , [ num("30.04"),30.04, "'30.04'"]
    , [ num("340"),340, "'340'"]
    , [ num("3450"),3450, "'3450'"]
    , [ num(".3"),0.3, "'.3'"]
    , [ num("-.3"),-0.3, "'-.3'"]
    , [ num("0.3"),0.3, "'0.3'"]
    , [ num("-0.3"),-0.3, "'-'0.3'"]
    , [ num("-15"),-15, "'-15'"]
    , [ num("-3.35"),-3.35, "'-3.35'"]
    , [ num("789.987"),789.987, "'789.987'"]
	, ""	
	, "// Convert to inches:"
    , [ num("2'6\""), 30, "2`6'"]
    //, [ num("3""), 3, "'3'"]
    , [ num("3'"), 36, "3\""]
	, ""	
    , [ num([3,4]),undef, "[3,4]"]
    , [ num("x"),undef, "'x'"]
    , [ num("34-5"),undef, "'34-5'"]
    , [ num("34x56"),undef, "'34x56'"]
    , [ num(""),undef, "''"]
	, [ num("x"),undef, "'x'"]
	, [ num([3]),undef, [3]]
	, [ num(true),undef, "true"]
    ,""
    ,"// Scientific notation (2015.7.29)"
    ,[ num("3e-2"),0.03, "'3e-2'"]
    ,[ num("3E2"),300, "'3E2'"]
    ,[ num("3E-8"),3e-8, "'3E-8'"]
    
	]
    ,mode=mode,opt=opt

	)
);
//echo( num_test( 12)[1] );

//function _getdeci(n)= index(str(n),".")<0?0:len(split(str(n),".")[1]);

//========================================================
numstr=["numstr","n,d=undef,dmax=undef,convert=undef", "string", "Number, String",
 " Convert a number to string. d = decimal
;; 
;;   numstr(3.8, d=3) = '3.800'   ... (a)
;;   numstr(3.8765, d=3) = '3.877' ... (b)
;; 
;; If we want to limit decimal points (b) but don't want to add
;; extra 0 (a), then we can use dmax:
;;
;;   numstr(3.8, dmax=3) = '3.8'
;;   numstr(3.8765, dmax=3) = '3.877'
;;
;;   numstr( 3.839, d=1 )= '3.8'
;;   numstr( 3.839, d=2 )= '3.84'
;;   numstr( 3.839, d=3 )= '3.839'
;;   numstr( 3.839, d=4 )= '3.8390'
;;   numstr( 3.839, d=1, dmax=2 )= '3.8'
;;   numstr( 3.839, d=2, dmax=2 )= '3.84'
;;   numstr( 3.839, d=3, dmax=2 )= '3.84'
;;   numstr( 3.839, d=4, dmax=2 )= '3.84'
;;
;;   numstr( 0.5,conv='ft:ftin' )= '6''
;;   numstr( 96,conv='in:ftin' )= '8''
"];

function numstr_test( mode=MODE, opt=[] )=
(
	doctest2( numstr,
	[
	  "// Integer: "
    , [ numstr(0), "0", "0"]
    , [ numstr(5), "5", "5"]
    , [ numstr(5,d=2), "5.00", "5,d=2"]
    , [ numstr(5,dmax=2), "5", "5,dmax=2"]
    , [ numstr(38, d=0), "38", "38,d=0"]
    , [ numstr(38, d=2), "38.00", "38,d=2"]
	, "// Float. Note the difference between d and dmax: "
    , [ numstr(3.80), "3.8", "3.80"]
    , [ numstr(38.2, dmax=3), "38.2", "38.2,dmax=3"]
    , [ numstr(38.2, d=3, dmax=2), "38.20", "38.2,d=3, dmax=2"]
    , [ numstr(38.2, d=3, dmax=4), "38.200", "38.2,d=3, dmax=4"]
    , [ numstr(38.2, d=5, dmax=4), "38.2000", "38.2,d=5, dmax=4"]
    , [ numstr(3.839,d=0), "4", "3.839, d=0"]
    , [ numstr(3.839,d=1), "3.8", "3.839, d=1"]
    , [ numstr(3.839,d=2), "3.84", "3.839, d=2"]
    , [ numstr(3.839,d=3), "3.839", "3.839, d=3"]
    , [ numstr(3.839,d=4), "3.8390", "3.839, d=4"]
    , [ numstr(3.839,d=1, dmax=2), "3.8", "3.839, d=1, dmax=2"]
    , [ numstr(3.839,d=2, dmax=2), "3.84", "3.839, d=2, dmax=2"]
    , [ numstr(3.839,d=3, dmax=2), "3.84", "3.839, d=3, dmax=2"]
	 ,[ numstr(3.839,d=4, dmax=2), "3.84", "3.839, d=4, dmax=2"]	
    , [ numstr(-3.839,d=1), "-3.8", "-3.839, d=1"]
    , [ numstr(-3.839,d=2), "-3.84", "-3.839, d=2"]
    , [ numstr(-3.839,d=4), "-3.8390", "-3.839, d=4"]

	, "// conversion: "
	 ,"// conv = 'ft:ftin'"
    , [ numstr(0.5,conv="ft:ftin"), "6\"", "0.5,conv='ft:ftin'"]
    , [ numstr(3,conv="ft:ftin"), "3'", "3,conv='ft:ftin'"]
    , [ numstr(3.5,conv="ft:ftin"), "3'6\"", "3.5,conv='ft:ftin'"]
    , [ numstr(3.5,d=2,conv="ft:ftin"), "3'6.00\"", "3.5,d=2,conv='ft:ftin'"]
    , [ numstr(3.343,conv="ft:ftin"), "3'4.116\"", "3.343,conv='ft:ftin'"]
    , [ numstr(3.343,d=2,conv="ft:ftin"), "3'4.12\"", "3.343,d=2, conv='ft:ftin'"]
	, "// conv = 'in:ftin'"
    , [ numstr(96,conv="in:ftin"), "8'", "96,conv='in:ftin'"]
    , [ numstr(32,conv="in:ftin"), "2'8\"", "32,conv='in:ftin'"]
	, "// d is ignored if negative:"
    , [ numstr(38, d=-1), "38", "38,d=-1"]
		
	], mode=mode, opt=opt )
);
//numstr_test();


//========================================================
onlinePt=[ "onlinePt", "PQ,len=false,ratio=0.5", "pt", "Point",
" Given a 2-pointer (PQ), return a point (pt) that lies along the
;; line between P and Q, with its distance away from P defined by 
;; *len* or by *ratio* if len is not given. When ratio=1, return Q.
;; Both *len* and *ratio* can be negative.
"];
function onlinePt_test( mode=MODE, opt=[] )=
(
	let( pq= randPts(2)
	   , d1pt= onlinePt( pq, len=1 )
	   , r3pt= onlinePt( pq, ratio=0.3)
	   , distpq = dist(pq)
       )
 
	doctest2( onlinePt, 
	[
      "var pq"
    , [ onlinePt(pq,0), pq[0] , "pq,0"]
    , [ onlinePt(pq,ratio=1), pq[1] , "pq,ratio=1"]

	, [ norm(pq[0]- onlinePt(pq,len=1))
		, 1, "pq,len=1"
        , ["myfunc", "norm( pq[0]- onlinePt{_} )"]]
    
	, [ norm(pq[1]- onlinePt(pq,len=-2))
		, distpq+2, "pq,len=-2"
        , ["myfunc", "norm( onlinePt{_}-pq[1] )"]]

	, [ norm(pq[0]- onlinePt(pq,ratio=0.3))
		, distpq*0.3, "pq,ratio=0.3"
        , ["myfunc", "norm( pq[0]- onlinePt{_} )"]]
        //, ["funcwrap", "norm({_}-pq[0])"]]
        
	, [ norm(pq[1]- onlinePt(pq,ratio=-0.3))
		, distpq*1.3, "pq,ratio=-0.3"
        , ["myfunc", "norm( pq[1]- onlinePt{_} )"]]
        //, ["funcwrap", "norm({_}-pq[1])"]]	 
	]

	, mode=mode, opt=opt, scope=["pq", pq, "distpq", distpq, 
                        "d1pt", d1pt, "r3pt", r3pt]
    )
);

//doc(onlinePt);



//========================================================
onlinePts=["onlinePts","PQ,lens=false,ratios=false","path","Point",
"
 Given a 2-pointer (PQ) and a numeric array assigned to either lens
;; or ratios, return an array containing pts along the PQ line with 
;; distances (from P to each pt) defined in the given array. Refer to
;; onlinePt().
;; This is good for getting multiple points on pq line. For example, to
;; get 4 points that divide pq line evenly into 5 parts:
;;
;;   onlinePts( pq, ratios=[ 0.2, 0.4, 0.6, 0.8 ] )
;;
;; This ratios can be produced this way: range(0.2,0.2,1)
"];

function onlinePts_test( mode=MODE, opt=[] )=
(
	let( pq= [[0,1,0],[10,1,0]] //randPts(2);
	   , lens= [-1,2]
	   , ratios= [0.2, 0.4, 0.6]
	   , distpq = dist(pq)
       )
	doctest2( onlinePts, 
	[
      "var pq"
	, [ onlinePts(pq,lens), [[-1, 1, 0], [2, 1, 0]], "pq,[-1,2]" ]
	, [ onlinePts(pq,ratios=ratios)
                 , [[2, 1, 0], [4, 1, 0], [6, 1, 0]], "pq,ratios=ratios"]
//    ,"// Case below produces exact same result, but why test fails ? " 
//    ,  [ "pq,ratios=range(0.2,0.2,0.7)", onlinePts(pq,ratios=range(0.2,0.2,0.7))
//                                        , [[2, 1, 0], [4, 1, 0], [6, 1, 0]]]
     ]
	, mode=mode, opt=opt, scope=["pq", pq
           ,"lens", lens
           ,"ratios",ratios
           ,"distpq",distpq] 
            )
);

//echo( onlinePts_test(mode=12)[1] );

//========================================================
or=["or","x,dval","any|dval",  "Core",
"
 Given x,dval, if x is evaluated as true, return x. Else return
;; dval. Same as x?x:dval but x only appears once.
"  
];
 
function or_test( mode=MODE, opt=[] )=
(
	doctest2( or, 
	[ [ or(false,3), 3, "false, 3"]
    , [ or(undef,3), 3, "undef, 3"]
    , [ or(0    ,3), 3, "0,     3"]
    , [ or(50   ,3), 50, "50,    3"]
	], mode=mode, opt=opt
	)
);
//echo( or_test()[1] );	

//========================================================
 othoPt= [ "othoPt", "pqr", "pt", "Point",
 "Given a 3-pointer (pqr), return the coordinate of the point D
;; such that line QD is at 90 degree with line PR.
;;
;;         Q
;;         :.
;;        /: :
;;       / :  :
;;      /  :   :
;;     /   :_   :
;;    /____:_|___:
;;   P     D      R
"];

 
function othoPt_test( mode=MODE, opt=[] )=
(
	let( pqr= randPts(3)
	   , oPt= othoPt( pqr )
       )
	doctest2( othoPt,
	[ 
      
      [ "pqr=",pqr,"", ["notest",true, "ml",1,"fname","pqr"] ]
    , [ is90([ pqr[0], oPt],[oPt, pqr[1]]), true,  "pqr",
        [ "fname"
        , "is90([ pqr[0], othoPt(pqr)],[othoPt(pqr), pqr[1]])"
        ]
      ]
    , [ is90([ pqr[0], oPt, pqr[1]]), true,  "pqr",
        [ "fname"
        , "is90([ pqr[0], othoPt(pqr), pqr[1]])"
        ]
      ]
//    ,[ is90([ pqr[0], opt, pqr[1]]), "pqr", true,
//        ["funcwrap", "is90([ pqr[0], {_},  pqr[1]])"]
//     ]
    , [ angle([ pqr[0], oPt, pqr[1]]), 90,  "pqr",
        [ "fname"
        , "angle([ pqr[0], othoPt(pqr), pqr[1]])"
        ]
      ]
//      ,[ angle([ pqr[0], opt, pqr[1]]), "pqr", 90,
//        ["funcwrap", "angle([ pqr[0], {_}, pqr[1]])"]
//      ]
	]
    , mode=mode, opt=opt //, scope=["pqr",pqr]
	)
);

//echo( othoPt_test()[1] );

//========================================================
packuni=["packuni","uni, prefix=''#/''","arr", "Array"
,"Given an arr with a data structure called *uni*, an arr of one-dimensional
;; arrs, each for a block:
;;
;;   [[''#/1'', ''*'', ''#/2''], [''x'', ''+'', 1], [''y'', ''+'', 2]]
;;
;; which is a return of getuni( ''(x+1)*(y+2)'' ), pack and stack those 
;; blocks into an array it represents:  
;;
;;   [ [''x'', ''+'', 1], ''*'', [''y'', ''+'', 2] ]
"];

function packuni_test( mode=MODE, opt=[] )=
(
   doctest2( packuni
   ,[ 
    
     [ packuni([["a","#/1"], ["b","#/2"], ["c","d"]])
      , ["a",["b",["c","d"]]]
      , [ ["a","#/1"], ["b","#/2"], ["c","d"]]
     ]

   , [ packuni( [["#/1"], ["a","x","#/2"], ["b","#/3"], ["c","d"]])
     , [["a", "x", ["b", ["c", "d"]]]]
     , [ ["#/1"], ["a","x","#/2"], ["b","#/3"], ["c","d"]]
     ]

   , [ packuni( [ ["#/1","*","#/2"], ["x","+","1"], ["y","+","2"]] )
     , [["x", "+", "1"], "*", ["y", "+", "2"]]
     , [["#/1","*","#/2"], ["x","+","1"], ["y","+","2"]]
     ]

    ,[ packuni( [["#/1","*","#/2"], ["x","+","1"], ["sin","y","+","2"]] )
     , [["x", "+", "1"], "*", ["sin", "y", "+", "2"]]
     , [["#/1","*","#/2"], ["x","+","1"], ["sin","y","+","2"]]
     ]
         
    ,[ packuni( [["#/1", "*", "#/2"], ["x", "+", 1], ["y", "+", 2]] )
     , [["x", "+", 1], "*", ["y", "+", 2]]
     , [["#/1", "*", "#/2"], ["x", "+", 1], ["y", "+", 2]]
     ]
     
//     ,[  [["#/1", "^"], ["left_arm", "+", 1]]
//     , packuni( [["#/1", "^"], ["left_arm", "+", 1]] )
//     , []
//     ]
//     
//     ,[  [["#/1", "+", 2], ["left_arm", "+", 1]]
//     , packuni( [["#/1", "+", 2], ["left_arm", "+", 1]] )
//     , []
//     ]
     
//     ,[  [["#/1", "^", 2], ["left_arm", "+", 1]]
//     , packuni( [["#/1", "^", 2], ["left_arm", "+", 1]] )
//     , []
//     ]
     
 
    ], mode=mode, opt=opt )
);

//==================================================
parsedoc=["parsedoc","string","string", "Doc",
 " Given a string for documentation, return formatted string.
"];

function parsedoc_test( mode=MODE, opt=[] )=
(
	doctest2( parsedoc, mode=mode, opt=opt )
);

//========================================================
permute=["permute","arr","array","Array, Math",
 " Given an array, return its permutation.
;;
;; [2,4,6] => [[2,4,6],[2,6,4],[4,2,6],[4,6,2],[6,2,4],[6,4,2]]
"];

function permute_test( mode=MODE, opt=[] )=
(
	doctest2( permute
	,[
	  [permute(range(2))
		, [[0,1],[1,0]]
        ,"range(2)", ["ml",3]
	  ]
      
	, [permute([2,4,6])
		, [[2,4,6],[2,6,4],[4,2,6],[4,6,2],[6,2,4],[6,4,2]]
        ,[2,4,6], ["ml",3]
	  ]
      
	,  [ permute(range(3)),[[0,1,2],[0,2,1],[1,0,2],[1,2,0],[2,0,1],[2,1,0]], "range(3)", ["ml",1]]
  
    , [permute([1,2,3,4]),
		[ [1, 2, 3, 4], [1, 2, 4, 3], [1, 3, 2, 4]
		, [1, 3, 4, 2], [1, 4, 2, 3], [1, 4, 3, 2]
		, [2, 1, 3, 4], [2, 1, 4, 3], [2, 3, 1, 4]
		, [2, 3, 4, 1], [2, 4, 1, 3], [2, 4, 3, 1]
		, [3, 1, 2, 4], [3, 1, 4, 2], [3, 2, 1, 4]
		, [3, 2, 4, 1], [3, 4, 1, 2], [3, 4, 2, 1]
		, [4, 1, 2, 3], [4, 1, 3, 2], [4, 2, 1, 3]
		, [4, 2, 3, 1], [4, 3, 1, 2], [4, 3, 2, 1]
	    ]
        ,[1,2,3,4] 
        , ["ml",1]
	   ]
//    , [permute(range(5)), // WARN: slow  ~ 40sec !!
//		[ 
//	    ]
//        ,[1,2,3,4] 
//        , ["ml",1]
//	   ] 
	], mode=mode, opt=opt
	)
);

//echo( permute_test()[1] );

//=======================================================
pick=["pick", "o,i", "pair", "Array",  
"
 Given an array or a string (o) and index (i), return a two-member array :
;;
;; [ o[i], the rest of o ] // when o is array
;; [ o[i], [ pre_i string, post_i_string] ] //when o is a string
;;
;; Note that index could be negative.
"];

function pick_test( mode=MODE, opt=[] )=
(
	let( arr = range(1,5)
	   , arr2= [ [-1,-2], [2,3],[5,7], [9,10]] 
	   )
	doctest2( pick
	,[
		"//Array:"
		,[ pick(arr, 2), [ 3, [1,2,4] ], "arr,2" ]
		,[ pick(arr,-3), [ 2, [1,3,4] ], "arr,-3" ]
		,[ pick(arr2,2), [ [5,7], [ [-1,-2], [2,3], [9,10]] ], "arr2,2" ]
		,"//String:"
		,[ pick("abcdef",3), ["d", ["abc", "ef"]], "'abcdef',3" ]
		,[ pick("this is",2), [ "i", ["th", "s is"] ], "'this is',2" ]
		,"//Note:"
		,str("split('this is','i') = ", split("this is","i") )
	]
    ,mode=mode, opt=opt, scope=["arr", arr, "arr2", arr2]
	)
);


//========================================================
planecoefs=["planecoefs", "pqr, has_k=true", "arr", "Geometry" ,
"
 Given 3-pointer pqr, return [a,b,c,k] for the plane ax+by+cz+k=0.
"];
 
function planecoefs_test( mode=MODE, opt=[] )=
(
	let( pqr = randPts(3)
	//pcs = planecoefs(pqr); // =[ a,b,c,k ]
	//a= pcs[0];
	//b= pcs[1];
	//c= pcs[2];
	//k= pcs[3];
	   , othopt = othoPt( pqr )
	//abpt= angleBisectPt( pqr );
	//mcpt= cornerPt( pqr );
	   )
	doctest2( planecoefs, 
	[
         [ pqr, pqr,"",["myfunc", "pqr", "ml",1, "notest",true]]
        //,str("pqr = ", arrbr( pqr ) )
		, [ othopt, othopt,"pqr",["myfunc", "othoPt", "notest",true]]
        , [ isOnPlane( planecoefs(pqr),othopt ), true ,"pqr", 
		, ["myfunc","isOnPlane( planecoefs{_}, othopt )"]
		//, ["funcwrap","isOnPlane( othopt, planecoefs= {_} )"]
		]
		,"// Refer to isOnPlane_test() for more tests."

	], mode=mode, opt=opt//, scope=["pqr", pqr, "othopt", othopt] 
    )
);



//========================================================
pqr=["pqr","a,p,r","pts","Geometry"
,""];
function pqr_test( mode=MODE, opt=[] )=(

    doctest2( pqr, [], mode=mode, opt=opt )

); // ???


//========================================================
pqr90=["pqr90","pqr,d=3,x,y,z,seed","pts","Geometry",
" Return a pqr that with anelg(pqr)=90. If pqr not given, make one randomly
;;
;; Refer to randPt() for other argument definitions;
;; Use randRightAnglePts() if need to specify leg lengths"
];

function pqr90_test( mode=MODE, opt=[] )=(

    doctest2( pqr90, [], mode=mode, opt=opt )

); // ???

//========================================================
prod=["prod", "arr","n","Math",
" prod( [n1,n2,n3...] ) = n1*n2*n3*...
"];
function prod_test( mode=MODE, opt=[])=
(
   doctest2( prod,
  [
    [ prod([1,2,3]), 6, [1,2,3] ]
   ,[ prod(range(1,0.5,3)),7.5, "range(1,0.5,3)" ]
  ], mode=mode, opt=opt )
);   
   

//========================================================
//projPt_demo();
projPt=["projPt", "a,b", "point", "Point",
"Return the point where a projects on b or b projects on a.
;; a, b could be: pt, line or plane. They can be entered
;; in either projPt(a,b) or projPt( [a,b] ) form. 
;;
"];

function projPt_test( mode=MODE, opt=[] )=(
    doctest2( projPt,
    []
    ,mode=mode, opt=opt )
);

//========================================================
//projPt_demo();
popt=["popt", "h,df=[],sp=';'", "opt", "opt,core",
"make sure h is a pure opt.
;;
;; h could be one of :
;;  sopt 'a;b=3;e'       // string opt
;;  mopt ['a;b=3','r',1] // mixed opt
;;  popt ['a',true,'b',3,'r',1] // pure opt
;; 
;; popt() makes sure the output is a popt.
"];

function popt_test( mode=MODE, opt=[] )=
(
    doctest2( popt,
    [
      [popt("a;b=3;e")
         ,["a",true,"b",3,"e",true], "'a;b=3;e'" 
      ]
    , [ popt(["a;b=3","e",false])
          ,["a",true,"b",3,"e",false], ["a;b=3","e",false] 
      ]   
    , [ popt(["a",true,"b",3,"e",false])
          ,["a",true,"b",3,"e",false]
          , ["a",true,"b",3,"e",false] 
          , ["nl",true]
      ] 
    ,""
    ,"Use <b>df</b> to define a shortcut for key names."
    ,"The name 'e' is converted to 'esc'."
    ,""
    , [popt("a;b=3;e",df=["e","esc"])
         ,["a",true,"b",3,"esc",true]
         , "'a;b=3;e',df=['e','esc']" 
          , ["nl",true]
      ]
    ,""
    ,"This <b>df</b> only works on the sopt. See that the"
    ,"'e' below is not converted:"
    ,""
    , [popt(["a;b=3","e",false],df=["e","esc"])
         ,["a",true,"b",3,"e",false]
         , "['a;b=3','e',false],df=['e','esc']" 
          , ["nl",true]
      ]
    
    ]
    ,mode=mode, opt=opt )
);

//echo( popt_test( mode=112) [1] );

//========================================================
quadrant=["quadrant", "pqr,s", "int", "Geometry",
 " Given a 3-pointer (pqr) and a point, return the quadrant of S on the 
;; yz plane in a coordinate system where P is on the positive X and Q 
;; is the origin. 
;;
;;       
;;       6
;;  +----N-----+
;;  : 2  |  1  :
;;  :    |     :
;; 7-----P-----R- 5
;;  :    :     :
;;  : 3  :  4  :
;;  +----:-----+ 
;;       8                 R
;;                        /|  
;;                       / |
;;                      /  |
;;                     /   |
;;                    /    |
;;        S.---------/--T  |
;;        |:        /   |  |   _.'
;;        | :      /    |  :.-'
;;        U.-:----/-----+-'
;;          '.:  /  _.-'
;;            ':/.-'
;;   N---------Q---------
;;       _.-' 
;;    P-'
;;
;; quadrant(pqr,S) => 1
;;
"];
function quadrant_test( mode=MODE, opt=[] )=
(
	doctest2( quadrant,
	mode=mode, opt=opt
	)
);



//========================================================
rand=["rand", "m=1, n=undef, seed=undef", "Any", "Random, Number, String",
" Given a number (m), return a random number between 0~m.
;; If another number (n) is given, return a random number between m~n.
;; If m is an array or string, return one of its member randomly.
;;
;;   arr= ['a', 'b', 'c']; 
;;
;;   rand(5)=3.63411
;;   rand(5,8)=6.4521
;;   rand(arr)='b'
;;
;; 2015.2.26: add arg 'seed'
;;
;;   rand( 5,seed=3 )= 2.75399
;;   rand( 5,8, seed=3 )= 6.65239
;;   rand( arr, seed=3 )= ''b''
"
];

function rand_test( mode=MODE, opt=[] )=
(
	let( arr=["a","b","c"] )
    //echo("rand_test, arr=", arr);
	doctest2( rand,
	[
		"// rand(a)"
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,str(">> rand(5):", rand(5))
		,"//rand(a,b)"
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,str(">> rand(5,8):", rand(5,8))
		,"//rand(arr)"
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,str(">> rand(arr):", rand(arr))
		,"// rand(len(arr))=> float"
		,"// rand(range(arr))=> integer = floor(rand(len(arr)))"
		,str(">> rand(range(arr)):", rand(range(arr)))
		,str(">> rand(range(arr)):", rand(range(arr)))
		,str(">> rand(range(arr)):", rand(range(arr)))
        ,"// Add arg seed 2015.2.26, making it repeatable"
        ,[ rand(5,seed=3),2.75399,"5,seed=3", ["asstr",true]]
        ,[ rand(5,8, seed=3),6.65239,"5,8, seed=3", ["asstr",true]]
        ,[ rand(arr, seed=3),"b","arr, seed=3"]
    
	], mode=mode, opt=opt, scope=["arr",arr]
	)
);
//function 


//========================================================
randc=["randc", "r=[0,1],g=[0,1],b=[0,1]", "arr", "Random,Color" ,
"
 Given r,g,b (all have default [0,1], which is the max range),
;; return an array with random values for color. Examples:
;;
;; >> color( randc() )  sphere(r=1)
;; >> darker = [0, .4]  // smaller number
;; >> color( randc( r=darker, g=darker ) )  sphere(r=1)
"];

function randc_test( mode=MODE, opt=[] )=
(
    doctest2(randc, [], mode=mode, opt=opt )
);
//function randc_demo()
//{
//	light = [0.8,1];
//	dark  = [0,0.4];
//	translate(randPt()) color(randc(r=dark,g=dark,b=dark)) sphere(r=0.5);
//	translate(randPt()) color(randc()) sphere(r=1);
//	translate(randPt()) color(randc(r=light, g=light,b=light)) sphere(r=1.5);
//}


//========================================================
randPt=["randPt", "d=3,x,y,z,seed", "pt", "Random,Point",  
"
 Get a random pt, covering the range defined by [-d, d]. Or d (distance 
;; to the ORIGIN) can be given as [-1,3], [0,3], etc.  Or, ranges along 
;; the x,y,z directions can be individually specified as x=3, (same
;; as x=[-3,3]), y=[-1,4], etc. Set seed to make it repeatable.
;; 
;;   randPt()          -3,+3 around ORIGIN
;;   randPt(1)         -1,+1 around ORIGIN
;;   randPt([4,5])      between 4, 5 of all axes 
;;   randPt([2,3],z=0)  between 2,3 on x and y on xy plane
;;   randPt(0, y=[2,4]) between y=2 and 4 along y axis
;;
;; Refer to randPts() that gives multiple random points; 
;; pqr() to get randPts(3), and pqr90() to get a pts of a random 
;; right triangle
"];

function randPt_test( mode=MODE, opt=[] )=
(
    doctest2( randPt, mode=mode, opt=opt )
);


//doc(randPt);

//========================================================
randPts=["randPts", "count,d,x,y,z", "pts", "Random, Point",
"
 Return random pts. See randPt() for arguments.
"];
 
function randPts_test( mode=MODE, opt=[] )=
(
    doctest2( randPts, mode=mode, opt=opt )
);


//randPts_test();
//doc(randPts);

//========================================================
//randCirclingPts=[[]];
//function randCirclingPts(count=4, r=undef)
//(
//3
//
//);

//========================================================
randInPlanePt=["randInPlanePt","pqr", "point","Random,Point"
, "Return a random pt inside the triangle pqr."
];
//randInPlanePt_demo();
function randInPlanePt_test( mode=MODE, opt=[] )=
(
    doctest2( randInPlanePt,
	[
    ]
    ,
    mode=mode, opt=opt
	)
);


//========================================================
randInPlanePts=["randInPlanePts","pqr,count=2", "point","Random,Point"
, "Return random points inside triangle pqr"
];
function randInPlanePts_test( mode=MODE, opt=[] )=
(
    doctest2( randInPlanePts,
	[
    ]
    ,
    mode=mode, opt=opt
	)
);


//========================================================
randOnPlanePt=["randOnPlanePt","pqr,r=undef,a=360", "Point", "Random, Point"
,"Return random a point on plane pqr"];
//
// optional r : distance from pqr's incenterPt. Can be: a number, an array [a,b] for range
// optional a : random range of aXQP (X = returned Pt). Default 360. Can be [a1,a2] for range] 
function randOnPlanePt_test( mode=MODE, opt=[] )=
(
    doctest2( randOnPlanePt,
	[
    ]
    ,
    mode=mode, opt=opt
	)
);


//========================================================
randOnPlanePts=["randOnPlanePts","count=2,pqr=randPts(3),r=undef,a=360", "Points", "Random, Point"
,"Return random points on plane pqr"];

function randOnPlanePts_test( mode=MODE, opt=[] )=
(
    doctest2( randOnPlanePts,
	[
    ]
    ,
    mode=mode, opt=opt
	)
);

//========================================================
randRightAnglePts=["randRightAnglePts","r=rand(5),dist=randPt(),r2=-1","Points","Point,Random",
" r: first arm length (default: rand(5) )
;; dist: dist from ORIGIN. It could be a point (indicating 
;; translocation) or integer (default: rand(3)). If it is 
;; an integer, will draw at range inside [dist,dist,dist] 
;; location, and the Q will always be the one closest to 
;; ORIGIN.  
;; r2: 2nd arm length. If not given (default), auto pick a 
;; random value	between 0~r
;; 
;; randRightAnglePts( r=3, r2=3) gives points of a *special 
;; right triangle* in which 3 angles are 90, 45, 45.
;;
;; ref: pqr90() for a quick right triangle with that the arm 
;; lengths can't be specified.
"];

 function randRightAnglePts_test( mode=MODE, opt=[] )=
(
    doctest2( randRightAnglePts,
	[
    ]
    ,
    mode=mode, opt=opt
	)
);

//========================================================
range=["range", "i,j,k,cycle,returnitems", "array", "index",
 " Given i, optional j,k, return a range or ranges of ordered numbers 
;; without the end point. 
;;
;; (1) basic
;;
;;   range(3)    => [0,1,2] // from 0 to 3, inc by 1 (= for(n=[0:i-1] )
;;   range(3,6)  => [3,4,5] // inc by 1  (= for(n=[i:j-1] )
;;   range(3,2,6)=> [3,5]   // inc by 2  (= for(n=[i:j:k-1] )
;;   range(6,3)  => [6,5,4]
;;   range(6,2,3)=> [6,4]
;;
;; (2) i= array or string
;;
;;   for(x=arr)        ... the items
;;   for(i=range(arr)) ... the indices 
;;
;;   range([3,4,5]) => [0,1,2]
;;   range( ''345'' )= [0, 1, 2]
;;
;; (3) cycle= i,-i, true: cycling (New 2014.9.7)
;;
;;   range( ... cycle= i|-i}true...)
;;
;; cycle= -1/+i: extend the index on the left/right side by 1 count
;; cycle= true: same as cycle=1 
;; cycle=[-1,1]
;;
;;   range( 4,cycle=1 )   = [0, 1, 2, 3, 0] // Add first to the right
;;   range( 4,cycle=-1 )  = [3, 0, 1, 2, 3] // Add last to the left
;;   range( 2,5,cycle=2 ) = [2, 3, 4, 2, 3] // Add 1st,2nd to the right
;;   range( 2,5,cycle=-1 )= [4, 2, 3, 4]    // Add last to the left
;;   range( 2,1.5,8,cycle=2 )= [2, 3.5, 5, 6.5, 2, 3.5]
;;
;; range(obj,cycle=i,-1,[-1,1],true...)
;;
;;   range( [11, 12, 13],cycle=2 )= [0, 1, 2, 0,1]
"];

function range_test( mode=MODE, opt=[] )=
(
	let( obj = [10,11,12,13] )
    
	doctest2( range,
	[
    
	  "// 1 arg: starting from 0"
    , [ range(5), [0,1,2,3,4], "5"]
    , [ range(-5), [0, -1, -2, -3, -4] , "-5"]
    , [ range(0), [] , "0"]
    , [ range([]), [] , "[]"]

	, ""
	, "// 2 args: "
    , [ range(2,5), [2,3,4], "2,5"]
    , [ range(5,2), [5,4,3], "5,2"]
    , [ range(-2,1), [-2,-1,0], "-2,1"]
    , [ range(-5,-1), [-5,,-4,-3,-2], "-5,-1"]

	,""
    , "// Note these are the same:"
    , [ range(3), [0,1,2], "3"]
    , [ range(0,3), [0,1,2], "0,3"]
    , [ range(-3), [0,-1,-2], "-3"]
    , [ range(0,-3), [0,-1,-2], "0,-3"]
    , [ range(1),[0] , "1"]
    , [ range(-1),[0] , "-1"]
		
    ,""
	,"// 3 args, the middle one is interval. Its sign has no effect"
    , [ range(2,0.5, 5), [2,2.5,3,3.5, 4, 4.5], "2,0.5,5"]
    , [ range(2, -0.5, 5), [2,2.5,3,3.5, 4, 4.5], "2,-0.5,5"]
    , [ range(5,-1,0), [5,4,3,2,1], "5,-1,0"]
    , [ range(5,1,2), [5,4,3], "5,1,2"]
    , [ range(8,2,0),[8,6,4,2], "8,2,0"]
	, [ range(0,2,8),[0, 2, 4, 6], "0,2,8"]			
    , [ range(0,3,8),[0, 3, 6], "0,3,8"]
		
	, ""
	, "// Extreme cases: "
		
    , [ range(), [], "", ["//", "Give nothing, get nothing"]]
    , [ range(0),[], "0", ["//", "Count from 0 to 0 gets you nothing"] ]
    , [ range(2,2),[], "2,2", ["//", "2 to 2 gets you nothing, either"] ]
    , [ range(2,0,4),[], "2,0,4", ["//", "No intv gets you nothing, too"] ]
    , [ range(0,1),[0] , "0,1"]
    , [ range(0,1,1),[0] , "0,1,1"]

	, "// When interval > range, count only the first: "
    , [ range(2,5,4),[2] , "2,5,4"]
    , [ range(2,5,-1),[2] , "2,5,-1"]
    , [ range(-2,5,-4),[-2] , "-2,5,-4"]
    , [ range(-2,5,1),[-2] , "-2,5,1"]

    , ""
    , " range( <b>obj</b> )"
    , ""
    , "// range( arr ) or range( str ) to return a range of an array"
    , "// so you don't have to do : range( len(arr) )"
    , ""
    , str("> obj = ",obj)
    , ""
    , [ range(obj),[0,1,2,3], "obj"]
    , [ range([3,4,5]), [0,1,2] , "[3,4,5]"]
    , [ range("345"), [0,1,2] , "'345'"]
    , [ range([]), [] , "[]"]
    , [ range(""), [] , "'"]

    , ""
    , " range( ... <b>cycle= i,-i,true</b>...)"
    , ""
    , "// New 2014.9.7: "
    , "// cycle= +i: extend the index on the right by +i count "
    , "// cycle= -i: extend the index on the right by -i count"
    , "// cycle= true: same as cycle=1 "
    , "" 
    , [ range(4,cycle=1), [0,1,2,3,0], "4,cycle=1", ["//","Add first to the right"] ]
    , [ range(4,cycle=-1), [3,0,1,2,3], "4,cycle=-1", ["//","Add last to the left"] ]

	, [ range(4,cycle=true), [0,1,2,3,0], "4,cycle=true", ["//","true=1"] ]
    
    
    ,""

    , [ range(2,5,cycle=1), [2,3,4,2], "2,5,cycle=1", ["//","Add first to the right"] ]
    , [ range(2,5,cycle=2), [2,3,4,2,3], "2,5,cycle=2", ["//","Add 1st,2nd to the right"] ]
	, [ range(2,5,cycle=-1), [4,2,3,4], "2,5,cycle=-1", ["//","Add last to the left"] ]
    , [ range(2,5,cycle=-2), [3,4,2,3,4], "2,5,cycle=-2"]
    , [ range(2,5,cycle=true), [2,3,4,2], "2,5,cycle=true", ["//","true=1"] ]
		
	, ""
    , [ range(2,1.5,8,cycle=2), [2, 3.5, 5, 6.5, 2, 3.5] , "2,1.5,8,cycle=2"]
    , [ range(2,1.5,8,cycle=-2), [5, 6.5, 2, 3.5, 5, 6.5] , "2,1.5,8,cycle=-2"]
    , [ range(2,1.5,8,cycle=true), [2, 3.5, 5, 6.5, 2] , "2,1.5,8,cycle=true"]
		
    , ""
    , " range(<b>obj</b>,<b>cycle=i,-1,[-1,1],true</b>...)"
    , ""
    , str("> obj = ",obj)
    ,[ range(obj,cycle=1)	,[0,1,2,3,0], "obj,cycle=1"] 
    ,[ range(obj,cycle=[-1,1])	,[3, 0,1,2,3,0], "obj,cycle=[-1,1]"] 
    ,[ range(obj,cycle=2)	,[0,1,2,3,0,1], "obj,cycle=2"] 
    ,[ range(obj,cycle=-1)	,[3,0,1,2,3], "obj,cycle=-1"] 
    ,[ range(obj,cycle=-2)	,[2,3,0,1,2,3], "obj,cycle=-2"] 

    , ""
    , " range( obj, <b>returnitems=true,false</b> )"
    , ""
    , "// When using obj, can set returnitems=true to return items."
    , ""
    , [ range([3,4,5],returnitems=true), [3,4,5] , "[3,4,5],returnitems=true"]
    ,[ range(obj,cycle=[-1,1],returnitems=true)	
        ,[13,10,11,12,13,10], "obj,cycle=[-1,1],returnitems=true"]
	]
	, mode=mode, opt=opt
	)
);

//echo( range_test( mode =12 )[1] );

//========================================================
ranges=["ranges", "i,j,k,cover=1,cycle=true,returnitems=false", "array", "index",
 " Similar to range(), but instead of returning an arr of indices,
;; this func returns an ''array of *array of indices*'':
;;
;;   range(3)   = [0,1,2]
;;   ranges(3)  = [[0,1], [1,2], [2,0]]  // cover= 1 or [0,1]
;;   ranges(3..)= [[2,0], [0,1], [1,2]]  // cover= -1 or [1,0]
;;   ranges(3..)= [[0,1,2], [1,2,0], [2,0,1]] //cover= 2 or [0,2]
;;
;; This is achieved by setting the *cover* : 
;;
;;   cover=+n: cover n more indices on the right side 
;;   cover=-n: cover n more indices on the left side
;;   cover=[i,j]: set both sides together
;;
;;   obj = [10, 11, 12, 13]
;;
;;   ranges( obj )= [[0, 1], [1, 2], [2, 3], [3, 0]]
;;   ranges( 0,2,6,cover=[0,2] )= [[0, 2, 4], [2, 4, 0], [4, 0, 2]]
;;
;; Usage of the rest of arguments, i,j,k cycle, are the same as in range()
"];

function ranges_test( mode=MODE, opt=[] )=
(
	let( obj = [10,11,12,13] )
    
	doctest2( ranges
	,[
	  "// 1 arg: starting from 0"
    , [ ranges(3), [[0, 1], [1, 2], [2, 0]] , "3"]
    , [ ranges(-3), [[0,-1], [-1,-2], [-2,0]] , "-3"]
	, ""
	, "// 2 args: "
    , [ ranges(2,5), [[2,3], [3,4], [4,2]] , "2,5"]
    , [ ranges(5,2), [[5,4], [4,3], [3,5]] , "5,2"]
    , [ ranges(-2,1), [[-2,-1], [-1,0], [0,-2]], "-2,1"]
    , [ ranges(-5,-1), [[-5,-4], [-4,-3], [-3,-2], [-2,-5]], "-5,-1"]

	, ""
    , "// Note these are the same:"
    , [ ranges(3), [[0, 1], [1, 2], [2, 0]] , "3"]
    , [ ranges(0,3), [[0, 1], [1, 2], [2, 0]], "0,3"]
    , [ ranges(-3), [[0, -1], [-1, -2], [-2, 0]], "-3"]
    , [ ranges(0,-3), [[0, -1], [-1, -2], [-2, 0]], "0,-3"]
    , [ ranges(1),[[0]] , "1"]
    , [ ranges(-1),[[0]] , "-1"]
    
    , ""
    , "// 3 args, the middle one is interval. Its sign has no effect"
    , [ranges(2,0.5,4) ,  [[2, 2.5], [2.5, 3], [3, 3.5], [3.5, 2]], "2,0.5,4"]
    , [ranges(2,-0.5,4) ,  [[2, 2.5], [2.5, 3], [3, 3.5], [3.5, 2]], "2,-0.5,4"]
    , [ranges(2,0.5,4,cover=2) ,  [[2,2.5,3], [2.5,3,3.5], [3,3.5,2], [3.5,2,2.5]],"2,0.5,4, cover=2"]
    , [ranges(2,0.5,4,cover=-1) 
        ,  [[3.5,2], [2,2.5], [2.5, 3], [3, 3.5]], "2,0.5,4, cover=-1"]
    , [ranges(2,0.5,4,cover=-2) ,  [[3, 3.5,2], [3.5, 2,2.5], [2, 2.5, 3], [2.5, 3, 3.5]], "2,0.5,4, cover=-2"]

    , ""
    , [ ranges(4,1,1),[[4, 3], [3, 2], [2, 4]], "4,1,1"]
    , [ ranges(8,2,0),[[8, 6], [6, 4], [4, 2], [2, 8]] , "8,2,0"]
    , [ ranges(0,3,8),[[0, 3], [3, 6], [6, 0]] , "0,3,8"]
	
    , ""
    , "// Extreme cases: "		
    , [ ranges(), [], "", ["//", "Give nothing, get nothing"]]
    , [ ranges(0),[], "0", ["//", "Count from 0 to 0 gets you nothing"] ]
    , [ ranges(2,2),[], "2,2", ["//", "2 to 2 gets you nothing, either"] ]
    , [ ranges(2,0,4),[], "2,0,4", ["//", "No intv gets you nothing, too"] ]
    , [ ranges(0,1),[[0]] , "0,1"]
    , [ ranges(0,1,1),[[0]] , "0,1,1"]

	, "// When interval > range, count only the first: "
    , [ ranges(2,5,4),[[2]] , "2,5,4"]
    , [ ranges(2,5,-1),[[2]] , "2,5,-1"]
    , [ ranges(-2,5,-4),[[-2]] , "-2,5,-4"]
    , [ ranges(-2,5,1),[[-2]] , "-2,5,1"]

    , ""
    , " ranges( <b>obj</b> )"
    , ""
    , "// ranges( arr ) or ranges( str ) to return a range of an array"
    , "// so you don't have to do : ranges( len(arr) )"
    , ""
    , str("> obj = ",obj)
    , ""
    , [ ranges(obj),[[0, 1], [1, 2], [2, 3], [3, 0]], "obj"]
    , [ ranges([3,4,5]), [[0, 1], [1, 2], [2, 0]], "[3,4,5]"]
    , [ ranges("345"), [[0, 1], [1, 2], [2, 0]]  , "'345'"]
    , [ ranges([]), [] , "[]"]
    , [ ranges(""), [] , "'"]

    , ""
    , " ranges( ... <b>cycle= i,-i,true</b>...)"
    , ""
    , "// cycle= true: extend the index on the right by +i count "
    , "" 
    , [ ranges(4,cycle=false), [[0, 1], [1, 2], [2, 3], [3]], "4,cycle=false"
                , ["//","Add first to the right"] ]
    , [ ranges(2,5,cycle=false), [[2, 3], [3, 4], [4]] , "2,5,cycle=false"]
    , [ ranges(2,1.5,8,cycle=false),
                [[2, 3.5], [3.5, 5], [5, 6.5], [6.5]], "2,1.5,8,cycle=false" ]
    
    , ""
    , " ranges(<b>obj</b>,<b>cycle=true</b>...)"
    , ""
    , str("> obj = ",obj)
    , [ ranges(obj,cycle=true),[[0, 1], [1, 2], [2, 3], [3, 0]], "obj,cycle=true"]
	
    , ""
    , " ranges(... <b>cover</b>=[i,j], n, -n )"
    , ""
    , "// cover=[1,2] means, instead of returning i, return"
    , "//[i-1,i,i+1,i+2]. Note that when cover is set, return array of arrays."
    , "// Also note that w/cover, cycle=+i/-i is treated as cycle=true."

    , [ ranges(3), [[0, 1], [1, 2], [2, 0]] , "3"]
    , [ ranges(3,cover=[0,1],cycle=false)
        , [[0, 1], [1, 2], [2]], "3,cover=[0,1]"]
    , [ ranges(3,cover=[0,1],cycle=true)
        , [[0, 1], [1, 2], [2,0]], "3,cover=[0,1],cycle=true"]
		
		
	, ""
    , [ ranges(3,cover=[0,0]), [[0], [1], [2]], "3,cover=[0,0]"]
    , [ ranges(5,8), [[5, 6], [6, 7], [7, 5]], "5,8"]
    , [ ranges(5,8,cover=[0,0]), [[5], [6], [7]], "5,8,cover=[0,0]"]
//		,["5,8,cover=[1,0]", ranges(5,8,cover=[1,0])
//						, [[5], [5, 6], [6, 7]]]
//		,["5,8,cover=[0,1]", ranges(5,8,cover=[0,1])
//						, [[5, 6], [6, 7], [7]]]
//		,["5,8,cover=[1,0],cycle=true", ranges(5,8,cover=[1,0],cycle=true)
//						, [[7,5], [5, 6], [6, 7]]]
//		,["5,8,cover=[0,2],cycle=true", ranges(5,8,cover=[0,2],cycle=true)
//						, [[5, 6,7], [6, 7, 5], [7,5,6]]]

    , ""
    , [ ranges(0,2,8,cover=[0,1])
                        ,[[0, 2], [2, 4], [4, 6], [6, 0]]
                        , "0,2,8,cover=[0,1]"]
                        
    , [ ranges(0,3,8,cover=[0,1])
                        ,[[0, 3], [3, 6], [6, 0]]
                        , "0,3,8,cover=[0,1]"
                        ]
    , [ ranges(0,2,6,cover=[0,2])
            ,[[0, 2, 4], [2, 4, 0], [4, 0, 2]]
            , "0,2,6,cover=[0,2]", ]  

		,""
    , [ ranges(obj),[[0, 1], [1, 2], [2, 3], [3, 0]], "obj"]
	, [ ranges(obj,cover=[1,1])
			,[[3, 0, 1], [0, 1, 2], [1, 2, 3], [2, 3, 0]]
            , "obj,cover=[1,1]" ] 

    , ""
    , " range( obj, <b>returnitems=true,false</b> )"
    , ""
    , "// When using obj, can set returnitems=true to return items."
    , "// Usage: Plane(w/ thickness)"
    , ""
	, [ ranges(obj,cover=[1,1],returnitems=true)
			,[[13, 10, 11], [10, 11, 12], [11, 12, 13], [12, 13, 10]]
            , "obj,cover=[1,1],returnitems=true" ] 
    
    
	]//doctest

	, mode=mode, opt=opt
	)
);

//echo( ranges_test( mode =12 )[1] );



//========================================================
repeat=["repeat", "x,n=2", "str|arr", "Array,String" ,
"
 Given x and n, duplicate x (a str|int|arr) n times.
"];

function repeat_test( mode=MODE, opt=[] )=
(
	doctest2( repeat, 
    [ 
      [ repeat("d"), "dd" , "'d'"]
    , [ repeat("d",5), "ddddd" , "'d',5'"]
    , [ repeat("d",0), "" , "'d',0'"]
    , [ repeat([2,3]), [2,3,2,3] , "[2,3]"]
    , [ repeat([2,3],3), [2,3,2,3,2,3] , "[2,3],3"]
    , [ repeat([[2,3]],3), [[2,3],[2,3],[2,3]] , "[[2,3]],3"]
    , [ repeat([2,3],0), [] , "[2,3],0"]
    , [ repeat(3), "33" , "3"]
    , [ repeat(3,5), "33333", "3,5", ["//","return a string"] ]
	], mode=mode, opt=opt

	)
);
//multi_test( ["mode",22] ); 
//doc(multi);


//========================================================
replace =[ "replace", "o,x,new=\"\"", "str|arr", "String, Array",
"
 Given a str|arr (o), x and optional new. Replace x in o with new when
;; o is a str; replace the item x (an index that could be negitave) 
;; with new when o is an array.
" ];

function replace_test( mode=MODE, opt=[] )=
(
	doctest2( replace, 
	[
	  [	replace("a_text", "e"), "a_txt", "'a_text', 'e'"]
	, [	replace("a_text", ""), "a_text", "'a_text', '"]
    , [ replace("a_text", "a_t", "A_T"), 	"A_Text", "'a_text', 'a_t', 'A_T'"]
	, [	replace("a_text", "ext", "EXT"), "a_tEXT", "'a_text', 'ext', 'EXT'"]
	, [	replace("a_text", "abc", "EXT"), "a_text", "'a_text', 'abc', 'EXT'"]
	, [	replace("a_text", "t", 	"{t}"), "a_{t}ex{t}", "'a_text', 't',   '{t}'"]
    , [ replace("a_text", "t", 	4), 		"a_4ex4", "'a_text', 't', 4"]
    , [ replace("a_text", "a_text","EXT"), "EXT", "'a_text', 'a_text', 'EXT'"]
    , [	replace("", "e"), "",  "', 'e'"]
	, "# Array # "
    , [ replace([2,3,5], 0, 4), [4,3,5] , "[2,3,5], 0, 4"]
    , [ replace([2,3,5], 1, 4), [2,4,5] , "[2,3,5], 1, 4"]
    , [ replace([2,3,5], 2, 4), [2,3,4] , "[2,3,5], 2, 4"]
    , [ replace([2,3,5], 3, 4), [2,3,5] , "[2,3,5], 3, 4"]
    , [ replace([2,3,5], -1, 4), [2,3,4] , "[2,3,5], -1, 4"]

	 ], mode=mode, opt=opt	
	)
);		



//replace_test( ["mode",22] );
//doc(replace);


////========================================================
//reverse=[ "reverse", "o", "str|arr", "Array, String",
//"
// Reverse a string or array.\\
//"];
//
//function reverse_test( mode=MODE, opt=[] )=
//(
//   doctest2( reverse,
//   [ [ reverse([2,3,4]), [4,3,2], [2,3,4] ]
//   , [ reverse([2]), [2], [2] ]
//   , [ reverse( "234" ), "432", "'234'"  ]
//   , [ reverse( "2" ), "2", "'2'" ]
//   , [ reverse( "" ), "", "'"  ]
//   ], mode=mode, opt=opt
//   )
//);
////reverse_test( ["mode",22] );
////doc(reverse);


//========================================================
ribbonPts=["ribbonPts","pts,len=1,paired=true,closed=false", "Points", "Point"
, ""];
function ribbonPts_test( mode=MODE, opt=[] )=
(
    doctest2( ribbonPts,
	[
    ]
    ,
    mode=mode, opt=opt
	)
);


//========================================================
function RM_test( mode=MODE, opt=[] )=
(
    doctest2(RM,[],mode=mode, opt=opt )
);

//RM_test( [["mode",22]] );
//doc(RM);
//function RM_demo()
//{
//	a3 = randPt(r=1)*360;
//	echo("in RM, a3= ",a3);
//	rm = RM(a3);
//	echo(" rm = ", rm);
//}
////RM_demo();


//========================================================
function RMx_test( mode=MODE, opt=[] )=
(
	doctest2( RMx
	,[
	
	],mode=mode, opt=opt )
);
//RMx_test( ["mode",22] );
//doc(RMx);


////========================================================
function RMy_test( mode=MODE, opt=[] )=
(
	doctest2( RMy
	,[
	
	],mode=mode, opt=opt )
);
//RMy_test( ["mode",22] );

//========================================================
function RMz_test( mode=MODE, opt=[] )=
(
	doctest2( RMz
	,[
	
	],mode=mode, opt=opt )
);
//RMz_test( ["mode",22] );
//doc(RMz);

//========================================================


function RMs_test( mode=MODE, opt=[] )=
(
    doctest2( RMs,
	[
    ]
    ,
    mode=mode, opt=opt
	)
);

//========================================================

//doc(RM2z);

//========================================================
rodfaces=["rodfaces","sides=6, hashole","array", "Faces",
 " Given count of sides (sides) of a rod( column, cylinder), return the 
;; faces required for the polyhedron() function to render the sides of
;; that rod. For a rod with sides=4 below: 
"];

function rodfaces_test( mode=MODE, opt=[] )=
(
    doctest2( rodfaces,[],mode=mode, opt=opt )
);                     

//========================================================

//========================================================
rodPts=["rodPts","","pts","Point",
 " Given 
;;
"];
function rodPts_test( mode=MODE, opt=[] )=
(
    doctest2( rodPts,[],mode=mode, opt=opt )
); 

//========================================================
rodsidefaces=["rodsidefaces","sides=6,twist=0","array", "Faces",
 " Given count of sides (sides) of a rod( column, cylinder), return the 
;; faces required for the polyhedron() function to render the sides of
;; that rod. For a rod with sides=4 below: 
;;         
;;       _6-_
;;    _-' |  '-_
;; 5 '_   |     '-7
;;   | '-_|   _-'| 
;;   |    '(4)   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;; 
;; twist=0
;;
;;   [0,1,5,4]
;; , [1,2,6,5]
;; , [2,3,7,6]
;; , [3,0,4,7]
;;
;; twist = 1:
;;
;;       _5-_
;;    _-' |  '-_
;;(4)'_   |     '-6
;;   | '-_|   _-'| 
;;   |    '-7'   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;;
;;   [0,1,4,7]
;; , [1,2,5,4]
;; , [2,3,6,5]
;; , [3,0,7,6]
;;
;;
;; twist = 2:
;;       (4)_
;;    _-' |  '-_
;; 7 '_   |     '-5
;;   | '-_|   _-'| 
;;   |    '-6'   |
;;   |   _-_|    |
;;   |_-' 2 |'-_ | 
;;  1-_     |   '-3
;;     '-_  | _-'
;;        '-.'
;;          0
;;
;;   [0,1,7,6]
;; , [1,2,4,7]
;; , [2,3,5,4]
;; , [3,0,6,5]
;;
;; Note that the top and bottom faces are NOT included. 
"];
function rodsidefaces_test( mode=MODE, opt=[] )=
(
	doctest2( rodsidefaces,
	[
	 [ rodsidefaces(4), 
			[ [0,1,5,4]
			, [1,2,6,5]
			, [2,3,7,6]
			, [3,0,4,7]
			], "4"
	]
	,[ rodsidefaces(5), 
			[ [0, 1, 6, 5]
			, [1, 2, 7, 6]
			, [2, 3, 8, 7]
			, [3, 4, 9, 8]
			, [4, 0, 5, 9]
			], "5"
	 ]
	,[ rodsidefaces(4,twist=1), 
			[ [0,1,4,7]
			, [1,2,5,4]
			, [2,3,6,5]
			, [3,0,7,6]
			], "4,twist=1"
	]
	,[ rodsidefaces(4,twist=-1), 
			[ [0,1,6,5]
			, [1,2,7,6]
			, [2,3,4,7]
			, [3,0,5,4]
			], "4,twist=-1"
	]
	,[ rodsidefaces(4,twist=2), 
			[ [0,1,7,6]
			, [1,2,4,7]
			, [2,3,5,4]
			, [3,0,6,5]
			], "4,twist=2"
	 ]

	], mode=mode, opt=opt
	)
);



////========================================================
//roll=["roll", "arr,count=1", "arr", "Array"
//," Given an array (arr) and an int (count), roll arr items
//;; as shown below:
//;;
//;; roll( [0,1,2,3] )   => [0,1,2],[3] => [3],[0,1,2] => [3,0,1,2]
//;; roll( [0,1,2,3],-1) => [0],[1,2,3] => [1,2,3],[0] => [1,2,3,0] 
//;; roll( [0,1,2,3], 2) => [0,1],[2,3] => [2,3],[0,1] => [2,3,0,1]
//;;
//;; Compare range w/cycle, which (1) returns indices (2) doesn't 
//;; maintain array size:
//;;
//;; |> obj = [10, 11, 12, 13]
//;; |> range( obj,cycle=1 )= [0, 1, 2, 3, 0]
//;; |> range( obj,cycle=2 )= [0, 1, 2, 3, 0, 1]
//;; |> range( obj,cycle=-1 )= [3, 0, 1, 2, 3]
//;; |> range( obj,cycle=-2 )= [2, 3, 0, 1, 2, 3]
//
//"];
//
//function roll_test( mode=MODE, opt=[] )=
//(
//	doctest2( roll,
//	[
//      [ roll([0,1,2,3]), [3,0,1,2] , "[0,1,2,3]"]
//    , [ roll([0,1,2,3],-1), [1,2,3,0] , "[0,1,2,3],-1"]
//    , [ roll([0,1,2,3], 2), [2,3,0,1] , "[0,1,2,3], 2"]
//    , [ roll([0,1,2,3,4,5], 2), [4,5,0,1,2,3] , "[0,1,2,3,4,5], 2"]
//    , [ roll([0,1,2,3],0), [0,1,2,3] , "[0,1,2,3],0"]
//    ], mode=mode, opt=opt
//	)
//);

//========================================================

function rotangles_p2z_test( mode=MODE, opt=[] )=
( 
    doctest2( rotangles_p2z,
    []
    , mode=mode, opt=opt 
    )
);
//rotangles_p2z_test( ["mode",22] );

//rotangles_p2z_demo();
//doc(rotangles_p2z);



//========================================================

function rotangles_z2p_test( mode=MODE, opt=[] )=
( doctest2( rotangles_z2p, mode=mode, opt=opt )
);

//rotangles_z2p_test( ["mode",22] );
//rotangles_z2p_demo();


//========================================================
rotPt=["rotPt","pt,axis, a","pt", "Geometry",
" Rotate pt for angle a about line pq.
"
];
function rotPt_test( mode=MODE, opt=[] )=
( 
    doctest2( rotPt,
    []
    , mode=mode, opt=opt 
    )
);

//========================================================
rotPts=["rotPts","pt,axis, a","pt", "Geometry",
" Rotate pt for angle a about line pq.
"
];
function rotPts_test( mode=MODE, opt=[] )=
( 
    doctest2( rotPts,
    []
    , mode=mode, opt=opt 
    )
);

//========================================================
run=["run", "f,d", "val", "Core"
," 
"];
function run_test( mode=MODE, opt=[] )=
(
   doctest2( run,
    [
      [ run("abs",-3), 3 , "'abs', -3"]
    , [ run("abs",[-3]), 3 , "'abs', [-3]"]
    , [ run("sign",-3), -1 , "'sign', -3"]
    , [ run("sin",30), 0.5 , "'sin', 30"]
    , [ run("max",[3,1,4,2]), 4 , "'max', [3,1,4,2]"]
    , [ run("pow",3), 9 , "'pow', 3"]
    , [ run("pow",[3,3]), 27 , "'pow', [3,3]"]
    , [ run("concat",[[3,4],[5,6],[7,8]])   
            , [3,4,5,6,7,8], "'concat', [[3,4],[5,6],[7,8]]"
            ,  ]
    ,""
    ,"// New: you can do: run(['sin, 30])"
    ,"//      instead of run(sin, 30)"
    ,""
    , [ run(["sin",30]), 0.5 , ["sin", 30]]
        
    ], mode=mode, opt=opt


    )
);   
//========================================================
_s= [ "_s", "s,arr,sp=''{_}''" , "str", "String",
"
 Given a string (s) and an array (arr), replace the sp in s with
;; items of arr one by one. This serves as a string template.
"
] ;

function _s_test( mode=MODE, opt=[] )=
(
    doctest2( _s,
    [ 
      [ _s("2014.{_}.{_}", [3,6]), "2014.3.6", "'2014.{_}.{_}', [3,6]"]
    , [ _s("", [3,6]), "", "'', [3,6]"]
    , [ _s("2014.{_}.{_}", []), "2014.{_}.{_}", "'2014.{_}.{_}', []"]
    , [ _s("2014.{_}.{_}", [3]), "2014.3.{_}", "'2014.{_}.{_}', [3]"]
    , [ _s("2014.3.6", [4,5]), "2014.3.6", "'2014.3.6', [4,5]"]
	, [ _s("{_} car {_} seats?", ["red","blue"]), "red car blue seats?"
        , "'{_} car {_} seats?', ['red','blue']"
		]
  	], mode=mode, opt=opt

	)
);
//echo( _s_test( mode=112 )[1] );

//========================================================
_s2= [ "_s2", "s,data,beg,df_fmtchars,morefmtchars,end,show" , "str", "String",
"Replace the d in {d`f} in s with data using optional format f. 
;; 
;; d: data selection (_,*,int,name)
;; f: format (see _f()) 
;; 
;; W/o format:
;;  _s2('2015.{_}.{_}', [5,24]) => 2015.5.24 
;;  _s2('2015.{*}.{*}', [5,24]) => 2015.[5, 24].[5, 24]
;;  _s2('2015.{-1}.{0}', [24,5])=> 2015.24.5
;;  _s2('2015.{m}.{d}', ['m',5,'d',24]) 
;;  
;; W format:
;;  _s2('2015.{_`fmt}.{_`fmt}', [5,24]) 
;;  _s2('2015.{1`fmt}.{0`fmt}', [24,5])
;;  _s2('2015.{m`fmt}.{d`fmt}', ['m',5,'d',24])
;;
;; beg,end: beginning/end patterns for the search. 
;;          
;; df_fmtchars: default chars for defining fmt. This is the
;;          chars for the patterns between beg and end.
;;              
;; morefmtchars: add your own chars for search pattern. For
;;          example, if your search term is:{.*..!..} where * 
;;          and ! are not in df_fmtchars, you can set
;;          morefmtchars='*!'
;;
;; Refer to match() for details of the above pattern settings.
;;    
;; show: for displaying property. For example, 
;;         _s2(... show='df_fmtchars')
;; 
;; Refer to _f() for what fmt could be. 
", "_s,_f,_h,_fmt,_fmth"
] ;

function _s2_test( mode=MODE, opt=[] )=
(
	let( data=["m",3,"d",6]
       , tmpl= "2014.{m}.{d}"
       )
    doctest2( _s2,
    [ 
     "// {_}"
    , [ _s2("2014.{_}.{_}", [3,6]), "2014.3.6", "'2014.{_}.{_}', [3,6]"]
    , [ _s2("", [3,6]), "", "'', [3,6]"]
    , [ _s2("2014.{_}.{_}", []), "2014.{_}.{_}", "'2014.{_}.{_}', []"]
    , [ _s2("2014.{_}.{_}", [3]), "2014.3.{_}", "'2014.{_}.{_}', [3]"]
    , [ _s2("2014.3.6", [4,5]), "2014.3.6", "'2014.3.6', [4,5]"]
	, [ _s2("{_} car {_} seats?", ["red","blue"]), "red car blue seats?"
        , "'{_} car {_} seats?', ['red','blue']"
		]
    , "","//==============="
    , "// {i}, i could be negative"
    , [ _s2("2014.{0}.{1}", [3,6]), "2014.3.6", "'2014.{0}.{1}', [3,6]"]
    , [ _s2("2014.{1}.{0}", [3,6]), "2014.6.3", "'2014.{1}.{0}', [3,6]"]
    , [ _s2("2014.{1}.{2}", [3,6]), "2014.6.{2}", "'2014.{1}.{2}', [3,6]"]
    , [ _s2("2014.{-1}.{2}", [3,6]), "2014.6.{2}", "'2014.{-1}.{2}', [3,6]"]
    , "","//==============="
    , "// {*}"
    , [ _s2("At pt {_} & {_}", [2,3,4]), "At pt 2 & 3"
           ,"'At pt {_} & {_}', [2,3,4]"
      ]
    , [ _s2("At pt {*} & {*}", [2,3,4]), "At pt [2, 3, 4] & [2, 3, 4]"
           ,"'At pt {*} & {*}', [2,3,4]"
      ]
    , ""
    , [ _s2("Px={_}, P={*}, Py={-2}", [2,3,4]), "Px=2, P=[2, 3, 4], Py=3"
           ,"'Px={_}, P={*}, Py={-2}', [2,3,4]"
      ]
    
    , "","//==============="
    , "// {name}"
    , [ _s2("2014.{m}.{d}",  data)
        , "2014.3.6"
        ,str( tmpl, ", ", data )
      ]
//    , "// showkey=true :" {abc`/}
//    , [
//        _s2("2014.[m].[d]",  h=data)
//        , "2014.m=3.d=6"
//        , "'2014.[m].[d]', h=data, showkey=true, wrap='[,]'"
//      ]
    , "// hash has less key:val " 
    , [
        _s2("2014.{m}.{d}", ["m",3])
        , "2014.3.{d}"
        , "tmpl, ['m',3]"
      ]
    , "// hash has more key:val " 
    , [
        _s2("2014.{m}.{d}", ["a",1,"m",3,"d",6] )
        , "2014.3.6"
        , "tmpl, ['a',1,'m',3,'d',6]"
      ]	
    , "","//==============="
    , [
        _s2("2014.{m}.{d}", ["a",1,"m",3,"d",6] )
        , "2014.3.6"
        , "'2014.{m}.{d}', ['a',1,'m',3,'d',6]"
      ]  

    ,"// The following case uses [] that are not in the"
    ,"// default fmt chars:"
    , [ _s2("2014.{m`%||wr=/,/}.{d`|d2|wr=[,]}"
            , ["a",1,"m",3,"d",6] 
            , morefmtchars="[]"
            )
        , "2014./300%/.[6.00]"
        , "'2014.{m`%||wr=/,/}.{d`|d2|wr=[,]}',
 ['a',1,'m',3,'d',6], morefmtchars='[]'"
        , ["nl",true]
      ]
    ,"// if not use morefmtchars:"  
    , [ _s2("2014.{m`%||wr=/,/}.{d`|d2|wr=[,]}"
            , ["a",1,"m",3,"d",6] 
            )
        , "2014./300%/.{d`|d2|wr=[,]}"
        , "'2014.{m`%||wr=/,/}.{d`|d2|wr=[,]}',
 ['a',1,'m',3,'d',6]"
        , ["nl",true]
      ]
      
      , [ _s2("% of {0}= {0`%}",[0.96]), "% of 0.96= 96%"
            ,"'% of {0}= {0`%}',[0.96]"]  
    , [ _s2("% of {0`u}= {0`%b}",[0.96]), "% of <u>0.96</u>= <b>96%</b>"
            ,"'% of {0`u}= {0`%b}',[96]"
            ,["nl",true]
      ]  
    , [ _s2("{0} to ftin:{0`||x=in:ftin}",[42])
            , "42 to ftin:3'6\""
            ,"'{0} to ftin:{0`||x=in:ftin}',[42]"
            ,["nl",true]
      ]
    , [ _s2("Take 2 decimal of {0}: {0`|d2}", [14.369])
            ,"Take 2 decimal of 14.369: 14.37"
            ,"'Take 2 decimal of {0}: {0`|d2}', [14.369]"
            ,["nl",true]
      ]  
    , [ _s2("W={W`||x=in:ftin}, L={L`||x=in:cm}cm"
            ,["L",1, "W",30])
            ,"W=2'6\", L=2.54cm"
            ,"'W={W`||x=in:ftin},L={L`||x=in:cm}cm', ['L',1, 'W',30]"
            ,["nl",true]
      ]
    ,""
    ,"// Change { } to something else:"
    ,[ _s2("2014.[m].(d)",["m",3,"d",6]
           , beg="[(", end="])")
       , "2014.3.6"
       , "'2014.[m].(d)',['m',3,'d',6], beg='[(', end='])'"
     ]      
    ,[ _s2("2014.[m].(d)",["m",3,"d",6]
           , beg="[(", end="])")
       , "2014.3.6"
       , "'2014.[m].(d)',['m',3,'d',6], beg='[(', end='])'"
     ]      
    ,""
    ,"// format items of array"
    ,""
    ,[ _s2( "At pt {_`a|d1}"
          , [ [0.2351, -9.72087, 1.5621]]
          )
          , "At pt [0.2, -9.7, 1.6]"
          , "'At pt {_`a|d1}',[[0.2351,-9.72087,1.5621]]"
          , ["nl",true]
      ] 
     ,""
    ,"// show: expose internal property. Avaliable: df_fmtchars,patterns, matched"
    ,[ _s2("2014.[m].(d)",["m",3,"d",6]
           , beg="[(", end="])", show="matched")
       , [[5, 7, "[m]", ["[", "m", "]"]], [9, 11, "(d)", ["(", "d", ")"]]]
       , "'2014.[m].(d)',['m',3,'d',6], beg='[(', end='])', show='matched'"
       , ["nl",true]
     ]      
    ], mode=mode, opt=opt, scope=["data",data,"tmpl",tmpl]

	)
);
//echo( _s2_test(mode=12)[1] );

//========================================================
sel = ["sel", "arr, indices", "arr", "Array",
"Select items according to indices. 
;;
;; indices could be:
;;
;;    [2,4,1] or [ [0,2,1],[3,2,0]...]"
];

function sel_test( mode=MODE, opt=[] )=
(
	doctest2( sel ,
    [

	],mode=mode, opt=opt

	)
);

////========================================================
//shift= [ "shift", "o,i=1", "str|arr", "String,Array" ,
//" Return a str|arr short than o by one on the right side
//;; = slice(o,0,-1).
//" 
//];
//   
//function shift_test( mode=MODE, opt=[] )=
//(
//	doctest2( shift ,
//    [
//      [ shift([2,3,4]), [2,3] , "[2,3,4]"]
//    , [ shift([2]), [] , "[2]" ]
//    , [ shift([]), [] , "[]" ]
//    , [ shift( "234" ), "23"  , "'234'"]
//    , [ shift( "2" ), ""  , "'2'"]
//    , [ shift( "" ), ""  , "'"]
//	],mode=mode, opt=opt
//
//	)
//);
////shift_test( ["mode",22] );

//========================================================
shortside=["shortside","b,c","number","Geometry",
 " Given two numbers (b,c) as the lengths of the longest side and one of 
;; the shorter sides a right triangle, return the the other short side a.
;; ( c^2= a^2 + b^2 )."
,"longside"
];
function shortside_test( mode=MODE, opt=[] )=
( doctest2( shortside, 
    [
	  [ shortside(5,4), 3, "5,4" ]
	, [ shortside(5,3), 4, "5,3" ]
	], mode=mode, opt=opt
	)
);



////========================================================
//shrink= [ "shrink", "o,i=1", "str|arr", "String, Array",
//"
// Return a str|arr short (on the left side) than o by one.\\
//"
//];
//
//function shrink_test( mode=MODE, opt=[] )=
//(
//	doctest2( shrink, 
//	[
//      [ shrink([2,3,4]), [3,4], [2,3,4] ]
//    , [ shrink([2]), [], [2] ]
//    , [ shrink( "234" ), "34",  "'234'" ]
//    , [ shrink( "2" ), "", "'2'" ]
//    , [ shrink( "" ), "", "'" ]
//    ], mode=mode, opt=opt
//	)
//);
////shrink_test( ["mode",22] );

////========================================================
//shuffle=["shuffle", "arr", "array", "Array",
//" Given an array (arr), return a new one with all items randomly shuffled.
//"];
//
//function shuffle_test( mode=MODE, opt=[] )=
//(
//	let( arr = range(6)
//	   , arr2= [ [-1,-2], [2,3],[5,7], [9,10]] 
//       )
//       
//	doctest2( shuffle,
//	[
//		str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr)= ", shuffle(arr) )
//		,str( "shuffle(arr2)= ", shuffle(arr2) )
//		,str( "shuffle(arr2)= ", shuffle(arr2) )
//		,str( "shuffle(arr2)= ", shuffle(arr2) )
//	]
//    , mode=mode, opt=opt, scope=["arr", arr, "arr2", arr2]
//	)
//);
////shuffle_test();

//========================================================
sinpqr= ["sinpqr","pqr","number","Math,Geometry",
 " Given a 3-pointer (pqr), return the len= sin( RT/QR ) where T is 
;; the R's projection on PQ line (T=othoPt([P,R,Q])).
;;
;;           R
;;          /| 
;;         / |.'
;;        / .'T
;;       /.'
;;  ----Q------
;;    .'
;;   P
"]; 

function sinpqr_test( mode=MODE, opt=[] )=
(
    doctest2( sinpqr,[],mode=mode, opt=opt )
);

//========================================================
slice=[ "slice", "s,i,j", "str|arr", "String,Array" ,
 " Slice s (str|arr) from i to j-1. 
;;
;; - If j not given or too large, slice from i to the end. 
;; - Both could be negative/positive.
;; - If i >j, do a reversed slicing
;;
;;   slice( ''012345'',2 )= ''2345''
;;   slice( ''012345'',2,4 )= ''23''
;;   slice( ''012345'',-2 )= ''45''
;;   slice( [10,11,12,13,14,15],2,20 )= [12, 13, 14, 15] // When out of range
;;
;; Reversed slicing:  
;;
;;   slice( [10,11,12,13,14,15], 5,2 )= [15, 14, 13]
;;   slice( ''012345'', 5,2 )= ''543''
;;   slice( ''012345'', -3,-5 )= ''32''
;;
;; Note that this func doesn't do cycling (i.e., when it is
;; out of range, it doesn't continue from the other end). 
;; Use *range()* for that purpose.
"
];

function slice_test( mode=MODE, opt=[] )=
(
	let( 
    s="012345"
	,a=[10,11,12,13,14,15]
	)
    doctest2( slice, 
	[
	  "## slice a string ##"
	  
	, [ slice(s,2),"2345", "'012345',2"]		 
    , [ slice(s,2,4),"23", "'012345',2,4"]
	, [ slice(s,0,2),"01", "'012345',0,2"]		 
	, [ slice(s,-2),"45","'012345',-2"]		 
	, [ slice(s,1,-1),"1234","'012345',1,-1"]		 
	, [ slice(s,-3,-1),"34","'012345',-3,-1"]		 
	, [ slice(s,3,3),"", "'012345',3,3"]		 
	, [ slice(s,0,0),"", "'012345',0,0"]		 
	, [ slice("",3),"","',3"]		 
	  
	, "## slice an array ##" 	
    , [ slice(a, 2), [12,13,14,15], "[10,11,12,13,14,15],2"]
    , [ slice(a, 2,5), [12,13,14], "[10,11,12,13,14,15],2,5"]
    , [ slice(a, 2,20), [12,13,14,15], "[10,11,12,13,14,15],2,20"
            , ["//", "When out of range"]]
    , [ slice(a, -3), [13,14,15], "[10,11,12,13,14,15],-3"]
    , [ slice(a, -20, -3), [10,11,12], "[10,11,12,13,14,15],-20,-3"
            , ["//", "When out of range"]]
    , [ slice(a, -8, -3), [10,11,12], "[10,11,12,13,14,15],-8,-3"
            , , ["//", "When out of range"]]
    , [ slice([[1,2],[3,4],[5,6]], 2), [[5,6]],"[[1,2],[3,4],[5,6]],2"]
    , [ slice([[1,2],[3,4],[5,6]], 1,1), [], "[[1,2],[3,4],[5,6]], 1,1"]
    , [ slice([], 1), [], "[], 1"]
    , [ slice([9], 1), [], "[9], 1"]

    ,""
	,"// Reversed slicing:"
	,""
    , [ slice(a, 2,5), [12,13,14], "[10,11,12,13,14,15], 2,5"]
    , [ slice(a, 5,2), [15,14,13], "[10,11,12,13,14,15], 5,2"]
    , [ slice(s, 5,2), "543", "'012345', 5,2"]
    , [ slice(s, 3,1), "32", "'012345', 3,1"]
	  
    , [ slice(s, -3,-5), "32", "'012345', -3,-5"]
	] , mode=mode, opt=opt
	)
);
//echo( slice_test()[1] );

//========================================================
slope= [ "slope", "p,q=undef", "num",  "Math,Geometry" ,
" Return slope. slope(p,q) where p,q are 2-D points or
;; slope(p) where p = [p1,p2]\\
"
];
function slope_test( mode=MODE, opt=[] )=
(	
    doctest2( slope, 
     [ 
        [ slope([0,0],[4,2]), 0.5,[[0,0],[4,2]] ]
     ,  [ slope( [[0,0],[4,2]] ), 0.5,[ [[0,0],[4,2]] ] ]
     ,  [ slope( [[0,0],[4,2]] ), 0.5,[ [[1,1],[4,2]] ] ]
     ],mode=mode, opt=opt
    // , [["echo_level", 1]]	
    )
);

//slope_test( ["mode",22] );

//========================================================
split= [ "split", "s, sp=\" \", keep=false, keepempty=true", "arr", "String" ,
"
 Split a string into an array. 
;; 
;; New 2015.2.11: (1) arg keep, set true to keep the sp. Default false: 
;; 
;; split('abc' 'b',false)=> ['a','c']  // keep = false
;; split('abc','b',true) => ['a','b','c'] // keep = true
;;
;; New 2015.3.2: new arg: keepempty=true
;; 
;;  split( ',t',',')= ['','t']
;;  split( ',t',',', keepempty=false )= ['t']
"
];

function split_test( mode=MODE, opt=[] )=
(
	doctest2( split,
    [ 
      [ split("this is it"), ["this","is","it"], "'this is it'"]
    , [ split("this is it","i"), ["th","s ","s ","t"], "'this is it','i'"]
    , [ split("this is it","t"), ["","his is i",""], "'this is it','t'"]
    , [ split("this is it","is"), ["th"," "," it"], "'this is it','is'"]
    , [ split("this is it",","), ["this is it"], "'this is it',','"]
    , [ split("Scadx",","), ["Scadx"], "'Scadx',','"]
    , [ split("Scadx",""), ["Scadx"], "'Scadx',''"]
    , [ split("Scadx","Scadx"), ["",""], "'Scadx','Scadx'"]
    , ""
	, "// keep: keep the sp"
    , ""
    , [ split("abcabc","b"), ["a","ca","c"], "'abcabc','b'"]
    , [ split("abcabc","a",true), ["", "a","bc","a","bc"], "'abcabc','a', keep=true"]
    
//    , [ split("abcabc","a",false), ["a","bc","a","bc"], "'abcabc','a',keepempty=false"]
//    , [ split("abcabc","c",true), ["ab","c","ab","c"], "'abcabc','c',keepempty=true"]
    , ""
    , "// keepempty: keep the empty string. Default:true "
    , ""
    , [ split("tt","t"), ["","",""] , "'ttt','t'"]
    , [ split("tt","t", keepempty=false), [] , "'tt','t' keepempty=false"]
    , [ split(",t",","), ["","t"] , "',t',','"]
    , [ split(",t",",", keepempty=false), ["t"] , "',t',',', keepempty=false"]
    ,""
    , [ split("abcabc","a",false,false), ["bc","bc"], "'abcabc','a', false,false"]
    //, [ split("",",", keepempty=false), [], "'',','"]
    //, [ split("",","), [""], "'',',', keepempty=true"]
    
	], mode=mode, opt=opt
	)
);

//echo( split_test( mode=11 )[1] );


//==================================================
sopt=["sopt","s,df=[],sp=';'","opt","String,Hash,Core,Inspect",
  "string shortcut to set an opt
;;
;;  s : list of k=v or to-be-true flags
;;     'b,c,grid,name=kkk,age=30'
;;
;;  flag could be a property name, like grid, or a shortcut
;;  of that, like c,b, which are defined in df:
;;
;; df: ['b','border','c','chain', 'd','diameter' ... ]
"
,"uopt,subopt1"];

function sopt_test( mode=MODE, opt=[] )=
(
    doctest2( sopt, 
    [
      [sopt("c;d"), ["c",true,"d",true], "'c,d'"]
    , [sopt("c;e=3;f=name;x=[0,1]")
            , ["c",true,"e",3,"f","name","x",[0,1]]
            , "'c,e=3,f=name'"
            , ["nl",true]]
    ,""
    ,"Use <b>df</b> to define a shortcut for key names."
    ,"The name 'e','c' are converted to 'esc','chain',"
    ,"respectively."
    ,"Also note that f='name' is the same as f=name." 
    ,""
    
    , [sopt("c;e=3;f=\"name\"", df=["c","chain","e","esc"])
            , ["chain",true,"esc",3,"f","name"]
            , "'c,e=3,f=\"name\"', df=['c','chain','e','esc']"
            , ["nl",true]]
            
    ]
    , mode=mode, opt=opt )
);
//echo( sopt_test( mode=112)[1] );
//echo(hash("abcd","c") );
//echo(hash("","c","x") );

//========================================================
splits= [ "splits", "s, sp=[], keep=false", "arr", "String",
 " Split a string into an array based on the item in array sp . 
;;
;; splits(''abcdef'',[''b'',''e'']) => [''a'',''cd'',''f'']
;;
;; Developer note: 
;;
;; Most other recursions in this file go through items in a list
;; with a pattern:
;;
;;   _I_&lt;len(arr)
;;   ? ...
;;   : []           // Note this
;; 
;; This code, however, demos a rare case of recursion into layers 
;; of a tree:
;;
;;   _I_&lt;len(arr)
;;   ? ...
;;   : arr          // major difference
;;
;; 2015.2.11: New arg: keep=false
"];
function splits_test( mode=MODE, opt=[] )=
(
	//s="this is it";
	let( s="abc_def_ghi" )
	doctest2( splits,
	[ 
      "var s"
    , [ splits(s,["b"]), ["a", "c_def_ghi"], "s,['b']"]
	, [ splits(s,["b","_"]), ["a", "c","def", "ghi"],"s1,['b','_']"]	
	, [ splits(s,["_","b"]), ["a", "c","def", "ghi"], "s1,['_','b']"]	
    , [ splits(s,["b","_","e"]), ["a", "c","d","f", "ghi"], "s1,['b','_','e']"]
    , "// New 2015.2.11: argument keep"
    , [ splits("abcabc",["b","c"]), ["a","a"], "'abcabc',['b','c']"]
    , [ splits("abcabc",["c","b"],true)
            , ["a","b","c","a","b","c"], "'abcabc',['c','b'],keep=true"]
    , [ splits("abcabc",["b","c"],true)
            , ["a","b","c","a","b","c"], "'abcabc',['b','c'],keep=true"]
    , "// New 2015.3.2: argument keepempty"
    , [ splits("abcabc",["b","c"],keepempty=true), ["a","","a","",""], 
            "'abcabc',['b','c'],keepempty=true"
       ]
//    , [ splits( "[3,[a,4],[5,7]]", ["[",",","]"],keep=true)
//          ["[",3,
       
	], mode=mode, opt=opt, scope=["s",s]

	)
);

//echo( splits_test(mode=112)[1] );

//========================================================
squarePts=["squarePts", "pqr,p=undef,r=undef", "pqrs", "Point",
" Given a 3-pointer pqr, return a 4-pointer that has p,q of the
;; given pqr plus two pts, together forms a square passing through
;; r or has r lies along one side. This is useful to find the
;; square defined by p,q,r.
;;
;; Given [p,q,r], return [P,Q,N,M]
;;   P-------Q
;;   |      /|
;;   |     / |
;;   |    /  |
;;   M---R---N
;;
;;   P------Q
;;   |'-_   |:
;;   |   '-_| :
;;   |      '-_:
;;   |      |  ':
;;   M------N    R
;;
;; New 2014.7.15: new OPTIONAL arg: p, r to set size. 
;;
;;          p  
;;   P---|-----Q
;;   |   |    /| 
;;   |   |   / | r
;;   |   +--/---
;;   |     /   |
;;   '----R----'
"
];

function squarePts_test( mode=MODE, opt=[] )=
(
	let( pqr = randPts(3)
	   , sqs = squarePts( pqr )
       )
	
    doctest2( squarePts, 
	[
	  [ is90( [for(i=[0,1,2]) sqs[i] ] ) , true, "pqr"
		, ["myfunc", "is90( [ for(i=[0,1,2]) squarePts{_}[i] ] ) "]
	  ]

	  ,[ is90( [for(i=[1,2,3]) sqs[i] ] ), true, "pqr", 
		, ["myfunc", "is90( [ for(i=[1,2,3]) squarePts{_}[i] ] )"]
	  ]

	,[ is90( [for(i=[2,3,0]) sqs[i] ] ), true, "pqr", 
		, ["myfunc", "is90( [ for(i=2,3,0]) squarePts{_}[i] ] )"]
	  ]
	], mode=mode
     )
);

//function subops_test( opt=[] )=
//(
//    /*
//       Let an obj has 2 sub unit groups: arms and legs, each has L and R
//       Obj: arms{armL,armR}
//          : legs{legL,legR}
//       
//       com_keys for arms: ["color","r"]
//       com_keys for legs: ["color","len"]
//       
//       
//    
//    
//    */
//    //=========================================
//    //=========================================
//    
//    function tester( 
//    // The following are to set defaults on the fly
//    //   for checking how different default settings work.
//    //   I.e., in real use, don't do it this way.
//    // Usage: as a test rec: 
//    //   [
//              u_ops
//              , df_color = undef
//              , df_r     = undef
//              , df_len   = undef
//              , df_show_arms = undef 
//              , df_show_armL = undef 
//              , df_show_armR = undef
//              , df_show_legs = undef
//              , df_show_legL = undef
//              , df_show_legR = undef 
//              
//              , df_arms = undef 
//              , df_armL = undef 
//              , df_armR = undef
//              , df_legs = undef
//              , df_legL = undef
//              , df_legR = undef 
//              
//              , com_keys_arms= ["color","r"]
//              , com_keys_legs= ["color","len"]
//    ){
//        df_ops= updates(
//            [
//              df_color==undef? []:["color", df_color]
//            , df_r    ==undef? []:["r",     df_r]
//            , df_len  ==undef? []:["len",   df_len]
//        
//            , df_show_arms ==undef?[]:["df_arms", df_show_arms]
//            , df_show_armL ==undef?[]:["df_armL", df_show_armL]
//            , df_show_armR ==undef?[]:["df_armR", df_show_armR]
//            , df_show_legs ==undef?[]:["df_legs", df_show_legs]
//            , df_show_legL ==undef?[]:["df_legL", df_show_legL]
//            , df_show_legR ==undef?[]:["df_legR", df_show_legR]
//            ]
//        );
//        
//        function get_ops( df_subs, com_keys )=
//            subops( u_ops
//                  , df_ops = df_ops
//                  , df_subs= df_subs
//                  , com_keys=com_keys
//                  );
//        function get_arm_ops( armname, df)=
//            get_ops( df_subs= [ "arms", df_arms, armname,df]
//                   , com_keys= com_keys_arms
//                   );
//        function get_leg_ops( legname, df)=
//            get_ops( df_subs= [ "legs", df_legs, legname,df]
//                   , com_keys= com_keys_legs
//                   );
//                   
//        // We need to set 4 ops:
//        
//        ops_armL = get_arm_ops( "armL", df_armL );
//        ops_armR = get_arm_ops( "armR", df_armR );
//        ops_legL = get_leg_ops( "legL", df_legL );
//        ops_legR = get_leg_ops( "legR", df_legR );
//            
//        
//    }
////    
////    opt=[];
////    df_arms=["len",3,"r",1];
////    df_armL=["fn",5];
////    df_armR=[];
////    df_ops= [ "len",2
////            , "arms", true
////            , "armL", true
////            , "armR", true
////            ];
////    com_keys= ["fn"];
////    
////    /*
////        setops: To simulate obj( ops=[...] )
////        args  : ops  
////                // In real use, following args are set inside obj()
////                df_ops
////                df_subops // like ["arms",[...], "armL",[...]] 
////                com_keys 
////                objname   // like "obj","armL","armR"          
////                propname  // like "len"
////    */
////    function setops(   
////            ops=opt
////            , df_ops=df_ops
////            , df_subops=[ "arms",df_arms, "armL",df_armL,"armR",df_armR]
////            , com_keys = com_keys
////            , objname ="obj"
////            , propname=undef
////            )=
////        let(
////             _ops= update( df_ops, ops )
////           , df_arms= hash(df_subops, "arms")  
////           , df_armL= hash(df_subops, "armL")  
////           , df_armR= hash(df_subops, "armR")  
////           , ops_armL= subops( _ops, df_ops = df_ops
////                    , sub_dfs= ["arms", df_arms, "armL", df_armL ]                    
////                    , com_keys= com_keys
////                    )
////            , ops_armR= subops( _ops, df_ops = df_ops
////                        , sub_dfs= ["arms", df_arms, "armR", df_armR ]
////                        , com_keys= com_keys
////                        )
////            , ops= objname=="obj"?_ops
////                    :objname=="df_ops"?df_ops
////                    :objname=="armL"?ops_armL
////                    :ops_armR
////            )
////        // ["ops",_ops,"df_arms", df_arms, "ops_armL",ops_armL, "ops_armR",ops_armR];
////        propname==undef? ops:hash(ops, propname);
////      
////     /**
////        Debugging Arrow(), in which a getsubops call results in the warning:
////        DEPRECATED: Using ranges of the form [begin:end] with begin value
////        greater than the end value is deprecated.
////            
////     */       
////     function debug_Arrow(ops=["r", 0.1, "heads", false, "color","red"])=
////        let (
////        
////           df_ops=[ "r",0.02
////               , "fn", $fn
////               , "heads",true
////               , "headP",true
////               , "headQ",true
////               , "debug",false
////               , "twist", 0
////               ]
////          ,_ops= update(df_ops,ops) 
////          ,df_heads= ["r", 1, "len", 2, "fn",$fn]    
////          , ops_headP= subops( _ops, df_ops=df_ops
////                , com_keys= [ "color","transp","fn"]
////                , sub_dfs= ["heads", df_heads, "headP", [] ]
////                ) 
////        )
////        ops_headP;
////        
////     doctest2(subops
////        , [ ""
////            ,"In above settings, ops is the user input, the rest are"
////            ,"set at design time. With these:"
////            ,""
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops"
////                 , setops()
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops_armL"
////                 , setops(objname="armL")
////                 , ["len", 3, "r", 1, "fn", 5]]
////            ,["ops_armR"
////                 , setops(objname="armR")
////                 , [ "len", 3, "r", 1]]
////                            
////            ,""
////            ,"Set r=4 at the obj level. Since r doesn't show up in com_keys, "
////            ,"it only applies to obj, but not either arm. "
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true],]
////            ,["ops"
////                 , setops(ops=["r",4])
////                 , ["len",2,"arms",true,"armL", true, "armR", true,"r",4],]
////            ,["ops_armL"
////                 , setops(ops=["r",4],objname="armL")
////                 , ["len", 3, "r", 1,"fn", 5]]
////            ,["ops_armR"
////                 , setops(ops=["r",4],objname="armR")
////                 , [ "len", 3, "r", 1]]
////                            
////            ,""
////            ,"Set fn=10 at the obj level. Since fn IS in com_keys, "
////            ,"it applies to obj and both arms. "
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true],]
////            ,["ops"
////                 , setops(ops=["fn",10])
////                 , ["len",2,"arms",true,"armL", true, "armR", true, "fn",10],]
////            ,["ops_armL"
////                 , setops(ops=["fn",10],objname="armL")
////                 , ["len", 3, "r", 1, "fn", 10]]
////            ,["ops_armR"
////                 , setops(ops=["fn",10],objname="armR")
////                 , ["len", 3, "r", 1, "fn", 10]]  
////                 
////        
////            ,""
////            ,"Disable armL:"
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops"
////                 , setops(ops=["armL",false])
////                 , ["len",2,"arms",true,"armL", false, "armR", true]]
////            ,["ops_armL"
////                 , setops(ops=["armL",false],objname="armL")
////                 , false]
////            ,["ops_armR"
////                 , setops(ops=["armL",false],objname="armR")
////                 , ["len", 3, "r", 1]]
////
////
////            ,""
////            ,"Disable both arms:"
////            ,""
////            
////            ,["df_ops"
////                 , setops(objname="df_ops")
////                 , ["len",2,"arms",true,"armL", true, "armR", true]]
////            ,["ops"
////                 , setops(ops=["arms",false])
////                 , ["len",2,"arms",false,"armL", true, "armR", true]]
////            ,["ops_armL"
////                 , setops(ops=["arms",false],objname="armL")
////                 , false]
////            ,["ops_armR"
////                 , setops(ops=["arms",false],objname="armR")
////                 , false]
////        
////        
//////            ,""
//////            ,"Debugging Arrow(), in which a getsubops call results in the warning:"
//////            ,"DEPRECATED: Using ranges of the form [begin:end] with begin value greater than the end value is deprecated."
//////            ,""
//////            
//////            ,["debug_Arrow(ops=['r', 0.1, 'heads', false, 'color','red']"
//////             , debug_Arrow(ops=["r", 0.1, "heads", false, "color","red"]),[]
//////             ]
////                            
////          ]
////          
////        , ops, [ "df_ops",df_ops, 
////               , "df_arms", df_arms
////               , "df_armL", df_armL
////               , "df_armR", df_armR
////               , "com_keys", com_keys
////               , "ops",opt
////               ]
////    )
//);

////========================================================
//sum=["sum", "arr", "number", "Math, Array",
//"Return the sum of arr's items, which could be pts.  
//
//"
//];
//function sum_test( mode=MODE, opt=[] )=
//(
//    doctest2( sum, 
//    [
//      [ sum([2,3,4,5]),14, [2,3,4,5]]
//    , [ sum([[1,2,3],[4,5,6]]),[5,7,9], [[1,2,3],[4,5,6]]]
//    ], mode=mode, opt=opt )
//);
//
////echo( sum_test(mode=12)[1] );
//
////========================================================
//sumto =[  "sumto", "n", "int", "Math" ,
//"
// Given n, return 1+2+...+n 
//"
//];
//
//function sumto_test( mode=MODE, opt=[] )=
//(
//    doctest2( sumto, 
//    [
//      [ sumto(5), 15, "5" ]
//    , [ sumto(6), 21, "6" ]
//    ], mode=mode, opt=opt )
//);


////========================================================
//switch=["switch","arr,i,j","array", "Array",
// " Given an array and two indices (i,j), return a new array
//;; with items i and j switched. i,j could be negative. If either
//;; one out of range, return undef.
//"];
//
//function switch_test( mode=MODE, opt=[] )=
//(
//	let( arr=[2,3,4,5,6] )
//    
//	doctest2( switch,
//	[		
//      [ switch(arr,1,2), [2,4,3,5,6] , "arr,1,2"]
//    , [ switch(arr,0,2), [4,3,2,5,6] , "arr,0,2"]
//    , [ switch(arr,0,-1), [6,3,4,5,2] , "arr,0,-1"]
//    , [ switch(arr,-1,-5), [6,3,4,5,2] , "arr,-1,-5"]
//    , [ switch(arr,-1,4), [2,3,4,5,6], "arr,-1,4", ["//","No switch"] ]
//
//	],mode=mode, opt=opt, ["arr", arr]
//
//	)
//);


////========================================================
//function subarr_test(mode=112)
//{
//	arr=[10,11,12,13];
//	doctest2( subarr,
//	[
//		 ["arr", subarr( arr ), [[13,10,11],[10, 11,12],[11,12,13],[12,13,10]] ]
//		,["arr,cycle=false", subarr( arr,cycle=false ), [[10,11],[10, 11,12],[11,12,13],[12,13]] ]
//
//		,["arr,cover=[0,1]", subarr( arr,cover=[0,1] ), [[10,11],[11,12],[12,13],[13,10]] ]
//		,["arr,cover=[2,0]", subarr( arr,cover=[2,0] ), [[12,13,10],[13,10,11],[10,11,12],[11,12,13]] ]
//	],mode=mode, ["arr",arr]
//	);
//}


//========================================================
symmedPt=["symmedPt","pqr","Point","Point"
, "Return the symedian pt of pqr, which is the S below:
;; 
;;    C---A---S
;;
;; where C = centroid pt, A= angleBisectPt of pqr.
"];

function symmedPt_test( mode=MODE, opt=[] )=
(
    doctest2( symmedPt,[],mode=mode, opt=opt )
);

////========================================================
//transpose= [ "transpose", "mm", "mm",  "Array" ,
//"Given a multmatrix, return its transpose.
//"
//];
//
//function transpose_test( mode=MODE, opt=[] )=
//(
//	let( mm = [[1,2,3],[4,5,6]]
//	   , mm2= [[1,4],[2,5],[3,6]]
//       )
//       
//	doctest2( transpose,
//    [
//      [  transpose(mm), mm2, mm ]
//    , [  transpose(mm2), mm, mm2 ]
//    ]
//    , mode=mode, opt=opt
//	)
//);
////transpose_test( ["mode",22] );

//========================================================
trim=["trim", "o, l=' ', r=' '", "arr/str", "Array, String",
, "Given o as a str/arry, trim the string or strings in arr on either
;; or both left and right. The characters to be trimmed away are 
;; defined in l or r. If defined as l='abc', all the chars a,b,c
;; on the far left will be trimmed: 
;;
;; l = 'abc'
;; 'abcxyz' => 'xyz'
;; 'bcaxyz' => 'xyz'
;; 'cbaxyz' => 'xyz'

"];

function trim_test( mode=MODE, opt=[] )=
(
    doctest2( trim,
    [
     [ trim("abcxyz",l="abc"), "xyz", "'abcxyz'"]
    , [ trim("bcaxyz",l="abc"), "xyz", "'bcaxyz'"]
    , [ trim("cabxyz",l="abc"), "xyz", "'cabxyz'"]
    , [ trim("  abc "), "abc", "'  abc '"]
    , [ trim(",abc!", l=",", r="!" ), "abc", "',abc!',l=',',r='!'"]
    , [ trim(", abc! . ", l=" ,", r="!. " ), "abc", "', abc! . ', l=' ,', r='!. '"]
    , [ trim([" a, ", ", b"], l=" ,.", r=" ,."), ["a","b"], "['a,',',b'], l=',.', r=' ,.'" ] 
    , [ trim(["# a, ", 3, undef, true], l=" #", r=", ."), ["a",3,undef,true]
        , "['# a, ', 3, undef, true], l=' #', r=', .'"] 
    ],mode=mode, opt=opt )
);   

//========================================================
trfPts=["trfPts", "pqr,pqr2", "n/a", "Geometry",
" A module to transform an obj from a coordnate (defined by pqr)
;; to another (pqr2). Args pqr, pqr2 are any 3-pointer.
;; 
"];

function trfPts_test( mode=MODE, opt=[] )=
(
    doctest2( trfPts,[],mode=mode, opt=opt )
);

//========================================================
twistangle=["twistangle","pqrs", "number", "Angle",
 " Given a 4-pointer (pqrs), looking down from Q to R (using QR
;; as the axis), return the angle representing how much the line_RS
;; twists away from line_PQ. 
;;
;; The following graphs looks down from Q to R: 

;;  +-----:-----+   a: 0~90
;;  :     :  S  :  
;;  :     : /   : 
;;  :     :R    :
;;  ------Q-----P 

;;  +-----:-----+   a:90~180
;;  : S   :     :   
;;  :  '. :     : 
;;  :    R:     :
;;  ------Q-----P 

;;  ------Q-----P  a: 0~ -90
;;  :     :R    :
;;  :     : '.  :
;;  :     :   S :
;;  +-----:-----+ 

;;  ------Q-----P  a: -90~ -180
;;  :    R:     :
;;  :   / :     :
;;  :  S  :     :
;;  +-----:-----+ 
;;
;;  a_PAB + a_DCS      D   S
;;                     :  /|
;;                     : / |
;;                F    :/  |   
;;                :    /   |  .'
;;           E    :   /:  .|'
;;           :    :  / :.' |
;;  P    B   :    : / .C---:
;;  |'.  |   :    :/.'_.-''
;;  |  '.| --:----R--'------
;;  |    |.  :  .'
;;  |    | '.:.'
;; -|----|-_.Q -------    
;;  |.'_.|.'
;;  '----A'
;;     .'

"];

function twistangle_test(mode=MODE, opt=[] )=
(
    let ( Q = [2,0,0]
        , R = [-2,0,0]
        )
	doctest2( twistangle
    ,[ 
       "", "var Q", "var R"
     , [twistangle( [[3,1,1],Q],[R,[-3,-1,1]] ), 90
        , "[[3,1,1],Q],[R,[-3,-1,1]]"]
	 , [twistangle( [3,1,1],Q,R,[-3,1,1] ), 0
        , "[3,1,1],Q,R,[-3,1,1]", ["prec",6]]
	 , [twistangle( [[3,0,1],Q,R,[-3,-1,1]] ), 45
        , "[[3,0,1],Q,R,[-3,-1,1]]"]
	 , [twistangle( [3,-1,1],Q,R,[-3,1,1] ), -90
        , "[3,-1,1],Q,R,[-3,1,1]"]
	 ]
    , mode=mode, opt=opt, scope=["Q",Q,"R",R]
	)
);

//echo( twistangle_test( mode=12 )[1] );

//module twistangle_demo()
//{	
//	pqrs = randPts(4);
//	P=P(pqrs); Q=Q(pqrs); R=R(pqrs);
//	S=pqrs[3];
//	
//	MarkPts([P,Q,R,S],["labels","PQRS"]);
//	Chain( [P,Q,R,S ] );
//	//Line0( [Q,S] );
//
//	echo( "quad:", quadrant( [Q,R,P],S), "twistangle:", twistangle( [P,Q,R, S] ));
//
//}
//twistangle_demo();



//========================================================
type=[ "type", "x", "str", "Type, Inspect" ,
"
 Given x, return type of x as a string.
"
];
function type_test( mode=MODE, opt=[] )=
(
	doctest2( type,
    [  
     [ type([2,3]), "arr" , [2,3]]
    , [ type([]), "arr" , "[]"]
    , [ type(-4), "int", "-4"]
    , [ type( 0 ), "int", "0"]
    , [ type( 0.3 ), "float", "0.3"]
    , [ type( -0.3 ), "float", "-0.3"]
    , [ type("a"), "str", "'a'"]
    , [ type("10"), "str", "'10'"]
    , [ type(true), "bool", "true"]
    , [ type(false), "bool", "false"]
    , [ type(undef), undef, "undef"]

    ],mode=mode, opt=opt

	)
);
//type_test( ["mode",22] );

//========================================================
typeplp=["typeplp","x,dim=3","pt|ln|pl|false", "Type,Inspect",
"Check if x is one of ('pt','ln','pl',false), indicating that
;; x is a point, line, plane, or none of them, respectively"
];

function typeplp_test( mode=MODE, opt=[] )=
(
    let( P=[2,3,4]
       , Q=[5,6,7]
       , R=[3,5,7]
       )
    
    doctest2( typeplp,
    [
      [typeplp( P ), "pt", "P"]
    , [typeplp( [P,Q] ), "ln", "[P,Q]"]
    , [typeplp( [P,Q,R] ), "pl", "[P,Q,R]"]
    , [typeplp( "a" ), false, "'a'"]
    , [typeplp( [P,Q,R,P] ), false, "[P,Q,R,P]"]
    ], mode=mode, opt=opt, scope=["P",P,"Q",Q,"R",R]
    )
);

//echo( typeplp_test()[1] );

//========================================================
ucase=["ucase", "s","str","String",
 " Convert all characters in the given string (s) to upper case. "
];
function ucase_test( mode=MODE, opt=[] )=
(
	doctest2( ucase,
	[
		[ucase("abc,Xyz"), "ABC,XYZ", "'abc,Xyz'", ]
	], mode=mode, opt=opt
	)
);

//========================================================
uconv=["uconv", "n, units=''in,cm'', utable=UCONV_TABLE)", "number", "Unit",
" Convert n's unit based on the *units*, which has default ''in,cm''. 
;; The conversion factor is stored in a constant UCONV_TABLE. To use
;; something not specified in UCONV_TABLE, do this:
;;
;;   uconv(n, ''u1,u2'', factor )
;;
;; Or you can define own table:
;;
;;   table=[''u1,u2'',factor]
;;   uconv( n, ''u1,u2'', table )
;;
;; n can be a string like \"2'8''\". If so the arg *from* is ignored.
;;
;; Related:
;;
;; num(''3'6\"'')= 3.5  // in feet
;; numstr( 3.5, conv=''ft:ftin'') = ''3'6\"'' // return str
;; ftin2in(''3'6\"'') = 42 
"];

function uconv_test( mode=MODE, opt=[] )=
(
	doctest2( uconv,
	[
     [ uconv(3,"cm,in"), 3/2.54 , "3,'cm,in'"]
    , [ uconv(100,"cm,ft"), 100/2.54/12, "100, 'cm,ft'"]
	  ,"// Use own units:"
    , [ uconv(100,"cm,aaa"), undef, "100, 'cm,aaa'"]
    , [ uconv(100,"cm,aaa", ["cm,aaa",2]), 200, ["//","Use your factor"], "100, 'cm,aaa', ['cm,aaa',2]"]
	  ,"// Simply set units= the conv factor: "
    , [ uconv(100,utable=3), 300, "100, utable=3", ["//","Enter a conv factor"]]
	   
     ,"// When n is a string like 2'8', arg *from* is ignored:"
    , [ uconv("1'3\"", ",in"), 15, "'1'3', 'in'"]
    , [ uconv("1'3\"", ",cm"), 15*2.54, "'1'3', 'cm'"]
    , [ uconv("1'3\"", ",ft"), 15/12, "'1'3', 'ft'"]
    , [ uconv("1'3\"", ",aaa"), undef, "'1'3', 'aaa'"]
 
	],mode=mode, opt=opt )
);

//==================================================
uopt=["uopt","opt1,opt2,df=[]","opt","String,Hash,Core,Inspect",
  "Merge sopt and opt allowing module to use both.
;;
;; See sopt() for definition of sopt.
;;
;;  MarkPts( pts, 'l,c,color=red', ['grid',true] )
;;  MarkPts( pts, ['color','red'], 'l,c,color=red' )
;;      
;;  The shortcut definition, df=['l','label','c','chain']
;;  is defined inside MarkPts()
;; 
;;  <b>Internal sopt</b>:
;; 
;;  The 1st 2 items are internal sopt:
;;
;;  opt = [<b>'sopt','l,c,r=3'</b>, 'color', 'red']
;;
"
,"sopt,subopt1"
];

function uopt_test( mode=MODE, opt=[] )=
(
    doctest2( uopt, 
    [
      [uopt("c,d"), ["c",true,"d",true], "'c,d'"]
    , [uopt(["a",5]), ["a",5], ["a",5]]
    , [uopt("c,r=3",["a",5])
            , ["c",true,"r",3,"a",5]
            , "'c,r=3',['a',5]"
            , ["nl",false]]
    , [uopt(["a",5],"c,r=3")
            , ["a",5,"c",true,"r",3]
            , "['a',5], 'c,r=3'"
            , ["nl",false]]
    , ""
    , "// Define shortcuts using df: r represents rad"
    , "// Note that 'a' is not changed to 'age' `cos "
    , "// it`s on an hash but not a sopt."
    , ""
    , [uopt(["a",5],"c,r=3",df=["a","age","r","rad","c","chain"])
            , ["a",5,"chain",true,"rad",3]
            , "['a',5],'c,r=3',df=['a','age','r','rad','c','chain']'"
            , ["nl",true]]

            
    ,""
    ,"<b>Using internal sopt </b>"
    ," " 
    ,"The following case has opt1=hash, opt2=sopt. However,"
    ,"opt1 also contains an <b>internal sopt</b>:"
    ,""
    ,"  ['a',5,<b>'sopt','d,e=1'</b>]"
    ,""
    ,"This is for cases of subobj opt:"
    ,""
    ,"  Rod(.... opt=['markpts', ['sopt','g,c','label'...]] )"
    ,""    
    ,"Let <b>df</b>=['d','date','r','rad','c','chain']"
    ,""
    , [uopt( ["a",5,"sopt","d,e=1"],"c,r=3"
           , df=["d","date","r","rad","c","chain"]
           )
            , ["a",5,"date",true, "e",1, "chain",true,"rad",3]
            , "['a',5,'sopt','d,e=1'],'c,r=3',df"
            , ["nl",true]
      ]
    ]
    , mode=mode, opt=opt )
);
//echo( uopt_test( mode=112 )[1] );


//========================================================
update= ["update", "h,h1,df=[],sp=';'", "hash", "Hash" ,
"
 Given two hashes (h,h1) update h with items in h1.
"
];
function update_test( mode=MODE, opt=[] )=
(
    let( a1= ["a",1,"b",2,"c",3] )
    
	doctest2( update,
	[
      ""
    ,[ update( [1,11],[2,22] ), [1,11,2,22]  , "[1,11],[2,22]"]
    , [ update( [1,11,2,22], [2,33] ), [1,11,2,33]  , "[1,11,2,22],[2,33]"]
    , [ update([1,11,2,22],[1,3,3,33]), [1,3,2,22,3,33], "[1,11,2,22],[1,3,3,33]"]
    , [ update([1,11,2,22],[]), [1,11,2,22], "[1,11,2,22],[]"]
	 , ""
	 , " Clone a hash: "
	 , ""
    , [ update( [],[3,33] ), [3,33]  , "[],[3,33]"]
	 , ""
	 , " Show that the order of h2 doesn't matter"
	 , ""
     , str("a1 = ", a1)
	 , [ update( a1,["b",22,"c",33] )
		, ["a",1,"b",22,"c",33], "a1, ['b',22,'c',33]"
		 ]
	 , [ update( a1, ["c",33,"b",22] )
		,["a",1,"b",22,"c",33], "a1, ['c',33,'b',22]"
		]
	 , ""
	 , "2015.6.3: can take <b>sopt</b> or <b>mopt</b>"
	 , ""
     , "// update a sopt w/ a popt:"
     , [ update("a;b",["c",1])
          , ["a",true, "b", true, "c",1]
          , "'a;b',['c',1]"
       ]   
     
     , "// update a popt w/ a sopt"  
     , [ update(["c",1],"a=2;x=[0,1]")
          , ["c",1,"a",2, "x",[0,1]]
          , "['c',1],'a=2;x=[0,1]'"
       ]  
  
     , "// update a popt w/ a mopt " 
     , [ update(["c",1],["a;b","x",[0,1]])
          , ["c",1,"a",true,"b",true, "x",[0,1]]
          , "['c',1],['a;b','x',[0,1]]"
          , ["nl",true]
       ]    

    ,""
    ,"Use <b>df</b> to define a shortcut for key names."
    ,"  The following <b>df</b> intends to define 'c','a'"
    ,"  and 'x' as 'cycle', 'angle' and 'Y', respectively."
    ,"  Only 'a'=>'angle' works, `cos <b>df</b> only works for"
    ,"  sopt."
    ,""
    
     , [ update(["c",1],["a;b","x",[0,1]]
             , df=["c","cycle","a","angle","x","Y"])
       , ["c",1,"angle",true,"b",true, "x",[0,1]]
       , "['c',1],['a;b','x',[0,1]] <br/>| 
          ,df=['c','cycle','a','angle','x','Y']"
       , ["nl",true]
       ]    
       
     , ""
	 , "Customize <b>sp</b> for sopt:"
	 , ""
     , [ update( "a/b=1","x=[0,1]/d", sp="/")
       , ["a", true, "b",1,"x",[0,1],"d",true]
       , "'a/b=1','x=[0,1]/d', sp='/'"
       , ["nl",true]
       ]
       
    ], mode=mode, opt=opt 
	)
);

//echo( update_test( mode=12 )[1]);

//========================================================
updates= ["updates", "h,hs", "hash", "Hash" ,
"
 Given a hash (h) and an array of multiple hashes (hs),
;; update h with hashes one by one.
;; 
;; New 2015.2.1: no hs, enter h as a one-layer array: updates(hs).
;; That is, updates the hs[0] with the rest in hs
"
];

function updates_test( mode=MODE, opt=[] )=
( 
    let( a1= [1,11,2,22]
       , a2= [1,3,3,33]
       , a3= ["a",1,"b",2,"c",3]
       )
	doctest2( updates	  
	,[
      ""
      ,str("a1=", a1)
	  ,str("a2=", a2)
	  ,[ updates( a1,[[2,20],a2] )
		, [1, 3, 2, 20, 3, 33] 
        , "a1,[[2,20],a2]"
	   ]	 
	 , [ updates( a1,[[1,3,3,33],[2,20]] )
		, [1, 3, 2, 20, 3, 33] 
        , "a1,[a2,[2,20]]"
		
 	   ]	 
	 , [ updates( a1,[[1,3,3,33],[],[2,20]] )
		, [1, 3, 2, 20, 3, 33]
        , "a1, [a2,[],[2,20]]"
	   ]	 
	 , [ updates( a1,[[1,3,3,33,2,20]] )
		, [1, 3, 2, 20, 3, 33] 
        , "a1, [[1,3,3,33,2,20]]"
	   ]	 
	 , [ updates( a1,[[1,3,3,33],[2,20],[2,12,4,40]] )
		, [1,3, 2,12, 3,33, 4,40] 
        ,"a1,[a2,[2,20],[2,12,4,40]]"
	   ]
    , [ updates( [],[[1,3,3,33]] )
		, [1,3,  3,33] 
        , "[],[a2]"
	   ]
    , [ updates( [],[[1,3,3,33],[2,20]] )
		, [1,3, 3,33, 2,20] 
        , "[],[a2,[2,20]]"
	   ]		 
    , [ updates( [],[[1,3,3,33],[2,20],[2,12,4,40]] )
		, [1,3, 3,33, 2,12, 4,40] 
        , "[],[a2,[2,20],[2,12,4,40]]"
	   ]		
     ,""
     ,"New 2015.2.1: enter (h,hs) as a one-layer array: updates(hs). That is, "
     ," updates the hs[0] with the rest in hs"
     ,""
     , str("a3=", a3)
	 , [updates( [a3,["a",11,"d",4],["c",33,"e",5] ])
        ,["a",11,"b",2,"c",33,"d",4,"e",5]
        ,"[ a3, ['a',11,'d',4],['c',33,'e',5] ]", ["nl",true]
       ] 
     , [updates( [a3,[],["c",33,"e",5] ])
        ,["a",1,"b",2,"c",33,"e",5]
        //,[a3,[],["c",33,"e",5] ]
        ,"[ a3,[],['c',33,'e',5] ]", ["nl",true]
       ] 
     , [updates( [[],["a",11,"d",4],["c",33,"e",5] ])
        ,["a",11,"d",4,"c",33,"e",5]
        ,[[],["a",11,"d",4],["c",33,"e",5] ], ["nl",true]
        //, "[[], ['a',11,'d',4],['c',33,'e',5] ]"
       ]     
     ,""
     ,"New 2015.6.3: can take sopt and mopt (other than popt)"
     ,""
   
     , [ updates( ["a;b",["c",1],["x=[0,1];d", "c",5]] )
       , ["a",true,"b",true,"c",5,"x",[0,1],"d",true]
       , ["a;b",["c",1],["x=[0,1];d", "c",5]]
       , ["nl",true]
       ]
        
	 ], mode=mode, opt=opt
	)
);
//echo( updates_test( mode=12 )[1]);


//========================================================
uv=["uv", "pq", "pt", "Geometry",
" Return the unit vector (len = 1 ) of pq "
];

function uv_test( mode=MODE, opt=[] )=
(
	doctest2( uv,
	[
	], mode=mode, opt=opt
	)
);


//========================================================
vals=[  "vals", "h", "arr",  "Hash",
"
 Return the vals of a hash as an array.
"
];

function vals_test( mode=MODE, opt=[] )=
(
	doctest2( 
	vals, [
		[ vals(["a",1,"b",2]), [1,2], "['a',1,'b',2]" ]
	  ,	[ vals([1,2,3,4]), [2,4], [1,2,3,4] ]
	  ,	[ vals([[1,2],3, 0,[3,4]]), [3,[3,4]],[[1,2],3, 0,[3,4]] ]
	  ,	[ vals([]), [],[] ]
	  ], mode=mode, opt=opt
	//, [["echo_level",1]]
	)
);
//vals_test( ["mode",22] );


//======================================
function vlinePt_test( mode=MODE, opt=[] )=
(
	doctest2( vlinePt, mode=mode, opt=opt )
);


//======================================
lineRotation=[ "lineRotation", "pq,p2", "?", "Angle"
, " The rotation needed for a line
;; on X-axis to align with line-p1-p2
"];

// The rotation needed for a line
// on X-axis to align with line-p1-p2
// 
function lineRotation(p1,p2)=
(
 [ 0,-sign(p2[2]-p1[2])*atan( abs((p2[2]-p1[2])
		                    / slopelen((p2[0]-p1[0]),(p2[1]-p1[1]))))
    , p2[0]==p1[0]?0
	  :sign(p2[1]-p1[1])*atan( abs((p2[1]-p1[1])/(p2[0]-p1[0])))]
);
  

//
// The funcs here are usually called by scadx_doctesting_dev.scad.
// To test them directly in this file, un-mark the following line:
//include <../doctest/doctest_dev.scad>
//
//FORHTML=false;
//MODE=112;

/* 
=================================================================
                    scadx_core_test.scad
=================================================================
*/
/*
      fname = doct[ iFNAME ]  // function name like "sum"
	, args  = doct[ DT_iARGS ]   // argument string like "x,y"
  //, rtntype = doct[ DT_iTYPE ] // return type
    , tags  = doct[ DT_iTAGS ]   // like, "Array, Inspect"
  //, doc   = doct[ DT_iDOC ]    // documentation (str)
    , rels  = doct[ DT_iREL ]
*/


//===================================================
//===================================================
//===================================================

module do_tests( title="Doctests for scadx_core"
                , mode=MODE, opt=["showmode",false] ){
    
    results= [
        accum_test( mode, opt ) //[math]
       ,addx_test( mode, opt ) // [geo]
       ,addy_test( mode, opt ) 
       ,addz_test( mode, opt )
       ,all_test( mode, opt ) //[core]
       ,angle_test( mode, opt ) //[geo]
       ,angle_bylen( mode, opt )  //[geo] ///// 
       ,angleBisectPt_test( mode, opt ) //[geo]
       , anglePt_test( mode, opt ) //[geo]            
       ////, anyAnglePt_test( mode, opt ) // to be replaced by anglePt. 2015.5.7             
       , app_test( mode, opt ) //[core]            
       , appi_test( mode, opt ) //[core]            
       , arcPts_test( mode, opt )  //[geo]           
       , arrblock_test( mode, opt ) //[echo]
       , arrprn_test( mode, opt )   //[echo]
       //, assert_test( mode, opt ) //[debug] /////             
       , begmatch_test( mode, opt ) //[match]            
       , begwith_test( mode, opt )  //[insp]           
       , begword_test( mode, opt )  //[insp]           
       , between_test( mode, opt )  //[range]           
       //, boxPts_test( mode, opt )      
       , calc_shrink_test( mode, opt )  //[math]     
       , calc_flat_test( mode, opt )    
       , calc_func_test( mode, opt ) 
       , centroidPt_test( mode, opt ) //[geo]
       //, chainPts0_test( mode, opt ) //[geo]////
       //, chainPts_test( mode, opt ) //[geo]////
       , cirfrags_test( mode, opt ) //[geo]
       //, _colorPts_test( mode, opt ) //[echo] ////
       , combine_test( mode, opt ) //[core]
       , coordPts_test( mode, opt ) //[geo]
       ////, iscoord_test( mode, opt )
       ////, tocoord_test( mode, opt )
       , cornerPt_test( mode, opt ) //[geo]
       , countArr_test( mode, opt ) //[core]  
       , countInt_test( mode, opt ) //[core]  
       , countNum_test( mode, opt ) //[core]  
       , countStr_test( mode, opt ) //[core]  
       , countType_test( mode, opt ) //[core]
       , cubePts_test( mode, opt ) //[geo]
       , deeprep_test( mode, opt ) //[core] 
       , del_test( mode, opt )  //[core]
       , delkey_test( mode, opt )  //[core]
       , dels_test( mode, opt )  //[core]
       , det_test( mode, opt )  //[math]
       , dist_test( mode, opt ) //[geo] 
       , distPtPl_test( mode, opt )  //[geo]
//       , distx_test( mode, opt )  
//       , disty_test( mode, opt )  
//       , distz_test( mode, opt )
       //, dor_test( mode, opt ) /////
       , dot_test( mode, opt ) //[math]
       , echoblock_test( mode, opt ) //[echo]
       , echo__test( mode, opt ) //[echo]
       , echofh_test( mode, opt )//[echo]
       , echoh_test( mode, opt )//[echo]
       , endwith_test( mode, opt ) //[insp]
       , endword_test( mode, opt ) //[match]
       , eval_test( mode, opt ) //[eval]
       , evalarr_test( mode, opt ) // [eval] //most complicated recur
       , expandPts_test( mode, opt ) //[geo]
       , fac_test( mode, opt ) //[match]
       , faces_test( mode, opt ) //[geo]
       , fidx_test( mode, opt )  //[rng]
        , flatten_test( mode, opt ) //[core]
        , _f_test( mode, opt ) // [echo]
        , _fmt_test( mode, opt ) // [echo]
        , _fmth_test( mode, opt ) // [echo]
        , ftin2in_test( mode, opt ) // [geo]
        , get_test( mode, opt ) //[rng]
        , getdeci_test( mode, opt ) //[core]
        , getSideFaces_test( mode, opt ) //[geo]
        ////, getuni_test( mode, opt ) // SLOW ...
       // , gtype_test( mode, opt )// [geo] /////
        ,_h_test( mode, opt ) //[echo]
        , has_test( mode, opt ) //[insp]
        , hash_test( mode, opt ) //[core]
        , hashex_test( mode, opt )//[core]
        , hashkvs_test( mode, opt )//[core]
        , haskey_test( mode, opt )//[core]
        , incenterPt_test( mode, opt ) //[geo]
        , idx_test( mode, opt )  //[rng]           
        , index_test( mode, opt )  //[rng]          
        , inrange_test( mode, opt ) //[rng]
        , int_test( mode, opt )  //[insp]
        //, intsec( mode, opt ) //[geo] ////
        , is0_test( mode, opt )             
        , is90_test( mode, opt )            
        , isarr_test( mode, opt )            
        , isat_test( mode, opt ) 
        //, isball_test( mode, opt )/////
        , isbool_test( mode, opt ) 
        //, iscir_test( mode, opt ) /////
        , isequal_test( mode, opt )  
        , isfloat_test( mode, opt )  
        , ishash_test( mode, opt )  
        , isint_test( mode, opt )  
        , isln_test( mode, opt )  
        , isnum_test( mode, opt )  
        , isOnPlane_test( mode, opt ) 
        , isparal_test( mode, opt )  
        , ispl_test( mode, opt )  
        , ispt_test( mode, opt )  
        //, ispts_test( mode, opt )  /////
        , isSameSide_test( mode, opt )  
        , isstr_test( mode, opt )  
        , istype_test( mode, opt )  
        , join_test( mode, opt )  
        , joinarr_test( mode, opt )  
        , keys_test( mode, opt )  //[core]
        , kidx_test( mode, opt )  //[core]
        , last_test( mode, opt )  // [rng]
        , lcase_test( mode, opt ) //[core]
        //, lineCrossPt_test( mode, opt )
        //, lineCrossPts_test( mode, opt )
        , linePts_test( mode, opt ) //[geo]
        , longside_test( mode, opt ) //[geo]
        
        //, lpCrossPt_test( mode ,opt ) // Do we wanna keep this ?
        
       , match_test( mode, opt ) //[match]            
       , match_at_test( mode, opt ) //[match]             
       , match_nps_at_test( mode, opt ) //[match]  
       , match_ps_at_test( mode, opt ) //[match]            
       , match_rp_test( mode, opt ) //[match]  
       , _mdoc_test( mode, opt ) //[echo]
       , midPt_test( mode, opt ) //[geo]
       , mod_test( mode, opt )  //[math]
       , normal_test( mode, opt )  //[geo]           
       , normalLn_test( mode, opt ) //[geo]            
       , normalPt_test( mode, opt ) //[geo]            
       , normalPts_test( mode, opt ) //[geo]
       //, notall_test( mode, opt )/////
       , num_test( mode, opt )  //[core]
       , numstr_test( mode, opt ) //[core]             
       , onlinePt_test( mode, opt )  //[geo]           
       , onlinePts_test( mode, opt )  //[geo]           
       , or_test( mode, opt )  //[core]           
       , othoPt_test( mode, opt ) //[geo]
      //// , packuni_test( mode, opt ) 
       , parsedoc_test( mode, opt )             
       , permute_test( mode, opt )             
       , pick_test( mode, opt )   
       ///, planeAt_test( mode, opt ) ////
       , planecoefs_test( mode, opt )             
      // , planePts_test( mode, opt )////
      /// , plat_test( mode, opt ) //////
       , ppEach_test( mode, opt )  //[core]
      // , popt_test( mode, opt ) ////
       , pqr90_test( mode, opt )  //[geo]           
       , prod_test( mode, opt )             
       , projPt_test( mode, opt ) //[geo]            
       , quadrant_test( mode, opt ) //[geo]            
       , quicksort_test( mode, opt ) //[core]            
       , rand_test( mode, opt )   //[core]          
       , randc_test( mode, opt )  //[core]
       //, randchain_test( mode, opt )
       , randPt_test( mode, opt ) //[geo]            
       , randPts_test( mode, opt ) //[geo]            
       , randInPlanePt_test( mode, opt ) //[geo]
       , randInPlanePts_test( mode, opt ) //[geo]
       , randOnPlanePt_test( mode, opt ) //[geo]
       , randOnPlanePts_test( mode, opt )   //[geo]         
       , randRightAnglePts_test( mode, opt ) //[geo]            
       , range_test( mode, opt )             
       , ranges_test( mode, opt )             
       , repeat_test( mode, opt )  //[core]           
       , replace_test( mode, opt ) //[core]            
       , reverse_test( mode, opt ) //[core]            
       , rodfaces_test( mode, opt )             
       , rodPts_test( mode, opt )        
       , rodsidefaces_test( mode, opt ) 
       
       , roll_test( mode, opt )   //[core]  
       //, rotpqr_test( mode, opt )       
       , rotPt_test( mode, opt )    //[geo]         
       , rotPts_test( mode, opt )  //[geo]           
       , run_test( mode, opt )  //[core]

       , _s_test( mode, opt )             
       , _s2_test( mode, opt ) 
       //,scomp( mode, opt )       
       , sel_test( mode, opt )             
       , shift_test( mode, opt )   //[core]          
       , shortside_test( mode, opt )             
       , shrink_test( mode, opt )  //[core]           
       , shuffle_test( mode, opt ) //[core]            
       , sinpqr_test( mode, opt )             
       , slice_test( mode, opt )   //[core]          
       , slope_test( mode, opt )
       , sopt_test( mode, opt )     /////////?????  
       , split_test( mode, opt )  //[core]           
       , splits_test( mode, opt )  //[core]           
       , squarePts_test( mode, opt ) //[geo]            
       , sum_test( mode, opt )             
       , sumto_test( mode, opt )             
       , switch_test( mode, opt )  //[core]           
       , symmedPt_test( mode, opt )  //[geo]           
       //, tanglnPt_test( mode, opt )
      // , toHtmlColor_test( mode, opt ) /////
       , transpose_test( mode, opt )   //[core]          
       , trim_test( mode, opt )    //[core]         
       , trfPts_test( mode, opt )  
       //, trslN_test( mode, opt )       
       , twistangle_test( mode, opt )             
       , type_test( mode, opt )             
       //, typeplp_test( mode, opt )  /////////           
       , ucase_test( mode, opt )   //[core]          
       , uconv_test( mode, opt )             
       , update_test( mode, opt )  //[core]           
       , updates_test( mode, opt ) //[core]
       //, uopt_test( mode, opt )       
       , uv_test( mode, opt )        
       //, uvN_test( mode, opt )     
       , vals_test( mode, opt )    //[core]         
//    
    ];    
    
    doctests( title=title
            , subtitle= str("scadx_core version: ", SCADX_CORE_VERSION)
            , mode=MODE
            , results=results
            , htmlstyle=htmlstyle );
}
    
//do_tests( title="Doctests for scadx_core"
/////   , mode=MODE );
//   , mode=11);

//===========================================
//
//           Version / History
//
//===========================================

scadx_core_test_ver=[
["20150207-1", "Separated from original scadx_core.scad"]
,["20150420-1", "Revised all to fit new doctest.scad. Time to run all tests at mode 112: 3 min"]
,["20150516-1", "Revised all to fit new doctest2()"]
,["20150524-1", "Detailed doc and tests for _f and _s2"]
,["20150524-2", "Minor bugfixes. Took 40sec with mode=11: 174 funcs, 1006 tests and 2 fail(s)."]
,["20150601-1", "evalarr_test; fix _f % error"]


];

SCADX_CORE_TEST_VERSION = last(scadx_core_test_ver)[0];

//echo_( "<code>||.........  <b>scadx core test version: {_}</b>  .........||</code>",[SCADX_CORE_TEST_VERSION]);
//echo_( "<code>||Last update: {_} ||</code>",[join(shrink(scadx_core_test_ver[0])," | ")]);    


    
    